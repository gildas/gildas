subroutine write_sdm_polarization(error)
  !---------------------------------------------------------------------
  ! Write the polarization SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_polarization
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (PolarizationRow) :: pRow
  type (PolarizationKey) :: pKey
  character sdmTable*12
  parameter (sdmTable='Polarization')
  integer :: ier, ic, jc, ia
  integer, parameter :: minalloc=2
  logical :: single(mrlband)
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     simple version for current status of CLIC: one product only; for
  !     completeness we enter also an entry for the total powers, which
  !     will be also there when we have more correlation products...
  !
  !     Also we have at this time at Bure only one polarization
  !     description, in the future it will be per correlator unit
  !     (baseband).
  !
  !
  !     Now for Bure data the numCorr is either 1 or 2 (not yet 4).  some
  !     units are H, some are V. If two units are at linked to the same
  !     spectral window, one in H and the other in V, then they are a
  !     couple, with numCorr 2.
  !
  !     search for couples
  !c      numCorr is at most 2...
  do ic = 1, r_lband
    single(ic) = .true.
  enddo
  v_lband = 0
  do ic = 1, r_lband
    if (r_lpolentry(1,ic).le.0) then
      do ia=1, r_nant
        r_lpolentry(ia,ic) = 1
      enddo
    endif
    if (ic.lt.r_lband) then
      do jc = ic+1, r_lband
        if (spectralWindow_Id(1,ic,1).eq.spectralWindow_Id(1,jc,1   &
          )) then
          pRow%numCorr = 2
          call allocPolarizationRow(pRow, pRow%numCorr,  error)
          if (error) return
          pRow%corrType = (/StokesParameter_XX,   &
            StokesParameter_YY/)
          pRow%corrProduct(1,1) = PolarizationType_X
          pRow%corrProduct(2,1) = PolarizationType_X
          pRow%corrProduct(1,2) = PolarizationType_Y
          pRow%corrProduct(2,2) = PolarizationType_Y
          call addPolarizationRow(pKey, pRow, error)
          if (error) return
          polarization_Id(ic) = pKey%polarizationId
          polarization_Id(jc) = pKey%polarizationId
          single(ic) = .false.
          single(jc) = .false.
          v_lband = v_lband+1
          !print *, 'D v_lband, polarization_Id(v_lband), ic, jc ', &
          !     v_lband, polarization_Id(v_lband), ic, jc
          !     I know, this is wrong. It can be reversed depending on the status
          !     of the quarters...  for test data it does not matter.
          !            print *, ' r_lpolentry(1,ic) ', r_lpolentry(1,ic)
          !            print *, ' r_lpolentry(1,jc) ', r_lpolentry(1,jc)
          v_bands(v_lband, r_lpolentry(1,ic)) = ic
          v_bands(v_lband, r_lpolentry(1,jc)) = jc
          v_ncorr(v_lband) = 2
        endif
      enddo
    endif
    if (single(ic)) then
      pRow%numCorr = 1
      call allocPolarizationRow(pRow, pRow%numCorr,  error)
      if (error) return
      if (r_lpolentry(1,ic).eq.1) then
        pRow%corrType = StokesParameter_XX
        pRow%corrProduct(1,1) = PolarizationType_X
        pRow%corrProduct(2,1) = PolarizationType_X
      elseif (r_lpolentry(1,ic).eq.2) then
        pRow%corrType = StokesParameter_YY
        pRow%corrProduct(1,1) = PolarizationType_Y
        pRow%corrProduct(2,1) = PolarizationType_Y
      endif
      call addPolarizationRow(pKey, pRow, error)
      if (error) return
      v_lband = v_lband+1
      polarization_Id(v_lband) = pKey%polarizationId
      !            print *, 'S v_lband, polarization_Id(v_lband), ic:  '
       !           print *, v_lband, polarization_Id(v_lband), ic
      !     I know, this is wrong. It can be reversed depending on the status
      !     of the quarters...  for test data it does not matter.
      !            print *, ' r_lpolentry(1,ic) ', r_lpolentry(1,ic)
      !!v_bands(v_lband, r_lpolentry(1,ic)) = ic
      v_bands(v_lband, 1) = ic
      v_ncorr(v_lband) = 1
    endif
  enddo
  !$$$      print *, v_ncorr
  !
  ! now for the total powers
  num_CorrTotPow = r_npol_rec
  pRow%numCorr = num_CorrTotPow
  call allocPolarizationRow(pRow, num_CorrTotPow,  error)
  if (error) return
  ! (no need to reallocate)
  if (num_CorrTotPow.eq.2) then
    pRow%corrType = (/StokesParameter_XX,   &
      StokesParameter_YY/)
    pRow%corrProduct(1,1) = PolarizationType_X
    pRow%corrProduct(2,1) = PolarizationType_X
    pRow%corrProduct(1,2) = PolarizationType_Y
    pRow%corrProduct(2,2) = PolarizationType_Y
  else
    pRow%corrType = StokesParameter_XX
    pRow%corrProduct(1,1) = PolarizationType_X
    pRow%corrProduct(2,1) = PolarizationType_X
  endif
  !
  call addPolarizationRow(pKey, pRow, error)
  if (error) return
  totPowPolarization_Id = pKey%polarizationId
  !$$$      print *, 'totPowPolarization_Id ', totPowPolarization_Id
  call sdmMessage(1,1,sdmTable ,'Written.')
!
end subroutine write_sdm_polarization
!---------------------------------------------------------------------
subroutine get_sdm_polarization(error)
  !---------------------------------------------------------------------
  ! Write the polarization SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_polarization
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (PolarizationRow) :: pRow
  type (PolarizationKey) :: pKey
  integer :: ier, ipp, ivc, ia, ic
  integer, parameter :: mcorrp = 4
  character sdmTable*12
  parameter (sdmTable='Polarization')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     simple version for current status of CLIC: one product only; for
  !     completeness we enter also an entry for the total powers, which
  !     will be also there when we have more correlation products...
  !
  !     Also we have at this time at Bure only one polarization
  !     description, in the future it will be per correlator unit
  !     (baseband).
  !
  !     *TBD* may corrType is  1 for 1mm receiver (or the reverse)...
  !
  !     most of this information cannot be put in Bure data format at this
  !     time (2006-01-27)
  pRow%numCorr = mcorrp
  call allocPolarizationRow(pRow, error)
  if (error) return
  pKey%PolarizationId = polarization_Id(1)
  call getPolarizationRow(pKey, pRow, error)
  r_npol_rec = 1
  if (pRow%numCorr.gt.1) then
    !call sdmMessageI(6,2,sdmTable   &
    !  ,'Too many correlation products: ', pRow%numCorr)
    !error =.true.
    !return
    !call sdmMessage(6,2,sdmTable,'Only first product will be used')
    r_npol_rec = 2
  endif
  num_Corr = pRow%numCorr
  

  ! move this to get polarization. define the r_lvand here
  r_lband = num_Corr *  v_lband
  r_nband = r_lband
  r_lpolmode = 1 !!! num_corr
  ic = 1
  do ivc=1, v_lband
    v_ncorr(ivc) = num_Corr
    do ipp = 1, num_corr
      if (pRow%corrType(ipp) .eq. StokesParameter_XX) then
        do ia=1, r_nant
          r_lpolentry(ia,ic) = 1 
        enddo
        v_bands(ivc, 1) = ic
      elseif (pRow%corrType(ipp) .eq. StokesParameter_YY) then
        do ia=1, r_nant
          r_lpolentry(ia,ic) = 2 
        enddo
        v_bands(ivc, 2) = ic
      endif
      ic = ic + 1
    enddo
  enddo
  !
  ! for the total powers: use another routine if needed...
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine get_sdm_polarization
