subroutine read_fitsti(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Support FITS /FROM_ALMA_TI command
  ! Convert  a FITS file in ALMA-TI FITS format to Plateau de Bure Format
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_dcomp.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: check, addrecord
  integer(kind=address_length) :: data_in, ip_data, kin, kout
  integer(kind=data_length) :: ndata, maxdata
  character(len=132) :: fich, argum
  integer ::  unit, status, l, hdutype, readonly
  integer ::  readwrite, hdunum, i, nch, oldnum, newnum
  integer ::  ir, nout, ifeed, nfeed, fitsfirstscan, fitslastscan
  parameter (readonly=0, readwrite=1)
  character(len=80) :: chain,comment
  character(len=16) :: extname
  character(len=11) :: pname
  character(len=7) :: done
  parameter (pname='READ_FITSTI')
  real :: buffer(4*mnbas*mcch)
  data maxdata/0/
  save oldnum, maxdata, data_in, ip_data, fitsfirstscan,   &
    fitslastscan
  !------------------------------------------------------------------------
  ! Code:
  status = 0
  fitsfirstscan = 0
  fitslastscan = 0
  call sic_delvariable('FITS_FIRST_SCAN',.false.,error)
  call sic_delvariable('FITS_LAST_SCAN',.false.,error)
  error = .false.
  call sic_def_inte('FITS_FIRST_SCAN',fitsfirstscan,0,1,.true.,   &
    error)
  call sic_def_inte('FITS_LAST_SCAN',fitslastscan,0,1,.true.,   &
    error)
  !
  ! Verify if any input file
  oldnum = -1
  call check_output_file(error)
  if (error) return
  write_mode = 'NEW'
  check = .true.
  !
  ! Get file name
  argum = 'clicFile'
  call sic_ch(line,0,1,argum,nch,.false.,error)
  if (error) return
  !      ifeed = 1
  !      CALL SIC_i4(LINE,0,1,ifeed,.FALSE.,ERROR)
  !      IF (ERROR) RETURN
  !      ifeed = max(1,min(2,ifeed))
  !      print *, 'Feed ',ifeed
  call sic_parsef(argum,fich,' ','.fits')
  call ftgiou(unit, status)
  if (status .gt. 0) goto 99
  call ftnopn(unit,fich,readonly,status)
  if (status .gt. 0) goto 99
  call ftthdu(unit,hdunum,status)
  if (status .gt. 0) goto 99
  l = lenc(fich)
  print *, 'File ',fich(1:l), hdunum, ' HDUs'
  do ifeed = 1, 2
    do i=1, hdunum
      call ftmahd(unit,i,hdutype,status)
      if (status .gt. 0) goto 99
      if (hdutype.eq.2) then
        call ftgkyj(unit,'OBS-NUM',newnum,comment,status)
        call get_fioerror(pname,'OBS-NUM',status,error,.false.)
        call ftgkys(unit,'EXTNAME',extname,comment,status)
        call get_fioerror(pname,'EXTAME',status,error,.false.)
        if (error) return
        print *, 'EXTNAME ',extname
        call ftgkyj(unit,'NO_FEED',nfeed,comment,status)
        call get_fioerror(pname,'NO_FEED',status,error,.true.)
        if (ifeed.gt.nfeed) goto 50
        !
        if (newnum.ne.oldnum) then
          if (oldnum.gt.0) then
            r_presec(rfset_sec)= .true.
            r_presec(contset_sec)= .true.
            r_presec(lineset_sec)= .true.
            r_presec(atparm_sec)= .true.
            r_presec(atmon_sec)= .true.
            r_presec(scanning_sec)= .true.
            r_presec(alma_sec)= .true.
            r_presec(atparm_sec)= .true.
            r_presec(abpcal_sec)= .true.
            r_presec(aical_sec)= .true.
            r_presec(bpcal_sec)= .true.
            r_presec(ical_sec)= .true.
            r_aicdeg = 3
            r_icdeg = 3
            if (addrecord) then
              call zero_record (r_ldatc,buffer)
              kin = ip_data
              kout = ip_data + r_ndump*r_ldump
              do ir=1, r_ndump
                kin = ip_data+(ir-1)*r_ldump
                call decode_header (memory(kin))
                call add_record (r_ldatc,memory(kin+r_ldpar),   &
                  buffer)
              enddo
              call output_record (r_ldatc,memory(kout+r_ldpar),   &
                buffer, nout)
              call r4tor4 (dcomp_void, dh_void, m_dh)
              call encode_header (memory(kout))
              addrecord = .false.
            endif
            new_receivers = .true.
            call ipb_write(write_mode,check,error)
            if (error) goto 99
            call wdata (ndata,memory(ip_data),.true.,error)
            if (error) goto 99
            call ipb_close(error)
            if (error) goto 99
            write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
              ' '//done(write_mode)//' (',r_ndump,' records)'
            call message(4,1,'CLIC_FITS_TI',chain(1:50))
          endif
          oldnum = newnum
          if (extname.ne.'DATAPAR-ALMATI') then
            call message(4,1,'FITS_TI_CLIC',   &
              'DATAPAR-ALMATI must come first in an observation')
            goto 99
          else
            call check_fits_datapar(unit, ndata, addrecord, error)
            if (ndata.gt.maxdata) then
              if (maxdata.gt.0) then
                call free_vm(maxdata,data_in)
              endif
              maxdata = ndata
              if (sic_getvm(maxdata,data_in).ne.1) goto 999
            endif
            ip_data = gag_pointer(data_in,memory)
            call get_fits_datapar(unit,ndata,memory(ip_data),   &
              addrecord,ifeed,error)
            if (error) return
            if (fitsfirstscan.le.0) then
              fitsfirstscan = r_scan
            endif
            fitslastscan = r_scan
          endif
        elseif (extname.eq.'CALIBR-ALMATI') then
          call get_fits_calibr(unit,ifeed,error)
        elseif (extname.eq.'GAIN-ALMATI') then
          call get_fits_gain(unit,error)
        elseif (extname.eq.'PASSBAND-ALMATI') then
          call get_fits_passband(unit,error)
        elseif (extname.eq.'AUTODATA-ALMATI' .or.   &
          extname.eq.'CORRDATA-ALMATI') then
          call get_fits_data(unit,ndata,memory(ip_data),error)
        elseif (extname.eq.'HOLODATA-ALMATI') then
          call get_fits_holodata(unit,ndata,memory(ip_data),error)
        endif
      endif
50    continue
    enddo
    oldnum=9999999
  enddo
  !
  ! Finish
  if (addrecord) then
    call zero_record (r_ldatc,buffer)
    kin = ip_data
    kout = ip_data + r_ndump*r_ldump
    do ir=1, r_ndump
      kin = ip_data+(ir-1)*r_ldump
      call decode_header (memory(kin))
      call add_record (r_ldatc,memory(kin+r_ldpar),   &
        buffer)
    enddo
    call output_record (r_ldatc,memory(kout+r_ldpar),   &
      buffer, nout)
    call r4tor4 (dcomp_void, dh_void, m_dh)
    call encode_header (memory(kout))
    addrecord = .false.
  endif
  !--
  new_receivers = .true.
  call ipb_write(write_mode,check,error)
  if (error) goto 99
  call wdata (ndata,memory(ip_data),.true.,error)
  if (error) goto 99
  call ipb_close(error)
  if (error) goto 99
  write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
    ' '//done(write_mode)//' (',r_ndump,' records)'
  call message(4,1,'CLIC_FITS_TI',chain(1:50))
  print *, 'First and last scan in file were :',   &
    fitsfirstscan, fitslastscan
  goto 999
  !
  ! Now loop on current index.
99 call printerror('CLIC_FITS_TI',status)
  status = 0
999 call ftclos(unit, status)
  call ftfiou(unit, status)
  if (status .gt. 0) call printerror('CLIC_FITS_TI',status)
  error = status.gt.0
  return
end subroutine read_fitsti
