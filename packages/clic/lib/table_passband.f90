subroutine sg_zpassband(line, error)
  use gildas_def
  use classic_api  
  character*(*), intent(inout) :: line
  logical, intent(inout) :: error
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !
  character(len=filename_length) :: file
  integer :: iband, i, nc
  integer :: nsubant                ! Number of antennas
  integer :: subant(mnant)          ! selected antennas in table
  !
  call sic_ch(line,0,1,file,nc,.true.,error)
  if (error) return
  !
  nsubant = r_nant
  do i=1, nsubant
    subant(i) = i
  enddo
  iband = 1
  call table_passband(file, iband, nsubant, subant, error)
end subroutine sg_zpassband
!
subroutine table_passband(file, iband, nsubant, subant, error)
  use image_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! Write the optional passband calibration fits table.
  ! assumes NO_POL=1
  !---------------------------------------------------------------------
  integer :: iband
  character(len=*) :: file
  integer :: nsubant                ! Number of antennas
  integer :: subant(nsubant)        ! selected antennas in table
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  !
  type (gildas) :: htab
  integer isub, j, k, iant
  !
  call gildas_null(htab, type= 'TABLE')
  htab%file = file
  htab%gil%dim(1) = 2 + 2 * (r_abpfdeg+1)  ! 
  htab%gil%dim(2) = nsubant
  !
  call gdf_allocate (htab, error)
  !
  do isub=1,nsubant
    iant = subant(isub)
    ! Put the band average value first
    k = 1
    call r4tor4 (r_abpcsba(1,iband,iant), htab%r2d(1,isub), 2)
    k = k+2
    ! Then the amplitude
    do j=0, r_abpfdeg
      htab%r2d(k,isub) = r_abpfamp(1,iband,iant,j)
      k = k+1
    enddo
    ! Then the phase
    do j=0, r_abpfdeg
      htab%r2d(k,isub) = r_abpfpha(1,iband,iant,j)
      k = k+1
    enddo
  enddo
  call gdf_write_image (htab, htab%r2d, error)
end subroutine table_passband
