subroutine get_fits_holodata(unit,ndata,data,error)
  use gildas_def
  use classic_api
  integer :: unit                   !
  integer(kind=data_length) :: ndata                  !
  integer :: data(ndata)            !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=data_length) :: c_offset, l_offset, h_offset
  character(len=8) :: ctype
  character(len=80) :: comment
  character(len=16) :: extname
  character(len=12) :: cf
  real*8 :: vv, ff,  ifval, ifinc, fobs, x
  real :: df, dvv, ifref, vref
  integer :: status, itest, r_npol, iband, isb, nch, ksb
  integer :: nrows, irow, kr, ntch, ich, k0, k1, kd, kh
  integer :: nsb
  integer :: integnum_col, holoss_col, holorr_col, holoqq_col
  integer :: holosr_col, holosq_col, holoqr_col
  integer :: ia, ja, ib, nullval, naxis, maxis
  real :: holoss, holorr, holoqq, holosr, holosq, holoqr
  parameter (maxis=5)
  integer :: naxes(maxis) , isb2 , icol(2), idtable, i
  logical :: anyf
  character(len=1) :: cd,cfr
  real :: rdata(2)
  !---------------------------------------------------------------------------
  r_presec(bpcal_sec)= .true.
  r_presec(ical_sec)= .true.
  r_iant(1) = 1
  r_jant(1) = 1
  r_kant(1) = 1
  status = 0
  r_flux = 1.0                 ! as a default
  ! Get  header keywords
  call ftgkys(unit,'EXTNAME',extname,comment,status)
  if (status .gt. 0) go to 99
  call ftgkyj(unit,'SCAN-NUM',itest,comment,status)
  if (status .gt. 0) go to 99
  if (itest.ne.r_scan) then
    call message(8,4,'GET_FITS_HOLODATA','Wrong scan number')
    error = .true.
    return
  endif
  call ftgkyj(unit,'TABLEID',idtable,comment,status)
  if (status .gt. 0) go to 99
  call ftgkyj(unit,'NO_POL',r_npol,comment,status)
  if (status .gt. 0) go to 99
  if (r_npol.ne.1) then
    call message(8,4,'GET_FITS_HOLODATA',   &
      'CLIC not yet ready for more than 1 polarization')
    error = .true.
  endif
  call ftgkyj(unit,'BASEBAND',iband,comment,status)
  if (status .gt. 0) go to 99
  do i=1,mcch
    r_cfwid(i) = 1.
  enddo
  !
  ! Get the columns numbers
  call ftgcno(unit,.false.,'INTEGNUM',integnum_col,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_HOLODATA','FTGCNO[INTEGNUM]',   &
      status,error)
    return
  endif
  call ftgcno(unit,.false.,'HOLOSS',holoss_col,status)
  call ftgcno(unit,.false.,'HOLORR',holorr_col,status)
  call ftgcno(unit,.false.,'HOLOQQ',holoqq_col,status)
  call ftgcno(unit,.false.,'HOLOSR',holosr_col,status)
  call ftgcno(unit,.false.,'HOLOSQ',holosq_col,status)
  call ftgcno(unit,.false.,'HOLOQR',holoqr_col,status)
  ! These must be present.
  !      CALL FTGKYJ(UNIT,'NO_SIDE',NSB,COMMENT,STATUS)
  !      IF (STATUS.GT.0) THEN
  !         CALL FIO_PRINTERROR('GET_FITS_HOLODATA','FTGKYJ[NO_SIDE]',
  !     $   STATUS,ERROR)
  !         RETURN
  !      ENDIF
  call ftgkye(unit,'TRANDIST',r_trandist,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_HOLODATA','FTGKYE[TRANDIST]',   &
      status,error)
    return
  endif
  call ftgkyd(unit,'TRANFREQ',r_restf,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_HOLODATA','FTGKYD[TRANFREQ]',   &
      status,error)
    return
  endif
  ! in MHz
  r_restf = r_restf/1d6
  r_flo1 = r_restf
  r_fif1 = 0
  call ftgkye(unit,'TRANFOCU',r_tranfocu,comment,status)
  ! keep this in in meters .
  if (status.gt.0) then
    call fio_printerror('GET_FITS_HOLODATA','FTGKYE[TRANFOCU]',   &
      status,error)
    return
  endif
  call ftgkye(unit,'IFLUX',r_flux,comment,status)
  status = 0
  !
  ! Get the number of rows
  call ftgnrw(unit,nrows,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_HOLODATA','FTGNRW',   &
      status,error)
    return
  endif
  if (nrows.ne.r_ndump) then
    print *, 'nrows, r_ndump  ', nrows, r_ndump
  endif
  if (r_teles.eq.'VTX-ALMATI') then
    r_trandist = 311.6d0
    r_tranfocu = 0.091d0
    r_antennaname(1) = 'VTX-ALMA'
    r_antennatype(1) = 'ALMA/Vertex Prototype'
    r_dishdiameter(1) = 12.
    r_execblock = 'Old AEG Data'
  elseif (r_teles.eq.'AEC-ALMATI') then
    r_trandist = 298.9d0
    r_tranfocu = 0.091d0
    r_antennaname(1) = 'AEC-ALMA'
    r_antennatype(1) = 'ALMA/AEC Prototype'
    r_dishdiameter(1) = 12.
    r_execblock = 'Old AEG Data'
  endif
  !
  ! Loop on rows
  do irow = 1, nrows
    call ftgcvj(unit,integnum_col,irow,1,1,nullval,   &
      kr,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_HOLODATA','FTGCVJ[INTEGNUM]',   &
        status,error)
      return
    endif
    !
    ! continuum, not averaged
    !         K0 = 1+C_OFFSET(IROW)
    k0 = 1+c_offset(kr)
    call ftgcve(unit,holoss_col,irow,1,1,nullval,   &
      holoss,anyf,status)
    call ftgcve(unit,holorr_col,irow,1,1,nullval,   &
      holorr,anyf,status)
    call ftgcve(unit,holoqq_col,irow,1,1,nullval,   &
      holoqq,anyf,status)
    call ftgcve(unit,holosr_col,irow,1,1,nullval,   &
      holosr,anyf,status)
    call ftgcve(unit,holosq_col,irow,1,1,nullval,   &
      holosq,anyf,status)
    call ftgcve(unit,holoqr_col,irow,1,1,nullval,   &
      holoqr,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_HOLODATA','FTGCVE[HOLOXX]',   &
        status,error)
      return
    endif
    kh = 1+ h_offset(kr)
    if (kh+r_ldpar.gt.ndata) then
      print *, '** error row ',kr, kh, ndata
      return
    endif
    call decode_header(data(kh))
    ! 1 subband, 2 band ,1 baseline (2 bands for compatibility)
    ! as clic poorly handles data wityh one sideband only .
    ! LSB is used to hold data with phases reversed.
    ! More precise formula is TBD
    holoqr= holoqr*1.0035
    holoqq= holoqq*1.0035
    if (k0+3.le.ndata) then
      holosr = holosr/holorr
      holoqr = holoqr/holorr
      call r4tor4(holosr,data(k0),1)   ! true SR in USB (real)
      call r4tor4(holoqr,data(k0+1),1) ! true QR in USB (imag)
      ! reversed phases in LSB
      !            CALL R4TOR4(HOLOSR,DATA(K0+2),1) !
      !            CALL R4TOR4(-HOLOQR,DATA(K0+3),1) !
      ! reference signal in LSB
      call r4tor4(holorr,data(k0+2),1) !
      call r4tor4(0.0,data(k0+3),1)    !
    else
      call message(6,2,'GET_FITS_HOLODATA','Too many rows')
    endif
    dh_test0(1) = holorr       ! true rr
    dh_test0(2) = holoss       ! true ss
    dh_test0(3) = holoqq       ! true qq
    dh_test0(4) = holosq       ! true sq
    dh_aflag(1) = 0
    dh_bflag(1) = 0
    call encode_header(data(kh))
  enddo
  return
  !
99 call printerror('GET_FITS_DATA',status)
  call message(6,2,'GET_FITS_DATA',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_holodata
