subroutine write_sdm_holography(error)
  !---------------------------------------------------------------------
  ! Write the holography SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_holography
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (HolographyRow) :: hRow
  type (HolographyKey) :: hKey
  character sdmTable*12
  parameter (sdmTable='Holography')
  integer :: ier, ic
  integer, parameter :: minalloc=2
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !     This table replaces Polarization for Holography Data sets.
  !     There are numCorr=6 types of products, SS, RR, QQ, SR, SQ, QR.
  !
  num_Corr = 6
  hRow%numCorr = num_Corr
  !     *TBD* maybe corrType is  1 for 1mm receiver (or the reverse)...
!!  call allocHolographyRow(hRow, hRow%numCorr,  error)
  call allocHolographyRow(hRow, error)
  if (error) return
  !      holo_type= (/'QQ', 'QR', 'QS', 'RR', 'RS', 'SS'/)
  holo_type = (/ HolographyChannelType_Q2, HolographyChannelType_QR,   &
    HolographyChannelType_QS, HolographyChannelType_R2,   &
    HolographyChannelType_RS, HolographyChannelType_S2 /)
  do ic = 1, num_corr
    hRow%type(ic) = holo_type(ic)
  enddo
  !
  !     These parameters were not in the FITS data files, so we reset them
  !     when reading the original hpb files to create test ASDMs
  if (r_teles.eq.'VTX-ALMATI') then
    hRow%distance = 311.6d0
    hRow%focus = 0.091d0
  elseif (r_teles.eq.'AEC-ALMATI') then
    hRow%distance = 298.9d0
    hRow%focus = 0.093d0
  else
    hRow%distance = r_trandist
    hRow%focus = r_tranfocu
  endif
!!!   hRow%flagRow = .false.
  call addHolographyRow(hKey, hRow, error)
  if (error) return
  polarization_Id = hKey%holographyId
  call sdmMessage(1,1,sdmTable ,'Written.')
!
end subroutine write_sdm_holography
!---------------------------------------------------------------------
subroutine get_sdm_holography(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the holography SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_holography
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (HolographyRow) :: hRow
  type (HolographyKey) :: hKey
  integer :: ier, ic
  integer, parameter :: mcorrp = 4
  character sdmTable*12
  parameter (sdmTable='Holography')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  call allocHolographyRow(hRow, 6,  error)
  if (error) return
  hKey%HolographyId = polarization_Id(1)
  call getHolographyRow(hKey, hRow, error)
  if (hRow%numCorr.ne.6) then
    call sdmMessageI(6,2,sdmTable   &
      ,'Wrong numCorr for Holography: ', hRow%numCorr)
    error = .true.
    return
  endif
  num_Corr = hRow%numCorr
  do ic=1, num_corr
    holo_type(ic) = hRow%type(ic)
    call sic_upper(holo_type(ic))
  enddo
  r_trandist = hRow%distance
  r_tranfocu = hRow%focus
  ! HOLO PATCH according to Ralph's email...
  ! is no more OK on Dec 15!!!
  !$$$      holo_type= (/'QQ', 'QR', 'QS', 'RR', 'RS', 'SS'/)
  holo_type = (/ HolographyChannelType_Q2, HolographyChannelType_QR,   &
    HolographyChannelType_QS, HolographyChannelType_R2,   &
    HolographyChannelType_RS, HolographyChannelType_S2 /)
  !      print *, r_antennaName(1), r_antennaType(1)
  if (r_antennaType(1).eq.'ALMA/Vertex Prototype') then
    r_trandist = 311.6d0       ! not yet...
  elseif (r_antennaType(1).eq.'ALMA/AEC Prototype') then
    r_trandist = 298.9d0
  endif
  r_tranfocu = 0.091
  ! HOLO PATCH according to Ralph's email...
  !$$$      print *, 'r_trandist, r_tranfocu, holo_type'
  !$$$      print *, r_trandist, r_tranfocu, holo_type
  !     (... ignore flagRow)
  !
  ! for the total powers: use another routine if needed...
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine get_sdm_holography
!---------------------------------------------------------------------
!!! subroutine allocHolographyRow(row, numCorr,  error)
!!!   !---------------------------------------------------------------------
!!!   !     integer, allocatable :: corrType(:)
!!!   !     integer, allocatable :: corrProduct(:,:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Holography
!!!   type(HolographyRow) :: row
!!!   integer :: numCorr, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*12 = 'Holography'
!!!   !
!!!   ! corrType(numCorr)
!!!   if (allocated(row%Type)) then
!!!     deallocate(row%Type, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%type(numCorr), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!---------------------------------------------------------------------
