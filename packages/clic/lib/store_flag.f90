subroutine store_flag(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_index
  !---------------------------------------------------------------------
  !	Stores flags for current index
  ! CLIC	Command CLIC\STORE FLAG f1 f2 ... [/ANTENNA a1 a2 ... ]
  !		[/BASELINE b1 b2 ...] [/RESET]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: end
  integer :: i, afl(mnbas), bfl(mnbas), ir
  logical :: saf(2*mbands,mnant),sbf(2*mbands,mnbas)
  integer(kind=address_length) :: data_in, kin, kh
  integer(kind=data_length) ::  h_offset, ndata
  logical :: reset
  character(len=255) :: out
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  if (.not.sic_present(9,0)) then
    call check_output_file(error)
    if (error) goto 999
    call check_equal_file(error)
    if (error) goto 999
    if (write_mode.eq.'UPDA') call check_equal_file(error)
    if (error) goto 999
  endif
  ! Reset all COMMAND flags: otherwise the system has memory !
  do i=1, mnant
    afl(i) = 0
    saf(:,i) = .false.
  enddo
  do i=1, mnbas
    bfl(i) = 0
    sbf(:,i) = .false.
  enddo
  call get_flags(line,2,2,3,4,-1,-1,afl,bfl,saf,sbf,reset,error)
  if (error) goto 999
  call list_flags(mnant,mnbas,afl,bfl,saf,sbf,out)
  if (out(1:2).eq.'no') then
    call message(8,3,'STORE_FLAG',   &
      'No flags for any antenna or baseline')
    error = .true.
    goto 999
  endif
  if (reset) then
    call message(4,1,'STORE_FLAG',   &
      'Resetting - '//out(1:lenc(out)))
  else
    call message(4,1,'STORE_FLAG',   &
      'Storing - '//out(1:lenc(out)))
  endif
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) goto 999
  end = .false.
  do while (.not.end)
    if (sic_present(9,0)) then
      ! Flag in memory
      call doflags(r_nant,r_nbas,ix_aflag(1,r_xnum),ix_bflag(1,r_xnum), &
                   afl,bfl,reset)
    else
      ! Flag in data
      if (r_presec(file_sec)) then 
        ! Update just the data modifiers for hpb
        call doflags(r_nant,r_nbas,r_dmaflag,r_dmbflag,afl,bfl,reset)
        call dosflags(r_nant,r_nbas,r_dmsaflag,r_dmsbflag,saf,sbf,reset)
      else
        call get_data (ndata,data_in,error)
        if (error) goto 999
        ! Loop on records (including average record, and corrected record)
        kin = gag_pointer(data_in,memory)
        do ir = 1, r_ndump + max(1,r_ndatl)
          kh = kin + h_offset(ir)
          call decode_header (memory(kh))
          call doflags(r_nant,r_nbas,dh_aflag,dh_bflag,afl,bfl,reset)
          call dosflags(r_nant,r_nbas,dh_saflag,dh_sbflag,saf,sbf,reset)
          call encode_header (memory(kh))
          ! Keep a copy of the new flags in the modifier section
          if (ir.eq.r_ndump+1) then
            call r4tor4(dh_aflag,r_dmaflag,r_nant)
            call r4tor4(dh_bflag,r_dmbflag,r_nbas)
            call w4tow4(dh_saflag,r_dmsaflag,r_nant*2*mbands)
            call w4tow4(dh_sbflag,r_dmsbflag,r_nbas*2*mbands)
          endif
        enddo
      endif
      r_presec(modify_sec) = .true.
      !
      ! Update scan
      call write_scan (.true.,error) ! data to be updated
      call loose_data                ! and force new reading 
      if (error) goto 999
    endif
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) goto 999
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_flag
!
subroutine doflags(nant,nbas,af,bf,afl,bfl,reset)
  integer :: nant                   !
  integer :: nbas                   !
  integer :: af(nant)               !
  integer :: bf(nbas)               !
  integer :: afl(nant)              !
  integer :: bfl(nbas)              !
  logical :: reset                  !
  ! Local
  integer :: i
  !-----------------------------------------------------------------------
  !
  ! Loop on antennas
  do i=1, nant
    if (afl(i).ne.0) then
      if (reset) then
        af(i) = iand(af(i),not(afl(i)))
      else
        af(i) = ior(af(i),afl(i))
      endif
    endif
  enddo
  !
  ! Baselines
  do i=1, nbas
    if (bfl(i).ne.0) then
      if (reset) then
        bf(i) = iand(bf(i),not(bfl(i)))
      else
        bf(i) = ior(bf(i),bfl(i))
      endif
    endif
  enddo
  return
end subroutine doflags
!
subroutine dosflags(nant,nbas,af,bf,afl,bfl,reset)
  include 'clic_parameter.inc'
  integer :: nant                   !
  integer :: nbas                   !
  logical :: af(2*mbands,nant)               !
  logical :: bf(2*mbands,nbas)               !
  logical :: afl(2*mbands,nant)              !
  logical :: bfl(2*mbands,nbas)              !
  logical :: reset                  !
  ! Local
  integer :: i,j
  !-----------------------------------------------------------------------
  !
  ! Loop on antennas
  do i=1, nant
    do j =1, 2*mbands
      if (afl(j,i)) then
        if (reset) then
          af(j,i) = .false.
        else
          af(j,i) = .true.
        endif
      endif
    enddo
  enddo
  !
  ! Baselines
  do i=1, nbas
    do j =1, 2*mbands
      if (bfl(j,i)) then
        if (reset) then
          bf(j,i) = .false.
        else
          bf(j,i) = .true.
        endif
      endif
    enddo
  enddo
  return
end subroutine dosflags
!
subroutine store_quality(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  !	Stores data quality for all scans in current index
  ! CLIC	Command CLIC\STORE QUALITY quality
  ! where quality is a keyword on an integer in range 0-9
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: end
  integer :: iqual, narg
  character(len=10) :: qual,argum
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  call check_output_file(error)
  if (error) goto 999
  call check_equal_file(error)
  if (error) goto 999
  !
  call sic_ke(line,0,2,argum,narg,.true.,error)
  if (error) return
  call get_quality(argum,iqual,qual,error)
  call message(4,1,'STORE_QUALITY',   &
    'Storing '//qual(1:lenc(qual))//' quality')
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) goto 999
  end = .false.
  do while (.not.end)
    r_qual = iqual
    r_cqual = quality(iqual)
    !
    ! Update scan
    call write_scan (.false.,error)    ! data not to be updated
    if (error) goto 999
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) goto 999
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_quality
!
subroutine get_quality(argum, iqual, qual, error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! decode quality value from argument argum
  !---------------------------------------------------------------------
  character(len=*) :: argum         !
  integer :: iqual                  !
  character(len=*) :: qual          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  character(len=10) :: qw
  integer :: long
  !-----------------------------------------------------------------------
  read(argum,'(i10)', err=235) long
  goto 236
235 call sic_upper(argum(1:1))
  call sic_lower(argum(2:lenc(argum)))
  call sic_ambigs('GET_QUALITY',argum,qw,long,   &
    quality,10,error)
  if (error) return
  long = long - 1
236 if (long.ge.0 .and. long.le.9) then
    iqual = long
    qual = quality(long)
  else
    call message(3,2,'GET_QUALITY',   &
      'Quality flag out of range 0-9')
    error = .true.
  endif
  return
end subroutine get_quality
!
subroutine store_correction(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  !	Stores validity of radiometric phase correction
  ! CLIC	Command CLIC\STORE CORRECTION GOOD|BAD|AUTO|SELF
  !      GOOD or BAD : force to value
  !      AUTO : use calibrator amplitude and spread result to neighbouring
  !             source observations
  !      SELF : use source amplitude, for all observations
  !      (will produce random result if no continuum, useful for point sources)
  !      /RECEIVER ir
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: okmon(mnant), write, okt(mnant)
  integer :: long, mcvocab, ncal, i, j, k
  integer :: na, ia(mnant), nff, krec,  iant
  integer(kind=entry_length) :: kx, kxsave,iff(2)
  parameter (mcvocab=4)
  character(len=10) :: cvocab(mcvocab),qual,argum
  real*8 :: tmin, frac
  data cvocab/'GOOD','BAD','AUTO','SELF'/
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  call check_output_file(error)
  if (error) goto 999
  call check_equal_file(error)
  if (error) goto 999
  !
  ! /ANTENNA: option 2 of STORE
  call get_antennas(line,2,na,ia,.false.,error)
  !
  ! Other receiver
  krec = 0
  if (sic_present(6,0)) then
    call sic_i4(line,6,1,krec,.true.,error)
    if (error) return
    if (krec.gt.mnrec .or. krec.lt.1) then
      call message(8,4,'STORE_CORRECTION','Invalid receiver')
      goto 999
    endif
    write(argum(1:2),'(i2)') krec
    call message(6,1,'STORE_CORRECTION',   &
      'Using correction validity from receiver '//argum(1:2))
  else
    !
    ! AUTO/SELF/GOOD/BAD
    tmin = 20.                 ! in minutes;
    qual = '*'
    call clic_kw(line,0,2,qual,long,cvocab,mcvocab,.true.,error,.true.)
    if (error) return
    if (qual.eq.'AUTO') then
      call sic_r8(line,0,3,tmin,.false.,error)
      if (error) return
      write(argum(1:5),'(f5.0)') tmin
      tmin = tmin/60./24.      ! now in days.
      call message(4,1,'STORE_CORRECTION',   &
        'Storing AUTO phase corr. validity with time constant of '   &
        //argum(1:5)//' min.')
    else
      call message(4,1,'STORE_CORRECTION',   &
        'Storing '//qual//' phase correction')
    endif
  endif
  frac = 0.0
  if (sic_present(8,0)) then
    call sic_r8(line,8,1,frac,.true.,error)
    write(argum(1:5),'(f5.3)') frac
    call message(8,1,'STORE_CORRECTION','Using '//argum(1:5)// &
                     ' as a fraction of amplitude for threshold')
  endif
  !
  ! AUTO: first pass for calibrators
  ncal = 0
  do i=1, cxnext-1
    kx = cx_ind(i)
    if (ix_itype(kx) .ge. 2) call get_num(kx,error)
    if (all(r_polswitch(1:r_nant,1).eq.0).and. &
       (ix_itype(kx) .ge. 2)) then
      do j=1, r_nant
        okmon(j) = r_ok_mon(j)
      enddo
      if (krec.ne.0) then
        kxsave = r_xnum
        if (krec.ne.r_nrec) then
          nff = 2
          fnum = .false.
          xscan(1) = r_scan
          yscan(1) = r_scan
          xnscan = 1
          fscan = .true.
          xrecei = krec
          call fix(nff,iff,.false.,.false.,error)
          error = .false.
          if (nff.lt.1) then
            call message(4,2,'STORE_CORRECTION',   &
              'Only one receiver available')
            error = .true.
            return
          endif
          kx = iff(1)
          call get_it(kx,error)
          if (error) return
          do iant=1, r_nant
            okt(iant) = r_ok_mon(iant)
          enddo
          if (r_xnum.ne.kxsave) then
            call get_it(kxsave,error)
          endif
          do iant=1, r_nant
            r_ok_mon(iant) = okt(iant)
          enddo
        endif
      elseif (qual.eq.'AUTO') then
        call check_correction(na,ia,ncal,.false.,tmin,frac,error)
      elseif (qual.eq.'SELF') then
        call check_correction(na,ia,ncal,.false.,tmin,frac,error)
      elseif (qual.eq.'GOOD') then
        do j=1, na
          k = ia(j)
          r_ok_mon(k) = .true.
        enddo
      else
        do j=1, na
          k = ia(j)
          r_ok_mon(k) = .false.
        enddo
      endif
      !
      ! Update scan (only header)
      write = .false.
      do j=1, r_nant
        write = write.or.(okmon(j).neqv.r_ok_mon(j))
      enddo
      !
      if (write) then
        call write_scan (.false.,error)
        if (error) goto 999
      endif
      !
      ! Next observation
      if (sic_ctrlc()) goto 999
    endif
  enddo
  !
  ! Loop on current index: other observations
  do i=1, cxnext-1
    kx = cx_ind(i)
    if (ix_itype(kx) .eq. 1) then
      call get_num(kx,error)
      do j=1, r_nant
        okmon(j) = r_ok_mon(j)
      enddo
      if (krec.ne.0) then
        kxsave = r_xnum
        if (krec.ne.r_nrec) then
          nff = 2
          fnum = .false.
          xscan(1) = r_scan
          yscan(1) = r_scan
          xnscan = 1
          fscan = .true.
          xrecei = krec
          call fix(nff,iff,.false.,.false.,error)
          error = .false.
          if (nff.lt.1) then
            call message(4,2,'STORE_CORRECTION',   &
              'Only one receiver available')
            error = .true.
            return
          endif
          kx = iff(1)
          call get_it(kx,error)
          if (error) return
          do iant=1, r_nant
            okt(iant) = r_ok_mon(iant)
          enddo
          if (r_xnum.ne.kxsave) then
            call get_it(kxsave,error)
          endif
          do iant=1, r_nant
            r_ok_mon(iant) = okt(iant)
          enddo
        endif
      elseif (qual.eq.'AUTO') then
        call check_correction(na,ia,ncal,.false.,tmin,frac,error)
      elseif (qual.eq.'SELF') then
        call check_correction(na,ia,ncal,.true.,tmin,frac,error)
      elseif (qual.eq.'GOOD') then
        do j=1, na
          k = ia(j)
          r_ok_mon(k) = .true.
        enddo
      elseif (krec.ne.0) then
      else
        do j=1, na
          k = ia(j)
          r_ok_mon(k) = .false.
        enddo
      endif
      !
      ! Update scan (only header)
      write = .false.
      do j=1, r_nant
        write = write.or.(okmon(j).neqv.r_ok_mon(j))
      enddo
      if (write) then
        call write_scan (.false.,error)
        if (error) goto 999
      endif
      !
      ! Next observation
      if (sic_ctrlc()) goto 999
    endif
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_correction
!
subroutine check_correction(na, ia, ncal, source, tmin, frac, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! for phase calibrators, compute the recorrelation factor due to
  ! the phase correction
  ! for other sources,interpolate between nearest calibrators.
  !---------------------------------------------------------------------
  include 'clic_parameter.inc'
  integer :: na                     !
  integer :: ia(mnant)              !
  integer :: ncal                   !
  logical :: source                 !
  real*8 :: tmin                    !
  real*8 :: frac                    !
  logical :: error                  !
  ! Global
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_sba.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  ! Local
  integer(kind=address_length) :: data_in, ipk, kin
  integer(kind=data_length)    :: ldata_in, h_offset
  integer :: mcal, isb, iba, ical, lc, i, j, kr
  real :: w, s_c(mnbas), s_u(mnbas), smin, tol, noise 
  parameter (smin = 2.0)       ! 2 sigma limit.
  parameter (mcal=2000)
  logical :: corr(mnrec,mcal,mnant), ok(mnant)
  real*8 :: tcorr(mnrec,mcal), try
  character(len=80) :: chain
  save corr, tcorr
  logical :: save_atm(mnant), zero
  !-----------------------------------------------------------------------
  !
  ! Save atm phase status
  do i=1, mnant
    save_atm(i) = do_phase_atm(i)
  enddo
  ! other antennas keep their default status ...
  do i=1, na
    j = ia(i)
    do_phase_atm(j) = .true.
    r_ok_mon(j) = .true.
  enddo
  ! calibrator (with both uncorr. and corr. data)
  if (r_lmode.ne.1 .or. r_ndatl.le.1) return
  if ((r_itype.ge.2 .or. source)           .and. &
      (all(r_polswitch(1:r_nant,1).eq.0)))   then
    ! ph corr enabled for all tested antennas:
    if (do_spidx) then
      call set_spidx(r_nsb,r_nband,r_nbas+r_ntri,   &
        r_lntch, spidc, spidl, r_spidx, error)
    endif
    call get_data (ldata_in,data_in,error)
    ! read the header to get the flag information (to be used in
    ! cont_avaerage) - 07.06.04
    kin = gag_pointer(data_in,memory)
    call spectral_dump(kr,0,0)
    ipk = kin + h_offset(kr)
    call decode_header (memory(ipk))
    !
    zero = .true.
    if (k_average.eq.1) then
      call line_average (r_lntch,r_nbas,r_lnsb,data_in,   &
        spidl,zero,error)
    else
      call cont_average (r_nband,r_nbas,data_in,   &
        spidc,zero,error)
    endif
    if (error) return
    isb = (3-r_isb)/2
    do iba = 1, r_nbas
      w = wsba(isb,iba)
      if (w.gt.0) then
        s_c(iba) = abs(zsba(isb,iba)/w)
        noise = 1e-3/sqrt(w)
        tol = max (smin*noise/s_c(iba),frac)
        s_c(iba) = abs(zsba(isb,iba)/w)*(1+tol)
      endif
    enddo
    ! ph corr disabled for each antenna in turn; check all baselines
    ! connected to it.
    do i=1, na
      j = ia(i)
      ok(j) = .true.
      do_phase_atm(j) = .false.
      zero = .true.
      if (k_average.eq.1) then
        call line_average (r_lntch,r_nbas,r_lnsb,data_in,   &
          spidl,zero,error)
      else
        call cont_average (r_nband,r_nbas,data_in,   &
          spidc,zero,error)
      endif
      if (error) return
      do iba = 1, r_nbas
        if (j.eq.r_iant(iba) .or. j.eq.r_jant(iba)) then
          w = wsba(isb,iba)
          if (w.gt.0) then
            s_u(iba) = abs(zsba(isb,iba)/w)
            noise = 1e-3/sqrt(w)
            tol = max (smin*noise/s_u(iba),frac)
            s_u(iba) = abs(zsba(isb,iba)/w)*(1-tol)
            ok(j) = ok(j) .and. (s_c(iba).ge.s_u(iba))
          endif
        endif
      enddo
      do_phase_atm(j) = .true.
      if (.not.ok(j)) then
        write(chain,1000) r_scan, r_nrec, j
        lc = lenc(chain)
        call message(4,2,'CHECK_CORRECTION',chain(1:lc))
      endif
    enddo
    if (r_itype.eq.2 .and. .not.source) then
      ncal = ncal+1
      if (ncal.gt.mcal) then
        call message(8,4,'CHECK_CORRECTION',   &
          'Too many calibrators')
        error = .true.
        return
      endif
      do i=1, na
        j = ia(i)
        corr(r_nrec,ncal,j) = ok(j)
      enddo
      tcorr(r_nrec,ncal) = r_dobs + r_ut/2d0/pi
    endif
    do i=1, na
      j = ia(i)
      r_ok_mon(j) = ok(j)
    enddo
  else
    try = r_dobs + r_ut/2d0/pi
    do i = 1, na
      j = ia(i)
      ok(j) = .true.
      do ical = 1, ncal
        if (abs(try-tcorr(r_nrec,ical)) .lt. tmin) then
          ok(j) = ok(j) .and. corr(r_nrec,ical,j)
        endif
      enddo
      if (.not.ok(j)) then
        write(chain,1001) r_scan, r_nrec, j
        lc = lenc(chain)
        call message(4,2,'CHECK_CORRECTION',chain(1:lc))
      endif
      r_ok_mon(j) = ok(j)
    enddo
  endif
  !
  ! reset phase corrections
99 continue
  do i=1, mnant
    do_phase_atm(i) = save_atm(i)
  enddo
  return
  !
1000 format('Phase corr. failed for scan ',i4,' Rec. ',i1,' Ant. ',i0)
1001 format('Phase corr. disabled for scan ',i4,' Rec. ',i1,   &
    ' Ant. ',i0)
end subroutine check_correction
