subroutine solve_wvr(line,error)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_xy_code.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: m_wvr
  integer(kind=address_length) :: ipx,ipy,ipw,ips,ipwvr,data_wvr
  integer :: nk, iy, iba, ib, i, l, l1, nn, nd
  integer :: n_br, k_br(10), ikn, nkn, j, ia, ipol, old_pen
  real :: x4, x2, y2, x3, y3, t_br(10), tkn(40), step, w0
  real :: airmin, dtmax, dtmaxsec
  logical :: poly, reset, plot
  integer :: mvoc1, nkey
  parameter (mvoc1=2)
  character(len=12) :: kw, voc1(mvoc1)
  data voc1 /'JUMPS','CAL'/
  !------------------------------------------------------------------------
  ! Code:
  ! get main argument (second, as first was WVR)
  kw = 'JUMPS'
  call clic_kw(line,0,2,kw,nkey,voc1,mvoc1,.false.,error,.true.)
  ! Jumps in Delta  T Triple
  if (kw.eq.'JUMPS') then
    !
    ! airmin
    airmin = 0.05
    call sic_r4(line,0,3,airmin,.false.,error)
    if (error) goto 999
    ! dtmaxsec
    dtmaxsec = 300.
    call sic_r4(line,0,4,dtmaxsec,.false.,error)
    if (error) goto 999
    dtmax = dtmaxsec/3600.
  elseif (kw.eq.'CAL') then
  ! no specific input so far.
  endif
  ! degree
  ipol = 0
  call sic_i4(line,9,1,ipol,.false.,error)
  if (error) goto 999
  ipol = max(min(ipol+1,m_pol),1)  ! degree+1
  !
  reset = sic_present(4,0)
  if (reset) then
    call reset_time
  endif
  !
  ! Switch to antennas
  call switch_antenna
  !
  ! Time in X
  n_x = 1
  i_x(1) = xy_time
  sm_x1(1) = '='
  sm_x2(1) = '='
  do_bin = .false.
  change_display = .true.
  !
  ! Jumps setup
  if (kw.eq.'JUMPS') then
    !
    ! Y variables: wvrdtri and airmass
    n_y = 2
    i_y(1) = xy_wvr_dtri
    sm_y1(1) = '*'
    gu1_y(1) = 0.
    sm_y2(1) = '*'
    i_y(2) = xy_airmass
    sm_y1(2) = '*'
    gu1_y(2) = 0.
    sm_y2(2) = '*'
  elseif (kw.eq.'CAL') then
    n_y = 3
    i_y(1) = xy_wvrtcal1
    sm_y1(1) = '*'
    sm_y2(1) = '*'
    i_y(2) = xy_wvrtcal2
    sm_y1(2) = '*'
    sm_y2(2) = '*'
    i_y(3) = xy_wvrtcal3
    sm_y1(3) = '*'
    sm_y2(3) = '*'
  endif
  change_display = .true.
  call set_display(error)
  if (error) return
  !
  poly = sic_present(9,0)
  ipol = 1
  if (poly) then
    call sic_i4(line,9,1,ipol,.false.,error)
    if (error) goto 999
    ipol = max(min(ipol+1,m_pol),1)    ! degree+1
  endif
  reset = sic_present(4,0)
  if (reset) then
    call reset_time
  endif
  !
  if (change_display) then
    ! plot all scans in index, sorted, plot buffer re-initialized.
    call read_data('ALL',.true.,.true., error)
    plotted = .false.
    if (error) goto 999
    change_display = .false.
  else
    plotted = .true.
  endif
  plot = sic_present(1,0)
  !
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ips = gag_pointer(data_s,memory)
  !
  ! just in case m_data has changed
  call get_first(.true.,error)
  !
  if (kw.eq.'JUMPS') then
    call  get_wvr_jumps(m_data,m_boxes,memory(ipx),   &
      memory(ipy),memory(ipw), airmin, dtmax, error)
    !
    ! Redimension variables
    call resetvar(error)
  endif
  if (plot) then
    clear = .true.
    call sub_plot('A',.false.,.false.,0,error)
    if (error) goto 999
    plotted = .true.
  endif
  m_wvr = m_data*8
  error = sic_getvm(m_wvr,data_wvr).ne.1
  if (error) goto 999
  ipwvr = gag_pointer(data_wvr,memory)
  call sub_solve_wvr(m_data,m_boxes,memory(ipx),   &
    memory(ipy), memory(ipw),   &
    memory(ipwvr+6*m_data),   &
    memory(ipwvr),   &
    memory(ipwvr+2*m_data), memory(ipwvr+4*m_data),   &
    line, plot, ipol, nkey, error)
  call free_vm(m_wvr,data_wvr)
  return
  !
999 error = .true.
  return
end subroutine solve_wvr
!
subroutine get_wvr_jumps(md,mb,x_data,y_data,w_data,   &
    airmin, dtmax,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  real :: airmin                    !
  real :: dtmax                     !
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: nk, iy, iba, ib, i, l, l1, nn, nd, it
  integer :: n_br, k_br(10), ikn, nkn, j, ia, jb, kb
  real*8 :: cs(m_spl+4), ks(m_spl+8), rss, ss, sss
  real*8 :: xx(10*m_spl), yy(10*m_spl), xx1
  real*8 :: wk2(4*(m_spl+8)), wk3(2*m_pol)
  real*8 :: w0, xold, xnew
  real*8 :: apol(m_pol,m_pol), spol(m_pol), aa(m_pol)
  real :: x4, x2, y2, x3, y3, t_br(10), tkn(40), step
  logical :: plot, reset, poly
  character(len=132) :: chain
  integer :: ipol, nt, old_pen, itt, iair
  real :: rms_err(mbox), s
  real :: ymean, wmean, air0, tt0, t0, dair, dt, dtt
  save rms_err
  !------------------------------------------------------------------------
  ! Code:
  t_splwvr = t_step
  old_pen = -1
  !
  ! Loop on antennas
  do jb = 1, n_base
    ia = -i_base(jb)
    ! Loop on boxes
    do ib = 1, n_boxes
      if (k_base(ib).eq.jb) then
        iy = i_y(k_y(ib))
        if (iy.eq.xy_wvr_dtri) then
          itt = ib
        elseif (iy.eq.xy_airmass) then
          iair = ib
        endif
      endif
    enddo
    if (n_data(iair).eq.0) then
      call message(4,3,'SOLVE_WVR','No airmass data')
      goto 999
    endif
    !
    ! Look for jumps
    nd = 0
    ymean = 0
    wmean = 0
    air0 = y_data(1,iair)
    tt0 = y_data(1,itt)
    t0 = x_data(1,iair)
    do i=1, n_data(iair)
      if (y_data(i,iair) .ne. blank4 .and.   &
        w_data(i,iair).gt.0) then
        dair = abs(y_data(i,iair)-air0)
        dt = abs(x_data(i,iair)-t0)
        dtt = abs(y_data(i,itt)-tt0)
        air0 = y_data(i,iair)
        tt0 = y_data(i,itt)
        t0 = x_data(i,iair)
        if (dair.gt.airmin .and. dt.lt.dtmax) then
          nd = nd+1
          !                  X_data(ND,it) = X_DATA(I,Iair)
          y_data(i,iair) = dtt/dair
          ! The MTH_FITPOL routine requires 1/sigma values
          w_data(i,iair) = dair
        else
          w_data(i,iair) = 0
          y_data(i,iair) = blank4
        endif
      endif
    enddo
    if (nd.eq.0) goto 999
  !
  ! Second box  per antenna
  enddo
  !
  ! normalize
  do i=1, n_data(2)
    if (w_data(i,2).gt.0) then
      s = 0
      do kb = 2, n_boxes, 2
        s = s + y_data(i,kb)
      enddo
      s = s*2/n_boxes
      do kb = 2, n_boxes, 2
        y_data(i,kb) = y_data(i,kb) / s
      enddo
    endif
  enddo
  return
  !
999 error = .true.
  return
end subroutine get_wvr_jumps
!
subroutine  sub_solve_wvr(md,mb, x_data, y_data, w_data,   &
    wk1, xxx, yyy, www, line, plot, ipol, nkey, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  real*8 :: wk1(3*md)               !
  real*8 :: xxx(md)                 !
  real*8 :: yyy(md)                 !
  real*8 :: www(md)                 !
  character(len=*) :: line          !
  logical :: plot                   !
  integer :: ipol                   !
  integer :: nkey                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: ind(md)
  real*8 :: wk3(2*m_pol), apol(m_pol,m_pol), spol(m_pol), aa(m_pol)
  real*8 :: rss, ss, sss
  real*8 :: xx(10*m_spl), yy(10*m_spl), xx1
  real*8 :: wk2(4*(m_spl+8)), w0, xold, xnew
  real*4 :: step
  character(len=256) :: chain
  integer :: ib, ia, nd, i, l, l1, l2, l3, nn, old_pen
  integer :: iy, ich, ky(2)
  logical :: reset, ok
  !
  !------------------------------------------------------------------------
  ! Code:
  if (plot) then
    old_pen = gr_spen (1)
    call gr_segm('FIT',error)
  endif
  !
  ! Loop on boxes
  do ib = 1, n_boxes
    ia = - i_base(k_base(ib))
    iy = i_y(k_y(ib))
    ok = (nkey.eq.1 .and. iy.eq.xy_airmass)
    ok = ok .or.   &
      (nkey.eq.2   &
      .and. iy.ge.xy_wvrtcal1 .and. iy.le.xy_wvrtcal3)
    if (ok) then
      nd = 0
      do i=1, n_data(ib)
        if (y_data(i,ib) .ne. blank4) then
          nd = nd+1
          xxx(nd) = x_data(i,ib)
          yyy(nd) = y_data(i,ib)
          www(nd) = sqrt(w_data(i,ib))
        endif
      enddo
      if (nd.eq.0) goto 999
      sss = 0
      do i=1, nd
        sss = sss + www(i)**2
      enddo
      ipol = min(ipol,nd)
      call mth_fitpol('SOLVE_WVR',nd,ipol,m_pol,   &
        xxx,yyy,www,wk1,wk3,apol,spol,error)
      if (error) goto 999
      rss = sqrt(sss/nd)       ! rms obtenu
      rss = spol(ipol)/rss
      !
      l = min(lenc(y_label(ib)),5)
      l1 = lenc(header_1(ib))
      l2 = lenc(header_2(ib))
      l3 = lenc(header_3(ib))
      write(chain,1000) y_label(ib)(1:3), header_1(ib)(1:l1),   &
        rss, 2*rss/apol(ipol,1)
1000  format(a,'. ',a,' rms: ',1pg10.3,' (rel ',1pg10.3,')')
      call message(6,1,'SOLVE_WVR',chain(1:lenc(chain)))
      if (plot) then
        nn = 10.*m_spl
        step = (gb2_x(ib)-gb1_x(ib)) / (nn-1)
        if (step.eq.0) step = 0.1
        do i=1, ipol
          aa(i) = apol(ipol,i)
        enddo
        do i=1, nn
          xx(i) = gb1_x(ib) + (i-1) * step
          xx1 = min(max(xx(i),xxx(1)),xxx(nd))
          xx1 = ((xx1-xxx(1))-(xxx(nd)-xx1))   &
            /(xxx(nd)-xxx(1))
          call mth_getpol('SOLVE_WVR',   &
            ipol,aa,xx1,yy(i),error)
          if (error) goto 999
        enddo
        write(chain,'(I4.4)') ib
        call gr_execl('CHANGE DIRECTORY BOX'//chain)
        error = gr_error()
        if (error) goto 999
        call gr8_connect (nn,xx,yy,0.0d0,-1.0d0)
        call gr_execl('CHANGE DIRECTORY')
      endif
      !
      ! Save results
      n_polwvr(ia) = ipol
      if (nkey.eq.1) then
        ich = 1
      !     in proper channel ... (for tcal)
      elseif (nkey.eq.2) then
        ich = 1+iy-xy_wvrtcal1
      endif
      do i=1, ipol
        c_polwvr(i,ia,ich) = apol(ipol,i)
      enddo
      t_polwvr(1,ia) = xxx(1)
      t_polwvr(2,ia) = xxx(nd)
      rms_polwvr(ia,ich) = rss
      f_polwvr(ia) = nkey
      f_splwvr(ia) = 0
    endif
  enddo
  !      CALL SIC_DELVARIABLE ('RMS_ERR',.FALSE.,ERROR)
  !      ERROR = .FALSE.
  !      CALL SIC_DEF_REAL ('RMS_ERR',RMS_ERR,1,N_BOXES,.FALSE.,ERROR)
  !      ERROR = .FALSE.
998 continue
  if (old_pen.ne.-1)  then
    call gr_segm_close(error)
    i = gr_spen(old_pen)
  endif
  return
  !
999 error = .true.
  goto 998
end subroutine sub_solve_wvr
