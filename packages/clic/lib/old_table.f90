subroutine open_table (name,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  use clic_xypar
  !---------------------------------------------------------------------
  ! Open an existing table, to get the header information (in X);
  ! then copy the header information in Y too, for further use.
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  !-----------------------------------------------------------------------
  xima%loca%read = .false.
  call gdf_read_gildas (xima,name,'.uvt',error, data=.false.)
  if (error) return
  ! Copy to Y buffer,
  ! ( Y buffer is used when reading scan headers and comparing to X)
  call gdf_copy_header(xima,yima,error)
end subroutine open_table
!
!
subroutine close_table
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  ! Closes the output table.
  !---------------------------------------------------------------------
  ! Local
  logical :: error
  !-----------------------------------------------------------------------
!!  if (xima%loca%islo.eq.0) return
!!  call gdf_clis (xima%loca%islo,error)
  call gdf_close_image(xima,error)
end subroutine close_table
!
subroutine init_table (name,nvis,addr,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  use clic_xypar
  !---------------------------------------------------------------------
  ! Creates the output table header in X (the significant header parameters
  ! have already been defned in clic_table)
  !---------------------------------------------------------------------
  character(len=*) :: name              !
  integer :: nvis                       !
  integer(kind=address_length) :: addr  !
  logical :: error                      !
  !
  integer(kind=address_length) :: locwrd
  ! Do not nullify X header here: it has already been partly computed before.
  !
  xima%file = name
  xima%blc = 0
  xima%trc = 0
  xima%gil%blan_words = 2
  xima%gil%extr_words = 6
  xima%gil%desc_words = 18
  xima%gil%posi_words = 12
  xima%gil%proj_words = 9
  xima%gil%spec_words = 12
  xima%gil%reso_words = 3
  xima%gil%eval = 0.
  xima%gil%dim(2) = nvis              ! +1 ! = add the dummy uv = 7.5 m point
  xima%gil%ref(2) = 0.
  xima%gil%inc(2) = 1.                  ! needed to avoid crash in GRAPHIC
  xima%gil%val(2) = 0.
  xima%gil%ndim = 2
  xima%char%code(2) = 'RANDOM'
  xima%char%code(1) = 'UV-RAW'
  xima%char%unit = 'Jy'
  xima%char%syst = 'EQUATORIAL'
  xima%gil%ptyp = p_azimuthal
  xima%gil%pang = 0.0
  xima%gil%xaxi = 0
  xima%gil%yaxi = 0
  xima%gil%faxi = 1
  xima%loca%size = xima%gil%dim(1) * xima%gil%dim(2)
  xima%gil%form = fmt_r4
  !
  xima%gil%nchan = 1
  xima%gil%dim(1) = 10
  !
  call gdf_create_image(xima,error)
  if (error) return
  call gdf_print_header(xima) 
  call gdf_allocate (xima,error)
  xima%loca%addr = locwrd(xima%r2d(1,1))
  addr = xima%loca%addr
  !
  ! xima%loca%islo must be copied to yima%loca%islo
  call gdf_copy_header(xima,yima,error)
  yima%loca%islo = xima%loca%islo
end subroutine init_table
!
subroutine extend_table (name,nvis,addr,nn,ndobs,ndscan,error,lc)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  ! Open a Table to extend it.
  !---------------------------------------------------------------------
  character(len=*) :: name              !
  integer :: nvis                       !
  integer(kind=address_length) :: addr  !
  integer :: nn                         !
  integer :: ndobs(nn)                  !
  integer :: ndscan(nn)                 !
  logical :: error                      !
  logical :: lc                         !
  ! Global
  integer(kind=address_length) :: locwrd
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ip, ipa
  integer(kind=size_length) :: omore, xmore
  integer(kind=index_length) :: blc(2), trc(2)
  character(len=60) :: chain
  integer :: ier
  !-----------------------------------------------------------------------
  !
  if (name.ne.xima%file) then
    call message(6,2,'EXTEND_TABLE', 'Unexpected file change')
    error = .true.
    return
  endif
  !
  !
  ! Compute new size, and remember there may be more preallocated data
  ! than used in the table to be appended
  write(chain,1001) xima%gil%dim(2),nvis
1001 format('Old table size ',i6,' Adding ',i6)
  call message(6,1,'TABLE',chain)
  omore  = xima%loca%size-xima%gil%dim(1)*xima%gil%dim(2)
  xmore  = nvis*xima%gil%dim(1)
  xima%gil%dim(2) = xima%gil%dim(2)+nvis
  xima%loca%size = xima%gil%dim(1)*xima%gil%dim(2)
!  call gdf_writ (xima,xima%loca%islo,error) 
!  call gdf_exis (xima%loca%islo,xima%char%type,xima%file,xima%gil%form,xima%loca%size,error)
  call gdf_extend_image (xima, xmore, error)
  xima%gil%nvisi = xima%gil%nvisi+nvis
  if (error) then
    call message(8,4,'EXTEND_TABLE','Table extension failed')
    return
  endif
  !
  ! Check old region
  blc(1) = 1
  blc(2) = 1
  trc(1) = xima%gil%dim(1)
  trc(2) = xima%gil%dim(2)-nvis
  allocate (xima%r2d(xima%gil%dim(1),xima%trc(2)), stat=ier)
  error = ier.ne.0
  call gdf_read_data(xima, xima%r2d, error)
  xima%loca%addr = locwrd(xima%r2d)
!
!  call gdf_gems (xima%loca%mslo,xima%loca%islo,blc,trc,xima%loca%addr,xima%gil%form,error)
  ip = gag_pointer(xima%loca%addr,memory)
  if (error) then
    call close_table
    return
  endif
  if (lc) then
    call check_table(xima%gil%dim(1),xima%gil%dim(2)-nvis,memory(ip),   &
      nn,ndobs,ndscan,error)
    if (error) then
      call close_table
      return
    endif
  endif
!  call gdf_frms (xima%loca%mslo,error)
  deallocate (xima%r2d, stat=ier)
  if (error) return
  !
  ! Map only new region
  blc(1) = 1
  blc(2) = xima%gil%dim(2)+1-nvis
  trc(1) = xima%gil%dim(1)
  trc(2) = xima%gil%dim(2)
  allocate (xima%r2d(xima%gil%dim(1), nvis), stat=ier)
  error = ier.ne.0
!  call gdf_gems (xima%loca%mslo,xima%loca%islo,blc,trc,xima%loca%addr,xima%gil%form,error)
  if (error) then
    call message(8,4,'EXTEND_TABLE','Memory allocation failed')
    return
  endif
  xima%loca%addr = locwrd(xima%r2d)
  !
  ! Zero fill only new allocated space
  xima%r2d = 0.
!  if (xmore.gt.omore) then
!    addr = xima%loca%addr + 4*omore
!    ipa = gag_pointer(addr,memory)
!    call gdf_fill (xmore-omore,memory(ipa),0.0)
!  endif
  addr = xima%loca%addr
end subroutine extend_table
!
subroutine cut_table (name,nvis,error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  ! Update the number of visibilities
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: nvis                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  integer :: old
  character(len=80) :: chain
  !----------------------------------------------------------------------
! Write the data first, now
  call gdf_write_data (xima, xima%r2d, error)
  if (error) then
    call message(6,3,'TABLE','Cannot write output table')
    return
  endif
!
  old = xima%gil%dim(2)
  xima%gil%dim(2) = xima%gil%dim(2)+nvis
  call gdf_update_header(xima,error)
!!  call gdf_writ (xima,xima%loca%islo,error)
!!  call gdf_clis (xima%loca%islo,error)
!!  call gdf_upih (xima%loca%islo,name,error)
  !
  write(chain,1001) old,xima%gil%dim(2)
  call message(6,1,'TABLE',chain)
1001 format('Old size ',i6,' New ',i6)
end subroutine cut_table
!
subroutine fill_visi (qsb,qband,qbas,qntch,qnchan,   &
    ibase,isb,inbc,frout,fres,datac,datal,passc,passl,   &
    daps, visi, ndrop, good, scan, width, shape, resample, fft, &
    positions, offsets, error)
  use gildas_def
  use phys_const
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  ! Called by CLIC_TABLE
  ! creates one visibility point (i.e. one baseline and one band for a
  ! given scan), but many channels if in line mode.
  !---------------------------------------------------------------------
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbas                   !
  integer :: qntch                  !
  integer :: qnchan                 !
  integer :: ibase                  !
  integer :: isb                    !
  integer :: inbc                   !
  real*8 :: frout                   !
  real*4 :: fres
  complex :: datac(qband,qsb,qbas)  !
  complex :: datal(qntch,qsb,qbas)  !
  complex :: passc(qband,qsb,qbas)  !
  complex :: passl(qntch,qsb,qbas)  !
  real*4 :: daps(7)                 !
  real*4 :: visi(3, qnchan)         !
  real*4 :: offsets(2)              !
  integer :: ndrop(2)               !
  logical :: good                   !
  logical :: scan                   !
  real*4 :: width                   !
  character(len=*) :: shape         !
  logical :: resample               !
  logical :: fft                    !
  logical :: positions              !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  complex :: z, zz, zin(mlch), zout(mlch), zscale
  real :: wout(mlch)
  real :: w, ww, tsys(2), ri, afac, factor, fr, save_res, rchan
  integer :: is, i, j, nw, k, ni, js, i1, i2, nd1, nd2, nch, iw
  integer :: ifirst, ilast, l, nchan, ipola, ia, ja
  integer :: first_polar(mrlband*mch),second_polar(mrlband*mch)
  integer :: first_tsys(mrlband*mch),second_tsys(mrlband*mch)
  integer :: nfirst(mrlband*mch),nsecond(mrlband*mch)
  logical :: down_channel, ok
  real*8 :: f1, fn, fo1, fon, ff
  character(len=128) :: chain
  !------------------------------------------------------------------------
  ! Code:
  rchan = xima%gil%ref1
  nchan = (xima%gil%dim(1)-7)/3
  !
  ! NGRX: This needs more work, if we have here polarizations with different tsys
  ! as a quick workaround, I use only tsys of the first polar.
  !
  ia = r_iant(ibase)
  ja = r_jant(ibase)
  if (weight_tsys) then
    if (inbc.le.mnbcinput+r_nband_widex) then
      if (r_isb.eq.1) then
        tsys(1) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja)   &
          /dh_integ
        tsys(2) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja)   &
          /dh_integ
      else
        tsys(2) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja)   &
          /dh_integ
        tsys(1) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja)   &
          /dh_integ
      endif
    else
      call message(2,3,'SUB_TABLE',   &
        'Unknown correlator entry')
      error = .true.
      return
    endif
  else
    tsys(1) = 0
    tsys(2) = 0
  endif
  !
  do i = 1, 2*mch
    first_polar(i) = 0
    second_polar(i) = 0
    nfirst(i) = 0
    nsecond(i) = 0 
  enddo
  if (tsys(1).eq.0) then
    tsys(1) = 300.**2/dh_integ
  endif
  if (tsys(2).eq.0) then
    tsys(2) = 300.**2/dh_integ
  endif
  !
  if (dobs_0 .eq.0) then
    dobs_0 = mod(dh_obs+32768,65536)-32768
  endif
  ! In CONT mode, correct u and v in each sideband
  ! to the same frequency frout
  if (cont_select) then
    if (isb.eq.1) then
      factor = (r_flo1 + r_fif1)/frout
    elseif (isb.eq.2) then
      factor = (r_flo1 - r_fif1)/frout
    endif
  else
    factor = 1.0
  endif
  daps(1) = dh_uvm(1,ibase)*factor
  daps(2) = dh_uvm(2,ibase)*factor
  daps(3) = 0
  if (scan) daps(3) = r_scan
  daps(4) = dh_obs
  daps(5) = dh_utc
  daps(6) = r_iant(ibase)
  daps(7) = r_jant(ibase)
  !
  if (positions) then
! The following code is for the MOSAIC observing mode
!     offsets(1) = r_lamof
!     offsets(2) = r_betof
! The following code is for the OTF observing mode
     offsets(1) = dh_offlam(1)*rad_per_sec
     offsets(2) = dh_offbet(1)*rad_per_sec
  endif
  !
  do i=1, nchan
    do j=1,3
      visi(j,i) = 0
    enddo
  enddo
  good = .false.
  !
  ! Continuum out
  if (cont_select) then
    zz = 0
    ww = 0
!    afac = 1.
!    zscale = 1.
!    !
!    ! Do the scaling only once for old receivers
!    call scaling (1,1,isb,ibase,zscale,afac,error)
!    if (error) then
!      call message(8,3,'FILL_VISI',   &
!        'Invalid calibration factor for '//cband(isb))
!      ! will be ignored anyway.
!      error = .false.
!      good = .false.
!      return
!    endif
    do js = 1, lsubb_out
      is = isubb_out(js)
      if (.not.down_channel(ibase,is)) then
        !
        ! Spectrum from continuum backend:
        if (is.le.r_nband) then
!          if (new_receivers) then
!            if (r_lpolmode(is).ne.1) then
!              call message(2,3,'SUB_TABLE',   &
!                'Cannot apply scaling with mixed polarisation')
!              error = .true.
!              return
!            endif
!            ipola = r_lpolentry(1,is)  ! Same pol. entry for all ant.
!            inbc = r_bb(is)
!            if (ipola.ne.1.or.inbc.ne.1) then
              afac = 1.
              zscale = 1.
              call scaling (is,isb,ibase,zscale,afac,   &
                error)
!            endif
!          endif
          z = datac(is,isb,ibase)
          w = 2./tsys(isb)*abs(r_cfwid(is))
          if (do_pass) then
            z = z*passc(is,isb,ibase)
          endif
          z = z * zscale
          if (weight_cal) then
            w = w/afac**2
          endif
          !
          zz = zz + z*w
          ww = ww + w
        !
        ! Continuum  from line  backend:
        elseif (is.gt.mbands .and. is.le.r_lband+mbands) then
          is = is - mbands
!          if (new_receivers) then
!            if (r_lpolmode(is).ne.1) then
!              call message(2,3,'SUB_TABLE',   &
!                'Cannot apply scaling with mixed polarisation')
!              error = .true.
!              return
!            endif
!            ipola = r_lpolentry(1,is)  ! Same pol. entry for all ant.
!            inbc = r_bb(is)
!            if (ipola.ne.1.or.inbc.ne.1) then
              afac = 1.
              zscale = 1.
              call scaling (is,isb,ibase,zscale,afac,   &
                error)
!            endif
!          endif
          nch = r_lnch(is)
          nd1 = max(ndrop(1),nint(fdrop(1)*nch))+1
          nd2 = nch-max(ndrop(2),nint(fdrop(2)*nch))
          !
          ! Avoid Gibbs-affected channels:
          if (ngibbs.gt.0 .and. abs(r_lfres(is)).gt.0.1) then
            i1 = nch/2-ngibbs
            i2 = nch/2+ngibbs+1
          else
            i1 = nch/2
            i2 = i1+1
          endif
          do i = nd1, nd2
            fr = r_lrfoff(isb,is)   &
              +(i-r_lrch(isb,is))*r_lrfres(isb,is)
            if (nwin_out.gt.0) then
              ok = .false.
              do iw = 1, nwin_out
                ok = ok .or. (fr.gt.window_out(1,iw).and.   &
                  fr.le.window_out(2,iw))
              enddo
            else
              ok = .true.
            endif
            ok = ok .and. (i.le.i1 .or. i.ge.i2)
            if (ok) then
              j = i+r_lich(is)
              z = datal(j,isb,ibase)  * zscale
              if (do_pass) then
                z = z*passl(j,isb,ibase)
              endif
              w = 2./tsys(isb)*abs(r_lfres(is))
              if (weight_cal) then
                w = w/afac**2
              endif
              zz = zz + z*w
              ww = ww + w
            endif
          enddo
        endif
      endif
    enddo
    ! Put the correct weights now 
    if (new_receivers) then
      if (weight_cal) then
        w = 2./tsys(isb)/abs(zscale)**2 * abs(fres)
      else
        w = 2./tsys(isb) * abs(fres)
      endif
    else
       w = ww
    endif
    !
    if (ww.gt.0) then
      zz = zz / ww
      visi(1,1) = real(zz)
      visi(2,1) = aimag(zz)
      visi(3,1) = w
      good = .true.
    endif
  !
  ! Spectrum out
  else
    nw = 0
!    zscale = 1.
!    afac = 1.
    !
    ! Do the scaling only once for old receivers
!    call scaling (1,1,isb,ibase,zscale,afac,error)
!    if (error) then
!      call message(8,3,'FILL_VISI',   &
!        'Invalid calibration factor for '//cband(isb))
!      ! will be ignored anyway.
!      error = .false.
!      good = .false.
!      return
!    endif
    !
    ! Spectrum from Continuum backend
    if (isubb_out(1).le.mbands) then
!      if (new_receivers) then
!        if (r_lpolmode(isubb_out(1)).ne.1) then
!          call message(2,3,'SUB_TABLE',   &
!            'Cannot apply scaling with mixed polarisation')
!          error = .true.
!          return
!        endif
!        ipola = r_lpolentry(1,isubb_out(1))
!        inbc = r_bb(isubb_out(1))
!        if (ipola.ne.1.or.inbc.ne.1) then
!          afac = 1.
!          zscale = 1.
!          call scaling (inbc,ipola,isb,ibase,zscale,afac,error)
!        endif
!      endif
      k = 1
      do j = 1, lsubb_out
        i = isubb_out(j)
!        if (new_receivers) then
!          if (r_lpolmode(i).ne.1) then
!            call message(2,3,'SUB_TABLE',   &
!              'Cannot apply scaling with mixed polarisation')
!            error = .true.
!            return
!          endif
!          ipola = r_lpolentry(1,i)
!          inbc = r_bb(i)
!          if (ipola.ne.1.or.inbc.ne.1) then
            afac = 1.
            zscale = 1.
            call scaling (i,isb,ibase,zscale,afac,   &
              error)
!          endif
!        endif
        if (.not.down_channel(ibase,i) .and.   &
          i.le.r_nband) then
          z = datac(i,isb,ibase) * zscale
          if (do_pass) then
            z = z * passc(i,isb,ibase)
          endif
          visi(1,k) = real(z)
          visi(2,k) = aimag(z)
          visi(3,k) = w
          good = .true.
        endif
        k = k + 1
      enddo
    !
    ! Spectrum from Line backend
    else
      save_res = xima%gil%fres
      do js = 1, lsubb_out
        is = isubb_out(js)-mbands
!        if (new_receivers) then
!          if (r_lpolmode(is).ne.1) then
!            call message(2,3,'SUB_TABLE',   &
!              'Cannot apply scaling with mixed polarisation')
!            error = .true.
!            return
!          endif
!          ipola = r_lpolentry(1,is)
!          inbc = r_bb(is)
!          if (ipola.ne.1) then
            afac = 1.
            zscale = 1.
            call scaling (is,isb,ibase,zscale,afac,   &
              error)
!          endif
!        endif
        if (is.le.r_lband .and.   &
          .not.down_channel(ibase,is+mbands)) then
          nch = r_lnch(is)
          ! Drop edge channels and unnecessary channels.
          nd1 = max(ndrop(1),nint(fdrop(1)*nch))
          nd2 = max(ndrop(2),nint(fdrop(2)*nch))
          ff = r_lrfoff(isb,is)-xima%gil%freq   &
            -(r_lvoff(isb,is)-xima%gil%voff)/299792.d0*xima%gil%freq
          f1 = ff+(nd1+0.5-r_lrch(isb,is))*r_lrfres(isb,is)
          fn = ff+(nch-nd2+0.5-r_lrch(isb,is))*r_lrfres(isb,is)
          if (fft) then
            if (r_lrfres(isb,is)*xima%gil%fres.gt.0) then
              ifirst = nint(f1/xima%gil%fres+rchan+1)
              ilast  = nint(fn/xima%gil%fres+rchan-1)
            else
              ilast = nint(f1/xima%gil%fres+rchan-1)
              ifirst  = nint(fn/xima%gil%fres+rchan+1)
            endif
            if ((ilast.lt.1) .or. (ifirst.gt.nchan)) then
              write(chain,2000) csub(is+mbands), ifirst, ilast
              l = lenc(chain)
              call message(6,3,'FILL_VISI',chain(1:l))
              goto 98
            endif
            ifirst = max(ifirst,1)
            ilast = min(ilast,nchan)
            fo1 = 1d-3*nint(1d3*   &
              (ifirst-0.5-rchan)*xima%gil%fres)
            fon = 1d-3*nint(1d3*   &
              (ilast+0.5-rchan)*xima%gil%fres)
            do while ((f1-fo1)*(f1-fon).gt.0 .and. nd1.lt.nch)
              f1 = f1+r_lrfres(isb,is)
              nd1 = nd1 +1
            enddo
            ni = nint((ilast-ifirst+1)   &
              *abs(xima%gil%fres/r_lrfres(isb,is)))
          else
            if (r_lrfres(isb,is)*xima%gil%fres.gt.0) then
              ifirst = nint(f1/xima%gil%fres+rchan)
              ilast  = nint(fn/xima%gil%fres+rchan)
            else
              ilast = nint(f1/xima%gil%fres+rchan)
              ifirst  = nint(fn/xima%gil%fres+rchan)
            endif
            if ((ilast.lt.1) .or. (ifirst.gt.nchan)) then
              write(chain,2000) csub(is+mbands), ifirst, ilast
              l = lenc(chain)
              call message(6,3,'FILL_VISI',chain(1:l))
              goto 98
            endif
            ni = nch-nd1-nd2
            ifirst = max(ifirst,1)
            ilast = min(ilast,nchan)
            ilast = min(ilast,ifirst+ni-1)
          endif
          ri = r_lrch(isb,is)-nd1
          do i=1, ni
            j = i+r_lich(is)+nd1
            zin(i) = datal(j,isb,ibase)  * zscale
            if (do_pass) then
              zin(i) = zin(i)*passl(j,isb,ibase)
            endif
          enddo
          !
          ! Interpolate Gibbs-affected channels:
          if (ngibbs.gt.0 .and. abs(r_lfres(is)).gt.0.1) then
            i1 = nch/2-ngibbs -nd1
            i2 = nch/2+ngibbs+1 -nd1
            do i=i1+1,i2-1
              zin(i) = (zin(i1)*(i2-i)+zin(i2)*(i-i1))/(i2-i1)
            enddo
          endif
          !
          if (resample) then
            ff = (r_lvoff(isb,is)-xima%gil%voff)/299792.d0*xima%gil%freq
            if (ifirst.le.ilast-4 .and. fft) then
              call fft_interpolate (zout(ifirst),   &
                ilast-ifirst+1, xima%gil%fres,   &
                rchan-ifirst+1, xima%gil%freq+ff,   &
                width, shape,   &
                wout(ifirst), zin, ni, r_lrfres(isb,is),   &
                ri, r_lrfoff(isb,is),   &
                1., 'TPAR', error)
              if (error) goto 98
            else
              call interpolate_2 (zout(ifirst),   &
                ilast-ifirst+1, xima%gil%fres,   &
                rchan-ifirst+1, xima%gil%freq+ff,   &
                wout(ifirst), zin, ni, r_lrfres(isb,is),   &
                ri, r_lrfoff(isb,is))
            endif
          endif
          xima%gil%vres = -xima%gil%fres*299792.458/xima%gil%freq
          if (resample) then
            do i=ifirst, ilast
              visi(1,i) =   &
                visi(1,i)+real(zout(i))*wout(i)
              visi(2,i) =   &
                visi(2,i)+aimag(zout(i))*wout(i)
              visi(3,i) = visi(3,i)+wout(i)
              if (first_polar(i).eq.0) then
                 first_polar(i) = ipola
                 if (isb.eq.(3-r_isb)/2) then
                    first_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                 else
                    first_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                 endif
                 nfirst(i) = 1
              elseif(ipola.eq.first_polar(i)) then
                 if (isb.eq.(3-r_isb)/2) then
                    first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                 else
                    first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                 endif
                 nfirst(i) = nfirst(i)+1
              elseif(second_polar(i).eq.0) then
                 second_polar(i) = ipola
                 if (isb.eq.(3-r_isb)/2) then
                    second_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                 else
                    second_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                 endif
                 nsecond(i) = 1
              elseif(ipola.eq.second_polar(i)) then
                 if (isb.eq.(3-r_isb)/2) then
                    second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                 else
                    second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                 endif
                 nsecond(i) = nsecond(i)+1
              endif
            enddo
          else
            k = 1
            w = 1.0            ! /(ILAST-IFIRST+1) removed 27.07.06 FG
            if (r_lrfres(isb,is)*xima%gil%fres.gt.0) then
              do i=ifirst, ilast
                visi(1,i) = visi(1,i)+real(zin(k))*w
                visi(2,i) = visi(2,i)+aimag(zin(k))*w
                visi(3,i) = visi(3,i)+w
                if (first_polar(i).eq.0) then
                   first_polar(i) = ipola
                   if (isb.eq.(3-r_isb)/2) then
                      first_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                   else
                      first_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                   endif
                   nfirst(i) = 1
                elseif(ipola.eq.first_polar(i)) then
                   if (isb.eq.(3-r_isb)/2) then
                      first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                   else
                      first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                   endif
                   nfirst(i) = nfirst(i)+1
                elseif(second_polar(i).eq.0) then
                   second_polar(i) = ipola
                   if (isb.eq.(3-r_isb)/2) then
                      second_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                   else
                      second_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                   endif
                   nsecond(i) = 1
                elseif(ipola.eq.second_polar(i)) then
                   if (isb.eq.(3-r_isb)/2) then
                      second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                   else
                      second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                   endif
                   nsecond(i) = nsecond(i)+1
                endif   
                k = k+1
              enddo
            else
              do i= ilast, ifirst, -1
                visi(1,i) = visi(1,i)+real(zin(k))*w
                visi(2,i) = visi(2,i)+aimag(zin(k))*w
                visi(3,i) = visi(3,i)+w
                k = k+1
                if (first_polar(i).eq.0) then
                   first_polar(i) = ipola
                   if (isb.eq.(3-r_isb)/2) then
                      first_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                   else
                      first_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                   endif
                   nfirst(i) = 1
                elseif(ipola.eq.first_polar(i)) then
                   if (isb.eq.(3-r_isb)/2) then
                      first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                   else
                      first_tsys(i) = first_tsys(i)                     &
     &                      +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                   endif
                   nfirst(i) = nfirst(i)+1
                elseif(second_polar(i).eq.0) then
                   second_polar(i) = ipola
                   if (isb.eq.(3-r_isb)/2) then
                      second_tsys(i) = r_tsyss(inbc,ia)*r_tsyss(inbc,ja) &
     &                   /dh_integ
                   else
                      second_tsys(i) = r_tsysi(inbc,ia)*r_tsysi(inbc,ja) &
     &                   /dh_integ
                   endif
                   nsecond(i) = 1
                elseif(ipola.eq.second_polar(i)) then
                   if (isb.eq.(3-r_isb)/2) then
                      second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsyss(inbc,ia)*r_tsyss(inbc,ja)/dh_integ
                   else
                      second_tsys(i) =second_tsys(i)                     &
     &                   +r_tsysi(inbc,ia)*r_tsysi(inbc,ja)/dh_integ
                   endif
                   nsecond(i) = nsecond(i)+1
                endif
              enddo
            endif
          endif
98        error = .false.
          continue
        endif
      enddo
      !
      ! Set the proper channel width now
      if (weight_cal) then
        w = 2./abs(zscale)**2 * abs(xima%gil%fres)
      else
        w = 2. * abs(xima%gil%fres)
      endif
      do i=1, nchan
        if (visi(3,i).ne.0) then
          visi(1,i) = visi(1,i)/visi(3,i)
          visi(2,i) = visi(2,i)/visi(3,i)
          if (nfirst(i).ne.0) then
             first_tsys(i) = first_tsys(i)/nfirst(i)
             ww = w / first_tsys(i)             
          endif
          if (nsecond(i).ne.0) then
             second_tsys(i) = second_tsys(i)/nsecond(i)
             ww = ww+ w / second_tsys(i) 
          endif
          if (ww.ne.0) then
             visi(3,i) = ww
          endif
          good = .true.
        endif
      enddo
      if (abs((save_res-xima%gil%fres)/xima%gil%fres).gt.1e-4) then
        write(chain,'(a,1pg13.6)')   &
          'Channel separation rounded to ',xima%gil%fres
        call message(4,1,'FILL_VISI',chain(1:lenc(chain)))
      endif
    endif
  endif
  return
2000 format('Subband ',a,' maps to output channels ',i9,' to ',i9)
end subroutine fill_visi
!
