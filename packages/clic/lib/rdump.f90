subroutine dldump(line,error)
  use gildas_def
  use gkernel_types
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  logical :: first_l, found
  character(len=64) :: csize
  integer :: nsize, ndatal
  type(sic_descriptor_t) :: desc
  integer(kind=address_length) :: ibx,iby,iba,ibp,ibi
  !
  data first_l/.true./
  save first_l
  !
  if (.not.first_l) then
    call sic_delvariable ('BERK_X',.false.,error)
    call sic_delvariable ('BERK_Y',.false.,error)
    call sic_delvariable ('BERK_A',.false.,error)
    call sic_delvariable ('BERK_P',.false.,error)
    call sic_delvariable ('BERK_I',.false.,error)
  endif
  first_l = .false.
  ndatal = r_ldatl/2
  write(csize,'(I12)') ndatal
  nsize = lenc(csize)
  call sic_black(csize,nsize)
  !
  call sic_libr('DEFINE REAL BERK_X['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('BERK_X', desc, found)
  if (.not. found) goto 999
  ibx = gag_pointer(desc%addr,memory)
  !
  call sic_libr('DEFINE REAL BERK_Y['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('BERK_Y', desc, found)
  if (.not. found) goto 999
  iby = gag_pointer(desc%addr,memory)
  !
  call sic_libr('DEFINE REAL BERK_A['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('BERK_A', desc, found)
  if (.not. found) goto 999
  iba = gag_pointer(desc%addr,memory)
  !
  call sic_libr('DEFINE REAL BERK_P['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('BERK_P', desc, found)
  if (.not. found) goto 999
  ibp = gag_pointer(desc%addr,memory)
  !
  call sic_libr('DEFINE REAL BERK_I['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('BERK_I', desc, found)
  if (.not. found) goto 999
  ibi = gag_pointer(desc%addr,memory)
  !
  call sub_dldump(line,memory(ibx),memory(iby),memory(iba),   &
    memory(ibp),memory(ibi),ndatal,error)
999 return
end subroutine dldump
!
subroutine sub_dldump(line,berkx,berky,berka,berkp,berki,ndatal,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  character(len=*) :: line          !
  integer :: ndatal                 !
  real :: berkx(ndatal)             !
  real :: berky(ndatal)             !
  real :: berka(ndatal)             !
  real :: berkp(ndatal)             !
  real :: berki(ndatal)             !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  character(len=255) :: ch
  !
  integer :: i,i1,i2,ib,nch
  integer(kind=size_length) :: nc,min8,max8
  real :: faz, ymax, gx1, gx2, gy1, gy2
  integer :: mvoc1, nkw
  parameter (mvoc1=4)
  character(len=12) :: argum, kw, voc1(mvoc1)
  data voc1/'AMPLITUDE','PHASE','REAL','IMAGINARY'/
  !
  do i=1, ndatal
    berkx(i) = real(datal(i))
    berky(i) = aimag(datal(i))
    berka(i) = abs(datal(i))
    berkp(i) = faz(datal(i))
    berki(i) = i
  enddo
  i1 = 1
  i2 = r_ldatl/2
  call sic_i4(line,0,2,i1,.false.,error)
  if (error) goto 999
  call sic_i4(line,0,3,i2,.false.,error)
  if (error) goto 999
  i1 = min(r_ldatl/2,max(1,i1))
  i2 = min(r_ldatl/2,max(1,i2))
  write(6,*) ' '
  write(6,*) 'Line Data channels (',i1,' to ',i2,') -----'
  write(6,1005) (datal(i),i=i1, i2)
  !
  if (sic_present(1,0)) then
    argum = 'AMPLITUDE'
    call sic_ke(line,1,1,argum,nch,.false.,error)
    if (error) goto 999
    call sic_ambigs('DUMP',argum,kw,nkw,voc1,mvoc1,error)
    if (error) goto 999
    call gtclear
    ymax = 1. / r_nbas
    nc = r_ldatl/2/r_nbas
    do ib=1, r_nbas
      gx1 = 0.15
      gx2 = 0.95
      gy1 = 0.20 + (0.75/r_nbas)*(r_nbas-ib)
      gy2 = 0.20 + (0.75/r_nbas)*(r_nbas-ib+1)
      write(ch,1100) 'SET VIEWPORT', gx1 ,gx2 ,gy1 ,gy2
      call gr_exec(ch(1:lenc(ch)))
      gx1 = (ib-1)*nc + 1
      gx2 = ib*nc
      if (kw.eq.'AMPLITUDE') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,berka(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS',  gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), berki(i1), berka(i1),blank4,d_blank4)
      elseif (kw.eq.'PHASE') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,berkp(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), berki(i1), berkp(i1),blank4,d_blank4)
      elseif (kw.eq.'REAL') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,berkx(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), berki(i1), berkx(i1),blank4,d_blank4)
      elseif (kw.eq.'IMAGINARY') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,berky(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), berki(i1), berky(i1),blank4,d_blank4)
      endif
      call gr_exec('BOX')
    enddo
  endif
  return
  !
1005 format(8(1pg10.2))
1100 format(a,4(1x,f19.12))
999 error = .true.
end subroutine sub_dldump
!
subroutine sdump(error)
  use gildas_def
  use classic_api
  use clic_rdata
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  !
  include 'clic_work.inc'
  !
  print *,'MNANT ',mnant,' ! Number of antennas'
  print *,'MNREC ',mnrec,' ! Number of receivers'
  print *,'MNBAD ',mnbas,' ! Number of baselines'
  print *,'MNTRI ',mntri,' ! Number of phase closures'
  print *,'MNQUAD ',mnquad,' ! Number of amplitude closures'
  print *,'MNBAT ',mnbat,' ! Number of complex data (MNBAD(+MNTRI))'
  print *,'MINTERC ',minterc,' ! Size of inteferometer data '
  print *,'MRFSET ',mrfset,' ! RF setup size'
  print *,'MCONTSET ',mcontset,'! Continuum setup size'
  print *,'MRLBAND ',mrlband,' ! Line setup'
  print *,'MLINESET_0 ',mlineset_0,' ! Size per correlator'
  print *,'MLINESET_1 ',mlineset_1,' ! Size per correlator per band'
  print *,'MLINESET ',mlineset,' ! Line setup size'
  print *,'MSCANNING ',mscanning,' ! Scanning '
  print *,'MPMODEL ',mpmodel,' ! Pointing model '
  print *,'MATPARM ',matparm,' ! Atmospheric parameters'
  print *,'MATPARM_0 ',matparm_0,' ! Atmospheric parameters'
  print *,'MATPARM_1 ',matparm_1,' ! Atmospheric parameters'
  print *,'MATMON ',matmon,' ! Atm monitoring'
  print *,'MWVR ',mwvr,' ! WVR monitoring'
  print *,'MWVRCH ',mwvrch,' ! Number of WVR channels'
  print *,'MALMA ',malma,' ! ALMA-specific data '
  print *,'MCH ',mch,' ! Number of channels per correlator'
  print *,'MCCH ',mcch,' ! number of continuum channels '
  print *,'MLCH ',mlch,' ! Number of line channels'
  print *,'MDATAC ',mdatac,' ! Size of Continuum data '
  print *,'MDATAL ',mdatal,' ! Size of Line data '
  !
  print *,'MBPC ',mbpc,' ! Size of Baseline bandpass section '
  print *,'MBPCDEF ',mbpcdeg,' ! Max Degree of bandpass '
  print *,'MABPC ',mabpc,' ! Size of Antenna bandpas section '
  !
  print *,'MIC',mic,' Size of Instrumental Calibration section'
  print *,'MICDEG',micdeg,' Max degree of Instrumental Calibration'
  print *,'MAIC',maic,' Size of Antenna Calibration'
  print *,'MDESCR ',mdescr,' ! Size of Description section'
  print *,'MDFILE ',mdfile,' ! Size of FILENAME section (?) '
  print *,'MDMODIF ',mdmodif,' ! Size of modification section'
  !
  ! Header
  print *,'M_HEADER ',m_header,' ! Size of Header'
  print *,'M_HEADER_NO_RF ',m_header_no_rf,' ! Size of Header w/o BP'
  print *,'M_HEADER_ANT_RF ',m_header_ant_rf,' ! Size of Header w/o Bas. BP'
  print *,'M_DH ',m_dh,' ! Size of Data Header'
  print *,'MALL ',mall,' ! Size of ALL sections'
  print *,'MMSEC ',mmsec,' ! Maximum size of all sections'
  !
  print *,'MDATA ',mdata,' ! Size of DATA BUFFER'
end subroutine sdump
!
subroutine rdump(line,error)
  use gildas_def
  use gkernel_interfaces
  use clic_file
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC	Debug routine
  ! 	Types the contents of an obs.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  character(len=11) :: cdobs,cdred
  integer :: j,l,k,m, nch, ii, i1, i2
  real :: faz
  !
  integer :: mvoc1, nkw, band
  parameter (mvoc1=23)
  character(len=12) :: argum, kw, voc1(mvoc1)
  character(len=2) :: chcode
  data voc1/'ALL','HEADER','GENERAL','POSITION','INTERFERO',   &
    'FREQUENCY','CONTINUUM','LINE','SCANNING','ATMOSPHERE',   &
    'MONITOR','PC','APC','IC','AIC',   &
    'DD','PHASES','FILE','DMOD','WVR','ALMA','STATUS','VLBI' /
  !------------------------------------------------------------------------
  ! Code:
  argum = 'ALL'
  call sic_ke(line,0,2,argum,nch,.false.,error)
  if (error) goto 999
  call sic_ambigs('DUMP',argum,kw,nkw,voc1,mvoc1,error)
  if (error) goto 999
  if (kw.eq.'ALL'.or.kw.eq.'HEADER') then
    write(6,*) '------------------------------------------------'
    write(6,*) 'HEADER -----------------------------------------'
    write(6,*) 'index # ',r_xnum,' version # ',e%version 
    write(6,*) 'block # ',ibuff%rstart, ' word # ', ibuff%wstart
    write(6,*) 'length in words ',e%nword
    write(6,*) 'data address ',e%adata,   &
      '    data length (words) ',e%ldata
    write(6,*) 'number of sections ',e%nsec,'   sections :'
    do j=1,e%nsec
      write(6,1006) j,' ',section(-e%seciden(j)),' code ',e%seciden(j),   &
        ' addr ',e%secaddr(j), ' len ',e%secleng(j)
    enddo
1006 format(i3,a,a,a,i3,a,i8,a,i8)
  endif
  !
  ! General
  !
  if (kw.eq.'ALL' .or. kw.eq.'GENERAL') then
    write(6,*) ' '
    write(6,*) 'GENERAL (GE)-------------------------------------'
    write(6,*) 'r_num = ',r_num,' r_ver = ',r_ver
    write(6,*) 'r_scan = ',r_scan,' r_qual = ', r_qual
    write(6,*) 'r_kind = ',r_kind
    write(6,1001) r_teles
1001 format(' r_teles = "',a,'"')
    write(6,*) 'r_dobs = ',r_dobs,'   r_dred = ',r_dred
    call datec(r_dobs,cdobs,error)
    call datec(r_dred,cdred,error)
    write(6,*) 'cdobs = ',cdobs,'   cdred = ',cdred
    write(6,*) 'r_typec = ', r_typec
    write(6,*) 'r_ut = ',r_ut,'   r_st = ',r_st
    write(6,*) 'r_az = ',r_az,'   r_el = ',r_el
    write(6,*) 'r_tau = ',r_tau,'   r_tsys = ',r_tsys,   &
      '   r_time = ',r_time
    write(6,*) 'r_typof = ',r_typof,'   r_proc = ',r_proc
    write(6,*) 'r_project = ', r_project
  endif
  !
  ! Position
  !
  if (kw.eq.'ALL' .or. kw.eq.'POSITION') then
    write(6,*) ' '
    write(6,*) 'POSITION (PO)-----------------------------------'
    write(6,1002) r_sourc
1002 format(' r_sourc = "',a,'"')
    write(6,*) 'r_proj = ', r_proj
    write(6,*) 'r_epoch = ',r_epoch,'   r_lam = ',r_lam,   &
      '   r_bet = ',r_bet
    write(6,*) 'r_lamof = ',r_lamof,'   r_betof = ',r_betof
    write(6,*) 'r_focus = ',r_focus,'   r_flux = ',r_flux, &
      '   r_spidx = ',r_spidx
    write(6,*) 'r_stokes_i = ', r_stokes_i,'    r_stokes_q = ',&
      r_stokes_q,'    r_stokes_u = ', r_stokes_u,'   r_stokes_v = ', &
      r_stokes_v
    write(6,*) 'r_spidx_stokes = ',(r_spidx_stokes(j),j=1,mnstokes)

  endif
  !
  ! Interferometer Configuration
  !
  if (kw.eq.'ALL' .or. kw.eq.'INTERFERO') then
    write(6,*) ' '
    write(6,*) 'Interferometer Configuration (IN)---------------'
    write(6,*) 'r_config = "',r_config(1:lenc(r_config)),'"'
    write(6,*) 'r_nant = ', r_nant,'   r_nbas = ', r_nbas
    write(6,*) 'r_itype = ', r_itype,'   r_houra = ', r_houra
    write(6,*) 'r_kant = ', (r_kant(m), m=1, r_nant)
    write(6,*) 'r_kent = ', (r_kent(m), m=1, r_nant)
    write(6,*) 'r_istat = ', (r_istat(m), m=1, r_nant)
    write(6,*) 'r_iant = ', (r_iant(m), m=1, r_nbas)
    write(6,*) 'r_jant = ', (r_jant(m), m=1, r_nbas)
    do j=1, 3
      write(6,*) 'r_bas(',j,',*) = ',(r_bas(j,m),m=1, r_nbas)
    enddo
    do j=1, 3
      write(6,*) 'r_ant(',j,',*) = ',(r_ant(j,m),m=1, r_nant)
    enddo
    write(6,*) 'r_ntri = ', r_ntri
    if (r_ntri.gt.0) then
      do j=1, 3
        write(6,*) 'r_atri(',j,',*) = ',(r_atri(j,m),m=1, r_ntri)
      enddo
    endif
    write(6,*) 'r_axes(*) = ',(r_axes(m),m=1, r_nant)
    write(6,*) 'r_phlo1(*) = ',(r_phlo1(m),m=1, r_nant)
    write(6,*) 'r_phlo3(*) = ',(r_phlo3(m),m=1, r_nant)
    write(6,*) 'r_nband_widex = ',r_nband_widex
  endif
  !
  ! RF Frequency Setup
  !
  if (kw.eq.'ALL' .or. kw.eq.'FREQUENCY') then
    write(6,*) ' '
    write(6,*) 'RF Frequency Setup (FR)-------------------------'
    write(6,1003) r_line
1003 format(' r_line = "',a,'"')
    write(6,*) 'r_isb = ', r_isb,'   r_lock = ',r_lock
    write(6,*) 'r_restf = ', r_restf, '   r_flo1 = ', r_flo1
    write(6,*) ' r_flo1_ref = ', r_flo1_ref,'   r_fif1 = ', r_fif1
    write(6,*) ' r_veloc = ',r_veloc,'   r_typev = ',r_typev
    write(6,*) 'r_doppl = ',r_doppl
    write(6,*) 'r_npol_rec = ', r_npol_rec
    write(6,*) 'r_quarter = ', r_quarter
    write(6,*) 'r_qwplate = ', r_qwplate
    write(6,*) 'r_nif = ', r_nif
    write(6,*) 'r_kif = ', (r_kif(m),m=1,r_nif)
    write(6,*) 'r_ifname = ', (r_ifname(m),m=1,r_nif)
    write(6,*) 'r_nbb = ',r_nbb 
    write(6,*) 'r_mapbb = ',(r_mapbb(m),m=1,r_nbb)
    write(6,*) 'r_kbb = ',(r_kbb(m),m=1,r_nbb)
    write(6,*) 'r_bbname = ',(r_bbname(m),m=1,r_nbb)
    write(6,*) 'r_polswitch(1) = ',(r_polswitch(m,1),m=1,r_nant)
    write(6,*) 'r_polswitch(2) = ',(r_polswitch(m,2),m=1,r_nant)
  endif
  !
  ! Continuum Setup
  !
  if (kw.eq.'ALL' .or. kw.eq.'CONTINUUM') then
    write(6,*) ' '
    write(6,*) 'Continuum Setup (CO)----------------------------'
    write(6,*) 'r_nsb = ', r_nsb, '    r_nband = ', r_nband
    write(6,*) 'r_ncdat = ', r_ncdat
    write(6,*) 'r_sband(1) = ', (r_sband(1,m),m=1,r_nband)
    write(6,*) 'r_sband(2) = ', (r_sband(2,m),m=1,r_nband)
    write(6,*) 'r_crch = ', r_crch
    write(6,*) 'r_cvres = ', r_cvres
    write(6,*) 'r_cvoff = ', r_cvoff
    write(6,*) 'r_crfoff = ', r_crfoff
    write(6,*) 'r_crfres = ', r_crfres
    write(6,*) 'r_cnam = ', r_cnam
    write(6,*) 'r_cfcen = ', (r_cfcen(m),m=1,r_nband)
    write(6,*) 'r_cfwid = ', (r_cfwid(m),m=1,r_nband)
  endif
  !
  ! Line Setup
  !
  if (kw.eq.'ALL' .or. kw.eq.'LINE') then
    band = 0
    call sic_i4(line,0,3,band,.false.,error)
    if (error) goto 999
    print *,"Band",band
    write(6,*) ' '
    write(6,*) 'Line Setup (LM)---------------------------------'
    write(6,*) 'r_lband = ', r_lband,'   r_lfour = ', r_lfour
    write(6,*) 'r_lntch = ',r_lntch,'   r_lmode = ', r_lmode
    write(6,*) 'r_lnbs = ',r_lnsb
    if (band.eq.0) then
      i1 = 1
      i2 = r_lband
    elseif(band.ge.0.and.band.le.r_lband) then
      i1 = band
      i2 = band
    else
      write(6,*) 'Invalid subband selection' 
      return
    endif
    do m=i1, i2
      write(6,*) '------------------------------------------------'
      write(6,*) 'Subband ',m,': r_lnch = ', r_lnch(m),   &
        '  r_lich = ',r_lich(m)
      write(6,*) 'r_lfcen = ', r_lfcen(m),   &
        '   r_lfres = ',r_lfres(m)
      write(6,*) 'r_lcench = ', r_lcench(m),   &
        '	r_iunit = ',r_iunit(m)
      write(6,*) 'r_lrch = ', (r_lrch(j,m), j=1,2)
      write(6,*) 'r_lrfoff = ', (r_lrfoff(j,m), j=1,2)
      write(6,*) 'r_lrfres = ', (r_lrfres(j,m), j=1,2)
      write(6,*) 'r_lvres = ', (r_lvres(j,m), j=1,2)
      write(6,*) 'r_lvoff = ', (r_lvoff(j,m), j=1,2)
      write(6,*) 'r_lnam = ', (r_lnam(j,m), j=1,2)
      write(6,*) 'r_lilevu'
      write(6,*) (r_lilevu(m,j),j=1, r_nant)
      write(6,*) 'r_lilevl'
      write(6,*) (r_lilevl(m,j),j=1, r_nant)
      write(6,*) 'r_band4 = ', r_band4(m)
      write(6,*) 'r_phselect'
      write(6,*) (r_phselect(j,m),j=1, r_nant)
      write(6,*) 'r_flo2',r_flo2(m)
      write(6,*) 'r_band2',r_band2(m)
      write(6,*) 'r_flo2bis',r_flo2bis(m)
      write(6,*) 'r_band2bis',r_band2bis(m)
      write(6,*) 'r_flo3',r_flo3(m)
      write(6,*) 'r_flo4',r_flo4(m)
      write(6,*) 'r_lpolmode',r_lpolmode(m)
      write(6,*) 'r_lpolentry', (r_lpolentry(j,m),j=1,r_nant)
      write(6,*) 'r_if',r_if(m),'r_bb', r_bb(m),'r_sb ',r_sb(m)
      write(6,*) 'r_lnsb', r_lnsb,'r_lsband', (r_lsband(j,m),j=1,r_lnsb)
      write(6,*) 'r_stachu', r_stachu(m),'r_nchu',r_nchu(m)
      write(6,*) 'r_code_stokes '
      write(6,*) (r_code_stokes(j,m),j=1, r_nbas)
    enddo
  endif
  !
  ! Scanning
  !
  if (kw.eq.'ALL' .or. kw.eq.'SCANNING') then
    write(6,*) ' '
    write(6,*) 'Scanning (SC)-----------------------'
    write(6,*) 'r_scaty = ',r_scaty
    write(6,*) 'r_mobil = ',r_mobil
    write(6,*) 'r_collaz = ',r_collaz
    write(6,*) 'r_collel = ',r_collel
    write(6,*) 'r_corfoc = ',r_corfoc
    write(6,*) 'r_pmodel = ',r_pmodel,'   r_npmodel',r_npmodel
    do m=1, r_npmodel
      write(6,*) 'i, r_cpmodel(m) = ', j,   &
        (r_cpmodel(m,j),j=1, r_nant)
    enddo
    write(6,*) 'r_nswitch = ',r_nswitch
    if (r_nswitch.gt.0) then
      write(6,*) 'r_wswitch = ',(r_wswitch(m),m=1, r_nswitch)
      write(6,*) 'r_pswitch(1) = ',(r_pswitch(1,m),m=1, r_nswitch)
      write(6,*) 'r_pswitch(2) = ',(r_pswitch(2,m),m=1, r_nswitch)
    endif
  endif
  !
  ! Status
  !
  if (kw.eq.'ALL' .or. kw.eq.'STATUS') then
    write(6,*) ' '
    write(6,*) ' Status (SC)-----------------------'
    write(6,*) 'r_status = ',r_status
    write(6,*) 'r_substatus = ',r_substatus
  endif
  !
  ! VLBI
  !
  if (kw.eq.'ALL' .or. kw.eq.'VLBI') then
    write(6,*) ' '
    write(6,*) ' VLBI (SC)-----------------------'
    write(6,*) 'r_refant = ',r_refant
    write(6,*) 'r_comparison_ant = ',r_comparison_ant
    write(6,*) 'r_nant_vlbi = ',r_nant_vlbi
    write(6,*) 'r_antmask = ',(r_antmask(j),j=1, r_nant_vlbi)
    write(6,*) 'r_maser_gps_diff = ',r_maser_gps_diff
    write(6,*) 'r_efftsys = ',(r_efftsys(j),j=1,r_nbb)
  endif
  !
  !
  ! Atmospheric Parameters
  !
  if (kw.eq.'ALL' .or. kw.eq.'ATMOSPHERE') then
    write(6,*) ' '
    write(6,*) 'Atmospheric Parameters (AT)---------------------'
    write(6,*) 'r_mode = ', r_mode,'   r_nrec = ',r_nrec
    write(6,*) 'r_pamb = ', r_pamb,'   r_tamb = ', r_tamb
    write(6,*) 'r_alti = ', r_alti,'   r_humid = ',r_humid
    do j=1, r_nbb
      write(6,*) 'r_h2omm(',j,',*) = ', (r_h2omm(j,m),m=1,r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tatms(',j,',*) = ', (r_tatms(j,m),m=1,r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tatmi(',j,',*) = ', (r_tatmi(j,m),m=1,r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_taus(',j,',*) = ', (r_taus(j,m),m=1,r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_taui(',j,',*) = ', (r_taui(j,m),m=1,r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_beef(',j,',*) = ', (r_beef(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_feff(',j,',*) = ', (r_feff(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tchop(',j,',*) = ', (r_tchop(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tcold(',j,',*) = ', (r_tcold(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_trec(',j,',*) = ', (r_trec(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_gim(',j,',*) = ', (r_gim(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_csky(',j,',*) = ', (r_csky(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,*) 'r_cchop(',j,',*) = ', (r_cchop(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_ccold(',j,',*) = ', (r_ccold(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tsyss(',j,',*) = ', (r_tsyss(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_tsysi(',j,',*) = ', (r_tsysi(j,m), m=1, r_nant   &
        )
    enddo
    do j=1, r_nbb
      write(6,*) 'r_ceff(',j,',*) = ', (r_ceff(j,m), m=1, r_nant)
    enddo
    do j=1, r_nbb
      write(6,1011) 'r_cmode(',j,') = ',   &
        (chcode(r_cmode(j,m)), m=1, r_nant)
    enddo
1011 format(1x,a,i2,a,12(1x,a2))
    do j=1, r_nbb
      write(6,*) 'r_jykel(',j,',*) = ', (r_jykel(j,m), m=1, r_nant   &
        )
    enddo
    write(6,*) 'r_avwind = ', r_avwind,   &
      ' r_avwindir = ', r_avwindir
    write(6,*) 'r_topwind = ', r_topwind,   &
      ' r_topwindir = ', r_topwindir
    write(6,*) 'r_tcabin = ', (r_tcabin(m), m=1, r_nant)
    write(6,*) 'r_tdewar(1) = ', (r_tdewar(1,m), m=1, r_nant)
    write(6,*) 'r_tdewar(2) = ', (r_tdewar(2,m), m=1, r_nant)
    write(6,*) 'r_tdewar(3) = ', (r_tdewar(3,m), m=1, r_nant)
    do j=1, r_nbb
      write(6,*) 'r_totscale(',j,',*) = ', (r_totscale(j,m), m=1,   &
        r_nant)
    enddo
  endif
  !
  ! Atmospheric Monitor
  !
  if (kw.eq.'MONITOR' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'Atmospheric Monitor (MO)---------------------'
    write(6,*) 'r_nrec_mon = ',r_nrec_mon
    write(6,*) 'r_frs_mon = ', r_frs_mon,   &
      '   r_fri_mon = ', r_fri_mon
    write(6,*) 'r_h2o_mon = ',   (r_h2o_mon(m),m=1,r_nant)
    write(6,*) 'r_tatms_mon = ', (r_tatms_mon(m),m=1,r_nant)
    write(6,*) 'r_tatmi_mon = ', (r_tatmi_mon(m),m=1,r_nant)
    write(6,*) 'r_taus_mon = ',  (r_taus_mon(m),m=1,r_nant)
    write(6,*) 'r_taui_mon = ',  (r_taui_mon(m),m=1,r_nant)
    write(6,*) 'r_feff_mon = ',  (r_feff_mon(m), m=1, r_nant)
    write(6,*) 'r_tchop_mon = ', (r_tchop_mon(m), m=1, r_nant)
    write(6,*) 'r_tcold_mon = ', (r_tcold_mon(m), m=1, r_nant)
    write(6,*) 'r_trec_mon = ',  (r_trec_mon(m), m=1, r_nant)
    write(6,*) 'r_gim_mon = ',   (r_gim_mon(m), m=1, r_nant)
    write(6,*) 'r_csky_mon = ', (r_csky_mon(m), m=1, r_nant)
    write(6,*) 'r_cchop_mon = ', (r_cchop_mon(m), m=1, r_nant)
    write(6,*) 'r_ccold_mon = ', (r_ccold_mon(m), m=1, r_nant)
    write(6,*) 'r_tsys_mon = ', (r_tsys_mon(m), m=1, r_nant)
    write(6,'(1x,a,12(1x,a2))') 'r_cmode_mon = ',   &
      (chcode(r_cmode_mon(m)), m=1, r_nant)
    write(6,*) 'r_path_mon = ',  (r_path_mon(m), m=1, r_nant)
    write(6,*) 'r_tem_mon = ',   (r_tem_mon(m), m=1, r_nant)
    write(6,*) 'r_dpath_mon = ', (r_dpath_mon(m), m=1, r_nant)
    write(6,*) 'r_ok_mon = ', (r_ok_mon(m), m=1, r_nant)
  endif
  !
  ! WVR section
  !
  if (kw.eq.'WVR' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'Water Vapor Radiometer (WVR)---------------------'
    write(6,*) 'r_wvrnch = ', (r_wvrnch(m), m=1, r_nant)
    do j=1, mwvrch
      write(6,*) 'r_wvrfreq(',j,') = ',   &
        (r_wvrfreq(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrbw(',j,') = ', (r_wvrbw(j,m), m=1, r_nant)
    enddo
    write(6,*) 'r_wvrtamb = ', (r_wvrtamb(m), m=1, r_nant)
    write(6,*) 'r_wvrtpel = ', (r_wvrtpel(m), m=1, r_nant)
    do j=1, mwvrch
      write(6,*) 'r_wvrtcal(',j,') = ',   &
        (r_wvrtcal(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrref(',j,') = ',   &
        (r_wvrref(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvraver(',j,') = ',   &
        (r_wvraver(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvramb(',j,') = ',   &
        (r_wvramb(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrtrec(',j,') = ',   &
        (r_wvrtrec(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrfeff(',j,') = ',   &
        (r_wvrfeff(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrlabtcal(',j,') = ',   &
        (r_wvrlabtcal(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrlabtrec(',j,') = ',   &
        (r_wvrlabtrec(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrlabtdio(',j,') = ',   &
        (r_wvrlabtdio(j,m), m=1, r_nant)
    enddo
    write(6,*) 'r_wvrmode = ', (r_wvrmode(m), m=1, r_nant)
    write(6,*) 'r_wvrh2o = ', (r_wvrh2o(m), m=1, r_nant)
    write(6,*) 'r_wvrpath = ', (r_wvrpath(m), m=1, r_nant)
    do j=1, mwvrch
      write(6,*) 'r_wvrtsys(',j,') = ',   &
        (r_wvrtsys(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrdpath(',j,') = ',   &
        (r_wvrdpath(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrfpath(',j,') = ',   &
        (r_wvrfpath(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrliq(',j,') = ',   &
        (r_wvrliq(j,m), m=1, r_nant)
    enddo
    do j=1, mwvrch
      write(6,*) 'r_wvrdcloud(',j,') = ',   &
        (r_wvrdcloud(j,m), m=1, r_nant)
    enddo
    write(6,*) 'r_wvrtatm = ', (r_wvrtatm(m), m=1, r_nant)
    write(6,*) 'r_wvrqual = ', (r_wvrqual(m), m=1, r_nant)
  endif
  !
  ! Baseline Passband Calibration
  !
  if (kw.eq.'ALL' .or. kw.eq.'PC') then
    write(6,*) ' '
    write(6,*) 'RF Passband Calibration (PC)---------------'
    write(6,2000) 'r_bpc = ', r_bpc, '   r_bpcdeg = ', r_bpcdeg
2000 format (1x,a,o11.11,a,i3)
    if (r_bpc.ne.0) then
      do j=1, 2
        write(6,*) 'r_bpcsba(',j,',*) = ',   &
          (faz(r_bpcsba(1,j,l)),l=1, r_nbas)
      enddo
      do ii = 1,2
        do m=1, r_nband
          if (r_bpccamp(ii,j,1,m).ne.0 .and.   &
            r_bpccamp(ii,j,1,m).ne.1) then
            do j=1, 2
              write(6,*) 'r_bpccamp(',ii,',',j,',*,',m,') = ',   &
                (r_bpccamp(ii,j,l,m),l=1, r_nbas)
            enddo
          endif
        enddo
        do m=1, r_nband
          do j=1, 2
            if (r_bpccpha(ii,j,1,m).ne.0) then
              write(6,*) 'r_bpccpha(',ii,',',j,',*,',m,') = ',   &
                (r_bpccpha(ii,j,l,m),l=1, r_nbas)
            endif
          enddo
        enddo
        do m=0,r_bpcdeg
          do k=1, r_lband
            do j=1, 2
              if (r_bpclamp(ii,j,1,k,m).ne.0 .and.   &
                r_bpclamp(ii,j,1,k,m).ne.1 ) then
                write(6,*)   &
                  'r_bpclamp(',ii,',',j,',*,',k,',',m,') = ',   &
                  (r_bpclamp(ii,j,l,k,m),l=1, r_nbas)
              endif
            enddo
          enddo
        enddo
        do m=0,r_bpcdeg
          do k=1, r_lband
            do j=1, 2
              if (r_bpclpha(ii,j,1,k,m).ne.0) then
                write(6,*)   &
                  'r_bpclpha(',ii,',',j,',*,',k,',',m,') = ',   &
                  (r_bpclpha(ii,j,l,k,m),l=1, r_nbas)
              endif
            enddo
          enddo
        enddo
      enddo
      write(6,*) 'r_bpfdeg = ',r_bpfdeg
      write(6,*) 'r_bpflim = ',r_bpflim
      do m=0,r_bpfdeg
        do j=1, 2
          do k=1, 2
            if (r_bpfamp(k,j,1,m).ne.0 .and.   &
              r_bpfamp(k,j,1,m).ne.1) then
              write(6,*) 'r_bpfamp(',k,',',j,',*,',m,') = ',   &
                (r_bpfamp(k,j,l,m),l=1, r_nbas)
            endif
          enddo
        enddo
      enddo
      do m=0,r_bpfdeg
        do j=1, 2
          do k =1, 2
            if (r_bpfpha(k,j,1,m).ne.0) then
              write(6,*) 'r_bpfpha(',k,',',j,',*,',m,') = ',   &
                (r_bpfpha(k,j,l,m),l=1, r_nbas)
            endif
          enddo
        enddo
      enddo
    endif
  endif
  !
  ! Antenna Passband Calibration
  !
  if (kw.eq.'ALL' .or. kw.eq.'APC') then
    write(6,*) ' '
    write(6,*) 'Antenna RF Passband Calibration (APC)-----------'
    write(6,2000) 'r_abpc = ', r_abpc, '   r_abpcdeg = ', r_abpcdeg
    if (r_abpc.ne.0) then
      do j=1, 2
        write(6,*) 'r_abpcsba(',j,',*) = ',   &
          (faz(r_abpcsba(1,j,l)),l=1, r_nant)
      enddo
      do ii = 1,2
        do m=1, r_nband
          do j=1, 2
            if (r_abpccamp(ii,j,1,m).ne.0 .and.   &
              r_abpccamp(ii,j,1,m).ne.1) then
              write(6,*)   &
                'r_abpccamp(',ii,',',j,',*,',m,') = ',   &
                (r_abpccamp(ii,j,l,m),l=1, r_nant)
            endif
          enddo
        enddo
        do m=1, r_nband
          do j=1, 2
            if (r_abpccpha(ii,j,1,m).ne.0) then
              write(6,*)   &
                'r_abpccpha(',ii,',',j,',*,',m,') = ',   &
                (r_abpccpha(ii,j,l,m),l=1, r_nant)
            endif
          enddo
        enddo
        do m=0,r_abpcdeg
          do k=1, r_lband
            do j=1, 2
              if (r_abpclamp(ii,j,1,k,m).ne.0 .and.   &
                r_abpclamp(ii,j,1,k,m).ne.1) then
                write(6,*)   &
                  'r_abpclamp(',ii,',',j,',*,',k,',',m,') = ',   &
                  (r_abpclamp(ii,j,l,k,m),l=1, r_nant)
              endif
            enddo
          enddo
        enddo
        do m=0,r_abpcdeg
          do k=1, r_lband
            do j=1, 2
              if (r_abpclpha(ii,j,r_nant,k,m).ne.0) then
                write(6,*)   &
                  'r_abpclpha(',ii,',',j,',*,',k,',',m,') = ',   &
                  (r_abpclpha(ii,j,l,k,m),l=1, r_nant)
              endif
            enddo
          enddo
        enddo
      enddo
      write(6,*) 'r_abpfdeg = ',r_abpfdeg
      write(6,*) 'r_abpflim = ',r_abpflim
      do m=0,r_abpfdeg
        do j=1, 2
          do k =1,2
            if (r_abpfamp(k,j,1,m).ne.0 .and.   &
              r_abpfamp(k,j,1,m).ne.1) then
              write(6,*) 'r_abpfamp(',k,',',j,',*,',m,') = ',   &
                (r_abpfamp(k,j,l,m),l=1, r_nant)
            endif
          enddo
        enddo
      enddo
      do m=0,r_abpfdeg
        do j=1, 2
          do k =1,2
            if (r_abpfpha(k,j,r_nant,m).ne.0) then
              write(6,*) 'r_abpfpha(',k,',',j,',*,',m,') = ',   &
                (r_abpfpha(k,j,l,m),l=1, r_nant)
            endif
          enddo
        enddo
      enddo
    endif
  endif
  !
  ! Instrumental Calibration
  !
  if (kw.eq.'ALL' .or. kw.eq.'IC') then
    write(6,*) ' '
    write(6,*) 'Instrumental Calibration (IC)-------------------'
    write(6,2000) 'r_ic= ', r_ic, '   r_icdeg= ', r_icdeg
    if (r_ic.ne.0) then
      do m=1,2
        do j=1,2
          do l=1,2
            write(6,*) 'r_icrms(',l,',',j,',*,',m,') = ',   &
              (r_icrms(l,j,k,m),k=1, r_nbas)
          enddo
        enddo
      enddo
      do m=0,r_icdeg
        do j=1, 2
          do l=1,2
            if (r_icamp(l,j,1,m).ne.0) then
              write(6,*) 'r_icamp(',l,',',j,',*,',m,') = ',   &
                (r_icamp(l,j,k,m),k=1, r_nbas)
            endif
          enddo
        enddo
      enddo
      do m=0,r_icdeg
        do j=1, 2
          do l=1,2
            if (r_icpha(l,j,r_nant,m).ne.0) then
              write(6,*) 'r_icpha(',l,',',j,',*,',m,') = ',   &
                (r_icpha(l,j,k,m),k=1, r_nbas)
            endif
          enddo
        enddo
      enddo
    endif
  endif
  !
  ! Antenna Instrumental Calibration
  !
  if (kw.eq.'ALL' .or. kw.eq.'AIC') then
    write(6,*) ' '
    write(6,*) 'Antenna Instrumental Calibration (AIC)------------'
    write(6,2000) 'r_aic= ', r_aic,   &
      '   r_aicdeg= ', r_aicdeg
    if (r_aic.ne.0) then
      do m=1,2
        do j=1,2
          do l=1,2
            write(6,*) 'r_aicrms(',l,',',j,',*,',m,') = ',   &
              (r_aicrms(l,j,k,m),k=1, r_nbas)
          enddo
        enddo
      enddo
      do m=0,r_aicdeg
        do j=1, 2
          do l=1,2
            if (r_aicamp(l,j,1,m).ne.0) then
              write(6,*) 'r_aicamp(',l,',',j,',*,',m,') = ',   &
                (r_aicamp(l,j,k,m),k=1, r_nant)
            endif
          enddo
        enddo
      enddo
      do m=0,r_aicdeg
        do j=1, 2
          do l=1,2
            if (r_aicpha(l,j,r_nant,m).ne.0) then
              write(6,*) 'r_aicpha(',l,',',j,',*,',m,') = ',   &
                (r_aicpha(l,j,k,m),k=1, r_nant)
            endif
          enddo
        enddo
      enddo
    endif
  endif
  !
  ! Data Section Descriptor
  !
  if (kw.eq.'DD' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'Data Section Descriptor (DD)--------------------'
    write(6,*) 'r_ndump = ', r_ndump
    write(6,*) 'r_ldump = ', r_ldump
    write(6,*) 'r_ldpar = ',r_ldpar
    write(6,*) 'r_ldatc = ',r_ldatc
    write(6,*) 'r_ldatl = ',r_ldatl
    write(6,*) 'r_ndatl = ',r_ndatl
  endif
  if (kw.eq.'ALMA' .or. kw.eq.'ALL') then
    write(6,*) 'r_trandist = ',r_trandist,   &
      '   r_tranfocu = ', r_tranfocu
    l = lenc(r_execblock)
    write(6,*) 'R_ExecBlock = ',r_execblock(1:l)
    write(6,*) 'R_AntennaName = ',(r_antennaname(m)//' ',m=1,   &
      r_nant)
    write(6,*) 'R_AntennaType = ',(r_antennatype(m)//' ',m=1,   &
      r_nant)
    write(6,*) 'R_DishDiameter = ',(r_dishdiameter(m), m=1   &
      ,r_nant)
  endif
  !
  ! Phases (summary)
  !
  if (kw.eq.'PHASES' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'Summary Phases (PH) in radians ----------------'
    write(6,1000) 'Antennas: ', (m,m=1,r_nant)
1000 format(a12,6i12)
1004 format(a12,6f12.3)
    write(6,1004) 'USB SBA     ',   &
      (faz(r_abpcsba(1,1,l)),l=1, r_nant)
    write(6,1004) 'USB CAL Int ',(r_aicpha(1,1,l,0),l=1, r_nant)
    write(6,1004) 'USB     Ext ',(r_aicpha(1,1,l,1),l=1, r_nant)
    write(6,1004) 'USB CAL Ext ',(r_aicpha(1,1,l,2),l=1, r_nant)
    write(6,1004) 'LSB SBA     ',   &
      (faz(r_abpcsba(1,2,l)),l=1, r_nant)
    write(6,1004) 'LSB CAL Int ',(r_aicpha(1,2,l,0),l=1, r_nant)
    write(6,1004) 'LSB     Ext ',(r_aicpha(1,2,l,1),l=1, r_nant)
    write(6,1004) 'LSB CAL Ext ',(r_aicpha(1,2,l,2),l=1, r_nant)
  endif
  !
  ! Data File
  !
  if (kw.eq.'FILE' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'External Data File (FM)--------------------'
    write(6,*) 'r_dxnum = ', r_dxnum
    write(6,*) 'r_dfile = ',r_dfile(1:lenc(r_dfile))
  endif
  !
  ! Data Modifier
  !
  if (kw.eq.'DMOD' .or. kw.eq.'ALL') then
    write(6,*) ' '
    write(6,*) 'Data Modifier (DM)--------------------'
    write(6,*) 'r_dmaflag = ',(r_dmaflag(m),m=1,r_nant)
    write(6,*) 'r_dmbflag = ',(r_dmbflag(m),m=1,r_nbas)
    do k=1, r_nbb
      write(6,*) 'r_dmatfac(',k,',1,*) = ',   &
        (r_dmatfac(k,1,j),j=1, r_nant)
    enddo
    do k=1, r_nbb
      write(6,*) 'r_dmatfac(',k,',2,*) = ',   &
        (r_dmatfac(k,2,j),j=1, r_nant)
    enddo
    do k=1, r_nbb
      write(6,*) 'r_dmdelay(',r_mapbb(k),',*) = ', &
          (r_dmdelay(r_mapbb(k),m),m=1,r_nant)
    enddo
    do m=1,r_nband
      do j=1, 2
        write(6,*) 'r_dmcamp(',j,',*,',m,') = ',   &
          (r_dmcamp(j,k,m),k=1, r_nbas)
      enddo
    enddo
    do m=1,r_nband
      do j=1, 2
        write(6,*) 'r_dmcpha(',j,',*,',m,') = ',   &
          (r_dmcpha(j,k,m),k=1, r_nbas)
      enddo
    enddo
    do m=1,r_lband
      do j=1, 2
        write(6,*) 'r_dmlamp(',j,',*,',m,') = ',   &
          (r_dmlamp(j,k,m),k=1, r_nbas)
      enddo
    enddo
    do m=1,r_lband
      do j=1, 2
        write(6,*) 'r_dmlpha(',j,',*,',m,') = ',   &
          (r_dmlpha(j,k,m),k=1, r_nbas)
      enddo
    enddo
    do m=1,r_lband
      do j=1, 2
        write(6,*) 'r_dmldph(',j,',*,',m,') = ',   &
          (r_dmldph(j,k,m),k=1, r_nbas)
      enddo
    enddo
    do j=1,r_nant
      write(6,'(a,i0,a,(32L))') 'r_dmsaflag_g(,',j,') =',(r_dmsaflag(m,j),m=1, 2*mbands)
    enddo
    do j=1,r_nbas
      write(6,'(a,i0,a,(32L))') 'r_dmsbflag(,',j,') =',(r_dmsbflag(m,j),m=1, 2*mbands)
    enddo
!    write(6,*) 'r_dmsaflag = ',(r_dmsaflag(m),m=1,r_nant)
!    write(6,*) 'r_dmsbflag = ',(r_dmsbflag(m),m=1,r_nbas)
  endif
  return
999 error = .true.
  return
end subroutine rdump
!
subroutine dhdump(error)
  use gildas_def
  use classic_api
  use clic_rdata
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  character(len=11) :: cdobs
  integer :: i,j, k
  !
  ! Data Header
  !
  write(6,*) ' '
  write(6,*) '------------------------------------------------'
  write(6,*) 'Data Header ------------------------------------'
  write(6,*) 'dh_dump = ',dh_dump
  write(6,*) 'dh_integ = ',dh_integ,'	dh_obs = ', dh_obs
  call datec(dh_obs,cdobs,error)
  write(6,*) 'cdobs = ',cdobs
  write(6,*) 'dh_utc = ', dh_utc
  write(6,*) 'dh_svec = ', dh_svec
  write(6,*) 'dh_test0 = ', (dh_test0(i),i=1,10)
  write(6,*) 'dh_ntime = ',dh_ntime
  write(6,*) 'dh_time_monitoring = ', (dh_time_monitoring(i),i=1,mntime)
  do j=1, mnant
    write(6,*) 'dh_test1 (ant ',j,') = ', (dh_test1(i,j),i=1,5)
  enddo
  do j=1, mnbas
    write(6,*) 'dh_rmsamp(1,*,',j,') = ',(dh_rmsamp(1,i,j),i=1,2)
    write(6,*) 'dh_rmsamp(2,*,',j,') = ',(dh_rmsamp(2,i,j),i=1,2)
    write(6,*) 'dh_rmspha(1,*,',j,') = ',(dh_rmspha(1,i,j),i=1,2)
    write(6,*) 'dh_rmspha(2,*,',j,') = ',(dh_rmspha(1,i,j),i=1,2)
  enddo
  write(6,'(a,(6z10))') 'dh_aflag = ',(dh_aflag(i),i=1,r_nant)
  write(6,'(a,(6z10))') 'dh_bflag = ',(dh_bflag(i),i=1,r_nbas)
  do k=1, r_nbb
    write(6,*) 'dh_total(',k,') = ', (dh_total(k,i), i=1, r_nant)
  enddo
  write(6,*) 'dh_uvm(1,*) = ', (dh_uvm(1,i),i=1, r_nbas)
  write(6,*) 'dh_uvm(2,*) = ', (dh_uvm(2,i),i=1, r_nbas)
  write(6,*) 'dh_atfac(1,1,*) = ', (dh_atfac(1,1,i),i=1, r_nant)
  write(6,*) 'dh_atfac(1,2,*) = ', (dh_atfac(1,2,i),i=1, r_nant)
  write(6,*) 'dh_atfac(2,1,*) = ', (dh_atfac(2,1,i),i=1, r_nant)
  write(6,*) 'dh_atfac(2,2,*) = ', (dh_atfac(2,2,i),i=1, r_nant)
  write(6,*) 'dh_rmspe(1,*) = ', (dh_rmspe(1,i),i=1, r_nant)
  write(6,*) 'dh_rmspe(2,*) = ', (dh_rmspe(2,i),i=1, r_nant)
  do j=1, r_nbb ! Number of bbands
    write(6,*) 'dh_delay(',r_mapbb(j),',*) = ', (dh_delay(j,i),i=1, r_nant)
  enddo
  write(6,*) 'dh_delayc = ', (dh_delayc(i),i=1, r_nant)
  write(6,*) 'dh_delcon = ', (dh_delcon(i),i=1, r_nant)
  do j=1, r_nbb ! Number of bbands
     write(6,*) 'dh_dellin(',r_mapbb(k),',*) = ', (dh_dellin(r_mapbb(j),i),i=1, r_nant)
  enddo
  write(6,*) 'dh_phasec = ', (dh_phasec(i),i=1, r_nant)
  write(6,*) 'dh_phase = ', (dh_phase(i),i=1, r_nant)
  write(6,*) 'dh_ratec = ', (dh_ratec(i),i=1, r_nant)
  write(6,*) 'dh_cable = ', (dh_cable(i),i=1, r_nant)
  write(6,*) 'dh_cable_alterante = ', (dh_cable_alternate(i),i=1, r_nant)
  write(6,*) 'dh_gamme = ', (dh_gamme(i),i=1, r_nant)
  write(6,*) 'dh_offlam = ', (dh_offlam(i),i=1, r_nant)
  write(6,*) 'dh_offbet = ', (dh_offbet(i),i=1, r_nant)
  write(6,*) 'dh_offfoc = ', (dh_offfoc(i),i=1, r_nant)
  write(6,*) 'dh_infac(1,*) = ', (dh_infac(1,i),i=1, r_nbas)
  write(6,*) 'dh_infac(2,*) = ', (dh_infac(2,i),i=1, r_nbas)
  do j=1, mwvrch
    write(6,*) 'dh_wvr(',j,',*) = ', (dh_wvr(j,i),i=1, r_nant)
  enddo
  write(6,'(a,6z10)') 'dh_wvrstat = ', (dh_wvrstat(i),i=1, r_nant)
  do j=1,mrlband
    write(6,*) 'dh_actp(1,*,',j,') = ',(dh_actp(1,i,j),i=1, mnant)
    write(6,*) 'dh_actp(2,*,',j,') = ',(dh_actp(2,i,j),i=1, mnant)
  enddo
  do j=1, r_nif
    write(6,*) 'dh_anttp(',j,',*) = ',(dh_anttp(j,i),i=1, mnant)
  enddo
  do k=1,r_nant
    write(6,'(a,i0,a,(32L))') 'dh_saflag(,',k,') =',(dh_saflag(i,k),i=1, 2*mrlband)
  enddo
  do k=1,r_nbas
    write(6,'(a,i0,a,(32L))') 'dh_sbflag(,',k,') =',(dh_sbflag(i,k),i=1, 2*mrlband)
  enddo
!  write(6,'(a,(6z10))') 'dh_saflag = ',(dh_saflag(i),i=1,r_nant)
!  write(6,'(a,(6z10))') 'dh_sbflag = ',(dh_sbflag(i),i=1,r_nbas)
!  enddo
end subroutine dhdump
!
subroutine dcdump(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  character(len=255) :: ch
  logical :: first_c
  real :: faz, ymax, gx1, gx2, gy1, gy2
  real :: contx(mdatac),conty(mdatac),conta(mdatac),contp(mdatac)
  real :: conti(mdatac)
  integer :: i,i1,i2,ib,nch
  integer(kind=size_length) :: nc,min8,max8
  !
  integer :: mvoc1, nkw
  parameter (mvoc1=4)
  character(len=12) :: argum, kw, voc1(mvoc1)
  data voc1/'AMPLITUDE','PHASE','REAL','IMAGINARY'/
  !
  data first_c/.true./
  save first_c
  !
  if (.not.first_c) then
    call sic_delvariable ('CONT_X',.false.,error)
    call sic_delvariable ('CONT_Y',.false.,error)
    call sic_delvariable ('CONT_A',.false.,error)
    call sic_delvariable ('CONT_P',.false.,error)
    call sic_delvariable ('CONT_I',.false.,error)
  endif
  first_c = .false.
  call sic_def_real('CONT_X',contx,1,r_ldatc/2,.false.,error)
  call sic_def_real('CONT_Y',conty,1,r_ldatc/2,.false.,error)
  call sic_def_real('CONT_A',conta,1,r_ldatc/2,.false.,error)
  call sic_def_real('CONT_P',contp,1,r_ldatc/2,.false.,error)
  call sic_def_real('CONT_I',conti,1,r_ldatc/2,.false.,error)
  do i=1, r_ldatc/2
    contx(i) = real(datac(i))
    conty(i) = aimag(datac(i))
    conta(i) = abs(datac(i))
    contp(i) = faz(datac(i))
    conti(i) = i
  enddo
  i1 = 1
  i2 = r_ldatc/2
  call sic_i4(line,0,2,i1,.false.,error)
  if (error) goto 999
  call sic_i4(line,0,3,i2,.false.,error)
  if (error) goto 999
  i1 = min(r_ldatc/2,max(1,i1))
  i2 = min(r_ldatc/2,max(1,i2))
  write(6,*) 'Cont. Data channels (',i1,' to ',i2,') -----'
  write(6,1005) (datac(i),i=i1, i2)
  if (sic_present(1,0)) then
    argum = 'AMPLITUDE'
    call sic_ke(line,1,1,argum,nch,.false.,error)
    if (error) goto 999
    call sic_ambigs('DUMP',argum,kw,nkw,voc1,mvoc1,error)
    call gtclear
    ymax = 1. / r_nbas
    nc = r_ldatc/2/r_nbas
    do ib=1, r_nbas
      gx1 = 0.15
      gx2 = 0.95
      gy1 = 0.20 + (0.75/r_nbas)*(r_nbas-ib)
      gy2 = 0.20 + (0.75/r_nbas)*(r_nbas-ib+1)
      write(ch,1100) 'SET VIEWPORT',  gx1 ,gx2 ,gy1 ,gy2
      call gr_exec(ch(1:lenc(ch)))
      gx1 = (ib-1)*nc + 1
      gx2 = ib*nc
      if (kw.eq.'AMPLITUDE') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,conta(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        if (gy1*gy2.eq.0) gy2 = 1.
        i1 = (ib-1)*nc + 1
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), conti(i1), conta(i1),blank4,d_blank4)
      elseif (kw.eq.'PHASE') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,contp(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), conti(i1), contp(i1),blank4,d_blank4)
      elseif (kw.eq.'REAL') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,contx(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), conti(i1), contx(i1),blank4,d_blank4)
      elseif (kw.eq.'IMAGINARY') then
        i1 = (ib-1)*nc + 1
        call gr4_extrema(nc,conty(i1),blank4,d_blank4,   &
          gy1, gy2, min8, max8)
        i1 = (ib-1)*nc + 1
        if (gy1*gy2.eq.0) gy2 = 1.
        write(ch,1100) 'LIMITS', gx1,gx2,0.95*gy1,1.05*gy2
        call gr_exec(ch(1:lenc(ch)))
        call gr4_histo (int(nc,kind=4), conti(i1), conty(i1),blank4,d_blank4)
      endif
      call gr_exec('BOX')
    enddo
  endif
  return
  !
1005 format(8(1pg10.2))
1100 format(a,4(1x,f19.12))
999 error = .true.
end subroutine dcdump
!
subroutine idump(error)
  use gildas_def
  use clic_file
  use clic_index
  use clic_rdata
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: ii
  !
  write(6,*) '------------------------------------------------'
  write(6,*) 'INDEX ------------------------------------------'
  write(6,*) 'Input file index :'
  write(6,*) 'ixnext =',i%desc%xnext
  do ii=1,i%desc%xnext-1
    write(6,*) ii,ix_num(ii),ix_ver(ii),ix_bloc(ii),ix_word(ii)
  enddo
  write(6,*) ' '
  write(6,*) 'Output file index :'
  write(6,*) 'oxnext =',o%desc%xnext
  do ii=1,o%desc%xnext-1
    write(6,*) ii,ox_num(ii),ox_ver(ii),ox_bloc(ii),ox_word(ii)
  enddo
  write(6,*) ' '
  write(6,*) 'Current index :'
  write(6,*) 'cxnext =',cxnext
  do ii=1,cxnext-1
    write(6,*) cx_ind(ii),cx_num(ii),cx_ver(ii),cx_bloc(ii),cx_word(ii)
  enddo
end subroutine idump
!
subroutine filedump(error)
  use gildas_def
  use clic_file
  use clic_index
  use clic_rdata
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: ii
  !
  write(6,*) '------------------------------------------------'
  write(6,*) 'FILE -------------------------------------------'
  write(6,*) 'ilun = ',i%lun
  if (i%lun.ne.0) then
    write(6,*) i%spec(1:i%nspec)
    write(6,*) 'inext = ',i%desc%nextrec,'  ixnext = ',i%desc%xnext,   &
      '  ilex = ',i%desc%lex1,'  imex = ',i%desc%mex
    if (i%desc%mex.ne.0) write(6,*) 'iex = ',(i%desc%aex(ii),ii=1,i%desc%mex)
  endif
  write(6,*) 'olun = ',o%lun
  if (o%lun.ne.0) then
    write(6,*) o%spec(1:o%nspec)
    write(6,*) 'onext = ',o%desc%nextrec,'  oxnext = ',o%desc%xnext,   &
      '  olex = ',o%desc%lex1,'  omex = ',o%desc%mex
    if (o%desc%mex.ne.0) write(6,*) 'oex = ',(o%desc%aex(ii),ii=1,o%desc%mex)
  endif
  write(6,*) 'dlun = ',d%lun
  if (d%lun.ne.0) then
    write(6,*) d%spec(1:d%nspec)
    write(6,*) 'dnext = ',d%desc%nextrec,'  dxnext = ',d%desc%xnext,   &
      '  dlex = ',d%desc%lex1,'  dmex = ',d%desc%mex
    if (d%desc%mex.ne.0) write(6,*) 'dex = ',(d%desc%aex(ii),ii=1,o%desc%mex)
  endif
end subroutine filedump
!
subroutine vdump(error)
  use gildas_def
  use clic_file
  use clic_index
  use clic_rdata
  use clic_virtual
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: ii
  !
  write(6,*) 'Headers ------------------------------'
  do ii=1, i%desc%xnext-1
    if (got_header(ii)) then
      write(6,*) ii, got_header(ii), v_header(ii),v_header_length(ii)
    endif
  enddo
  write(6,*) 'Data    ------------------------------'
  do ii=1, i%desc%xnext-1
    if (got_data(ii)) then
      write(6,*) ii, got_data(ii), v_data(ii), v_data_length(ii)
    endif
  enddo
end subroutine vdump
!
subroutine iidump(chain,error)
  use gildas_def
  use clic_file
  use clic_index
  use clic_rdata
  !---------------------------------------------------------------------
  ! A simple debugging routine, to help debug
  ! Call it when needed to make a pause in the program...
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: mx,ii
  return
  !
  mx = 20
  write(6,*) '------------------------------------------------'
  write(6,*) 'INDEX ------------------------------------------'
  write(6,*) 'Input file index :'
  write(6,*) 'ixnext =',i%desc%xnext
  do ii=1,min(mx,i%desc%xnext-1)
    write(6,*) ii,ix_num(ii),ix_ver(ii),ix_bloc(ii),ix_word(ii)
  enddo
  write(6,*) ' '
  write(6,*) 'Output file index :'
  write(6,*) 'oxnext =',o%desc%xnext
  do ii=1,min(mx,o%desc%xnext-1)
    write(6,*) ii,ox_num(ii),ox_ver(ii),ox_bloc(ii),ox_word(ii)
  enddo
  write(6,*) ' '
  write(6,*) 'Current index :'
  write(6,*) 'cxnext =',cxnext
  do ii=1,min(mx,cxnext-1)
    write(6,*) cx_ind(ii),cx_num(ii),cx_ver(ii),cx_bloc(ii),cx_word(ii)
  enddo
  print *,chain
  read(5,*) ii
end subroutine iidump
!
character*2 function chcode(i)
  integer i
  call bytoch(i,chcode,2)
  return
end function chcode


subroutine bdump(error)
  use gildas_def
  use classic_api
  use clic_buffers
  logical :: error
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer  :: i,j,ia,ic
  !
  print *,"---------"
  print *,"Total power of last calibration"
  do j= 1, mnbb
    print *,"s_lev(*,1,",j,",h,chop) = ",(s_lev(i,1,j,1,chop),i=1,r_nant)
    print *,"s_lev(*,1,",j,",v,chop) = ",(s_lev(i,1,j,2,chop),i=1,r_nant)
    print *,"s_lev(*,1,",j,",h,cold) = ",(s_lev(i,1,j,1,cold),i=1,r_nant)
    print *,"s_lev(*,1,",j,",v,cold) = ",(s_lev(i,1,j,2,cold),i=1,r_nant)
    print *,"s_lev(*,1,",j,",h,sky)  = ",(s_lev(i,1,j,1,sky),i=1,r_nant)
    print *,"s_lev(*,1,",j,",v,sky)  = ",(s_lev(i,1,j,2,sky),i=1,r_nant)
  enddo
  print *,"---------"
  print *,"Current total power "
  do j= 1, mnbb
    print *,"c_lev(*,1,",j,",h,chop) = ",(c_lev(i,1,j,1,chop),i=1,r_nant)
    print *,"c_lev(*,1,",j,",v,chop) = ",(c_lev(i,1,j,2,chop),i=1,r_nant)
    print *,"c_lev(*,1,",j,",h,cold) = ",(c_lev(i,1,j,1,cold),i=1,r_nant)
    print *,"c_lev(*,1,",j,",v,cold) = ",(c_lev(i,1,j,2,cold),i=1,r_nant)
    print *,"c_lev(*,1,",j,",h,sky)  = ",(c_lev(i,1,j,1,sky),i=1,r_nant)
    print *,"c_lev(*,1,",j,",v,sky)  = ",(c_lev(i,1,j,2,sky),i=1,r_nant)
  enddo
  print *,"---------"
  print *,"Integration times"
  do ia =1, r_nant
    do j=1, mnrec
      do ic = 1, mnbb
  print *,"integ(ia=",ia,",rec=",j,",ic=",ic,",h,*) = ",(integ(ia,j,ic,1,i),i=1,3)
  print *,"integ(ia=",ia,",rec=",j,",ic=",ic,",v,*) = ",(integ(ia,j,ic,2,i),i=1,3)
      enddo
    enddo
  enddo
  print *,"c_c",(c_c(i,1,1,1,1),i=1,mcch)
  print *,"c_c",(c_c(i,1,1,2,1),i=1,mcch)
  return
end subroutine bdump

subroutine rfdump(error)
  use gildas_def
  use classic_api
  use clic_bpc
  logical :: error
  !
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  integer  :: i,j
  !
  print *,"----------------------------------------------------"
  do i =1, mnbb
    print *,"Calibration unit ",i
    print *,"BP_NANT = ",bp_nant(i)
    print *,"BP_ANT = ",(bp_ant(j,i),j=1,r_nant)
    print *,"BP_PHYS = ",(bp_phys(j,i),j=1,r_nant)
    print *,"----------------------------------------------------"
  enddo
  return
end subroutine rfdump

subroutine sbadump(error)
  use gildas_def
  use classic_api
  logical :: error
  !
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_sba.inc'
  integer :: ibb,ib,isb
  do ibb=1,r_nbb
    do isb=1,2
      print *,"zrsba(",ibb,",",isb,",)=",(zrsba(ibb,isb,ib),ib=1,r_nbas)
    enddo
  enddo
end subroutine sbadump
