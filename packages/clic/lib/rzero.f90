subroutine rzero
  use classic_api  
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLASS	Internal routine
  !	Initialize the current observation (R)
  ! No subroutine
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: i, ipol, isb, ic, j 
  !
  r_xnum=0
  r_lam=0
  r_bet=0
  r_nant_vlbi = 0
  r_lband = 0
  r_lntch = 0
  do i=-mnsec,0
    r_presec(i) = .false.
  enddo
  do i=1, r_nant
    r_dmaflag(i) = 0
    do j=1, 2*mbands
      r_dmsaflag(j,i) = .false.
    enddo
    do ipol = 1, mnbb
      r_dmatfac(ipol,1,i) = 0
      r_dmatfac(ipol,2,i) = 0
    enddo
    do ipol = 1, mnbb
      r_dmdelay(ipol,i) = 0
    enddo
  enddo
  do i=1, r_nbas
    r_dmbflag(i) = 0
    do j=1, 2*mbands
      r_dmsbflag(j,i) = .false.
    enddo
    do isb=1, 2
      do ic = 1, mcch
        r_dmcamp(isb,i,ic) = 1.0
        r_dmcpha(isb,i,ic) = 0.0
      enddo
      do ic = 1, mrlband
        r_dmlamp(isb,i,ic) = 1.0
        r_dmlpha(isb,i,ic) = 0.0
        r_dmldph(isb,i,ic) = 0.0
      enddo
    enddo
  enddo
end subroutine rzero
