!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CLIC messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clic_message_private
  use gpack_def
  !---------------------------------------------------------------------
  ! Identifier used for message identification
  !---------------------------------------------------------------------
  integer :: clic_message_id = gpack_global_id  ! Default value for startup message
  !
end module clic_message_private
!
subroutine clic_message_set_id(id)
  use clic_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id         !
  ! Local
  character(len=message_length) :: mess
  !
  clic_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',clic_message_id
  call clic_message(seve%d,'clic_message_set_id',mess)
  !
end subroutine clic_message_set_id
!
subroutine clic_message(mkind,procname,message)
  use gkernel_interfaces
  use classic_api  
  use clic_message_private
  !---------------------------------------------------------------------
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  character(len=50) :: prefix
  !
  write (prefix,100) trim(procname),mod(r_scan,10000)
  !
  ! Temporary for CLIC: write only in message file (SB, 16-jun-2008).
  ! Messages to screen are handled by CLIC itself (routine MESSAGE).
  call gmessage_write_in_mesfile(clic_message_id,mkind,prefix,message)
  !
100  format(A,'[',I4.4,']')
  !
end subroutine clic_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
