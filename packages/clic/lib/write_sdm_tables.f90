subroutine write_sdm_tables (ldata, data, switch,   &
    iswitch, simAddressing, error)
  !---------------------------------------------------------------------
  !     holodata: write SD holography data
  !
  !     nsubant, subant: number of antennas and antennas to use (for
  !     holography so far)
  !
  !     simAddressing : write special test files for tracing bugs ...
  !---------------------------------------------------------------------
  USE GILDAS_DEF
  use sdm_enumerations
  !
  ! Global variables:
  INCLUDE 'clic_parameter.inc'
  INCLUDE 'clic_clic.inc'
  INCLUDE 'clic_display.inc'
  INCLUDE 'clic_par.inc'
  INCLUDE 'clic_dheader.inc'
  INCLUDE 'clic_constant.inc'
  INCLUDE 'clic_proc_par.inc'
  INCLUDE 'gbl_pi.inc'
  INCLUDE 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  !
  ! dummy variables :
  integer :: ldata, data(ldata), iswitch
  logical :: error, switch, simAddressing
  !
  ! local variables :
  integer scanNumber, pointingSize, totalPowerSize
  integer k, kc, kpc, klu, klc,  kl, ir, h_offset, c_offset,   &
    l_offset, isw
  character ch*12, uidChanAver*40, uidFullRes*40, uidTotPow*40,   &
    obsMode*256
  character :: dattim*20
  integer*8 :: oldtime, iti
  logical :: tpOnly, holodata

  !---------------------------------------------------------------------
  call message(1,1,'Write_sdm_tables','Starting ...')
  tpOnly = .false.
  if (switch) then
    tpOnly = .true.
  endif
  holodata = .false.
  ir = r_ndump+1
  k = 1+ h_offset(ir)
  call decode_header (data(k))
  ! patch to ensure time_interval (subscan) is consistent with that of each integration...
  call get_time_interval
  ! found another way, 2009-03-08
  !iti = time_interval(2)
  !k = 1+ h_offset(1)
  !call decode_header (data(k))
  !call get_time_interval
  !time_interval(2) = iti
  !
  !     Define time_Interval:
  !     - the interval center in MJD nano seconds...
  !     - the interval length in MJD nanoseconds...
  !$$$      dh_utc = nint(2*dh_utc)*0.5d0
  !$$$c      print *, (dh_obs+60549.0d0)*86400.d0, dh_utc, dh_integ
  !$$$      time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-0.5d0
  !$$$     &     *dh_integ)*1d9
  !$$$      time_interval(2) = dh_integ*1d9
  !c      print *, '== write_sdm_tables , time_interval ', time_interval
  if (time_start(1).lt.0) then
    time_start(1) = time_interval(1)
    time_start(2) = -1
  endif
  !     deal with scan/subscan  numbers.
  if (r_scan.ne.scan_Number) then
    if (scan_Number.ge.0) then
      call write_sdm_scan(error)
      if (error) return
    endif
    subScan_Number = 1
    num_subscan = 1
    scan_startTime = time_Interval(1)
    scan_endTime = time_Interval(1)+time_Interval(2)
  else
    num_subscan = num_subscan + 1
    subScan_Number = subScan_Number + 1
    scan_endTime = time_Interval(1)+time_Interval(2)
  endif
  scan_Number = r_scan
  call set_intent(scan_Intent, subScan_Intent, obsMode, data_type, holodata)
  !c      print *, obsMode
  !      subscan_startTime = time_Interval(1)
  !      subscan_endTime = time_Intervwrite_sdm_tables.f90al(1)+time_Interval(2)
  !
  !     tables that hold parameters unchanged during one Bure observation
  !     each call returns the pointers in the last item in the call list
  !    if (error) return
    dh_utc = nint(dh_utc/.001d0)*0.001d0
    time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-0.5d0   &
      *dh_integ)*1d9
    ! Patch for TAI-UTC valin in 2006,2007, 2008; replace by correct software!
    ! REMOVED 2010-05-24 We now have UTC in the ASDM. 
    ! time_interval(1) = time_interval(1) + 33.*1d9
    time_interval(2) = dh_integ*1d9

  !     --- Hardware setup information:
  !
  !     Station table ++ + 90
  call write_sdm_Station (error)
  if (error) return
  !
  !     Antenna Table ++ +  90
  call write_sdm_Antenna (error)
  if (error) return
  !
  !     FocusModel Table ++ Not implemented
  !!call write_sdm_FocusModel(error)
  !!if (error) return
  !
  !     PointingModel Table ++ (TBD)  +
  !$$$      call write_sdm_PointingModel(error)
  !$$$      if (error) return
  !
  !     SpectralWindow Tables (line and continuum) ++ + 90
  call write_sdm_SpectralWindow(holodata, error)
  if (error) return
  !
  !     Polarization Table ++ + 90
  !
  !     For holography, we write this in polarization table for the time
  !     being.
  call write_sdm_Polarization(error)
  if (error) return
  !
  !     DataDescription Table ++  + 90
  call write_sdm_DataDescription (holodata, error)
  if (error) return
  !
  !     AlmaCorrelatorMode table ++ + 90
  if (.not. holodata) then
    call write_sdm_CorrelatorMode(error)
    if (error) return
    call write_sdm_SquareLawDetector(holodata, error)
    if (error) return
  else
    call write_sdm_SquareLawDetector(holodata, error)
    if (error) return
  endif
  !
  !     Processor Table ++  + 90
  call write_sdm_Processor(holodata, error)
  if (error) return
  !
  !     Receiver Table ++  + 90
  call write_sdm_Receiver(holodata, error)
  if (error) return
  !
  !     Beam Table ++
  !      call write_sdm_Beam (error)
  !      if (error) return
  beam_Id = 0                  ! instead...
  !
  !     Feed Table ++  + 90
  call write_sdm_Feed(holodata, error)
  if (error) return
  !
  !     FreqOffset Table ++  + 90
  call write_sdm_FreqOffset(holodata, error)
  if (error) return
  !
  !     SwitchCycle Table ++  + 90
  call write_sdm_SwitchCycle(error)
  if (error) return
  !
  !     ConfigDescription Table ++  + 90
  call write_sdm_ConfigDescription(holodata, tpOnly, error)
  if (error) return
  !
  !     SysCal table NEW
  call write_sdm_syscal(error)
  if (error) return
  !
  !     --- Source information:
  !
  !     Source Table ++  + 90
  call write_sdm_Source(holodata, error)
  if (error) return
  !
  !     Removed in v1! Source Parameter Table (??) ++  +
  !call write_sdm_SourceParameter(error)
  !if (error) return
  !
  !     Doppler Table ++  + 90
  if (.not. holodata) then
    call write_sdm_Doppler (error)
    if (error) return
  endif
  !
  !     Ephemeris Table ++  +
  !c      call write_sdm_Ephemeris(error)
  !c      if (error) return
  !
  !     Field Table ++   + 90
  call write_sdm_Field(error)
  if (error) return
  !
  !     SBSummary  Table
  call write_sdm_SBSummary(error)
  if (error) return
  !
  !     ExecBlock Table ++  + 90
  call write_sdm_ExecBlock(error)
  if (error) return
  !     Seeing Table (deprecated)
  !
  !     SquareLawDetector Table (??)
  !
  !     Weather Table ++  + 90
  call write_sdm_Weather(error)
  if (error) return
  !
  !     Calibration Tables:
  !     write_sdm_SysCal (deprecated)
  !     write_sdm_WVMCal (deprecated)
  !     write_sdm_CalAmpli
  !
  ! Atmosphere Table  ++ + 90
  if (.not. holodata) then
    call write_sdm_calAtmosphere(error)
    if (error) return
    ! Cal Device
    call write_sdm_CalDevice(error)
    if (error) return
  endif
  !     write_sdm_CalCurve
  !     write_sdm_CalData
  !     write_sdm_CalDevice
  !     write_sdm_CalPhase
  !     write_sdm_CalPointingModel
  !     write_sdm_CalPointing
  !     write_sdm_CalPosition
  !     write_sdm_CalReduction
  !     write_sdm_CalSeeing
  !
  !
  ! ------------------------------0--------------------------------------------
  !     tables that change during one Bure observation : loop on data
  !     header parameters.
  !
  !cc   write_sdm_AlmaRadiometer     TBD table
  !cc   write_sdm_FlagCmd            Not needed here?
  !
  !c    Tables for the average record (= ASDM INtegration), uncorrected
  !      klu = 1 + l_offset(r_ndump+1)
  !
  !     for corrected spectral data:
  !      klc = 1 + l_offset(r_ndump+2)
  !
  !     for uncorrected channel average data:
  kc = 1 + c_offset(r_ndump+1)
  !
  !     Data Binaries
  if (.not. holodata) then
! until binaries work
     if (.not.tpOnly) then
       !call sdm_getUid(uidChanAver)
       call write_sdm_CorrelatorData(   &
            SpectralResolutionType_CHANNEL_AVERAGE ,ldata, data,   &
            uidChanAver, simAddressing, error)

       !call sdm_getUid(uidFullRes)
       call write_sdm_CorrelatorData(   &
            SpectralResolutionType_FULL_RESOLUTION ,ldata, data,   &
            uidFullRes, simAddressing, error)
     endif
     !call sdm_getUid(uidTotPow)
     call write_sdm_TotalPowerData(ldata, data, uidTotPow,   &
       switch, iswitch, error)
  endif
  !
  !     State Table ++ + 90
  call write_sdm_State(error)
  if (error) return
  !
  !     TotalPower Table - Now used only for Holography...
  !
  if (holodata) then
    call write_sdm_TotalPower(holodata, r_ldatc, data(kc)   &
      , error)
    if (error) return
  endif
  !
  !     Main Table ++ + 90
  !     will write both integration and subintegrations ...
  ir = r_ndump+1
  k = 1+ h_offset(ir)
  call decode_header (data(k))
  ! patch to ensure time_interval (subscan) is consistent with that of each integration...
  call get_time_interval
  if (.not. holodata) then
    if (.not.tpOnly) then
      !     subintegrations
      call write_sdm_Main(uidChanAver, .false., .false., error)
      if (error) return
      !     Integrations
      call write_sdm_Main(uidFullRes, .true., .false., error)
      if (error) return
    endif
    !     Total Power
    call write_sdm_Main(uidTotPow, .true., .true., error)
    if (error) return
  endif
  !     SBSummary Table (not needed?)
  !     Scan and subscan Tables ++  + 90
  call write_sdm_subScan(holodata, error)
  if (error) return
  !
  !     History Table (irrelevant?) ++  + 90
  !
  call write_sdm_History(error)
  if (error) return
  !
  !     Loop on records (= ASDM sub-integrations)
  oldtime = 0
  do ir=1, r_ndump
    write (ch,'(i4.0,1h/,i4.0)') ir, r_ndump
    call message(1,1,'Write_sdm_tables','Record '//ch)
    k = 1+ h_offset(ir)
    call decode_header (data(k))
    if (error) return
    ! get the time interval from CLIC data_header.
    call get_time_interval
    !
    ! SHould nto happen, but it does...
    if (time_interval(1).eq.oldtime) goto 900
    oldtime = time_interval(1)
    !     Tables for the individual records (= ASDM SubIntegration),
    !     uncorrected channel average data:
    kc = 1 + c_offset(ir)
    !
    !     Data Binaries--------------
    !
    !$$$         if (.not. holodata) then
    !$$$            call write_sdm_data(.true., data(kc), data(klu), data(klc),
    !$$$     &           uidSubint, error)
    !$$$         endif
    !     tables with timeInterval as a key cannot be used for the 'average
    !     record' of CLIC. So they must be written here.
    !
    !     Pointing Table ++ + 90
    !if (r_nswitch.gt.0) then
    !  isw = dh_test0(1)
    !  dh_offlam = dh_offlam - r_pswitch(1,isw)
    !  dh_offlam = dh_offlam - r_pswitch(2,isw)
    !endif
    !call write_sdm_Pointing(ldata, data, error)
    !if (error) return
    !
    !     Focus Table ++ + 90
    call write_sdm_Focus(error)
    if (error) return
    !
    !     Gain Tracking Table ---
    !c         call write_sdm_GainTracking(error)
    !
    !g
    !     State Table ++ (because subintegration) + 90
    call write_sdm_State(error)
    if (error) return
    !
    !     TotalPower Table  ++ +
    !         if (.not. holodata) then
    !         call write_sdm_TotalPower(holodata, r_ldatc, data(kc)
    !     &        , error)
    !         if (error) return
    !         print *, '900 continue'
    !         endif
    !
    !     Main Table ++ 90
    !$$$         if (.not. holodata) then
    !$$$            call write_sdm_Main(uid, holodata, error)
    !$$$            if (error) return
    !$$$         endif
900 continue
  enddo
  !
  ! Pointing Table now by subscan
  !
  call write_sdm_Pointing(ldata, data, error)
  if (error) return
  !      call sic_date(dattim)
  call getPointingTableSize(pointingSize, error)
  !      call getTotalPowerTableSize(totalPowerSize, error)
  !      print 1000, ' pointingSize ', pointingSize,
  !     &     ' totalPowerSize ', totalPowerSize
1000 format (a, i10, a , i10)
  call message(1,1,'Write_sdm_tables','Ended.')
  return
  !
  !     error return
99 error = .true.
end subroutine write_sdm_tables
!------------------------------------------------------------------
subroutine init_sdm
  USE GILDAS_DEF
  ! Global variables:
  INCLUDE 'clic_parameter.inc'
  include 'sdm_identifiers.inc'
  integer ia, i, j, k
  !
  scan_number = -1
  Weather_Station_Id = -1
  Polarization_Id = -1
  TotPowPolarization_Id = -1
  TotPowSpectralWindow_Id = -1
  CorrelatorMode_Id = -1
  Processor_Id = -1
  Beam_Id = -1
  SwitchCycle_Id = -1
  ConfigDescription_Id = -1
  contConfigDescription_Id = -1
  totPowConfigDescription_Id = -1
  Field_Id  = -1
  ExecBlock_Id = -1
  Ephemeris_Id = -1
  Observation_Id = -1
  sourceParameter_Id  = -1
  History_Id = -1
  weather_Id  = -1
  state_Id = -1
  feed_Id  = -1
  Doppler_Id  = -1
  Source_Id = -1
  totPowDataDescription_Id = -1
  do ia = 1, mnant
    Station_Id(ia) = -1
    Antenna_Id(ia) = -1
    FocusModel_Id(ia) = -1
    PointingModel_Id(ia) = -1
  enddo
  do k=1, 2
    do j=1, mrlband
      do i=1, msideband
        spectralWindow_Id(i,j,k) = -1
        dataDescription_Id(i,j,k) = -1
      enddo
    enddo
  enddo
  freq_lo1 = 0
  freq_lo2 = 0
end subroutine init_sdm
!------------------------------------------------------------------------
integer*8 function roundTimeR4(t,it)
  integer*8 jt
  integer it
  real*4 t
  real*8 rt
  rt = 1d9/it
  roundTimeR4 = rt*t+0.5
  roundTimeR4 = roundTimeR4 * it
end function roundTimeR4
!------------------------------------------------------------------------
integer*8 function roundTimeR8(tt,it)
  integer*8 jt
  integer it
  real*8 tt
  real*8 rt
  rt = 1d9/it
  roundTimeR8 = rt*tt+0.5
  roundTimeR8 = roundTimeR8 * it
end function roundTimeR8
!------------------------------------------------------------------------
subroutine get_time_interval
  !
  !     Define timeInterval:
  !     - the interval center in MJD nanoseconds...
  !     - the interval length in MJD nanoseconds...
  !
  !     force the times to be multiples of 1ms as dh_utc has not enough
  !     precision
  !
  INCLUDE 'clic_parameter.inc'
  INCLUDE 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  integer*8 :: idhutc, idhobs, idhinteg, roundTimeR4, roundTimeR8 !, TAI_UTC 
  real*8 :: mjd ! , mjd_to_TAIUTC
  !
  ! For the time being we need to go to TAI as the casa fillere expects this; 
  ! asdm will be in UTC in the near future...
  ! the future has now happenned.
  !
  ! mjd = dh_obs+60549.0d0+dh_utc/86400d0
  ! TAI_UTC = mjd_to_TAIUTC(mjd)*1000000000 ! in ns.
  !
  time_interval(2) = roundTimeR4(dh_integ, 1000000)
  idhutc = roundTimeR8(dh_utc, 1000000)
  idhobs = dh_obs+60549
  !!  time_interval(1) = idhobs*86400*1000000000 + idhutc + TAI_UTC
  time_interval(1) = idhobs*86400*1000000000 + idhutc 
  time_interval(1) = time_interval(1)-time_interval(2)/2
end subroutine get_time_interval
!
!------------------------------------------------------------------------
!
function mjd_to_TAIUTC(mjd)
! Function qui calcule TAI - UTC en fonction du mjd
! Entrée : mjd (double)
! Sortie : TAI_moins_UTC (double)
! le tableau TABLEAU_SAUTS_SECONDE doit etre mis a jour chaque annee environ.

! TODO move into Astro as Astro could use this too.
!
  double precision  mjd, mjd_to_TAIUTC
  integer i
  double precision TAI_moins_UTC
  !
  !
  integer, parameter :: NB_SAUTS_SECONDE=24
  double precision   :: TABLEAU_SAUTS_SECONDE(NB_SAUTS_SECONDE)
  !
  ! update this table when needed, and increment the size NB_SAUTS_SECONDE accordingly ...
  !
  data TABLEAU_SAUTS_SECONDE /&
       41499,  & !!  1 juillet 1972
       41683,  & !!  1 janvier 1973
       42048,  & !!  1 janvier 1974
       42413,  & !!  1 janvier 1975
       42778,  & !!  1 janvier 1976
       43144,  & !!  1 janvier 1977
       43509,  & !!  1 janvier 1978
       43874,  & !!  1 janvier 1979
       44239,  & !!  1 janvier 1980
       44786,  & !!  1 juillet 1981
       45151,  & !!  1 juillet 1982
       45516,  & !!  1 juillet 1983
       46247,  & !!  1 juillet 1985
       47161,  & !!  1 janvier 1988
       47892,  & !!  1 Janvier 1990
       48257,  & !!  1 janvier 1991
       48804,  & !!  1 juillet 1992
       49169,  & !!  1 juillet 1993
       49534,  & !!  1 juillet 1994
       50083,  & !!  1 janvier 1996
       50630,  & !!  1 juillet 1997
       51179,  & !!  1 janvier 1999
       53736,  & !!  1 janvier 2006
       54832   / !!  1 janvier 2009
 
  if(mjd >= 41317.0)  then
    TAI_moins_UTC=10.0
    do i=1,NB_SAUTS_SECONDE	
      if(mjd>= TABLEAU_SAUTS_SECONDE(i)) TAI_moins_UTC = TAI_moins_UTC + 1.0
    enddo
  else
    if(mjd < 41317)  TAI_moins_UTC = 4.213170 + (mjd - 39126)*0.002592 ! jusqu'au 1 jan  1972
    if(mjd < 39887)  TAI_moins_UTC = 4.313170 + (mjd - 39126)*0.002592 ! jusqu'au 1 fev  1968
    if(mjd < 39126)  TAI_moins_UTC = 3.840130 + (mjd - 38761)*0.001296 ! jusqu'au 1 jan  1966
    if(mjd < 39004)  TAI_moins_UTC = 3.740130 + (mjd - 38761)*0.001296 ! jusqu'au 1 sept 1965
    if(mjd < 38942)  TAI_moins_UTC = 3.640130 + (mjd - 38761)*0.001296 ! jusqu'au 1 juil 1965
    if(mjd < 38820)  TAI_moins_UTC = 3.540130 + (mjd - 38761)*0.001296 ! jusqu'au 1 mars 1965
    if(mjd < 38761)  TAI_moins_UTC = 3.440130 + (mjd - 38761)*0.001296 ! jusqu'au 1 jan  1965
    if(mjd < 38639)  TAI_moins_UTC = 3.340130 + (mjd - 38761)*0.001296 ! jusqu'au 1 sept 1964
    if(mjd < 38486)  TAI_moins_UTC = 3.240130 + (mjd - 38761)*0.001296 ! jusqu'au 1 avr  1964
    if(mjd < 38395)  TAI_moins_UTC = 1.945858 + (mjd - 37665)*0.0011232 ! jusqu'au 1 jan  1964
    if(mjd < 38334)  TAI_moins_UTC = 1.845858 + (mjd - 37665)*0.0011232 ! jusqu'au 1 nov  1963
    if(mjd < 37665)  TAI_moins_UTC = 1.372818 + (mjd - 37300)*0.001296 ! jusqu'au 1 jan  1962
    if(mjd < 37512)  TAI_moins_UTC = 1.422818 + (mjd - 37300)*0.001296 ! jusqu'au 1 aout 1961
    if(mjd < 37300)  TAI_moins_UTC = 0.d0
  endif
  mjd_to_TAIUTC=TAI_moins_UTC			

  return

end function mjd_to_taiutc
