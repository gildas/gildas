subroutine write_sdm_main(uid, integration, totalpower, error)
  use gkernel_interfaces
  !---------------------------------------------------------------------I
  !
  !     Write the Main SDM table; these lines for correlator data.
  !     at this point one line per integration or subintegration
  !
  !     holodata not used...
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_main
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical :: error, integration, totalpower
  character*(*) uid
  ! Local
  type (MainRow) :: mRow
  type (MainKey) :: mKey
  !      type (MainOpt) :: mOpt
  !     there are up to 4 polarization products  (e.g. XX, YY, XY, YX).
  integer :: correlationMode, ib, ia, luid
  character, parameter ::  sdmTable*4='Main'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     binary writing process -> bitsize, scaleFactor, dataOid may be
  !     allocated in the binary data routine...
  !
  !     In the current model (2005-12-01) the dimensions of bitsize and
  !     scalefactor are (CorrelationMode, numBaseBand)
  !     for bure there is allways either auto or corr (CorrelationMode = 1)
  !
  !     note that scalefactor and bitsize will not be used for
  !     autocorrelations
  !     note also that bitzise is one number for each integration (not
  !     dependent on baseband).
  !
  if (r_lmode.eq.1) then       ! correlation
    correlationMode = 1
  elseif (r_lmode.eq.2) then   ! auto correlation only
    correlationMode = 1
  elseif (r_lmode.eq.3) then   ! auto correlation and correlation (some day),
    !  ... or ALMA data
    correlationMode = 2
  endif
  mRow%numAntenna = r_nant
  !!call  allocMainRow(mRow, r_nant, error)
  call  allocMainRow(mRow, error)
  if (error) return
  !      call  allocMainOpt(mOpt, mRow%numIntegration, r_nant, r_lband,
  !     &     num_corr, error)
  !      if (error) return
  !!mRow%flagRow = .false.
  mKey%time = time_Interval(1) + time_Interval(2)/2.
  mRow%interval = time_Interval(2)
  !
  !     I expect all Bure data to have the same state for all connected antennas.
  do ia = 1, r_nant
    mRow%stateId(ia) = state_Id
  enddo
  luid = lenc(uid)
  mRow%dataOId= '<EntityRef '//   &
    'entityId="'//uid(1:luid)//   &
    '" partId="X00000002" '//   &
    'entityTypeName="Main" '//   &
    'documentVersion="1"/>'
  !
  ! *TBD* Need to fill in the flags...
  !
  ! May be better pass an argument for this test...
  if (totalpower) then
    mKey%configDescriptionId = TotPowConfigDescription_Id
    mRow%timeSampling = TimeSampling_SUBINTEGRATION
    if (r_nswitch.gt.0) then
      mRow%numIntegration = r_ndump/r_nswitch
    else
      mRow%numIntegration = r_ndump
    endif
  elseif (integration) then
    mKey%configDescriptionId = configDescription_Id
    mRow%timeSampling = TimeSampling_INTEGRATION
    mRow%numIntegration = 1
  else
    mKey%configDescriptionId = COntconfigDescription_Id
    mRow%timeSampling = TimeSampling_SUBINTEGRATION
    mRow%numIntegration = r_ndump
  endif
  mKey%fieldId = field_Id
  mRow%execBlockId = execBlock_Id
  mRow%scanNumber = Scan_Number
  mRow%subScanNumber = subScan_Number
  call addMainRow(mKey, mRow, error)
  if (error) return
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_main
!---------------------------------------------------------------------
! Deprecated
subroutine write_sdm_mainTP(uid, error)
  !---------------------------------------------------------------------I
  !
  !     Write the Main SDM table; these lines for total power  data.
  !     at this point one line per Subscan...
  !     holodata not used...
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_main
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical :: error
  character*(*) uid
  ! Local
  type (MainRow) :: mRow
  type (MainKey) :: mKey
  !      type (MainOpt) :: mOpt
  !     there are up to 4 polarization products  (e.g. XX, YY, XY, YX).
  integer :: correlationMode, ib, ia
  character, parameter ::  sdmTable*6='MainTP'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !
  mRow%numAntenna = r_nant
  call  allocMainRow(mRow, error)
  if (r_nswitch.gt.1) then
    mRow%numIntegration = r_ndump / r_nswitch
  endif
  !      call  allocMainOpt(mOpt, mRow%numIntegration, r_nant, r_lband,
  !     &     r_npol_rec, error)
  !      if (error) return
  !!mRow%flagRow = .false.
  mKey%time = time_Interval(1) + time_Interval(2)/2.
  mRow%interval = time_Interval(2)
  !
  !     I expect all Bure data to have the same state for all connected antennas.
  do ia = 1, r_nant
    mRow%stateId(ia) = state_Id
  enddo
  mRow%dataOId= '<EntityRef '//   &
    'entityId="'//uid(1:33)//   &
    '" partId="X00000002" '//   &
    'entityTypeName="Main" '//   &
    'documentVersion="1"/>'
  !
  ! *TBD* Need to fill in the flags...
  !
  mKey%configDescriptionId = TotPowConfigDescription_Id
  mKey%fieldId = field_Id
  mRow%execBlockId = execBlock_Id
  mRow%scanNumber = Scan_Number
  mRow%subScanNumber = subScan_Number
  mRow%timeSampling = TimeSampling_INTEGRATION
  call addMainRow(mKey, mRow, error)
  if (error) return
  !
  ! uvw now optional, do not write it here.
  ! do not write exposure and time centroid, nor flags....
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_mainTP
!---------------------------------------------------------------------
subroutine get_sdm_main(uid, error)
  !---------------------------------------------------------------------
  !
  !     Read the Main SDM table.
  !
  !     Key: (configDescriptionId, fieldId, time)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_main
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical error
  character*(*) uid
  ! Local
  type (MainRow) :: mRow
  type (MainKey) :: mKey
  !      type (MainOpt) :: mOpt
  integer :: ia, ib, l, lenz, k, k2, mcorr, minteg
  logical :: present
  character, parameter ::  sdmTable*4='Main'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  mcorr = 4
  minteg = 1
  mRow%numAntenna = mnant
  call  allocMainRow(mRow, error)
  if (error) return
  ! optional stuff
  !      call  allocMainOpt(mOpt, minteg, mnant,
  !     &     mrlband, mCorr, error)
  !      if (error) return
  mKey%configDescriptionId = configDescription_Id
  mKey%fieldId = field_Id
  mKey%time = time_interval(1) + time_interval(2)/2
  !
  call getMainRow(mKey, mRow, error)
  if (error) then
    print *, 'mKey ',mKey
    return
  endif
  execBlock_Id = mRow%execBlockId
  Scan_Number = mRow%scanNumber
  subScan_Number  = mRow%subScanNumber
  state_Id = mRow%stateId(1)
  do ia = 1, r_nant
     if(state_Id.ne.mRow%stateId(ia)) then
      error = .true.
      call sdmmessageI(8,4,sdmTable ,'Unexpected State_Id '   &
        ,mRow%stateId(ia))
      return
    endif
  enddo
  !
  l = lenz(mRow%DataOid)
  k = index(mRow%DataOid(1:l),'"uid:')
  k2 = index(mRow%DataOid(k+1:),'"')
  uid =  mRow%DataOid(k+1:k+k2-1)
  !cc   Other reading to do I guess...
  !
  ! get the uvw
  !      call getMainUvw(mKey, mOpt, present, error)
  !      if (present .and. r_nbas.gt.0) then
  !         do ib = 1, r_nbas
  !            if (r_iant(ib).eq.1) then
  !               dh_uvm(1,ib) = mOpt%uvw(1,r_jant(ib))
  !               dh_uvm(2,ib) = mOpt%uvw(2,r_jant(ib))
  !            endif
  !         enddo
  !      else
  !cc **** NEED TO CALCULATE THIS HERE ***
  dh_uvm = 0
  !      endif
  !
  !     Get the subintegration number: Not there any mode.
  ! Must be calculated somewhat using the time and the subscan time.
  !     Or just counted?
  !c      dh_dump = dh_dump + 1
  !      call getMainSubintegNum(mKey, mOpt, present, error)
  !      if (error) return
  !      if (present) then
  !         dh_dump = mOpt%subintegNum
  !      endif
  !
  !$$$      call getMainSubintegrationNumber(mKey, mOpt, present, error)
  !$$$      if (error) return
  !$$$      if (present) then
  !$$$         dh_dump = mOpt%subintegrationNumber
  !$$$      endif
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
!
!$$$ 99   error = .true.
end subroutine get_sdm_main
!---------------------------------------------------------------------
!---------------------------------------------------------------------
subroutine get_sdm_mainTP(uid, error)
  !---------------------------------------------------------------------
  !
  !     Read the Main SDM table for TP OID
  !
  !     Key: (configDescriptionId, fieldId, time)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_main
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical error
  character*(*) uid
  ! Local
  type (MainRow) :: mRow
  type (MainKey) :: mKey
  !      type (MainOpt) :: mOpt
  integer :: ia, ib, l, lenz, k, k2, mcorr, minteg
  logical :: present
  character, parameter ::  sdmTable*8='Main(TP)'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  mcorr = 4
  minteg = 1
  call  allocMainRow(mRow, mnant, error)
  if (error) return
  !      call  allocMainOpt(mOpt, minteg, mnant,
  !     &     mrlband, mCorr, error)
  !      if (error) return
  mKey%configDescriptionId = configDescription_Id
  mKey%fieldId = field_Id
  mKey%time = time_interval(1) + time_interval(2)/2
  call getMainRow(mKey, mRow, error)
  if (error) return
  !
  execBlock_Id = mRow%execBlockId
  Scan_Number = mRow%scanNumber
  subScan_Number  = mRow%subScanNumber
  !$$$      scale_Factor = mRow%scaleFactor
  !$$$      bit_Size = mRow%bitSize
  state_Id = mRow%stateId(1)
  do ia = 1, r_nant
    if(state_Id.ne.mRow%stateId(ia)) then
      error = .true.
      call sdmmessageI(8,4,sdmTable ,'Unexpected State_Id '   &
        ,mRow%stateId(ia))
      return
    endif
  enddo
  !
  l = lenz(mRow%DataOid)
  k = index(mRow%DataOid(1:l),'"uid:')
  k2 = index(mRow%DataOid(k+1:),'"')
  uid =  mRow%DataOid(k+1:k+k2-1)
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
!
!$$$ 99   error = .true.
end subroutine get_sdm_mainTP
!---------------------------------------------------------------------
!!! subroutine allocMainRow(row, numAntenna, error)
!!!   !---------------------------------------------------------------------
!!!   !    integer, allocatable :: stateId(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Main
!!!   type(MainRow) :: row
!!!   integer :: numAntenna, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'Main'
!!!   !
!!!   ! stateId (numAntenna)
!!!   if (allocated(row%stateId)) then
!!!     deallocate(row%stateId, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%stateId(numAntenna), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!---------------------------------------------------------------------
!$$$      subroutine allocMainOpt(opt, numIntegration, numAntenna,
!$$$     &     numBaseband, numCorr,  error)
!$$$C---------------------------------------------------------------------
!$$$c    real*8, allocatable :: uvw(:,:,:)
!$$$c    integer*8, allocatable :: exposure(:,:,:)
!$$$c    integer*8, allocatable :: timeCentroid(:,:,:)
!$$$c    integer, allocatable :: flagAnt(:)
!$$$c    integer, allocatable :: flagPol(:,:,:)
!$$$c    integer, allocatable :: flagBaseband(:,:,:,:)
!$$$C---------------------------------------------------------------------
!$$$      use sdm_Main
!$$$      type(MainOpt) :: opt
!$$$      integer :: numIntegration, numAntenna,
!$$$     &     numBaseband, numCorr, ier
!$$$      logical :: error
!$$$      character, parameter :: sdmTable*8 = 'Main'
!$$$c
!$$$c uvw (3, numAntenna)
!$$$      if (allocated(opt%uvw)) then
!$$$         deallocate(opt%uvw, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%uvw(3, numAntenna), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c exposure (numBaseband, numAntenna)
!$$$      if (allocated(opt%exposure)) then
!$$$         deallocate(opt%exposure, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%exposure(numBaseband, numAntenna),
!$$$     &     stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c timeCentroid (numBaseband, numAntenna)
!$$$      if (allocated(opt%timeCentroid)) then
!$$$         deallocate(opt%timeCentroid, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%timeCentroid(numBaseband, numAntenna), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c flagAnt (numAntenna)
!$$$      if (allocated(opt%flagAnt)) then
!$$$         deallocate(opt%flagAnt, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%flagAnt(numAntenna), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c flagPol (numCorr, numAntenna)
!$$$      if (allocated(opt%flagPol)) then
!$$$         deallocate(opt%flagPol, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%flagPol(numCorr, numAntenna), stat
!$$$     &     =ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c flagBaseband (numBaseband, numCorr, numAntenna)
!$$$      if (allocated(opt%flagBaseband)) then
!$$$         deallocate(opt%flagBaseband, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(opt%flagBaseband(numBaseband, numCorr, numAntenna), stat
!$$$     &     =ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$      return
!$$$c
!$$$ 98   call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!$$$      error = .true.
!$$$      return
!$$$c
!$$$ 99   call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!$$$      error = .true.
!$$$      return
!$$$      end
!$$$
