subroutine write_sdm_Processor(holodata, error)
  !---------------------------------------------------------------------
  ! Write the AlmaCorrelatorMode SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Processor
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  type (ProcessorRow) :: pRow
  type (ProcessorKey) :: pKey
  character sdmTable*12
  parameter (sdmTable='Processor')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much TBD at this stage.
  !
  ! the Transmitter holography
  !
  if (holodata) then
    pRow%processorType = ProcessorType_Radiometer
    !! pRow%processorSubType = ProcessorSubType_HOLOGRAPHY
    pRow%processorSubType = ProcessorSubType_SQUARE_LAW_DETECTOR
    pRow%modeId = squareLawDetector_Id
    call addProcessorRow(pKey, pRow, error)
    if (error) return
    TotPowprocessor_Id = pKey%processorId
  !
  ! normal case
  !
  else
    pRow%processorType = ProcessorType_CORRELATOR
    pRow%processorSubType = ProcessorSubType_ALMA_CORRELATOR_MODE
    pRow%modeId = correlatorMode_Id
    call addProcessorRow(pKey, pRow, error)
    if (error) return
    processor_Id = pKey%processorId
    !
    pRow%processorType = ProcessorType_RADIOMETER
    pRow%ProcessorSubType = ProcessorSubType_SQUARE_LAW_DETECTOR
    pRow%modeId = squareLawDetector_id
    call addProcessorRow(pKey, pRow, error)
    if (error) return
    TotPowprocessor_Id = pKey%processorId
    !
    pRow%processorType = ProcessorType_RADIOMETER
    ! ProcessorSubType_ALMA_RADIOMETER is missing
    pRow%ProcessorSubType = ProcessorSubType_ALMA_RADIOMETER
    !pRow%ProcessorSubType = ProcessorSubType_SQUARE_LAW_DETECTOR
    pRow%modeId = squareLawDetector_id
    call addProcessorRow(pKey, pRow, error)
    if (error) return
    RadiometerProcessor_Id = pKey%processorId
  endif
  !
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_Processor
!---------------------------------------------------------------------
subroutine get_sdm_Processor(AlmaCorr, error)
  !---------------------------------------------------------------------
  ! Get  the AlmaCorrelatorMode SDM table row for an integration
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_processor
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error, AlmaCorr
  ! Local
  type (ProcessorRow) :: pRow
  type (ProcessorKey) :: pKey
  character sdmTable*12
  parameter (sdmTable='Processor')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much TBD at this stage.
  pKey%processorId = processor_Id
  !      print *, 'processor_Id ',processor_Id
  call getProcessorRow(pKey, pRow, error)
  if (error) return
!!!   correlatorMode_Id = pRow%almaCorrelatorModeId
  correlatorMode_Id = pRow%modeId
  !      l = lenc(pRow%type)
  ! This means we will get Correlator binary data...
  !
  !     make it simple for the time being...
  AlmaCorr = pRow%ProcessorType .eq. ProcessorType_CORRELATOR
  !$$$      AlmaCorr = pRow%type .eq. ProcessorType_CORRELATOR .and.
  !     &     pRow%subtype .eq. 'ALMA_CORRELATOR_MODE'
  !cc Should we test this one??
  !cc OPTIONAL in SS2      if (pRow%flagRow) then
  !cc OPTIONAL in SS2         call sdmMessage(4,2,sdmTable ,'ProcessorRow is flagged')
  !cc OPTIONAL in SS2      endif
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_Processor
