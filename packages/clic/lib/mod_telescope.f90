subroutine ini_telescope(line,iopt,error)
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! 	MODIFY TELESCOPE
  ! compute the telescope name according to new denomination
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  integer :: iopt                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, ia, lsupp, lmiss, ic, lc, iant4, iant5, imin
  integer :: lmin
  character(len=4) :: cstat(mnant)
  character(len=1) :: carm(4)
  character(len=80) :: cmiss, csupp
  character(len=12) :: teles
  !      CHARACTER*11 CANT5, CANT4
  ! 4ant, 5ant operational :
  parameter (iant4 = -11441, iant5 = -10222)
  ! Data
  data carm/'N','W','E','E'/
  !      data cant4,cant5/'01-MAY-1993','01-SEP-1996'/
  !------------------------------------------------------------------------
  ! Code:
  !      CALL CDATE(Cant4,Iant4,ERROR)
  !      CALL CDATE(Cant5,Iant5,ERROR)
  call check_equal_file(error)
  return
  !
entry mod_telescope(error)
  lmin = 100
  if (r_dobs.lt.iant4) then
    imin = 17
  elseif (r_dobs.lt.iant5) then
    imin = 7
  else
    imin = 1
  endif
  do ia=1, r_nant
    write (cstat(ia),'(a1,i3.3)') carm(r_istat(ia)/100),   &
      mod(r_istat(ia),100)+(r_istat(ia)/400)*100
  enddo
  if (r_nant.eq.1) then
    r_teles = cstat(1)
  elseif (r_nant.eq.2) then
    r_teles = cstat(1)//cstat(2)
  else
    if (r_nant.lt.10) then
      write (r_teles,'(i1,a)') r_nant,'ant-Special'
    else
      write (r_teles,'(i2,a)') r_nant,'an-Special'
    endif
    lmin = 16
    do i=imin, mconf
      teles = config_name(i)
      cmiss = '-'//config(i)//' '
      lmiss = lenc(cmiss)
      csupp = '+'
      lsupp = 1
      do ia=1, r_nant
        ic = index(cmiss,cstat(ia))
        if (ic.ne.0) then
          cmiss = cmiss(1:ic-1)//cmiss(ic+4:)
          lmiss = lmiss-4
        else
          csupp = csupp(1:lsupp)//cstat(ia)
          lsupp = lsupp+4
        endif
      enddo
      if (lmiss.le.9) then
        lc = lenc(teles)
        if (lc+lmiss+lsupp.lt.lmin) then
          if (lmiss.gt.1) then
            teles = teles(1:lc)//cmiss(1:lmiss)
            lc = lc + lmiss
          endif
          if (lsupp.gt.1 .and. lc+lsupp.le.16) then
            teles = teles(1:lc)//csupp(1:lsupp)
            lc = lc + lsupp
          endif
          lmin = min(16,lenc(teles))
          r_teles = teles(1:lmin)
        endif
      endif
    enddo
  endif
  return
end subroutine ini_telescope
