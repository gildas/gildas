subroutine write_sdm_squareLawDetector(holodata, error)
  !---------------------------------------------------------------------
  ! Write the AlmaCorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_squareLawDetector
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error, holodata
  ! Local
  type (SquareLawDetectorKey) :: sldKey
  type (SquareLawDetectorRow) :: sldRow
  character sdmTable*20
  parameter (sdmTable='SquareLawDetector')
  integer ier, i
  !---------------------------------------------------------------------
  ier = 0
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much *TBD* at this stage. Think of better filling this in...
  sldRow%numBand = r_lband
  if (holodata) then
    sldRow%bandType = DetectorBandType_HOLOGRAPHY_RECEIVER
  else
    sldRow%bandType = DetectorBandType_SUBBAND
  endif
  if (error) return
  call addSquareLawDetectorRow(sldKey, sldRow, error)
  if (error) return
  !
  squareLawDetector_Id = sldKey%squareLawDetectorId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_squareLawDetector
!---------------------------------------------------------------------
subroutine get_sdm_squareLawDetector(error)
  !---------------------------------------------------------------------
  ! Get the AlmaCorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_squareLawDetector
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (SquareLawDetectorKey) :: sldKey
  type (SquareLawDetectorRow) :: sldRow
  !$$$      integer ier
  !$$$      integer, parameter :: maxBaseband = 8
  character  :: sdmTable*20
  parameter (sdmTable='SquareLawDetector')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much *TBD* at this stage. Think of better filling this in...
  !$$$      print *, 'CorrelatorMode_Id ', CorrelatorMode_Id
  !$$$      call  allocSquareLawDetectorRow(sldRow, maxBaseband, error
  !$$$     &     )
  !$$$      if (error) return
  sldKey%squareLawDetectorId  = correlatorMode_Id
  !$$$      print *, 'sldKey%modeId ',sldKey%modeId
  call getSquareLawDetectorRow(sldKey, sldRow, error)
  if (error) return
  if (sldRow%numband.gt.mrlband) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Wrong numBaseband',sldRow%numband)
    goto 99
  endif
  r_lband = sldRow%numband
  !
  !
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_squareLawDetector
!$$$C---------------------------------------------------------------------
!$$$      subroutine allocAlmaCorrelatorModeRow(row, numBaseband, error)
!$$$C---------------------------------------------------------------------
!$$$c$$$    integer, allocatable :: basebandIndex(:)
!$$$c$$$    integer, allocatable :: basebandConfig(:)
!$$$c$$$    integer, allocatable :: axesOrderArray(:)
!$$$C---------------------------------------------------------------------
!$$$      use sdm_AlmaCorrelatorMode
!$$$      type(AlmaCorrelatorModeRow) :: row
!$$$      integer :: numBaseband, ier
!$$$      logical :: error
!$$$      character, parameter :: sdmTable*8 = 'calReduction'
!$$$c
!$$$c basebandIndex
!$$$      if (allocated(row%basebandIndex)) then
!$$$         deallocate(row%basebandIndex, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(row%basebandIndex(numBaseband), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c basebandConfig
!$$$      if (allocated(row%basebandConfig)) then
!$$$         deallocate(row%basebandConfig, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(row%basebandConfig(numBaseband), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$c axesOrderArray (dimension???)
!$$$      if (allocated(row%axesOrderArray)) then
!$$$         deallocate(row%axesOrderArray, stat=ier)
!$$$         if (ier.ne.0) goto 98
!$$$      endif
!$$$      allocate(row%axesOrderArray(numBaseband), stat=ier)
!$$$      if (ier.ne.0) goto 99
!$$$c
!$$$      return
!$$$c
!$$$ 98   call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!$$$      error = .true.
!$$$      return
!$$$c
!$$$ 99   call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!$$$      error = .true.
!$$$      return
!$$$      end
