subroutine out0(type,x,y,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! SAS	Internal routine
  !	Sortie terminal ou graphique d'un texte tabule
  !	'G',x,y : en graphique en x,y
  !	'T',0.,0. : ecriture normale sur le terminal
  !	'F',0.,0. : fichier imprime par le call out2
  !
  ! (2-Jan-1985)
  !---------------------------------------------------------------------
  character(len=*) :: type          !
  real*4 :: x                       !
  real*4 :: y                       !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  character(len=*) :: cin,ch
  integer :: lin
  integer :: p_lun,ier, l, i, lstring
  logical :: err
  common /ciout/ p_lun,type1,filnam
  character(len=1) :: type1
  character(len=132) :: string
  character(len=80) :: filnam
  real*4 :: x1,y1,cdef
  !------------------------------------------------------------------------
  save /ciout/, string, lstring
  ! Code:
  !
  type1 = type(1:1)
  x1=x
  y1=y
  if (type1 .eq. 'G') then
    call grelocate(x1,y1)
  elseif (type1 .eq. 'F') then
    ier = sic_getlun(p_lun)
    ier = sic_open(p_lun,filnam,'NEW',.false.)
    if (ier.ne.0) then
      call message(6,3,'out', 'Cannot open file '//filnam)
      call putios ('        ',ier)
      error = .true.
      call sic_frelun(p_lun)
      return
    endif
  else
    p_lun = 6
  endif
  return
  !
  ! ERROR was missing. To be checked
entry outlin(cin,lin)
  l = lenc(cin)
  if (type1 .eq. 'G') then
    do i=1, l
      if (ichar(cin(i:i)).lt.ichar(' ')) cin(i:i) = ' '
    enddo
    call sic_blanc(cin,l)
    call grelocate(x1,y1)
    call gr_labe_cent(2)
    call gr_labe(cin(1:l))
    call sic_get_real('CHARACTER_SIZE',cdef,err)
    if (err) then
      err = .false.
      cdef = 1.
    endif
    y1 = y1-cdef*1.15
  elseif (type1 .eq. 'C') then
    call sic_blanc(cin,l)
    string = cin(1:l)
    lstring = l
    return
  elseif (p_lun.ne.6) then
    write(p_lun,'(A)') cin(1:l)
  else
    write(6,'(A)') cin(1:l)
  endif
  return
  !
entry out1(error)
  if (type1.eq.'G') then
    continue
  elseif (type1.eq.'F') then
    close(unit=p_lun,iostat=ier)
    call sic_frelun(p_lun)
  endif
  return                       ! Was missing...
  !
entry out2(ch,error)
  if (type1.eq.'C') ch = string(1:lstring)
end subroutine out0
