subroutine write_sdm_SwitchCycle(error)
  !---------------------------------------------------------------------
  ! Write the FreqOffset SDM table.
  !
  !     int sdm_addSwitchCycleRow (int * numStep, float * weightArray, int
  !     * weightArrayDimOne, double * offsetArray, int *
  !     offsetArrayDimOne, int * offsetArrayDimTwo, double *
  !     freqOffsetArray, int * freqOffsetArrayDimOne)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_SwitchCycle
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (SwitchCycleRow) :: scRow
  type (SwitchCycleKey) :: scKey
  character sdmTable*12
  parameter (sdmTable='SwitchCycle')
  integer :: ier
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! we do not cycle.
  !
  scRow%numStep = 1
  call  allocSwitchCycleRow(scRow, scRow%numStep, error)
  if (error) return
  scRow%weightArray = 1.0
  scRow%dirOffsetArray = 0.0
  scRow%freqOffsetArray = 0.0
  scRow%stepDurationArray = 0.0
  call addSwitchCycleRow(scKey, scRow, error)
  if (error) return
  SwitchCycle_Id = scKey%switchCycleId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_SwitchCycle
!---------------------------------------------------------------------
subroutine get_sdm_SwitchCycle(error)
  !---------------------------------------------------------------------
  ! Read the FreqOffset SDM table.
  !
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_SwitchCycle
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (SwitchCycleRow) :: scRow
  type (SwitchCycleKey) :: scKey
  integer, parameter :: mstep = 2
  character sdmTable*12
  parameter (sdmTable='SwitchCycle')
  integer :: ier
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! we do not cycle.
  !
  call  allocSwitchCycleRow(scRow, mStep, error)
  if (error) return
  scKey%switchCycleId = SwitchCycle_Id
  !$$$      print *, 'SwitchCycle_Id ', SwitchCycle_Id
  call getSwitchCycleRow(scKey, scRow, error)
  if (error) return
  !
  if (scRow%numStep .gt. 1) then
    call sdmMessageI(6,3,sdmTable ,'Too many switch steps '   &
      ,scRow%numStep)
    error = .true.
    return
  endif
  call sdmMessage(1,1,sdmTable ,'Read.')
  !
  return
end subroutine get_sdm_SwitchCycle
!---------------------------------------------------------------------
!!! subroutine allocSwitchCycleRow(row, numStep, error)
!!!   !---------------------------------------------------------------------
!!!   !     real, allocatable :: weightArray(:)
!!!   !     real*8, allocatable :: offsetArray(:,:)
!!!   !     real*8, allocatable :: freqOffsetArray(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_SwitchCycle
!!!   type(SwitchCycleRow) :: row
!!!   integer :: numStep, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*12 = 'SwitchCycle'
!!!   !
!!!   ! weightArray(numStep)
!!!   if (allocated(row%weightArray)) then
!!!     deallocate(row%weightArray, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%weightArray(numStep), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! offsetArray(2, numStep)
!!!   if (allocated(row%dirOffsetArray)) then
!!!     deallocate(row%dirOffsetArray, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%dirOffsetArray(2, numStep), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! freqOffsetArray(numStep)
!!!   if (allocated(row%freqOffsetArray)) then
!!!     deallocate(row%freqOffsetArray, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%freqOffsetArray(numStep), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! stepDurationArray(numStep)
!!!   if (allocated(row%stepDurationArray)) then
!!!     deallocate(row%stepDurationArray, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%stepDurationArray(numStep), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!!! !---------------------------------------------------------------------
