subroutine clic_set (line,error)
  use gildas_def
  use gkernel_interfaces
  use gkernel_types
  use clic_index
  use clic_find
  use clic_file,except=>i
  use clic_virtual
  use gio_params
  !---------------------------------------------------------------------
  ! CLIC Support routine for command
  !      SET Something [value1, value2, ...]
  ! Arguments :
  !	LINE	C*(*)	Command line		Input
  !	ERROR	L	Logical error flag	Output
  !----------------------------------------------------------------------
  !
  ! Options:
  !   1 	/LIMITS
  !   2 	/WINDOW
  !   3 	/ANTENNA
  !   4 	/DEFAULT
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_constant.inc'
  include 'gbl_pi.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: ante_data,base_data
  logical :: ante_switched, base_switched, fr_ok, d_ok, more
  common /switched/ ante_switched, base_switched
  real*8 :: test, ff
  real :: f1, f2, ftry(2,mwin_out)
  real :: rarg, freq
  integer :: iant, iarg, if, ic, iv, lch
  integer :: l_savb(mbox),i_savb(mbands,mbox), n_savb, i1, nn, l
  integer :: iso(mbands), ier, ia(mnant), na, phys_log(mnant)
  integer :: k, n_save, i_save(mbox), isb, isbb, narg
  integer :: i, j, nkey, nl, nla, nch, dayf
  logical :: and, to, reset, old_degrees
  logical :: default, bad, angle_data
  logical :: first,second
  integer :: first_quarter, second_quarter, factor, lc
  character(len=80) :: ch, fich
  character(len=12) :: argum,argum1,argum2,argum3,kw,keywor,keywor2
  character(len=12) :: nombre
  real :: frtol, frmhz
  parameter (frtol=1e3)        ! 1 GHz ...
  character(len=10) :: qw
  integer :: q, old_do_polar, n, old_narrow_input, old_widex_unit
  type(sic_listi4_t) :: list
  !
  ! Vocabularies:
  integer :: mvocab, mvband, mvsub, mvsub2
  integer :: mvoc1, mvoc2, mvoc3, mvoc4, mvoc5, mvoc6, mvoc7, mvoc8
  integer :: mvoc9, mvoc10, mvoc11, mvoc12, mvoc13, mvoc14, mvoc15
  integer :: mvoc16, mvoc17, mvoc18,mvoc19, mvoc20,mvoc21,mvoc22,mvoc23
  integer :: mamp, mpha, mrf
  parameter (mvocab=80, mvband=5, mvsub=6, mvsub2=15,   &
    mvoc1=4, mvoc2=2, mvoc3=3, mvoc4=2, mvoc5=4, mvoc6=2,   &
    mvoc7=6, mvoc8=2, mvoc9=8, mvoc10=4, mvoc11=10, mvoc12=21,   &
    mvoc13=4, mvoc14=2, mvoc15=2, mvoc16=3, mvoc17=3, mvoc18=2,  &
    mvoc19=2,mvoc20=5,mvoc21=2,mvoc22=2,mvoc23=3,                &
    mamp=13, mpha=20, mrf=9)
  character*12 vamp(mamp), vpha(mpha), vrf(mrf), vocab(mvocab),   &
    vband(mvband), vsub(mvsub), vsub2(mvsub2),   &
    voc1(mvoc1), voc2(mvoc2), voc3(mvoc3),   &
    voc4(mvoc4), voc5(mvoc5), voc6(mvoc6), voc7(mvoc7),   &
    voc8(mvoc8), voc9(mvoc9), voc10(mvoc10), voc11(mvoc11),   &
    voc12(mvoc12), voc13(mvoc13), voc14(mvoc14), voc15(mvoc15),   &
    voc16(mvoc16), voc17(mvoc17), voc18(mvoc18), voc19(mvoc19),   &
    voc20(mvoc20), voc21(mvoc21), voc22(mvoc22), voc23(mvoc23)
  !
  ! Datas
  data voc1/'DEGREE','MINUTE','SECOND','RADIAN'/
  data voc2/'YES','NO'/
  data voc3/'PHYSICAL','LOGICAL','ALL'/
  data voc4/'ATMOSPHERE','NOISE'/
  data voc5/'OBJECT','PHASE','RF_PASSBAND','*'/
  data voc6/'EXACT','AUTO'/
  data voc7/'NONE','SCAN','TIME','METHOD','CONT','LINE'/
  data voc8/'VECTOR','SCALAR'/
  data voc9/'LINE','POINTS','HISTOGRAMS','BARS','PHYSICAL','LOGICAL', &
     'HEADER','IPB'/
  data voc10/'TIME','SPECTRAL','LOWRES','FULL'/
  data voc11/'AUTO','MANUAL','TREC','FILE',   &
    'OLD','NEW','1985','2003','2009','INTERPOLATE'/
  data voc12/'CORRELATION','AUTOCORR','GAIN','DELAY','FOCUS',   &
    'POINTING','CALIBRATE','IFPB','ONOFF','HOLOGRAPHY',   &
    'FIVE_POINT','PSEUDO_CONT','FLUX','STABILITY','CWVR',   &
    'VLBI','VLBG','OTF','BANDPASS','SKYDIP','*'/
  data voc13/'TSYS','INSTRUMENTAL','CALIBRATION','BANDPASS'/
  data voc14/'ON','OFF'/
  data voc15/'UPDATE','NOUPDATE'/
  data voc16/'*','PRIMARY','FILE'/
  data voc17/'RECEIVER','SCAN','NUM'/
  data voc18/'DATA','NODATA'/
  data voc19/'OLD','NEW'/
  data voc20/'OFF','HORIZONTAL','VERTICAL','BOTH','EACH'/
  data voc21/'RAW','PHYSICAL'/
  data voc22/'STANDARD','PROTOTYPE'/
  data voc23/'NATIVE','LEAKAGE','CORRECTED'/
  !
  ! SET X|Y keywords --> moved to clic_xy_code.inc
  !
  ! SET SUBBANDS
  data vsub/'TO','AND','ALL','EACH','LINE','CONTINUUM'   &
    /
  data vsub2/'ALL','TO','AND','LINE','CONTINUUM','FREQUENCY',   &
    'UPPER','USB','LOWER','LSB','AVERAGE','DSB','AUTO','SSB','WIDEX'/
  data vband /'UPPER','LOWER','AVERAG','RATIO','DIFFER'/
  !
  ! SET AMPLITUDE|PHASE|RF
  data vamp/'ABSOLUTE','RELATIVE','SCALED','UNSCALED',   &
    'KELVIN','JANSKY','ANTENNA','BASELINE','RAW',        &
    'SIZED','UNSIZED','SPIDX','NOSPIDX'/
  data vpha/'ABSOLUTE','RELATIVE','ANTENNA','BASELINE',   &
    'DEGREES','RADIANS','JUMPY','CONTINUOUS',   &
    'ATMOSPHERE','NOATMOSPHERE', 'EXTERNAL','INTERNAL',   &
    'FILE','NOFILE','WVR','NOWVR','MODEL','EMPIRICAL',   &
    'TOTAL_POWER','NOTOTAL_POWER'/
  data  vrf/'ANTENNA','BASELINE','CHANNEL','FREQUENCY',   &
    'FILE','MEMORY','OFF','ON','SPECTRUM'/
  !
  ! List of SET keywords
  data vocab /   &
    'PROJECT',   &             !1  H
    'ANGLE',   &               !2  H
    'APC',   &                 !3  H obsolete
    'IC',   &                  !4  H
    'PC',   &                  !5  H
    'TRIANGLE',   &            !6  H
    'ERRORS',   &              !7  H
    'DEFAULT',   &             !8  H
    'EXTENSION',   &           !9  H
    'FORMAT',   &              !10 H
    'HOUR_ANGLE',   &          !11 H
    'LEVEL',   &               !12 H
    'LINE',   &                !13 H
    'CORRECTION',   &          !14 no help (will not get one)
    'NUMBER',   &              !15 H
    'OBSERVED',   &            !16 H
    'OFFSET',   &              !17 H
    'RANGE',   &               !18 H
    'REDUCED',   &             !19 H
    'SOURCE',   &              !20 H
    'TELESCOPE',   &           !21 H
    'TYPE',   &                !22 H
    'QUALITY',   &             !23 H
    'SCAN',   &                !24 H
    'BASELINES',   &           !25 H
    'BANDS',   &               !26 H
    'SUBBANDS',   &            !27 H
    'X',   &                   !28 H
    'Y',   &                   !29 H
    'ASPECT_RATIO',   &        !30 H
    'AVERAGING',   &           !31 H
    'PLOT',   &                !32 H
    'STEP',   &                !33 H
    'PHASE',   &               !34 H
    'MODE',   &                !35 H obsolete
    'ATMOSPHERE',   &          !36 H
    'REFERENCE',   &           !37 H
    'FREQUENCY',   &           !38 H
    'PROCEDURE',   &           !39 H
    'CLOSURE',   &             !40 H obsolete
    'AMPLITUDE',   &           !41 H
    'RF_PASSBAND',   &         !42 H
    'FLUX',   &                !43 H
    'WATER',   &               !44 H
    'TREC',   &                !45 H
    'VIRTUAL',   &             !46 obsolete
    'SELECTION',   &           !47 H
    'ANTENNAS',   &            !48 H
    'SPECTRUM',   &            !49 obsolete
    'BINNING',   &             !50 H
    'WEIGHTS',   &             !51 H
    'DATA',   &                !52 H
    'RECORD',   &              !53 H
    'CRITERIA',   &            !54 H
    'GAIN_IMAGE',   &          !55 H
    'DROP_FRACTIO',   &        !56 H
    'GIBBS',   &               !57 H
    'FIND',   &                !58 no help
    'PLANET',   &              !59 H
    'RECEIVER',   &            !60 H
    'UV_RANGE',   &            !61 H
    'TIME_ORIGIN',   &         !62 H
    'SORT',   &                !63 H
    'COPY',   &                !64 no help
    'GAIN_METHOD',   &         !65 H 
    'POLARIZATION',   &        !66 H
    'FAKE',   &                !67 no help (will not get one)
    'TOTAL_POWER',   &         !68 H
    'NARROW_INPUT',  &         !69 H
    'WIDEX_UNIT',    &         !70 H
    'BBAND',         &         !71 no help
    'IF',            &         !72
    'SPW',           &         !73
    'SKIP',          &         !74
    'EXCLUDE',       &         !75
    'SPIDX',         &         !76
    'VERSION',       &         !77
    'STOKES',        &         !78
    'TABLE',         &         !79
    'FINDPOL'/                 !80
  !
  !------------------------------------------------------------------------
  !
  ! Code:
  reset = .false.
  default = sic_present(4,0)
  if (default .and. .not.sic_present(0,1)) then
    call setdef(line,error)
    goto 90
  endif
  call sic_ke(line,0,1,argum,nch,.true.,error)
  if (error) return
  error = .false.
  call sic_ambigs('SET',argum,keywor,nkey,vocab,mvocab,error)
  if (error) return
  narg = sic_narg(0)
  !
  ! SET PROJECT
  if (nkey.eq.1) then
    sproject = '*'
    call sic_ke(line,0,2,sproject,nch,.true.,error)
    if (error) return
    !
    ! Reset RF Pass Band also...
    do_pass = .false.
    find_needed = .true.
    change_display = .true.
    call show_criteria('PROJECT',.false.,error)
    call show_general('RF_PASSBAND',.false.,line,error)
  !
  ! SET ANGLE
  elseif (nkey.eq.2) then
    argum1='SECOND'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET ANGLE',argum1,kw,nkey,voc1,mvoc1,error)
    if (error) return
    sangle=kw(1:1)
    if (sangle.eq.'R') then
      fangle = 1
    elseif (sangle.eq.'D') then
      fangle = 180.d0/pi
    elseif (sangle.eq.'M') then
      fangle = 180.d0*60.d0/pi
    elseif (sangle.eq.'S') then
      fangle = 180.d0*60.d0*60.d0/pi
    endif
  !
  ! SET APC **OBSOLETE**
  elseif (nkey.eq.3) then
    sapc = -1
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET APC',argum1,kw,nkey,voc2,mvoc2,error)
    if (error) return
    if (kw.eq.'YES') then
      sapc = 1
    elseif (kw.eq.'NO') then
      sapc = 0
    endif
    find_needed = .true.
    call show_criteria('APC',.false.,error)
  !
  ! SET IC
  elseif (nkey.eq.4) then
    setic = -1
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET IC',argum1,kw,nkey,voc2,mvoc2,error)
    if (error) return
    if (kw.eq.'YES') then
      setic = 1
    elseif (kw.eq.'NO') then
      setic = 0
    endif
    call show_criteria('IC',.false.,error)
    find_needed = .true.
  !
  ! SET BPC
  elseif (nkey.eq.5) then
    sbpc = -1
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET BPC',argum1,kw,nkey,voc2,mvoc2,error)
    if (error) return
    if (kw.eq.'YES') then
      sbpc = 1
    elseif (kw.eq.'NO') then
      sbpc = 0
    endif
    call show_criteria('BPC',.false.,error)
    find_needed = .true.
  !
  ! SET TRIANGLE				! internally coded as mnbas+1, ..
  elseif (nkey.eq.6) then
    if (narg.le.1) goto 90
    n_save = n_base
    do i=1, n_save
      i_save(i) = i_base(i)
    enddo
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) goto 69
    call sic_ambigs('SET TRIANGLE',argum1,kw,nkey,voc3,mvoc3,error)
!    if (error) goto 69
    if (kw.eq.'PHYSICAL') then
      phys_base = .true.
      i1 = 3
    elseif (kw.eq.'LOGICAL') then
      phys_base = .false.
      i1 = 3
    else
      i1 = 2
    endif
    if (narg.lt.i1) goto 69
    n_base = 0
    do i = i1, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 69
      call sic_ambigs('SET',argum1,kw,nkey,ctri,mntri,error)
      if (error) goto 69
      n_base = n_base+1
      i_base(n_base) = mnbas + nkey
    enddo
    if (n_base.gt.0) call set_display(error)
69  continue
    if (error) then
      n_base = n_save
      do i=1, n_save
        i_base(i) = i_save(i)
      enddo
    else
      call show_display('TRIANGLE',.false.,error)
      change_display = .true.
    endif
  !
  ! SET ERRORS
  elseif (nkey.eq.7) then
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET ERRORS',argum1,kw,nkey,voc4,mvoc4,error)
    if (error) return
    if (kw.eq.'A') then
      do_bar_atm = .true.
    else
      do_bar_atm = .false.
    endif
  !
  ! SET DEFAULT
  elseif (nkey.eq.8) then
    call setdef(line,error)
    find_needed = .true.
  !
  ! SET EXTENSION e1 e2 ...
  elseif (nkey.eq.9) then
    ndefext = 0
    do i=2, sic_narg(0)
      argum1 = '.hpb'
      call sic_ch(line,0,i,argum1,nch,.false.,error)
      if (error) return
      if (argum1(1:1).ne.'.') argum1 = '.'//argum1
      call check_extension (argum1, lenc(argum1), error)
      if (error) return
      ndefext = ndefext+1
      defext(ndefext) = argum1
    enddo
    write(ch,*) 'Default file extensions set to ',   &
      (defext(i),i=1,ndefext)
    lch = lenc(ch)
    call message(2,1,'SET',ch(1:lch))
  !
  ! SET FORMAT
  elseif (nkey.eq.10) then
    call setfor(line,error)
  !
  ! SET HOUR_ANGLE
  elseif (nkey.eq.11) then
    argum1  = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    if (argum1(1:1).ne.'*') then
      call sic_sexa(argum1,lenc(argum1),test,error)
      if (error) return
      if (test.gt.12. .or. test.lt.-12.) then
        call message(6,3,'SET HOUR','Invalid Hour Angle')
        error = .true.
        return
      endif
      shoura1 = pi / 12. * test
    else
      shoura1 = -pi
    endif
    argum2  = '*'
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    if (argum1.ne.'*') then
      call sic_sexa(argum2,lenc(argum2),test,error)
      if (error) return
      if (test.gt.12. .or. test.lt.-12.) then
        call message(6,3,'SET HOUR','Invalid Hour Angle')
        error = .true.
        return
      endif
      shoura2 = pi / 12. * test
    else
      shoura2 = pi
    endif
    call show_criteria('HOUR',.false.,error)
    find_needed = .true.
  !
  ! SET LEVEL
  elseif (nkey.eq.12) then
    i = 2
    call sic_i4(line,0,2,i,.false.,error)
    if (error) return
    i = min(8,i)
    i = max(0,i)
    write(nombre,'(I12)') i
    call message_level(i)
    call message(4,1,'SET','Message display level set to '//nombre)
  !
  ! SET LINE
  elseif (nkey.eq.13) then
    sline = '*'
    !         CALL SIC_CH(LINE,0,2,SLINE,NCH,.FALSE.,ERROR)
    call sic_ke(line,0,2,sline,nch,.false.,error)
    if (error) return
    call show_criteria('LINE',.false.,error)
    find_needed = .true.
  !
  ! SET CORRECTION
  elseif (nkey.eq.14) then
    phase_corr(1,1) = 50.
    do i=1, mnant
      phase_corr(1,i) = phase_corr(1,1)
      call sic_r4(line,0,1+i,phase_corr(1,i),.false.,error)
      phase_corr(2,i) = phase_corr(1,i)
    enddo
    call show_general('CORRECTION',.false.,line,error)
  !
  ! SET NUMBER
  elseif (nkey.eq.15) then
    argum1='*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    if (argum1(1:1).eq.'*') then
      snume1=0
    else
      call sic_i4(line,0,2,snume1,.false.,error)
      if (error) return
    endif
    argum2='*'
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    if (argum2(1:1).eq.'*') then
      snume2=2147483647
    else
      call sic_i4(line,0,3,snume2,.false.,error)
      if (error) return
    endif
    call show_criteria('NUMBER',.false.,error)
    find_needed = .true.
  !
  ! SET OBSERVED_DATE
  elseif (nkey.eq.16) then
    argum1  = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    argum2 = argum1
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    read(argum1,'(i6)',iostat=ier) sobse1
    if (abs(sobse1).gt.32768 .or. ier.ne.0) then
      sobse1 = -32768
      call cdate(argum1,sobse1,error)
    endif
    read(argum2,'(i6)',iostat=ier) sobse2
    if (abs(sobse2).gt.32768 .or. ier.ne.0) then
      sobse2 = 32767
      call cdate(argum2,sobse2,error)
    endif
    call show_criteria('OBSERVED',.false.,error)
    find_needed = .true.
  !
  ! SET OFFSET
  elseif (nkey.eq.17) then
    argum1  = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    argum2 = '*'
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    soffs1 = 4.0*pi
    call coffse(argum1,soffs1,error)
    soffl1  = soffs1
    soffs2 = 4.0*pi
    call coffse(argum2,soffs2,error)
    soffl2  = soffs2
    call show_criteria('OFFSETS',.false.,error)
    find_needed = .true.
  !
  ! SET RANGE
  elseif (nkey.eq.18) then
    argum1='*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    soffs1= 4.0*pi
    call coffse(argum1,soffs1,error)
    argum2='*'
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    soffl1= 4.0*pi
    call coffse(argum2,soffl1,error)
    argum1='*'
    call sic_ke(line,0,4,argum1,nch,.false.,error)
    if (error) return
    soffs2= 4.0*pi
    call coffse(argum1,soffs2,error)
    argum2='*'
    call sic_ke(line,0,5,argum2,nch,.false.,error)
    if (error) return
    soffl2= 4.0*pi
    call coffse(argum2,soffl2,error)
    call show_criteria('RANGE',.false.,error)
    find_needed = .true.
  !
  ! SET REDUCED_DATE
  elseif (nkey.eq.19) then
    argum1  = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    argum2 = argum1
    call sic_ke(line,0,3,argum2,nch,.false.,error)
    if (error) return
    sredu1 = -32768
    call cdate(argum1,sredu1,error)
    sredu2 = 32767
    call cdate(argum2,sredu2,error)
    call show_criteria('REDUCED',.false.,error)
    find_needed = .true.
  !
  ! SET SOURCE
  elseif (nkey.eq.20) then
    nsour = narg-1
    find_needed = .true.
    if (nsour.gt.10) then
      call message(6,2,'SET SOURCES','Only 10 sources retained')
      nsour = 10
    endif
    do i=1,nsour
      !            CALL SIC_CH(LINE,0,I+1,ARGUM1,NCH,.TRUE.,ERROR)
      call sic_ke(line,0,i+1,argum1,nch,.true.,error)
      if (error) return
      j = index(argum1,'*')
      if (j.eq.0) then
        sisour(i) = 12
      elseif (j.ne.1) then
        sisour(i) = j-1
      else
        nsour = 0
        goto 90
      endif
      sssour(i) = argum1
    enddo
    call show_criteria('SOURCE',.false.,error)
  !
  ! SET TELESCOPE
  elseif (nkey.eq.21) then
    steles = '*'
    call sic_ke(line,0,2,steles,nch,.false.,error)
    if (error) return
    call show_criteria('TELESCOPE',.false.,error)
    find_needed = .true.
  !
  ! SET TYPE Source|Phase|RF_Passband
  elseif (nkey.eq.22) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET TYPE',argum1,kw,nkey,voc5,mvoc5,error)
    if (error) return
    if (kw.eq.'OBJECT') then
      sitype = 1
    elseif (kw.eq.'PHASE') then
      sitype = 2
    elseif (kw.eq.'RF_PASSBAND') then
      sitype = 3
    elseif (kw.eq.'*') then
      sitype = -1
    endif
    call show_criteria('TYPE',.false.,error)
    find_needed = .true.
  !
  ! SET QUALITY
  elseif (nkey.eq.23) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call get_quality(argum1,squal,qw,error)
    if (error) return
    call show_criteria('QUALITY',.false.,error)
    find_needed = .true.
  !
  ! SET SCAN
  elseif (nkey.eq.24) then
    argum1='*'
    snscan = 0
    do i=2, sic_narg(0)-1, 2
      call sic_ke(line,0,i,argum1,nch,.false.,error)
      if (error) return
      argum2='*'
      call sic_ke(line,0,i+1,argum2,nch,.false.,error)
      if (error) return
      snscan = snscan+1
      if (argum1(1:1).eq.'*') then
        sscan1(snscan)=0
      else
        call sic_i4(line,0,i,sscan1(snscan),.false.,error)
        if (error) return
      endif
      if (argum2(1:1).eq.'*') then
        sscan2(snscan)=2147483647
      else
        call sic_i4(line,0,i+1,sscan2(snscan),.false.,error)
        if (error) return
      endif
    enddo
    call show_criteria('SCAN',.false.,error)
    find_needed = .true.
  !
  ! SET BASELINES
  elseif (nkey.eq.25) then
    if (narg.le.1) then
      call switch_baseline
      change_display = .true.
      call set_display(error)
      goto 90
    endif
    n_save = n_base
    do i=1, n_save
      i_save(i) = i_base(i)
    enddo
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) goto 259
    all_base = .false.
    nch = ichar(argum1(1:1))
    if (nch.gt.ichar('9')) then
      call sic_ambigs('SET BASELINE',argum1,kw,nkey,voc3,mvoc3,   &
        error)
      if (error) goto 259
      if (kw.eq.'PHYSICAL') then
        phys_base = .true.
        i1 = 3
      elseif (kw.eq.'LOGICAL') then
        phys_base = .false.
        i1 = 3
      elseif (kw.eq.'ALL') then
        all_base = .true.
        i_base(1) = 1
        call set_all_baselines
        i1 = 3
      endif
    else
      i1 = 2
    endif
    if (narg.lt.i1) goto 259
    n_base = 0
    error = .true.
    do i = i1, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 259
      call sic_ambigs('SET',argum1,kw,nkey,cbas,mnbas,error)
      if (error) goto 259
      n_base = n_base+1
      i_base(n_base) = nkey
    enddo
    error = .false.
    if (n_base.gt.0) call set_display(error)
259 continue
    if (error) then
      n_base = n_save
      do i=1, n_save
        i_base(i) = i_save(i)
      enddo
    else
      call show_display('BASELINES',.false.,error)
      change_display = .true.
    endif
  !
  ! SET BANDS
  elseif (nkey.eq.26) then
    if (narg.le.1) goto 90
    bad = .true.
    n_save = n_band
    do i=1, n_save
      i_save(i) = i_band(i)
    enddo
    n_band = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 269
      if (argum1(1:2).eq.'US') then
        argum1 = 'UPPER'
      elseif (argum1(1:2).eq.'LS') then
        argum1 = 'LOWER'
      elseif (argum1(1:2).eq.'DS') then
        argum1 = 'AVER'
      endif
      call sic_ambigs('SET',argum1,kw,nkey,vband,mvband,error)
      if (error) goto 269
      n_band = n_band+1
      i_band(n_band) = nkey
    enddo
    error = .false.
    if (n_band.gt.0) call set_display(error)
    bad = error
269 continue
    if (bad) then
      n_band = n_save
      do i=1, n_save
        i_band(i) = i_save(i)
      enddo
    else
      change_display = .true.
      call show_display('BAND',.false.,error)
    endif
  !
  ! SET SUBBANDS
  elseif (nkey.eq.27) then
    if (narg.le.1) goto 90
    bad = .true.
    n_savb = n_subb
    do i=1, n_savb
      l_savb(i) = l_subb(i)
      do j=1, l_savb(i)
        i_savb(j,i) = i_subb(j,i)
      enddo
    enddo
    n_subb = 0
    to = .false.
    and = .false.
    k = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 279
      call sic_ambigs(' ',argum1,kw,nkey,csub,mcsub,more)
      if (more) call sic_ambigs('SET SUBBANDS',argum1,kw,nkey,   &
        vsub, mvsub, error)
      if (error) goto 279
      !
      if (kw.eq.'TO') then
        to = .true.
      elseif (kw.eq.'AND') then
        and = .true.
      !
      elseif (kw.eq.'ALL') then
        n_subb = n_savb
        all_subb = .true.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        call set_all_subbands
      elseif (kw.eq.'EACH') then
        n_subb = n_savb
        all_subb = .false.
        each_subb = .true.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        call set_each_subband
      elseif (kw.eq.'LINE') then
        n_subb = n_savb
        do j=1, n_subb
          if (i_subb(1,j).le.mbands) then
            do k=1, l_subb(j)
              i_subb(k,j) = i_subb(k,j)+mbands
            enddo
          endif
        enddo
      elseif (kw.eq.'CONTINUUM') then
        n_subb = n_savb
        do j=1, n_subb
          if (i_subb(1,j).gt.mbands) then
            do k=1, l_subb(j)
              i_subb(k,j) = i_subb(k,j)-mbands
            enddo
          endif
        enddo
      else
        all_subb = .false.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        if (to) then
          if (k.eq.0 .or. nkey.le.k) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
            goto 279
          endif
          do j = k+1, nkey
            l_subb(n_subb) = l_subb(n_subb) + 1
            i_subb(l_subb(n_subb),n_subb) = j
          enddo
          k = nkey
          to = .false.
        elseif (and) then
          if (k.eq.0) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
            goto 279
          endif
          l_subb(n_subb) = l_subb(n_subb) + 1
          i_subb(l_subb(n_subb),n_subb) = nkey
          k = nkey
          and = .false.
        else
          n_subb = n_subb+1
          k = nkey
          i_subb(1,n_subb) = nkey
          l_subb(n_subb) = 1
        endif
        !
        if (sic_present(2,0)) then
          argum1 = sw1(n_subb)
          call sic_ke(line,2,2*n_subb-1,argum1,nch,.true.,error)
          if (error) goto 279
          if (argum1.eq.'*') then
            sw1(n_subb) = '*'
            iw1(n_subb) = 0
          elseif (argum1.ne.'=') then
            call sic_i4(line,2,2*n_subb-1,iw1(n_subb),.true.,   &
              error)
            if (error) goto 279
            sw1(n_subb) = 'F'
          endif
          argum1 = sw2(n_subb)
          call sic_ke(line,2,2*n_subb,argum1,nch,.true.,error)
          if (error) goto 279
          if (argum1.eq.'*') then
            sw2(n_subb) = '*'
            iw2(n_subb) = 0
          elseif (argum1.ne.'=') then
            call sic_i4(line,2,2*n_subb, iw2(n_subb),.true.,   &
              error)
            if (error) goto 279
            sw2(n_subb) = 'F'
          endif
        else
          sw1(n_subb) = '*'
          iw1(n_subb) = 0
          sw2(n_subb) = '*'
          iw2(n_subb) = 0
        endif
      endif
    enddo
    !
    error = .false.
    if (.not.sic_present(6,0)) call select_subb
    !
    ! Now check/switch polarization mode
    !
    if ((new_receivers).and.(narrow_input.eq.0).and.(widex_unit.eq.0)) then
      old_do_polar = do_polar
      call check_pol_subbands(error)
      kw = voc20(do_polar+1)
      n = lenc(kw)
      if (do_polar.ne.old_do_polar) then
        call message(6,1,'CLIC_SET',   &
          'Switching to polarization mode: '//kw(1:n))
      else
        call message(6,1,'CLIC_SET',   &
          'Polarization mode: '//kw(1:n))
      endif
    endif
    !
    ! Now check/switch narrow band correlator input
    !
    if ((new_receivers).and.(narrow_input.ne.0)) then
      old_narrow_input = narrow_input
      call check_nbc_subbands(error)
      if (narrow_input.ne.old_narrow_input) then
        write (ch, '(A,I1)')   &
          'Switching to narrow-band correlator input ',   &
          narrow_input
        call message(6,1,'CLIC_SET',ch)
      else
        write (ch, '(A,I1)')   &
          'Narrow-band correlator input ',   &
          narrow_input
        call message(6,1,'CLIC_SET',ch)
      endif
    endif
    !
    call set_display(error)
    bad = error
279 continue
    if (bad) then
      n_subb = n_savb
      do i=1, n_savb
        l_subb(i) = l_savb(i)
        do j=1, l_savb(i)
          i_subb(j,i) = i_savb(j,i)
        enddo
      enddo
    else
      change_display = .true.
      call show_display('SUBBAND',.false.,error)
    endif
  !
  ! SET X
  elseif (nkey.eq.28) then
    if (narg.le.1) goto 90
    bad = .true.
    n_save = n_x
    do i=1, n_save
      i_save(i) = i_x(i)
    enddo
    n_x = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 289
      call sic_ambigs('SET',argum1,kw,nkey,vxy,mxy,error)
      if (error) goto 289
      n_x = n_x+1
      i_x(n_x) = nkey
      if (.not.new_receivers.and.nkey.eq.xy_i_fre3) then
        print *,"E-SET THERE IS NO IF3 IN OLD RECEIVERS"
        error = .false.
        goto 289
      endif
    enddo
    if (n_x.le.0) goto 289
    do i=1, n_x
      if (sic_present(1,2*i-1)) then
        call sic_ke(line,1,2*i-1,argum1,nch,.true.,error)
        if (error) goto 289
        if (argum1.eq.'*') then
          sm_x1(i) = '*'
        elseif (argum1.eq.'=') then
          sm_x1(i) = '='
        else
          call sic_r4(line,1,2*i-1,gu1_x(i),.true.,error)
          if (error) goto 289
          sm_x1(i) = 'F'
        endif
      elseif (i_x(i).eq.1) then
        sm_x1(i) = 'F'
        gu1_x(i) = 0.
      elseif (angle_data(i_x(i)) .and. .not.continuous) then
        sm_x1(i) = 'F'
        if (degrees) then
          gu1_x(i) = -180.
        else
          gu1_x(i) = -pi
        endif
      elseif (i_x(i).eq.xy_i_fre) then
        sm_x1(i) = '='         ! LIMITS DEFAULT TO = FOR IF.
      else
        sm_x1(i) = '*'
      endif
      if (sic_present(1,2*i)) then
        call sic_ke(line,1,2*i,argum1,nch,.true.,error)
        if (error) goto 289
        if (argum1.eq.'*') then
          sm_x2(i) = '*'
        elseif (argum1.eq.'=') then
          sm_x2(i) = '='
        else
          call sic_r4(line,1,2*i,gu2_x(i),.true.,error)
          if (error) goto 289
          sm_x2(i) = 'F'
        endif
      elseif (angle_data(i_x(i)) .and. .not.continuous) then
        sm_x2(i) = 'F'
        if (degrees) then
          gu2_x(i) = +180.
        else
          gu2_x(i) = +pi
        endif
      elseif (i_x(i).eq.xy_i_fre) then
        sm_x2(i) = '='         ! LIMITS DEFAULT TO = FOR IF.
      else
        sm_x2(i) = '*'
      endif
    enddo
    error = .false.
    call set_display(error)
    bad = error
289 continue
    if (bad) then
      n_x = n_save
      do i=1, n_save
        i_x(i) = i_save(i)
      enddo
    else
      change_display = change_display.or.n_save.ne.n_x
      if (.not.change_display) then
        do i=1, n_save
          change_display = change_display   &
            .or. i_x(i).ne.i_save(i)
        enddo
      endif
      call show_display('X   ',.false.,error)
      if (do_bin .and. i_x(1).ne.i_save(1)) then
        do_bin = .false.
        call message(4,1,'CLIC_SET','BINNING IS OFF')
      endif
    endif
  !
  ! SET Y
  elseif (nkey.eq.29) then
29  if (narg.le.1) goto 90
    bad = .true.
    n_save = n_y
    do i=1, n_save
      i_save(i) = i_y(i)
    enddo
    n_y = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 299
      call sic_ambigs('SET',argum1,kw,nkey,vxy,mxy,error)
      if (error) goto 299
      n_y = n_y+1
      i_y(n_y) = nkey
    enddo
    if (n_y.le.0) goto 299
    do i=1, n_y
      if (sic_present(1,2*i-1)) then
        call sic_ke(line,1,2*i-1,argum1,nch,.true.,error)
        if (error) goto 299
        if (argum1.eq.'*') then
          sm_y1(i) = '*'
        elseif (argum1.eq.'=') then
          sm_y1(i) = '='
        else
          call sic_r4(line,1,2*i-1,gu1_y(i),.true.,error)
          if (error) goto 299
          sm_y1(i) = 'F'
        endif
      elseif (i_y(i).eq.1) then
        sm_y1(i) = 'F'
        gu1_y(i) = 0.
      elseif (angle_data(i_y(i)) .and. .not.continuous) then
        sm_y1(i) = 'F'
        if (degrees) then
          gu1_y(i) = -180.
        else
          gu1_y(i) = -pi
        endif
      else
        sm_y1(i) = '*'
      endif
      if (sic_present(1,2*i)) then
        call sic_ke(line,1,2*i,argum1,nch,.true.,error)
        if (error) goto 299
        if (argum1.eq.'*') then
          sm_y2(i) = '*'
        elseif (argum1.eq.'=') then
          sm_y2(i) = '='
        else
          call sic_r4(line,1,2*i,gu2_y(i),.true.,error)
          if (error) goto 299
          sm_y2(i) = 'F'
        endif
      elseif (angle_data(i_y(i)) .and. .not.continuous) then
        sm_y2(i) = 'F'
        if (degrees) then
          gu2_y(i) = +180.
        else
          gu2_y(i) = +pi
        endif
      else
        sm_y2(i) = '*'
      endif
    enddo
    i = i_y(1)
    if (ante_data(i)) then
      call switch_antenna
    !            ANTE_SWITCHED = .TRUE.
    !         ELSEIF (ANTE_SWITCHED) THEN
    !            CALL SWITCH_BASELINE
    !            ANTE_SWITCHED = .FALSE.
    endif
    if (base_data(i)) then
      call switch_baseline
    !           BASE_SWITCHED = .TRUE.
    !         ELSEIF (BASE_SWITCHED) THEN
    !            CALL SWITCH_ANTENNA
    !            BASE_SWITCHED = .FALSE.
    endif
    error = .false.
    call set_display(error)
    bad = error
299 continue
    if (bad) then
      n_y = n_save
      do i=1, n_save
        i_y(i)  = i_save(i)
      enddo
    else
      change_display = change_display.or.n_save.ne.n_y
      if (.not.change_display) then
        do i=1, n_save
          change_display = change_display   &
            .or. i_y(i).ne.i_save(i)
        enddo
      endif
      call show_display('Y    ',.false.,error)
    endif
  !
  ! SET ASPECT_RATIO
  elseif (nkey.eq.30) then
    call sic_r4(line,0,2,rarg,.true.,error)
    if (error) return
    if (rarg .le.0) then
      call message(6,3,'CLIC_SET','INVALID ASPECT RATIO')
      error = .true.
      return
    endif
    argum1 = 'EXACT'
    call sic_ke(line,0,3,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET ASPECT_RATIO',argum1,kw,nkey,voc6,mvoc6,   &
      error)
    if (error) return
    if (kw.eq.'AUTO') then
      aratio = - rarg
    else
      aratio = rarg
    endif
    call set_display(error)
    call show_display('ASPECT_RATIO',.false.,error)
  !
  ! SET AVERAGING
  elseif (nkey.eq.31) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET AVERAGING',argum1,kw,nkey,voc7,mvoc7,   &
      error)
    if (error) return
    if (kw.eq.'NONE') then
      i_average = 0
      s_average = 0
    elseif (kw.eq.'SCAN') then
      i_average = 1
      s_average = 0
      if (sic_narg(0).gt.2) then
        call sic_i4(line,0,3,iarg,.false.,error)
        if (iarg.gt.0) s_average = iarg
      endif
    elseif (kw.eq.'TIME') then
      i_average = 2
      s_average = 0
      call sic_r4(line,0,3,rarg,.true.,error)
      if (error) return
      if (rarg.lt.0) then
        call message(6,3,'CLIC_SET','INVALID AVERAGING TIME')
        error = .true.
        return
      endif
      t_average = rarg
    elseif (kw.eq.'METHOD') then
      call sic_ke(line,0,3,argum2,nch,.true.,error)
      if (error) return
      call sic_ambigs('SET AVERAGING METHOD',argum2,kw,nkey,   &
        voc8,mvoc8, error)
      if (error) return
      if (kw.eq.'VECTOR') then
        j_average = 1
      elseif (kw.eq.'SCALAR') then
        j_average = 0
      else
        call message(6,3,'CLIC_SET',   &
          'INVALID AVERAGING METHOD '//argum2)
      endif
    elseif (kw.eq.'CONT') then
      k_average = 0
    elseif (kw.eq.'LINE') then
      k_average = 1  
    else
      call message(6,3,'CLIC_SET',   &
        'INVALID AVERAGING MODE '//argum1)
      error = .true.
      return
    endif
    call show_display('AVERAGE',.false.,error)
    change_display = .true.
  !
  ! SET PLOT
  elseif (nkey.eq.32) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET PLOT',argum1,kw,nkey,voc9,mvoc9,error)
    if (error) return
    if (kw.eq.'PHYSICAL') then
      do_physical = .true.
    elseif(kw.eq.'LOGICAL') then
      do_physical = .false.
    elseif(kw.eq.'HEADER') then
      skip_flag = .true.
    elseif(kw.eq.'IPB') then
      skip_flag = .false.
    else
      plot_type = kw(1:4)
    endif
    call show_display('PLOT',.false.,error)
  !
  ! SET STEP
  elseif (nkey.eq.33) then
    call sic_r4(line,0,2,rarg,.true.,error)
    if (error) return
    if (rarg .le.0) then
      call message(6,3,'CLIC_SET','INVALID TIME STEP')
      error = .true.
      return
    endif
    t_step = rarg
    call show_display('STEP',.false.,error)
  !
  ! SET PHASE RELATIVE|ABSOLUTE ANTENNA|BASELINE DEGREE|RADIAN JUMPY|CONT N
  ! SET PHASE INTERNAL|EXTERNAL
  ! SET PHASE ATM|NOATM /ANTENNA ALL|{1 3 ...}
  ! SET PHASE WVR|NOWVR [MODEL|EMPIRICAL] /ANTENNA ALL|{1 3 ...}
  ! SET PHASE TOTAL|NOTOTAL /ANTENNA ALL|{1 3 ...}
  elseif (nkey.eq.34) then
    do i=2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) return
      if (ichar(argum1(1:1)).ge.ichar('0')   &
        .and. ichar(argum1(1:1)).le.ichar('9')   &
        .and. continuous) then
        read(argum1(1:lenc(argum1)),*) n_cont
        n_cont = max(0,n_cont)
        n_cont = min(n_cont,30)
        reset = .true.
      else
        old_degrees = degrees
        call sic_ambigs('SET',argum1,keywor2,nkey,vpha,mpha,   &
          error)
        if (error) return
        ! ABSOLUTE
        if (nkey.eq.1) then
          do_phase = .false.
          change_display = .true.
        ! RELATIVE
        elseif (nkey.eq.2) then
          do_phase = .true.
          change_display = .true.
        ! ATMOSPHERE|NOATMOPSHERE
        elseif (nkey.eq.9) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_atm(ia(j)) = .true.
          enddo
          if (i_average.ne.1) then
            i_average = 1
            call message(6,1,'CLIC_SET',   &
              'SWITCHING ON SET AVERAGE SCAN METHOD')
          endif
        elseif (nkey.eq.10) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_atm(ia(j)) = .false.
          enddo
        !
        ! CAUTION: IT IS POSSIBLE TO HAVE CORRECTION BASED ON WVR FOR SOME
        ! ANTENNAS, AND ON 1.3 MM TOTAL POWER MONITOR FOR OTHERS
        !
        ! WVR
        elseif (nkey.eq.15) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            if (r_wvrnch(ia(j)).eq.3) then
              do_phase_wvr(ia(j)) = .true.
              do_phase_mon(ia(j)) = .false.
            else
              do_phase_wvr(ia(j)) = .false.
            endif
          enddo
        elseif (nkey.eq.16) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_wvr(ia(j)) = .false.
          enddo
        elseif (nkey.eq.17) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_wvr_e(ia(j)) = .false.
          enddo
        elseif (nkey.eq.18) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_wvr_e(ia(j)) = .true.
          enddo
        ! TOTAL POWER MONITOR
        elseif (nkey.eq.19) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_mon(ia(j)) = .true.
            do_phase_wvr(ia(j)) = .false.
          enddo
        elseif (nkey.eq.20) then
          call get_antennas(line,3,na,ia,.false.,error)
          do j=1, na
            do_phase_mon(ia(j)) = .false.
          enddo
        ! EXTERNAL/INTERNAL
        elseif (nkey.eq.11) then
          do_phase_ext = .true.
        elseif (nkey.eq.12) then
          do_phase_ext = .false.
        ! FILE/NOFILE
        elseif (nkey.eq.13) then
          do_phase_nofile = .false.
        elseif (nkey.eq.14) then
          do_phase_nofile = .true.
        ! ANTENNA
        elseif (nkey.eq.3) then
          do_phase_antenna = .true.
        ! BASELINE
        elseif (nkey.eq.4) then
          do_phase_antenna = .false.
        ! DEGREES
        elseif (nkey.eq.5) then
          reset = .not.degrees
          degrees = .true.
          do k=1, n_x
            if (i_x(k).eq.xy_phase .and. reset) then
              if (sm_x1(k).eq.'F') then
                gu1_x(k) = 180./pi*gu1_x(k)
                gu2_x(k) = 180./pi*gu2_x(k)
              endif
            endif
          enddo
          do k=1, n_y
            if (i_y(k).eq.xy_phase .and. reset) then
              if (sm_y1(k).eq.'F') then
                gu1_y(k) = 180./pi*gu1_y(k)
                gu2_y(k) = 180./pi*gu2_y(k)
              endif
            endif
          enddo
        ! RADIANS
        elseif (nkey.eq.6) then
          reset = degrees
          degrees = .false.
          do k=1, n_x
            if (i_x(k).eq.xy_phase .and. reset) then
              if (sm_x1(k).eq.'F') then
                gu1_x(k) = pi/180.*gu1_x(k)
                gu2_x(k) = pi/180.*gu2_x(k)
              endif
            endif
          enddo
          do k=1, n_y
            if (i_y(k).eq.xy_phase .and. reset) then
              if (sm_y1(k).eq.'F') then
                gu1_y(k) = pi/180.*gu1_y(k)
                gu2_y(k) = pi/180.*gu2_y(k)
              endif
            endif
          enddo
        ! CONTINUOUS
        elseif (argum1(1:1).eq.'C') then
          reset = .not.continuous
          do k = 1, n_x
            if (i_x(k).eq.xy_phase .and. .not.continuous) then
              sm_x1(k) = '*'
              sm_x2(k) = '*'
            endif
          enddo
          do k=1, n_y
            if (i_y(k).eq.xy_phase .and. .not.continuous) then
              sm_y1(k) = '*'
              sm_y2(k) = '*'
            endif
          enddo
          continuous = .true.
        ! JUMPY
        elseif (nkey.eq.7) then
          reset = continuous
          do k=1, n_x
            if (i_x(k).eq.2 .and. continuous) then
              sm_x1(k) = 'F'
              sm_x2(k) = 'F'
              if (degrees) then
                gu1_x(k) = -180.
                gu2_x(k) = +180.
              else
                gu1_x(k) = -pi
                gu2_x(k) = +pi
              endif
            endif
          enddo
          do k=1, n_y
            if (i_y(k).eq.xy_phase .and. .not.continuous) then
              sm_y1(k) = 'F'
              sm_y2(k) = 'F'
              if (degrees) then
                gu1_y(k) = -180.
                gu2_y(k) = +180.
              else
                gu1_y(k) = -pi
                gu2_y(k) = +pi
              endif
            endif
          enddo
          continuous = .false.
        endif
      endif
    enddo
    call set_display(error)
    call show_display('PHASES',.false.,error)
    call show_general('PHASE',.false.,line,error)
    change_display = .true.
    if (reset) call reset_phases
  !
  ! SET MODE
  elseif (nkey.eq.35) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET MODE',argum1,kw,nkey,voc10,mvoc10,error)
    if (error) return
    if (kw.eq.'LOWRES') then
      lowres = .true.
      call loose_data
      call free_memory
      bp_spectrum_memory = .false.
      bp_spectrum_memory_pol = .false.
    elseif (kw.eq.'FULL') then
      lowres = .false.
      call loose_data
      call free_memory
      bp_spectrum_memory = .false.
      bp_spectrum_memory_pol = .false.
    else
      if (kw.eq.'TIME') then
        call message (8,3,'CLIC_SET','This command is obsolete')
      elseif (kw.eq.'SPECTRAL') then
        call message (8,3,'CLIC_SET','This command is obsolete')
      endif
    endif
  !
  ! SET ATMOSPHERE
  elseif (nkey.eq.36) then
    argum1 = '*'
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET ATM',argum1,kw,nkey,voc11,mvoc11,error)
    if (error) return
    !
    ! ANTENNA INDEPENDENT ATM MODE (MODEL VERSION):
    if (kw.eq.'1985' .or. kw.eq.'2003' .or.kw.eq.'2009' .or.  &
        kw.eq.'OLD'  .or. kw.eq.'NEW') then
      if (atmmodel.ne.kw) then
        call atm_setup(kw,error)
        if (.not.error) then
          atmmodel = kw
          atm_interpolate = .false.
        endif
      endif
    endif
    !
    ! USE TABLE
    if (kw.eq.'INTERPOLATE') then
       call atm_read_table(error)
       if (error) then
         call message(6,3,'CLIC_SET','Cannot find atmospheric model table')
         atm_interpolate = .false.
         return
       else
         atm_interpolate = .true.
       endif
    endif
    !
    ! ANTENNA DEPENDENT ATM MODE:
    if (kw.eq.'AUTO' .or. kw.eq.'TREC') then
      call get_antennas(line,3,na,ia,.true.,error)
      if (error) return
      do j=1, na
        atm_mode(ia(j)) = kw(1:4)
      enddo
      if (kw.eq.'AUTO') then
        call sic_r4(line,0,3,atm_water,.false.,error)
        if (error) return
      elseif (kw.eq.'TREC') then
        call sic_r4(line,0,3,rarg,.false.,error)
        if (error) return
        do j=1, na
          atm_trec(ia(j)) = rarg
        enddo
      endif
    endif
    call show_general('ATMOSPHERE',.false.,line,error)
  !
  ! SET REFERENCE
  elseif (nkey.eq.37) then
    call sic_i4(line,0,2,iant,.true.,error)
    if (error) return
    if (iant.lt.1 .or. iant.gt.mnant) then
      call message(6,3,'CLIC_SET','INVALID ANTENNA NUMBER')
      error = .true.
      return
    endif
    ref_ant = iant
    call show_general('REFERENCE',.false.,line,error)
  !
  ! SET FREQUENCY {C|L01..} {U|L} NAME FREQ
  elseif (nkey.eq.38) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1(1:1).eq.'C') then
      call message(6,4,'CLIC_SET','Operation not allowed')
      error =  .true.
      return
      call sic_ke(line,0,3,argum2,nch,.true.,error)
      if (error) return
      if (argum2(1:1).eq.'U') then
        isb = 1
      elseif (argum2(1:1).eq.'L') then
        isb = 2
      else
        call message(6,3,'CLIC_SET','INVALID SIDEBAND')
        error = .true.
        return
      endif
      call sic_ch(line,0,4,c_line(isb),nch,.true.,error)
      call sic_r8(line,0,5,ff,.true.,error)
      if (error) return
      if (ff.le.0 .or. ff.gt.500000.) then
        call message(6,3,'CLIC_SET','INVALID FREQUENCY')
        error = .true.
        return
      endif
      c_freq(isb) = ff
    elseif (argum1(1:1).eq.'L') then
      call message(6,4,'CLIC_SET','Operation not allowed')
      error =  .true.
      return
      read (argum1(2:3),'(I2)',err=98) isbb
      if (isbb.le.0 .or. isbb.gt.mrlband) then
        call message(6,3,'CLIC_SET','INVALID SIDEBAND//ARGUM1')
        error = .true.
        return
      endif
      call sic_ke(line,0,3,argum2,nch,.true.,error)
      if (error) return
      if (argum2(1:1).eq.'U') then
        isb = 1
      elseif (argum2(1:1).eq.'L') then
        isb = 2
      else
        call message(6,3,'CLIC_SET','INVALID SIDEBAND')
        error = .true.
        return
      endif
      call sic_ch(line,0,4,l_line(isbb,isb),nch,.true.,error)
      call sic_r8(line,0,5,ff,.true.,error)
      if (error) return
      if (ff.le.0 .or. ff.gt.500000.) then
        call message(6,3,'CLIC_SET','INVALID FREQUENCY')
        error = .true.
        return
      endif
      l_freq(isbb,isb) = ff
    elseif (argum1(1:3).eq.'ALL') then
      call sic_ch(line,0,3,argum2,nch,.true.,error)
      call sic_r8(line,0,4,ff,.true.,error)
      if (error) return
      if (ff.le.0 .or. ff.gt.500000.) then
        call message(6,3,'CLIC_SET','INVALID FREQUENCY')
        error = .true.
        return
      endif
      do isb = 1, 2
        c_line(isb) = argum2
        c_freq(isb) = ff
        do isbb = 1, mrlband
          l_freq(isbb,isb) = ff
          l_line(isbb, isb) = argum2
        enddo
      enddo
    else
      call message(6,3,'CLIC_SET','INVALID ARGUMENT'//argum1)
      error = .true.
      return
    endif
    call show_general('FREQUENCY',.false.,line,error)
  !
  ! SET PROCEDURE
  elseif (nkey.eq.39) then
    nproc = narg-1
    if (nproc.gt.10) then
      call message(6,2,'SET PROCEDURE',   &
        'ONLY 10 PROCEDURES RETAINED')
      nproc = min(nproc,10)
    endif
    do i=1,nproc
      call sic_ke(line,0,i+1,argum1,nch,.true.,error)
      if (error) return
      call sic_ambigs('SET MODE',argum1,kw,nkey,voc12,mvoc12,   &
        error)
      if (error) then
        nproc = 0
        return
      endif
      if (nkey.le.mvoc12-2) then
        ssproc(i) = nkey+10
      elseif (kw.eq.'SKYDIP') then
        ssproc(i) = 6
      elseif (kw.eq.'*') then
        nproc = 0
      endif
    enddo
    call show_criteria('PROCEDURE',.false.,error)
    find_needed = .true.
  !
  ! SET CLOSURE B1 W1 ... **OBSOLETE**
  elseif (nkey.eq.40) then
    do i = 2, narg,2
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 98
      call sic_ambigs('SET',argum1,kw,nkey,cbas,mnbas,error)
      if (error) return
      call sic_r4(line,0,i+1,w_c(nkey),.true.,error)
      if (error) return
      w_c(nkey) = abs(w_c(nkey))
    enddo
  !
  ! SET AMPLITUDE ABS:REL SCALED|UNSCALED  KELVIN|KANSKY|RAW ANTENNA|BASELINE
  elseif (nkey.eq.41) then
    do i=2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) return
      call sic_ambigs('SET ',argum1,keywor2,nkey,vamp,mamp,error)
      if (error) return
      ! ABSOLUTE
      if (nkey.eq.1) then
        do_amplitude = .false.
        do_scale = .false.
        do_flux = .false.      ! DEFAULT
        do_raw = .false.
        change_display = .true.
      ! RELATIVE
      elseif (nkey.eq.2) then  ! RELATIVE AMPLITUDE CAL.
        do_amplitude = .true.
        do_flux = .true.       ! DEFAULT
        do_raw = .false.
        change_display = .true.
      ! SCALED
      elseif (nkey.eq.3) then
        do_scale = .true.
        do_raw = .false.
        change_display = .true.
      ! UN SCALED
      elseif (nkey.eq.4) then
        do_scale = .false.
        do_raw = .false.
        change_display = .true.
      ! KELVIN
      elseif (nkey.eq.5) then
        do_flux = .false.
        do_raw = .false.
        change_display = .true.
      ! JANSKY
      elseif (nkey.eq.6) then
        do_flux = .true.
        do_raw = .false.
        change_display = .true.
      ! ANTENNA
      elseif (nkey.eq.7) then
        do_amplitude_antenna = .true.
      ! BASELINE
      elseif (nkey.eq.8) then
        do_amplitude_antenna = .false.
      ! RAW
      elseif (nkey.eq.9) then
        do_raw = .true.
        do_scale = .false.
        do_flux = .false.
        do_amplitude = .false.
      ! SIZED
      elseif (nkey.eq.10) then
        do_size = .true.
        do_scale = .false.
        change_display = .true.
      ! UNSIZED
      elseif (nkey.eq.11) then
        do_size = .false.
        change_display = .true.
      ! SPIDX
      elseif (nkey.eq.12) then
        do_spidx = .true.
        change_display = .true.
      ! NOSPIDX
      elseif (nkey.eq.13) then
        do_spidx = .false.
        change_display = .true.
      endif
    enddo
    call set_display(error)
    change_display = .true.
    call show_general('AMPLITUDE',.false.,line,error)
  !
  ! SET RF_PASSBAND ON|OFF|FREQ|CHANNEL|MEMORY|FILE|ANTENNA|BASELINE
  elseif (nkey.eq.42) then
    do i=2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) return
      call sic_ambigs(' ',argum1,keywor2,nkey,vrf,mrf,more)
      if (more) then
         call sic_i4(line,0,i,bp_file_id,.true.,error)
         if (error) return
         nkey = 0
         bp_spectrum_memory = .false.
      endif
      ! OFF
      if (nkey.eq.9) then
        do_pass_spectrum = .true.
      elseif (nkey.eq.7) then
        do_pass = .false.
      ! ON
      elseif (nkey.eq.8) then
        do_pass = .true.
      ! CHANNEL
      elseif (nkey.eq.3) then
        do_pass_spectrum = .false.
        do_pass_freq = .false.
      ! FREQUENCY
      elseif (nkey.eq.4) then
        do_pass_spectrum = .false.
        do_pass_freq = .true.
      ! FILE
      elseif (nkey.eq.5) then
        do_pass_memory = .false.
      ! MEMORY
      elseif (nkey.eq.6) then
        do_pass_memory = .true.
        if (do_pass_spectrum) bp_spectrum_memory = .false.
      ! BASELINE
      elseif (nkey.eq.2) then
        do_pass_antenna = .false.
      ! ANTENNA
      elseif (nkey.eq.1) then
        do_pass_antenna = .true.
      elseif (nkey.eq.0) then
      else
        goto 98
      endif
    enddo
    if (bp_file_id.ne.0.and.(.not.do_pass_spectrum.or.do_pass_memory)) then
      call message(8,1,'CLIC_SET',&
                   'Not in SPECTRUM file mode. Resetting solution number')
      bp_file_id = 0
    endif
    change_display = .true.
    call show_general('RF_PASSBAND',.false.,line,error)
  !
  ! SET FLUX NAME [VALUE] [FREQUENCY] ([DATE]?) [/RESET]
  elseif (nkey.eq.43) then
    if (sic_present(5,0)) then
      call sic_ke(line,0,2,argum1,nch,.true.,error)
      ! WITH /RESET THE 3RD AND 4TH  ARGUMENTS ARE OPTIONALLY FREQUENCY AND DATE
      freq = -1e6
      call sic_r4(line,0,3,freq,.false.,error)
      if (error) return
      dayf = -1000000
      argum3 = '*'
      call sic_ke(line,0,4,argum3,nch,.false.,error)
      if (argum3.ne.'*') call cdate(argum3,dayf,error)
      if (error) return
      frmhz = freq*1000.
      if ((argum1.eq.'ALL').or.(argum1.eq.'all')) then
        n_flux = 0
      else
        do i=1, n_flux
          if (argum1.eq.c_flux(i)) then
            fr_ok = (frmhz.le.0)   &
              .or. (abs(frmhz-freq_flux(i)).lt.frtol)
            d_ok = (dayf.lt.-100000)   &
              .or. (abs(date_flux(i)-dayf).le.1)
            if (fr_ok .and. d_ok) c_flux(i) = '~'
          endif
        enddo
      endif
    else
      call sic_ke(line,0,2,argum1,nch,.true.,error)
      call sic_ke(line,0,3,argum2,nch,.true.,error)
      if (error) return
      if (argum2.eq.'*') then
        rarg = 0
      else
        call sic_r4(line,0,3,rarg,.true.,error)
        if (error) return
      endif
      freq = -1e6
      call sic_r4(line,0,4,freq,.false.,error)
      if (error) return
      dayf = -1000000
      argum3 = '*'
      call sic_ke(line,0,5,argum3,nch,.false.,error)
      if (argum3.ne.'*') call cdate(argum3,dayf,error)
      if (error) return
      frmhz = freq*1000.
      if = 0
      do i=1, n_flux
        if (argum1.eq.c_flux(i)) then
          ! WAS FIXED:
          if (f_flux(i).and.(rarg.le.0)) then
            c_flux(i) = '~'
            if = -1
          elseif (rarg.le.0) then
            if = -1
          else
            fr_ok = (freq_flux(i).le.0)   &
              .or. (abs(frmhz-freq_flux(i)).lt.frtol)
            d_ok = (date_flux(i).lt.-100000)   &
              .or. (abs(date_flux(i)-dayf).le.1)
            if (fr_ok .and. d_ok) then
              if (if.eq.0) then
                if = i
              else
                c_flux(i) = '~'
              endif
            endif
          endif
        endif
      enddo
      ! NEW ONE
      if (if.eq.0) then
        if (n_flux.lt.m_flux) then
          n_flux = n_flux+1
          c_flux(n_flux) = argum1
          f_flux(n_flux) = rarg.ne.0
          freq_flux(n_flux) = frmhz
          date_flux(n_flux) = dayf ! THE OBSERVATION DATE
          flux(n_flux) = rarg
        else
          call message(8,3,'CLIC_SET','TOO MANY FLUXES')
          error = .true.
        endif
      elseif (rarg.ne.0) then
        f_flux(if) = rarg.ne.0
        date_flux(if) = dayf   ! THE OBSERVATION DATE
        freq_flux(if) = max(freq_flux(if),frmhz)
        if (rarg.ne.0) flux(if) = rarg
      endif
    endif
    call sort_fluxes(error)
  !
  ! SET WATER *|VALUE
  elseif (nkey.eq.44) then
    argum1 = '*'
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1(1:1).eq.'*') then
      atm_water = -100.
    else
      call sic_r4(line,0,2,rarg,.true.,error)
      if (error) return
      if (rarg.gt.0.) then
        atm_water = rarg
      else
        call message(6,3,'CLIC_SET','POSITIVE VALUE, PLEASE')
        error = .true.
        return
      endif
    endif
  !
  ! SET TREC *|VALUE /ANTENNA I|ALL
  elseif (nkey.eq.45) then
    argum1 = '*'
    call get_antennas(line,3,na,ia,.true.,error)
    if (error) return
    iant = ia(1)
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1(1:1).eq.'*') then
      do j=1, na
        atm_trec(ia(j)) = -100.
      enddo
    else
      call sic_r4(line,0,2,rarg,.true.,error)
      if (error) return
      if (rarg.gt.0.) then
        do j=1, na
          atm_trec(ia(j)) = rarg
        enddo
      else
        call message(6,3,'CLIC_SET','POSITIVE VALUE, PLEASE')
        error = .true.
        return
      endif
    endif
  !
  ! SET VIRTUAL **OBSOLETE**
  elseif (nkey.eq.46) then
    call message (8,2,'CLIC_SET',   &
      'CLIC\SET VIRTUAL IS NOW OBSOLETE')
  !
  ! SET SELECTION CO|LI  UP|LO|AV|AU SUBBAND-CODE FR FREQ /WINDOW
  elseif (nkey.eq.47) then
    i = 2
    k = 0
    to = .false.
    and = .false.
    l = 0
    widex_select = .false.
    do while (i.le.narg)
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 479
      call sic_ambigs(' ',argum1,kw,nkey,vsub2, mvsub2, more)
      if (more) then
        lc = len_trim(argum1)  ! ou la sortie de sic_ch
        if (lc.le.1) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (argum1(1:1).eq."C") then
          factor = 0
        elseif (argum1(1:1).eq."L") then
          factor = 1
        else
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (lc.gt.4) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        read(argum1(2:lc),'(I3)',iostat=ier) nkey
        if (ier.ne.0)  then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (nkey.lt.1 .or. nkey.gt.mbands) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif  
        nkey = nkey + factor * mbands
      endif
      i = i+1
      if (kw.eq.'CONTINUUM') then
        cont_select = .true.
      elseif (kw.eq.'LINE') then
        cont_select = .false.
      elseif (kw.eq.'UPPER' .or. kw.eq.'USB') then
        isideb_out = 1
      elseif (kw.eq.'LOWER' .or. kw.eq.'LSB') then
        isideb_out = 2
      elseif (kw.eq.'AVERAGE' .or. kw.eq.'DSB') then
        isideb_out = 3
        if (.not. cont_select) then
          call message(6,2,'CLIC_SET',   &
            'AUTOMATIC SIDEBAND SELECTION IN LINE  MODE')
        endif
      elseif (kw.eq.'WIDEX') then
        widex_select = .true.
      elseif (kw.eq.'AUTO') then
        isideb_out = 4
      elseif (kw.eq.'SSB') then
        isideb_out = 5
      elseif (kw.eq.'TO') then
        to = .true.
      elseif (kw.eq.'AND') then
        and = .true.
      elseif (kw.eq.'ALL') then
        all_select = .true.
      elseif (kw.eq.'FREQUENCY') then
        call sic_r4(line,0,i,freq_out,.true.,error)
        if (error) goto 98
        i = i+1
      else
        all_select = .false.
        if (to) then
          if (k.le.0 .or. nkey.le.k .or. nkey.gt.2*mbands) then
            call message(6,3,'CLIC_SET',   &
              'INVALID SUBBAND RANGE')
            error = .true.
            return
          endif
          do j = k+1, nkey
            l = l + 1
            iso(l) = j
          enddo
          k = nkey
          to = .false.
        elseif (and) then
          if (k.eq.0) then
            call message(6,3,'CLIC_SET',   &
              'INVALID SUBBAND RANGE')
            goto 479
          endif
          l = l  + 1
          iso(l) = nkey
          k = nkey
          and = .false.
        elseif (k.eq.0) then
          l = 1
          k = nkey
          iso(1) =  nkey
        endif
      endif
    enddo
    if (l.ne.0) then
      if (widex_select.and..not.all_select) then
         call message(8,3,'SELECT','SET SELECTION WIDEX can only be &
                      used with SET SELECTION ALL')
         error = .true.
         return
      endif
      lsubb_out = l
      do i=1, l
          isubb_out(i) = iso(i)
      enddo
      if (widex_select.and..not.all_select) then
         call message(8,3,'SELECT','SET SELECTION WIDEX can only be &
                      used with SET SELECTION ALL')
         error = .true.
         return
      endif
    endif
    !
    if (sic_present(2,0)) then
      nn = sic_narg(2)
      if (nn.lt.2) go to 478
      if (nn/2 .gt. mwin_out) then
        call message(6,3,'CLIC_SET','TOO MANY WINDOWS')
        error = .true.
        return
      endif
      do i=1, nn/2
        argum1 = '*'
        call sic_ke(line,2,2*i-1,argum1,nch,.true.,error)
        if (error) goto 478
        if (argum1.eq.'*') then
          f1  = 0.0
        else
          call sic_r4(line,2,2*i-1,f1,.true.,error)
          if (error) goto 478
        endif
        argum1 = '*'
        call sic_ke(line,2,2*i,argum1,nch,.true.,error)
        if (error) goto 478
        if (argum1.eq.'*') then
          f2  = 1e30
        else
          call sic_r4(line,2,2*i,f2,.true.,error)
          if (error) goto 478
        endif
        if (f1.lt.f2) then
          ftry (1,i) = f1
          ftry (2,i) = f2
        else
          goto 478
        endif
      enddo
      nwin_out = nn/2
      do i=1, nwin_out
        window_out(1,i) = ftry(1,i)
        window_out(2,i) = ftry(2,i)
      enddo
    endif
    call show_general('SELECTION',.false.,line,error)
    goto 90
478 call message(6,3,'CLIC_SET','INVALID SELECTION WINDOW')
    error = .true.
    return
479 call message(6,3,'CLIC_SET','INVALID SELECTION '//argum1)
    error = .true.
    return
  !
  ! SET ANTENNAS [P..] A1 A2 A3
  elseif (nkey.eq.48) then
    if (narg.le.1) then
      call switch_antenna
      change_display = .true.
      call set_display(error)
      goto 90
    endif
    n_save = n_base
    do i=1, n_save
      i_save(i) = i_base(i)
    enddo
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) goto 489
    all_base = .false.
    phys_base = .false.
    nch = ichar(argum1(1:1))
    if (nch.gt.ichar('9')) then
      call sic_ambigs('SET ANTENNAS',argum1,kw,nkey,voc3,mvoc3,   &
        error)
      if (kw.eq.'PHYSICAL') then
        i1 = 3
        if ((narg.ge.i1).and.(r_nant.eq.0)) then
          call message(6,3,'SET ANTENNA PHYSICAL',   &
            'NO DATA READ')
          error=.true.
          goto 489
        endif
        phys_base = .true.
        do i=1,mnant
          phys_log(i) = mnant+1
        enddo
        do i=1,r_nant
          phys_log(r_kant(i)) = i
        enddo
      elseif (kw.eq.'LOGICAL') then
        i1 = 3
      elseif (kw.eq.'ALL') then
        all_base = .true.
        i_base(1) = -1
        call set_all_baselines
        i1 = 3
      endif
    else
      i1 = 2
    endif
    if (narg.lt.i1) goto 489
    error = .true.
    n_base = 0
    do i = i1, narg
      call sic_i4(line,0,i,iarg,.false.,error)
      if (iarg.gt.mnant .or. iarg.le.0) error = .true.
      if (error) goto 489
      if(phys_base) then
        n_base = n_base+1
        if (phys_log(iarg).le.r_nant) then
          i_base(n_base) = -phys_log(iarg)
        else
          ! IF PHYSICAL ANTENNA IS NOT IN THE
          ! ARRAY, THEN USE FAKE LOGICAL ANTENNA
          ! = PHYSICAL + 10 (TO BE DECODED BY
          ! SHOW COMMAND)
          i_base(n_base) = -(iarg+mnant)
        endif
      else
        n_base = n_base+1
        i_base(n_base) = -iarg
      endif
    enddo
    error = .false.
    if (n_base.gt.0) call set_display(error)
489 continue
    if (error) then
      n_base = n_save
      do i=1, n_save
        i_base(i) = i_save(i)
      enddo
    else
      if (n_base.gt.0) then
        call show_display('ANTENNAS',.false.,error)
        change_display = .true.
      endif
    endif
  !
  ! SET SPECTRUM ON|OFF
  elseif (nkey.eq.49) then
    call message (8,2,'CLIC_SET',   &
      'CLIC\SET SPECTRUM ON|OFF IS NOW OBSOLETE')
  !
  ! SET BINNING OFF:SISE [POS]
  elseif (nkey.eq.50) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1(1:3).eq.'OFF') then
      do_bin = .false.
      call message(4,1,'CLIC_SET','BINNING IS OFF')
    else
      call sic_r4(line,0,2,rarg,.true.,error)
      if (error) return
      bin_pos = 0.
      call sic_r4(line,0,3,bin_pos,.false.,error)
      if (error) return
      if (rarg.gt.0.) then
        do_bin = .true.
        bin_size = rarg
        write(ch,'(A,2(1X,1PG11.4))')   &
          'SIZE AND POSITION OF BINS ',bin_size, bin_pos
        call message(4,1,'CLIC_SET',ch(1:lenc(ch)))
      else
        call message(6,3,'CLIC_SET','INVALID BIN SIZE')
      endif
    endif
    change_display = .true.
  !
  ! SET WEIGHTS ON|OFF
  elseif (nkey.eq.51) then
    i = 2
    do while (sic_present(0,i))
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) return
      call sic_ambigs('SET WEIGHTS',argum1,kw,nkey,   &
        voc13,mvoc13,error)
      if (error) return
      if (kw.eq.'TSYS') then
        call sic_ke(line,0,i+1,argum1,nch,.true.,error)
        if (error) return
        call sic_ambigs('SET WEIGHTS TSYS',argum1,kw,nkey,   &
          voc14,mvoc14,error)
        if (error) return
        if (kw.eq.'ON') then
          weight_tsys =  .true.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY 1/TSYS**2 APPLIED IN TABLE')
        else
          weight_tsys = .false.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY 1/TSYS**2 NOT APPLIED IN TABLE')
        endif
      elseif (kw.eq.'INSTRUMENTAL' .or. kw.eq.'CALIBRATION') then
        call sic_ke(line,0,i+1,argum1,nch,.true.,error)
        if (error) return
        call sic_ambigs('SET WEIGHTS CALIBRATION',argum1,kw,nkey,   &
          voc14,mvoc14,error)
        if (error) return
        if (kw.eq.'ON') then
          weight_cal =  .true.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY CALIBRATION FUNCTION APPLIED IN TABLE')
        else
          weight_cal = .false.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY CALIBRATION FUNCTION '//   &
            'NOT APPLIED IN TABLE')
        endif
      elseif (kw.eq.'BANDPASS') then
        call sic_ke(line,0,i+1,argum1,nch,.true.,error)
        if (error) return
        call sic_ambigs('SET WEIGHTS BANDPASS',argum1,kw,nkey,   &
          voc14,mvoc14,error)
        if (error) return
        if (kw.eq.'ON') then
          weight_pass =  .true.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY BANDPASS FUNCTION APPLIED IN TABLE')
        else
          weight_pass = .false.
          call message(4,1,'CLIC_SET',   &
            'WEIGHTING BY BANDPASS FUNCTION '//   &
            'NOT APPLIED IN TABLE')
        endif
      endif
      i = i + 2
    enddo
  !
  ! SET DATA NP NB
  elseif (nkey.eq.52) then
    call sic_i4(line,0,2,i,.true.,error)
    if (error) return
    i = max(2,i)
    j = m_boxes
    call sic_i4(line,0,3,j,.false.,error)
    if (error) return
    j = max(1,min(j,mbox))
    call set_data(i,j)
    call set_display(error)
  !
  ! SET RECORD LIST
  elseif (nkey.eq.53) then
    ic = sic_start(0,2)
    if (line(ic:ic).eq.'*') then
      n_loop = 1
      rec_1(1) = 1
      rec_2(1) = 1000000
      rec_3(1) = 1
      goto 90
    endif
    iv = sic_end(0,narg)
    call sic_parse_listi4('CLIC_SET',line(ic:iv),list,m_loop,error)
    if (error) return
    n_loop = list%nlist
    rec_1(:) = list%i1(:)
    rec_2(:) = list%i2(:)
    rec_3(:) = list%i3(:)
  !
  ! SET CRITERIA /DEFAULT
  elseif (nkey.eq.54) then
    if (default) then
      call setdef(line, error)
    else
      argum1 = 'CRITERIA'
      goto 98
    endif
  !
  ! SET GAIN_IMAGE
  elseif (nkey.eq.55) then
    argum1 = '*'
    call get_antennas(line,3,na,ia,.true.,error)
    if (error) return
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1(1:1).eq.'*') then
      do j=1, na
        atm_gim(ia(j)) = -1.
      enddo
    else
      call sic_r4(line,0,2,rarg,.true.,error)
      if (error) return
      if (rarg.gt.0.) then
        do j=1, na
          atm_gim(ia(j)) = rarg
        enddo
      else
        call message(6,3,'CLIC_SET','POSITIVE VALUE, PLEASE')
        error = .true.
        return
      endif
    endif
  !
  ! SET DROP_FRACTION
  elseif (nkey.eq.56) then
    rarg = fdrop(1)
    call sic_r4(line,0,2,rarg,.true.,error)
    if (error) return
    if (rarg.ge.0. .and. rarg.le.0.5) then
      fdrop(1) = rarg
    else
      call message(6,3,'CLIC_SET','INVALID BANDWIDTH FRACTION')
      error = .true.
      return
    endif
    rarg = fdrop(2)
    call sic_r4(line,0,3,rarg,.true.,error)
    if (error) return
    if (rarg.ge.0. .and. rarg.le.0.5) then
      fdrop(2) = rarg
    else
      call message(6,3,'CLIC_SET','INVALID BANDWIDTH FRACTION')
      error = .true.
      return
    endif
  !
  ! SET GIBBS
  elseif (nkey.eq.57) then
    iarg = ngibbs
    call sic_i4(line,0,2,iarg,.true.,error)
    if (error) return
    ngibbs = min(32,max(0,iarg))
  !
  ! SET FIND
  elseif (nkey.eq.58) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET FIND',argum1,kw,nkey,voc15,mvoc15,error)
    if (error) return
    if (kw.eq.'UPDATE') then
      find_update = .true.
    else
      find_update = .false.
    endif
  !
  ! SET PLANET
  elseif (nkey.eq.59) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    call sic_ambigs('SET PLANET',argum1,kw,nkey,   &
      voc16,mvoc16,error)
    if (error) return
    if (kw.eq.'*') then
      planet_model = -1        ! DEFAULT ELLIPTIC FORMULA
      call message(6,2,'SET PLANET','DEFAULT BEHAVIOUR RESET')
    elseif (kw.eq.'PRIMARY') then
      planet_model = -2        ! CIRCULAR DISK + PRIMARY BEAM ATT.
    elseif (kw.eq.'FILE') then
      call sic_ch(line,0,3,ch,nch,.true.,error)
      if (error) return
      fich = ch
      call sic_parsef(ch,fich,' ','.SPL')
      call set_fluxspl(fich,planet_model)
    endif
  !
  ! SET RECEIVER
  elseif (nkey.eq.60) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1.eq.'*') then
      srecei = -1
    else
      call sic_i4(line,0,2,l,.true.,error)
      if (error) return
      srecei = l
    endif
  !
  ! SET UV_RANGE
  elseif (nkey.eq.61) then
    call sic_ke(line,0,2,argum1,nch,.true.,error)
    if (error) return
    if (argum1.eq.'*') then
      uv_range(1) = 0
    else
      call sic_r4(line,0,2,uv_range(1),.true.,error)
      if (error) return
      uv_range(1) = max(uv_range(1),0.)
    endif
    call sic_ke(line,0,3,argum1,nch,.true.,error)
    if (error) return
    if (argum1.eq.'*') then
      uv_range(2) = 1000.
    else
      call sic_r4(line,0,3,uv_range(2),.true.,error)
      if (error) return
      uv_range(2) = max(uv_range(1),uv_range(2))
    endif
    call show_display('UV_RANGE',.false.,error)
  !
  ! SET TIME_ORIGIN
  elseif (nkey.eq.62) then
    argum1 = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    call reset_time
    if (argum1.ne.'*') then
      call cdate(argum1,i,error)
      if (.not.error) dobs_0 = i
    endif
    if (dobs_0.eq.0) then
      call message(6,1,'SET','TIME ORIGIN IS AUTOMATIC')
    else
      call datec(i,argum1,error)
      call message(6,1,'SET','TIME ORIGIN IS '//argum1(1:12))
    endif
  !
  ! SET SORT RECEIVER|SCAN
  elseif (nkey.eq.63) then
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET SORT',argum1,kw,nkey,voc17,mvoc17,error)
    if (error) return
    if (kw.eq.'RECEIVER') then
      do_sort_receiver = .true.
      do_sort_num      = .false.
    elseif (kw.eq.'SCAN') then
      do_sort_receiver = .false.
      do_sort_num      = .false.
    elseif (kw.eq.'NUM') then
      do_sort_receiver = .false.
      do_sort_num      = .true.
    endif
  !
  ! SET COPY DATA|NODATA
  elseif (nkey.eq.64) then
    argum1 = '*'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET WRITE',argum1,kw,nkey,voc18,mvoc18,error)
    if (error) return
    if (kw.eq.'DATA') then
      do_write_data = .true.
    elseif (kw.eq.'NODATA') then
      do_write_data = .false.
    endif
  !
  ! SET GAIN_METHOD OLD|NEW
  elseif (nkey.eq.65) then
    argum1 = 'OLD'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET GAIN_METHOD',argum1,kw,nkey,voc19,   &
      mvoc19,error)
    if (error) return
    if (kw.eq.'OLD') then
      call message(6,1,'SET','OLD GAIN METHOD')
      new_ant_gain = .false.
    elseif (kw.eq.'NEW') then
      call message(6,1,'SET','NEW GAIN METHOD')
      new_ant_gain = .true.
    endif
  !
  ! SET POLARIZATION NO|HOR|VER|BOTH|EACH
  elseif (nkey.eq.66) then
    do_polar = 0
    argum1 = 'OFF'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET POLARIZATION',argum1,kw,nkey,voc20,   &
      mvoc20,error)
    if (error) return
    each_subb = .false.
    quar_subb = 0
    nbc_subb = 0
    widex_subb = 0
    narrow_input = 0
    widex_unit = 0
    each_polar = .false.
    if (kw.eq.'OFF') then
      call message(6,1,'CLIC_SET',   &
        'Selecting polarization mode: OFF')
      do_polar = 0
      all_subb = .true.
      pol_subb = 0
      call set_all_subbands
    elseif (kw.eq.'HORIZONTAL') then
      call message(6,1,'CLIC_SET',   &
        'Selecting polarization mode: HORIZONTAL')
      do_polar = 1
      all_subb = .false.
      pol_subb = 1
      call set_pol_subbands(error)
      if (error) return
    elseif (kw.eq.'VERTICAL') then
      call message(6,1,'CLIC_SET',   &
        'Selecting polarization mode: VERTICAL')
      do_polar = 2
      all_subb = .false.
      pol_subb = 2
      call set_pol_subbands(error)
      if (error) return
    elseif (kw.eq.'BOTH') then
      call message(6,1,'CLIC_SET',   &
        'Selecting polarization mode: BOTH')
      do_polar = 3
      all_subb =.true.
      pol_subb = 0
      call set_all_subbands
    elseif (kw.eq.'EACH') then
      call message(6,1,'CLIC_SET',   &
        'Selecting polarization mode: EACH')
      do_polar = 0
      all_subb =.false.
      each_polar =.true.
      pol_subb = 0
      call set_each_pol_subbands
    endif
    call select_pol
    call set_display(error)
    change_display = .true.
    call show_display('POLARIZATION',.false.,error)
  !
  ! SET FAKE OFF | (1|2|3|4)*8
  elseif (nkey.eq.67) then
    print *,'W-CLIC_SET, Highly experimental command, go away!'
    argum1='OFF'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (argum1.eq.'OFF') then
      print *,'I-CLIC_SET, SET FAKE OFF'
      fake_ngrx = .false.
      new_receivers = .false.
    else
      fake_fif1 = 6500
      first = .true.
      second = .false.
      do i=1,8
        fake_lpolmode(i) = 1
        call sic_i4(line,0,i+1,q,.true.,error)
        if (first) then
          first_quarter = q
          first = .false.
          print *,'I-CLIC_SET FIRST QUARTER: ',first_quarter
        else
          if (q.ne.first_quarter) then
            if (.not.second) then
              second_quarter = q
              second = .true.
              print *,'I-CLIC_SET SECOND QUARTER:',second_quarter
            else
              if (q.ne.second_quarter) then
                print *,'E-CLIC_SET, SELECT ONLY 2 QUARTERS'
                return
              endif
            endif
          endif
        endif
        if (q.eq.1) then
          fake_flo2(i) = 8100
          fake_flo2bis(i) = 4000
          fake_band2bis(i) = -1
          if (q.eq.first_quarter) then
            do j=1,mnant
              fake_lpolentry(j,i) = 1
            enddo
          else
            do j=1,mnant
              fake_lpolentry(j,i) = 2
            enddo
          endif
        elseif (q.eq.2) then
          fake_flo2(i) = 8100
          fake_flo2bis(i) = 2000
          fake_band2bis(i) = 1
          if (q.eq.first_quarter) then
            do j=1,mnant
              fake_lpolentry(j,i) = 1
            enddo
          else
            do j=1,mnant
              fake_lpolentry(j,i) = 2
            enddo
          endif
        elseif (q.eq.3) then
          fake_flo2(i) = 9900
          fake_flo2bis(i) = 4000
          fake_band2bis(i) = -1
          if (q.eq.first_quarter) then
            do j=1,mnant
              fake_lpolentry(j,i) = 2
            enddo
          else
            do j=1,mnant
              fake_lpolentry(j,i) = 1
            enddo
          endif
        elseif (q.eq.4) then
          fake_flo2(i) = 9900
          fake_flo2bis(i) = 2000
          fake_band2bis(i) = 1
          if (q.eq.first_quarter) then
            do j=1,mnant
              fake_lpolentry(j,i) = 2
            enddo
          else
            do j=1,mnant
              fake_lpolentry(j,i) = 1
            enddo
          endif
        else
          print *,'E-CLIC_SET, quarter must be 1-2-3-4'
          error = .true.
          return
        endif
      enddo
      print *,'FAKE_ENTRY',fake_lpolentry
      fake_ngrx = .true.
      new_receivers = .true.
      print *,'I-CLIC_SET, SET FAKE ON'
      do i=1,8
        write(*,'(A,I3,F6.0,F6.0)') '   Unit, FLO2, FLO2bis ',   &
          i, fake_flo2(i), fake_flo2bis(i)
      enddo
    endif
  !
  ! SET TOTAL_POWER RAW|PHYSICAL
  elseif (nkey.eq.68) then
    argum1 = 'PHYSICAL'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET TOTAL_POWER',argum1,kw,nkey,voc21,   &
      mvoc21,error)
    if (error) return
    if (kw.eq.'RAW') then
      call message(6,1,'SET','Raw total power scale')
      physical_power = .false.
    elseif (kw.eq.'PHYSICAL') then
      call message(6,1,'SET','Physical total power scale')
      physical_power = .true.
    endif
  !
  ! SET NARROW_INPUT 0|1|2
  elseif (nkey.eq.69) then
    call sic_delvariable('NARROW',.false.,error)
    error = .false.
    narrow_input = 0
    call sic_i4(line,0,2,iarg,.true.,error)
    if (error) return
    if ((iarg.lt.0).or.(iarg.gt.2)) then
      call message(6,3,'SET','Invalid narrow-band corr. input')
      error = .true.
      return
    endif
    narrow_input = iarg
    call sic_def_inte('NARROW',narrow_input,0,0,.false.,error)
    write (ch,'(A,I1)')   &
      'Selecting narrow-band correlator input ',narrow_input
    call message(6,1,'CLIC_SET',ch)
    each_subb = .false.
    pol_subb = 0
    quar_subb = 0
    widex_subb = 0
    each_polar = .false.
    if (narrow_input.ne.0) then
      nbc_subb = narrow_input
      all_subb = .false.
      call set_nbc_subbands(error)
      n_baseband = 1
      l_baseband(1) = 1
      i_baseband(1,1) = narrow_input
      bb_select = .true.
    else
      nbc_subb = 0
      all_subb = .true.
      bb_select = .false.
      call set_all_subbands
    endif
    if (error) return
    !
    ! Update polarization mode
    !         IF (NEW_RECEIVERS) THEN
    !            OLD_DO_POLAR = DO_POLAR
    !            CALL CHECK_POL_SUBBANDS(ERROR)
    !            KW = VOC20(DO_POLAR+1)
    !            N = LENC(KW)
    !            IF (DO_POLAR.NE.OLD_DO_POLAR) THEN
    !               CALL MESSAGE(6,1,'CLIC_SET',
    !     &         'Switching to polarization mode: '//KW(1:N))
    !            ELSE
    !               CALL MESSAGE(6,1,'CLIC_SET',
    !     &         'Polarization mode: '//KW(1:N))
    !            ENDIF
    !         ENDIF
    do_polar = 0
    widex_unit = 0
    call set_display(error)
    change_display = .true.
  !
  ! End
  elseif (nkey.eq.70) then
    call sic_delvariable('WIDEX',.false.,error)
    error = .false.
    widex_unit = 0
    call sic_i4(line,0,2,iarg,.true.,error)
    if (error) return
    if ((iarg.lt.0).or.(iarg.gt.4)) then
      call message(6,3,'SET','Invalid widex unit selection')
      error = .true.
      return
    endif
    widex_unit = iarg
    call sic_def_inte('WIDEX',widex_unit,0,0,.false.,error)
    write (ch,'(A,I1)')   &
      'Selecting widex unit number ',widex_unit
    n_baseband = 1
    l_baseband(1) = 1
    i_baseband(1,1) = 2+widex_unit
    bb_select = .true.
    call message(6,1,'CLIC_SET',ch)
    each_subb = .false.
    pol_subb = 0
    quar_subb = 0
    nbc_subb =0
    widex_subb = widex_unit
    all_subb = .false.
    call set_widex_subbands(error)
    if (error) return
    do_polar = 0
    narrow_input = 0
    each_polar = .false.
    call set_display(error)
    change_display = .true.
  !
  ! SET BBAND
  elseif (nkey.eq.71) then
    if (narg.le.1) goto 90
    bad = .true.
    n_savb = n_baseband
    do i=1, n_savb
      l_savb(i) = l_baseband(i)
      do j=1, l_savb(i)
        i_savb(j,i) = i_baseband(j,i)
      enddo
    enddo
    n_baseband = 0
    to = .false.
    and = .false.
    k = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      call sic_ambigs_sub('SET BBAND',argum1,kw,nkey,   &
        vsub, mvsub, error)
      if (nkey.eq.0) then
          call sic_i4(line,0,i,nkey,.true.,error)
          kw = 'NUMBER'
      endif
!      if (error) goto 279
      !
      if (kw.eq.'TO') then
        to = .true.
      elseif (kw.eq.'AND') then
        and = .true.
      !
      elseif (kw.eq.'ALL') then
        all_bband = .true.
        call set_all_bbands
      elseif (kw.eq.'EACH') then
        print *,"Work needed here"
        all_bband = .false.
      else
        all_bband = .false.
        all_subb = .false.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        if (to) then
          if (k.eq.0 .or. nkey.le.k) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
!            goto 279
          endif
          do j = k+1, nkey
            l_baseband(n_baseband) = l_baseband(n_baseband) + 1
            i_baseband(l_baseband(n_baseband),n_baseband) = j
          enddo
          k = nkey
          to = .false.
        elseif (and) then
          if (k.eq.0) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
!            goto 279
          endif
          l_baseband(n_baseband) = l_baseband(n_baseband) + 1
          i_baseband(l_baseband(n_baseband),n_baseband) = nkey
          k = nkey
          and = .false.
        else
          n_baseband = n_baseband+1
          k = nkey
          i_baseband(1,n_baseband) = nkey
          l_baseband(n_baseband) = 1
        endif
      endif
    enddo
    !
    error = .false.
    if (.not.sic_present(6,0)) call select_bb
    call set_bb_subbands(error)
    call set_subbands_if(error)
    !
    ! Now check/switch polarization mode
    !
    if ((new_receivers).and.(narrow_input.eq.0).and.(widex_unit.eq.0)) then
      old_do_polar = do_polar
      call check_pol_subbands(error)
      kw = voc20(do_polar+1)
      n = lenc(kw)
      if (do_polar.ne.old_do_polar) then
        call message(6,1,'CLIC_SET',   &
          'Switching to polarization mode: '//kw(1:n))
      else
        call message(6,1,'CLIC_SET',   &
          'Polarization mode: '//kw(1:n))
      endif
    endif
    !
    call set_display(error)
    bad = error
!279 continue
    if (bad) then
      n_subb = n_savb
      do i=1, n_savb
        l_subb(i) = l_savb(i)
        do j=1, l_savb(i)
          i_subb(j,i) = i_savb(j,i)
        enddo
      enddo
    else
      change_display = .true.
      call show_display('BBAND',.false.,error)
    endif
  !
  ! SET IF
  elseif (nkey.eq.72) then
    if (narg.le.1) goto 90
    bad = .true.
    n_savb = n_if
    do i=1, n_savb
      l_savb(i) = l_if(i)
      do j=1, l_savb(i)
        i_savb(j,i) = i_if(j,i)
      enddo
    enddo
    n_if = 0
    to = .false.
    and = .false.
    k = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      call sic_ambigs_sub('SET BBAND',argum1,kw,nkey,   &
        vsub, mvsub, error)
      if (nkey.eq.0) then
          call sic_i4(line,0,i,nkey,.true.,error)
          kw = 'NUMBER'
      endif
!      if (error) goto 279
      !
      if (kw.eq.'TO') then
        to = .true.
      elseif (kw.eq.'AND') then
        and = .true.
      !
      elseif (kw.eq.'ALL') then
        print *,"Work needed here"
!        n_subb = n_savb
!        all_subb = .true.
!        each_subb = .false.
!        pol_subb = 0
!        quar_subb = 0
!        nbc_subb = 0
!        widex_subb = 0
!        call set_all_subbands
      elseif (kw.eq.'EACH') then
        print *,"Work needed here"
!        n_subb = n_savb
!        all_subb = .false.
!        each_subb = .true.
!        pol_subb = 0
!        quar_subb = 0
!        nbc_subb = 0
!        widex_subb = 0
!        call set_each_subband
      else
        all_subb = .false.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        if (to) then
          if (k.eq.0 .or. nkey.le.k) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
!            goto 279
          endif
          do j = k+1, nkey
            l_if(n_if) = l_if(n_if) + 1
            i_if(l_if(n_if),n_if) = j
          enddo
          k = nkey
          to = .false.
        elseif (and) then
          if (k.eq.0) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
!            goto 279
          endif
          l_if(n_if) = l_if(n_if) + 1
          i_if(l_if(n_if),n_if) = nkey
          k = nkey
          and = .false.
        else
          n_if = n_if+1
          k = nkey
          i_if(1,n_if) = nkey
          l_if(n_if) = 1
        endif
      endif
    enddo
    !
    error = .false.
    if (.not.sic_present(6,0)) call select_if
    call set_if_subbands(error)
    call set_subbands_bb(error)
    !
    ! Now check/switch polarization mode
    !
    if ((new_receivers).and.(narrow_input.eq.0).and.(widex_unit.eq.0)) then
      old_do_polar = do_polar
      call check_pol_subbands(error)
      kw = voc20(do_polar+1)
      n = lenc(kw)
      if (do_polar.ne.old_do_polar) then
        call message(6,1,'CLIC_SET',   &
          'Switching to polarization mode: '//kw(1:n))
      else
        call message(6,1,'CLIC_SET',   &
          'Polarization mode: '//kw(1:n))
      endif
    endif
    !
    call set_display(error)
    bad = error
!279 continue
    if (bad) then
      n_if = n_savb
      do i=1, n_if
        l_if(i) = l_savb(i)
        do j=1, l_savb(i)
          i_if(j,i) = i_savb(j,i)
        enddo
      enddo
    else
      change_display = .true.
      call show_display('BBAND',.false.,error)
    endif
  !
  ! SET SPW
  elseif (nkey.eq.73) then
    if (narg.le.1) goto 90
    bad = .true.
    n_savb = n_subb
    do i=1, n_savb
      l_savb(i) = l_subb(i)
      do j=1, l_savb(i)
        i_savb(j,i) = i_subb(j,i)
      enddo
    enddo
    n_subb = 0
    to = .false.
    and = .false.
    k = 0
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.true.,error)
      if (error) goto 280
      call sic_ambigs(' ',argum1,kw,nkey,vsub, mvsub, more)
      if (more) then
        lc = len_trim(argum1)  ! ou la sortie de sic_ch
        if (lc.le.1) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (argum1(1:1).eq."C") then
          factor = 0
        elseif (argum1(1:1).eq."L") then
          factor = 1
        else
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (lc.gt.4) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        read(argum1(2:lc),'(I3)',iostat=ier) nkey
        if (ier.ne.0)  then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif
        if (nkey.lt.1 .or. nkey.gt.mbands) then
          call message(6,3,'CLIC_SET',   &
              'Invalid keyword '//argum1)
           error = .true.
           return
        endif  
        nkey = nkey + factor * mbands
      endif
      if (error) goto 280
      !
      if (kw.eq.'TO') then
        to = .true.
      elseif (kw.eq.'AND') then
        and = .true.
      !
      elseif (kw.eq.'ALL') then
        n_subb = n_savb
        all_subb = .true.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        call set_all_subbands
      elseif (kw.eq.'LINE') then
        n_subb = n_savb
        do j=1, n_subb
          if (i_subb(1,j).le.mbands) then
            do k=1, l_subb(j)
              i_subb(k,j) = i_subb(k,j)+mbands
            enddo
          endif
        enddo
      elseif (kw.eq.'CONTINUUM') then
        n_subb = n_savb
        do j=1, n_subb
          if (i_subb(1,j).gt.mbands) then
            do k=1, l_subb(j)
              i_subb(k,j) = i_subb(k,j)-mbands
            enddo
          endif
        enddo
      else
        all_subb = .false.
        each_subb = .false.
        pol_subb = 0
        quar_subb = 0
        nbc_subb = 0
        widex_subb = 0
        if (to) then
          if (k.eq.0 .or. nkey.le.k) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
            goto 280
          endif
          do j = k+1, nkey
            l_subb(n_subb) = l_subb(n_subb) + 1
            i_subb(l_subb(n_subb),n_subb) = j
          enddo
          k = nkey
          to = .false.
        elseif (and) then
          if (k.eq.0) then
            call message(6,3,'CLIC_SET',   &
              'Invalid subband range')
            goto 280
          endif
          l_subb(n_subb) = l_subb(n_subb) + 1
          i_subb(l_subb(n_subb),n_subb) = nkey
          k = nkey
          and = .false.
        else
          n_subb = n_subb+1
          k = nkey
          i_subb(1,n_subb) = nkey
          l_subb(n_subb) = 1
        endif
        !
        if (sic_present(2,0)) then
          argum1 = sw1(n_subb)
          call sic_ke(line,2,2*n_subb-1,argum1,nch,.true.,error)
          if (error) goto 280
          if (argum1.eq.'*') then
            sw1(n_subb) = '*'
            iw1(n_subb) = 0
          elseif (argum1.ne.'=') then
            call sic_i4(line,2,2*n_subb-1,iw1(n_subb),.true.,   &
              error)
            if (error) goto 280
            sw1(n_subb) = 'F'
          endif
          argum1 = sw2(n_subb)
          call sic_ke(line,2,2*n_subb,argum1,nch,.true.,error)
          if (error) goto 280
          if (argum1.eq.'*') then
            sw2(n_subb) = '*'
            iw2(n_subb) = 0
          elseif (argum1.ne.'=') then
            call sic_i4(line,2,2*n_subb, iw2(n_subb),.true.,   &
              error)
            if (error) goto 280
            sw2(n_subb) = 'F'
          endif
        else
          sw1(n_subb) = '*'
          iw1(n_subb) = 0
          sw2(n_subb) = '*'
          iw2(n_subb) = 0
        endif
      endif
    enddo
    !
    error = .false.
    if (.not.sic_present(6,0)) call select_subb
    call set_subbands_bb(error)
    call set_subbands_if(error)
    !
    ! Now check/switch polarization mode
    !
    if ((new_receivers).and.(narrow_input.eq.0).and.(widex_unit.eq.0)) then
      old_do_polar = do_polar
      call check_pol_subbands(error)
      kw = voc20(do_polar+1)
      n = lenc(kw)
      if (do_polar.ne.old_do_polar) then
        call message(6,1,'CLIC_SET',   &
          'Switching to polarization mode: '//kw(1:n))
      else
        call message(6,1,'CLIC_SET',   &
          'Polarization mode: '//kw(1:n))
      endif
    endif
    !
    ! Now check/switch narrow band correlator input
    !
    if ((new_receivers).and.(narrow_input.ne.0)) then
      old_narrow_input = narrow_input
      call check_nbc_subbands(error)
      if (narrow_input.ne.old_narrow_input) then
        write (ch, '(A,I1)')   &
          'Switching to narrow-band correlator input ',   &
          narrow_input
        call message(6,1,'CLIC_SET',ch)
      else
        write (ch, '(A,I1)')   &
          'Narrow-band correlator input ',   &
          narrow_input
        call message(6,1,'CLIC_SET',ch)
      endif
    endif
    !
    call set_display(error)
    bad = error
280 continue
    if (bad) then
      n_subb = n_savb
      do i=1, n_savb
        l_subb(i) = l_savb(i)
        do j=1, l_savb(i)
          i_subb(j,i) = i_savb(j,i)
        enddo
      enddo
    else
      change_display = .true.
      call show_display('SUBBAND',.false.,error)
    endif
  !
  ! SET SKIP YES|NO
  elseif (nkey.eq.74) then
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET SPIDX',argum1,kw,nkey,voc2,    &
      mvoc2,error)
    if (error) return
    if (kw.eq.'YES') then
      iskip = 1
    elseif(kw.eq.'NO') then
      iskip = 0
    endif
  !
  ! SET EXCLUDE
  elseif (nkey.eq.75) then
    nexcl = narg-1
    if (nexcl.gt.10) then
      call message(6,2,'SET EXCLUDE',   &
        'ONLY 10 PROCEDURES RETAINED')
      nexcl = min(nexcl,10)
    endif
    do i=1,nexcl
      call sic_ke(line,0,i+1,argum1,nch,.true.,error)
      if (error) return
      call sic_ambigs('SET EXCL',argum1,kw,nkey,voc12,mvoc12,   &
        error)
      if (error) then
        nexcl = 0
        return
      endif
      if (nkey.le.mvoc12-2) then
        ssexcl(i) = nkey+10
      elseif (kw.eq.'SKYDIP') then
        ssexcl(i) = 6
      elseif (kw.eq.'*') then
        nexcl = 0
      endif
    enddo
    find_needed = .true.
    call show_criteria('EXCLUDE',.false.,error)
  !
  ! SET SPIDX NAME [VALUE] [FREQUENCY] ([DATE]?) [/RESET]
  elseif (nkey.eq.76) then
    if (sic_present(5,0)) then
      call sic_ke(line,0,2,argum1,nch,.true.,error)
      ! WITH /RESET THE 3RD AND 4TH  ARGUMENTS ARE OPTIONALLY FREQUENCY AND DATE
      freq = -1e6
      call sic_r4(line,0,3,freq,.false.,error)
      if (error) return
      dayf = -1000000
      argum3 = '*'
      call sic_ke(line,0,4,argum3,nch,.false.,error)
      if (argum3.ne.'*') call cdate(argum3,dayf,error)
      if (error) return
      frmhz = freq*1000.
      if ((argum1.eq.'ALL').or.(argum1.eq.'all')) then
        n_spidx = 0
      else
        do i=1, n_spidx
          if (argum1.eq.c_spidx(i)) then
            fr_ok = (frmhz.le.0)   &
              .or. (abs(frmhz-freq_spidx(i)).lt.frtol)
            d_ok = (dayf.lt.-100000)   &
              .or. (abs(date_spidx(i)-dayf).le.1)
            if (fr_ok .and. d_ok) c_spidx(i) = '~'
          endif
        enddo
      endif
    else
      call sic_ke(line,0,2,argum1,nch,.true.,error)
      call sic_ke(line,0,3,argum2,nch,.true.,error)
      if (error) return
      if (argum2.eq.'*') then
        rarg = 0
      else
        call sic_r4(line,0,3,rarg,.true.,error)
        if (error) return
      endif
      freq = -1e6
      call sic_r4(line,0,4,freq,.false.,error)
      if (error) return
      dayf = -1000000
      argum3 = '*'
      call sic_ke(line,0,5,argum3,nch,.false.,error)
      if (argum3.ne.'*') call cdate(argum3,dayf,error)
      if (error) return
      frmhz = freq*1000.
      if = 0
      do i=1, n_spidx
        if (argum1.eq.c_spidx(i)) then
          ! WAS FIXED:
          if (f_spidx(i).and.(rarg.le.0)) then
            c_spidx(i) = '~'
            if = -1
          elseif (rarg.le.0) then
            if = -1
          else
            fr_ok = (freq_spidx(i).le.0)   &
              .or. (abs(frmhz-freq_spidx(i)).lt.frtol)
            d_ok = (date_spidx(i).lt.-100000)   &
              .or. (abs(date_spidx(i)-dayf).le.1)
            if (fr_ok .and. d_ok) then
              if (if.eq.0) then
                if = i
              else
                c_spidx(i) = '~'
              endif
            endif
          endif
        endif
      enddo
      ! NEW ONE
      if (if.eq.0) then
        if (n_spidx.lt.m_spidx) then
          n_spidx = n_spidx+1
          c_spidx(n_spidx) = argum1
          f_spidx(n_spidx) = rarg.lt.100
          freq_spidx(n_spidx) = frmhz
          date_spidx(n_spidx) = dayf ! THE OBSERVATION DATE
          spidx(n_spidx) = rarg
        else
          call message(8,3,'CLIC_SET','TOO MANY SPIDX')
          error = .true.
        endif
      elseif (rarg.ne.0) then
        f_spidx(if) = rarg.ne.0
        date_spidx(if) = dayf   ! THE OBSERVATION DATE
        freq_spidx(if) = max(freq_spidx(if),frmhz)
        if (rarg.ne.0) spidx(if) = rarg
      endif
    endif
    call sort_spidx(error)
  !
  ! SET VERSION 
  elseif (nkey.eq.77) then
    call sic_i4(line,0,2,iarg,.true.,error)
    if (error) return
    if (iarg.ge.2.and.iarg.le.version_last) then
      write (ch, '(A,I0)')   &
       'Setting observation version to ',iarg
      call message(6,1,'CLIC_SET',ch)
      write (ch, '(A,I0,A)')   &
       'Caution: use only if you know what you are doing'
      call message(6,2,'CLIC_SET',ch)
      version_current = iarg
    else
      write (ch, '(A,I0,A)')   &
       'Observation version ',iarg,' is not supported'
      call message(6,3,'CLIC_SET',ch)
      write (ch, '(A,I0)')   &
       'Latest supported version is ',version_last
      call message(6,3,'CLIC_SET',ch)
      error = .true.
      return
    endif
  !
  ! SET STOKES 
  elseif (nkey.eq.78) then
    if (narg.lt.2) return
    do i = 2, narg
      call sic_ke(line,0,i,argum1,nch,.false.,error)
      if (error) return
      if (argum1.eq.'RATIO') then
        nkey = code_stokes_ratio
      else
        call sic_ambigs_sub('SET STOKES',argum1,kw,nkey,voc23,   &
          mvoc23,error)
        if (nkey.ne.0) then
          if (kw.eq.'NATIVE') then
            do_leakage = .false.
            do_corrected = .false.
          elseif (kw.eq.'LEAKAGE') then
            do_leakage = .true.
            do_corrected = .false.
          elseif (kw.eq.'CORRECTED') then
            do_leakage = .false.
            do_corrected = .true.
          else
          endif
          return
        endif
        n_stokes = 0
        call sic_ambigs('SET STOKES',argum1,kw,nkey,gio_stokes_names,    &
          21,error)
        nkey = nkey-17
        if (nkey.eq.code_stokes_hh) do_polar = 1
        if (nkey.eq.code_stokes_vv) do_polar = 2
      endif
      n_stokes = n_stokes+1
      i_stokes(n_stokes) = nkey
    enddo
    if (error) return
    call show_display('STOKES',.false.,error)
  !
  ! SET TABLE
  elseif (nkey.eq.79) then
    argum1 = 'STANDARD'
    call sic_ke(line,0,2,argum1,nch,.false.,error)
    if (error) return
    call sic_ambigs('SET TABLE',argum1,kw,nkey,voc22,   &
      mvoc22,error)
    if (error) return
    if (kw.eq.'STANDARD') then
      call message(6,1,'SET','Standard TABLE command')
      table_prototype = .false.
    elseif (kw.eq.'PROTOTYPE') then
      call message(6,1,'SET','Prototype TABLE commad')
      table_prototype = .true.
    endif
  !
  ! SET FINDPOL
  elseif (nkey.eq.80) then
    argum1='*'
    snpol = 0
    do i=2,sic_narg(0)
      call sic_ke(line,0,i,argum1,nch,.false.,error)
      if (error) return
      snpol = snpol+1
      if (argum1(1:1).eq.'*') then 
        snpol = 0
        call show_criteria('FINDPOL',.false.,error)
        find_needed = .true.
        return
      else
        call sic_i4(line,0,i,spol(snpol),.false.,error)
        if (spol(snpol).lt.-1.or.spol(snpol).gt.mnpolscan) then
          call message(6,3,'FIND','Invalid polarisation subscan')
          error = .true.
          return
        endif
        if (spol(snpol).eq.-1) then
          snpol = 16
          do j=1,snpol
            spol(j)=j
          enddo
        endif
      endif
    enddo
    call show_criteria('FINDPOL',.false.,error)
    find_needed = .true.
  else
    call message(6,3,'SET',keywor//' not implemented')
    error = .true.
    return
  endif
  !
90 nl = index(line,' ')+1
  nla = nl+lenc(argum)
  line(nl:) = keywor(1:lenc(keywor))//line(nla:)
  return
  !
98 call message(6,3,'CLIC_SET','Error decoding '//argum1)
  error = .true.
  return
end subroutine clic_set
!
subroutine get_antennas(line,iopt,nant,iant,needed,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Get list of antennas from /ANTENNA option
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  integer :: iopt                   !
  integer :: nant                   !
  integer :: iant(*)                !
  logical :: needed                 !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  character(len=4) :: arg
  integer :: i, nch
  !-----------------------------------------------------------------------
  do i=1, mnant
    iant(i) = 0
  enddo
  ! error in needed and /ANTENNA option not present
  if (.not.sic_present(iopt,1)) then
    if (needed) then
      call message(6,3,'GET_ANTENNAS','Give /ANTENNA i, please')
      error = .true.
      return
    endif
  endif
  ! All antennas (default if not needed)
  arg = 'ALL'
  call sic_ke(line,iopt,1,arg,nch,needed,error)
  if (arg.eq.'ALL') then
    nant = mnant
    do i=1, mnant
      iant(i) = i
    enddo
    return
  endif
  ! Get antenna list
  nant = sic_narg(iopt)
  do i=1, nant
    call sic_i4(line,iopt,i,iant(i),.true.,error)
    if (iant(i).gt.mnant .or. iant(i).le.0) then
      call message(6,3,'GET_ANTENNAS','Invalid antenna number')
    endif
  enddo
  return
end subroutine get_antennas
