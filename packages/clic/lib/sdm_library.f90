!
! ALMA - Atacama Large Millimeter Array
! (c) European Southern Observatory, 2002
! (c) Associated Universities Inc., 2002
! Copyright by ESO (in the framework of the ALMA collaboration),
! Copyright by AUI (in the framework of the ALMA collaboration),
! All rights reserved.
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY, without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place, Suite 330, Boston,
! MA 02111-1307  USA
!
! File sdm_library.f90
!
!----------------------------------------------------------------------------
subroutine charcut(string)
! find unused end part of string and replace with nulls.
!
  character *(*) string
  integer i, j, k, l
  k = 1
  l = len(string)
  do i=1, l
    j = ichar(string(i:i))
    if (j.ge.ichar('!').and.j.le.ichar('~')) k=i+1
  enddo
  k = min(k,l)
  string(k:l) = char(0)
end subroutine charcut
!
! ===========================================================================
!
! SBSummary Table:
!
! ===========================================================================
!
subroutine addSBSummaryRow(key, row, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryRow) :: row
  type(SBSummaryKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSBSummaryRow, i, j
  integer :: sbSummaryUIDDim(2)
  integer :: projectUIDDim(2)
  integer :: obsUnitSetIdDim(2)
  integer, parameter :: centerDirectionRank = 1
  integer :: centerDirectionDim(2,centerDirectionRank)
  integer, parameter :: observingModeRank = 1
  integer :: observingModeDim(2,observingModeRank+1)
  integer, parameter :: scienceGoalRank = 1
  integer :: scienceGoalDim(2,scienceGoalRank+1)
  integer, parameter :: weatherConstraintRank = 1
  integer :: weatherConstraintDim(2,weatherConstraintRank+1)
  ! ----------------
  ! Deal with dimensions
  sbSummaryUIDDim = len(row%sbSummaryUID)
  call charcut(row%sbSummaryUID)
  projectUIDDim = len(row%projectUID)
  call charcut(row%projectUID)
  obsUnitSetIdDim = len(row%obsUnitSetId)
  call charcut(row%obsUnitSetId)
  if (.not.allocated(row%centerDirection)) then
    call sdmMessage(8,3,'SBSummaryTable','row%centerDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, centerDirectionRank
    centerDirectionDim(:,i) = size(row%centerDirection,i)
  enddo
  if (.not.allocated(row%observingMode)) then
    call sdmMessage(8,3,'SBSummaryTable','row%observingMode not allocated.')
    error = .true.
    return
  endif
  observingModeDim(:,1) = len(row%observingMode)
  do i=1, observingModeRank
    observingModeDim(:,i+1) = size(row%observingMode,i)
  enddo
  do i=1, observingModeDim(1,2)
    call charcut(row%observingMode(i))
  enddo
  if (.not.allocated(row%scienceGoal)) then
    call sdmMessage(8,3,'SBSummaryTable','row%scienceGoal not allocated.')
    error = .true.
    return
  endif
  scienceGoalDim(:,1) = len(row%scienceGoal)
  do i=1, scienceGoalRank
    scienceGoalDim(:,i+1) = size(row%scienceGoal,i)
  enddo
  do i=1, scienceGoalDim(1,2)
    call charcut(row%scienceGoal(i))
  enddo
  if (.not.allocated(row%weatherConstraint)) then
    call sdmMessage(8,3,'SBSummaryTable','row%weatherConstraint not allocated.')
    error = .true.
    return
  endif
  weatherConstraintDim(:,1) = len(row%weatherConstraint)
  do i=1, weatherConstraintRank
    weatherConstraintDim(:,i+1) = size(row%weatherConstraint,i)
  enddo
  do i=1, weatherConstraintDim(1,2)
    call charcut(row%weatherConstraint(i))
  enddo
  !
  ireturn = sdm_addSBSummaryRow(row%sbSummaryUID, sbSummaryUIDDim, row%projectUID, projectUIDDim, row%obsUnitSetId,&
      & obsUnitSetIdDim, row%frequency, row%frequencyBand, row%sbType, row%sbDuration, row%centerDirection,&
      & centerDirectionDim, row%numObservingMode, row%observingMode, observingModeDim, row%numberRepeats,&
      & row%numScienceGoal, row%scienceGoal, scienceGoalDim, row%numWeatherConstraint, row%weatherConstraint,&
      & weatherConstraintDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_addSBSummaryRow', ireturn)
    error = .true.
  else
    key%sBSummaryId = ireturn
  endif
end subroutine addSBSummaryRow
!
! ---------------------------------------------------------------------------
!
subroutine getSBSummaryRow(key, row, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryRow) :: row
  type(SBSummaryKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSBSummaryRow, i
  integer :: sbSummaryUIDDim(2)
  integer :: projectUIDDim(2)
  integer :: obsUnitSetIdDim(2)
  integer, parameter :: centerDirectionRank = 1
  integer :: centerDirectionDim(2,centerDirectionRank)
  integer, parameter :: observingModeRank = 1
  integer :: observingModeDim(2,observingModeRank+1)
  integer, parameter :: scienceGoalRank = 1
  integer :: scienceGoalDim(2,scienceGoalRank+1)
  integer, parameter :: weatherConstraintRank = 1
  integer :: weatherConstraintDim(2,weatherConstraintRank+1)
  ! Deal with dimensions
  sbSummaryUIDDim = len(row%sbSummaryUID)
  call charcut(row%sbSummaryUID)
  projectUIDDim = len(row%projectUID)
  call charcut(row%projectUID)
  obsUnitSetIdDim = len(row%obsUnitSetId)
  call charcut(row%obsUnitSetId)
  if (.not.allocated(row%centerDirection)) then
    call sdmMessage(8,3,'SBSummaryTable','row%centerDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, centerDirectionRank
    centerDirectionDim(:,i) = size(row%centerDirection,i)
  enddo
  if (.not.allocated(row%observingMode)) then
    call sdmMessage(8,3,'SBSummaryTable','row%observingMode not allocated.')
    error = .true.
    return
  endif
  observingModeDim(:,1) = len(row%observingMode)
  do i=1, observingModeRank
    observingModeDim(:,i+1) = size(row%observingMode,i)
  enddo
  if (.not.allocated(row%scienceGoal)) then
    call sdmMessage(8,3,'SBSummaryTable','row%scienceGoal not allocated.')
    error = .true.
    return
  endif
  scienceGoalDim(:,1) = len(row%scienceGoal)
  do i=1, scienceGoalRank
    scienceGoalDim(:,i+1) = size(row%scienceGoal,i)
  enddo
  if (.not.allocated(row%weatherConstraint)) then
    call sdmMessage(8,3,'SBSummaryTable','row%weatherConstraint not allocated.')
    error = .true.
    return
  endif
  weatherConstraintDim(:,1) = len(row%weatherConstraint)
  do i=1, weatherConstraintRank
    weatherConstraintDim(:,i+1) = size(row%weatherConstraint,i)
  enddo
  !
  ireturn = sdm_getSBSummaryRow(key%sBSummaryId, row%sbSummaryUID, sbSummaryUIDDim, row%projectUID, projectUIDDim,&
      & row%obsUnitSetId, obsUnitSetIdDim, row%frequency, row%frequencyBand, row%sbType, row%sbDuration,&
      & row%centerDirection, centerDirectionDim, row%numObservingMode, row%observingMode, observingModeDim,&
      & row%numberRepeats, row%numScienceGoal, row%scienceGoal, scienceGoalDim, row%numWeatherConstraint,&
      & row%weatherConstraint, weatherConstraintDim)
  row%sbSummaryUID(sbSummaryUIDDim(1)+1:) = ''
  row%projectUID(projectUIDDim(1)+1:) = ''
  row%obsUnitSetId(obsUnitSetIdDim(1)+1:) = ''
  do i=1,observingModeDim(1,2)
    row%observingMode(i)(observingModeDim(1,1)+1:) = ''
  enddo
  do i=1,scienceGoalDim(1,2)
    row%scienceGoal(i)(scienceGoalDim(1,1)+1:) = ''
  enddo
  do i=1,weatherConstraintDim(1,2)
    row%weatherConstraint(i)(weatherConstraintDim(1,1)+1:) = ''
  enddo
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_getSBSummaryRow', ireturn)
    error = .true.
  endif
end subroutine getSBSummaryRow
!
! ---------------------------------------------------------------------------
!
subroutine getSBSummaryTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSBSummaryTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSBSummaryTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSBSummaryTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSBSummaryKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_SBSummary
  !
  integer :: tableSize
  logical :: error
  type(SBSummaryKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSBSummaryKeys, i
  integer, allocatable :: sBSummaryIdList(:)
  !
  allocate(sBSummaryIdList(tableSize))
  !
  ireturn = sdm_getSBSummaryKeys(sBSummaryIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_getSBSummaryKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%sBSummaryId = sBSummaryIdList(i)
  enddo
end subroutine getSBSummaryKeys
! ---------------------------------------------------------------------------
!
subroutine allocSBSummaryRow(row, error)
  use sdm_SBSummary
  type(SBSummaryRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'SBSummary'
  ! row%centerDirection allocation
  if (allocated(row%centerDirection)) then
    deallocate(row%centerDirection, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%observingMode allocation
  if (allocated(row%observingMode)) then
    deallocate(row%observingMode, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%scienceGoal allocation
  if (allocated(row%scienceGoal)) then
    deallocate(row%scienceGoal, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%weatherConstraint allocation
  if (allocated(row%weatherConstraint)) then
    deallocate(row%weatherConstraint, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%centerDirection(2), row%observingMode(row%numObservingMode), row%scienceGoal(row%numScienceGoal),&
      & row%weatherConstraint(row%numWeatherConstraint), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSBSummaryRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSBSummaryCenterDirectionCode(key, opt, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryKey) :: key
  type(SBSummaryOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSBSummaryCenterDirectionCode, i
  !! integer :: centerDirectionCode
  ! Deal with dimensions
  ireturn = sdm_addSBSummaryCenterDirectionCode(key%sBSummaryId, opt%centerDirectionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_addSBSummaryCenterDirectionCode', ireturn)
    error = .true.
  endif
end subroutine addSBSummaryCenterDirectionCode
!
! ---------------------------------------------------------------------------
!
subroutine getSBSummaryCenterDirectionCode(key, opt, present, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryKey) :: key
  type(SBSummaryOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSBSummaryCenterDirectionCode, i
  ! Deal with dimensions
  ireturn = sdm_getSBSummaryCenterDirectionCode(key%sBSummaryId, opt%centerDirectionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_getSBSummaryCenterDirectionCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSBSummaryCenterDirectionCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addSBSummaryCenterDirectionEquinox(key, opt, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryKey) :: key
  type(SBSummaryOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSBSummaryCenterDirectionEquinox, i
  !! integer*8 :: centerDirectionEquinox
  ! Deal with dimensions
  ireturn = sdm_addSBSummaryCenterDirectionEquinox(key%sBSummaryId, opt%centerDirectionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_addSBSummaryCenterDirectionEquinox', ireturn)
    error = .true.
  endif
end subroutine addSBSummaryCenterDirectionEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getSBSummaryCenterDirectionEquinox(key, opt, present, error)
  !
  use sdm_SBSummary
  !
  type(SBSummaryKey) :: key
  type(SBSummaryOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSBSummaryCenterDirectionEquinox, i
  ! Deal with dimensions
  ireturn = sdm_getSBSummaryCenterDirectionEquinox(key%sBSummaryId, opt%centerDirectionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SBSummaryTable','Error in sdm_getSBSummaryCenterDirectionEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSBSummaryCenterDirectionEquinox
!
!
! ===========================================================================
!
! ConfigDescription Table:
!
! ===========================================================================
!
subroutine addConfigDescriptionRow(key, row, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionRow) :: row
  type(ConfigDescriptionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addConfigDescriptionRow, i, j
  integer, parameter :: atmPhaseCorrectionRank = 1
  integer :: atmPhaseCorrectionDim(2,atmPhaseCorrectionRank)
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  integer, parameter :: feedIdRank = 1
  integer :: feedIdDim(2,feedIdRank)
  integer, parameter :: switchCycleIdRank = 1
  integer :: switchCycleIdDim(2,switchCycleIdRank)
  integer, parameter :: dataDescriptionIdRank = 1
  integer :: dataDescriptionIdDim(2,dataDescriptionIdRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%atmPhaseCorrection)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%atmPhaseCorrection not allocated.')
    error = .true.
    return
  endif
  do i=1, atmPhaseCorrectionRank
    atmPhaseCorrectionDim(:,i) = size(row%atmPhaseCorrection,i)
  enddo
  if (.not.allocated(row%antennaId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(row%antennaId,i)
  enddo
  if (.not.allocated(row%feedId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%feedId not allocated.')
    error = .true.
    return
  endif
  do i=1, feedIdRank
    feedIdDim(:,i) = size(row%feedId,i)
  enddo
  if (.not.allocated(row%switchCycleId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%switchCycleId not allocated.')
    error = .true.
    return
  endif
  do i=1, switchCycleIdRank
    switchCycleIdDim(:,i) = size(row%switchCycleId,i)
  enddo
  if (.not.allocated(row%dataDescriptionId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%dataDescriptionId not allocated.')
    error = .true.
    return
  endif
  do i=1, dataDescriptionIdRank
    dataDescriptionIdDim(:,i) = size(row%dataDescriptionId,i)
  enddo
  !
  ireturn = sdm_addConfigDescriptionRow(row%numAntenna, row%numDataDescription, row%numFeed, row%correlationMode,&
      & row%numAtmPhaseCorrection, row%atmPhaseCorrection, atmPhaseCorrectionDim, row%processorType, row%spectralType,&
      & row%antennaId, antennaIdDim, row%feedId, feedIdDim, row%switchCycleId, switchCycleIdDim, row%dataDescriptionId,&
      & dataDescriptionIdDim, row%processorId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_addConfigDescriptionRow', ireturn)
    error = .true.
  else
    key%configDescriptionId = ireturn
  endif
end subroutine addConfigDescriptionRow
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionRow(key, row, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionRow) :: row
  type(ConfigDescriptionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getConfigDescriptionRow, i
  integer, parameter :: atmPhaseCorrectionRank = 1
  integer :: atmPhaseCorrectionDim(2,atmPhaseCorrectionRank)
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  integer, parameter :: feedIdRank = 1
  integer :: feedIdDim(2,feedIdRank)
  integer, parameter :: switchCycleIdRank = 1
  integer :: switchCycleIdDim(2,switchCycleIdRank)
  integer, parameter :: dataDescriptionIdRank = 1
  integer :: dataDescriptionIdDim(2,dataDescriptionIdRank)
  ! Deal with dimensions
  if (.not.allocated(row%atmPhaseCorrection)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%atmPhaseCorrection not allocated.')
    error = .true.
    return
  endif
  do i=1, atmPhaseCorrectionRank
    atmPhaseCorrectionDim(:,i) = size(row%atmPhaseCorrection,i)
  enddo
  if (.not.allocated(row%antennaId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(row%antennaId,i)
  enddo
  if (.not.allocated(row%feedId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%feedId not allocated.')
    error = .true.
    return
  endif
  do i=1, feedIdRank
    feedIdDim(:,i) = size(row%feedId,i)
  enddo
  if (.not.allocated(row%switchCycleId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%switchCycleId not allocated.')
    error = .true.
    return
  endif
  do i=1, switchCycleIdRank
    switchCycleIdDim(:,i) = size(row%switchCycleId,i)
  enddo
  if (.not.allocated(row%dataDescriptionId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','row%dataDescriptionId not allocated.')
    error = .true.
    return
  endif
  do i=1, dataDescriptionIdRank
    dataDescriptionIdDim(:,i) = size(row%dataDescriptionId,i)
  enddo
  !
  ireturn = sdm_getConfigDescriptionRow(key%configDescriptionId, row%numAntenna, row%numDataDescription, row%numFeed,&
      & row%correlationMode, row%numAtmPhaseCorrection, row%atmPhaseCorrection, atmPhaseCorrectionDim,&
      & row%processorType, row%spectralType, row%antennaId, antennaIdDim, row%feedId, feedIdDim, row%switchCycleId,&
      & switchCycleIdDim, row%dataDescriptionId, dataDescriptionIdDim, row%processorId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionRow', ireturn)
    error = .true.
  endif
end subroutine getConfigDescriptionRow
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getConfigDescriptionTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getConfigDescriptionTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getConfigDescriptionTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_ConfigDescription
  !
  integer :: tableSize
  logical :: error
  type(ConfigDescriptionKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getConfigDescriptionKeys, i
  integer, allocatable :: configDescriptionIdList(:)
  !
  allocate(configDescriptionIdList(tableSize))
  !
  ireturn = sdm_getConfigDescriptionKeys(configDescriptionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%configDescriptionId = configDescriptionIdList(i)
  enddo
end subroutine getConfigDescriptionKeys
! ---------------------------------------------------------------------------
!
subroutine allocConfigDescriptionRow(row, error)
  use sdm_ConfigDescription
  type(ConfigDescriptionRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'ConfigDescription'
  ! row%atmPhaseCorrection allocation
  if (allocated(row%atmPhaseCorrection)) then
    deallocate(row%atmPhaseCorrection, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%antennaId allocation
  if (allocated(row%antennaId)) then
    deallocate(row%antennaId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%feedId allocation
  if (allocated(row%feedId)) then
    deallocate(row%feedId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%switchCycleId allocation
  if (allocated(row%switchCycleId)) then
    deallocate(row%switchCycleId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%dataDescriptionId allocation
  if (allocated(row%dataDescriptionId)) then
    deallocate(row%dataDescriptionId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%atmPhaseCorrection(row%numAtmPhaseCorrection), row%antennaId(row%numAntenna),&
      & row%feedId(row%numAntenna*row%numFeed), row%switchCycleId(row%numDataDescription),&
      & row%dataDescriptionId(row%numDataDescription), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocConfigDescriptionRow
!
! ---------------------------------------------------------------------------
!
subroutine allocConfigDescriptionOpt(row, opt, error)
  use sdm_ConfigDescription
  type(ConfigDescriptionRow) :: row
  type(ConfigDescriptionOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'ConfigDescription'
  ! opt%phasedArrayList allocation
  if (allocated(opt%phasedArrayList)) then
    deallocate(opt%phasedArrayList, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%assocNature allocation
  if (allocated(opt%assocNature)) then
    deallocate(opt%assocNature, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%assocConfigDescriptionId allocation
  if (allocated(opt%assocConfigDescriptionId)) then
    deallocate(opt%assocConfigDescriptionId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%phasedArrayList(row%numAntenna), opt%assocNature(opt%numAssocValues),&
      & opt%assocConfigDescriptionId(opt%numAssocValues), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocConfigDescriptionOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addConfigDescriptionPhasedArrayList(key, opt, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addConfigDescriptionPhasedArrayList, i
  !! integer, allocatable :: phasedArrayList(:)
  integer, parameter :: phasedArrayListRank = 1
  integer :: phasedArrayListDim(2,phasedArrayListRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phasedArrayList)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%phasedArrayList not allocated.')
    error = .true.
    return
  endif
  do i=1, phasedArrayListRank
    phasedArrayListDim(:,i) = size(opt%phasedArrayList,i)
  enddo
  ireturn = sdm_addConfigDescriptionPhasedArrayList(key%configDescriptionId, opt%phasedArrayList, phasedArrayListDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_addConfigDescriptionPhasedArrayList', ireturn)
    error = .true.
  endif
end subroutine addConfigDescriptionPhasedArrayList
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionPhasedArrayList(key, opt, present, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getConfigDescriptionPhasedArrayList, i
  integer, parameter :: phasedArrayListRank = 1
  integer :: phasedArrayListDim(2,phasedArrayListRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phasedArrayList)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%phasedArrayList not allocated.')
    error = .true.
    return
  endif
  do i=1, phasedArrayListRank
    phasedArrayListDim(:,i) = size(opt%phasedArrayList,i)
  enddo
  ireturn = sdm_getConfigDescriptionPhasedArrayList(key%configDescriptionId, opt%phasedArrayList, phasedArrayListDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionPhasedArrayList', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getConfigDescriptionPhasedArrayList
!
!
! ---------------------------------------------------------------------------
!
subroutine addConfigDescriptionNumAssocValues(key, opt, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addConfigDescriptionNumAssocValues, i
  !! integer :: numAssocValues
  ! Deal with dimensions
  ireturn = sdm_addConfigDescriptionNumAssocValues(key%configDescriptionId, opt%numAssocValues)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_addConfigDescriptionNumAssocValues', ireturn)
    error = .true.
  endif
end subroutine addConfigDescriptionNumAssocValues
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionNumAssocValues(key, opt, present, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getConfigDescriptionNumAssocValues, i
  ! Deal with dimensions
  ireturn = sdm_getConfigDescriptionNumAssocValues(key%configDescriptionId, opt%numAssocValues)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionNumAssocValues', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getConfigDescriptionNumAssocValues
!
!
! ---------------------------------------------------------------------------
!
subroutine addConfigDescriptionAssocNature(key, opt, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addConfigDescriptionAssocNature, i
  !! integer, allocatable :: assocNature(:)
  integer, parameter :: assocNatureRank = 1
  integer :: assocNatureDim(2,assocNatureRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocNature)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%assocNature not allocated.')
    error = .true.
    return
  endif
  do i=1, assocNatureRank
    assocNatureDim(:,i) = size(opt%assocNature,i)
  enddo
  ireturn = sdm_addConfigDescriptionAssocNature(key%configDescriptionId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_addConfigDescriptionAssocNature', ireturn)
    error = .true.
  endif
end subroutine addConfigDescriptionAssocNature
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionAssocNature(key, opt, present, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getConfigDescriptionAssocNature, i
  integer, parameter :: assocNatureRank = 1
  integer :: assocNatureDim(2,assocNatureRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocNature)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%assocNature not allocated.')
    error = .true.
    return
  endif
  do i=1, assocNatureRank
    assocNatureDim(:,i) = size(opt%assocNature,i)
  enddo
  ireturn = sdm_getConfigDescriptionAssocNature(key%configDescriptionId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionAssocNature', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getConfigDescriptionAssocNature
!
!
! ---------------------------------------------------------------------------
!
subroutine addConfigDescriptionAssocConfigDescriptionId(key, opt, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addConfigDescriptionAssocConfigDescriptionId, i
  !! integer, allocatable :: assocConfigDescriptionId(:)
  integer, parameter :: assocConfigDescriptionIdRank = 1
  integer :: assocConfigDescriptionIdDim(2,assocConfigDescriptionIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocConfigDescriptionId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%assocConfigDescriptionId not allocated.')
    error = .true.
    return
  endif
  do i=1, assocConfigDescriptionIdRank
    assocConfigDescriptionIdDim(:,i) = size(opt%assocConfigDescriptionId,i)
  enddo
  ireturn = sdm_addConfigDescriptionAssocConfigDescriptionId(key%configDescriptionId, opt%assocConfigDescriptionId,&
      & assocConfigDescriptionIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_addConfigDescriptionAssocConfigDescriptionId', ireturn)
    error = .true.
  endif
end subroutine addConfigDescriptionAssocConfigDescriptionId
!
! ---------------------------------------------------------------------------
!
subroutine getConfigDescriptionAssocConfigDescriptionId(key, opt, present, error)
  !
  use sdm_ConfigDescription
  !
  type(ConfigDescriptionKey) :: key
  type(ConfigDescriptionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getConfigDescriptionAssocConfigDescriptionId, i
  integer, parameter :: assocConfigDescriptionIdRank = 1
  integer :: assocConfigDescriptionIdDim(2,assocConfigDescriptionIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocConfigDescriptionId)) then
    call sdmMessage(8,3,'ConfigDescriptionTable','opt%assocConfigDescriptionId not allocated.')
    error = .true.
    return
  endif
  do i=1, assocConfigDescriptionIdRank
    assocConfigDescriptionIdDim(:,i) = size(opt%assocConfigDescriptionId,i)
  enddo
  ireturn = sdm_getConfigDescriptionAssocConfigDescriptionId(key%configDescriptionId, opt%assocConfigDescriptionId,&
      & assocConfigDescriptionIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ConfigDescriptionTable','Error in sdm_getConfigDescriptionAssocConfigDescriptionId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getConfigDescriptionAssocConfigDescriptionId
!
!
! ===========================================================================
!
! Field Table:
!
! ===========================================================================
!
subroutine addFieldRow(key, row, error)
  !
  use sdm_Field
  !
  type(FieldRow) :: row
  type(FieldKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFieldRow, i, j
  integer :: fieldNameDim(2)
  integer :: codeDim(2)
  integer, parameter :: delayDirRank = 2
  integer :: delayDirDim(2,delayDirRank)
  integer, parameter :: phaseDirRank = 2
  integer :: phaseDirDim(2,phaseDirRank)
  integer, parameter :: referenceDirRank = 2
  integer :: referenceDirDim(2,referenceDirRank)
  ! ----------------
  ! Deal with dimensions
  fieldNameDim = len(row%fieldName)
  call charcut(row%fieldName)
  codeDim = len(row%code)
  call charcut(row%code)
  if (.not.allocated(row%delayDir)) then
    call sdmMessage(8,3,'FieldTable','row%delayDir not allocated.')
    error = .true.
    return
  endif
  do i=1, delayDirRank
    delayDirDim(:,i) = size(row%delayDir,i)
  enddo
  if (.not.allocated(row%phaseDir)) then
    call sdmMessage(8,3,'FieldTable','row%phaseDir not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDirRank
    phaseDirDim(:,i) = size(row%phaseDir,i)
  enddo
  if (.not.allocated(row%referenceDir)) then
    call sdmMessage(8,3,'FieldTable','row%referenceDir not allocated.')
    error = .true.
    return
  endif
  do i=1, referenceDirRank
    referenceDirDim(:,i) = size(row%referenceDir,i)
  enddo
  !
  ireturn = sdm_addFieldRow(row%fieldName, fieldNameDim, row%code, codeDim, row%numPoly, row%delayDir, delayDirDim,&
      & row%phaseDir, phaseDirDim, row%referenceDir, referenceDirDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldRow', ireturn)
    error = .true.
  else
    key%fieldId = ireturn
  endif
end subroutine addFieldRow
!
! ---------------------------------------------------------------------------
!
subroutine getFieldRow(key, row, error)
  !
  use sdm_Field
  !
  type(FieldRow) :: row
  type(FieldKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFieldRow, i
  integer :: fieldNameDim(2)
  integer :: codeDim(2)
  integer, parameter :: delayDirRank = 2
  integer :: delayDirDim(2,delayDirRank)
  integer, parameter :: phaseDirRank = 2
  integer :: phaseDirDim(2,phaseDirRank)
  integer, parameter :: referenceDirRank = 2
  integer :: referenceDirDim(2,referenceDirRank)
  ! Deal with dimensions
  fieldNameDim = len(row%fieldName)
  call charcut(row%fieldName)
  codeDim = len(row%code)
  call charcut(row%code)
  if (.not.allocated(row%delayDir)) then
    call sdmMessage(8,3,'FieldTable','row%delayDir not allocated.')
    error = .true.
    return
  endif
  do i=1, delayDirRank
    delayDirDim(:,i) = size(row%delayDir,i)
  enddo
  if (.not.allocated(row%phaseDir)) then
    call sdmMessage(8,3,'FieldTable','row%phaseDir not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDirRank
    phaseDirDim(:,i) = size(row%phaseDir,i)
  enddo
  if (.not.allocated(row%referenceDir)) then
    call sdmMessage(8,3,'FieldTable','row%referenceDir not allocated.')
    error = .true.
    return
  endif
  do i=1, referenceDirRank
    referenceDirDim(:,i) = size(row%referenceDir,i)
  enddo
  !
  ireturn = sdm_getFieldRow(key%fieldId, row%fieldName, fieldNameDim, row%code, codeDim, row%numPoly, row%delayDir,&
      & delayDirDim, row%phaseDir, phaseDirDim, row%referenceDir, referenceDirDim)
  row%fieldName(fieldNameDim(1)+1:) = ''
  row%code(codeDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldRow', ireturn)
    error = .true.
  endif
end subroutine getFieldRow
!
! ---------------------------------------------------------------------------
!
subroutine getFieldTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFieldTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFieldTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFieldTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFieldKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Field
  !
  integer :: tableSize
  logical :: error
  type(FieldKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFieldKeys, i
  integer, allocatable :: fieldIdList(:)
  !
  allocate(fieldIdList(tableSize))
  !
  ireturn = sdm_getFieldKeys(fieldIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%fieldId = fieldIdList(i)
  enddo
end subroutine getFieldKeys
! ---------------------------------------------------------------------------
!
subroutine allocFieldRow(row, error)
  use sdm_Field
  type(FieldRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Field'
  ! row%delayDir allocation
  if (allocated(row%delayDir)) then
    deallocate(row%delayDir, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%phaseDir allocation
  if (allocated(row%phaseDir)) then
    deallocate(row%phaseDir, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%referenceDir allocation
  if (allocated(row%referenceDir)) then
    deallocate(row%referenceDir, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%delayDir(2, row%numPoly),  row%phaseDir(2, row%numPoly),  row%referenceDir(2, row%numPoly), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFieldRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addFieldTime(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldTime, i
  !! integer*8 :: time
  ! Deal with dimensions
  ireturn = sdm_addFieldTime(key%fieldId, opt%time)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldTime', ireturn)
    error = .true.
  endif
end subroutine addFieldTime
!
! ---------------------------------------------------------------------------
!
subroutine getFieldTime(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldTime, i
  ! Deal with dimensions
  ireturn = sdm_getFieldTime(key%fieldId, opt%time)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldTime', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldTime
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldDirectionCode(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldDirectionCode, i
  !! integer :: directionCode
  ! Deal with dimensions
  ireturn = sdm_addFieldDirectionCode(key%fieldId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldDirectionCode', ireturn)
    error = .true.
  endif
end subroutine addFieldDirectionCode
!
! ---------------------------------------------------------------------------
!
subroutine getFieldDirectionCode(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldDirectionCode, i
  ! Deal with dimensions
  ireturn = sdm_getFieldDirectionCode(key%fieldId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldDirectionCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldDirectionCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldDirectionEquinox(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldDirectionEquinox, i
  !! integer*8 :: directionEquinox
  ! Deal with dimensions
  ireturn = sdm_addFieldDirectionEquinox(key%fieldId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldDirectionEquinox', ireturn)
    error = .true.
  endif
end subroutine addFieldDirectionEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getFieldDirectionEquinox(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldDirectionEquinox, i
  ! Deal with dimensions
  ireturn = sdm_getFieldDirectionEquinox(key%fieldId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldDirectionEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldDirectionEquinox
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldAssocNature(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldAssocNature, i
  !! character*256 :: assocNature
  integer :: assocNatureDim(2)
  ! Deal with dimensions
  assocNatureDim = len(opt%assocNature)
  call charcut(opt%assocNature)
  ireturn = sdm_addFieldAssocNature(key%fieldId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldAssocNature', ireturn)
    error = .true.
  endif
end subroutine addFieldAssocNature
!
! ---------------------------------------------------------------------------
!
subroutine getFieldAssocNature(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldAssocNature, i
  integer :: assocNatureDim(2)
  ! Deal with dimensions
  assocNatureDim = len(opt%assocNature)
  ireturn = sdm_getFieldAssocNature(key%fieldId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldAssocNature', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldAssocNature
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldEphemerisId(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldEphemerisId, i
  !! integer :: ephemerisId
  ! Deal with dimensions
  ireturn = sdm_addFieldEphemerisId(key%fieldId, opt%ephemerisId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldEphemerisId', ireturn)
    error = .true.
  endif
end subroutine addFieldEphemerisId
!
! ---------------------------------------------------------------------------
!
subroutine getFieldEphemerisId(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldEphemerisId, i
  ! Deal with dimensions
  ireturn = sdm_getFieldEphemerisId(key%fieldId, opt%ephemerisId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldEphemerisId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldEphemerisId
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldSourceId(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldSourceId, i
  !! integer :: sourceId
  ! Deal with dimensions
  ireturn = sdm_addFieldSourceId(key%fieldId, opt%sourceId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldSourceId', ireturn)
    error = .true.
  endif
end subroutine addFieldSourceId
!
! ---------------------------------------------------------------------------
!
subroutine getFieldSourceId(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldSourceId, i
  ! Deal with dimensions
  ireturn = sdm_getFieldSourceId(key%fieldId, opt%sourceId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldSourceId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldSourceId
!
!
! ---------------------------------------------------------------------------
!
subroutine addFieldAssocFieldId(key, opt, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFieldAssocFieldId, i
  !! integer :: assocFieldId
  ! Deal with dimensions
  ireturn = sdm_addFieldAssocFieldId(key%fieldId, opt%assocFieldId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_addFieldAssocFieldId', ireturn)
    error = .true.
  endif
end subroutine addFieldAssocFieldId
!
! ---------------------------------------------------------------------------
!
subroutine getFieldAssocFieldId(key, opt, present, error)
  !
  use sdm_Field
  !
  type(FieldKey) :: key
  type(FieldOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFieldAssocFieldId, i
  ! Deal with dimensions
  ireturn = sdm_getFieldAssocFieldId(key%fieldId, opt%assocFieldId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FieldTable','Error in sdm_getFieldAssocFieldId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFieldAssocFieldId
!
!
! ===========================================================================
!
! State Table:
!
! ===========================================================================
!
subroutine addStateRow(key, row, error)
  !
  use sdm_State
  !
  type(StateRow) :: row
  type(StateKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addStateRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addStateRow(row%calDeviceName, row%sig, row%ref, row%onSky)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StateTable','Error in sdm_addStateRow', ireturn)
    error = .true.
  else
    key%stateId = ireturn
  endif
end subroutine addStateRow
!
! ---------------------------------------------------------------------------
!
subroutine getStateRow(key, row, error)
  !
  use sdm_State
  !
  type(StateRow) :: row
  type(StateKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getStateRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getStateRow(key%stateId, row%calDeviceName, row%sig, row%ref, row%onSky)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StateTable','Error in sdm_getStateRow', ireturn)
    error = .true.
  endif
end subroutine getStateRow
!
! ---------------------------------------------------------------------------
!
subroutine getStateTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getStateTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getStateTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getStateTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getStateKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_State
  !
  integer :: tableSize
  logical :: error
  type(StateKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getStateKeys, i
  integer, allocatable :: stateIdList(:)
  !
  allocate(stateIdList(tableSize))
  !
  ireturn = sdm_getStateKeys(stateIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StateTable','Error in sdm_getStateKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%stateId = stateIdList(i)
  enddo
end subroutine getStateKeys
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addStateWeight(key, opt, error)
  !
  use sdm_State
  !
  type(StateKey) :: key
  type(StateOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addStateWeight, i
  !! real :: weight
  ! Deal with dimensions
  ireturn = sdm_addStateWeight(key%stateId, opt%weight)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StateTable','Error in sdm_addStateWeight', ireturn)
    error = .true.
  endif
end subroutine addStateWeight
!
! ---------------------------------------------------------------------------
!
subroutine getStateWeight(key, opt, present, error)
  !
  use sdm_State
  !
  type(StateKey) :: key
  type(StateOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getStateWeight, i
  ! Deal with dimensions
  ireturn = sdm_getStateWeight(key%stateId, opt%weight)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StateTable','Error in sdm_getStateWeight', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getStateWeight
!
!
! ===========================================================================
!
! Antenna Table:
!
! ===========================================================================
!
subroutine addAntennaRow(key, row, error)
  !
  use sdm_Antenna
  !
  type(AntennaRow) :: row
  type(AntennaKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addAntennaRow, i, j
  integer :: nameDim(2)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  integer, parameter :: offsetRank = 1
  integer :: offsetDim(2,offsetRank)
  ! ----------------
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%position)) then
    call sdmMessage(8,3,'AntennaTable','row%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(row%position,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'AntennaTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  !
  ireturn = sdm_addAntennaRow(row%name, nameDim, row%antennaMake, row%antennaType, row%dishDiameter, row%position,&
      & positionDim, row%offset, offsetDim, row%time, row%stationId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AntennaTable','Error in sdm_addAntennaRow', ireturn)
    error = .true.
  else
    key%antennaId = ireturn
  endif
end subroutine addAntennaRow
!
! ---------------------------------------------------------------------------
!
subroutine getAntennaRow(key, row, error)
  !
  use sdm_Antenna
  !
  type(AntennaRow) :: row
  type(AntennaKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getAntennaRow, i
  integer :: nameDim(2)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  integer, parameter :: offsetRank = 1
  integer :: offsetDim(2,offsetRank)
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%position)) then
    call sdmMessage(8,3,'AntennaTable','row%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(row%position,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'AntennaTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  !
  ireturn = sdm_getAntennaRow(key%antennaId, row%name, nameDim, row%antennaMake, row%antennaType, row%dishDiameter,&
      & row%position, positionDim, row%offset, offsetDim, row%time, row%stationId)
  row%name(nameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AntennaTable','Error in sdm_getAntennaRow', ireturn)
    error = .true.
  endif
end subroutine getAntennaRow
!
! ---------------------------------------------------------------------------
!
subroutine getAntennaTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getAntennaTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getAntennaTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getAntennaTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getAntennaKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Antenna
  !
  integer :: tableSize
  logical :: error
  type(AntennaKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getAntennaKeys, i
  integer, allocatable :: antennaIdList(:)
  !
  allocate(antennaIdList(tableSize))
  !
  ireturn = sdm_getAntennaKeys(antennaIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AntennaTable','Error in sdm_getAntennaKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
  enddo
end subroutine getAntennaKeys
! ---------------------------------------------------------------------------
!
subroutine allocAntennaRow(row, error)
  use sdm_Antenna
  type(AntennaRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Antenna'
  ! row%position allocation
  if (allocated(row%position)) then
    deallocate(row%position, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%offset allocation
  if (allocated(row%offset)) then
    deallocate(row%offset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%position(3),  row%offset(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocAntennaRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addAntennaAssocAntennaId(key, opt, error)
  !
  use sdm_Antenna
  !
  type(AntennaKey) :: key
  type(AntennaOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAntennaAssocAntennaId, i
  !! integer :: assocAntennaId
  ! Deal with dimensions
  ireturn = sdm_addAntennaAssocAntennaId(key%antennaId, opt%assocAntennaId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AntennaTable','Error in sdm_addAntennaAssocAntennaId', ireturn)
    error = .true.
  endif
end subroutine addAntennaAssocAntennaId
!
! ---------------------------------------------------------------------------
!
subroutine getAntennaAssocAntennaId(key, opt, present, error)
  !
  use sdm_Antenna
  !
  type(AntennaKey) :: key
  type(AntennaOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAntennaAssocAntennaId, i
  ! Deal with dimensions
  ireturn = sdm_getAntennaAssocAntennaId(key%antennaId, opt%assocAntennaId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AntennaTable','Error in sdm_getAntennaAssocAntennaId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAntennaAssocAntennaId
!
!
! ===========================================================================
!
! DataDescription Table:
!
! ===========================================================================
!
subroutine addDataDescriptionRow(key, row, holoType, error)
  !
  use sdm_DataDescription
  !
  type(DataDescriptionRow) :: row
  type(DataDescriptionKey) :: key
  logical error
  logical holoType
  !
  integer :: ireturn, sdm_addDataDescriptionRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addDataDescriptionRow(row%polOrHoloId, row%spectralWindowId, holoType)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DataDescriptionTable','Error in sdm_addDataDescriptionRow', ireturn)
    error = .true.
  else
    key%dataDescriptionId = ireturn
  endif
end subroutine addDataDescriptionRow
!
! ---------------------------------------------------------------------------
!
subroutine getDataDescriptionRow(key, row, error)
  !
  use sdm_DataDescription
  !
  type(DataDescriptionRow) :: row
  type(DataDescriptionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getDataDescriptionRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getDataDescriptionRow(key%dataDescriptionId, row%polOrHoloId, row%spectralWindowId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DataDescriptionTable','Error in sdm_getDataDescriptionRow', ireturn)
    error = .true.
  endif
end subroutine getDataDescriptionRow
!
! ---------------------------------------------------------------------------
!
subroutine getDataDescriptionTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getDataDescriptionTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getDataDescriptionTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getDataDescriptionTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getDataDescriptionKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_DataDescription
  !
  integer :: tableSize
  logical :: error
  type(DataDescriptionKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getDataDescriptionKeys, i
  integer, allocatable :: dataDescriptionIdList(:)
  !
  allocate(dataDescriptionIdList(tableSize))
  !
  ireturn = sdm_getDataDescriptionKeys(dataDescriptionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DataDescriptionTable','Error in sdm_getDataDescriptionKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%dataDescriptionId = dataDescriptionIdList(i)
  enddo
end subroutine getDataDescriptionKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! SwitchCycle Table:
!
! ===========================================================================
!
subroutine addSwitchCycleRow(key, row, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleRow) :: row
  type(SwitchCycleKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSwitchCycleRow, i, j
  integer, parameter :: weightArrayRank = 1
  integer :: weightArrayDim(2,weightArrayRank)
  integer, parameter :: dirOffsetArrayRank = 2
  integer :: dirOffsetArrayDim(2,dirOffsetArrayRank)
  integer, parameter :: freqOffsetArrayRank = 1
  integer :: freqOffsetArrayDim(2,freqOffsetArrayRank)
  integer, parameter :: stepDurationArrayRank = 1
  integer :: stepDurationArrayDim(2,stepDurationArrayRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%weightArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%weightArray not allocated.')
    error = .true.
    return
  endif
  do i=1, weightArrayRank
    weightArrayDim(:,i) = size(row%weightArray,i)
  enddo
  if (.not.allocated(row%dirOffsetArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%dirOffsetArray not allocated.')
    error = .true.
    return
  endif
  do i=1, dirOffsetArrayRank
    dirOffsetArrayDim(:,i) = size(row%dirOffsetArray,i)
  enddo
  if (.not.allocated(row%freqOffsetArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%freqOffsetArray not allocated.')
    error = .true.
    return
  endif
  do i=1, freqOffsetArrayRank
    freqOffsetArrayDim(:,i) = size(row%freqOffsetArray,i)
  enddo
  if (.not.allocated(row%stepDurationArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%stepDurationArray not allocated.')
    error = .true.
    return
  endif
  do i=1, stepDurationArrayRank
    stepDurationArrayDim(:,i) = size(row%stepDurationArray,i)
  enddo
  !
  ireturn = sdm_addSwitchCycleRow(row%numStep, row%weightArray, weightArrayDim, row%dirOffsetArray, dirOffsetArrayDim,&
      & row%freqOffsetArray, freqOffsetArrayDim, row%stepDurationArray, stepDurationArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_addSwitchCycleRow', ireturn)
    error = .true.
  else
    key%switchCycleId = ireturn
  endif
end subroutine addSwitchCycleRow
!
! ---------------------------------------------------------------------------
!
subroutine getSwitchCycleRow(key, row, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleRow) :: row
  type(SwitchCycleKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSwitchCycleRow, i
  integer, parameter :: weightArrayRank = 1
  integer :: weightArrayDim(2,weightArrayRank)
  integer, parameter :: dirOffsetArrayRank = 2
  integer :: dirOffsetArrayDim(2,dirOffsetArrayRank)
  integer, parameter :: freqOffsetArrayRank = 1
  integer :: freqOffsetArrayDim(2,freqOffsetArrayRank)
  integer, parameter :: stepDurationArrayRank = 1
  integer :: stepDurationArrayDim(2,stepDurationArrayRank)
  ! Deal with dimensions
  if (.not.allocated(row%weightArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%weightArray not allocated.')
    error = .true.
    return
  endif
  do i=1, weightArrayRank
    weightArrayDim(:,i) = size(row%weightArray,i)
  enddo
  if (.not.allocated(row%dirOffsetArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%dirOffsetArray not allocated.')
    error = .true.
    return
  endif
  do i=1, dirOffsetArrayRank
    dirOffsetArrayDim(:,i) = size(row%dirOffsetArray,i)
  enddo
  if (.not.allocated(row%freqOffsetArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%freqOffsetArray not allocated.')
    error = .true.
    return
  endif
  do i=1, freqOffsetArrayRank
    freqOffsetArrayDim(:,i) = size(row%freqOffsetArray,i)
  enddo
  if (.not.allocated(row%stepDurationArray)) then
    call sdmMessage(8,3,'SwitchCycleTable','row%stepDurationArray not allocated.')
    error = .true.
    return
  endif
  do i=1, stepDurationArrayRank
    stepDurationArrayDim(:,i) = size(row%stepDurationArray,i)
  enddo
  !
  ireturn = sdm_getSwitchCycleRow(key%switchCycleId, row%numStep, row%weightArray, weightArrayDim, row%dirOffsetArray,&
      & dirOffsetArrayDim, row%freqOffsetArray, freqOffsetArrayDim, row%stepDurationArray, stepDurationArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_getSwitchCycleRow', ireturn)
    error = .true.
  endif
end subroutine getSwitchCycleRow
!
! ---------------------------------------------------------------------------
!
subroutine getSwitchCycleTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSwitchCycleTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSwitchCycleTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSwitchCycleTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSwitchCycleKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_SwitchCycle
  !
  integer :: tableSize
  logical :: error
  type(SwitchCycleKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSwitchCycleKeys, i
  integer, allocatable :: switchCycleIdList(:)
  !
  allocate(switchCycleIdList(tableSize))
  !
  ireturn = sdm_getSwitchCycleKeys(switchCycleIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_getSwitchCycleKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%switchCycleId = switchCycleIdList(i)
  enddo
end subroutine getSwitchCycleKeys
! ---------------------------------------------------------------------------
!
subroutine allocSwitchCycleRow(row, error)
  use sdm_SwitchCycle
  type(SwitchCycleRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'SwitchCycle'
  ! row%weightArray allocation
  if (allocated(row%weightArray)) then
    deallocate(row%weightArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%dirOffsetArray allocation
  if (allocated(row%dirOffsetArray)) then
    deallocate(row%dirOffsetArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%freqOffsetArray allocation
  if (allocated(row%freqOffsetArray)) then
    deallocate(row%freqOffsetArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%stepDurationArray allocation
  if (allocated(row%stepDurationArray)) then
    deallocate(row%stepDurationArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%weightArray(row%numStep), row%dirOffsetArray(2, row%numStep), row%freqOffsetArray(row%numStep),&
      & row%stepDurationArray(row%numStep), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSwitchCycleRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSwitchCycleDirectionCode(key, opt, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleKey) :: key
  type(SwitchCycleOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSwitchCycleDirectionCode, i
  !! integer :: directionCode
  ! Deal with dimensions
  ireturn = sdm_addSwitchCycleDirectionCode(key%switchCycleId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_addSwitchCycleDirectionCode', ireturn)
    error = .true.
  endif
end subroutine addSwitchCycleDirectionCode
!
! ---------------------------------------------------------------------------
!
subroutine getSwitchCycleDirectionCode(key, opt, present, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleKey) :: key
  type(SwitchCycleOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSwitchCycleDirectionCode, i
  ! Deal with dimensions
  ireturn = sdm_getSwitchCycleDirectionCode(key%switchCycleId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_getSwitchCycleDirectionCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSwitchCycleDirectionCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addSwitchCycleDirectionEquinox(key, opt, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleKey) :: key
  type(SwitchCycleOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSwitchCycleDirectionEquinox, i
  !! integer*8 :: directionEquinox
  ! Deal with dimensions
  ireturn = sdm_addSwitchCycleDirectionEquinox(key%switchCycleId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_addSwitchCycleDirectionEquinox', ireturn)
    error = .true.
  endif
end subroutine addSwitchCycleDirectionEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getSwitchCycleDirectionEquinox(key, opt, present, error)
  !
  use sdm_SwitchCycle
  !
  type(SwitchCycleKey) :: key
  type(SwitchCycleOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSwitchCycleDirectionEquinox, i
  ! Deal with dimensions
  ireturn = sdm_getSwitchCycleDirectionEquinox(key%switchCycleId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SwitchCycleTable','Error in sdm_getSwitchCycleDirectionEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSwitchCycleDirectionEquinox
!
!
! ===========================================================================
!
! Source Table:
!
! ===========================================================================
!
subroutine addSourceRow(key, row, error)
  !
  use sdm_Source
  !
  type(SourceRow) :: row
  type(SourceKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSourceRow, i, j
  integer :: codeDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: properMotionRank = 1
  integer :: properMotionDim(2,properMotionRank)
  integer :: sourceNameDim(2)
  ! ----------------
  ! Deal with dimensions
  codeDim = len(row%code)
  call charcut(row%code)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'SourceTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%properMotion)) then
    call sdmMessage(8,3,'SourceTable','row%properMotion not allocated.')
    error = .true.
    return
  endif
  do i=1, properMotionRank
    properMotionDim(:,i) = size(row%properMotion,i)
  enddo
  sourceNameDim = len(row%sourceName)
  call charcut(row%sourceName)
  !
  ireturn = sdm_addSourceRow(key%timeInterval, key%spectralWindowId, row%code, codeDim, row%direction, directionDim,&
      & row%properMotion, properMotionDim, row%sourceName, sourceNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceRow', ireturn)
    error = .true.
  else
    key%sourceId = ireturn
  endif
end subroutine addSourceRow
!
! ---------------------------------------------------------------------------
!
subroutine getSourceRow(key, row, error)
  !
  use sdm_Source
  !
  type(SourceRow) :: row
  type(SourceKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSourceRow, i
  integer :: codeDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: properMotionRank = 1
  integer :: properMotionDim(2,properMotionRank)
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  codeDim = len(row%code)
  call charcut(row%code)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'SourceTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%properMotion)) then
    call sdmMessage(8,3,'SourceTable','row%properMotion not allocated.')
    error = .true.
    return
  endif
  do i=1, properMotionRank
    properMotionDim(:,i) = size(row%properMotion,i)
  enddo
  sourceNameDim = len(row%sourceName)
  call charcut(row%sourceName)
  !
  ireturn = sdm_getSourceRow(key%sourceId, key%timeInterval, key%spectralWindowId, row%code, codeDim, row%direction,&
      & directionDim, row%properMotion, properMotionDim, row%sourceName, sourceNameDim)
  row%code(codeDim(1)+1:) = ''
  row%sourceName(sourceNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceRow', ireturn)
    error = .true.
  endif
end subroutine getSourceRow
!
! ---------------------------------------------------------------------------
!
subroutine getSourceTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSourceTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSourceTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSourceTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSourceKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Source
  !
  integer :: tableSize
  logical :: error
  type(SourceKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSourceKeys, i
  integer, allocatable :: sourceIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: spectralWindowIdList(:)
  !
  allocate(sourceIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  !
  ireturn = sdm_getSourceKeys(sourceIdList, timeIntervalList, spectralWindowIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%sourceId = sourceIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
  enddo
end subroutine getSourceKeys
! ---------------------------------------------------------------------------
!
subroutine allocSourceRow(row, error)
  use sdm_Source
  type(SourceRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Source'
  ! row%direction allocation
  if (allocated(row%direction)) then
    deallocate(row%direction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%properMotion allocation
  if (allocated(row%properMotion)) then
    deallocate(row%properMotion, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%direction(2),  row%properMotion(2), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSourceRow
!
! ---------------------------------------------------------------------------
!
subroutine allocSourceOpt(row, opt, error)
  use sdm_Source
  type(SourceRow) :: row
  type(SourceOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'Source'
  ! opt%position allocation
  if (allocated(opt%position)) then
    deallocate(opt%position, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%transition allocation
  if (allocated(opt%transition)) then
    deallocate(opt%transition, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%restFrequency allocation
  if (allocated(opt%restFrequency)) then
    deallocate(opt%restFrequency, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sysVel allocation
  if (allocated(opt%sysVel)) then
    deallocate(opt%sysVel, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%rangeVel allocation
  if (allocated(opt%rangeVel)) then
    deallocate(opt%rangeVel, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%frequency allocation
  if (allocated(opt%frequency)) then
    deallocate(opt%frequency, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%frequencyInterval allocation
  if (allocated(opt%frequencyInterval)) then
    deallocate(opt%frequencyInterval, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%stokesParameter allocation
  if (allocated(opt%stokesParameter)) then
    deallocate(opt%stokesParameter, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%flux allocation
  if (allocated(opt%flux)) then
    deallocate(opt%flux, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%fluxErr allocation
  if (allocated(opt%fluxErr)) then
    deallocate(opt%fluxErr, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%positionAngle allocation
  if (allocated(opt%positionAngle)) then
    deallocate(opt%positionAngle, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%positionAngleErr allocation
  if (allocated(opt%positionAngleErr)) then
    deallocate(opt%positionAngleErr, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%size allocation
  if (allocated(opt%size)) then
    deallocate(opt%size, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sizeErr allocation
  if (allocated(opt%sizeErr)) then
    deallocate(opt%sizeErr, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%position(3), opt%transition(opt%numLines), opt%restFrequency(opt%numLines), opt%sysVel(opt%numLines),&
      & opt%rangeVel(2), opt%frequency(opt%numFreq), opt%frequencyInterval(opt%numFreq),&
      & opt%stokesParameter(opt%numStokes), opt%flux(opt%numStokes, opt%numFreq), opt%fluxErr(opt%numStokes,&
      & opt%numFreq), opt%positionAngle(opt%numFreq), opt%positionAngleErr(opt%numFreq), opt%size(2, opt%numFreq),&
      & opt%sizeErr(2, opt%numFreq), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSourceOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSourceDirectionCode(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceDirectionCode, i
  !! integer :: directionCode
  ! Deal with dimensions
  ireturn = sdm_addSourceDirectionCode(key%sourceId, key%timeInterval, key%spectralWindowId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceDirectionCode', ireturn)
    error = .true.
  endif
end subroutine addSourceDirectionCode
!
! ---------------------------------------------------------------------------
!
subroutine getSourceDirectionCode(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceDirectionCode, i
  ! Deal with dimensions
  ireturn = sdm_getSourceDirectionCode(key%sourceId, key%timeInterval, key%spectralWindowId, opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceDirectionCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceDirectionCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceDirectionEquinox(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceDirectionEquinox, i
  !! integer*8 :: directionEquinox
  ! Deal with dimensions
  ireturn = sdm_addSourceDirectionEquinox(key%sourceId, key%timeInterval, key%spectralWindowId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceDirectionEquinox', ireturn)
    error = .true.
  endif
end subroutine addSourceDirectionEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getSourceDirectionEquinox(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceDirectionEquinox, i
  ! Deal with dimensions
  ireturn = sdm_getSourceDirectionEquinox(key%sourceId, key%timeInterval, key%spectralWindowId, opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceDirectionEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceDirectionEquinox
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceCalibrationGroup(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceCalibrationGroup, i
  !! integer :: calibrationGroup
  ! Deal with dimensions
  ireturn = sdm_addSourceCalibrationGroup(key%sourceId, key%timeInterval, key%spectralWindowId, opt%calibrationGroup)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceCalibrationGroup', ireturn)
    error = .true.
  endif
end subroutine addSourceCalibrationGroup
!
! ---------------------------------------------------------------------------
!
subroutine getSourceCalibrationGroup(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceCalibrationGroup, i
  ! Deal with dimensions
  ireturn = sdm_getSourceCalibrationGroup(key%sourceId, key%timeInterval, key%spectralWindowId, opt%calibrationGroup)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceCalibrationGroup', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceCalibrationGroup
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceCatalog(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceCatalog, i
  !! character*256 :: catalog
  integer :: catalogDim(2)
  ! Deal with dimensions
  catalogDim = len(opt%catalog)
  call charcut(opt%catalog)
  ireturn = sdm_addSourceCatalog(key%sourceId, key%timeInterval, key%spectralWindowId, opt%catalog, catalogDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceCatalog', ireturn)
    error = .true.
  endif
end subroutine addSourceCatalog
!
! ---------------------------------------------------------------------------
!
subroutine getSourceCatalog(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceCatalog, i
  integer :: catalogDim(2)
  ! Deal with dimensions
  catalogDim = len(opt%catalog)
  ireturn = sdm_getSourceCatalog(key%sourceId, key%timeInterval, key%spectralWindowId, opt%catalog, catalogDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceCatalog', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceCatalog
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceDeltaVel(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceDeltaVel, i
  !! real*8 :: deltaVel
  ! Deal with dimensions
  ireturn = sdm_addSourceDeltaVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%deltaVel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceDeltaVel', ireturn)
    error = .true.
  endif
end subroutine addSourceDeltaVel
!
! ---------------------------------------------------------------------------
!
subroutine getSourceDeltaVel(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceDeltaVel, i
  ! Deal with dimensions
  ireturn = sdm_getSourceDeltaVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%deltaVel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceDeltaVel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceDeltaVel
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourcePosition(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourcePosition, i
  !! real*8, allocatable :: position(:)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%position)) then
    call sdmMessage(8,3,'SourceTable','opt%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(opt%position,i)
  enddo
  ireturn = sdm_addSourcePosition(key%sourceId, key%timeInterval, key%spectralWindowId, opt%position, positionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourcePosition', ireturn)
    error = .true.
  endif
end subroutine addSourcePosition
!
! ---------------------------------------------------------------------------
!
subroutine getSourcePosition(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourcePosition, i
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%position)) then
    call sdmMessage(8,3,'SourceTable','opt%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(opt%position,i)
  enddo
  ireturn = sdm_getSourcePosition(key%sourceId, key%timeInterval, key%spectralWindowId, opt%position, positionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourcePosition', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourcePosition
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceNumLines(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceNumLines, i
  !! integer :: numLines
  ! Deal with dimensions
  ireturn = sdm_addSourceNumLines(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numLines)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceNumLines', ireturn)
    error = .true.
  endif
end subroutine addSourceNumLines
!
! ---------------------------------------------------------------------------
!
subroutine getSourceNumLines(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceNumLines, i
  ! Deal with dimensions
  ireturn = sdm_getSourceNumLines(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numLines)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceNumLines', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceNumLines
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceTransition(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceTransition, i
  !! character*256, allocatable :: transition(:)
  integer, parameter :: transitionRank = 1
  integer :: transitionDim(2,transitionRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%transition)) then
    call sdmMessage(8,3,'SourceTable','opt%transition not allocated.')
    error = .true.
    return
  endif
  transitionDim(:,1) = len(opt%transition)
  do i=1, transitionRank
    transitionDim(:,i+1) = size(opt%transition,i)
    call charcut(opt%transition(i))
  enddo
  ireturn = sdm_addSourceTransition(key%sourceId, key%timeInterval, key%spectralWindowId, opt%transition,&
      & transitionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceTransition', ireturn)
    error = .true.
  endif
end subroutine addSourceTransition
!
! ---------------------------------------------------------------------------
!
subroutine getSourceTransition(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceTransition, i
  integer, parameter :: transitionRank = 1
  integer :: transitionDim(2,transitionRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%transition)) then
    call sdmMessage(8,3,'SourceTable','opt%transition not allocated.')
    error = .true.
    return
  endif
  transitionDim(:,1) = len(opt%transition)
  do i=1, transitionRank
    transitionDim(:,i+1) = size(opt%transition,i)
  enddo
  ireturn = sdm_getSourceTransition(key%sourceId, key%timeInterval, key%spectralWindowId, opt%transition,&
      & transitionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceTransition', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceTransition
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceRestFrequency(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceRestFrequency, i
  !! real*8, allocatable :: restFrequency(:)
  integer, parameter :: restFrequencyRank = 1
  integer :: restFrequencyDim(2,restFrequencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%restFrequency)) then
    call sdmMessage(8,3,'SourceTable','opt%restFrequency not allocated.')
    error = .true.
    return
  endif
  do i=1, restFrequencyRank
    restFrequencyDim(:,i) = size(opt%restFrequency,i)
  enddo
  ireturn = sdm_addSourceRestFrequency(key%sourceId, key%timeInterval, key%spectralWindowId, opt%restFrequency,&
      & restFrequencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceRestFrequency', ireturn)
    error = .true.
  endif
end subroutine addSourceRestFrequency
!
! ---------------------------------------------------------------------------
!
subroutine getSourceRestFrequency(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceRestFrequency, i
  integer, parameter :: restFrequencyRank = 1
  integer :: restFrequencyDim(2,restFrequencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%restFrequency)) then
    call sdmMessage(8,3,'SourceTable','opt%restFrequency not allocated.')
    error = .true.
    return
  endif
  do i=1, restFrequencyRank
    restFrequencyDim(:,i) = size(opt%restFrequency,i)
  enddo
  ireturn = sdm_getSourceRestFrequency(key%sourceId, key%timeInterval, key%spectralWindowId, opt%restFrequency,&
      & restFrequencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceRestFrequency', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceRestFrequency
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceSysVel(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceSysVel, i
  !! real*8, allocatable :: sysVel(:)
  integer, parameter :: sysVelRank = 1
  integer :: sysVelDim(2,sysVelRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sysVel)) then
    call sdmMessage(8,3,'SourceTable','opt%sysVel not allocated.')
    error = .true.
    return
  endif
  do i=1, sysVelRank
    sysVelDim(:,i) = size(opt%sysVel,i)
  enddo
  ireturn = sdm_addSourceSysVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sysVel, sysVelDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceSysVel', ireturn)
    error = .true.
  endif
end subroutine addSourceSysVel
!
! ---------------------------------------------------------------------------
!
subroutine getSourceSysVel(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceSysVel, i
  integer, parameter :: sysVelRank = 1
  integer :: sysVelDim(2,sysVelRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sysVel)) then
    call sdmMessage(8,3,'SourceTable','opt%sysVel not allocated.')
    error = .true.
    return
  endif
  do i=1, sysVelRank
    sysVelDim(:,i) = size(opt%sysVel,i)
  enddo
  ireturn = sdm_getSourceSysVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sysVel, sysVelDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceSysVel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceSysVel
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceRangeVel(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceRangeVel, i
  !! real*8, allocatable :: rangeVel(:)
  integer, parameter :: rangeVelRank = 1
  integer :: rangeVelDim(2,rangeVelRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rangeVel)) then
    call sdmMessage(8,3,'SourceTable','opt%rangeVel not allocated.')
    error = .true.
    return
  endif
  do i=1, rangeVelRank
    rangeVelDim(:,i) = size(opt%rangeVel,i)
  enddo
  ireturn = sdm_addSourceRangeVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%rangeVel, rangeVelDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceRangeVel', ireturn)
    error = .true.
  endif
end subroutine addSourceRangeVel
!
! ---------------------------------------------------------------------------
!
subroutine getSourceRangeVel(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceRangeVel, i
  integer, parameter :: rangeVelRank = 1
  integer :: rangeVelDim(2,rangeVelRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rangeVel)) then
    call sdmMessage(8,3,'SourceTable','opt%rangeVel not allocated.')
    error = .true.
    return
  endif
  do i=1, rangeVelRank
    rangeVelDim(:,i) = size(opt%rangeVel,i)
  enddo
  ireturn = sdm_getSourceRangeVel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%rangeVel, rangeVelDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceRangeVel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceRangeVel
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceSourceModel(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceSourceModel, i
  !! integer :: sourceModel
  ! Deal with dimensions
  ireturn = sdm_addSourceSourceModel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sourceModel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceSourceModel', ireturn)
    error = .true.
  endif
end subroutine addSourceSourceModel
!
! ---------------------------------------------------------------------------
!
subroutine getSourceSourceModel(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceSourceModel, i
  ! Deal with dimensions
  ireturn = sdm_getSourceSourceModel(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sourceModel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceSourceModel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceSourceModel
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceFrequencyRefCode(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceFrequencyRefCode, i
  !! integer :: frequencyRefCode
  ! Deal with dimensions
  ireturn = sdm_addSourceFrequencyRefCode(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequencyRefCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceFrequencyRefCode', ireturn)
    error = .true.
  endif
end subroutine addSourceFrequencyRefCode
!
! ---------------------------------------------------------------------------
!
subroutine getSourceFrequencyRefCode(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceFrequencyRefCode, i
  ! Deal with dimensions
  ireturn = sdm_getSourceFrequencyRefCode(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequencyRefCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceFrequencyRefCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceFrequencyRefCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceNumFreq(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceNumFreq, i
  !! integer :: numFreq
  ! Deal with dimensions
  ireturn = sdm_addSourceNumFreq(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numFreq)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceNumFreq', ireturn)
    error = .true.
  endif
end subroutine addSourceNumFreq
!
! ---------------------------------------------------------------------------
!
subroutine getSourceNumFreq(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceNumFreq, i
  ! Deal with dimensions
  ireturn = sdm_getSourceNumFreq(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numFreq)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceNumFreq', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceNumFreq
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceNumStokes(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceNumStokes, i
  !! integer :: numStokes
  ! Deal with dimensions
  ireturn = sdm_addSourceNumStokes(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numStokes)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceNumStokes', ireturn)
    error = .true.
  endif
end subroutine addSourceNumStokes
!
! ---------------------------------------------------------------------------
!
subroutine getSourceNumStokes(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceNumStokes, i
  ! Deal with dimensions
  ireturn = sdm_getSourceNumStokes(key%sourceId, key%timeInterval, key%spectralWindowId, opt%numStokes)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceNumStokes', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceNumStokes
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceFrequency(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceFrequency, i
  !! real*8, allocatable :: frequency(:)
  integer, parameter :: frequencyRank = 1
  integer :: frequencyDim(2,frequencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%frequency)) then
    call sdmMessage(8,3,'SourceTable','opt%frequency not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRank
    frequencyDim(:,i) = size(opt%frequency,i)
  enddo
  ireturn = sdm_addSourceFrequency(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequency, frequencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceFrequency', ireturn)
    error = .true.
  endif
end subroutine addSourceFrequency
!
! ---------------------------------------------------------------------------
!
subroutine getSourceFrequency(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceFrequency, i
  integer, parameter :: frequencyRank = 1
  integer :: frequencyDim(2,frequencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%frequency)) then
    call sdmMessage(8,3,'SourceTable','opt%frequency not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRank
    frequencyDim(:,i) = size(opt%frequency,i)
  enddo
  ireturn = sdm_getSourceFrequency(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequency, frequencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceFrequency', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceFrequency
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceFrequencyInterval(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceFrequencyInterval, i
  !! real*8, allocatable :: frequencyInterval(:)
  integer, parameter :: frequencyIntervalRank = 1
  integer :: frequencyIntervalDim(2,frequencyIntervalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%frequencyInterval)) then
    call sdmMessage(8,3,'SourceTable','opt%frequencyInterval not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyIntervalRank
    frequencyIntervalDim(:,i) = size(opt%frequencyInterval,i)
  enddo
  ireturn = sdm_addSourceFrequencyInterval(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequencyInterval,&
      & frequencyIntervalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceFrequencyInterval', ireturn)
    error = .true.
  endif
end subroutine addSourceFrequencyInterval
!
! ---------------------------------------------------------------------------
!
subroutine getSourceFrequencyInterval(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceFrequencyInterval, i
  integer, parameter :: frequencyIntervalRank = 1
  integer :: frequencyIntervalDim(2,frequencyIntervalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%frequencyInterval)) then
    call sdmMessage(8,3,'SourceTable','opt%frequencyInterval not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyIntervalRank
    frequencyIntervalDim(:,i) = size(opt%frequencyInterval,i)
  enddo
  ireturn = sdm_getSourceFrequencyInterval(key%sourceId, key%timeInterval, key%spectralWindowId, opt%frequencyInterval,&
      & frequencyIntervalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceFrequencyInterval', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceFrequencyInterval
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceStokesParameter(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceStokesParameter, i
  !! integer, allocatable :: stokesParameter(:)
  integer, parameter :: stokesParameterRank = 1
  integer :: stokesParameterDim(2,stokesParameterRank)
  ! Deal with dimensions
  if (.not.allocated(opt%stokesParameter)) then
    call sdmMessage(8,3,'SourceTable','opt%stokesParameter not allocated.')
    error = .true.
    return
  endif
  do i=1, stokesParameterRank
    stokesParameterDim(:,i) = size(opt%stokesParameter,i)
  enddo
  ireturn = sdm_addSourceStokesParameter(key%sourceId, key%timeInterval, key%spectralWindowId, opt%stokesParameter,&
      & stokesParameterDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceStokesParameter', ireturn)
    error = .true.
  endif
end subroutine addSourceStokesParameter
!
! ---------------------------------------------------------------------------
!
subroutine getSourceStokesParameter(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceStokesParameter, i
  integer, parameter :: stokesParameterRank = 1
  integer :: stokesParameterDim(2,stokesParameterRank)
  ! Deal with dimensions
  if (.not.allocated(opt%stokesParameter)) then
    call sdmMessage(8,3,'SourceTable','opt%stokesParameter not allocated.')
    error = .true.
    return
  endif
  do i=1, stokesParameterRank
    stokesParameterDim(:,i) = size(opt%stokesParameter,i)
  enddo
  ireturn = sdm_getSourceStokesParameter(key%sourceId, key%timeInterval, key%spectralWindowId, opt%stokesParameter,&
      & stokesParameterDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceStokesParameter', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceStokesParameter
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceFlux(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceFlux, i
  !! real*8, allocatable :: flux(:,:)
  integer, parameter :: fluxRank = 2
  integer :: fluxDim(2,fluxRank)
  ! Deal with dimensions
  if (.not.allocated(opt%flux)) then
    call sdmMessage(8,3,'SourceTable','opt%flux not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxRank
    fluxDim(:,i) = size(opt%flux,i)
  enddo
  ireturn = sdm_addSourceFlux(key%sourceId, key%timeInterval, key%spectralWindowId, opt%flux, fluxDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceFlux', ireturn)
    error = .true.
  endif
end subroutine addSourceFlux
!
! ---------------------------------------------------------------------------
!
subroutine getSourceFlux(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceFlux, i
  integer, parameter :: fluxRank = 2
  integer :: fluxDim(2,fluxRank)
  ! Deal with dimensions
  if (.not.allocated(opt%flux)) then
    call sdmMessage(8,3,'SourceTable','opt%flux not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxRank
    fluxDim(:,i) = size(opt%flux,i)
  enddo
  ireturn = sdm_getSourceFlux(key%sourceId, key%timeInterval, key%spectralWindowId, opt%flux, fluxDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceFlux', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceFlux
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceFluxErr(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceFluxErr, i
  !! real*8, allocatable :: fluxErr(:,:)
  integer, parameter :: fluxErrRank = 2
  integer :: fluxErrDim(2,fluxErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%fluxErr)) then
    call sdmMessage(8,3,'SourceTable','opt%fluxErr not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxErrRank
    fluxErrDim(:,i) = size(opt%fluxErr,i)
  enddo
  ireturn = sdm_addSourceFluxErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%fluxErr, fluxErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceFluxErr', ireturn)
    error = .true.
  endif
end subroutine addSourceFluxErr
!
! ---------------------------------------------------------------------------
!
subroutine getSourceFluxErr(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceFluxErr, i
  integer, parameter :: fluxErrRank = 2
  integer :: fluxErrDim(2,fluxErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%fluxErr)) then
    call sdmMessage(8,3,'SourceTable','opt%fluxErr not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxErrRank
    fluxErrDim(:,i) = size(opt%fluxErr,i)
  enddo
  ireturn = sdm_getSourceFluxErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%fluxErr, fluxErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceFluxErr', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceFluxErr
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourcePositionAngle(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourcePositionAngle, i
  !! real*8, allocatable :: positionAngle(:)
  integer, parameter :: positionAngleRank = 1
  integer :: positionAngleDim(2,positionAngleRank)
  ! Deal with dimensions
  if (.not.allocated(opt%positionAngle)) then
    call sdmMessage(8,3,'SourceTable','opt%positionAngle not allocated.')
    error = .true.
    return
  endif
  do i=1, positionAngleRank
    positionAngleDim(:,i) = size(opt%positionAngle,i)
  enddo
  ireturn = sdm_addSourcePositionAngle(key%sourceId, key%timeInterval, key%spectralWindowId, opt%positionAngle,&
      & positionAngleDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourcePositionAngle', ireturn)
    error = .true.
  endif
end subroutine addSourcePositionAngle
!
! ---------------------------------------------------------------------------
!
subroutine getSourcePositionAngle(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourcePositionAngle, i
  integer, parameter :: positionAngleRank = 1
  integer :: positionAngleDim(2,positionAngleRank)
  ! Deal with dimensions
  if (.not.allocated(opt%positionAngle)) then
    call sdmMessage(8,3,'SourceTable','opt%positionAngle not allocated.')
    error = .true.
    return
  endif
  do i=1, positionAngleRank
    positionAngleDim(:,i) = size(opt%positionAngle,i)
  enddo
  ireturn = sdm_getSourcePositionAngle(key%sourceId, key%timeInterval, key%spectralWindowId, opt%positionAngle,&
      & positionAngleDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourcePositionAngle', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourcePositionAngle
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourcePositionAngleErr(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourcePositionAngleErr, i
  !! real*8, allocatable :: positionAngleErr(:)
  integer, parameter :: positionAngleErrRank = 1
  integer :: positionAngleErrDim(2,positionAngleErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%positionAngleErr)) then
    call sdmMessage(8,3,'SourceTable','opt%positionAngleErr not allocated.')
    error = .true.
    return
  endif
  do i=1, positionAngleErrRank
    positionAngleErrDim(:,i) = size(opt%positionAngleErr,i)
  enddo
  ireturn = sdm_addSourcePositionAngleErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%positionAngleErr,&
      & positionAngleErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourcePositionAngleErr', ireturn)
    error = .true.
  endif
end subroutine addSourcePositionAngleErr
!
! ---------------------------------------------------------------------------
!
subroutine getSourcePositionAngleErr(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourcePositionAngleErr, i
  integer, parameter :: positionAngleErrRank = 1
  integer :: positionAngleErrDim(2,positionAngleErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%positionAngleErr)) then
    call sdmMessage(8,3,'SourceTable','opt%positionAngleErr not allocated.')
    error = .true.
    return
  endif
  do i=1, positionAngleErrRank
    positionAngleErrDim(:,i) = size(opt%positionAngleErr,i)
  enddo
  ireturn = sdm_getSourcePositionAngleErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%positionAngleErr,&
      & positionAngleErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourcePositionAngleErr', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourcePositionAngleErr
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceSize(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceSize, i
  !! real*8, allocatable :: size(:,:)
  integer, parameter :: sizeRank = 2
  integer :: sizeDim(2,sizeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%size)) then
    call sdmMessage(8,3,'SourceTable','opt%size not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeRank
    sizeDim(:,i) = size(opt%size,i)
  enddo
  ireturn = sdm_addSourceSize(key%sourceId, key%timeInterval, key%spectralWindowId, opt%size, sizeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceSize', ireturn)
    error = .true.
  endif
end subroutine addSourceSize
!
! ---------------------------------------------------------------------------
!
subroutine getSourceSize(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceSize, i
  integer, parameter :: sizeRank = 2
  integer :: sizeDim(2,sizeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%size)) then
    call sdmMessage(8,3,'SourceTable','opt%size not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeRank
    sizeDim(:,i) = size(opt%size,i)
  enddo
  ireturn = sdm_getSourceSize(key%sourceId, key%timeInterval, key%spectralWindowId, opt%size, sizeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceSize', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceSize
!
!
! ---------------------------------------------------------------------------
!
subroutine addSourceSizeErr(key, opt, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSourceSizeErr, i
  !! real*8, allocatable :: sizeErr(:,:)
  integer, parameter :: sizeErrRank = 2
  integer :: sizeErrDim(2,sizeErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sizeErr)) then
    call sdmMessage(8,3,'SourceTable','opt%sizeErr not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeErrRank
    sizeErrDim(:,i) = size(opt%sizeErr,i)
  enddo
  ireturn = sdm_addSourceSizeErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sizeErr, sizeErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_addSourceSizeErr', ireturn)
    error = .true.
  endif
end subroutine addSourceSizeErr
!
! ---------------------------------------------------------------------------
!
subroutine getSourceSizeErr(key, opt, present, error)
  !
  use sdm_Source
  !
  type(SourceKey) :: key
  type(SourceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSourceSizeErr, i
  integer, parameter :: sizeErrRank = 2
  integer :: sizeErrDim(2,sizeErrRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sizeErr)) then
    call sdmMessage(8,3,'SourceTable','opt%sizeErr not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeErrRank
    sizeErrDim(:,i) = size(opt%sizeErr,i)
  enddo
  ireturn = sdm_getSourceSizeErr(key%sourceId, key%timeInterval, key%spectralWindowId, opt%sizeErr, sizeErrDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SourceTable','Error in sdm_getSourceSizeErr', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSourceSizeErr
!
!
! ===========================================================================
!
! Feed Table:
!
! ===========================================================================
!
subroutine addFeedRow(key, row, error)
  !
  use sdm_Feed
  !
  type(FeedRow) :: row
  type(FeedKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFeedRow, i, j
  integer, parameter :: beamOffsetRank = 2
  integer :: beamOffsetDim(2,beamOffsetRank)
  integer, parameter :: focusReferenceRank = 2
  integer :: focusReferenceDim(2,focusReferenceRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: polResponseRank = 2
  integer :: polResponseDim(2,polResponseRank)
  integer, parameter :: receptorAngleRank = 1
  integer :: receptorAngleDim(2,receptorAngleRank)
  integer, parameter :: receiverIdRank = 1
  integer :: receiverIdDim(2,receiverIdRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%beamOffset)) then
    call sdmMessage(8,3,'FeedTable','row%beamOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, beamOffsetRank
    beamOffsetDim(:,i) = size(row%beamOffset,i)
  enddo
  if (.not.allocated(row%focusReference)) then
    call sdmMessage(8,3,'FeedTable','row%focusReference not allocated.')
    error = .true.
    return
  endif
  do i=1, focusReferenceRank
    focusReferenceDim(:,i) = size(row%focusReference,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'FeedTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%polResponse)) then
    call sdmMessage(8,3,'FeedTable','row%polResponse not allocated.')
    error = .true.
    return
  endif
  do i=1, polResponseRank
    polResponseDim(:,i) = size(row%polResponse,i)
  enddo
  if (.not.allocated(row%receptorAngle)) then
    call sdmMessage(8,3,'FeedTable','row%receptorAngle not allocated.')
    error = .true.
    return
  endif
  do i=1, receptorAngleRank
    receptorAngleDim(:,i) = size(row%receptorAngle,i)
  enddo
  if (.not.allocated(row%receiverId)) then
    call sdmMessage(8,3,'FeedTable','row%receiverId not allocated.')
    error = .true.
    return
  endif
  do i=1, receiverIdRank
    receiverIdDim(:,i) = size(row%receiverId,i)
  enddo
  !
  ireturn = sdm_addFeedRow(key%antennaId, key%spectralWindowId, key%timeInterval, row%numReceptor, row%beamOffset,&
      & beamOffsetDim, row%focusReference, focusReferenceDim, row%polarizationTypes, polarizationTypesDim,&
      & row%polResponse, polResponseDim, row%receptorAngle, receptorAngleDim, row%receiverId, receiverIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_addFeedRow', ireturn)
    error = .true.
  else
    key%feedId = ireturn
  endif
end subroutine addFeedRow
!
! ---------------------------------------------------------------------------
!
subroutine getFeedRow(key, row, error)
  !
  use sdm_Feed
  !
  type(FeedRow) :: row
  type(FeedKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFeedRow, i
  integer, parameter :: beamOffsetRank = 2
  integer :: beamOffsetDim(2,beamOffsetRank)
  integer, parameter :: focusReferenceRank = 2
  integer :: focusReferenceDim(2,focusReferenceRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: polResponseRank = 2
  integer :: polResponseDim(2,polResponseRank)
  integer, parameter :: receptorAngleRank = 1
  integer :: receptorAngleDim(2,receptorAngleRank)
  integer, parameter :: receiverIdRank = 1
  integer :: receiverIdDim(2,receiverIdRank)
  ! Deal with dimensions
  if (.not.allocated(row%beamOffset)) then
    call sdmMessage(8,3,'FeedTable','row%beamOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, beamOffsetRank
    beamOffsetDim(:,i) = size(row%beamOffset,i)
  enddo
  if (.not.allocated(row%focusReference)) then
    call sdmMessage(8,3,'FeedTable','row%focusReference not allocated.')
    error = .true.
    return
  endif
  do i=1, focusReferenceRank
    focusReferenceDim(:,i) = size(row%focusReference,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'FeedTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%polResponse)) then
    call sdmMessage(8,3,'FeedTable','row%polResponse not allocated.')
    error = .true.
    return
  endif
  do i=1, polResponseRank
    polResponseDim(:,i) = size(row%polResponse,i)
  enddo
  if (.not.allocated(row%receptorAngle)) then
    call sdmMessage(8,3,'FeedTable','row%receptorAngle not allocated.')
    error = .true.
    return
  endif
  do i=1, receptorAngleRank
    receptorAngleDim(:,i) = size(row%receptorAngle,i)
  enddo
  if (.not.allocated(row%receiverId)) then
    call sdmMessage(8,3,'FeedTable','row%receiverId not allocated.')
    error = .true.
    return
  endif
  do i=1, receiverIdRank
    receiverIdDim(:,i) = size(row%receiverId,i)
  enddo
  !
  ireturn = sdm_getFeedRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%numReceptor,&
      & row%beamOffset, beamOffsetDim, row%focusReference, focusReferenceDim, row%polarizationTypes,&
      & polarizationTypesDim, row%polResponse, polResponseDim, row%receptorAngle, receptorAngleDim, row%receiverId,&
      & receiverIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedRow', ireturn)
    error = .true.
  endif
end subroutine getFeedRow
!
! ---------------------------------------------------------------------------
!
subroutine getFeedTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFeedTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFeedTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFeedTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFeedKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Feed
  !
  integer :: tableSize
  logical :: error
  type(FeedKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFeedKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: feedIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(feedIdList(tableSize))
  !
  ireturn = sdm_getFeedKeys(antennaIdList, spectralWindowIdList, timeIntervalList, feedIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%feedId = feedIdList(i)
  enddo
end subroutine getFeedKeys
! ---------------------------------------------------------------------------
!
subroutine allocFeedRow(row, error)
  use sdm_Feed
  type(FeedRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Feed'
  ! row%beamOffset allocation
  if (allocated(row%beamOffset)) then
    deallocate(row%beamOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%focusReference allocation
  if (allocated(row%focusReference)) then
    deallocate(row%focusReference, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polResponse allocation
  if (allocated(row%polResponse)) then
    deallocate(row%polResponse, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%receptorAngle allocation
  if (allocated(row%receptorAngle)) then
    deallocate(row%receptorAngle, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%receiverId allocation
  if (allocated(row%receiverId)) then
    deallocate(row%receiverId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%beamOffset(2, row%numReceptor), row%focusReference(3, row%numReceptor),&
      & row%polarizationTypes(row%numReceptor), row%polResponse(row%numReceptor, row%numReceptor),&
      & row%receptorAngle(row%numReceptor), row%receiverId(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFeedRow
!
! ---------------------------------------------------------------------------
!
subroutine allocFeedOpt(row, opt, error)
  use sdm_Feed
  type(FeedRow) :: row
  type(FeedOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'Feed'
  ! opt%illumOffset allocation
  if (allocated(opt%illumOffset)) then
    deallocate(opt%illumOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%position allocation
  if (allocated(opt%position)) then
    deallocate(opt%position, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%beamId allocation
  if (allocated(opt%beamId)) then
    deallocate(opt%beamId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%illumOffset(2),  opt%position(3),  opt%beamId(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFeedOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addFeedFeedNum(key, opt, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFeedFeedNum, i
  !! integer :: feedNum
  ! Deal with dimensions
  ireturn = sdm_addFeedFeedNum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%feedNum)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_addFeedFeedNum', ireturn)
    error = .true.
  endif
end subroutine addFeedFeedNum
!
! ---------------------------------------------------------------------------
!
subroutine getFeedFeedNum(key, opt, present, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFeedFeedNum, i
  ! Deal with dimensions
  ireturn = sdm_getFeedFeedNum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%feedNum)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedFeedNum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFeedFeedNum
!
!
! ---------------------------------------------------------------------------
!
subroutine addFeedIllumOffset(key, opt, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFeedIllumOffset, i
  !! real*8, allocatable :: illumOffset(:)
  integer, parameter :: illumOffsetRank = 1
  integer :: illumOffsetDim(2,illumOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%illumOffset)) then
    call sdmMessage(8,3,'FeedTable','opt%illumOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, illumOffsetRank
    illumOffsetDim(:,i) = size(opt%illumOffset,i)
  enddo
  ireturn = sdm_addFeedIllumOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%illumOffset,&
      & illumOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_addFeedIllumOffset', ireturn)
    error = .true.
  endif
end subroutine addFeedIllumOffset
!
! ---------------------------------------------------------------------------
!
subroutine getFeedIllumOffset(key, opt, present, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFeedIllumOffset, i
  integer, parameter :: illumOffsetRank = 1
  integer :: illumOffsetDim(2,illumOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%illumOffset)) then
    call sdmMessage(8,3,'FeedTable','opt%illumOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, illumOffsetRank
    illumOffsetDim(:,i) = size(opt%illumOffset,i)
  enddo
  ireturn = sdm_getFeedIllumOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%illumOffset,&
      & illumOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedIllumOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFeedIllumOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addFeedPosition(key, opt, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFeedPosition, i
  !! real*8, allocatable :: position(:)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%position)) then
    call sdmMessage(8,3,'FeedTable','opt%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(opt%position,i)
  enddo
  ireturn = sdm_addFeedPosition(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%position,&
      & positionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_addFeedPosition', ireturn)
    error = .true.
  endif
end subroutine addFeedPosition
!
! ---------------------------------------------------------------------------
!
subroutine getFeedPosition(key, opt, present, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFeedPosition, i
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%position)) then
    call sdmMessage(8,3,'FeedTable','opt%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(opt%position,i)
  enddo
  ireturn = sdm_getFeedPosition(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%position,&
      & positionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedPosition', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFeedPosition
!
!
! ---------------------------------------------------------------------------
!
subroutine addFeedBeamId(key, opt, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFeedBeamId, i
  !! integer, allocatable :: beamId(:)
  integer, parameter :: beamIdRank = 1
  integer :: beamIdDim(2,beamIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamId)) then
    call sdmMessage(8,3,'FeedTable','opt%beamId not allocated.')
    error = .true.
    return
  endif
  do i=1, beamIdRank
    beamIdDim(:,i) = size(opt%beamId,i)
  enddo
  ireturn = sdm_addFeedBeamId(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%beamId, beamIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_addFeedBeamId', ireturn)
    error = .true.
  endif
end subroutine addFeedBeamId
!
! ---------------------------------------------------------------------------
!
subroutine getFeedBeamId(key, opt, present, error)
  !
  use sdm_Feed
  !
  type(FeedKey) :: key
  type(FeedOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFeedBeamId, i
  integer, parameter :: beamIdRank = 1
  integer :: beamIdDim(2,beamIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamId)) then
    call sdmMessage(8,3,'FeedTable','opt%beamId not allocated.')
    error = .true.
    return
  endif
  do i=1, beamIdRank
    beamIdDim(:,i) = size(opt%beamId,i)
  enddo
  ireturn = sdm_getFeedBeamId(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%beamId, beamIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FeedTable','Error in sdm_getFeedBeamId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFeedBeamId
!
!
! ===========================================================================
!
! SpectralWindow Table:
!
! ===========================================================================
!
subroutine addSpectralWindowRow(key, row, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowRow) :: row
  type(SpectralWindowKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSpectralWindowRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addSpectralWindowRow(row%basebandName, row%netSideband, row%numChan, row%refFreq,&
      & row%sidebandProcessingMode, row%totBandwidth, row%windowFunction)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowRow', ireturn)
    error = .true.
  else
    key%spectralWindowId = ireturn
  endif
end subroutine addSpectralWindowRow
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowRow(key, row, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowRow) :: row
  type(SpectralWindowKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSpectralWindowRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getSpectralWindowRow(key%spectralWindowId, row%basebandName, row%netSideband, row%numChan, row%refFreq,&
      & row%sidebandProcessingMode, row%totBandwidth, row%windowFunction)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowRow', ireturn)
    error = .true.
  endif
end subroutine getSpectralWindowRow
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSpectralWindowTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSpectralWindowTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSpectralWindowTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_SpectralWindow
  !
  integer :: tableSize
  logical :: error
  type(SpectralWindowKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSpectralWindowKeys, i
  integer, allocatable :: spectralWindowIdList(:)
  !
  allocate(spectralWindowIdList(tableSize))
  !
  ireturn = sdm_getSpectralWindowKeys(spectralWindowIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
  enddo
end subroutine getSpectralWindowKeys
! ---------------------------------------------------------------------------
!
subroutine allocSpectralWindowOpt(row, opt, error)
  use sdm_SpectralWindow
  type(SpectralWindowRow) :: row
  type(SpectralWindowOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'SpectralWindow'
  ! opt%chanFreqArray allocation
  if (allocated(opt%chanFreqArray)) then
    deallocate(opt%chanFreqArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%chanWidthArray allocation
  if (allocated(opt%chanWidthArray)) then
    deallocate(opt%chanWidthArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%effectiveBwArray allocation
  if (allocated(opt%effectiveBwArray)) then
    deallocate(opt%effectiveBwArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%lineArray allocation
  if (allocated(opt%lineArray)) then
    deallocate(opt%lineArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%resolutionArray allocation
  if (allocated(opt%resolutionArray)) then
    deallocate(opt%resolutionArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%assocNature allocation
  if (allocated(opt%assocNature)) then
    deallocate(opt%assocNature, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%assocSpectralWindowId allocation
  if (allocated(opt%assocSpectralWindowId)) then
    deallocate(opt%assocSpectralWindowId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%chanFreqArray(row%numChan), opt%chanWidthArray(row%numChan), opt%effectiveBwArray(row%numChan),&
      & opt%lineArray(row%numChan), opt%resolutionArray(row%numChan), opt%assocNature(opt%numAssocValues),&
      & opt%assocSpectralWindowId(opt%numAssocValues), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSpectralWindowOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowChanFreqStart(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowChanFreqStart, i
  !! real*8 :: chanFreqStart
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowChanFreqStart(key%spectralWindowId, opt%chanFreqStart)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowChanFreqStart', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowChanFreqStart
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowChanFreqStart(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowChanFreqStart, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowChanFreqStart(key%spectralWindowId, opt%chanFreqStart)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowChanFreqStart', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowChanFreqStart
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowChanFreqStep(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowChanFreqStep, i
  !! real*8 :: chanFreqStep
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowChanFreqStep(key%spectralWindowId, opt%chanFreqStep)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowChanFreqStep', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowChanFreqStep
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowChanFreqStep(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowChanFreqStep, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowChanFreqStep(key%spectralWindowId, opt%chanFreqStep)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowChanFreqStep', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowChanFreqStep
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowChanFreqArray(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowChanFreqArray, i
  !! real*8, allocatable :: chanFreqArray(:)
  integer, parameter :: chanFreqArrayRank = 1
  integer :: chanFreqArrayDim(2,chanFreqArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%chanFreqArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%chanFreqArray not allocated.')
    error = .true.
    return
  endif
  do i=1, chanFreqArrayRank
    chanFreqArrayDim(:,i) = size(opt%chanFreqArray,i)
  enddo
  ireturn = sdm_addSpectralWindowChanFreqArray(key%spectralWindowId, opt%chanFreqArray, chanFreqArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowChanFreqArray', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowChanFreqArray
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowChanFreqArray(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowChanFreqArray, i
  integer, parameter :: chanFreqArrayRank = 1
  integer :: chanFreqArrayDim(2,chanFreqArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%chanFreqArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%chanFreqArray not allocated.')
    error = .true.
    return
  endif
  do i=1, chanFreqArrayRank
    chanFreqArrayDim(:,i) = size(opt%chanFreqArray,i)
  enddo
  ireturn = sdm_getSpectralWindowChanFreqArray(key%spectralWindowId, opt%chanFreqArray, chanFreqArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowChanFreqArray', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowChanFreqArray
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowChanWidth(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowChanWidth, i
  !! real*8 :: chanWidth
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowChanWidth(key%spectralWindowId, opt%chanWidth)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowChanWidth', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowChanWidth
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowChanWidth(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowChanWidth, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowChanWidth(key%spectralWindowId, opt%chanWidth)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowChanWidth', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowChanWidth
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowChanWidthArray(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowChanWidthArray, i
  !! real*8, allocatable :: chanWidthArray(:)
  integer, parameter :: chanWidthArrayRank = 1
  integer :: chanWidthArrayDim(2,chanWidthArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%chanWidthArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%chanWidthArray not allocated.')
    error = .true.
    return
  endif
  do i=1, chanWidthArrayRank
    chanWidthArrayDim(:,i) = size(opt%chanWidthArray,i)
  enddo
  ireturn = sdm_addSpectralWindowChanWidthArray(key%spectralWindowId, opt%chanWidthArray, chanWidthArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowChanWidthArray', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowChanWidthArray
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowChanWidthArray(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowChanWidthArray, i
  integer, parameter :: chanWidthArrayRank = 1
  integer :: chanWidthArrayDim(2,chanWidthArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%chanWidthArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%chanWidthArray not allocated.')
    error = .true.
    return
  endif
  do i=1, chanWidthArrayRank
    chanWidthArrayDim(:,i) = size(opt%chanWidthArray,i)
  enddo
  ireturn = sdm_getSpectralWindowChanWidthArray(key%spectralWindowId, opt%chanWidthArray, chanWidthArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowChanWidthArray', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowChanWidthArray
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowCorrelationBit(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowCorrelationBit, i
  !! integer :: correlationBit
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowCorrelationBit(key%spectralWindowId, opt%correlationBit)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowCorrelationBit', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowCorrelationBit
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowCorrelationBit(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowCorrelationBit, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowCorrelationBit(key%spectralWindowId, opt%correlationBit)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowCorrelationBit', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowCorrelationBit
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowEffectiveBw(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowEffectiveBw, i
  !! real*8 :: effectiveBw
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowEffectiveBw(key%spectralWindowId, opt%effectiveBw)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowEffectiveBw', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowEffectiveBw
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowEffectiveBw(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowEffectiveBw, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowEffectiveBw(key%spectralWindowId, opt%effectiveBw)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowEffectiveBw', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowEffectiveBw
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowEffectiveBwArray(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowEffectiveBwArray, i
  !! real*8, allocatable :: effectiveBwArray(:)
  integer, parameter :: effectiveBwArrayRank = 1
  integer :: effectiveBwArrayDim(2,effectiveBwArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%effectiveBwArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%effectiveBwArray not allocated.')
    error = .true.
    return
  endif
  do i=1, effectiveBwArrayRank
    effectiveBwArrayDim(:,i) = size(opt%effectiveBwArray,i)
  enddo
  ireturn = sdm_addSpectralWindowEffectiveBwArray(key%spectralWindowId, opt%effectiveBwArray, effectiveBwArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowEffectiveBwArray', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowEffectiveBwArray
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowEffectiveBwArray(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowEffectiveBwArray, i
  integer, parameter :: effectiveBwArrayRank = 1
  integer :: effectiveBwArrayDim(2,effectiveBwArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%effectiveBwArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%effectiveBwArray not allocated.')
    error = .true.
    return
  endif
  do i=1, effectiveBwArrayRank
    effectiveBwArrayDim(:,i) = size(opt%effectiveBwArray,i)
  enddo
  ireturn = sdm_getSpectralWindowEffectiveBwArray(key%spectralWindowId, opt%effectiveBwArray, effectiveBwArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowEffectiveBwArray', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowEffectiveBwArray
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowFreqGroup(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowFreqGroup, i
  !! integer :: freqGroup
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowFreqGroup(key%spectralWindowId, opt%freqGroup)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowFreqGroup', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowFreqGroup
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowFreqGroup(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowFreqGroup, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowFreqGroup(key%spectralWindowId, opt%freqGroup)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowFreqGroup', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowFreqGroup
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowFreqGroupName(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowFreqGroupName, i
  !! character*256 :: freqGroupName
  integer :: freqGroupNameDim(2)
  ! Deal with dimensions
  freqGroupNameDim = len(opt%freqGroupName)
  call charcut(opt%freqGroupName)
  ireturn = sdm_addSpectralWindowFreqGroupName(key%spectralWindowId, opt%freqGroupName, freqGroupNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowFreqGroupName', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowFreqGroupName
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowFreqGroupName(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowFreqGroupName, i
  integer :: freqGroupNameDim(2)
  ! Deal with dimensions
  freqGroupNameDim = len(opt%freqGroupName)
  ireturn = sdm_getSpectralWindowFreqGroupName(key%spectralWindowId, opt%freqGroupName, freqGroupNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowFreqGroupName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowFreqGroupName
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowLineArray(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowLineArray, i
  !! logical*1, allocatable :: lineArray(:)
  integer, parameter :: lineArrayRank = 1
  integer :: lineArrayDim(2,lineArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%lineArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%lineArray not allocated.')
    error = .true.
    return
  endif
  do i=1, lineArrayRank
    lineArrayDim(:,i) = size(opt%lineArray,i)
  enddo
  ireturn = sdm_addSpectralWindowLineArray(key%spectralWindowId, opt%lineArray, lineArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowLineArray', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowLineArray
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowLineArray(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowLineArray, i
  integer, parameter :: lineArrayRank = 1
  integer :: lineArrayDim(2,lineArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%lineArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%lineArray not allocated.')
    error = .true.
    return
  endif
  do i=1, lineArrayRank
    lineArrayDim(:,i) = size(opt%lineArray,i)
  enddo
  ireturn = sdm_getSpectralWindowLineArray(key%spectralWindowId, opt%lineArray, lineArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowLineArray', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowLineArray
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowMeasFreqRef(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowMeasFreqRef, i
  !! integer :: measFreqRef
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowMeasFreqRef(key%spectralWindowId, opt%measFreqRef)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowMeasFreqRef', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowMeasFreqRef
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowMeasFreqRef(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowMeasFreqRef, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowMeasFreqRef(key%spectralWindowId, opt%measFreqRef)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowMeasFreqRef', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowMeasFreqRef
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowName(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowName, i
  !! character*256 :: name
  integer :: nameDim(2)
  ! Deal with dimensions
  nameDim = len(opt%name)
  call charcut(opt%name)
  ireturn = sdm_addSpectralWindowName(key%spectralWindowId, opt%name, nameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowName', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowName
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowName(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowName, i
  integer :: nameDim(2)
  ! Deal with dimensions
  nameDim = len(opt%name)
  ireturn = sdm_getSpectralWindowName(key%spectralWindowId, opt%name, nameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowName
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowOversampling(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowOversampling, i
  !! logical*1 :: oversampling
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowOversampling(key%spectralWindowId, opt%oversampling)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowOversampling', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowOversampling
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowOversampling(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowOversampling, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowOversampling(key%spectralWindowId, opt%oversampling)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowOversampling', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowOversampling
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowQuantization(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowQuantization, i
  !! logical*1 :: quantization
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowQuantization(key%spectralWindowId, opt%quantization)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowQuantization', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowQuantization
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowQuantization(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowQuantization, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowQuantization(key%spectralWindowId, opt%quantization)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowQuantization', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowQuantization
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowRefChan(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowRefChan, i
  !! real*8 :: refChan
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowRefChan(key%spectralWindowId, opt%refChan)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowRefChan', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowRefChan
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowRefChan(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowRefChan, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowRefChan(key%spectralWindowId, opt%refChan)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowRefChan', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowRefChan
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowResolution(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowResolution, i
  !! real*8 :: resolution
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowResolution(key%spectralWindowId, opt%resolution)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowResolution', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowResolution
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowResolution(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowResolution, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowResolution(key%spectralWindowId, opt%resolution)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowResolution', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowResolution
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowResolutionArray(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowResolutionArray, i
  !! real*8, allocatable :: resolutionArray(:)
  integer, parameter :: resolutionArrayRank = 1
  integer :: resolutionArrayDim(2,resolutionArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%resolutionArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%resolutionArray not allocated.')
    error = .true.
    return
  endif
  do i=1, resolutionArrayRank
    resolutionArrayDim(:,i) = size(opt%resolutionArray,i)
  enddo
  ireturn = sdm_addSpectralWindowResolutionArray(key%spectralWindowId, opt%resolutionArray, resolutionArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowResolutionArray', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowResolutionArray
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowResolutionArray(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowResolutionArray, i
  integer, parameter :: resolutionArrayRank = 1
  integer :: resolutionArrayDim(2,resolutionArrayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%resolutionArray)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%resolutionArray not allocated.')
    error = .true.
    return
  endif
  do i=1, resolutionArrayRank
    resolutionArrayDim(:,i) = size(opt%resolutionArray,i)
  enddo
  ireturn = sdm_getSpectralWindowResolutionArray(key%spectralWindowId, opt%resolutionArray, resolutionArrayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowResolutionArray', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowResolutionArray
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowNumAssocValues(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowNumAssocValues, i
  !! integer :: numAssocValues
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowNumAssocValues(key%spectralWindowId, opt%numAssocValues)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowNumAssocValues', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowNumAssocValues
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowNumAssocValues(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowNumAssocValues, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowNumAssocValues(key%spectralWindowId, opt%numAssocValues)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowNumAssocValues', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowNumAssocValues
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowAssocNature(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowAssocNature, i
  !! integer, allocatable :: assocNature(:)
  integer, parameter :: assocNatureRank = 1
  integer :: assocNatureDim(2,assocNatureRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocNature)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%assocNature not allocated.')
    error = .true.
    return
  endif
  do i=1, assocNatureRank
    assocNatureDim(:,i) = size(opt%assocNature,i)
  enddo
  ireturn = sdm_addSpectralWindowAssocNature(key%spectralWindowId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowAssocNature', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowAssocNature
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowAssocNature(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowAssocNature, i
  integer, parameter :: assocNatureRank = 1
  integer :: assocNatureDim(2,assocNatureRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocNature)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%assocNature not allocated.')
    error = .true.
    return
  endif
  do i=1, assocNatureRank
    assocNatureDim(:,i) = size(opt%assocNature,i)
  enddo
  ireturn = sdm_getSpectralWindowAssocNature(key%spectralWindowId, opt%assocNature, assocNatureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowAssocNature', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowAssocNature
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowAssocSpectralWindowId(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowAssocSpectralWindowId, i
  !! integer, allocatable :: assocSpectralWindowId(:)
  integer, parameter :: assocSpectralWindowIdRank = 1
  integer :: assocSpectralWindowIdDim(2,assocSpectralWindowIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocSpectralWindowId)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%assocSpectralWindowId not allocated.')
    error = .true.
    return
  endif
  do i=1, assocSpectralWindowIdRank
    assocSpectralWindowIdDim(:,i) = size(opt%assocSpectralWindowId,i)
  enddo
  ireturn = sdm_addSpectralWindowAssocSpectralWindowId(key%spectralWindowId, opt%assocSpectralWindowId,&
      & assocSpectralWindowIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowAssocSpectralWindowId', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowAssocSpectralWindowId
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowAssocSpectralWindowId(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowAssocSpectralWindowId, i
  integer, parameter :: assocSpectralWindowIdRank = 1
  integer :: assocSpectralWindowIdDim(2,assocSpectralWindowIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%assocSpectralWindowId)) then
    call sdmMessage(8,3,'SpectralWindowTable','opt%assocSpectralWindowId not allocated.')
    error = .true.
    return
  endif
  do i=1, assocSpectralWindowIdRank
    assocSpectralWindowIdDim(:,i) = size(opt%assocSpectralWindowId,i)
  enddo
  ireturn = sdm_getSpectralWindowAssocSpectralWindowId(key%spectralWindowId, opt%assocSpectralWindowId,&
      & assocSpectralWindowIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowAssocSpectralWindowId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowAssocSpectralWindowId
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowImageSpectralWindowId(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowImageSpectralWindowId, i
  !! integer :: imageSpectralWindowId
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowImageSpectralWindowId(key%spectralWindowId, opt%imageSpectralWindowId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowImageSpectralWindowId', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowImageSpectralWindowId
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowImageSpectralWindowId(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowImageSpectralWindowId, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowImageSpectralWindowId(key%spectralWindowId, opt%imageSpectralWindowId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowImageSpectralWindowId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowImageSpectralWindowId
!
!
! ---------------------------------------------------------------------------
!
subroutine addSpectralWindowDopplerId(key, opt, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSpectralWindowDopplerId, i
  !! integer :: dopplerId
  ! Deal with dimensions
  ireturn = sdm_addSpectralWindowDopplerId(key%spectralWindowId, opt%dopplerId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_addSpectralWindowDopplerId', ireturn)
    error = .true.
  endif
end subroutine addSpectralWindowDopplerId
!
! ---------------------------------------------------------------------------
!
subroutine getSpectralWindowDopplerId(key, opt, present, error)
  !
  use sdm_SpectralWindow
  !
  type(SpectralWindowKey) :: key
  type(SpectralWindowOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSpectralWindowDopplerId, i
  ! Deal with dimensions
  ireturn = sdm_getSpectralWindowDopplerId(key%spectralWindowId, opt%dopplerId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SpectralWindowTable','Error in sdm_getSpectralWindowDopplerId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSpectralWindowDopplerId
!
!
! ===========================================================================
!
! FreqOffset Table:
!
! ===========================================================================
!
subroutine addFreqOffsetRow(key, row, error)
  !
  use sdm_FreqOffset
  !
  type(FreqOffsetRow) :: row
  type(FreqOffsetKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFreqOffsetRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addFreqOffsetRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%offset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FreqOffsetTable','Error in sdm_addFreqOffsetRow', ireturn)
    error = .true.
  endif
end subroutine addFreqOffsetRow
!
! ---------------------------------------------------------------------------
!
subroutine getFreqOffsetRow(key, row, error)
  !
  use sdm_FreqOffset
  !
  type(FreqOffsetRow) :: row
  type(FreqOffsetKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFreqOffsetRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getFreqOffsetRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%offset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FreqOffsetTable','Error in sdm_getFreqOffsetRow', ireturn)
    error = .true.
  endif
end subroutine getFreqOffsetRow
!
! ---------------------------------------------------------------------------
!
subroutine getFreqOffsetTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFreqOffsetTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFreqOffsetTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFreqOffsetTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFreqOffsetKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_FreqOffset
  !
  integer :: tableSize
  logical :: error
  type(FreqOffsetKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFreqOffsetKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: feedIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(feedIdList(tableSize))
  !
  ireturn = sdm_getFreqOffsetKeys(antennaIdList, spectralWindowIdList, timeIntervalList, feedIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FreqOffsetTable','Error in sdm_getFreqOffsetKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%feedId = feedIdList(i)
  enddo
end subroutine getFreqOffsetKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Polarization Table:
!
! ===========================================================================
!
subroutine addPolarizationRow(key, row, error)
  !
  use sdm_Polarization
  !
  type(PolarizationRow) :: row
  type(PolarizationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addPolarizationRow, i, j
  integer, parameter :: corrTypeRank = 1
  integer :: corrTypeDim(2,corrTypeRank)
  integer, parameter :: corrProductRank = 2
  integer :: corrProductDim(2,corrProductRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%corrType)) then
    call sdmMessage(8,3,'PolarizationTable','row%corrType not allocated.')
    error = .true.
    return
  endif
  do i=1, corrTypeRank
    corrTypeDim(:,i) = size(row%corrType,i)
  enddo
  if (.not.allocated(row%corrProduct)) then
    call sdmMessage(8,3,'PolarizationTable','row%corrProduct not allocated.')
    error = .true.
    return
  endif
  do i=1, corrProductRank
    corrProductDim(:,i) = size(row%corrProduct,i)
  enddo
  !
  ireturn = sdm_addPolarizationRow(row%numCorr, row%corrType, corrTypeDim, row%corrProduct, corrProductDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PolarizationTable','Error in sdm_addPolarizationRow', ireturn)
    error = .true.
  else
    key%polarizationId = ireturn
  endif
end subroutine addPolarizationRow
!
! ---------------------------------------------------------------------------
!
subroutine getPolarizationRow(key, row, error)
  !
  use sdm_Polarization
  !
  type(PolarizationRow) :: row
  type(PolarizationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getPolarizationRow, i
  integer, parameter :: corrTypeRank = 1
  integer :: corrTypeDim(2,corrTypeRank)
  integer, parameter :: corrProductRank = 2
  integer :: corrProductDim(2,corrProductRank)
  ! Deal with dimensions
  if (.not.allocated(row%corrType)) then
    call sdmMessage(8,3,'PolarizationTable','row%corrType not allocated.')
    error = .true.
    return
  endif
  do i=1, corrTypeRank
    corrTypeDim(:,i) = size(row%corrType,i)
  enddo
  if (.not.allocated(row%corrProduct)) then
    call sdmMessage(8,3,'PolarizationTable','row%corrProduct not allocated.')
    error = .true.
    return
  endif
  do i=1, corrProductRank
    corrProductDim(:,i) = size(row%corrProduct,i)
  enddo
  !
  ireturn = sdm_getPolarizationRow(key%polarizationId, row%numCorr, row%corrType, corrTypeDim, row%corrProduct,&
      & corrProductDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PolarizationTable','Error in sdm_getPolarizationRow', ireturn)
    error = .true.
  endif
end subroutine getPolarizationRow
!
! ---------------------------------------------------------------------------
!
subroutine getPolarizationTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getPolarizationTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getPolarizationTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getPolarizationTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getPolarizationKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Polarization
  !
  integer :: tableSize
  logical :: error
  type(PolarizationKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getPolarizationKeys, i
  integer, allocatable :: polarizationIdList(:)
  !
  allocate(polarizationIdList(tableSize))
  !
  ireturn = sdm_getPolarizationKeys(polarizationIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PolarizationTable','Error in sdm_getPolarizationKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%polarizationId = polarizationIdList(i)
  enddo
end subroutine getPolarizationKeys
! ---------------------------------------------------------------------------
!
subroutine allocPolarizationRow(row, error)
  use sdm_Polarization
  type(PolarizationRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Polarization'
  ! row%corrType allocation
  if (allocated(row%corrType)) then
    deallocate(row%corrType, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%corrProduct allocation
  if (allocated(row%corrProduct)) then
    deallocate(row%corrProduct, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%corrType(row%numCorr),  row%corrProduct(2, row%numCorr), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocPolarizationRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addPolarizationFlagRow(key, opt, error)
  !
  use sdm_Polarization
  !
  type(PolarizationKey) :: key
  type(PolarizationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPolarizationFlagRow, i
  !! logical*1 :: flagRow
  ! Deal with dimensions
  ireturn = sdm_addPolarizationFlagRow(key%polarizationId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PolarizationTable','Error in sdm_addPolarizationFlagRow', ireturn)
    error = .true.
  endif
end subroutine addPolarizationFlagRow
!
! ---------------------------------------------------------------------------
!
subroutine getPolarizationFlagRow(key, opt, present, error)
  !
  use sdm_Polarization
  !
  type(PolarizationKey) :: key
  type(PolarizationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPolarizationFlagRow, i
  ! Deal with dimensions
  ireturn = sdm_getPolarizationFlagRow(key%polarizationId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PolarizationTable','Error in sdm_getPolarizationFlagRow', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPolarizationFlagRow
!
!
! ===========================================================================
!
! Receiver Table:
!
! ===========================================================================
!
subroutine addReceiverRow(key, row, error)
  !
  use sdm_Receiver
  !
  type(ReceiverRow) :: row
  type(ReceiverKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addReceiverRow, i, j
  integer :: nameDim(2)
  integer, parameter :: freqLORank = 1
  integer :: freqLODim(2,freqLORank)
  integer, parameter :: sidebandLORank = 1
  integer :: sidebandLODim(2,sidebandLORank)
  ! ----------------
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%freqLO)) then
    call sdmMessage(8,3,'ReceiverTable','row%freqLO not allocated.')
    error = .true.
    return
  endif
  do i=1, freqLORank
    freqLODim(:,i) = size(row%freqLO,i)
  enddo
  if (.not.allocated(row%sidebandLO)) then
    call sdmMessage(8,3,'ReceiverTable','row%sidebandLO not allocated.')
    error = .true.
    return
  endif
  do i=1, sidebandLORank
    sidebandLODim(:,i) = size(row%sidebandLO,i)
  enddo
  !
  ireturn = sdm_addReceiverRow(key%spectralWindowId, key%timeInterval, row%name, nameDim, row%numLO, row%frequencyBand,&
      & row%freqLO, freqLODim, row%receiverSideband, row%sidebandLO, sidebandLODim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ReceiverTable','Error in sdm_addReceiverRow', ireturn)
    error = .true.
  else
    key%receiverId = ireturn
  endif
end subroutine addReceiverRow
!
! ---------------------------------------------------------------------------
!
subroutine getReceiverRow(key, row, error)
  !
  use sdm_Receiver
  !
  type(ReceiverRow) :: row
  type(ReceiverKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getReceiverRow, i
  integer :: nameDim(2)
  integer, parameter :: freqLORank = 1
  integer :: freqLODim(2,freqLORank)
  integer, parameter :: sidebandLORank = 1
  integer :: sidebandLODim(2,sidebandLORank)
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%freqLO)) then
    call sdmMessage(8,3,'ReceiverTable','row%freqLO not allocated.')
    error = .true.
    return
  endif
  do i=1, freqLORank
    freqLODim(:,i) = size(row%freqLO,i)
  enddo
  if (.not.allocated(row%sidebandLO)) then
    call sdmMessage(8,3,'ReceiverTable','row%sidebandLO not allocated.')
    error = .true.
    return
  endif
  do i=1, sidebandLORank
    sidebandLODim(:,i) = size(row%sidebandLO,i)
  enddo
  !
  ireturn = sdm_getReceiverRow(key%receiverId, key%spectralWindowId, key%timeInterval, row%name, nameDim, row%numLO,&
      & row%frequencyBand, row%freqLO, freqLODim, row%receiverSideband, row%sidebandLO, sidebandLODim)
  row%name(nameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ReceiverTable','Error in sdm_getReceiverRow', ireturn)
    error = .true.
  endif
end subroutine getReceiverRow
!
! ---------------------------------------------------------------------------
!
subroutine getReceiverTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getReceiverTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getReceiverTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getReceiverTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getReceiverKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Receiver
  !
  integer :: tableSize
  logical :: error
  type(ReceiverKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getReceiverKeys, i
  integer, allocatable :: receiverIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(receiverIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getReceiverKeys(receiverIdList, spectralWindowIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ReceiverTable','Error in sdm_getReceiverKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%receiverId = receiverIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getReceiverKeys
! ---------------------------------------------------------------------------
!
subroutine allocReceiverRow(row, error)
  use sdm_Receiver
  type(ReceiverRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Receiver'
  ! row%freqLO allocation
  if (allocated(row%freqLO)) then
    deallocate(row%freqLO, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%sidebandLO allocation
  if (allocated(row%sidebandLO)) then
    deallocate(row%sidebandLO, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%freqLO(row%numLO),  row%sidebandLO(row%numLO), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocReceiverRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Doppler Table:
!
! ===========================================================================
!
subroutine addDopplerRow(key, row, error)
  !
  use sdm_Doppler
  !
  type(DopplerRow) :: row
  type(DopplerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addDopplerRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addDopplerRow(key%sourceId, row%transitionIndex, row%velDef)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DopplerTable','Error in sdm_addDopplerRow', ireturn)
    error = .true.
  else
    key%dopplerId = ireturn
  endif
end subroutine addDopplerRow
!
! ---------------------------------------------------------------------------
!
subroutine getDopplerRow(key, row, error)
  !
  use sdm_Doppler
  !
  type(DopplerRow) :: row
  type(DopplerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getDopplerRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getDopplerRow(key%dopplerId, key%sourceId, row%transitionIndex, row%velDef)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DopplerTable','Error in sdm_getDopplerRow', ireturn)
    error = .true.
  endif
end subroutine getDopplerRow
!
! ---------------------------------------------------------------------------
!
subroutine getDopplerTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getDopplerTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getDopplerTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getDopplerTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getDopplerKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Doppler
  !
  integer :: tableSize
  logical :: error
  type(DopplerKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getDopplerKeys, i
  integer, allocatable :: dopplerIdList(:)
  integer, allocatable :: sourceIdList(:)
  !
  allocate(dopplerIdList(tableSize))
  allocate(sourceIdList(tableSize))
  !
  ireturn = sdm_getDopplerKeys(dopplerIdList, sourceIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DopplerTable','Error in sdm_getDopplerKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%dopplerId = dopplerIdList(i)
    keyList(i)%sourceId = sourceIdList(i)
  enddo
end subroutine getDopplerKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Processor Table:
!
! ===========================================================================
!
subroutine addProcessorRow(key, row, error)
  !
  use sdm_Processor
  !
  type(ProcessorRow) :: row
  type(ProcessorKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addProcessorRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addProcessorRow(row%modeId, row%processorType, row%processorSubType)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ProcessorTable','Error in sdm_addProcessorRow', ireturn)
    error = .true.
  else
    key%processorId = ireturn
  endif
end subroutine addProcessorRow
!
! ---------------------------------------------------------------------------
!
subroutine getProcessorRow(key, row, error)
  !
  use sdm_Processor
  !
  type(ProcessorRow) :: row
  type(ProcessorKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getProcessorRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getProcessorRow(key%processorId, row%modeId, row%processorType, row%processorSubType)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ProcessorTable','Error in sdm_getProcessorRow', ireturn)
    error = .true.
  endif
end subroutine getProcessorRow
!
! ---------------------------------------------------------------------------
!
subroutine getProcessorTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getProcessorTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getProcessorTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getProcessorTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getProcessorKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Processor
  !
  integer :: tableSize
  logical :: error
  type(ProcessorKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getProcessorKeys, i
  integer, allocatable :: processorIdList(:)
  !
  allocate(processorIdList(tableSize))
  !
  ireturn = sdm_getProcessorKeys(processorIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ProcessorTable','Error in sdm_getProcessorKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%processorId = processorIdList(i)
  enddo
end subroutine getProcessorKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CorrelatorMode Table:
!
! ===========================================================================
!
subroutine addCorrelatorModeRow(key, row, error)
  !
  use sdm_CorrelatorMode
  !
  type(CorrelatorModeRow) :: row
  type(CorrelatorModeKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCorrelatorModeRow, i, j
  integer, parameter :: basebandNamesRank = 1
  integer :: basebandNamesDim(2,basebandNamesRank)
  integer, parameter :: basebandConfigRank = 1
  integer :: basebandConfigDim(2,basebandConfigRank)
  integer, parameter :: axesOrderArrayRank = 1
  integer :: axesOrderArrayDim(2,axesOrderArrayRank)
  integer, parameter :: filterModeRank = 1
  integer :: filterModeDim(2,filterModeRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%basebandNames)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%basebandNames not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandNamesRank
    basebandNamesDim(:,i) = size(row%basebandNames,i)
  enddo
  if (.not.allocated(row%basebandConfig)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%basebandConfig not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandConfigRank
    basebandConfigDim(:,i) = size(row%basebandConfig,i)
  enddo
  if (.not.allocated(row%axesOrderArray)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%axesOrderArray not allocated.')
    error = .true.
    return
  endif
  do i=1, axesOrderArrayRank
    axesOrderArrayDim(:,i) = size(row%axesOrderArray,i)
  enddo
  if (.not.allocated(row%filterMode)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%filterMode not allocated.')
    error = .true.
    return
  endif
  do i=1, filterModeRank
    filterModeDim(:,i) = size(row%filterMode,i)
  enddo
  !
  ireturn = sdm_addCorrelatorModeRow(row%numBaseband, row%basebandNames, basebandNamesDim, row%basebandConfig,&
      & basebandConfigDim, row%accumMode, row%binMode, row%numAxes, row%axesOrderArray, axesOrderArrayDim,&
      & row%filterMode, filterModeDim, row%correlatorName)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CorrelatorModeTable','Error in sdm_addCorrelatorModeRow', ireturn)
    error = .true.
  else
    key%correlatorModeId = ireturn
  endif
end subroutine addCorrelatorModeRow
!
! ---------------------------------------------------------------------------
!
subroutine getCorrelatorModeRow(key, row, error)
  !
  use sdm_CorrelatorMode
  !
  type(CorrelatorModeRow) :: row
  type(CorrelatorModeKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCorrelatorModeRow, i
  integer, parameter :: basebandNamesRank = 1
  integer :: basebandNamesDim(2,basebandNamesRank)
  integer, parameter :: basebandConfigRank = 1
  integer :: basebandConfigDim(2,basebandConfigRank)
  integer, parameter :: axesOrderArrayRank = 1
  integer :: axesOrderArrayDim(2,axesOrderArrayRank)
  integer, parameter :: filterModeRank = 1
  integer :: filterModeDim(2,filterModeRank)
  ! Deal with dimensions
  if (.not.allocated(row%basebandNames)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%basebandNames not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandNamesRank
    basebandNamesDim(:,i) = size(row%basebandNames,i)
  enddo
  if (.not.allocated(row%basebandConfig)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%basebandConfig not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandConfigRank
    basebandConfigDim(:,i) = size(row%basebandConfig,i)
  enddo
  if (.not.allocated(row%axesOrderArray)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%axesOrderArray not allocated.')
    error = .true.
    return
  endif
  do i=1, axesOrderArrayRank
    axesOrderArrayDim(:,i) = size(row%axesOrderArray,i)
  enddo
  if (.not.allocated(row%filterMode)) then
    call sdmMessage(8,3,'CorrelatorModeTable','row%filterMode not allocated.')
    error = .true.
    return
  endif
  do i=1, filterModeRank
    filterModeDim(:,i) = size(row%filterMode,i)
  enddo
  !
  ireturn = sdm_getCorrelatorModeRow(key%correlatorModeId, row%numBaseband, row%basebandNames, basebandNamesDim,&
      & row%basebandConfig, basebandConfigDim, row%accumMode, row%binMode, row%numAxes, row%axesOrderArray,&
      & axesOrderArrayDim, row%filterMode, filterModeDim, row%correlatorName)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CorrelatorModeTable','Error in sdm_getCorrelatorModeRow', ireturn)
    error = .true.
  endif
end subroutine getCorrelatorModeRow
!
! ---------------------------------------------------------------------------
!
subroutine getCorrelatorModeTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCorrelatorModeTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCorrelatorModeTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCorrelatorModeTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCorrelatorModeKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CorrelatorMode
  !
  integer :: tableSize
  logical :: error
  type(CorrelatorModeKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCorrelatorModeKeys, i
  integer, allocatable :: correlatorModeIdList(:)
  !
  allocate(correlatorModeIdList(tableSize))
  !
  ireturn = sdm_getCorrelatorModeKeys(correlatorModeIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CorrelatorModeTable','Error in sdm_getCorrelatorModeKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%correlatorModeId = correlatorModeIdList(i)
  enddo
end subroutine getCorrelatorModeKeys
! ---------------------------------------------------------------------------
!
subroutine allocCorrelatorModeRow(row, error)
  use sdm_CorrelatorMode
  type(CorrelatorModeRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CorrelatorMode'
  ! row%basebandNames allocation
  if (allocated(row%basebandNames)) then
    deallocate(row%basebandNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%basebandConfig allocation
  if (allocated(row%basebandConfig)) then
    deallocate(row%basebandConfig, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%axesOrderArray allocation
  if (allocated(row%axesOrderArray)) then
    deallocate(row%axesOrderArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%filterMode allocation
  if (allocated(row%filterMode)) then
    deallocate(row%filterMode, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%basebandNames(row%numBaseband), row%basebandConfig(row%numBaseband), row%axesOrderArray(row%numAxes),&
      & row%filterMode(row%numBaseband), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCorrelatorModeRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalDevice Table:
!
! ===========================================================================
!
subroutine addCalDeviceRow(key, row, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceRow) :: row
  type(CalDeviceKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalDeviceRow, i, j
  integer, parameter :: calLoadNamesRank = 1
  integer :: calLoadNamesDim(2,calLoadNamesRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%calLoadNames)) then
    call sdmMessage(8,3,'CalDeviceTable','row%calLoadNames not allocated.')
    error = .true.
    return
  endif
  do i=1, calLoadNamesRank
    calLoadNamesDim(:,i) = size(row%calLoadNames,i)
  enddo
  !
  ireturn = sdm_addCalDeviceRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%numCalload,&
      & row%calLoadNames, calLoadNamesDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_addCalDeviceRow', ireturn)
    error = .true.
  endif
end subroutine addCalDeviceRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceRow(key, row, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceRow) :: row
  type(CalDeviceKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalDeviceRow, i
  integer, parameter :: calLoadNamesRank = 1
  integer :: calLoadNamesDim(2,calLoadNamesRank)
  ! Deal with dimensions
  if (.not.allocated(row%calLoadNames)) then
    call sdmMessage(8,3,'CalDeviceTable','row%calLoadNames not allocated.')
    error = .true.
    return
  endif
  do i=1, calLoadNamesRank
    calLoadNamesDim(:,i) = size(row%calLoadNames,i)
  enddo
  !
  ireturn = sdm_getCalDeviceRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%numCalload,&
      & row%calLoadNames, calLoadNamesDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceRow', ireturn)
    error = .true.
  endif
end subroutine getCalDeviceRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalDeviceTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalDeviceTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalDeviceTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalDevice
  !
  integer :: tableSize
  logical :: error
  type(CalDeviceKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalDeviceKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: feedIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(feedIdList(tableSize))
  !
  ireturn = sdm_getCalDeviceKeys(antennaIdList, spectralWindowIdList, timeIntervalList, feedIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%feedId = feedIdList(i)
  enddo
end subroutine getCalDeviceKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalDeviceRow(row, error)
  use sdm_CalDevice
  type(CalDeviceRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalDevice'
  ! row%calLoadNames allocation
  if (allocated(row%calLoadNames)) then
    deallocate(row%calLoadNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%calLoadNames(row%numCalload), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDeviceRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalDeviceOpt(row, opt, error)
  use sdm_CalDevice
  type(CalDeviceRow) :: row
  type(CalDeviceOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalDevice'
  ! opt%calEff allocation
  if (allocated(opt%calEff)) then
    deallocate(opt%calEff, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%noiseCal allocation
  if (allocated(opt%noiseCal)) then
    deallocate(opt%noiseCal, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%temperatureLoad allocation
  if (allocated(opt%temperatureLoad)) then
    deallocate(opt%temperatureLoad, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%calEff(row%numCalload, opt%numReceptor), opt%noiseCal(row%numCalload),&
      & opt%temperatureLoad(row%numCalload), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDeviceOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalDeviceNumReceptor(key, opt, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDeviceNumReceptor, i
  !! integer :: numReceptor
  ! Deal with dimensions
  ireturn = sdm_addCalDeviceNumReceptor(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%numReceptor)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_addCalDeviceNumReceptor', ireturn)
    error = .true.
  endif
end subroutine addCalDeviceNumReceptor
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceNumReceptor(key, opt, present, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDeviceNumReceptor, i
  ! Deal with dimensions
  ireturn = sdm_getCalDeviceNumReceptor(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%numReceptor)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceNumReceptor', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDeviceNumReceptor
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDeviceCalEff(key, opt, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDeviceCalEff, i
  !! real, allocatable :: calEff(:,:)
  integer, parameter :: calEffRank = 2
  integer :: calEffDim(2,calEffRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calEff)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%calEff not allocated.')
    error = .true.
    return
  endif
  do i=1, calEffRank
    calEffDim(:,i) = size(opt%calEff,i)
  enddo
  ireturn = sdm_addCalDeviceCalEff(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%calEff,&
      & calEffDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_addCalDeviceCalEff', ireturn)
    error = .true.
  endif
end subroutine addCalDeviceCalEff
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceCalEff(key, opt, present, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDeviceCalEff, i
  integer, parameter :: calEffRank = 2
  integer :: calEffDim(2,calEffRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calEff)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%calEff not allocated.')
    error = .true.
    return
  endif
  do i=1, calEffRank
    calEffDim(:,i) = size(opt%calEff,i)
  enddo
  ireturn = sdm_getCalDeviceCalEff(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%calEff,&
      & calEffDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceCalEff', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDeviceCalEff
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDeviceNoiseCal(key, opt, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDeviceNoiseCal, i
  !! real*8, allocatable :: noiseCal(:)
  integer, parameter :: noiseCalRank = 1
  integer :: noiseCalDim(2,noiseCalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%noiseCal)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%noiseCal not allocated.')
    error = .true.
    return
  endif
  do i=1, noiseCalRank
    noiseCalDim(:,i) = size(opt%noiseCal,i)
  enddo
  ireturn = sdm_addCalDeviceNoiseCal(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%noiseCal,&
      & noiseCalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_addCalDeviceNoiseCal', ireturn)
    error = .true.
  endif
end subroutine addCalDeviceNoiseCal
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceNoiseCal(key, opt, present, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDeviceNoiseCal, i
  integer, parameter :: noiseCalRank = 1
  integer :: noiseCalDim(2,noiseCalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%noiseCal)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%noiseCal not allocated.')
    error = .true.
    return
  endif
  do i=1, noiseCalRank
    noiseCalDim(:,i) = size(opt%noiseCal,i)
  enddo
  ireturn = sdm_getCalDeviceNoiseCal(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%noiseCal,&
      & noiseCalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceNoiseCal', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDeviceNoiseCal
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDeviceTemperatureLoad(key, opt, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDeviceTemperatureLoad, i
  !! real*8, allocatable :: temperatureLoad(:)
  integer, parameter :: temperatureLoadRank = 1
  integer :: temperatureLoadDim(2,temperatureLoadRank)
  ! Deal with dimensions
  if (.not.allocated(opt%temperatureLoad)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%temperatureLoad not allocated.')
    error = .true.
    return
  endif
  do i=1, temperatureLoadRank
    temperatureLoadDim(:,i) = size(opt%temperatureLoad,i)
  enddo
  ireturn = sdm_addCalDeviceTemperatureLoad(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%temperatureLoad, temperatureLoadDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_addCalDeviceTemperatureLoad', ireturn)
    error = .true.
  endif
end subroutine addCalDeviceTemperatureLoad
!
! ---------------------------------------------------------------------------
!
subroutine getCalDeviceTemperatureLoad(key, opt, present, error)
  !
  use sdm_CalDevice
  !
  type(CalDeviceKey) :: key
  type(CalDeviceOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDeviceTemperatureLoad, i
  integer, parameter :: temperatureLoadRank = 1
  integer :: temperatureLoadDim(2,temperatureLoadRank)
  ! Deal with dimensions
  if (.not.allocated(opt%temperatureLoad)) then
    call sdmMessage(8,3,'CalDeviceTable','opt%temperatureLoad not allocated.')
    error = .true.
    return
  endif
  do i=1, temperatureLoadRank
    temperatureLoadDim(:,i) = size(opt%temperatureLoad,i)
  enddo
  ireturn = sdm_getCalDeviceTemperatureLoad(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%temperatureLoad, temperatureLoadDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDeviceTable','Error in sdm_getCalDeviceTemperatureLoad', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDeviceTemperatureLoad
!
!
! ===========================================================================
!
! FlagCmd Table:
!
! ===========================================================================
!
subroutine addFlagCmdRow(key, row, error)
  !
  use sdm_FlagCmd
  !
  type(FlagCmdRow) :: row
  type(FlagCmdKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFlagCmdRow, i, j
  integer :: typeDim(2)
  integer :: reasonDim(2)
  integer :: commandDim(2)
  ! ----------------
  ! Deal with dimensions
  typeDim = len(row%type)
  call charcut(row%type)
  reasonDim = len(row%reason)
  call charcut(row%reason)
  commandDim = len(row%command)
  call charcut(row%command)
  !
  ireturn = sdm_addFlagCmdRow(key%timeInterval, row%type, typeDim, row%reason, reasonDim, row%level, row%severity,&
      & row%applied, row%command, commandDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FlagCmdTable','Error in sdm_addFlagCmdRow', ireturn)
    error = .true.
  endif
end subroutine addFlagCmdRow
!
! ---------------------------------------------------------------------------
!
subroutine getFlagCmdRow(key, row, error)
  !
  use sdm_FlagCmd
  !
  type(FlagCmdRow) :: row
  type(FlagCmdKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFlagCmdRow, i
  integer :: typeDim(2)
  integer :: reasonDim(2)
  integer :: commandDim(2)
  ! Deal with dimensions
  typeDim = len(row%type)
  call charcut(row%type)
  reasonDim = len(row%reason)
  call charcut(row%reason)
  commandDim = len(row%command)
  call charcut(row%command)
  !
  ireturn = sdm_getFlagCmdRow(key%timeInterval, row%type, typeDim, row%reason, reasonDim, row%level, row%severity,&
      & row%applied, row%command, commandDim)
  row%type(typeDim(1)+1:) = ''
  row%reason(reasonDim(1)+1:) = ''
  row%command(commandDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FlagCmdTable','Error in sdm_getFlagCmdRow', ireturn)
    error = .true.
  endif
end subroutine getFlagCmdRow
!
! ---------------------------------------------------------------------------
!
subroutine getFlagCmdTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFlagCmdTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFlagCmdTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFlagCmdTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFlagCmdKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_FlagCmd
  !
  integer :: tableSize
  logical :: error
  type(FlagCmdKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFlagCmdKeys, i
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getFlagCmdKeys(timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FlagCmdTable','Error in sdm_getFlagCmdKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getFlagCmdKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Focus Table:
!
! ===========================================================================
!
subroutine addFocusRow(key, row, error)
  !
  use sdm_Focus
  !
  type(FocusRow) :: row
  type(FocusKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFocusRow, i, j
  integer, parameter :: focusOffsetRank = 1
  integer :: focusOffsetDim(2,focusOffsetRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%focusOffset)) then
    call sdmMessage(8,3,'FocusTable','row%focusOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, focusOffsetRank
    focusOffsetDim(:,i) = size(row%focusOffset,i)
  enddo
  !
  ireturn = sdm_addFocusRow(key%antennaId, key%timeInterval, row%focusTracking, row%focusOffset, focusOffsetDim,&
      & row%focusModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusTable','Error in sdm_addFocusRow', ireturn)
    error = .true.
  endif
end subroutine addFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine getFocusRow(key, row, error)
  !
  use sdm_Focus
  !
  type(FocusRow) :: row
  type(FocusKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFocusRow, i
  integer, parameter :: focusOffsetRank = 1
  integer :: focusOffsetDim(2,focusOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(row%focusOffset)) then
    call sdmMessage(8,3,'FocusTable','row%focusOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, focusOffsetRank
    focusOffsetDim(:,i) = size(row%focusOffset,i)
  enddo
  !
  ireturn = sdm_getFocusRow(key%antennaId, key%timeInterval, row%focusTracking, row%focusOffset, focusOffsetDim,&
      & row%focusModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusTable','Error in sdm_getFocusRow', ireturn)
    error = .true.
  endif
end subroutine getFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine getFocusTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFocusTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFocusTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFocusTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFocusKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Focus
  !
  integer :: tableSize
  logical :: error
  type(FocusKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFocusKeys, i
  integer, allocatable :: antennaIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getFocusKeys(antennaIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusTable','Error in sdm_getFocusKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getFocusKeys
! ---------------------------------------------------------------------------
!
subroutine allocFocusRow(row, error)
  use sdm_Focus
  type(FocusRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Focus'
  ! row%focusOffset allocation
  if (allocated(row%focusOffset)) then
    deallocate(row%focusOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%focusOffset(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine allocFocusOpt(row, opt, error)
  use sdm_Focus
  type(FocusRow) :: row
  type(FocusOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'Focus'
  ! opt%measuredFocusPosition allocation
  if (allocated(opt%measuredFocusPosition)) then
    deallocate(opt%measuredFocusPosition, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%measuredFocusPosition(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFocusOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addFocusMeasuredFocusPosition(key, opt, error)
  !
  use sdm_Focus
  !
  type(FocusKey) :: key
  type(FocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addFocusMeasuredFocusPosition, i
  !! real*8, allocatable :: measuredFocusPosition(:)
  integer, parameter :: measuredFocusPositionRank = 1
  integer :: measuredFocusPositionDim(2,measuredFocusPositionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%measuredFocusPosition)) then
    call sdmMessage(8,3,'FocusTable','opt%measuredFocusPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, measuredFocusPositionRank
    measuredFocusPositionDim(:,i) = size(opt%measuredFocusPosition,i)
  enddo
  ireturn = sdm_addFocusMeasuredFocusPosition(key%antennaId, key%timeInterval, opt%measuredFocusPosition,&
      & measuredFocusPositionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusTable','Error in sdm_addFocusMeasuredFocusPosition', ireturn)
    error = .true.
  endif
end subroutine addFocusMeasuredFocusPosition
!
! ---------------------------------------------------------------------------
!
subroutine getFocusMeasuredFocusPosition(key, opt, present, error)
  !
  use sdm_Focus
  !
  type(FocusKey) :: key
  type(FocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getFocusMeasuredFocusPosition, i
  integer, parameter :: measuredFocusPositionRank = 1
  integer :: measuredFocusPositionDim(2,measuredFocusPositionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%measuredFocusPosition)) then
    call sdmMessage(8,3,'FocusTable','opt%measuredFocusPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, measuredFocusPositionRank
    measuredFocusPositionDim(:,i) = size(opt%measuredFocusPosition,i)
  enddo
  ireturn = sdm_getFocusMeasuredFocusPosition(key%antennaId, key%timeInterval, opt%measuredFocusPosition,&
      & measuredFocusPositionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusTable','Error in sdm_getFocusMeasuredFocusPosition', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getFocusMeasuredFocusPosition
!
!
! ===========================================================================
!
! History Table:
!
! ===========================================================================
!
subroutine addHistoryRow(key, row, error)
  !
  use sdm_History
  !
  type(HistoryRow) :: row
  type(HistoryKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addHistoryRow, i, j
  integer :: messageDim(2)
  integer :: priorityDim(2)
  integer :: originDim(2)
  integer :: objectIdDim(2)
  integer :: applicationDim(2)
  integer :: cliCommandDim(2)
  integer :: appParmsDim(2)
  ! ----------------
  ! Deal with dimensions
  messageDim = len(row%message)
  call charcut(row%message)
  priorityDim = len(row%priority)
  call charcut(row%priority)
  originDim = len(row%origin)
  call charcut(row%origin)
  objectIdDim = len(row%objectId)
  call charcut(row%objectId)
  applicationDim = len(row%application)
  call charcut(row%application)
  cliCommandDim = len(row%cliCommand)
  call charcut(row%cliCommand)
  appParmsDim = len(row%appParms)
  call charcut(row%appParms)
  !
  ireturn = sdm_addHistoryRow(key%execBlockId, key%time, row%message, messageDim, row%priority, priorityDim,&
      & row%origin, originDim, row%objectId, objectIdDim, row%application, applicationDim, row%cliCommand,&
      & cliCommandDim, row%appParms, appParmsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HistoryTable','Error in sdm_addHistoryRow', ireturn)
    error = .true.
  endif
end subroutine addHistoryRow
!
! ---------------------------------------------------------------------------
!
subroutine getHistoryRow(key, row, error)
  !
  use sdm_History
  !
  type(HistoryRow) :: row
  type(HistoryKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getHistoryRow, i
  integer :: messageDim(2)
  integer :: priorityDim(2)
  integer :: originDim(2)
  integer :: objectIdDim(2)
  integer :: applicationDim(2)
  integer :: cliCommandDim(2)
  integer :: appParmsDim(2)
  ! Deal with dimensions
  messageDim = len(row%message)
  call charcut(row%message)
  priorityDim = len(row%priority)
  call charcut(row%priority)
  originDim = len(row%origin)
  call charcut(row%origin)
  objectIdDim = len(row%objectId)
  call charcut(row%objectId)
  applicationDim = len(row%application)
  call charcut(row%application)
  cliCommandDim = len(row%cliCommand)
  call charcut(row%cliCommand)
  appParmsDim = len(row%appParms)
  call charcut(row%appParms)
  !
  ireturn = sdm_getHistoryRow(key%execBlockId, key%time, row%message, messageDim, row%priority, priorityDim,&
      & row%origin, originDim, row%objectId, objectIdDim, row%application, applicationDim, row%cliCommand,&
      & cliCommandDim, row%appParms, appParmsDim)
  row%message(messageDim(1)+1:) = ''
  row%priority(priorityDim(1)+1:) = ''
  row%origin(originDim(1)+1:) = ''
  row%objectId(objectIdDim(1)+1:) = ''
  row%application(applicationDim(1)+1:) = ''
  row%cliCommand(cliCommandDim(1)+1:) = ''
  row%appParms(appParmsDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HistoryTable','Error in sdm_getHistoryRow', ireturn)
    error = .true.
  endif
end subroutine getHistoryRow
!
! ---------------------------------------------------------------------------
!
subroutine getHistoryTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getHistoryTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getHistoryTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getHistoryTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getHistoryKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_History
  !
  integer :: tableSize
  logical :: error
  type(HistoryKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getHistoryKeys, i
  integer, allocatable :: execBlockIdList(:)
  integer*8, allocatable :: timeList(:)
  !
  allocate(execBlockIdList(tableSize))
  allocate(timeList(tableSize))
  !
  ireturn = sdm_getHistoryKeys(execBlockIdList, timeList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HistoryTable','Error in sdm_getHistoryKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%execBlockId = execBlockIdList(i)
    keyList(i)%time = timeList(i)
  enddo
end subroutine getHistoryKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Pointing Table:
!
! ===========================================================================
!
subroutine addPointingRow(key, row, error)
  !
  use sdm_Pointing
  !
  type(PointingRow) :: row
  type(PointingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addPointingRow, i, j
  integer, parameter :: encoderRank = 2
  integer :: encoderDim(2,encoderRank)
  integer, parameter :: pointingDirectionRank = 2
  integer :: pointingDirectionDim(2,pointingDirectionRank)
  integer, parameter :: targetRank = 2
  integer :: targetDim(2,targetRank)
  integer, parameter :: offsetRank = 2
  integer :: offsetDim(2,offsetRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%encoder)) then
    call sdmMessage(8,3,'PointingTable','row%encoder not allocated.')
    error = .true.
    return
  endif
  do i=1, encoderRank
    encoderDim(:,i) = size(row%encoder,i)
  enddo
  if (.not.allocated(row%pointingDirection)) then
    call sdmMessage(8,3,'PointingTable','row%pointingDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, pointingDirectionRank
    pointingDirectionDim(:,i) = size(row%pointingDirection,i)
  enddo
  if (.not.allocated(row%target)) then
    call sdmMessage(8,3,'PointingTable','row%target not allocated.')
    error = .true.
    return
  endif
  do i=1, targetRank
    targetDim(:,i) = size(row%target,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'PointingTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  !
  ireturn = sdm_addPointingRow(key%antennaId, key%timeInterval, row%numSample, row%encoder, encoderDim,&
      & row%pointingTracking, row%usePolynomials, row%timeOrigin, row%numTerm, row%pointingDirection,&
      & pointingDirectionDim, row%target, targetDim, row%offset, offsetDim, row%pointingModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingRow', ireturn)
    error = .true.
  endif
end subroutine addPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine getPointingRow(key, row, error)
  !
  use sdm_Pointing
  !
  type(PointingRow) :: row
  type(PointingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getPointingRow, i
  integer, parameter :: encoderRank = 2
  integer :: encoderDim(2,encoderRank)
  integer, parameter :: pointingDirectionRank = 2
  integer :: pointingDirectionDim(2,pointingDirectionRank)
  integer, parameter :: targetRank = 2
  integer :: targetDim(2,targetRank)
  integer, parameter :: offsetRank = 2
  integer :: offsetDim(2,offsetRank)
  ! Deal with dimensions
  if (.not.allocated(row%encoder)) then
    call sdmMessage(8,3,'PointingTable','row%encoder not allocated.')
    error = .true.
    return
  endif
  do i=1, encoderRank
    encoderDim(:,i) = size(row%encoder,i)
  enddo
  if (.not.allocated(row%pointingDirection)) then
    call sdmMessage(8,3,'PointingTable','row%pointingDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, pointingDirectionRank
    pointingDirectionDim(:,i) = size(row%pointingDirection,i)
  enddo
  if (.not.allocated(row%target)) then
    call sdmMessage(8,3,'PointingTable','row%target not allocated.')
    error = .true.
    return
  endif
  do i=1, targetRank
    targetDim(:,i) = size(row%target,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'PointingTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  !
  ireturn = sdm_getPointingRow(key%antennaId, key%timeInterval, row%numSample, row%encoder, encoderDim,&
      & row%pointingTracking, row%usePolynomials, row%timeOrigin, row%numTerm, row%pointingDirection,&
      & pointingDirectionDim, row%target, targetDim, row%offset, offsetDim, row%pointingModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingRow', ireturn)
    error = .true.
  endif
end subroutine getPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine getPointingTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getPointingTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getPointingTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getPointingTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getPointingKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Pointing
  !
  integer :: tableSize
  logical :: error
  type(PointingKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getPointingKeys, i
  integer, allocatable :: antennaIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getPointingKeys(antennaIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getPointingKeys
! ---------------------------------------------------------------------------
!
subroutine allocPointingRow(row, error)
  use sdm_Pointing
  type(PointingRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Pointing'
  ! row%encoder allocation
  if (allocated(row%encoder)) then
    deallocate(row%encoder, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%pointingDirection allocation
  if (allocated(row%pointingDirection)) then
    deallocate(row%pointingDirection, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%target allocation
  if (allocated(row%target)) then
    deallocate(row%target, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%offset allocation
  if (allocated(row%offset)) then
    deallocate(row%offset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%encoder(2, row%numSample), row%pointingDirection(2, row%numTerm), row%target(2, row%numTerm),&
      & row%offset(2, row%numTerm), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine allocPointingOpt(row, opt, error)
  use sdm_Pointing
  type(PointingRow) :: row
  type(PointingOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'Pointing'
  ! opt%sourceOffset allocation
  if (allocated(opt%sourceOffset)) then
    deallocate(opt%sourceOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sampledTimeInterval allocation
  if (allocated(opt%sampledTimeInterval)) then
    deallocate(opt%sampledTimeInterval, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%sourceOffset(2, row%numTerm),  opt%sampledTimeInterval(row%numSample), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocPointingOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addPointingOverTheTop(key, opt, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingOverTheTop, i
  !! logical*1 :: overTheTop
  ! Deal with dimensions
  ireturn = sdm_addPointingOverTheTop(key%antennaId, key%timeInterval, opt%overTheTop)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingOverTheTop', ireturn)
    error = .true.
  endif
end subroutine addPointingOverTheTop
!
! ---------------------------------------------------------------------------
!
subroutine getPointingOverTheTop(key, opt, present, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingOverTheTop, i
  ! Deal with dimensions
  ireturn = sdm_getPointingOverTheTop(key%antennaId, key%timeInterval, opt%overTheTop)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingOverTheTop', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingOverTheTop
!
!
! ---------------------------------------------------------------------------
!
subroutine addPointingSourceOffset(key, opt, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingSourceOffset, i
  !! real*8, allocatable :: sourceOffset(:,:)
  integer, parameter :: sourceOffsetRank = 2
  integer :: sourceOffsetDim(2,sourceOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceOffset)) then
    call sdmMessage(8,3,'PointingTable','opt%sourceOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, sourceOffsetRank
    sourceOffsetDim(:,i) = size(opt%sourceOffset,i)
  enddo
  ireturn = sdm_addPointingSourceOffset(key%antennaId, key%timeInterval, opt%sourceOffset, sourceOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingSourceOffset', ireturn)
    error = .true.
  endif
end subroutine addPointingSourceOffset
!
! ---------------------------------------------------------------------------
!
subroutine getPointingSourceOffset(key, opt, present, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingSourceOffset, i
  integer, parameter :: sourceOffsetRank = 2
  integer :: sourceOffsetDim(2,sourceOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceOffset)) then
    call sdmMessage(8,3,'PointingTable','opt%sourceOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, sourceOffsetRank
    sourceOffsetDim(:,i) = size(opt%sourceOffset,i)
  enddo
  ireturn = sdm_getPointingSourceOffset(key%antennaId, key%timeInterval, opt%sourceOffset, sourceOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingSourceOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingSourceOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addPointingSourceOffsetReferenceCode(key, opt, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingSourceOffsetReferenceCode, i
  !! integer :: sourceOffsetReferenceCode
  ! Deal with dimensions
  ireturn = sdm_addPointingSourceOffsetReferenceCode(key%antennaId, key%timeInterval, opt%sourceOffsetReferenceCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingSourceOffsetReferenceCode', ireturn)
    error = .true.
  endif
end subroutine addPointingSourceOffsetReferenceCode
!
! ---------------------------------------------------------------------------
!
subroutine getPointingSourceOffsetReferenceCode(key, opt, present, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingSourceOffsetReferenceCode, i
  ! Deal with dimensions
  ireturn = sdm_getPointingSourceOffsetReferenceCode(key%antennaId, key%timeInterval, opt%sourceOffsetReferenceCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingSourceOffsetReferenceCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingSourceOffsetReferenceCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addPointingSourceOffsetEquinox(key, opt, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingSourceOffsetEquinox, i
  !! integer*8 :: sourceOffsetEquinox
  ! Deal with dimensions
  ireturn = sdm_addPointingSourceOffsetEquinox(key%antennaId, key%timeInterval, opt%sourceOffsetEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingSourceOffsetEquinox', ireturn)
    error = .true.
  endif
end subroutine addPointingSourceOffsetEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getPointingSourceOffsetEquinox(key, opt, present, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingSourceOffsetEquinox, i
  ! Deal with dimensions
  ireturn = sdm_getPointingSourceOffsetEquinox(key%antennaId, key%timeInterval, opt%sourceOffsetEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingSourceOffsetEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingSourceOffsetEquinox
!
!
! ---------------------------------------------------------------------------
!
subroutine addPointingSampledTimeInterval(key, opt, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingSampledTimeInterval, i
  !! type(ArrayTimeInterval), allocatable :: sampledTimeInterval(:)
  integer, parameter :: sampledTimeIntervalRank = 1
  integer :: sampledTimeIntervalDim(2,sampledTimeIntervalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sampledTimeInterval)) then
    call sdmMessage(8,3,'PointingTable','opt%sampledTimeInterval not allocated.')
    error = .true.
    return
  endif
  do i=1, sampledTimeIntervalRank
    sampledTimeIntervalDim(:,i) = size(opt%sampledTimeInterval,i)
  enddo
  ireturn = sdm_addPointingSampledTimeInterval(key%antennaId, key%timeInterval, opt%sampledTimeInterval,&
      & sampledTimeIntervalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_addPointingSampledTimeInterval', ireturn)
    error = .true.
  endif
end subroutine addPointingSampledTimeInterval
!
! ---------------------------------------------------------------------------
!
subroutine getPointingSampledTimeInterval(key, opt, present, error)
  !
  use sdm_Pointing
  !
  type(PointingKey) :: key
  type(PointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingSampledTimeInterval, i
  integer, parameter :: sampledTimeIntervalRank = 1
  integer :: sampledTimeIntervalDim(2,sampledTimeIntervalRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sampledTimeInterval)) then
    call sdmMessage(8,3,'PointingTable','opt%sampledTimeInterval not allocated.')
    error = .true.
    return
  endif
  do i=1, sampledTimeIntervalRank
    sampledTimeIntervalDim(:,i) = size(opt%sampledTimeInterval,i)
  enddo
  ireturn = sdm_getPointingSampledTimeInterval(key%antennaId, key%timeInterval, opt%sampledTimeInterval,&
      & sampledTimeIntervalDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingSampledTimeInterval', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingSampledTimeInterval
!
! ---------------------------------------------------------------------------
! Columns methods for Table Pointing
!
! ---------------------------------------------------------------------------
!
subroutine getPointingAntennaIdColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingAntennaIdColumn, tableSize, i, j, ier
  integer, parameter :: antennaIdRank = 0
  if (.not.allocated(table%antennaId)) then
    call sdmMessage(8,3,'PointingTable','table%antennaId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%antennaId,antennaIdRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingAntennaIdColumn(table%antennaId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingAntennaId', ireturn)
    error = .true.
  endif
end subroutine getPointingAntennaIdColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingTimeIntervalColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingTimeIntervalColumn, tableSize, i, j, ier
  integer, parameter :: timeIntervalRank = 0
  if (.not.allocated(table%timeInterval)) then
    call sdmMessage(8,3,'PointingTable','table%timeInterval not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%timeInterval,timeIntervalRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingTimeIntervalColumn(table%timeInterval)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingTimeInterval', ireturn)
    error = .true.
  endif
end subroutine getPointingTimeIntervalColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingNumSampleColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingNumSampleColumn, tableSize, i, j, ier
  integer, parameter :: numSampleRank = 0
  if (.not.allocated(table%numSample)) then
    call sdmMessage(8,3,'PointingTable','table%numSample not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%numSample,numSampleRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingNumSampleColumn(table%numSample)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingNumSample', ireturn)
    error = .true.
  endif
end subroutine getPointingNumSampleColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingEncoderColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingEncoderColumn, tableSize, i, j, ier
  integer, allocatable :: encoderDim(:,:,:)
  integer, parameter :: encoderRank = 2
  if (.not.allocated(table%encoder)) then
    call sdmMessage(8,3,'PointingTable','table%encoder not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%encoder,encoderRank+1)
  ! Deal with dimensions
  allocate(encoderDim(2,encoderRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'PointingTable','Allocate error for encoderDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, encoderRank
      encoderDim(:,i,j) = size(table%encoder,i)
    enddo
  enddo
  ireturn = sdm_getPointingEncoderColumn(table%encoder, encoderDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingEncoder', ireturn)
    error = .true.
  endif
end subroutine getPointingEncoderColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingPointingTrackingColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingPointingTrackingColumn, tableSize, i, j, ier
  integer, parameter :: pointingTrackingRank = 0
  if (.not.allocated(table%pointingTracking)) then
    call sdmMessage(8,3,'PointingTable','table%pointingTracking not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%pointingTracking,pointingTrackingRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingPointingTrackingColumn(table%pointingTracking)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingPointingTracking', ireturn)
    error = .true.
  endif
end subroutine getPointingPointingTrackingColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingUsePolynomialsColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingUsePolynomialsColumn, tableSize, i, j, ier
  integer, parameter :: usePolynomialsRank = 0
  if (.not.allocated(table%usePolynomials)) then
    call sdmMessage(8,3,'PointingTable','table%usePolynomials not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%usePolynomials,usePolynomialsRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingUsePolynomialsColumn(table%usePolynomials)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingUsePolynomials', ireturn)
    error = .true.
  endif
end subroutine getPointingUsePolynomialsColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingTimeOriginColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingTimeOriginColumn, tableSize, i, j, ier
  integer, parameter :: timeOriginRank = 0
  if (.not.allocated(table%timeOrigin)) then
    call sdmMessage(8,3,'PointingTable','table%timeOrigin not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%timeOrigin,timeOriginRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingTimeOriginColumn(table%timeOrigin)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingTimeOrigin', ireturn)
    error = .true.
  endif
end subroutine getPointingTimeOriginColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingNumTermColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingNumTermColumn, tableSize, i, j, ier
  integer, parameter :: numTermRank = 0
  if (.not.allocated(table%numTerm)) then
    call sdmMessage(8,3,'PointingTable','table%numTerm not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%numTerm,numTermRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingNumTermColumn(table%numTerm)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingNumTerm', ireturn)
    error = .true.
  endif
end subroutine getPointingNumTermColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingPointingDirectionColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingPointingDirectionColumn, tableSize, i, j, ier
  integer, allocatable :: pointingDirectionDim(:,:,:)
  integer, parameter :: pointingDirectionRank = 2
  if (.not.allocated(table%pointingDirection)) then
    call sdmMessage(8,3,'PointingTable','table%pointingDirection not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%pointingDirection,pointingDirectionRank+1)
  ! Deal with dimensions
  allocate(pointingDirectionDim(2,pointingDirectionRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'PointingTable','Allocate error for pointingDirectionDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, pointingDirectionRank
      pointingDirectionDim(:,i,j) = size(table%pointingDirection,i)
    enddo
  enddo
  ireturn = sdm_getPointingPointingDirectionColumn(table%pointingDirection, pointingDirectionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingPointingDirection', ireturn)
    error = .true.
  endif
end subroutine getPointingPointingDirectionColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingTargetColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingTargetColumn, tableSize, i, j, ier
  integer, allocatable :: targetDim(:,:,:)
  integer, parameter :: targetRank = 2
  if (.not.allocated(table%target)) then
    call sdmMessage(8,3,'PointingTable','table%target not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%target,targetRank+1)
  ! Deal with dimensions
  allocate(targetDim(2,targetRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'PointingTable','Allocate error for targetDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, targetRank
      targetDim(:,i,j) = size(table%target,i)
    enddo
  enddo
  ireturn = sdm_getPointingTargetColumn(table%target, targetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingTarget', ireturn)
    error = .true.
  endif
end subroutine getPointingTargetColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingOffsetColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingOffsetColumn, tableSize, i, j, ier
  integer, allocatable :: offsetDim(:,:,:)
  integer, parameter :: offsetRank = 2
  if (.not.allocated(table%offset)) then
    call sdmMessage(8,3,'PointingTable','table%offset not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%offset,offsetRank+1)
  ! Deal with dimensions
  allocate(offsetDim(2,offsetRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'PointingTable','Allocate error for offsetDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, offsetRank
      offsetDim(:,i,j) = size(table%offset,i)
    enddo
  enddo
  ireturn = sdm_getPointingOffsetColumn(table%offset, offsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingOffset', ireturn)
    error = .true.
  endif
end subroutine getPointingOffsetColumn
!
! ---------------------------------------------------------------------------
!
subroutine getPointingPointingModelIdColumn(table, error)
  !
  use sdm_Pointing
  !
  type(Pointing) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getPointingPointingModelIdColumn, tableSize, i, j, ier
  integer, parameter :: pointingModelIdRank = 0
  if (.not.allocated(table%pointingModelId)) then
    call sdmMessage(8,3,'PointingTable','table%pointingModelId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%pointingModelId,pointingModelIdRank+1)
  ! Deal with dimensions
  ireturn = sdm_getPointingPointingModelIdColumn(table%pointingModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingTable','Error in sdm_getPointingPointingModelId', ireturn)
    error = .true.
  endif
end subroutine getPointingPointingModelIdColumn
!
! ===========================================================================
!
! Seeing Table:
!
! ===========================================================================
!
subroutine addSeeingRow(key, row, error)
  !
  use sdm_Seeing
  !
  type(SeeingRow) :: row
  type(SeeingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSeeingRow, i, j
  integer, parameter :: baseLengthRank = 1
  integer :: baseLengthDim(2,baseLengthRank)
  integer, parameter :: phaseRmsRank = 1
  integer :: phaseRmsDim(2,phaseRmsRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%baseLength)) then
    call sdmMessage(8,3,'SeeingTable','row%baseLength not allocated.')
    error = .true.
    return
  endif
  do i=1, baseLengthRank
    baseLengthDim(:,i) = size(row%baseLength,i)
  enddo
  if (.not.allocated(row%phaseRms)) then
    call sdmMessage(8,3,'SeeingTable','row%phaseRms not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRmsRank
    phaseRmsDim(:,i) = size(row%phaseRms,i)
  enddo
  !
  ireturn = sdm_addSeeingRow(key%timeInterval, row%numBaseLength, row%baseLength, baseLengthDim, row%phaseRms,&
      & phaseRmsDim, row%seeing, row%exponent)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SeeingTable','Error in sdm_addSeeingRow', ireturn)
    error = .true.
  endif
end subroutine addSeeingRow
!
! ---------------------------------------------------------------------------
!
subroutine getSeeingRow(key, row, error)
  !
  use sdm_Seeing
  !
  type(SeeingRow) :: row
  type(SeeingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSeeingRow, i
  integer, parameter :: baseLengthRank = 1
  integer :: baseLengthDim(2,baseLengthRank)
  integer, parameter :: phaseRmsRank = 1
  integer :: phaseRmsDim(2,phaseRmsRank)
  ! Deal with dimensions
  if (.not.allocated(row%baseLength)) then
    call sdmMessage(8,3,'SeeingTable','row%baseLength not allocated.')
    error = .true.
    return
  endif
  do i=1, baseLengthRank
    baseLengthDim(:,i) = size(row%baseLength,i)
  enddo
  if (.not.allocated(row%phaseRms)) then
    call sdmMessage(8,3,'SeeingTable','row%phaseRms not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRmsRank
    phaseRmsDim(:,i) = size(row%phaseRms,i)
  enddo
  !
  ireturn = sdm_getSeeingRow(key%timeInterval, row%numBaseLength, row%baseLength, baseLengthDim, row%phaseRms,&
      & phaseRmsDim, row%seeing, row%exponent)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SeeingTable','Error in sdm_getSeeingRow', ireturn)
    error = .true.
  endif
end subroutine getSeeingRow
!
! ---------------------------------------------------------------------------
!
subroutine getSeeingTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSeeingTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSeeingTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSeeingTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSeeingKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Seeing
  !
  integer :: tableSize
  logical :: error
  type(SeeingKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSeeingKeys, i
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getSeeingKeys(timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SeeingTable','Error in sdm_getSeeingKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getSeeingKeys
! ---------------------------------------------------------------------------
!
subroutine allocSeeingRow(row, error)
  use sdm_Seeing
  type(SeeingRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Seeing'
  ! row%baseLength allocation
  if (allocated(row%baseLength)) then
    deallocate(row%baseLength, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%phaseRms allocation
  if (allocated(row%phaseRms)) then
    deallocate(row%phaseRms, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%baseLength(row%numBaseLength),  row%phaseRms(row%numBaseLength), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSeeingRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! SysCal Table:
!
! ===========================================================================
!
subroutine addSysCalRow(key, row, error)
  !
  use sdm_SysCal
  !
  type(SysCalRow) :: row
  type(SysCalKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSysCalRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addSysCalRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%numReceptor,&
      & row%numChan)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalRow', ireturn)
    error = .true.
  endif
end subroutine addSysCalRow
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalRow(key, row, error)
  !
  use sdm_SysCal
  !
  type(SysCalRow) :: row
  type(SysCalKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSysCalRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getSysCalRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%numReceptor,&
      & row%numChan)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalRow', ireturn)
    error = .true.
  endif
end subroutine getSysCalRow
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSysCalTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSysCalTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSysCalTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_SysCal
  !
  integer :: tableSize
  logical :: error
  type(SysCalKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSysCalKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: feedIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(feedIdList(tableSize))
  !
  ireturn = sdm_getSysCalKeys(antennaIdList, spectralWindowIdList, timeIntervalList, feedIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%feedId = feedIdList(i)
  enddo
end subroutine getSysCalKeys
! ---------------------------------------------------------------------------
!
subroutine allocSysCalOpt(row, opt, error)
  use sdm_SysCal
  type(SysCalRow) :: row
  type(SysCalOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'SysCal'
  ! opt%tcalSpectrum allocation
  if (allocated(opt%tcalSpectrum)) then
    deallocate(opt%tcalSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%trxSpectrum allocation
  if (allocated(opt%trxSpectrum)) then
    deallocate(opt%trxSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%tskySpectrum allocation
  if (allocated(opt%tskySpectrum)) then
    deallocate(opt%tskySpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%tsysSpectrum allocation
  if (allocated(opt%tsysSpectrum)) then
    deallocate(opt%tsysSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%tantSpectrum allocation
  if (allocated(opt%tantSpectrum)) then
    deallocate(opt%tantSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%tantTsysSpectrum allocation
  if (allocated(opt%tantTsysSpectrum)) then
    deallocate(opt%tantTsysSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%phaseDiffSpectrum allocation
  if (allocated(opt%phaseDiffSpectrum)) then
    deallocate(opt%phaseDiffSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%tcalSpectrum(row%numChan, row%numReceptor), opt%trxSpectrum(row%numChan, row%numReceptor),&
      & opt%tskySpectrum(row%numChan, row%numReceptor), opt%tsysSpectrum(row%numChan, row%numReceptor),&
      & opt%tantSpectrum(row%numChan, row%numReceptor), opt%tantTsysSpectrum(row%numChan, row%numReceptor),&
      & opt%phaseDiffSpectrum(row%numChan, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSysCalOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTcalFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTcalFlag, i
  !! logical*1 :: tcalFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTcalFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tcalFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTcalFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTcalFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTcalFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTcalFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTcalFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tcalFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTcalFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTcalFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTcalSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTcalSpectrum, i
  !! real*8, allocatable :: tcalSpectrum(:,:)
  integer, parameter :: tcalSpectrumRank = 2
  integer :: tcalSpectrumDim(2,tcalSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tcalSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tcalSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tcalSpectrumRank
    tcalSpectrumDim(:,i) = size(opt%tcalSpectrum,i)
  enddo
  ireturn = sdm_addSysCalTcalSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tcalSpectrum, tcalSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTcalSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTcalSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTcalSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTcalSpectrum, i
  integer, parameter :: tcalSpectrumRank = 2
  integer :: tcalSpectrumDim(2,tcalSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tcalSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tcalSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tcalSpectrumRank
    tcalSpectrumDim(:,i) = size(opt%tcalSpectrum,i)
  enddo
  ireturn = sdm_getSysCalTcalSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tcalSpectrum, tcalSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTcalSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTcalSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTrxFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTrxFlag, i
  !! logical*1 :: trxFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTrxFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%trxFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTrxFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTrxFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTrxFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTrxFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTrxFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%trxFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTrxFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTrxFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTrxSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTrxSpectrum, i
  !! real*8, allocatable :: trxSpectrum(:,:)
  integer, parameter :: trxSpectrumRank = 2
  integer :: trxSpectrumDim(2,trxSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%trxSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%trxSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, trxSpectrumRank
    trxSpectrumDim(:,i) = size(opt%trxSpectrum,i)
  enddo
  ireturn = sdm_addSysCalTrxSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%trxSpectrum, trxSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTrxSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTrxSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTrxSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTrxSpectrum, i
  integer, parameter :: trxSpectrumRank = 2
  integer :: trxSpectrumDim(2,trxSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%trxSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%trxSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, trxSpectrumRank
    trxSpectrumDim(:,i) = size(opt%trxSpectrum,i)
  enddo
  ireturn = sdm_getSysCalTrxSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%trxSpectrum, trxSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTrxSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTrxSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTskyFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTskyFlag, i
  !! logical*1 :: tskyFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTskyFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tskyFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTskyFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTskyFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTskyFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTskyFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTskyFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tskyFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTskyFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTskyFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTskySpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTskySpectrum, i
  !! real*8, allocatable :: tskySpectrum(:,:)
  integer, parameter :: tskySpectrumRank = 2
  integer :: tskySpectrumDim(2,tskySpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tskySpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tskySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tskySpectrumRank
    tskySpectrumDim(:,i) = size(opt%tskySpectrum,i)
  enddo
  ireturn = sdm_addSysCalTskySpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tskySpectrum, tskySpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTskySpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTskySpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTskySpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTskySpectrum, i
  integer, parameter :: tskySpectrumRank = 2
  integer :: tskySpectrumDim(2,tskySpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tskySpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tskySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tskySpectrumRank
    tskySpectrumDim(:,i) = size(opt%tskySpectrum,i)
  enddo
  ireturn = sdm_getSysCalTskySpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tskySpectrum, tskySpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTskySpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTskySpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTsysFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTsysFlag, i
  !! logical*1 :: tsysFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTsysFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tsysFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTsysFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTsysFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTsysFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTsysFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTsysFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tsysFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTsysFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTsysFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTsysSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTsysSpectrum, i
  !! real*8, allocatable :: tsysSpectrum(:,:)
  integer, parameter :: tsysSpectrumRank = 2
  integer :: tsysSpectrumDim(2,tsysSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tsysSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tsysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tsysSpectrumRank
    tsysSpectrumDim(:,i) = size(opt%tsysSpectrum,i)
  enddo
  ireturn = sdm_addSysCalTsysSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tsysSpectrum, tsysSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTsysSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTsysSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTsysSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTsysSpectrum, i
  integer, parameter :: tsysSpectrumRank = 2
  integer :: tsysSpectrumDim(2,tsysSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tsysSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tsysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tsysSpectrumRank
    tsysSpectrumDim(:,i) = size(opt%tsysSpectrum,i)
  enddo
  ireturn = sdm_getSysCalTsysSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tsysSpectrum, tsysSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTsysSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTsysSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTantFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTantFlag, i
  !! logical*1 :: tantFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTantFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tantFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTantFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTantFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTantFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTantFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTantFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%tantFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTantFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTantFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTantSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTantSpectrum, i
  !! real, allocatable :: tantSpectrum(:,:)
  integer, parameter :: tantSpectrumRank = 2
  integer :: tantSpectrumDim(2,tantSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tantSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tantSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tantSpectrumRank
    tantSpectrumDim(:,i) = size(opt%tantSpectrum,i)
  enddo
  ireturn = sdm_addSysCalTantSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantSpectrum, tantSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTantSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTantSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTantSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTantSpectrum, i
  integer, parameter :: tantSpectrumRank = 2
  integer :: tantSpectrumDim(2,tantSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tantSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tantSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tantSpectrumRank
    tantSpectrumDim(:,i) = size(opt%tantSpectrum,i)
  enddo
  ireturn = sdm_getSysCalTantSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantSpectrum, tantSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTantSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTantSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTantTsysFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTantTsysFlag, i
  !! logical*1 :: tantTsysFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalTantTsysFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantTsysFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTantTsysFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalTantTsysFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTantTsysFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTantTsysFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalTantTsysFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantTsysFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTantTsysFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTantTsysFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalTantTsysSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalTantTsysSpectrum, i
  !! real, allocatable :: tantTsysSpectrum(:,:)
  integer, parameter :: tantTsysSpectrumRank = 2
  integer :: tantTsysSpectrumDim(2,tantTsysSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tantTsysSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tantTsysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tantTsysSpectrumRank
    tantTsysSpectrumDim(:,i) = size(opt%tantTsysSpectrum,i)
  enddo
  ireturn = sdm_addSysCalTantTsysSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantTsysSpectrum, tantTsysSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalTantTsysSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalTantTsysSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalTantTsysSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalTantTsysSpectrum, i
  integer, parameter :: tantTsysSpectrumRank = 2
  integer :: tantTsysSpectrumDim(2,tantTsysSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tantTsysSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%tantTsysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tantTsysSpectrumRank
    tantTsysSpectrumDim(:,i) = size(opt%tantTsysSpectrum,i)
  enddo
  ireturn = sdm_getSysCalTantTsysSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%tantTsysSpectrum, tantTsysSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalTantTsysSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalTantTsysSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalPhaseDiffFlag(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalPhaseDiffFlag, i
  !! logical*1 :: phaseDiffFlag
  ! Deal with dimensions
  ireturn = sdm_addSysCalPhaseDiffFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseDiffFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalPhaseDiffFlag', ireturn)
    error = .true.
  endif
end subroutine addSysCalPhaseDiffFlag
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalPhaseDiffFlag(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalPhaseDiffFlag, i
  ! Deal with dimensions
  ireturn = sdm_getSysCalPhaseDiffFlag(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseDiffFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalPhaseDiffFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalPhaseDiffFlag
!
!
! ---------------------------------------------------------------------------
!
subroutine addSysCalPhaseDiffSpectrum(key, opt, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSysCalPhaseDiffSpectrum, i
  !! real, allocatable :: phaseDiffSpectrum(:,:)
  integer, parameter :: phaseDiffSpectrumRank = 2
  integer :: phaseDiffSpectrumDim(2,phaseDiffSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseDiffSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%phaseDiffSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDiffSpectrumRank
    phaseDiffSpectrumDim(:,i) = size(opt%phaseDiffSpectrum,i)
  enddo
  ireturn = sdm_addSysCalPhaseDiffSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseDiffSpectrum, phaseDiffSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_addSysCalPhaseDiffSpectrum', ireturn)
    error = .true.
  endif
end subroutine addSysCalPhaseDiffSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getSysCalPhaseDiffSpectrum(key, opt, present, error)
  !
  use sdm_SysCal
  !
  type(SysCalKey) :: key
  type(SysCalOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSysCalPhaseDiffSpectrum, i
  integer, parameter :: phaseDiffSpectrumRank = 2
  integer :: phaseDiffSpectrumDim(2,phaseDiffSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseDiffSpectrum)) then
    call sdmMessage(8,3,'SysCalTable','opt%phaseDiffSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDiffSpectrumRank
    phaseDiffSpectrumDim(:,i) = size(opt%phaseDiffSpectrum,i)
  enddo
  ireturn = sdm_getSysCalPhaseDiffSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseDiffSpectrum, phaseDiffSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SysCalTable','Error in sdm_getSysCalPhaseDiffSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSysCalPhaseDiffSpectrum
!
!
! ===========================================================================
!
! TotalPower Table:
!
! ===========================================================================
!
subroutine addTotalPowerRow(key, row, error)
  !
  use sdm_TotalPower
  !
  type(TotalPowerRow) :: row
  type(TotalPowerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addTotalPowerRow, i, j
  integer, parameter :: uvwRank = 2
  integer :: uvwDim(2,uvwRank)
  integer, parameter :: exposureRank = 2
  integer :: exposureDim(2,exposureRank)
  integer, parameter :: timeCentroidRank = 2
  integer :: timeCentroidDim(2,timeCentroidRank)
  integer, parameter :: floatDataRank = 3
  integer :: floatDataDim(2,floatDataRank)
  integer, parameter :: flagAntRank = 1
  integer :: flagAntDim(2,flagAntRank)
  integer, parameter :: flagPolRank = 2
  integer :: flagPolDim(2,flagPolRank)
  integer, parameter :: stateIdRank = 1
  integer :: stateIdDim(2,stateIdRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%uvw)) then
    call sdmMessage(8,3,'TotalPowerTable','row%uvw not allocated.')
    error = .true.
    return
  endif
  do i=1, uvwRank
    uvwDim(:,i) = size(row%uvw,i)
  enddo
  if (.not.allocated(row%exposure)) then
    call sdmMessage(8,3,'TotalPowerTable','row%exposure not allocated.')
    error = .true.
    return
  endif
  do i=1, exposureRank
    exposureDim(:,i) = size(row%exposure,i)
  enddo
  if (.not.allocated(row%timeCentroid)) then
    call sdmMessage(8,3,'TotalPowerTable','row%timeCentroid not allocated.')
    error = .true.
    return
  endif
  do i=1, timeCentroidRank
    timeCentroidDim(:,i) = size(row%timeCentroid,i)
  enddo
  if (.not.allocated(row%floatData)) then
    call sdmMessage(8,3,'TotalPowerTable','row%floatData not allocated.')
    error = .true.
    return
  endif
  do i=1, floatDataRank
    floatDataDim(:,i) = size(row%floatData,i)
  enddo
  if (.not.allocated(row%flagAnt)) then
    call sdmMessage(8,3,'TotalPowerTable','row%flagAnt not allocated.')
    error = .true.
    return
  endif
  do i=1, flagAntRank
    flagAntDim(:,i) = size(row%flagAnt,i)
  enddo
  if (.not.allocated(row%flagPol)) then
    call sdmMessage(8,3,'TotalPowerTable','row%flagPol not allocated.')
    error = .true.
    return
  endif
  do i=1, flagPolRank
    flagPolDim(:,i) = size(row%flagPol,i)
  enddo
  if (.not.allocated(row%stateId)) then
    call sdmMessage(8,3,'TotalPowerTable','row%stateId not allocated.')
    error = .true.
    return
  endif
  do i=1, stateIdRank
    stateIdDim(:,i) = size(row%stateId,i)
  enddo
  !
  ireturn = sdm_addTotalPowerRow(key%time, key%configDescriptionId, key%fieldId, row%scanNumber, row%subscanNumber,&
      & row%integrationNumber, row%uvw, uvwDim, row%exposure, exposureDim, row%timeCentroid, timeCentroidDim,&
      & row%floatData, floatDataDim, row%flagAnt, flagAntDim, row%flagPol, flagPolDim, row%flagRow, row%interval,&
      & row%stateId, stateIdDim, row%execBlockId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_addTotalPowerRow', ireturn)
    error = .true.
  endif
end subroutine addTotalPowerRow
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerRow(key, row, error)
  !
  use sdm_TotalPower
  !
  type(TotalPowerRow) :: row
  type(TotalPowerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getTotalPowerRow, i
  integer, parameter :: uvwRank = 2
  integer :: uvwDim(2,uvwRank)
  integer, parameter :: exposureRank = 2
  integer :: exposureDim(2,exposureRank)
  integer, parameter :: timeCentroidRank = 2
  integer :: timeCentroidDim(2,timeCentroidRank)
  integer, parameter :: floatDataRank = 3
  integer :: floatDataDim(2,floatDataRank)
  integer, parameter :: flagAntRank = 1
  integer :: flagAntDim(2,flagAntRank)
  integer, parameter :: flagPolRank = 2
  integer :: flagPolDim(2,flagPolRank)
  integer, parameter :: stateIdRank = 1
  integer :: stateIdDim(2,stateIdRank)
  ! Deal with dimensions
  if (.not.allocated(row%uvw)) then
    call sdmMessage(8,3,'TotalPowerTable','row%uvw not allocated.')
    error = .true.
    return
  endif
  do i=1, uvwRank
    uvwDim(:,i) = size(row%uvw,i)
  enddo
  if (.not.allocated(row%exposure)) then
    call sdmMessage(8,3,'TotalPowerTable','row%exposure not allocated.')
    error = .true.
    return
  endif
  do i=1, exposureRank
    exposureDim(:,i) = size(row%exposure,i)
  enddo
  if (.not.allocated(row%timeCentroid)) then
    call sdmMessage(8,3,'TotalPowerTable','row%timeCentroid not allocated.')
    error = .true.
    return
  endif
  do i=1, timeCentroidRank
    timeCentroidDim(:,i) = size(row%timeCentroid,i)
  enddo
  if (.not.allocated(row%floatData)) then
    call sdmMessage(8,3,'TotalPowerTable','row%floatData not allocated.')
    error = .true.
    return
  endif
  do i=1, floatDataRank
    floatDataDim(:,i) = size(row%floatData,i)
  enddo
  if (.not.allocated(row%flagAnt)) then
    call sdmMessage(8,3,'TotalPowerTable','row%flagAnt not allocated.')
    error = .true.
    return
  endif
  do i=1, flagAntRank
    flagAntDim(:,i) = size(row%flagAnt,i)
  enddo
  if (.not.allocated(row%flagPol)) then
    call sdmMessage(8,3,'TotalPowerTable','row%flagPol not allocated.')
    error = .true.
    return
  endif
  do i=1, flagPolRank
    flagPolDim(:,i) = size(row%flagPol,i)
  enddo
  if (.not.allocated(row%stateId)) then
    call sdmMessage(8,3,'TotalPowerTable','row%stateId not allocated.')
    error = .true.
    return
  endif
  do i=1, stateIdRank
    stateIdDim(:,i) = size(row%stateId,i)
  enddo
  !
  ireturn = sdm_getTotalPowerRow(key%time, key%configDescriptionId, key%fieldId, row%scanNumber, row%subscanNumber,&
      & row%integrationNumber, row%uvw, uvwDim, row%exposure, exposureDim, row%timeCentroid, timeCentroidDim,&
      & row%floatData, floatDataDim, row%flagAnt, flagAntDim, row%flagPol, flagPolDim, row%flagRow, row%interval,&
      & row%stateId, stateIdDim, row%execBlockId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerRow', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerRow
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getTotalPowerTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getTotalPowerTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getTotalPowerTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_TotalPower
  !
  integer :: tableSize
  logical :: error
  type(TotalPowerKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getTotalPowerKeys, i
  integer*8, allocatable :: timeList(:)
  integer, allocatable :: configDescriptionIdList(:)
  integer, allocatable :: fieldIdList(:)
  !
  allocate(timeList(tableSize))
  allocate(configDescriptionIdList(tableSize))
  allocate(fieldIdList(tableSize))
  !
  ireturn = sdm_getTotalPowerKeys(timeList, configDescriptionIdList, fieldIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%time = timeList(i)
    keyList(i)%configDescriptionId = configDescriptionIdList(i)
    keyList(i)%fieldId = fieldIdList(i)
  enddo
end subroutine getTotalPowerKeys
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addTotalPowerSubintegrationNumber(key, opt, error)
  !
  use sdm_TotalPower
  !
  type(TotalPowerKey) :: key
  type(TotalPowerOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addTotalPowerSubintegrationNumber, i
  !! integer :: subintegrationNumber
  ! Deal with dimensions
  ireturn = sdm_addTotalPowerSubintegrationNumber(key%time, key%configDescriptionId, key%fieldId,&
      & opt%subintegrationNumber)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_addTotalPowerSubintegrationNumber', ireturn)
    error = .true.
  endif
end subroutine addTotalPowerSubintegrationNumber
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerSubintegrationNumber(key, opt, present, error)
  !
  use sdm_TotalPower
  !
  type(TotalPowerKey) :: key
  type(TotalPowerOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getTotalPowerSubintegrationNumber, i
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerSubintegrationNumber(key%time, key%configDescriptionId, key%fieldId,&
      & opt%subintegrationNumber)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerSubintegrationNumber', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getTotalPowerSubintegrationNumber
!
! ---------------------------------------------------------------------------
! Columns methods for Table TotalPower
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerTimeColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerTimeColumn, tableSize, i, j, ier
  integer, parameter :: timeRank = 0
  if (.not.allocated(table%time)) then
    call sdmMessage(8,3,'TotalPowerTable','table%time not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%time,timeRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerTimeColumn(table%time)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerTime', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerTimeColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerConfigDescriptionIdColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerConfigDescriptionIdColumn, tableSize, i, j, ier
  integer, parameter :: configDescriptionIdRank = 0
  if (.not.allocated(table%configDescriptionId)) then
    call sdmMessage(8,3,'TotalPowerTable','table%configDescriptionId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%configDescriptionId,configDescriptionIdRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerConfigDescriptionIdColumn(table%configDescriptionId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerConfigDescriptionId', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerConfigDescriptionIdColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerFieldIdColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerFieldIdColumn, tableSize, i, j, ier
  integer, parameter :: fieldIdRank = 0
  if (.not.allocated(table%fieldId)) then
    call sdmMessage(8,3,'TotalPowerTable','table%fieldId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%fieldId,fieldIdRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerFieldIdColumn(table%fieldId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerFieldId', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerFieldIdColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerScanNumberColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerScanNumberColumn, tableSize, i, j, ier
  integer, parameter :: scanNumberRank = 0
  if (.not.allocated(table%scanNumber)) then
    call sdmMessage(8,3,'TotalPowerTable','table%scanNumber not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%scanNumber,scanNumberRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerScanNumberColumn(table%scanNumber)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerScanNumber', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerScanNumberColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerSubscanNumberColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerSubscanNumberColumn, tableSize, i, j, ier
  integer, parameter :: subscanNumberRank = 0
  if (.not.allocated(table%subscanNumber)) then
    call sdmMessage(8,3,'TotalPowerTable','table%subscanNumber not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%subscanNumber,subscanNumberRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerSubscanNumberColumn(table%subscanNumber)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerSubscanNumber', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerSubscanNumberColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerIntegrationNumberColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerIntegrationNumberColumn, tableSize, i, j, ier
  integer, parameter :: integrationNumberRank = 0
  if (.not.allocated(table%integrationNumber)) then
    call sdmMessage(8,3,'TotalPowerTable','table%integrationNumber not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%integrationNumber,integrationNumberRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerIntegrationNumberColumn(table%integrationNumber)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerIntegrationNumber', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerIntegrationNumberColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerUvwColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerUvwColumn, tableSize, i, j, ier
  integer, allocatable :: uvwDim(:,:,:)
  integer, parameter :: uvwRank = 2
  if (.not.allocated(table%uvw)) then
    call sdmMessage(8,3,'TotalPowerTable','table%uvw not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%uvw,uvwRank+1)
  ! Deal with dimensions
  allocate(uvwDim(2,uvwRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for uvwDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, uvwRank
      uvwDim(:,i,j) = size(table%uvw,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerUvwColumn(table%uvw, uvwDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerUvw', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerUvwColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerExposureColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerExposureColumn, tableSize, i, j, ier
  integer, allocatable :: exposureDim(:,:,:)
  integer, parameter :: exposureRank = 2
  if (.not.allocated(table%exposure)) then
    call sdmMessage(8,3,'TotalPowerTable','table%exposure not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%exposure,exposureRank+1)
  ! Deal with dimensions
  allocate(exposureDim(2,exposureRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for exposureDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, exposureRank
      exposureDim(:,i,j) = size(table%exposure,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerExposureColumn(table%exposure, exposureDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerExposure', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerExposureColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerTimeCentroidColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerTimeCentroidColumn, tableSize, i, j, ier
  integer, allocatable :: timeCentroidDim(:,:,:)
  integer, parameter :: timeCentroidRank = 2
  if (.not.allocated(table%timeCentroid)) then
    call sdmMessage(8,3,'TotalPowerTable','table%timeCentroid not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%timeCentroid,timeCentroidRank+1)
  ! Deal with dimensions
  allocate(timeCentroidDim(2,timeCentroidRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for timeCentroidDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, timeCentroidRank
      timeCentroidDim(:,i,j) = size(table%timeCentroid,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerTimeCentroidColumn(table%timeCentroid, timeCentroidDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerTimeCentroid', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerTimeCentroidColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerFloatDataColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerFloatDataColumn, tableSize, i, j, ier
  integer, allocatable :: floatDataDim(:,:,:)
  integer, parameter :: floatDataRank = 3
  if (.not.allocated(table%floatData)) then
    call sdmMessage(8,3,'TotalPowerTable','table%floatData not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%floatData,floatDataRank+1)
  ! Deal with dimensions
  allocate(floatDataDim(2,floatDataRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for floatDataDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, floatDataRank
      floatDataDim(:,i,j) = size(table%floatData,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerFloatDataColumn(table%floatData, floatDataDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerFloatData', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerFloatDataColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerFlagAntColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerFlagAntColumn, tableSize, i, j, ier
  integer, allocatable :: flagAntDim(:,:,:)
  integer, parameter :: flagAntRank = 1
  if (.not.allocated(table%flagAnt)) then
    call sdmMessage(8,3,'TotalPowerTable','table%flagAnt not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%flagAnt,flagAntRank+1)
  ! Deal with dimensions
  allocate(flagAntDim(2,flagAntRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for flagAntDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, flagAntRank
      flagAntDim(:,i,j) = size(table%flagAnt,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerFlagAntColumn(table%flagAnt, flagAntDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerFlagAnt', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerFlagAntColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerFlagPolColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerFlagPolColumn, tableSize, i, j, ier
  integer, allocatable :: flagPolDim(:,:,:)
  integer, parameter :: flagPolRank = 2
  if (.not.allocated(table%flagPol)) then
    call sdmMessage(8,3,'TotalPowerTable','table%flagPol not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%flagPol,flagPolRank+1)
  ! Deal with dimensions
  allocate(flagPolDim(2,flagPolRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for flagPolDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, flagPolRank
      flagPolDim(:,i,j) = size(table%flagPol,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerFlagPolColumn(table%flagPol, flagPolDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerFlagPol', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerFlagPolColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerFlagRowColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerFlagRowColumn, tableSize, i, j, ier
  integer, parameter :: flagRowRank = 0
  if (.not.allocated(table%flagRow)) then
    call sdmMessage(8,3,'TotalPowerTable','table%flagRow not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%flagRow,flagRowRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerFlagRowColumn(table%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerFlagRow', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerFlagRowColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerIntervalColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerIntervalColumn, tableSize, i, j, ier
  integer, parameter :: intervalRank = 0
  if (.not.allocated(table%interval)) then
    call sdmMessage(8,3,'TotalPowerTable','table%interval not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%interval,intervalRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerIntervalColumn(table%interval)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerInterval', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerIntervalColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerStateIdColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerStateIdColumn, tableSize, i, j, ier
  integer, allocatable :: stateIdDim(:,:,:)
  integer, parameter :: stateIdRank = 1
  if (.not.allocated(table%stateId)) then
    call sdmMessage(8,3,'TotalPowerTable','table%stateId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%stateId,stateIdRank+1)
  ! Deal with dimensions
  allocate(stateIdDim(2,stateIdRank,tableSize),stat=ier)
  if (ier.ne.0) then
    call sdmMessage(8,3,'TotalPowerTable','Allocate error for stateIdDim')
    error = .true.
    return
  endif
  do j=1, tableSize
    do i=1, stateIdRank
      stateIdDim(:,i,j) = size(table%stateId,i)
    enddo
  enddo
  ireturn = sdm_getTotalPowerStateIdColumn(table%stateId, stateIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerStateId', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerStateIdColumn
!
! ---------------------------------------------------------------------------
!
subroutine getTotalPowerExecBlockIdColumn(table, error)
  !
  use sdm_TotalPower
  !
  type(TotalPower) :: table
  logical :: error
  !
  integer :: ireturn, sdm_getTotalPowerExecBlockIdColumn, tableSize, i, j, ier
  integer, parameter :: execBlockIdRank = 0
  if (.not.allocated(table%execBlockId)) then
    call sdmMessage(8,3,'TotalPowerTable','table%execBlockId not allocated.')
    error = .true.
    return
  endif
  tableSize = size(table%execBlockId,execBlockIdRank+1)
  ! Deal with dimensions
  ireturn = sdm_getTotalPowerExecBlockIdColumn(table%execBlockId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'TotalPowerTable','Error in sdm_getTotalPowerExecBlockId', ireturn)
    error = .true.
  endif
end subroutine getTotalPowerExecBlockIdColumn
!
! ===========================================================================
!
! Weather Table:
!
! ===========================================================================
!
subroutine addWeatherRow(key, row, error)
  !
  use sdm_Weather
  !
  type(WeatherRow) :: row
  type(WeatherKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addWeatherRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addWeatherRow(key%stationId, key%timeInterval, row%pressure, row%pressureFlag, row%relHumidity,&
      & row%relHumidityFlag, row%temperature, row%temperatureFlag, row%windDirection, row%windDirectionFlag,&
      & row%windSpeed, row%windSpeedFlag, row%windMax, row%windMaxFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_addWeatherRow', ireturn)
    error = .true.
  endif
end subroutine addWeatherRow
!
! ---------------------------------------------------------------------------
!
subroutine getWeatherRow(key, row, error)
  !
  use sdm_Weather
  !
  type(WeatherRow) :: row
  type(WeatherKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getWeatherRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getWeatherRow(key%stationId, key%timeInterval, row%pressure, row%pressureFlag, row%relHumidity,&
      & row%relHumidityFlag, row%temperature, row%temperatureFlag, row%windDirection, row%windDirectionFlag,&
      & row%windSpeed, row%windSpeedFlag, row%windMax, row%windMaxFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_getWeatherRow', ireturn)
    error = .true.
  endif
end subroutine getWeatherRow
!
! ---------------------------------------------------------------------------
!
subroutine getWeatherTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getWeatherTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getWeatherTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getWeatherTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getWeatherKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Weather
  !
  integer :: tableSize
  logical :: error
  type(WeatherKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getWeatherKeys, i
  integer, allocatable :: stationIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(stationIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getWeatherKeys(stationIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_getWeatherKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%stationId = stationIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getWeatherKeys
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addWeatherDewPoint(key, opt, error)
  !
  use sdm_Weather
  !
  type(WeatherKey) :: key
  type(WeatherOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addWeatherDewPoint, i
  !! real*8 :: dewPoint
  ! Deal with dimensions
  ireturn = sdm_addWeatherDewPoint(key%stationId, key%timeInterval, opt%dewPoint)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_addWeatherDewPoint', ireturn)
    error = .true.
  endif
end subroutine addWeatherDewPoint
!
! ---------------------------------------------------------------------------
!
subroutine getWeatherDewPoint(key, opt, present, error)
  !
  use sdm_Weather
  !
  type(WeatherKey) :: key
  type(WeatherOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getWeatherDewPoint, i
  ! Deal with dimensions
  ireturn = sdm_getWeatherDewPoint(key%stationId, key%timeInterval, opt%dewPoint)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_getWeatherDewPoint', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getWeatherDewPoint
!
!
! ---------------------------------------------------------------------------
!
subroutine addWeatherDewPointFlag(key, opt, error)
  !
  use sdm_Weather
  !
  type(WeatherKey) :: key
  type(WeatherOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addWeatherDewPointFlag, i
  !! logical*1 :: dewPointFlag
  ! Deal with dimensions
  ireturn = sdm_addWeatherDewPointFlag(key%stationId, key%timeInterval, opt%dewPointFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_addWeatherDewPointFlag', ireturn)
    error = .true.
  endif
end subroutine addWeatherDewPointFlag
!
! ---------------------------------------------------------------------------
!
subroutine getWeatherDewPointFlag(key, opt, present, error)
  !
  use sdm_Weather
  !
  type(WeatherKey) :: key
  type(WeatherOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getWeatherDewPointFlag, i
  ! Deal with dimensions
  ireturn = sdm_getWeatherDewPointFlag(key%stationId, key%timeInterval, opt%dewPointFlag)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WeatherTable','Error in sdm_getWeatherDewPointFlag', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getWeatherDewPointFlag
!
!
! ===========================================================================
!
! WVMCal Table:
!
! ===========================================================================
!
subroutine addWVMCalRow(key, row, error)
  !
  use sdm_WVMCal
  !
  type(WVMCalRow) :: row
  type(WVMCalKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addWVMCalRow, i, j
  integer, parameter :: polyFreqLimitsRank = 1
  integer :: polyFreqLimitsDim(2,polyFreqLimitsRank)
  integer, parameter :: pathCoeffRank = 2
  integer :: pathCoeffDim(2,pathCoeffRank)
  integer, parameter :: refTempRank = 1
  integer :: refTempDim(2,refTempRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%polyFreqLimits)) then
    call sdmMessage(8,3,'WVMCalTable','row%polyFreqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, polyFreqLimitsRank
    polyFreqLimitsDim(:,i) = size(row%polyFreqLimits,i)
  enddo
  if (.not.allocated(row%pathCoeff)) then
    call sdmMessage(8,3,'WVMCalTable','row%pathCoeff not allocated.')
    error = .true.
    return
  endif
  do i=1, pathCoeffRank
    pathCoeffDim(:,i) = size(row%pathCoeff,i)
  enddo
  if (.not.allocated(row%refTemp)) then
    call sdmMessage(8,3,'WVMCalTable','row%refTemp not allocated.')
    error = .true.
    return
  endif
  do i=1, refTempRank
    refTempDim(:,i) = size(row%refTemp,i)
  enddo
  !
  ireturn = sdm_addWVMCalRow(key%antennaId, key%spectralWindowId, key%timeInterval, row%wvrMethod, row%polyFreqLimits,&
      & polyFreqLimitsDim, row%numChan, row%numPoly, row%pathCoeff, pathCoeffDim, row%refTemp, refTempDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WVMCalTable','Error in sdm_addWVMCalRow', ireturn)
    error = .true.
  endif
end subroutine addWVMCalRow
!
! ---------------------------------------------------------------------------
!
subroutine getWVMCalRow(key, row, error)
  !
  use sdm_WVMCal
  !
  type(WVMCalRow) :: row
  type(WVMCalKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getWVMCalRow, i
  integer, parameter :: polyFreqLimitsRank = 1
  integer :: polyFreqLimitsDim(2,polyFreqLimitsRank)
  integer, parameter :: pathCoeffRank = 2
  integer :: pathCoeffDim(2,pathCoeffRank)
  integer, parameter :: refTempRank = 1
  integer :: refTempDim(2,refTempRank)
  ! Deal with dimensions
  if (.not.allocated(row%polyFreqLimits)) then
    call sdmMessage(8,3,'WVMCalTable','row%polyFreqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, polyFreqLimitsRank
    polyFreqLimitsDim(:,i) = size(row%polyFreqLimits,i)
  enddo
  if (.not.allocated(row%pathCoeff)) then
    call sdmMessage(8,3,'WVMCalTable','row%pathCoeff not allocated.')
    error = .true.
    return
  endif
  do i=1, pathCoeffRank
    pathCoeffDim(:,i) = size(row%pathCoeff,i)
  enddo
  if (.not.allocated(row%refTemp)) then
    call sdmMessage(8,3,'WVMCalTable','row%refTemp not allocated.')
    error = .true.
    return
  endif
  do i=1, refTempRank
    refTempDim(:,i) = size(row%refTemp,i)
  enddo
  !
  ireturn = sdm_getWVMCalRow(key%antennaId, key%spectralWindowId, key%timeInterval, row%wvrMethod, row%polyFreqLimits,&
      & polyFreqLimitsDim, row%numChan, row%numPoly, row%pathCoeff, pathCoeffDim, row%refTemp, refTempDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WVMCalTable','Error in sdm_getWVMCalRow', ireturn)
    error = .true.
  endif
end subroutine getWVMCalRow
!
! ---------------------------------------------------------------------------
!
subroutine getWVMCalTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getWVMCalTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getWVMCalTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getWVMCalTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getWVMCalKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_WVMCal
  !
  integer :: tableSize
  logical :: error
  type(WVMCalKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getWVMCalKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getWVMCalKeys(antennaIdList, spectralWindowIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'WVMCalTable','Error in sdm_getWVMCalKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getWVMCalKeys
! ---------------------------------------------------------------------------
!
subroutine allocWVMCalRow(row, error)
  use sdm_WVMCal
  type(WVMCalRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'WVMCal'
  ! row%polyFreqLimits allocation
  if (allocated(row%polyFreqLimits)) then
    deallocate(row%polyFreqLimits, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%pathCoeff allocation
  if (allocated(row%pathCoeff)) then
    deallocate(row%pathCoeff, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%refTemp allocation
  if (allocated(row%refTemp)) then
    deallocate(row%refTemp, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%polyFreqLimits(2),  row%pathCoeff(row%numPoly, row%numChan),  row%refTemp(row%numChan), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocWVMCalRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! ExecBlock Table:
!
! ===========================================================================
!
subroutine addExecBlockRow(key, row, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockRow) :: row
  type(ExecBlockKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addExecBlockRow, i, j
  integer :: execBlockUIDDim(2)
  integer :: projectIdDim(2)
  integer :: configNameDim(2)
  integer :: telescopeNameDim(2)
  integer :: observerNameDim(2)
  integer :: observingLogDim(2)
  integer :: sessionReferenceDim(2)
  integer :: sbSummaryDim(2)
  integer :: schedulerModeDim(2)
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  ! ----------------
  ! Deal with dimensions
  execBlockUIDDim = len(row%execBlockUID)
  call charcut(row%execBlockUID)
  projectIdDim = len(row%projectId)
  call charcut(row%projectId)
  configNameDim = len(row%configName)
  call charcut(row%configName)
  telescopeNameDim = len(row%telescopeName)
  call charcut(row%telescopeName)
  observerNameDim = len(row%observerName)
  call charcut(row%observerName)
  observingLogDim = len(row%observingLog)
  call charcut(row%observingLog)
  sessionReferenceDim = len(row%sessionReference)
  call charcut(row%sessionReference)
  sbSummaryDim = len(row%sbSummary)
  call charcut(row%sbSummary)
  schedulerModeDim = len(row%schedulerMode)
  call charcut(row%schedulerMode)
  if (.not.allocated(row%antennaId)) then
    call sdmMessage(8,3,'ExecBlockTable','row%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(row%antennaId,i)
  enddo
  !
  ireturn = sdm_addExecBlockRow(row%startTime, row%endTime, row%execBlockNum, row%execBlockUID, execBlockUIDDim,&
      & row%projectId, projectIdDim, row%configName, configNameDim, row%telescopeName, telescopeNameDim,&
      & row%observerName, observerNameDim, row%observingLog, observingLogDim, row%sessionReference,&
      & sessionReferenceDim, row%sbSummary, sbSummaryDim, row%schedulerMode, schedulerModeDim, row%baseRangeMin,&
      & row%baseRangeMax, row%baseRmsMinor, row%baseRmsMajor, row%basePa, row%siteAltitude, row%siteLongitude,&
      & row%siteLatitude, row%aborted, row%numAntenna, row%antennaId, antennaIdDim, row%sBSummaryId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_addExecBlockRow', ireturn)
    error = .true.
  else
    key%execBlockId = ireturn
  endif
end subroutine addExecBlockRow
!
! ---------------------------------------------------------------------------
!
subroutine getExecBlockRow(key, row, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockRow) :: row
  type(ExecBlockKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getExecBlockRow, i
  integer :: execBlockUIDDim(2)
  integer :: projectIdDim(2)
  integer :: configNameDim(2)
  integer :: telescopeNameDim(2)
  integer :: observerNameDim(2)
  integer :: observingLogDim(2)
  integer :: sessionReferenceDim(2)
  integer :: sbSummaryDim(2)
  integer :: schedulerModeDim(2)
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  ! Deal with dimensions
  execBlockUIDDim = len(row%execBlockUID)
  call charcut(row%execBlockUID)
  projectIdDim = len(row%projectId)
  call charcut(row%projectId)
  configNameDim = len(row%configName)
  call charcut(row%configName)
  telescopeNameDim = len(row%telescopeName)
  call charcut(row%telescopeName)
  observerNameDim = len(row%observerName)
  call charcut(row%observerName)
  observingLogDim = len(row%observingLog)
  call charcut(row%observingLog)
  sessionReferenceDim = len(row%sessionReference)
  call charcut(row%sessionReference)
  sbSummaryDim = len(row%sbSummary)
  call charcut(row%sbSummary)
  schedulerModeDim = len(row%schedulerMode)
  call charcut(row%schedulerMode)
  if (.not.allocated(row%antennaId)) then
    call sdmMessage(8,3,'ExecBlockTable','row%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(row%antennaId,i)
  enddo
  !
  ireturn = sdm_getExecBlockRow(key%execBlockId, row%startTime, row%endTime, row%execBlockNum, row%execBlockUID,&
      & execBlockUIDDim, row%projectId, projectIdDim, row%configName, configNameDim, row%telescopeName,&
      & telescopeNameDim, row%observerName, observerNameDim, row%observingLog, observingLogDim, row%sessionReference,&
      & sessionReferenceDim, row%sbSummary, sbSummaryDim, row%schedulerMode, schedulerModeDim, row%baseRangeMin,&
      & row%baseRangeMax, row%baseRmsMinor, row%baseRmsMajor, row%basePa, row%siteAltitude, row%siteLongitude,&
      & row%siteLatitude, row%aborted, row%numAntenna, row%antennaId, antennaIdDim, row%sBSummaryId)
  row%execBlockUID(execBlockUIDDim(1)+1:) = ''
  row%projectId(projectIdDim(1)+1:) = ''
  row%configName(configNameDim(1)+1:) = ''
  row%telescopeName(telescopeNameDim(1)+1:) = ''
  row%observerName(observerNameDim(1)+1:) = ''
  row%observingLog(observingLogDim(1)+1:) = ''
  row%sessionReference(sessionReferenceDim(1)+1:) = ''
  row%sbSummary(sbSummaryDim(1)+1:) = ''
  row%schedulerMode(schedulerModeDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_getExecBlockRow', ireturn)
    error = .true.
  endif
end subroutine getExecBlockRow
!
! ---------------------------------------------------------------------------
!
subroutine getExecBlockTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getExecBlockTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getExecBlockTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getExecBlockTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getExecBlockKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_ExecBlock
  !
  integer :: tableSize
  logical :: error
  type(ExecBlockKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getExecBlockKeys, i
  integer, allocatable :: execBlockIdList(:)
  !
  allocate(execBlockIdList(tableSize))
  !
  ireturn = sdm_getExecBlockKeys(execBlockIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_getExecBlockKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%execBlockId = execBlockIdList(i)
  enddo
end subroutine getExecBlockKeys
! ---------------------------------------------------------------------------
!
subroutine allocExecBlockRow(row, error)
  use sdm_ExecBlock
  type(ExecBlockRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'ExecBlock'
  ! row%antennaId allocation
  if (allocated(row%antennaId)) then
    deallocate(row%antennaId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%antennaId(row%numAntenna), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocExecBlockRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addExecBlockReleaseDate(key, opt, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockKey) :: key
  type(ExecBlockOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addExecBlockReleaseDate, i
  !! integer*8 :: releaseDate
  ! Deal with dimensions
  ireturn = sdm_addExecBlockReleaseDate(key%execBlockId, opt%releaseDate)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_addExecBlockReleaseDate', ireturn)
    error = .true.
  endif
end subroutine addExecBlockReleaseDate
!
! ---------------------------------------------------------------------------
!
subroutine getExecBlockReleaseDate(key, opt, present, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockKey) :: key
  type(ExecBlockOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getExecBlockReleaseDate, i
  ! Deal with dimensions
  ireturn = sdm_getExecBlockReleaseDate(key%execBlockId, opt%releaseDate)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_getExecBlockReleaseDate', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getExecBlockReleaseDate
!
!
! ---------------------------------------------------------------------------
!
subroutine addExecBlockFlagRow(key, opt, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockKey) :: key
  type(ExecBlockOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addExecBlockFlagRow, i
  !! logical*1 :: flagRow
  ! Deal with dimensions
  ireturn = sdm_addExecBlockFlagRow(key%execBlockId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_addExecBlockFlagRow', ireturn)
    error = .true.
  endif
end subroutine addExecBlockFlagRow
!
! ---------------------------------------------------------------------------
!
subroutine getExecBlockFlagRow(key, opt, present, error)
  !
  use sdm_ExecBlock
  !
  type(ExecBlockKey) :: key
  type(ExecBlockOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getExecBlockFlagRow, i
  ! Deal with dimensions
  ireturn = sdm_getExecBlockFlagRow(key%execBlockId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ExecBlockTable','Error in sdm_getExecBlockFlagRow', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getExecBlockFlagRow
!
!
! ===========================================================================
!
! Scan Table:
!
! ===========================================================================
!
subroutine addScanRow(key, row, error)
  !
  use sdm_Scan
  !
  type(ScanRow) :: row
  type(ScanKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addScanRow, i, j
  integer, parameter :: scanIntentRank = 1
  integer :: scanIntentDim(2,scanIntentRank)
  integer, parameter :: calDataTypeRank = 1
  integer :: calDataTypeDim(2,calDataTypeRank)
  integer, parameter :: calibrationOnLineRank = 1
  integer :: calibrationOnLineDim(2,calibrationOnLineRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%scanIntent)) then
    call sdmMessage(8,3,'ScanTable','row%scanIntent not allocated.')
    error = .true.
    return
  endif
  do i=1, scanIntentRank
    scanIntentDim(:,i) = size(row%scanIntent,i)
  enddo
  if (.not.allocated(row%calDataType)) then
    call sdmMessage(8,3,'ScanTable','row%calDataType not allocated.')
    error = .true.
    return
  endif
  do i=1, calDataTypeRank
    calDataTypeDim(:,i) = size(row%calDataType,i)
  enddo
  if (.not.allocated(row%calibrationOnLine)) then
    call sdmMessage(8,3,'ScanTable','row%calibrationOnLine not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationOnLineRank
    calibrationOnLineDim(:,i) = size(row%calibrationOnLine,i)
  enddo
  !
  ireturn = sdm_addScanRow(key%execBlockId, key%scanNumber, row%startTime, row%endTime, row%numIntent, row%numSubScan,&
      & row%scanIntent, scanIntentDim, row%calDataType, calDataTypeDim, row%calibrationOnLine, calibrationOnLineDim,&
      & row%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanRow', ireturn)
    error = .true.
  endif
end subroutine addScanRow
!
! ---------------------------------------------------------------------------
!
subroutine getScanRow(key, row, error)
  !
  use sdm_Scan
  !
  type(ScanRow) :: row
  type(ScanKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getScanRow, i
  integer, parameter :: scanIntentRank = 1
  integer :: scanIntentDim(2,scanIntentRank)
  integer, parameter :: calDataTypeRank = 1
  integer :: calDataTypeDim(2,calDataTypeRank)
  integer, parameter :: calibrationOnLineRank = 1
  integer :: calibrationOnLineDim(2,calibrationOnLineRank)
  ! Deal with dimensions
  if (.not.allocated(row%scanIntent)) then
    call sdmMessage(8,3,'ScanTable','row%scanIntent not allocated.')
    error = .true.
    return
  endif
  do i=1, scanIntentRank
    scanIntentDim(:,i) = size(row%scanIntent,i)
  enddo
  if (.not.allocated(row%calDataType)) then
    call sdmMessage(8,3,'ScanTable','row%calDataType not allocated.')
    error = .true.
    return
  endif
  do i=1, calDataTypeRank
    calDataTypeDim(:,i) = size(row%calDataType,i)
  enddo
  if (.not.allocated(row%calibrationOnLine)) then
    call sdmMessage(8,3,'ScanTable','row%calibrationOnLine not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationOnLineRank
    calibrationOnLineDim(:,i) = size(row%calibrationOnLine,i)
  enddo
  !
  ireturn = sdm_getScanRow(key%execBlockId, key%scanNumber, row%startTime, row%endTime, row%numIntent, row%numSubScan,&
      & row%scanIntent, scanIntentDim, row%calDataType, calDataTypeDim, row%calibrationOnLine, calibrationOnLineDim,&
      & row%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanRow', ireturn)
    error = .true.
  endif
end subroutine getScanRow
!
! ---------------------------------------------------------------------------
!
subroutine getScanTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getScanTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getScanTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getScanTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getScanKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Scan
  !
  integer :: tableSize
  logical :: error
  type(ScanKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getScanKeys, i
  integer, allocatable :: execBlockIdList(:)
  integer, allocatable :: scanNumberList(:)
  !
  allocate(execBlockIdList(tableSize))
  allocate(scanNumberList(tableSize))
  !
  ireturn = sdm_getScanKeys(execBlockIdList, scanNumberList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%execBlockId = execBlockIdList(i)
    keyList(i)%scanNumber = scanNumberList(i)
  enddo
end subroutine getScanKeys
! ---------------------------------------------------------------------------
!
subroutine allocScanRow(row, error)
  use sdm_Scan
  type(ScanRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Scan'
  ! row%scanIntent allocation
  if (allocated(row%scanIntent)) then
    deallocate(row%scanIntent, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%calDataType allocation
  if (allocated(row%calDataType)) then
    deallocate(row%calDataType, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%calibrationOnLine allocation
  if (allocated(row%calibrationOnLine)) then
    deallocate(row%calibrationOnLine, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%scanIntent(row%numIntent), row%calDataType(row%numIntent), row%calibrationOnLine(row%numIntent),&
      & stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocScanRow
!
! ---------------------------------------------------------------------------
!
subroutine allocScanOpt(row, opt, error)
  use sdm_Scan
  type(ScanRow) :: row
  type(ScanOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'Scan'
  ! opt%calibrationFunction allocation
  if (allocated(opt%calibrationFunction)) then
    deallocate(opt%calibrationFunction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%calibrationSet allocation
  if (allocated(opt%calibrationSet)) then
    deallocate(opt%calibrationSet, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%calPattern allocation
  if (allocated(opt%calPattern)) then
    deallocate(opt%calPattern, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%fieldName allocation
  if (allocated(opt%fieldName)) then
    deallocate(opt%fieldName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%calibrationFunction(row%numIntent), opt%calibrationSet(row%numIntent), opt%calPattern(row%numIntent),&
      & opt%fieldName(opt%numField), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocScanOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addScanCalibrationFunction(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanCalibrationFunction, i
  !! integer, allocatable :: calibrationFunction(:)
  integer, parameter :: calibrationFunctionRank = 1
  integer :: calibrationFunctionDim(2,calibrationFunctionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calibrationFunction)) then
    call sdmMessage(8,3,'ScanTable','opt%calibrationFunction not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationFunctionRank
    calibrationFunctionDim(:,i) = size(opt%calibrationFunction,i)
  enddo
  ireturn = sdm_addScanCalibrationFunction(key%execBlockId, key%scanNumber, opt%calibrationFunction,&
      & calibrationFunctionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanCalibrationFunction', ireturn)
    error = .true.
  endif
end subroutine addScanCalibrationFunction
!
! ---------------------------------------------------------------------------
!
subroutine getScanCalibrationFunction(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanCalibrationFunction, i
  integer, parameter :: calibrationFunctionRank = 1
  integer :: calibrationFunctionDim(2,calibrationFunctionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calibrationFunction)) then
    call sdmMessage(8,3,'ScanTable','opt%calibrationFunction not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationFunctionRank
    calibrationFunctionDim(:,i) = size(opt%calibrationFunction,i)
  enddo
  ireturn = sdm_getScanCalibrationFunction(key%execBlockId, key%scanNumber, opt%calibrationFunction,&
      & calibrationFunctionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanCalibrationFunction', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanCalibrationFunction
!
!
! ---------------------------------------------------------------------------
!
subroutine addScanCalibrationSet(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanCalibrationSet, i
  !! integer, allocatable :: calibrationSet(:)
  integer, parameter :: calibrationSetRank = 1
  integer :: calibrationSetDim(2,calibrationSetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calibrationSet)) then
    call sdmMessage(8,3,'ScanTable','opt%calibrationSet not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationSetRank
    calibrationSetDim(:,i) = size(opt%calibrationSet,i)
  enddo
  ireturn = sdm_addScanCalibrationSet(key%execBlockId, key%scanNumber, opt%calibrationSet, calibrationSetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanCalibrationSet', ireturn)
    error = .true.
  endif
end subroutine addScanCalibrationSet
!
! ---------------------------------------------------------------------------
!
subroutine getScanCalibrationSet(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanCalibrationSet, i
  integer, parameter :: calibrationSetRank = 1
  integer :: calibrationSetDim(2,calibrationSetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calibrationSet)) then
    call sdmMessage(8,3,'ScanTable','opt%calibrationSet not allocated.')
    error = .true.
    return
  endif
  do i=1, calibrationSetRank
    calibrationSetDim(:,i) = size(opt%calibrationSet,i)
  enddo
  ireturn = sdm_getScanCalibrationSet(key%execBlockId, key%scanNumber, opt%calibrationSet, calibrationSetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanCalibrationSet', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanCalibrationSet
!
!
! ---------------------------------------------------------------------------
!
subroutine addScanCalPattern(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanCalPattern, i
  !! integer, allocatable :: calPattern(:)
  integer, parameter :: calPatternRank = 1
  integer :: calPatternDim(2,calPatternRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calPattern)) then
    call sdmMessage(8,3,'ScanTable','opt%calPattern not allocated.')
    error = .true.
    return
  endif
  do i=1, calPatternRank
    calPatternDim(:,i) = size(opt%calPattern,i)
  enddo
  ireturn = sdm_addScanCalPattern(key%execBlockId, key%scanNumber, opt%calPattern, calPatternDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanCalPattern', ireturn)
    error = .true.
  endif
end subroutine addScanCalPattern
!
! ---------------------------------------------------------------------------
!
subroutine getScanCalPattern(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanCalPattern, i
  integer, parameter :: calPatternRank = 1
  integer :: calPatternDim(2,calPatternRank)
  ! Deal with dimensions
  if (.not.allocated(opt%calPattern)) then
    call sdmMessage(8,3,'ScanTable','opt%calPattern not allocated.')
    error = .true.
    return
  endif
  do i=1, calPatternRank
    calPatternDim(:,i) = size(opt%calPattern,i)
  enddo
  ireturn = sdm_getScanCalPattern(key%execBlockId, key%scanNumber, opt%calPattern, calPatternDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanCalPattern', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanCalPattern
!
!
! ---------------------------------------------------------------------------
!
subroutine addScanNumField(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanNumField, i
  !! integer :: numField
  ! Deal with dimensions
  ireturn = sdm_addScanNumField(key%execBlockId, key%scanNumber, opt%numField)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanNumField', ireturn)
    error = .true.
  endif
end subroutine addScanNumField
!
! ---------------------------------------------------------------------------
!
subroutine getScanNumField(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanNumField, i
  ! Deal with dimensions
  ireturn = sdm_getScanNumField(key%execBlockId, key%scanNumber, opt%numField)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanNumField', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanNumField
!
!
! ---------------------------------------------------------------------------
!
subroutine addScanFieldName(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanFieldName, i
  !! character*256, allocatable :: fieldName(:)
  integer, parameter :: fieldNameRank = 1
  integer :: fieldNameDim(2,fieldNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%fieldName)) then
    call sdmMessage(8,3,'ScanTable','opt%fieldName not allocated.')
    error = .true.
    return
  endif
  fieldNameDim(:,1) = len(opt%fieldName)
  do i=1, fieldNameRank
    fieldNameDim(:,i+1) = size(opt%fieldName,i)
    call charcut(opt%fieldName(i))
  enddo
  ireturn = sdm_addScanFieldName(key%execBlockId, key%scanNumber, opt%fieldName, fieldNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanFieldName', ireturn)
    error = .true.
  endif
end subroutine addScanFieldName
!
! ---------------------------------------------------------------------------
!
subroutine getScanFieldName(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanFieldName, i
  integer, parameter :: fieldNameRank = 1
  integer :: fieldNameDim(2,fieldNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%fieldName)) then
    call sdmMessage(8,3,'ScanTable','opt%fieldName not allocated.')
    error = .true.
    return
  endif
  fieldNameDim(:,1) = len(opt%fieldName)
  do i=1, fieldNameRank
    fieldNameDim(:,i+1) = size(opt%fieldName,i)
  enddo
  ireturn = sdm_getScanFieldName(key%execBlockId, key%scanNumber, opt%fieldName, fieldNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanFieldName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanFieldName
!
!
! ---------------------------------------------------------------------------
!
subroutine addScanSourceName(key, opt, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addScanSourceName, i
  !! character*256 :: sourceName
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  sourceNameDim = len(opt%sourceName)
  call charcut(opt%sourceName)
  ireturn = sdm_addScanSourceName(key%execBlockId, key%scanNumber, opt%sourceName, sourceNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_addScanSourceName', ireturn)
    error = .true.
  endif
end subroutine addScanSourceName
!
! ---------------------------------------------------------------------------
!
subroutine getScanSourceName(key, opt, present, error)
  !
  use sdm_Scan
  !
  type(ScanKey) :: key
  type(ScanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getScanSourceName, i
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  sourceNameDim = len(opt%sourceName)
  ireturn = sdm_getScanSourceName(key%execBlockId, key%scanNumber, opt%sourceName, sourceNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'ScanTable','Error in sdm_getScanSourceName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getScanSourceName
!
!
! ===========================================================================
!
! Subscan Table:
!
! ===========================================================================
!
subroutine addSubscanRow(key, row, error)
  !
  use sdm_Subscan
  !
  type(SubscanRow) :: row
  type(SubscanKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSubscanRow, i, j
  integer :: fieldNameDim(2)
  integer, parameter :: numberSubintegrationRank = 1
  integer :: numberSubintegrationDim(2,numberSubintegrationRank)
  ! ----------------
  ! Deal with dimensions
  fieldNameDim = len(row%fieldName)
  call charcut(row%fieldName)
  if (.not.allocated(row%numberSubintegration)) then
    call sdmMessage(8,3,'SubscanTable','row%numberSubintegration not allocated.')
    error = .true.
    return
  endif
  do i=1, numberSubintegrationRank
    numberSubintegrationDim(:,i) = size(row%numberSubintegration,i)
  enddo
  !
  ireturn = sdm_addSubscanRow(key%execBlockId, key%scanNumber, key%subscanNumber, row%startTime, row%endTime,&
      & row%fieldName, fieldNameDim, row%subscanIntent, row%numberIntegration, row%numberSubintegration,&
      & numberSubintegrationDim, row%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_addSubscanRow', ireturn)
    error = .true.
  endif
end subroutine addSubscanRow
!
! ---------------------------------------------------------------------------
!
subroutine getSubscanRow(key, row, error)
  !
  use sdm_Subscan
  !
  type(SubscanRow) :: row
  type(SubscanKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSubscanRow, i
  integer :: fieldNameDim(2)
  integer, parameter :: numberSubintegrationRank = 1
  integer :: numberSubintegrationDim(2,numberSubintegrationRank)
  ! Deal with dimensions
  fieldNameDim = len(row%fieldName)
  call charcut(row%fieldName)
  if (.not.allocated(row%numberSubintegration)) then
    call sdmMessage(8,3,'SubscanTable','row%numberSubintegration not allocated.')
    error = .true.
    return
  endif
  do i=1, numberSubintegrationRank
    numberSubintegrationDim(:,i) = size(row%numberSubintegration,i)
  enddo
  !
  ireturn = sdm_getSubscanRow(key%execBlockId, key%scanNumber, key%subscanNumber, row%startTime, row%endTime,&
      & row%fieldName, fieldNameDim, row%subscanIntent, row%numberIntegration, row%numberSubintegration,&
      & numberSubintegrationDim, row%flagRow)
  row%fieldName(fieldNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_getSubscanRow', ireturn)
    error = .true.
  endif
end subroutine getSubscanRow
!
! ---------------------------------------------------------------------------
!
subroutine getSubscanTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSubscanTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSubscanTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSubscanTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSubscanKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Subscan
  !
  integer :: tableSize
  logical :: error
  type(SubscanKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSubscanKeys, i
  integer, allocatable :: execBlockIdList(:)
  integer, allocatable :: scanNumberList(:)
  integer, allocatable :: subscanNumberList(:)
  !
  allocate(execBlockIdList(tableSize))
  allocate(scanNumberList(tableSize))
  allocate(subscanNumberList(tableSize))
  !
  ireturn = sdm_getSubscanKeys(execBlockIdList, scanNumberList, subscanNumberList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_getSubscanKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%execBlockId = execBlockIdList(i)
    keyList(i)%scanNumber = scanNumberList(i)
    keyList(i)%subscanNumber = subscanNumberList(i)
  enddo
end subroutine getSubscanKeys
! ---------------------------------------------------------------------------
!
subroutine allocSubscanRow(row, error)
  use sdm_Subscan
  type(SubscanRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Subscan'
  ! row%numberSubintegration allocation
  if (allocated(row%numberSubintegration)) then
    deallocate(row%numberSubintegration, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%numberSubintegration(row%numberIntegration), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocSubscanRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addSubscanSubscanMode(key, opt, error)
  !
  use sdm_Subscan
  !
  type(SubscanKey) :: key
  type(SubscanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSubscanSubscanMode, i
  !! integer :: subscanMode
  ! Deal with dimensions
  ireturn = sdm_addSubscanSubscanMode(key%execBlockId, key%scanNumber, key%subscanNumber, opt%subscanMode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_addSubscanSubscanMode', ireturn)
    error = .true.
  endif
end subroutine addSubscanSubscanMode
!
! ---------------------------------------------------------------------------
!
subroutine getSubscanSubscanMode(key, opt, present, error)
  !
  use sdm_Subscan
  !
  type(SubscanKey) :: key
  type(SubscanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSubscanSubscanMode, i
  ! Deal with dimensions
  ireturn = sdm_getSubscanSubscanMode(key%execBlockId, key%scanNumber, key%subscanNumber, opt%subscanMode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_getSubscanSubscanMode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSubscanSubscanMode
!
!
! ---------------------------------------------------------------------------
!
subroutine addSubscanCorrelatorCalibration(key, opt, error)
  !
  use sdm_Subscan
  !
  type(SubscanKey) :: key
  type(SubscanOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addSubscanCorrelatorCalibration, i
  !! integer :: correlatorCalibration
  ! Deal with dimensions
  ireturn = sdm_addSubscanCorrelatorCalibration(key%execBlockId, key%scanNumber, key%subscanNumber,&
      & opt%correlatorCalibration)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_addSubscanCorrelatorCalibration', ireturn)
    error = .true.
  endif
end subroutine addSubscanCorrelatorCalibration
!
! ---------------------------------------------------------------------------
!
subroutine getSubscanCorrelatorCalibration(key, opt, present, error)
  !
  use sdm_Subscan
  !
  type(SubscanKey) :: key
  type(SubscanOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getSubscanCorrelatorCalibration, i
  ! Deal with dimensions
  ireturn = sdm_getSubscanCorrelatorCalibration(key%execBlockId, key%scanNumber, key%subscanNumber,&
      & opt%correlatorCalibration)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SubscanTable','Error in sdm_getSubscanCorrelatorCalibration', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getSubscanCorrelatorCalibration
!
!
! ===========================================================================
!
! Main Table:
!
! ===========================================================================
!
subroutine addMainRow(key, row, error)
  !
  use sdm_Main
  !
  type(MainRow) :: row
  type(MainKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addMainRow, i, j
  integer :: dataOidDim(2)
  integer, parameter :: stateIdRank = 1
  integer :: stateIdDim(2,stateIdRank)
  ! ----------------
  ! Deal with dimensions
  dataOidDim = len(row%dataOid)
  call charcut(row%dataOid)
  if (.not.allocated(row%stateId)) then
    call sdmMessage(8,3,'MainTable','row%stateId not allocated.')
    error = .true.
    return
  endif
  do i=1, stateIdRank
    stateIdDim(:,i) = size(row%stateId,i)
  enddo
  !
  ireturn = sdm_addMainRow(key%time, key%configDescriptionId, key%fieldId, row%numAntenna, row%timeSampling,&
      & row%interval, row%numIntegration, row%scanNumber, row%subscanNumber, row%dataSize, row%dataOid, dataOidDim,&
      & row%stateId, stateIdDim, row%execBlockId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'MainTable','Error in sdm_addMainRow', ireturn)
    error = .true.
  endif
end subroutine addMainRow
!
! ---------------------------------------------------------------------------
!
subroutine getMainRow(key, row, error)
  !
  use sdm_Main
  !
  type(MainRow) :: row
  type(MainKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getMainRow, i
  integer :: dataOidDim(2)
  integer, parameter :: stateIdRank = 1
  integer :: stateIdDim(2,stateIdRank)
  ! Deal with dimensions
  dataOidDim = len(row%dataOid)
  call charcut(row%dataOid)
  if (.not.allocated(row%stateId)) then
    call sdmMessage(8,3,'MainTable','row%stateId not allocated.')
    error = .true.
    return
  endif
  do i=1, stateIdRank
    stateIdDim(:,i) = size(row%stateId,i)
  enddo
  !
  ireturn = sdm_getMainRow(key%time, key%configDescriptionId, key%fieldId, row%numAntenna, row%timeSampling,&
      & row%interval, row%numIntegration, row%scanNumber, row%subscanNumber, row%dataSize, row%dataOid, dataOidDim,&
      & row%stateId, stateIdDim, row%execBlockId)
  row%dataOid(dataOidDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'MainTable','Error in sdm_getMainRow', ireturn)
    error = .true.
  endif
end subroutine getMainRow
!
! ---------------------------------------------------------------------------
!
subroutine getMainTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getMainTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getMainTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getMainTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getMainKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Main
  !
  integer :: tableSize
  logical :: error
  type(MainKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getMainKeys, i
  integer*8, allocatable :: timeList(:)
  integer, allocatable :: configDescriptionIdList(:)
  integer, allocatable :: fieldIdList(:)
  !
  allocate(timeList(tableSize))
  allocate(configDescriptionIdList(tableSize))
  allocate(fieldIdList(tableSize))
  !
  ireturn = sdm_getMainKeys(timeList, configDescriptionIdList, fieldIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'MainTable','Error in sdm_getMainKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%time = timeList(i)
    keyList(i)%configDescriptionId = configDescriptionIdList(i)
    keyList(i)%fieldId = fieldIdList(i)
  enddo
end subroutine getMainKeys
! ---------------------------------------------------------------------------
!
subroutine allocMainRow(row, error)
  use sdm_Main
  type(MainRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Main'
  ! row%stateId allocation
  if (allocated(row%stateId)) then
    deallocate(row%stateId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%stateId(row%numAntenna), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocMainRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addMainFlagRow(key, opt, error)
  !
  use sdm_Main
  !
  type(MainKey) :: key
  type(MainOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addMainFlagRow, i
  !! logical*1 :: flagRow
  ! Deal with dimensions
  ireturn = sdm_addMainFlagRow(key%time, key%configDescriptionId, key%fieldId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'MainTable','Error in sdm_addMainFlagRow', ireturn)
    error = .true.
  endif
end subroutine addMainFlagRow
!
! ---------------------------------------------------------------------------
!
subroutine getMainFlagRow(key, opt, present, error)
  !
  use sdm_Main
  !
  type(MainKey) :: key
  type(MainOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getMainFlagRow, i
  ! Deal with dimensions
  ireturn = sdm_getMainFlagRow(key%time, key%configDescriptionId, key%fieldId, opt%flagRow)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'MainTable','Error in sdm_getMainFlagRow', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getMainFlagRow
!
!
! ===========================================================================
!
! FocusModel Table:
!
! ===========================================================================
!
subroutine addFocusModelRow(key, row, error)
  !
  use sdm_FocusModel
  !
  type(FocusModelRow) :: row
  type(FocusModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addFocusModelRow, i, j
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer :: assocNatureDim(2)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  do i=1, coeffNameDim(1,2)
    call charcut(row%coeffName(i))
  enddo
  if (.not.allocated(row%coeffFormula)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(row%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(row%coeffFormula,i)
  enddo
  do i=1, coeffFormulaDim(1,2)
    call charcut(row%coeffFormula(i))
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  assocNatureDim = len(row%assocNature)
  call charcut(row%assocNature)
  !
  ireturn = sdm_addFocusModelRow(key%antennaId, row%polarizationType, row%receiverBand, row%numCoeff, row%coeffName,&
      & coeffNameDim, row%coeffFormula, coeffFormulaDim, row%coeffVal, coeffValDim, row%assocNature, assocNatureDim,&
      & row%assocFocusModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusModelTable','Error in sdm_addFocusModelRow', ireturn)
    error = .true.
  else
    key%focusModelId = ireturn
  endif
end subroutine addFocusModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getFocusModelRow(key, row, error)
  !
  use sdm_FocusModel
  !
  type(FocusModelRow) :: row
  type(FocusModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getFocusModelRow, i
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer :: assocNatureDim(2)
  ! Deal with dimensions
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  if (.not.allocated(row%coeffFormula)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(row%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(row%coeffFormula,i)
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'FocusModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  assocNatureDim = len(row%assocNature)
  call charcut(row%assocNature)
  !
  ireturn = sdm_getFocusModelRow(key%antennaId, key%focusModelId, row%polarizationType, row%receiverBand, row%numCoeff,&
      & row%coeffName, coeffNameDim, row%coeffFormula, coeffFormulaDim, row%coeffVal, coeffValDim, row%assocNature,&
      & assocNatureDim, row%assocFocusModelId)
  do i=1,coeffNameDim(1,2)
    row%coeffName(i)(coeffNameDim(1,1)+1:) = ''
  enddo
  do i=1,coeffFormulaDim(1,2)
    row%coeffFormula(i)(coeffFormulaDim(1,1)+1:) = ''
  enddo
  row%assocNature(assocNatureDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusModelTable','Error in sdm_getFocusModelRow', ireturn)
    error = .true.
  endif
end subroutine getFocusModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getFocusModelTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getFocusModelTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getFocusModelTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getFocusModelTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getFocusModelKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_FocusModel
  !
  integer :: tableSize
  logical :: error
  type(FocusModelKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getFocusModelKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: focusModelIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(focusModelIdList(tableSize))
  !
  ireturn = sdm_getFocusModelKeys(antennaIdList, focusModelIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'FocusModelTable','Error in sdm_getFocusModelKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%focusModelId = focusModelIdList(i)
  enddo
end subroutine getFocusModelKeys
! ---------------------------------------------------------------------------
!
subroutine allocFocusModelRow(row, error)
  use sdm_FocusModel
  type(FocusModelRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'FocusModel'
  ! row%coeffName allocation
  if (allocated(row%coeffName)) then
    deallocate(row%coeffName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffFormula allocation
  if (allocated(row%coeffFormula)) then
    deallocate(row%coeffFormula, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffVal allocation
  if (allocated(row%coeffVal)) then
    deallocate(row%coeffVal, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%coeffName(row%numCoeff),  row%coeffFormula(row%numCoeff),  row%coeffVal(row%numCoeff), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocFocusModelRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! GainTracking Table:
!
! ===========================================================================
!
subroutine addGainTrackingRow(key, row, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingRow) :: row
  type(GainTrackingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addGainTrackingRow, i, j
  integer, parameter :: cableDelayRank = 1
  integer :: cableDelayDim(2,cableDelayRank)
  integer, parameter :: loPropagationDelayRank = 1
  integer :: loPropagationDelayDim(2,loPropagationDelayRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: receiverDelayRank = 1
  integer :: receiverDelayDim(2,receiverDelayRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%cableDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%cableDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, cableDelayRank
    cableDelayDim(:,i) = size(row%cableDelay,i)
  enddo
  if (.not.allocated(row%loPropagationDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%loPropagationDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, loPropagationDelayRank
    loPropagationDelayDim(:,i) = size(row%loPropagationDelay,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'GainTrackingTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%receiverDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%receiverDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, receiverDelayRank
    receiverDelayDim(:,i) = size(row%receiverDelay,i)
  enddo
  !
  ireturn = sdm_addGainTrackingRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%attenuator,&
      & row%numLO, row%numReceptor, row%cableDelay, cableDelayDim, row%crossPolarizationDelay, row%loPropagationDelay,&
      & loPropagationDelayDim, row%polarizationTypes, polarizationTypesDim, row%receiverDelay, receiverDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingRow', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingRow
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingRow(key, row, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingRow) :: row
  type(GainTrackingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getGainTrackingRow, i
  integer, parameter :: cableDelayRank = 1
  integer :: cableDelayDim(2,cableDelayRank)
  integer, parameter :: loPropagationDelayRank = 1
  integer :: loPropagationDelayDim(2,loPropagationDelayRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: receiverDelayRank = 1
  integer :: receiverDelayDim(2,receiverDelayRank)
  ! Deal with dimensions
  if (.not.allocated(row%cableDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%cableDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, cableDelayRank
    cableDelayDim(:,i) = size(row%cableDelay,i)
  enddo
  if (.not.allocated(row%loPropagationDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%loPropagationDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, loPropagationDelayRank
    loPropagationDelayDim(:,i) = size(row%loPropagationDelay,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'GainTrackingTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%receiverDelay)) then
    call sdmMessage(8,3,'GainTrackingTable','row%receiverDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, receiverDelayRank
    receiverDelayDim(:,i) = size(row%receiverDelay,i)
  enddo
  !
  ireturn = sdm_getGainTrackingRow(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, row%attenuator,&
      & row%numLO, row%numReceptor, row%cableDelay, cableDelayDim, row%crossPolarizationDelay, row%loPropagationDelay,&
      & loPropagationDelayDim, row%polarizationTypes, polarizationTypesDim, row%receiverDelay, receiverDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingRow', ireturn)
    error = .true.
  endif
end subroutine getGainTrackingRow
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getGainTrackingTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getGainTrackingTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getGainTrackingTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_GainTracking
  !
  integer :: tableSize
  logical :: error
  type(GainTrackingKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getGainTrackingKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: spectralWindowIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  integer, allocatable :: feedIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(spectralWindowIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  allocate(feedIdList(tableSize))
  !
  ireturn = sdm_getGainTrackingKeys(antennaIdList, spectralWindowIdList, timeIntervalList, feedIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%spectralWindowId = spectralWindowIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
    keyList(i)%feedId = feedIdList(i)
  enddo
end subroutine getGainTrackingKeys
! ---------------------------------------------------------------------------
!
subroutine allocGainTrackingRow(row, error)
  use sdm_GainTracking
  type(GainTrackingRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'GainTracking'
  ! row%cableDelay allocation
  if (allocated(row%cableDelay)) then
    deallocate(row%cableDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%loPropagationDelay allocation
  if (allocated(row%loPropagationDelay)) then
    deallocate(row%loPropagationDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%receiverDelay allocation
  if (allocated(row%receiverDelay)) then
    deallocate(row%receiverDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%cableDelay(row%numReceptor), row%loPropagationDelay(row%numLO), row%polarizationTypes(row%numReceptor),&
      & row%receiverDelay(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocGainTrackingRow
!
! ---------------------------------------------------------------------------
!
subroutine allocGainTrackingOpt(row, opt, error)
  use sdm_GainTracking
  type(GainTrackingRow) :: row
  type(GainTrackingOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'GainTracking'
  ! opt%freqOffset allocation
  if (allocated(opt%freqOffset)) then
    deallocate(opt%freqOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%phaseOffset allocation
  if (allocated(opt%phaseOffset)) then
    deallocate(opt%phaseOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%attFreq allocation
  if (allocated(opt%attFreq)) then
    deallocate(opt%attFreq, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%attSpectrum allocation
  if (allocated(opt%attSpectrum)) then
    deallocate(opt%attSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%freqOffset(row%numLO), opt%phaseOffset(row%numLO), opt%attFreq(opt%numAttFreq),&
      & opt%attSpectrum(opt%numAttFreq), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocGainTrackingOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingDelayOffset(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingDelayOffset, i
  !! real*8 :: delayOffset
  ! Deal with dimensions
  ireturn = sdm_addGainTrackingDelayOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%delayOffset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingDelayOffset', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingDelayOffset
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingDelayOffset(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingDelayOffset, i
  ! Deal with dimensions
  ireturn = sdm_getGainTrackingDelayOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%delayOffset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingDelayOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingDelayOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingFreqOffset(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingFreqOffset, i
  !! real*8, allocatable :: freqOffset(:)
  integer, parameter :: freqOffsetRank = 1
  integer :: freqOffsetDim(2,freqOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%freqOffset)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%freqOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, freqOffsetRank
    freqOffsetDim(:,i) = size(opt%freqOffset,i)
  enddo
  ireturn = sdm_addGainTrackingFreqOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%freqOffset, freqOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingFreqOffset', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingFreqOffset
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingFreqOffset(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingFreqOffset, i
  integer, parameter :: freqOffsetRank = 1
  integer :: freqOffsetDim(2,freqOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%freqOffset)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%freqOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, freqOffsetRank
    freqOffsetDim(:,i) = size(opt%freqOffset,i)
  enddo
  ireturn = sdm_getGainTrackingFreqOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%freqOffset, freqOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingFreqOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingFreqOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingPhaseOffset(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingPhaseOffset, i
  !! real*8, allocatable :: phaseOffset(:)
  integer, parameter :: phaseOffsetRank = 1
  integer :: phaseOffsetDim(2,phaseOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseOffset)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%phaseOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseOffsetRank
    phaseOffsetDim(:,i) = size(opt%phaseOffset,i)
  enddo
  ireturn = sdm_addGainTrackingPhaseOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseOffset, phaseOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingPhaseOffset', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingPhaseOffset
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingPhaseOffset(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingPhaseOffset, i
  integer, parameter :: phaseOffsetRank = 1
  integer :: phaseOffsetDim(2,phaseOffsetRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseOffset)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%phaseOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseOffsetRank
    phaseOffsetDim(:,i) = size(opt%phaseOffset,i)
  enddo
  ireturn = sdm_getGainTrackingPhaseOffset(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%phaseOffset, phaseOffsetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingPhaseOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingPhaseOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingSamplingLevel(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingSamplingLevel, i
  !! real :: samplingLevel
  ! Deal with dimensions
  ireturn = sdm_addGainTrackingSamplingLevel(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%samplingLevel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingSamplingLevel', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingSamplingLevel
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingSamplingLevel(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingSamplingLevel, i
  ! Deal with dimensions
  ireturn = sdm_getGainTrackingSamplingLevel(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%samplingLevel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingSamplingLevel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingSamplingLevel
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingNumAttFreq(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingNumAttFreq, i
  !! integer :: numAttFreq
  ! Deal with dimensions
  ireturn = sdm_addGainTrackingNumAttFreq(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%numAttFreq)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingNumAttFreq', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingNumAttFreq
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingNumAttFreq(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingNumAttFreq, i
  ! Deal with dimensions
  ireturn = sdm_getGainTrackingNumAttFreq(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%numAttFreq)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingNumAttFreq', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingNumAttFreq
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingAttFreq(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingAttFreq, i
  !! real*8, allocatable :: attFreq(:)
  integer, parameter :: attFreqRank = 1
  integer :: attFreqDim(2,attFreqRank)
  ! Deal with dimensions
  if (.not.allocated(opt%attFreq)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%attFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, attFreqRank
    attFreqDim(:,i) = size(opt%attFreq,i)
  enddo
  ireturn = sdm_addGainTrackingAttFreq(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%attFreq,&
      & attFreqDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingAttFreq', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingAttFreq
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingAttFreq(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingAttFreq, i
  integer, parameter :: attFreqRank = 1
  integer :: attFreqDim(2,attFreqRank)
  ! Deal with dimensions
  if (.not.allocated(opt%attFreq)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%attFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, attFreqRank
    attFreqDim(:,i) = size(opt%attFreq,i)
  enddo
  ireturn = sdm_getGainTrackingAttFreq(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId, opt%attFreq,&
      & attFreqDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingAttFreq', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingAttFreq
!
!
! ---------------------------------------------------------------------------
!
subroutine addGainTrackingAttSpectrum(key, opt, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addGainTrackingAttSpectrum, i
  !! complex*16, allocatable :: attSpectrum(:)
  integer, parameter :: attSpectrumRank = 1
  integer :: attSpectrumDim(2,attSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%attSpectrum)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%attSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, attSpectrumRank
    attSpectrumDim(:,i) = size(opt%attSpectrum,i)
  enddo
  ireturn = sdm_addGainTrackingAttSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%attSpectrum, attSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_addGainTrackingAttSpectrum', ireturn)
    error = .true.
  endif
end subroutine addGainTrackingAttSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getGainTrackingAttSpectrum(key, opt, present, error)
  !
  use sdm_GainTracking
  !
  type(GainTrackingKey) :: key
  type(GainTrackingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getGainTrackingAttSpectrum, i
  integer, parameter :: attSpectrumRank = 1
  integer :: attSpectrumDim(2,attSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%attSpectrum)) then
    call sdmMessage(8,3,'GainTrackingTable','opt%attSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, attSpectrumRank
    attSpectrumDim(:,i) = size(opt%attSpectrum,i)
  enddo
  ireturn = sdm_getGainTrackingAttSpectrum(key%antennaId, key%spectralWindowId, key%timeInterval, key%feedId,&
      & opt%attSpectrum, attSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'GainTrackingTable','Error in sdm_getGainTrackingAttSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getGainTrackingAttSpectrum
!
!
! ===========================================================================
!
! PointingModel Table:
!
! ===========================================================================
!
subroutine addPointingModelRow(key, row, error)
  !
  use sdm_PointingModel
  !
  type(PointingModelRow) :: row
  type(PointingModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addPointingModelRow, i, j
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer :: assocNatureDim(2)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'PointingModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  do i=1, coeffNameDim(1,2)
    call charcut(row%coeffName(i))
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'PointingModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  assocNatureDim = len(row%assocNature)
  call charcut(row%assocNature)
  !
  ireturn = sdm_addPointingModelRow(key%antennaId, row%numCoeff, row%coeffName, coeffNameDim, row%coeffVal,&
      & coeffValDim, row%polarizationType, row%receiverBand, row%assocNature, assocNatureDim, row%assocPointingModelId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingModelTable','Error in sdm_addPointingModelRow', ireturn)
    error = .true.
  else
    key%pointingModelId = ireturn
  endif
end subroutine addPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getPointingModelRow(key, row, error)
  !
  use sdm_PointingModel
  !
  type(PointingModelRow) :: row
  type(PointingModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getPointingModelRow, i
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer :: assocNatureDim(2)
  ! Deal with dimensions
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'PointingModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'PointingModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  assocNatureDim = len(row%assocNature)
  call charcut(row%assocNature)
  !
  ireturn = sdm_getPointingModelRow(key%antennaId, key%pointingModelId, row%numCoeff, row%coeffName, coeffNameDim,&
      & row%coeffVal, coeffValDim, row%polarizationType, row%receiverBand, row%assocNature, assocNatureDim,&
      & row%assocPointingModelId)
  do i=1,coeffNameDim(1,2)
    row%coeffName(i)(coeffNameDim(1,1)+1:) = ''
  enddo
  row%assocNature(assocNatureDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingModelTable','Error in sdm_getPointingModelRow', ireturn)
    error = .true.
  endif
end subroutine getPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getPointingModelTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getPointingModelTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getPointingModelTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getPointingModelTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getPointingModelKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_PointingModel
  !
  integer :: tableSize
  logical :: error
  type(PointingModelKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getPointingModelKeys, i
  integer, allocatable :: antennaIdList(:)
  integer, allocatable :: pointingModelIdList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(pointingModelIdList(tableSize))
  !
  ireturn = sdm_getPointingModelKeys(antennaIdList, pointingModelIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingModelTable','Error in sdm_getPointingModelKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%pointingModelId = pointingModelIdList(i)
  enddo
end subroutine getPointingModelKeys
! ---------------------------------------------------------------------------
!
subroutine allocPointingModelRow(row, error)
  use sdm_PointingModel
  type(PointingModelRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'PointingModel'
  ! row%coeffName allocation
  if (allocated(row%coeffName)) then
    deallocate(row%coeffName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffVal allocation
  if (allocated(row%coeffVal)) then
    deallocate(row%coeffVal, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%coeffName(row%numCoeff),  row%coeffVal(row%numCoeff), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine allocPointingModelOpt(row, opt, error)
  use sdm_PointingModel
  type(PointingModelRow) :: row
  type(PointingModelOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'PointingModel'
  ! opt%coeffFormula allocation
  if (allocated(opt%coeffFormula)) then
    deallocate(opt%coeffFormula, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%coeffFormula(row%numCoeff), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocPointingModelOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addPointingModelCoeffFormula(key, opt, error)
  !
  use sdm_PointingModel
  !
  type(PointingModelKey) :: key
  type(PointingModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addPointingModelCoeffFormula, i
  !! character*256, allocatable :: coeffFormula(:)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%coeffFormula)) then
    call sdmMessage(8,3,'PointingModelTable','opt%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(opt%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(opt%coeffFormula,i)
    call charcut(opt%coeffFormula(i))
  enddo
  ireturn = sdm_addPointingModelCoeffFormula(key%antennaId, key%pointingModelId, opt%coeffFormula, coeffFormulaDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingModelTable','Error in sdm_addPointingModelCoeffFormula', ireturn)
    error = .true.
  endif
end subroutine addPointingModelCoeffFormula
!
! ---------------------------------------------------------------------------
!
subroutine getPointingModelCoeffFormula(key, opt, present, error)
  !
  use sdm_PointingModel
  !
  type(PointingModelKey) :: key
  type(PointingModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getPointingModelCoeffFormula, i
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%coeffFormula)) then
    call sdmMessage(8,3,'PointingModelTable','opt%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(opt%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(opt%coeffFormula,i)
  enddo
  ireturn = sdm_getPointingModelCoeffFormula(key%antennaId, key%pointingModelId, opt%coeffFormula, coeffFormulaDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'PointingModelTable','Error in sdm_getPointingModelCoeffFormula', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getPointingModelCoeffFormula
!
!
! ===========================================================================
!
! CalAmpli Table:
!
! ===========================================================================
!
subroutine addCalAmpliRow(key, row, error)
  !
  use sdm_CalAmpli
  !
  type(CalAmpliRow) :: row
  type(CalAmpliKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalAmpliRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: apertureEfficiencyRank = 1
  integer :: apertureEfficiencyDim(2,apertureEfficiencyRank)
  integer, parameter :: apertureEfficiencyErrorRank = 1
  integer :: apertureEfficiencyErrorDim(2,apertureEfficiencyErrorRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalAmpliTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalAmpliTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%apertureEfficiency)) then
    call sdmMessage(8,3,'CalAmpliTable','row%apertureEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, apertureEfficiencyRank
    apertureEfficiencyDim(:,i) = size(row%apertureEfficiency,i)
  enddo
  if (.not.allocated(row%apertureEfficiencyError)) then
    call sdmMessage(8,3,'CalAmpliTable','row%apertureEfficiencyError not allocated.')
    error = .true.
    return
  endif
  do i=1, apertureEfficiencyErrorRank
    apertureEfficiencyErrorDim(:,i) = size(row%apertureEfficiencyError,i)
  enddo
  !
  ireturn = sdm_addCalAmpliRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%receiverBand,&
      & key%calDataId, key%calReductionId, row%numReceptor, row%polarizationTypes, polarizationTypesDim,&
      & row%startValidTime, row%endValidTime, row%frequencyRange, frequencyRangeDim, row%apertureEfficiency,&
      & apertureEfficiencyDim, row%apertureEfficiencyError, apertureEfficiencyErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAmpliTable','Error in sdm_addCalAmpliRow', ireturn)
    error = .true.
  endif
end subroutine addCalAmpliRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalAmpliRow(key, row, error)
  !
  use sdm_CalAmpli
  !
  type(CalAmpliRow) :: row
  type(CalAmpliKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalAmpliRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: apertureEfficiencyRank = 1
  integer :: apertureEfficiencyDim(2,apertureEfficiencyRank)
  integer, parameter :: apertureEfficiencyErrorRank = 1
  integer :: apertureEfficiencyErrorDim(2,apertureEfficiencyErrorRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalAmpliTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalAmpliTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%apertureEfficiency)) then
    call sdmMessage(8,3,'CalAmpliTable','row%apertureEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, apertureEfficiencyRank
    apertureEfficiencyDim(:,i) = size(row%apertureEfficiency,i)
  enddo
  if (.not.allocated(row%apertureEfficiencyError)) then
    call sdmMessage(8,3,'CalAmpliTable','row%apertureEfficiencyError not allocated.')
    error = .true.
    return
  endif
  do i=1, apertureEfficiencyErrorRank
    apertureEfficiencyErrorDim(:,i) = size(row%apertureEfficiencyError,i)
  enddo
  !
  ireturn = sdm_getCalAmpliRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%receiverBand,&
      & key%calDataId, key%calReductionId, row%numReceptor, row%polarizationTypes, polarizationTypesDim,&
      & row%startValidTime, row%endValidTime, row%frequencyRange, frequencyRangeDim, row%apertureEfficiency,&
      & apertureEfficiencyDim, row%apertureEfficiencyError, apertureEfficiencyErrorDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAmpliTable','Error in sdm_getCalAmpliRow', ireturn)
    error = .true.
  endif
end subroutine getCalAmpliRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalAmpliTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalAmpliTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalAmpliTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalAmpliTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalAmpliKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalAmpli
  !
  integer :: tableSize
  logical :: error
  type(CalAmpliKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalAmpliKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalAmpliKeys(antennaNameList, antennaNameListDim, atmPhaseCorrectionList, receiverBandList,&
      & calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAmpliTable','Error in sdm_getCalAmpliKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalAmpliKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalAmpliRow(row, error)
  use sdm_CalAmpli
  type(CalAmpliRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalAmpli'
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%apertureEfficiency allocation
  if (allocated(row%apertureEfficiency)) then
    deallocate(row%apertureEfficiency, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%apertureEfficiencyError allocation
  if (allocated(row%apertureEfficiencyError)) then
    deallocate(row%apertureEfficiencyError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%polarizationTypes(row%numReceptor), row%frequencyRange(2), row%apertureEfficiency(row%numReceptor),&
      & row%apertureEfficiencyError(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalAmpliRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalAmpliCorrectionValidity(key, opt, error)
  !
  use sdm_CalAmpli
  !
  type(CalAmpliKey) :: key
  type(CalAmpliOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAmpliCorrectionValidity, i
  integer :: antennaNameDim(2)
  !! logical*1 :: correctionValidity
  ! Deal with dimensions
  ireturn = sdm_addCalAmpliCorrectionValidity(key%antennaName, antennaNameDim, key%atmPhaseCorrection,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%correctionValidity)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAmpliTable','Error in sdm_addCalAmpliCorrectionValidity', ireturn)
    error = .true.
  endif
end subroutine addCalAmpliCorrectionValidity
!
! ---------------------------------------------------------------------------
!
subroutine getCalAmpliCorrectionValidity(key, opt, present, error)
  !
  use sdm_CalAmpli
  !
  type(CalAmpliKey) :: key
  type(CalAmpliOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAmpliCorrectionValidity, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalAmpliCorrectionValidity(key%antennaName, antennaNameDim, key%atmPhaseCorrection,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%correctionValidity)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAmpliTable','Error in sdm_getCalAmpliCorrectionValidity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAmpliCorrectionValidity
!
!
! ===========================================================================
!
! CalData Table:
!
! ===========================================================================
!
subroutine addCalDataRow(key, row, error)
  !
  use sdm_CalData
  !
  type(CalDataRow) :: row
  type(CalDataKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalDataRow, i, j
  integer :: execBlockUIDDim(2)
  integer, parameter :: scanSetRank = 1
  integer :: scanSetDim(2,scanSetRank)
  ! ----------------
  ! Deal with dimensions
  execBlockUIDDim = len(row%execBlockUID)
  call charcut(row%execBlockUID)
  if (.not.allocated(row%scanSet)) then
    call sdmMessage(8,3,'CalDataTable','row%scanSet not allocated.')
    error = .true.
    return
  endif
  do i=1, scanSetRank
    scanSetDim(:,i) = size(row%scanSet,i)
  enddo
  !
  ireturn = sdm_addCalDataRow(row%startTimeObserved, row%endTimeObserved, row%execBlockUID, execBlockUIDDim,&
      & row%calDataType, row%calType, row%numScan, row%scanSet, scanSetDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataRow', ireturn)
    error = .true.
  else
    key%calDataId = ireturn
  endif
end subroutine addCalDataRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataRow(key, row, error)
  !
  use sdm_CalData
  !
  type(CalDataRow) :: row
  type(CalDataKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalDataRow, i
  integer :: execBlockUIDDim(2)
  integer, parameter :: scanSetRank = 1
  integer :: scanSetDim(2,scanSetRank)
  ! Deal with dimensions
  execBlockUIDDim = len(row%execBlockUID)
  call charcut(row%execBlockUID)
  if (.not.allocated(row%scanSet)) then
    call sdmMessage(8,3,'CalDataTable','row%scanSet not allocated.')
    error = .true.
    return
  endif
  do i=1, scanSetRank
    scanSetDim(:,i) = size(row%scanSet,i)
  enddo
  !
  ireturn = sdm_getCalDataRow(key%calDataId, row%startTimeObserved, row%endTimeObserved, row%execBlockUID,&
      & execBlockUIDDim, row%calDataType, row%calType, row%numScan, row%scanSet, scanSetDim)
  row%execBlockUID(execBlockUIDDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataRow', ireturn)
    error = .true.
  endif
end subroutine getCalDataRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalDataTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalDataTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalDataTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalData
  !
  integer :: tableSize
  logical :: error
  type(CalDataKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalDataKeys, i
  integer, allocatable :: calDataIdList(:)
  !
  allocate(calDataIdList(tableSize))
  !
  ireturn = sdm_getCalDataKeys(calDataIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%calDataId = calDataIdList(i)
  enddo
end subroutine getCalDataKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalDataRow(row, error)
  use sdm_CalData
  type(CalDataRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalData'
  ! row%scanSet allocation
  if (allocated(row%scanSet)) then
    deallocate(row%scanSet, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%scanSet(row%numScan), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDataRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalDataOpt(row, opt, error)
  use sdm_CalData
  type(CalDataRow) :: row
  type(CalDataOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalData'
  ! opt%fieldName allocation
  if (allocated(opt%fieldName)) then
    deallocate(opt%fieldName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sourceName allocation
  if (allocated(opt%sourceName)) then
    deallocate(opt%sourceName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sourceCode allocation
  if (allocated(opt%sourceCode)) then
    deallocate(opt%sourceCode, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%scanIntent allocation
  if (allocated(opt%scanIntent)) then
    deallocate(opt%scanIntent, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%fieldName(row%numScan), opt%sourceName(row%numScan), opt%sourceCode(row%numScan),&
      & opt%scanIntent(row%numScan), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDataOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataAssocCalDataId(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataAssocCalDataId, i
  !! integer :: assocCalDataId
  ! Deal with dimensions
  ireturn = sdm_addCalDataAssocCalDataId(key%calDataId, opt%assocCalDataId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataAssocCalDataId', ireturn)
    error = .true.
  endif
end subroutine addCalDataAssocCalDataId
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataAssocCalDataId(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataAssocCalDataId, i
  ! Deal with dimensions
  ireturn = sdm_getCalDataAssocCalDataId(key%calDataId, opt%assocCalDataId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataAssocCalDataId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataAssocCalDataId
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataAssocCalNature(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataAssocCalNature, i
  !! integer :: assocCalNature
  ! Deal with dimensions
  ireturn = sdm_addCalDataAssocCalNature(key%calDataId, opt%assocCalNature)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataAssocCalNature', ireturn)
    error = .true.
  endif
end subroutine addCalDataAssocCalNature
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataAssocCalNature(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataAssocCalNature, i
  ! Deal with dimensions
  ireturn = sdm_getCalDataAssocCalNature(key%calDataId, opt%assocCalNature)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataAssocCalNature', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataAssocCalNature
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataFieldName(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataFieldName, i
  !! character*256, allocatable :: fieldName(:)
  integer, parameter :: fieldNameRank = 1
  integer :: fieldNameDim(2,fieldNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%fieldName)) then
    call sdmMessage(8,3,'CalDataTable','opt%fieldName not allocated.')
    error = .true.
    return
  endif
  fieldNameDim(:,1) = len(opt%fieldName)
  do i=1, fieldNameRank
    fieldNameDim(:,i+1) = size(opt%fieldName,i)
    call charcut(opt%fieldName(i))
  enddo
  ireturn = sdm_addCalDataFieldName(key%calDataId, opt%fieldName, fieldNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataFieldName', ireturn)
    error = .true.
  endif
end subroutine addCalDataFieldName
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataFieldName(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataFieldName, i
  integer, parameter :: fieldNameRank = 1
  integer :: fieldNameDim(2,fieldNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%fieldName)) then
    call sdmMessage(8,3,'CalDataTable','opt%fieldName not allocated.')
    error = .true.
    return
  endif
  fieldNameDim(:,1) = len(opt%fieldName)
  do i=1, fieldNameRank
    fieldNameDim(:,i+1) = size(opt%fieldName,i)
  enddo
  ireturn = sdm_getCalDataFieldName(key%calDataId, opt%fieldName, fieldNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataFieldName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataFieldName
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataSourceName(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataSourceName, i
  !! character*256, allocatable :: sourceName(:)
  integer, parameter :: sourceNameRank = 1
  integer :: sourceNameDim(2,sourceNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceName)) then
    call sdmMessage(8,3,'CalDataTable','opt%sourceName not allocated.')
    error = .true.
    return
  endif
  sourceNameDim(:,1) = len(opt%sourceName)
  do i=1, sourceNameRank
    sourceNameDim(:,i+1) = size(opt%sourceName,i)
    call charcut(opt%sourceName(i))
  enddo
  ireturn = sdm_addCalDataSourceName(key%calDataId, opt%sourceName, sourceNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataSourceName', ireturn)
    error = .true.
  endif
end subroutine addCalDataSourceName
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataSourceName(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataSourceName, i
  integer, parameter :: sourceNameRank = 1
  integer :: sourceNameDim(2,sourceNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceName)) then
    call sdmMessage(8,3,'CalDataTable','opt%sourceName not allocated.')
    error = .true.
    return
  endif
  sourceNameDim(:,1) = len(opt%sourceName)
  do i=1, sourceNameRank
    sourceNameDim(:,i+1) = size(opt%sourceName,i)
  enddo
  ireturn = sdm_getCalDataSourceName(key%calDataId, opt%sourceName, sourceNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataSourceName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataSourceName
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataSourceCode(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataSourceCode, i
  !! character*256, allocatable :: sourceCode(:)
  integer, parameter :: sourceCodeRank = 1
  integer :: sourceCodeDim(2,sourceCodeRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceCode)) then
    call sdmMessage(8,3,'CalDataTable','opt%sourceCode not allocated.')
    error = .true.
    return
  endif
  sourceCodeDim(:,1) = len(opt%sourceCode)
  do i=1, sourceCodeRank
    sourceCodeDim(:,i+1) = size(opt%sourceCode,i)
    call charcut(opt%sourceCode(i))
  enddo
  ireturn = sdm_addCalDataSourceCode(key%calDataId, opt%sourceCode, sourceCodeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataSourceCode', ireturn)
    error = .true.
  endif
end subroutine addCalDataSourceCode
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataSourceCode(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataSourceCode, i
  integer, parameter :: sourceCodeRank = 1
  integer :: sourceCodeDim(2,sourceCodeRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%sourceCode)) then
    call sdmMessage(8,3,'CalDataTable','opt%sourceCode not allocated.')
    error = .true.
    return
  endif
  sourceCodeDim(:,1) = len(opt%sourceCode)
  do i=1, sourceCodeRank
    sourceCodeDim(:,i+1) = size(opt%sourceCode,i)
  enddo
  ireturn = sdm_getCalDataSourceCode(key%calDataId, opt%sourceCode, sourceCodeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataSourceCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataSourceCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDataScanIntent(key, opt, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDataScanIntent, i
  !! integer, allocatable :: scanIntent(:)
  integer, parameter :: scanIntentRank = 1
  integer :: scanIntentDim(2,scanIntentRank)
  ! Deal with dimensions
  if (.not.allocated(opt%scanIntent)) then
    call sdmMessage(8,3,'CalDataTable','opt%scanIntent not allocated.')
    error = .true.
    return
  endif
  do i=1, scanIntentRank
    scanIntentDim(:,i) = size(opt%scanIntent,i)
  enddo
  ireturn = sdm_addCalDataScanIntent(key%calDataId, opt%scanIntent, scanIntentDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_addCalDataScanIntent', ireturn)
    error = .true.
  endif
end subroutine addCalDataScanIntent
!
! ---------------------------------------------------------------------------
!
subroutine getCalDataScanIntent(key, opt, present, error)
  !
  use sdm_CalData
  !
  type(CalDataKey) :: key
  type(CalDataOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDataScanIntent, i
  integer, parameter :: scanIntentRank = 1
  integer :: scanIntentDim(2,scanIntentRank)
  ! Deal with dimensions
  if (.not.allocated(opt%scanIntent)) then
    call sdmMessage(8,3,'CalDataTable','opt%scanIntent not allocated.')
    error = .true.
    return
  endif
  do i=1, scanIntentRank
    scanIntentDim(:,i) = size(opt%scanIntent,i)
  enddo
  ireturn = sdm_getCalDataScanIntent(key%calDataId, opt%scanIntent, scanIntentDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDataTable','Error in sdm_getCalDataScanIntent', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDataScanIntent
!
!
! ===========================================================================
!
! CalReduction Table:
!
! ===========================================================================
!
subroutine addCalReductionRow(key, row, error)
  !
  use sdm_CalReduction
  !
  type(CalReductionRow) :: row
  type(CalReductionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalReductionRow, i, j
  integer, parameter :: appliedCalibrationsRank = 1
  integer :: appliedCalibrationsDim(2,appliedCalibrationsRank+1)
  integer, parameter :: paramSetRank = 1
  integer :: paramSetDim(2,paramSetRank+1)
  integer, parameter :: invalidConditionsRank = 1
  integer :: invalidConditionsDim(2,invalidConditionsRank)
  integer :: messagesDim(2)
  integer :: softwareDim(2)
  integer :: softwareVersionDim(2)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%appliedCalibrations)) then
    call sdmMessage(8,3,'CalReductionTable','row%appliedCalibrations not allocated.')
    error = .true.
    return
  endif
  appliedCalibrationsDim(:,1) = len(row%appliedCalibrations)
  do i=1, appliedCalibrationsRank
    appliedCalibrationsDim(:,i+1) = size(row%appliedCalibrations,i)
  enddo
  do i=1, appliedCalibrationsDim(1,2)
    call charcut(row%appliedCalibrations(i))
  enddo
  if (.not.allocated(row%paramSet)) then
    call sdmMessage(8,3,'CalReductionTable','row%paramSet not allocated.')
    error = .true.
    return
  endif
  paramSetDim(:,1) = len(row%paramSet)
  do i=1, paramSetRank
    paramSetDim(:,i+1) = size(row%paramSet,i)
  enddo
  do i=1, paramSetDim(1,2)
    call charcut(row%paramSet(i))
  enddo
  if (.not.allocated(row%invalidConditions)) then
    call sdmMessage(8,3,'CalReductionTable','row%invalidConditions not allocated.')
    error = .true.
    return
  endif
  do i=1, invalidConditionsRank
    invalidConditionsDim(:,i) = size(row%invalidConditions,i)
  enddo
  messagesDim = len(row%messages)
  call charcut(row%messages)
  softwareDim = len(row%software)
  call charcut(row%software)
  softwareVersionDim = len(row%softwareVersion)
  call charcut(row%softwareVersion)
  !
  ireturn = sdm_addCalReductionRow(row%numApplied, row%appliedCalibrations, appliedCalibrationsDim, row%numParam,&
      & row%paramSet, paramSetDim, row%numInvalidConditions, row%invalidConditions, invalidConditionsDim,&
      & row%timeReduced, row%messages, messagesDim, row%software, softwareDim, row%softwareVersion, softwareVersionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalReductionTable','Error in sdm_addCalReductionRow', ireturn)
    error = .true.
  else
    key%calReductionId = ireturn
  endif
end subroutine addCalReductionRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalReductionRow(key, row, error)
  !
  use sdm_CalReduction
  !
  type(CalReductionRow) :: row
  type(CalReductionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalReductionRow, i
  integer, parameter :: appliedCalibrationsRank = 1
  integer :: appliedCalibrationsDim(2,appliedCalibrationsRank+1)
  integer, parameter :: paramSetRank = 1
  integer :: paramSetDim(2,paramSetRank+1)
  integer, parameter :: invalidConditionsRank = 1
  integer :: invalidConditionsDim(2,invalidConditionsRank)
  integer :: messagesDim(2)
  integer :: softwareDim(2)
  integer :: softwareVersionDim(2)
  ! Deal with dimensions
  if (.not.allocated(row%appliedCalibrations)) then
    call sdmMessage(8,3,'CalReductionTable','row%appliedCalibrations not allocated.')
    error = .true.
    return
  endif
  appliedCalibrationsDim(:,1) = len(row%appliedCalibrations)
  do i=1, appliedCalibrationsRank
    appliedCalibrationsDim(:,i+1) = size(row%appliedCalibrations,i)
  enddo
  if (.not.allocated(row%paramSet)) then
    call sdmMessage(8,3,'CalReductionTable','row%paramSet not allocated.')
    error = .true.
    return
  endif
  paramSetDim(:,1) = len(row%paramSet)
  do i=1, paramSetRank
    paramSetDim(:,i+1) = size(row%paramSet,i)
  enddo
  if (.not.allocated(row%invalidConditions)) then
    call sdmMessage(8,3,'CalReductionTable','row%invalidConditions not allocated.')
    error = .true.
    return
  endif
  do i=1, invalidConditionsRank
    invalidConditionsDim(:,i) = size(row%invalidConditions,i)
  enddo
  messagesDim = len(row%messages)
  call charcut(row%messages)
  softwareDim = len(row%software)
  call charcut(row%software)
  softwareVersionDim = len(row%softwareVersion)
  call charcut(row%softwareVersion)
  !
  ireturn = sdm_getCalReductionRow(key%calReductionId, row%numApplied, row%appliedCalibrations, appliedCalibrationsDim,&
      & row%numParam, row%paramSet, paramSetDim, row%numInvalidConditions, row%invalidConditions, invalidConditionsDim,&
      & row%timeReduced, row%messages, messagesDim, row%software, softwareDim, row%softwareVersion, softwareVersionDim)
  do i=1,appliedCalibrationsDim(1,2)
    row%appliedCalibrations(i)(appliedCalibrationsDim(1,1)+1:) = ''
  enddo
  do i=1,paramSetDim(1,2)
    row%paramSet(i)(paramSetDim(1,1)+1:) = ''
  enddo
  row%messages(messagesDim(1)+1:) = ''
  row%software(softwareDim(1)+1:) = ''
  row%softwareVersion(softwareVersionDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalReductionTable','Error in sdm_getCalReductionRow', ireturn)
    error = .true.
  endif
end subroutine getCalReductionRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalReductionTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalReductionTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalReductionTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalReductionTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalReductionKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalReduction
  !
  integer :: tableSize
  logical :: error
  type(CalReductionKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalReductionKeys, i
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalReductionKeys(calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalReductionTable','Error in sdm_getCalReductionKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalReductionKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalReductionRow(row, error)
  use sdm_CalReduction
  type(CalReductionRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalReduction'
  ! row%appliedCalibrations allocation
  if (allocated(row%appliedCalibrations)) then
    deallocate(row%appliedCalibrations, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%paramSet allocation
  if (allocated(row%paramSet)) then
    deallocate(row%paramSet, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%invalidConditions allocation
  if (allocated(row%invalidConditions)) then
    deallocate(row%invalidConditions, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%appliedCalibrations(row%numApplied), row%paramSet(row%numParam),&
      & row%invalidConditions(row%numInvalidConditions), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalReductionRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalPhase Table:
!
! ===========================================================================
!
subroutine addCalPhaseRow(key, row, error)
  !
  use sdm_CalPhase
  !
  type(CalPhaseRow) :: row
  type(CalPhaseKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalPhaseRow, i, j
  integer, parameter :: ampliRank = 2
  integer :: ampliDim(2,ampliRank)
  integer, parameter :: antennaNamesRank = 2
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer, parameter :: baselineLengthsRank = 1
  integer :: baselineLengthsDim(2,baselineLengthsRank)
  integer, parameter :: decorrelationFactorRank = 2
  integer :: decorrelationFactorDim(2,decorrelationFactorRank)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: phaseRank = 2
  integer :: phaseDim(2,phaseRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: phaseRMSRank = 2
  integer :: phaseRMSDim(2,phaseRMSRank)
  integer, parameter :: statPhaseRMSRank = 2
  integer :: statPhaseRMSDim(2,statPhaseRMSRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%ampli)) then
    call sdmMessage(8,3,'CalPhaseTable','row%ampli not allocated.')
    error = .true.
    return
  endif
  do i=1, ampliRank
    ampliDim(:,i) = size(row%ampli,i)
  enddo
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalPhaseTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  do i=1, antennaNamesDim(1,3)
    do j=1, antennaNamesDim(1,2)
      call charcut(row%antennaNames(j,i))
    enddo
  enddo
  if (.not.allocated(row%baselineLengths)) then
    call sdmMessage(8,3,'CalPhaseTable','row%baselineLengths not allocated.')
    error = .true.
    return
  endif
  do i=1, baselineLengthsRank
    baselineLengthsDim(:,i) = size(row%baselineLengths,i)
  enddo
  if (.not.allocated(row%decorrelationFactor)) then
    call sdmMessage(8,3,'CalPhaseTable','row%decorrelationFactor not allocated.')
    error = .true.
    return
  endif
  do i=1, decorrelationFactorRank
    decorrelationFactorDim(:,i) = size(row%decorrelationFactor,i)
  enddo
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalPhaseTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPhaseTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%phase)) then
    call sdmMessage(8,3,'CalPhaseTable','row%phase not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRank
    phaseDim(:,i) = size(row%phase,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPhaseTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%phaseRMS)) then
    call sdmMessage(8,3,'CalPhaseTable','row%phaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRMSRank
    phaseRMSDim(:,i) = size(row%phaseRMS,i)
  enddo
  if (.not.allocated(row%statPhaseRMS)) then
    call sdmMessage(8,3,'CalPhaseTable','row%statPhaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, statPhaseRMSRank
    statPhaseRMSDim(:,i) = size(row%statPhaseRMS,i)
  enddo
  !
  ireturn = sdm_addCalPhaseRow(key%basebandName, key%receiverBand, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%numBaseline, row%numReceptor, row%ampli,&
      & ampliDim, row%antennaNames, antennaNamesDim, row%baselineLengths, baselineLengthsDim, row%decorrelationFactor,&
      & decorrelationFactorDim, row%direction, directionDim, row%frequencyRange, frequencyRangeDim,&
      & row%integrationTime, row%phase, phaseDim, row%polarizationTypes, polarizationTypesDim, row%phaseRMS,&
      & phaseRMSDim, row%statPhaseRMS, statPhaseRMSDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPhaseTable','Error in sdm_addCalPhaseRow', ireturn)
    error = .true.
  endif
end subroutine addCalPhaseRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPhaseRow(key, row, error)
  !
  use sdm_CalPhase
  !
  type(CalPhaseRow) :: row
  type(CalPhaseKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalPhaseRow, i
  integer, parameter :: ampliRank = 2
  integer :: ampliDim(2,ampliRank)
  integer, parameter :: antennaNamesRank = 2
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer, parameter :: baselineLengthsRank = 1
  integer :: baselineLengthsDim(2,baselineLengthsRank)
  integer, parameter :: decorrelationFactorRank = 2
  integer :: decorrelationFactorDim(2,decorrelationFactorRank)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: phaseRank = 2
  integer :: phaseDim(2,phaseRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: phaseRMSRank = 2
  integer :: phaseRMSDim(2,phaseRMSRank)
  integer, parameter :: statPhaseRMSRank = 2
  integer :: statPhaseRMSDim(2,statPhaseRMSRank)
  ! Deal with dimensions
  if (.not.allocated(row%ampli)) then
    call sdmMessage(8,3,'CalPhaseTable','row%ampli not allocated.')
    error = .true.
    return
  endif
  do i=1, ampliRank
    ampliDim(:,i) = size(row%ampli,i)
  enddo
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalPhaseTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  if (.not.allocated(row%baselineLengths)) then
    call sdmMessage(8,3,'CalPhaseTable','row%baselineLengths not allocated.')
    error = .true.
    return
  endif
  do i=1, baselineLengthsRank
    baselineLengthsDim(:,i) = size(row%baselineLengths,i)
  enddo
  if (.not.allocated(row%decorrelationFactor)) then
    call sdmMessage(8,3,'CalPhaseTable','row%decorrelationFactor not allocated.')
    error = .true.
    return
  endif
  do i=1, decorrelationFactorRank
    decorrelationFactorDim(:,i) = size(row%decorrelationFactor,i)
  enddo
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalPhaseTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPhaseTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%phase)) then
    call sdmMessage(8,3,'CalPhaseTable','row%phase not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRank
    phaseDim(:,i) = size(row%phase,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPhaseTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%phaseRMS)) then
    call sdmMessage(8,3,'CalPhaseTable','row%phaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRMSRank
    phaseRMSDim(:,i) = size(row%phaseRMS,i)
  enddo
  if (.not.allocated(row%statPhaseRMS)) then
    call sdmMessage(8,3,'CalPhaseTable','row%statPhaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, statPhaseRMSRank
    statPhaseRMSDim(:,i) = size(row%statPhaseRMS,i)
  enddo
  !
  ireturn = sdm_getCalPhaseRow(key%basebandName, key%receiverBand, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%numBaseline, row%numReceptor, row%ampli,&
      & ampliDim, row%antennaNames, antennaNamesDim, row%baselineLengths, baselineLengthsDim, row%decorrelationFactor,&
      & decorrelationFactorDim, row%direction, directionDim, row%frequencyRange, frequencyRangeDim,&
      & row%integrationTime, row%phase, phaseDim, row%polarizationTypes, polarizationTypesDim, row%phaseRMS,&
      & phaseRMSDim, row%statPhaseRMS, statPhaseRMSDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPhaseTable','Error in sdm_getCalPhaseRow', ireturn)
    error = .true.
  endif
end subroutine getCalPhaseRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPhaseTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalPhaseTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalPhaseTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalPhaseTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalPhaseKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalPhase
  !
  integer :: tableSize
  logical :: error
  type(CalPhaseKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalPhaseKeys, i
  integer, allocatable :: basebandNameList(:)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(basebandNameList(tableSize))
  allocate(receiverBandList(tableSize))
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalPhaseKeys(basebandNameList, receiverBandList, atmPhaseCorrectionList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPhaseTable','Error in sdm_getCalPhaseKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%basebandName = basebandNameList(i)
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalPhaseKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalPhaseRow(row, error)
  use sdm_CalPhase
  type(CalPhaseRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalPhase'
  ! row%ampli allocation
  if (allocated(row%ampli)) then
    deallocate(row%ampli, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%antennaNames allocation
  if (allocated(row%antennaNames)) then
    deallocate(row%antennaNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%baselineLengths allocation
  if (allocated(row%baselineLengths)) then
    deallocate(row%baselineLengths, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%decorrelationFactor allocation
  if (allocated(row%decorrelationFactor)) then
    deallocate(row%decorrelationFactor, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%direction allocation
  if (allocated(row%direction)) then
    deallocate(row%direction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%phase allocation
  if (allocated(row%phase)) then
    deallocate(row%phase, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%phaseRMS allocation
  if (allocated(row%phaseRMS)) then
    deallocate(row%phaseRMS, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%statPhaseRMS allocation
  if (allocated(row%statPhaseRMS)) then
    deallocate(row%statPhaseRMS, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%ampli(row%numBaseline, row%numReceptor), row%antennaNames(2, row%numBaseline),&
      & row%baselineLengths(row%numBaseline), row%decorrelationFactor(row%numBaseline, row%numReceptor),&
      & row%direction(2), row%frequencyRange(2), row%phase(row%numBaseline, row%numReceptor),&
      & row%polarizationTypes(row%numReceptor), row%phaseRMS(row%numBaseline, row%numReceptor),&
      & row%statPhaseRMS(row%numBaseline, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPhaseRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalPhaseOpt(row, opt, error)
  use sdm_CalPhase
  type(CalPhaseRow) :: row
  type(CalPhaseOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalPhase'
  ! opt%correctionValidity allocation
  if (allocated(opt%correctionValidity)) then
    deallocate(opt%correctionValidity, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%correctionValidity(row%numBaseline), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPhaseOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalPhaseCorrectionValidity(key, opt, error)
  !
  use sdm_CalPhase
  !
  type(CalPhaseKey) :: key
  type(CalPhaseOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPhaseCorrectionValidity, i
  !! logical*1, allocatable :: correctionValidity(:)
  integer, parameter :: correctionValidityRank = 1
  integer :: correctionValidityDim(2,correctionValidityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%correctionValidity)) then
    call sdmMessage(8,3,'CalPhaseTable','opt%correctionValidity not allocated.')
    error = .true.
    return
  endif
  do i=1, correctionValidityRank
    correctionValidityDim(:,i) = size(opt%correctionValidity,i)
  enddo
  ireturn = sdm_addCalPhaseCorrectionValidity(key%basebandName, key%receiverBand, key%atmPhaseCorrection,&
      & key%calDataId, key%calReductionId, opt%correctionValidity, correctionValidityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPhaseTable','Error in sdm_addCalPhaseCorrectionValidity', ireturn)
    error = .true.
  endif
end subroutine addCalPhaseCorrectionValidity
!
! ---------------------------------------------------------------------------
!
subroutine getCalPhaseCorrectionValidity(key, opt, present, error)
  !
  use sdm_CalPhase
  !
  type(CalPhaseKey) :: key
  type(CalPhaseOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPhaseCorrectionValidity, i
  integer, parameter :: correctionValidityRank = 1
  integer :: correctionValidityDim(2,correctionValidityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%correctionValidity)) then
    call sdmMessage(8,3,'CalPhaseTable','opt%correctionValidity not allocated.')
    error = .true.
    return
  endif
  do i=1, correctionValidityRank
    correctionValidityDim(:,i) = size(opt%correctionValidity,i)
  enddo
  ireturn = sdm_getCalPhaseCorrectionValidity(key%basebandName, key%receiverBand, key%atmPhaseCorrection,&
      & key%calDataId, key%calReductionId, opt%correctionValidity, correctionValidityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPhaseTable','Error in sdm_getCalPhaseCorrectionValidity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPhaseCorrectionValidity
!
!
! ===========================================================================
!
! CalSeeing Table:
!
! ===========================================================================
!
subroutine addCalSeeingRow(key, row, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingRow) :: row
  type(CalSeeingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalSeeingRow, i, j
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: baselineLengthsRank = 1
  integer :: baselineLengthsDim(2,baselineLengthsRank)
  integer, parameter :: phaseRMSRank = 1
  integer :: phaseRMSDim(2,phaseRMSRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalSeeingTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%baselineLengths)) then
    call sdmMessage(8,3,'CalSeeingTable','row%baselineLengths not allocated.')
    error = .true.
    return
  endif
  do i=1, baselineLengthsRank
    baselineLengthsDim(:,i) = size(row%baselineLengths,i)
  enddo
  if (.not.allocated(row%phaseRMS)) then
    call sdmMessage(8,3,'CalSeeingTable','row%phaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRMSRank
    phaseRMSDim(:,i) = size(row%phaseRMS,i)
  enddo
  !
  ireturn = sdm_addCalSeeingRow(key%atmPhaseCorrection, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%frequencyRange, frequencyRangeDim, row%integrationTime, row%numBaseLengths,&
      & row%baselineLengths, baselineLengthsDim, row%phaseRMS, phaseRMSDim, row%seeing, row%seeingError)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_addCalSeeingRow', ireturn)
    error = .true.
  endif
end subroutine addCalSeeingRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingRow(key, row, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingRow) :: row
  type(CalSeeingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalSeeingRow, i
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: baselineLengthsRank = 1
  integer :: baselineLengthsDim(2,baselineLengthsRank)
  integer, parameter :: phaseRMSRank = 1
  integer :: phaseRMSDim(2,phaseRMSRank)
  ! Deal with dimensions
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalSeeingTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%baselineLengths)) then
    call sdmMessage(8,3,'CalSeeingTable','row%baselineLengths not allocated.')
    error = .true.
    return
  endif
  do i=1, baselineLengthsRank
    baselineLengthsDim(:,i) = size(row%baselineLengths,i)
  enddo
  if (.not.allocated(row%phaseRMS)) then
    call sdmMessage(8,3,'CalSeeingTable','row%phaseRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseRMSRank
    phaseRMSDim(:,i) = size(row%phaseRMS,i)
  enddo
  !
  ireturn = sdm_getCalSeeingRow(key%atmPhaseCorrection, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%frequencyRange, frequencyRangeDim, row%integrationTime, row%numBaseLengths,&
      & row%baselineLengths, baselineLengthsDim, row%phaseRMS, phaseRMSDim, row%seeing, row%seeingError)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_getCalSeeingRow', ireturn)
    error = .true.
  endif
end subroutine getCalSeeingRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalSeeingTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalSeeingTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalSeeingTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalSeeing
  !
  integer :: tableSize
  logical :: error
  type(CalSeeingKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalSeeingKeys, i
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalSeeingKeys(atmPhaseCorrectionList, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_getCalSeeingKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalSeeingKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalSeeingRow(row, error)
  use sdm_CalSeeing
  type(CalSeeingRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalSeeing'
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%baselineLengths allocation
  if (allocated(row%baselineLengths)) then
    deallocate(row%baselineLengths, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%phaseRMS allocation
  if (allocated(row%phaseRMS)) then
    deallocate(row%phaseRMS, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%frequencyRange(2), row%baselineLengths(row%numBaseLengths), row%phaseRMS(row%numBaseLengths), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalSeeingRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalSeeingExponent(key, opt, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalSeeingExponent, i
  !! real :: exponent
  ! Deal with dimensions
  ireturn = sdm_addCalSeeingExponent(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%exponent)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_addCalSeeingExponent', ireturn)
    error = .true.
  endif
end subroutine addCalSeeingExponent
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingExponent(key, opt, present, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalSeeingExponent, i
  ! Deal with dimensions
  ireturn = sdm_getCalSeeingExponent(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%exponent)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_getCalSeeingExponent', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalSeeingExponent
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalSeeingOuterScale(key, opt, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalSeeingOuterScale, i
  !! real*8 :: outerScale
  ! Deal with dimensions
  ireturn = sdm_addCalSeeingOuterScale(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%outerScale)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_addCalSeeingOuterScale', ireturn)
    error = .true.
  endif
end subroutine addCalSeeingOuterScale
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingOuterScale(key, opt, present, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalSeeingOuterScale, i
  ! Deal with dimensions
  ireturn = sdm_getCalSeeingOuterScale(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%outerScale)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_getCalSeeingOuterScale', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalSeeingOuterScale
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalSeeingOuterScaleRMS(key, opt, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalSeeingOuterScaleRMS, i
  !! real*8 :: outerScaleRMS
  ! Deal with dimensions
  ireturn = sdm_addCalSeeingOuterScaleRMS(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%outerScaleRMS)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_addCalSeeingOuterScaleRMS', ireturn)
    error = .true.
  endif
end subroutine addCalSeeingOuterScaleRMS
!
! ---------------------------------------------------------------------------
!
subroutine getCalSeeingOuterScaleRMS(key, opt, present, error)
  !
  use sdm_CalSeeing
  !
  type(CalSeeingKey) :: key
  type(CalSeeingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalSeeingOuterScaleRMS, i
  ! Deal with dimensions
  ireturn = sdm_getCalSeeingOuterScaleRMS(key%atmPhaseCorrection, key%calDataId, key%calReductionId, opt%outerScaleRMS)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalSeeingTable','Error in sdm_getCalSeeingOuterScaleRMS', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalSeeingOuterScaleRMS
!
!
! ===========================================================================
!
! CalPosition Table:
!
! ===========================================================================
!
subroutine addCalPositionRow(key, row, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionRow) :: row
  type(CalPositionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalPositionRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: antennaPositionRank = 1
  integer :: antennaPositionDim(2,antennaPositionRank)
  integer :: stationNameDim(2)
  integer, parameter :: stationPositionRank = 1
  integer :: stationPositionDim(2,stationPositionRank)
  integer, parameter :: refAntennaNamesRank = 1
  integer :: refAntennaNamesDim(2,refAntennaNamesRank+1)
  integer, parameter :: positionOffsetRank = 1
  integer :: positionOffsetDim(2,positionOffsetRank)
  integer, parameter :: positionErrRank = 1
  integer :: positionErrDim(2,positionErrRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%antennaPosition)) then
    call sdmMessage(8,3,'CalPositionTable','row%antennaPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaPositionRank
    antennaPositionDim(:,i) = size(row%antennaPosition,i)
  enddo
  stationNameDim = len(row%stationName)
  call charcut(row%stationName)
  if (.not.allocated(row%stationPosition)) then
    call sdmMessage(8,3,'CalPositionTable','row%stationPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, stationPositionRank
    stationPositionDim(:,i) = size(row%stationPosition,i)
  enddo
  if (.not.allocated(row%refAntennaNames)) then
    call sdmMessage(8,3,'CalPositionTable','row%refAntennaNames not allocated.')
    error = .true.
    return
  endif
  refAntennaNamesDim(:,1) = len(row%refAntennaNames)
  do i=1, refAntennaNamesRank
    refAntennaNamesDim(:,i+1) = size(row%refAntennaNames,i)
  enddo
  do i=1, refAntennaNamesDim(1,2)
    call charcut(row%refAntennaNames(i))
  enddo
  if (.not.allocated(row%positionOffset)) then
    call sdmMessage(8,3,'CalPositionTable','row%positionOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, positionOffsetRank
    positionOffsetDim(:,i) = size(row%positionOffset,i)
  enddo
  if (.not.allocated(row%positionErr)) then
    call sdmMessage(8,3,'CalPositionTable','row%positionErr not allocated.')
    error = .true.
    return
  endif
  do i=1, positionErrRank
    positionErrDim(:,i) = size(row%positionErr,i)
  enddo
  !
  ireturn = sdm_addCalPositionRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaPosition, antennaPositionDim,&
      & row%stationName, stationNameDim, row%stationPosition, stationPositionDim, row%positionMethod, row%receiverBand,&
      & row%numAntenna, row%refAntennaNames, refAntennaNamesDim, row%axesOffset, row%axesOffsetErr,&
      & row%axesOffsetFixed, row%positionOffset, positionOffsetDim, row%positionErr, positionErrDim,&
      & row%reducedChiSquared)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_addCalPositionRow', ireturn)
    error = .true.
  endif
end subroutine addCalPositionRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPositionRow(key, row, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionRow) :: row
  type(CalPositionKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalPositionRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: antennaPositionRank = 1
  integer :: antennaPositionDim(2,antennaPositionRank)
  integer :: stationNameDim(2)
  integer, parameter :: stationPositionRank = 1
  integer :: stationPositionDim(2,stationPositionRank)
  integer, parameter :: refAntennaNamesRank = 1
  integer :: refAntennaNamesDim(2,refAntennaNamesRank+1)
  integer, parameter :: positionOffsetRank = 1
  integer :: positionOffsetDim(2,positionOffsetRank)
  integer, parameter :: positionErrRank = 1
  integer :: positionErrDim(2,positionErrRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%antennaPosition)) then
    call sdmMessage(8,3,'CalPositionTable','row%antennaPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaPositionRank
    antennaPositionDim(:,i) = size(row%antennaPosition,i)
  enddo
  stationNameDim = len(row%stationName)
  call charcut(row%stationName)
  if (.not.allocated(row%stationPosition)) then
    call sdmMessage(8,3,'CalPositionTable','row%stationPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, stationPositionRank
    stationPositionDim(:,i) = size(row%stationPosition,i)
  enddo
  if (.not.allocated(row%refAntennaNames)) then
    call sdmMessage(8,3,'CalPositionTable','row%refAntennaNames not allocated.')
    error = .true.
    return
  endif
  refAntennaNamesDim(:,1) = len(row%refAntennaNames)
  do i=1, refAntennaNamesRank
    refAntennaNamesDim(:,i+1) = size(row%refAntennaNames,i)
  enddo
  if (.not.allocated(row%positionOffset)) then
    call sdmMessage(8,3,'CalPositionTable','row%positionOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, positionOffsetRank
    positionOffsetDim(:,i) = size(row%positionOffset,i)
  enddo
  if (.not.allocated(row%positionErr)) then
    call sdmMessage(8,3,'CalPositionTable','row%positionErr not allocated.')
    error = .true.
    return
  endif
  do i=1, positionErrRank
    positionErrDim(:,i) = size(row%positionErr,i)
  enddo
  !
  ireturn = sdm_getCalPositionRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaPosition, antennaPositionDim,&
      & row%stationName, stationNameDim, row%stationPosition, stationPositionDim, row%positionMethod, row%receiverBand,&
      & row%numAntenna, row%refAntennaNames, refAntennaNamesDim, row%axesOffset, row%axesOffsetErr,&
      & row%axesOffsetFixed, row%positionOffset, positionOffsetDim, row%positionErr, positionErrDim,&
      & row%reducedChiSquared)
  key%antennaName(antennaNameDim(1)+1:) = ''
  row%stationName(stationNameDim(1)+1:) = ''
  do i=1,refAntennaNamesDim(1,2)
    row%refAntennaNames(i)(refAntennaNamesDim(1,1)+1:) = ''
  enddo
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_getCalPositionRow', ireturn)
    error = .true.
  endif
end subroutine getCalPositionRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPositionTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalPositionTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalPositionTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalPositionTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalPositionKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalPosition
  !
  integer :: tableSize
  logical :: error
  type(CalPositionKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalPositionKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalPositionKeys(antennaNameList, antennaNameListDim, atmPhaseCorrectionList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_getCalPositionKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalPositionKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalPositionRow(row, error)
  use sdm_CalPosition
  type(CalPositionRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalPosition'
  ! row%antennaPosition allocation
  if (allocated(row%antennaPosition)) then
    deallocate(row%antennaPosition, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%stationPosition allocation
  if (allocated(row%stationPosition)) then
    deallocate(row%stationPosition, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%refAntennaNames allocation
  if (allocated(row%refAntennaNames)) then
    deallocate(row%refAntennaNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%positionOffset allocation
  if (allocated(row%positionOffset)) then
    deallocate(row%positionOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%positionErr allocation
  if (allocated(row%positionErr)) then
    deallocate(row%positionErr, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%antennaPosition(3), row%stationPosition(3), row%refAntennaNames(row%numAntenna), row%positionOffset(3),&
      & row%positionErr(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPositionRow
!
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalPositionDelayRms(key, opt, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionKey) :: key
  type(CalPositionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPositionDelayRms, i
  integer :: antennaNameDim(2)
  !! real*8 :: delayRms
  ! Deal with dimensions
  ireturn = sdm_addCalPositionDelayRms(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, opt%delayRms)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_addCalPositionDelayRms', ireturn)
    error = .true.
  endif
end subroutine addCalPositionDelayRms
!
! ---------------------------------------------------------------------------
!
subroutine getCalPositionDelayRms(key, opt, present, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionKey) :: key
  type(CalPositionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPositionDelayRms, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPositionDelayRms(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, opt%delayRms)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_getCalPositionDelayRms', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPositionDelayRms
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPositionPhaseRms(key, opt, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionKey) :: key
  type(CalPositionOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPositionPhaseRms, i
  integer :: antennaNameDim(2)
  !! real*8 :: phaseRms
  ! Deal with dimensions
  ireturn = sdm_addCalPositionPhaseRms(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, opt%phaseRms)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_addCalPositionPhaseRms', ireturn)
    error = .true.
  endif
end subroutine addCalPositionPhaseRms
!
! ---------------------------------------------------------------------------
!
subroutine getCalPositionPhaseRms(key, opt, present, error)
  !
  use sdm_CalPosition
  !
  type(CalPositionKey) :: key
  type(CalPositionOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPositionPhaseRms, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPositionPhaseRms(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%calDataId,&
      & key%calReductionId, opt%phaseRms)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPositionTable','Error in sdm_getCalPositionPhaseRms', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPositionPhaseRms
!
!
! ===========================================================================
!
! CalPointing Table:
!
! ===========================================================================
!
subroutine addCalPointingRow(key, row, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingRow) :: row
  type(CalPointingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalPointingRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: collOffsetRelativeRank = 2
  integer :: collOffsetRelativeDim(2,collOffsetRelativeRank)
  integer, parameter :: collOffsetAbsoluteRank = 2
  integer :: collOffsetAbsoluteDim(2,collOffsetAbsoluteRank)
  integer, parameter :: collErrorRank = 2
  integer :: collErrorDim(2,collErrorRank)
  integer, parameter :: collOffsetTiedRank = 2
  integer :: collOffsetTiedDim(2,collOffsetTiedRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalPointingTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPointingTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPointingTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%collOffsetRelative)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetRelative not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetRelativeRank
    collOffsetRelativeDim(:,i) = size(row%collOffsetRelative,i)
  enddo
  if (.not.allocated(row%collOffsetAbsolute)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetAbsolute not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetAbsoluteRank
    collOffsetAbsoluteDim(:,i) = size(row%collOffsetAbsolute,i)
  enddo
  if (.not.allocated(row%collError)) then
    call sdmMessage(8,3,'CalPointingTable','row%collError not allocated.')
    error = .true.
    return
  endif
  do i=1, collErrorRank
    collErrorDim(:,i) = size(row%collError,i)
  enddo
  if (.not.allocated(row%collOffsetTied)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetTied not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetTiedRank
    collOffsetTiedDim(:,i) = size(row%collOffsetTied,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalPointingTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_addCalPointingRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId, key%calReductionId,&
      & row%startValidTime, row%endValidTime, row%ambientTemperature, row%antennaMake, row%atmPhaseCorrection,&
      & row%direction, directionDim, row%frequencyRange, frequencyRangeDim, row%pointingModelMode, row%pointingMethod,&
      & row%numReceptor, row%polarizationTypes, polarizationTypesDim, row%collOffsetRelative, collOffsetRelativeDim,&
      & row%collOffsetAbsolute, collOffsetAbsoluteDim, row%collError, collErrorDim, row%collOffsetTied,&
      & collOffsetTiedDim, row%reducedChiSquared, reducedChiSquaredDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingRow', ireturn)
    error = .true.
  endif
end subroutine addCalPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingRow(key, row, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingRow) :: row
  type(CalPointingKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalPointingRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: collOffsetRelativeRank = 2
  integer :: collOffsetRelativeDim(2,collOffsetRelativeRank)
  integer, parameter :: collOffsetAbsoluteRank = 2
  integer :: collOffsetAbsoluteDim(2,collOffsetAbsoluteRank)
  integer, parameter :: collErrorRank = 2
  integer :: collErrorDim(2,collErrorRank)
  integer, parameter :: collOffsetTiedRank = 2
  integer :: collOffsetTiedDim(2,collOffsetTiedRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalPointingTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPointingTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPointingTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%collOffsetRelative)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetRelative not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetRelativeRank
    collOffsetRelativeDim(:,i) = size(row%collOffsetRelative,i)
  enddo
  if (.not.allocated(row%collOffsetAbsolute)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetAbsolute not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetAbsoluteRank
    collOffsetAbsoluteDim(:,i) = size(row%collOffsetAbsolute,i)
  enddo
  if (.not.allocated(row%collError)) then
    call sdmMessage(8,3,'CalPointingTable','row%collError not allocated.')
    error = .true.
    return
  endif
  do i=1, collErrorRank
    collErrorDim(:,i) = size(row%collError,i)
  enddo
  if (.not.allocated(row%collOffsetTied)) then
    call sdmMessage(8,3,'CalPointingTable','row%collOffsetTied not allocated.')
    error = .true.
    return
  endif
  do i=1, collOffsetTiedRank
    collOffsetTiedDim(:,i) = size(row%collOffsetTied,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalPointingTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_getCalPointingRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId, key%calReductionId,&
      & row%startValidTime, row%endValidTime, row%ambientTemperature, row%antennaMake, row%atmPhaseCorrection,&
      & row%direction, directionDim, row%frequencyRange, frequencyRangeDim, row%pointingModelMode, row%pointingMethod,&
      & row%numReceptor, row%polarizationTypes, polarizationTypesDim, row%collOffsetRelative, collOffsetRelativeDim,&
      & row%collOffsetAbsolute, collOffsetAbsoluteDim, row%collError, collErrorDim, row%collOffsetTied,&
      & collOffsetTiedDim, row%reducedChiSquared, reducedChiSquaredDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingRow', ireturn)
    error = .true.
  endif
end subroutine getCalPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalPointingTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalPointingTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalPointingTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalPointing
  !
  integer :: tableSize
  logical :: error
  type(CalPointingKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalPointingKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalPointingKeys(antennaNameList, antennaNameListDim, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalPointingKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalPointingRow(row, error)
  use sdm_CalPointing
  type(CalPointingRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalPointing'
  ! row%direction allocation
  if (allocated(row%direction)) then
    deallocate(row%direction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%collOffsetRelative allocation
  if (allocated(row%collOffsetRelative)) then
    deallocate(row%collOffsetRelative, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%collOffsetAbsolute allocation
  if (allocated(row%collOffsetAbsolute)) then
    deallocate(row%collOffsetAbsolute, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%collError allocation
  if (allocated(row%collError)) then
    deallocate(row%collError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%collOffsetTied allocation
  if (allocated(row%collOffsetTied)) then
    deallocate(row%collOffsetTied, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%reducedChiSquared allocation
  if (allocated(row%reducedChiSquared)) then
    deallocate(row%reducedChiSquared, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%direction(2), row%frequencyRange(2), row%polarizationTypes(row%numReceptor), row%collOffsetRelative(2,&
      & row%numReceptor), row%collOffsetAbsolute(2, row%numReceptor), row%collError(2, row%numReceptor),&
      & row%collOffsetTied(2, row%numReceptor), row%reducedChiSquared(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPointingRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalPointingOpt(row, opt, error)
  use sdm_CalPointing
  type(CalPointingRow) :: row
  type(CalPointingOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalPointing'
  ! opt%beamPA allocation
  if (allocated(opt%beamPA)) then
    deallocate(opt%beamPA, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%beamPAError allocation
  if (allocated(opt%beamPAError)) then
    deallocate(opt%beamPAError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%beamWidth allocation
  if (allocated(opt%beamWidth)) then
    deallocate(opt%beamWidth, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%beamWidthError allocation
  if (allocated(opt%beamWidthError)) then
    deallocate(opt%beamWidthError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%beamWidthWasFixed allocation
  if (allocated(opt%beamWidthWasFixed)) then
    deallocate(opt%beamWidthWasFixed, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%offIntensity allocation
  if (allocated(opt%offIntensity)) then
    deallocate(opt%offIntensity, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%offIntensityError allocation
  if (allocated(opt%offIntensityError)) then
    deallocate(opt%offIntensityError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%peakIntensity allocation
  if (allocated(opt%peakIntensity)) then
    deallocate(opt%peakIntensity, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%peakIntensityError allocation
  if (allocated(opt%peakIntensityError)) then
    deallocate(opt%peakIntensityError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%beamPA(row%numReceptor), opt%beamPAError(row%numReceptor), opt%beamWidth(2, row%numReceptor),&
      & opt%beamWidthError(2, row%numReceptor), opt%beamWidthWasFixed(2), opt%offIntensity(row%numReceptor),&
      & opt%offIntensityError(row%numReceptor), opt%peakIntensity(row%numReceptor),&
      & opt%peakIntensityError(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPointingOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingAveragedPolarizations(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingAveragedPolarizations, i
  integer :: antennaNameDim(2)
  !! logical*1 :: averagedPolarizations
  ! Deal with dimensions
  ireturn = sdm_addCalPointingAveragedPolarizations(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%averagedPolarizations)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingAveragedPolarizations', ireturn)
    error = .true.
  endif
end subroutine addCalPointingAveragedPolarizations
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingAveragedPolarizations(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingAveragedPolarizations, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPointingAveragedPolarizations(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%averagedPolarizations)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingAveragedPolarizations', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingAveragedPolarizations
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamPA(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamPA, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: beamPA(:)
  integer, parameter :: beamPARank = 1
  integer :: beamPADim(2,beamPARank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamPA)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamPA not allocated.')
    error = .true.
    return
  endif
  do i=1, beamPARank
    beamPADim(:,i) = size(opt%beamPA,i)
  enddo
  ireturn = sdm_addCalPointingBeamPA(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPA, beamPADim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamPA', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamPA
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamPA(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamPA, i
  integer :: antennaNameDim(2)
  integer, parameter :: beamPARank = 1
  integer :: beamPADim(2,beamPARank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamPA)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamPA not allocated.')
    error = .true.
    return
  endif
  do i=1, beamPARank
    beamPADim(:,i) = size(opt%beamPA,i)
  enddo
  ireturn = sdm_getCalPointingBeamPA(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPA, beamPADim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamPA', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamPA
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamPAError(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamPAError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: beamPAError(:)
  integer, parameter :: beamPAErrorRank = 1
  integer :: beamPAErrorDim(2,beamPAErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamPAError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamPAError not allocated.')
    error = .true.
    return
  endif
  do i=1, beamPAErrorRank
    beamPAErrorDim(:,i) = size(opt%beamPAError,i)
  enddo
  ireturn = sdm_addCalPointingBeamPAError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPAError, beamPAErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamPAError', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamPAError
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamPAError(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamPAError, i
  integer :: antennaNameDim(2)
  integer, parameter :: beamPAErrorRank = 1
  integer :: beamPAErrorDim(2,beamPAErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamPAError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamPAError not allocated.')
    error = .true.
    return
  endif
  do i=1, beamPAErrorRank
    beamPAErrorDim(:,i) = size(opt%beamPAError,i)
  enddo
  ireturn = sdm_getCalPointingBeamPAError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPAError, beamPAErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamPAError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamPAError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamPAWasFixed(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamPAWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1 :: beamPAWasFixed
  ! Deal with dimensions
  ireturn = sdm_addCalPointingBeamPAWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPAWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamPAWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamPAWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamPAWasFixed(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamPAWasFixed, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPointingBeamPAWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamPAWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamPAWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamPAWasFixed
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamWidth(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamWidth, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: beamWidth(:,:)
  integer, parameter :: beamWidthRank = 2
  integer :: beamWidthDim(2,beamWidthRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidth)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthRank
    beamWidthDim(:,i) = size(opt%beamWidth,i)
  enddo
  ireturn = sdm_addCalPointingBeamWidth(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidth, beamWidthDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamWidth', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamWidth
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamWidth(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamWidth, i
  integer :: antennaNameDim(2)
  integer, parameter :: beamWidthRank = 2
  integer :: beamWidthDim(2,beamWidthRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidth)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthRank
    beamWidthDim(:,i) = size(opt%beamWidth,i)
  enddo
  ireturn = sdm_getCalPointingBeamWidth(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidth, beamWidthDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamWidth', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamWidth
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamWidthError(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamWidthError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: beamWidthError(:,:)
  integer, parameter :: beamWidthErrorRank = 2
  integer :: beamWidthErrorDim(2,beamWidthErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidthError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidthError not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthErrorRank
    beamWidthErrorDim(:,i) = size(opt%beamWidthError,i)
  enddo
  ireturn = sdm_addCalPointingBeamWidthError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidthError, beamWidthErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamWidthError', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamWidthError
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamWidthError(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamWidthError, i
  integer :: antennaNameDim(2)
  integer, parameter :: beamWidthErrorRank = 2
  integer :: beamWidthErrorDim(2,beamWidthErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidthError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidthError not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthErrorRank
    beamWidthErrorDim(:,i) = size(opt%beamWidthError,i)
  enddo
  ireturn = sdm_getCalPointingBeamWidthError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidthError, beamWidthErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamWidthError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamWidthError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingBeamWidthWasFixed(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingBeamWidthWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1, allocatable :: beamWidthWasFixed(:)
  integer, parameter :: beamWidthWasFixedRank = 1
  integer :: beamWidthWasFixedDim(2,beamWidthWasFixedRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidthWasFixed)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidthWasFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthWasFixedRank
    beamWidthWasFixedDim(:,i) = size(opt%beamWidthWasFixed,i)
  enddo
  ireturn = sdm_addCalPointingBeamWidthWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidthWasFixed, beamWidthWasFixedDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingBeamWidthWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalPointingBeamWidthWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingBeamWidthWasFixed(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingBeamWidthWasFixed, i
  integer :: antennaNameDim(2)
  integer, parameter :: beamWidthWasFixedRank = 1
  integer :: beamWidthWasFixedDim(2,beamWidthWasFixedRank)
  ! Deal with dimensions
  if (.not.allocated(opt%beamWidthWasFixed)) then
    call sdmMessage(8,3,'CalPointingTable','opt%beamWidthWasFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, beamWidthWasFixedRank
    beamWidthWasFixedDim(:,i) = size(opt%beamWidthWasFixed,i)
  enddo
  ireturn = sdm_getCalPointingBeamWidthWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%beamWidthWasFixed, beamWidthWasFixedDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingBeamWidthWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingBeamWidthWasFixed
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingOffIntensity(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingOffIntensity, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: offIntensity(:)
  integer, parameter :: offIntensityRank = 1
  integer :: offIntensityDim(2,offIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensity)) then
    call sdmMessage(8,3,'CalPointingTable','opt%offIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityRank
    offIntensityDim(:,i) = size(opt%offIntensity,i)
  enddo
  ireturn = sdm_addCalPointingOffIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensity, offIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingOffIntensity', ireturn)
    error = .true.
  endif
end subroutine addCalPointingOffIntensity
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingOffIntensity(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingOffIntensity, i
  integer :: antennaNameDim(2)
  integer, parameter :: offIntensityRank = 1
  integer :: offIntensityDim(2,offIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensity)) then
    call sdmMessage(8,3,'CalPointingTable','opt%offIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityRank
    offIntensityDim(:,i) = size(opt%offIntensity,i)
  enddo
  ireturn = sdm_getCalPointingOffIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensity, offIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingOffIntensity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingOffIntensity
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingOffIntensityError(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingOffIntensityError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: offIntensityError(:)
  integer, parameter :: offIntensityErrorRank = 1
  integer :: offIntensityErrorDim(2,offIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensityError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%offIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityErrorRank
    offIntensityErrorDim(:,i) = size(opt%offIntensityError,i)
  enddo
  ireturn = sdm_addCalPointingOffIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityError, offIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingOffIntensityError', ireturn)
    error = .true.
  endif
end subroutine addCalPointingOffIntensityError
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingOffIntensityError(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingOffIntensityError, i
  integer :: antennaNameDim(2)
  integer, parameter :: offIntensityErrorRank = 1
  integer :: offIntensityErrorDim(2,offIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensityError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%offIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityErrorRank
    offIntensityErrorDim(:,i) = size(opt%offIntensityError,i)
  enddo
  ireturn = sdm_getCalPointingOffIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityError, offIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingOffIntensityError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingOffIntensityError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingOffIntensityWasFixed(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingOffIntensityWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1 :: offIntensityWasFixed
  ! Deal with dimensions
  ireturn = sdm_addCalPointingOffIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingOffIntensityWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalPointingOffIntensityWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingOffIntensityWasFixed(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingOffIntensityWasFixed, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPointingOffIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingOffIntensityWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingOffIntensityWasFixed
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingPeakIntensity(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingPeakIntensity, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: peakIntensity(:)
  integer, parameter :: peakIntensityRank = 1
  integer :: peakIntensityDim(2,peakIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensity)) then
    call sdmMessage(8,3,'CalPointingTable','opt%peakIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityRank
    peakIntensityDim(:,i) = size(opt%peakIntensity,i)
  enddo
  ireturn = sdm_addCalPointingPeakIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensity, peakIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingPeakIntensity', ireturn)
    error = .true.
  endif
end subroutine addCalPointingPeakIntensity
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingPeakIntensity(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingPeakIntensity, i
  integer :: antennaNameDim(2)
  integer, parameter :: peakIntensityRank = 1
  integer :: peakIntensityDim(2,peakIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensity)) then
    call sdmMessage(8,3,'CalPointingTable','opt%peakIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityRank
    peakIntensityDim(:,i) = size(opt%peakIntensity,i)
  enddo
  ireturn = sdm_getCalPointingPeakIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensity, peakIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingPeakIntensity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingPeakIntensity
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingPeakIntensityError(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingPeakIntensityError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: peakIntensityError(:)
  integer, parameter :: peakIntensityErrorRank = 1
  integer :: peakIntensityErrorDim(2,peakIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensityError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%peakIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityErrorRank
    peakIntensityErrorDim(:,i) = size(opt%peakIntensityError,i)
  enddo
  ireturn = sdm_addCalPointingPeakIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityError, peakIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingPeakIntensityError', ireturn)
    error = .true.
  endif
end subroutine addCalPointingPeakIntensityError
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingPeakIntensityError(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingPeakIntensityError, i
  integer :: antennaNameDim(2)
  integer, parameter :: peakIntensityErrorRank = 1
  integer :: peakIntensityErrorDim(2,peakIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensityError)) then
    call sdmMessage(8,3,'CalPointingTable','opt%peakIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityErrorRank
    peakIntensityErrorDim(:,i) = size(opt%peakIntensityError,i)
  enddo
  ireturn = sdm_getCalPointingPeakIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityError, peakIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingPeakIntensityError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingPeakIntensityError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingPeakIntensityWasFixed(key, opt, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingPeakIntensityWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1 :: peakIntensityWasFixed
  ! Deal with dimensions
  ireturn = sdm_addCalPointingPeakIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_addCalPointingPeakIntensityWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalPointingPeakIntensityWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingPeakIntensityWasFixed(key, opt, present, error)
  !
  use sdm_CalPointing
  !
  type(CalPointingKey) :: key
  type(CalPointingOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingPeakIntensityWasFixed, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPointingPeakIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingTable','Error in sdm_getCalPointingPeakIntensityWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingPeakIntensityWasFixed
!
!
! ===========================================================================
!
! CalPointingModel Table:
!
! ===========================================================================
!
subroutine addCalPointingModelRow(key, row, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelRow) :: row
  type(CalPointingModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalPointingModelRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer, parameter :: coeffErrorRank = 1
  integer :: coeffErrorDim(2,coeffErrorRank)
  integer, parameter :: coeffFixedRank = 1
  integer :: coeffFixedDim(2,coeffFixedRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  do i=1, coeffNameDim(1,2)
    call charcut(row%coeffName(i))
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  if (.not.allocated(row%coeffError)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffError not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffErrorRank
    coeffErrorDim(:,i) = size(row%coeffError,i)
  enddo
  if (.not.allocated(row%coeffFixed)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffFixedRank
    coeffFixedDim(:,i) = size(row%coeffFixed,i)
  enddo
  !
  ireturn = sdm_addCalPointingModelRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%pointingModelMode,&
      & row%polarizationType, row%numCoeff, row%coeffName, coeffNameDim, row%coeffVal, coeffValDim, row%coeffError,&
      & coeffErrorDim, row%coeffFixed, coeffFixedDim, row%azimuthRMS, row%elevationRms, row%skyRMS,&
      & row%reducedChiSquared)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_addCalPointingModelRow', ireturn)
    error = .true.
  endif
end subroutine addCalPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingModelRow(key, row, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelRow) :: row
  type(CalPointingModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalPointingModelRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffValRank = 1
  integer :: coeffValDim(2,coeffValRank)
  integer, parameter :: coeffErrorRank = 1
  integer :: coeffErrorDim(2,coeffErrorRank)
  integer, parameter :: coeffFixedRank = 1
  integer :: coeffFixedDim(2,coeffFixedRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  if (.not.allocated(row%coeffVal)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffVal not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValRank
    coeffValDim(:,i) = size(row%coeffVal,i)
  enddo
  if (.not.allocated(row%coeffError)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffError not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffErrorRank
    coeffErrorDim(:,i) = size(row%coeffError,i)
  enddo
  if (.not.allocated(row%coeffFixed)) then
    call sdmMessage(8,3,'CalPointingModelTable','row%coeffFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffFixedRank
    coeffFixedDim(:,i) = size(row%coeffFixed,i)
  enddo
  !
  ireturn = sdm_getCalPointingModelRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%pointingModelMode,&
      & row%polarizationType, row%numCoeff, row%coeffName, coeffNameDim, row%coeffVal, coeffValDim, row%coeffError,&
      & coeffErrorDim, row%coeffFixed, coeffFixedDim, row%azimuthRMS, row%elevationRms, row%skyRMS,&
      & row%reducedChiSquared)
  key%antennaName(antennaNameDim(1)+1:) = ''
  do i=1,coeffNameDim(1,2)
    row%coeffName(i)(coeffNameDim(1,1)+1:) = ''
  enddo
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_getCalPointingModelRow', ireturn)
    error = .true.
  endif
end subroutine getCalPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingModelTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalPointingModelTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalPointingModelTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalPointingModelTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingModelKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalPointingModel
  !
  integer :: tableSize
  logical :: error
  type(CalPointingModelKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalPointingModelKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalPointingModelKeys(antennaNameList, antennaNameListDim, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_getCalPointingModelKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalPointingModelKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalPointingModelRow(row, error)
  use sdm_CalPointingModel
  type(CalPointingModelRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalPointingModel'
  ! row%coeffName allocation
  if (allocated(row%coeffName)) then
    deallocate(row%coeffName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffVal allocation
  if (allocated(row%coeffVal)) then
    deallocate(row%coeffVal, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffError allocation
  if (allocated(row%coeffError)) then
    deallocate(row%coeffError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffFixed allocation
  if (allocated(row%coeffFixed)) then
    deallocate(row%coeffFixed, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%coeffName(row%numCoeff), row%coeffVal(row%numCoeff), row%coeffError(row%numCoeff),&
      & row%coeffFixed(row%numCoeff), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPointingModelRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalPointingModelOpt(row, opt, error)
  use sdm_CalPointingModel
  type(CalPointingModelRow) :: row
  type(CalPointingModelOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalPointingModel'
  ! opt%coeffFormula allocation
  if (allocated(opt%coeffFormula)) then
    deallocate(opt%coeffFormula, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%coeffFormula(row%numCoeff), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPointingModelOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingModelNumObs(key, opt, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelKey) :: key
  type(CalPointingModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingModelNumObs, i
  integer :: antennaNameDim(2)
  !! integer :: numObs
  ! Deal with dimensions
  ireturn = sdm_addCalPointingModelNumObs(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%numObs)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_addCalPointingModelNumObs', ireturn)
    error = .true.
  endif
end subroutine addCalPointingModelNumObs
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingModelNumObs(key, opt, present, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelKey) :: key
  type(CalPointingModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingModelNumObs, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalPointingModelNumObs(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%numObs)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_getCalPointingModelNumObs', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingModelNumObs
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalPointingModelCoeffFormula(key, opt, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelKey) :: key
  type(CalPointingModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalPointingModelCoeffFormula, i
  integer :: antennaNameDim(2)
  !! character*256, allocatable :: coeffFormula(:)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%coeffFormula)) then
    call sdmMessage(8,3,'CalPointingModelTable','opt%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(opt%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(opt%coeffFormula,i)
    call charcut(opt%coeffFormula(i))
  enddo
  ireturn = sdm_addCalPointingModelCoeffFormula(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%coeffFormula, coeffFormulaDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_addCalPointingModelCoeffFormula', ireturn)
    error = .true.
  endif
end subroutine addCalPointingModelCoeffFormula
!
! ---------------------------------------------------------------------------
!
subroutine getCalPointingModelCoeffFormula(key, opt, present, error)
  !
  use sdm_CalPointingModel
  !
  type(CalPointingModelKey) :: key
  type(CalPointingModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalPointingModelCoeffFormula, i
  integer :: antennaNameDim(2)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%coeffFormula)) then
    call sdmMessage(8,3,'CalPointingModelTable','opt%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(opt%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(opt%coeffFormula,i)
  enddo
  ireturn = sdm_getCalPointingModelCoeffFormula(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%coeffFormula, coeffFormulaDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPointingModelTable','Error in sdm_getCalPointingModelCoeffFormula', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalPointingModelCoeffFormula
!
!
! ===========================================================================
!
! CalHolography Table:
!
! ===========================================================================
!
subroutine addCalHolographyRow(key, row, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyRow) :: row
  type(CalHolographyKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalHolographyRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: focusPositionRank = 1
  integer :: focusPositionDim(2,focusPositionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer :: beamMapUIDDim(2)
  integer :: surfaceMapUIDDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%focusPosition)) then
    call sdmMessage(8,3,'CalHolographyTable','row%focusPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, focusPositionRank
    focusPositionDim(:,i) = size(row%focusPosition,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalHolographyTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalHolographyTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  beamMapUIDDim = len(row%beamMapUID)
  call charcut(row%beamMapUID)
  surfaceMapUIDDim = len(row%surfaceMapUID)
  call charcut(row%surfaceMapUID)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalHolographyTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  !
  ireturn = sdm_addCalHolographyRow(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & row%antennaMake, row%startValidTime, row%endValidTime, row%ambientTemperature, row%focusPosition,&
      & focusPositionDim, row%frequencyRange, frequencyRangeDim, row%illuminationTaper, row%numReceptor,&
      & row%polarizationTypes, polarizationTypesDim, row%numPanelModes, row%receiverBand, row%beamMapUID,&
      & beamMapUIDDim, row%rawRMS, row%weightedRMS, row%surfaceMapUID, surfaceMapUIDDim, row%direction, directionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyRow', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyRow(key, row, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyRow) :: row
  type(CalHolographyKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalHolographyRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: focusPositionRank = 1
  integer :: focusPositionDim(2,focusPositionRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer :: beamMapUIDDim(2)
  integer :: surfaceMapUIDDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%focusPosition)) then
    call sdmMessage(8,3,'CalHolographyTable','row%focusPosition not allocated.')
    error = .true.
    return
  endif
  do i=1, focusPositionRank
    focusPositionDim(:,i) = size(row%focusPosition,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalHolographyTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalHolographyTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  beamMapUIDDim = len(row%beamMapUID)
  call charcut(row%beamMapUID)
  surfaceMapUIDDim = len(row%surfaceMapUID)
  call charcut(row%surfaceMapUID)
  if (.not.allocated(row%direction)) then
    call sdmMessage(8,3,'CalHolographyTable','row%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(row%direction,i)
  enddo
  !
  ireturn = sdm_getCalHolographyRow(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & row%antennaMake, row%startValidTime, row%endValidTime, row%ambientTemperature, row%focusPosition,&
      & focusPositionDim, row%frequencyRange, frequencyRangeDim, row%illuminationTaper, row%numReceptor,&
      & row%polarizationTypes, polarizationTypesDim, row%numPanelModes, row%receiverBand, row%beamMapUID,&
      & beamMapUIDDim, row%rawRMS, row%weightedRMS, row%surfaceMapUID, surfaceMapUIDDim, row%direction, directionDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  row%beamMapUID(beamMapUIDDim(1)+1:) = ''
  row%surfaceMapUID(surfaceMapUIDDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyRow', ireturn)
    error = .true.
  endif
end subroutine getCalHolographyRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalHolographyTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalHolographyTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalHolographyTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalHolography
  !
  integer :: tableSize
  logical :: error
  type(CalHolographyKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalHolographyKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalHolographyKeys(antennaNameList, antennaNameListDim, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalHolographyKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalHolographyRow(row, error)
  use sdm_CalHolography
  type(CalHolographyRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalHolography'
  ! row%focusPosition allocation
  if (allocated(row%focusPosition)) then
    deallocate(row%focusPosition, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%direction allocation
  if (allocated(row%direction)) then
    deallocate(row%direction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%focusPosition(3), row%frequencyRange(2), row%polarizationTypes(row%numReceptor), row%direction(2),&
      & stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalHolographyRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalHolographyOpt(row, opt, error)
  use sdm_CalHolography
  type(CalHolographyRow) :: row
  type(CalHolographyOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalHolography'
  ! opt%screwName allocation
  if (allocated(opt%screwName)) then
    deallocate(opt%screwName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%screwMotion allocation
  if (allocated(opt%screwMotion)) then
    deallocate(opt%screwMotion, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%screwMotionError allocation
  if (allocated(opt%screwMotionError)) then
    deallocate(opt%screwMotionError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%gravOptRange allocation
  if (allocated(opt%gravOptRange)) then
    deallocate(opt%gravOptRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%tempOptRange allocation
  if (allocated(opt%tempOptRange)) then
    deallocate(opt%tempOptRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%screwName(opt%numScrew), opt%screwMotion(opt%numScrew), opt%screwMotionError(opt%numScrew),&
      & opt%gravOptRange(2), opt%tempOptRange(2), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalHolographyOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyNumScrew(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyNumScrew, i
  integer :: antennaNameDim(2)
  !! integer :: numScrew
  ! Deal with dimensions
  ireturn = sdm_addCalHolographyNumScrew(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%numScrew)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyNumScrew', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyNumScrew
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyNumScrew(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyNumScrew, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalHolographyNumScrew(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%numScrew)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyNumScrew', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyNumScrew
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyScrewName(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyScrewName, i
  integer :: antennaNameDim(2)
  !! character*256, allocatable :: screwName(:)
  integer, parameter :: screwNameRank = 1
  integer :: screwNameDim(2,screwNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%screwName)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwName not allocated.')
    error = .true.
    return
  endif
  screwNameDim(:,1) = len(opt%screwName)
  do i=1, screwNameRank
    screwNameDim(:,i+1) = size(opt%screwName,i)
    call charcut(opt%screwName(i))
  enddo
  ireturn = sdm_addCalHolographyScrewName(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwName, screwNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyScrewName', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyScrewName
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyScrewName(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyScrewName, i
  integer :: antennaNameDim(2)
  integer, parameter :: screwNameRank = 1
  integer :: screwNameDim(2,screwNameRank+1)
  ! Deal with dimensions
  if (.not.allocated(opt%screwName)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwName not allocated.')
    error = .true.
    return
  endif
  screwNameDim(:,1) = len(opt%screwName)
  do i=1, screwNameRank
    screwNameDim(:,i+1) = size(opt%screwName,i)
  enddo
  ireturn = sdm_getCalHolographyScrewName(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwName, screwNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyScrewName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyScrewName
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyScrewMotion(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyScrewMotion, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: screwMotion(:)
  integer, parameter :: screwMotionRank = 1
  integer :: screwMotionDim(2,screwMotionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%screwMotion)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwMotion not allocated.')
    error = .true.
    return
  endif
  do i=1, screwMotionRank
    screwMotionDim(:,i) = size(opt%screwMotion,i)
  enddo
  ireturn = sdm_addCalHolographyScrewMotion(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwMotion, screwMotionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyScrewMotion', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyScrewMotion
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyScrewMotion(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyScrewMotion, i
  integer :: antennaNameDim(2)
  integer, parameter :: screwMotionRank = 1
  integer :: screwMotionDim(2,screwMotionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%screwMotion)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwMotion not allocated.')
    error = .true.
    return
  endif
  do i=1, screwMotionRank
    screwMotionDim(:,i) = size(opt%screwMotion,i)
  enddo
  ireturn = sdm_getCalHolographyScrewMotion(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwMotion, screwMotionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyScrewMotion', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyScrewMotion
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyScrewMotionError(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyScrewMotionError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: screwMotionError(:)
  integer, parameter :: screwMotionErrorRank = 1
  integer :: screwMotionErrorDim(2,screwMotionErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%screwMotionError)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwMotionError not allocated.')
    error = .true.
    return
  endif
  do i=1, screwMotionErrorRank
    screwMotionErrorDim(:,i) = size(opt%screwMotionError,i)
  enddo
  ireturn = sdm_addCalHolographyScrewMotionError(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwMotionError, screwMotionErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyScrewMotionError', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyScrewMotionError
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyScrewMotionError(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyScrewMotionError, i
  integer :: antennaNameDim(2)
  integer, parameter :: screwMotionErrorRank = 1
  integer :: screwMotionErrorDim(2,screwMotionErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%screwMotionError)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%screwMotionError not allocated.')
    error = .true.
    return
  endif
  do i=1, screwMotionErrorRank
    screwMotionErrorDim(:,i) = size(opt%screwMotionError,i)
  enddo
  ireturn = sdm_getCalHolographyScrewMotionError(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%screwMotionError, screwMotionErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyScrewMotionError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyScrewMotionError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyGravCorrection(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyGravCorrection, i
  integer :: antennaNameDim(2)
  !! logical*1 :: gravCorrection
  ! Deal with dimensions
  ireturn = sdm_addCalHolographyGravCorrection(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%gravCorrection)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyGravCorrection', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyGravCorrection
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyGravCorrection(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyGravCorrection, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalHolographyGravCorrection(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%gravCorrection)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyGravCorrection', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyGravCorrection
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyGravOptRange(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyGravOptRange, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: gravOptRange(:)
  integer, parameter :: gravOptRangeRank = 1
  integer :: gravOptRangeDim(2,gravOptRangeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%gravOptRange)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%gravOptRange not allocated.')
    error = .true.
    return
  endif
  do i=1, gravOptRangeRank
    gravOptRangeDim(:,i) = size(opt%gravOptRange,i)
  enddo
  ireturn = sdm_addCalHolographyGravOptRange(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%gravOptRange, gravOptRangeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyGravOptRange', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyGravOptRange
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyGravOptRange(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyGravOptRange, i
  integer :: antennaNameDim(2)
  integer, parameter :: gravOptRangeRank = 1
  integer :: gravOptRangeDim(2,gravOptRangeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%gravOptRange)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%gravOptRange not allocated.')
    error = .true.
    return
  endif
  do i=1, gravOptRangeRank
    gravOptRangeDim(:,i) = size(opt%gravOptRange,i)
  enddo
  ireturn = sdm_getCalHolographyGravOptRange(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%gravOptRange, gravOptRangeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyGravOptRange', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyGravOptRange
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyTempCorrection(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyTempCorrection, i
  integer :: antennaNameDim(2)
  !! logical*1 :: tempCorrection
  ! Deal with dimensions
  ireturn = sdm_addCalHolographyTempCorrection(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%tempCorrection)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyTempCorrection', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyTempCorrection
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyTempCorrection(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyTempCorrection, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalHolographyTempCorrection(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%tempCorrection)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyTempCorrection', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyTempCorrection
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalHolographyTempOptRange(key, opt, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalHolographyTempOptRange, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: tempOptRange(:)
  integer, parameter :: tempOptRangeRank = 1
  integer :: tempOptRangeDim(2,tempOptRangeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tempOptRange)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%tempOptRange not allocated.')
    error = .true.
    return
  endif
  do i=1, tempOptRangeRank
    tempOptRangeDim(:,i) = size(opt%tempOptRange,i)
  enddo
  ireturn = sdm_addCalHolographyTempOptRange(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%tempOptRange, tempOptRangeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_addCalHolographyTempOptRange', ireturn)
    error = .true.
  endif
end subroutine addCalHolographyTempOptRange
!
! ---------------------------------------------------------------------------
!
subroutine getCalHolographyTempOptRange(key, opt, present, error)
  !
  use sdm_CalHolography
  !
  type(CalHolographyKey) :: key
  type(CalHolographyOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalHolographyTempOptRange, i
  integer :: antennaNameDim(2)
  integer, parameter :: tempOptRangeRank = 1
  integer :: tempOptRangeDim(2,tempOptRangeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%tempOptRange)) then
    call sdmMessage(8,3,'CalHolographyTable','opt%tempOptRange not allocated.')
    error = .true.
    return
  endif
  do i=1, tempOptRangeRank
    tempOptRangeDim(:,i) = size(opt%tempOptRange,i)
  enddo
  ireturn = sdm_getCalHolographyTempOptRange(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId,&
      & opt%tempOptRange, tempOptRangeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalHolographyTable','Error in sdm_getCalHolographyTempOptRange', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalHolographyTempOptRange
!
!
! ===========================================================================
!
! CalAtmosphere Table:
!
! ===========================================================================
!
subroutine addCalAtmosphereRow(key, row, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereRow) :: row
  type(CalAtmosphereKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalAtmosphereRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: forwardEffSpectrumRank = 2
  integer :: forwardEffSpectrumDim(2,forwardEffSpectrumRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: frequencySpectrumRank = 1
  integer :: frequencySpectrumDim(2,frequencySpectrumRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: powerSkySpectrumRank = 2
  integer :: powerSkySpectrumDim(2,powerSkySpectrumRank)
  integer, parameter :: powerLoadSpectrumRank = 3
  integer :: powerLoadSpectrumDim(2,powerLoadSpectrumRank)
  integer, parameter :: tAtmSpectrumRank = 2
  integer :: tAtmSpectrumDim(2,tAtmSpectrumRank)
  integer, parameter :: tRecSpectrumRank = 2
  integer :: tRecSpectrumDim(2,tRecSpectrumRank)
  integer, parameter :: tSysSpectrumRank = 2
  integer :: tSysSpectrumDim(2,tSysSpectrumRank)
  integer, parameter :: tauSpectrumRank = 2
  integer :: tauSpectrumDim(2,tauSpectrumRank)
  integer, parameter :: tAtmRank = 1
  integer :: tAtmDim(2,tAtmRank)
  integer, parameter :: tRecRank = 1
  integer :: tRecDim(2,tRecRank)
  integer, parameter :: tSysRank = 1
  integer :: tSysDim(2,tSysRank)
  integer, parameter :: tauRank = 1
  integer :: tauDim(2,tauRank)
  integer, parameter :: waterRank = 1
  integer :: waterDim(2,waterRank)
  integer, parameter :: waterErrorRank = 1
  integer :: waterErrorDim(2,waterErrorRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%forwardEffSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%forwardEffSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEffSpectrumRank
    forwardEffSpectrumDim(:,i) = size(row%forwardEffSpectrum,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%frequencySpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%frequencySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencySpectrumRank
    frequencySpectrumDim(:,i) = size(row%frequencySpectrum,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%powerSkySpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%powerSkySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, powerSkySpectrumRank
    powerSkySpectrumDim(:,i) = size(row%powerSkySpectrum,i)
  enddo
  if (.not.allocated(row%powerLoadSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%powerLoadSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, powerLoadSpectrumRank
    powerLoadSpectrumDim(:,i) = size(row%powerLoadSpectrum,i)
  enddo
  if (.not.allocated(row%tAtmSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tAtmSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tAtmSpectrumRank
    tAtmSpectrumDim(:,i) = size(row%tAtmSpectrum,i)
  enddo
  if (.not.allocated(row%tRecSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tRecSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tRecSpectrumRank
    tRecSpectrumDim(:,i) = size(row%tRecSpectrum,i)
  enddo
  if (.not.allocated(row%tSysSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tSysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tSysSpectrumRank
    tSysSpectrumDim(:,i) = size(row%tSysSpectrum,i)
  enddo
  if (.not.allocated(row%tauSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tauSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tauSpectrumRank
    tauSpectrumDim(:,i) = size(row%tauSpectrum,i)
  enddo
  if (.not.allocated(row%tAtm)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tAtm not allocated.')
    error = .true.
    return
  endif
  do i=1, tAtmRank
    tAtmDim(:,i) = size(row%tAtm,i)
  enddo
  if (.not.allocated(row%tRec)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tRec not allocated.')
    error = .true.
    return
  endif
  do i=1, tRecRank
    tRecDim(:,i) = size(row%tRec,i)
  enddo
  if (.not.allocated(row%tSys)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tSys not allocated.')
    error = .true.
    return
  endif
  do i=1, tSysRank
    tSysDim(:,i) = size(row%tSys,i)
  enddo
  if (.not.allocated(row%tau)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tau not allocated.')
    error = .true.
    return
  endif
  do i=1, tauRank
    tauDim(:,i) = size(row%tau,i)
  enddo
  if (.not.allocated(row%water)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%water not allocated.')
    error = .true.
    return
  endif
  do i=1, waterRank
    waterDim(:,i) = size(row%water,i)
  enddo
  if (.not.allocated(row%waterError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%waterError not allocated.')
    error = .true.
    return
  endif
  do i=1, waterErrorRank
    waterErrorDim(:,i) = size(row%waterError,i)
  enddo
  !
  ireturn = sdm_addCalAtmosphereRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%numFreq, row%numLoad, row%numReceptor,&
      & row%forwardEffSpectrum, forwardEffSpectrumDim, row%frequencyRange, frequencyRangeDim, row%groundPressure,&
      & row%groundRelHumidity, row%frequencySpectrum, frequencySpectrumDim, row%groundTemperature,&
      & row%polarizationTypes, polarizationTypesDim, row%powerSkySpectrum, powerSkySpectrumDim, row%powerLoadSpectrum,&
      & powerLoadSpectrumDim, row%syscalType, row%tAtmSpectrum, tAtmSpectrumDim, row%tRecSpectrum, tRecSpectrumDim,&
      & row%tSysSpectrum, tSysSpectrumDim, row%tauSpectrum, tauSpectrumDim, row%tAtm, tAtmDim, row%tRec, tRecDim,&
      & row%tSys, tSysDim, row%tau, tauDim, row%water, waterDim, row%waterError, waterErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereRow', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereRow(key, row, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereRow) :: row
  type(CalAtmosphereKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalAtmosphereRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: forwardEffSpectrumRank = 2
  integer :: forwardEffSpectrumDim(2,forwardEffSpectrumRank)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: frequencySpectrumRank = 1
  integer :: frequencySpectrumDim(2,frequencySpectrumRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: powerSkySpectrumRank = 2
  integer :: powerSkySpectrumDim(2,powerSkySpectrumRank)
  integer, parameter :: powerLoadSpectrumRank = 3
  integer :: powerLoadSpectrumDim(2,powerLoadSpectrumRank)
  integer, parameter :: tAtmSpectrumRank = 2
  integer :: tAtmSpectrumDim(2,tAtmSpectrumRank)
  integer, parameter :: tRecSpectrumRank = 2
  integer :: tRecSpectrumDim(2,tRecSpectrumRank)
  integer, parameter :: tSysSpectrumRank = 2
  integer :: tSysSpectrumDim(2,tSysSpectrumRank)
  integer, parameter :: tauSpectrumRank = 2
  integer :: tauSpectrumDim(2,tauSpectrumRank)
  integer, parameter :: tAtmRank = 1
  integer :: tAtmDim(2,tAtmRank)
  integer, parameter :: tRecRank = 1
  integer :: tRecDim(2,tRecRank)
  integer, parameter :: tSysRank = 1
  integer :: tSysDim(2,tSysRank)
  integer, parameter :: tauRank = 1
  integer :: tauDim(2,tauRank)
  integer, parameter :: waterRank = 1
  integer :: waterDim(2,waterRank)
  integer, parameter :: waterErrorRank = 1
  integer :: waterErrorDim(2,waterErrorRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%forwardEffSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%forwardEffSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEffSpectrumRank
    forwardEffSpectrumDim(:,i) = size(row%forwardEffSpectrum,i)
  enddo
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%frequencySpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%frequencySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencySpectrumRank
    frequencySpectrumDim(:,i) = size(row%frequencySpectrum,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%powerSkySpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%powerSkySpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, powerSkySpectrumRank
    powerSkySpectrumDim(:,i) = size(row%powerSkySpectrum,i)
  enddo
  if (.not.allocated(row%powerLoadSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%powerLoadSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, powerLoadSpectrumRank
    powerLoadSpectrumDim(:,i) = size(row%powerLoadSpectrum,i)
  enddo
  if (.not.allocated(row%tAtmSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tAtmSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tAtmSpectrumRank
    tAtmSpectrumDim(:,i) = size(row%tAtmSpectrum,i)
  enddo
  if (.not.allocated(row%tRecSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tRecSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tRecSpectrumRank
    tRecSpectrumDim(:,i) = size(row%tRecSpectrum,i)
  enddo
  if (.not.allocated(row%tSysSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tSysSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tSysSpectrumRank
    tSysSpectrumDim(:,i) = size(row%tSysSpectrum,i)
  enddo
  if (.not.allocated(row%tauSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tauSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, tauSpectrumRank
    tauSpectrumDim(:,i) = size(row%tauSpectrum,i)
  enddo
  if (.not.allocated(row%tAtm)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tAtm not allocated.')
    error = .true.
    return
  endif
  do i=1, tAtmRank
    tAtmDim(:,i) = size(row%tAtm,i)
  enddo
  if (.not.allocated(row%tRec)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tRec not allocated.')
    error = .true.
    return
  endif
  do i=1, tRecRank
    tRecDim(:,i) = size(row%tRec,i)
  enddo
  if (.not.allocated(row%tSys)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tSys not allocated.')
    error = .true.
    return
  endif
  do i=1, tSysRank
    tSysDim(:,i) = size(row%tSys,i)
  enddo
  if (.not.allocated(row%tau)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%tau not allocated.')
    error = .true.
    return
  endif
  do i=1, tauRank
    tauDim(:,i) = size(row%tau,i)
  enddo
  if (.not.allocated(row%water)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%water not allocated.')
    error = .true.
    return
  endif
  do i=1, waterRank
    waterDim(:,i) = size(row%water,i)
  enddo
  if (.not.allocated(row%waterError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','row%waterError not allocated.')
    error = .true.
    return
  endif
  do i=1, waterErrorRank
    waterErrorDim(:,i) = size(row%waterError,i)
  enddo
  !
  ireturn = sdm_getCalAtmosphereRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%numFreq, row%numLoad, row%numReceptor,&
      & row%forwardEffSpectrum, forwardEffSpectrumDim, row%frequencyRange, frequencyRangeDim, row%groundPressure,&
      & row%groundRelHumidity, row%frequencySpectrum, frequencySpectrumDim, row%groundTemperature,&
      & row%polarizationTypes, polarizationTypesDim, row%powerSkySpectrum, powerSkySpectrumDim, row%powerLoadSpectrum,&
      & powerLoadSpectrumDim, row%syscalType, row%tAtmSpectrum, tAtmSpectrumDim, row%tRecSpectrum, tRecSpectrumDim,&
      & row%tSysSpectrum, tSysSpectrumDim, row%tauSpectrum, tauSpectrumDim, row%tAtm, tAtmDim, row%tRec, tRecDim,&
      & row%tSys, tSysDim, row%tau, tauDim, row%water, waterDim, row%waterError, waterErrorDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereRow', ireturn)
    error = .true.
  endif
end subroutine getCalAtmosphereRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalAtmosphereTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalAtmosphereTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalAtmosphereTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalAtmosphere
  !
  integer :: tableSize
  logical :: error
  type(CalAtmosphereKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalAtmosphereKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalAtmosphereKeys(antennaNameList, antennaNameListDim, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalAtmosphereKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalAtmosphereRow(row, error)
  use sdm_CalAtmosphere
  type(CalAtmosphereRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalAtmosphere'
  ! row%forwardEffSpectrum allocation
  if (allocated(row%forwardEffSpectrum)) then
    deallocate(row%forwardEffSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%frequencySpectrum allocation
  if (allocated(row%frequencySpectrum)) then
    deallocate(row%frequencySpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%powerSkySpectrum allocation
  if (allocated(row%powerSkySpectrum)) then
    deallocate(row%powerSkySpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%powerLoadSpectrum allocation
  if (allocated(row%powerLoadSpectrum)) then
    deallocate(row%powerLoadSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tAtmSpectrum allocation
  if (allocated(row%tAtmSpectrum)) then
    deallocate(row%tAtmSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tRecSpectrum allocation
  if (allocated(row%tRecSpectrum)) then
    deallocate(row%tRecSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tSysSpectrum allocation
  if (allocated(row%tSysSpectrum)) then
    deallocate(row%tSysSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tauSpectrum allocation
  if (allocated(row%tauSpectrum)) then
    deallocate(row%tauSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tAtm allocation
  if (allocated(row%tAtm)) then
    deallocate(row%tAtm, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tRec allocation
  if (allocated(row%tRec)) then
    deallocate(row%tRec, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tSys allocation
  if (allocated(row%tSys)) then
    deallocate(row%tSys, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%tau allocation
  if (allocated(row%tau)) then
    deallocate(row%tau, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%water allocation
  if (allocated(row%water)) then
    deallocate(row%water, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%waterError allocation
  if (allocated(row%waterError)) then
    deallocate(row%waterError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%forwardEffSpectrum(row%numFreq, row%numReceptor), row%frequencyRange(2),&
      & row%frequencySpectrum(row%numFreq), row%polarizationTypes(row%numReceptor), row%powerSkySpectrum(row%numFreq,&
      & row%numReceptor), row%powerLoadSpectrum(row%numFreq, row%numReceptor, row%numLoad),&
      & row%tAtmSpectrum(row%numFreq, row%numReceptor), row%tRecSpectrum(row%numFreq, row%numReceptor),&
      & row%tSysSpectrum(row%numFreq, row%numReceptor), row%tauSpectrum(row%numFreq, row%numReceptor),&
      & row%tAtm(row%numReceptor), row%tRec(row%numReceptor), row%tSys(row%numReceptor), row%tau(row%numReceptor),&
      & row%water(row%numReceptor), row%waterError(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalAtmosphereRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalAtmosphereOpt(row, opt, error)
  use sdm_CalAtmosphere
  type(CalAtmosphereRow) :: row
  type(CalAtmosphereOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalAtmosphere'
  ! opt%alphaSpectrum allocation
  if (allocated(opt%alphaSpectrum)) then
    deallocate(opt%alphaSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%forwardEfficiency allocation
  if (allocated(opt%forwardEfficiency)) then
    deallocate(opt%forwardEfficiency, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%forwardEfficiencyError allocation
  if (allocated(opt%forwardEfficiencyError)) then
    deallocate(opt%forwardEfficiencyError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sbGain allocation
  if (allocated(opt%sbGain)) then
    deallocate(opt%sbGain, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sbGainError allocation
  if (allocated(opt%sbGainError)) then
    deallocate(opt%sbGainError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sbGainSpectrum allocation
  if (allocated(opt%sbGainSpectrum)) then
    deallocate(opt%sbGainSpectrum, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%alphaSpectrum(row%numFreq, row%numReceptor), opt%forwardEfficiency(row%numReceptor),&
      & opt%forwardEfficiencyError(row%numReceptor), opt%sbGain(row%numReceptor), opt%sbGainError(row%numReceptor),&
      & opt%sbGainSpectrum(row%numFreq, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalAtmosphereOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereAlphaSpectrum(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereAlphaSpectrum, i
  integer :: antennaNameDim(2)
  !! real, allocatable :: alphaSpectrum(:,:)
  integer, parameter :: alphaSpectrumRank = 2
  integer :: alphaSpectrumDim(2,alphaSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%alphaSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%alphaSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, alphaSpectrumRank
    alphaSpectrumDim(:,i) = size(opt%alphaSpectrum,i)
  enddo
  ireturn = sdm_addCalAtmosphereAlphaSpectrum(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%alphaSpectrum, alphaSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereAlphaSpectrum', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereAlphaSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereAlphaSpectrum(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereAlphaSpectrum, i
  integer :: antennaNameDim(2)
  integer, parameter :: alphaSpectrumRank = 2
  integer :: alphaSpectrumDim(2,alphaSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%alphaSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%alphaSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, alphaSpectrumRank
    alphaSpectrumDim(:,i) = size(opt%alphaSpectrum,i)
  enddo
  ireturn = sdm_getCalAtmosphereAlphaSpectrum(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%alphaSpectrum, alphaSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereAlphaSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereAlphaSpectrum
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereForwardEfficiency(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereForwardEfficiency, i
  integer :: antennaNameDim(2)
  !! real, allocatable :: forwardEfficiency(:)
  integer, parameter :: forwardEfficiencyRank = 1
  integer :: forwardEfficiencyDim(2,forwardEfficiencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%forwardEfficiency)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%forwardEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEfficiencyRank
    forwardEfficiencyDim(:,i) = size(opt%forwardEfficiency,i)
  enddo
  ireturn = sdm_addCalAtmosphereForwardEfficiency(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%forwardEfficiency, forwardEfficiencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereForwardEfficiency', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereForwardEfficiency
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereForwardEfficiency(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereForwardEfficiency, i
  integer :: antennaNameDim(2)
  integer, parameter :: forwardEfficiencyRank = 1
  integer :: forwardEfficiencyDim(2,forwardEfficiencyRank)
  ! Deal with dimensions
  if (.not.allocated(opt%forwardEfficiency)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%forwardEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEfficiencyRank
    forwardEfficiencyDim(:,i) = size(opt%forwardEfficiency,i)
  enddo
  ireturn = sdm_getCalAtmosphereForwardEfficiency(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%forwardEfficiency, forwardEfficiencyDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereForwardEfficiency', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereForwardEfficiency
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereForwardEfficiencyError(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereForwardEfficiencyError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: forwardEfficiencyError(:)
  integer, parameter :: forwardEfficiencyErrorRank = 1
  integer :: forwardEfficiencyErrorDim(2,forwardEfficiencyErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%forwardEfficiencyError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%forwardEfficiencyError not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEfficiencyErrorRank
    forwardEfficiencyErrorDim(:,i) = size(opt%forwardEfficiencyError,i)
  enddo
  ireturn = sdm_addCalAtmosphereForwardEfficiencyError(key%antennaName, antennaNameDim, key%receiverBand,&
      & key%calDataId, key%calReductionId, opt%forwardEfficiencyError, forwardEfficiencyErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereForwardEfficiencyError', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereForwardEfficiencyError
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereForwardEfficiencyError(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereForwardEfficiencyError, i
  integer :: antennaNameDim(2)
  integer, parameter :: forwardEfficiencyErrorRank = 1
  integer :: forwardEfficiencyErrorDim(2,forwardEfficiencyErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%forwardEfficiencyError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%forwardEfficiencyError not allocated.')
    error = .true.
    return
  endif
  do i=1, forwardEfficiencyErrorRank
    forwardEfficiencyErrorDim(:,i) = size(opt%forwardEfficiencyError,i)
  enddo
  ireturn = sdm_getCalAtmosphereForwardEfficiencyError(key%antennaName, antennaNameDim, key%receiverBand,&
      & key%calDataId, key%calReductionId, opt%forwardEfficiencyError, forwardEfficiencyErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereForwardEfficiencyError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereForwardEfficiencyError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereSbGain(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereSbGain, i
  integer :: antennaNameDim(2)
  !! real, allocatable :: sbGain(:)
  integer, parameter :: sbGainRank = 1
  integer :: sbGainDim(2,sbGainRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGain)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGain not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainRank
    sbGainDim(:,i) = size(opt%sbGain,i)
  enddo
  ireturn = sdm_addCalAtmosphereSbGain(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGain, sbGainDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereSbGain', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereSbGain
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereSbGain(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereSbGain, i
  integer :: antennaNameDim(2)
  integer, parameter :: sbGainRank = 1
  integer :: sbGainDim(2,sbGainRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGain)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGain not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainRank
    sbGainDim(:,i) = size(opt%sbGain,i)
  enddo
  ireturn = sdm_getCalAtmosphereSbGain(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGain, sbGainDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereSbGain', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereSbGain
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereSbGainError(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereSbGainError, i
  integer :: antennaNameDim(2)
  !! real, allocatable :: sbGainError(:)
  integer, parameter :: sbGainErrorRank = 1
  integer :: sbGainErrorDim(2,sbGainErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGainError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGainError not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainErrorRank
    sbGainErrorDim(:,i) = size(opt%sbGainError,i)
  enddo
  ireturn = sdm_addCalAtmosphereSbGainError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGainError, sbGainErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereSbGainError', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereSbGainError
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereSbGainError(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereSbGainError, i
  integer :: antennaNameDim(2)
  integer, parameter :: sbGainErrorRank = 1
  integer :: sbGainErrorDim(2,sbGainErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGainError)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGainError not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainErrorRank
    sbGainErrorDim(:,i) = size(opt%sbGainError,i)
  enddo
  ireturn = sdm_getCalAtmosphereSbGainError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGainError, sbGainErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereSbGainError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereSbGainError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalAtmosphereSbGainSpectrum(key, opt, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalAtmosphereSbGainSpectrum, i
  integer :: antennaNameDim(2)
  !! real, allocatable :: sbGainSpectrum(:,:)
  integer, parameter :: sbGainSpectrumRank = 2
  integer :: sbGainSpectrumDim(2,sbGainSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGainSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGainSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainSpectrumRank
    sbGainSpectrumDim(:,i) = size(opt%sbGainSpectrum,i)
  enddo
  ireturn = sdm_addCalAtmosphereSbGainSpectrum(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGainSpectrum, sbGainSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_addCalAtmosphereSbGainSpectrum', ireturn)
    error = .true.
  endif
end subroutine addCalAtmosphereSbGainSpectrum
!
! ---------------------------------------------------------------------------
!
subroutine getCalAtmosphereSbGainSpectrum(key, opt, present, error)
  !
  use sdm_CalAtmosphere
  !
  type(CalAtmosphereKey) :: key
  type(CalAtmosphereOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalAtmosphereSbGainSpectrum, i
  integer :: antennaNameDim(2)
  integer, parameter :: sbGainSpectrumRank = 2
  integer :: sbGainSpectrumDim(2,sbGainSpectrumRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sbGainSpectrum)) then
    call sdmMessage(8,3,'CalAtmosphereTable','opt%sbGainSpectrum not allocated.')
    error = .true.
    return
  endif
  do i=1, sbGainSpectrumRank
    sbGainSpectrumDim(:,i) = size(opt%sbGainSpectrum,i)
  enddo
  ireturn = sdm_getCalAtmosphereSbGainSpectrum(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%sbGainSpectrum, sbGainSpectrumDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalAtmosphereTable','Error in sdm_getCalAtmosphereSbGainSpectrum', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalAtmosphereSbGainSpectrum
!
!
! ===========================================================================
!
! CalCurve Table:
!
! ===========================================================================
!
subroutine addCalCurveRow(key, row, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveRow) :: row
  type(CalCurveKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalCurveRow, i, j
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: antennaNamesRank = 1
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer :: refAntennaNameDim(2)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: curveRank = 3
  integer :: curveDim(2,curveRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalCurveTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalCurveTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  do i=1, antennaNamesDim(1,2)
    call charcut(row%antennaNames(i))
  enddo
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalCurveTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%curve)) then
    call sdmMessage(8,3,'CalCurveTable','row%curve not allocated.')
    error = .true.
    return
  endif
  do i=1, curveRank
    curveDim(:,i) = size(row%curve,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalCurveTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_addCalCurveRow(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%frequencyRange, frequencyRangeDim,&
      & row%numAntenna, row%numPoly, row%numReceptor, row%antennaNames, antennaNamesDim, row%refAntennaName,&
      & refAntennaNameDim, row%polarizationTypes, polarizationTypesDim, row%curve, curveDim, row%reducedChiSquared,&
      & reducedChiSquaredDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_addCalCurveRow', ireturn)
    error = .true.
  endif
end subroutine addCalCurveRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalCurveRow(key, row, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveRow) :: row
  type(CalCurveKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalCurveRow, i
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: antennaNamesRank = 1
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer :: refAntennaNameDim(2)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: curveRank = 3
  integer :: curveDim(2,curveRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! Deal with dimensions
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalCurveTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalCurveTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalCurveTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%curve)) then
    call sdmMessage(8,3,'CalCurveTable','row%curve not allocated.')
    error = .true.
    return
  endif
  do i=1, curveRank
    curveDim(:,i) = size(row%curve,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalCurveTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_getCalCurveRow(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%frequencyRange, frequencyRangeDim,&
      & row%numAntenna, row%numPoly, row%numReceptor, row%antennaNames, antennaNamesDim, row%refAntennaName,&
      & refAntennaNameDim, row%polarizationTypes, polarizationTypesDim, row%curve, curveDim, row%reducedChiSquared,&
      & reducedChiSquaredDim)
  do i=1,antennaNamesDim(1,2)
    row%antennaNames(i)(antennaNamesDim(1,1)+1:) = ''
  enddo
  row%refAntennaName(refAntennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_getCalCurveRow', ireturn)
    error = .true.
  endif
end subroutine getCalCurveRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalCurveTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalCurveTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalCurveTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalCurveTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalCurveKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalCurve
  !
  integer :: tableSize
  logical :: error
  type(CalCurveKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalCurveKeys, i
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: typeCurveList(:)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(typeCurveList(tableSize))
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalCurveKeys(atmPhaseCorrectionList, typeCurveList, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_getCalCurveKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%typeCurve = typeCurveList(i)
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalCurveKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalCurveRow(row, error)
  use sdm_CalCurve
  type(CalCurveRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalCurve'
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%antennaNames allocation
  if (allocated(row%antennaNames)) then
    deallocate(row%antennaNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%curve allocation
  if (allocated(row%curve)) then
    deallocate(row%curve, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%reducedChiSquared allocation
  if (allocated(row%reducedChiSquared)) then
    deallocate(row%reducedChiSquared, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%frequencyRange(2), row%antennaNames(row%numAntenna), row%polarizationTypes(row%numReceptor),&
      & row%curve(row%numPoly, row%numReceptor, row%numAntenna), row%reducedChiSquared(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalCurveRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalCurveOpt(row, opt, error)
  use sdm_CalCurve
  type(CalCurveRow) :: row
  type(CalCurveOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalCurve'
  ! opt%rms allocation
  if (allocated(opt%rms)) then
    deallocate(opt%rms, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%rms(opt%numBaseline, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalCurveOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalCurveNumBaseline(key, opt, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveKey) :: key
  type(CalCurveOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalCurveNumBaseline, i
  !! integer :: numBaseline
  ! Deal with dimensions
  ireturn = sdm_addCalCurveNumBaseline(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%numBaseline)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_addCalCurveNumBaseline', ireturn)
    error = .true.
  endif
end subroutine addCalCurveNumBaseline
!
! ---------------------------------------------------------------------------
!
subroutine getCalCurveNumBaseline(key, opt, present, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveKey) :: key
  type(CalCurveOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalCurveNumBaseline, i
  ! Deal with dimensions
  ireturn = sdm_getCalCurveNumBaseline(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%numBaseline)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_getCalCurveNumBaseline', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalCurveNumBaseline
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalCurveRms(key, opt, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveKey) :: key
  type(CalCurveOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalCurveRms, i
  !! real, allocatable :: rms(:,:)
  integer, parameter :: rmsRank = 2
  integer :: rmsDim(2,rmsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rms)) then
    call sdmMessage(8,3,'CalCurveTable','opt%rms not allocated.')
    error = .true.
    return
  endif
  do i=1, rmsRank
    rmsDim(:,i) = size(opt%rms,i)
  enddo
  ireturn = sdm_addCalCurveRms(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%rms, rmsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_addCalCurveRms', ireturn)
    error = .true.
  endif
end subroutine addCalCurveRms
!
! ---------------------------------------------------------------------------
!
subroutine getCalCurveRms(key, opt, present, error)
  !
  use sdm_CalCurve
  !
  type(CalCurveKey) :: key
  type(CalCurveOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalCurveRms, i
  integer, parameter :: rmsRank = 2
  integer :: rmsDim(2,rmsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rms)) then
    call sdmMessage(8,3,'CalCurveTable','opt%rms not allocated.')
    error = .true.
    return
  endif
  do i=1, rmsRank
    rmsDim(:,i) = size(opt%rms,i)
  enddo
  ireturn = sdm_getCalCurveRms(key%atmPhaseCorrection, key%typeCurve, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%rms, rmsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalCurveTable','Error in sdm_getCalCurveRms', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalCurveRms
!
!
! ===========================================================================
!
! Station Table:
!
! ===========================================================================
!
subroutine addStationRow(key, row, error)
  !
  use sdm_Station
  !
  type(StationRow) :: row
  type(StationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addStationRow, i, j
  integer :: nameDim(2)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! ----------------
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%position)) then
    call sdmMessage(8,3,'StationTable','row%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(row%position,i)
  enddo
  !
  ireturn = sdm_addStationRow(row%name, nameDim, row%position, positionDim, row%type)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StationTable','Error in sdm_addStationRow', ireturn)
    error = .true.
  else
    key%stationId = ireturn
  endif
end subroutine addStationRow
!
! ---------------------------------------------------------------------------
!
subroutine getStationRow(key, row, error)
  !
  use sdm_Station
  !
  type(StationRow) :: row
  type(StationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getStationRow, i
  integer :: nameDim(2)
  integer, parameter :: positionRank = 1
  integer :: positionDim(2,positionRank)
  ! Deal with dimensions
  nameDim = len(row%name)
  call charcut(row%name)
  if (.not.allocated(row%position)) then
    call sdmMessage(8,3,'StationTable','row%position not allocated.')
    error = .true.
    return
  endif
  do i=1, positionRank
    positionDim(:,i) = size(row%position,i)
  enddo
  !
  ireturn = sdm_getStationRow(key%stationId, row%name, nameDim, row%position, positionDim, row%type)
  row%name(nameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StationTable','Error in sdm_getStationRow', ireturn)
    error = .true.
  endif
end subroutine getStationRow
!
! ---------------------------------------------------------------------------
!
subroutine getStationTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getStationTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getStationTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getStationTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getStationKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Station
  !
  integer :: tableSize
  logical :: error
  type(StationKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getStationKeys, i
  integer, allocatable :: stationIdList(:)
  !
  allocate(stationIdList(tableSize))
  !
  ireturn = sdm_getStationKeys(stationIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'StationTable','Error in sdm_getStationKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%stationId = stationIdList(i)
  enddo
end subroutine getStationKeys
! ---------------------------------------------------------------------------
!
subroutine allocStationRow(row, error)
  use sdm_Station
  type(StationRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Station'
  ! row%position allocation
  if (allocated(row%position)) then
    deallocate(row%position, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%position(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocStationRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! AlmaRadiometer Table:
!
! ===========================================================================
!
subroutine addAlmaRadiometerRow(key, row, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerRow) :: row
  type(AlmaRadiometerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addAlmaRadiometerRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addAlmaRadiometerRow()
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_addAlmaRadiometerRow', ireturn)
    error = .true.
  else
    key%almaRadiometerId = ireturn
  endif
end subroutine addAlmaRadiometerRow
!
! ---------------------------------------------------------------------------
!
subroutine getAlmaRadiometerRow(key, row, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerRow) :: row
  type(AlmaRadiometerKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getAlmaRadiometerRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getAlmaRadiometerRow(key%almaRadiometerId)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_getAlmaRadiometerRow', ireturn)
    error = .true.
  endif
end subroutine getAlmaRadiometerRow
!
! ---------------------------------------------------------------------------
!
subroutine getAlmaRadiometerTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getAlmaRadiometerTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getAlmaRadiometerTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getAlmaRadiometerTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getAlmaRadiometerKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_AlmaRadiometer
  !
  integer :: tableSize
  logical :: error
  type(AlmaRadiometerKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getAlmaRadiometerKeys, i
  integer, allocatable :: almaRadiometerIdList(:)
  !
  allocate(almaRadiometerIdList(tableSize))
  !
  ireturn = sdm_getAlmaRadiometerKeys(almaRadiometerIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_getAlmaRadiometerKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%almaRadiometerId = almaRadiometerIdList(i)
  enddo
end subroutine getAlmaRadiometerKeys
! ---------------------------------------------------------------------------
!
subroutine allocAlmaRadiometerOpt(row, opt, error)
  use sdm_AlmaRadiometer
  type(AlmaRadiometerRow) :: row
  type(AlmaRadiometerOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'AlmaRadiometer'
  ! opt%spectralWindowId allocation
  if (allocated(opt%spectralWindowId)) then
    deallocate(opt%spectralWindowId, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%spectralWindowId(opt%numAntenna), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocAlmaRadiometerOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addAlmaRadiometerNumAntenna(key, opt, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerKey) :: key
  type(AlmaRadiometerOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAlmaRadiometerNumAntenna, i
  !! integer :: numAntenna
  ! Deal with dimensions
  ireturn = sdm_addAlmaRadiometerNumAntenna(key%almaRadiometerId, opt%numAntenna)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_addAlmaRadiometerNumAntenna', ireturn)
    error = .true.
  endif
end subroutine addAlmaRadiometerNumAntenna
!
! ---------------------------------------------------------------------------
!
subroutine getAlmaRadiometerNumAntenna(key, opt, present, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerKey) :: key
  type(AlmaRadiometerOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAlmaRadiometerNumAntenna, i
  ! Deal with dimensions
  ireturn = sdm_getAlmaRadiometerNumAntenna(key%almaRadiometerId, opt%numAntenna)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_getAlmaRadiometerNumAntenna', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAlmaRadiometerNumAntenna
!
!
! ---------------------------------------------------------------------------
!
subroutine addAlmaRadiometerSpectralWindowId(key, opt, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerKey) :: key
  type(AlmaRadiometerOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAlmaRadiometerSpectralWindowId, i
  !! integer, allocatable :: spectralWindowId(:)
  integer, parameter :: spectralWindowIdRank = 1
  integer :: spectralWindowIdDim(2,spectralWindowIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%spectralWindowId)) then
    call sdmMessage(8,3,'AlmaRadiometerTable','opt%spectralWindowId not allocated.')
    error = .true.
    return
  endif
  do i=1, spectralWindowIdRank
    spectralWindowIdDim(:,i) = size(opt%spectralWindowId,i)
  enddo
  ireturn = sdm_addAlmaRadiometerSpectralWindowId(key%almaRadiometerId, opt%spectralWindowId, spectralWindowIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_addAlmaRadiometerSpectralWindowId', ireturn)
    error = .true.
  endif
end subroutine addAlmaRadiometerSpectralWindowId
!
! ---------------------------------------------------------------------------
!
subroutine getAlmaRadiometerSpectralWindowId(key, opt, present, error)
  !
  use sdm_AlmaRadiometer
  !
  type(AlmaRadiometerKey) :: key
  type(AlmaRadiometerOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAlmaRadiometerSpectralWindowId, i
  integer, parameter :: spectralWindowIdRank = 1
  integer :: spectralWindowIdDim(2,spectralWindowIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%spectralWindowId)) then
    call sdmMessage(8,3,'AlmaRadiometerTable','opt%spectralWindowId not allocated.')
    error = .true.
    return
  endif
  do i=1, spectralWindowIdRank
    spectralWindowIdDim(:,i) = size(opt%spectralWindowId,i)
  enddo
  ireturn = sdm_getAlmaRadiometerSpectralWindowId(key%almaRadiometerId, opt%spectralWindowId, spectralWindowIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AlmaRadiometerTable','Error in sdm_getAlmaRadiometerSpectralWindowId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAlmaRadiometerSpectralWindowId
!
!
! ===========================================================================
!
! SquareLawDetector Table:
!
! ===========================================================================
!
subroutine addSquareLawDetectorRow(key, row, error)
  !
  use sdm_SquareLawDetector
  !
  type(SquareLawDetectorRow) :: row
  type(SquareLawDetectorKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addSquareLawDetectorRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addSquareLawDetectorRow(row%numBand, row%bandType)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SquareLawDetectorTable','Error in sdm_addSquareLawDetectorRow', ireturn)
    error = .true.
  else
    key%squareLawDetectorId = ireturn
  endif
end subroutine addSquareLawDetectorRow
!
! ---------------------------------------------------------------------------
!
subroutine getSquareLawDetectorRow(key, row, error)
  !
  use sdm_SquareLawDetector
  !
  type(SquareLawDetectorRow) :: row
  type(SquareLawDetectorKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getSquareLawDetectorRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getSquareLawDetectorRow(key%squareLawDetectorId, row%numBand, row%bandType)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SquareLawDetectorTable','Error in sdm_getSquareLawDetectorRow', ireturn)
    error = .true.
  endif
end subroutine getSquareLawDetectorRow
!
! ---------------------------------------------------------------------------
!
subroutine getSquareLawDetectorTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getSquareLawDetectorTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getSquareLawDetectorTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getSquareLawDetectorTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getSquareLawDetectorKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_SquareLawDetector
  !
  integer :: tableSize
  logical :: error
  type(SquareLawDetectorKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getSquareLawDetectorKeys, i
  integer, allocatable :: squareLawDetectorIdList(:)
  !
  allocate(squareLawDetectorIdList(tableSize))
  !
  ireturn = sdm_getSquareLawDetectorKeys(squareLawDetectorIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'SquareLawDetectorTable','Error in sdm_getSquareLawDetectorKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%squareLawDetectorId = squareLawDetectorIdList(i)
  enddo
end subroutine getSquareLawDetectorKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalFocus Table:
!
! ===========================================================================
!
subroutine addCalFocusRow(key, row, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusRow) :: row
  type(CalFocusKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalFocusRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: pointingDirectionRank = 1
  integer :: pointingDirectionDim(2,pointingDirectionRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: wereFixedRank = 1
  integer :: wereFixedDim(2,wereFixedRank)
  integer, parameter :: offsetRank = 2
  integer :: offsetDim(2,offsetRank)
  integer, parameter :: offsetErrorRank = 2
  integer :: offsetErrorDim(2,offsetErrorRank)
  integer, parameter :: offsetWasTiedRank = 2
  integer :: offsetWasTiedDim(2,offsetWasTiedRank)
  integer, parameter :: reducedChiSquaredRank = 2
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalFocusTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%pointingDirection)) then
    call sdmMessage(8,3,'CalFocusTable','row%pointingDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, pointingDirectionRank
    pointingDirectionDim(:,i) = size(row%pointingDirection,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalFocusTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%wereFixed)) then
    call sdmMessage(8,3,'CalFocusTable','row%wereFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, wereFixedRank
    wereFixedDim(:,i) = size(row%wereFixed,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'CalFocusTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  if (.not.allocated(row%offsetError)) then
    call sdmMessage(8,3,'CalFocusTable','row%offsetError not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetErrorRank
    offsetErrorDim(:,i) = size(row%offsetError,i)
  enddo
  if (.not.allocated(row%offsetWasTied)) then
    call sdmMessage(8,3,'CalFocusTable','row%offsetWasTied not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetWasTiedRank
    offsetWasTiedDim(:,i) = size(row%offsetWasTied,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalFocusTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_addCalFocusRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId, key%calReductionId,&
      & row%startValidTime, row%endValidTime, row%ambientTemperature, row%atmPhaseCorrection, row%focusMethod,&
      & row%frequencyRange, frequencyRangeDim, row%pointingDirection, pointingDirectionDim, row%numReceptor,&
      & row%polarizationTypes, polarizationTypesDim, row%wereFixed, wereFixedDim, row%offset, offsetDim,&
      & row%offsetError, offsetErrorDim, row%offsetWasTied, offsetWasTiedDim, row%reducedChiSquared,&
      & reducedChiSquaredDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusRow', ireturn)
    error = .true.
  endif
end subroutine addCalFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusRow(key, row, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusRow) :: row
  type(CalFocusKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalFocusRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: pointingDirectionRank = 1
  integer :: pointingDirectionDim(2,pointingDirectionRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: wereFixedRank = 1
  integer :: wereFixedDim(2,wereFixedRank)
  integer, parameter :: offsetRank = 2
  integer :: offsetDim(2,offsetRank)
  integer, parameter :: offsetErrorRank = 2
  integer :: offsetErrorDim(2,offsetErrorRank)
  integer, parameter :: offsetWasTiedRank = 2
  integer :: offsetWasTiedDim(2,offsetWasTiedRank)
  integer, parameter :: reducedChiSquaredRank = 2
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalFocusTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%pointingDirection)) then
    call sdmMessage(8,3,'CalFocusTable','row%pointingDirection not allocated.')
    error = .true.
    return
  endif
  do i=1, pointingDirectionRank
    pointingDirectionDim(:,i) = size(row%pointingDirection,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalFocusTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%wereFixed)) then
    call sdmMessage(8,3,'CalFocusTable','row%wereFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, wereFixedRank
    wereFixedDim(:,i) = size(row%wereFixed,i)
  enddo
  if (.not.allocated(row%offset)) then
    call sdmMessage(8,3,'CalFocusTable','row%offset not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetRank
    offsetDim(:,i) = size(row%offset,i)
  enddo
  if (.not.allocated(row%offsetError)) then
    call sdmMessage(8,3,'CalFocusTable','row%offsetError not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetErrorRank
    offsetErrorDim(:,i) = size(row%offsetError,i)
  enddo
  if (.not.allocated(row%offsetWasTied)) then
    call sdmMessage(8,3,'CalFocusTable','row%offsetWasTied not allocated.')
    error = .true.
    return
  endif
  do i=1, offsetWasTiedRank
    offsetWasTiedDim(:,i) = size(row%offsetWasTied,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalFocusTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_getCalFocusRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId, key%calReductionId,&
      & row%startValidTime, row%endValidTime, row%ambientTemperature, row%atmPhaseCorrection, row%focusMethod,&
      & row%frequencyRange, frequencyRangeDim, row%pointingDirection, pointingDirectionDim, row%numReceptor,&
      & row%polarizationTypes, polarizationTypesDim, row%wereFixed, wereFixedDim, row%offset, offsetDim,&
      & row%offsetError, offsetErrorDim, row%offsetWasTied, offsetWasTiedDim, row%reducedChiSquared,&
      & reducedChiSquaredDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusRow', ireturn)
    error = .true.
  endif
end subroutine getCalFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalFocusTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalFocusTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalFocusTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalFocus
  !
  integer :: tableSize
  logical :: error
  type(CalFocusKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalFocusKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalFocusKeys(antennaNameList, antennaNameListDim, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalFocusKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalFocusRow(row, error)
  use sdm_CalFocus
  type(CalFocusRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalFocus'
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%pointingDirection allocation
  if (allocated(row%pointingDirection)) then
    deallocate(row%pointingDirection, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%wereFixed allocation
  if (allocated(row%wereFixed)) then
    deallocate(row%wereFixed, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%offset allocation
  if (allocated(row%offset)) then
    deallocate(row%offset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%offsetError allocation
  if (allocated(row%offsetError)) then
    deallocate(row%offsetError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%offsetWasTied allocation
  if (allocated(row%offsetWasTied)) then
    deallocate(row%offsetWasTied, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%reducedChiSquared allocation
  if (allocated(row%reducedChiSquared)) then
    deallocate(row%reducedChiSquared, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%frequencyRange(2), row%pointingDirection(2), row%polarizationTypes(row%numReceptor), row%wereFixed(3),&
      & row%offset(3, row%numReceptor), row%offsetError(3, row%numReceptor), row%offsetWasTied(3, row%numReceptor),&
      & row%reducedChiSquared(3, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalFocusRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalFocusOpt(row, opt, error)
  use sdm_CalFocus
  type(CalFocusRow) :: row
  type(CalFocusOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalFocus'
  ! opt%focusCurveWidth allocation
  if (allocated(opt%focusCurveWidth)) then
    deallocate(opt%focusCurveWidth, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%focusCurveWidthError allocation
  if (allocated(opt%focusCurveWidthError)) then
    deallocate(opt%focusCurveWidthError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%focusCurveWasFixed allocation
  if (allocated(opt%focusCurveWasFixed)) then
    deallocate(opt%focusCurveWasFixed, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%offIntensity allocation
  if (allocated(opt%offIntensity)) then
    deallocate(opt%offIntensity, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%offIntensityError allocation
  if (allocated(opt%offIntensityError)) then
    deallocate(opt%offIntensityError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%peakIntensity allocation
  if (allocated(opt%peakIntensity)) then
    deallocate(opt%peakIntensity, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%peakIntensityError allocation
  if (allocated(opt%peakIntensityError)) then
    deallocate(opt%peakIntensityError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%focusCurveWidth(3, row%numReceptor), opt%focusCurveWidthError(3, row%numReceptor),&
      & opt%focusCurveWasFixed(3), opt%offIntensity(row%numReceptor), opt%offIntensityError(row%numReceptor),&
      & opt%peakIntensity(row%numReceptor), opt%peakIntensityError(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalFocusOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusPolarizationsAveraged(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusPolarizationsAveraged, i
  integer :: antennaNameDim(2)
  !! logical*1 :: polarizationsAveraged
  ! Deal with dimensions
  ireturn = sdm_addCalFocusPolarizationsAveraged(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%polarizationsAveraged)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusPolarizationsAveraged', ireturn)
    error = .true.
  endif
end subroutine addCalFocusPolarizationsAveraged
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusPolarizationsAveraged(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusPolarizationsAveraged, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFocusPolarizationsAveraged(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%polarizationsAveraged)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusPolarizationsAveraged', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusPolarizationsAveraged
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusFocusCurveWidth(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusFocusCurveWidth, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: focusCurveWidth(:,:)
  integer, parameter :: focusCurveWidthRank = 2
  integer :: focusCurveWidthDim(2,focusCurveWidthRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWidth)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWidthRank
    focusCurveWidthDim(:,i) = size(opt%focusCurveWidth,i)
  enddo
  ireturn = sdm_addCalFocusFocusCurveWidth(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWidth, focusCurveWidthDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusFocusCurveWidth', ireturn)
    error = .true.
  endif
end subroutine addCalFocusFocusCurveWidth
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusFocusCurveWidth(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusFocusCurveWidth, i
  integer :: antennaNameDim(2)
  integer, parameter :: focusCurveWidthRank = 2
  integer :: focusCurveWidthDim(2,focusCurveWidthRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWidth)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWidthRank
    focusCurveWidthDim(:,i) = size(opt%focusCurveWidth,i)
  enddo
  ireturn = sdm_getCalFocusFocusCurveWidth(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWidth, focusCurveWidthDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusFocusCurveWidth', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusFocusCurveWidth
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusFocusCurveWidthError(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusFocusCurveWidthError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: focusCurveWidthError(:,:)
  integer, parameter :: focusCurveWidthErrorRank = 2
  integer :: focusCurveWidthErrorDim(2,focusCurveWidthErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWidthError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWidthError not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWidthErrorRank
    focusCurveWidthErrorDim(:,i) = size(opt%focusCurveWidthError,i)
  enddo
  ireturn = sdm_addCalFocusFocusCurveWidthError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWidthError, focusCurveWidthErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusFocusCurveWidthError', ireturn)
    error = .true.
  endif
end subroutine addCalFocusFocusCurveWidthError
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusFocusCurveWidthError(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusFocusCurveWidthError, i
  integer :: antennaNameDim(2)
  integer, parameter :: focusCurveWidthErrorRank = 2
  integer :: focusCurveWidthErrorDim(2,focusCurveWidthErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWidthError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWidthError not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWidthErrorRank
    focusCurveWidthErrorDim(:,i) = size(opt%focusCurveWidthError,i)
  enddo
  ireturn = sdm_getCalFocusFocusCurveWidthError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWidthError, focusCurveWidthErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusFocusCurveWidthError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusFocusCurveWidthError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusFocusCurveWasFixed(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusFocusCurveWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1, allocatable :: focusCurveWasFixed(:)
  integer, parameter :: focusCurveWasFixedRank = 1
  integer :: focusCurveWasFixedDim(2,focusCurveWasFixedRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWasFixed)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWasFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWasFixedRank
    focusCurveWasFixedDim(:,i) = size(opt%focusCurveWasFixed,i)
  enddo
  ireturn = sdm_addCalFocusFocusCurveWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWasFixed, focusCurveWasFixedDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusFocusCurveWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalFocusFocusCurveWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusFocusCurveWasFixed(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusFocusCurveWasFixed, i
  integer :: antennaNameDim(2)
  integer, parameter :: focusCurveWasFixedRank = 1
  integer :: focusCurveWasFixedDim(2,focusCurveWasFixedRank)
  ! Deal with dimensions
  if (.not.allocated(opt%focusCurveWasFixed)) then
    call sdmMessage(8,3,'CalFocusTable','opt%focusCurveWasFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, focusCurveWasFixedRank
    focusCurveWasFixedDim(:,i) = size(opt%focusCurveWasFixed,i)
  enddo
  ireturn = sdm_getCalFocusFocusCurveWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%focusCurveWasFixed, focusCurveWasFixedDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusFocusCurveWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusFocusCurveWasFixed
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusOffIntensity(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusOffIntensity, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: offIntensity(:)
  integer, parameter :: offIntensityRank = 1
  integer :: offIntensityDim(2,offIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensity)) then
    call sdmMessage(8,3,'CalFocusTable','opt%offIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityRank
    offIntensityDim(:,i) = size(opt%offIntensity,i)
  enddo
  ireturn = sdm_addCalFocusOffIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensity, offIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusOffIntensity', ireturn)
    error = .true.
  endif
end subroutine addCalFocusOffIntensity
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusOffIntensity(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusOffIntensity, i
  integer :: antennaNameDim(2)
  integer, parameter :: offIntensityRank = 1
  integer :: offIntensityDim(2,offIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensity)) then
    call sdmMessage(8,3,'CalFocusTable','opt%offIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityRank
    offIntensityDim(:,i) = size(opt%offIntensity,i)
  enddo
  ireturn = sdm_getCalFocusOffIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensity, offIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusOffIntensity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusOffIntensity
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusOffIntensityError(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusOffIntensityError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: offIntensityError(:)
  integer, parameter :: offIntensityErrorRank = 1
  integer :: offIntensityErrorDim(2,offIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensityError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%offIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityErrorRank
    offIntensityErrorDim(:,i) = size(opt%offIntensityError,i)
  enddo
  ireturn = sdm_addCalFocusOffIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityError, offIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusOffIntensityError', ireturn)
    error = .true.
  endif
end subroutine addCalFocusOffIntensityError
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusOffIntensityError(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusOffIntensityError, i
  integer :: antennaNameDim(2)
  integer, parameter :: offIntensityErrorRank = 1
  integer :: offIntensityErrorDim(2,offIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%offIntensityError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%offIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, offIntensityErrorRank
    offIntensityErrorDim(:,i) = size(opt%offIntensityError,i)
  enddo
  ireturn = sdm_getCalFocusOffIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityError, offIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusOffIntensityError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusOffIntensityError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusOffIntensityWasFixed(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusOffIntensityWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1 :: offIntensityWasFixed
  ! Deal with dimensions
  ireturn = sdm_addCalFocusOffIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusOffIntensityWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalFocusOffIntensityWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusOffIntensityWasFixed(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusOffIntensityWasFixed, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFocusOffIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%offIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusOffIntensityWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusOffIntensityWasFixed
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusPeakIntensity(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusPeakIntensity, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: peakIntensity(:)
  integer, parameter :: peakIntensityRank = 1
  integer :: peakIntensityDim(2,peakIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensity)) then
    call sdmMessage(8,3,'CalFocusTable','opt%peakIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityRank
    peakIntensityDim(:,i) = size(opt%peakIntensity,i)
  enddo
  ireturn = sdm_addCalFocusPeakIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensity, peakIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusPeakIntensity', ireturn)
    error = .true.
  endif
end subroutine addCalFocusPeakIntensity
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusPeakIntensity(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusPeakIntensity, i
  integer :: antennaNameDim(2)
  integer, parameter :: peakIntensityRank = 1
  integer :: peakIntensityDim(2,peakIntensityRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensity)) then
    call sdmMessage(8,3,'CalFocusTable','opt%peakIntensity not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityRank
    peakIntensityDim(:,i) = size(opt%peakIntensity,i)
  enddo
  ireturn = sdm_getCalFocusPeakIntensity(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensity, peakIntensityDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusPeakIntensity', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusPeakIntensity
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusPeakIntensityError(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusPeakIntensityError, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: peakIntensityError(:)
  integer, parameter :: peakIntensityErrorRank = 1
  integer :: peakIntensityErrorDim(2,peakIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensityError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%peakIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityErrorRank
    peakIntensityErrorDim(:,i) = size(opt%peakIntensityError,i)
  enddo
  ireturn = sdm_addCalFocusPeakIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityError, peakIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusPeakIntensityError', ireturn)
    error = .true.
  endif
end subroutine addCalFocusPeakIntensityError
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusPeakIntensityError(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusPeakIntensityError, i
  integer :: antennaNameDim(2)
  integer, parameter :: peakIntensityErrorRank = 1
  integer :: peakIntensityErrorDim(2,peakIntensityErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%peakIntensityError)) then
    call sdmMessage(8,3,'CalFocusTable','opt%peakIntensityError not allocated.')
    error = .true.
    return
  endif
  do i=1, peakIntensityErrorRank
    peakIntensityErrorDim(:,i) = size(opt%peakIntensityError,i)
  enddo
  ireturn = sdm_getCalFocusPeakIntensityError(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityError, peakIntensityErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusPeakIntensityError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusPeakIntensityError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFocusPeakIntensityWasFixed(key, opt, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFocusPeakIntensityWasFixed, i
  integer :: antennaNameDim(2)
  !! logical*1 :: peakIntensityWasFixed
  ! Deal with dimensions
  ireturn = sdm_addCalFocusPeakIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_addCalFocusPeakIntensityWasFixed', ireturn)
    error = .true.
  endif
end subroutine addCalFocusPeakIntensityWasFixed
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusPeakIntensityWasFixed(key, opt, present, error)
  !
  use sdm_CalFocus
  !
  type(CalFocusKey) :: key
  type(CalFocusOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFocusPeakIntensityWasFixed, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFocusPeakIntensityWasFixed(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, opt%peakIntensityWasFixed)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusTable','Error in sdm_getCalFocusPeakIntensityWasFixed', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFocusPeakIntensityWasFixed
!
!
! ===========================================================================
!
! CalDelay Table:
!
! ===========================================================================
!
subroutine addCalDelayRow(key, row, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayRow) :: row
  type(CalDelayKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalDelayRow, i, j
  integer :: antennaNameDim(2)
  integer :: refAntennaNameDim(2)
  integer, parameter :: delayErrorRank = 1
  integer :: delayErrorDim(2,delayErrorRank)
  integer, parameter :: delayOffsetRank = 1
  integer :: delayOffsetDim(2,delayOffsetRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%delayError)) then
    call sdmMessage(8,3,'CalDelayTable','row%delayError not allocated.')
    error = .true.
    return
  endif
  do i=1, delayErrorRank
    delayErrorDim(:,i) = size(row%delayError,i)
  enddo
  if (.not.allocated(row%delayOffset)) then
    call sdmMessage(8,3,'CalDelayTable','row%delayOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, delayOffsetRank
    delayOffsetDim(:,i) = size(row%delayOffset,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalDelayTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalDelayTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_addCalDelayRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%refAntennaName,&
      & refAntennaNameDim, row%numReceptor, row%delayError, delayErrorDim, row%delayOffset, delayOffsetDim,&
      & row%polarizationTypes, polarizationTypesDim, row%reducedChiSquared, reducedChiSquaredDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayRow', ireturn)
    error = .true.
  endif
end subroutine addCalDelayRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayRow(key, row, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayRow) :: row
  type(CalDelayKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalDelayRow, i
  integer :: antennaNameDim(2)
  integer :: refAntennaNameDim(2)
  integer, parameter :: delayErrorRank = 1
  integer :: delayErrorDim(2,delayErrorRank)
  integer, parameter :: delayOffsetRank = 1
  integer :: delayOffsetDim(2,delayOffsetRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%delayError)) then
    call sdmMessage(8,3,'CalDelayTable','row%delayError not allocated.')
    error = .true.
    return
  endif
  do i=1, delayErrorRank
    delayErrorDim(:,i) = size(row%delayError,i)
  enddo
  if (.not.allocated(row%delayOffset)) then
    call sdmMessage(8,3,'CalDelayTable','row%delayOffset not allocated.')
    error = .true.
    return
  endif
  do i=1, delayOffsetRank
    delayOffsetDim(:,i) = size(row%delayOffset,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalDelayTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalDelayTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_getCalDelayRow(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%refAntennaName,&
      & refAntennaNameDim, row%numReceptor, row%delayError, delayErrorDim, row%delayOffset, delayOffsetDim,&
      & row%polarizationTypes, polarizationTypesDim, row%reducedChiSquared, reducedChiSquaredDim)
  key%antennaName(antennaNameDim(1)+1:) = ''
  row%refAntennaName(refAntennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayRow', ireturn)
    error = .true.
  endif
end subroutine getCalDelayRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalDelayTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalDelayTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalDelayTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalDelay
  !
  integer :: tableSize
  logical :: error
  type(CalDelayKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalDelayKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: basebandNameList(:)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(basebandNameList(tableSize))
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalDelayKeys(antennaNameList, antennaNameListDim, atmPhaseCorrectionList, basebandNameList,&
      & receiverBandList, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%basebandName = basebandNameList(i)
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalDelayKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalDelayRow(row, error)
  use sdm_CalDelay
  type(CalDelayRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalDelay'
  ! row%delayError allocation
  if (allocated(row%delayError)) then
    deallocate(row%delayError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%delayOffset allocation
  if (allocated(row%delayOffset)) then
    deallocate(row%delayOffset, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%reducedChiSquared allocation
  if (allocated(row%reducedChiSquared)) then
    deallocate(row%reducedChiSquared, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%delayError(row%numReceptor), row%delayOffset(row%numReceptor), row%polarizationTypes(row%numReceptor),&
      & row%reducedChiSquared(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDelayRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalDelayOpt(row, opt, error)
  use sdm_CalDelay
  type(CalDelayRow) :: row
  type(CalDelayOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalDelay'
  ! opt%refFreq allocation
  if (allocated(opt%refFreq)) then
    deallocate(opt%refFreq, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%refFreqPhase allocation
  if (allocated(opt%refFreqPhase)) then
    deallocate(opt%refFreqPhase, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sidebands allocation
  if (allocated(opt%sidebands)) then
    deallocate(opt%sidebands, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%refFreq(opt%numSideband), opt%refFreqPhase(opt%numSideband), opt%sidebands(opt%numSideband), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalDelayOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelayCrossDelayOffset(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelayCrossDelayOffset, i
  integer :: antennaNameDim(2)
  !! real*8 :: crossDelayOffset
  ! Deal with dimensions
  ireturn = sdm_addCalDelayCrossDelayOffset(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%crossDelayOffset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayCrossDelayOffset', ireturn)
    error = .true.
  endif
end subroutine addCalDelayCrossDelayOffset
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayCrossDelayOffset(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelayCrossDelayOffset, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalDelayCrossDelayOffset(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%crossDelayOffset)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayCrossDelayOffset', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelayCrossDelayOffset
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelayCrossDelayOffsetError(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelayCrossDelayOffsetError, i
  integer :: antennaNameDim(2)
  !! real*8 :: crossDelayOffsetError
  ! Deal with dimensions
  ireturn = sdm_addCalDelayCrossDelayOffsetError(key%antennaName, antennaNameDim, key%atmPhaseCorrection,&
      & key%basebandName, key%receiverBand, key%calDataId, key%calReductionId, opt%crossDelayOffsetError)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayCrossDelayOffsetError', ireturn)
    error = .true.
  endif
end subroutine addCalDelayCrossDelayOffsetError
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayCrossDelayOffsetError(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelayCrossDelayOffsetError, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalDelayCrossDelayOffsetError(key%antennaName, antennaNameDim, key%atmPhaseCorrection,&
      & key%basebandName, key%receiverBand, key%calDataId, key%calReductionId, opt%crossDelayOffsetError)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayCrossDelayOffsetError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelayCrossDelayOffsetError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelayNumSideband(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelayNumSideband, i
  integer :: antennaNameDim(2)
  !! integer :: numSideband
  ! Deal with dimensions
  ireturn = sdm_addCalDelayNumSideband(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%numSideband)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayNumSideband', ireturn)
    error = .true.
  endif
end subroutine addCalDelayNumSideband
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayNumSideband(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelayNumSideband, i
  integer :: antennaNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalDelayNumSideband(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%numSideband)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayNumSideband', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelayNumSideband
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelayRefFreq(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelayRefFreq, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: refFreq(:)
  integer, parameter :: refFreqRank = 1
  integer :: refFreqDim(2,refFreqRank)
  ! Deal with dimensions
  if (.not.allocated(opt%refFreq)) then
    call sdmMessage(8,3,'CalDelayTable','opt%refFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, refFreqRank
    refFreqDim(:,i) = size(opt%refFreq,i)
  enddo
  ireturn = sdm_addCalDelayRefFreq(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%refFreq, refFreqDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayRefFreq', ireturn)
    error = .true.
  endif
end subroutine addCalDelayRefFreq
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayRefFreq(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelayRefFreq, i
  integer :: antennaNameDim(2)
  integer, parameter :: refFreqRank = 1
  integer :: refFreqDim(2,refFreqRank)
  ! Deal with dimensions
  if (.not.allocated(opt%refFreq)) then
    call sdmMessage(8,3,'CalDelayTable','opt%refFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, refFreqRank
    refFreqDim(:,i) = size(opt%refFreq,i)
  enddo
  ireturn = sdm_getCalDelayRefFreq(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%refFreq, refFreqDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayRefFreq', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelayRefFreq
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelayRefFreqPhase(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelayRefFreqPhase, i
  integer :: antennaNameDim(2)
  !! real*8, allocatable :: refFreqPhase(:)
  integer, parameter :: refFreqPhaseRank = 1
  integer :: refFreqPhaseDim(2,refFreqPhaseRank)
  ! Deal with dimensions
  if (.not.allocated(opt%refFreqPhase)) then
    call sdmMessage(8,3,'CalDelayTable','opt%refFreqPhase not allocated.')
    error = .true.
    return
  endif
  do i=1, refFreqPhaseRank
    refFreqPhaseDim(:,i) = size(opt%refFreqPhase,i)
  enddo
  ireturn = sdm_addCalDelayRefFreqPhase(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%refFreqPhase, refFreqPhaseDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelayRefFreqPhase', ireturn)
    error = .true.
  endif
end subroutine addCalDelayRefFreqPhase
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelayRefFreqPhase(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelayRefFreqPhase, i
  integer :: antennaNameDim(2)
  integer, parameter :: refFreqPhaseRank = 1
  integer :: refFreqPhaseDim(2,refFreqPhaseRank)
  ! Deal with dimensions
  if (.not.allocated(opt%refFreqPhase)) then
    call sdmMessage(8,3,'CalDelayTable','opt%refFreqPhase not allocated.')
    error = .true.
    return
  endif
  do i=1, refFreqPhaseRank
    refFreqPhaseDim(:,i) = size(opt%refFreqPhase,i)
  enddo
  ireturn = sdm_getCalDelayRefFreqPhase(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%refFreqPhase, refFreqPhaseDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelayRefFreqPhase', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelayRefFreqPhase
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalDelaySidebands(key, opt, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalDelaySidebands, i
  integer :: antennaNameDim(2)
  !! integer, allocatable :: sidebands(:)
  integer, parameter :: sidebandsRank = 1
  integer :: sidebandsDim(2,sidebandsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sidebands)) then
    call sdmMessage(8,3,'CalDelayTable','opt%sidebands not allocated.')
    error = .true.
    return
  endif
  do i=1, sidebandsRank
    sidebandsDim(:,i) = size(opt%sidebands,i)
  enddo
  ireturn = sdm_addCalDelaySidebands(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%sidebands, sidebandsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_addCalDelaySidebands', ireturn)
    error = .true.
  endif
end subroutine addCalDelaySidebands
!
! ---------------------------------------------------------------------------
!
subroutine getCalDelaySidebands(key, opt, present, error)
  !
  use sdm_CalDelay
  !
  type(CalDelayKey) :: key
  type(CalDelayOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalDelaySidebands, i
  integer :: antennaNameDim(2)
  integer, parameter :: sidebandsRank = 1
  integer :: sidebandsDim(2,sidebandsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sidebands)) then
    call sdmMessage(8,3,'CalDelayTable','opt%sidebands not allocated.')
    error = .true.
    return
  endif
  do i=1, sidebandsRank
    sidebandsDim(:,i) = size(opt%sidebands,i)
  enddo
  ireturn = sdm_getCalDelaySidebands(key%antennaName, antennaNameDim, key%atmPhaseCorrection, key%basebandName,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%sidebands, sidebandsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalDelayTable','Error in sdm_getCalDelaySidebands', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalDelaySidebands
!
!
! ===========================================================================
!
! Holography Table:
!
! ===========================================================================
!
subroutine addHolographyRow(key, row, error)
  !
  use sdm_Holography
  !
  type(HolographyRow) :: row
  type(HolographyKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addHolographyRow, i, j
  integer, parameter :: typeRank = 1
  integer :: typeDim(2,typeRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%type)) then
    call sdmMessage(8,3,'HolographyTable','row%type not allocated.')
    error = .true.
    return
  endif
  do i=1, typeRank
    typeDim(:,i) = size(row%type,i)
  enddo
  !
  ireturn = sdm_addHolographyRow(row%distance, row%focus, row%numCorr, row%type, typeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HolographyTable','Error in sdm_addHolographyRow', ireturn)
    error = .true.
  else
    key%holographyId = ireturn
  endif
end subroutine addHolographyRow
!
! ---------------------------------------------------------------------------
!
subroutine getHolographyRow(key, row, error)
  !
  use sdm_Holography
  !
  type(HolographyRow) :: row
  type(HolographyKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getHolographyRow, i
  integer, parameter :: typeRank = 1
  integer :: typeDim(2,typeRank)
  ! Deal with dimensions
  if (.not.allocated(row%type)) then
    call sdmMessage(8,3,'HolographyTable','row%type not allocated.')
    error = .true.
    return
  endif
  do i=1, typeRank
    typeDim(:,i) = size(row%type,i)
  enddo
  !
  ireturn = sdm_getHolographyRow(key%holographyId, row%distance, row%focus, row%numCorr, row%type, typeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HolographyTable','Error in sdm_getHolographyRow', ireturn)
    error = .true.
  endif
end subroutine getHolographyRow
!
! ---------------------------------------------------------------------------
!
subroutine getHolographyTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getHolographyTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getHolographyTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getHolographyTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getHolographyKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Holography
  !
  integer :: tableSize
  logical :: error
  type(HolographyKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getHolographyKeys, i
  integer, allocatable :: holographyIdList(:)
  !
  allocate(holographyIdList(tableSize))
  !
  ireturn = sdm_getHolographyKeys(holographyIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'HolographyTable','Error in sdm_getHolographyKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%holographyId = holographyIdList(i)
  enddo
end subroutine getHolographyKeys
! ---------------------------------------------------------------------------
!
subroutine allocHolographyRow(row, error)
  use sdm_Holography
  type(HolographyRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'Holography'
  ! row%type allocation
  if (allocated(row%type)) then
    deallocate(row%type, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%type(row%numCorr), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocHolographyRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalBandpass Table:
!
! ===========================================================================
!
subroutine addCalBandpassRow(key, row, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassRow) :: row
  type(CalBandpassKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalBandpassRow, i, j
  integer, parameter :: antennaNamesRank = 1
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer :: refAntennaNameDim(2)
  integer, parameter :: freqLimitsRank = 1
  integer :: freqLimitsDim(2,freqLimitsRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: curveRank = 3
  integer :: curveDim(2,curveRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalBandpassTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  do i=1, antennaNamesDim(1,2)
    call charcut(row%antennaNames(i))
  enddo
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%freqLimits)) then
    call sdmMessage(8,3,'CalBandpassTable','row%freqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, freqLimitsRank
    freqLimitsDim(:,i) = size(row%freqLimits,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalBandpassTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%curve)) then
    call sdmMessage(8,3,'CalBandpassTable','row%curve not allocated.')
    error = .true.
    return
  endif
  do i=1, curveRank
    curveDim(:,i) = size(row%curve,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalBandpassTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_addCalBandpassRow(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%numAntenna,&
      & row%numPoly, row%numReceptor, row%antennaNames, antennaNamesDim, row%refAntennaName, refAntennaNameDim,&
      & row%freqLimits, freqLimitsDim, row%polarizationTypes, polarizationTypesDim, row%curve, curveDim,&
      & row%reducedChiSquared, reducedChiSquaredDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_addCalBandpassRow', ireturn)
    error = .true.
  endif
end subroutine addCalBandpassRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalBandpassRow(key, row, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassRow) :: row
  type(CalBandpassKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalBandpassRow, i
  integer, parameter :: antennaNamesRank = 1
  integer :: antennaNamesDim(2,antennaNamesRank+1)
  integer :: refAntennaNameDim(2)
  integer, parameter :: freqLimitsRank = 1
  integer :: freqLimitsDim(2,freqLimitsRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: curveRank = 3
  integer :: curveDim(2,curveRank)
  integer, parameter :: reducedChiSquaredRank = 1
  integer :: reducedChiSquaredDim(2,reducedChiSquaredRank)
  ! Deal with dimensions
  if (.not.allocated(row%antennaNames)) then
    call sdmMessage(8,3,'CalBandpassTable','row%antennaNames not allocated.')
    error = .true.
    return
  endif
  antennaNamesDim(:,1) = len(row%antennaNames)
  do i=1, antennaNamesRank
    antennaNamesDim(:,i+1) = size(row%antennaNames,i)
  enddo
  refAntennaNameDim = len(row%refAntennaName)
  call charcut(row%refAntennaName)
  if (.not.allocated(row%freqLimits)) then
    call sdmMessage(8,3,'CalBandpassTable','row%freqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, freqLimitsRank
    freqLimitsDim(:,i) = size(row%freqLimits,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalBandpassTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%curve)) then
    call sdmMessage(8,3,'CalBandpassTable','row%curve not allocated.')
    error = .true.
    return
  endif
  do i=1, curveRank
    curveDim(:,i) = size(row%curve,i)
  enddo
  if (.not.allocated(row%reducedChiSquared)) then
    call sdmMessage(8,3,'CalBandpassTable','row%reducedChiSquared not allocated.')
    error = .true.
    return
  endif
  do i=1, reducedChiSquaredRank
    reducedChiSquaredDim(:,i) = size(row%reducedChiSquared,i)
  enddo
  !
  ireturn = sdm_getCalBandpassRow(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%numAntenna,&
      & row%numPoly, row%numReceptor, row%antennaNames, antennaNamesDim, row%refAntennaName, refAntennaNameDim,&
      & row%freqLimits, freqLimitsDim, row%polarizationTypes, polarizationTypesDim, row%curve, curveDim,&
      & row%reducedChiSquared, reducedChiSquaredDim)
  do i=1,antennaNamesDim(1,2)
    row%antennaNames(i)(antennaNamesDim(1,1)+1:) = ''
  enddo
  row%refAntennaName(refAntennaNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_getCalBandpassRow', ireturn)
    error = .true.
  endif
end subroutine getCalBandpassRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalBandpassTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalBandpassTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalBandpassTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalBandpassTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalBandpassKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalBandpass
  !
  integer :: tableSize
  logical :: error
  type(CalBandpassKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalBandpassKeys, i
  integer, allocatable :: basebandNameList(:)
  integer, allocatable :: sidebandList(:)
  integer, allocatable :: atmPhaseCorrectionList(:)
  integer, allocatable :: typeCurveList(:)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(basebandNameList(tableSize))
  allocate(sidebandList(tableSize))
  allocate(atmPhaseCorrectionList(tableSize))
  allocate(typeCurveList(tableSize))
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalBandpassKeys(basebandNameList, sidebandList, atmPhaseCorrectionList, typeCurveList,&
      & receiverBandList, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_getCalBandpassKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%basebandName = basebandNameList(i)
    keyList(i)%sideband = sidebandList(i)
    keyList(i)%atmPhaseCorrection = atmPhaseCorrectionList(i)
    keyList(i)%typeCurve = typeCurveList(i)
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalBandpassKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalBandpassRow(row, error)
  use sdm_CalBandpass
  type(CalBandpassRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalBandpass'
  ! row%antennaNames allocation
  if (allocated(row%antennaNames)) then
    deallocate(row%antennaNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%freqLimits allocation
  if (allocated(row%freqLimits)) then
    deallocate(row%freqLimits, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%curve allocation
  if (allocated(row%curve)) then
    deallocate(row%curve, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%reducedChiSquared allocation
  if (allocated(row%reducedChiSquared)) then
    deallocate(row%reducedChiSquared, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%antennaNames(row%numAntenna), row%freqLimits(2), row%polarizationTypes(row%numReceptor),&
      & row%curve(row%numPoly, row%numReceptor, row%numAntenna), row%reducedChiSquared(row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalBandpassRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalBandpassOpt(row, opt, error)
  use sdm_CalBandpass
  type(CalBandpassRow) :: row
  type(CalBandpassOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalBandpass'
  ! opt%rms allocation
  if (allocated(opt%rms)) then
    deallocate(opt%rms, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%rms(opt%numBaseline, row%numReceptor), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalBandpassOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalBandpassNumBaseline(key, opt, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassKey) :: key
  type(CalBandpassOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalBandpassNumBaseline, i
  !! integer :: numBaseline
  ! Deal with dimensions
  ireturn = sdm_addCalBandpassNumBaseline(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%numBaseline)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_addCalBandpassNumBaseline', ireturn)
    error = .true.
  endif
end subroutine addCalBandpassNumBaseline
!
! ---------------------------------------------------------------------------
!
subroutine getCalBandpassNumBaseline(key, opt, present, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassKey) :: key
  type(CalBandpassOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalBandpassNumBaseline, i
  ! Deal with dimensions
  ireturn = sdm_getCalBandpassNumBaseline(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%numBaseline)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_getCalBandpassNumBaseline', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalBandpassNumBaseline
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalBandpassRms(key, opt, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassKey) :: key
  type(CalBandpassOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalBandpassRms, i
  !! real, allocatable :: rms(:,:)
  integer, parameter :: rmsRank = 2
  integer :: rmsDim(2,rmsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rms)) then
    call sdmMessage(8,3,'CalBandpassTable','opt%rms not allocated.')
    error = .true.
    return
  endif
  do i=1, rmsRank
    rmsDim(:,i) = size(opt%rms,i)
  enddo
  ireturn = sdm_addCalBandpassRms(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%rms, rmsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_addCalBandpassRms', ireturn)
    error = .true.
  endif
end subroutine addCalBandpassRms
!
! ---------------------------------------------------------------------------
!
subroutine getCalBandpassRms(key, opt, present, error)
  !
  use sdm_CalBandpass
  !
  type(CalBandpassKey) :: key
  type(CalBandpassOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalBandpassRms, i
  integer, parameter :: rmsRank = 2
  integer :: rmsDim(2,rmsRank)
  ! Deal with dimensions
  if (.not.allocated(opt%rms)) then
    call sdmMessage(8,3,'CalBandpassTable','opt%rms not allocated.')
    error = .true.
    return
  endif
  do i=1, rmsRank
    rmsDim(:,i) = size(opt%rms,i)
  enddo
  ireturn = sdm_getCalBandpassRms(key%basebandName, key%sideband, key%atmPhaseCorrection, key%typeCurve,&
      & key%receiverBand, key%calDataId, key%calReductionId, opt%rms, rmsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalBandpassTable','Error in sdm_getCalBandpassRms', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalBandpassRms
!
!
! ===========================================================================
!
! CalFlux Table:
!
! ===========================================================================
!
subroutine addCalFluxRow(key, row, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxRow) :: row
  type(CalFluxKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalFluxRow, i, j
  integer :: sourceNameDim(2)
  integer, parameter :: frequencyRangesRank = 2
  integer :: frequencyRangesDim(2,frequencyRangesRank)
  integer, parameter :: fluxRank = 2
  integer :: fluxDim(2,fluxRank)
  integer, parameter :: fluxErrorRank = 2
  integer :: fluxErrorDim(2,fluxErrorRank)
  integer, parameter :: stokesRank = 1
  integer :: stokesDim(2,stokesRank)
  ! ----------------
  ! Deal with dimensions
  sourceNameDim = len(key%sourceName)
  call charcut(key%sourceName)
  if (.not.allocated(row%frequencyRanges)) then
    call sdmMessage(8,3,'CalFluxTable','row%frequencyRanges not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangesRank
    frequencyRangesDim(:,i) = size(row%frequencyRanges,i)
  enddo
  if (.not.allocated(row%flux)) then
    call sdmMessage(8,3,'CalFluxTable','row%flux not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxRank
    fluxDim(:,i) = size(row%flux,i)
  enddo
  if (.not.allocated(row%fluxError)) then
    call sdmMessage(8,3,'CalFluxTable','row%fluxError not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxErrorRank
    fluxErrorDim(:,i) = size(row%fluxError,i)
  enddo
  if (.not.allocated(row%stokes)) then
    call sdmMessage(8,3,'CalFluxTable','row%stokes not allocated.')
    error = .true.
    return
  endif
  do i=1, stokesRank
    stokesDim(:,i) = size(row%stokes,i)
  enddo
  !
  ireturn = sdm_addCalFluxRow(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%numFrequencyRanges, row%numStokes, row%frequencyRanges, frequencyRangesDim,&
      & row%fluxMethod, row%flux, fluxDim, row%fluxError, fluxErrorDim, row%stokes, stokesDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxRow', ireturn)
    error = .true.
  endif
end subroutine addCalFluxRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxRow(key, row, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxRow) :: row
  type(CalFluxKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalFluxRow, i
  integer :: sourceNameDim(2)
  integer, parameter :: frequencyRangesRank = 2
  integer :: frequencyRangesDim(2,frequencyRangesRank)
  integer, parameter :: fluxRank = 2
  integer :: fluxDim(2,fluxRank)
  integer, parameter :: fluxErrorRank = 2
  integer :: fluxErrorDim(2,fluxErrorRank)
  integer, parameter :: stokesRank = 1
  integer :: stokesDim(2,stokesRank)
  ! Deal with dimensions
  sourceNameDim = len(key%sourceName)
  call charcut(key%sourceName)
  if (.not.allocated(row%frequencyRanges)) then
    call sdmMessage(8,3,'CalFluxTable','row%frequencyRanges not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangesRank
    frequencyRangesDim(:,i) = size(row%frequencyRanges,i)
  enddo
  if (.not.allocated(row%flux)) then
    call sdmMessage(8,3,'CalFluxTable','row%flux not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxRank
    fluxDim(:,i) = size(row%flux,i)
  enddo
  if (.not.allocated(row%fluxError)) then
    call sdmMessage(8,3,'CalFluxTable','row%fluxError not allocated.')
    error = .true.
    return
  endif
  do i=1, fluxErrorRank
    fluxErrorDim(:,i) = size(row%fluxError,i)
  enddo
  if (.not.allocated(row%stokes)) then
    call sdmMessage(8,3,'CalFluxTable','row%stokes not allocated.')
    error = .true.
    return
  endif
  do i=1, stokesRank
    stokesDim(:,i) = size(row%stokes,i)
  enddo
  !
  ireturn = sdm_getCalFluxRow(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%numFrequencyRanges, row%numStokes, row%frequencyRanges, frequencyRangesDim,&
      & row%fluxMethod, row%flux, fluxDim, row%fluxError, fluxErrorDim, row%stokes, stokesDim)
  key%sourceName(sourceNameDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxRow', ireturn)
    error = .true.
  endif
end subroutine getCalFluxRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalFluxTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalFluxTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalFluxTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalFlux
  !
  integer :: tableSize
  logical :: error
  type(CalFluxKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalFluxKeys, i
  character*256, allocatable :: sourceNameList(:)
  integer :: sourceNameListDim(2,2)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(sourceNameList(tableSize))
  sourceNameListDim = len(keyList%sourceName)
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalFluxKeys(sourceNameList, sourceNameListDim, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%sourceName = sourceNameList(i)(1:sourceNameListDim(1,1))
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalFluxKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalFluxRow(row, error)
  use sdm_CalFlux
  type(CalFluxRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalFlux'
  ! row%frequencyRanges allocation
  if (allocated(row%frequencyRanges)) then
    deallocate(row%frequencyRanges, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%flux allocation
  if (allocated(row%flux)) then
    deallocate(row%flux, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%fluxError allocation
  if (allocated(row%fluxError)) then
    deallocate(row%fluxError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%stokes allocation
  if (allocated(row%stokes)) then
    deallocate(row%stokes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%frequencyRanges(2, row%numFrequencyRanges), row%flux(row%numFrequencyRanges, row%numStokes),&
      & row%fluxError(row%numFrequencyRanges, row%numStokes), row%stokes(row%numStokes), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalFluxRow
!
! ---------------------------------------------------------------------------
!
subroutine allocCalFluxOpt(row, opt, error)
  use sdm_CalFlux
  type(CalFluxRow) :: row
  type(CalFluxOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'CalFlux'
  ! opt%direction allocation
  if (allocated(opt%direction)) then
    deallocate(opt%direction, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%PA allocation
  if (allocated(opt%PA)) then
    deallocate(opt%PA, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%PAError allocation
  if (allocated(opt%PAError)) then
    deallocate(opt%PAError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%size allocation
  if (allocated(opt%size)) then
    deallocate(opt%size, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%sizeError allocation
  if (allocated(opt%sizeError)) then
    deallocate(opt%sizeError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%direction(2), opt%PA(row%numFrequencyRanges, row%numStokes), opt%PAError(row%numFrequencyRanges,&
      & row%numStokes), opt%size(2, row%numFrequencyRanges, row%numStokes), opt%sizeError(2, row%numFrequencyRanges,&
      & row%numStokes), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalFluxOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxDirection(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxDirection, i
  integer :: sourceNameDim(2)
  !! real*8, allocatable :: direction(:)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%direction)) then
    call sdmMessage(8,3,'CalFluxTable','opt%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(opt%direction,i)
  enddo
  ireturn = sdm_addCalFluxDirection(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%direction,&
      & directionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxDirection', ireturn)
    error = .true.
  endif
end subroutine addCalFluxDirection
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxDirection(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxDirection, i
  integer :: sourceNameDim(2)
  integer, parameter :: directionRank = 1
  integer :: directionDim(2,directionRank)
  ! Deal with dimensions
  if (.not.allocated(opt%direction)) then
    call sdmMessage(8,3,'CalFluxTable','opt%direction not allocated.')
    error = .true.
    return
  endif
  do i=1, directionRank
    directionDim(:,i) = size(opt%direction,i)
  enddo
  ireturn = sdm_getCalFluxDirection(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%direction,&
      & directionDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxDirection', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxDirection
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxDirectionCode(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxDirectionCode, i
  integer :: sourceNameDim(2)
  !! integer :: directionCode
  ! Deal with dimensions
  ireturn = sdm_addCalFluxDirectionCode(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxDirectionCode', ireturn)
    error = .true.
  endif
end subroutine addCalFluxDirectionCode
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxDirectionCode(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxDirectionCode, i
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFluxDirectionCode(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%directionCode)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxDirectionCode', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxDirectionCode
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxDirectionEquinox(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxDirectionEquinox, i
  integer :: sourceNameDim(2)
  !! real*8 :: directionEquinox
  ! Deal with dimensions
  ireturn = sdm_addCalFluxDirectionEquinox(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxDirectionEquinox', ireturn)
    error = .true.
  endif
end subroutine addCalFluxDirectionEquinox
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxDirectionEquinox(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxDirectionEquinox, i
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFluxDirectionEquinox(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%directionEquinox)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxDirectionEquinox', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxDirectionEquinox
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxPA(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxPA, i
  integer :: sourceNameDim(2)
  !! real*8, allocatable :: PA(:,:)
  integer, parameter :: PARank = 2
  integer :: PADim(2,PARank)
  ! Deal with dimensions
  if (.not.allocated(opt%PA)) then
    call sdmMessage(8,3,'CalFluxTable','opt%PA not allocated.')
    error = .true.
    return
  endif
  do i=1, PARank
    PADim(:,i) = size(opt%PA,i)
  enddo
  ireturn = sdm_addCalFluxPA(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%PA, PADim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxPA', ireturn)
    error = .true.
  endif
end subroutine addCalFluxPA
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxPA(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxPA, i
  integer :: sourceNameDim(2)
  integer, parameter :: PARank = 2
  integer :: PADim(2,PARank)
  ! Deal with dimensions
  if (.not.allocated(opt%PA)) then
    call sdmMessage(8,3,'CalFluxTable','opt%PA not allocated.')
    error = .true.
    return
  endif
  do i=1, PARank
    PADim(:,i) = size(opt%PA,i)
  enddo
  ireturn = sdm_getCalFluxPA(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%PA, PADim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxPA', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxPA
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxPAError(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxPAError, i
  integer :: sourceNameDim(2)
  !! real*8, allocatable :: PAError(:,:)
  integer, parameter :: PAErrorRank = 2
  integer :: PAErrorDim(2,PAErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%PAError)) then
    call sdmMessage(8,3,'CalFluxTable','opt%PAError not allocated.')
    error = .true.
    return
  endif
  do i=1, PAErrorRank
    PAErrorDim(:,i) = size(opt%PAError,i)
  enddo
  ireturn = sdm_addCalFluxPAError(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%PAError,&
      & PAErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxPAError', ireturn)
    error = .true.
  endif
end subroutine addCalFluxPAError
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxPAError(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxPAError, i
  integer :: sourceNameDim(2)
  integer, parameter :: PAErrorRank = 2
  integer :: PAErrorDim(2,PAErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%PAError)) then
    call sdmMessage(8,3,'CalFluxTable','opt%PAError not allocated.')
    error = .true.
    return
  endif
  do i=1, PAErrorRank
    PAErrorDim(:,i) = size(opt%PAError,i)
  enddo
  ireturn = sdm_getCalFluxPAError(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%PAError,&
      & PAErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxPAError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxPAError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxSize(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxSize, i
  integer :: sourceNameDim(2)
  !! real*8, allocatable :: size(:,:,:)
  integer, parameter :: sizeRank = 3
  integer :: sizeDim(2,sizeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%size)) then
    call sdmMessage(8,3,'CalFluxTable','opt%size not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeRank
    sizeDim(:,i) = size(opt%size,i)
  enddo
  ireturn = sdm_addCalFluxSize(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%size, sizeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxSize', ireturn)
    error = .true.
  endif
end subroutine addCalFluxSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxSize(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxSize, i
  integer :: sourceNameDim(2)
  integer, parameter :: sizeRank = 3
  integer :: sizeDim(2,sizeRank)
  ! Deal with dimensions
  if (.not.allocated(opt%size)) then
    call sdmMessage(8,3,'CalFluxTable','opt%size not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeRank
    sizeDim(:,i) = size(opt%size,i)
  enddo
  ireturn = sdm_getCalFluxSize(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%size, sizeDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxSize', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxSize
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxSizeError(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxSizeError, i
  integer :: sourceNameDim(2)
  !! real*8, allocatable :: sizeError(:,:,:)
  integer, parameter :: sizeErrorRank = 3
  integer :: sizeErrorDim(2,sizeErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sizeError)) then
    call sdmMessage(8,3,'CalFluxTable','opt%sizeError not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeErrorRank
    sizeErrorDim(:,i) = size(opt%sizeError,i)
  enddo
  ireturn = sdm_addCalFluxSizeError(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%sizeError,&
      & sizeErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxSizeError', ireturn)
    error = .true.
  endif
end subroutine addCalFluxSizeError
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxSizeError(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxSizeError, i
  integer :: sourceNameDim(2)
  integer, parameter :: sizeErrorRank = 3
  integer :: sizeErrorDim(2,sizeErrorRank)
  ! Deal with dimensions
  if (.not.allocated(opt%sizeError)) then
    call sdmMessage(8,3,'CalFluxTable','opt%sizeError not allocated.')
    error = .true.
    return
  endif
  do i=1, sizeErrorRank
    sizeErrorDim(:,i) = size(opt%sizeError,i)
  enddo
  ireturn = sdm_getCalFluxSizeError(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId, opt%sizeError,&
      & sizeErrorDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxSizeError', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxSizeError
!
!
! ---------------------------------------------------------------------------
!
subroutine addCalFluxSourceModel(key, opt, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addCalFluxSourceModel, i
  integer :: sourceNameDim(2)
  !! integer :: sourceModel
  ! Deal with dimensions
  ireturn = sdm_addCalFluxSourceModel(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%sourceModel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_addCalFluxSourceModel', ireturn)
    error = .true.
  endif
end subroutine addCalFluxSourceModel
!
! ---------------------------------------------------------------------------
!
subroutine getCalFluxSourceModel(key, opt, present, error)
  !
  use sdm_CalFlux
  !
  type(CalFluxKey) :: key
  type(CalFluxOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getCalFluxSourceModel, i
  integer :: sourceNameDim(2)
  ! Deal with dimensions
  ireturn = sdm_getCalFluxSourceModel(key%sourceName, sourceNameDim, key%calDataId, key%calReductionId,&
      & opt%sourceModel)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFluxTable','Error in sdm_getCalFluxSourceModel', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getCalFluxSourceModel
!
!
! ===========================================================================
!
! CalFocusModel Table:
!
! ===========================================================================
!
subroutine addCalFocusModelRow(key, row, error)
  !
  use sdm_CalFocusModel
  !
  type(CalFocusModelRow) :: row
  type(CalFocusModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalFocusModelRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  integer, parameter :: coeffValueRank = 1
  integer :: coeffValueDim(2,coeffValueRank)
  integer, parameter :: coeffErrorRank = 1
  integer :: coeffErrorDim(2,coeffErrorRank)
  integer, parameter :: coeffFixedRank = 1
  integer :: coeffFixedDim(2,coeffFixedRank)
  integer :: focusModelDim(2)
  integer, parameter :: focusRMSRank = 1
  integer :: focusRMSDim(2,focusRMSRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  do i=1, coeffNameDim(1,2)
    call charcut(row%coeffName(i))
  enddo
  if (.not.allocated(row%coeffFormula)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(row%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(row%coeffFormula,i)
  enddo
  do i=1, coeffFormulaDim(1,2)
    call charcut(row%coeffFormula(i))
  enddo
  if (.not.allocated(row%coeffValue)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffValue not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValueRank
    coeffValueDim(:,i) = size(row%coeffValue,i)
  enddo
  if (.not.allocated(row%coeffError)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffError not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffErrorRank
    coeffErrorDim(:,i) = size(row%coeffError,i)
  enddo
  if (.not.allocated(row%coeffFixed)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffFixedRank
    coeffFixedDim(:,i) = size(row%coeffFixed,i)
  enddo
  focusModelDim = len(row%focusModel)
  call charcut(row%focusModel)
  if (.not.allocated(row%focusRMS)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%focusRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, focusRMSRank
    focusRMSDim(:,i) = size(row%focusRMS,i)
  enddo
  !
  ireturn = sdm_addCalFocusModelRow(key%antennaName, antennaNameDim, key%receiverBand, key%polarizationType,&
      & key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%numCoeff,&
      & row%numSourceObs, row%coeffName, coeffNameDim, row%coeffFormula, coeffFormulaDim, row%coeffValue,&
      & coeffValueDim, row%coeffError, coeffErrorDim, row%coeffFixed, coeffFixedDim, row%focusModel, focusModelDim,&
      & row%focusRMS, focusRMSDim, row%reducedChiSquared)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusModelTable','Error in sdm_addCalFocusModelRow', ireturn)
    error = .true.
  endif
end subroutine addCalFocusModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusModelRow(key, row, error)
  !
  use sdm_CalFocusModel
  !
  type(CalFocusModelRow) :: row
  type(CalFocusModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalFocusModelRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: coeffNameRank = 1
  integer :: coeffNameDim(2,coeffNameRank+1)
  integer, parameter :: coeffFormulaRank = 1
  integer :: coeffFormulaDim(2,coeffFormulaRank+1)
  integer, parameter :: coeffValueRank = 1
  integer :: coeffValueDim(2,coeffValueRank)
  integer, parameter :: coeffErrorRank = 1
  integer :: coeffErrorDim(2,coeffErrorRank)
  integer, parameter :: coeffFixedRank = 1
  integer :: coeffFixedDim(2,coeffFixedRank)
  integer :: focusModelDim(2)
  integer, parameter :: focusRMSRank = 1
  integer :: focusRMSDim(2,focusRMSRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%coeffName)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffName not allocated.')
    error = .true.
    return
  endif
  coeffNameDim(:,1) = len(row%coeffName)
  do i=1, coeffNameRank
    coeffNameDim(:,i+1) = size(row%coeffName,i)
  enddo
  if (.not.allocated(row%coeffFormula)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffFormula not allocated.')
    error = .true.
    return
  endif
  coeffFormulaDim(:,1) = len(row%coeffFormula)
  do i=1, coeffFormulaRank
    coeffFormulaDim(:,i+1) = size(row%coeffFormula,i)
  enddo
  if (.not.allocated(row%coeffValue)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffValue not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffValueRank
    coeffValueDim(:,i) = size(row%coeffValue,i)
  enddo
  if (.not.allocated(row%coeffError)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffError not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffErrorRank
    coeffErrorDim(:,i) = size(row%coeffError,i)
  enddo
  if (.not.allocated(row%coeffFixed)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%coeffFixed not allocated.')
    error = .true.
    return
  endif
  do i=1, coeffFixedRank
    coeffFixedDim(:,i) = size(row%coeffFixed,i)
  enddo
  focusModelDim = len(row%focusModel)
  call charcut(row%focusModel)
  if (.not.allocated(row%focusRMS)) then
    call sdmMessage(8,3,'CalFocusModelTable','row%focusRMS not allocated.')
    error = .true.
    return
  endif
  do i=1, focusRMSRank
    focusRMSDim(:,i) = size(row%focusRMS,i)
  enddo
  !
  ireturn = sdm_getCalFocusModelRow(key%antennaName, antennaNameDim, key%receiverBand, key%polarizationType,&
      & key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%numCoeff,&
      & row%numSourceObs, row%coeffName, coeffNameDim, row%coeffFormula, coeffFormulaDim, row%coeffValue,&
      & coeffValueDim, row%coeffError, coeffErrorDim, row%coeffFixed, coeffFixedDim, row%focusModel, focusModelDim,&
      & row%focusRMS, focusRMSDim, row%reducedChiSquared)
  key%antennaName(antennaNameDim(1)+1:) = ''
  do i=1,coeffNameDim(1,2)
    row%coeffName(i)(coeffNameDim(1,1)+1:) = ''
  enddo
  do i=1,coeffFormulaDim(1,2)
    row%coeffFormula(i)(coeffFormulaDim(1,1)+1:) = ''
  enddo
  row%focusModel(focusModelDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusModelTable','Error in sdm_getCalFocusModelRow', ireturn)
    error = .true.
  endif
end subroutine getCalFocusModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusModelTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalFocusModelTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalFocusModelTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalFocusModelTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalFocusModelKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalFocusModel
  !
  integer :: tableSize
  logical :: error
  type(CalFocusModelKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalFocusModelKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: polarizationTypeList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(polarizationTypeList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalFocusModelKeys(antennaNameList, antennaNameListDim, receiverBandList, polarizationTypeList,&
      & calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalFocusModelTable','Error in sdm_getCalFocusModelKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%polarizationType = polarizationTypeList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalFocusModelKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalFocusModelRow(row, error)
  use sdm_CalFocusModel
  type(CalFocusModelRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalFocusModel'
  ! row%coeffName allocation
  if (allocated(row%coeffName)) then
    deallocate(row%coeffName, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffFormula allocation
  if (allocated(row%coeffFormula)) then
    deallocate(row%coeffFormula, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffValue allocation
  if (allocated(row%coeffValue)) then
    deallocate(row%coeffValue, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffError allocation
  if (allocated(row%coeffError)) then
    deallocate(row%coeffError, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%coeffFixed allocation
  if (allocated(row%coeffFixed)) then
    deallocate(row%coeffFixed, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%focusRMS allocation
  if (allocated(row%focusRMS)) then
    deallocate(row%focusRMS, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%coeffName(row%numCoeff), row%coeffFormula(row%numCoeff), row%coeffValue(row%numCoeff),&
      & row%coeffError(row%numCoeff), row%coeffFixed(row%numCoeff), row%focusRMS(3), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalFocusModelRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalGain Table:
!
! ===========================================================================
!
subroutine addCalGainRow(key, row, error)
  !
  use sdm_CalGain
  !
  type(CalGainRow) :: row
  type(CalGainKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalGainRow, i, j
  ! ----------------
  ! Deal with dimensions
  !
  ireturn = sdm_addCalGainRow(key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%gain,&
      & row%gainValid, row%fit, row%fitWeight, row%totalGainValid, row%totalFit, row%totalFitWeight)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalGainTable','Error in sdm_addCalGainRow', ireturn)
    error = .true.
  endif
end subroutine addCalGainRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalGainRow(key, row, error)
  !
  use sdm_CalGain
  !
  type(CalGainRow) :: row
  type(CalGainKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalGainRow, i
  ! Deal with dimensions
  !
  ireturn = sdm_getCalGainRow(key%calDataId, key%calReductionId, row%startValidTime, row%endValidTime, row%gain,&
      & row%gainValid, row%fit, row%fitWeight, row%totalGainValid, row%totalFit, row%totalFitWeight)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalGainTable','Error in sdm_getCalGainRow', ireturn)
    error = .true.
  endif
end subroutine getCalGainRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalGainTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalGainTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalGainTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalGainTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalGainKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalGain
  !
  integer :: tableSize
  logical :: error
  type(CalGainKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalGainKeys, i
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalGainKeys(calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalGainTable','Error in sdm_getCalGainKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalGainKeys
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalPrimaryBeam Table:
!
! ===========================================================================
!
subroutine addCalPrimaryBeamRow(key, row, error)
  !
  use sdm_CalPrimaryBeam
  !
  type(CalPrimaryBeamRow) :: row
  type(CalPrimaryBeamKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalPrimaryBeamRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: mainBeamEfficiencyRank = 1
  integer :: mainBeamEfficiencyDim(2,mainBeamEfficiencyRank)
  integer :: beamMapUIDDim(2)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%mainBeamEfficiency)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%mainBeamEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, mainBeamEfficiencyRank
    mainBeamEfficiencyDim(:,i) = size(row%mainBeamEfficiency,i)
  enddo
  beamMapUIDDim = len(row%beamMapUID)
  call charcut(row%beamMapUID)
  !
  ireturn = sdm_addCalPrimaryBeamRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%frequencyRange,&
      & frequencyRangeDim, row%numReceptor, row%polarizationTypes, polarizationTypesDim, row%mainBeamEfficiency,&
      & mainBeamEfficiencyDim, row%beamMapUID, beamMapUIDDim, row%relativeAmplitudeRms)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPrimaryBeamTable','Error in sdm_addCalPrimaryBeamRow', ireturn)
    error = .true.
  endif
end subroutine addCalPrimaryBeamRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPrimaryBeamRow(key, row, error)
  !
  use sdm_CalPrimaryBeam
  !
  type(CalPrimaryBeamRow) :: row
  type(CalPrimaryBeamKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalPrimaryBeamRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: frequencyRangeRank = 1
  integer :: frequencyRangeDim(2,frequencyRangeRank)
  integer, parameter :: polarizationTypesRank = 1
  integer :: polarizationTypesDim(2,polarizationTypesRank)
  integer, parameter :: mainBeamEfficiencyRank = 1
  integer :: mainBeamEfficiencyDim(2,mainBeamEfficiencyRank)
  integer :: beamMapUIDDim(2)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%frequencyRange)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%frequencyRange not allocated.')
    error = .true.
    return
  endif
  do i=1, frequencyRangeRank
    frequencyRangeDim(:,i) = size(row%frequencyRange,i)
  enddo
  if (.not.allocated(row%polarizationTypes)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%polarizationTypes not allocated.')
    error = .true.
    return
  endif
  do i=1, polarizationTypesRank
    polarizationTypesDim(:,i) = size(row%polarizationTypes,i)
  enddo
  if (.not.allocated(row%mainBeamEfficiency)) then
    call sdmMessage(8,3,'CalPrimaryBeamTable','row%mainBeamEfficiency not allocated.')
    error = .true.
    return
  endif
  do i=1, mainBeamEfficiencyRank
    mainBeamEfficiencyDim(:,i) = size(row%mainBeamEfficiency,i)
  enddo
  beamMapUIDDim = len(row%beamMapUID)
  call charcut(row%beamMapUID)
  !
  ireturn = sdm_getCalPrimaryBeamRow(key%antennaName, antennaNameDim, key%receiverBand, key%calDataId,&
      & key%calReductionId, row%startValidTime, row%endValidTime, row%antennaMake, row%frequencyRange,&
      & frequencyRangeDim, row%numReceptor, row%polarizationTypes, polarizationTypesDim, row%mainBeamEfficiency,&
      & mainBeamEfficiencyDim, row%beamMapUID, beamMapUIDDim, row%relativeAmplitudeRms)
  key%antennaName(antennaNameDim(1)+1:) = ''
  row%beamMapUID(beamMapUIDDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPrimaryBeamTable','Error in sdm_getCalPrimaryBeamRow', ireturn)
    error = .true.
  endif
end subroutine getCalPrimaryBeamRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalPrimaryBeamTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalPrimaryBeamTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalPrimaryBeamTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalPrimaryBeamTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalPrimaryBeamKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalPrimaryBeam
  !
  integer :: tableSize
  logical :: error
  type(CalPrimaryBeamKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalPrimaryBeamKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: receiverBandList(:)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(receiverBandList(tableSize))
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalPrimaryBeamKeys(antennaNameList, antennaNameListDim, receiverBandList, calDataIdList,&
      & calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalPrimaryBeamTable','Error in sdm_getCalPrimaryBeamKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%receiverBand = receiverBandList(i)
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalPrimaryBeamKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalPrimaryBeamRow(row, error)
  use sdm_CalPrimaryBeam
  type(CalPrimaryBeamRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalPrimaryBeam'
  ! row%frequencyRange allocation
  if (allocated(row%frequencyRange)) then
    deallocate(row%frequencyRange, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polarizationTypes allocation
  if (allocated(row%polarizationTypes)) then
    deallocate(row%polarizationTypes, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%mainBeamEfficiency allocation
  if (allocated(row%mainBeamEfficiency)) then
    deallocate(row%mainBeamEfficiency, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%frequencyRange(2), row%polarizationTypes(row%numReceptor), row%mainBeamEfficiency(row%numReceptor),&
      & stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalPrimaryBeamRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! CalWVR Table:
!
! ===========================================================================
!
subroutine addCalWVRRow(key, row, error)
  !
  use sdm_CalWVR
  !
  type(CalWVRRow) :: row
  type(CalWVRKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addCalWVRRow, i, j
  integer :: antennaNameDim(2)
  integer, parameter :: inputAntennaNamesRank = 1
  integer :: inputAntennaNamesDim(2,inputAntennaNamesRank+1)
  integer, parameter :: chanFreqRank = 1
  integer :: chanFreqDim(2,chanFreqRank)
  integer, parameter :: chanWidthRank = 1
  integer :: chanWidthDim(2,chanWidthRank)
  integer, parameter :: refTempRank = 2
  integer :: refTempDim(2,refTempRank)
  integer, parameter :: pathCoeffRank = 3
  integer :: pathCoeffDim(2,pathCoeffRank)
  integer, parameter :: polyFreqLimitsRank = 1
  integer :: polyFreqLimitsDim(2,polyFreqLimitsRank)
  integer, parameter :: wetPathRank = 1
  integer :: wetPathDim(2,wetPathRank)
  integer, parameter :: dryPathRank = 1
  integer :: dryPathDim(2,dryPathRank)
  ! ----------------
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%inputAntennaNames)) then
    call sdmMessage(8,3,'CalWVRTable','row%inputAntennaNames not allocated.')
    error = .true.
    return
  endif
  inputAntennaNamesDim(:,1) = len(row%inputAntennaNames)
  do i=1, inputAntennaNamesRank
    inputAntennaNamesDim(:,i+1) = size(row%inputAntennaNames,i)
  enddo
  do i=1, inputAntennaNamesDim(1,2)
    call charcut(row%inputAntennaNames(i))
  enddo
  if (.not.allocated(row%chanFreq)) then
    call sdmMessage(8,3,'CalWVRTable','row%chanFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, chanFreqRank
    chanFreqDim(:,i) = size(row%chanFreq,i)
  enddo
  if (.not.allocated(row%chanWidth)) then
    call sdmMessage(8,3,'CalWVRTable','row%chanWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, chanWidthRank
    chanWidthDim(:,i) = size(row%chanWidth,i)
  enddo
  if (.not.allocated(row%refTemp)) then
    call sdmMessage(8,3,'CalWVRTable','row%refTemp not allocated.')
    error = .true.
    return
  endif
  do i=1, refTempRank
    refTempDim(:,i) = size(row%refTemp,i)
  enddo
  if (.not.allocated(row%pathCoeff)) then
    call sdmMessage(8,3,'CalWVRTable','row%pathCoeff not allocated.')
    error = .true.
    return
  endif
  do i=1, pathCoeffRank
    pathCoeffDim(:,i) = size(row%pathCoeff,i)
  enddo
  if (.not.allocated(row%polyFreqLimits)) then
    call sdmMessage(8,3,'CalWVRTable','row%polyFreqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, polyFreqLimitsRank
    polyFreqLimitsDim(:,i) = size(row%polyFreqLimits,i)
  enddo
  if (.not.allocated(row%wetPath)) then
    call sdmMessage(8,3,'CalWVRTable','row%wetPath not allocated.')
    error = .true.
    return
  endif
  do i=1, wetPathRank
    wetPathDim(:,i) = size(row%wetPath,i)
  enddo
  if (.not.allocated(row%dryPath)) then
    call sdmMessage(8,3,'CalWVRTable','row%dryPath not allocated.')
    error = .true.
    return
  endif
  do i=1, dryPathRank
    dryPathDim(:,i) = size(row%dryPath,i)
  enddo
  !
  ireturn = sdm_addCalWVRRow(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%wvrMethod, row%numInputAntennas, row%inputAntennaNames, inputAntennaNamesDim,&
      & row%numChan, row%chanFreq, chanFreqDim, row%chanWidth, chanWidthDim, row%refTemp, refTempDim, row%numPoly,&
      & row%pathCoeff, pathCoeffDim, row%polyFreqLimits, polyFreqLimitsDim, row%wetPath, wetPathDim, row%dryPath,&
      & dryPathDim, row%water)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalWVRTable','Error in sdm_addCalWVRRow', ireturn)
    error = .true.
  endif
end subroutine addCalWVRRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalWVRRow(key, row, error)
  !
  use sdm_CalWVR
  !
  type(CalWVRRow) :: row
  type(CalWVRKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getCalWVRRow, i
  integer :: antennaNameDim(2)
  integer, parameter :: inputAntennaNamesRank = 1
  integer :: inputAntennaNamesDim(2,inputAntennaNamesRank+1)
  integer, parameter :: chanFreqRank = 1
  integer :: chanFreqDim(2,chanFreqRank)
  integer, parameter :: chanWidthRank = 1
  integer :: chanWidthDim(2,chanWidthRank)
  integer, parameter :: refTempRank = 2
  integer :: refTempDim(2,refTempRank)
  integer, parameter :: pathCoeffRank = 3
  integer :: pathCoeffDim(2,pathCoeffRank)
  integer, parameter :: polyFreqLimitsRank = 1
  integer :: polyFreqLimitsDim(2,polyFreqLimitsRank)
  integer, parameter :: wetPathRank = 1
  integer :: wetPathDim(2,wetPathRank)
  integer, parameter :: dryPathRank = 1
  integer :: dryPathDim(2,dryPathRank)
  ! Deal with dimensions
  antennaNameDim = len(key%antennaName)
  call charcut(key%antennaName)
  if (.not.allocated(row%inputAntennaNames)) then
    call sdmMessage(8,3,'CalWVRTable','row%inputAntennaNames not allocated.')
    error = .true.
    return
  endif
  inputAntennaNamesDim(:,1) = len(row%inputAntennaNames)
  do i=1, inputAntennaNamesRank
    inputAntennaNamesDim(:,i+1) = size(row%inputAntennaNames,i)
  enddo
  if (.not.allocated(row%chanFreq)) then
    call sdmMessage(8,3,'CalWVRTable','row%chanFreq not allocated.')
    error = .true.
    return
  endif
  do i=1, chanFreqRank
    chanFreqDim(:,i) = size(row%chanFreq,i)
  enddo
  if (.not.allocated(row%chanWidth)) then
    call sdmMessage(8,3,'CalWVRTable','row%chanWidth not allocated.')
    error = .true.
    return
  endif
  do i=1, chanWidthRank
    chanWidthDim(:,i) = size(row%chanWidth,i)
  enddo
  if (.not.allocated(row%refTemp)) then
    call sdmMessage(8,3,'CalWVRTable','row%refTemp not allocated.')
    error = .true.
    return
  endif
  do i=1, refTempRank
    refTempDim(:,i) = size(row%refTemp,i)
  enddo
  if (.not.allocated(row%pathCoeff)) then
    call sdmMessage(8,3,'CalWVRTable','row%pathCoeff not allocated.')
    error = .true.
    return
  endif
  do i=1, pathCoeffRank
    pathCoeffDim(:,i) = size(row%pathCoeff,i)
  enddo
  if (.not.allocated(row%polyFreqLimits)) then
    call sdmMessage(8,3,'CalWVRTable','row%polyFreqLimits not allocated.')
    error = .true.
    return
  endif
  do i=1, polyFreqLimitsRank
    polyFreqLimitsDim(:,i) = size(row%polyFreqLimits,i)
  enddo
  if (.not.allocated(row%wetPath)) then
    call sdmMessage(8,3,'CalWVRTable','row%wetPath not allocated.')
    error = .true.
    return
  endif
  do i=1, wetPathRank
    wetPathDim(:,i) = size(row%wetPath,i)
  enddo
  if (.not.allocated(row%dryPath)) then
    call sdmMessage(8,3,'CalWVRTable','row%dryPath not allocated.')
    error = .true.
    return
  endif
  do i=1, dryPathRank
    dryPathDim(:,i) = size(row%dryPath,i)
  enddo
  !
  ireturn = sdm_getCalWVRRow(key%antennaName, antennaNameDim, key%calDataId, key%calReductionId, row%startValidTime,&
      & row%endValidTime, row%wvrMethod, row%numInputAntennas, row%inputAntennaNames, inputAntennaNamesDim,&
      & row%numChan, row%chanFreq, chanFreqDim, row%chanWidth, chanWidthDim, row%refTemp, refTempDim, row%numPoly,&
      & row%pathCoeff, pathCoeffDim, row%polyFreqLimits, polyFreqLimitsDim, row%wetPath, wetPathDim, row%dryPath,&
      & dryPathDim, row%water)
  key%antennaName(antennaNameDim(1)+1:) = ''
  do i=1,inputAntennaNamesDim(1,2)
    row%inputAntennaNames(i)(inputAntennaNamesDim(1,1)+1:) = ''
  enddo
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalWVRTable','Error in sdm_getCalWVRRow', ireturn)
    error = .true.
  endif
end subroutine getCalWVRRow
!
! ---------------------------------------------------------------------------
!
subroutine getCalWVRTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getCalWVRTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getCalWVRTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getCalWVRTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getCalWVRKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_CalWVR
  !
  integer :: tableSize
  logical :: error
  type(CalWVRKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getCalWVRKeys, i
  character*256, allocatable :: antennaNameList(:)
  integer :: antennaNameListDim(2,2)
  integer, allocatable :: calDataIdList(:)
  integer, allocatable :: calReductionIdList(:)
  !
  allocate(antennaNameList(tableSize))
  antennaNameListDim = len(keyList%antennaName)
  allocate(calDataIdList(tableSize))
  allocate(calReductionIdList(tableSize))
  !
  ireturn = sdm_getCalWVRKeys(antennaNameList, antennaNameListDim, calDataIdList, calReductionIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'CalWVRTable','Error in sdm_getCalWVRKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaName = antennaNameList(i)(1:antennaNameListDim(1,1))
    keyList(i)%calDataId = calDataIdList(i)
    keyList(i)%calReductionId = calReductionIdList(i)
  enddo
end subroutine getCalWVRKeys
! ---------------------------------------------------------------------------
!
subroutine allocCalWVRRow(row, error)
  use sdm_CalWVR
  type(CalWVRRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'CalWVR'
  ! row%inputAntennaNames allocation
  if (allocated(row%inputAntennaNames)) then
    deallocate(row%inputAntennaNames, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%chanFreq allocation
  if (allocated(row%chanFreq)) then
    deallocate(row%chanFreq, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%chanWidth allocation
  if (allocated(row%chanWidth)) then
    deallocate(row%chanWidth, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%refTemp allocation
  if (allocated(row%refTemp)) then
    deallocate(row%refTemp, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%pathCoeff allocation
  if (allocated(row%pathCoeff)) then
    deallocate(row%pathCoeff, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%polyFreqLimits allocation
  if (allocated(row%polyFreqLimits)) then
    deallocate(row%polyFreqLimits, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%wetPath allocation
  if (allocated(row%wetPath)) then
    deallocate(row%wetPath, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%dryPath allocation
  if (allocated(row%dryPath)) then
    deallocate(row%dryPath, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%inputAntennaNames(row%numInputAntennas), row%chanFreq(row%numChan), row%chanWidth(row%numChan),&
      & row%refTemp(row%numChan, row%numInputAntennas), row%pathCoeff(row%numPoly, row%numChan, row%numInputAntennas),&
      & row%polyFreqLimits(2), row%wetPath(row%numPoly), row%dryPath(row%numPoly), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocCalWVRRow
!
! ---------------------------------------------------------------------------
!
! ===========================================================================
!
! Annotation Table:
!
! ===========================================================================
!
subroutine addAnnotationRow(key, row, error)
  !
  use sdm_Annotation
  !
  type(AnnotationRow) :: row
  type(AnnotationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addAnnotationRow, i, j
  integer :: issueDim(2)
  integer :: detailsDim(2)
  ! ----------------
  ! Deal with dimensions
  issueDim = len(row%issue)
  call charcut(row%issue)
  detailsDim = len(row%details)
  call charcut(row%details)
  !
  ireturn = sdm_addAnnotationRow(row%time, row%issue, issueDim, row%details, detailsDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationRow', ireturn)
    error = .true.
  else
    key%annotationId = ireturn
  endif
end subroutine addAnnotationRow
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationRow(key, row, error)
  !
  use sdm_Annotation
  !
  type(AnnotationRow) :: row
  type(AnnotationKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getAnnotationRow, i
  integer :: issueDim(2)
  integer :: detailsDim(2)
  ! Deal with dimensions
  issueDim = len(row%issue)
  call charcut(row%issue)
  detailsDim = len(row%details)
  call charcut(row%details)
  !
  ireturn = sdm_getAnnotationRow(key%annotationId, row%time, row%issue, issueDim, row%details, detailsDim)
  row%issue(issueDim(1)+1:) = ''
  row%details(detailsDim(1)+1:) = ''
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationRow', ireturn)
    error = .true.
  endif
end subroutine getAnnotationRow
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getAnnotationTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getAnnotationTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getAnnotationTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_Annotation
  !
  integer :: tableSize
  logical :: error
  type(AnnotationKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getAnnotationKeys, i
  integer, allocatable :: annotationIdList(:)
  !
  allocate(annotationIdList(tableSize))
  !
  ireturn = sdm_getAnnotationKeys(annotationIdList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%annotationId = annotationIdList(i)
  enddo
end subroutine getAnnotationKeys
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationNumAntenna(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationNumAntenna, i
  !! integer :: numAntenna
  ! Deal with dimensions
  ireturn = sdm_addAnnotationNumAntenna(key%annotationId, opt%numAntenna)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationNumAntenna', ireturn)
    error = .true.
  endif
end subroutine addAnnotationNumAntenna
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationNumAntenna(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationNumAntenna, i
  ! Deal with dimensions
  ireturn = sdm_getAnnotationNumAntenna(key%annotationId, opt%numAntenna)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationNumAntenna', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationNumAntenna
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationBasebandName(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationBasebandName, i
  !! integer, allocatable :: basebandName(:)
  integer, parameter :: basebandNameRank = 1
  integer :: basebandNameDim(2,basebandNameRank)
  ! Deal with dimensions
  if (.not.allocated(opt%basebandName)) then
    call sdmMessage(8,3,'AnnotationTable','opt%basebandName not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandNameRank
    basebandNameDim(:,i) = size(opt%basebandName,i)
  enddo
  ireturn = sdm_addAnnotationBasebandName(key%annotationId, opt%basebandName, basebandNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationBasebandName', ireturn)
    error = .true.
  endif
end subroutine addAnnotationBasebandName
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationBasebandName(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationBasebandName, i
  integer, parameter :: basebandNameRank = 1
  integer :: basebandNameDim(2,basebandNameRank)
  ! Deal with dimensions
  if (.not.allocated(opt%basebandName)) then
    call sdmMessage(8,3,'AnnotationTable','opt%basebandName not allocated.')
    error = .true.
    return
  endif
  do i=1, basebandNameRank
    basebandNameDim(:,i) = size(opt%basebandName,i)
  enddo
  ireturn = sdm_getAnnotationBasebandName(key%annotationId, opt%basebandName, basebandNameDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationBasebandName', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationBasebandName
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationNumBaseband(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationNumBaseband, i
  !! integer :: numBaseband
  ! Deal with dimensions
  ireturn = sdm_addAnnotationNumBaseband(key%annotationId, opt%numBaseband)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationNumBaseband', ireturn)
    error = .true.
  endif
end subroutine addAnnotationNumBaseband
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationNumBaseband(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationNumBaseband, i
  ! Deal with dimensions
  ireturn = sdm_getAnnotationNumBaseband(key%annotationId, opt%numBaseband)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationNumBaseband', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationNumBaseband
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationInterval(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationInterval, i
  !! integer*8 :: interval
  ! Deal with dimensions
  ireturn = sdm_addAnnotationInterval(key%annotationId, opt%interval)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationInterval', ireturn)
    error = .true.
  endif
end subroutine addAnnotationInterval
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationInterval(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationInterval, i
  ! Deal with dimensions
  ireturn = sdm_getAnnotationInterval(key%annotationId, opt%interval)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationInterval', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationInterval
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationDValue(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationDValue, i
  !! real*8 :: dValue
  ! Deal with dimensions
  ireturn = sdm_addAnnotationDValue(key%annotationId, opt%dValue)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationDValue', ireturn)
    error = .true.
  endif
end subroutine addAnnotationDValue
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationDValue(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationDValue, i
  ! Deal with dimensions
  ireturn = sdm_getAnnotationDValue(key%annotationId, opt%dValue)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationDValue', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationDValue
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationVdValue(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationVdValue, i
  !! real*8, allocatable :: vdValue(:)
  integer, parameter :: vdValueRank = 1
  integer :: vdValueDim(2,vdValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vdValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vdValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vdValueRank
    vdValueDim(:,i) = size(opt%vdValue,i)
  enddo
  ireturn = sdm_addAnnotationVdValue(key%annotationId, opt%vdValue, vdValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationVdValue', ireturn)
    error = .true.
  endif
end subroutine addAnnotationVdValue
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationVdValue(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationVdValue, i
  integer, parameter :: vdValueRank = 1
  integer :: vdValueDim(2,vdValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vdValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vdValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vdValueRank
    vdValueDim(:,i) = size(opt%vdValue,i)
  enddo
  ireturn = sdm_getAnnotationVdValue(key%annotationId, opt%vdValue, vdValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationVdValue', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationVdValue
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationVvdValues(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationVvdValues, i
  !! real*8, allocatable :: vvdValues(:,:)
  integer, parameter :: vvdValuesRank = 2
  integer :: vvdValuesDim(2,vvdValuesRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vvdValues)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vvdValues not allocated.')
    error = .true.
    return
  endif
  do i=1, vvdValuesRank
    vvdValuesDim(:,i) = size(opt%vvdValues,i)
  enddo
  ireturn = sdm_addAnnotationVvdValues(key%annotationId, opt%vvdValues, vvdValuesDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationVvdValues', ireturn)
    error = .true.
  endif
end subroutine addAnnotationVvdValues
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationVvdValues(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationVvdValues, i
  integer, parameter :: vvdValuesRank = 2
  integer :: vvdValuesDim(2,vvdValuesRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vvdValues)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vvdValues not allocated.')
    error = .true.
    return
  endif
  do i=1, vvdValuesRank
    vvdValuesDim(:,i) = size(opt%vvdValues,i)
  enddo
  ireturn = sdm_getAnnotationVvdValues(key%annotationId, opt%vvdValues, vvdValuesDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationVvdValues', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationVvdValues
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationLlValue(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationLlValue, i
  !! integer*8 :: llValue
  ! Deal with dimensions
  ireturn = sdm_addAnnotationLlValue(key%annotationId, opt%llValue)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationLlValue', ireturn)
    error = .true.
  endif
end subroutine addAnnotationLlValue
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationLlValue(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationLlValue, i
  ! Deal with dimensions
  ireturn = sdm_getAnnotationLlValue(key%annotationId, opt%llValue)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationLlValue', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationLlValue
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationVllValue(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationVllValue, i
  !! integer*8, allocatable :: vllValue(:)
  integer, parameter :: vllValueRank = 1
  integer :: vllValueDim(2,vllValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vllValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vllValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vllValueRank
    vllValueDim(:,i) = size(opt%vllValue,i)
  enddo
  ireturn = sdm_addAnnotationVllValue(key%annotationId, opt%vllValue, vllValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationVllValue', ireturn)
    error = .true.
  endif
end subroutine addAnnotationVllValue
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationVllValue(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationVllValue, i
  integer, parameter :: vllValueRank = 1
  integer :: vllValueDim(2,vllValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vllValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vllValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vllValueRank
    vllValueDim(:,i) = size(opt%vllValue,i)
  enddo
  ireturn = sdm_getAnnotationVllValue(key%annotationId, opt%vllValue, vllValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationVllValue', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationVllValue
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationVvllValue(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationVvllValue, i
  !! integer*8, allocatable :: vvllValue(:,:)
  integer, parameter :: vvllValueRank = 2
  integer :: vvllValueDim(2,vvllValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vvllValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vvllValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vvllValueRank
    vvllValueDim(:,i) = size(opt%vvllValue,i)
  enddo
  ireturn = sdm_addAnnotationVvllValue(key%annotationId, opt%vvllValue, vvllValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationVvllValue', ireturn)
    error = .true.
  endif
end subroutine addAnnotationVvllValue
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationVvllValue(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationVvllValue, i
  integer, parameter :: vvllValueRank = 2
  integer :: vvllValueDim(2,vvllValueRank)
  ! Deal with dimensions
  if (.not.allocated(opt%vvllValue)) then
    call sdmMessage(8,3,'AnnotationTable','opt%vvllValue not allocated.')
    error = .true.
    return
  endif
  do i=1, vvllValueRank
    vvllValueDim(:,i) = size(opt%vvllValue,i)
  enddo
  ireturn = sdm_getAnnotationVvllValue(key%annotationId, opt%vvllValue, vvllValueDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationVvllValue', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationVvllValue
!
!
! ---------------------------------------------------------------------------
!
subroutine addAnnotationAntennaId(key, opt, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addAnnotationAntennaId, i
  !! integer, allocatable :: antennaId(:)
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%antennaId)) then
    call sdmMessage(8,3,'AnnotationTable','opt%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(opt%antennaId,i)
  enddo
  ireturn = sdm_addAnnotationAntennaId(key%annotationId, opt%antennaId, antennaIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_addAnnotationAntennaId', ireturn)
    error = .true.
  endif
end subroutine addAnnotationAntennaId
!
! ---------------------------------------------------------------------------
!
subroutine getAnnotationAntennaId(key, opt, present, error)
  !
  use sdm_Annotation
  !
  type(AnnotationKey) :: key
  type(AnnotationOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getAnnotationAntennaId, i
  integer, parameter :: antennaIdRank = 1
  integer :: antennaIdDim(2,antennaIdRank)
  ! Deal with dimensions
  if (.not.allocated(opt%antennaId)) then
    call sdmMessage(8,3,'AnnotationTable','opt%antennaId not allocated.')
    error = .true.
    return
  endif
  do i=1, antennaIdRank
    antennaIdDim(:,i) = size(opt%antennaId,i)
  enddo
  ireturn = sdm_getAnnotationAntennaId(key%annotationId, opt%antennaId, antennaIdDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'AnnotationTable','Error in sdm_getAnnotationAntennaId', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getAnnotationAntennaId
!
!
! ===========================================================================
!
! DelayModel Table:
!
! ===========================================================================
!
subroutine addDelayModelRow(key, row, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelRow) :: row
  type(DelayModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_addDelayModelRow, i, j
  integer, parameter :: atmDryDelayRank = 1
  integer :: atmDryDelayDim(2,atmDryDelayRank)
  integer, parameter :: atmWetDelayRank = 1
  integer :: atmWetDelayDim(2,atmWetDelayRank)
  integer, parameter :: clockDelayRank = 1
  integer :: clockDelayDim(2,clockDelayRank)
  integer, parameter :: geomDelayRank = 1
  integer :: geomDelayDim(2,geomDelayRank)
  ! ----------------
  ! Deal with dimensions
  if (.not.allocated(row%atmDryDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%atmDryDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, atmDryDelayRank
    atmDryDelayDim(:,i) = size(row%atmDryDelay,i)
  enddo
  if (.not.allocated(row%atmWetDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%atmWetDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, atmWetDelayRank
    atmWetDelayDim(:,i) = size(row%atmWetDelay,i)
  enddo
  if (.not.allocated(row%clockDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%clockDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, clockDelayRank
    clockDelayDim(:,i) = size(row%clockDelay,i)
  enddo
  if (.not.allocated(row%geomDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%geomDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, geomDelayRank
    geomDelayDim(:,i) = size(row%geomDelay,i)
  enddo
  !
  ireturn = sdm_addDelayModelRow(key%antennaId, key%timeInterval, row%timeOrigin, row%numPoly, row%atmDryDelay,&
      & atmDryDelayDim, row%atmWetDelay, atmWetDelayDim, row%clockDelay, clockDelayDim, row%geomDelay, geomDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_addDelayModelRow', ireturn)
    error = .true.
  endif
end subroutine addDelayModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelRow(key, row, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelRow) :: row
  type(DelayModelKey) :: key
  logical error
  !
  integer :: ireturn, sdm_getDelayModelRow, i
  integer, parameter :: atmDryDelayRank = 1
  integer :: atmDryDelayDim(2,atmDryDelayRank)
  integer, parameter :: atmWetDelayRank = 1
  integer :: atmWetDelayDim(2,atmWetDelayRank)
  integer, parameter :: clockDelayRank = 1
  integer :: clockDelayDim(2,clockDelayRank)
  integer, parameter :: geomDelayRank = 1
  integer :: geomDelayDim(2,geomDelayRank)
  ! Deal with dimensions
  if (.not.allocated(row%atmDryDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%atmDryDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, atmDryDelayRank
    atmDryDelayDim(:,i) = size(row%atmDryDelay,i)
  enddo
  if (.not.allocated(row%atmWetDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%atmWetDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, atmWetDelayRank
    atmWetDelayDim(:,i) = size(row%atmWetDelay,i)
  enddo
  if (.not.allocated(row%clockDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%clockDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, clockDelayRank
    clockDelayDim(:,i) = size(row%clockDelay,i)
  enddo
  if (.not.allocated(row%geomDelay)) then
    call sdmMessage(8,3,'DelayModelTable','row%geomDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, geomDelayRank
    geomDelayDim(:,i) = size(row%geomDelay,i)
  enddo
  !
  ireturn = sdm_getDelayModelRow(key%antennaId, key%timeInterval, row%timeOrigin, row%numPoly, row%atmDryDelay,&
      & atmDryDelayDim, row%atmWetDelay, atmWetDelayDim, row%clockDelay, clockDelayDim, row%geomDelay, geomDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_getDelayModelRow', ireturn)
    error = .true.
  endif
end subroutine getDelayModelRow
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelTableSize(tableSize, error)
!
! return the size of the table
!
  integer :: tableSize, sdm_getDelayModelTableSize, ireturn
  logical :: error
  !
  ireturn = sdm_getDelayModelTableSize()
  tableSize = ireturn
  if (ireturn.lt.0) error = .true.
end subroutine getDelayModelTableSize
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelKeys(tableSize, keyList, error)
!
! return the list of keys
!
  use sdm_DelayModel
  !
  integer :: tableSize
  logical :: error
  type(DelayModelKey) :: keyList(tableSize)
  integer :: ireturn, sdm_getDelayModelKeys, i
  integer, allocatable :: antennaIdList(:)
  type(ArrayTimeInterval), allocatable :: timeIntervalList(:)
  !
  allocate(antennaIdList(tableSize))
  allocate(timeIntervalList(tableSize))
  !
  ireturn = sdm_getDelayModelKeys(antennaIdList, timeIntervalList)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_getDelayModelKeys', ireturn)
    error = .true.
    return
  endif
  do i=1, tableSize
    keyList(i)%antennaId = antennaIdList(i)
    keyList(i)%timeInterval = timeIntervalList(i)
  enddo
end subroutine getDelayModelKeys
! ---------------------------------------------------------------------------
!
subroutine allocDelayModelRow(row, error)
  use sdm_DelayModel
  type(DelayModelRow) :: row
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32= 'DelayModel'
  ! row%atmDryDelay allocation
  if (allocated(row%atmDryDelay)) then
    deallocate(row%atmDryDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%atmWetDelay allocation
  if (allocated(row%atmWetDelay)) then
    deallocate(row%atmWetDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%clockDelay allocation
  if (allocated(row%clockDelay)) then
    deallocate(row%clockDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! row%geomDelay allocation
  if (allocated(row%geomDelay)) then
    deallocate(row%geomDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( row%atmDryDelay(row%numPoly), row%atmWetDelay(row%numPoly), row%clockDelay(row%numPoly),&
      & row%geomDelay(row%numPoly), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocDelayModelRow
!
! ---------------------------------------------------------------------------
!
subroutine allocDelayModelOpt(row, opt, error)
  use sdm_DelayModel
  type(DelayModelRow) :: row
  type(DelayModelOpt) :: opt
  logical :: error
  integer :: ier
  character, parameter :: sdmTable*32 = 'DelayModel'
  ! opt%dispDelay allocation
  if (allocated(opt%dispDelay)) then
    deallocate(opt%dispDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%groupDelay allocation
  if (allocated(opt%groupDelay)) then
    deallocate(opt%groupDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  ! opt%phaseDelay allocation
  if (allocated(opt%phaseDelay)) then
    deallocate(opt%phaseDelay, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate( opt%dispDelay(row%numPoly),  opt%groupDelay(row%numPoly),  opt%phaseDelay(row%numPoly), stat=ier)
  if (ier.ne.0) then
    call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
    error = .true.
  endif
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Opt. Deallocation error ier ',ier)
  error = .true.
  return
endsubroutine allocDelayModelOpt
! ---------------------------------------------------------------------------
!
! ---------------------------------------------------------------------------
!
subroutine addDelayModelDispDelay(key, opt, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addDelayModelDispDelay, i
  !! real*8, allocatable :: dispDelay(:)
  integer, parameter :: dispDelayRank = 1
  integer :: dispDelayDim(2,dispDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%dispDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%dispDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, dispDelayRank
    dispDelayDim(:,i) = size(opt%dispDelay,i)
  enddo
  ireturn = sdm_addDelayModelDispDelay(key%antennaId, key%timeInterval, opt%dispDelay, dispDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_addDelayModelDispDelay', ireturn)
    error = .true.
  endif
end subroutine addDelayModelDispDelay
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelDispDelay(key, opt, present, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getDelayModelDispDelay, i
  integer, parameter :: dispDelayRank = 1
  integer :: dispDelayDim(2,dispDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%dispDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%dispDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, dispDelayRank
    dispDelayDim(:,i) = size(opt%dispDelay,i)
  enddo
  ireturn = sdm_getDelayModelDispDelay(key%antennaId, key%timeInterval, opt%dispDelay, dispDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_getDelayModelDispDelay', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getDelayModelDispDelay
!
!
! ---------------------------------------------------------------------------
!
subroutine addDelayModelGroupDelay(key, opt, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addDelayModelGroupDelay, i
  !! real*8, allocatable :: groupDelay(:)
  integer, parameter :: groupDelayRank = 1
  integer :: groupDelayDim(2,groupDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%groupDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%groupDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, groupDelayRank
    groupDelayDim(:,i) = size(opt%groupDelay,i)
  enddo
  ireturn = sdm_addDelayModelGroupDelay(key%antennaId, key%timeInterval, opt%groupDelay, groupDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_addDelayModelGroupDelay', ireturn)
    error = .true.
  endif
end subroutine addDelayModelGroupDelay
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelGroupDelay(key, opt, present, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getDelayModelGroupDelay, i
  integer, parameter :: groupDelayRank = 1
  integer :: groupDelayDim(2,groupDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%groupDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%groupDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, groupDelayRank
    groupDelayDim(:,i) = size(opt%groupDelay,i)
  enddo
  ireturn = sdm_getDelayModelGroupDelay(key%antennaId, key%timeInterval, opt%groupDelay, groupDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_getDelayModelGroupDelay', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getDelayModelGroupDelay
!
!
! ---------------------------------------------------------------------------
!
subroutine addDelayModelPhaseDelay(key, opt, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error
  !
  integer :: ireturn, sdm_addDelayModelPhaseDelay, i
  !! real*8, allocatable :: phaseDelay(:)
  integer, parameter :: phaseDelayRank = 1
  integer :: phaseDelayDim(2,phaseDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%phaseDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDelayRank
    phaseDelayDim(:,i) = size(opt%phaseDelay,i)
  enddo
  ireturn = sdm_addDelayModelPhaseDelay(key%antennaId, key%timeInterval, opt%phaseDelay, phaseDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_addDelayModelPhaseDelay', ireturn)
    error = .true.
  endif
end subroutine addDelayModelPhaseDelay
!
! ---------------------------------------------------------------------------
!
subroutine getDelayModelPhaseDelay(key, opt, present, error)
  !
  use sdm_DelayModel
  !
  type(DelayModelKey) :: key
  type(DelayModelOpt) :: opt
  logical :: error, present
  !
  integer :: ireturn, sdm_getDelayModelPhaseDelay, i
  integer, parameter :: phaseDelayRank = 1
  integer :: phaseDelayDim(2,phaseDelayRank)
  ! Deal with dimensions
  if (.not.allocated(opt%phaseDelay)) then
    call sdmMessage(8,3,'DelayModelTable','opt%phaseDelay not allocated.')
    error = .true.
    return
  endif
  do i=1, phaseDelayRank
    phaseDelayDim(:,i) = size(opt%phaseDelay,i)
  enddo
  ireturn = sdm_getDelayModelPhaseDelay(key%antennaId, key%timeInterval, opt%phaseDelay, phaseDelayDim)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,'DelayModelTable','Error in sdm_getDelayModelPhaseDelay', ireturn)
    error = .true.
  else
    present = ireturn.eq.0
  endif
end subroutine getDelayModelPhaseDelay
!
