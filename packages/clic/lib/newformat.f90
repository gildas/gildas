subroutine newformat(nd,d)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     convert the data section to new format (spectral data averaged).
  ! Arguments:
  !     ND      INTEGER    Data length (Input/Output)
  !     D       INTEGER    Data address
  !---------------------------------------------------------------------
  integer :: nd                      !
  integer(kind=address_length) :: d  !
  ! Global
  include 'clic_parameter.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: inbuf, outbuf, addr
  integer :: size, ier
  !
  data size/0/
  save size
  !
  if (size.eq.0) then
    size = mdatac+mdatal
    ier = sic_getvm(2*size,addr)
    inbuf = gag_pointer(addr,memory)
    outbuf = inbuf + size
  endif
  call sub_newformat(memory(inbuf),memory(outbuf),nd,d)
end subroutine newformat
!------------------------------------------------------------------------
!
subroutine sub_newformat(inbuf,outbuf,nd,d)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC
  !     convert the data section to new format (spectral data averaged).
  ! Arguments:
  !     ND      INTEGER    Data length (Input/Output)
  !     D       INTEGER    Data address
  !---------------------------------------------------------------------
  include 'clic_parameter.inc'
  real :: inbuf(mdatac+mdatal)       !
  real :: outbuf(mdatac+mdatal)      !
  integer :: nd                      !
  integer(kind=address_length) :: d  !
  ! Global
  include 'clic_par.inc'
  include 'clic_proc_par.inc'
  include 'clic_dheader.inc'
  include 'clic_dcomp.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipk, ipo
  integer :: i, ir, nout
  integer :: ndat, t_ldatl, t_ldatc
  logical :: corr
  character(len=20) :: chain
  !------------------------------------------------------------------------
  ! Code:
  corr = r_proc.ne.p_cal   &
    .and. r_proc.ne.p_skydip .and. r_proc.ne.p_onoff   &
    .and. r_proc.ne.p_sky
  do i=1, r_lband
    r_lcench(i) = 1            ! for berkeley
  enddo
  if (r_proc.eq.p_pass .and. r_ndump.gt.1) then
    ipk = gag_pointer(d,memory)
    ipk = ipk+r_ldump          ! ignore first record
    r_ndump = 1
  else
    ipk = gag_pointer(d,memory)
  endif
  ipo = gag_pointer(d,memory)
  nout = 0
  if (corr) then
    t_ldatc = r_ldatc
    t_ldatl = r_ldatl
  else
    t_ldatc = r_nband*r_nant
    t_ldatl = r_lntch*r_nant
  endif
  ndat = t_ldatc+t_ldatl
  ! Loop on records
  if (corr) then
    r_lmode = 1
  else
    r_lmode = 2
  endif
  call zero_record(ndat,outbuf)
  do ir = 1, r_ndump
    ! Data header
    call r4tor4 (memory(ipk),memory(ipo),r_ldpar)
    call decode_header(memory(ipk))
    ipk = ipk + r_ldpar
    ipo = ipo + r_ldpar
    ! Continuum
    call r4tor4 (memory(ipk),datac,r_ldatc)
    if (corr) then
      call cmodec(r_nband,r_nbas,datac,memory(ipo))
    else
      call amodec(r_nband,r_nbas,datac,r_nant,memory(ipo))
    endif
    call r4tor4 (memory(ipo),inbuf,t_ldatc)    ! save for averaging
    ipk = ipk + r_ldatc
    ipo = ipo + t_ldatc
    ! Line
    if (corr) then
      call r4tor4(memory(ipk),inbuf(t_ldatc+1),r_ldatl)
    else
      call amodel(r_lntch,r_nbas,memory(ipk),   &
        r_nant,inbuf(t_ldatc+1))
    endif
    ipk = ipk + r_ldatl
    ! Increment
    if (dh_dump.eq.ir) then
      call add_record (ndat,inbuf,outbuf)
    else
      write(chain,'(i6)') ir
      call message(1,1,'NEWFORMAT','rec. '//chain(1:lenc(chain))   &
        //' is bad')
    endif
  enddo
  ipo = ipo + r_ldpar
  call output_record (ndat, memory(ipo), outbuf, nout)
  ipo = ipo - r_ldpar
  call r4tor4 (dcomp_void, dh_void, m_dh)
  call encode_header (memory(ipo))
  if (corr) then
    r_lmode = 1
  else
    r_ldatc = t_ldatc
    r_ldatl = t_ldatl
    r_lmode = 2
  endif
  r_ldump = r_ldpar + r_ldatc  ! new record length
  nd = r_ldump*r_ndump+r_ldpar+r_ldatc+r_ldatl
  write(chain,'(i6)') r_xnum
  call message(1,1,'NEWFORMAT','obs. '//chain(1:lenc(chain))   &
    //' converted')
end subroutine sub_newformat
!
subroutine amodec(nl,nb,cin,na,rout)
  !---------------------------------------------------------------------
  ! convert from old data order for continuum, autocorrelations
  !
  !	NL       INTEGER     Number of channels
  !       NB       INTEGER     Number of baselines
  !       NA       INTEGER     Number of antennas
  !       CIN(2,NL,NB) COMPLEX Input pseudo-complex autocorrelation data
  !       ROUT(NL,NA)  REAL    Output autocorrelation data
  !---------------------------------------------------------------------
  integer :: nl                     !
  integer :: nb                     !
  complex :: cin(2, nl, nb)         !
  integer :: na                     !
  real :: rout(nl, na)              !
  ! Local
  integer :: iant(3), jant(3)
  integer :: ib, ic, ia, ja
  !
  data iant/1,1,2/, jant/2,3,3/
  !------------------------------------------------------------------------
  ! Code:
  do ia = 1, na
    do ic=1, nl
      rout(ic, ia) = 0
    enddo
  enddo
  do ib=1, nb
    ia = iant(ib)
    ja = jant(ib)
    do ic=1, nl
      if (ia.le.na .and. rout(ic,ia).eq.0)   &
        rout(ic,ia) = real(cin(1,ic,ib))
      if (ja.le.na .and. rout(ic,ja).eq.0)   &
        rout(ic,ja) = real(cin(2,ic,ib))
    enddo
  enddo
end subroutine amodec
!
subroutine amodel(nl,nb,cin,na,rout)
  !---------------------------------------------------------------------
  ! convert from old data order for line, autocorrelations
  !
  !	NL       INTEGER     Number of channels
  !       NB       INTEGER     Number of baselines
  !       NA       INTEGER     Number of antennas
  !       CIN(2,NL,NB) COMPLEX Input pseudo-complex autocorrelation data
  !       ROUT(NL,NA)  REAL    Output autocorrelation data
  !---------------------------------------------------------------------
  integer :: nl                     !
  integer :: nb                     !
  complex :: cin(nl, 2, nb)         !
  integer :: na                     !
  real :: rout(nl, na)              !
  ! Local
  integer :: iant(3), jant(3)
  integer :: ib, ic, ia, ja
  !
  data iant/1,1,2/, jant/2,3,3/
  !------------------------------------------------------------------------
  ! Code:
  do ia = 1, na
    do ic=1, nl
      rout(ic, ia) = 0
    enddo
  enddo
  do ib=1, nb
    ia = iant(ib)
    ja = jant(ib)
    do ic=1, nl
      if (ia.le.na .and. rout(ic,ia).eq.0)   &
        rout(ic,ia) = real(cin(ic,1,ib))
      if (ja.le.na .and. rout(ic,ja).eq.0)   &
        rout(ic,ja) = real(cin(ic,2,ib))
    enddo
  enddo
end subroutine amodel
!
subroutine cmodec(nl,nb,cin,cout)
  !---------------------------------------------------------------------
  ! convert from old data order for continuum, correlations
  !
  !	NL       INTEGER     Number of channels
  !       NB       INTEGER     Number of baselines
  !       CIN(2,NL,NB)  COMPLEX Input complex correlation data
  !       COUT(NL,2,NB) COMPLEX Output correlation data
  !---------------------------------------------------------------------
  integer :: nl                     !
  integer :: nb                     !
  complex :: cin(2,nl,nb)           !
  complex :: cout(nl,2,nb)          !
  ! Local
  integer :: ib, ic
  !------------------------------------------------------------------------
  ! Code:
  do ib=1, nb
    do ic=1, nl
      cout(ic,1,ib) = cin(1,ic,ib)
      cout(ic,2,ib) = cin(2,ic,ib)
    enddo
  enddo
end subroutine cmodec
