subroutine read_index(file,error)
  use classic_types
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in observation title v2 from buffer (whatever version)
  !-----------------------------------------------------------------------
  type(classic_file_t), intent(in) :: file
  logical, intent(inout)           :: error
  ! Code
  if    (file%desc%vind.eq.vind_v1) then
    call index_fromv1(file%conv%code,error)
  elseif(file%desc%vind.eq.vind_v2) then
    call index_fromv2(file%conv%code,error)
  else
    call message(8,3,'INDEX','Unsupported index version')
    error = .true.
    return
  endif
end subroutine read_index
!
subroutine write_index(file,error)
  use classic_types
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in buffer (whatever version) from observation title v2
  !-----------------------------------------------------------------------
  type(classic_file_t), intent(in) :: file
  logical, intent(inout)           :: error
  ! Local
  type(title_v1_t) :: title_v1
  ! Code
  if    (file%desc%vind.eq.vind_v1) then
    call index_tov1(file%conv%code,error)
  elseif(file%desc%vind.eq.vind_v2) then
    call index_tov2(file%conv%code,error)
  else
    call message(8,3,'INDEX','Unsupported index version')
    error = .true.
    return
  endif
end subroutine write_index
!
subroutine index_fromv1(conv,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_convert
  use classic_api
  use clic_file
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in observation title v2 from v1 buffer
  !-----------------------------------------------------------------------
  integer(kind=4),intent(in)  :: conv
  logical, intent(inout)      :: error
  ! Local
  type(title_v1_t) :: title_v1
  ! Code
  if (conv.eq.0) then
    call r4tor4(tbuf(1)   ,title_v1%bloc,   32)
  elseif (conv.eq.vax_to_ieee) then
    call r4tor4(tbuf(1)   ,title_v1%bloc,   32)
    call var4ie(tbuf(15)  ,title_v1%off1,    2)
    call var4ie(tbuf(23)  ,title_v1%houra,   1)
    call var4ie(tbuf(29)  ,title_v1%ut,      1)
  elseif (conv.eq.ieee_to_vax) then
    call r4tor4(tbuf(1)   ,title_v1%bloc,   32)
    call ier4va(tbuf(15)  ,title_v1%off1,    2)
    call ier4va(tbuf(23)  ,title_v1%houra,   1)
    call ier4va(tbuf(29)  ,title_v1%ut,      1)
  elseif (conv.eq.vax_to_eeei) then
    call vai4ei (tbuf(1)  ,title_v1%bloc,    3)
    call r4tor4 (tbuf(4)  ,title_v1%sourc,   9)
    call vai4ei (tbuf(13) ,title_v1%dobs,    2)
    call var4ei (tbuf(15) ,title_v1%off1,    2)
    call vai4ei (tbuf(17) ,title_v1%typec,   6)
    call var4ei (tbuf(23) ,title_v1%houra,   1)
    call r4tor4 (tbuf(24) ,title_v1%project, 1)
    call vai4ei (tbuf(26) ,title_v1%bpc,     3)
    call var4ei (tbuf(29) ,title_v1%ut,      1)
  elseif (conv.eq.eeei_to_vax) then
    call eii4va (tbuf(1)  ,title_v1%bloc,    3)
    call r4tor4 (tbuf(4)  ,title_v1%sourc,   9)
    call eii4va (tbuf(13) ,title_v1%dobs,    2)
    call eir4va (tbuf(15) ,title_v1%off1,    2)
    call eii4va (tbuf(17) ,title_v1%typec,   6)
    call eir4va (tbuf(23) ,title_v1%houra,   1)
    call r4tor4 (tbuf(24) ,title_v1%project, 1)
    call eii4va (tbuf(26) ,title_v1%bpc,     3)
    call eir4va (tbuf(29) ,title_v1%ut,      1)
  elseif (conv.eq.ieee_to_eeei) then
    call iei4ei (tbuf(1)  ,title_v1%bloc,    3)
    call r4tor4 (tbuf(4)  ,title_v1%sourc,   9)
    call iei4ei (tbuf(13) ,title_v1%dobs,    2)
    call ier4ei (tbuf(15) ,title_v1%off1,    2)
    call iei4ei (tbuf(17) ,title_v1%typec,   6)
    call ier4ei (tbuf(23) ,title_v1%houra,   1)
    call r4tor4 (tbuf(24) ,title_v1%project, 1)
    call iei4ei (tbuf(26) ,title_v1%bpc,     3)
    call ier4ei (tbuf(29) ,title_v1%ut,      1)
  elseif (conv.eq.eeei_to_ieee) then
    call eii4ie (tbuf(1)  ,title_v1%bloc,    3)
    call r4tor4 (tbuf(4)  ,title_v1%sourc,   9)
    call eii4ie (tbuf(13) ,title_v1%dobs,    2)
    call eir4ie (tbuf(15) ,title_v1%off1,    2)
    call eii4ie (tbuf(17) ,title_v1%typec,   6)
    call eir4ie (tbuf(23) ,title_v1%houra,   1)
    call r4tor4 (tbuf(24) ,title_v1%project, 1)
    call eii4ie (tbuf(26) ,title_v1%bpc,     3)
    call eir4ie (tbuf(29) ,title_v1%ut,      1)
  endif
  !
  if (title_v1%scan.eq.0) title_v1%scan = title_v1%num
  if (title_v1%recei.lt.1) title_v1%recei =1
  !
  call title_v1tov2(title_v1,error)
  !
end subroutine index_fromv1
!
subroutine index_fromv2(conv,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_convert
  use classic_api
  use clic_file
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in observation title v2 from v2 buffer
  !-----------------------------------------------------------------------
  integer(kind=4),intent(in)  :: conv
  logical, intent(inout)      :: error
  ! Local
  integer(kind=4) :: scan
  ! Code
  if (conv.eq.0) then
    call r4tor4(tbuf(1)   ,title%bloc,   32)
  elseif (conv.eq.vax_to_ieee) then
    call r4tor4(tbuf(1)   ,title%bloc,   32)
    call var4ie(tbuf(18)  ,title%off1,    2)
    call var4ie(tbuf(26)  ,title%houra,   1)
    call var4ie(tbuf(32)  ,title%ut,      1)
  elseif (conv.eq.ieee_to_vax) then
    call r4tor4(tbuf(1)   ,title%bloc,   32)
    call ier4va(tbuf(18)  ,title%off1,    2)
    call ier4va(tbuf(26)  ,title%houra,   1)
    call ier4va(tbuf(32)  ,title%ut,      1)
  elseif (conv.eq.vax_to_eeei) then
    call vai4ei (tbuf(1)  ,title%bloc,    6)
    call r4tor4 (tbuf(7)  ,title%sourc,   9)
    call vai4ei (tbuf(16) ,title%dobs,    2)
    call var4ei (tbuf(18) ,title%off1,    2)
    call vai4ei (tbuf(20) ,title%typec,   6)
    call var4ei (tbuf(26) ,title%houra,   1)
    call r4tor4 (tbuf(27) ,title%project, 2)
    call vai4ei (tbuf(29) ,title%bpc,     3)
    call var4ei (tbuf(32) ,title%ut,      1)
  elseif (conv.eq.eeei_to_vax) then
    call eii4va (tbuf(1)  ,title%bloc,    6)
    call r4tor4 (tbuf(7)  ,title%sourc,   9)
    call eii4va (tbuf(16) ,title%dobs,    2)
    call eir4va (tbuf(18) ,title%off1,    2)
    call eii4va (tbuf(20) ,title%typec,   6)
    call eir4va (tbuf(26) ,title%houra,   1)
    call r4tor4 (tbuf(27) ,title%project, 2)
    call eii4va (tbuf(29) ,title%bpc,     3)
    call eir4va (tbuf(32) ,title%ut,      1)
  elseif (conv.eq.ieee_to_eeei) then
    call iei8ei (tbuf(1)  ,title%bloc,    1)
    call iei4ei (tbuf(3)  ,title%word,    2)
    call iei8ei (tbuf(5)  ,title%num,     1)
    call r4tor4 (tbuf(7)  ,title%sourc,   9)
    call iei4ei (tbuf(16) ,title%dobs,    2)
    call ier4ei (tbuf(18) ,title%off1,    2)
    call iei4ei (tbuf(20) ,title%typec,   6)
    call ier4ei (tbuf(26) ,title%houra,   1)
    call r4tor4 (tbuf(27) ,title%project, 2)
    call iei4ei (tbuf(29) ,title%bpc,     3)
    call ier4ei (tbuf(32) ,title%ut,      1)
  elseif (conv.eq.eeei_to_ieee) then
    call eii8ie (tbuf(1)  ,title%bloc,    1)
    call eii4ie (tbuf(3)  ,title%word,    2)
    call eii8ie (tbuf(5)  ,title%num,     1)
    call r4tor4 (tbuf(7)  ,title%sourc,   9)
    call eii4ie (tbuf(16) ,title%dobs,    2)
    call eir4ie (tbuf(18) ,title%off1,    2)
    call eii4ie (tbuf(17) ,title%typec,   6)
    call eir4ie (tbuf(26) ,title%houra,   1)
    call r4tor4 (tbuf(27) ,title%project, 2)
    call eii4ie (tbuf(29) ,title%bpc,     3)
    call eir4ie (tbuf(32) ,title%ut,      1)
  endif
  !
  if (title%recei.lt.1) title%recei =1
  if (title%scan.eq.0) then
    call i8toi4_fini(title%num,scan,1,error)
    if (error) return     
    title%scan = scan
  endif
  !
end subroutine index_fromv2
!
subroutine title_v1tov2(title_v1,error)
  use gildas_def
  use gkernel_interfaces
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in observation title v2 from an observation title v1
  !-----------------------------------------------------------------------
  ! Local
  type(title_v1_t),intent(in):: title_v1
  logical,intent(inout)      :: error
  ! Code
  title%bloc       = title_v1%bloc  
  title%word       = 1
  title%num        = title_v1%num
  title%ver        = title_v1%ver
  title%sourc      = title_v1%sourc
  title%line       = title_v1%line
  title%teles      = title_v1%teles
  title%dobs       = title_v1%dobs
  title%dred       = title_v1%dred
  title%off1       = title_v1%off1
  title%off2       = title_v1%off2
  title%typec      = title_v1%typec
  title%kind       = title_v1%kind
  title%qual       = title_v1%qual
  title%scan       = title_v1%scan
  title%proc       = title_v1%proc
  title%itype      = title_v1%itype
  title%houra      = title_v1%houra
  title%project(1) = title_v1%project
  title%project(2) = fourblanks
  title%bpc        = title_v1%bpc
  title%ic         = title_v1%ic
  title%recei      = title_v1%recei
  title%ut         = title_v1%ut
  ! 
end subroutine title_v1tov2
!
subroutine index_tov1(conv,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_convert
  use classic_api
  use clic_file
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in v1 buffer from observation title v2
  !-----------------------------------------------------------------------
  integer(kind=4),intent(in)  :: conv
  logical, intent(inout)      :: error
  ! Local
  type(title_v1_t) :: title_v1
  ! Code
  call index_v2tov1(title_v1,error)
  if (error) return
  !  
  if (conv.eq.0) then
    call r4tor4(title_v1%bloc   ,tbuf(1),32)
  elseif (conv.eq.vax_to_ieee) then
    call r4tor4(title_v1%bloc   ,tbuf(1),32)
    call ier4va(title_v1%off1   ,tbuf(15),2)
    call ier4va(title_v1%houra  ,tbuf(23),1)
    call ier4va(title_v1%ut     ,tbuf(29),1)
  elseif (conv.eq.ieee_to_vax) then
    call r4tor4(title_v1%bloc   ,tbuf(1),32)
    call var4ie(title_v1%off1   ,tbuf(15),2)
    call var4ie(title_v1%houra  ,tbuf(23),1)
    call var4ie(title_v1%ut     ,tbuf(29),1)
  elseif (conv.eq.vax_to_eeei) then
    call eii4va(title_v1%bloc   ,tbuf(1), 3)
    call r4tor4(title_v1%sourc  ,tbuf(4), 9)
    call eii4va(title_v1%dobs   ,tbuf(13),2)
    call eir4va(title_v1%off1   ,tbuf(15),2)
    call eii4va(title_v1%typec  ,tbuf(17),6)
    call eir4va(title_v1%houra  ,tbuf(23),1)
    call r4tor4(title_v1%project,tbuf(24),1)
    call eii4va(title_v1%bpc    ,tbuf(26),3)
    call eir4va(title_v1%ut     ,tbuf(29),1)
  elseif (conv.eq.eeei_to_vax) then
    call vai4ei(title_v1%bloc   ,tbuf(1), 3)
    call r4tor4(title_v1%sourc  ,tbuf(4), 9)
    call vai4ei(title_v1%dobs   ,tbuf(13),2)
    call var4ei(title_v1%off1   ,tbuf(15),2)
    call vai4ei(title_v1%typec  ,tbuf(17),6)
    call var4ei(title_v1%houra  ,tbuf(23),1)
    call r4tor4(title_v1%project,tbuf(24),1)
    call vai4ei(title_v1%bpc    ,tbuf(26),3)
    call var4ei(title_v1%ut     ,tbuf(29),1)
  elseif (conv.eq.ieee_to_eeei) then
    call eii4ie(title_v1%bloc   ,tbuf(1), 3)
    call r4tor4(title_v1%sourc  ,tbuf(4), 9)
    call eii4ie(title_v1%dobs   ,tbuf(13),2)
    call eir4ie(title_v1%off1   ,tbuf(15),2)
    call eii4ie(title_v1%typec  ,tbuf(17),6)
    call eir4ie(title_v1%houra  ,tbuf(23),1)
    call r4tor4(title_v1%project,tbuf(24),1)
    call eii4ie(title_v1%bpc    ,tbuf(26),3)
    call eir4ie(title_v1%ut     ,tbuf(29),1)
  elseif (conv.eq.eeei_to_ieee) then
    call iei4ei(title_v1%bloc   ,tbuf(1), 3)
    call r4tor4(title_v1%sourc  ,tbuf(4), 9)
    call iei4ei(title_v1%dobs   ,tbuf(13),2)
    call ier4ei(title_v1%off1   ,tbuf(15),2)
    call iei4ei(title_v1%typec  ,tbuf(17),6)
    call ier4ei(title_v1%houra  ,tbuf(23),1)
    call r4tor4(title_v1%project,tbuf(24),1)
    call iei4ei(title_v1%bpc    ,tbuf(26),3)
    call ier4ei(title_v1%ut     ,tbuf(29),1)
  endif
end subroutine index_tov1
!
subroutine index_tov2(conv,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_convert
  use classic_api
  use clic_file
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in v2 buffer from observation title v2
  !-----------------------------------------------------------------------
  integer(kind=4),intent(in)  :: conv
  logical, intent(inout)      :: error
  ! Code
  if (conv.eq.0) then
    call r4tor4 (title%bloc,   tbuf(1)  ,32)
  elseif (conv.eq.vax_to_ieee) then
    call r4tor4 (title%bloc,   tbuf(1)  ,32)
    call var4ie (title%off1,   tbuf(18) , 2)
    call var4ie (title%houra,  tbuf(26) , 1)
    call var4ie (title%ut,     tbuf(32) , 1)
  elseif (conv.eq.ieee_to_vax) then
    call r4tor4 (title%bloc,   tbuf(1)  ,32)
    call ier4va (title%off1,   tbuf(18) , 2)
    call ier4va (title%houra,  tbuf(26) , 1)
    call ier4va (title%ut,     tbuf(32) , 1)
  elseif (conv.eq.vax_to_eeei) then
    call vai4ei (title%bloc,   tbuf(1)  , 6)
    call r4tor4 (title%sourc,  tbuf(7)  , 9)
    call vai4ei (title%dobs,   tbuf(16) , 2)
    call var4ei (title%off1,   tbuf(18) , 2)
    call vai4ei (title%typec,  tbuf(20) , 6)
    call var4ei (title%houra,  tbuf(26) , 1)
    call r4tor4 (title%project,tbuf(27) , 2)
    call vai4ei (title%bpc,    tbuf(29) , 3)
    call var4ei (title%ut,     tbuf(32) , 1)
  elseif (conv.eq.eeei_to_vax) then
    call eii4va (title%bloc,   tbuf(1)  , 6)
    call r4tor4 (title%sourc,  tbuf(7)  , 9)
    call eii4va (title%dobs,   tbuf(16) , 2)
    call eir4va (title%off1,   tbuf(18) , 2)
    call eii4va (title%typec,  tbuf(20) , 6)
    call eir4va (title%houra,  tbuf(26) , 1)
    call r4tor4 (title%project,tbuf(27) , 2)
    call eii4va (title%bpc,    tbuf(29) , 3)
    call eir4va (title%ut,     tbuf(32) , 1)
  elseif (conv.eq.ieee_to_eeei) then
    call iei8ei (title%bloc,   tbuf(1)  , 1)
    call iei4ei (title%word,   tbuf(3)  , 2)
    call iei8ei (title%num,    tbuf(5)  , 1)
    call r4tor4 (title%sourc,  tbuf(7)  , 9)
    call iei4ei (title%dobs,   tbuf(16) , 2)
    call ier4ei (title%off1,   tbuf(18) , 2)
    call iei4ei (title%typec,  tbuf(20) , 6)
    call ier4ei (title%houra,  tbuf(26) , 1)
    call r4tor4 (title%project,tbuf(27) , 2)
    call iei4ei (title%bpc,    tbuf(29) , 3)
    call ier4ei (title%ut,     tbuf(32) , 1)
  elseif (conv.eq.eeei_to_ieee) then
    call eii8ie (title%bloc,   tbuf(1)  , 1)
    call eii4ie (title%word,   tbuf(3)  , 2)
    call eii8ie (title%num,    tbuf(5)  , 1)
    call r4tor4 (title%sourc,  tbuf(7)  , 9)
    call eii4ie (title%dobs,   tbuf(16) , 2)
    call eir4ie (title%off1,   tbuf(18) , 2)
    call eii4ie (title%typec,  tbuf(17) , 6)
    call eir4ie (title%houra,  tbuf(26) , 1)
    call r4tor4 (title%project,tbuf(27) , 2)
    call eii4ie (title%bpc,    tbuf(29) , 3)
    call eir4ie (title%ut,     tbuf(32) , 1)
  endif
  !
end subroutine index_tov2
!
subroutine index_v2tov1(title_v1,error)
  use gildas_def
  use gkernel_interfaces
  use clic_title
  !-----------------------------------------------------------------------
  ! Fill in observation title v1 from an observation title v2
  !-----------------------------------------------------------------------
  ! Local
  type(title_v1_t),intent(out) :: title_v1
  logical,intent(inout)        :: error
  ! Code
  call i8toi4_fini(title%bloc,title_v1%bloc,1,error)
  if (error) return
  if (title%word.ne.1) then
    call message(8,3,'INDEX','Observation must start at the beginning of the record of CLASSIC v1 files.')
    error = .true.
    return
  endif
  call i8toi4(title%num,title_v1%num,1,error)
  if (error) return
  title_v1%ver     = title%ver
  title_v1%sourc   = title%sourc
  title_v1%line    = title%line
  title_v1%teles   = title%teles
  title_v1%dobs    = title%dobs
  title_v1%dred    = title%dred
  title_v1%off1    = title%off1
  title_v1%off2    = title%off2
  title_v1%typec   = title%typec
  title_v1%kind    = title%kind
  title_v1%qual    = title%qual
  title_v1%scan    = title%scan
  title_v1%proc    = title%proc
  title_v1%itype   = title%itype
  title_v1%houra   = title%houra
  title_v1%project = title%project(1)
  if (title%project(2).ne.fourblanks) then
    call message(8,3,'INDEX','Cannot write a character*8 project name &
                 in a v1 file')
    error = .true.
  endif
  title_v1%bpc     = title%bpc
  title_v1%ic      = title%ic
  title_v1%recei   = title%recei
  title_v1%ut      = title%ut
  ! 
end subroutine index_v2tov1



