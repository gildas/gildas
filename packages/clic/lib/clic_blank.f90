subroutine clic_blank(line,error)
  use gkernel_interfaces
  !-------------------------------------------------------------------------
  ! CLIC\BLANK c1 c2  /SPW /RESET
  !-------------------------------------------------------------------------
  character(len=*) :: line
  logical          :: error
  !-------------------------------------------------------------------------
  ! Global:
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  !-------------------------------------------------------------------------
  ! Local:
  integer          :: c1, c2, ispw, lc
  character*128    :: chain
  !-------------------------------------------------------------------------
  ! Code:
  error = .false.
  if (sic_present(3,0)) then
    imask = 1
  endif
  if (sic_present(4,0)) then
    imask = 0
  endif
  if (.not.sic_present(1,0)) then
    if (.not.sic_present(3,0).and..not.sic_present(4,0)) then
      call message(6,3,'CLIC_BLANK','/SPW is mandatory')
      error = .true.
    endif
    return
  endif
  call sic_i4(line,1,1,ispw,.true.,error)
  if (error) then
    call message(6,3,'CLIC_BLANK','Error reading spw')
    return
  endif
  ! 
  ! Reset
  if (sic_present(2,0)) then
    write (chain,'(a,i0)') 'Resetting spw ',ispw
    lc = len_trim(chain)
    call message(6,1,'CLIC_BLANK',chain(1:lc))
    nmask(ispw) = 0
    return
  endif  
  !
  call sic_i4(line,0,1,c1,.true.,error)
  if (error) then
    call message(6,3,'CLIC_BLANK','Error reading first argument')
    return
  endif
  call sic_i4(line,0,2,c2,.true.,error)
  if (error) then
    call message(6,3,'CLIC_BLANK','Error reading first argument')
    return
  endif
  if (nmask(ispw).lt.mmask) then
    nmask(ispw) = nmask(ispw)+1
    cmask(1,nmask(ispw),ispw) = c1
    cmask(2,nmask(ispw),ispw) = c2
    write (chain,'(a,i0,a,i0,a,i0)') 'Masking spw ',ispw,', channels ', &
         c1,' to ',c2
    lc = len_trim(chain)
    call message(6,1,'CLIC_BLANK',chain(1:lc))
  else
    call message(6,3,'CLIC_BLANK','Maximum number of ranges reached')
    error = .true.
    return
  endif  
  return   
end subroutine clic_blank
