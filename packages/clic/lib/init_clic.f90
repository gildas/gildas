subroutine init_clic
  use gkernel_interfaces
  use astro_interfaces
  use gildas_def
  use gbl_message
  use clic_title
  use classic_api
  ! Global
  external :: run_clic
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  ! vocabulaire de CLIC
  logical :: error
  integer :: mclic
  parameter (mclic=167+9) !+8 : SG_TABLE command + 1 ZPASSBAND - 1 TABLE /NEW
  character(len=12) :: vocab(mclic)
  character(len=20) :: version
  character(len=message_length) :: mess
  integer(kind=address_length) :: id
#if defined(CFITSIO)
  external :: run_tifits
#endif
#if defined(SDM)
  external :: run_sdm
#endif
  !
  ! Force BLOCK DATA loading
  external clicda
  ! Data
  data vocab /   &
    ' ATMOSPHERE', '/COMPRESS', '/RESET', '/NOWRITE', '/NOMONITOR',   &
    ' BLANK', '/SPW', '/RESET', '/EXTRAPOLATE', '/NOEXTRAPOLATE',       &
    ' COMPRESS', '/TRIANGLE',   &
    ' CLONE',   &
    ' COPY', '/EXTRACT',  &
    ' CURSOR',   &
    ' DIRECTORY',   &
    ' DROP',   &
    ' DUMP', '/PLOT',   &
    ' FILE', '/WINDOW', '/DIRECTORY',   &
    ' FIND',  '/TYPE', '/LINE', '/NUMBER',   &
    '/SCAN',  '/OFFSET', '/SOURCE', '/PROCEDURE',   &
    '/QUALITY',  '/TELESCOPE', '/POLAR', '/RECEIVER',   &
    '/MODE', '/LOAD', '/SKIPAUTO', '/EXCLUDE',  &
    ' FLAG', '/ANTENNA', '/BASELINE', '/RESET',   &
    ' FREE', &
    ' FITS',   &
    ' GET',  '/RECORD', '/HEADER', '/ATM',  &
    ' GRID_TOTAL', '/PLOT',   &
    ' HEADER', '/PLOT', '/LIST',   &
    ' MARK', '/ANTENNA', '/BASELINE', '/RESET',   &
    ' IGNORE',   &
    ' LIST', '/BRIEF', '/LONG', '/OUTPUT',   &
    '/FLAG', '/SHORT', '/PROJECT',  '/SOURCE', '/VARIABLE',   &
    '/FORMAT', '/OFFSET', '/MEMORY','/TELESCOPE','/BANDPASS',   &
    ' MASK', '/ANTENNA', '/BASELINE', '/RESET', '/ALL', '/SPECTRAL',  &
    ' MINMAX',   &
    ' MONITOR',   &
    ' MODIFY', '/BEFORE', '/AFTER', '/OFFSET', '/RECORD',   &
    '/POLARISATION', '/ATM', '/BBAND', '/IF', '/INSTRUMENTAL', '/ALL',  &
    '/HEADER', '/SWITCH', '/NOWRITE', &
    ' NEW_DATA',   &
    ' PLOT', '/NOFRAME', '/RECORD',  '/IDENTIFY',   &
    '/RESET', '/NODRAW', '/APPEND', '/PHYSICAL', '/BOXES', '/NOSORT',  &
    ' POPUP',   &
    ' PRINT', '/FILE',   &
    ' RESIDUALS',   &
    ' SAVE',   &
    ' SET',  '/LIMITS', '/WINDOW', '/ANTENNA',   &
    '/DEFAULT', '/RESET', '/NOSELECT',  &
    ' SELECT',   &
    ' SHOW',   &
    ' SOLVE', '/PLOT', '/WEIGHT', '/OUTPUT',      &
    '/RESET', '/OFFSET', '/COMPRESS', '/SEARCH',  &
    '/BREAK', '/POLYNOMIAL', '/PRINT', '/FIX',    &
    '/TOTAL', '/IGNORE', '/SPECTRUM', '/CROSSED', &
    '/SIN',   '/THRESHOLD',  &
    ' STORE', '/BAND', '/ANTENNA', '/BASELINE',   &
    '/RESET', '/SELF', '/RECEIVER', '/POLARISATION', '/FRACTION','/MEMORY',  &
    '/SCAN', '/MULTIPLE', '/OVERWRITE','/BBAND', &
    ' SG_TABLE', '/ADD', '/DROP', '/FFT', '/FREQUENCY', '/NOCHECK', '/RESAMPLE', '/POLARIZATION', '/STOKES', &
    ' TABLE', '/RESAMPLE', '/DROP', '/COMPRESS',   &
    '/FREQUENCY', '/NOCHECK', '/SCAN', '/FFT',   &
    '/MOSAIC', &   ! /MOSAIC replaces /POSITION and /NEW was unused
    ' TAG',   &
    ' VARIABLES',   &
    ' WVR', '/CMODE', '/NOWRITE', &
    ' ZPASSBAND' /
  !
  ! TIFITS vocabulary
  integer :: mtifits
  parameter (mtifits=8)
  character(len=12) :: vocabti(mtifits)
  character(len=20) :: versionti
  data vocabti /   &
    ' READ',   &
    ' WRITE',  '/DATA',  '/NOCAL', '/HOLOGRAPHY', '/ANTENNAS',   &
    '/TELESCOPE', '/SIMULATE'/
  !
  ! SDM vocabulary
  integer :: msdm
  parameter (msdm=8)
  character(len=12) :: vocabsdm(msdm)
  character(len=20) :: versionsdm
  data vocabsdm /   &
    ' READ',   &
    ' WRITE',  '/DATA',  '/NOCAL', '/HOLOGRAPHY', '/ANTENNAS',   &
    '/TELESCOPE', '/SIMULATE'/
  save vocab, vocabti, vocabsdm
  !
  include 'clic_version.inc'
  !$ call omp_set_num_threads(1) ! Turn off Parallel mode by default
  !
  id = locwrd(clicda)
  error = gr_error()
  r_teles = ' '
  !
  ! Allocate buffers
  call allocate_buffers(error)
  if (error) then
    call message(8,4,'INIT_CLIC','Cannot allocate buffers' )
    call sysexi(fatale)  
  endif
  !
  ! Check COMMON alignments
  call check_commons
  !
  !      if (vocab(mclic).ne.'/NOWRITE') STOP 'Vocab. Error'
  !
  call sic_begin('CLIC','GAG_HELP_CLIC',mclic,vocab,   &
    version//' V Pietu, R.Lucas',run_clic,gr_error)
  !
#if defined(CFITSIO)
  versionti = '1.1-01 07-May-2003'
  call sic_begin('TIFITS','GAG_HELP_TIFITS',mtifits,vocabti,   &
    versionti//' R.Lucas',run_tifits,gr_error)
#endif
  !
#if defined(SDM)
  versionsdm = '0.1-01 27-Mar-2007'
  call sic_begin('SDM','GAG_HELP_ASDM',msdm,vocabsdm,   &
    versionsdm//' R.Lucas',run_sdm,gr_error)
#endif
  !
  planet_model = -1
  skind = 4
  find_needed = .true.
  m_header = (locwrd(r_lastone)-locwrd(r_xnum))/4+1
  m_header_no_rf = (locwrd(r_last_rf_ant)-locwrd(r_xnum))/4+1
  m_header_ant_rf = (locwrd(r_last_rf_bas)-locwrd(r_xnum))/4+1
  m_dh = (locwrd(dh_averag(1,1))-locwrd(dh_void))/4+4*mnbas
  bp_spectrum_memory = .false.
  bp_spectrum_ready = .false.
  write(mess,'(3(A,I8))') 'Header ',m_header,' Data_Header ',m_dh,   &
    ' Line_Data ',mdatal
  call clic_message(seve%i,'INIT',mess)
  call astro_observatory('05:54:26.0','44:38:01.2',2.560d0,45.d0,error)
  call atm_i(error)
  call set_numbers
  allow_pol = .false.
  lowres = .false.
  call sic_def_logi ('ALLOW_POL',allow_pol,.false.,error)
  call chtoby('    ',fourblanks,4)
  !
  ! Initialize conversion code
  call classic_init(error)
end subroutine init_clic
!
subroutine check_commons
  use gkernel_interfaces
  use gildas_def
  use clic_index
  use clic_virtual
  use classic_api
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_constant.inc'
  include 'clic_dcomp.inc'
  include 'clic_dheader.inc'
  include 'clic_display.inc'
  include 'clic_fit.inc'
  include 'clic_fits.inc'
  include 'clic_flags.inc'
  ! include 'clic_gaussdata.inc'
  include 'clic_gauss.inc'
  include 'clic_number.inc'
  ! include 'clic_panels.inc'
  include 'clic_par.inc'
  ! include 'clic_point.inc'
  include 'clic_sba.inc'
  ! include 'clic_skydip.inc'
  include 'clic_stations.inc'
  include 'clic_tabbuf.inc'
  include 'clic_title.inc'
  include 'clic_windowfile.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: error
  !
  error = .false.
  ! clic_clic.inc:226:      COMMON /SELECTION/ SELECT, CONT_SELECT,
  call check_iaddress(error,'SELECTION',select)
  ! clic_clic.inc:230:      COMMON /SASSET/
  ! clic_clic.inc:258:      COMMON /SASCET/ SLINE,SSOURC,STELES,
  ! clic_clic.inc:268:      COMMON /FITS/
  ! clic_common.inc:56:      COMMON/COM/ INTERVAL,
!! call check_caddress(error,'CARCOM',ispec)
  ! clic_common.inc:67:      COMMON/CARCOM/ ISPEC,OSPEC,DSPEC
  ! clic_dcomp.inc:47:      COMMON /DCOMP_COM/
  call check_iaddress(error,'DCOMP_COM',dcomp_void)
  call check_iaddress(error,'DH_COM',dh_void)
  ! clic_dheader.inc:49:      COMMON /DH_COM/
  call check_caddress(error,'CDISPLAY',sw1)
  call check_iaddress(error,'DISPLAY',n_boxes)
  ! clic_display.inc:117:      COMMON /CDISPLAY/ SW1, SW2, C_SETBOX, SM_X1, SM_X2, SM_Y1,
  ! clic_display.inc:121:      COMMON / DISPLAY/ N_BOXES, N_X, I_X, K_X, N_Y, I_Y, K_Y,
  call check_iaddress(error,'FITCOM',x)
  ! clic_fit.inc:27:      COMMON /FITCOM/ X,XT,DIRIN, U,WERR,ALIM,BLIM, V,
  ! clic_fits.inc:58:      COMMON /CFITS01/
  call check_iaddress(error,'CFITS01',axval)
  ! clic_fits.inc:74:      COMMON /CFITSC1/ AXTYPE,KIND,COLTYPE,COLFORM
  ! clic_flags.inc:10:      COMMON /FLAGS/ AF, BF
  call check_caddress(error,'FLAGS',af)
  ! clic_gaussdata.inc:9:      COMMON /CRGAUSS/ NDATA,RDATAX,RDATAY
  !!      call CHECK_IADDRESS(error,'CRGAUSS',ndata)
  ! clic_gauss.inc:13:      COMMON /CGUESS/ NLINE,SPAR,DELTAV,
  ! clic_gauss.inc:20:      COMMON /CPOIDS/ WFIT(MXCAN)
  ! clic_gauss.inc:25:      COMMON /CRGAUS/ NGLINE,SIGBAS,SIGRAI,PAR,ERR
  ! clic_number.inc:34:      COMMON /NUMBERS/ BASANT, ANTBAS, TRIANT, ANTTRI, BASTRI,
  ! clic_panels.inc:14:      COMMON /PANEL/ ANT_TYPE, DIAMETER, FOCUS, NPAN, NRING, RAY, C2,
  ! clic_parameter.inc:59:      COMMON /COM_SECTION/ SECTION
  ! clic_parameter.inc:174:      COMMON /PARAMS/ M_HEADER, M_DH
  ! clic_parameter.inc:184:      COMMON       /CONFIGS/ CONFIG, CONFIG_NAME, CONFIG_PATCH
  ! clic_par.inc:286:      COMMON /R_COM/
  call check_iaddress(error,'R_XNUM',r_xnum)
  ! CLIC_POINT.INC:18:      COMMON /SPOINT/
  ! clic_rdata.inc:12:      COMMON /CRDATA/ DATAC, DATAL, PASSC, PASSL,
  ! clic_rdata.inc:19:      COMMON /CRGAIN/ GAINC, GAINL, WGAINC, WGAINL,
  ! clic_sba.inc:12:      COMMON /SBA/ ZSBA, ZRSBA, WSBA, WRSBA
  ! clic_skydip.inc:14:      COMMON /CSKYDI/ PAR, EL, SKY, SIGRMS, IFE, NP,
  ! clic_stations.inc:6:      COMMON /STATIONS/ STAT89, STAT96, STAT99
  ! clic_tabbuf.inc:35:      COMMON /TABLE_COM/
  ! clic_tabbuf.inc:79:      COMMON /OUT_COM/
  ! clic_title.inc:7:      COMMON /LTITLE/ TITLE_LINES, T_INTERFERO, T_POSITION, T_QUALITY,
  ! clic_virtual.inc:28:      COMMON /VIRTUAL/
  ! clic_windowfile.inc:8:      COMMON /SELECTED/ NFILE, IFILE, HFILEDEF
  ! clic_windowfile.inc:9:      COMMON /CELECTED/ DATAFILE, CODE, HFILE, HFILEMODE
  if (error) call sysexi(fatale)
end subroutine check_commons
!
subroutine check_iaddress(error,name,int)
  use gildas_def
  use gkernel_interfaces
  logical :: error                  !
  character(len=*) :: name          !
  integer :: int                    !
  ! Local
  integer(kind=address_length) :: addr
  !
  addr = locwrd(int)
  if (mod(addr,4_address_length).ne.0) then
    error = .true.
    print *,'Bad alignment ',name,addr,dble(addr)*0.25d0,addr/4
  elseif  (error) then
    print *,name,' at ',dble(addr)*0.25d0
  endif
end subroutine check_iaddress
subroutine check_caddress(error,name,int)
  use gildas_def
  logical :: error                  !
  character(len=*) :: name          !
  character(len=*) :: int           !
  ! Global
  integer(kind=address_length) :: locstr
  ! Local
  integer(kind=address_length) :: addr
  !
  addr = locstr(int)
  if (mod(addr,4_address_length).ne.0) then
    error = .true.
    print *,'Bad alignment ',name,addr,dble(addr)*0.25d0,addr/4
  elseif  (error) then
    print *,name,' at ',dble(addr)*0.25d0
  endif
end subroutine check_caddress

subroutine allocate_buffers(error)
  use clic_rdata
  use clic_buffers
  use clic_bpc
  use clic_index
  use clic_virtual
  use gkernel_interfaces
  include 'clic_parameter.inc'
  !
  ! Dummy
  logical :: error
  !
  ! Local
  integer :: ier
  character(len=*), parameter :: rname='INIT_CLIC'
  !
  ! First clic_rdata module
  allocate( DATAC(MDATAC), DATAL(MDATAL), stat=ier)
  if (failed_allocate(rname,'Buffers 1',ier,error))  return
  allocate( PASSC(MDATAC), PASSL(MDATAL), stat=ier)
  if (failed_allocate(rname,'Buffers 2',ier,error))  return
  allocate( FACTC(MDATAC), FACTL(MDATAL), stat=ier)
  if (failed_allocate(rname,'Buffers 3',ier,error))  return
  allocate( SPIDC(MDATAC), SPIDL(MDATAL), stat=ier)
  if (failed_allocate(rname,'Buffers 4',ier,error))  return
  allocate( CAMPMAXU(MNBAS),LAMPMAXU(MNBAS),                        &
  CAMPMAXL(MNBAS),LAMPMAXL(MNBAS),                                  &
  CAMPMINU(MNBAS),LAMPMINU(MNBAS),                                  &
  CAMPMINL(MNBAS),LAMPMINL(MNBAS),                                  &
  AMPFACU(MNBAS),AMPFACL(MNBAS), stat=ier)
  if (failed_allocate(rname,'Buffers 5',ier,error))  return
  allocate( GAINC(2,MCCH,MNANT), GAINL(2,MLCH,MNANT), stat=ier)
  if (failed_allocate(rname,'Buffers 6',ier,error))  return
  allocate( WGAINC(2,MCCH,MNANT), WGAINL(2,MLCH,MNANT), stat=ier)
  if (failed_allocate(rname,'Buffers 7',ier,error))  return
  !
  ! and initialize 
  datac = 0
  datal = 0
  passc = 0
  passl = 0
  factc = 0
  factl = 0
  spidc = 0
  spidl = 0
  campmaxl = 0
  lampmaxl = 0
  campmaxu = 0
  lampmaxu = 0
  campminl = 0
  lampminl = 0
  campminu = 0
  lampminu = 0
  ampfacu = 0
  ampfacl = 0
  gainc = 0
  gainl = 0
  wgainc = 0
  wgainl = 0
  !
  ! then clic_buffers module
  allocate(tcal_u(mnant,mnbb), tcal_l(mnant,mnbb),stat=ier)
  if (failed_allocate(rname,'Buffers 8',ier,error))  return
  allocate(tcal(mnbb,2,mnant),stat=ier)
  if (failed_allocate(rname,'Buffers 9',ier,error))  return
  allocate(c_lev(mnant,mnrec,mnbb,m_pol_rec,3),stat=ier)
  if (failed_allocate(rname,'Buffers 10',ier,error))  return
  allocate(s_lev(mnant,mnrec,mnbb,m_pol_rec,3),stat=ier)
  if (failed_allocate(rname,'Buffers 11',ier,error))  return
  allocate(c_c(mcch,mnant,mnrec,m_pol_rec,3),stat=ier)
  if (failed_allocate(rname,'Buffers 12',ier,error))  return
  allocate(c_l(mlch,mnant,mnrec,m_pol_rec,3),stat=ier)
  if (failed_allocate(rname,'Buffers 13',ier,error))  return
  allocate(h_atm(matparm,mnrec),stat=ier)
  if (failed_allocate(rname,'Buffers 14',ier,error))  return
  allocate(h_mon(matmon,mnrec),stat=ier)
  if (failed_allocate(rname,'Buffers 15',ier,error))  return
  allocate(h_line(mlineset,mnrec),stat=ier)
  if (failed_allocate(rname,'Buffers 16',ier,error))  return
  allocate(h_inter(minterc,mnrec),stat=ier)
  if (failed_allocate(rname,'Buffers 17',ier,error))  return
  allocate(sky_flags(mnant,mnbb),stat=ier)
  if (failed_allocate(rname,'Buffers 18',ier,error))  return
  allocate(sky_scan(mnrec),stat=ier)
  if (failed_allocate(rname,'Buffers 19',ier,error))  return
  allocate(integ(mnant,mnrec,mnbb,m_pol_rec,3),stat=ier)
  if (failed_allocate(rname,'Buffers 20',ier,error))  return
  !
  ! and initialize
  tcal_u = 0
  tcal_l = 0
  tcal = 0
  c_lev = 0
  s_lev = 0
  c_c = 0
  c_l = 0
  h_atm = 0
  h_mon = 0
  h_line = 0
  h_inter =0
  sky_flags = 0 
  sky_scan = 0
  integ = 0
  !
  ! then clic_bpc module
  allocate(BP_NANT(MNBB),stat=ier)          
  if (failed_allocate(rname,'Buffers 21',ier,error))  return
  allocate(BP_ANT(MNANT,MNBB),stat=ier)     
  if (failed_allocate(rname,'Buffers 22',ier,error))  return
  allocate(BP_PHYS(MNANT,MNBB),stat=ier)    
  if (failed_allocate(rname,'Buffers 23',ier,error))  return
  allocate(FBP_CAMP(2,MNBB,-MNANT:MNBAS,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 24',ier,error))  return
  allocate(FBP_CPHA(2,MNBB,-MNANT:MNBAS,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 25',ier,error))  return
  allocate(BP_CAMP(2,MNBB,-MNANT:MNBAS,MCCH,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 26',ier,error))  return
  allocate(BP_CPHA(2,MNBB,-MNANT:MNBAS,MCCH,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 27',ier,error))  return
  allocate(FBP_LAMP(2,MNBB,-MNANT:MNBAS,MRLBAND,MNREC),stat=ier) 
  if (failed_allocate(rname,'Buffers 28',ier,error))  return
  allocate(FBP_LPHA(2,MNBB,-MNANT:MNBAS,MRLBAND,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 29',ier,error))  return
  allocate(BPC_DEG(2,MNBB,-MNANT:MNBAS,MRLBAND,MNREC),stat=ier)  
  if (failed_allocate(rname,'Buffers 30',ier,error))  return
  allocate(BP_LAMP(2,MNBB,-MNANT:MNBAS,MRLBAND,0:MBPCDEG,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 31',ier,error))  return
  allocate(BP_LPHA(2,MNBB,-MNANT:MNBAS,MRLBAND,0:MBPCDEG,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 32',ier,error))  return
  allocate(FBP_FAMP(2,MNBB,-MNANT:MNBAS,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 33',ier,error))  return
  allocate(FBP_FPHA(2,MNBB,-MNANT:MNBAS,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 34',ier,error))  return
  allocate(BPF_DEG(2,MNBB,-MNANT:MNBAS,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 35',ier,error))  return
  allocate(BP_FLIM(2,MNBB,-MNANT:MNBAS,2,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 36',ier,error))  return
  allocate(BP_FAMP(2,MNBB,-MNANT:MNBAS,0:MBPCDEG,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 37',ier,error))  return
  allocate(BP_FPHA(2,MNBB,-MNANT:MNBAS,0:MBPCDEG,MNREC),stat=ier)
  if (failed_allocate(rname,'Buffers 38',ier,error))  return
  allocate( SPECC(MDATAC), SPECL(MDATAL), stat=ier)
  if (failed_allocate(rname,'Buffers 39',ier,error))  return
  allocate(bp_samp(2,mnbb,-mnant:mnbas,mch),stat=ier)
  if (failed_allocate(rname,'Buffers 40',ier,error))  return
  allocate(bp_spha(2,mnbb,-mnant:mnbas,mch),stat=ier)
  if (failed_allocate(rname,'Buffers 41',ier,error))  return
  allocate(bp_fsamp(2,mnbb,mch),stat=ier)
  if (failed_allocate(rname,'Buffers 42',ier,error))  return
  allocate(bp_fspha(2,mnbb,mch),stat=ier)
  if (failed_allocate(rname,'Buffers 43',ier,error))  return
  allocate(fbp_samp(2,mnbb,-mnant:mnbas),stat=ier)
  if (failed_allocate(rname,'Buffers 44',ier,error))  return
  allocate(fbp_spha(2,mnbb,-mnant:mnbas),stat=ier)
  if (failed_allocate(rname,'Buffers 45',ier,error))  return
  allocate(nbp_samp(2,mnbb),stat=ier)
  if (failed_allocate(rname,'Buffers 46',ier,error))  return
  allocate(nbp_spha(2,mnbb),stat=ier)
  if (failed_allocate(rname,'Buffers 47',ier,error))  return

  !
  ! and initialize
  bp_nant = 0
  bp_ant = 0
  bp_phys = 0
  fbp_camp = .false.
  fbp_cpha = .false.
  bp_camp = 0
  bp_cpha = 0
  fbp_lamp = .false.
  fbp_lpha = .false.
  bp_lamp = 0
  bp_lpha = 0
  bpc_deg  = 0
  fbp_famp = .false.
  fbp_fpha = .false.
  bpf_deg = 0
  bp_flim = 0
  bp_famp = 0
  bp_fpha = 0
  specc = 0
  specl = 0
  bp_samp = 0
  bp_spha = 0
  bp_fsamp = 0
  bp_fspha = 0
  fbp_samp = .false.
  fbp_spha = .false.
  nbp_samp = 0
  nbp_spha = 0
  !
  ! Allocate memory flags
  allocate(ix_aflag(mnant,m_ix),stat=ier)
  if (failed_allocate(rname,'Buffers 48',ier,error))  return
  allocate(ix_bflag(mnbas,m_ix),stat=ier)
  if (failed_allocate(rname,'Buffers 49',ier,error))  return
  ix_aflag = 0
  ix_bflag = 0
  !
  ! Allocate virtual
  allocate(got_header(m_ix),v_header(m_ix),v_header_length(m_ix),stat=ier)
  if (failed_allocate(rname,'Buffers 50',ier,error))  return
  allocate(got_data(m_ix),v_data(m_ix),v_data_length(m_ix),stat=ier)
  if (failed_allocate(rname,'Buffers 51',ier,error))  return
  got_header(:) = .false.
  got_data(:) = .false. 
end subroutine allocate_buffers
