block data clicda
  use gildas_def
  use clic_index
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_panels.inc'
  include 'clic_par.inc'
  include 'clic_fits.inc'
  include 'clic_flags.inc'
  include 'clic_number.inc'
  include 'clic_stations.inc'
  ! Local
  integer :: mm1,mm2,mm3,ma1,ma2,ma3,mm0,ma0, mm4, mm5
  parameter (mm1=mnbb*2*mnbas*mcch,   &
    ma1=mnbb*2*mnant*mcch, &
    mm0=mnbb*2*mnbas*mrlband,   &
    ma0=mnbb*2*mnant*mrlband,   &
    mm2=mm0*(mbpcdeg+1), mm3 = mnbb*2*mnbas*(mbpcdeg+1),   &
    ma2=ma0*(mbpcdeg+1), ma3 = mnbb*2*mnant*(mbpcdeg+1),   &
    mm4 = 2*mnbas*mcch,   &
    mm5 = 2*mnbas*mrlband)
  !
  logical loaded
  common /modify/ loaded
  !
  ! Parameter
  data section/ 'Comment', 'General', 'Position',                 &  ! 1
    'Spectroscopy', 'Baseline', 'Origin', 'Plot',                 &  ! 4
    'Freq. Switch', 'Gauss Fit', 'Continuum', 'Beam. Switch',     &  ! 8
    'Shell Fit', 'NH3 fit', 'Calibration', 'Cont. Fit',           &  ! 12
    'Skydip', ' ',  ' ',  ' ',                                    &  ! 16
    'Point.Corr.', 'Interf.Gen.', 'RF Fr. Setup', 'Cont. Setup',  &  ! 20
    'Line Setup', 'Scanning', 'Atmos.Param.', 'Ant.P.B.Cal.',     &  ! 24-27
    'Bas.P.B.Cal.', 'Bas.Ins.Cal.', 'Data S.Desc.',               &  ! 28-30
    'Ant.Ins.Cal.','Atm.Monitor.', 'Data Modif.','Data File',     &  ! 31-34
    'WVRadiometer','ALMA','Monitoring','Status','VLBI'/              ! 35-39
  !
  data quality /'Unknown','Excellent','Good','Fair','Average',   &
    'Poor','Bad','Awful','Worst','Deleted'/
  !-----------------------------------------------------------------------
  !
  ! Display
  data m_data/0/, m_boxes/0/
  data csub   &
     /'C01','C02','C03','C04','C05','C06','C07','C08','C09','C10','C11','C12',&
      'C13','C14','C15','C16',120*'tbd',              &
      'L01','L02','L03','L04','L05','L06','L07','L08',&
      'L09','L10','L11','L12','L13','L14','L15','L16',120*'tbd'/
  !      DATA CBAS /'12','13','23','14','24','34','15','25','35','45',
  !     &'16','26','36','46','56'/
  !      DATA CTRI /'123','124','134','234',
  !     &'125','135','235','145','245','345',
  !     &'126','136','236','146','246','346','156','256','356','456'/
  data cband /'USB','LSB','SB Ave','SB Ratio','SB Diff'/
  data cpol /'Horizontal','Vertical','both'/
  ! clab moved to clic_xy_code.inc
  !-----------------------------------------------------------------------
  !
  ! Common. CODE1, CODE2, CODE3 are initialized in CLIC_CODE, called by INIT_CLIC
  !-----------------------------------------------------------------------
  !
  ! Modify
  data loaded/.false./
  !-----------------------------------------------------------------------
  !
  ! Antenna panels:
  !
  ! # panels per ring
  !      DATA NPAN/16,32,32,32,32,32/
  !-----------------------------------------------------------------------
  !
  !      DATA R_ABPCCAMP/MM1*1.0/
  data r_abpccamp/ma1*0.0/
  data r_abpccpha/ma1*0.0/
  data r_abpclamp/ma2*0.0/
  !      DATA R_ABPCLAMP/MM2*1.0/
  data r_abpclpha/ma2*0.0/
  !      DATA R_ABPFAMP/MM3*1.0/
  data r_abpfamp/ma3*0.0/
  data r_abpfpha/ma3*0.0/
  !      DATA R_BPCCAMP/MM1*1.0/
  data r_bpccamp/mm1*0.0/
  data r_bpccpha/mm1*0.0/
  !      DATA R_BPCLAMP/MM2*1.0/
  data r_bpclamp/mm2*0.0/
  data r_bpclpha/mm2*0.0/
  !      DATA R_BPFAMP/MM3*1.0/
  data r_bpfamp/mm3*0.0/
  data r_bpfpha/mm3*0.0/
  !
  data r_dmcamp/mm4*1.0/
  data r_dmcpha/mm4*0.0/
  data r_dmlamp/mm5*1.0/
  data r_dmlpha/mm5*0.0/
  data r_dmldph/mm5*0.0/
  !
  !      DATA R_TELES/' '/
  !
  data snbit /32/
  !
  !-----------------------------------------------------------------------
  !
  ! Antenna flags
  data af/   &
    'XC01',   &                 ! 1 	: bad continuum subband 1
    'XC02',   &                 ! 2 	: bad continuum subband 2
    'XC03',   &                 ! 3 	: bad continuum subband 3
    'XC04',   &                 ! 4 	: bad continuum subband 4
    'XC05',   &                 ! 5 	: bad continuum subband 5
    'XC06',   &                 ! 6 	: bad continuum subband 6
    'XC07',   &                 ! 7 	: bad continuum subband 7
    'XC08',   &                 ! 8 	: bad continuum subband 8
    'XC09',   &                 ! 9 	: bad continuum subband 9
    'XC10',   &                 ! 10 	: bad continuum subband 10
    'XL01',   &                 ! 11 	: bad line subband 1
    'XL02',   &                 ! 12 	: bad line subband 2
    'XL03',   &                 ! 13 	: bad line subband 3
    'XL04',   &                 ! 14 	: bad line subband 4
    'XL05',   &                 ! 15	: bad line subband 5
    'XL06',   &                 ! 16	: bad line subband 6
    'XL07',   &                 ! 17	: bad line subband 7
    'XL08',   &                 ! 18	: bad line subband 8
    '19',   &                   ! 19	: reserved
    '20',   &                   ! 20	: reserved
    '21',   &                   ! 21	: reserved
    '22',   &                   ! 22	: reserved
    'TRACKING',   &             ! 23	: reserved
    'REDUCTIO',   &             ! 24	: bad data (aod's REDUCTION)
    'DOPPLER',   &              ! 25	: reserved
    'TIME',   &                 ! 26	: Time discontinuity
    'SATURATI',   &             ! 27	: Saturation
    'SHADOW',   &               ! 28	: Antenna is shadowed
    'LOCK',   &                 ! 29	: Phase-lock
    'TSYS',   &                 ! 30	: Bad TSYS ( too high)
    'POINTING',   &             ! 31	: bad rms
    'DATA'/                     ! 32	: bad data
  !
  ! Baseline flags
  data bf/   &
    'XC01',   &                 ! 1 	: bad continuum subband 1
    'XC02',   &                 ! 2 	: bad continuum subband 2
    'XC03',   &                 ! 3 	: bad continuum subband 3
    'XC04',   &                 ! 4 	: bad continuum subband 4
    'XC05',   &                 ! 5 	: bad continuum subband 5
    'XC06',   &                 ! 6 	: bad continuum subband 6
    'XC07',   &                 ! 7 	: bad continuum subband 7
    'XC08',   &                 ! 8 	: bad continuum subband 8
    'XC09',   &                 ! 9 	: bad continuum subband 9
    'XC10',   &                 ! 10 	: bad continuum subband 10
    'XL01',   &                 ! 11 	: bad line subband 1
    'XL02',   &                 ! 12 	: bad line subband 2
    'XL03',   &                 ! 13 	: bad line subband 3
    'XL04',   &                 ! 14 	: bad line subband 4
    'XL05',   &                 ! 15	: bad line subband 5
    'XL06',   &                 ! 16	: bad line subband 6
    'XL07',   &                 ! 17	: bad line subband 7
    'XL08',   &                 ! 18	: bad line subband 8
    '19',   &                  ! 19	: reserved
    '20',   &                  ! 20	: reserved
    '21',   &                  ! 21	: reserved
    '22',   &                  ! 22	: reserved
    '23',   &                  ! 23	: reserved
    '24',   &                  ! 24	: reserved
    '25',   &                  ! 25	: reserved
    '26',   &                  ! 26	: reserved
    '27',   &                  ! 27	: reserved
    '28',   &                  ! 28	: reserved
    '29',   &                  ! 29	: reserved
    '30',   &                  ! 30	: reserved
    'REDUCTIO',   &            ! 31	: bad data (aod's REDUCTION)
    'DATA'/                    ! 32	: bad data
  ! Spectral flags
  data sf/  & 
    'C01',   &                 ! 1      : bad continuum subband 1
    'C02',   &                 ! 2      : bad continuum subband 2
    'C03',   &                 ! 3      : bad continuum subband 3
    'C04',   &                 ! 4      : bad continuum subband 4
    'C05',   &                 ! 5      : bad continuum subband 5
    'C06',   &                 ! 6      : bad continuum subband 6
    'C07',   &                 ! 7      : bad continuum subband 7
    'C08',   &                 ! 8      : bad continuum subband 8
    'C09',   &                 ! 9      : bad continuum subband 9
    'C10',   &                 ! 10     : bad continuum subband 10
    'C11',   &                 ! 11     : bad continumm subband 11
    'C12',   &                 ! 12     : bad continumm subband 12            
    'C13',   &                 ! 13     : bad continumm subband 13
    'C14',   &                 ! 14     : bad continumm subband 14
    'C15',   &                 ! 15     : bad continumm subband 15
    'C16',   &                 ! 16     : bad continumm subband 16
     120*'tbd', &
    'L01',   &                 ! 17     : bad line subband 1
    'L02',   &                 ! 18     : bad line subband 2
    'L03',   &                 ! 19     : bad line subband 3
    'L04',   &                 ! 20     : bad line subband 4
    'L05',   &                 ! 21     : bad line subband 5
    'L06',   &                 ! 22     : bad line subband 6
    'L07',   &                 ! 23     : bad line subband 7
    'L08',   &                 ! 24     : bad line subband 8
    'L09',   &                 ! 25     : bad line subband 9
    'L10',   &                 ! 26     : bad line subband 10
    'L11',   &                 ! 27     : bad line subband 11
    'L12',   &                 ! 28     : bad line subband 12
    'L13',   &                 ! 29     : bad line subband 13
    'L14',   &                 ! 30     : bad line subband 14
    'L15',   &                 ! 31     : bad line subband 15
    'L16',   &                 ! 32     : bad line subband 16
     120*'tbd'/

  !------------------------------------------------------------------------
  !
  ! Number.inc
  !      DATA BASANT /
  !     &1, 0, 0, 0, 0,
  !     &2, 3, 0, 0, 0,
  !     &4, 5, 6, 0, 0,
  !     &7, 8, 9,10, 0,
  !     &11,12,13,14,15/
  ! antbas(1,i) is the first ant. of bas. i
  ! antbas(2,i) is the second ant. of bas. i
  !      DATA ANTBAS /
  !     &1,2,
  !     &1,3, 2,3,
  !     &1,4, 2,4, 3,4,
  !     &1,5, 2,5, 3,5, 4,5,
  !     &1,6, 2,6, 3,6, 4,6, 5,6/
  !
  ! Triangles (6*5*4 | 6 = 20)
  !      DATA TRIANT /
  !     &1, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
  !     &2, 0, 0, 0,  3, 4, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,
  !     &5, 0, 0, 0,  6, 7, 0, 0,  8, 9,10, 0,  0, 0, 0, 0,
  !     &11, 0, 0, 0, 12,13, 0, 0, 14,15,16, 0, 17,18,19,20/
  ! anttri(1,i) is the first ant. of tri. i
  ! anttri(2,i) is the second ant. of tri. i
  ! anttri(3,i) is the third ant. of tri. i
  !      DATA ANTTRI /
  !     &1,2,3,
  !     &1,2,4, 1,3,4, 2,3,4,
  !     &1,2,5, 1,3,5, 2,3,5, 1,4,5, 2,4,5, 3,4,5,
  !     &1,2,6, 1,3,6, 2,3,6, 1,4,6, 2,4,6, 3,4,6,
  !     &1,5,6, 2,5,6, 3,5,6, 4,5,6/
  ! bastri(1,i) is side 12 of triangle i
  ! bastri(2,i) is side 13 of triangle i
  ! bastri(3,i) is side 23 of triangle i
  !      DATA BASTRI /
  !     &1,2,3,
  !     &1,4,5,   2,4,6,   3,5,6,
  !     &1,7,8,   2,7,9,   3,8,9,   4,7,10,  5,8,10,  6,9,10,
  !     &1,11,12, 2,11,13, 3,12,13, 4,11,14, 5,12,14, 6,13,14,
  !     &7,11,15, 8,12,15, 9,13,15, 10,14,15/
  !
  ! Quadrilateres (6*5*4*3 | 24 = 15)
  !      DATA QUADANT /
  !     &1, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..34
  !     &0, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..44
  !     &0, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..54
  !
  !     &2, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..35
  !     &3, 0, 0,   4, 5, 0,   0, 0, 0,     ! ..45
  !     &0, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..55
  !
  !     &6, 0, 0,   0, 0, 0,   0, 0, 0,     ! ..36
  !     &7, 0, 0,   8, 9, 0,   0, 0, 0,     ! ..46
  !     &10, 0, 0,  11,12, 0,  13,14,15/    ! ..56
  ! antquad(1,i) is the first ant. of quad. i
  ! antquad(2,i) is the second ant. of quad. i
  ! antquad(3,i) is the third ant. of quad. i
  ! antquad(4,i) is the 4th ant. of quad. i
  !      DATA ANTQUAD /
  !     &1,2,3,4,
  !     &1,2,3,5, 1,2,4,5, 1,3,4,5, 2,3,4,5,
  !     &1,2,3,6,
  !     &1,2,4,6, 1,3,4,6, 2,3,4,6,
  !     &1,2,5,6, 1,3,5,6, 2,3,5,6, 1,4,5,6, 2,4,5,6, 3,4,5,6/
  ! basquad(1,i) is side 12 of quadr. i
  ! basquad(2,i) is side 13 of quadr. i
  ! basquad(3,i) is side 23 of quadr. i
  ! basquad(4,i) is side 14 of quadr. i
  ! basquad(5,i) is side 24 of quadr. i
  ! basquad(6,i) is side 34 of quadr. i
  !      DATA BASQUAD /
  !     &1,2,3,4,5,6,
  !     &1,2,3,7,8,9, 1,4,5,7,8,10, 2,4,6,7,9,10, 3,5,6,8,9,10,
  !     &1,2,3,11,12,13,
  !     &1,4,5,11,12,14, 2,4,6,11,13,14, 3,5,6,12,13,14,
  !     &1,5,8,11,12,15, 2,5,9,11,13,15, 3,8,9,12,13,15,
  !     &4,7,10,11,14,15, 5,8,10,12,14,15, 6,9,10,13,14,15/
  !--------------------------------------------------------------------
  !
  ! Stations (1989 values)
  data stat89 /   &
    11.8205,     -55.0239,     -11.9726,   &   ! N001
    6.2035,      -55.3708,      -6.2833,   &   ! N002
    0.5857,      -55.7221,      -0.5933,   &   ! N003
    3*0.,   &
    -10.6405,     -56.4189,      10.7774,   &  ! N005
    3*0.,   &
    -21.8693,     -57.1182,      22.1506,   &  ! N007
    3*0.,   &
    -33.1017,     -57.8165,      33.5275,   &  ! N009
    3*0.,   &
    -44.3372,     -58.5130,      44.9075,   &  ! N011
    3*0.,   &
    -55.5599,     -59.2169,      56.2746,   &  ! N013
    3*0.,   &
    -66.7881,     -59.9200,      67.6472,   &  ! N015
    3*0.,   &
    -78.0183,     -60.6193,      79.0218,   &  ! N017
    6*0.,   &
    -94.8628,     -61.6698,      96.0830,   &  ! N020
    24*0.,   &
    -145.3956,    -64.8191,     147.2738,   &  ! N029
    210*0.,   &                ! -> n99
    17.4321,      -54.6713,     -17.6563,   &  ! W000
    16.2166,      -62.4867,     -16.4257,   &  ! W001
    9*0.,   &
    11.3547,      -93.7218,     -11.5031,   &  ! W005
    6*0.,   &
    7.7097,      -117.1541,      -7.8126,   &  ! W008
    6.4934,      -124.9644,      -6.5812,   &  ! W009
    5.2791,      -132.7718,      -5.3517,   &  ! W010
    3*0.,   &
    2.8491,      -148.3855,      -2.8914,   &  ! W012
    21*0.,   &
    -6.8729,     -210.8616,       6.9519,   &  ! W020
    6*0.,   &
    -10.5186,    -234.2902,      10.6431,   &  ! W023
    9*0.,   &
    -15.3796,    -265.5282,      15.5647,   &  ! W027
    216*0.,   &                ! -> w99
    9*0.,   &
    21.0774,      -31.2325,     -21.3471,   &  ! E003
    22.2933,      -23.4317,     -22.5781,   &  ! E004
    15*0.,   &
    29.5829,       23.4363,     -29.9587,   &  ! E010
    3*0.,   &
    32.0173,       39.0597,     -32.4235,   &  ! E012
    9*0.,   &
    36.8777,       70.2988,     -37.3445,   &  ! E016
    3*0.,   &
    39.3095,       85.9188,     -39.8067,   &  ! E018
    12*0.,   &
    45.3885,      124.9789,     -45.9615,   &  ! E023
    46.6036,      132.7861,     -47.1917/  ! E024
  !
  ! Stations (1996 improved values)
  data stat96 /   &
    11.8205,     -55.0239,      -11.9726,   &  ! N001
    6.2035,      -55.3708,       -6.2833,   &  ! N002
    0.5854,      -55.7266,       -0.5917,   &  ! N003
    3*0.,   &
    -10.6405,    -56.4212,       10.7788,   &  ! N005
    3*0.,   &
    -21.8693,    -57.1182,       22.1506,   &  ! N007
    3*0.,   &
    -33.1009,    -57.8204,       33.5292,   &  ! N009
    3*0.,   &
    -44.3362,    -58.5142,       44.9087,   &  ! N011
    3*0.,   &
    -55.5577,    -59.2187,       56.2762,   &  ! N013
    3*0.,   &
    -66.7869,    -59.9217,       67.6495,   &  ! N015
    3*0.,   &
    -78.0162,    -60.6214,       79.0234,   &  ! N017
    6*0.,   &
    -94.8617,    -61.6689,       96.0852,   &  ! N020
    24*0.,   &
    -145.4009,   -64.8186,      147.2730,   &  ! N029
    210*0.,   &                ! -> n99
    17.4321,     -54.6713,      -17.6563,   &  ! W000
    16.2166,     -62.4867,      -16.4257,   &  ! W001
    9*0.,   &
    11.3516,     -93.7223,      -11.5027,   &  ! W005
    6*0.,   &
    7.7097,     -117.1541,       -7.8126,   &  ! W008
    6.4910,     -124.9651,       -6.5802,   &  ! W009
    5.2791,     -132.7718,       -5.3517,   &  ! W010
    3*0.,   &
    2.8470,     -148.3862,       -2.8913,   &  ! W012
    21*0.,   &
    -6.8770,    -210.8716,        6.9552,   &  ! W020
    6*0.,   &
    -10.5222,   -234.3136,       10.6445,   &  ! W023
    9*0.,   &
    -15.3889,   -265.5573,       15.5715,   &  ! W027
    216*0.,   &                ! -> w99
    9*0.,   &
    21.0765,     -31.2325,      -21.3479,   &  ! E003
    22.2903,     -23.4325,      -22.5789,   &  ! E004
    15*0.,   &
    29.5834,      23.4354,      -29.9591,   &  ! E010
    3*0.,   &
    32.0173,      39.0597,      -32.4235,   &  ! E012
    9*0.,   &
    36.8747,      70.2990,      -37.3474,   &  ! E016
    3*0.,   &
    39.3047,      85.9184,      -39.8089,   &  ! E018
    12*0.,   &
    45.3885,     124.9789,      -45.9615,   &  ! E023
    46.5958,     132.7866,      -47.1938/  ! E024
  !
  ! Stations (1999 improved values)
  data stat99 /   &
    11.8205,     -55.0239,      -11.9726,   &  ! N001
    6.2035,      -55.3708,       -6.2833,   &  ! N002
    0.5849,      -55.7232,       -0.5926,   &  ! N003
    3*0.,   &
    -10.6409,    -56.4214,       10.7789,   &  ! N005
    3*0.,   &
    -21.8689,    -57.1214,       22.1518,   &  ! N007
    3*0.,   &
    -33.1008,    -57.8196,       33.5285,   &  ! N009
    3*0.,   &
    -44.3355,    -58.5136,       44.9087,   &  ! N011
    3*0.,   &
    -55.5592,    -59.2193,       56.2766,   &  ! N013
    3*0.,   &
    -66.7863,    -59.9207,       67.6481,   &  ! N015
    3*0.,   &
    -78.0155,    -60.6202,       79.0233,   &  ! N017
    6*0.,   &
    -94.8616,    -61.6677,       96.0841,   &  ! N020
    24*0.,   &
    -145.4009,   -64.8169,      147.2738,   &  ! N029
    48*0.,   &
    -240.8684,   -70.7531,      243.9625,   &  ! N046
    159*0.,   &                ! -> n99
    17.4321,     -54.6713,      -17.6563,   &  ! W000
    16.2163,     -62.4889,      -16.4266,   &  ! W001
    9*0.,   &
    11.3515,     -93.7227,      -11.5024,   &  ! W005
    6*0.,   &
    7.7069,     -117.1487,       -7.8139,   &  ! W008
    6.4919,     -124.9643,       -6.5813,   &  ! W009
    5.2780,     -132.7724,       -5.3512,   &  ! W010
    3*0.,   &
    2.8484,     -148.3853,       -2.8915,   &  ! W012
    21*0.,   &
    -6.8766,    -210.8708,        6.9551,   &  ! W020
    6*0.,   &
    -10.5202,   -234.3135,       10.6451,   &  ! W023
    9*0.,   &
    -15.3883,   -265.5590,       15.5711,   &  ! W027
    57*0.,  &
    -38.196441, -421.768313,     41.655322, &  ! W047
    156*0., &                           ! -> w99  
    9*0.,   &
    21.0757,     -31.2318,      -21.3473,   &  ! E003
    22.2913,     -23.4309,      -22.5785,   &  ! E004
    15*0.,   &
    29.5828,      23.4370,      -29.9600,   &  ! E010
    3*0.,   &
    32.0177,      39.0608,      -32.4249,   &  ! E012
    9*0.,   &
    36.8755,      70.3000,      -37.3469,   &  ! E016
    3*0.,   &
    39.3054,      85.9189,      -39.8081,   &  ! E018
    12*0.,   &
    45.3817,     124.9799,      -45.9624,   &  ! E023
    46.5964,     132.7882,      -47.1932,   &  ! E024
    102*0.,   &
    89.123,      406.166,       -90.285,    &  ! E059
    24*0.,     &
    100.0733,    476.4759,     -101.3757,   &  ! E068
    159*0.,    &
    162.875,     898.241,      -170.62,     &  ! E122
    75*0.,     &
    192.551,    1101.32,       -204.522,    &  ! E148
    36*0.,     &
    208.35,     1202.86,       -220.524 /      ! E161 
  !
  !--------------------------------------------------------------------------
  ! Recognised Configurations (32)
  !5D+N13 : w05 w00 e03 n05 n09 n13
  !6C     : w12 e10 e16 n02 n09 n20
  !6B     : w12 e04 e23 n07 n17 n29
  data config_name/   &
    ! 12-antenna configuration
    '12A', '12Aa','12B', '12C', '12Ca', '12D',       &
    ! 11-antenna configuration
    '11A', '11C', '11D',       &
    ! 10-antenna configuration
    '10A', '10C', '10Cw', '10D',       &
    ! 9-antenna configuration
    '9A', '9C', '9D',          &
    ! 8-antenna configuration
    '8A', '8C', '8D',          &
    ! 7-antenna configuration
    '7A', '7B', '7C', '7D',    &
    ! 6-antenna with extended baselines (starting Nov. 2005)
    '6Aq','6Bq','6Cq','6Dq',   &
    ! 6-antenna (provisional -- 2002)
    '6Ap','6Bp','6Cp','6Dp',   &
    ! 5-antenna (2006)
    '5Dq',   &
    ! 5-antenna
    '5A', '5B1','5B2','5C1','5C2','5D',   &
    ! 4-antenna (new and old)
    '4A1','4A2','4B1','4B2','4C1','4C2','4D1','4D2',   &
    '4B1o','4C1o',   &
    ! 3-antenna
    '3B1','3B2','3B3','3C1','3C2','3C3','3D1','3D2'/
  data config/   &
    !
    'W047W027W009E161E148E122E068E059E023N046N029N020', & ! 12A
    'W047W027W009W005E068E024E018E010N046N029N020N007', & ! 12Aa
    'W027W020W009W005E059E024E018E010N046N029N017N011', & ! 12B
    'W023W020W009W005E023E018E010E003N029N020N011N007', & ! 12C 
    'W027W023W008W005E023E018E010E004N029N017N011N009', & ! 12Ca
    'W020W012W009W005E023E018E010E003N017N013N007N002', & ! 12D
    !
    'W027W023W012E068E024E018E010N046N029N017N002', & ! 11A
    'W023W020W010W005E018E012E003N029N020N011N007', & ! 11C
    'W020W012W009W005E016E012E003N017N013N009N002', & ! 11D
    !
    'W027W023W008E068E024E016E003N046N029N020', & ! 10A
    'W023W020W009E023E018E010E003N020N017N011', & ! 10C
    'W027W020W009E016E012E004N029N020N011N008', & ! 10Cw
    'W012W008W005E010E004N017N013N009N005N002', & ! 10D
    ! 
    'W027W010E068E024E012E004N046N029N020', & ! 9A
    'W020W012W009E016E010E003N029N020N011', & ! 9C
    'W012W009W005E010E004N013N009N005N002', & ! 9D
    !
    'W027W009E068E023E012N046N029N020', & ! 8A
    'W023W020W005E023E016E004N020N013', & ! 8C
    'W012W008W005E010E004N013N009N002', & ! 8D
    !
    'W027W010E068E024E012N046N029', & !    7A
    'W023W005E024E018E004N046N020', & !    7B
    'W012W009E018E012E004N017N011', & !    7C
    'W008W005E010E004N011N007N002', & !    7D
    !
    'W027E068N046E024E004N029',   &  !    6Aq
    'W012W027N046E023E012N020',   &  !    6Bq
    'W012E010N017N011E004W009',   &  !    6Cq
    'W008E003N007N011N002W005',   &  !    6Dq
    !
    'E016E023W023W027N013N029',   &  !    6Ap
    'E023E004W012N007N017N029',   &  !    6Bp
    'E016E010W012N002N009N020',   &  !    6Cp
    'E003W000W005N005N009N013',   &  !    6Dp
    !
    'E003W005W008N007N011',   &     !    5Dq
    !
    'E024E016W023W027N029',   &     !    5A
    'E023E018W012N013N020',   &     !    5B1
    'E012W012W023N017N029',   &     !    5B2
    'E010W001W005N007N013',   &     !    5C1
    'E010W009W012N005N015',   &     !    5C2
    'E003W000W005N005N009',   &     !    5D
    !
    'E023W020W027N029',   &        !    4A1
    'E004E024W023N029',   &        !    4A2
    'E024E016N011N017',   &        !    4B1
    'E024W012W009N020',   &        !    4B2
    'W020W005N009N017',   &        !    4C1
    'E010W009N003N015',   &        !    4C2
    'E003W005W000N005',   &        !    4D1
    'E003W010W001N009',   &        !    4D2
    'E024E018N011N017',   &        !    4B1o
    'E010W012N005N013',   &        !    4C1o
    !
    'E018N020W005',   &           !    3B1
    'E024N020W012',   &           !    3B2
    'E024W008N017',   &           !    3B3
    'N005N013W012',   &           !    3C1
    'N003W008W005',   &           !    3C2
    'N013W000E018',   &           !    3C3
    'N020W012E010',   &           !    3D1
    'N017N007N001'/               !    3D2
!--------------------------------------------------------------------------
end block data clicda
