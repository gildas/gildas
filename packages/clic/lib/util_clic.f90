function header_data(ix)
  logical :: header_data            !
  integer :: ix                     !
  ! Global
  include 'clic_xy_code.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !
  header_data = .false.
  if (ix.eq.xy_time) then
    header_data = .true.
  elseif (ix.eq.xy_scan) then
    header_data = .true.
  elseif (ix.eq.xy_hour) then
    header_data = .false.
  elseif (ix.eq.xy_decli) then
    header_data = .false.
  elseif (ix.eq.xy_atm_val) then
    header_data = .true.
  elseif (ix.eq.xy_atm_ref) then
    header_data = .true.
  elseif (ix.eq.xy_cal_pha) then
    if (do_phase_antenna) then
      header_data = .true.
    else
      header_data = .false.
    endif
  elseif (ix.eq.xy_cal_amp) then
    if (do_amplitude_antenna) then
      header_data = .true.
    else
      header_data = .false.
    endif
  ! more work needed here
  elseif (ix.eq.xy_rf_pha) then
    header_data = .true.
  elseif (ix.eq.xy_rf_amp) then
    header_data = .true.
  elseif (ix.eq.xy_flux) then
    header_data = .true.
  elseif (ix.eq.xy_lo1_f) then
    header_data = .true.
  elseif (ix.eq.xy_lo1_ref) then
    header_data = .true.
  elseif (ix.eq.xy_doppl) then
    header_data = .true.
  elseif (ix.eq.xy_az_co) then
    header_data = .true.
  elseif (ix.eq.xy_el_co) then
    header_data = .true.
  elseif (ix.eq.xy_foc_c) then
    header_data = .true.
  elseif (ix.eq.xy_gain) then
    header_data = .true.
  elseif (ix.eq.xy_opaci) then
    header_data = .true.
  elseif (ix.eq.xy_trec) then
    header_data = .true.
  elseif (ix.eq.xy_tsys) then
    header_data = .true.
  elseif (ix.eq.xy_pamb) then
    header_data = .true.
  elseif (ix.eq.xy_tamb) then
    header_data = .true.
  elseif (ix.eq.xy_humid) then
    header_data = .true.
  elseif (ix.eq.xy_windav) then
    header_data = .true.
  elseif (ix.eq.xy_windirav) then
    header_data = .true.
  elseif (ix.eq.xy_wintop) then
    header_data = .true.
  elseif (ix.eq.xy_windirtop) then
    header_data = .true.
  elseif (ix.eq.xy_numbe) then
    header_data = .true.
  elseif (ix.eq.xy_quali) then
    header_data = .true.
  !      ELSEIF (IX.EQ.XY_AZIMU) THEN
  !         header_data = .true.
  !         X = R_AZ*180./PI + DH_OFFLAM(IA)/3600.
  !      ELSEIF (IX.EQ.XY_ELEVA) THEN
  !         header_data = .true.
  !         X = R_EL*180./PI + DH_OFFBET(IA)/3600.
  elseif (ix.ge.xy_water .and. ix.le.xy_95) then
    header_data = .true.
  elseif (ix.eq.xy_wvrqual) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtatm) then
    header_data = .true.
  elseif (ix.eq.xy_wvrdcloud3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrdcloud2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrdcloud1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrliq1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrliq2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrliq3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfpath1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfpath2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfpath3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrdpath1) then ! Was WVRPATH3 which does not exist
    header_data = .true.
  elseif (ix.eq.xy_wvrdpath2) then ! Was WVRPATH2
    header_data = .true.
  elseif (ix.eq.xy_wvrdpath3) then ! Was WVRPATH3
    header_data = .true.
  elseif (ix.eq.xy_wvrtsys1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtsys2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtsys3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrpath) then
    header_data = .true.
  elseif (ix.eq.xy_wvrh2o) then
    header_data = .true.
  elseif (ix.eq.xy_wvrmode) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfeff3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfeff2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrfeff1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtrec1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtrec2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtrec3) then
    header_data = .true.
  elseif (ix.eq.xy_wvramb1) then
    header_data = .true.
  elseif (ix.eq.xy_wvramb2) then
    header_data = .true.
  elseif (ix.eq.xy_wvramb3) then
    header_data = .true.
  elseif (ix.eq.xy_wvraver1) then
    header_data = .true.
  elseif (ix.eq.xy_wvraver2) then
    header_data = .true.
  elseif (ix.eq.xy_wvraver3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrref1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrref2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrref3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtcal1) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtcal2) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtcal3) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtpel) then
    header_data = .true.
  elseif (ix.eq.xy_wvrtamb) then
    header_data = .true.
  elseif (ix.eq.xy_tdewar1) then
    header_data = .true.
  elseif (ix.eq.xy_tdewar2) then
    header_data = .true.
  elseif (ix.eq.xy_tdewar3) then
    header_data = .true.
  elseif (ix.eq.xy_hiq_atten1) then
    header_data = .true.
  elseif (ix.eq.xy_hiq_atten2) then
    header_data = .true.
  elseif (ix.eq.xy_attenuation) then
    header_data = .true.
  elseif (ix.eq.xy_eleva) then
    header_data = .true.
  elseif (ix.eq.xy_azimu) then
    header_data = .true.
  endif
end function header_data
!
function angle_data(xy)
  logical :: angle_data             !
  integer :: xy                     !
  ! Global
  include 'clic_xy_code.inc'
  !
  angle_data = (xy.eq.xy_phase) .or. (xy.eq.xy_cable)   &
    .or. (xy.eq.xy_lo_ph) .or. (xy.eq.xy_lo_ra)   &
    .or. (xy.eq.xy_rms_p) .or. (xy.eq.xy_cal_pha)   &
    .or. (xy.eq.xy_rf_pha) .or. (xy.eq.xy_atm_pha)   &
    .or. (xy.eq.xy_atm_cor) .or. (xy.eq.xy_atm_unc)   &
    .or. (xy.eq.xy_parang) .or. (xy.eq.xy_wvr_pha_m)   &
    .or. (xy.eq.xy_wvr_pha_e) .or. (xy.eq.xy_wvr_pha_c) &
    .or. (xy.eq.xy_cable_alternate)
end function angle_data
!
function corr_data(x,y)
  logical :: corr_data              !
  integer :: x                      !
  integer :: y                      !
  ! Global
  include 'clic_xy_code.inc'
  !
  corr_data = (x.le.xy_imag).or.(y.le.xy_imag)
  corr_data = corr_data.or.(x.eq.xy_atm_unc).or.(x.eq.xy_atm_cor)
  corr_data = corr_data.or.(y.eq.xy_atm_unc).or.(y.eq.xy_atm_cor)
end function corr_data
!
function auto_data(x,y)
  logical :: auto_data              !
  integer :: x                      !
  integer :: y                      !
  ! Global
  include 'clic_xy_code.inc'
  !
  auto_data = (x.eq.xy_auto).or.(y.eq.xy_auto)
  if (.not.auto_data) return
  auto_data = .not.((x.le.xy_imag).or.(y.le.xy_imag))
end function auto_data
!
function ante_data(x)
  logical :: ante_data              !
  integer :: x                      !
  ! Global
  include 'clic_xy_code.inc'
  !
  ante_data = (x.ge.xy_delay .and. x.le.xy_lo_ra) .or.   &
    (x.ge.xy_az_co.and. x.le.xy_tsys) .or.   &
    (x.eq.xy_trec) .or. (x.eq.xy_tsys)  .or.   &
    (x.ge.xy_azimu .and. x.le.xy_water) .or.   &
    (x.eq.xy_auto) .or. (x.ge.xy_t10 .and. x.le.xy_t14) .or.   &
    (x.ge.xy_az_co .and.  x.le.xy_airmass)
end function ante_data
!
function base_data(x)
  logical :: base_data              !
  integer :: x                      !
  ! Global
  include 'clic_xy_code.inc'
  !
  base_data = (x.ge.xy_u).and.(x.le.xy_angle)
end function base_data
!
function if_data(x)
  logical :: if_data
  integer :: x
  ! Global
  include 'clic_xy_code.inc'
  !
  if_data = (x.eq.xy_dh_anttp) .or. (x.eq.xy_attenuation) .or. &
            (x.eq.xy_laser_level)
end function if_data 
!
function bb_data(x)
  logical :: bb_data
  integer :: x
  ! Global
  include 'clic_xy_code.inc'
  !
  bb_data = (x.ge.xy_gain  .and. x.le.xy_emis)    .or.  &
            (x.eq.xy_total)                       .or.  &
            (x.eq.xy_trec)                        .or.  &
            (x.ge.xy_tchop .and. x.le.xy_tsys)    .or.  &
            (x.ge.xy_water .and. x.le.xy_tcold)   .or.  &
            (x.eq.xy_atm)                         .or.  &
            (x.ge.xy_tatm_s .and. x.le.xy_tatm_i) .or.  &
            (x.eq.xy_scale)                       .or.  &
            (x.eq.xy_delay)                       
end function bb_data 
!
!      SUBROUTINE GR8_TRI(X,IT,N,error)
!------------------------------------------------------------------------
! Compatibility routine for old programs using alternate returns.
! Should ultimately disapear
!------------------------------------------------------------------------
!      REAL*8 X(*)
!      INTEGER IT(*),N
!*
!      LOGICAL ERROR
!*
!      ERROR = .FALSE.
!      CALL GR8_TRIE(X,IT,N,ERROR)
!      IF (ERROR) RETURN
!      END
