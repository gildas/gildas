subroutine write_sdm_FreqOffset(holodata, error)
  !---------------------------------------------------------------------
  ! Write the FreqOffset SDM table.
  !
  !     int sdm_addFreqOffsetRow (int * antennaId, int * feedId, int *
  !     spectralWindowId, long long * timeInterval, double * offset)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_FreqOffset
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  type(FreqOffsetRow) :: row
  type(FreqOffsetKey) :: key
  real*8 offset, fConv
  integer ia, ic, isb, ilc, ireturn, sdm_addFreqOffsetRow
  character sdmTable*12
  parameter (sdmTable='FreqOffset')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  fConv = 1d6
  !     loop on antennas
  do ia=1, r_nant
    !
    !     Normal case
    !
    if (.not. holodata) then
      !     loops on spectral windows:
      !     loop on side bands, line/continuum
      do isb = 1, r_lnsb
        !     loop on line subbands
        do ic = 1, r_lband
          do ilc = 1, 2
            !     not sure what to be put as an offset...  I'm suppose this is the
            !     way to contain the variable frequency offset in the image band at
            !     Bure.  In that case it needs to be recalculated here.
            !
            !     finally I put here the time variable part of the frequency...
            !
            ! USB isb=1, LSB isb=2 in CLIC.
            if (.not.new_receivers) then
              r_flo2(ic) = r_flo1_ref
            endif
            if (isb.eq.1) then
              row%offset = (r_flo1-freq_lo1+(r_flo2(ic)   &
                -freq_lo2))* fConv
            else
              row%offset = (r_flo1-freq_lo1-(r_flo2(ic)   &
                -freq_lo2))* fConv
            endif
            key%antennaId = antenna_Id(ia)
            key%feedId = feed_Id
            key%spectralWindowId = spectralWindow_Id(isb, ic,   &
              ilc)
            key%timeInterval =   &
              ArrayTimeInterval(time_Interval(1)   &
              ,time_Interval(2))
            !
            ! Why was this done???
            !                     key%timeInterval = ArrayTimeInterval(time_Start(1)
            !     &                    ,time_Start(2))
            call addFreqOffsetRow(key, row, error)
            if (error) return
          enddo
        enddo
        key%antennaId = antenna_Id(ia)
        key%feedId = feed_Id
        key%spectralWindowId = totPowSpectralWindow_Id(isb)
        row%offset = 0
        call addFreqOffsetRow(key, row, error)
        if (error) return
      enddo
    else
      !
      !     holography
      !
      row%offset = 0
      key%antennaId = antenna_Id(ia)
      key%feedId = feed_Id
      key%spectralWindowId = totPowSpectralWindow_Id(1)
      key%timeInterval = ArrayTimeInterval(time_Start(1)   &
        ,time_Start(2))
      call addFreqOffsetRow(key, row, error)
      if (error) return
    endif
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_FreqOffset
!---------------------------------------------------------------------
subroutine get_sdm_FreqOffset(error)
  !---------------------------------------------------------------------
  ! Read the FreqOffset SDM table. NOT USED ???
  !
  !     Key: (antennaId, feedId, spectralWindowId, timeInterval)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_FreqOffset
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(FreqOffsetRow) :: row
  type(FreqOffsetKey) :: key
  !$$$      real*8 offset, fConv
  real*8 fConv
  !$$$      integer ia, i, ic, isb, ilc, ireturn, sdm_getFreqOffsetRow
  integer ia, i, ic, isb, ilc
  character sdmTable*12
  parameter (sdmTable='FreqOffset')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  fConv = 1d6
  !     loop on antennas
  !      print *, 'ddId_Size, (swId_List(i), i=1,ddId_Size)'
  !      print *, ddId_Size, (swId_List(i), i=1,ddId_Size)
  !cc      feed_Id = 0
  !      print *, 'feed_Id ',feed_Id
  do ia=1, r_nant
    do i=1, ddId_Size
      !
      !     finally I put here the time variable part of the frequency...
      !
      ! USB isb=1, LSB isb=2 in CLIC.
      !            print *, 'feed_Id ',feed_Id
      key%antennaId = antenna_Id(ia)
      key%feedId = feed_Id
      key%spectralWindowId = swId_List(i)
      key%timeInterval = ArrayTimeInterval(time_Interval(1)   &
        ,time_Interval(2))
      !            print *, 'key%feedId ', key%feedId
      !            print *, 'key%timeInterval ', key%timeInterval
      call getFreqOffsetRow(key, row, error)
      if (error) return
    !$$$
    !$$$            ireturn = sdm_getFreqOffsetRow(antenna_Id(ia), feed_Id ,
    !$$$     &           swId_List(i), time_Interval, offset)
    !$$$            print *, row%offset/fconv, r_flo1, r_flo2
    !$$$            if (ireturn.lt.0) then
    !$$$               call sdmMessageI(8,3,sdmTable
    !$$$     &              ,'Error in sdm_addFreqOffsetRow',ireturn)
    !$$$               goto 99
    !$$$            endif
    !
    !     we assume this is not antenna dependent...
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_FreqOffset
!---------------------------------------------------------------------
