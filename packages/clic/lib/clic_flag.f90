subroutine clic_flag(line,error)
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC	Command CLIC\FLAG f1 f2 ... [/ANTENNA a1 a2 ... ]
  !		[/BASELINE b1 b2 ...] [/RESET]
  ! changes the flags in the observation and record currently in memory
  ! (to be used in a MODIFY DATA_HEADER procedure)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_dheader.inc'
  include 'clic_par.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, j, k ,afl(mnant), bfl(mnbas)
  logical :: saf(2*mbands,mnant), sbf(2*mbands,mnbas)
  logical :: reset
  character(len=132) :: out
  character(len=164) :: ch
  !------------------------------------------------------------------------
  ! Code:
  ! Reset all COMMAND flags: otherwise the system has memory !
  error = .false.
  do i=1, r_nant
    afl(i) = 0
    saf(:,i) = .false.
  enddo
  do i=1, r_nbas
    bfl(i) = 0
    sbf(:,i) = .false.
  enddo
  call get_flags(line,1,1,2,3,-1,-1,afl,bfl,saf,sbf,reset,error)
  if (error) return
  !
  ! Loop on antennas
  do i=1, r_nant
    if (afl(i).ne.0) then
      if (reset) then
        dh_aflag(i) = iand(dh_aflag(i),not(afl(i)))
      else
        dh_aflag(i) = ior(dh_aflag(i),afl(i))
      endif
      r_dmaflag(i) = dh_aflag(i)
      r_presec(modify_sec) = .true.
    endif
    do j=1, 2*mbands
      if (saf(j,i)) then
        if (reset) then
          dh_saflag(j,i) = .false.
        else 
          dh_saflag(j,i) = .true.
        endif
!!      r_dmaflag(i) = dh_aflag(i)
!!      r_presec(modify_sec) = .true.
      endif 
    enddo
  enddo
  !
  ! Baselines
  do i=1, r_nbas
    if (bfl(i).ne.0) then
      if (reset) then
        dh_bflag(i) = iand(dh_bflag(i),not(bfl(i)))
      else
        dh_bflag(i) = ior(dh_bflag(i),bfl(i))
      endif
      r_dmbflag(i) = dh_bflag(i)
      r_presec(modify_sec) = .true.
    endif
    do j=1, 2*mbands
      if (sbf(j,i)) then
        if (reset) then
          dh_sbflag(j,i) = .false.
        else
          dh_sbflag(j,i) = .true.
        endif   
!!      r_dmbflag(i) = dh_bflag(i)
!!      r_presec(modify_sec) = .true.
      endif
    enddo 
  enddo
  !
  call list_flags(r_nant, r_nbas, dh_aflag, dh_bflag, &
    dh_saflag, dh_sbflag, out)
  write(ch,1000) r_scan, dh_dump, out(1:lenc(out))
  call message(6,1,'CLIC_FLAG',ch(1:lenc(ch)))
  return
  !
99 error = .true.
  return
1000 format(' Scan ',i4,' record ',i3,' - ',a)
end subroutine clic_flag
!
subroutine get_flags(line,iarg,iao,ibo,iro,ilo,iso,afl,bfl,saf,sbf, &
    reset, error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  include 'clic_parameter.inc'
  character(len=*) :: line          !
  integer :: iarg                   !
  integer :: iao                    !
  integer :: ibo                    !
  integer :: iro                    !
  integer :: ilo                    !
  integer :: iso                    !
  integer :: afl(mnant)             !
  integer :: bfl(mnbas)             !
  logical :: saf(2*mbands,mnant)             !
  logical :: sbf(2*mbands,mnbas)             ! 
  logical :: reset                  !
  logical :: error                  !
  ! Global
  include 'clic_display.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: n, i, j, ib(mnbas), nb, narg, mask, nch, k, l
  logical :: antenna, baseline, spectral, unspectral, all, allspectral
  logical :: smask(2*mbands), ssmask
  character(len=3) :: arg
  character(len=12) :: arg1
  character(len=6) :: kw
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  spectral = .false.
  unspectral  = .false.
  do i=1, mnant
    afl(i) = 0
    saf(:,i) = .false.
  enddo
  do i=1, mnbas
    bfl(i) = 0
    sbf(:,i) = .false.
  enddo
  mask = 0
  smask = .false.
  baseline = sic_present(ibo,0)
  antenna = sic_present(iao,0)
  if (sic_narg(0).ge.iarg) then
    do i=iarg, sic_narg(0)
      arg1 = '*'
      if (antenna) then
        call clic_kw(line,0,i,arg1,n,af,maf,.false.,error,.false.)
      elseif (baseline) then
        call clic_kw(line,0,i,arg1,n,bf,mbf,.false.,error,.false.)
      endif
      if (error) then
        call clic_kw(line,0,i,arg1,n,sf,msf,.true.,error,.true.)
        if (error) return
        spectral = .true.
        smask(n) = .true.
      else
        unspectral = .true.
        mask = ior(mask,ishft(1,n-1))
      endif
    enddo
  endif
  reset = sic_present(iro,0)
  all = sic_present(ilo,0)
  if (all) then
    spectral = .true.
    unspectral = .true.
    do i=1, 32
      mask = ibset(mask,i)
    enddo
    do i=1, 2*mbands
      smask(i) = .true.
    enddo
    ssmask = .true.
  endif
  allspectral = sic_present(iso,0)
  if (allspectral) then
    spectral = .true.
    do i=1, 2*mbands
      smask(i) = .true.
    enddo
    ssmask = .true.
  endif
  !
  ! Antenna
  if (antenna) then
    !
    ! Get antenna(s)
    narg = sic_narg(iao)
    call sic_ke(line,iao,1,arg,nch,.true.,error)
    if (error) return
    if (arg(1:3).eq.'ALL') then
      nb = mnant
      do i=1, mnant
        ib(i) = i
      enddo
    else
      nb = narg
      do i=1, narg
        call sic_i4(line,iao,i,n,.false.,error)
        if (error) return
        if (n.lt.1 .or. n.gt.mnant) then
          call message(6,3,'CLIC_FLAG','Invalid antenna')
          error = .true.
          return
        endif
        ib(i) = n
      enddo
    endif
    !
    ! Loop on antennas
    do j=1, nb
      i = ib(j)
      if (spectral) then
        do k=1, 2*mbands
          saf(k,i) = smask(k)
        enddo
      endif
      if (unspectral) then
        afl(i) = mask
      endif
    enddo
  !
  ! Baselines
  elseif (baseline) then
    !
    ! Get baseline(s)
    narg = sic_narg(ibo)
    call sic_ke(line,ibo,1,arg,nch,.true.,error)
    if (error) return
    if (arg(1:3).eq.'ALL') then
      nb = mnbas
      do i=1, mnbas
        ib(i) = i
      enddo
    else
      nb = narg
      do i=1, narg
        arg1 = '*'
        call clic_kw(line,ibo,i,arg1,n,cbas,mnbas,.false.,error,.true.)
        if (error) return
        ib(i) = n
      enddo
    endif
    !
    ! Loop on baselines
    do j=1, nb
      i = ib(j)
      if (spectral) then
        do k=1, 2*mbands
          sbf(k,i) = smask(k)
        enddo
      endif
      if (unspectral) then
        bfl(i) = mask
      endif
    enddo
  elseif (mask.ne.0.or..not.(ssmask)) then
    call message(6,3,'CLIC_FLAG','/ANTENNA or /BASELINE')
    error = .true.
    return
  endif
  !
  return
end subroutine get_flags
!
subroutine get_ant_mask(chain,mask,error)
  use gkernel_interfaces
  character(len=*) :: chain         !
  integer :: mask                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, n, lpar, lc
  character(len=6) :: kw,par
  !------------------------------------------------------------------------
  ! Code:
  i = 1
  mask = 0
  lc = lenc(chain)
  do while (i.le.lc)
    lpar = 6
    call sic_next(chain(i:),par,lpar,i)
    if (lpar.gt.0) then
      call sic_upper(par(1:lpar))
      call sic_ambigs('GET_ANT_MASK',par(1:lpar),kw,n,af,maf,   &
        error)
      if (error) return
      mask = ior(mask,ishft(1,n-1))
    else
      return
    endif
  enddo
  return
end subroutine get_ant_mask
!
subroutine get_base_mask(chain,mask,error)
  use gkernel_interfaces
  character(len=*) :: chain         !
  integer :: mask                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, n, lpar, lc
  character(len=6) :: kw,par
  !------------------------------------------------------------------------
  ! Code:
  i = 1
  mask = 0
  lc = lenc(chain)
  do while (i.le.lc)
    lpar = 6
    call sic_next(chain(i:),par,lpar,i)
    if (lpar.gt.0) then
      call sic_upper(par(1:lpar))
      call sic_ambigs('GET_BASE_MASK',par(1:lpar),kw,n,bf,mbf,   &
        error)
      if (error) return
      mask = ior(mask,ishft(1,n-1))
    else
      return
    endif
  enddo
end subroutine get_base_mask
!
function down_baseline (ib)
  use classic_api
  use clic_index
  logical :: down_baseline          !
  integer :: ib                     !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_flags.inc'
  include 'clic_clic.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer ::  ia, ja, ibf, iaf, jaf, it, i, k, ib1, ffc00000
  data ffc00000/z'FFC00000'/
  !------------------------------------------------------------------------
  ! Code:
  if (ib.le.mnbas) then
    ibf = ior(ior(iand(dh_bflag(ib),not(bas_mask(ib))), bas_kill(ib)),&
              ix_bflag(ib,r_xnum))
    ia = r_iant(ib)
    ja = r_jant(ib)
    if (ia.gt.mnant .or. ja.gt.mnant .or. ib.gt.mnbas   &
      .or. ia.lt.1 .or. ib.lt.1 .or. ja.lt.1) then
      write(*,*) 'down_baseline ib, ia, ja ',ib,ia,ja
    endif
    iaf = ior(ior(iand(dh_aflag(ia),not(ant_mask(ia))), ant_kill(ia)), &
              ix_aflag(ia,r_xnum))
    jaf = ior(ior(iand(dh_aflag(ja),not(ant_mask(ja))), ant_kill(ja)), &
              ix_aflag(ja,r_xnum))
    k = ior(ibf,ior(iaf,jaf))
  else
    ! Triangle ...
    it = ib-mnbas
    k = 0
    do i = 1, 3
      ia = anttri(i,it)
      iaf = ior(iand(dh_aflag(ia),not(ant_mask(ia))),   &
        ant_kill(ia))
      k = ior(k,iaf)
      ib1 = bastri(i,it)
      ibf = ior(iand(dh_aflag(ib1),not(ant_mask(ib1))),   &
        ant_kill(ib1))
      k = ior(k,ibf)
    enddo
  endif
  down_baseline = iand(ffc00000,k).ne.0
end function down_baseline
!
function down_antenna (iant)
  use classic_api
  use clic_index
  !---------------------------------------------------------------------
  ! Is ANTENNA Iant Flagged ?
  !---------------------------------------------------------------------
  logical :: down_antenna           !
  integer :: iant                   !
  ! Global
  logical :: down_baseline
  include 'clic_parameter.inc'
  include 'clic_flags.inc'
  include 'clic_clic.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: iaf, ffc00000, i, ibase
  logical :: all_down
  data ffc00000/z'FFC00000'/
  !
  iaf = ior(ior(iand(dh_aflag(iant),not(ant_mask(iant))),   &
    ant_kill(iant)),ix_aflag(iant,r_xnum))
  down_antenna = iand(ffc00000,iaf).ne.0
  if (r_nant.gt.1) then
    ! Antenna is also down is all baselines connected to it are down;
    ! this is not tested for a singtle antenna... RL
    all_down = .true.
    !
    do i = 1, r_nant
      if (i.lt.iant) then
        ibase = basant(i,iant)
        all_down = down_baseline(ibase).and.all_down
      elseif(i.gt.iant) then
        ibase = basant(iant,i)
        all_down = down_baseline(ibase).and.all_down
      endif
    enddo
    down_antenna = down_antenna.or.all_down
  endif
end function down_antenna
!
function down_channel (ib,ic)
  use classic_api
  !---------------------------------------------------------------------
  ! Is ANTENNA Iant Flagged ?
  !---------------------------------------------------------------------
  logical :: down_channel           !
  integer :: ib                     !
  integer :: ic                     !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_flags.inc'
  include 'clic_clic.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: it,i,ia,ib1,ja
  logical :: ibf, iaf, jaf, k
  !
  if (ib.gt.mnbas) then
    it = ib-mnbas
    k = 0
    do i = 1, 3
      ia = anttri(i,it)
      iaf = (dh_saflag(ic,ia).and..not.san_mask(ic,ib)).or.san_kill(ic,ia)
      ib1 = bastri(i,it)
      ibf = (dh_sbflag(ic,ib1).and..not.sba_mask(ic,ib1)).or.sba_kill(ic,ib1)
      k = iaf.or.ibf
    enddo
  else
    ia = r_iant(ib)
    ja = r_jant(ib)
    ibf = (dh_sbflag(ic,ib).and..not.sba_mask(ic,ib)).or.sba_kill(ic,ib)
    iaf = (dh_saflag(ic,ia).and..not.san_mask(ic,ia)).or.san_kill(ic,ia)
    jaf = (dh_saflag(ic,ja).and..not.san_mask(ic,ja)).or.san_kill(ic,ja)
    k = ibf.or.iaf.or.jaf
  endif
  down_channel = k
end function down_channel
