subroutine solve_pseudo (line,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !	Support routine for command
  !	SOLVE PSEUDO [BEAM] [/PLOT] [/OUTPUT File [OLD|NEW|PROC]]
  !              [/COMPRESS TIME_MAX]
  !	[/Store /Reset /Offset /Search] Not relevant here
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  logical :: plot, output, end, flux, tpoint
  real :: beam, lo1, bsize, time_max
  integer :: p_lun, q_lun
  integer :: icount, naddr, nant
  integer(kind=address_length) :: iaddr
  integer :: nch, nkey
  !
  integer :: mvoc1
  parameter (mvoc1=2)
  character(len=6) :: kw, voc1(mvoc1), argum
  data voc1/'PSEUDO','TOTAL'/
  !----------------------------------------------------------------------
  !------------------------------------------------------------------------
  ! Code:
  call sic_ke(line,0,1,argum,nch,.true.,error)
  if (error) goto 999
  call sic_ambigs('SOLVE_PSEUDO',argum,kw,nkey,voc1,mvoc1,error)
  if (error) return
  !
  naddr = 0
  iaddr = 0
  q_lun = 0
  output = .false.
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  plot = sic_present(1,0)
  !
  ! /COMPRESS TIME_MAX option
  time_max = 4.0
  call sic_r4 (line,6,1,time_max,.false.,error)
  if (error) goto 999
  !
  ! /OUTPUT FLUX|OLD|NEW, /PRINT
  call file_point(line,output,p_lun,flux,q_lun,tpoint,error)
  !
  ! Get beam size
  beam = -1.0                  ! fix to frequency dep. value
  call sic_r4(line,0,2,beam,.false.,error)
  !
  if (plot) call gr_exec1('TICKSPACE 0 0 0 0')
  !
  ! Get  a first one
  call get_first(.true.,error)
  if (error) goto 999
  end = .false.
  call check_pseudo(kw,error)
  do while (error .and. .not.end)
    call get_next(end,error)
    if (.not.end) call check_pseudo(kw,error)
  enddo
  if (error) goto 999
  icount = 0
  call load_pseudo (icount,iaddr,naddr,error,lo1,nant,time_max)
  if (error) goto 999
  call load_beam  (beam,bsize,lo1,nant)
  !
  ! Get next ones alike
  call get_next(end,error)
  if (error) goto 999
  do while (.not.end)
    call check_pseudo(kw,error)
    if (error) then
      if (icount.ne.0) then
        call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
          error,bsize,flux,end)
        icount = 0
        if (error) goto 998
        call free_vm(naddr,iaddr)
        naddr = 0
        iaddr = 0
      endif
    elseif (icount.eq.0) then
      call load_pseudo (icount,iaddr,naddr,error,lo1,nant,   &
        time_max)
      if (error) goto 998
      call load_beam  (beam,bsize,lo1,nant)
    else
      call load_pseudo (icount,iaddr,naddr,error,lo1,nant,   &
        time_max)
      if (icount.eq.0) then
        call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
          error,bsize,flux,end)
        if (error) goto 998
        call free_vm(naddr,iaddr)
        naddr = 0
        iaddr = 0
        call load_pseudo (icount,iaddr,naddr,error,lo1,nant,   &
          time_max)
        if (error) goto 998
      endif
      call load_beam  (beam,bsize,lo1,nant)
    endif
    call get_next(end,error)
    if (error) goto 999
    if (sic_ctrlc()) goto 999
  enddo
  !
  if (icount.ne.0) then
    call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
      error,bsize,flux,end)
    call free_vm(naddr,iaddr)
    naddr = 0
    iaddr = 0
  endif
  !
  goto 998
  !
999 error = .true.
998 if (output) then
    close(unit=p_lun)
    call sic_frelun(p_lun)
  endif
  if (naddr.ne.0 .and. iaddr.ne.0) then
    call free_vm(naddr,iaddr)
    iaddr = 0
    naddr = 0
  endif
  if (q_lun.ne.0) then
    close (unit=q_lun)
    call  sic_frelun(q_lun)
  endif
end subroutine solve_pseudo
!
subroutine check_pseudo(argum,error)
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !	Check if data is a PSEUDOing scan
  !---------------------------------------------------------------------
  character(len=*) :: argum         !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_proc_par.inc'
  include 'clic_point.inc'
  !
  if (argum.eq.'PSEUDO') then
    if (r_proc.eq.p_pseudo) then
      iauto = 1
      return
    endif
  elseif  (argum.eq.'TOTAL') then
    if (r_proc.eq.p_pseudo) then
      iauto = 2
      return
    elseif (r_proc.eq.p_point) then
      iauto = 2
      return
    endif
  endif
  call message(6,3,'CHECK_PSEUDO',   &
    'Scan is not a pointing measurement')
  error = .true.
end subroutine check_pseudo
!
subroutine load_pseudo (icount,jaddr,naddr,error,lo1,nant,   &
    time_max)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !	Support routine for SOLVE PSEUDO
  !	Measure antenna pointing position from a specialised
  !	routine in the real-time acquisition system
  ! Arguments:
  !	ICOUNT	I	Subscan number 0 : not loaded
  !	JADDR	I	Array address
  !	NADDR	I	Array length
  !	LO1	R*4 	LO1 frequency
  !	NANT	I	Number of antennas
  !       TIME_MAX R*4    Max. integration time (compression)
  !---------------------------------------------------------------------
  integer :: icount                      !
  integer(kind=address_length) :: jaddr  !
  integer :: naddr                       !
  logical :: error                       !
  real :: lo1                            !
  integer :: nant                        !
  real :: time_max                       !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_point.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: iaddr, visi, posi, weig
  integer(kind=address_length) :: data_in, ip_data
  integer(kind=data_length)    :: ldata_in
  integer :: i, j, nout, k1, kp1, kv1, kw1, mdump
  real :: weight_factor, seuil
  real*8 :: fnu, size(3), ff
  !
  ! Compute threshold and weight
  error = .false.
  seuil = 40.0                 ! Big displacement is bad
  weight_factor = 0.1          ! and really bad
  if (icount.ne.0 .and.   &
    (r_scan.ne.iscan  .or. r_scaty.ne.icode)) then
    icount = 0
    return
  endif
  !
  ! Compress (to time_max)
  call get_data (ldata_in,data_in,error)
  if (error) goto 999
  ip_data = gag_pointer(data_in,memory)
  mdump = r_ndump
  ! FG 04.03.04 Compression is suppressed because of on-going
  ! modifications in the COMPRESS routine for the ATF.
  !      CALL COMPRESS(TIME_MAX, 7.5, MEMORY(IP_DATA),NOUT,ERROR)
  !      IF (ERROR) GOTO 999
  !      R_NDUMP = NOUT
  !      CALL LOOSE_DATA
  !
  if (icount.eq.0) then
    iscan = r_scan
    icode = r_scaty
    lo1   = r_flo1
    nfix = 0
    nmob = 0
    nant = r_nant
    do i=1,r_nant
      if (r_mobil(i)) then
        nmob = nmob+1
        imob(nmob) = i
      else
        nfix = nfix+1
        ifix(nfix) = i
      endif
      if (r_scaty.eq.2) then
        coll(i) = r_collaz(i)
      elseif (r_scaty.eq.3) then
        coll(i) = r_collel(i)
      endif
      kant(i) = r_kant(i)
      istat(i) = r_istat(i)
    enddo
    source = r_sourc
    az = r_az
    el = r_el
    ut = r_ut
    a_flux = r_flux
    if (iauto.eq.2) then
      fnu = 0
      do i=1, l_baseband(1)
        j = i_baseband(i,1)
        fnu = fnu+r_flo1+r_sb(j)*(r_flo2(j) &
              + r_band2(j)*(r_flo2bis(j)+r_band2bis(j)*r_lfcen(j)))
      enddo
      fnu = fnu/l_baseband(1) * 1d-3
    else 
      fnu = r_restf * 1d-3
    endif
    call planet_flux (source,r_dobs,r_ut,   &
      fnu,ff,a_flux,size,error)
    mp = 8*mdump
    !
    ! Get storage for up to 8 subscans like that one...
    !	IADDR (MP,3,NMOB)
    !
    naddr = mp*3*nmob + mdump*r_nant*3
    if (sic_getvm(naddr,jaddr).ne.1) goto 999
    np = 0
  endif
  !
  ! Read visibilities and positions
  iaddr = gag_pointer(jaddr,memory)
  visi = iaddr + 3*mp*nmob
  posi = visi + mdump*r_nant
  weig = posi + mdump*r_nant
  call fill_pseu_posi (mdump, r_nbas, r_nant,   &
    memory(visi),memory(posi),memory(weig),time_max,data_in,error)
  if (error) goto 999
  !
  ! Put in final array
  do i=1,nmob
    k1 = (imob(i)-1)*mdump
    kp1 = mdump*(24*(i-1)+icount)
    kv1 = mdump*(24*(i-1)+8+icount)
    kw1 = mdump*(24*(i-1)+16+icount)
    call r4tor4 (memory(posi+k1),memory(iaddr+kp1),mdump)
    call r4tor4 (memory(visi+k1),memory(iaddr+kv1),mdump)
    call r4tor4 (memory(weig+k1),memory(iaddr+kw1),mdump)
  enddo
  icount = icount+1
  np = np+mdump
  return
  !
999 error = .true.
end subroutine load_pseudo
!
subroutine fill_pseu_posi (qdump,qbas,qant,visi,posi,weig,   &
    time_max,data_in,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! Read the data section and extract the visibilities for all baselines
  ! and the positions of all antennas
  ! Arguments:
  !	R 	Struc.	Header
  !	VISI	R*4(*)	Visibilities to be returned
  !	POSI	R*4(*)	"Position" of antenna. The "position"
  !			is actually a parameter defined by R.SCATY
  !	WEIG	R*4(*)	Weights to be returned
  !---------------------------------------------------------------------
  integer :: qdump                         !
  integer :: qbas                          !
  integer :: qant                          !
  real :: visi(qdump,qant)                 !
  real :: posi(qdump,qant)                 !
  real :: weig(qdump,qant)                 !
  real :: time_max                         !
  integer(kind=address_length) :: data_in  !
  logical :: error                         !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_point.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: change, saved_physical
  integer :: iband, ir, iant, ix, iy, jw1, jw2, kb, lb, nb, ipol, kbb, kif
  integer :: irmin, irmax, iv, im
  integer :: in, in1, in2, nn, bb
  real :: vzero, vslop, visi_on, visi_off, airmass, cf(mnant)
  real*4 :: vt(4,3), wt(4,3)
  integer(kind=address_length) :: ipk, ipkc, ipkl, kin
  integer(kind=data_length) :: h_offset, c_offset, l_offset
  character*132 :: chain
  !------------------------------------------------------------------------
  ! Code:
  nb = 2
  ipol = 1 ! Dummy
  kbb = 1
  kif = 1
  !
  saved_physical = physical_power
  physical_power = .false.
  !
  ! Select subbands and channels to be averaged
  if (iauto.eq.1) then
    call check_subb(nb,.true.,error)
    if (error) goto 999
    iband = 1                  ! Does not matter USB or LSB...
    iy = xy_auto               ! Auto-correlation
  else
    ! Total power only : compute DSB calibration factors
    if (.not.bb_select.or.n_baseband.ne.1) then
      call show_display('BBAND',.false.,error)
      write(chain,'(A)')   &
        'Select a single baseband group using SET BBAND command'
      call message(6,3,'SOLVE_PSEUDO',chain)
      error = .true.
      return
    else
      in1 = 1
      in2 = l_baseband(1)
    endif
    iband = 1
    iy = xy_total
    airmass = 1.0/sin(r_el)
    do iant=1, r_nant
      cf(iant) = 0.0
      nn = 0
      do bb = in1,in2
        in = i_baseband(bb,1)
        if (r_csky(in,iant).ne.0) then
          cf(iant) = cf(iant)+r_tsyss(in,iant)/r_csky(in,iant)/   &
            (1.+r_gim(in,iant)   &
            *exp((r_taus(in,iant)-r_taui(in,iant))*airmass))
          nn = nn +1
        endif
      enddo
      cf(iant) = cf(iant)/nn
    enddo
  endif
  !
  if (r_scaty.eq.1) then
    ix = xy_focus              ! Focus
  elseif (r_scaty.eq.2) then
    ix = xy_lambd              ! Lambda
  elseif (r_scaty.eq.3) then
    ix = xy_beta               ! Beta
  elseif (r_scaty.eq.6) then
    ix = xy_lambd              ! Lambda first, Then Beta
  endif
  !
  ! Loop on records
  kin = gag_pointer(data_in, memory)
  do ir = 1, min(qdump, r_ndump)
    ipk = kin + h_offset(ir)
    call decode_header (memory(ipk))
    if (ir.eq.1) then
      call set_scaling(error)
      if (error) return
    endif
    ipkc = kin + c_offset(ir)
    ipkl = kin + l_offset(ir)
    do iant = 1, r_nant
      !
      kb = k_subb(1)
      lb = l_subb(kb)
      jw1 = iw1(kb)
      jw2 = iw2(kb)
      !            VISI_ON = 0
      !            POSI(IR,IANT) = 0
      !            VISI(IR,IANT) = 0
      !            WEIG(IR,IANT) = 0
      do iv=1,4
        do im=1,3
          vt(iv,im) = 0
          wt(iv,im) = 0
        enddo
      enddo
      call arecord (   &
        r_nsb, r_nband, r_nbas, r_lntch,   &
        memory(ipkc), memory(ipkl), passc, passl,   &
        iant, iband, ipol, kb, kif, lb, i_subb(1,kb),jw1,jw2,ix,iy,   &
        vt,wt,.false.,error)
      !     $      POSI(IR,IANT), VISI_ON, ZZZ, WEIG(IR,IANT),
      !     $      WWW, TTT, .FALSE.,ERROR)
      if (error) goto 999
      weig(ir,iant) =  wt(2,2)
      if (wt(2,2).gt.0) then
        visi_on = vt(2,2)/wt(2,2)
        if (iauto.ne.1) visi_on = visi_on*cf(iant)
        posi(ir,iant) = vt(1,2)/wt(1,2)
      endif
      !            IF (WEIG(IR,IANT).NE.0) THEN
      !               VISI_ON = VISI_ON/WEIG(IR,IANT)
      !               IF (IAUTO.NE.1) VISI_ON = VISI_ON*CF(IANT)
      !               POSI(IR,IANT) = POSI(IR,IANT)/WEIG(IR,IANT)
      !            ENDIF
      !
      if (iauto.eq.1 .and. nb.eq.2) then
        kb = k_subb(2)
        lb = l_subb(kb)
        jw1 = iw1(kb)
        jw2 = iw2(kb)
        do iv=1,4
          do im=1,3
            vt(iv,im) = 0
            wt(iv,im) = 0
          enddo
        enddo
        !               POSI(IR,IANT) = 0
        !               VISI(IR,IANT) = 0
        !               WEIG(IR,IANT) = 0
        !               VISI_OFF = 0
        call arecord (   &
          r_nsb, r_nband, r_nbas, r_lntch,   &
          memory(ipkc), memory(ipkl), passc, passl,   &
          iant, iband, ipol, kbb, kif, lb, i_subb(1,kb),jw1,jw2,ix,iy,   &
          vt,wt,.false.,error)
        !     $         POSI(IR,IANT), VISI_OFF, ZZZ, WEIG(IR,IANT),
        !     $         WWW, TTT, .FALSE.,ERROR)
        if (error) goto 999
        weig(ir,iant) =  wt(2,2)
        if (wt(2,2).gt.0) then
          visi_off = vt(2,2)/wt(2,2)
          visi(ir,iant) = (visi_on-visi_off)/visi_off
          posi(ir,iant) = vt(1,2)/wt(1,2)
        endif
      !               IF (WEIG(IR,IANT).NE.0) THEN
      !                  VISI_OFF = VISI_OFF/WEIG(IR,IANT)
      !                  VISI(IR,IANT) = (VISI_ON-VISI_OFF)/VISI_OFF
      !                  POSI(IR,IANT) = POSI(IR,IANT)/WEIG(IR,IANT)
      !               ENDIF
      else
        visi(ir,iant) = visi_on
      endif
    enddo
    !         K = K + R_LDUMP*4
    !
    ! Change scanning direction if on (0,0) for Five-Points
    if (r_scaty.eq.6) then
      change = .true.
      do iant=1,r_nant
        if (posi(ir,iant).ne.0.0) change = .false.
      enddo
      if (change) ix = xy_beta
    endif
  enddo
  if (qdump.gt.r_ndump) then
    do ir=r_ndump+1, qdump
      do iant = 1, r_nant
        posi(ir,iant) = 0
        visi(ir,iant) = 0
        weig(ir,iant) = 0
      enddo
    enddo
  endif
  !
  ! Patch : Bad coordinates on first point
  !      IF (R_SCATY.EQ.2 .OR. R_SCATY.EQ.3) THEN
  !         DO IANT = 1,R_NANT
  !            POSI(1,IANT) = 2.0*POSI(2,IANT)-POSI(3,IANT)
  !         ENDDO
  !      ENDIF
  !
  ! Subtract zero order-baseline
  do iant=1,r_nant
    irmin = 1
    irmax = r_ndump
    do while (weig(irmin,iant).le.0)
      irmin = irmin+1
    enddo
    do while (weig(irmax,iant).le.0)
      irmax = irmax-1
    enddo
    vzero = visi(irmin,iant)
    vslop = (visi(irmax,iant)-visi(irmin,iant))/(irmax-irmin)
    do ir=1, r_ndump
      if (weig(ir,iant).gt.0) then
        visi(ir,iant) = visi(ir,iant)-(ir-irmin)*vslop-vzero
      else
        visi(ir,iant) = blank4
      endif
    enddo
  enddo
  physical_power = saved_physical
  return
  !
999 error = .true.
  return
end subroutine fill_pseu_posi
