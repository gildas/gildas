subroutine cdate(date,idate,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! ASTRO Utility routine
  !	Converts the date '01-JAN-1984' in internal code
  ! Arguments
  !	DATE 	C*(*)	String date			Input
  !	IDATE 	I	Internal coded date value	Output
  !	ERROR 	L	Logical error flag		Output
  ! Subroutines :
  !	(SIC)
  !	DATJ,JDAT
  ! No commons
  !---------------------------------------------------------------------
  character(len=*) :: date          !
  integer :: idate                  !
  logical :: error                  !
  ! Local
  character(len=3) :: cm(12),m
  integer :: id, iy, im
  !
  data cm/'JAN','FEB','MAR','APR','MAY','JUN',   &
    'JUL','AUG','SEP','OCT','NOV','DEC'/
  !
  if (date.eq.'*') return
  read(date,'(I2,1X,A,1X,I4)',err=900) id,m,iy
  call sic_upper(m)
  do 10 im=1,12
    if (cm(im).eq.m) then
      call datj(id,im,iy,idate)
      if (idate.eq.32767 .or. idate.eq.-32768) goto 900
      return
    endif
10 continue
  !
  ! Wrong date
900 call message(6,3,'DATE', 'Date conversion error')
  error=.true.
  return
  !
entry datec(idate,date,error)
  !-------
  !	Converts the internal code to formatted date
  !-------
  if (idate.eq.32767 .or. idate.eq.-32768) then
    date = '*'
  else
    call jdat(idate,id,im,iy)
    write(date,1000,err=900) id,cm(im),iy
  endif
1000 format(i2.2,'-',a,'-',i4)
end subroutine cdate
!
subroutine datj(id,im,iy,jour)
  !---------------------------------------------------------------------
  ! ASTRO	Utility routine
  !	Convert a date (id/im/iy) in number of days till the
  !	01/01/1984
  ! Arguments :
  !	ID	I	Day number		Input
  !	IM	I	Month number		Input
  !	IY	I	Year number		Input
  !	JOUR	I	Elapsed day count	Output
  ! Subroutine
  !	JULDA
  !---------------------------------------------------------------------
  integer :: id                     !
  integer :: im                     !
  integer :: iy                     !
  integer :: jour                   !
  ! Global
  integer :: julda
  ! Local
  integer :: ideb(13), ibiss, j, m, jour4
  integer :: izero
  parameter (izero=2025)
  !
  data ideb/0,31,59,90,120,151,181,212,243,273,304,334,365/
  !
  jour4=id+ideb(im)
  ibiss=julda(iy+1)-julda(iy)-365
  if (im.ge.3) jour4=jour4+ibiss
  jour4=jour4+julda(iy)
  jour4=min(32767,jour4)
  jour4=max(-32768,jour4)
  jour=jour4
  return
  !
entry jdat(jour,id,im,iy)
  !-------
  ! 	Reverse conversion
  !-------
  ! magic formula for bugged real-time codes ...
  !
  jour = mod(jour+32768,65536)-32768
  iy=izero+jour/365
1 j=jour-julda(iy)
  if (j.le.0) then
    iy=iy-1
    go to 1
  elseif (j.gt.365) then
    ! Special case for 31 december of leap years...
    if (j.eq.366) then
      ibiss=julda(iy+1)-julda(iy)-365
      if (ibiss.eq.0) then
        iy=iy+1
        go to 1
      endif
    else
      iy=iy+1
      go to 1
    endif
  endif
  ibiss=julda(iy+1)-julda(iy)-365
  do 2 m=12,1,-1
    id=j-ideb(m)
    if (m.ge.3) id=id-ibiss
    if (id.gt.0) go to 3
2 continue
3 im=m
end subroutine datj
!
function julda(nyr)
  !---------------------------------------------------------------------
  ! ASTRO	Utility routine
  !	Returns Julian date of 1 day of Year
  ! Arguments
  !	NYR	I	Year				Input
  !	JULDA	I	Date returned			Output
  !---------------------------------------------------------------------
  integer :: julda                  !
  integer :: nyr                    !
  ! Local
  integer :: nyrm1, ic, nadd
  integer :: izero
  parameter (izero=2025)
  nyrm1=nyr-1
  ic=nyrm1/100
  nadd=nyrm1/4-ic+ic/4
  julda=365.*(nyrm1-izero)+nadd
end function julda
!
subroutine adate(chain,id,im,iy,error)
  use gkernel_interfaces
  character(len=*) :: chain         !
  integer :: id                     !
  integer :: im                     !
  integer :: iy                     !
  logical :: error                  !
  ! Local
  integer :: i,ier
  character(len=3) :: cm(12),m
  data cm/'JAN','FEB','MAR','APR','MAY','JUN',   &
    'JUL','AUG','SEP','OCT','NOV','DEC'/
  !
  error = .true.
  read(chain(1:11),'(I2,1X,A3,1X,I4)',iostat=ier) id,m,iy
  if (ier.ne.0) return
  call sic_upper(m)
  do i=1,12
    if (cm(i).eq.m) then
      im = i
      error = .false.
      return
    endif
  enddo
end subroutine adate
