!
subroutine convert_dh (iwin,nw,conve,nant,nbas,nband,npol)
  use gbl_convert
  use classic_api
  !---------------------------------------------------------------------
  !	Convert data header IWIN using conversion code CONVE,
  !	assuming NW is the length of the data header.
  ! Arguments
  !	IWIN	I(*)	Array containing data header to be converted
  !	NW	I	Size of IWIN
  !	CONVE	I	Conversion code
  !	NANT	I	Number of antennas
  !	NBAS	I	Number of baselines
  !---------------------------------------------------------------------
  integer :: iwin(*)                !
  integer(kind=data_length) :: nw                     !
  integer :: conve                  !
  integer :: nant                   !
  integer :: nbas                   !
  integer :: nband                  !
  integer :: npol                   !
  ! Global
  external :: r4tor4,r8tor8
  external :: ier4va,eir4va,eir4ie,ier4ei,var4ie,var4ei
  external :: ier8va,eir8va,eir8ie,ier8ei,var8ie,var8ei
  external ::        eii4va,eii4ie,iei4ei       ,vai4ei
  include 'clic_parameter.inc'
  !
  !------------------------------------------------------------------------
  ! Code:
  ! test the conversion works
  if (conve.eq.0) then
    return
  ! test it works ...
  !$$$         CALL CONVERT_DHSUB (IWIN,NW,NANT,NBAS,NBAND,NPOL,
  !$$$     &   R4TOR4,R8TOR8,R4TOR4,R4TOR4)
  elseif (conve.eq.vax_to_ieee .or. conve.eq.vax_fr_ieee) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      var4ie,var8ie,r4tor4,r4tor4)
  elseif (conve.eq.ieee_to_vax .or. conve.eq.ieee_fr_vax) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      ier4va,ier8va,r4tor4,r4tor4)
  elseif (conve.eq.vax_to_eeei .or. conve.eq.vax_fr_eeei) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      var4ei,var8ei,vai4ei,r4tor4)
  elseif (conve.eq.ieee_to_eeei .or. conve.eq.ieee_fr_eeei) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      ier4ei,ier8ei,iei4ei,r4tor4)
  elseif (conve.eq.eeei_to_vax .or. conve.eq.eeei_fr_vax) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      eir4va,eir8va,eii4va,r4tor4)
  elseif (conve.eq.eeei_to_ieee .or. conve.eq.eeei_fr_ieee) then
    call convert_dhsub (iwin,nw,nant,nbas,nband,npol,   &
      eir4ie,eir8ie,eii4ie,r4tor4)
  endif
end subroutine convert_dh
!
subroutine convert_dhsub(iwin,nw,nant,nbas,nband,npol,r4,r8,i4,cc)
  use classic_api
  use clic_file
  !---------------------------------------------------------------------
  !	Convert data header IWIN using conversion code CONVE,
  !	assuming NW is the length of the data header.
  ! Arguments
  !	IWIN	I(*)	Array containing data header to be converted
  !	NW	I	Size of IWIN
  !	CONVE	I	Conversion code
  !	NANT	I	Number of antennas
  !	NBAS	I	Number of baselines
  !---------------------------------------------------------------------
  integer :: iwin(*)                !
  integer(kind=data_length) :: nw                     !
  integer :: nant                   !
  integer :: nbas                   !
  integer :: nband                  !
  integer :: npol                   !
  external :: r4                    !
  external :: r8                    !
  external :: i4                    !
  external :: cc                    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: k, temp_dim, natfac
  real*8 ::  rtmp(8)
  !------------------------------------------------------------------------
  ! Code:
  !$$$      print *, 'CONVERT_DHSUB , NW,NANT,NBAS,NBAND,NPOL'
  !$$$      print *, NW,NANT,NBAS,NBAND,NPOL
  call i4 (iwin(1),iwin(1),2)  ! Dump + obs MJ date
  call r4 (iwin(3),iwin(3),1)  ! Integ
  !
  !      CALL R8 (IWIN(4),IWIN(4),8) ! UTC + Svec(3)
  call r4tor4(iwin(4),rtmp,16) ! Alignment problems
  call r8 (rtmp,rtmp,8)
  call r4tor4(rtmp,iwin(4),8)
  !
  if (de%version.le.2) then
    call r4 (iwin(12),iwin(12),10)   ! Test0
    k = 22
  else
    call r4 (iwin(12),iwin(12),10)   ! Test0
    k = 32
  endif
  call i4 (iwin(22),iwin(22),nant) ! A Flag
  k = k+nant
  if (new_receivers) then
    if (r_dobs.lt.(-6406)) then    ! changed the 13/02/2007
      temp_dim = npol
    else
      temp_dim = 2
    endif
    call r4 (iwin(k),iwin(k), temp_dim*nant)   ! Total
    k = k+npol*nant
  else
    call r4 (iwin(k),iwin(k),nant) ! Total
    k = k+nant
  endif
  if (new_receivers) then
    natfac = max(npol,nband)
    call r4 (iwin(k),iwin(k),2*natfac*nant)    ! atfac
    k = k+2*natfac*nant
  else
    call r4 (iwin(k),iwin(k),2*nant)   ! atfac
    k = k+2*nant
  endif
  call r4 (iwin(k),iwin(k),2*nant) ! rmspe 
  k = k+2*nant
  if (de%version.le.2) then
    call r4 (iwin(k),iwin(k),nant) !  delcon
    k = k+nant
  endif
  if (new_receivers) then
    call r4 (iwin(k),iwin(k),npol*nant)    ! dellin
    k = k+npol*nant
  else
    call r4 (iwin(k),iwin(k),nant) ! dellin
    k = k+nant
  endif
  call r4 (iwin(k),iwin(k),nant)   ! delayc
  k = k+nant
  if (new_receivers) then
    call r4 (iwin(k),iwin(k),npol*nant)    ! delay
    k = k+npol*nant
  else
    !ccc         CALL R4 (IWIN(K),IWIN(K),NPOL*NANT) ! delay
    call r4 (iwin(k),iwin(k),nant) ! delay
    k = k+nant
  endif
  call r4 (iwin(k),iwin(k),4*nant) ! phasec to cable
  k = k+4*nant
  if (de%version.le.2) then
    call i4 (iwin(k),iwin(k),nant)   ! Gamme
    k = k + nant
  endif
  if (de%version.le.2) then
    call r4 (iwin(k),iwin(k),5*nant) ! Test1
    k = k + 5*nant
  else
    call r4 (iwin(k),iwin(k),20*nant) ! Test1
    k = k + 20*nant
  endif
  if (new_receivers) then
    call r4 (iwin(k),iwin(k),4*npol*nbas)  ! RMS Amp & Pha
    k = k + 4*npol*nbas
  else
    call r4 (iwin(k),iwin(k),4*nbas)   ! RMS Amp & Pha
    k = k + 4*nbas
  endif
  call r4 (iwin(k),iwin(k),3*nant) ! Offsets (Foc,Lam,Bet)
  k = k + 3*nant
  call i4 (iwin(k),iwin(k),nbas)   ! B Flag
  k = k + nbas
  call r4 (iwin(k),iwin(k),2*nbas) ! UV 
  k = k + 2*nbas
  if (de%version.le.2) then
    call r4 (iwin(k),iwin(k),4*nbas) ! Infac
    k = k + 4*nbas
  endif
  if (k.lt.nw) then
    call r4 (iwin(k),iwin(k),mwvrch*nant)  ! DH_wvr
    k = k + mwvrch*nant
    call i4 (iwin(k),iwin(k),nant) ! DH_wvrstat
    k = k + nant
  endif
  if (k.lt.nw) then
    call r4 (iwin(k),iwin(k),npol*nant*nband)  ! DH_ACTP
    k = k + 2*nant*nband
    call r4 (iwin(k),iwin(k),2*nant)   ! DH_ANTP
    k = k + 2*nant
  endif
  if (r_nband_widex.ne.0) then
    call r4 (iwin(k),iwin(k),nant)     ! dh_sflag
    k = k + nant
    call r4 (iwin(k),iwin(k),nbas)     ! dh_sbflag
    k = k + nbas
  endif
  if (de%version.ge.3) then
    call r4 (iwin(k),iwin(k),nant)     ! dh_cable_alternate
    k = k + nant
    call r4 (iwin(k),iwin(k),1)        ! dh_ntime
    dh_ntime = iwin(k)
    k = k + 1
    if (dh_ntime.gt.0) then
      call r4 (iwin(k),iwin(k),dh_ntime)     ! dh_time
      k = k + dh_ntime
    endif
    call r4 (iwin(k),iwin(k),3*nant)     ! incli, incli_temp
    k = k + 3* nant
    call r4 (iwin(k),iwin(k),5*nant)     ! tilfoc,offfoc
    k = k + 5* nant
  endif
  ! trick to get the size (for data fillers).
  if (nw .gt. 999999) then
    nw = k-1
  !
  elseif (k.gt.nw+1) then      ! R_LDPAR is actually more (142) ...
    write(6,*) 'CONVERT_DHSUB: Wrong data record K,NANT,NBAS,NW'
    write(6,*) k,nant,nbas,nw
  endif
end subroutine convert_dhsub
!
subroutine convert_data (ndata,data,jconve)
  use gbl_convert
  integer :: ndata                  !
  integer :: data(*)                !
  integer :: jconve                 !
  ! Global
  !
  if (jconve.eq.0) then
    return
  elseif (jconve.eq.vax_to_ieee .or. jconve.eq.vax_fr_ieee) then
    call var4ie (data,data,ndata)
  elseif (jconve.eq.ieee_to_vax .or. jconve.eq.ieee_fr_vax) then
    call ier4va (data,data,ndata)
  elseif (jconve.eq.vax_to_eeei .or. jconve.eq.vax_fr_eeei) then
    call var4ei (data,data,ndata)
  elseif (jconve.eq.ieee_to_eeei .or. jconve.eq.ieee_fr_eeei) then
    call ier4ei (data,data,ndata)
  elseif (jconve.eq.eeei_to_vax .or. jconve.eq.eeei_fr_vax) then
    call eir4va (data,data,ndata)
  elseif (jconve.eq.eeei_to_ieee .or. jconve.eq.eeei_fr_ieee) then
    call eir4ie (data,data,ndata)
  else
    return
  endif
end subroutine convert_data
