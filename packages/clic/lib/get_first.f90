subroutine get_first(forward,error)
  use gildas_def
  use classic_api
  use clic_index
  use clic_virtual
  logical :: forward                !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  logical :: end
  logical :: direction, go_forward
  integer :: n, offmin, offset
  integer(kind=entry_length) :: i, kx, ktry, kix
  character(len=100) :: nombre
  logical :: got(m_cx), first
  save got, first
  save direction,go_forward
  ! Data (at END!)
  data direction /.true./,go_forward/.true./
  !--------------------------------------------------------------------------
  ! Code:
  !
  do i=1, cxnext-1
    got(i) = .false.
  enddo
  knext = 0
  first = .true.
  go_forward = forward
  !
entry get_next(end,error)
  ! (GET NEXT after a FIND means GET FIRST)
  if (knext.eq.0) then
    do i=1, cxnext-1
      got(i) = .false.
    enddo
    first = .true.
  endif
  ktry = 0
  !
  ! Get oldest in memory  ...
  if (.not. go_forward) then
    offmin = region_length(1)
    do i=1, cxnext-1
      kx = cx_ind(i)
      if (.not. got(i) .and. got_data(kx)) then
        offset = mod(v_data(kx)-next_offset+region_length(1),   &
          region_length(1))
        if (offset.lt.offmin) then
          ktry = i
        endif
      endif
    enddo
    !
    ! Get the rest of it ...
    if (ktry.eq.0) then
      do i=1, cxnext-1
        if (.not. got(i)) then
          ktry = i
        endif
      enddo
    endif
  !
  ! GO_FORWARD: get really first one
  elseif (knext.lt.cxnext-1) then
    ktry = knext+1
  endif
  if (ktry.eq.0) then
    end = .true.
    return
  endif
  knext = ktry
  !
  ! Now get it
  got(knext) = .true.
  kx = cx_ind(knext)
  call get_it(kx,error)
  ! Other initialisations
  if (first) then
    if (all_base) call set_all_baselines
    if (all_subb) call set_all_subbands
    if (all_bband) call set_all_bbands
    if (each_subb) call set_each_subband
    if (each_polar) call set_each_pol_subbands(error)
    if (error) return
    if (pol_select.and.pol_subb.ne.0) then
      call set_pol_subbands(error)
      if (error) return
    endif
    if (quar_subb.ne.0) then
      call set_quar_subbands(error)
      if (error) return
    endif
    if (nbc_subb.ne.0) then
      call set_nbc_subbands(error)
      if (error) return
    endif
    if (widex_subb.ne.0) then
      call set_widex_subbands(error)
      if (error) return
    endif
    if (bb_select.and.n_subb.eq.0) then
      call set_bb_subbands(error)
      if (error) return
    endif
    if (if_select.and.n_subb.eq.0) then
      call set_if_subbands(error)
      if (error) return
    endif
    if (all_base .or. all_subb .or. each_subb .or.   &
      pol_select .or. (quar_subb.ne.0) .or.   &
      (nbc_subb.ne.0).or. bb_select .or. if_select )   &
      call set_display(error)
    first = .false.
    call set_subbands_if(error)
    call set_subbands_bb(error)
  endif
  return
  !
entry get_last(error)
  kx = cx_ind(cxnext-1)
  call get_it(kx,error)
  return
  !
entry get_num(kix,error)
  ! Search in index
  kx = kix
  do i=1,cxnext-1
    if (kix.eq.cx_ind(i)) then
      knext = i
      call get_it(kx,error)
      return
    endif
  enddo
  write (nombre,'(A,I12,A)') 'Observation ',kx,' not in index'
  call noir (nombre,nombre,n)
  call message(4,3,'GET',nombre(1:n))
  error = .true.
  return
end subroutine get_first
!
subroutine get_data(ndata,data,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  use clic_index
  use clic_file
  use clic_virtual
  !---------------------------------------------------------------------
  ! Read data
  !---------------------------------------------------------------------
  integer(kind=data_length)    :: ndata ! Data array length
  integer(kind=address_length) :: data  ! Data starting address
  logical :: error                      ! Erro logical flag
  ! Global
  include 'clic_parameter.inc'
  include 'clic_proc_par.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipv, dumm_add_length
  integer(kind=data_length)    :: ldc, ldl 
  integer ::  lch, j, lntch
  character(len=80) :: ch
  integer(kind=entry_length)   :: num
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Get storage
  if (.not. got_data(r_xnum)) then
    ! Patch
    num = r_xnum
    call robs (num, error)
    if (error) return
    r_dobs = mod(r_dobs+32768,65536)-32768
    r_dred = mod(r_dred+32768,65536)-32768
    v_data_length(r_xnum) = e%ldata 
    if (r_lmode.eq.0) then
      if (r_proc.ne.p_cal   &
        .and. r_proc.ne.p_skydip .and. r_proc.ne.p_onoff   &
        .and. r_proc.ne.p_sky) then
        ldc = r_ldatc
        ldl = r_ldatl
      else
        ldc = r_nband*r_nant
        ldl = r_lntch*r_nant
      endif
      v_data_length(r_xnum) = max(e%ldata,   &
        (ldc+int(r_ldpar,kind=data_length))*int(r_ndump,kind=data_length) &
        +int(r_ldpar,kind=data_length)+ldc+ldl)
    endif
    !
    ! LR mode: set size accordingly (only for hpbs and crosscorrelations)
    if (lowres.and.r_presec(file_sec).and.r_lmode.eq.1) then
      lntch = 0
      do j=1, r_nbb
        lntch = lntch+r_lnch(j)
      enddo
      ldl = 2*lntch*r_lnsb*r_nbas
      if (r_ndatl.gt.1) then
        v_data_length(r_xnum) = (r_ndump+2)*r_ldump+2*ldl
      else 
        v_data_length(r_xnum) = (r_ndump+1)*r_ldump+ldl
      endif
    endif
    if (address_length.lt.data_length) then
      ! Assume that we are on a 32 bits system
      ! Waiting for a more general conversion routine
      call i8toi4_fini(v_data_length(r_xnum),dumm_add_length,1,error)
      if (error) then
        call message(8,3,'GET_DATA', &
             'Cannot get enough memory on this system to address the data')
        return
      endif
      call get_memory(dumm_add_length,v_data(r_xnum),error)
    else 
      call get_memory(int(v_data_length(r_xnum),kind=address_length), &
                      v_data(r_xnum),error)
    endif
    if (error) return
    ipv = gag_pointer(v_data(r_xnum),memory)
    call rdata (v_data_length(r_xnum),memory(ipv),   &
      error)
    if (error) return
    if (.not.got_data(r_xnum)) then
      got_data(r_xnum) = .true.
    endif
    if (r_lmode.eq.0) then
      call newformat(v_data_length(r_xnum),v_data(r_xnum))
      call update_header       ! header was changed too
    endif
  endif
  ndata = v_data_length(r_xnum)
  data  = v_data(r_xnum)
  return
end subroutine get_data
!
!
subroutine update_header
  use gkernel_interfaces
  use gildas_def
  use clic_index
  use clic_virtual
  !---------------------------------------------------------------------
  ! Read data
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipv
  !------------------------------------------------------------------------
  if (got_header(r_xnum)) then
    ipv = gag_pointer(v_header(r_xnum),memory)
    call r4tor4 (r_xnum,memory(ipv),v_header_length(r_xnum))
  endif
  return
  !
entry loose_data
  got_data(r_xnum) = .false.
  got_header(r_xnum) = .false.
  current_region = min(current_region,1)
  next_offset = 0
  return
end subroutine update_header
!
