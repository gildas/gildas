subroutine title(long)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! SAS	Internal routine
  !	draw a header above the box
  ! Arguments
  !	LONG	C*(*)	Header type (Long, Brief or Full)
  !---------------------------------------------------------------------
  character(len=*) :: long          !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  logical :: error
  real :: xmax, ymax
  !
  call sic_get_real('PAGE_X',xmax,error)
  call sic_get_real('PAGE_Y',ymax,error)
  call out0('Graphic',xmax/2,ymax,error)
  call titout(long,error)
  call out1(error)
end subroutine title
