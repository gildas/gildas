subroutine mth_init
  call message(6,1,'MTH_INIT','Using LAPACK math library')
end subroutine mth_init
!=================================================================
! Spline package
!
subroutine mth_fitspl (name, nd, nk8, x, y, w, ks,   &
    wk1, wk2, cs, ss, error)
  character(len=*) :: name          !
  integer :: nd                     !
  integer :: nk8                    !
  real*8 :: x(nd)                   !
  real*8 :: y(nd)                   !
  real*8 :: w(nd)                   !
  real*8 :: ks(nk8)                 !
  real*8 :: wk1(nd)                 !
  real*8 :: wk2(4,nk8)              !
  real*8 :: cs(nk8)                 !
  real*8 :: ss                      !
  logical :: error                  !
  ! Local
  integer :: ifail, i
  character(len=60) :: chain
  !
  ! Fit splines
  ifail = 1
  error = .false.
  call fitbspl (nd, nk8, x, y, w, ks, wk1, wk2, cs,   &
    ss, ifail)
  if (ifail.eq.5) then
    error = .true.
    call message(8,4,name,'Too many breaks')
    write(chain,*) 'Using ',nk8, ' breaks :'
    call message(4,1,name,chain)
    do i=1, nk8
      write(chain,*) i,' at ',ks(i+4)
      call message(4,1,name,chain)
    enddo
    error = .true.
  elseif (ifail.ne.0) then
    call mth_fail (name,'MTH_FITSPL',ifail,error)
  endif
end subroutine mth_fitspl
!
subroutine mth_getspl(name,nk8, ks, cs, xx, yy, error)
  character(len=*) :: name          !
  integer :: nk8                    !
  real*8 :: ks(nk8)                 !
  real*8 :: cs(nk8)                 !
  real*8 :: xx                      !
  real*8 :: yy                      !
  logical :: error                  !
  ! Local
  integer :: ifail
  !
  call getbspl  (nk8, ks, cs, xx, yy, ifail)
  call nag_fail (name,'MTH_GETSPL',ifail,error)
end subroutine mth_getspl
!
subroutine mth_getspd(name,nk8, ks, cs, xx, left, yy, error)
  character(len=*) :: name          !
  integer :: nk8                    !
  real*8 :: ks(nk8)                 !
  real*8 :: cs(nk8)                 !
  real*8 :: xx                      !
  integer :: left                   !
  real*8 :: yy(4)                   !
  logical :: error                  !
  ! Local
  integer :: ifail
  !
  call getbspd  (nk8, ks, cs, xx, left, yy, ifail)
  call nag_fail (name,'MTH_GETSPD',ifail,error)
end subroutine mth_getspd
!
subroutine fitbspl(m,ncap7,x,y,w,k,work1,work2,c,ss,ifail)
  !---------------------------------------------------------------------
  ! Computes a weighted least-squares approximation to an arbitrary set
  ! of data points by a cubic spline with knots prescribed by the user.
  ! Cubic spline interpolation can also be carried out.
  !
  ! COX-DE BOOR method for evaluating B-splines with adaptation of
  ! GENTLEMAN*s plane rotation scheme for solving over-determined linear systems.
  !
  ! Redesigned to use classical GIVENS rotations in order to
  ! avoid the occasional underflow (and hence overflow) problems
  ! experienced by GENTLEMAN*s 3-multiplication plane rotation scheme
  !
  ! WORK1 and WORK2 are workspace areas.
  !
  !     WORK1(R)  contains the value of the  R th  distinct data
  !     abscissa and, subsequently, for  R = 1, 2, 3, 4,  the
  !     values of the non-zero B-splines for each successive
  !     abscissa value.
  !
  !     WORK2(L, J)  contains, for  L = 1, 2, 3, 4,  the value of
  !     the  J th  element in the  L th  diagonal of the
  !     upper triangular matrix of bandwidth  4  in the
  !     triangular system defining the B-spline coefficients.
  !---------------------------------------------------------------------
  integer :: m                      !
  integer :: ncap7                  !
  real*8 :: x(m)                    !
  real*8 :: y(m)                    !
  real*8 :: w(m)                    !
  real*8 :: k(ncap7)                !
  real*8 :: work1(m)                !
  real*8 :: work2(4,ncap7)          !
  real*8 :: c(ncap7)                !
  real*8 :: ss                      !
  integer :: ifail                  !
  ! Local
  real*8 :: acol, arow, ccol, cosine, crow, d4, d5, d6, d7
  real*8 :: d8, d9,d, dprime, e2, e3, e4, e5, k0, k1, k2, k3
  real*8 :: k4, k5, k6, n1, n2, n3, relemt, s, sigma, sine, wi, xi
  integer :: i, iplusj, iu, j, jold, jplusl, jrev, l4,l
  integer :: lplus1, lplusu, ncap3, ncap, ncapm1, r
  ! Code
  !
  ! Check that the values of  M  and  NCAP7  are reasonable
  ifail = 4
  if (ncap7.lt.8 .or. m.lt.ncap7-4) return
  ncap = ncap7 - 7
  ncapm1 = ncap - 1
  ncap3 = ncap + 3
  !
  ! In order to define the full B-spline basis, augment the prescribed
  ! interior knots by knots of multiplicity four at each end of the data range.
  do j=1,4
    i = ncap3 + j
    k(j) = x(1)
    k(i) = x(m)
  enddo
  !
  ! test the validity of the data.
  !
  ! Check that the knots are ordered and are interior to the data interval.
  ifail = 1
  if (k(5).le.x(1) .or. k(ncap3).ge.x(m)) return
  do j=4,ncap3
    if (k(j).gt.k(j+1)) return
  enddo
  !
  ! Check that the weights are strictly positive.
  ifail = 2
  do i=1,m
    if (w(i).le.0.0d0) return
  enddo
  !
  ! Check that the data abscissae are ordered, then form the
  ! array  WORK1  from the array  X.  The array  WORK1  contains
  ! the set of distinct data abscissae.
  ifail = 3
  work1(1) = x(1)
  j = 2
  do i=2,m
    if (x(i).lt.work1(j-1)) return
    if (x(i).ne.work1(j-1)) then
      work1(j) = x(i)
      j = j + 1
    endif
  enddo
  r = j - 1
  !
  ! Check that there are sufficient distinct data abscissae for
  ! the prescribed number of knots.
  ifail = 4
  if (r.lt.ncap3) return
  !
  ! Check the first  S  and the last  S  SCHOENBERG-WHITNEY
  ! conditions ( S = MIN(NCAP - 1, 4) ).
  !
  ifail = 5
  do j=1,min(4,ncap-1)
    i = ncap3 - j + 1
    l = r - j + 1
    if (work1(j).ge.k(j+4) .or. k(i).ge.work1(l)) return
  enddo
  !
  ! Check all the remaining SCHOENBERG-WHITNEY conditions.
  !
  if (ncap.gt.5) then
    r = r - 4
    i = 4
    do j=5,ncapm1
      k0 = k(j+4)
      k4 = k(j)
10    i = i + 1
      if (work1(i).le.k4) goto 10
      if (i.gt.r .or. work1(i).ge.k0) return
    enddo
  endif
  !
  ! Initialise a band triangular system (i.e. a matrix and a right
  ! hand side) to zero. The processing of each data point in turn
  ! results in an updating of this system. The subsequent solution
  ! of the resulting band triangular system yields the coefficients
  ! of the B-splines.
  !
  do i=1,ncap3
    do l=1,4
      work2(l,i) = 0.0d0
    enddo
    c(i) = 0.0d0
  enddo
  sigma = 0.0d0
  j = 0
  jold = 0
  !
  do i=1,m
    !
    ! For the data point  (X(I), Y(I))  determine an interval
    ! K(J + 3) .LE. X .LT. K(J + 4)  containing  X(I).  (In the
    ! case  J + 4 .EQ. NCAP  the second equality is relaxed to
    ! include equality).
    !
    wi = w(i)
    xi = x(i)
    do while (xi.ge.k(j+4) .and. j.le.ncapm1)
      j = j+1
    enddo
    !
    ! Set certain constants relating to the interval
    ! K(J + 3) .LE. X .LE. K(J + 4).
    !
    if (j.ne.jold) then
      k1 = k(j+1)
      k2 = k(j+2)
      k3 = k(j+3)
      k4 = k(j+4)
      k5 = k(j+5)
      k6 = k(j+6)
      d4 = 1.0d0/(k4-k1)
      d5 = 1.0d0/(k5-k2)
      d6 = 1.0d0/(k6-k3)
      d7 = 1.0d0/(k4-k2)
      d8 = 1.0d0/(k5-k3)
      d9 = 1.0d0/(k4-k3)
      jold = j
    endif
    !
    ! Compute and store in  WORK1(L) (L = 1, 2, 3, 4)  the value of
    ! the four normalized cubic B-splines which are non-zero at X=X(I).
    !
    e5 = k5 - xi
    e4 = k4 - xi
    e3 = xi - k3
    e2 = xi - k2
    n1 = wi*d9
    n2 = e3*n1*d8
    n1 = e4*n1*d7
    n3 = e3*n2*d6
    n2 = (e2*n1+e5*n2)*d5
    n1 = e4*n1*d4
    work1(4) = e3*n3
    work1(3) = e2*n2 + (k6-xi)*n3
    work1(2) = (xi-k1)*n1 + e5*n2
    work1(1) = e4*n1
    crow = y(i)*wi
    !
    ! Rotate this row into the band triangular system using plane rotations.
    !
    do lplus1=1,4
      l = lplus1 - 1
      relemt = work1(lplus1)
      if (relemt.ne.0.0d0) then
        jplusl = j + l
        l4 = 4 - l
        d = work2(1,jplusl)
        if (dabs(relemt).ge.d) then
          dprime = dabs(relemt)*dsqrt(1.0d0+(d/relemt)**2)
        else                   ! IF (DABS(RELEMT).LT.D)
          dprime = d*dsqrt(1.0d0+(relemt/d)**2)
        endif
        work2(1,jplusl) = dprime
        cosine = d/dprime
        sine = relemt/dprime
        do iu=2,l4
          lplusu = l + iu
          acol = work2(iu,jplusl)
          arow = work1(lplusu)
          work2(iu,jplusl) = cosine*acol + sine*arow
          work1(lplusu) = cosine*arow - sine*acol
        enddo
        ccol = c(jplusl)
        c(jplusl) = cosine*ccol + sine*crow
        crow = cosine*crow - sine*ccol
      endif
    enddo
    sigma = sigma + crow**2
  enddo
  ss = sigma
  !
  ! Solve the band triangular system for the b-spline coefficients.
  ! If a diagonal element is zero, and hence the triangular system
  ! is singular, the implication is that the SCHOENBERG-WHITNEY conditions
  ! are only just satisfied. Thus it is appropriate to exit in this
  ! case with the same value  (IFAIL=5)  of the error indicator.
  !
  l = -1
  do jrev=1,ncap3
    j = ncap3 - jrev + 1
    d = work2(1,j)
    if (d.eq.0.0d0) return
    if (l.lt.3) l = l + 1
    s = c(j)
    do i=1,l
      iplusj = i + j
      s = s - work2(i+1,j)*c(iplusj)
    enddo
    c(j) = s/d
  enddo
  ifail = 0
end subroutine fitbspl
!
subroutine getbspl(ncap7, k, c, x, s, ifail)
  !---------------------------------------------------------------------
  ! Evaluates a cubic spline from its B-spline representation.
  !
  ! Uses DE BOOR*s method of convex combinations.
  !---------------------------------------------------------------------
  integer :: ncap7                  !
  real*8 :: k(ncap7)                !
  real*8 :: c(ncap7)                !
  real*8 :: x                       !
  real*8 :: s                       !
  integer :: ifail                  !
  ! Local
  integer :: j, j1, l
  real*8 :: c1, c2, c3, e2, e3, e4
  real*8 :: e5,k1, k2, k3, k4, k5, k6
  !
  ! Check enough data
  if (ncap7.lt.8) then
    ifail = 2
    return
  endif
  ! Check in boundary
  if (x.lt.k(4) .or. x.gt.k(ncap7-3)) then
    ifail = 1
    s = 0.0d0
    return
  endif
  !
  ! Determine  J  such that  K(J + 3) .LE. X .LE. K(J + 4).
  j1 = 0
  j = ncap7 - 7
  do while (j-j1.gt.1)
    l = (j1+j)/2
    if (x.ge.k(l+4)) then
      j1 = l
    else
      j = l
    endif
  enddo
  !
  ! Use the method of convex combinations to compute  S(X).
  k1 = k(j+1)
  k2 = k(j+2)
  k3 = k(j+3)
  k4 = k(j+4)
  k5 = k(j+5)
  k6 = k(j+6)
  e2 = x - k2
  e3 = x - k3
  e4 = k4 - x
  e5 = k5 - x
  c2 = c(j+1)
  c3 = c(j+2)
  c1 = ((x-k1)*c2+e4*c(j))/(k4-k1)
  c2 = (e2*c3+e5*c2)/(k5-k2)
  c3 = (e3*c(j+3)+(k6-x)*c3)/(k6-k3)
  c1 = (e2*c2+e4*c1)/(k4-k2)
  c2 = (e3*c3+e5*c2)/(k5-k3)
  s = (e3*c2+e4*c1)/(k4-k3)
  ifail = 0
end subroutine getbspl
!
subroutine getbspd (ncap7, k, c, x, left, s, ifail)
  !---------------------------------------------------------------------
  !  Evaluation of cubic spline and its derivatives from its
  !  B-spline representation
  !---------------------------------------------------------------------
  integer :: ncap7                  !
  real*8 :: k(ncap7)                !
  real*8 :: c(ncap7)                !
  real*8 :: x                       !
  integer :: left                   !
  real*8 :: s(4)                    !
  integer :: ifail                  !
  ! Local
  real*8 :: c1, c2, c3, c4, d1n41, d1n42, d1n43, d1n44
  real*8 :: d2n41,d2n42, d2n43, d2n44, d3n41, d3n42, d3n43, d3n44
  real*8 :: e2, e3, e4, e5, half, k1, k2, k3, k4, k5, k6
  real*8 :: m11, m21, m22, m32, n41, n42, n43, n44
  real*8 :: p4, p5, p6, six, three
  integer :: j1, j, l
  !
  half = 0.5d+00
  three = 3.0d+00
  six = 6.0d+00
  !
  ! Enough data
  ifail = 1
  if (ncap7.lt.8) return
  !
  ! Check whether the range of definition of the spline is strictly
  ! positive in length and  whether  X is in range
  ifail = 2
  if (k(4).ge.k(ncap7-3)) return
  if (x.lt.k(4) .or. x.gt.k(ncap7-3)) return
  !
  ! binary search for interval containing  X  -
  !
  ! if right-hand derivatives are required  (LEFT .NE. 1)
  ! search for  J  satisfying
  !     K(J + 3) .LE. X .LT. K(J + 4)
  ! (setting  J = NCAP  in the exceptional case  X = K(NCAP + 4)).
  !
  ! if left-hand derivatives are required  (LEFT .EQ. 1)
  ! search for  J  satisfying
  !     K(J + 3) .LT. X .LE. K(J + 4)
  ! (setting  J = 1  in the exceptional case  X = K(4)).
  !
  ifail = 0
  j1 = 4
  j = ncap7 - 3
  do while (j-j1.gt.1)
    l = (j1+j)/2
    if (left.ne.1 .and. x.ge.k(l)) then
      j1 = l
    elseif (left.eq.1 .and. x.gt.k(l)) then
      j1 = l
    else
      j = l
    endif
  enddo
  j = j - 4
  !
  ! Setup the constants
  k1 = k(j+1)
  k2 = k(j+2)
  k3 = k(j+3)
  k4 = k(j+4)
  k5 = k(j+5)
  k6 = k(j+6)
  e2 = x - k2
  e3 = x - k3
  e4 = k4 - x
  e5 = k5 - x
  p4 = k4 - k1
  p5 = k5 - k2
  p6 = k6 - k3
  !
  ! Form basis functions and their derivatives -
  !
  ! The values of the non-zero un-normalized B-splines of order R
  ! are denoted by  MR1, MR2,...  .  the corresponding normalized
  ! B-splines are denoted by  NR1, NR2,...  and their derivatives
  ! of order  L  by  DLNR1, DLNR2,...  .
  !
  m11 = six/(k4-k3)
  m21 = -m11/(k4-k2)
  m22 = m11/(k5-k3)
  d3n41 = m21/p4
  m32 = (m21-m22)/p5
  d3n44 = m22/p6
  d3n42 = -d3n41 - m32
  d3n43 = m32 - d3n44
  m21 = -e4*m21
  m22 = e3*m22
  d2n41 = m21/p4
  m32 = (m21-m22)/p5
  d2n44 = m22/p6
  d2n42 = -d2n41 - m32
  d2n43 = m32 - d2n44
  m21 = half*m21
  m22 = half*m22
  d1n41 = -e4*m21/p4
  m32 = (e2*m21+e5*m22)/p5
  d1n44 = e3*m22/p6
  d1n42 = -d1n41 - m32
  d1n43 = m32 - d1n44
  n41 = -e4*d1n41/three
  n42 = (-(x-k1)*d1n41+e5*m32)/three
  n43 = (e2*m32+(k6-x)*d1n44)/three
  n44 = e3*d1n44/three
  !
  ! Compute the cubic spline and its derivatives -
  c1 = c(j)
  c2 = c(j+1)
  c3 = c(j+2)
  c4 = c(j+3)
  s(1) = c1*n41 + c2*n42 + c3*n43 + c4*n44
  s(2) = c1*d1n41 + c2*d1n42 + c3*d1n43 + c4*d1n44
  s(3) = c1*d2n41 + c2*d2n42 + c3*d2n43 + c4*d2n44
  s(4) = c1*d3n41 + c2*d3n42 + c3*d3n43 + c4*d3n44
end subroutine getbspd
!
!
! End of Spline package
!=================================================================
! Start of Polynom package
!
!
subroutine mth_fitpol(name,nd,ipol,m_pol,x,y,w,   &
    wk1,wk2,apol,spol,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !     ND Number of data points
  !     X(ND),Y(ND),W(ND) data points and weights
  !                       X must be non-decreasing
  !     WK1(3*ND)         work space
  !     WK2(2*IPOL)       work space
  !     M_POL             First size of matrix (>= IPOL)
  !     IPOL              Maximum degree of polynomials
  !     APOL(M_POL,IPOL)  coefficients of polynomials for degrees 0 to IPOL-1
  !     SPOL(IPOL)        Rms of fit for corresponding degree
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: nd                     !
  integer :: ipol                   !
  integer :: m_pol                  !
  real*8 :: x(nd)                   !
  real*8 :: y(nd)                   !
  real*8 :: w(nd)                   !
  real*8 :: wk1(3*nd)               !
  real*8 :: wk2(2*ipol)             !
  real*8 :: apol(m_pol,ipol)        !
  real*8 :: spol(ipol)              !
  logical :: error                  !
  ! Global
  external :: fcheb
  include 'gbl_memory.inc'
  ! Local
  integer :: ifail
  !
  integer*4 :: status
  integer(kind=address_length) :: addr,ipu,ipv
  integer :: i
  real*8 :: xmin,xmax,chisq
  !
  !      SAVE MEMORY
  !
  ! Use singular value decomposition if possible
  ifail = 0
  error = .false.
  !
  ! Normalize range to [-1,1]
  xmin = x(1)
  xmax = x(nd)
  do i=1,nd
    wk1(i) = (x(i)-xmin+x(i)-xmax)/(xmax-xmin)
  enddo
  !
  status = sic_getvm((2*(ipol+nd)*ipol),addr)
  if (status.eq.1) then
    ipu = gag_pointer(addr,memory)
    ipv = ipu+2*nd*ipol
    call svdfit(   &
      wk1,y,   &               ! Normalized X, Y values
      w,   &                   ! Weights (see SVDFIT)
      nd,   &                  ! Number of data points     (input)
      wk2,   &                 ! Coefficient of polynomial (output)
      ipol,   &                ! Degree of polynomial + 1
      memory(ipu),   &         ! Work array of (M,IPOL)
      memory(ipv),   &         ! Work array of (IPOL,IPOL)
      spol,   &                ! Something related to the Sigmas (IPOL)
      nd,   &                  ! Number of points
      ipol,   &                ! Degree + 1
      chisq,fcheb,   &         ! Chi2 + Function
      wk1(nd+1),   &           ! Workspace of size ND
      wk2(ipol+1),   &         ! Workspace of size IPOL
      error)
    call free_vm(2*(nd+ipol)*ipol,addr)
    if (error) then
      call message(6,3,'MTH_FITPOL',   &
        'Error in singular value decomposition')
      return
    endif
  else
    !
    ! Fall-back on Gauss-Jordan elimination
    status = sic_getvm(2*ipol*ipol,addr)
    if (status.ne.1) then
      call message(8,3,   &
        'MTH_FITPOL','Insufficient memory for work space')
      error = .true.
      return
    endif
    ipv = gag_pointer(addr,memory)
    call lfit(wk1,y,w,nd,wk2,ipol,memory(ipv),ipol,chisq,   &
      fcheb,error)
    call free_vm(2*ipol*ipol,addr)
    if (error) then
      call message(8,3,   &
        'MTH_FITPOL','Matrix inversion error')
      error = .true.
      return
    endif
  endif
  !
  ! Move the coefficients at the appropriate place
  do i=1,ipol-1
    spol(i) = 0.d0
  enddo
  spol(ipol) = sqrt(chisq/dble(nd-ipol-1))
  call svdput(wk2,ipol,apol,m_pol)
end subroutine mth_fitpol
!
subroutine mth_getpol(name,ipol,aa,xx,yy,error)
  !---------------------------------------------------------------------
  ! Evaluate polynomial YY of order IPOL-1 at position XX
  ! from its Chebychev coefficients AA(IPOL)
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: ipol                   ! Degree of polynomial + 1
  real*8 :: aa(ipol)                !
  real*8 :: xx                      !
  real*8 :: yy                      !
  logical :: error                  !
  ! Local
  real*8 :: zz,z1,z2,zn
  integer :: j
  !
  error = .false.
  yy = 0.0d0
  z1 = 0.0d0
  zn = 0.0d0
  z2 = 2.0*xx
  do j=ipol,2,-1
    zz = z1
    z1 = z2*z1-zn+aa(j)
    zn = zz
  enddo
  yy = xx*z1-zn+0.5*aa(1)
end subroutine mth_getpol
!
!=========================================================================
! End of polynom package
!
! General Linear Least Square fit
!
subroutine mth_fitlin (name,ndd,nvar,a,b,nd,sigma)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: name          !
  integer :: ndd                    ! Number of data points
  integer :: nvar                   ! Number of variables
  integer :: nd                     ! Size of original problem
  real*8 :: a(nd,nvar)              ! Input matrix/Output covariance
  real*8 :: b(nd)                   ! Input data/Output solutions
  real*8 :: sigma                   ! Sigma
  ! Global
  include 'gbl_memory.inc'
  ! Local
  logical :: error
  !
  integer :: mvar
  parameter (mvar=50)          ! Be large enough SVP
  real*8 :: work(2*mvar)
  real*8 :: x,wmax,wmin,tol
  integer :: i,j,irank,ier
  integer(kind=address_length) :: addr,ipu,ipv
  !
  tol = 1.d-5
  error = .false.
  !
  ! Make a copy: do not work with A directly (see below why)
  ier = sic_getvm(2*(nd+nvar)*nvar,addr)
  ipv = gag_pointer(addr,memory)
  ipu = ipv+2*nvar*nvar
  call r8tor8 (a,memory(ipu),nd*nvar)
  !
  ! Use Singular Value decomposition
  call svdcmp (   &
    memory(ipu),   &           ! U: Input matrix (transformed)
    ndd,nvar,nd,nvar,   &      ! Sizes of problem
    work,   &                  ! W: Work array of size > NVAR
    memory(ipv),   &           ! V: Work array of size > NVARxNVAR
    error)
  ! Compute maximum singular value
  wmax = 0.0d0
  do j=1,nvar
    if (work(j).gt.wmax) wmax = work(j)
  enddo
  ! Edit singular values
  wmin = wmax*tol
  irank = nvar
  do j=1,nvar
    if (work(j).lt.wmin) then
      work(j) = 0.d0
      irank = irank-1
    endif
  enddo
  !
  ! Now back-substitute
  call svbksb(   &
    memory(ipu),   &           ! U
    work,   &                  ! W Work array of size > N
    memory(ipv),   &           ! V Work array of size > NxN
    ndd,nvar,nd,nvar,   &      ! Size of problem
    b,   &                     ! Input vector
    work(nvar+1),   &          ! Solution vector
    error)
  !
  ! Compute RMS. When I have more time, I will optimize this step,
  ! which could be done using some of the above decomposition rather
  ! than by storing the whole A matrix and computing a Big problem...
  !
  sigma = 0.d0
  do i=1,ndd
    x = -b(i)
    do j=1,nvar
      x = x+a(i,j)*work(nvar+j)
    enddo
    sigma = sigma + x*x
  enddo
  sigma = sqrt(sigma/dble(ndd-irank))
  !      SIGMA = SQRT(SIGMA/DBLE(ND-IRANK))
  !
  ! Move solution vector to appropriate place, and free workspace
  do j=1,nvar
    b(j) = work(nvar+j)
  enddo
  !
  ! Get covariance matrix back into A
  call svdvar (memory(ipv),nvar,nvar,work,a,nd,work(nvar+1),   &
    sigma)
  call free_vm (2*(nvar+nd)*nvar,addr)
end subroutine mth_fitlin
!
!=========================================================================
! Linear Algebra: use LAPACK routines
!
subroutine mth_dpotrf (name, uplo, n, a, lda, error)
  character(len=*) :: name           !
  character(len=*) :: uplo           !
  integer :: n                       !
  integer :: lda                     !
  real*8 :: a(lda,*)                 !
  logical :: error                   !
  ! Local
  integer :: info
  !
  !  Purpose
  !  =======
  !
  !  DPOTRF computes the Cholesky factorization of a real symmetric
  !  positive definite matrix A.
  !
  !  The factorization has the form
  !     A = U**T * U,  if UPLO = 'U', or
  !     A = L  * L**T,  if UPLO = 'L',
  !  where U is an upper triangular matrix and L is lower triangular.
  !
  !  This is the block version of the algorithm, calling Level 3 BLAS.
  !
  !  Arguments
  !  =========
  !
  !  UPLO    (input) CHARACTER*1
  !          = 'U':  Upper triangle of A is stored;
  !          = 'L':  Lower triangle of A is stored.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
  !          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
  !          N-by-N upper triangular part of A contains the upper
  !          triangular part of the matrix A, and the strictly lower
  !          triangular part of A is not referenced.  If UPLO = 'L', the
  !          leading N-by-N lower triangular part of A contains the lower
  !          triangular part of the matrix A, and the strictly upper
  !          triangular part of A is not referenced.
  !
  !          On exit, if INFO = 0, the factor U or L from the Cholesky
  !          factorization A = U**T*U or A = L*L**T.
  !
  !  LDA     (input) INTEGER
  !          The leading dimension of the array A.  LDA >= max(1,N).
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit
  !          < 0:  if INFO = -i, the i-th argument had an illegal value
  !          > 0:  if INFO = i, the leading minor of order i is not
  !                positive definite, and the factorization could not be
  !                completed.
  !
  ! Call LAPACK routine
  call dpotrf  (uplo, n, a, lda, info )
  call mth_fail(name,'MTH_DPOTRF',info,error)
end subroutine mth_dpotrf
!
subroutine mth_dpotrs (name,   &
    uplo, n, nrhs, a, lda, b, ldb, info )
  character(len=*) :: name           !
  character(len=*) :: uplo           !
  integer :: n                       !
  integer :: nrhs                    !
  integer :: lda                     !
  real*8 :: a(lda,*)                 !
  integer :: ldb                     !
  real*8 :: b(ldb,*)                 !
  ! Local
  integer :: info
  logical :: error
  !
  !  Purpose
  !  =======
  !
  !  DPOTRS solves a system of linear equations A*X = B with a symmetric
  !  positive definite matrix A using the Cholesky factorization
  !  A = U**T*U or A = L*L**T computed by DPOTRF.
  !
  !  Arguments
  !  =========
  !
  !  UPLO    (input) CHARACTER*1
  !          = 'U':  Upper triangle of A is stored;
  !          = 'L':  Lower triangle of A is stored.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  NRHS    (input) INTEGER
  !          The number of right hand sides, i.e., the number of columns
  !          of the matrix B.  NRHS >= 0.
  !
  !  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
  !          The triangular factor U or L from the Cholesky factorization
  !          A = U**T*U or A = L*L**T, as computed by DPOTRF.
  !
  !  LDA     (input) INTEGER
  !          The leading dimension of the array A.  LDA >= max(1,N).
  !
  !  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  !          On entry, the right hand side matrix B.
  !          On exit, the solution matrix X.
  !
  !  LDB     (input) INTEGER
  !          The leading dimension of the array B.  LDB >= max(1,N).
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit
  !          < 0:  if INFO = -i, the i-th argument had an illegal value
  !
  ! Call LAPACK routine
  call dpotrs  (uplo, n, nrhs, a, lda, b, ldb, info )
  call mth_fail(name,'MTH_DPOTRF',info,error)
end subroutine mth_dpotrs
!
subroutine mth_dpbtrf (name, uplo, n, kd, ab, ldab, error)
  character(len=*) :: name             !
  character(len=*) :: uplo             !
  integer :: n                         !
  integer :: kd                        !
  integer :: ldab                      !
  real*8 :: ab(ldab,*)                 !
  logical :: error                     !
  ! Local
  integer :: info
  !
  !  Purpose
  !  =======
  !
  !  DPBTRF computes the Cholesky factorization of a real symmetric
  !  positive definite band matrix A.
  !
  !  The factorization has the form
  !     A = U**T * U,  if UPLO = 'U', or
  !     A = L  * L**T,  if UPLO = 'L',
  !  where U is an upper triangular matrix and L is lower triangular.
  !
  !  Arguments
  !  =========
  !
  !  UPLO    (input) CHARACTER*1
  !          = 'U':  Upper triangle of A is stored;
  !          = 'L':  Lower triangle of A is stored.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  KD      (input) INTEGER
  !          The number of superdiagonals of the matrix A if UPLO = 'U',
  !          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
  !
  !  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)
  !          On entry, the upper or lower triangle of the symmetric band
  !          matrix A, stored in the first KD+1 rows of the array.  The
  !          j-th column of A is stored in the j-th column of the array AB
  !          as follows:
  !          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
  !          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
  !
  !          On exit, if INFO = 0, the triangular factor U or L from the
  !          Cholesky factorization A = U**T*U or A = L*L**T of the band
  !          matrix A, in the same storage format as A.
  !
  !  LDAB    (input) INTEGER
  !          The leading dimension of the array AB.  LDAB >= KD+1.
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit
  !          < 0:  if INFO = -i, the i-th argument had an illegal value
  !          > 0:  if INFO = i, the leading minor of order i is not
  !                positive definite, and the factorization could not be
  !                completed.
  !
  !  Further Details
  !  ===============
  !
  !  The band storage scheme is illustrated by the following example, when
  !  N = 6, KD = 2, and UPLO = 'U':
  !
  !  On entry:                       On exit:
  !
  !      *    *   a13  a24  a35  a46      *    *   u13  u24  u35  u46
  !      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
  !     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
  !
  !  Similarly, if UPLO = 'L' the format of A is as follows:
  !
  !  On entry:                       On exit:
  !
  !     a11  a22  a33  a44  a55  a66     l11  l22  l33  l44  l55  l66
  !     a21  a32  a43  a54  a65   *      l21  l32  l43  l54  l65   *
  !     a31  a42  a53  a64   *    *      l31  l42  l53  l64   *    *
  !
  !  Array elements marked * are not used by the routine.
  !
  ! Call LAPACK routine
  call dpbtrf  (uplo, n, kd, ab, ldab, info )
  call mth_fail(name,'MTH_DPBTRF',info,error)
end subroutine mth_dpbtrf
!
subroutine mth_dpbtrs (name,   &
    uplo, n, kd, nrhs, ab, ldab, b, ldb, error)
  character(len=*) :: name             !
  character(len=*) :: uplo             !
  integer :: n                         !
  integer :: kd                        !
  integer :: nrhs                      !
  integer :: ldab                      !
  real*8 :: ab(ldab,*)                 !
  integer :: ldb                       !
  real*8 :: b(ldb,*)                   !
  logical :: error                     !
  ! Local
  integer :: info
  !
  !  Purpose
  !  =======
  !
  !  DPBTRS solves a system of linear equations A*X = B with a symmetric
  !  positive definite band matrix A using the Cholesky factorization
  !  A = U**T*U or A = L*L**T computed by DPBTRF.
  !
  !  Arguments
  !  =========
  !
  !  UPLO    (input) CHARACTER*1
  !          = 'U':  Upper triangular factor stored in AB;
  !          = 'L':  Lower triangular factor stored in AB.
  !
  !  N       (input) INTEGER
  !          The order of the matrix A.  N >= 0.
  !
  !  KD      (input) INTEGER
  !          The number of superdiagonals of the matrix A if UPLO = 'U',
  !          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
  !
  !  NRHS    (input) INTEGER
  !          The number of right hand sides, i.e., the number of columns
  !          of the matrix B.  NRHS >= 0.
  !
  !  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
  !          The triangular factor U or L from the Cholesky factorization
  !          A = U**T*U or A = L*L**T of the band matrix A, stored in the
  !          first KD+1 rows of the array.  The j-th column of U or L is
  !          stored in the j-th column of the array AB as follows:
  !          if UPLO ='U', AB(kd+1+i-j,j) = U(i,j) for max(1,j-kd)<=i<=j;
  !          if UPLO ='L', AB(1+i-j,j)    = L(i,j) for j<=i<=min(n,j+kd).
  !
  !  LDAB    (input) INTEGER
  !          The leading dimension of the array AB.  LDAB >= KD+1.
  !
  !  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
  !          On entry, the right hand side matrix B.
  !          On exit, the solution matrix X.
  !
  !  LDB     (input) INTEGER
  !          The leading dimension of the array B.  LDB >= max(1,N).
  !
  !  INFO    (output) INTEGER
  !          = 0:  successful exit
  !          < 0:  if INFO = -i, the i-th argument had an illegal value
  !
  ! Call LAPACK routine
  call dpbtrs (uplo, n, kd, nrhs, ab, ldab, b, ldb, info )
  call mth_fail(name,'MTH_DPBTRS',info,error)
end subroutine mth_dpbtrs
!
! End of Linear Algebra
!========================================================================
! Special functions
!
function mth_bessj0 (x)
  !---------------------------------------------------------------------
  ! Compute Bessel function J0
  !---------------------------------------------------------------------
  real*8 :: mth_bessj0              !
  real*8 :: x                       !
  ! Local
  real*8 :: ax,z,xx,y
  real*8 :: p1,p2,p3,p4,p5
  real*8 :: q1,q2,q3,q4,q5
  real*8 :: r1,r2,r3,r4,r5,r6
  real*8 :: s1,s2,s3,s4,s5,s6
  !
  data p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,   &
    -.2073370639d-5,.2093887211d-6/
  data q1,q2,q3,q4,q5/-.1562499995d-1,.1430488765d-3,   &
    -.6911147651d-5,.7621095161d-6,-.934945152d-7/
  data r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,   &
    651619640.7d0,   &
    -11214424.18d0,77392.33017d0,-184.9052456d0/
  data s1,s2,s3,s4,s5,s6/57568490411.d0,1029532985.d0,   &
    9494680.718d0,59272.64853d0,267.8532712d0,1.d0/
  !
  if (abs(x).lt.8.d0) then
    y=x*x
    mth_bessj0 = (r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))   &
      /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
  else
    ax=abs(x)
    z=8.d0/ax
    y=z*z
    xx=ax-.785398164d0
    mth_bessj0 = sqrt(.636619772d0/ax)*(cos(xx)*   &
      (p1+y*(p2+y*(p3+y*(p4+y*p5))))   &
      -z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
  endif
end function mth_bessj0
!
function mth_bessj1 (x)
  !---------------------------------------------------------------------
  ! Compute Bessel function J1
  !---------------------------------------------------------------------
  real*8 :: mth_bessj1              !
  real*8 :: x                       !
  ! Local
  real*8 :: ax,z,xx
  real*8 :: y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6
  real*8 :: s1,s2,s3,s4,s5,s6
  data r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,   &
    242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0/
  data s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,   &
    18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
  data p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,   &
    .2457520174d-5,-.240337019d-6/
  data q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,   &
    .8449199096d-5,-.88228987d-6,.105787412d-6/
  !
  if (abs(x).lt.8.d0) then
    y=x*x
    mth_bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))   &
      /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
  else
    ax=abs(x)
    z=8.d0/ax
    y=z*z
    xx=ax-2.356194491d0
    mth_bessj1=sqrt(.636619772d0/ax)*(cos(xx)*   &
      (p1+y*(p2+y*(p3+y*(p4+y*p5))))   &
      -z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))   &
      *sign(1.d0,x)
  endif
end function mth_bessj1
!
subroutine mth_bessjn (x,j,n)
  !---------------------------------------------------------------------
  ! Compute Bessels functions from J1 to Jn
  !---------------------------------------------------------------------
  real*8 :: x                       !
  integer :: n                      !
  real*8 :: j(n)                    !
  ! Global
  real*8 :: mth_bessj1,mth_bessj
  ! Local
  integer :: i
  !
  ! Unefficient implementation, if you don't care
  j(1) = mth_bessj1(x)
  do i=2,n
    j(i) = mth_bessj(i,x)
  enddo
end subroutine mth_bessjn
!
subroutine mth_fail (fac,prog,ifail,error)
  !---------------------------------------------------------------------
  ! Error handling routine
  !---------------------------------------------------------------------
  character(len=*) :: fac           !
  character(len=*) :: prog          !
  integer :: ifail                  !
  logical :: error                  !
  ! Local
  character(len=60) :: chain
  !
  if (ifail.eq.0) then
    error = .false.
  else
    write(chain,'(A,A,A,I4)')   &
      'ERROR in ',prog,', ifail = ',ifail
    call message(8,4,fac,chain)
    error = .true.
  endif
end subroutine mth_fail
!
function mth_bessj(n,x)
  !---------------------------------------------------------------------
  ! Compute Bessel function Jn, with n integer
  !---------------------------------------------------------------------
  real*8 :: mth_bessj               !
  integer :: n                      !
  real*8 :: x                       !
  ! Global
  real*8 :: mth_bessj1,mth_bessj0
  ! Local
  real*8 :: bigno,bigni,bessj
  real*8 :: tox, bjm, bj, bjp, sum
  integer :: iacc, j, m, jsum
  parameter (iacc=40,bigno=1.d10,bigni=1.d-10)
  !
  if (n.lt.2) return
  tox=2.d0/x
  if (x.gt.dble(n))then
    bjm = mth_bessj0(x)
    bj  = mth_bessj1(x)
    do j=1,n-1
      bjp= j*tox*bj-bjm
      bjm= bj
      bj = bjp
    enddo
    bessj = bj
  else
    m = 2*((n+int(sqrt(float(iacc*n))))/2)
    jsum=0
    sum=0.d0
    bjp=0.d0
    bj =1.d9
    do j=m,1,-1
      bjm= j*tox*bj-bjp
      bjp= bj
      bj = bjm
      if (abs(bj).gt.bigno) then
        bj = bj*bigni
        bjp = bjp*bigni
        bessj = bessj*bigni
        sum = sum*bigni
      endif
      if (jsum.ne.0) sum=sum+bj
      jsum = 1-jsum
      if (j.eq.n) bessj=bjp
    enddo
    sum = sum+sum-bj
    bessj = bessj/sum
  endif
  mth_bessj = bessj
end function mth_bessj
