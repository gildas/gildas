subroutine set_display (error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  !	Set up the display page according to previous SET commands
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  include 'clic_par.inc'
  logical :: if_data, bb_data
  ! Local
  integer :: ix, k, iy, ibs, ic, ibd, i, l, lk, a1, a2, ip, ibb
  character(len=20) :: ch1, ch2, ch3, ch4
  character(len=40) :: ch5
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Check consistency
  do i=1, n_x
    if (i_x(i).eq.xy_auto .and. i_base(1).gt.0) then
      call message(6,3,'SET_DISPLAY',   &
        'Use SET ANTENNA for Autocorrelation data')
      error = .true.
      return
    endif
    if (plot_mode.eq.'TIME' .and.   &
      i_x(i).ge.xy_chann .and. i_x(i).le.xy_sky_f.or.   &
      i_x(i).eq.xy_i_fre1.or.i_x(i).eq.xy_i_fre2.or.   &
      i_x(i).eq.xy_i_fre3) then
      call switch_spectral
    elseif (plot_mode.eq.'SPEC' .and.   &
      ((i_x(i).gt.xy_imag .and. i_x(i).lt.xy_chann)   &
      .or.(i_x(i).gt.xy_sky_f .and. i_x(i).ne.xy_auto.and.   &
      i_x(i).lt.xy_i_fre1))) then
      call switch_time
    endif
  enddo
  do i=1, n_y
    if (i_y(i).eq.xy_auto .and. i_base(1).gt.0) then
      call message(6,3,'SET_DISPLAY',   &
        'Use SET ANTENNA for Autocorrelation data')
      error = .true.
      return
    endif
    if (plot_mode.eq.'TIME' .and.   &
      i_y(i).ge.xy_chann .and. i_y(i).le.xy_sky_f) then
      call switch_spectral
    elseif (plot_mode.eq.'SPEC' .and.   &
      ((i_y(i).gt.xy_imag .and. i_y(i).lt.xy_chann)   &
      .or.(i_y(i).gt.xy_sky_f .and. i_y(i).ne.xy_auto))) then
      call switch_time
    endif
  enddo
  !
  k = n_x * n_y * n_base * n_subb * n_band  * n_stokes
  if (k .gt. mbox) then
    call message(8,3,'SET_DISPLAY','Too many plots')
    error = .true.
    return
  else
    i = 0
    call set_data(i,k)
  endif
  n_boxes = k
  k = 0
  do ibs = 1, n_base
    do ic = 1, n_subb
      do ibd = 1, n_band
        do ix = 1, n_x
          do iy = 1, n_y
            do ip = 1, n_stokes
               k = k + 1
               k_x(k) = ix        ! i_x(ix)
               x_label(k) =   &
                 clab(i_x(ix))(1:lenc(clab(i_x(ix))))
               ! Special label for amplitudes
               if (i_x(ix).eq.xy_ampli.or.i_x(ix).eq.xy_auto   &
                 .or.i_x(ix).eq.xy_rms_a) then
                 if (do_flux) then
                   x_label(k) =   &
                     clab(i_x(ix))(1:lenc(clab(i_x(ix))))//' (Jy)'
                 else
                   x_label(k) =   &
                     clab(i_x(ix))(1:lenc(clab(i_x(ix))))//' (K)'
                 endif
               endif
               if ((i_x(ix).eq.xy_ampli).and.(do_scale)) then
                 x_label(k) =   &
                   clab(i_x(ix))(1:lenc(clab(i_x(ix))))//' (K/Jy)'
               endif
               !
               k_y(k) = iy        !i_y(iy)
               y_label(k) =   &
                 clab(i_y(iy))(1:lenc(clab(i_y(iy))))
               ! Special label for amplitudes
               if (i_y(iy).eq.xy_ampli.or.i_y(iy).eq.xy_auto   &
                 .or.i_y(iy).eq.xy_rms_a) then
                 if (do_flux) then
                   y_label(k) =   &
                     clab(i_y(iy))(1:lenc(clab(i_y(iy))))//' (Jy)'
                 else
                   y_label(k) =   &
                     clab(i_y(iy))(1:lenc(clab(i_y(iy))))//' (K)'
                 endif
               endif
               if ((i_y(iy).eq.xy_ampli).and.(do_scale)) then
                 y_label(k) =   &
                   clab(i_y(iy))(1:lenc(clab(i_y(iy))))//' (K/Jy)'
               endif
               !
               if (sm_x1(ix).eq.'F') then
                 write(ch1,*) gu1_x(ix)
               else
                 ch1 = '*'
               endif
               if (sm_x2(ix).eq.'F') then
                 write(ch2,*) gu2_x(ix)
               else
                 ch2 = '*'
               endif
               if (sm_y1(iy).eq.'F') then
                 write(ch3,*) gu1_y(iy)
               else
                 ch3 = '*'
               endif
               if (sm_y2(iy).eq.'F') then
                 write(ch4,*) gu2_y(iy)
               else
                 ch4 = '*'
               endif
               write(ch5,1005) k,k
               c_limits(k) = 'LIMITS '//ch1(1:lenc(ch1))//' '   &
                 //ch2(1:lenc(ch2))//' '//ch3(1:lenc(ch3))//' '   &
                 //ch4(1:lenc(ch4))   &
                 //ch5(1:lenc(ch5))
               k_base(k) = ibs    !i_base(ibs)
               k_subb(k) = ic
               k_band(k) = ibd    !i_band(ibd)
               k_polar(k) = do_polar
               k_baseband(k) = ic
               k_if(k) = ic
               k_stokes(k) = ip
               if (each_polar) then
                 k_polar(k) = ic
               endif
               if (i_base(ibs).gt.0 .and.   &
                 i_base(ibs).le.mnbas) then
                 if (phys_base) then
                   if (mnant.gt.9) then
                     ch2=cbas(i_base(ibs))
                     a1=iachar(ch2(1:1))-48
                     a2=iachar(ch2(2:2))-48
                     l = a1*10+a2
                     a1=iachar(ch2(4:4))-48
                     a2=iachar(ch2(5:5))-48
                     i = a1*10+a2
                     write (header_1(k),'(a,i2.2,a,i2.2)')   &
                       'Ant. ',r_kant(l),'-',r_kant(i)
                   else
                     ch2=cbas(i_base(ibs))
                     a1=iachar(ch2(1:1))-48
                     a2=iachar(ch2(2:2))-48
                     write (header_1(k),'(a,i1.1,i1.1)')   &
                       'Ant. ',r_kant(a1),r_kant(a2)
                   endif
                 else
                   header_1(k) = 'Bas. '//cbas(i_base(ibs))
                 endif
               elseif (i_base(ibs).gt.mnbas) then
                 header_1(k) = 'Tri. '//ctri(i_base(ibs)-mnbas)
               else
                 if (phys_base) then
                   if (mnant.le.9) then
                     write (header_1(k),'(a,i1.1)')   &
                       'Ant. ',r_kant(-i_base(ibs))
                   else
                     write (header_1(k),'(a,i2.2)')   &
                    'Ant. ',r_kant(-i_base(ibs))
                   endif
                 else
                   if (mnant.le.9) then
                     write (header_1(k),'(a,i1.1)')   &
                       'Ant. ',-i_base(ibs)
                   else
                     write (header_1(k),'(a,i2.2)')   &
                       'Ant. ',-i_base(ibs)
                   endif
                 endif
               endif
               if (pol_select) then
                 header_2(k) = ' '
                 lk = 1
                   if (k_polar(k).eq.1) then
                     header_2(k)(lk:) = 'Pol.H'
                   else if (k_polar(k).eq.2) then
                     header_2(k)(lk:) = 'Pol.V'
                   else if (k_polar(k).eq.3) then
                     header_2(k)(lk:) = 'Pol.B'
                   endif
                   lk = lk+5
               elseif (bb_select) then
                    header_2(k)(1:) = 'BB '
                    lk = 4
                    if (l_baseband(ic).eq.r_nbb) then
                      header_2(k:)(lk:) = 'All'
                      lk = lk+4
                    else
                      do l =1, l_baseband(ic)
!                       write(header_2(k:)(lk:),'(i1,a)') i_baseband(l,ic),' '
!                       lk=lk+2
                         header_2(k:)(lk:) = r_bbname(i_baseband(l,ic))(1:4)
                         lk=lk+4
                      enddo
                    endif
               elseif (if_select) then
                    header_2(k)(1:) = 'IF '
                    lk = 4
                    if (l_if(ic).eq.r_nif) then
                      header_2(k:)(lk:) = 'All'
                      lk = lk+4
                    else
                      do l =1, l_if(ic)
                         write(header_2(k:)(lk:),'(i1,a)') i_if(l,ic),' '
                         lk=lk+2
!                       header_2(k:)(lk:) = r_bbname(i_baseband(l,ic))(1:2)
!                       lk=lk+3
                      enddo
                    endif
               elseif (subb_select) then
                 header_2(k) = ' '
                 lk = 1
                 do l = 1, l_subb(ic)
                   header_2(k)(lk:) = csub(i_subb(l,ic))//' '
                   lk = lk+4
                 enddo
               endif
               !
               ! Second pass to set header correctly
               if (bb_data(i_y(iy))) then
                 header_2(k)(1:) = 'BB '
                 lk = 4
                 if (l_baseband(ic).eq.r_nbb) then
                   header_2(k:)(lk:) = 'All'
                   lk = lk+4
                 else
                   do l =1, l_baseband(ic)
                      header_2(k:)(lk:) = r_bbname(i_baseband(l,ic))(1:4)
                      lk=lk+4
                   enddo
                 endif
               elseif(if_data(i_y(iy))) then
                 header_2(k)(1:) = 'IF '
                 lk = 4
                 if (l_if(ic).eq.r_nif) then
                   header_2(k:)(lk:) = 'All'
                   lk = lk+4
                 else
                   do l =1, l_if(ic)
                      write(header_2(k:)(lk:),'(i1,a)') i_if(l,ic),' '
                      lk=lk+2
                   enddo
                 endif
               endif
               header_3(k) = cband(i_band(ibd))
             enddo
           enddo
         enddo
       enddo
     enddo
  enddo
  ! Position boxes
  if (n_boxes.le.0) then
    call message(6,3,'SET_DISPLAY','Nothing selected')
    error = .true.
    return
  endif
  if (user_box) then
    call position_boxes_user(user_box_nx, user_box_ny,error)
  else
    call position_boxes(error)
  endif
  clear = .true.
  return
1005 format(' /VARIABLE X_DATA[',i4.4,'] Y_DATA[',i4.4,']')
end subroutine set_display
!
subroutine position_boxes(error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Position the boxes according to the Greg plot page if it has changed
  ! or if the number of boxes, or aspect ratio has changed.
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  ! include 'clic_xy_code.inc'
  ! Local
  logical :: errtmp
  integer :: m, n, j, k, i, saved_boxes
  real*4 :: xmax, ymax, ar1, ar2, gx1, gx2, gy1, gy2, bratio
  real*4 :: saved_x, saved_y, saved_ratio
  save saved_x, saved_y, saved_boxes
  data saved_x, saved_y, saved_ratio, saved_boxes/0.,0.,0., 0/
  !-----------------------------------------------------------------------
  call sic_get_real('PAGE_X',xmax,errtmp)
  call sic_get_real('PAGE_Y',ymax,errtmp)
  error = error .or. errtmp
  if (n_boxes.eq.saved_boxes .and. xmax.eq.saved_x   &
    .and. ymax.eq.saved_y .and. aratio.eq.saved_ratio) return
  !
  ! Leave a header space of aspect ratio (.08,1)
  ymax = ymax-xmax*.08
  bratio = abs(aratio)*0.8
  m = min(n_boxes,max(1,nint(sqrt(n_boxes/bratio*xmax/ymax))))
  n = n_boxes/m
  do while (m*n .lt. n_boxes)
    ar1 = (xmax/float(m+1))/(ymax /float(n))
    ar2 = (xmax/float(m))/(ymax /float(n+1))
    if (abs(ar1-bratio).lt.abs(ar2-bratio)) then
      m = m+1
    else
      n = n+1
    endif
  enddo
  k = 0
  xmax = xmax/m
  ymax = ymax/n
  if (aratio.gt.0) then
    if (ymax.gt.xmax/aratio/0.8) then
      ymax = xmax/aratio/0.8
    else
      xmax = ymax*aratio*0.8
    endif
  endif
  do j=1, n
    do i=1, m
      k = k+1
      gx1 = (i-1)*xmax + xmax*.15
      gx2 = i*xmax - xmax*.05
      gy1 = (n-j)*ymax + ymax*.20
      gy2 = (n-j+1)*ymax - ymax*.16
      c_setbox(k) = ' '
      write(c_setbox(k),'(a,4(1x,f7.2))') 'SET BOX',   &
        gx1 ,gx2 ,gy1 ,gy2
    enddo
  enddo
  saved_x = xmax
  saved_y = ymax
  saved_boxes = n_boxes
  saved_ratio = aratio
  return
end subroutine position_boxes
!
subroutine position_boxes_user(nc,nl, error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Position the boxes according to the Greg plot page if it has changed
  ! or if the number of boxes, or aspect ratio has changed.
  !---------------------------------------------------------------------
  logical :: error                  !
  integer :: nl
  integer :: nc
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  ! include 'clic_xy_code.inc'
  ! Local
  logical :: errtmp
  integer :: m, n, j, k, i, saved_boxes
  real*4 :: xmax, ymax, ar1, ar2, gx1, gx2, gy1, gy2, bratio
  real*4 :: saved_x, saved_y, saved_ratio
  save saved_x, saved_y, saved_boxes
  data saved_x, saved_y, saved_ratio, saved_boxes/0.,0.,0., 0/
  !-----------------------------------------------------------------------
  call sic_get_real('PAGE_X',xmax,errtmp)
  call sic_get_real('PAGE_Y',ymax,errtmp)
  error = error .or. errtmp
!  if (n_boxes.eq.saved_boxes .and. xmax.eq.saved_x   &
!    .and. ymax.eq.saved_y .and. aratio.eq.saved_ratio) return
  !
  ! Leave a header space of aspect ratio (.08,1)
  ymax = ymax-xmax*.08
  if (nc.le.0) nc = int(float(n_boxes-1)/nl)+1
  if (nl.le.0) nl = int(float(n_boxes-1)/nc)+1
  m = nc
  n = nl
  user_nboxes = min(nc*nl,n_boxes)
  k = 0
  xmax = xmax/m
  ymax = ymax/n
  if (aratio.gt.0) then
    if (ymax.gt.xmax/aratio/0.8) then
      ymax = xmax/aratio/0.8
    else
      xmax = ymax*aratio*0.8
    endif
  endif
  do j=1, n
    do i=1, m
      k = k+1
      gx1 = (i-1)*xmax + xmax*.15
      gx2 = i*xmax - xmax*.05
      gy1 = (n-j)*ymax + ymax*.20
      gy2 = (n-j+1)*ymax - ymax*.16
      c_setbox(k) = ' '
      write(c_setbox(k),'(a,4(1x,f7.2))') 'SET BOX',   &
        gx1 ,gx2 ,gy1 ,gy2
    enddo
  enddo
  saved_x = xmax
  saved_y = ymax
  saved_boxes = n_boxes
  saved_ratio = aratio
  return
end subroutine position_boxes_user
!
subroutine switch_spectral
  use gildas_def
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  integer :: i, j
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  call message(2,1,'SET_DISPLAY','Switching to SPECTRAL mode')
  plot_mode = 'SPEC'
  do i=1, n_subb
    do j=1, l_subb(i)
      if (i_subb(j,i).le.mbands) i_subb(j,i) = i_subb(j,i)+mbands
    enddo
  enddo
  change_display = .true.
  call show_display ('SUBBAND', .false.,error)
  return
end subroutine switch_spectral
!
subroutine switch_time
  use gildas_def
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  integer :: i, j
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  call message(2,1,'SET_DISPLAY','Switching to TIME mode')
  plot_mode = 'TIME'
  do i=1, n_subb
    do j=1, l_subb(i)
      if (i_subb(j,i).gt.mbands) i_subb(j,i) = i_subb(j,i)-mbands
    enddo
  enddo
  change_display = .true.
  call show_display ('SUBBAND', .false.,error)
  return
end subroutine switch_time
!
subroutine set_format(l,h,error)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: l             !
  character(len=*) :: h             !
  logical :: error                  !
  ! Local
  character(len=20) :: par,test
  integer :: lpar, npar, k
  !------------------------------------------------------------------------
  ! Code:
  h = ' '
  k = 0
  npar = 1
  do while (.true.)
    call sic_next (l(npar:),par,lpar,npar)
    if (lpar.eq.0) return
    test = par(1:lpar)
    call sic_upper(test)
    if (sic_varexist(test)) then
      h(k+1:) = ' '''//par(1:lpar)//''''
    else
      h(k+1:) = ' "'//par(1:lpar)//'"'
    endif
    k = k + 3 + lpar
  enddo
end subroutine set_format
!
subroutine setdef (line,error)
  use gkernel_interfaces
  use gildas_def
  use clic_index
  use clic_find
  use gio_params
  !---------------------------------------------------------------------
  ! CLIC	Internal routine
  !	Setup default parameters
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_title.inc'
  include 'clic_constant.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  include 'clic_flags.inc'
  include 'clic_proc_par.inc'
  ! Local
  real :: fourpi
  parameter (fourpi=4.0*pis)
  integer :: mfile, tprio, tfile, csev
  common /mesfil/ mfile, tprio, tfile, csev
  integer :: i, nch
  character(len=12) :: argum1
  logical :: all
  integer(kind=address_length) :: ip0
  !------------------------------------------------------------------------
  argum1 = 'ALL'
  call sic_ke(line,0,1,argum1,nch,.false.,error)
  if (error) return
  all = argum1(1:3).eq.'ALL' .or. argum1(1:3).eq.'DEF'
  !
  ! Selection criteria
  if (all .or. argum1(1:2).eq.'CR') then
    soffs1 = fourpi
    soffs2 = fourpi
    soffl1 = fourpi
    soffl2 = fourpi
    sobse1 = -32768
    sobse2 = 32767
    sredu1 = -32768
    sredu2 = 32767
    skind = 5
    sline  = '*'
    stole = 0.05*pi/180.0/3600.0
    nsour = 0
    nproc = 0
    nexcl = 1
    ssexcl(1) = p_band
    sproject = '*'
    ssourc = '*'
    steles = '*'
    squal  = 8
    snume1 = 0
    snume2 = 2147483647
    snscan = 1
    sscan1(1) = 0
    sscan2(1) = 2147483647
    sproject = '*'
    setic = -1
    sapc = -1
    sbpc = -1
    sitype = -1
    sproc = -1
    sut1 = 0
    sut2 = 2*pi
    shoura1 = -pi
    shoura2 = pi
    srecei = -1
    iskip = 1
  endif
  !
  ! Display
  if (all .or. argum1(1:2).eq.'DI') then
    !         WRITE(6,*) 'SET DISP /DEF'
    continuous = .false.
    all_base = .true.
    all_subb = .true.
    each_subb = .false.
    t_interfero = .true.
    t_position = .true.
    t_quality = .true.
    t_spectral = .false.
    t_calibration = .false.
    t_atmosphere = .false.
    t_status = .false.
    t_pointing = .false.
    t_step = 3.
    change_display = .true.
    n_x = 1
    i_x(1) = xy_time
    sm_x1(1) = '='
    sm_x2(1) = '='
    n_y = 2
    i_y(1) = xy_ampli
    sm_y1(1) = 'F'
    gu1_y(1) = 0.
    sm_y2(1) = '='
    i_y(2) = xy_phase
    sm_y1(2) = 'F'
    gu1_y(2) = -180.
    sm_y2(2) = 'F'
    gu2_y(2) = +180.
    n_base = mnbas
    do i=1, mnbas
      i_base(i) = i
      w_c(i) = 1.
    enddo
    user_box = .false.
    many_pages = .false.
    phys_base = .false.
    do_physical = .false.
    n_subb = 1
    l_subb(1) = 8
    do i=1, mrlband
      i_subb(i,1) = i
    enddo
    n_stokes = 1
    i_stokes(1) = code_stokes_none
    sw1(1) = '*'
    iw1(1) = 0
    sw2(1) = '*'
    iw2(1) = 0
    fdrop(1) = 0.05
    fdrop(2) = 0.05
    n_band = 1
    i_band(1) = 2              ! default to LSB !
    aratio = -2.0
    plot_type = 'POIN'
    call gr_exec('SET MARKER 4 0 0.1')
    i_average = 0
    j_average = 1
    t_average = 60.
    k_average = 0
    plot_mode = 'TIME'
    do_bin = .false.
    call set_display(error)
    ! reset the plot buffers to zero as a safety measure.
    ip0 = gag_pointer(data_x,memory)
    call init_data(m_data*m_boxes*10,memory(ip0),0)
    degrees = .true.
    do i=1, mnant
      atm_mode(i) = 'FILE'
      atm_trec(i) = -1.
      atm_gim(i) = -1.
    enddo
    atm_water = -3.
    atm_interpolate = .false.
    ! Causes problems, should be changed only by FILE
    !         WRITE_MODE = 'UPDA'
    do i=1,mrlband
      l_line(i,1) = '*'
      l_line(i,2) = '*'
    enddo
    c_line(1) = '*'
    c_line(2) = '*'
    n_cont = 1
    cont_select =.true.
    all_select =.true.
    !         LSUBB_OUT = 5
    !         DO I=1, MRLBAND
    !            ISUBB_OUT(I) = I+10          ! default to line subb.
    !         ENDDO
    isideb_out = 3             ! default to DSB
    freq_out = 0.
    nwin_out = 0
    do_pass_freq = .true.
    do_pass = .false.
    do_pass_antenna = .true.
    do_pass_memory = .false.
    do_pass_spectrum = .false.
    do_spidx = .false.
    do_spec = .false.
    rec_1(1) = 1
    rec_2(1) = 1000000000
    rec_3(1) = 1
    n_loop = 1
    new_ant_gain = .false.
    do_flux = .false.
    do_raw = .false.
    do_scale = .false.
    do_amplitude = .false.
    do_amplitude_antenna = .true.
    do_phase = .false.
    do_phase_ext = .false.
    do_phase_antenna = .true.
    do i=1, mnant
      do_phase_atm(i) = .false.
      do_phase_wvr(i) = .false.
      do_phase_wvr_e(i) = .false.
      do_phase_mon(i) = .false.
    enddo
    do_phase_nofile = .false.
    do_bar_atm = .false.
    save_pass = .false.
    save_amplitude = .false.
    save_phase = .false.
    save_scale = .false.
    save_flux = .false.
    save_spec = .false.
    save_raw = .false.
    ngibbs =  0
    nmask = 0
    cmask = 0
    uv_range(1) = 0
    uv_range(2) = 2000.
    !
    ! NGRx
    pol_subb = 0
    quar_subb = 0
    nbc_subb = 0
    do_polar = 0
    physical_power = .true.
    narrow_input = 0
    !
    ! Widex
    widex_unit = 0
    widex_subb = 0
    !
    call select_subb
    !
    ! Reset also Stokes parameters
    do_leakage = .false.
    do_corrected = .false.
    snpol = 0 ! and FINDPOL *
  endif
  !
  ! General
  if (all  .or. argum1(1:2).eq.'GE') then
    do_write_data = .true.
    !!$      do_write_data = .false.
    find_update = .false.
    ref_ant = 1
    weight_cal = .true.
    weight_tsys = .true.
    weight_pass = .false.
    defext(1) = '.hpb'
    defext(2) = '.ipb'
    ndefext = 2
    sheade = 'B'
    !         TPRIO = 1 already in message_init (clic.fpp)
    sangle = 'S'
    fangle = 60.0d0*180.d0*60.d0/pi
    do_sort_receiver = .false.
    do_sort_num = .false.
    planet_model = -1
    atmmodel = '1985'
    new_receivers = .true.
    skip_flag = .false.
    !
    ! Also reset masks and marks
    do i=1,mnant
      ant_mask(i) =  .false.
      san_mask(:,i) = .false.
      ant_kill(i) = .false.
      san_kill(:,i) = .false.
    enddo
    do i=1,mnbas
      bas_mask(i) = .false.
      sba_mask(:,i) = .false.
      bas_kill(i) = .false.
      sba_kill(:,i) = .false.
    enddo
  endif
  return
end subroutine setdef
!
subroutine set_data(np, nb)
  use gildas_def
  use gkernel_interfaces
  integer :: np                     !
  integer :: nb                     !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  ! Local
  character(len=80) :: ch
  integer :: m_size, itest
  parameter (m_size=30*147328)
  !------------------------------------------------------------------------
  ! Code:
  nb = max(min(mbox,nb),1)
  if (np.le.0) then
    np = max(m_data*m_boxes,m_size)/nb
  endif
  itest = np*nb*10/512
  if (np*nb.gt.m_data*m_boxes) then
    if (m_data*m_boxes .gt.0) then
      call free_vm(m_data*m_boxes*10,data_x)   ! 10 words per point
    endif
    if (sic_getvm(np*nb*10,data_x).ne.1) then
      call gagout   &
        ('F-SET_DATA,  Fatal shortage of virtual memory')
      call sysexi(fatale)
    endif
  endif
  m_data = np
  m_boxes = nb
  data_y = data_x + m_data*m_boxes*4   ! y coords.
  data_z = data_y + m_data*m_boxes*4   ! complex vis.
  data_w = data_z + m_data*m_boxes*8   ! weights
  data_i = data_w + m_data*m_boxes*4   ! source id.
  data_r = data_i + m_data*m_boxes*4   ! rec. numbers
  data_u = data_r + m_data*m_boxes*4   ! obs numbers
  data_s = data_u + m_data*m_boxes*4   ! scratch space : 2 words/pt
  write(ch,'(A,I6,A,I6,A)') 'Displaying ',m_data,   &
    ' points in each of ',m_boxes,' boxes '
  call message(2,1,'SET_DATA',ch(1:lenc(ch)))
end subroutine set_data
!
subroutine coffse(line,offset,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! SAS	Internal routine
  !	Converts the formatted offset angle value in current units to
  !	internal value.
  ! Arguments :
  !	LINE	C*(*)	Formatted value			Input
  !	OFFSET	R*4	Offset value in radian		Output
  !	ERROR	L	Logical error flag		Output
  ! (5-mar-1985)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  real*4 :: offset                  !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  real :: fourpi
  parameter (fourpi=4.0*pi)
  !
  real*8 :: dble
  integer :: n
  !
  n = lenc(line)
  if (line(1:n).eq.'*') return
  call sic_math_dble(line,n,dble,error)
  if (error) return
  offset = dble/fangle
  return
  !
entry offsec(offset,line,error)
  !----------------------------------------------------------------------
  ! 	Converts an offset value in radian to a formatted string in
  !	current angle units.
  ! Arguments :
  !	OFFSET	R*4	Offset value			Input
  !	LINE	C*(*)	Formatted string		Output
  !       ERROR	L	Logical error flag		Output
  ! (5-mar-1985)
  !----------------------------------------------------------------------
  if (offset.eq.fourpi) then
    line = '  *'
  elseif (abs(offset).lt.stole) then
    line = '  0.0 '
  else
    dble = offset*fangle
    write(line,'(f7.1)') dble
  endif
end subroutine coffse
!
subroutine setfor(line,error)
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_title.inc'
  ! Local
  integer :: mvoc1, nkey, nch, mvoc2
  parameter (mvoc1=12, mvoc2=2)
  character(len=12) :: argum, kw1, kw2, voc1(mvoc1), voc2(mvoc2)
  logical :: flag
  !
  data voc1/'BRIEF','LONG','FULL','POSITION',   &
    'QUALITY','SPECTRAL','CALIBRATION',   &
    'ATMOSPHERE','STATUS','INTERFERO','POINTING',   &
    'LIST'/
  data voc2/'ON','OFF'/
  !
  kw1 = ' '
  call clic_kw(line,0,2,kw1,nkey,voc1,mvoc1,.true.,error,.true.)
  if (kw1.eq.'BRIEF' .or. kw1.eq.'LONG' .or.  kw1.eq.'FULL') then
    sheade = kw1(1:1)
    return
  endif
  kw2 = 'ON'
  call clic_kw(line,0,3,kw2,nkey,voc2,mvoc2,.false.,error,.true.)
  if (error) return
  if (kw2.eq.'ON') then
    flag = .true.
  elseif (kw2.eq.'OFF') then
    flag = .false.
  endif
  if (kw1.eq.'POSITION') then
    t_position = flag
  elseif (kw1.eq.'QUALITY') then
    t_quality = flag
  elseif (kw1.eq.'SPECTRAL') then
    t_spectral = flag
  elseif (kw1.eq.'CALIBRATION') then
    t_calibration = flag
  elseif (kw1.eq.'ATMOSPHERE') then
    t_atmosphere = flag
  elseif (kw1.eq.'STATUS') then
    t_status = flag
  elseif (kw1.eq.'INTERFERO') then
    t_interfero = flag
  elseif (kw1.eq.'POINTING') then
    t_pointing = flag
  elseif (kw1.eq.'LIST') then
    t_list = flag
  endif
  return
end subroutine setfor
!
subroutine reset_phases
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Process phases in plot buffer so that they are between -pi and pi,
  ! and optionnally insure continuity for plot.
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: angle_data
  integer :: ix, iy, ib, nc, ko
  integer(kind=address_length) :: ipx,ipy,ipw
  !------------------------------------------------------------------------
  ! Code:
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  do ib = 1, n_boxes
    nc = min(n_cont, n_data(ib)/2)
    ix = i_x(k_x(ib))
    if (angle_data(ix) .and. ix.ne.xy_lo_ra) then
      ko = (ib-1)*m_data
      call prophase(memory(ipy+ko),memory(ipx+ko),n_data(ib),   &
        memory(ipw+ko),degrees,continuous,nc)
    endif
    iy = i_y(k_y(ib))
    if (angle_data(iy) .and. iy.ne.xy_lo_ra) then
      ko = (ib-1)*m_data
      call prophase(memory(ipx+ko),memory(ipy+ko),n_data(ib),   &
        memory(ipw+ko),degrees,continuous,nc)
    endif
  enddo
end subroutine reset_phases
!
subroutine prophase(x,p,n,w,degrees,continuous,nc)
  real*4 :: x(*)                    !
  real*4 :: p(*)                    !
  integer :: n                      !
  real*4 :: w(*)                    !
  logical :: degrees                !
  logical :: continuous             !
  integer :: nc                     !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  integer :: i, j, k
  real*4 :: sp, pd, step, wmin
  !------------------------------------------------------------------------
  ! Code:
  if (degrees) then
    step = 360.
    wmin = 1e-8*(pi/180.)**2
  else
    step = 2.*pi
    wmin = 1e-8
  endif
  if (continuous) then
    do i=1, n
      if (w(i).gt.wmin) then
        pd = 0
        if (i.gt.1) then
          sp = 0
          j = 1
          k = 1
          do while (k.le.nc .and. (i-j).gt.0)
            if (w(i-j).gt.0) then
              sp = sp + p(i-j)
              k = k + 1
            endif
            j = j + 1
          enddo
          if (k.gt.1) pd = sp/(k-1)
        endif
        if (pd.ne.0) then
          p(i) = mod(p(i)-pd+101.5*step,step)-0.5*step+pd
        endif
      endif
    enddo
    do i=n,1,-1
      if (w(i).gt.wmin) then
        pd = 0
        if (i.le.n-nc) then
          sp = 0
          j = 1
          k = 1
          do while (k.le.nc .and. (i+j).le.n)
            if (w(i+j).gt.0) then
              sp = sp + p(i+j)
              k = k + 1
            endif
            j = j + 1
          enddo
          if (k.gt.1) pd = sp/(k-1)
        endif
        if (pd.ne.0) then
          p(i) = mod(p(i)-pd+101.5*step,step)-0.5*step+pd
        endif
      endif
    enddo
  else
    do i=1, n
      if (w(i).gt.0) then
        p(i) = mod(p(i)+21.5*step,step)-0.5*step
      endif
    enddo
  endif
  return
end subroutine prophase
!
subroutine prophase_r8(x,p,n,w,degrees,continuous,nc)
  real*8 :: x(*)                    !
  real*8 :: p(*)                    !
  integer :: n                      !
  real*8 :: w(*)                    !
  logical :: degrees                !
  logical :: continuous             !
  integer :: nc                     !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  integer :: i, j, k
  real*4 :: sp, pd, step, wmin
  !------------------------------------------------------------------------
  ! Code:
  if (degrees) then
    step = 360.
    wmin = 1e-8*(pi/180.)**2
  else
    step = 2.*pi
    wmin = 1e-8
  endif
  if (continuous) then
    do i=1, n
      if (w(i).gt.wmin) then
        pd = 0
        if (i.gt.1) then
          sp = 0
          j = 1
          k = 1
          do while (k.le.nc .and. (i-j).gt.0)
            if (w(i-j).gt.0) then
              sp = sp + p(i-j)
              k = k + 1
            endif
            j = j + 1
          enddo
          if (k.gt.1) pd = sp/(k-1)
        endif
        if (pd.ne.0) then
          p(i) = mod(p(i)-pd+101.5*step,step)-0.5*step+pd
        endif
      endif
    enddo
    do i=n,1,-1
      if (w(i).gt.wmin) then
        pd = 0
        if (i.le.n-nc) then
          sp = 0
          j = 1
          k = 1
          do while (k.le.nc .and. (i+j).le.n)
            if (w(i+j).gt.0) then
              sp = sp + p(i+j)
              k = k + 1
            endif
            j = j + 1
          enddo
          if (k.gt.1) pd = sp/(k-1)
        endif
        if (pd.ne.0) then
          p(i) = mod(p(i)-pd+101.5*step,step)-0.5*step+pd
        endif
      endif
    enddo
  else
    do i=1, n
      if (w(i).gt.0) then
        p(i) = mod(p(i)+21.5*step,step)-0.5*step
      endif
    enddo
  endif
  return
end subroutine prophase_r8

!
subroutine cvphase(n,p,w,degrees,old_degrees)
  !---------------------------------------------------------------------
  ! Convert phases between degrees and radians
  !---------------------------------------------------------------------
  integer :: n                      !
  real*4 :: p(n)                    !
  real*4 :: w(n)                    !
  logical :: degrees                !
  logical :: old_degrees            !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  integer :: i
  !------------------------------------------------------------------------
  ! Code:
  if (degrees .and. .not.old_degrees) then
    do i=1, n
      if (w(i).gt.0) then
        p(i) = p(i) * 180. / pi
        w(i) = w(i) * (pi/180.)**2
      endif
    enddo
  elseif (.not.degrees .and. old_degrees) then
    do i=1, n
      if (w(i).gt.0) then
        p(i) = p(i) / 180. * pi
        w(i) = w(i) / (pi/180.)**2
      endif
    enddo
  endif
  return
end subroutine cvphase
!
subroutine switch_antenna
  use gildas_def
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_number.inc'
  ! Local
  logical :: la(mnant), error
  integer :: ia, ib
  !------------------------------------------------------------------------
  ! Code:
  if (i_base(1).lt.0) return
  do ia=1, mnant
    la(ia) = .false.
  enddo
  do ib = 1, n_base
    la(antbas(1,i_base(ib))) = .true.
    la(antbas(2,i_base(ib))) = .true.
  enddo
  n_base = 0
  do ia=1, mnant
    if (la(ia)) then
      n_base = n_base+1
      i_base(n_base) = -ia
    endif
  enddo
  call show_display('BASELINE',.false.,error)
  return
end subroutine switch_antenna
!
subroutine switch_baseline
  use gildas_def
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_number.inc'
  ! Local
  logical :: lb(mnbas), error
  integer :: ia, ib, i, j, ja
  !------------------------------------------------------------------------
  ! Code:
  if (i_base(1).gt.0) return
  do ib=1, mnbas
    lb(ib) = .false.
  enddo
  do i = 1, n_base-1
    ia = -i_base(i)
    do j = i+1, n_base
      ja = -i_base(j)
      lb (basant(ia,ja)) = .true.
    enddo
  enddo
  n_base = 0
  do ib=1, mnbas
    if (lb(ib)) then
      n_base = n_base+1
      i_base(n_base) = ib
    endif
  enddo
  call show_display('BASELINE',.false.,error)
  return
end subroutine switch_baseline
!
subroutine set_all_baselines
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! take all currect baselines
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  integer :: ib
  !------------------------------------------------------------------------
  ! Code:
  if (i_base(1).gt.0) then
    n_base = max(1,r_nbas)
    do ib=1, n_base
      i_base(ib) = ib
    enddo
  else
    n_base = max(1,r_nant)
    do ib=1, n_base
      i_base(ib) = -ib
    enddo
  endif
  return
end subroutine set_all_baselines
!
subroutine set_all_subbands
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! take all current subbands
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: is, nspw
  !------------------------------------------------------------------------
  ! Code:
  if (i_subb(1,1).le.mbands) then
    if (lowres) then 
      nspw = r_nbb
    else
      nspw = r_nband
    endif
    l_subb(1) = max(1,nspw)
    i_subb(1,1) = 1
  else
    if (lowres) then 
      nspw = r_nbb
    else
      nspw = r_lband
    endif
    l_subb(1) = max(1,nspw)
    i_subb(1,1) = 1+mbands
  endif
  do is=2, l_subb(1)
    i_subb(is,1) = i_subb(1,1)+is-1
  enddo
  n_subb = 1
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_all_subbands
!
subroutine set_each_subband
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! take all current subbands
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: is, nspw
  !------------------------------------------------------------------------
  ! Code:
  if (i_subb(1,1).le.mbands) then
    if (lowres) then 
      nspw = r_nbb
    else
      nspw = r_nband
    endif
    n_subb = max(1,nspw)
    i_subb(1,1) = 1
  else
    if (lowres) then 
      nspw = r_nbb
    else
      nspw = r_lband
    endif
    n_subb = max(1,nspw)
    i_subb(1,1) = 1+mbands
  endif
  do is=1, n_subb
    l_subb(is) = 1
    i_subb(1,is) = is-1+i_subb(1,1)
    sw1(is) = '*'
    iw1(is) = 0
    sw2(is) = '*'
    iw2(is) = 0
  enddo
  return
end subroutine set_each_subband
!
subroutine set_pol_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! select current subbands according to their polarization
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, n, pol, factor, nspw
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  n_subb = 0
  if (pol_subb.eq.1) then
    call message(2,1,'SET_POL_SUBBANDS',   &
      'Selecting subbands with horizontal polarization')
  elseif (pol_subb.eq.2) then
    call message(2,1,'SET_POL_SUBBANDS',   &
      'Selecting subbands with vertical polarization')
  endif
  !
  ! No data read so far - select LO1
  !
  if (r_lband.eq.0) then
    n_subb = 1
    l_subb(1) = 1
    i_subb(1,1) = 1+mbands
    return
  endif
  !
  if (lowres) then
    nspw = r_nbb
  else
    nspw = r_lband
  endif
  !
  ! Data already read
  !
  if (.not.new_receivers) then
    call message(2,3,'SET_POL_SUBBANDS','Need new receivers data')
    error = .true.
    return
  endif
  n = 0
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  do i = 1, nspw
    if (r_lpolmode(i).ne.1) then
      call message(2,3,'SET_POL_SUBBANDS',   &
        'Cannot select subbands on polarization criteria')
      pol_subb = 0
      error = .true.
      return
    endif
    pol = r_lpolentry(1,i)     ! Same pol. entry for all antennas
    if (pol.eq.pol_subb) then
      n_subb = 1
      n = n+1
      l_subb(1) = n
      i_subb(n,1) = i+factor
    endif
  enddo
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_POL_SUBBANDS', 'No subbands found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_pol_subbands
!
subroutine set_each_pol_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! select current subbands according to their polarization
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, n, pol, factor, ip, nspw
  logical :: is_pol(m_pol_rec)
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  n_subb = 0
!  if (pol_subb.eq.1) then
!    call message(2,1,'SET_POL_SUBBANDS',   &
!      'Selecting subbands with horizontal polarization')
!  elseif (pol_subb.eq.2) then
!    call message(2,1,'SET_POL_SUBBANDS',   &
!      'Selecting subbands with vertical polarization')
!  endif
  !
  ! No data read so far - select LO1
  !
  if (r_lband.eq.0) then
    n_subb = 1
    l_subb(1) = 1
    i_subb(1,1) = 1+mbands
    return
  endif
  !
  if (lowres) then
    nspw = r_nbb
  else
    nspw = r_lband
  endif
  !
  ! Data already read
  !
  if (.not.new_receivers) then
    call message(2,3,'SET_POL_SUBBANDS','Need new receivers data')
    error = .true.
    return
  endif
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  do ip = 1,m_pol_rec
     n = 0
     is_pol(ip) = .false.
     do i = 1, nspw
       if (r_lpolmode(i).ne.1) then
         call message(2,3,'SET_POL_SUBBANDS',   &
           'Cannot select subbands on polarization criteria')
         pol_subb = 0
         error = .true.
         return
       endif
       pol = r_lpolentry(1,i)     ! Same pol. entry for all antennas
       if (pol.eq.ip) then
         if (.not.is_pol(ip)) then
           is_pol(ip) = .true.
           n_subb = n_subb+1
         endif
         n = n+1
         l_subb(n_subb) = n
         i_subb(n,n_subb) = i+factor
       endif
     enddo
     if (.not.is_pol(ip)) then
        if (ip.eq.1) then
           call message(2,3,'SET_EACH_POL_SUBBANDS',   &
             'did not find subbands with horizontal polarization')
        elseif (ip.eq.2) then
           call message(2,3,'SET_EACH_POL_SUBBANDS',   &
             'did not find subbands with horizontal polarization')
        endif
     endif
  enddo
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_EACH_POL_SUBBANDS', 'No subbands found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  sw1(2) = '*'
  iw1(2) = 0
  sw2(2) = '*'
  iw2(2) = 0
  return
end subroutine set_each_pol_subbands
!
subroutine set_quar_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! select current subbands according to the quarter they are connected to
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  integer :: quarter
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, n, pol, factor, qq
  character(len=2) :: quar(4)
  data quar/'Q1','Q2','Q3','Q4'/
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  n_subb = 0
  call message(2,1,'SET_QUAR_SUBBANDS',   &
    'Selecting subbands connected to '//quar(quar_subb))
  !
  ! No data read so far - select LO1
  !
  if (r_lband.eq.0) then
    n_subb = 1
    l_subb(1) = 1
    i_subb(1,1) = 1+mbands
    return
  endif
  !
  ! Data already read
  !
  if (.not.new_receivers) then
    call message(2,3,'SET_QUAR_SUBBANDS','Need new receivers data')
    error = .true.
    return
  endif
  n = 0
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  do i = 1,r_lband
    qq = quarter(r_flo2(i),r_flo2bis(i))
    if (qq.eq.quar_subb) then
      n_subb = 1
      n = n+1
      l_subb(1) = n
      i_subb(n,1) = i+factor
    endif
  enddo
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_QUAR_SUBBANDS', 'No subbands found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_quar_subbands
!
subroutine set_nbc_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! select current subbands according to the Narrow Band Correlator entry
  ! they are connected to
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, n, pol, factor, ent
  character(len=60) :: chain
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  n_subb = 0
  write (chain,'(a,i1)') 'Selecting subbands connected '//   &
    'to correlator input ',nbc_subb
  call message(2,1,'SET_NBC_SUBBANDS',chain)
  !
  ! No data read so far - select LO1
  !
  if (r_lband.eq.0) then
    n_subb = 1
    l_subb(1) = 1
    i_subb(1,1) = 1+mbands
    return
  endif
  !
  ! Data already read
  !
  if (.not.new_receivers) then
    call message(2,3,'SET_NBC_SUBBANDS','Need new receivers data')
    error = .true.
    return
  endif
  n = 0
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  do i = 1,r_lband
    ent = r_bb(i)
    if (ent.eq.nbc_subb) then
      n_subb = 1
      n = n+1
      l_subb(1) = n
      i_subb(n,1) = i+factor
    endif
  enddo
  !
  if (n_subb.eq.0) then
    if (allow_pol) then
      call message(6,3,'SET_NBC_SUBBANDS', 'No subbands found, using L09')
      n_subb = 1
      l_subb(1) = 1
      i_subb(1,1) = 9+mbands
    else
      call message(8,4,'SET_NBC_SUBBANDS', 'No subbands found')
      error =.true.
    endif
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_nbc_subbands
!
subroutine set_widex_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! select a widex subband
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, n, pol, factor, ent
  character(len=60) :: chain
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  n_subb = 0
  write (chain,'(a,i1)') 'Selecting the WIDEX unit '//   &
    'number ',widex_subb
  call message(2,1,'SET_WIDEX_SUBBAND',chain)
  !
  ! No data read so far - select LO9
  !
  if (r_lband.eq.0) then
    n_subb = 1
    l_subb(1) = 1
    i_subb(1,1) = 9+mbands
    return
  endif
  !
  ! Data already read
  !
  if (.not.new_receivers) then
    call message(2,3,'SET_NBC_SUBBANDS','Need new receivers data')
    error = .true.
    return
  endif
  n = 0
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
   if (widex_subb.le.r_nband_widex) then
      i = r_lband-r_nband_widex+widex_subb
      n_subb = 1
      n = n+1
      l_subb(1) = 1
      i_subb(n,1) = i+factor
    endif
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_WIDEX_SUBBAND', 'WIDEX subband not found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_widex_subbands
!
subroutine set_all_bbands
  use gildas_def
  !
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  integer :: ibb
  !
  if (r_nbb.le.0) return
  !
  n_baseband = 1
  l_baseband(1) = r_nbb
  do ibb =1, r_nbb
    i_baseband(ibb,1) = r_mapbb(ibb)
  enddo
  return
end subroutine set_all_bbands

subroutine set_bb_subbands(error)
  use gildas_def
  !
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  logical :: error
  !
  integer :: bb, ibb, nbb, factor, ispw, nspw
  !
  error = .false.
  n_subb = 0
  if (lowres) then
    nspw = r_nbb
  else
    nspw = r_lband
  endif
  !
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  !
  do nbb=1, n_baseband
    n_subb = n_subb+1
    l_subb(n_subb) = 0
    do ibb = 1, l_baseband(nbb)
      bb = i_baseband(ibb,nbb)
      do ispw = 1, nspw
        if (r_bb(ispw).eq.bb) then
          l_subb(n_subb) = l_subb(n_subb) + 1
          i_subb(l_subb(n_subb),n_subb) = ispw + factor
        endif
      enddo
    enddo
  enddo
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_BB_SUBBAND', 'baseband subband not found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_bb_subbands
!
subroutine set_subbands_bb(error)
  use gildas_def
  !
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  logical :: error
  !
  logical :: new_baseband
  integer :: isg, is, js, ibb, jbb
  !
  n_baseband = n_subb
  l_baseband = 0
  i_baseband = 0
  !
  do isg = 1, n_subb
    do is = 1, l_subb(isg)
      js = i_subb(is,isg)
      if (js.gt.mbands) js=js-mbands
      jbb = r_bb(js)
      if (l_baseband(isg).eq.0) then
        l_baseband(isg) = 1
        i_baseband(1,isg) = jbb
      else
        new_baseband = .true.
        do ibb = 1, l_baseband(isg)
          if (jbb.eq.i_baseband(ibb,isg)) then
            new_baseband = .false.
            exit
          endif
        enddo
        if (new_baseband) then
          l_baseband(isg) = l_baseband(isg)+1
          i_baseband(l_baseband(isg),isg) = jbb
        endif
      endif
    enddo
  enddo
  ! 
  return
end subroutine set_subbands_bb
!
subroutine set_subbands_if(error)
  use gildas_def
  !
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  logical :: error
  !
  logical :: new_if
  integer :: isg, is, js, iif, jif
  !
  n_if = n_subb
  l_if = 0
  i_if = 0
  !
  do isg = 1, n_subb
    do is = 1, l_subb(isg)
      js = i_subb(is,isg)
      if (js.gt.mbands) js=js-mbands
      jif = r_if(js)
      if (l_if(isg).eq.0) then
        l_if(isg) = 1
        i_if(1,isg) = jif
      else
        new_if = .true.
        do iif = 1, l_if(isg)
          if (jif.eq.i_if(iif,isg)) then
            new_if = .false.
            exit
          endif
        enddo
        if (new_if) then
          l_if(isg) = l_if(isg)+1
          i_if(l_if(isg),isg) = jif
        endif
      endif
    enddo
  enddo
  ! 
  return
end subroutine set_subbands_if
!
subroutine set_if_subbands(error)
  use gildas_def
  !
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  logical :: error
  !
  integer :: iif, jif, nif, factor, ispw, nspw
  !
  error = .false.
  n_subb = 0
  if (lowres) then
    nspw = r_nbb
  else
    nspw = r_lband
  endif
  !
  if (i_subb(1,1).le.mbands) then
    factor = 0
  else
    factor = mbands
  endif
  !
  do iif=1, n_if
    n_subb = n_subb+1
    l_subb(n_subb) = 0
    do jif = 1, l_if(iif)
      nif = i_if(jif,iif)
      do ispw = 1, nspw
        if (r_if(ispw).eq.nif) then
          l_subb(n_subb) = l_subb(n_subb) + 1
          i_subb(l_subb(n_subb),n_subb) = ispw + factor
        endif
      enddo
    enddo
  enddo
  !
  if (n_subb.eq.0) then
    call message(2,2,'SET_IF_SUBBAND', 'IF subband not found')
    error =.true.
  endif
  sw1(1) = '*'
  iw1(1) = 0
  sw2(1) = '*'
  iw2(1) = 0
  return
end subroutine set_if_subbands
!
subroutine check_pol_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! check the polarization of all subbands and update DO_POLAR accordingly
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, ii, pol, savepol
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  !
  ! If several groups: unclear what to do, but a priori only
  ! for plotting, not calibration
  !
  if (n_subb.gt.1) then
    do_polar = 0
    return
  endif
  !
  ! If only one group
  !
  do i = 1,l_subb(1)
    ii = i_subb(i,1)
    if (ii.gt.mbands) then
      ii = ii-mbands
    endif
    if (r_lpolmode(ii).ne.1) then
      do_polar = 0
      return
    endif
    pol = r_lpolentry(1,ii)    ! Same pol. entry for all antennas
    if (i.eq.1) then
      savepol = pol
      do_polar = savepol
    endif
    if (pol.ne.savepol) then
      do_polar = 3
      return
    endif
  enddo
  return
end subroutine check_pol_subbands
!
subroutine check_nbc_subbands(error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! check the NBC entry of all subbands and update NARROW_INPUT accordingly
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: i, ii, nbc, savenbc
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  !
  ! If several groups: unclear what to do, but a priori only
  ! for plotting, not calibration
  !
  if (n_subb.gt.1) then
    narrow_input = 0
    return
  endif
  !
  ! If only one group
  !
  do i = 1,l_subb(1)
    ii = i_subb(i,1)
    if (ii.gt.mbands) then
      ii = ii-mbands
    endif
    if (r_lpolmode(ii).ne.1) then
      narrow_input = 0
      return
    endif
    nbc = r_bb(ii)
    if (i.eq.1) then
      savenbc = nbc
      narrow_input = nbc
    endif
    if (nbc.ne.savenbc) then
      narrow_input = 0
      return
    endif
  enddo
  return
end subroutine check_nbc_subbands
!
function quarter(flo2,flo2bis)
  !---------------------------------------------------------------------
  ! This function returns the quarter to which an unit is connected
  ! It depends on the values of FLO2 and FLO2BIS
  !---------------------------------------------------------------------
  integer :: quarter                !
  real*8 :: flo2                    !
  real*8 :: flo2bis                 !
  !
  quarter = 0
  if (flo2.eq.8100) then
    if (flo2bis.eq.4000) quarter=1
    if (flo2bis.eq.2000) quarter=2
  elseif (flo2.eq.9900) then
    if (flo2bis.eq.4000) quarter=3
    if (flo2bis.eq.2000) quarter=4
  endif
  return
end function quarter
!
function nbc_entry(lunit,punit,flo2,flo2bis,lpolmode,lpolentry)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! This function returns the correlator entry to which an unit is
  ! connected (for narrow-band correlator) or the "logical" widex unit 
  ! number. 
  !
  ! First correlator entry can only be  Q1H, Q2H, Q3V, or Q4V
  ! Second correlator entry can only be Q3H, Q4H, Q1V, or Q2V
  !---------------------------------------------------------------------
  integer :: nbc_entry              !
  integer :: lunit                  !
  integer :: punit                  !
  real*8 :: flo2                    !
  real*8 :: flo2bis                 !
  integer :: lpolmode               !
  integer :: lpolentry              !
  ! Global
  integer :: quarter
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: qq
  !
  if (.not.new_receivers) then
    nbc_entry = 1
    return
  endif
  nbc_entry = 0
  ! Direct link between pol. entry and correlator entry...
  if (r_presec(alma_sec)) then
    nbc_entry = lpolentry
  ! Bure case is more intricate:
  else
    if (punit.lt.9) then
       qq = quarter(flo2,flo2bis)
       if (lpolmode.eq.1) then
         if (lpolentry.eq.1) then
           if (qq.le.2) nbc_entry = 1
           if (qq.gt.2) nbc_entry = 2
         else
           if (qq.le.2) nbc_entry = 2
           if (qq.gt.2) nbc_entry = 1
         endif
       endif
    else
       nbc_entry = lunit - r_lband + r_nband_widex + mnbcinput
    endif
  endif
  return
end function nbc_entry
!
subroutine set_numbers
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! sets up numbering system for baselines, triangles, quadrangles.
  !---------------------------------------------------------------------
  ! Global
  include 'clic_parameter.inc'
  include 'clic_number.inc'
  include 'clic_display.inc'
  include 'clic_work.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, j, k, l, ib, it, iq
  character(len=80) :: chain
  !------------------------------------------------------------------------
  write(chain,'(a,i2,a)') 'This version is for ',   &
    mnant,' antennas'
  call message(8,1,'SET_NUMBERS',chain)
  !
  ! Check WORK space area
  !!$      mmSEC = MAX(MGEN,MPOS,MINTERC,MRFSET,MCONTSET,MLINESET,
  !!$     &     MSCANNING,MATPARM,MATMON,MBPC,MABPC,MIC,MAIC,
  !!$     &     MDESCR,MDMODIF,MDFILE,MWVR,Malma)
  if (mdata.lt.4*mnbas*mcch) then
    print *,'MDATA ',mdata,' is too small for COPY '
    print *,' Increase to ',4*mnbas*mcch
    call sysexi(fatale)
  elseif (mdata.lt.mmsec) then
    print *,'MDATA ',mdata,' is too small for RALL '
    print *,' Increase to ',mmsec
    call sysexi(fatale)
  endif
  write(chain,'(a,i12)') 'DATA_BUFFER size ',mdata
  call message(8,1,'SET_NUMBERS',chain)
  !
  ! antbas(1,i) is the first ant. of bas. i
  ! antbas(2,i) is the second ant. of bas. i
  do j=2, mnant
    do i=1, mnant-1
      basant(i,j) = 0
    enddo
  enddo
  ib = 0
  do j=2, mnant
    do i=1, j-1
      ib = ib+1
      basant(i,j) = ib
      antbas(1,ib) = i
      antbas(2,ib) = j
      if (mnant.le.9) then
        write (cbas(ib),'(2i1)') i,j
      else
        write (cbas(ib),'(i2.2,1h-,i2.2)') i,j
      endif
    enddo
  enddo
  !      print *, 'basant', basant
  !      print *, 'antbas', antbas
  ! triangles
  ! anttri(1,i) is the first ant. of tri. i
  ! anttri(2,i) is the second ant. of tri. i
  ! anttri(3,i) is the third ant. of tri. i
  ! bastri(1,i) is side 12 of triangle i
  ! bastri(2,i) is side 13 of triangle i
  ! bastri(3,i) is side 23 of triangle i
  do k=3, mnant
    do j=2, mnant-1
      do i=1, mnant-2
        triant(i,j,k) = 0
      enddo
    enddo
  enddo
  it = 0
  do k=3, mnant
    do j=2, k-1
      do i=1, j-1
        it = it+1
        triant(i,j,k) = it
        anttri(1,it) = i
        anttri(2,it) = j
        anttri(3,it) = k
        bastri(1,it) = basant(i,j)
        bastri(2,it) = basant(i,k)
        bastri(3,it) = basant(j,k)
        if (mnant.le.9) then
          write (ctri(it),'(3i1)') i,j, k
        else
          write (ctri(it),'(i2.2,1h-,i2.2,1h-,i2.2)') i,j,k
        endif
      enddo
    enddo
  enddo
  !      print *, 'triant ', triant
  !      print *, 'anttri ', anttri
  !      print *, 'bastri ', bastri
  ! quadrangles
  ! antquad(1,i) is the first ant. of quad. i
  ! antquad(2,i) is the second ant. of quad. i
  ! antquad(3,i) is the third ant. of quad. i
  ! antquad(4,i) is the 4th ant. of quad. i
  ! basquad(1,i) is side 12 of quadr. i
  ! basquad(2,i) is side 13 of quadr. i
  ! basquad(3,i) is side 23 of quadr. i
  ! basquad(4,i) is side 14 of quadr. i
  ! basquad(5,i) is side 24 of quadr. i
  ! basquad(6,i) is side 34 of quadr. i
  do l=4, mnant
    do k=3, mnant-1
      do j=2, mnant-2
        do i=1, mnant-3
          quadant(i,j,k,l) = 0
        enddo
      enddo
    enddo
  enddo
  iq = 0
  do l=4, mnant
    do k=3, l-1
      do j=2, k-1
        do i=1, j-1
          iq = iq+1
          quadant(i,j,k,l) = iq
          antquad(1,iq) = i
          antquad(2,iq) = j
          antquad(3,iq) = k
          antquad(4,iq) = l
          basquad(1,iq) = basant(i,j)
          basquad(2,iq) = basant(i,k)
          basquad(3,iq) = basant(j,k)
          basquad(4,iq) = basant(i,l)
          basquad(5,iq) = basant(j,l)
          basquad(6,iq) = basant(k,l)
        enddo
      enddo
    enddo
  enddo
  !      print *, 'quadant ', quadant
  !      print *, 'antquad ', antquad
  !      print *, 'basquad ', basquad
  do i=1, mbands
    write(csub(i),'(a,i3.3)') 'C',i
    write(csub(i+mbands),'(a,i3.3)') 'L',i
  enddo
  !
  do i=1, mbands
    write(sf(i),'(a,i3.3)') 'C',i
    write(sf(i+mbands),'(a,i3.3)') 'L',i
  enddo
  return
end subroutine set_numbers
!
subroutine select_subb
  use gildas_def
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  !
  subb_select = .true.
  pol_select  = .false.
  bb_select   = .false.
  if_select   = .false.
  return 
end subroutine select_subb
!
subroutine select_pol
  use gildas_def
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  !
  subb_select = .false.
  pol_select  = .true.
  bb_select   = .false.
  if_select   = .false.
  return 
end subroutine select_pol
!
subroutine select_bb
  use gildas_def
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  !
  subb_select = .false.
  pol_select  = .false.
  bb_select   = .true.
  if_select   = .false.
  return 
end subroutine select_bb
!
subroutine select_if
  use gildas_def
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  !
  subb_select = .false.
  pol_select  = .false.
  bb_select   = .false.
  if_select   = .true.
  return 
end subroutine select_if
