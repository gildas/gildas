subroutine clic_cursor(line,error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Returns x_data and y_data at cursor position
  ! Il importe d'appeler gt_curs avec une chaine de plus de 1 caractere car
  ! elle deborde ...
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx, ipy, ipw, ipu, ipr
  integer(kind=address_length) :: ips
  character(len=1) :: argum,code
  character(len=4) :: code4, chain
  character(len=3) :: code3
  equivalence (code4, code)
  integer :: i, ib, l, ko, nch
  real*4 :: gx1(mbox), gx2(mbox), gy1(mbox), gy2(mbox)
  real*4 :: x4, y4
  real*8 :: xu, yu
  integer :: mvoc1
  parameter (mvoc1=3)
  character(len=12) :: voc1(mvoc1)
  data voc1/'X','Y','DISTANCE'/
  !------------------------------------------------------------------------
  ! Code:
  argum = 'X'
  if (change_display) then
    call message(6,3,   &
      'CLIC_CURSOR','Display configuration changed')
    error = .true.
    return
  endif
  call clic_kw(line,0,1,argum,nch,voc1,mvoc1,.false.,error,.true.)
  if (error) return
  if (argum.eq.'X') then
    call message(4,1,'CLIC_CURSOR','Use closest point in X')
  elseif (argum.eq.'Y') then
    call message(4,1,'CLIC_CURSOR','Use closest point in Y')
  elseif (argum.eq.'DISTANCE') then
    call message(4,1,'CLIC_CURSOR','Use closest point in distance')
  endif
  !
  ! Get box corners
  do i=1, n_boxes
    call gr_execl('CHANGE DIRECTORY')
    l = lenc(c_setbox(i))
    write (chain,'(I4.4)') i
    call gr_execl('CHANGE DIRECTORY BOX'//chain)
    call gr_exec  (c_setbox(i)(1:l))
    call sic_get_real ('BOX_XMIN',gx1(i),error)
    call sic_get_real ('BOX_XMAX',gx2(i),error)
    call sic_get_real ('BOX_YMIN',gy1(i),error)
    call sic_get_real ('BOX_YMAX',gy2(i),error)
  enddo
  call gr_execl('CHANGE DIRECTORY')
  do while (.true.)
    error = .false.
    call gr_curs (xu, yu, x4, y4, code4)
    if (code.eq.'E' .or. code.eq.'e') then
      call resetvar(error)
      return
    endif
    do i=1, n_boxes
      if((x4-gx1(i))*(x4-gx2(i)) .le. 0) then
        if((y4-gy1(i))*(y4-gy2(i)) .le. 0) then
          ib = i
          goto 10
        endif
      endif
    enddo
    write(6,*) ' Cursor (',x4,',',y4,') is not in any box'
    goto 20
    !
    ! Box found
10  ko = (ib-1)*m_data
    ipx = gag_pointer(data_x,memory)+ko
    ipy = gag_pointer(data_y,memory)+ko
    ipw = gag_pointer(data_w,memory)+ko
    ipu = gag_pointer(data_u,memory)+ko
    ipr = gag_pointer(data_r,memory)+ko
    ips = gag_pointer(data_s,memory)+ko
    call sub_cursor(argum,ib,n_data(ib),   &
      memory(ipx),memory(ipy),memory(ipw),   &
      memory(ipu),memory(ipr),memory(ips),   &
      code,x4,y4,error)
    !
20 enddo
  return
end subroutine clic_cursor
!
subroutine sub_cursor(argum,ib,nd,xd,yd,wd,ud,rd,sd,   &
    code,x4,y4,error)
  use gildas_def
  use gkernel_interfaces
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! Returns parameters of the observation displayed closest to the
  ! cursor position.
  !
  !     ARGUM C*(*)    Proximity mode ( X, Y, Distance )
  !     IB    INTEGER  Box number
  !     ND    INTEGER  Number of points in box
  !     XD    REAL     X coordinates
  !     YD    REAL     Y values
  !     WD    REAL     Weights
  !     UD    REAL     Observation number
  !     RD    REAL     Record number
  !     sD    REAL     work area
  !     CODE  C*(*)    Code of operation
  !     X4,Y4 REAL     cursor coordinates
  !---------------------------------------------------------------------
  character(len=1) :: argum         !
  integer :: ib                     !
  integer :: nd                     !
  real :: xd(nd)                    !
  real :: yd(nd)                    !
  real :: wd(nd)                    !
  integer :: ud(nd)                 !
  integer :: rd(nd)                 !
  logical :: sd(nd)                 !
  character(len=1) :: code          !
  real :: x4                        !
  real :: y4                        !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: reset
  integer :: ii, l, l1, l2, l3, j, jd, next, is
  integer :: ix, iy, ic, nqual, jsave, lchain
  integer :: flag, ibase, old_pen, i1, i2
  real*8 :: xu, yu, di, dd
  real*4 :: xsave, ysave, xt, yt, x1, x2, y1, y2
  character(len=132) :: ch
  character(len=80) :: chain
  type (title_v2_t),save :: title_save
  !
  save jsave,xsave,ysave
  ! Data
  data jsave/0/
  !------------------------------------------------------------------------
  ! Code:
  l = lenc(c_setbox(ib))
  call gr_exec  (c_setbox(ib)(1:l))
  l = lenc(c_limits(ib))
  call gr_exec1 (c_limits(ib)(1:l))
  call grelocate (x4, y4)
  call gr_where (xu, yu, x4, y4)
  ix = i_x(k_x(ib))
  iy = i_y(k_y(ib))
  di = 1e35
  jd = -1
  do j=1, nd
    if (wd(j).gt.2e-15) then
      call gr4_user_phys(xd(j),yd(j),xt,yt,1)
      if (argum.eq.'D') then
        dd = sqrt((xt-x4)**2+(yt-y4)**2)
      !            ELSEIF (ARGUM.EQ.'X') THEN
      !               DD = ABS(XT-X4)
      elseif (argum.eq.'Y') then
        dd = abs(yt-y4)
      else
        dd = abs(xt-x4)
      endif
      if (dd .lt. di) then
        di = dd
        jd = j
      endif
    endif
  enddo
  if (jd.le.0) then
    write(*,*) 'Bad point'
    goto 999
  endif
  if (code.eq.'K' .or. code.eq.'k') then
    wd(jd) = 1d-35           ! zero weight
    write(6,*) 'Killed '//clab(ix)//' = ', xd(jd),   &
      '	'//clab(iy)//' = ',yd(jd)
    yd(jd) = blank4
  elseif (code.eq.'N' .or. code.eq.'n') then
    jsave = jd
    xsave = x4
    ysave = y4
  elseif (code.eq.'I' .or. code.eq.'i') then
    if (.not.sorted) then
      write(6,*) 'F-CURSOR, Data unsorted, please PLOT again'
    elseif (jsave.le.0) then
      write(6,*) 'F-CURSOR, Use N first'
    else
      old_pen = gr_spen(1)
      call gr_segm('FLAGGING',error)
      call grelocate(xsave,ysave)
      call gdraw(xsave,y4)
      call gdraw(x4,y4)
      call gdraw(x4,ysave)
      call gdraw(xsave,ysave)
      call gr_segm_close(error)
      ii = gr_spen(old_pen)
      ch = 'Will ignore points in the red rectangle ...'
      call message(6,1,'CURSOR',ch(1:lenc(ch)))
      i1 = min(jsave,jd)
      i2 = max(jsave,jd)
      x1 = min(xsave,x4)
      x2 = max(xsave,x4)
      y1 = min(ysave,y4)
      y2 = max(ysave,y4)
      do ii=i1, i2
        call gr4_user_phys(xd(ii),yd(ii),xt,yt,1)
        if (xt.ge.x1 .and. xt.le.x2 .and.   &
          yt.ge.y1 .and. yt.le.y2) then
          do ic = 1, cxnext-1
            if (cx_num(ic).eq.ud(ii)) then
              ix_qual(cx_ind(ic)) = 9
              goto 5
            endif
          enddo
5         continue
        endif
      enddo
      old_pen = gr_spen(1)
      call grelocate((x4+xsave)/2,(y4+ysave)/2)
      call gr_labe_cent(2)
      call gr_labe('Ignored')
      ii = gr_spen(old_pen)
      jsave = 0
    endif
  elseif (code.eq.'T' .or. code.eq.'t') then
    call check_output_file(error)
    if (error) goto 999
    call check_equal_file(error)
    if (error) goto 999
    if (.not.sorted) then
      write(6,*) 'F-CURSOR, Data unsorted, please PLOT again'
    elseif (jsave.le.0) then
      write(6,*) 'F-CURSOR, Use N first'
    else
      old_pen = gr_spen(1)
      call gr_segm('FLAGGING',error)
      call grelocate(xsave,ysave)
      call gdraw(xsave,y4)
      call gdraw(x4,y4)
      call gdraw(x4,ysave)
      call gdraw(xsave,ysave)
      call gr_segm_close(error)
      ii = gr_spen(old_pen)
      ch = 'Will tag scans with points in the red rectangle ...'
      call message(6,1,'CURSOR',ch(1:lenc(ch)))
      next = ud(jd)
9     write(6,'(a)') '$Quality (0-9) ?'
      read (5,*,err=9,end=9) nqual
      nqual = min(9,max(0,nqual))
      i1 = min(jsave,jd)
      i2 = max(jsave,jd)
      x1 = min(xsave,x4)
      x2 = max(xsave,x4)
      y1 = min(ysave,y4)
      y2 = max(ysave,y4)
      title_save = title  
      do ii=i1, i2
        call gr4_user_phys(xd(ii),yd(ii),xt,yt,1)
        if (xt.ge.x1 .and. xt.le.x2 .and.   &
          yt.ge.y1 .and. yt.le.y2) then
          do ic = 1, cxnext-1
            if (cx_num(ic).eq.ud(ii)) then
              call rox(cx_ind(ic),error)
              title%qual = nqual 
              call mox(cx_ind(ic),error)
              goto 15
            endif
          enddo
15        continue
        endif
      enddo
      title = title_save
      old_pen = gr_spen(1)
      call grelocate((x4+xsave)/2,(y4+ysave)/2)
      call gr_labe_cent(2)
      call gr_labe(quality(nqual))
      ii = gr_spen(old_pen)
      jsave = 0
    endif
  elseif (code.eq.'F' .or. code.eq.'f'   &
    .or. code.eq.'R' .or. code.eq.'r') then
    reset = code.eq.'R' .or. code.eq.'r'
    call check_output_file(error)
    if (error) goto 999
    call check_equal_file(error)
    if (error) goto 999
    if (.not.sorted) then
      write(6,*) 'F-CURSOR, Data unsorted, please PLOT again'
    elseif (jsave.le.0) then
      write(6,*) 'F-CURSOR, Use N first'
    else
      old_pen = gr_spen(1)
      call gr_segm('FLAGGING',error)
      call grelocate(xsave,ysave)
      call gdraw(xsave,y4)
      call gdraw(x4,y4)
      call gdraw(x4,ysave)
      call gdraw(xsave,ysave)
      call gr_segm_close(error)
      ii = gr_spen(old_pen)
      ch = 'Will flag points in the red rectangle ...'
      !            WRITE(CH,1000) 'Will flag from obs. ',
      !     $      UD(JSAVE),' rec. ',RD(JSAVE),
      !     $      ' to obs. ',UD(JD),' rec. ',RD(JD),' (rectangle)'
      call message(6,1,'CURSOR',ch(1:lenc(ch)))
      write(6,*) '$Flags (name1 name2 ...), default DATA ?'
      read (5,'(a)') chain
      lchain = lenc(chain)
      if (lchain.eq.0) then
        chain = 'DATA'
        lchain = 4
      endif
      ibase = i_base(k_base(ib))
      call sic_blanc(chain,lchain)
      if (ibase.gt.0) then
        call get_base_mask(chain(1:lchain),flag,error)
      elseif (ibase.lt.0) then
        call get_ant_mask(chain(1:lchain),flag,error)
      endif
      if (error) goto 999
      i1 = min(jsave,jd)
      i2 = max(jsave,jd)
      x1 = min(xsave,x4)
      x2 = max(xsave,x4)
      y1 = min(ysave,y4)
      y2 = max(ysave,y4)
      do ii=i1, i2
        call gr4_user_phys(xd(ii),yd(ii),xt,yt,1)
        sd(ii) = xt.ge.x1 .and. xt.le.x2 .and.   &
          yt.ge.y1 .and. yt.le.y2
      enddo
      call do_flags(min(jsave,jd),max(jsave,jd),   &
        ibase, flag, reset,ud,rd,sd,nd,error)
      if (error) goto 999
      old_pen = gr_spen(1)
      call grelocate((x4+xsave)/2,(y4+ysave)/2)
      call gr_labe_cent(2)
      call gr_labe('Flagged '//chain(1:lchain))
      ii = gr_spen(old_pen)
      jsave = 0
    endif
  elseif (code.eq.'H' .or. code.eq.'h') then
    title_save = title  
    do ic = 1, cxnext-1
      if (cx_num(ic).eq.ud(jd)) then
        call rix(cx_ind(ic),error)
        call out0 ('Terminal', 0,0,error)
        call titout ('Normal',error)
        call out1(error)
        goto 25
      endif
    enddo
25  title = title_save 
  else
    l1 = lenc(header_1(ib))
    l2 = lenc(header_2(ib))
    l3 = lenc(header_3(ib))
    write(6,*) '---------- Cursor ----------'
    write(6,*) header_1(ib)(1:l1)//' 	'//header_2(ib)(1:l2)   &
      //' 	'//header_3(ib)(1:l3)
    l1 = lenc(x_label(ib))
    l2 = lenc(y_label(ib))
    write (6,1002) 'X: '//x_label(ib)(1:l1)//' = ',xu,   &
      '	Y: '//y_label(ib)(1:l2)//' = ',yu
    ic = 1
    do while (ic.le.i%desc%xnext .and. ix_num(ic).ne.ud(jd) )
      ic = ic+1
    enddo
    is = ix_scan(ic)
    write(6,1001) 'Scan: ',is,' Obs: ',ud(jd),' Rec: ',rd(jd),   &
      ' Weight = ',wd(jd)
  endif
  return
999 error = .true.
  return
1000 format(a,i5,a,i3,a,i5,a,i3,a)
1001 format(1x,a,i5,a,i5,a,i3,a,1pg15.9)
1002 format(1x,2(a,1pg15.9))
end subroutine sub_cursor
!
subroutine do_flags(imin, imax, ibase, fl, reset, ud, rd, sd, nd,   &
    error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Loop to set or reset flags in current index
  !---------------------------------------------------------------------
  integer :: imin                   !
  integer :: imax                   !
  integer :: ibase                  !
  integer :: fl                     !
  logical :: reset                  !
  integer :: nd                     !
  integer :: ud(nd)                 !
  integer :: rd(nd)                 !
  logical :: sd(nd)                 !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: data_in, ip_data, ipk
  integer(kind=address_length) :: kin
  integer(kind=data_length)    :: ndata, h_offset
  integer :: ip, u0
  character(len=7) :: done
  character(len=80) :: chain
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Loop on data points
  u0 = 0
  do ip= imin, imax
    if (sd(ip) .and. ud(ip).ne.u0) then
      if (u0.gt.0) then
        call ipb_write(write_mode,.true.,error)
        if (error) goto 999
        ndata = (r_ndump+1)*r_ldump+r_ldatl
        if (r_ndatl.gt.1) then
          ndata = ndata + r_ldump+r_ldatl
        endif
        ip_data = gag_pointer(data_in,memory)
        call wdata (ndata,memory(ip_data),.false.,error)
        if (error) goto 999
        call ipb_close(error)
        if (error) goto 999
        write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
          ' '//done(write_mode)//' (',r_ndump,' records)'
        call message(4,1,'DO_FLAGS',chain(1:50))
      endif
      call get_sub (ud(ip),0,error)
      if (error) goto 999
      call get_data (ndata,data_in,error)
      if (error) goto 999
      u0 = ud(ip)
    endif
    if (sd(ip) .and. rd(ip).ge.1 .and. rd(ip).le.r_ndump) then
      kin = gag_pointer(data_in, memory)
      ipk = kin + h_offset(rd(ip))
      call decode_header (memory(ipk))
      if (reset) then
        if (ibase.gt.0) then
          dh_bflag(ibase) = iand(dh_bflag(ibase),not(fl))
        else
          dh_aflag(-ibase) = iand(dh_aflag(-ibase),not(fl))
        endif
      else
        if (ibase.gt.0) then
          dh_bflag(ibase) = ior(dh_bflag(ibase),fl)
        else
          dh_aflag(-ibase) = ior(dh_aflag(-ibase),fl)
        endif
      endif
      call encode_header (memory(ipk))
      ! do same with the average record
      ipk = kin + h_offset(r_ndump+1)
      call decode_header (memory(ipk))
      if (reset) then
        if (ibase.gt.0) then
          dh_bflag(ibase) = iand(dh_bflag(ibase),not(fl))
          r_dmbflag(ibase) = dh_bflag(ibase)
        else
          dh_aflag(-ibase) = iand(dh_aflag(-ibase),not(fl))
          r_dmaflag(-ibase) = dh_aflag(-ibase)
        endif
      else
        if (ibase.gt.0) then
          dh_bflag(ibase) = ior(dh_bflag(ibase),fl)
          r_dmbflag(ibase) = dh_bflag(ibase)
        else
          dh_aflag(-ibase) = ior(dh_aflag(-ibase),fl)
          r_dmaflag(-ibase) = dh_aflag(-ibase)
        endif
      endif
      r_presec(modify_sec) = .true.
      call encode_header (memory(ipk))
      ! do same with the corrected record if present
      if (r_ndatl.gt.1) then
        ipk = kin + h_offset(r_ndump+2)
        call decode_header (memory(ipk))
        if (reset) then
          if (ibase.gt.0) then
            dh_bflag(ibase) = iand(dh_bflag(ibase),not(fl))
          else
            dh_aflag(-ibase) = iand(dh_aflag(-ibase),not(fl))
          endif
        else
          if (ibase.gt.0) then
            dh_bflag(ibase) = ior(dh_bflag(ibase),fl)
          else
            dh_aflag(-ibase) = ior(dh_aflag(-ibase),fl)
          endif
        endif
        call encode_header (memory(ipk))
      endif
    endif
  enddo
  call ipb_write(write_mode,.true.,error)
  if (error) goto 999
  ip_data = gag_pointer(data_in,memory)
  call wdata (ndata,memory(ip_data),.false.,error)
  if (error) goto 999
  call ipb_close(error)
  if (error) goto 999
  write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
    ' '//done(write_mode)//' (',r_ndump,' records)'
  call message(4,1,'DO_FLAGS',chain(1:50))
  return
  !
999 error = .true.
  return
end subroutine do_flags
