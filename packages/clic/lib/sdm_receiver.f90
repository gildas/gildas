subroutine write_sdm_Receiver(holodata, error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Receiver SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Receiver
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  integer :: ia, ic, isb, ilc, ier
  character sdmTable*12
  parameter (sdmTable='Receiver')
  type (ReceiverRow) :: row
  type (ReceiverRow) :: rowtp
  type (ReceiverKey) :: key
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !     Normal case
  !
  if (.not. holodata) then
    row%numLo = 4
    call allocReceiverRow(row, error)
    if (error) return
    row%name = 'BureMarkN'
   ! very tentative ... calculate from frequency.
    select case(r_nrec)
    case(1)
      row%frequencyBand  = ReceiverBand_BURE_01
    case(2)
      row%frequencyBand  = ReceiverBand_BURE_02
    case(3)
      row%frequencyBand  = ReceiverBand_BURE_03
    case(4)
      row%frequencyBand  = ReceiverBand_BURE_04
    end select
    row%freqLo(1) = r_flo1*1d6
    row%sidebandLo(3) = NetSideBand_LSB
    row%freqLo(4) = 1280.260*1d6
    !     loop on side bands
    do isb = 1, r_lnsb
      row%receiverSideband = ReceiverSideBand_SSB
      if (isb.eq.1) then
        row%sidebandLo(1) = NetSideBand_USB ! 1  !
      elseif (isb.eq.2) then
        row%sidebandLo(1) = NetSideBand_LSB! -1 !
      endif
      key%timeInterval = ArrayTimeInterval(time_Interval(1)   &
        ,time_Interval(2))
      do ia = 1, r_nant
        !!               row%tDewar = r_tdewar(1,ia)
        !! row%tDewar = 0
        !
        !     loop on correlator units
        do ic = 1, r_lband
          row%numLo = 4
          if (new_receivers) then
            row%freqLo(2) = r_flo2(ic)*1d6
          else
            row%freqLo(2) = r_flo1_ref*1d6
          endif
          !
          row%sidebandLo(2) = NetSideBand_LSB
          row%freqLo(3) = r_lfcen(ic)*1d6
          if (r_band4(ic).eq.0) then
            row%sidebandLo(4) = NetSideBand_USB
          elseif (r_band4(ic).eq.1) then
            row%sidebandLo(4) = NetSideBand_LSB
          elseif (r_band4(ic).eq.2) then
            row%sidebandLo(4) = NetSideBand_DSB
          endif
          !     line/continuum
          do ilc = 1, 2
            key%spectralWindowId = spectralWindow_Id(isb, ic   &
              ,ilc)
            call addReceiverRow(key, row, error)
            if (error) return
            receiver_Id(ia, isb, ic, ilc) = key%ReceiverId
          enddo                ! ilc
        enddo                  ! ic
        !
        !     for total power
        rowtp%numLo = 2
        call allocReceiverRow(rowtp, rowtp%numLo,  error)
        rowtp%freqLo(1) = row%freqLo(1)
        rowtp%freqLo(2) = r_flo2(1)*1d6
        rowtp%sidebandLo(1) = row%sidebandLo(1)
        rowtp%sidebandLo(2) = row%sidebandLo(2)
        rowtp%name = row%name
        rowtp%frequencyBand = row%frequencyBand


        key%spectralWindowId = TotPowSpectralWindow_Id(isb)
        call addReceiverRow(key, rowtp, error)
        if (error) return
        TotPowReceiver_Id(ia, isb) = key%ReceiverId
      enddo
    enddo
  !
  !     Holography case
  !
  else
    row%numLo = 1
    call allocReceiverRow(row, row%numLo,  error)
    if (error) return
    row%name = 'Holography Receiver'
    row%frequencyBand = ReceiverBand_ALMA_HOLOGRAPHY_RECEIVER
    row%freqLo(1) = r_flo1*1d6
    row%sidebandLo(1) = NetSideBand_USB
    !
    do ia = 1, r_nant
      key%spectralWindowId = totPowSpectralWindow_Id(1)
      key%timeInterval = ArrayTimeInterval(time_start(1),   &
        time_start(2))
      call addReceiverRow(key, row, error)
      if (error) return
      TotPowreceiver_Id = key%ReceiverId
    enddo
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
!
end subroutine write_sdm_Receiver
!---------------------------------------------------------------------
subroutine get_sdm_Receiver(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Read the Receiver SDM table. NOT USED
  !
  !     Key:  (receiverId, spectralWindowId, timeInterval)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Receiver
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (ReceiverRow) :: row
  type (ReceiverKey) :: key
  integer :: ia, ic, isb, ilc, i, ier
  integer, parameter :: mLo = 4
  !      integer*8 stabilityDuration
  character sdmTable*12
  parameter (sdmTable='Receiver')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  row%numLO = 4
  call allocReceiverRow(row, error)
  if (error) return
  !
  !     loop on spectral windows
  do i=1, ddId_Size
    do ia = 1, r_nant
      key%receiverId = receiverId_List(ia, i)
      key%spectralWindowId = swId_List(i)
      key%timeInterval = ArrayTimeInterval(time_Interval(1)   &
        ,time_Interval(2))
      call getReceiverRow (key, row, error)
      if (error) then
        print *,  'key%receiverId, key%spectralWindowId, ',   &
          'key%timeInterval:'
        print *,  key%receiverId, key%spectralWindowId,   &
          key%timeInterval
      endif
      if (error) return
      !!r_tdewar(1,ia) = row%tDewar
      ! read in from spectral window...
      ic = bb_List(i)
      if (row%sidebandLo(4).eq.NetSideBand_USB) then
        r_band4(ic) = 0
      elseif (row%sidebandLo(4).eq.NetSideBand_LSB) then
        r_band4(ic) = 1
      elseif (row%sidebandLo(4).eq.NetSideBand_DSB) then
        r_band4(ic) = 2
      endif
      if (row%sidebandLo(1) .eq. NetSideBand_USB) then
         sb_list(i) = 1
         r_isb = 1
      else
         sb_list(i) = 2
         r_isb = -1
      endif
      !     assume the same for all antennas!
      r_flo1 = row%freqLo(1)/1d6
      r_flo2(ic) = row%freqLo(2)/1d6
      r_flo3(ic) = row%freqLo(3)/1d6
      r_flo4(ic) = row%freqLo(4)/1d6
      r_lfcen(ic) = row%freqLo(3)/1d6
      ! note this works for the ATF  - - may be changed at the OSF
      ! print *, 'row%sidebandLo(2) , NetSideBand_USB ', row%sidebandLo(2) , NetSideBand_USB
      if (row%sidebandLo(2) .eq. NetSideBand_USB) then
         r_fif1 = r_flo2(ic) + (r_flo3(ic) - 3000.) 
      else
         r_fif1 = r_flo2(ic) - (r_flo3(ic) - 3000.) 
      endif            
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine get_sdm_Receiver
