! Title format
      LOGICAL T_INTERFERO, T_POSITION, T_QUALITY, T_SPECTRAL,           &
     &T_CALIBRATION, T_STATUS, T_POINTING, T_ATMOSPHERE, T_LIST
      INTEGER TITLE_LINES
      COMMON /LTITLE/ TITLE_LINES, T_INTERFERO, T_POSITION, T_QUALITY,  &
     &T_SPECTRAL, T_CALIBRATION, T_STATUS, T_POINTING, T_ATMOSPHERE,    &
     &T_LIST
      SAVE /LTITLE/
