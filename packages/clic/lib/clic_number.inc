! Antenna to Baseline and Triangle conversion (for up to 6 antennas)
! basant(i,j) is the number of the baseline from Ant i to Ant j
!      INTEGER BASANT(1:5,2:6)
      INTEGER BASANT(1:mnant-1,2:mnant)
! antbas(1,i) is the first ant. of bas. i
! antbas(2,i) is the second ant. of bas. i
!      INTEGER ANTBAS(2,15)
      INTEGER ANTBAS(2,mnbas)
!
! Triangles
!      INTEGER TRIANT(1:4,2:5,3:6)
      INTEGER TRIANT(1:mnant-2,2:mnant-1,3:mnant)
! anttri(1,i) is the first ant. of tri. i
! anttri(2,i) is the second ant. of tri. i
! anttri(3,i) is the third ant. of tri. i
      INTEGER ANTTRI(3,mntri)
!      INTEGER ANTTRI(3,20)
! bastri(1,i) is side 12 of triangle i
! bastri(2,i) is side 13 of triangle i
! bastri(3,i) is side 23 of triangle i
      INTEGER BASTRI(3,mntri)
!
! Quadrilateres
      INTEGER QUADANT(1:mnant-3,2:mnant-2,3:mnant-1,4:mnant)
! antquad(1,i) is the first ant. of quad. i
! antquad(2,i) is the second ant. of quad. i
! antquad(3,i) is the third ant. of quad. i
! antquad(4,i) is the 4th ant. of quad. i
      INTEGER ANTQUAD(4,mnquad)
      INTEGER BASQUAD(6,mnquad)
!
      COMMON /NUMBERS/ BASANT, ANTBAS, TRIANT, ANTTRI, BASTRI,          &
     &     QUADANT, ANTQUAD, BASQUAD






