subroutine write_sdm_focusModel(error)
  !---------------------------------------------------------------------
  ! Write the Focus Model SDM table.
  ! (very much TBD at present)
  !     int sdm_addFocusModelRow ()
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  !type(FocusModelRow) :: fmRow
  !type(FocusModelKey) :: fmKey
  integer ia
  integer sdm_addfocusModelrow, ireturn
  character sdmTable*12
  parameter (sdmTable='FocusModel')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !!fmRow%numCoeff = 1
  !call allocFocusModelRow(fmrow, error)
  if (error) return
  !
  do ia = 1, r_nant
    !ireturn = sdm_addfocusModelrow(fmrow, error)
    if (ireturn.lt.0) then
      call sdmMessageI(8,3,sdmTable   &
        ,'Error in sdm_addFocusModelRow',ireturn)
      goto 99
    endif
    FocusModel_Id(ia) = ireturn
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_focusModel
!---------------------------------------------------------------------
