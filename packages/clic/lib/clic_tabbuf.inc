!
! Data Structures : Header table_
      INTEGER TABLE_XNUM                 ! number in index (0:no obs, -1:not fro
      INTEGER TABLE_NUM                  ! Scan number
      INTEGER TABLE_SCAN                 ! Original scan number
      INTEGER TABLE_PROC                 ! observing procedure
      CHARACTER*12 TABLE_SOURC           ! Source name
      REAL*4  TABLE_EPOCH                ! Epoch of observation
      REAL*8  TABLE_RA                   ! Alpha of pointing center
      REAL*8  TABLE_DEC                  ! Delta  ----
      REAL*8  TABLE_A0                   ! Alpha of phase center
      REAL*8  TABLE_D0                   ! Delta  ----
      INTEGER TABLE_PROJ                 ! Projection type
      CHARACTER*12 TABLE_SLINE           ! Line name
      REAL*8  TABLE_SRESTF               ! Rest frequency
      INTEGER  TABLE_SNCHAN              ! Number of channels
      REAL*4  TABLE_SRCHAN               ! Reference channels
      REAL*4  TABLE_SFRES                ! Frequency resolution
      REAL*4  TABLE_SFOFF                ! Frequency offset
      REAL*4  TABLE_SVRES                ! Velocity resolution
      REAL*4  TABLE_SVOFF                ! Velocity at reference channel
      REAL*4  TABLE_SBAD                 ! Blanking value
      REAL*8  TABLE_SIMAGE               ! Image frequency
      INTEGER  TABLE_SVTYPE              ! Type of velocity
      REAL*8  TABLE_SFOBS                ! Observing frequency (local frame)
      INTEGER TABLE_NANT                 ! Number of Antennas (used, e.g. virtua
      INTEGER TABLE_NBAS                 ! Number of baselines ('')
      INTEGER TABLE_ITYPE                ! Type of interf. obs.
      INTEGER TABLE_NDUMP                ! Number of records
      REAL*8  TABLE_RESTF                ! Rest Frequency (MHz)
      INTEGER TABLE_ALIGN1, TABLE_ALIGN2
!
      COMMON /TABLE_COM/                                                &
     &TABLE_XNUM,TABLE_NUM, TABLE_SCAN,TABLE_PROC,                      &
     &TABLE_SOURC,TABLE_EPOCH,                                          &
     &TABLE_RA,TABLE_DEC,TABLE_A0,TABLE_D0,                             &
     &TABLE_PROJ,TABLE_SLINE,                                           &
     &TABLE_SRESTF,TABLE_SNCHAN,                                        &
     &TABLE_SRCHAN,TABLE_SFRES,TABLE_SFOFF,TABLE_SVRES,                 &
     &TABLE_SVOFF,TABLE_SBAD,TABLE_ALIGN1,TABLE_SIMAGE,TABLE_SVTYPE,    &
     &TABLE_ALIGN2,TABLE_SFOBS,TABLE_NANT,TABLE_NBAS,TABLE_ITYPE,       &
     &TABLE_NDUMP,TABLE_RESTF
      SAVE /TABLE_COM/
!-------------------------------------------------------------------------
!
! Data Structures : Header out_
      INTEGER OUT_XNUM                   ! number in index (0:no obs, -1:not fro
      INTEGER OUT_NUM                    !
      INTEGER OUT_SCAN                   !
      INTEGER OUT_PROC                   ! observing procedure
      CHARACTER*12 OUT_SOURC             ! Source name
      REAL*4  OUT_EPOCH                  ! Epoch of observation
      REAL*8  OUT_RA                     ! Alpha of pointing center
      REAL*8  OUT_DEC                    ! Delta  ----
      REAL*8  OUT_A0                     ! Alpha of phase center
      REAL*8  OUT_D0                     ! Delta  ----
      INTEGER OUT_PROJ                   ! Projection type
      CHARACTER*12 OUT_SLINE             ! Line name
      REAL*8  OUT_SRESTF                 ! Rest frequency
      INTEGER  OUT_SNCHAN                ! Number of channels
      REAL*4  OUT_SRCHAN                 ! Reference channels
      REAL*4  OUT_SFRES                  ! Frequency resolution
      REAL*4  OUT_SFOFF                  ! Frequency offset
      REAL*4  OUT_SVRES                  ! Velocity resolution
      REAL*4  OUT_SVOFF                  ! Velocity at reference channel
      REAL*4  OUT_SBAD                   ! Blanking value
      REAL*8  OUT_SIMAGE                 ! Image frequency
      INTEGER  OUT_SVTYPE                ! Type of velocity
      REAL*8  OUT_SFOBS                  ! Observing frequency (local frame)
      INTEGER OUT_NANT                   ! Number of Antennas (used, e.g. virtua
      INTEGER OUT_NBAS                   ! Number of baselines ('')
      INTEGER OUT_ITYPE                  ! Type of interf. obs.
      INTEGER OUT_NDUMP                  ! Number of records
      REAL*8  OUT_RESTF                  ! Rest Frequency (MHz)
      INTEGER OUT_ALIGN1, OUT_ALIGN2
!
      COMMON /OUT_COM/                                                  &
     &OUT_XNUM,OUT_NUM,OUT_SCAN,                                        &
     &OUT_PROC,OUT_SOURC,OUT_EPOCH,                                     &
     &OUT_RA,OUT_DEC,OUT_A0,OUT_D0,                                     &
     &OUT_PROJ,                                                         &
     &OUT_SLINE,OUT_SRESTF,OUT_SNCHAN,                                  &
     &OUT_SRCHAN,OUT_SFRES,OUT_SFOFF,OUT_SVRES,                         &
     &OUT_SVOFF,OUT_SBAD,OUT_ALIGN1,OUT_SIMAGE,OUT_SVTYPE,              &
     &OUT_ALIGN2,OUT_SFOBS,OUT_NANT,OUT_NBAS,OUT_ITYPE,                 &
     &OUT_NDUMP,OUT_RESTF
      SAVE /OUT_COM/
!-------------------------------------------------------------------------
