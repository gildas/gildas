subroutine write_sdm_AlmaCorrelatorMode(error)
  !---------------------------------------------------------------------
  ! Write the AlmaCorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_almaCorrelatorMode
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (AlmaCorrelatorModeKey) :: acmKey
  type (AlmaCorrelatorModeRow) :: acmRow
  character sdmTable*20
  parameter (sdmTable='AlmaCorrelatorMode')
  integer ier, i
  !---------------------------------------------------------------------
  ier = 0
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much *TBD* at this stage. Think of better filling this in...
  acmRow%numBaseband = r_lband
  call  allocAlmaCorrelatorModeRow(acmRow, acmRow%numBaseband, error   &
    )
  if (error) return
  do i=1, acmRow%numBaseband
    acmRow%basebandIndex(i) = i
    acmRow%basebandConfig(i) = 0
    acmRow%axesOrderArray(i) = 0
  enddo
  acmRow%accumMode = 0
  acmRow%binMode = 0
  acmRow%quantization = .true.
  acmRow%windowFunction = 'Parabolic'
  call addAlmaCorrelatorModeRow(acmKey, acmRow, error)
  if (error) return
  !
  CorrelatorMode_Id = acmKey%almaCorrelatorModeId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_AlmaCorrelatorMode
!---------------------------------------------------------------------
subroutine get_sdm_AlmaCorrelatorMode(error)
  !---------------------------------------------------------------------
  ! Get the AlmaCorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_almaCorrelatorMode
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (AlmaCorrelatorModeKey) :: acmKey
  type (AlmaCorrelatorModeRow) :: acmRow
  integer ier
  integer, parameter :: maxBaseband = 8
  character  :: sdmTable*20
  parameter (sdmTable='AlmaCorrelatorMode')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much *TBD* at this stage. Think of better filling this in...
  !$$$      print *, 'CorrelatorMode_Id ', CorrelatorMode_Id
  call  allocAlmaCorrelatorModeRow(acmRow, maxBaseband, error   &
    )
  if (error) return
  acmKey%almaCorrelatorModeId  = CorrelatorMode_Id
  call getAlmaCorrelatorModeRow(acmKey, acmRow, error)
  if (error) return
  if (acmRow%numBaseband.gt.mrlband) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Wrong numBaseband',acmRow%numBaseband)
    goto 99
  endif
  !
  ! ### Note: with current PDB-NG  format, if there are two polarization products (Polarization.numCorr = 2)
  ! we have to double  double the subbands. SO we should also read the POlarization table.
  ! keep num_Baseband as a global item?
  r_lband = acmRow%numBaseband
  !
  !     baseband index ???
  !
  !     ignore accumMode, binMode, quantization, windowFunction,
  !     axesOrderArray for the time being
  !
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_AlmaCorrelatorMode
!---------------------------------------------------------------------
subroutine allocAlmaCorrelatorModeRow(row, numBaseband, error)
  !---------------------------------------------------------------------
  !$$$    integer, allocatable :: basebandIndex(:)
  !$$$    integer, allocatable :: basebandConfig(:)
  !$$$    integer, allocatable :: axesOrderArray(:)
  !---------------------------------------------------------------------
  use sdm_AlmaCorrelatorMode
  type(AlmaCorrelatorModeRow) :: row
  integer :: numBaseband, ier
  logical :: error
  character, parameter :: sdmTable*8 = 'calReduction'
  !
  ! basebandIndex
  if (allocated(row%basebandIndex)) then
    deallocate(row%basebandIndex, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate(row%basebandIndex(numBaseband), stat=ier)
  if (ier.ne.0) goto 99
  !
  ! basebandConfig
  if (allocated(row%basebandConfig)) then
    deallocate(row%basebandConfig, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate(row%basebandConfig(numBaseband), stat=ier)
  if (ier.ne.0) goto 99
  !
  ! axesOrderArray (dimension???)
  if (allocated(row%axesOrderArray)) then
    deallocate(row%axesOrderArray, stat=ier)
    if (ier.ne.0) goto 98
  endif
  allocate(row%axesOrderArray(numBaseband), stat=ier)
  if (ier.ne.0) goto 99
  !
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
  error = .true.
  return
  !
99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
  error = .true.
  return
end subroutine allocAlmaCorrelatorModeRow
