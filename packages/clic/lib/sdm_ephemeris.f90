subroutine write_sdm_ephemeris(error)
  !---------------------------------------------------------------------
  ! Write the Ephemeris SDM table.
  ! *TBD* To be implemented...
  !
  !     int sdm_addEphemerisRow ()
  !
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  integer sdm_addEphemerisRow, ireturn
  character sdmTable*12
  parameter (sdmTable='Ephemeris')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ireturn  = sdm_addEphemerisRow()
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,sdmTable ,'Error in sdm_addEphemerisRow'   &
      ,ireturn)
    goto 99
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_ephemeris
!---------------------------------------------------------------------
