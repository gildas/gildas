subroutine write_sdm_sbsummary(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Source Sbsummary  table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Sbsummary
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (SbSummaryKey) :: sbKey
  type (SbSummaryRow) :: sbRow
  real*8 :: releaseDays, lonlat(2), altitude
  integer :: numObservingLog, numSchedulerMode, ia, l
  character, parameter :: sdmTable*12 = 'SBSummary'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  sbRow%numObservingMode = 1
  sbRow%numScienceGoal = 1
  sbRow%numWeatherConstraint = 1

  call allocSBSummaryRow(sbRow, error)
  if (error) return
  ! inprinciple it's a different UID ...
  l = lenc(schedBlock_uid)
  sbRow%sbSummaryUID = '<EntityRef entityId="' //   &
    schedBlock_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="SchedBlock" '//   &
    'documentVersion="1"/>'//char(0)
  l = lenc(project_uid)
  sbRow%projectUID = '<EntityRef '// 'entityId="' //   &
    project_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="ObsProject" '//   &
    'documentVersion="1"/>'//char(0)
  l = lenc(obsUnitSet_uid)
  sbRow%obsUnitSetId= '<EntityRef '// 'entityId="' //   &
    obsUnitSet_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="ObsUnitSet" '//   &
    'documentVersion="1"/>'//char(0)
  sbRow%numberRepeats = 1
  sbRow%frequency = r_restf*1d6
  select case(r_nrec)
  case(1)
    sbrow%frequencyBand  = ReceiverBand_BURE_01
  case(2)
    sbrow%frequencyBand  = ReceiverBand_BURE_02
  case(3)
    sbrow%frequencyBand  = ReceiverBand_BURE_03
  case(4)
    sbrow%frequencyBand  = ReceiverBand_BURE_04
  end select
  write(sbRow%frequencyBand,'(a,i2.2)') 'BURE_', r_nrec
  sbRow%sbType = SBType_OBSERVER
  sbRow%sbDuration = 3600. ! hardcoded to 1 hour.
  sbRow%centerDirection = 0. ! Point Gamma
  sbRow%observingMode(1) = 'SingleFieldInterferometry'
  sbRow%scienceGoal(1) = 'Spot these LGM'
  sbRow%weatherConstraint = 'Clear Skies'
  !
  call addSBSummaryRow(sbKey, sbRow, error)
  if (error) return
  !
  SBSummary_Id = sbKey%sbSummaryId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_sbsummary
!---------------------------------------------------------------------
! Get routine TBW.
!---------------------------------------------------------------------
