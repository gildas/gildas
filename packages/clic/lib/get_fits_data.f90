!
subroutine get_fits_data(unit,ndata,data,error)
  use gildas_def
  use classic_api  
  integer :: unit                   !
  integer :: ndata                  !
  integer :: data(ndata)            !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  character(len=8) :: ctype
  character(len=80) :: comment
  character(len=16) :: extname
  character(len=12) :: cf
  real*8 :: vv, ff,  ifval, ifinc, fobs, x
  real :: df, dvv, ifref, vref, af
  integer :: status, itest, r_npol, iband, isb, nch, ksb
  integer :: nrows, irow, kr, ntch, ich, k0, k1, kd
  integer :: nsb, c_offset, l_offset
  integer :: integnum_col,startant_col, endant_col, antenna_col, data_col
  integer :: ia, ja, ib, nullval, naxis, maxis, nlo
  parameter (maxis=5)
  integer :: naxes(maxis) , isb2 , icol(2), idtable
  logical :: anyf
  character(len=1) :: cd,cfr
  character(len=13) :: pname
  integer :: jdata(2)
  real :: rdata(2)
  equivalence (jdata(1), rdata(1))
  parameter (pname='GET_FITS_DATA')
  !---------------------------------------------------------------------------
  status = 0
  ! Get  header keywords
  call ftgkys(unit,'EXTNAME',extname,comment,status)
  if (status .gt. 0) go to 99
  call ftgkyj(unit,'SCAN-NUM',itest,comment,status)
  if (status .gt. 0) go to 99
  if (itest.ne.r_scan) then
    call message(8,4,'GET_FITS_DATA','Wrong scan number')
    error = .true.
    return
  endif
  call ftgkyj(unit,'TABLEID',idtable,comment,status)
  if (status .gt. 0) go to 99
  call ftgkyj(unit,'NO_POL',r_npol,comment,status)
  if (status .gt. 0) go to 99
  if (r_npol.ne.1) then
    call message(8,4,'GET_FITS_DATA',   &
      'CLIC not yet ready for more than 1 polarization')
    !         ERROR = .TRUE.
    error = .false.
    r_npol = 1
  endif
  call ftgkyj(unit,'BASEBAND',iband,comment,status)
  call get_fioerror(pname,'BASEBAND',status,error,.false.)
  if (error) return
  !
  ! Get the columns numbers
  call ftgcno(unit,.false.,'INTEGNUM',integnum_col,status)
  call get_fioerror(pname,'INTEGNUM',status,error,.false.)
  if (error) return
  !      IF (STATUS.GT.0) GOTO 99
  if (r_lmode.eq.1) then
    call ftgcno(unit,.false.,'STARTANT',startant_col,status)
    call get_fioerror(pname,'STARTANT',status,error,.false.)
    !         IF (STATUS.GT.0) GOTO 99
    call ftgcno(unit,.false.,'ENDANT',endant_col,status)
    call get_fioerror(pname,'ENDANT',status,error,.false.)
    if (error) return
  !         IF (STATUS.GT.0) GOTO 99
  else
    call ftgcno(unit,.false.,'ANTENNA',antenna_col,status)
    call get_fioerror(pname,'ANTENNA',status,error,.false.)
    if (error) return
  !         IF (STATUS.GT.0) GOTO 99
  endif
  ! These two must be present.
  call ftgkyj(unit,'NO_SIDE',nsb,comment,status)
  call get_fioerror(pname,'NO_SIDE',status,error,.false.)
  call ftgkyj(unit,'SIDEBLO1',r_isb,comment,status)
  call get_fioerror(pname,'SIDEBLO1',status,error,.false.)
  if (error) return
  !
  ! Get frequency information
  nlo = 2                      ! by default
  call ftgkyj(unit,'NO_LO',nlo,comment,status)
  call get_fioerror(pname,'NO_LO',status,error,.true.)
  !
  do ia=1, r_nant
    isb = (3-r_isb)/2
    dh_atfac(1,isb,ia) = r_tsyss(1,ia)
    isb = (3+r_isb)/2
    dh_atfac(1,isb,ia) = r_tsysi(1,ia)
  enddo
  call ftgkyd(unit,'FREQLO1',r_flo1,comment,status)
  call get_fioerror(pname,'FREQLO1',status,error,.true.)
  r_flo1 = r_flo1/1d6
  ifval = 350d6
  call ftgkyd(unit,'INTERFRE',ifval,comment,status)
  call get_fioerror(pname,'INTERFRE',status,error,.true.)
  ifval = ifval/1d6
  if (nlo.ge.1) then
    r_restf = r_flo1+r_isb*ifval
  endif
  if (nlo.ge.2) then
    call ftgkyj(unit,'SIDEBLO2',isb2,comment,status)
    call get_fioerror(pname,'SIDEBLO2',status,error,.true.)
    call ftgkyd(unit,'FREQLO2',r_flo1_ref,comment,status)
    call get_fioerror(pname,'FREQLO2',status,error,.true.)
    r_flo1_ref = r_flo1_ref/1d6
    r_flo2 = r_flo1_ref
    r_restf = r_flo1+r_isb*(ifval*isb2+r_flo1_ref)
    r_fif1 = r_flo1_ref-350.d0 ! by definition in CLIC
  endif
  ! Use only 1 flux in CLIC for all basebands.
  status = 0
  call ftgkye(unit,'IFLUX',r_flux,comment,status)
  call get_fioerror(pname,'IFLUX',status,error,.false.)
  if (status .gt. 0) then
    r_flux = 0
    status = 0
    error = .false.
  endif
  !      call ftgkye(unit,'QFLUX',r_qflux,comment,status)
  !      if (status .eq. 0) then
  !         call ftgkye(unit,'UFLUX',r_uflux,comment,status)
  !         if (status .gt. 0) go to 99
  !         call ftgkye(unit,'VFLUX',r_vflux,comment,status)
  !         if (status .gt. 0) go to 99
  !      endif
  !      status = 0
  ! cfr codes the frequency axis, cd(1) the data column for USB
  ! cd(2) the data column for LSB
  !
  ! Loop on sidebands in order to fill in the CLIC header
  !c      sideb: DO ISB = 1, 2
  sideb: do isb = 1, nsb
    !c         KSB = 3-2*ISB
    ksb = r_isb
    ! +1, -1
    !
    ! autocorrelation
    !         print *, 'isb, ksb'
    !         print *, isb, ksb
    if (extname(1:4).eq.'AUTO') then
      call ftgcno(unit,.false.,'DATA',data_col,status)
      call get_fioerror(pname,'FTGCNO [DATA]',   &
        status,error,.false.)
      !            IF (STATUS.GT.0) GOTO 99
      write(cd,'(i1)') data_col
      if (status .gt. 0) go to 99
      call ftgtdm(unit,data_col,maxis,naxis,naxes,status)
      call get_fioerror(pname,'FTGTDM [DATA_COL]',status,   &
        error,.false.)
      if (status .gt. 0) go to 99
      nch = naxes(1)
      cfr = '1'
    !
    ! correlation
    elseif (extname(1:4).eq.'CORR') then
      !c            IF (ISB.EQ.1) THEN
      if (ksb.eq.+1) then
        call ftgcno(unit,.false.,'DATAUSB1',data_col,status)
        if (status.ne.0) exit sideb
        !               call get_fioerror(pname,'FTGCNO [DATAUSB1]',status,
        !     $           error,.false.)
        !              IF (STATUS.GT.0) GOTO 99
        write(cd,'(i1)') data_col
      !c            ELSEIF  (ISB.EQ.2) THEN
      elseif  (ksb.eq.-1) then
        call ftgcno(unit,.false.,'DATALSB1',data_col,status)
        !               print *, 'status ',status
        if (status.ne.0) exit sideb
        !               print *, 'status ',status
        !               call get_fioerror(pname,'FTGCNO [DATALSB1]',status,
        !     $           error,.false.)
        !               IF (STATUS.GT.0) GOTO 99
        write(cd,'(i1)') data_col
      endif
      call ftgtdm(unit,data_col,maxis,naxis,naxes,status)
      call get_fioerror(pname,'FTGTDM [DATA_COL]',status,   &
        error,.false.)
      !            IF (STATUS .GT. 0) GO TO 99
      nch = naxes(2)
      cfr = '2'
    endif
    !         print *, 'data_col ',data_col
    !         print *, 'naxis ',naxis
    !         print *, 'naxes ',naxes
    if (error) return
    ! observed frequency
    call ftgkye(unit,cfr//'CRPX'//cd,ifref,comment,status)
    if (status .gt. 0) go to 99
    call ftgkyd(unit,cfr//'CRVL'//cd,fobs,comment,status)
    if (status .gt. 0) go to 99
    call ftgkye(unit,cfr//cfr//'CD'//cd,df,comment,status)
    if (status .gt. 0) go to 99
    ! IF values
    ifinc = isb2*(3-2*isb)*df
    ! Velocity values (A)
    ctype = 'nvel'
    call ftgkys(unit,cfr//'CTYP'//cd//'A',ctype,comment,status)
    if (status .eq. 0) then
      if (ctype(1:4).ne.'VRAD') then
        write(6,*) cfr//'CTYP'//cd//'A', ':  ',ctype,   &
          ' not supported'
      else
        call ftgkye(unit,cfr//'CRPX'//cd//'A',vref,comment,   &
          status)
        if (status .gt. 0) go to 99
        call ftgkyd(unit,cfr//'CRVL'//cd//'A',vv,comment,status)
        if (status .gt. 0) go to 99
        call ftgkye(unit,cfr//cfr//'CD'//cd//'A',dvv,comment,   &
          status)
        if (status .gt. 0) go to 99
        call ftgkyd(unit,'RESTFREA',ff,comment,status)
        if (status .gt. 0) go to 99
        call ftgkys(unit,'TRANSITA',cf,comment,status)
        if (status .gt. 0) go to 99
      endif
    endif
    status = 0
    ! Velocity values (B)
    call ftgkys(unit,cfr//'CTYP'//cd//'B',ctype,comment,status)
    if (status .eq. 0) then
      if (ctype(1:4).ne.'VRAD') then
        write(6,*) cfr//'CTYP'//cd//'B', ':  ',ctype,   &
          ' not supported'
      else
        call ftgkye(unit,cfr//'CRPX'//cd//'B',vref,comment,   &
          status)
        if (status .gt. 0) go to 99
        call ftgkyd(unit,cfr//'CRVL'//cd//'B',vv,comment,status)
        if (status .gt. 0) go to 99
        call ftgkye(unit,cfr//cfr//'CD'//cd//'B',dvv,comment,   &
          status)
        if (status .gt. 0) go to 99
        call ftgkyd(unit,'RESTFREB',ff,comment,status)
        if (status .gt. 0) go to 99
        call ftgkys(unit,'TRANSITB',cf,comment,status)
        if (status .gt. 0) go to 99
      endif
    endif
    status = 0
    !
    !c         print *, 'ctype, ff, cf  ', ctype, ff, cf
    if (ctype.ne.'nvel' .and. iband.eq.1 .and. r_isb.eq.ksb) then
      r_restf = ff/1d6
      r_line = cf
      r_veloc = vv/1d3
    endif
    if (extname(1:4).eq.'AUTO' .and. ksb.ne.r_isb) then
      dvv = -dvv
      df = -df
      ifinc = -ifinc
      x = (r_flo1_ref+r_fif1)*1d6   &
        - ksb*(ff-r_restf*1d6)*(1.+r_doppl)
      vref = ifref + (x-ifval)/ifinc
    endif
    if (nch.eq.1) then
      r_nsb = nsb
      r_cnam(isb) = cf
      r_cvoff(isb) = vv/1d3
      r_cvres(isb) = dvv/1d3
      r_crfoff(isb) = ff/1d6
      ntch = r_nband
      ich = iband-1
      r_cfcen(iband) = ifval/1d6
      r_cfwid(iband) = ifinc/1d6
    else
      r_lnsb = nsb
      r_lnam(isb,iband) =  cf
      r_lnch(iband) = nch
      r_lcench(iband) = ifref
      r_lfcen(iband) = ifval/1d6
      r_lfres(iband) = ifinc/1d6
      r_lrch(isb,iband) = vref
      r_lvoff(isb,iband) = vv/1d3
      r_lvres(isb,iband) = dvv/1d3
      r_lrfoff(isb,iband) = ff/1d6
      r_lrfres(isb,iband) = df/ 1d6
      ntch = r_lntch
      if (iband.eq.1) r_lich(iband) = 0
      if (iband.lt.r_lband) then
        r_lich(iband+1) = r_lich(iband) + nch
      endif
      ich = r_lich(iband)
    endif
  enddo sideb
  !      print *, 'end sideb'
  !
  ! Get the number of rows
  call ftgnrw(unit,nrows,status)
  if (status.gt.0) goto 99
  !      print *, 'nrows ',nrows
  !
  ! Some checks of consistency ...
  !
  !      IF (nrows.ne.r_ndump .and. nrows.gt.1) then
  !         call message(8,3,'GET_FITS_DATA',
  !     $        'Use only one row in averaged data')
  !         print *, 'nrows, r_ndump'
  !         print *, nrows, r_ndump
  !      endif
  if (r_lmode.eq.1) then
    ksb = r_isb
    do isb = 1, nsb
      !            print *, 'isb, ksb ',isb , ksb
      if (ksb.eq.1) then
        call ftgcno(unit,.false.,'DATAUSB1',icol(1),status)
        call get_fioerror(pname,'FTGCNO [DATAUSB1]',status,   &
          error,.false.)
      !         IF (STATUS.GT.0) GOTO 99
      elseif (ksb.eq.-1) then
        call ftgcno(unit,.false.,'DATALSB1',icol(2),status)
        call get_fioerror(pname,'FTGCNO [DATALSB1]',status,   &
          error,.false.)
      !         IF (STATUS.GT.0) GOTO 99
      endif
      ksb = -ksb
    enddo
  else
    call ftgcno(unit,.false.,'DATA',icol(1),status)
    call get_fioerror(pname,'FTGCNO [DATA]',status,   &
      error,.false.)
    if (status.gt.0) goto 99
  endif
  !      print *, 'nsb, icol'
  !      print *, nsb, icol
  !
  ! Loop on rows
  do irow = 1, nrows
    call ftgcvj(unit,integnum_col,irow,1,1,nullval,   &
      kr,anyf,status)
    if (status.gt.0) goto 99
    !
    ! continuum, not averaged
    if (nch.eq.1 .and. idtable.eq.1) then
      k0 = 1+c_offset(kr)
      k1 = 0
    else
      if (nch.eq.1) then
        k0 = 1+c_offset(r_ndump+1)
        k1 = 1+c_offset(r_ndump+2)
      else
        k0 = 1+l_offset(r_ndump+1)
        k1 = 1+l_offset(r_ndump+2)
      endif
    endif
    !         if (idtable.eq.3) then
    !            print *, 'idtable, k0, k1, ich, nch, ntch'
    !            print *, idtable, k0, k1, ich, nch, ntch
    !         endif
    ! Correlation
    if (r_lmode.eq.1) then
      call ftgcvj(unit,startant_col,irow,1,1,nullval,   &
        ia,anyf,status)
      if (status.gt.0) goto 99
      ia = min(r_nant,max(1,ia))
      call ftgcvj(unit,endant_col,irow,1,1,nullval,   &
        ja,anyf,status)
      if (status.gt.0) goto 99
      ja = min(r_nant,max(1,ja))
      ib = basant(ia,ja)
      do isb = 1, nsb
        af = 1./sqrt(dh_atfac(1,isb,ia)*dh_atfac(1,isb,ja))
        kd = k0   &
          + (ich+(isb-1+(ib-1)*nsb)*ntch)*2
        call ftgcve(unit,icol(isb),irow,1,2*nch,nullval,   &
          data(kd),anyf,status)
        call rescale(2*nch,data(kd),af)
        !         if (idtable.eq.3) then
        !            print *, 'irow, ib, isb, kd-k0, DATA(KD),DATA(KD+1)'
        !            jdata(1) = data(kd)
        !            jdata(2) = data(kd+1)
        !            print *, irow, ib, isb, kd-k0,  rdata
        !         endif
        if (status.gt.0) goto 99
        ! corrected data
        if (r_ndatl.eq.2 .and. k1.ne.0) then
          kd = k1   &
            + (ich+(isb-1+(ib-1)*nsb)*ntch)*2
          call ftgcve(unit,icol(isb),irow,1+2*nch,2*nch,nullval,   &
            data(kd),anyf,status)
          if (status.gt.0) goto 99
          af = 1./sqrt(dh_atfac(1,isb,ia)*dh_atfac(1,isb,ja))
          call rescale(2*nch,data(kd),af)
        endif
      enddo
    !     Autocorrelation
    elseif (r_lmode.eq.2) then
      af = 1./dh_atfac(1,1,ia)
      call ftgcvj(unit,antenna_col,irow,1,1,nullval,   &
        ia,anyf,status)
      if (status.gt.0) goto 99
      kd = k0 + ich + (ia-1)*ntch
      call ftgcve(unit,icol(1),irow,1,nch,nullval,   &
        data(kd),anyf,status)
      if (status.gt.0) goto 99
      call rescale(nch,data(kd),af)
    endif
  enddo
  return
  !
99 call printerror('GET_FITS_DATA',status)
  call message(6,2,'GET_FITS_DATA',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_data
!
