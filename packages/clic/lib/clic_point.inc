      INTEGER IFIX(MNANT),NFIX     ! Fixed antennas
      INTEGER IMOB(MNANT),NMOB     ! Mobile antennas
      INTEGER NP                   ! Number of data points
      INTEGER MP                   ! Size of arrays
      INTEGER INUM                 ! Observation number
      INTEGER ISCAN                ! Scan number
      INTEGER ICODE                ! Scanning type
      REAL COLL(MNANT)             ! Collimations
      INTEGER KANT(MNANT)          ! Connected antenna
      INTEGER ISTAT(MNANT)         ! Station number
      CHARACTER*12 SOURCE          ! Source name
      REAL*8 AZ,EL,UT,A_FLUX       ! Az,el,ut,freq
      INTEGER IPROC                ! Type of Procedure
      INTEGER IAUTO                ! For pseudo continuum
!
      COMMON /SPOINT/                                                   &
     &AZ,EL,UT,A_FLUX,                                                  &
     &IFIX,NFIX,IMOB,NMOB,NP,MP,INUM,ISCAN,ICODE,                       &
     &COLL,KANT,ISTAT,                                                  &
     &SOURCE,IPROC,IAUTO
      SAVE /SPOINT/
