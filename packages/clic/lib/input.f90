subroutine input(spec,nspec,error)
  use gbl_format
  use gkernel_interfaces
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  include 'clic_parameter.inc' 
  include 'clic_clic.inc'
  !---------------------------------------------------------------------
  ! CLIC	Internal routine
  !	Close any input file, set the file specification for input,
  !	open this file.
  ! Arguments :
  ! 	SPEC	C*(*)	File name including extension	Input
  !	NSPEC	I	Length of SPEC			Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  character(len=*) :: spec          !
  integer :: nspec                  !
  logical :: error                  !
  ! Local
  integer(kind=entry_length) :: lsize
  integer :: version,reclen,vind,lind
  integer*4 :: saved(32)
  logical :: lsingle
  real :: gex
  integer*4 :: max_record
  parameter (max_record=2147483647)
  !
  integer :: lname, ier, ilun0, olun0, lun_1, lun_2
  integer :: iind
  integer(kind=entry_length) :: ientry
  character(len=256) :: name,mess
  character(len=20) :: aconv
  character(len=16) :: logs(10)
  character(len=4) :: rname
  logical :: lres, err
  type(title_v2_t) :: title_save
  !
  data logs/'IPB_DATA:','IPB_DATA1:','IPB_DATA2:',   &
    'IPB_DATA3:','IPB_DATA4:','IPB_DATA5:',   &
    'IPB_DATA6:','IPB_DATA7:','IPB_DATA8:','IPB_DATA9:'/
  data lun_1/0/, lun_2/0/
  save lun_1,lun_2
  !
  ! Save current short title
  title_save = title
  !
  ! initializations
  ier = 1
  if (lun_1.eq.0) ier = sic_getlun(lun_1)
  if (lun_2.eq.0) ier = sic_getlun(lun_2)
  if (ier.ne.1) then
    error = .true.
    call message(8,4,'INPUT','No logical unit left')
    return
  endif
  !
  ! Get the full filename of the specified input file
  inquire(file=spec(1:nspec),exist=lres)
  if (.not.lres) then
    error = .true.
    call message(8,4,'INPUT','File '//spec(1:nspec)//   &
      ' does not exist')
    return
  endif
  inquire(file=spec(1:nspec),opened=lres)
  !
  ! If it is opened, recover its full file name
  if (lres) then
    inquire(file=spec(1:nspec),name=name)
    nspec = lenc(name)
    spec = name(1:nspec)
  endif
  !
  ! Save current unit to help error recovery
  ilun0 = i%lun
  if (nspec.eq.o%nspec .and. spec(1:nspec).eq.o%spec) then
    ! The same file is already opened on unit OLUN
    if (i%lun.ne.o%lun) close(unit=i%lun)
    i%lun = o%lun
  else
    if (i%lun.ne.0 .and. i%lun.eq.o%lun) then
      ! A file is already opened for Input and Output on unit OLUN/ILUN
      ! Allocate the other available unit and open it
      if (o%lun.eq.lun_1) then
        i%lun = lun_2
      else
        i%lun = lun_1
      endif
    else
      ! A file may have been opened for Input only on unit ILUN
      ! Close the Input unit or allocate it, and open it
      if (i%lun.eq.0) then
        if (o%lun.ne.lun_1) then
          i%lun = lun_1
        else
          i%lun = lun_2
        endif
      else
        close(unit=i%lun)
      endif
    endif
    !
    ! Close raw data file if necessary
    if (d%lun.ne.0 .and. d%spec(1:d%nspec).eq.spec(1:nspec)) then
      close (unit=d%lun)
      d%lun = 0
    endif
    !
    ! Open the file with V1 record length
    i%desc%reclen = classic_reclen_v1
    i%nspec = nspec
    i%spec = spec(1:nspec)
    i%readwrite = .false.
    open (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
      status='OLD',form='UNFORMATTED',recl=i%desc%reclen*facunf,   &
      action='READ',iostat=ier)
    if (ier.ne.0) then
      error = .true.
      mess = 'Open error file '//spec(1:nspec)
      call message(8,4,'INPUT',mess)
      call messios(8,4,'INPUT',ier)
      goto 19
    endif
  endif
  !
  ! Classic lib: read the file descriptor
  call classic_filedesc_open(i,error)
  if (error) goto 19
  !
  ! Classic: reallocate buffer for new record length
  call reallocate_recordbuf(ibuff,i%desc%reclen,error)
  call reallocate_recordbuf(ibufi,i%desc%reclen,error)
  call classic_recordbuf_nullify(ibufi)
  !
  ! read the index : Read all blocks even first one
  if (i%desc%xnext-1.gt.m_ix) then
    write (mess,'(A,I0,A)') "Input file has ",i%desc%xnext-1," entries"
    call message(8,3,'INPUT',mess)
    write (mess,'(A,I0,A)') "CLIC can only proces up to ",m_ix," entries"
    call message(8,3,'INPUT',mess)
    error = .true.
    goto 19
  endif
  do ientry = 1, i%desc%xnext-1
    call rix (ientry,error)
    if (error) goto 100
    ix_num(ientry)   = title%num
    ix_bloc(ientry)  = title%bloc
    ix_word(ientry)  = title%word
    ix_ver(ientry)   = title%ver
    ix_kind(ientry)  = title%kind
    ix_qual(ientry)  = title%qual
    ix_scan(ientry)  = title%scan         ! CLIC
    ix_itype(ientry) = title%itype        ! CLIC
    ix_proc(ientry)  = title%proc         ! CLIC
    ix_rece(ientry)  = title%recei
  enddo
  !
  ! Reset EIX routine
  call eix(-1_8,0_8,.false.,error)
  !
  ! Reset the current index :
  call zero_index
  !
  ! OK Reset the input file name and acknowledge the operation
  inquire (unit=i%lun,name=name)
  i%nspec= lenc(name)
  i%spec =name(1:i%nspec)
  mess = name(1:i%nspec)//' successfully opened'
  call message(5,1,'INPUT',mess)
  goto 100
  !
  ! Try to recover from the error
19 if (i%lun.ne.o%lun) close(unit=i%lun)
  i%lun = ilun0
  if (i%spec.eq.' ' .or. ilun0.eq.0) goto 100
  !
  ! Reopen the file if necessary
  if (i%lun.ne.o%lun) then
    open (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
      status='OLD',form='UNFORMATTED',recl=i%desc%reclen*facunf,   &
      action='READ',iostat=ier)
    if (ier.ne.0) goto 20
  endif
  !
  call classic_filedesc_open(i,err)
  if (err) goto 20
  !
  call classic_recordbuf_nullify(ibufi)
  !
  inquire (unit=i%lun,name=name)
  mess = name(1:lenc(name))//' is reopened'
  call message(2,1,'INPUT',mess)
  goto 100
20 call messios(6,3,'INPUT',ier)
  call message(6,2,'INPUT','No input file opened')
  goto 100
  !<FF>
entry output(spec,nspec,error)
  !----------------------------------------------------------------------
  !CLIC	Internal routine
  !	Close any output file, set the file specifications for output
  !	open this file.
  !	(also selected for input if infile not defined)
  ! Arguments :
  !	SPEC	C*(*)	File name			Input
  !	NSPEC	I	Length of SPEC			Input
  !	ERROR	L	Logical error flag		Output
  !----------------------------------------------------------------------
  !
  ! Save current short title
  title_save = title
  ier = 1
  if (lun_1.eq.0) ier = sic_getlun(lun_1)
  if (lun_2.eq.0) ier = sic_getlun(lun_2)
  if (ier.ne.1) then
    error = .true.
    call message(8,4,'OUTPUT','No logical unit left')
    return
  endif
  olun0 = o%lun
  !
  ! Get the full filename of the specified output file
  inquire(file=spec(1:nspec),exist=lres)
  if (.not.lres) then
    error = .true.
    call message(8,4,'OUTPUT','File '//spec(1:nspec)//   &
      ' does not exist')
    return
  endif
  inquire(file=spec(1:nspec),opened=lres)
  !
  ! If it is opened, recover its full file name
  if (lres) then
    inquire(file=spec(1:nspec),name=name)
    nspec = lenc(name)
    spec = name(1:nspec)
  endif
  !
  if (nspec.eq.i%nspec .and. spec(1:nspec).eq.i%spec) then
    ! The input file is identical to the output file
    if (i%lun.ne.o%lun) close(unit=o%lun)
    o%lun = i%lun
  elseif (i%lun.ne.0 .and. i%lun.eq.o%lun) then
    ! A file is already opened for Input and Output on unit I%LUN
    ! Allocate the other available unit and open it
    if (o%lun.eq.lun_1) then
      o%lun = lun_2
    else
      o%lun = lun_1
    endif
  else
    ! A file may have been opened for Output only on unit O%LUN
    ! Close the Output unit or allocate it, and open it.
    if (o%lun.eq.0) then
      if (i%lun.ne.lun_2) then
        o%lun = lun_2
      else
        o%lun = lun_1
      endif
    endif
  endif
  !
  ! In any case, close the output file and reopen it with write access
  close (unit=o%lun)
  !
  o%nspec = nspec
  o%spec = spec(1:nspec)
  o%desc%reclen = classic_reclen_v1
  !
  ! Output file must have write access (reopened in classic_filedesc_open)
  o%readwrite = .true.  
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',form='UNFORMATTED',recl=o%desc%reclen*facunf,   &
    action='READWRITE',iostat=ier)
  !
  if (ier.ne.0) then
    error = .true.
    mess = 'Open error file '//spec(1:nspec)
    call message(8,4,'OUTPUT',mess)
    call messios(8,4,'OUTPUT',ier)
    goto 29
  endif
  !
  call classic_filedesc_open(o,error)
  if (error) goto 29
  !
  ! Reallocate the buffers if reclen changed
  call reallocate_recordbuf(obuff,o%desc%reclen,error)
  call reallocate_recordbuf(obufi,o%desc%reclen,error)
  call classic_recordbuf_nullify(obufi) 
  !
  ! read the index
  if (o%desc%xnext-1.gt.m_ox) then
    write (mess,'(A,I0,A)') "Output file has ",i%desc%xnext-1," entries"
    call message(8,3,'INPUT',mess)
    write (mess,'(A,I0,A)') "CLIC can only proces up to ",m_ix," entries"
    call message(8,3,'INPUT',mess)
    error = .true.
    goto 30
  endif
  do ientry = 1, o%desc%xnext-1
    call rox (ientry,error)
    if (error) goto 100
    ox_num(ientry)  = title%num
    ox_bloc(ientry) = title%bloc
    ox_word(ientry) = title%word
    ox_ver(ientry)  = title%ver
    ox_scan(ientry) = title%scan
    ox_ut(ientry)   = title%ut
    ox_rece(ientry) = title%recei
  enddo
  inquire (unit=o%lun,name=name)
  o%nspec=lenc(name)
  o%spec =name(1:o%nspec)
  mess = name(1:lenc(name))//' successfully opened'
  call message(1,1,'OUTPUT',mess)
  goto 100
  !
  ! Try to recover from the error
29 if (i%lun.ne.o%lun) then
    close(unit=o%lun)
  elseif (i%lun.ne.0 .and. i%spec.ne.' ') then
    close (unit=o%lun)          
    open (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
      status='OLD',form='UNFORMATTED',recl=i%desc%reclen*facunf,   &
      action='READWRITE',iostat=ier)
    if (ier.ne.0) goto 30
  endif
  o%lun = olun0
  if (o%spec.eq.' ' .or. olun0.eq.0) goto 100
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',form='UNFORMATTED',recl=o%desc%reclen*facunf,   &
    action='READWRITE',iostat=ier)
    if (ier.ne.0) goto 30
  call classic_filedesc_open(o,err)
  if (err) goto 30
  !
  inquire (unit=o%lun,name=name)
  mess = name(1:lenc(name))//' is reopened'
  call message(2,1,'OUTPUT',mess)
  goto 100
30 call messios(6,3,'OUTPUT',ier)
  call message(6,2,'OUTPUT','No output file opened')
  call classic_recordbuf_nullify(obufi) 
  goto 100
  !<FF>
entry init (spec,nspec,lsize,error)
  !----------------------------------------------------------------------
  !CLIC	Internal routine
  !	Initialise an output file
  !	Close any output file, set the file specifications for output
  !	open this file.
  ! Arguments :
  !	SPEC	C*(*)	Name of file
  !	NSPEC	I	Length of SPEC
  !	LSIZE 	I	maximum number of observations allowed
  !	ERROR	L	Logical error flag
  !----------------------------------------------------------------------
  !
  ! Save current short title
  title_save = title
  !
  ! No test on file name since another version must be created anyway
  !
  ier = 1
  if (lun_1.eq.0) ier = sic_getlun(lun_1)
  if (lun_2.eq.0) ier = sic_getlun(lun_2)
  if (ier.ne.1) then
    error = .true.
    call message(8,4,'INPUT','No logical unit left')
    return
  endif
  olun0 = o%lun
  if (i%lun.ne.0 .and. i%lun.eq.o%lun) then
    ! A file is already opened for Input and Output on unit I%LUN/O%LUN
    ! Allocate the other available unit and open it
    if (o%lun.eq.lun_1) then
      o%lun = lun_2
    else
      o%lun = lun_1
    endif
  else
    ! A file may have been opened for Output only on unit O%LUN.
    ! Close the Output unit or allocate it, and open it.
    if (o%lun.eq.0) then
      if (i%lun.ne.lun_2) then
        o%lun = lun_2
      else
        o%lun = lun_1
      endif
    endif
  endif
  !
  ! In any case, close the output file and reopen it with write access
  close (unit=o%lun)
  !
  o%desc%reclen = classic_reclen_v1
  o%spec = spec(1:nspec)
  o%nspec = nspec
!  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
!    status='NEW',form='UNFORMATTED',recl=o%desc%reclen*facunf,   &
!    action='READWRITE',iostat=ier)
  !
  ier = 0
  if (ier.ne.0) then
    error = .true.
    mess = 'Open error file '//spec(1:nspec)
    call message(8,4,'INIT',mess)
    call messios(8,4,'INIT',ier)
    goto 39
  endif
  !
  ! Default is to write classic v2 format
  version = 2
  ier = sic_getlog('CLIC_FILE_VERSION',version)
  if (version.eq.1) then
    reclen = classic_reclen_v1 ! Default for v1 file
  else
    reclen = 1024 ! [words]  Default for v2 file
    ier = sic_getlog('CLIC_FILE_RECORD',reclen)
  endif
  call classic_file_init(o,version,reclen,error)
  if (error) goto 39 
  !
  ! Deal with index version and length
  lsingle = .false.
  if (version.eq.1) then
    vind = classic_vind_v1 ! Mandatory for v1 file
    lind = classic_lind_v1 ! Mandatory for v1 file
  else
    vind = vind_v2
    lind = lind_v2
  endif
  !
  ! Linear growth in all cases
  gex = 1.0
  call classic_filedesc_init(o,classic_kind_clic,lsingle, &
       lsize,vind,lind,gex,error)
  if (error) goto 39
  !
  !
  ! Close and reopen the file to flush first blocks
  inquire (unit=o%lun,name=name)
  close (unit=o%lun)
  lname = lenc(name)
  o%spec = name(1:lname)
  o%nspec = lname
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',form='UNFORMATTED',recl=o%desc%reclen*facunf,   &
    action='READWRITE',iostat=ier)
  if (ier.ne.0) then
    error = .true.
    mess = 'Open error file '//spec(1:nspec)
    call message(8,4,'INIT',mess)
    call messios(8,4,'INIT',ier)
    goto 39
  endif
  !
  mess = name(1:lname)//' initialized'
  call message(1,1,'INIT',mess)
  !
  ! Reallocate the buffers if reclen changed
  call reallocate_recordbuf(obuff,o%desc%reclen,error)
  call reallocate_recordbuf(obufi,o%desc%reclen,error)
  call classic_recordbuf_nullify(obufi) 
  !
  goto 100
  !
  ! Try to recover from the error
39 close(unit=o%lun)
  o%lun = olun0
  if (o%spec.eq.' ' .or. olun0.eq.0) goto 100
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',form='UNFORMATTED',recl=o%desc%reclen*facunf,   &
    action='READWRITE',iostat=ier)
  if (ier.ne.0) goto 40
  call classic_filedesc_open(o,err)
  if (err) goto 40
  inquire (unit=o%lun,name=name)
  o%nspec=lenc(name)
  o%spec=name(1:o%nspec) 
  mess = name(1:lenc(name))//' is reopened'
  call message(2,1,'OUTPUT',mess)
  goto 100
40 call messios(6,3,'OUTPUT',ier)
  call message(6,3,'OUTPUT','No output file opened')
  goto 100
  !
100 continue
  title = title_save
  bp_spectrum_memory = .false.
  return
  !<FF>
entry info(error)
  !----------------------------------------------------------------------
  ! SAS	Internal routine
  !	Prints input and output file names
  !	Use of message suppressed
  !----------------------------------------------------------------------
  if (i%lun.le.0) then
    write(6,1000) 'No input file opened'
  else
    call gdf_conversion(i%conv%code,aconv)
    inquire(unit=i%lun,name=name)
    write(6,1000) 'Input file ',name(1:lenc(name)),aconv
  endif
  if (o%lun.le.0) then
    write(6,1000) 'No output file opened'
  else
    if (o%lun.ne.i%lun) then
      inquire(unit=o%lun,name=name)
      call gdf_conversion(o%conv%code,aconv)
    endif
    write(6,1000) 'Output file ',name(1:lenc(name)),aconv
  endif
  !
  write(6,1000) 'Raw data files are searched in: '
  do iind=1,10
    name = logs(iind)
    ier = sic_getlog(name)
    if (name.ne.logs(iind) .and. name.ne.' ') then
      write(6,1000) '    ',name(1:lenc(name))
    endif
  enddo
1000 format(1x,10(a))
end subroutine input
