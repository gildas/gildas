subroutine newdata (line,error)
  use gkernel_interfaces
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! SAS	Support routine for command
  !	NEW_DATA
  !	Find a set of new observations of any kind.
  ! Arguments :
  !	LINE	C*(*)	Command line			Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  integer*4 :: lsave(32)
  integer(kind=8) :: ii, nnn, inew, jnew  
  character(len=10) :: nombre
  logical :: dump
  type(title_v2_t) :: title_save
  !----------------------------------------------------------------------
  interval = 10.0
  number_trys = 12
  call sic_r4 (line,0,1,interval,.false.,error)
  if (error) return
  interval = max(1.,interval)
  call sic_i4 (line,0,2,number_trys,.false.,error)
  if (error) return
  number_trys = max(10,number_trys)
  !
  call check_input_file (error)
  if (error) return
  !
  ! Save the current short title
  title_save = title
  !
  ! Zeroes the current index
  cxnext = 1
  knext = 0
  !
  ! Find new data
  call eix(inew,jnew,.true.,error) ! Allow to change type
  if (error) goto 999
  if (jnew.eq.0) then
    dump = .true.
    jnew = inew
  else
    dump = .false.
  endif
  nnn = 0
  !
  ! Select only last versions
  do 10 ii = inew,jnew
    title%ver  = ix_ver(ii)
    if (title%ver.lt.0) goto 10
    title%num  = ix_num(ii)
    title%bloc = ix_bloc(ii)
    title%word = ix_word(ii)
    nnn = nnn+1
    if (nnn.gt.m_cx) go to 97
    cx_ind (cxnext) = ii
    cx_num (cxnext) = title%num
    cx_ver (cxnext) = title%ver
    cx_bloc(cxnext) = title%bloc
    cx_word(cxnext) = title%word
    cxnext = cxnext + 1
10 continue
  !
  if (dump) then
    call message(5,1,'NEW_DATA','More dumps found')
  elseif (nnn.eq.1) then
    call message(5,1,'NEW_DATA','1 observation found')
  elseif (nnn.ne.0) then
    write(nombre,'(I10)') nnn
    call message(5,1,'NEW_DATA',nombre//' observations found')
  else
    call message(6,2,'NEW_DATA','Nothing found')
  endif
  call new_file
  !
  ! Restore the current short title
999 title = title_save
  call index_variable
  return
  !
  ! Error returns
  !
97 write(nombre,'(I6)') m_cx
  call message(8,2,'EIX','More than '//nombre//   &
    ' observations found.')
  goto 999
end subroutine newdata
!
subroutine eix(first_new,last_new,change,error)
  use gkernel_interfaces
  use gbl_format
  use classic_api
  use clic_file
  use clic_title
  !---------------------------------------------------------------------
  ! LAS
  !	Examine input index, until new data is written.
  !	FIRST_NEW = -1 to initialise
  !---------------------------------------------------------------------
  integer(kind=8) :: first_new              ! First new index entry
  integer(kind=8) :: last_new               ! Last new index entry
  logical         :: change                 ! Change type of observations
  logical         :: error                  ! Error return
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: ier, errcount, icount
  integer(kind=8) :: old_inext
  integer(kind=4) :: old_word
  data old_inext/0/
  data old_word/1/
  !------------------------------------------------------------------------
  ! Code
  if (first_new.eq.-1) then
    old_ixnext = i%desc%xnext
    old_inext  = i%desc%nextrec
    old_word   = i%desc%nextword
    return
  endif
  error = .false.
  ier = 0
  errcount = 0
  do
    do icount = 1, number_trys
      call classic_filedesc_read(i,error)
      if (error) goto 11
      !
      ! New observation has been written
      if (old_ixnext.lt.i%desc%xnext) then
        close (unit=i%lun)
        open  (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
          recl=facunf*i%desc%reclen,status='OLD',action='READ',iostat=ier)
        if (ier.ne.0) goto 20
        call eix_newdata(first_new,last_new,change,old_inext,old_word,error)
        if (error) exit
        return
      !
      ! Else wait and check if last observation extended
      else
        close (unit=i%lun)
        if (old_inext.eq.i%desc%nextrec.and.old_word.eq.i%desc%nextword) then
          call sic_wait(interval)
        endif
        open (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
          recl=facunf*i%desc%reclen,status='OLD',action='READ',iostat=ier)
        if (ier.ne.0) goto 20
        if (sic_ctrlc()) then
          error = .true.
          call message(8,1,'EIX','Waiting loop aborted by ^C')
          return
        endif
        !
        ! In case last observation has been extended
        if (old_inext.ne.i%desc%nextrec.or.old_word.ne.i%desc%nextword) then
          old_inext = i%desc%nextrec
          old_word  = i%desc%nextword
          first_new = i%desc%xnext-1
          last_new  = first_new
          return
        endif
      endif
    enddo
    call message(2,2,'EIX','File '//i%spec(1:i%nspec)//' timed out')
    error = .true.
    return
    !
    ! Various errors
11  if (errcount.eq.2) then
      error = .true.
      call message(8,4,'EIX','Read error file '//i%spec(1:i%nspec))
      call messios(8,4,'EIX',ier)
      return
    endif
    errcount = errcount+1
    call sic_wait(1.0)
  enddo
20 error = .true.
  call messios (6,3,'EIX',ier)
  call message (6,2,'EIX','No input file opened')
end subroutine eix
!
subroutine eix_newdata(first_new,last_new,change,old_inext,old_word,error)
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  use clic_find
  !--------------------------------------------------------------------------
  ! New data found
  !--------------------------------------------------------------------------
  integer(kind=entry_length), intent(inout) :: first_new
  integer(kind=entry_length), intent(out)   :: last_new
  logical,                    intent(in)    :: change 
  integer(kind=8),            intent(out)   :: old_inext
  integer(kind=4),            intent(out)   :: old_word
  logical,                    intent(inout) :: error
  !
  ! Global  
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !
  ! Local
  integer(kind=entry_length) :: ii
  ! 
  ! Re-read file desc
  call classic_filedesc_read(i,error)
  if (error) return
  !
  old_inext = i%desc%nextrec
  old_word  = i%desc%nextword
  !
  ! Nullify input index buffer
  call classic_recordbuf_nullify(ibufi)
  !
  ! Enforce re-reading of observation buffer
  ! Needed from V2, since 2 observations can share a common record
  call classic_recordbuf_nullify(ibuff)
  !
  if (change) then
    first_new  = old_ixnext
    last_new   = first_new
    old_ixnext = old_ixnext + 1
    call rix (first_new,error)
    if (error) return
    !
    ! Copy new observation to input index
    ix_num(first_new)   = title%num
    ix_bloc(first_new)  = title%bloc
    ix_word(first_new)  = title%word
    ix_ver(first_new)   = title%ver
    ix_kind(first_new)  = title%kind
    ix_qual(first_new)  = title%qual
    ix_scan(first_new)  = title%scan
    ix_itype(first_new) = title%itype
    ix_proc(first_new)  = title%proc
    ix_rece(first_new)  = title%recei
    if (skind.ne.title%kind) then
      if (skind.ge.3) then
        call message(4,1,'IEX',   &
          'Switching to Interferometer Mode')
        skind = 5
        xkind = 5
        ykind = 3
      elseif  (skind.lt.3) then
        call message(4,1,'IEX',   &
          'Switching to Single-Dish Mode')
        skind = title%kind
        ykind = 0
        xkind = title%kind
      endif
    endif
    if (i%desc%xnext.le.old_ixnext) return
    !
    ! If more than one :
    ! Care is taken that modes are not mixed.
    do ii = first_new+1, i%desc%xnext-1
      call rix (ii,error)
      if (error) return
      if (title%kind.eq.skind) then
        ix_num(ii)   = title%num
        ix_bloc(ii)  = title%bloc
        ix_word(ii)  = title%word
        ix_ver(ii)   = title%ver
        ix_kind(ii)  = title%kind
        ix_qual(ii)  = title%qual
        ix_scan(ii)  = title%scan
        ix_itype(ii) = title%itype
        ix_proc(ii)  = title%proc
        ix_rece(ii)  = title%recei
        last_new     = last_new + 1
        old_ixnext   = last_new + 1
      else
        return
        ! stop there ; keep other data for next FIND NEW_DATA command
      endif
    enddo
    return
  else
    call rix (old_ixnext,error)
    old_ixnext = old_ixnext + 1
    if (error) return
    do while(skind.ne.title%kind)
      if (i%desc%xnext.le.old_ixnext) return
      call rix (old_ixnext,error)
      old_ixnext = old_ixnext + 1
      if (error) return
    enddo
    call message(4,1,'EIX','New data present')
    first_new = old_ixnext-1
    last_new  = first_new
    ix_num(first_new)   = title%num
    ix_bloc(first_new)  = title%bloc
    ix_word(first_new)  = title%word
    ix_ver(first_new)   = title%ver
    ix_kind(first_new)  = title%kind
    ix_qual(first_new)  = title%qual
    ix_scan(first_new)  = title%scan
    ix_itype(first_new) = title%itype
    ix_proc(first_new)  = title%proc
    ix_rece(first_new)  = title%recei
    if (i%desc%xnext.le.old_ixnext) return
    !
    ! If more than one :
    ! Care is taken that line and continuum mode are not mixed.
    do ii = first_new+1, i%desc%xnext-1
      call rix (ii,error)
      if (error) return
      if (title%kind.eq.skind) then
        ix_num(ii)   = title%num
        ix_bloc(ii)  = title%bloc
        ix_word(ii)  = title%word
        ix_ver(ii)   = title%ver
        ix_kind(ii)  = title%kind
        ix_qual(ii)  = title%qual
        ix_scan(ii)  = title%scan
        ix_itype(ii) = title%itype
        ix_proc(ii)  = title%proc
        ix_rece(ii)  = title%recei
        last_new     = last_new + 1
        old_ixnext   = last_new + 1
      else
        ! stop there ; keep other data for next FIND NEW_DATA command
        return
      endif
    enddo
    return
  endif
end subroutine eix_newdata


