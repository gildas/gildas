subroutine write_fits_datapar (unit, nsubant, subant,   &
    ldata, data , holodata, switch, simpolar, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use gbl_constant
  !---------------------------------------------------------------------
  ! Write the DATAPAR fits table.
  ! assumes NO_POL=1, NO_FEED=1
  ! switch forces the writing of 2 switch phases for each record
  !     ISWITCH is ON and OFF
  !     WSWITCH is +1., -1.
  !---------------------------------------------------------------------
  integer :: unit                   !
  integer :: nsubant                !
  integer :: subant(nsubant)        ! selected antennas in table
  integer :: ldata                  !
  integer :: data(1)                !
  logical :: holodata               !
  logical :: switch                 !
  logical :: simpolar               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'clic_proc_par.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: k, mf, status, nrows, tfields, varidat, ir, h_offset
  integer :: i, j, naxis, naxes(4), r_npol, r_nfeed, r_nside, mdigit2, mdigit1
  integer :: ncorr, nauto, nholo, isub, felem, jr, nswitch, isw
  logical :: datanum(5), corr, holo, auto
  real :: rrlamof, rrbetof, t(2), dlam, dtot, ntot, beamsize, dtt
  real :: dist2
  parameter(r_npol = 1, r_nside=2, mdigit2=13,   &
    mdigit1=8)
  real*8 :: uvw(3), sind, cosd, sinh, cosh
  parameter (mf = 23)
  character(len=16) :: ttype(mf),tform(mf),tunit(mf), extname
  character(len=8) :: vcode(0:4), scode(0:3)
  ! switch simulation
  real :: wsw(2)
  data wsw/+1.,-1./
  real :: l0, b0, f0              ! pointing and focus errors
  data l0/-4.5/, b0/8.5/, f0/0.45/
  data beamsize /40./          ! 40 arc sec.
  data dtot, ntot / 12., 0.25/
  !
  data ttype/   &
    'INTEGNUM', 'INTEGTIM',  'MJD',      &  ! 1  2  3
    'UUVVWW',   'AZELERR',   'SOURDIR',  &  ! 4 5  6
    'DELAYGEO', 'DELAYOFF',              &  ! 7 8
    'PHASEGEO', 'PHASEOFF',              &  ! 9 10
    'RATEGEO',  'RATEOFF',               &  ! 11 12
    'FOCUSOFF', 'LONGOFF',   'LATOFF',   &  ! 13 14 15
    'TOTPOWER',  'WINDSPEE', 'WINDDIRE', 'FLAG',  &  ! 16 17 18 19
    'RDPOWER',  'CORR',                  &  ! 20 21
    'ISWITCH', 'WSWITCH'/                   ! 22 23
  data tform/   &
    '1J',       '1E',      '1D',   &
    '*D',       '*D',      '*D',   &
    '*D',       '*D',   &
    '*D',       '*D',   &
    '*D',       '*D',   &
    '*E',       '*E',      '*E',   &
    '*E',       '1E',      '1E',       '*J',   &
    '*E',       '2L',   &
    '1J',       '1E'/
  data tunit/   &
    ' ',        's',       'day',   &
    's',        'deg',     ' ',   &
    's',        's',   &
    'rad',      'rad',   &
    'rad/s',    'rad/s',   &
    'm',        'deg',     'deg',   &
    'adu',        'm/s',      'deg',      ' ',   &
    'adu',      ' ',   &
    ' ',        ' '/
  !
  data vcode/'VELO-UNK','VELO-LSR','VELO-HEL','VELO-OBS','VELO-EAR'/
  data scode/ '?...','OBJ','PHAS','?...'/
  !      DATA SYSTEXT /'Un','Un','Eq','Ga','Ho','Pl','Ea'/
  !      DATA VELTEXT /'Unkn.','LSR  ','Hel. ','Obs. ','Null '/
  !      DATA CHARTYPE /'*','O','P','R'/
  !---------------------------------------------------------------------
  if (simpolar) then
    r_nfeed = 2
  else
    r_nfeed = 1
  endif
  !
  !  Append/create a new empty HDU onto the end of the file and move to it.
  status = 0
  call ftcrhd(unit,status)
  !
  !  Define parameters for the binary table (see the above data statements)
  nswitch = 1
  tfields = mf-2
  if (holodata) then
    nrows = r_ndump
  ! nutator switch simulation (switch = .true.)
  elseif (switch) then
    nswitch = 2
    nrows = nswitch*r_ndump+1
    tfields = mf
  else
    nrows = r_ndump+1
  endif
  extname = 'DATAPAR-ALMATI'
  varidat = 0
  !
  ! number of data tables of various kinds.
  nholo = 0
  nauto = 0
  ncorr = 0
  if(r_project.eq.'HOLO' .and. r_lmode.eq.1) then
    holo = .true.
    auto = .false.
    corr = .false.
    nholo = 1
    ttype(21) = 'HOLO'
    call f_tform(tform(21),1,'L')  ! HOLO
  elseif (r_lmode.eq.1) then
    holo = .false.
    auto = .false.
    corr = .true.
    ncorr = 3
    ttype(21) = 'CORR'
    call f_tform(tform(21),3,'L')  ! CORR
  elseif (r_lmode.eq.2) then
    holo = .false.
    auto = .true.
    corr = .false.
    nauto = 3
    ttype(21) = 'AUTO'
    call f_tform(tform(21),3,'L')  ! AUTO
  endif
  ! columns in datapar
  ! changed to use nsubant instead or r_nant
  call f_tform(tform(4),3*nsubant,'D') ! UUVVWW
  call f_tform(tform(5),2*nsubant,'E') ! AZELERR
  call f_tform(tform(6),3*nsubant,'D') ! SOURDIR
  call f_tform(tform(7),nsubant,'D')   ! DELAYGEO
  call f_tform(tform(8),r_lband*nsubant,'D')   ! DELAYOFF
  call f_tform(tform(9),nsubant,'D')   ! PHASEGEO
  call f_tform(tform(10),r_lband*nsubant,'D')  ! PHASEOFF
  call f_tform(tform(11),nsubant,'D')  ! RATEGEO
  call f_tform(tform(12),r_lband*nsubant,'D')  ! RATEOFF
  call f_tform(tform(13),nsubant,'E')  ! FOCUSOFF
  call f_tform(tform(14),nsubant,'E')  ! LATOFF
  call f_tform(tform(15),nsubant,'E')  ! LONGOFF
  ! rl 2004-08-24 Revert to original (wrong) configuration used for aips++test
  call f_tform(tform(16),r_lband*nsubant,'E')  ! TOTPOWER * rl 2004-08-24
  ! rl 2004-08-24       CALL F_TFORM(TFORM(16),R_nfeed*NSUBANT,'E')    ! TOTPOWER
  call f_tform(tform(19),1*r_lband*nsubant,'J')    ! FLAG
  call f_tform(tform(20),nsubant,'E')  ! RDPOWER
  call ftphbn(unit,nrows,tfields,ttype,tform,tunit,   &
    extname,varidat,status)
  naxis = 2
  naxes(1) = 3
  naxes(2) = nsubant
  call ftptdm(unit, 4, naxis, naxes, status)
  call ftptdm(unit, 6, naxis, naxes, status)
  naxis = 2
  naxes(1) = 2
  naxes(2) = nsubant
  call ftptdm(unit, 5, naxis, naxes, status)
  naxis = 2
  naxes(1) = r_lband
  naxes(2) = nsubant
  call ftptdm(unit, 8, naxis, naxes, status)
  call ftptdm(unit, 10, naxis, naxes, status)
  call ftptdm(unit, 12, naxis, naxes, status)
  ! rl 2004-08-24 revert ot original config (aips++ test)
  call ftptdm(unit, 16, naxis, naxes, status)  ! TOTPOWER
  ! rl 2004-08-24       naxis = 2
  ! rl 2004-08-24       naxes(1) = r_nfeed
  ! rl 2004-08-24       naxes(2) = nsubant
  ! rl 2004-08-24       call ftptdm(unit, 16, naxis, naxes, status) ! TOTPOWER
  naxis = 3
  naxes(1) = 1
  naxes(2) = r_lband
  naxes(3) = nsubant
  call ftptdm(unit, 19, naxis, naxes, status)
  ! Other header keywords
  call ftpkys(unit,'TABLEREV','v1.0 2001-07-03',   &
    extname//' release',status)
  call ftpkys(unit,'TELESCOP',r_teles,   &
    'Telescope name',status)
  call write_fits_dobs(unit,error)
  if (error) return
  !      CALL FTPKYJ(UNIT,'SCAN-NUM',R_SCAN,'Scan number',STATUS)
  !      CALL FTPKYJ(UNIT,'OBS-NUM',R_NUM,'Observation number',STATUS)
  !?
  !      CALL JJDATE (R_DOBS+2460549.5D0+R_UT/PI/2,DATE)
  !      CALL FTTM2S(DATE(1),DATE(2),DATE(3),DATE(4),DATE(5),
  !     $DATE(6)+DATE(7)*0.001D0,
  !     $3,CHAIN,STATUS)
  !      CALL FTPKYS(UNIT,'DATE-OBS',CHAIN,'Date',STATUS)
  !      call ftpkyd(unit,'DATE-OBS',r_dobs+2460549.5d0,mdigit2,'Date',
  !     &     status)
  !      call ftpkyd(unit,'UT',r_ut*43200/pi,mdigit2,
  !     &     'Universal time (start)', status)
  call ftpkys(unit,'TIMESYS','UTC','Time system',status)
  call ftpkyd(unit,'LST',r_st*43200/pi,mdigit2,   &
    'Apparent sidereal time (start)',  status)
  !
  ! r_cproc will be  put into FITS OBSMODE keyword:
  ! CALIBRATION
  if (r_cproc.eq.'CALI') then
    if (r_scaty.eq.5) then
      r_cproc = 'CSKY'
    elseif (r_scaty.eq.7) then
      r_cproc = 'COLD'
    elseif (r_scaty.eq.4) then
      r_cproc = 'CALI'
    endif
  ! SKYDIP
  elseif (r_cproc.eq.'SKYD') then
    if (r_scaty.eq.5) then
      r_cproc = 'SKYD'
    elseif (r_scaty.eq.4) then
      r_cproc = 'SKYC'
    endif
  ! FOCUS
  elseif (r_cproc.eq.'FOCU') then
    if (r_scaty.eq.1) then
      r_cproc = 'FOCU'
    elseif (r_scaty.eq.11) then
      r_cproc = 'FOCX'
    elseif (r_scaty.eq.12) then
      r_cproc = 'FOCY'
    endif
  ! (POINTING)
  endif
  !
  call ftpkys(unit,'OBSMODE',r_cproc,'Scan type',status)
  call ftpkys(unit,'PROJID',r_project,'Project ID',status)
  call ftpkyd(unit,'AZIMUTH',r_az*180d0/pi,mdigit2,   &
    'Azimuth (degrees)',status)
  call ftpkyd(unit,'ELEVATIO',r_el*180d0/pi,mdigit2,   &
    'Elevation (degrees)',status)
  !
  ! Offset system
  if (r_typof.eq.type_eq) then
    call ftpkys(unit,'LATSYS','RA---SIN',   &
      'type of latitude-like offsets',status)
    call ftpkys(unit,'LONGSYS','DEC--SIN',   &
      'type of longitude-like offsets',status)
  elseif (r_typof.eq.type_ho) then
    call ftpkys(unit,'LATSYS','AZIM-SIN',   &
      'type of latitude-like offsets',status)
    call ftpkys(unit,'LONGSYS','ELEV-SIN',   &
      'type of longitude-like offsets',status)
  elseif (r_typof.eq.type_ga) then
    call ftpkys(unit,'LATSYS','GLON-SIN',   &
      'type of latitude-like offsets',status)
    call ftpkys(unit,'LONGSYS','GLAT-SIN',   &
      'type of longitude-like offsets',status)
  else
    call ftpkys(unit,'LATSYS','ELON-SIN',   &
      'type of latitude-like offsets',status)
    call ftpkys(unit,'LONGSYS','ELAT-SIN',   &
      'type of longitude-like offsets',status)
  endif
  call ftpkye(unit,'EXPOSURE',r_ndump*1.,mdigit1,   &
    'Total integration time (s)',status)
  call ftpkyj(unit,'NO_ANT',nsubant,'Number of antennas',status)
  call ftpkyj(unit,'NO_BAND',r_lband,'Number of basebands',status)
  call ftpkyj(unit,'NO_CHAN',r_lntch,'Total number of channels',   &
    status)
  call ftpkyj(unit,'NO_POL',r_npol,'Number of pols.',status)
  call ftpkyj(unit,'NO_FEED',r_nfeed,'Number of feeds.',status)
  call ftpkyj(unit,'NO_SIDE',r_nside,'Number of side bands',status)
  call ftpkyd(unit,'VFRAME',r_doppl*299792458d0,mdigit2,   &
    'radial vel. corr. (m/s)',status)
  call ftpkyj(unit,'NO_PHCOR',r_ndatl,'Number of phase corr. data',   &
    status)
  call ftpkyj(unit,'NO_CORR',ncorr ,   &
    'Number of CORRDATA Tables per baseband',status)
  call ftpkyj(unit,'NO_AUTO',nauto,   &
    'Number of AUTODATA Tables per baseband',status)
  call ftpkyj(unit,'NO_HOLO',nholo,   &
    'Number of HOLODATA Tables per baseband',status)
  if (switch) then
    call ftpkyj(unit,'NO_SWITC',nswitch,   &
      'Number of switch states in a switch cycle',status)
  endif
  call ftpkyj(unit,'FRONTEND',r_nrec,   &
    'number ID of front end used',status)
  !
  ! 05:54:28.500  44:38:02.000    Alt. :      2.560
  call ftpkyd(unit,'OBS-LAT',44.63389d0,mdigit2,   &
    'Observatory Latitude (degrees)',status)
  !?
  call ftpkyd(unit,'OBS-LONG',5.9079167d0,mdigit2,   &
    'Observatory Latitude (degrees)',status)
  call ftpkye(unit,'OBS-ELEV',2560.,mdigit1,   &
    'Observatory Elevation (m)',   &
    status)
  !
  ! Source information
  call ftpkys(unit,'SOURCE',r_sourc,'Source Name',status)
  call ftpkys(unit,'CALCODE',scode(r_itype),'Calibrator code',   &
    status)
  ! coordinate system
  if (r_typec.eq.type_eq) then
    if (r_epoch.eq.2000.) then
      call ftpkys(unit,'RADESYS','FK5',   &
        'Equatorial Coordinate system', status)
    elseif (r_epoch.eq.1950.) then
      call ftpkys(unit,'RADESYS','FK4',   &
        'Equatorial Coordinate system', status)
    endif
    call ftpkyd(unit,'RA',r_lam*180d0/pi,mdigit2,   &
      'Right Ascension (ICRF)',status)
    call ftpkyd(unit,'DEC',r_bet*180d0/pi,mdigit2,   &
      'Declination (ICRF)',status)
    call ftpkyd(unit,'PMRA',0d0,mdigit2,   &
      'RA Pr.Motion  (deg./Jul.y.) [dRA*cos(Dec)/dt]',status)
    call ftpkyd(unit,'PMDEC',0d0,mdigit2,   &
      'DEC Pr.Motion  (deg./Jul.y.)',status)
    call ftpkye(unit,'EQUINOX',r_epoch,mdigit1,'Equinox',   &
      status)
  elseif (r_typec.eq.type_ga) then
    call ftpkyd(unit,'GLON',r_lam*180d0/pi,mdigit2,   &
      'Galactic Longitude (deg.)',status)
    call ftpkyd(unit,'GLAT',r_bet*180d0/pi,mdigit2,   &
      'Galactic Latitude (deg.)',status)
  elseif (r_typec.eq.type_ho) then
    call ftpkyd(unit,'AZIM-FIX',r_lam*180d0/pi,mdigit2,   &
      'Fixed Azimuth (deg.)',status)
    call ftpkyd(unit,'ELEV-FIX',r_bet*180d0/pi,mdigit2,   &
      'Fixed Elevation (deg.)',status)
  else
    call ftpkyd(unit,'ELON',r_lam*180d0/pi,mdigit2,   &
      ' Longitude (deg.)',status)
    call ftpkyd(unit,'ELAT',r_bet*180d0/pi,mdigit2,   &
      'Latitude (deg.)',status)
  endif
  call ftpkys(unit,'VELTYP',vcode(r_typev),'Velocity type',status)
  !
  ! Default frequency information
  call ftpkyd(unit,'FREQLO1',r_flo1*1d6,mdigit2,   &
    'LO1 frequency (Hz)',status)
  call ftpkyj(unit,'SIDEBLO1',r_isb,'LO1 side band',status)
  call ftpkyd(unit,'FREQLO2',r_flo1_ref*1d6,mdigit2,   &
    'LO2 frequency (Hz)',status)
  call ftpkyj(unit,'SIDEBLO2',-1,'LO2 side band',status)
  call ftpkyd(unit,'INTERFRE',350d6,mdigit2,   &
    '2nd IF frequency (Hz)',status)
  call ftpkyd(unit,'FREQRES',600d6,mdigit2,   &
    'Total power bandwidth (Hz)',status)
  !
  ! Astrometry ...
  ! Note these are not real values, as we do not keep track of them in CLIC
  !
  call ftpkyd(unit,'UT1UTC',0.380901d0,mdigit2,'UT1-UTC', status)
  call ftpkyd(unit,'TAIUTC',32.d0,mdigit2,'TAI-UTC', status)
  call ftpkyd(unit,'POLARX',0.000103d0,mdigit2,   &
    'x coordinate of North Pole', status)
  call ftpkyd(unit,'POLARY',0.000107d0,mdigit2,   &
    'x coordinate of North Pole', status)
  !
  if (status .gt. 0) goto 99
  do ir=1, nrows
    if (switch) then
      jr = (ir+1)/2
      isw = ir+2-2*jr
      ! add 120" in lambda for OFF state (in degrees)
      if (isw.eq.2) then
        dlam = 240./3600.
      else
        dlam = 0.
      endif
      dtot = 10.
    else
      jr = ir
      isw = 0
      dlam = 0.
      dtot = 0.
    endif
    k = 1+ h_offset(jr)
    call decode_header (data(k))
    !
    ! Fill in the columns.
    ! INTEGNUM
    if (switch) then
      call ftpclj(unit,1,ir,1,1,ir,status)
    else
      call ftpclj(unit,1,ir,1,1,dh_dump,status)
    endif
    if (status .gt. 0) goto 99
    ! INTEGTIM
    if (switch) then
      call ftpcle(unit,2,ir,1,1,dh_integ/2.,status)
    else
      call ftpcle(unit,2,ir,1,1,dh_integ,status)
    endif
    if (status.gt.0) then
      call fio_printerror('WRITE_FITS_DATAPAR','FTPCLE',   &
        status,error)
      return
    endif
    ! MJD
    ! MJD = JD - 2400000.5
    call ftpcld(unit,3,ir,1,1,   &
      dh_obs+60549.0d0+dh_utc/86400d0,status)
    if (status .gt. 0) goto 99
    ! UUVVWW
    sind = dh_svec(3)
    cosd = sqrt(1.d0-sind**2)
    if (cosd.ne.0.d0) then
      sinh = -dh_svec(2) / cosd
      cosh = dh_svec(1) / cosd
    endif
    ! recompute by antenna (in CLIC only by baseline).
    felem = 1
    do isub = 1, nsubant
      i = subant(isub)
      uvw(1) = (r_ant(1,i)*sinh + r_ant(2,i)*cosh)
      uvw(2) = (-r_ant(1,i)*cosh + r_ant(2,i)*sinh)*sind   &
        + r_ant(3,i)*cosd
      uvw(3) = (r_ant(1,i)*cosh - r_ant(2,i)*sinh)*cosd   &
        + r_ant(3,i)*sind
      call ftpcld(unit,4,ir,felem,3,uvw,status)
      felem = felem+3
      if (status .gt. 0) then
        goto 99
      endif
    enddo
    ! AZELERR
    do isub = 1, nsubant
      i = subant(isub)
      do k=1, 2
        t(k) = dh_rmspe(k,i)/3600.
      enddo
      call ftpcle(unit,5,ir,1+2*(isub-1),2,t,status)
      if (status .gt. 0) goto 99
    enddo
    ! SOURDIR
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcld(unit,6,ir,1+3*(isub-1),3,dh_svec,status)
      if (status .gt. 0) goto 99
    enddo
    ! DELAYGEO ...
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcld(unit,7,ir,isub,1,dh_delayc(i)*1d-9,status)
    enddo
    if (status .gt. 0) goto 99
    ! DELAYOFF ...
    do isub = 1, nsubant
      i = subant(isub)
      do j=1, r_lband
        call ftpcld(unit,8,ir,(isub-1)*r_lband+j,1,   &
          dh_delay(1,i)*1d-9,status)
      enddo
    enddo
    if (status .gt. 0) goto 99
    ! PHASEGEO ...
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcld(unit,9,ir,isub,1,2d0*pi*dh_phasec(i),status)
      if (status .gt. 0) goto 99
    enddo
    ! PHASEOFF ...
    do isub = 1, nsubant
      i = subant(isub)
      do j=1, r_lband
        call ftpcld(unit,10,ir,(isub-1)*r_lband+j,1,   &
          2d0*pi*dh_phase(i),status)
      enddo
    enddo
    if (status .gt. 0) goto 99
    ! RATEGEO ...
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcld(unit,11,ir,isub,1,1d0*dh_ratec(i),status)
      if (status .gt. 0) goto 99
    enddo
    ! RATEOFF ...
    do isub = 1, nsubant
      i = subant(isub)
      do j=1, r_lband
        call ftpcld(unit,12,ir,(isub-1)*r_lband+j,1,   &
          0d0,status)
      enddo
    enddo
    if (status .gt. 0) goto 99
    ! FOCUSOFF
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcle(unit,13,ir,isub,1,dh_offfoc(i),status)
      if (status .gt. 0) goto 99
    enddo
    ! LONGOFF
    if (r_proc.ne.p_point) then
      rrlamof = r_lamof/pi*180.
    else
      rrlamof = 0
    endif
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcle(unit,14,ir,isub,1,   &
        rrlamof+dh_offlam(i)/3600.+dlam, status)
    enddo
    if (status .gt. 0) goto 99
    ! LATOFF
    if (r_proc.ne.p_point) then
      rrbetof = r_betof/pi*180.
    else
      rrbetof = 0
    endif
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcle(unit,15,ir,isub,1,rrbetof+dh_offbet(i)/3600.,   &
        status)
    enddo
    if (status .gt. 0) goto 99
    ! TOTPOWER
    do isub = 1, nsubant
      i = subant(isub)
      !     cread only the first polar for the time being...
      dtt = dh_total(1,i)
      if (isw.ne.0) then
        dist2 =   &
          (rrlamof+dh_offlam(i)/3600.+dlam-l0/3600.)**2 +   &
          (rrbetof+dh_offbet(i)/3600.-b0/3600.)**2
        dtt = dtt   &
          + dtot*exp(-dist2/((beamsize/3600./2.))**2*alog(2.))   &
          *exp(-((dh_offfoc(i)-f0)/2.)**2) + rangau(ntot)
      endif
      ! 2004-08-24
      do j=1, r_lband
        call ftpcle(unit,16,ir,(isub-1)+j, 1,   &
          dtt, status)
      enddo
    ! 2004-08-24            CALL FTPCLE(UNIT,16,IR,(ISUB-1)*R_nfeed+1,1,
    ! 2004-08-24     &           dtt,STATUS)
    ! simulate 2nd feed ...
    ! 2004-08-24            if (r_nfeed.gt.1) then
    ! 2004-08-24               CALL FTPCLE(UNIT,16,IR,(ISUB-1)*R_nfeed+2,1,
    ! 2004-08-24     &              dtt*1.23, STATUS)
    ! 2004-08-24            endif
    enddo
    if (status .gt. 0) goto 99
    ! WINDSPEE
    call ftpcle(unit,17,ir,1,1,r_avwind,status)
    if (status .gt. 0) goto 99
    ! WINDDIRE
    call ftpcle(unit,18,ir,1,1,r_avwindir,status)
    if (status .gt. 0) goto 99
    ! FLAG
    do isub = 1, nsubant
      i = subant(isub)
      ! flag is repeated for each subband as described in the format.
      do j=1, r_lband
        call ftpclj(unit,19,ir,(isub-1)*r_lband+j,1,   &
          dh_aflag(i),status)
      enddo
      !cc   CALL FTPCLJ(UNIT,19,IR,ISUB,1,DH_AFLAG(I),STATUS)
      if (status .gt. 0) goto 99
    enddo
    ! RDPOWER
    !         DO I=1, R_NANT
    do isub = 1, nsubant
      i = subant(isub)
      call ftpcle(unit,20,ir,isub,1,dh_test1(2,i),status)
    enddo
    if (status .gt. 0) goto 99
    ! CORR / AUTO /HOLO
    ! The Bure standard: continuum for all records,
    !                    continuum and line for the average record
    ! [only one column]
    if (holo) then
      datanum(1) = holo
      call ftpcll(unit,21,ir,1,1,datanum,status)
    else
      if (ir.le.r_ndump*nswitch) then
        datanum(1) = .true.
        datanum(2) = .false.
        datanum(3) = .false.
      else
        datanum(1) = .false.
        datanum(2) = .true.
        datanum(3) = .true.
      endif
      call ftpcll(unit,21,ir,1,3,datanum,status)
    endif
    if (status .gt. 0) goto 99
    ! SWITCH columns
    if (switch) then
      call ftpclj(unit,22,ir,1,1,isw,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR','FTPCLJ',   &
          status,error)
        return
      endif
      call ftpcle(unit,23,ir,1,1,wsw(isw),status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR','FTPCLE',   &
          status,error)
        return
      endif
    endif
  enddo
  return
  ! error return
99 error = .true.
  call printerror('WRITE_FITS_DATAPAR',status)
end subroutine write_fits_datapar
