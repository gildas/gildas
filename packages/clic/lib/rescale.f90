subroutine rescale(n,d,a)
  implicit none
  !---------------------------------------------------------------------
  ! Rescale array D dividing by A
  !---------------------------------------------------------------------
  integer :: n                      !
  real :: d(n)                      !
  real :: a                         !
  ! Local
  integer :: i
  !
  do i=1, n
    d(i) = d(i) / a
  enddo
end subroutine rescale
