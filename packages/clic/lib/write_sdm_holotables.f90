subroutine write_sdm_holoTables (ldata, data, error)
  !---------------------------------------------------------------------
  !     holodata: write SD holography data
  !
  !     nsubant, subant: number of antennas and antennas to use (for
  !     holography so far)
  !---------------------------------------------------------------------
  USE GILDAS_DEF
  !
  !     Global variables:
  INCLUDE 'clic_parameter.inc'
  INCLUDE 'clic_clic.inc'
  INCLUDE 'clic_display.inc'
  INCLUDE 'clic_par.inc'
  INCLUDE 'clic_dheader.inc'
  INCLUDE 'clic_constant.inc'
  INCLUDE 'clic_proc_par.inc'
  INCLUDE 'gbl_pi.inc'
  INCLUDE 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  !
  !     dummy variables :
  integer :: ldata, data(ldata)
  logical :: holodata, error
  !
  !     local variables :
  integer scanNumber, pointingSize, totalPowerSize
  integer k, kc, kpc, klu, klc,  kl, ir, h_offset, c_offset,   &
    l_offset
  character :: ch*12, uid*40, obsMode*256
  character :: dattim*20

  !---------------------------------------------------------------------
  holodata = .true.
  r_scan = 1
  r_typec = type_ho
  call message(1,1,'Write_sdm_holoTables','Starting ...')
  ir = r_ndump+1
  k = 1+ h_offset(ir)
  call decode_header (data(k))

  !
  !     Define time_Interval:
  !     - the interval center in MJD nano seconds...
  !     - the interval length in MJD nanoseconds...
  !     !      dh_utc = nint(2*dh_utc)*0.5d0
  !      dh_utc = nint(dh_utc/.001d0)*0.001d0
  !     print *, (dh_obs+60549.0d0)*86400.d0, dh_utc, dh_integ
  !      time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-0.5d0
  !     &     *dh_integ)*1d9
  !      time_interval(2) = dh_integ*1d9
  !      print *, ' holotables time_interval ',time_interval, dh_utc
  !     print *, time_interval
  !
  !     --------------------------------------------------------------------------
  !     tables that change during one Bure observation : loop on data
  !     header parameters.


  !     c    Tables for the average record (= ASDM INtegration), uncorrected
  klu = 1 + l_offset(r_ndump+1)
  !
  !     for corrected spectral data:
  klc = 1 + l_offset(r_ndump+2)
  !
  !     for uncorrected channel average data:
  kc = 1 + c_offset(r_ndump+1)
  !
  !
  !     TotalPower Table -
  !
  !     No TotalPower for the integration record as we have a
  !     single ConfigDescriptionId for Total power.
  !     except for holography data...
  !
  !
  !     Loop on records (NOW = ASDM integrations)
  do ir=1, r_ndump
    write (ch,'(i4.0,1h/,i4.0)') ir, r_ndump
    call message(1,1,'Write_sdm_holoTables','Record '//ch)
    k = 1+ h_offset(ir)
    call decode_header (data(k))
    if (error) return
    call get_time_interval
    if ((ir.lt.10).or.((r_ndump -ir) .lt.10)) then
      !print *, dh_obs, dh_integ, dh_utc
      !print *, ir, time_interval
    endif
    !
    !     Define timeInterval:
    !     - the interval start in MJD nanoseconds...
    !     - the interval length in MJD nanoseconds...
    !
    !     force the times to be multiples of 1ms as dh_utc has not enough
    !     precision
    !
!!!     dh_utc = nint(dh_utc/.001d0)*0.001d0
!!!     !     print *, (dh_obs+60549.0d0)*86400., dh_utc, dh_integ
!!!     time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-dh_integ   &
!!!       /2)*1d9
!!!     time_interval(1) = time_interval(1) + 33.*1d9
!!!     time_interval(2) = dh_integ*1d9
!!!     if (time_start(1).lt.0) then
!!!       time_start(1) = time_interval(1)
!!!       time_start(2) = -1
!!!     endif
    !     Tables for the individual records (= ASDM SubIntegration),
    !     uncorrected channel average data:
    kc = 1 + c_offset(ir)
    !     tables with timeInterval as a key cannot be used for the 'average
    !     record' of CLIC. So they must be written here.
    if (ir.eq.1) then
      !     deal with scan/subscan  numbers.
      if (r_scan.ne.scan_Number) then
        if (scan_Number.ge.0) then
          call write_sdm_scan(error)
          if (error) return
        endif
        subScan_Number = 1
        num_subscan = 1
        scan_startTime = time_Interval(1)
        scan_endTime = time_Interval(1)+time_Interval(2)
      else
        num_subscan = num_subscan + 1
        subScan_Number = subScan_Number + 1
        scan_endTime = time_Interval(1)+time_Interval(2)
      endif
      !print *, 'scan_startTime, scan_endTime'
      !print *, scan_startTime, scan_endTime
      scan_Number = r_scan
      call set_intent(scan_Intent, subScan_Intent, obsMode, data_type, holodata)
      !     subscan_startTime = time_Interval(1)
      !     subscan_endTime = time_Interval(1)+time_Interval(2)
      !
      !     tables that hold parameters unchanged during one Bure observation
      !     each call returns the pointers in the last item in the call list
      !
      !     --- Hardware setup information:
      !
      !     Station table ++ + 90
      call write_sdm_Station (error)
      if (error) return
      !
      !     Antenna Table ++ +  90
      call write_sdm_Antenna (error)
      if (error) return
      !
      !     FocusModel Table ++ Not implemented
      !$$$  call write_sdm_FocusModel(error)
      !$$$  if (error) return
      !
      !     PointingModel Table ++ (TBD)  +
      !$$$  call write_sdm_PointingModel(error)
      !$$$  if (error) return
      !
      !     SpectralWindow Tables (line and continuum) ++ + 90
      call write_sdm_SpectralWindow(holodata, error)
      if (error) return
      !
      !     Polarization Table ++ + 90
      !
      !     For holography, we write this in polarization table for the time
      !     being.
      !$$$
      !$$$            call write_sdm_Polarization(error)
      !$$$            if (error) return
      call write_sdm_holography(error)
      if (error) return
      !
      !     DataDescription Table ++  + 90
      call write_sdm_DataDescription (holodata, error)
      if (error) return
      !
      !     CorrelatorMode table ++ + 90
      if (.not. holodata) then
        call write_sdm_CorrelatorMode(error)
        if (error) return
      else
        call write_sdm_SquareLawDetector(error)
        if (error) return
      endif
      !
      !     Processor Table ++  + 90
      call write_sdm_Processor(holodata, error)
      if (error) return
      !
      !     Receiver Table ++  + 90
      call write_sdm_Receiver(holodata, error)
      if (error) return
      !
      !     Beam Table ++
      !     call write_sdm_Beam (error)
      !     if (error) return
      beam_Id = 0              ! instead...
      !
      !     Feed Table ++  + 90
      call write_sdm_Feed(holodata, error)
      if (error) return
      !
      !     FreqOffset Table ++  + 90
      call write_sdm_FreqOffset(holodata, error)
      if (error) return
      !
      !     SwitchCycle Table ++  + 90
      call write_sdm_SwitchCycle(error)
      if (error) return
      !
      !     ConfigDescription Table ++  + 90
      !     2nd arg is for Tp in the main table...
      call write_sdm_ConfigDescription(holodata, .false., error)
      if (error) return
      !
      !     --- Source information:
      !
      !     Source Table ++  + 90
      call write_sdm_Source(holodata, error)
      if (error) return
      !
      !     Source Parameter Table (??) ++  +
      !cc   call write_sdm_SourceParameter(error)
      if (error) return
      !
      !     Doppler Table ++  + 90
      if (.not. holodata) then
        call write_sdm_Doppler (error)
        if (error) return
      endif
      !
      !     Ephemeris Table ++  +
      !     c      call write_sdm_Ephemeris(error)
      !     c      if (error) return
      !
      !     Field Table ++   + 90
      call write_sdm_Field(error)
      if (error) return
      !
      !     ExecBlock Table ++  + 90
      call write_sdm_ExecBlock(error)
      if (error) return
      !     Seeing Table (deprecated)
      !
      !     SquareLawDetector Table (??)
      !
      !     Weather Table ++  + 90
      call write_sdm_Weather(error)
      if (error) return
      !
      !     State Table ++ + 90
      call write_sdm_State(error)
      if (error) return
      !     SBSummary Table (not needed?)
      !     Scan and subscan Tables ++  + 90
      call write_sdm_subScan(holodata, error)
      if (error) return
      !
      !     History Table (irrelevant?) ++  + 90
      call write_sdm_History(error)
      if (error) return
    endif
    !
    !     Pointing Table ++ + 90
    !!call write_sdm_Pointing(error)
    !!if (error) return
    !
    !     Focus Table ++ + 90
    call write_sdm_Focus(error)
    if (error) return
    !  print *, dh_obs, dh_integ, dh_utc

    !     State Table ++ (because subintegration) + 90
    call write_sdm_State(error)
    if (error) return
    !
    !     TotalPower Table  ++ +
    !$$$         if (ir.le.5) then
    !$$$            print *, 'ir, TotalPower, time_interval ',ir,
    !$$$     &           time_interval, dh_utc
    !$$$         endif
    !$$$         print *, 'WRITE_SDM_TOTPM ',holo_type
    call write_sdm_TotalPower(holodata, r_ldatc, data(kc)   &
      , error)
    if (error) return
  !     endif
  enddo
  
  call write_sdm_Pointing(ldata, data, error)
  if (error) return
  !$$$      call sic_date(dattim)
  !$$$      call getPointingTableSize(pointingSize, error)
  !$$$      call getTotalPowerTableSize(totalPowerSize, error)
  !$$$      print 1000, dattim, ' pointingSize ', pointingSize,
  !$$$     &     ' totalPowerSize ', totalPowerSize
  !$$$ 1000 format (a20, a, i10, a , i10)
  call message(1,1,'Write_sdm_tables','Ended.')
  return
  !
  !     error return
99 error = .true.
end subroutine write_sdm_holoTables
