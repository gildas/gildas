subroutine message(prio,sever,progra,line)
  use gkernel_interfaces
  use gbl_message
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC  Internal routine
  !
  !       Output message according to current priority levels, and
  !       set the severity information
  !---------------------------------------------------------------------
  integer :: prio                   !
  integer :: sever                  !
  character(len=*) :: progra        !
  character(len=*) :: line          !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: mfile, tprio, tfile, csev
  common /mesfil/ mfile, tprio, tfile, csev
  save /mesfil/
  character(len=1) :: severity(4)
  !      CHARACTER DATTIM*24
  integer :: nl,n0, n2, i, lin, sever2(4)
  parameter (lin=132)
  !
  data severity /    'I',   'W',   'E',   'F'/
  data sever2   /seve%i,seve%w,seve%e,seve%f/
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Severity correction
  csev = min(max(sever,1),4)
  !
  ! Line too long (>132) are splitted
  nl = lenc(line)
  n0 = lin-(14+lenc(progra))
  do i=1, nl, n0
    n2 = min(nl,i+n0-1)
    !
    ! If PRIO>"on-screen-prio", display message on screen
    if (tprio.le.prio) then
      write(6,100) severity(csev),progra,mod(r_scan,10000),line(i:n2)
    endif
    !
    ! If PRIO>"in-file-prio", write message in log
    if (tfile.le.prio .or. prio.eq.0) then
      call clic_message(sever2(csev),progra,line)
    endif
  enddo
  return
  !
100 format(a,'-',a,',[',i4.4,'] ',a)
101 format(1x,a,1x,i2,'-',a,'-',a,',[',i4.4,'] ',a)
end subroutine message
!
subroutine message_level(ip)
  !---------------------------------------------------------------------
  ! entry point to set priority level returns the previous value.
  !---------------------------------------------------------------------
  integer :: ip                     !
  ! Local
  integer :: mfile, tprio, tfile, csev, isave
  common /mesfil/ mfile, tprio, tfile, csev
  save /mesfil/
  isave = tprio
  tprio = ip
  ip = isave
end subroutine message_level
!
subroutine message_init(line,it,ip)
  !---------------------------------------------------------------------
  ! Init of priorities. Obsolescent (SB, 16-jun-2008)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  integer :: it                     !
  integer :: ip                     !
  ! Local
  integer :: mfile, tprio, tfile, csev
  common /mesfil/ mfile, tprio, tfile, csev
  save /mesfil/
  !
  tfile = ip
  tprio = it
end subroutine message_init
!
subroutine noir (cin,cout,n)
  !---------------------------------------------------------------------
  ! ASTRO	Utility routine
  ! 	Reduce all separators to one.
  ! Arguments :
  !	CIN	C*(*)	Input character string
  !	COUT	C*(*)	Output character string
  !	N	I	Length of COUT
  !---------------------------------------------------------------------
  character(len=*) :: cin           !
  character(len=*) :: cout          !
  integer :: n                      !
  ! Local
  integer :: i,m
  logical :: lo,li,nobl
  nobl=.false.
  !
1 m = len(cout)
  lo=.true.
  n=0
  !
  ! Reduit les Separateurs
  do i=1,len(cin)
    li=.false.
    if (cin(i:i).ne.' ' .and. cin(i:i).ne.'	') go to 10
    !
    ! C'est un blanc
    li=.true.
    ! Il en existe deja un contigu
    if (lo)  go to 100
    ! C'est le premier
    if (nobl) go to 100
    n = n+1
    cout(n:n) = ' '
    go to 100
10  n = n+1
    cout(n:n)=cin(i:i)
100 lo=li
    if (n.ge.m) return         ! Prevent overflow
  enddo
  !
  cout(n+1:)=' '
end subroutine noir
