! The side band average complex visibility values,
! and their associated weights.
! absolute values
      COMPLEX ZSBA(2,-MNANT:MNBAS)
      REAL WSBA(2,-MNANT:MNBAS)
! relative values: use as a reference for passband calibration.
      COMPLEX ZRSBA(MNBB,2,-MNANT:MNBAS)
      REAL WRSBA(MNBB,2,-MNANT:MNBAS)
!
      COMMON /SBA/ ZSBA, ZRSBA, WSBA, WRSBA
      SAVE /SBA/
