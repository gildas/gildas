subroutine fix (nfound,ifound,update,new_wanted,error)
  use gkernel_interfaces
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	search the index of the input data file
  !	for parameters in the common rindex.inc
  !	entry FOX does the same thing for the output data file
  !
  !	IFOUND is unused when UPDATE = .TRUE.
  !	NEW_WANTED = .TRUE. : wait for new data to be present in input file.
  !	(RL 05-oct-1987)
  !---------------------------------------------------------------------
  integer :: nfound                 !
  integer(kind=entry_length) :: ifound(1)              !
  logical :: update                 !
  logical :: new_wanted             !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_proc_par.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=10) :: nombre
  logical :: lire
  !
  real :: fourpi
  parameter (fourpi=4.0*pis)
  integer :: k, j, is, l, m
  integer(kind=entry_length) :: ix,inew,jnew
  integer*4 :: tver, td, lun, ier
  logical :: good, ok
  character(len=12) :: uctele, ucline, ucsour, ucproject
  !
  integer, save :: tpol(mnant,0:mnpolscan)
  data tpol /204*0/  
  !
  if (new_wanted) then
    call eix(inew,jnew,.false.,error)
    if (error) return
  else
    inew = 1
    jnew = i%desc%xnext-1
  endif
  if (i%desc%xnext.le.1) then
    nfound = 0
    return
  endif
  k = 0
  lire = fline .or. fsourc .or. fteles .or. fdobs .or. fdred   &
    .or. foff1 .or. foff2   &
    .or. fhoura .or. fproject   &
    .or. fbpc .or. fic .or. fut .or. fskip
  !
  lire = lire .or. xrecei.ge.0
  !
  do 10 ix = inew,jnew
    !
    ! General search in memory
    title%kind = ix_kind(ix)
    if (title%kind.lt.xkind .or. title%kind.gt.ykind) goto 10
    title%qual = ix_qual(ix)
    if (title%qual.gt.xqual) goto 10
    title%ver = ix_ver(ix)
    if (last) then
      if (title%ver.lt.0) goto 10
    endif
    title%num = ix_num(ix)
    title%bloc = ix_bloc(ix)
    title%word = ix_word(ix)
    if (fnum) then
      if (title%num.lt.xnum .or. title%num.gt.ynum) goto 10
    endif
    if (fver) then
      tver = abs(title%ver)
      if (tver.lt.xver .or. tver.gt.yver) goto 10
    endif
    if (fscan) then
      title%scan = ix_scan(ix)
      ok = .false.
      do j=1, xnscan
        if (xscan(j).le.yscan(j)) then
          ok = ok .or.   &
            ((title%scan.ge.xscan(j)).and.(title%scan.le.yscan(j)))
        !                  IF (LSCAN.LT.XSCAN .OR. LSCAN.GT.YSCAN) GOTO 10
        else
          ok = ok .or.   &
            ((title%scan.ge.xscan(j)).or.(title%scan.le.yscan(j)))
        !                  IF (LSCAN.LT.XSCAN .AND. LSCAN.GT.YSCAN) GOTO 10
        endif
      enddo
      if (.not.ok) goto 10
    endif
    if (fproc.or.fexcl) then
      title%proc = ix_proc(ix)
      if (fproc) then
        good = .false.
        do j=1,xnproc
          if (title%proc.eq.xsproc(j)) good = .true.
        enddo
      else
        good = .true.
      endif
      if (fexcl) then
        do j=1,xnexcl
          if (title%proc.eq.xsexcl(j)) good = .false.
        enddo
      endif
      if (.not.good) goto 10
    endif
    if (fitype) then
      if (ix_itype(ix).ne.xitype) goto 10
    endif
    !
    ! More restrictive search
    if (lire) then
      call rix(ix,error)
      if (error) return
      if (xrecei.ge.0) then
        if (title%recei.ne.xrecei) goto 10
      endif
      if (fsourc) then
        good = .false.
        call bytoch(title%sourc,ucsour,12)  ! UCSOUR = LCSOUR
        call sic_upper(ucsour)
        do j=1,xnsour
          is = xisour(j)
          if (ucsour(1:is).eq.xssour(j)(1:is)) good = .true.
        enddo
        if (.not.good) goto 10
      endif
      if (fline) then
        call bytoch(title%line,ucline,12)   ! UCLINE = LCLINE
        call sic_upper(ucline)
        if (ucline(1:iline).ne.xcline(1:iline)) goto 10
      endif
      if (fteles) then
        call bytoch(title%teles,uctele,12)  ! UCTELE = LCTELE
        call sic_upper(uctele)
        if (uctele(1:iteles).ne.xctele(1:iteles)) goto 10
      endif
      if (fproject) then
        call bytoch(title%project,ucproject,8) ! UCPROJECT = LCPROJECT
        if (ucproject(1:iproject).ne.   &
          xcproject(1:iproject)) goto 10
      endif
      if (fdobs) then
        td = mod(title%dobs+32768,65536)-32768
        if (td.lt.xdobs .or. td.gt.ydobs) goto 10
      endif
      if (fdred) then
        td = mod(title%dred+32768,65536)-32768
        if (td.lt.xdred .or. td.gt.ydred) goto 10
      endif
      if (foff1) then
        if (title%off1.lt.xoff1 .and. xoff1.ne.fourpi) goto 10
        if (title%off1.gt.yoff1 .and. yoff1.ne.fourpi) goto 10
      endif
      if (foff2) then
        if (title%off2.lt.xoff2 .and. xoff2.ne.fourpi) goto 10
        if (title%off2.gt.yoff2 .and. yoff2.ne.fourpi) goto 10
      endif
      if (fbpc) then
        if (title%bpc.ne.xbpc) goto 10
      endif
      if (fic) then
        if (title%ic.ne.xic) goto 10
      endif
      if (fhoura) then
        if (title%houra.lt.xhoura .or. title%houra.gt.yhoura) goto 10
      endif
      if (fut) then
        if (title%ut.lt.xut .or. title%ut.gt.yut) goto 10
      endif
    endif
    !
    ! Polar Test: Find Entries in the same mode. 
    if (fpol) then
      !
      ! Read configuration file if needed
      if (tpol(1,1).eq.0) then
        call read_polseq(tpol(1,1),mnant,mnpolscan,error)
        if (error) return
      endif
      ! Read the Line section, but also the Inter Conf section
      call robs(ix,error)
      if (error) goto 10
      call rgen(error)
      if (error) goto 10
      call rinterc(error)
      if (error) goto 10
      call rrfset(error)
      if (error) goto 10
      good = .false.
      do j=1,xnpol
        ok = .true.
        do l=1,r_nant
          m = r_kant(l)
          if (r_polswitch(l,1).ne.tpol(m,xpol(j))) ok=.false.
        enddo
        if (ok) good = .true.
      enddo
      if (.not.good) goto 10
    endif
    !
    ! Correlator mode: AUTO/CROSS
    if (fmod) then
      call robs(ix,error)
      if (error) goto 10
      call rgen(error)
      if (error) goto 10
      call rinterc(error)
      if (error) goto 10
      call rlineset(error)
      if (error) goto 10
      if (r_lmode.ne.xmod) goto 10
    endif
    !
    ! Special case: do not find auto in cross
    if (fskip) then
      if (title%proc.eq.p_source .or.  &
          title%proc.eq.p_gain   .or.  &
          title%proc.eq.p_delay  .or.  &
          title%proc.eq.p_focus  .or.  &
          title%proc.eq.p_point  .or.  &
          title%proc.eq.p_holog  .or.  &
          title%proc.eq.p_five   .or.  &
          title%proc.eq.p_flux   .or.  &
          title%proc.eq.p_vlbi   .or.  &
          title%proc.eq.p_vlbg   ) then
        call robs(ix,error)
        if (error) goto 10
        call rgen(error)
        if (error) goto 10
        call rinterc(error)
        if (error) goto 10
        call rlineset(error)
        if (error) goto 10
        if (r_lmode.eq.2) goto 10
      endif
    endif
    !
    ! Load
    if (fload) then
      call robs(ix,error)
      call rgen(error)
      if (error) goto 10
      call rinterc(error)
      if (error) goto 10
      call rscanning(error)
      if (error) goto 10
      if (r_scaty.eq.4.or.r_scaty.eq.7) then 
        if (r_scaty.ne.xload) goto 10
      else
        if(xload.ne.3) goto 10
      endif
    endif
    !
    k = k+1
    if (k.gt.nfound) go to 97
    if (update) then
      if (cxnext.gt.m_cx) then
        call message(8,3,'FIX','Current index is full')
        error = .true.
        return
      endif
      cx_ind(cxnext)  = ix
      cx_num(cxnext)  = title%num
      cx_ver(cxnext)  = title%ver
      cx_bloc(cxnext) = title%bloc
      cx_word(cxnext) = title%word
      cxnext = cxnext + 1
    else
      ifound(k) = ix
    endif
10 continue
  nfound = k
  !
  ! Reset search flags
  last = .true.
  fnum = .false.
  fver = .false.
  fline = .false.
  fsourc = .false.
  fteles = .false.
  fdobs = .false.
  fdred = .false.
  foff1 = .false.
  foff2 = .false.
  fproc = .false.
  fitype = .false.
  fhoura = .false.
  fut = .false.
  fproject = .false.
  fbpc = .false.
  fic = .false.
  fpol = .false.
  return
  !
  ! Error returns
97 error = .true.
  write(nombre,'(I6)') nfound
  call message(8,2,'FIX','More than '//nombre//   &
    ' observations found.')
  return
end subroutine fix
!
subroutine fix_bp (nfound,ifound,error)
  use gkernel_interfaces
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	search the index of the input data file
  !	for parameters in the common rindex.inc
  !	entry FOX does the same thing for the output data file
  !
  !---------------------------------------------------------------------
  integer :: nfound                 !
  integer(kind=entry_length) :: ifound(1)              !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_proc_par.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=10) :: nombre
  !
  integer :: k, j, is, l, m
  integer(kind=entry_length) :: ix,inew,jnew
  integer*4 :: tver, td
  logical :: ok
  !
  inew = 1
  jnew = i%desc%xnext-1
  !
  if (i%desc%xnext.le.1) then
    nfound = 0
    return
  endif
  k = 0
  !
  do ix = inew,jnew
    !
    ! General search in memory
    title%bloc = ix_bloc(ix)
    title%word = ix_word(ix)
    title%proc = ix_proc(ix)
    if (title%proc.ne.p_band) cycle
    if (fscan) then
      title%scan = ix_scan(ix)
      ok = .false.
      do j=1, xnscan
        if (xscan(j).le.yscan(j)) then
          ok = ok .or.   &
            ((title%scan.ge.xscan(j)).and.(title%scan.le.yscan(j)))
        else
          ok = ok .or.   &
            ((title%scan.ge.xscan(j)).or.(title%scan.le.yscan(j)))
        endif
      enddo
      if (.not.ok) cycle
    endif
    !
    k = k+1
    if (k.gt.nfound) go to 97
    ifound(k) = ix
  enddo
  nfound = k
  !
  return
  !
  ! Error returns
97 error = .true.
  write(nombre,'(I6)') nfound
  call message(8,2,'FIX_BP','More than '//nombre//   &
    ' observations found.')
  return
end subroutine fix_bp
!
subroutine fox(nfound,ifound,error)
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  ! RW	Same as FIX for output file
  !---------------------------------------------------------------------
  integer         :: nfound                 !
  integer (kind=entry_length):: ifound(1)              !
  logical         :: error                  !
  ! Local
  real :: iut
  character(len=10) :: nombre
  integer :: k, iver, j, iscan, irec 
  integer(kind=entry_length) :: ix, inum
  logical :: ok
  !
  if (o%desc%xnext.le.1) then
    nfound = 0
    return
  endif
  !
  k = 0
  do 10 ix=1,o%desc%xnext-1
    inum  = ox_num(ix)
    iver  = ox_ver(ix)
    iscan = ox_scan(ix)
    iut   = ox_ut(ix)
    irec  = ox_rece(ix)
    if (last) then
      if (iver.lt.0) goto 10
    endif
    if (fnum) then
      if (inum.lt.xnum .or. inum.gt.ynum) goto 10
    endif
    if (fver) then
      iver = abs(iver)
      if (iver.lt.xver .or. iver.gt.yver) goto 10
    endif
    if (fscan) then
        ok = .false.
        do j=1, xnscan
          if (xscan(j).le.yscan(j)) then
            ok = ok .or.   &
              ((iscan.ge.xscan(j)).and.(iscan.le.yscan(j)))
          else
            ok = ok .or.   &
              ((iscan.ge.xscan(j)).or.(iscan.le.yscan(j)))
          endif
        enddo
        if (.not.ok) goto 10
    endif
    if (fut) then
      if (iut.lt.xut .or. iut.gt.yut) goto 10
    endif
    if (xrecei.ge.0) then
      if (irec.ne.xrecei) goto 10
    endif
    k = k+1
    if (k.gt.nfound) goto 97
    ifound(k)=ix
10 continue
  nfound = k
  fnum = .false.
  fver = .false.
  last = .true.
  return
  !
97 error = .true.
  write(nombre,'(I6)') nfound
  call message(8,2,'FOX','More than '//nombre//   &
    ' observations found')
end subroutine fox
!
subroutine read_polseq(polseq,nant,nsub,error)
  use gkernel_interfaces
  integer :: nant
  integer :: nsub
  integer :: polseq(nant,nsub)
  logical :: error
  !
  character(len=256) :: polfile, chain
  integer :: ier, lun, j
  !
  polfile = 'pol_sequence'
  if (.not.sic_query_file(polfile,'data#dir:','.dat',polfile)) then
    call message(8,3,'FIX',polfile(1:lenc(polfile))// &
      ' not found')
    error = .true.
    return
  endif
  ier = sic_getlun(lun)
  if (ier.ne.1) then
    call message(8,3,'FIX','Problem getting LUN')
    error = .true.
    return
  endif
  open(unit=lun,file=polfile(1:lenc(polfile)),status='OLD',iostat=ier)
  if (ier.ne.0) then
    call message(8,3,'FIX','Cannot open file '//polfile(1:lenc(polfile)))
    error = .true.
    return
  endif
  read(lun,'(a)') chain
  do j=1, 16
    read(lun,*) polseq(:,j)
  enddo
  polseq = (polseq+1)/2
  close(unit=lun)
  call sic_frelun(lun)
  return
end subroutine read_polseq
