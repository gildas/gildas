subroutine set_pass (qsb,qband,qbt,qntch,pc,pl,error)
  use gildas_def
  use classic_api  
  use clic_bpc
  use clic_rdata
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Computes the passband calibration factors
  ! Baseline or Antenna based.
  !---------------------------------------------------------------------
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbt                    !
  integer :: qntch                  !
  complex :: pc(qband,qsb,qbt)      !
  complex :: pl(qntch,qsb,qbt)      !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  ! Local
  real*8 :: xx, yamp, ypha, aa(0:mbpcdeg), ap(0:mbpcdeg)
  real*8 :: ak1, ak2
  complex, allocatable :: apc(:,:,:), apl(:,:,:)
  ! number of channels
  integer :: nc, nl
  integer :: i, isb, ib, ic, k, it, ib1, ib2, ib3, ndeg, ia, ja
  integer :: inbc, j, isb1, isb2, iscan
  integer :: ier
  logical :: freq, same, pol, good
  character(len=256) :: chain
  character(len=12)  :: rf_chain
  character(len=*), parameter :: rname='SET_PASS'
  integer, save :: tpol(mnant,mnpolscan)
  data tpol /192*0/
  !
  !------------------------------------------------------------------------
  ! Code:
  pol = .false.
  iscan = 0
  allocate(apc(mcch,2,mnant),apl(mlch,2,mnant),stat=ier)
  if (ier.ne.0) then
    write(chain,'(a,i0)') 'Memory allocation error', ier 
    call message(6,3,'SET_PASS',chain(1:lenc(chain)))
    error = .true.
    return
  endif
  if (do_pass_spectrum) then
    if (any(r_polswitch(1:r_nant,1).ne.0)) then
      pol = .true.
      if (tpol(1,1).eq.0) then
        call read_polseq(tpol,mnant,mnpolscan,error)
        if (error) return
      endif
      iscan = 0
      do i=1, mnpolscan
        good = .true.
        do j=1, r_nant
          k = r_kant(j)
          if (r_polswitch(j,1).ne.tpol(k,i)) good = .false.
        enddo
        if (.not.good) cycle
        iscan = i
        exit
      enddo
      if (iscan.eq.0) then
        call message(8,3,'SET_PASS','Could not determine polarisation subscan')
        error = .true.
        return
      endif
    endif
    if (do_pass_memory) then
      if (bp_spectrum_memory.and.(bp_memory_origin.eq.bp_memory_spec).and.   &
          (bp_memory_antenna.eqv.do_pass_antenna)) return
      if (bp_spectrum_ready.and.(bp_memory_antenna.eqv.do_pass_antenna)) then
         call set_spectrum_memory(qsb,qband,qbt,qntch,pc,pl,specc,specl,error)
         if (error) return
         call message(1,1,'SET_PASS', 'Spectrum RF solution from memory')
         bp_spectrum_memory = .true.
         bp_memory_origin = bp_memory_spec
      else
         call message(6,2,'SET_PASS', &
           'No spectrum in memory')
         error = .true.
         return
      endif
    else
      if (pol) then
        if (bp_spectrum_memory_pol(iscan).and. &
            bp_memory_origin_pol(iscan).eq.bp_memory_file) then
          call c4toc4(cpassc(1,iscan),passc,mdatac)
          call c4toc4(cpassl(1,iscan),passl,mdatal)
          bp_spectrum_memory = .false.
          !! get solution from memory
          return
        endif
      else
        if (bp_spectrum_memory.and.bp_memory_origin.eq.bp_memory_file) return
      endif
      call get_pass_scan(rf_chain,iscan,error)
      if (error) return
      call message(1,1,'SET_PASS', 'Getting RF solution from file')
      if (pol) then
        !! store solution in memory as well
        if (.not.allocated(cpassc)) then
          allocate(cpassc(mdatac,mnpolscan), cpassl(mdatal,mnpolscan),stat=ier)
          if (failed_allocate(rname,'Buffers 49',ier,error))  return
        endif
        call c4toc4(passc,cpassc(1,iscan),mdatac)
        call c4toc4(passl,cpassl(1,iscan),mdatal)
        bp_spectrum_memory_pol(iscan) = .true.
        bp_memory_origin_pol(iscan) = bp_memory_file
      else
        bp_spectrum_memory = .true.
        bp_memory_origin = bp_memory_file
      endif
    endif
    return
  endif
  if (do_pass_memory) then
    if (do_pass_antenna) then
      call  sub_store_pass_ant (.false.,error)
      if (error) goto 999
    else
      call  sub_store_pass (.false.,error)
      if (error) goto 999
    endif
  elseif (do_pass_antenna .and. iand(r_abpc,1).eq.0) then
    call message(6,2,'SET_PASS',   &
      'No Antenna RF Passband stored with the data')
    return
  elseif (.not.do_pass_antenna .and. iand(r_bpc,1).eq.0) then
    call message(6,2,'SET_PASS',   &
      'No Baseline RF Passband stored with the data')
    return
  endif
  freq = do_pass_freq
  nc = r_nband
  nl = r_lband
  !
  ! Check if solution was derived from same coefficients
  call check_coefficients_solution(do_pass_antenna,do_pass_freq,same)
  if (same.and.bp_memory_origin.eq.bp_memory_freq) then
    return
  else
    call message(1,1,'SET_PASS', 'Computing RF solution')
  endif
  ! Antenna based
  if (do_pass_antenna) then
    do i=1, nc             ! loop on continuun subbands
      ! Number of sidebands.
      if (r_lnsb.eq.1) then
        isb1 = r_sband(1,i)
        isb2 = isb1
      else 
        isb1 = 1
        isb2 = 2 
      endif
      !
      do isb=isb1, isb2
        do ia = 1, r_nant
          !
          ! Find NBC entry
          inbc = r_bb(i)
          !
          if (freq) then
            do j=0, r_abpfdeg
              aa(j) = r_abpfamp(inbc,isb,ia,j)
              ap(j) = r_abpfpha(inbc,isb,ia,j)
            enddo
            ak1 = r_abpflim(inbc,1)
            ak2 = r_abpflim(inbc,2)
            ndeg = r_abpfdeg
          endif
          !
          ! Continuum, by channel
          if (.not.freq) then
            yamp = r_abpccamp(inbc,isb,ia,i)
            ypha = r_abpccpha(inbc,isb,ia,i)
          ! Antenna-based calibration is done on log: default value
          ! should be zero, not 1
          !                     IF (YAMP.EQ.0) YAMP = 1.0
          ! Continuum, by frequency
          else
            xx = r_cfcen(i)
            xx = (2*xx-ak1-ak2)/(ak2-ak1)
            xx = min(1d0,max(-1d0,xx))
            call mth_getpol('SET_PASS',   &
              ndeg+1, aa, xx, yamp, error)
            if (error) goto 999
            call mth_getpol('SET_PASS',   &
              ndeg+1, ap, xx, ypha, error)
            if (error) goto 999
          endif
          apc(i,isb,ia) = exp(cmplx(-yamp,-ypha))
        enddo
        do ib=1, r_nbas
          ia = r_iant(ib)
          ja = r_jant(ib)
          pc(i,isb,ib) =   &
            apc(i,isb,ja)*conjg(apc(i,isb,ia))
        enddo
      enddo
    enddo
    !
    ! Line, by channel or frequency
    ! initialise, because of the unassigned channels due to possible
    ! narrow-band channels ...
    do ia = 1, r_nant
      do isb =1, 2
        do i=1, qntch
          apl(i,isb,ia) = cmplx(1.0,0.0)
        enddo
      enddo
    enddo
    k = 1
    do ic=1, nl
      ! Number of sidebands.
      if (r_lnsb.eq.1) then
        isb1 = r_lsband(1,ic)
        isb2 = isb1
      else 
        isb1 = 1
        isb2 = 2 
      endif
      !
      !
      inbc = r_bb(ic)
      !
      do isb=isb1, isb2
        do ia=1, r_nant
          k = r_lich(ic)+1
          if (freq) then
            do j=0, r_abpfdeg
              aa(j) = r_abpfamp(inbc,isb,ia,j)
              ap(j) = r_abpfpha(inbc,isb,ia,j)
            enddo
            ak1 = r_abpflim(inbc,1)
            ak2 = r_abpflim(inbc,2)
            ndeg = r_abpfdeg
          endif
          !
          if (.not.freq) then
            do i=0, r_abpcdeg
              aa(i) = r_abpclamp(inbc,isb,ia,ic,i)
              ap(i) = r_abpclpha(inbc,isb,ia,ic,i)
            enddo
            ak1 = k
            ak2 = k+r_lnch(ic)-1
            ndeg = r_abpcdeg
          endif
          do i=1, r_lnch(ic)
            if (freq) then
              xx = r_lfcen(ic) + (i-r_lcench(ic))*r_lfres(ic)
              xx = (2*xx-ak1-ak2)/(ak2-ak1)
            else
              xx= (2*k-ak1-ak2)/(ak2-ak1)
            endif
            xx = min(1d0,max(-1d0,xx))
            call mth_getpol('SET_PASS',   &
              ndeg+1, aa, xx, yamp, error)
            if (error) goto 999
            call mth_getpol('SET_PASS',   &
              ndeg+1, ap, xx, ypha, error)
            if (error) goto 999
            if (abs(yamp).lt.40) then
              apl(k,isb,ia) = exp(cmplx(-yamp,-ypha))
            else
              apl(k,isb,ia) =  blankc
            endif
            k = k + 1
          enddo
        enddo
        do ib = 1, r_nbas
          ia = r_iant(ib)
          ja = r_jant(ib)
          do i = r_lich(ic)+1, r_lich(ic)+r_lnch(ic)
            if ((apl(i,isb,ja).eq.blankc).or.apl(i,isb,ia).eq.blankc) then
              pl(i,isb,ib) =  blankc
            else
              pl(i,isb,ib) =   &
                apl(i,isb,ja)*conjg(apl(i,isb,ia))
            endif
          enddo
        enddo
      enddo
    enddo
  bp_memory_antenna = .true.
  !
  ! Baseline based
  else
    do i=1, nc
      !
      inbc = r_bb(i)
      ! Number of sidebands.
      if (r_lnsb.eq.1) then
        isb1 = r_lsband(1,i)
        isb2 = isb1
      else 
        isb1 = 1
        isb2 = 2
      endif
      !
      do isb=isb1, isb2
        do ib = 1, r_nbas
          if (freq) then
            do j=0, r_bpfdeg   ! default to continuum polyn.
              aa(j) = r_bpfamp(inbc,isb,ib,j)
              ap(j) = r_bpfpha(inbc,isb,ib,j)
            enddo
            ak1 = r_bpflim(inbc,1)
            ak2 = r_bpflim(inbc,2)
            ndeg = r_bpfdeg
          endif
          !
          ! Continuum, by channel
          if (.not.freq) then
            yamp = r_bpccamp(inbc,isb,ib,i)
            ypha = r_bpccpha(inbc,isb,ib,i)
            if (yamp.eq.0) yamp = 1.0
            if (yamp.ne.0) then
              pc(i,isb,ib) = 1./yamp*exp(cmplx(0.d0,-ypha))
            else
              pc(i,isb,ib) = 1.0
            endif
          ! Continuum, by frequency
          else
            xx = r_cfcen(i)
            xx = (2*xx-ak1-ak2)/(ak2-ak1)
            xx = min(1d0,max(-1d0,xx))
            call mth_getpol('SET_PASS',   &
              ndeg+1, aa, xx, yamp, error)
            if (error) goto 999
            call mth_getpol('SET_PASS',   &
              ndeg+1, ap, xx, ypha, error)
            if (error) goto 999
          endif
          if (yamp.ne.0) then
            pc(i,isb,ib) = 1./yamp*exp(cmplx(0.d0,-ypha))
          else
            pc(i,isb,ib) = 1.0
          endif
        enddo
     enddo
   enddo 
   !
   ! Line, by channel or frequency
   k=1
   do ic=1, nl
     !
     inbc = r_bb(ic)
     ! Number of sidebands.
     if (r_lnsb.eq.1) then
       isb1 = r_lsband(1,ic)
       isb2 = isb1
     else 
       isb1 = 1
       isb2 = 2
     endif
     do isb = isb1, isb2
       do ib =1, r_nbas
          if (freq) then
            do j=0, r_bpfdeg   ! default to continuum polyn.
              aa(j) = r_bpfamp(inbc,isb,ib,j)
              ap(j) = r_bpfpha(inbc,isb,ib,j)
            enddo
            ak1 = r_bpflim(inbc,1)
            ak2 = r_bpflim(inbc,2)
            ndeg = r_bpfdeg
          endif
          !
          if (k .lt. r_lich(ic)+1) then
            do i=k, r_lich(ic)
              pl(i,isb,ib) = cmplx(1.0,0.0)
            enddo
            k = r_lich(ic)+1
          endif
          if (k .gt. r_lich(ic)+r_lnch(ic)) k = r_lich(ic)+1
          if (.not.freq) then
            do i=0, r_bpcdeg
              aa(i) = r_bpclamp(inbc,isb,ib,ic,i)
              ap(i) = r_bpclpha(inbc,isb,ib,ic,i)
            enddo
            ak1 = k
            ak2 = k+r_lnch(ic)-1
            ndeg = r_bpcdeg
          endif
          do i=1, r_lnch(ic)
            if (freq) then
              xx = r_lfcen(ic) + (i-r_lcench(ic))*r_lfres(ic)
              xx = (2*xx-ak1-ak2)/(ak2-ak1)
            else
              xx= (2*k-ak1-ak2)/(ak2-ak1)
            endif
            xx = min(1d0,max(-1d0,xx))
            call mth_getpol('SET_PASS',   &
              ndeg+1, aa, xx, yamp, error)
            if (error) goto 999
            call mth_getpol('SET_PASS',   &
              ndeg+1, ap, xx, ypha, error)
            if (error) goto 999
            if (yamp.ne.0) then
              pl(k,isb,ib) = 1d0/yamp*exp(cmplx(0d0,-ypha))
            else
              pl(k,isb,ib) = blankc
            endif
            k = k + 1
          enddo
        enddo
      enddo
    enddo
  bp_memory_antenna = .false.
  !
  endif
  bp_spectrum_memory = .true.
  bp_memory_origin = bp_memory_freq
  !
  ! Store coefficients used to compute solution
  call store_coefficients_solution(do_pass_antenna,do_pass_freq)
  !
  ! Compute also for triangles, if any
  if (r_ntri.le.0) return
  do it = 1, r_ntri
    ib1 = bastri(1,it)
    ib2 = bastri(2,it)
    ib3 = bastri(3,it)
    !
    ! Number of sidebands.
    do ic=1, nc
      if (r_lnsb.eq.1) then
        isb1 = r_lsband(1,ic)
        isb2 = isb1
      else 
        isb1 = 1
        isb2 = 2
      endif
      !
      do isb=isb1, isb2
        pc(i,isb,r_nbas+it) = pc(i,isb,ib1)   &
          * conjg(pc(i,isb,ib2)) * pc(i,isb,ib3)
      enddo
    enddo
    do ic=1, nl
      if (r_lnsb.eq.1) then
        isb1 = r_lsband(1,ic)
        isb2 = isb1
      else 
        isb1 = 1
        isb2 = 2
      endif
      !
      do isb=isb1, isb2
        do i=r_lich(ic)+1, r_lich(ic)+r_lnch(ic)
          pl(i,isb,r_nbas+it) = pl(i,isb,ib1)   &
            * conjg(pl(i,isb,ib2)) * pl(i,isb,ib3)
        enddo
      enddo
    enddo
  enddo
  return
999 error = .true.
  return
end subroutine set_pass
!
subroutine store_coefficients_solution(antenna,freq)
  use clic_rf_solution
  !
  logical  :: antenna
  logical  :: freq
  !
  include 'clic_par.inc'
  !
  integer :: ia,ibb,ic,il,isb,isb1,isb2,j
  !
  rf%antenna = antenna
  rf%freq    = freq
  !
  if (antenna) then
    rf%ant%nant = r_nant
    rf%ant%nc   = r_nband
    rf%ant%nl   = r_lband
    rf%ant%nsb  = r_lnsb
    rf%ant%nbb  = r_nbb
    do il=1, rf%ant%nl
      rf%ant%isb(il) = r_lsband(1,il)
      rf%ant%ibb(il) = r_bb(il)
    enddo
    if (freq) then
      rf%ant%freq%deg = r_abpfdeg
      do il=1, rf%ant%nl
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(il)
        !
        rf%ant%freq%lim(ibb,:) = r_abpflim(ibb,:)
        do isb=isb1, isb2
          do ia=1, rf%ant%nant
            do j=0, rf%ant%freq%deg
              rf%ant%freq%amp(ibb,isb,ia,j) = r_abpfamp(ibb,isb,ia,j)
              rf%ant%freq%pha(ibb,isb,ia,j) = r_abpfpha(ibb,isb,ia,j)
            enddo
          enddo
        enddo
      enddo 
    else
      do ic=1, rf%ant%nc
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(ic)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(ic)
        !
        do isb=isb1, isb2
          do ia=1, rf%ant%nant
            rf%ant%chan%camp(ibb,isb,ia,ic) = r_abpccamp(ibb,isb,ia,ic)
            rf%ant%chan%cpha(ibb,isb,ia,ic) = r_abpccpha(ibb,isb,ia,ic)
          enddo
        enddo       
      enddo 
      rf%ant%chan%deg = r_abpcdeg
      do il=1, rf%ant%nl
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(il)
        !
        do isb=isb1, isb2
          do ia=1, rf%ant%nant
            do j=0, rf%ant%chan%deg
              rf%ant%chan%lamp(ibb,isb,ia,il,j) = r_abpclamp(ibb,isb,ia,il,j)
              rf%ant%chan%lpha(ibb,isb,ia,il,j) = r_abpclpha(ibb,isb,ia,il,j)
            enddo
          enddo
        enddo
      enddo
    endif
  else
    rf%bas%nbas = r_nbas
    rf%bas%nc   = r_nband
    rf%bas%nl   = r_lband
    rf%bas%nsb  = r_lnsb
    rf%bas%nbb  = r_nbb
    do il=1, rf%bas%nl
      rf%bas%isb(il) = r_lsband(1,il)
      rf%bas%ibb(il) = r_bb(il)
    enddo
    if (freq) then
      rf%bas%freq%deg = r_bpfdeg
      do il=1, rf%bas%nl
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(il)
        !
        rf%bas%freq%lim(ibb,:) = r_bpflim(ibb,:)
        do isb=isb1, isb2
          do ia=1, rf%bas%nbas
            do j=0, rf%bas%freq%deg
              rf%bas%freq%amp(ibb,isb,ia,j) = r_bpfamp(ibb,isb,ia,j)
              rf%bas%freq%pha(ibb,isb,ia,j) = r_bpfpha(ibb,isb,ia,j)
            enddo
          enddo
        enddo
      enddo 
    else
      do ic=1, rf%bas%nc
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(ic)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(ic)
        !
        do isb=isb1, isb2 
          do ia=1, rf%bas%nbas
            rf%bas%chan%camp(ibb,isb,ia,ic) = r_bpccamp(ibb,isb,ia,ic)
            rf%bas%chan%cpha(ibb,isb,ia,ic) = r_bpccpha(ibb,isb,ia,ic)
          enddo
        enddo       
      enddo 
      rf%bas%chan%deg = r_bpcdeg
      do il=1, rf%bas%nl
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(il)
        !
        do isb=isb1, isb2
          do ia=1, rf%bas%nbas
            do j=0, rf%bas%chan%deg
              rf%bas%chan%lamp(ibb,isb,ia,il,j) = r_bpclamp(ibb,isb,ia,il,j)
              rf%bas%chan%lpha(ibb,isb,ia,il,j) = r_bpclpha(ibb,isb,ia,il,j)
            enddo
          enddo
        enddo
      enddo
    endif
  endif
  return
end subroutine store_coefficients_solution

subroutine check_coefficients_solution(antenna,freq,equal)
  use clic_rf_solution
  !
  logical  :: antenna
  logical  :: freq
  logical  :: equal
  !
  include 'clic_par.inc'
  !
  integer :: ia,ibb,ic,il,isb,isb1,isb2,j
  !
  equal = .false.
  !
  if ((antenna.neqv.rf%antenna).or.(freq.neqv.rf%freq)) return
  !
  if (antenna) then
    !
    ! Antenna-based
    if (rf%ant%nant .ne. r_nant ) return
    if (rf%ant%nc   .ne. r_nband) return
    if (rf%ant%nl   .ne. r_lband) return
    if (rf%ant%nsb  .ne. r_lnsb)  return
    if (rf%ant%nbb  .ne. r_nbb)   return
    if (any(rf%ant%isb(1:r_lband).ne.r_lsband(1,1:r_lband))) return
    if (any(rf%ant%ibb(1:r_lband).ne.r_bb(1:r_lband))) return
    !
    if (freq) then
      !
      ! By frequency
      if (rf%ant%freq%deg .ne. r_abpfdeg ) return
      do il=1, rf%ant%nl
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(il)
        !
        if (any(rf%ant%freq%lim(ibb,:).ne.r_abpflim(ibb,:))) return
        do isb=isb1, isb2
          if (any(rf%ant%freq%amp(ibb,isb,1:r_nant,1:r_abpfdeg) &
             .ne. r_abpfamp(ibb,isb,1:r_nant,1:r_abpfdeg))) return
          if (any(rf%ant%freq%pha(ibb,isb,1:r_nant,1:r_abpfdeg) &
             .ne. r_abpfpha(ibb,isb,1:r_nant,1:r_abpfdeg))) return
        enddo
      enddo 
    else
      !
      ! By channel
      do ic=1, rf%ant%nc
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(ic)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(ic)
        !
        do isb=isb1, isb2
          if (any(rf%ant%chan%camp(ibb,isb,1:r_nant,ic) &
             .ne. r_abpccamp(ibb,isb,1:r_nant,ic))) return
          if (any(rf%ant%chan%cpha(ibb,isb,1:r_nant,ic) &
             .ne. r_abpccpha(ibb,isb,1:r_nant,ic))) return
        enddo
      enddo
      if (rf%ant%chan%deg .ne. r_abpcdeg ) return
      do il=1, rf%ant%nl
        if (rf%ant%nsb.eq.1) then
          isb1 = rf%ant%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%ant%ibb(il)
        !
        do isb=isb1, isb2
          if (any(rf%ant%chan%lamp(ibb,isb,1:r_nant,il,1:r_abpfdeg) &
             .ne. r_abpclamp(ibb,isb,1:r_nant,il,1:r_abpfdeg))) return
          if (any(rf%ant%chan%lpha(ibb,isb,1:r_nant,il,1:r_abpfdeg) &
             .ne. r_abpclpha(ibb,isb,1:r_nant,il,1:r_abpfdeg))) return
        enddo
      enddo
    endif
  else
    !
    ! Baseline-based
    if (rf%bas%nbas .ne. r_nbas ) return
    if (rf%bas%nc   .ne. r_nband) return
    if (rf%bas%nl   .ne. r_lband) return
    if (rf%bas%nsb  .ne. r_lnsb)  return
    if (rf%bas%nbb  .ne. r_nbb)   return
    if (any(rf%bas%isb(1:r_lband).ne.r_lsband(1,1:r_lband))) return
    if (any(rf%bas%ibb(1:r_lband).ne.r_bb(1:r_lband))) return
    !
    if (freq) then
      !
      ! By frequency
      if (rf%bas%freq%deg .ne. r_bpfdeg ) return
      do il=1, rf%bas%nl
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(il)
        !
        if (any(rf%bas%freq%lim(ibb,:).ne.r_bpflim(ibb,:))) return
        do isb=isb1, isb2
          if (any(rf%bas%freq%amp(ibb,isb,1:r_nbas,1:r_bpfdeg) &
             .ne. r_bpfamp(ibb,isb,1:r_nbas,1:r_bpfdeg))) return
          if (any(rf%bas%freq%pha(ibb,isb,1:r_nbas,1:r_bpfdeg) &
             .ne. r_bpfpha(ibb,isb,1:r_nbas,1:r_bpfdeg))) return
        enddo
      enddo 
    else
      !
      ! By channel
      do ic=1, rf%bas%nc
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(ic)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(ic)
        !
        do isb=isb1, isb2
          if (any(rf%bas%chan%camp(ibb,isb,1:r_nbas,ic) &
             .ne. r_bpccamp(ibb,isb,1:r_nbas,ic))) return
          if (any(rf%bas%chan%cpha(ibb,isb,1:r_nbas,ic) &
             .ne. r_bpccpha(ibb,isb,1:r_nbas,ic))) return
        enddo
      enddo
      if (rf%bas%chan%deg .ne. r_bpcdeg ) return
      do il=1, rf%bas%nl
        if (rf%bas%nsb.eq.1) then
          isb1 = rf%bas%isb(il)
          isb2 = isb1
        else
          isb1 = 1
          isb2 = 2
        endif
        ! 
        ibb = rf%bas%ibb(il)
        !
        do isb=isb1, isb2
          if (any(rf%bas%chan%lamp(ibb,isb,1:r_nbas,il,1:r_bpfdeg) &
             .ne. r_bpclamp(ibb,isb,1:r_nbas,il,1:r_bpfdeg))) return
          if (any(rf%bas%chan%lpha(ibb,isb,1:r_nbas,il,1:r_bpfdeg) &
             .ne. r_bpclpha(ibb,isb,1:r_nbas,il,1:r_bpfdeg))) return
        enddo
      enddo
    endif
  endif
  equal = .true.
  return
end subroutine check_coefficients_solution
!
subroutine set_spidx (qsb,qband,qbt,qntch,pc,pl,idx,error)
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! Computes the passband spectral index correction
  !---------------------------------------------------------------------
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbt                    !
  integer :: qntch                  !
  complex :: pc(qband,qsb,qbt)      !
  complex :: pl(qntch,qsb,qbt)      !
  real    :: idx
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  ! Local
  real*8 :: xx, yamp, ypha, aa(0:mbpcdeg), ap(0:mbpcdeg)
  real*8 :: ak1, ak2
  ! number of channels
  integer :: nc, nl, old_spidx
  integer :: i, isb, ib, ic, k, it, ib1, ib2, ib3, ndeg, ia, ja
  integer :: inbc, j, isb1, isb2
  integer, save :: old_idx  = -1000
  real :: freq, ref
  logical, save :: old_lowres = .false.
  !
  !------------------------------------------------------------------------
  !
  ! Check if correction was computed for the same spectral index
  if ((old_idx.eq.idx).and.(old_lowres.eqv.lowres)) return
  nc = r_nband
  nl = r_lband
  ref = r_flo1+r_isb*r_fif1
  do i = 1, nc
    ! Number of sidebands.
    if (r_lnsb.eq.1) then
      isb1 = r_sband(1,i)
      isb2 = isb1
    else
      isb1 = 1
      isb2 = 2
    endif
    !
    do isb=isb1, isb2
      freq = r_flo1+(3-2*isb)*(r_flo2(i) &
           + r_band2(i)*(r_flo2bis(i)+r_band2bis(i)*r_lfcen(i)))
      pc(i,isb,1:qbt) = cmplx((freq/ref)**-idx,0)
    enddo
  enddo
  do ic =1, nl
    ! Number of sidebands.
    if (r_lnsb.eq.1) then
      isb1 = r_sband(1,ic)
      isb2 = isb1
    else
      isb1 = 1
      isb2 = 2
    endif
    !
    do isb=isb1, isb2
      k = r_lich(ic)+1
      do i=1, r_lnch(ic)
        freq = r_flo1+(3-2*isb)*(r_flo2(ic) &
           + r_band2(ic)*(r_flo2bis(ic)+r_band2bis(ic)*r_lfcen(ic) &
           + (i-r_lcench(ic))*r_lfres(ic)))
        pl(k,isb,1:qbt) = cmplx((freq/ref)**-idx,0)
        k = k + 1
      enddo
    enddo
  enddo
  old_idx = idx
  old_lowres = lowres
  return
end subroutine set_spidx
!
subroutine set_spectrum_memory(qsb,qband,qbt,qntch,pc,pl,sc,sl,error)
  use clic_bpc
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbt                    !
  integer :: qntch                  !
  complex :: pc(qband,qsb,qbt)      !
  complex :: pl(qntch,qsb,qbt)      !
  complex :: sc(qband,qsb,qbt)      !
  complex :: sl(qntch,qsb,qbt)      !
  logical :: error                  !
  !
  include 'clic_parameter.inc'
  integer :: i,j,k
  !
  do i=1, mnbas
    do j=1,2
      do k=1, mrlband
        pc(k,j,i) = sc(k,j,i)
      enddo
      do k=1, mlch
        pl(k,j,i) = sl(k,j,i)
      enddo
    enddo
  enddo
  return
end subroutine set_spectrum_memory
!
subroutine set_pass_spectrum_ant(isb,ibb,qband,qsb,qbt,qntch,datac,datal,error)
  use clic_bpc
  integer :: isb   ! Sideband
  integer :: ibb   ! Baseband
  integer :: qband ! spw dimension
  integer :: qsb   ! sideband dimension
  integer :: qbt   ! baseband dimension
  integer :: qntch ! channel dimension
  complex :: datac(qband,qsb,qbt) ! cont passband
  complex :: datal(qntch,qsb,qbt) ! line passband
  logical :: error ! Error flag
  !
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  integer :: n_spw, ispw, i_spw(r_lband), imoins, iplus, nbpa, nbpp
  integer :: ia, ja, ib, i, k, is
  real    :: xx, xmoins, xplus, ymoins, yplus, deltafa, deltafp 
  real    :: f1a, f1p, yy1
  complex,allocatable :: apl(:,:,:),apc(:,:,:)
  real    :: amp, pha
  real*8  :: pix
  integer :: ier,lenc
  character(len=256) :: chain
  !
  allocate(apc(mcch,2,mnant),apl(mlch,2,mnant),stat=ier)
  if (ier.ne.0) then
    write(chain,'(a,i0)') 'Memory allocation error', ier 
    call message(6,3,'SET_PASS_SPECTRUM',chain(1:lenc(chain)))
    error = .true.
    return
  endif
  !
  ! First pass to find spw connected to bb 
  error = .false.
  n_spw = 0
  do ispw =1, r_lband
    if (r_bb(ispw).eq.ibb) then
      n_spw = n_spw + 1
      i_spw(n_spw) = ispw
    endif
  enddo
  !
  ! Assume regularly sampled passband
  nbpa = nbp_samp(isb,ibb)
  deltafa = (bp_fsamp(isb,ibb,nbpa)-bp_fsamp(isb,ibb,1))/(nbpa-1)
  f1a = bp_fsamp(isb,ibb,1)
  nbpp = nbp_spha(isb,ibb)
  deltafp = (bp_fspha(isb,ibb,nbpp)-bp_fspha(isb,ibb,1))/(nbpp-1)
  f1p = bp_fspha(isb,ibb,1)
  !
  ! Now compute passband
  apl = blankc
  do ispw = 1, n_spw
    is = i_spw(ispw)
    !
    ! Continuum
    xx = r_cfcen(is)
    pix = (xx-f1a)/deltafa+1
    imoins = int(pix)
    iplus  = imoins + 1
    if ((imoins.lt.1).or.(iplus.gt.nbpa)) then
       do ia=1, r_nant
         apc(is,isb,ia) = blankc
       enddo
    else
       do ja=1, bp_nant(ibb)
         ia = bp_ant(ja,ibb)
         if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
             bp_samp(isb,ibb,-ia,iplus) .eq.blank4) then
            apc(is,isb,ia) = blankc
         else
           yy1 = (imoins+1-pix) * bp_samp(isb,ibb,-ia,imoins) &
                 + (pix-imoins) * bp_samp(isb,ibb,-ia,iplus)     
           apc(is,isb,ia) = cmplx(exp(-yy1))
         endif
       enddo
    endif
    !
    ! Phase
    pix = (xx-f1p)/deltafp+1
    imoins = int(pix)
    iplus  = imoins + 1
    if ((imoins.lt.1).or.(iplus.gt.nbpp)) then
       do ia=1, r_nant
         apc(is,isb,ia) = blankc
       enddo
    else
       do ja=1, bp_nant(ibb)
         ia = bp_ant(ja,ibb)
         if (bp_spha(isb,ibb,-ia,imoins).eq.blank4.or.     &
             bp_spha(isb,ibb,-ia,iplus) .eq.blank4.or.     & 
             apc(is,isb,ia).eq.blankc) then
           apc(is,isb,ia) = blankc
         else
           yy1 = (imoins+1-pix) * bp_spha(isb,ibb,-ia,imoins) &
                 + (pix-imoins) * bp_spha(isb,ibb,-ia,iplus)     
           call rotate(apc(is,isb,ia),-dble(yy1))
         endif
       enddo
    endif
    do ib=1, r_nbas
      ia = r_iant(ib)
      ja = r_jant(ib)
      if ((apc(is,isb,ja).eq.blankc).or.apc(is,isb,ia).eq.blankc) then
        datac(is,isb,ib) = blankc
      else
        datac(is,isb,ib) = apc(is,isb,ja)*conjg(apc(is,isb,ia))
      endif
    enddo
    ! 
    ! Line
    k = r_lich(is)+1
    do i=1, r_lnch(is)
      !
      ! Amplitude
      xx = r_lfcen(is)+(i-r_lcench(is))*r_lfres(is)
      pix = (xx-f1a)/deltafa+1
      imoins = int(pix)
      iplus  = imoins + 1
      if ((imoins.lt.-1).or.(iplus.gt.nbpa+2)) then
        do ia=1, r_nant
          apl(k,isb,ia) = blankc
        enddo
      else
        if (imoins.lt.1) then
          imoins = 1
          iplus = imoins+1
        elseif (iplus.gt.nbpa) then
          iplus = nbpa
          imoins = iplus-1
        endif
        do ja=1, bp_nant(ibb)
          ia = bp_ant(ja,ibb)
          if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
              bp_samp(isb,ibb,-ia,iplus) .eq.blank4) then
             apl(k,isb,ia) = blankc
          else
            yy1 = (imoins+1-pix) * bp_samp(isb,ibb,-ia,imoins) &
                  + (pix-imoins) * bp_samp(isb,ibb,-ia,iplus)     
            apl(k,isb,ia) = cmplx(exp(-yy1))
          endif
        enddo
      endif
      !
      ! Phase
      pix = (xx-f1p)/deltafp+1
      imoins = int(pix)
      iplus  = imoins + 1
      if ((imoins.lt.-1).or.(iplus.gt.nbpp+2)) then
        do ia=1, r_nant
          apl(k,isb,ia) = blankc
        enddo
      else
        if (imoins.lt.1) then
          imoins = 1
          iplus = imoins+1
        elseif (iplus.gt.nbpp) then
          iplus = nbpp
          imoins = iplus-1
        endif
        do ja=1, bp_nant(ibb)
          ia = bp_ant(ja,ibb)
          if (bp_spha(isb,ibb,-ia,imoins).eq.blank4.or.     &
              bp_spha(isb,ibb,-ia,iplus) .eq.blank4.or.     & 
              apl(k,isb,ia).eq.blankc) then
            apl(k,isb,ia) = blankc
          else
            yy1 = (imoins+1-pix) * bp_spha(isb,ibb,-ia,imoins) &
                  + (pix-imoins) * bp_spha(isb,ibb,-ia,iplus)     
            call rotate(apl(k,isb,ia),-dble(yy1))
          endif
        enddo
      endif
      do ib=1, r_nbas
        ia = r_iant(ib)
        ja = r_jant(ib)
        if ((apl(k,isb,ja).eq.blankc).or.apl(k,isb,ia).eq.blankc) then
          datal(k,isb,ib) = blankc
        else
          datal(k,isb,ib) = apl(k,isb,ja)*conjg(apl(k,isb,ia))
        endif
      enddo
      k = k +1
    enddo
  enddo
  bp_memory_antenna = .true.
  bp_spectrum_ready = .true.
  return  
end subroutine set_pass_spectrum_ant
!
subroutine set_pass_spectrum(isb,ibb,qband,qsb,qbt,qntch,datac,datal,error)
  use clic_bpc
  integer :: isb   ! Sideband
  integer :: ibb   ! Baseband
  integer :: qband ! spw dimension
  integer :: qsb   ! sideband dimension
  integer :: qbt   ! baseband dimension
  integer :: qntch ! channel dimension
  complex :: datac(qband,qsb,qbt) ! cont passband
  complex :: datal(qntch,qsb,qbt) ! line passband
  logical :: error ! Error flag
  !
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  integer :: n_spw, ispw, i_spw(r_lband), imoins, iplus, nbpa, nbpp
  integer :: ib, i, k, is
  real    :: xx, xmoins, xplus, ymoins, yplus, deltafa, deltafp 
  real    :: f1a, f1p, yy1
  complex,allocatable :: pl(:,:,:),pc(:,:,:)
  real    :: amp, pha
  real*8  :: pix
  integer :: ier,lenc
  character(len=256) :: chain
  !
  allocate(pc(mrlband,2,mnbas),pl(mlch,2,mnbas),stat=ier)
  if (ier.ne.0) then
    write(chain,'(a,i0)') 'Memory allocation error', ier 
    call message(6,3,'SET_PASS_SPECTRUM',chain(1:lenc(chain)))
    error = .true.
    return
  endif
  !
  ! First pass to find spw connected to bb 
  error = .false.
  n_spw = 0
  do ispw =1, r_lband
    if (r_bb(ispw).eq.ibb) then
      n_spw = n_spw + 1
      i_spw(n_spw) = ispw
    endif
  enddo
  if (n_spw.le.0.or.n_spw.gt.r_lband) return
  !
  ! Assume regularly sampled passband
  nbpa = nbp_samp(isb,ibb)
  if (nbpa.le.1) then
    call message(8,3,'SET_PASS_SPECTRUM','Less than 2 points')
    error = .true.
    return
  endif
  deltafa = (bp_fsamp(isb,ibb,nbpa)-bp_fsamp(isb,ibb,1))/(nbpa-1)
  f1a = bp_fsamp(isb,ibb,1)
  !
  nbpp = nbp_spha(isb,ibb)
  if (nbpp.le.1) then
    call message(8,3,'SET_PASS_SPECTRUM','Less than 2 points')
    error = .true.
    return
  endif
  deltafp = (bp_fspha(isb,ibb,nbpp)-bp_fspha(isb,ibb,1))/(nbpp-1)
  f1p = bp_fspha(isb,ibb,1)
  if (deltafa.eq.0.or.deltafp.eq.0) then
    call message(8,3,'SET_PASS_SPECTRUM','Increment is null')
    error = .true.
    return
  endif
  !
  ! Now compute passband
  pl = blankc
  do ispw = 1, n_spw
    is = i_spw(ispw)
    !
    ! Continuum
    xx = r_cfcen(is)
    pix = (xx-f1a)/deltafa+1
    imoins = int(pix)
    iplus  = imoins + 1
    if ((imoins.lt.1).or.(iplus.gt.nbpa)) then
!       do ia=1, r_nant
!         apl(k,isb,ia) = blankc
!       enddo
    else
       do ib=1, r_nbas
         if (bp_samp(isb,ibb,ib,imoins).eq.blank4.or. &
             bp_samp(isb,ibb,ib,iplus).eq.blank4) then
            pc(is,isb,ib) = blankc
         else
           yy1 = (imoins+1-pix) * bp_samp(isb,ibb,ib,imoins) &
                 + (pix-imoins) * bp_samp(isb,ibb,ib,iplus)     
           pc(is,isb,ib) = cmplx(exp(-yy1))
         endif
       enddo
    endif
    !
    ! Phase
    pix = (xx-f1p)/deltafp+1
    imoins = int(pix)
    iplus  = imoins + 1
    if ((imoins.lt.1).or.(iplus.gt.nbpp)) then
!       do ia=1, r_nant
!         apl(k,isb,ia) = blankc
!       enddo
    else
       do ib=1, r_nbas
         if (bp_spha(isb,ibb,ib,imoins).eq.blank4.or. &
             bp_spha(isb,ibb,ib,iplus).eq.blank4) then
           pc(is,isb,ib) = blankc
         else
           yy1 = (imoins+1-pix) * bp_spha(isb,ibb,ib,imoins) &
                 + (pix-imoins) * bp_spha(isb,ibb,ib,iplus)     
           call rotate(pc(is,isb,ib),-dble(yy1))
         endif
       enddo
    endif
    do ib=1, r_nbas
      if (pc(is,isb,ib).eq.blankc) then
        datac(is,isb,ib) = blankc
      else
        datac(is,isb,ib) = pc(is,isb,ib)
      endif
    enddo
    ! 
    ! Line
    k = r_lich(is)+1
    do i=1, r_lnch(is)
      !
      ! Amplitude
      xx = r_lfcen(is)+(i-r_lcench(is))*r_lfres(is)
      pix = (xx-f1a)/deltafa+1
      imoins = int(pix)
      iplus  = imoins + 1
      if ((imoins.lt.1).or.(iplus.gt.nbpa)) then
!         do ia=1, r_nant
!           apl(k,isb,ia) = blankc
!         enddo
      else
         do ib=1, r_nbas
           if (bp_samp(isb,ibb,ib,imoins).eq.blank4.or.     &
               bp_samp(isb,ibb,ib,iplus) .eq.blank4) then
              pl(k,isb,ib) = blankc
           else
             yy1 = (imoins+1-pix) * bp_samp(isb,ibb,ib,imoins) &
                   + (pix-imoins) * bp_samp(isb,ibb,ib,iplus)     
             pl(k,isb,ib) = cmplx(exp(-yy1))
           endif
         enddo
      endif
      !
      ! Phase
      pix = (xx-f1p)/deltafp+1
      imoins = int(pix)
      iplus  = imoins + 1
      if ((imoins.lt.1).or.(iplus.gt.nbpp)) then
!         do ia=1, r_nant
!           apl(k,isb,ia) = blankc
!         enddo
      else
         do ib=1, r_nbas
           if (bp_spha(isb,ibb,ib,imoins).eq.blank4.or.     &
               bp_spha(isb,ibb,ib,iplus) .eq.blank4.or.     & 
               pl(k,isb,ib).eq.blankc) then
             pl(k,isb,ib) = blankc
           else
             yy1 = (imoins+1-pix) * bp_spha(isb,ibb,ib,imoins) &
                   + (pix-imoins) * bp_spha(isb,ibb,ib,iplus)     
             call rotate(pl(k,isb,ib),-dble(yy1))
           endif
         enddo
      endif
      do ib=1, r_nbas
        if (pl(k,isb,ib).eq.blankc) then
          datal(k,isb,ib) = blankc
        else
          datal(k,isb,ib) = pl(k,isb,ib)
        endif
      enddo
      k = k +1
    enddo
  enddo
  bp_memory_antenna = .false.
  bp_spectrum_ready = .true.
  return  
end subroutine set_pass_spectrum
!
subroutine set_pass_spectrum_ant_pol(isb,ibb,qband,qsb,qbt,qntch, &
                                     datac,datal,iscan,error)
  use clic_bpc
  integer :: isb   ! Sideband
  integer :: ibb   ! Baseband
  integer :: qband ! spw dimension
  integer :: qsb   ! sideband dimension
  integer :: qbt   ! baseband dimension
  integer :: qntch ! channel dimension
  complex :: datac(qband,qsb,qbt) ! cont passband
  complex :: datal(qntch,qsb,qbt) ! line passband
  integer :: iscan ! subscan number of polarisation sequence
  logical :: error ! Error flag
  !
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  !
  integer :: n_spw, ispw, i_spw(r_lband), imoins, iplus, nbpa, nbpp
  integer :: ia, ja, ka, ib, i, k, is, nbpac, nbppc
  real    :: xx, xmoins, xplus, ymoins, yplus, deltafa, deltafp 
  real    :: deltafac, deltafpc, f1ac, f1pc 
  real    :: f1a, f1p, yy1
  complex,allocatable :: apl(:,:,:),apc(:,:,:)
  real    :: amp, pha
  real*8  :: pix
  integer :: ier,lenc
  character(len=256) :: chain
  !
  integer,save :: tpol(mnant,mnpolscan)
  data tpol /192*0/
  !
  ! Check that iscan is valid
  if (iscan.lt.1.or.iscan.gt.mnpolscan) then
    call message(6,3,'FIND','Invalid polarisation subscan')
    error = .true.
    return
  endif
  !
  ! Read pol sequence if needed
  if (tpol(1,1).eq.0) then
    call read_polseq(tpol,mnant,mnpolscan,error)
    if (error) return
  endif
  !
  ! Allocate arrays
  allocate(apc(mrlband,2,mnant),apl(mlch,2,mnant),stat=ier)
  if (ier.ne.0) then
    write(chain,'(a,i0)') 'Memory allocation error', ier 
    call message(6,3,'SET_PASS_SPECTRUM',chain(1:lenc(chain)))
    error = .true.
    return
  endif

  !
  ! First pass to find spw connected to bb 
  error = .false.
  n_spw = 0
  do ispw =1, r_lband
    if (r_bb(ispw).eq.ibb) then
      n_spw = n_spw + 1
      i_spw(n_spw) = ispw
    endif
  enddo
  !
  ! Assume regularly sampled passband
  nbpa = nbp_samp(isb,ibb)
  deltafa = (bp_fsamp(isb,ibb,nbpa)-bp_fsamp(isb,ibb,1))/(nbpa-1)
  f1a = bp_fsamp(isb,ibb,1)
  nbpp = nbp_spha(isb,ibb)
  deltafp = (bp_fspha(isb,ibb,nbpp)-bp_fspha(isb,ibb,1))/(nbpp-1)
  f1p = bp_fspha(isb,ibb,1)
  !
  nbpac = nbp_samp_cross(isb,ibb)
  deltafac = (bp_fsamp_cross(isb,ibb,nbpa)-bp_fsamp_cross(isb,ibb,1))/(nbpac-1)
  f1ac = bp_fsamp_cross(isb,ibb,1)
  nbppc = nbp_spha_cross(isb,ibb)
  deltafpc = (bp_fspha_cross(isb,ibb,nbpp)-bp_fspha_cross(isb,ibb,1))/(nbppc-1)
  f1pc = bp_fspha_cross(isb,ibb,1)
  !
  ! Now compute passband
  apc = blankc
  apl = blankc
  do ja=1, bp_nant(ibb)
    ia = bp_ant(ja,ibb)
    ka = r_kant(ia)
    do ispw = 1, n_spw
      is = i_spw(ispw)
      xx = r_cfcen(is)
      !
      ! Continuum
      if (tpol(ka,iscan).eq.0) then
        pix = (xx-f1a)/deltafa+1
        imoins = int(pix)
        iplus  = imoins + 1
        if ((imoins.lt.1).or.(iplus.gt.nbpa)) then
           do ia=1, r_nant
             apc(is,isb,ia) = blankc
           enddo
        else
           if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
               bp_samp(isb,ibb,-ia,iplus) .eq.blank4) then
              apc(is,isb,ia) = blankc
           else
             yy1 = (imoins+1-pix) * bp_samp(isb,ibb,-ia,imoins) &
                   + (pix-imoins) * bp_samp(isb,ibb,-ia,iplus)     
             apc(is,isb,ia) = cmplx(exp(-yy1))
           endif
        endif
      else
        pix = (xx-f1ac)/deltafac+1
        imoins = int(pix)
        iplus  = imoins + 1
        if ((imoins.lt.1).or.(iplus.gt.nbpac)) then
          apc(k,isb,ia) = blankc
        else
          if (bp_samp_cross(isb,ibb,-ia,imoins).eq.blank4.or.     &
             bp_samp_cross(isb,ibb,-ia,iplus) .eq.blank4) then
            apc(is,isb,ia) = blankc
          else
            yy1 = (imoins+1-pix) * bp_samp_cross(isb,ibb,-ia,imoins) &
                  + (pix-imoins) * bp_samp_cross(isb,ibb,-ia,iplus)     
            apc(is,isb,ia) = cmplx(exp(-yy1))
          endif
        endif
      endif
      !
      ! Phase
      if (tpol(ka,iscan).eq.0) then
        pix = (xx-f1p)/deltafp+1
        imoins = int(pix)
        iplus  = imoins + 1
        if ((imoins.lt.1).or.(iplus.gt.nbpp)) then
          apc(is,isb,ia) = blankc
        else
          if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
             bp_samp(isb,ibb,-ia,iplus) .eq.blank4.or.     & 
             apc(is,isb,ia).eq.blankc) then
            apc(is,isb,ia) = blankc
          else
            yy1 = (imoins+1-pix) * bp_spha(isb,ibb,-ia,imoins) &
                  + (pix-imoins) * bp_spha(isb,ibb,-ia,iplus)     
            call rotate(apc(is,isb,ia),-dble(yy1))
          endif
        endif
      else
        pix = (xx-f1pc)/deltafpc+1
        imoins = int(pix)
        iplus  = imoins + 1
        if ((imoins.lt.1).or.(iplus.gt.nbppc)) then
          apc(is,isb,ia) = blankc
        else
          if (bp_samp_cross(isb,ibb,-ia,imoins).eq.blank4.or.     &
             bp_samp_cross(isb,ibb,-ia,iplus) .eq.blank4.or.     & 
             apc(is,isb,ia).eq.blankc) then
            apc(is,isb,ia) = blankc
          else
            yy1 = (imoins+1-pix) * bp_spha_cross(isb,ibb,-ia,imoins) &
                  + (pix-imoins) * bp_spha_cross(isb,ibb,-ia,iplus)     
            call rotate(apc(is,isb,ia),-dble(yy1))
          endif
        endif
      endif
      ! 
      ! Line
      k = r_lich(is)+1
      do i=1, r_lnch(is)
        !
        ! Amplitude
        xx = r_lfcen(is)+(i-r_lcench(is))*r_lfres(is)
        if (tpol(ka,iscan).eq.0) then
          pix = (xx-f1a)/deltafa+1
          imoins = int(pix)
          iplus  = imoins + 1
          if ((imoins.lt.-1).or.(iplus.gt.nbpa+2)) then
            apl(k,isb,ia) = blankc
          else
            if (imoins.lt.1) then
              imoins = 1
              iplus = imoins+1
            elseif (iplus.gt.nbpa) then
              iplus = nbpa
              imoins = iplus-1
            endif
            if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
                bp_samp(isb,ibb,-ia,iplus) .eq.blank4) then
               apl(k,isb,ia) = blankc
            else
              yy1 = (imoins+1-pix) * bp_samp(isb,ibb,-ia,imoins) &
                    + (pix-imoins) * bp_samp(isb,ibb,-ia,iplus)     
              apl(k,isb,ia) = cmplx(exp(-yy1))
            endif
          endif
        else
          pix = (xx-f1ac)/deltafac+1
          imoins = int(pix)
          iplus  = imoins + 1
          if ((imoins.lt.-1).or.(iplus.gt.nbpac+2)) then
            apl(k,isb,ia) = blankc
          else
            if (imoins.lt.1) then
              imoins = 1
              iplus = imoins+1
            elseif (iplus.gt.nbpac) then
              iplus = nbpac
              imoins = iplus-1
            endif
            if (bp_samp_cross(isb,ibb,-ia,imoins).eq.blank4.or.     &
                bp_samp_cross(isb,ibb,-ia,iplus) .eq.blank4) then
               apl(k,isb,ia) = blankc
            else
              yy1 = (imoins+1-pix) * bp_samp_cross(isb,ibb,-ia,imoins) &
                    + (pix-imoins) * bp_samp_cross(isb,ibb,-ia,iplus)     
              apl(k,isb,ia) = cmplx(exp(-yy1))
            endif
          endif
        endif
        !
        ! Phase
        if (tpol(ka,iscan).eq.0) then
          pix = (xx-f1p)/deltafp+1
          imoins = int(pix)
          iplus  = imoins + 1
          if ((imoins.lt.-1).or.(iplus.gt.nbpp+2)) then
            apl(k,isb,ia) = blankc
          else
            if (imoins.lt.1) then
              imoins = 1
              iplus = imoins+1
            elseif (iplus.gt.nbpa) then
              iplus = nbpa
              imoins = iplus-1
            endif
            if (bp_samp(isb,ibb,-ia,imoins).eq.blank4.or.     &
                bp_samp(isb,ibb,-ia,iplus) .eq.blank4.or.     & 
                apl(k,isb,ia).eq.blankc) then
              apl(k,isb,ia) = blankc
            else
              yy1 = (imoins+1-pix) * bp_spha(isb,ibb,-ia,imoins) &
                    + (pix-imoins) * bp_spha(isb,ibb,-ia,iplus)     
              call rotate(apl(k,isb,ia),-dble(yy1))
            endif
          endif
        else
          pix = (xx-f1pc)/deltafpc+1
          imoins = int(pix)
          iplus  = imoins + 1
          if ((imoins.lt.-1).or.(iplus.gt.nbppc+2)) then
            apl(k,isb,ia) = blankc
          else
            if (imoins.lt.1) then
              imoins = 1
              iplus = imoins+1
            elseif (iplus.gt.nbppc) then
              iplus = nbppc
              imoins = iplus-1
            endif
            if (bp_spha_cross(isb,ibb,-ia,imoins).eq.blank4.or.     &
                bp_spha_cross(isb,ibb,-ia,iplus) .eq.blank4.or.     &
                apl(k,isb,ia).eq.blankc) then
              apl(k,isb,ia) = blankc
            else
              yy1 = (imoins+1-pix) * bp_spha_cross(isb,ibb,-ia,imoins) &
                    + (pix-imoins) * bp_spha_cross(isb,ibb,-ia,iplus)
              call rotate(apl(k,isb,ia),-dble(yy1))
            endif
          endif
        endif
        k = k +1
      enddo
    enddo
  enddo
  do ispw = 1, n_spw
    is = i_spw(ispw)
    do ib=1, r_nbas
      ia = r_iant(ib)
      ja = r_jant(ib)
      if ((apc(is,isb,ja).eq.blankc).or.apc(is,isb,ia).eq.blankc) then
        datac(is,isb,ib) = blankc
      else
        datac(is,isb,ib) = apc(is,isb,ja)*conjg(apc(is,isb,ia))
      endif
    enddo
    k = r_lich(is)+1
    do i=1, r_lnch(is)
      do ib=1, r_nbas
        ia = r_iant(ib)
        ja = r_jant(ib)
        if ((apl(k,isb,ja).eq.blankc).or.apl(k,isb,ia).eq.blankc) then
          datal(k,isb,ib) = blankc
        else
          datal(k,isb,ib) = apl(k,isb,ja)*conjg(apl(k,isb,ia))
        endif
      enddo
      k = k +1
    enddo
  enddo
  bp_memory_antenna = .true.
  bp_spectrum_ready = .true.
  return  
end subroutine set_pass_spectrum_ant_pol
