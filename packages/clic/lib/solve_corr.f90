subroutine solve_corr(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !     Interface routine for SOLVE corr/SEARCH
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'clic_stations.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: mtry
  parameter (mtry=100)
  integer :: nd, ix, iy, i, l, l1, l2, l3, ib, ntry, i1, i2
  logical :: plot
  real :: search_box, db(2), bmes(2), bmin(2), ddb, sig, sigmin
  real :: search_interval, emes(2), emin(2)
  character(len=132) :: chain
  integer :: m_base
  integer(kind=address_length) :: ipx,ipy, data_base, ipd
  integer(kind=address_length) :: ipi, ipj, ipp, ipt
  data m_base/0/
  save m_base, data_base
  !------------------------------------------------------------------------
  error = .false.
  ! Code:
  !
  ! Offset values:
  search_box = 180.            ! in degrees
  search_interval = 45.
  call sic_r4(line,7,1,search_box,.false.,error)
  if (error) goto 999
  call sic_r4(line,7,2,search_interval,.false.,error)
  if (error) goto 999
  ddb = search_interval
  ntry = min(mtry, int(search_box/ddb + 1.))
  ! Coords
  n_x = 3
  i_x(1) = xy_time
  i_x(2) = xy_i_atm
  i_x(3) = xy_j_atm
  do i=1, 3
    sm_x1(i) = '*'
    sm_x2(i) = '*'
  enddo
  n_y = 1
  i_y(1) = xy_phase
  sm_y1(1) = '*'
  sm_y2(1) = '*'
  call set_display(error)
  if (error) return
  change_display = .true.
  ! Plot all scans in index, unsorted/X, and re-initialize the plot.
  call read_data('ALL',.false.,.true.,error)
  if (error) return
  plot = sic_present(1,0)
  change_display = .true.
  plotted = .false.
  if (plot) then
    clear = .true.
    call sub_plot('All',.false.,.false.,0,error)
    if (error) return
  endif
  !
  ! Loop on : boxes
  ipi = 0
  ipj = 0
  ipt = 0
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    ix = i_x(k_x(ib))
    if (iy.eq.xy_phase) then
      if (ix.eq.xy_i_atm) then
        ipi = gag_pointer(data_x,memory)+m_data*(ib-1)
      elseif (ix.eq.xy_j_atm) then
        ipj = gag_pointer(data_x,memory)+m_data*(ib-1)
      elseif (ix.eq.xy_time) then
        ipt = gag_pointer(data_x,memory)+m_data*(ib-1)
        ipp = gag_pointer(data_y,memory)+m_data*(ib-1)
        nd = n_data(ib)
      endif
    else
      call message(6,3,'SOLVE_DELAY',   &
        'Use SET X I_ATM J_ATM TIME and SET Y PHASE')
    endif
    if (ipi.ne.0 .and. ipj.ne.0 .and. ipt.ne.0) then
      !
      ! Get auxiliary storage
      if (nd*16.gt.m_base) then
        if (m_base .gt.0) then
          call free_vm(m_base,data_base)
          m_base = 0
        endif
        error = (sic_getvm(nd*16,data_base).ne.1)
        if (error) return
        m_base = nd*16
      endif
      ipd = gag_pointer(data_base,memory)
      !
      sigmin = 1e20
      do i1 = 1, ntry
        do i2 = 1, ntry
          db(1)  = (i1-1)*ddb*pis/180.
          db(2)  = (i2-1)*ddb*pis/180.
          !               CALL SOLVE_CORR_SUB (ND,X_DATA(1,i_IB),
          !     $         x_DATA(1,j_iB), x_data(1,it_ib), y_data(1,it_ib),
          !     $              DB, BMES, emes,
          !     $         SIG, MEMORY(IPD+2*ND), MEMORY(IPD),
          !     $         ERROR)
          call solve_corr_sub (nd, memory(ipi), memory(ipj),   &
            memory(ipt), memory(ipp), db, bmes, emes,   &
            sig, memory(ipd+2*nd), memory(ipd), error)
          sig = sig/pis*180.
          if (sig.lt.sigmin) then
            sigmin = sig
            do i=1, 2
              bmin(i) = bmes(i)/pis*180.
              emin(i) = emes(i)/pis*180.
            enddo
          endif
        enddo
      enddo
      !
      ! Display results
      l = lenc(y_label(ib))
      l1 = lenc(header_1(ib))
      l2 = lenc(header_2(ib))
      l3 = lenc(header_3(ib))
      write(chain,1000) ib, header_1(ib)(1:l1),   &
        header_2(ib)(1:l2), header_3(ib)(1:l3), sigmin
      call message(6,1,'SOLVE_CORR',chain(1:lenc(chain)))
      write(chain,1001) (bmin(i), i=1,2)
      call message(6,1,'SOLVE_CORR',chain(1:lenc(chain)))
      write(chain,1002) (emin(i), i=1,2)
      call message(6,1,'SOLVE_CORR',chain(1:lenc(chain)))
      ipi = 0
      ipj = 0
      ipt = 0
    endif
  enddo
  return
999 error = .true.
  return
1000 format(i2,1x,a,' Ch. ',a,' Band ',a,' rms ',f12.3,' Deg.')
1001 format('Corrections = ',2f12.3,' Deg / T.Pow. unit.')
1002 format('Errors      = ',2f12.3,' Deg / T.Pow. unit.')
end subroutine solve_corr
!
subroutine solve_corr_sub (nd,   &
    x_iatm, x_jatm, x_time,   &
    y_pha, boff, bmes, emes, sig, a, b, error)
  use gildas_def
  !---------------------------------------------------------------------
  !     ND        INTEGER    Number of points
  !     X_IF(ND)  REAL       I_F frequency of points
  !     Y_PHA(ND) REAL       Phases of points
  !     BOFF      REAL       Original delay offset
  !     BMES(2)   REAL       Returned delay (?) or phase range (?)
  !     SIG       REAL       Rms error on phase (?) or delay (?)
  !     A(ND,MVAR)   REAL*8  Work space for minimization routine
  !                          on return, covariance matrix ?
  !     B(ND)        REAL*8  Ibid
  !     ERROR     LOGICAL    Error flag
  !---------------------------------------------------------------------
  integer :: nd                     !
  real :: x_iatm(nd)                !
  real :: x_jatm(nd)                !
  real :: x_time(nd)                !
  real :: y_pha(nd)                 !
  real :: boff(2)                   !
  real :: bmes(2)                   !
  real :: emes(2)                   !
  real :: sig                       !
  real*8 :: a(nd,*)                 !
  real*8 :: b(nd)                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: i,k,ndd,nvar
  real*8 :: sigma
  !------------------------------------------------------------------------
  ! Code:
  nvar = 4
  i = 0
  do k=1, nd
    if (abs(y_pha(k)-blank4).gt.d_blank4) then
      i = i + 1
      a(i,1) = +x_iatm(k)
      a(i,2) = -x_jatm(k)
      a(i,3) = x_time(k)
      a(i,4) = 1
      if (.not.degrees) then
        b(i) = y_pha(k)
      else
        b(i) = y_pha(k)*pis/180.
      endif
      b(i) = b(i)-boff(1)*a(i,1)-boff(2)*a(i,2)
      b(i) = mod(b(i)+11*pi,2*pi)-pi
    endif
  enddo
  ndd = i
  if (ndd.lt.nvar) then
    call message(8,4,'SOLVE_corr_SUB','Too few data points')
    error = .true.
    return
  endif
  call mth_fitlin ('SOLVE_CORR',ndd,nvar,a,b,nd,sigma)
  ! display results
  sig = sigma
  bmes(1) = b(1)+boff(1)
  bmes(2) = b(2)+boff(2)
  emes(1) = sqrt(a(1,1))
  emes(2) = sqrt(a(2,2))
end subroutine solve_corr_sub
