subroutine write_fits_holodata (unit, iband, ntch, ib, ldata,   &
    data ,error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Write the HOLO DATA  fits table, from CLIC continuum band iband
  ! it has three  columns.
  ! Assume one polarization, two sidebands.
  ! ibas is the baseline number used
  !---------------------------------------------------------------------
  integer :: unit                   !
  integer :: iband                  !
  integer :: ntch                   !
  integer :: ib                     !
  integer :: ldata                  !
  real :: data(ldata)               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: nch, itable
  integer :: k, mf, status, nrows, tfields, varidat, ir, h_offset
  integer :: krow, isb, mdigit1, mdigit2, r_npol, r_nside
  integer :: ich, k0, c_offset, nsb, kd
  parameter(r_npol = 1, r_nside=2, mdigit2=13, mdigit1=8)
  real*8 :: ifval, ifinc, ifref
  parameter (mf = 7)
  character(len=16) :: ttype(mf),tform(mf),tunit(mf), extname
  data ttype/     'INTEGNUM',   &
    'HOLOSS', 'HOLORR', 'HOLOQQ', 'HOLOSR', 'HOLOSQ', 'HOLOQR'/
  data tform/     '1J',   &
    '1E',      '1E',      '1E',      '1E',      '1E',      '1E'/
  data tunit/     ' ',   &
    ' ',       ' ',        ' ',       ' ',       ' ',       ' '/
  !---------------------------------------------------------------------
  !
  !  Append/create a new empty HDU onto the end of the file and move to it.
  status = 0
  call ftcrhd(unit,status)
  !
  !  Define parameters for the binary table (see the above data statements)
  !
  nrows = r_ndump
  nch = 1
  ich = 1
  nsb = 2
  itable = 1
  tfields = mf
  if (r_lmode.ne.1) then
    write (6,*) 'programming error, should be correlation ... '
    error = .true.
    return
  endif
  extname = 'HOLODATA-ALMATI'
  varidat = 0
  !
  call ftphbn(unit,nrows,tfields,ttype,tform,tunit,   &
    extname,varidat,status)
  ! Other header keywords
  call ftpkys(unit,'TABLEREV','v1.0 2001-07-03',   &
    extname//' release',status)
  if (status .gt. 0) goto 99
  call write_fits_dobs(unit,error)
  if (error) return
  !      CALL FTPKYJ(UNIT,'SCAN-NUM',R_SCAN,'Scan number',STATUS)
  !      IF (STATUS .GT. 0) GOTO 99
  !      CALL FTPKYJ(UNIT,'OBS-NUM',R_NUM,'Observation number',STATUS)
  !      IF (STATUS .GT. 0) GOTO 99
  call ftpkyj(unit,'NO_POL',1,'Number of pols.',status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'CHANNELS',1,   &
    'number of channels in baseband',status)
  call ftpkyj(unit,'BASEBAND',1,'Baseband number',status)
  call ftpkyj(unit,'TABLEID',1,'Baseband table number ',status)
  if (status .gt. 0) goto 99
  call ftpkye(unit,'TRANDIST',300.,mdigit1,   &
    'Transmitter Distance (m)',status)
  call ftpkyd(unit,'TRANFREQ',r_restf*1d6,mdigit2,   &
    'Transmitter Frequency (Hz)',status)
  call ftpkye(unit,'TRANFOCU',0.10123,mdigit1,   &
    'Focus Offset (m)',status)
  if (r_flux.ne.0) then
    call ftpkye(unit,'IFLUX',r_flux,mdigit1,'Flux in I (Jy)',   &
      status)
  endif
  ! loop on table rows
  krow = 0
  do ir=1, nrows
    k = 1 + h_offset(ir)
    call decode_header (data(k))
    k0 = 1 + c_offset(ir)
    krow = krow+1
    !
    ! Fill in the columns:
    ! INTEGNUM
    call ftpclj(unit,1,krow,1,1,dh_dump,status)
    isb = (1-r_isb)/2+1
    kd = k0 + (ich+(isb-1+(ib-1)*nsb)*ntch)*2
    ! col 2: s*s
    call ftpcle(unit,2,krow,1,1,sqrt(data(kd)**2+data(kd+1)**2),   &
      status)
    ! col 3: r*r
    call ftpcle(unit,3,krow,1,1,1.0,status)
    ! col 4: s*s
    call ftpcle(unit,4,krow,1,1,1.0,status)
    ! col 5: s*r
    call ftpcle(unit,5,krow,1,1,data(kd),status)
    ! col 6: s*q
    call ftpcle(unit,6,krow,1,1,data(kd+1),status)
    ! col 7: q*r
    call ftpcle(unit,7,krow,1,1,0.0,status)
  enddo
  if (status .gt. 0) goto 99
  return
  ! Error return
99 call printerror('WRITE_FITS_HOLODATA',status)
  error = .true.
end subroutine write_fits_holodata
