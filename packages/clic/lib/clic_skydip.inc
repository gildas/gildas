!
! Common for FSKY (minimization routine)
      INTEGER MP
      PARAMETER (MP=30)
      REAL EL(MP), SKY(MP), PAR(2), SIGRMS
      INTEGER NP , IFE
      LOGICAL TREC_MODE
!
! Common for calibration
      REAL TAUOXS, TAUWS, TAUOXI, TAUWI, TCAL,                          &
     &ATMSIG, ATMIMA, TAMB, GAINIM, TAUSIG, TAUIMA, TCHOP, FEFF
      COMMON /CSKYDI/ PAR, EL, SKY, SIGRMS, IFE, NP,                    &
     &TAUOXS, TAUWS, TAUOXI, TAUWI, TCAL,                               &
     &ATMSIG, ATMIMA, TAMB, GAINIM, TAUSIG, TAUIMA, TCHOP, FEFF,        &
     &TREC_MODE
      SAVE /CSKYDI/
