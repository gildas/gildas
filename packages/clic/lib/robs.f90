subroutine robs(ix,error)
  use classic_api
  use clic_file 
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! Read entry descriptor, then title and copy title into r_ structure
  !---------------------------------------------------------------------
  integer(kind=entry_length) :: ix                     !
  logical         :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: len0,add
  character(len=10) :: nombre
  !------------------------------------------------------------------------
  modify = .false.
  error = .false.
  !
  if (ix.le.0 .or. ix.ge.i%desc%xnext .or. ix.gt.m_ix) then
    error = .true.
    write(nombre,'(I10)') ix
    call message(8,3,'ROBS','Non-existant index entry '//nombre)
    return
  endif
  !
  ! Initialize Observation buffer
  call classic_recordbuf_open(i,ix_bloc(ix),ix_word(ix),ibuff,error)
  if (error) return
  !
  ! Then read entry descriptor
  call classic_entrydesc_read(i,ibuff,e,error)
  if (error) return
  !
  ! Check observation version
  if (e%version.lt.1.or.e%version.gt.version_last) then
    call message(8,3,'ROBS','Observation version not supported ')
    call message(8,1,'ROBS','Use a more recent CLIC version')
    error = .true.
    return
  endif
  !
  ! Duplicate index information
  call rix(ix,error)
  if (error) return
  r_num   = title%num
  r_ver   = title%ver
  r_dobs  = title%dobs
  r_dred  = title%dred
  r_typec = title%typec
  r_kind  = title%kind
  if (r_kind.ge.5) then
    new_receivers = .true.
  else
    new_receivers = .false.
  endif
  r_qual  = title%qual
  r_scan  = title%scan
  r_nrec  = title%recei
  call bytoch(title%project,r_project,8)
  call bytoch(title%teles,r_teles,12)
end subroutine robs
!
subroutine xobs (error)
  use classic_api
  use clic_file
  use clic_index
  !---------------------------------------------------------------------
  !	Opens an output observation in the output file to extend it.
  !	This works only if this obs. is the last in the file
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer(kind=entry_length) :: ix,inum
  integer :: iver
  ! Code:
  if (r_ver.lt.0) then
    call message(8,3,'XOBS','Can only extend last versions')
    error = .true.
    return
  endif
  ix = o%desc%xnext-1
  inum = ox_num(ix)
  iver = ox_ver(ix)
  if (iver.lt.0 .or. inum.ne.r_num) then
    call message(4,2,'XOBS',   &
      'Observation not found or may not be extended')
    error = .true.
    return
  endif
  !
  ! Initialize Observation buffer
  call classic_recordbuf_open(o,ox_bloc(ix),ox_word(ix),obuff,error)
  if (error) return
  !
  ! Then read entry descriptor
  call classic_entrydesc_read(o,obuff,e,error)
  if (error) return
  !
  ! Check observation version
  if (e%version.lt.1.or.e%version.gt.version_last) then
    call message(8,3,'XOBS','Observation version not supported ')
    error = .true.
    return
  endif
  ! 
  ! Observation is opened for modification
  modify = .true.
end subroutine xobs
!
subroutine mobs (error)
  use classic_api
  use clic_file
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  !	Open an observation of the output file to modify it.
  !	The length of each section modified must be smaller
  !	than the length of the corresponding section already present.
  !
  !	Find the latest version of this obs. in this file
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: nf
  integer(kind=entry_length) :: ix, ifind(10)
  !
  if (r_ver.lt.0) then
    call message(8,3,'UPDATE','Can only update last versions')
    error = .true.
    return
  endif
  !
  ! Find latest observation version
  fver =.false.
  fnum =.true.
  xnum =r_num
  ynum =r_num
  fscan = .false.
  xrecei = -1
  fut = .false.
  last =.true.
  nf = 1
  call fox(nf,ifind,error)
  if (error) return
  if (nf.eq.0) then
    error = .true.
    call message(8,4,'MOBS',   &
      'Observation to be modified was not found')
    return
  endif
  !
  ! Process last version of the observation
  ix = ifind(1)
  !
  ! Initialize Observation buffer
  call classic_recordbuf_open(o,ox_bloc(ix),ox_word(ix),obuff,error)
  if (error) return
  !
  ! Then read entry descriptor
  call classic_entrydesc_read(o,obuff,e,error)
  if (error) return
  !
  ! Check observation version
  if (e%version.lt.1.or.e%version.gt.version_last) then
    call message(8,3,'MOBS','Observation version not supported ')
    error = .true.
    return
  endif
  !
  ! Observation opened for modify
  modify = .true.
end subroutine mobs
!
subroutine cobs(ltype,error)
  use gkernel_interfaces
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  !	Ends the writing of the output observation.
  !---------------------------------------------------------------------
  integer :: ltype                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: nf,hver
  integer(kind=entry_length) :: ifind(10),jx
  !
  if (obuff%lun.ne.o%lun) then
    error = .true.
    call message(8,4,'COBS',   &
      'Observation not open for write nor modify')
    return
  endif
  !
  hver = 0
  if (.not.modify) then
    if (o%lun.ne.i%lun) then         ! Write on a new file:
      r_num = o%desc%xnext           ! Guarantee increase
    endif
    fver=.false.
    fnum=.true.
    xnum=r_num
    ynum=r_num
    last=.true.
    nf = 1
    fscan = .false.
    xrecei = -1
    fut = .false.
    call fox(nf,ifind,error)
    if (error) return
    if (nf.ne.0) then
      hver = ox_ver(ifind(1))
      if (hver.ge.0) ox_ver(ifind(1)) = -hver
      call rox(ifind(1),error)
      if (error) return
      title%ver = ox_ver(ifind(1))
      call mox(ifind(1),error)
      if (hver.lt.0) hver = -hver
      if (error) return
    else
      hver = 0
    endif
  endif
  !
  title%typec = ltype
  title%off1  = r_lamof
  title%off2  = r_betof
  ! Interferometer specific
  title%proc  = r_proc
  title%itype = r_itype
  title%houra = r_houra
  title%ut    = r_ut
  title%recei = r_nrec
  call chtoby(r_project,title%project,8)
  title%bpc   = r_bpc
  if (r_ic.ne.0) then
    title%ic  = r_ic
  else
    title%ic  = 0
  endif
  !
  ! Write the index entry
  ! The version number must increase
  !
  title%bloc = obuff%rstart
  title%word = obuff%wstart
  title%num  = r_num
  
  title%kind = r_kind
  title%qual = r_qual
  r_ver      = abs(r_ver)
  title%ver  = max(r_ver,hver)+1
  if (title%ver.gt.99) title%ver=11
  title%scan = r_scan
  call chtoby (r_sourc,title%sourc,12)
  call chtoby (r_line,title%line,12)
  call chtoby (r_teles,title%teles,12)
  title%dobs = r_dobs
  call sic_date (r_cdred)
  call cdate (r_cdred,r_dred,error)
  title%dred = r_dred
  if (modify) then
    ! Caution, if error before classic_entry_write, last buffer not written
    jx = e%xnum
    call mox(jx,error)
  else
    call wox(error)
  endif
  if (error) return
  !
  ! Write the observation header
  call classic_entrydesc_write(o,obuff,e,error)
  if (error) return
  !
  ! And close the entry
  call classic_entry_close(o,obuff,error)
  !
  ! File both: nullify input file record buffer, as it may have changed on disk
  if (i%lun.eq.o%lun) then
    call classic_recordbuf_nullify(ibuff)
  endif
  !
  ! Update the file header
  call cox(error)
end subroutine cobs
!
subroutine iobs(n,ix,check,error)
  use gkernel_interfaces
  use classic_api
  use clic_file
  use clic_title
  use clic_find
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	Starts the writing of an observation.
  ! Arguments :
  !	N 	I	maximum # of sections to be written
  !			(in addition of the data section)
  !	IX 	I	returns the index number of the observation
  !			in the output file
  !      CHECK    L       .true., check that observation is not a diplicate.
  !---------------------------------------------------------------------
  integer         :: n                      !
  integer(kind=entry_length) :: ix                     !
  logical         :: check                  !
  logical         :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  logical :: err, full
  integer :: ier, nff, lch, version
  integer(kind=entry_length) :: iff(100)
  character(len=80) :: ch
  !
  if (o%lun.eq.0) then
    call message(8,4,'IOBS','No output file opened')
    error=.true.
    return
  endif
  !
  ! Check if scan was written previously (with same receiver and UT time)
  ! Nevertheless, authorize writing if same scan number and not check
  !
  if (o%desc%xnext.gt.1) then
    !
    nff = 100
    fnum = .false.
    fver = .false.
    xscan(1) = r_scan
    yscan(1) = r_scan
    xnscan = 1
    fscan = .true.
    xrecei = r_nrec
    xut = real(r_ut,kind=4) ! Patch: r_ut is kind=8, but title%ut is kind=4
    yut = real(r_ut,kind=4) ! Patch: r_ut is kind=8, but title%ut is kind=4
    fut = .true.
    call fox(nff,iff,error)
    if (nff.ge.1) then
      write (ch,'(a,i5,a,i2,a)') 'Observation ',r_scan,   &
        ' receiver ',r_nrec, ' already written'
      lch = lenc(ch)
      call message(8,2,'IOBS',ch(1:lch))
      if (check) then
        error = .true.
        return
      endif
    endif
  endif
  !
  ! Proceed
20 continue
  call classic_filedesc_read(o,err)
  if (err) then
    if (error) then
      call message(8,3,'IOBS','Read error in file descriptor')
      call messios(8,3,'IOBS',ier)
      return
    else
      error = .true.
      call sic_wait(1.0)
      goto 20
    endif
  endif
  error = .false.
  !
  ! Add index extension if required before preparing the observation
  ! And initialize entry descriptor
  modify = .false.
  ix = o%desc%xnext
  if (o%desc%version.eq.1) then     ! CLASSIC V1
    version = classic_vobs_v1       !   Observation version v1
  elseif(o%desc%version.eq.2) then  ! CLASSIC V2
    version = version_current       !   Desired version
  endif
  call classic_entry_init(o,ix,n,version,full,e,error)
  if (full) call message(8,3,'IOBS','Output index is full')
  if (error) return
  !
  ! And initialize output buffer
  call classic_recordbuf_open(o,o%desc%nextrec,o%desc%nextword,obuff,error)
  if (error) return
  !
end subroutine iobs
