subroutine write_sdm_station(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Station SDM table.
  !
  !     int sdm_addStationRow (char * name, int * nameDim, double *
  !     position, int * positionDim, char * type, int * typeDim)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_station
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  include 'clic_stations.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical error
  ! Local
  type(StationRow) :: sRow
  type(StationKey) :: sKey
  integer ia, is, k, l, ier
  real*8 weather(3)
  CHARACTER*1 ARM(3)
  DATA ARM/'N','W','E'/
  character sdmTable*12
  parameter (sdmTable='Station')
  real*8 :: lonlat(2), altitude, s_5(2), x_5(3), x_2(3), s_2(2),   &
    trfm_52(3,3), psi, the, phi
  real*8, parameter :: radius=6367.435d3   ! the Earth radius
  !     approximately at center of interferometer
  data weather/3*0d0/
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  error = .false.
  !
  ! Weather station relocated at interferometer center...
  CALL SIC_SEXA ('05:54:28.5',10,LONLAT(1),ERROR)
  CALL SIC_SEXA ('44:38:02.0',10,LONLAT(2),ERROR)
  ALTITUDE = 2.560*1000.
  lonlat = lonlat*pi/180d0
  !
  ! Euler angles to go from horiz (2) to geocentric (5)
  psi = pi/2                   ! 90 degrees
  the = pi/2-lonlat(2)         ! 90 degrees minus latitude
  phi = pi/2-lonlat(1)         ! 90 degrees minus longitude
  call eulmat (psi, the, phi,trfm_52)
  !     center of Observatory in horizontal coordinates
  x_2 = (/0d0, 0d0, radius+altitude/)
  call matvec (x_2, trfm_52, x_5)
  weather = x_5
  !      do k=1, 3
  !         weather(k) = x_5(k)
  !      enddo
  !
  !
  call allocStationRow(srow, error)
  if (error) return
  sRow%name = 'PdB-WeatherStation'
  sRow%type = StationType_WEATHER_STATION
  sRow%position = weather
  !
  call addStationRow(sKey, sRow, error)
  if (error) return
  Weather_Station_Id = sKey%stationId
  !
  if (r_teles.eq.'VTX-ALMATI') then
    CALL SIC_SEXA ('-107:37:10.02',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('34:04:29.8',8,LONLAT(2),ERROR)
    ALTITUDE = 2.13542*1000.
    do ia = 1, r_nant
      sRow%name = 'Vertex ATF Pad'
      sRow%position = 0
      sRow%type = StationType_ANTENNA_PAD
      call addStationRow(sKey, sRow, error)
      if (error) return
      station_id(ia) = sKey%stationId
    enddo
  elseif (r_teles.eq.'AEC-ALMATI') then
    CALL SIC_SEXA ('-107:37:10.02',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('34:04:29.8',8,LONLAT(2),ERROR)
    ALTITUDE = 2.13542*1000.
    do ia = 1, r_nant
      sRow%name = 'AEC ATF Pad'
      sRow%position = 0
      sRow%type = StationType_ANTENNA_PAD
      call addStationRow(sKey, sRow, error)
      if (error) return
      station_id(ia) = sKey%stationId
    enddo
  else
    CALL SIC_SEXA ('05:54:28.5',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('44:38:02.0',10,LONLAT(2),ERROR)
    ALTITUDE = 2.560*1000.
    lonlat = lonlat*pi/180d0
    !     These coordinates are in the Bure system,
    !     we thus  rotate/translate  them to geocentric...
    do ia = 1, r_nant
      is = r_istat(ia)
      write (sRow%name,'(a5,i2.2)') 'PdB-'//arm(is/100), mod(is   &
        ,100)
      !     Now add the station local coordinates given in the Bure system: Z
      !     to N pole, X to south
      !
      !     Euler angles to go from hour angle system (Bure) to geocentric (5)
      psi = 0                  ! 90 degrees
      the = 0                  ! z axes are parallel
      phi = -lonlat(1)         ! 90 degrees minus longitude
      call eulmat (psi, the, phi,trfm_52)
      do k=1, 3
        x_2(k) = stat99(k,is)
      enddo
      call matvec (x_2, trfm_52, x_5)
      !            do k=1, 3
      !               sRow%position(k) = weather(k) + x_5(k)
      !            enddo
      sRow%position = weather + x_5
      do k=1, 3
        r_antgeo(k,ia) = sRow%position(k)
      enddo
      sRow%type = StationType_ANTENNA_PAD
      call addStationRow(sKey, sRow, error)
      if (error) return
      station_id(ia) = sKey%stationId
    enddo
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
!
end subroutine write_sdm_station
!---------------------------------------------------------------------
subroutine Get_sdm_station(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Read the Station SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_station
  logical error
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  include 'clic_stations.inc'
  ! Dummy
  ! Local
  type(StationRow) :: sRow
  type(StationKey) :: sKey
  type(StationKey), allocatable :: sKeyList(:)
  integer ia, is, k, l, ier, stationSize, i
  character*12 name
  real*8 distance, mindist
  CHARACTER*1 ARM(3), carm
  DATA ARM/'N','W','E'/
  character sdmTable*12
  parameter (sdmTable='Station')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call allocStationRow(srow, error)
  if (error) return
  !
  !     These coordinates are assumed to be in the Bure system,
  !     may be rotate/translate  them from geocentric...
  do ia = 1, r_nant
    sKey%stationId = station_id(ia)
    call getStationRow(sKey, sRow, error)
    if (error) return
    if (sRow%name(1:4).eq.'PdB-') then
      read (sRow%name,'(4x,a1,i2.2)') carm, is
      is = mod(is,100)
      do k=1, 3
        if (carm.eq.arm(k)) then
          r_istat(ia) = k*100+ia
        endif
      enddo
    else
      !c            print *, 'ia, sRow%position ', ia, sRow%position
      do k=1, 3
        r_antgeo(k,ia) = sRow%position(k)
      enddo
      is = 0
    endif
  !
  !     Note: we do not keep track of stations positions in CLIC.
  enddo

  call getStationTableSize(stationSize, error)
  if (error) return
  if (stationSize.gt.0) then
    if (allocated(sKeyList)) then
      deallocate(sKeyList, stat=ier)
      if (ier.ne.0) then
        call sdmmessageI(8,4,sdmTable   &
          ,'Deallocation error, ier= ',ier)
        error = .true.
        return
      endif
    endif
    allocate (sKeyList(stationSize), stat=ier)
    if (ier.ne.0) then
      call sdmmessageI(8,4,sdmTable   &
        ,'Allocation error, ier= ',ier)
      error = .true.
      return
    endif
    call getStationKeys(stationSize, sKeyList, error)
    if (error) return
  endif
  mindist = 1e6 ! 1000 km
  do i=1, stationSize
    call getStationRow(sKeyList(i), sRow, error)
    if (error) return
    distance = 0
    do k=1, 3
      distance = distance + (r_antgeo(k,1)-sRow%position(k))**2
    enddo
    if (sqrt(distance).lt.mindist) then
      name = sRow%name
      Weather_Station_Id = sKeyList(i)%stationId
    endif
  enddo
  ! print *, name, Weather_Station_Id
  !     (but we have tothing to use there...)
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine Get_sdm_station
!---------------------------------------------------------------------
!!! subroutine allocStationRow(row, error)
!!!   !---------------------------------------------------------------------
!!!   !     real*8, allocatable :: position(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Station
!!!   type(StationRow) :: row
!!!   integer :: ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'Station'
!!!   !
!!!   ! position(3)
!!!   if (allocated(row%position)) then
!!!     deallocate(row%position, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%position(3), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!---------------------------------------------------------------------
subroutine charcpp(chain,chainDim)
  use gkernel_interfaces
  integer chainDim(2), l, i
  !
  !     add a null, returns the useful length
  !
  !     truncate if too long
  !
  character*(*) chain
  l = lenc(chain)
  if (l.eq.len(chain)) then
    l = l-1
  endif
  chain = chain(1:l)//char(0)
  chainDim(1) = l+1
  chainDim(2) = len(chain)
end subroutine charcpp
