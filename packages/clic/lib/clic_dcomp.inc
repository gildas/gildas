!
! Data structures : Data Header DCOMP
!
      INTEGER DCOMP_VOID           ! Dummy alignment word
      INTEGER DCOMP_DUMP           ! dump number
      INTEGER DCOMP_OBS            ! MJ date
      REAL DCOMP_INTEG             ! Integration time
      REAL*8 DCOMP_UTC             ! UTC (sec.)
      REAL*8 DCOMP_SVEC(3)         ! source direction (H-DELTA coords)
      REAL DCOMP_TEST0(20)         ! test parameters (global)
      INTEGER DCOMP_AFLAG(MNANT)   ! Antenna flags
      real dcomp_total(MNBB,mnant) ! total power for each antenna  
!
! NGRX 11.10.06 added M_POL_REC dimension to DH_ATFAC, DH_DELLIN, 
!               DH_DELAY, DH_RMSAMP, DH_RMSPHA
!
      REAL DCOMP_ATFAC(mrlband,2,MNANT)   ! applied atmosph. cal factor(U,L) 
      REAL DCOMP_RMSPE(2,MNANT)           ! rms pointing error (az,el)
      REAL DCOMP_DELCON(MNANT)            ! delay used for continuum (ns) **OBSOLETE**
      REAL DCOMP_DELLIN(MNBB,MNANT)       ! delay used for line (ns)
      REAL DCOMP_DELAYC(MNANT)     ! delay computed (ns)
      REAL DCOMP_DELAY(MNBB,MNANT) ! delay offset (ns)
      REAL DCOMP_PHASEC(MNANT)     ! 1st LO phase, computed (radians)
      REAL DCOMP_PHASE(MNANT)      ! 1st LO phase rotation offset (radians)
      REAL DCOMP_RATEC(MNANT)      ! 1st LO rate, computed (radians/sec)
      REAL DCOMP_CABLE(MNANT)      ! measured cable length (radians)
      INTEGER DCOMP_GAMME(MNANT)   ! phasemeter gamme (by antenna)
      REAL DCOMP_TEST1(20,MNANT)    ! test parameters (by antenna)
      REAL DCOMP_RMSAMP(M_POL_REC,2,MNBAS) ! rms amplitude (0 if no aver)
      REAL DCOMP_RMSPHA(M_POL_REC,2,MNBAS) ! rms phase (rad, 0 if no aver)
      REAL DCOMP_OFFFOC(MNANT)     ! focus offset (mm)
      REAL DCOMP_OFFLAM(MNANT)     ! lambda offset (rad)
      REAL DCOMP_OFFBET(MNANT)     ! beta offset (rad)
      INTEGER DCOMP_BFLAG(MNBAS)   ! Baseline flags
      REAL DCOMP_UVM(2,MNBAS)      ! Base line components (meters)
      COMPLEX DCOMP_INFAC(2,MNBAS)         ! applied instrum. cal factor (U,L) ** OBSOLETE**
!
! WVR 11.06.01 added new parameters
      REAL DCOMP_WVR(MWVRCH,MNANT)         ! counts of each WVR channel
      INTEGER DCOMP_WVRSTAT(MNANT)         ! status word  1 = noise tube on
                                           !              2 = ambient load on (commanded)
                                           !              4 = ambient load on (sensor)
                                           !              8 = Peltier Alarm on
!
! NGRX 22.08.06 added new parameter
      REAL DCOMP_ACTP(2,MNANT,MRLBAND)     ! total power measured by auto-correlator
      REAL DCOMP_ANTTP(MNIF,MNANT)         ! total power measured at antenna
      LOGICAL DCOMP_SAFLAG(2*MBANDS,MNANT) ! antenna spectral flags
      LOGICAL DCOMP_SBFLAG(2*MBANDS,MNBAS) ! baseline spectral flags
! Version 3 observation
      REAL DCOMP_CABLE_ALTERNATE(MNANT)    ! cable phase for other HiQ
      INTEGER DCOMP_NTIME                  ! Number of time values
      REAL DCOMP_TIME_MONITORING(MNTIME)   ! bure1 times
      REAL DCOMP_INCLI(MNANT,2)            ! inclinometer values
      REAL DCOMP_INCLI_TEMP(MNANT)         ! inclinometer temperature
      REAL DCOMP_TILFOC(MNANT,3)           ! secondary tilt     
      REAL DCOMP_OFFFOC_XY(MNANT,2)        ! focus offset X, Y (mm)
! Version 6 observation
      REAL DCOMP_VLBI_PHASEFF(MNBB)         ! Phasing efficiency
      REAL DCOMP_VLBI_PHASE_INC(MNANT,MNBB) ! Incremental INCA phase correction
      REAL DCOMP_VLBI_PHASE_TOT(MNANT,MNBB) ! Total INCA phase correction
!
! NOT IN DATA 
      CHARACTER*12 DCOMP_TIME              ! time 
      CHARACTER*12 DCOMP_DATE              ! date dd-mm-yyy 
      COMPLEX DCOMP_AVERAG(2,MNBAS)! average visibility
      REAL ALI                             ! dummy alignement
!
! COMMON defintion
! Must EXACTLY match the order and content of DH_COM in clic_dheader.inc !
!
      COMMON /DCOMP_COM/                                                &
     &DCOMP_VOID,DCOMP_DUMP,DCOMP_OBS,DCOMP_INTEG,DCOMP_UTC,            &
     &DCOMP_SVEC,DCOMP_TEST0,DCOMP_AFLAG,DCOMP_TOTAL,DCOMP_ATFAC,       &
     &DCOMP_RMSPE,DCOMP_DELCON,DCOMP_DELLIN,DCOMP_DELAYC,               &
     &DCOMP_DELAY,DCOMP_PHASEC,DCOMP_PHASE,DCOMP_RATEC,DCOMP_CABLE,     &
     &DCOMP_GAMME,DCOMP_TEST1,DCOMP_RMSAMP,DCOMP_RMSPHA,                &
     &DCOMP_OFFFOC,DCOMP_OFFLAM,DCOMP_OFFBET,DCOMP_BFLAG,DCOMP_UVM,     &
     &ALI,DCOMP_INFAC,                                                  &
     &DCOMP_WVR,DCOMP_WVRSTAT,DCOMP_ACTP,DCOMP_ANTTP,                   &
     &DCOMP_SAFLAG,DCOMP_SBFLAG,                                        &
     &DCOMP_CABLE_ALTERNATE,DCOMP_NTIME,DCOMP_TIME_MONITORING,          &
     &DCOMP_INCLI,DCOMP_INCLI_TEMP,DCOMP_TILFOC,DCOMP_OFFFOC_XY,        &
     &DCOMP_VLBI_PHASEFF,DCOMP_VLBI_PHASE_INC,DCOMP_VLBI_PHASE_TOT,     &
     &DCOMP_TIME,DCOMP_DATE,DCOMP_AVERAG
!
      SAVE /DCOMP_COM/
!------------------------------------------------------------------------
