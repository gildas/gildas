subroutine clic_plot(line,error)
  use gildas_def
  use gkernel_interfaces
  use gkernel_types
  use classic_api
  !---------------------------------------------------------------------
  !	PLOT [SAME] [obs_number] [NEXT] [FIRST] [CLOURE]
  !		/NOFRAME [pen]		option 1
  !		/RECORD			option 2
  !		/IDENTIFY		option 3
  !		/RESET			option 4
  !		/NODRAW			option 5
  !		/APPEND                 option 6
  !               /PHYSICAL               option 7
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_xy_code.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: ident, do_record, init_data, reset
  logical :: do_frame, nodraw, colour, closure, reset_display, power
  logical :: save_bin, physical, sort
  character(len=12) :: argum
  integer :: ic, iv, i, nnn
  integer :: pen_plot
  integer :: n_loop_save, rec_1_save(m_loop), rec_2_save(m_loop)
  integer :: rec_3_save(m_loop), nd, nysave, nch
  integer(kind=address_length) :: ipx,ipy,ipw
  type(sic_listi4_t) :: list
  integer :: mvocab, nkey, mvoc1
  parameter (mvocab=6, mvoc1=2)
  character(len=12) :: vocab(mvocab), key, voc1(mvoc1)
  data voc1/'COLOR','NOCOLOR'/
  data vocab/'ALL','CLOSURE','FIRST','NEXT','POWER','SAME'/
  data reset_display/.false./
  save reset_display
  !------------------------------------------------------------------------
  ! Code:
  ! Options:
  ! /NOFRAME pen
  do_frame = .not.sic_present(1,0)
  pen_plot = 0
  call sic_i4(line,1,1,pen_plot,.false.,error)
  if (.not.do_frame.and.many_pages) then
   call message(4,4,'CLIC_PLOT','Cannot use /NOFRAME option on a many pages plot')
   error = .true.
   return
  endif
  ! /IDENT colour
  ident = sic_present(3,0)
  argum = 'COLOR'
  call clic_kw(line,3,1,argum,nkey,voc1,mvoc1,.false.,error,.true.)
  if (error) return
  colour = argum.eq.'COLOR'
  ! /NODRAW
  nodraw = sic_present(5,0)
  ! /APPEND
  init_data = .not.sic_present(6,0)
  ! /RESET
  reset = sic_present(4,0)
  if (reset) then
    call reset_time
  endif
  ! /RECORD
  do_record = sic_present(2,1)
  if (do_record) then
    n_loop_save = n_loop
    do i=1, n_loop
      rec_1_save(i) = rec_1(i)
      rec_2_save(i) = rec_2(i)
      rec_3_save(i) = rec_3(i)
    enddo
    ic = sic_start(2,1)
    iv = sic_end(2,sic_narg(2))
    call sic_parse_listi4('CLIC_PLOT',line(ic:iv),list,m_loop,error)
    if (error) return
    n_loop = list%nlist
    rec_1(:) = list%i1(:)
    rec_2(:) = list%i2(:)
    rec_3(:) = list%i3(:)
  endif
  !  /PHYSICAL
  physical = sic_present(7,0).or.do_physical
  if (physical) then
    if (.not.phys_base) then
      phys_base = .true.
      call set_display(error)
    endif
  else
    if (phys_base) then
      phys_base = .false.
      call set_display(error)
    endif
  endif
  ! /NOSORT
  sort = .not.sic_present(9,0)
  !
  ! Command Argument
  argum = 'ALL'
  call sic_ke(line,0,1,argum,nch,.false.,error)
  if (error) return
  nnn = ichar(argum(1:1))
  if (nnn.ge.ichar('0') .and. nnn.le.ichar('9')) then
    key = argum
  else
    call sic_ambigs('PLOT',argum,key,nkey,vocab,mvocab,error)
    if (error) return
  endif
  if (key.eq.'SAME' .and. change_display) then
    call message(4,2,'CLIC_PLOT','Data needs to be read')
    argum(1:3) = 'ALL'
  endif
  if (reset_display .and. key.ne.'SAME') then
    call set_display(error)
    reset_display = .false.
  endif
  ! /BOXES
  if (sic_present(8,0)) then
    call sic_ke(line,8,1,argum,nch,.true.,error)
    if (argum.eq.'*') then
       user_box_nx = -1
    else
       call sic_i4(line,8,1,user_box_nx,.false.,error)
    endif
    call sic_ke(line,8,2,argum,nch,.true.,error)
    if (argum.eq.'*') then
       user_box_ny = -1
    else
      call sic_i4(line,8,2,user_box_ny,.false.,error)
    endif
    user_box = (user_box_nx.gt.0.or.user_box_ny.gt.0)
  else
    user_box = .false.
  endif
  !
  ! Deal with Title
  call gr_exec('SET VIEWPORT /DEF')
  ! Closure plot:
  ! set some defaults for plotting
  closure = .false.
  power = .false.
  if (key.eq.'CLOSURE') then
    call get_first(.false.,error)
    if (error) return
    change_display = .true.
    ! Y variables: amplitude and/or phase
    nysave = n_y
    n_y = 1
    if ((i_y(1).ne. xy_ampli).and.(i_y(1).ne. xy_phase)) then
      call message (8,3,'CLIC_PLOT',   &
        'Use SET Y AMPLITUDE or SET Y PHASE for closure plots')
      error = .true.
    endif
    ! all available baselines:
    n_base = r_nbas
    do i=1, n_base
      i_base(i) = i
    enddo
    call set_display(error)
    if (error) return
    closure = .true.
    key = 'ALL'
  elseif (key.eq.'POWER') then
    power = .true.
    key = 'ALL'
  endif
  !
  if (key.ne.'SAME') then
    if (plot_mode.eq.'TIME') then
      if (power) then
        save_bin = do_bin
        do_bin = .false.
      endif
      call read_data(key,sort,init_data,error)
      if (power) do_bin = save_bin
    elseif (plot_mode.eq.'SPEC') then
      call read_spec(key,init_data,error)
    endif
    if (error) return
  endif
  if (do_record) then
    n_loop = n_loop_save
    do i=1, n_loop
      rec_1(i) = rec_1_save(i)
      rec_2(i) = rec_2_save(i)
      rec_3(i) = rec_3_save(i)
    enddo
  endif
  change_display = .false.
  clear = (clear .or. key.eq.'ALL' .or. key.eq.'SAME')   &
    .and. do_frame
  if (nodraw) return
  if (closure) then
    ipx = gag_pointer(data_x,memory)
    ipy = gag_pointer(data_y,memory)
    ipw = gag_pointer(data_w,memory)
    nd = m_data
    if (i_y(1).eq.xy_phase.and.r_nant.ge.3) then
      call sub_clos_pha(r_nant,nd,n_base,   &
        memory(ipx),memory(ipy),memory(ipw),error)
    elseif (r_nant.ge.4) then
      call sub_clos_amp(r_nant,nd,n_base,   &
        memory(ipx),memory(ipy),memory(ipw),error)
    endif
    if (error) return
    call sub_resetvar(m_data,m_boxes,   &
      memory(ipx),memory(ipy),memory(ipw),error)
    if (error) return
    call position_boxes(error)
    if (error) return
  elseif (power) then
    call plot_power(error)
    if (error) return
  endif
  call sub_plot(key, ident, colour, pen_plot, error)
  if (closure) then
    n_y = nysave
    reset_display = .true.
  endif
  user_box = .false.
  return
end subroutine clic_plot
!
subroutine sub_plot(argum, ident, colour, pen_plot, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! this intermediate entry point needed for solve ... /plot
  !---------------------------------------------------------------------
  character(len=*) :: argum         !
  logical :: ident                  !
  logical :: colour                 !
  integer :: pen_plot               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_xy_code.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy,ipw,ipi,ips,ipj
  !-----------------------------------------------------------------------
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ipi = gag_pointer(data_i,memory)
  ips = gag_pointer(data_s,memory)
  ipj = ips + m_data*m_boxes
  call sub_sub_plot(m_data, m_boxes, memory(ipx), memory(ipy),   &
    memory(ipw), memory(ipi), memory(ips),   &
    memory(ipj), argum, ident, colour, pen_plot,   &
    error)
  return
end subroutine sub_plot
!
subroutine sub_clos_pha(nant,nd,nb,x,y,w,error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! same comment as for amplitudes.
  !---------------------------------------------------------------------
  integer :: nant                   !
  integer :: nd                     !
  integer :: nb                     !
  real :: x(nd,nb)                  !
  real :: y(nd,nb)                  !
  real :: w(nd,nb)                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_number.inc'
  ! Local
  integer :: i, k, id, nclos, kbox, kb, islash, ii, nt
  integer :: jcl(3,mnbas), j1, j2, j3, ib, kk
  real :: xc(mnbas),yc(mnbas),wc(mnbas),p, ww(3), yy(3), www, xx
  character(len=3) :: cc,ccl(mnbas)
  character(len=80) :: ch
  logical :: new(mnbas)
  !-----------------------------------------------------------------------
  kbox = n_boxes/n_base
  if (degrees) then
    p = 180.
  else
    p = pi
  endif
  !
  ! Build the relations table starting from the list of triangles:
  nt = nant*(nant-1)*(nant-2)/6
  nclos = 0
  do i=1, mnbas
    new(i) = .true.
  enddo
  do k=1, nt
    j1 = bastri(1,k)
    j2 = bastri(2,k)
    j3 = bastri(3,k)
    write(cc,'(4i1)') anttri(1,k),anttri(2,k),anttri(3,k)
    if (new(j1).or.new(j2).or.new(j3)) then
      nclos = nclos+1
      new(j1) = .false.
      new(j2) = .false.
      new(j3) = .false.
      jcl(1,nclos) = j1
      jcl(2,nclos) = j2
      jcl(3,nclos) = j3
      ccl(nclos) = cc
    endif
  enddo
  !
  do ib=1, kbox
    do k=1, nclos
      kb = (k-1)*kbox+ib
      header_1(kb) = 'Clos '//ccl(k)
      header_2(kb) = header_2(ib)
      header_3(kb) = header_3(ib)
      x_label(kb)  = x_label(ib)
      y_label(kb)  = y_label(ib)
      write (ch,1005) kb, kb
      islash = index(c_limits(ib),'/')-1
      c_limits(kb) = c_limits(ib)(1:islash)//ch(1:lenc(ch))
    enddo
  enddo
  !
  do id=1, n_data(k)
    do ib=1, kbox
      do i=1, nclos
        kb = (i-1)*kbox+ib
        do kk=1,3
          ii = (jcl(kk,i)-1)*kbox+ib
          yy(kk) = y(id,ii)
          ww(kk) = w(id,ii)
          xx = x(id,ii)
        enddo
        www = ww(1)*ww(2)*ww(3)
        if (www.gt.0) then
          yc(i) = mod(yy(1)-yy(2)+yy(3)+11*p,2*p)-p
          wc(i) = 1./(1/ww(1)+1/ww(2)+1/ww(3))
          xc(i) = xx
        else
          yc(i) = blank4
          wc(i) = 0
          xc(i) = blank4
        endif
      enddo
      do i=1, nclos
        ii = (i-1)*kbox+ib
        y(id,ii) =  yc(i)
        w(id,ii) =  wc(i)
        x(id,ii) =  xc(i)
      enddo
    enddo
  enddo
  n_boxes = nclos*kbox
  !
  ! reset the variables
  call sub_resetvar(nd,n_boxes,x,y,w,error)
  return
1005 format(' /VARIABLE X_DATA[',i4.4,'] Y_DATA[',i4.4,']')
end subroutine sub_clos_pha
!
subroutine sub_clos_amp(nant,nd,nb,x,y,w,error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! take only 2 closures for each quadrangle (13.24/12.34 and 14.23/12.34)
  ! this would produce n*(n-1)*(n-2)*(n-3)|(4*3) closure
  ! relations of which only n*(n-3)|2 are independent. For 5 antennas these
  ! numbers are 10 and 5. For more than 5 antennas one has to sort out
  ! dependent relations because there will be more clos. relations produced
  ! than available baselines.
  ! Hint : start with 2 independent relations in a given quadrangle, and add
  ! relations with additional baselines; using only once each new baseline
  ! should give independent relations:
  !  (n-1)(n-4)|2 relations between n-1 antennas
  ! there are n-1 new baselines with a nth antenna
  ! add (n-2) relations (the first has 2 new baselines)
  ! one gets (n-1)(n-4)|2 + n-2 = (n^2-5n+4 +2n-4)|2 = n*(n-3)|2 relations.
  !---------------------------------------------------------------------
  integer :: nant                   !
  integer :: nd                     !
  integer :: nb                     !
  real :: x(nd,nb)                  !
  real :: y(nd,nb)                  !
  real :: w(nd,nb)                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_number.inc'
  ! Local
  integer ::  i, id, nclos, k, kb, kbox, l, islash, ii, nq
  integer ::  jcl(4,mnbas), j1, j2, j3, j4, ib, kk
  real :: xc(mnbas),yc(mnbas), wc(mnbas), yy(4), ww(4)
  real :: www, yyy, xx
  character(len=4) :: cc,ccl(mnbas)
  character(len=80) :: ch
  logical :: new(mnbas)
  !-----------------------------------------------------------------------
  kbox = n_boxes/n_base
  !
  ! Build the relations table starting from the list of quadrangles:
  nq = nant*(nant-1)*(nant-2)*(nant-3)/24
  nclos = 0
  do i=1, mnbas
    new(i) = .true.
  enddo
  !
  do k=1, nq
    j1 = basquad(1,k)
    j2 = basquad(6,k)
    do l=1, 2
      if (l.eq.1) then         ! (A_12*A_34)/(A_13*A_24)
        j3 = basquad(2,k)
        j4 = basquad(5,k)
        write(cc,'(4i1)') antquad(1,k),antquad(2,k),   &
          antquad(3,k),antquad(4,k)
      else                     ! (A_12*A_34)/(A_14*A_23)
        j3 = basquad(3,k)
        j4 = basquad(4,k)
        write(cc,'(4i1)') antquad(1,k),antquad(2,k),   &
          antquad(4,k),antquad(3,k)
      endif
      if (new(j1).or.new(j2).or.new(j3).or.new(j4)) then
        nclos = nclos+1
        new(j1) = .false.
        new(j2) = .false.
        new(j3) = .false.
        new(j4) = .false.
        jcl(1,nclos) = j1
        jcl(2,nclos) = j2
        jcl(3,nclos) = j3
        jcl(4,nclos) = j4
        ccl(nclos) = cc
      endif
    enddo
  enddo
  !
  do ib=1, kbox
    do k=1, nclos
      kb = (k-1)*kbox+ib
      header_1(kb) = 'Clos '//ccl(k)
      header_2(kb) = header_2(ib)
      header_3(kb) = header_3(ib)
      x_label(kb)  = x_label(ib)
      y_label(kb)  = y_label(ib)
      write (ch,1005) kb, kb
      islash = index(c_limits(ib),'/')-1
      c_limits(kb) = c_limits(ib)(1:islash)//ch(1:lenc(ch))
    enddo
  enddo
  !
  do id=1, n_data(k)
    do ib=1, kbox
      do i=1, nclos
        kb = (i-1)*kbox+ib
        do kk=1,4
          ii = (jcl(kk,i)-1)*kbox+ib
          yy(kk) = y(id,ii)
          ww(kk) = w(id,ii)
          xx = x(id,ii)
        enddo
        www = ww(1)*ww(2)*ww(3)*ww(4)
        yyy = yy(1)*yy(2)*yy(3)*yy(4)
        if (www.gt.0) then
          if (yyy.ne.0) then
            do kk=1,4
              ww(kk) = 1./ww(kk)/yy(kk)**2
            enddo
            yc(i) = (yy(1)*yy(2))/(yy(3)*yy(4))
            wc(i) = 1./(ww(1)+ww(2)+ww(3)+ww(4))/yc(i)**2
            xc(i) = xx
          endif
        else
          yc(i) = blank4
          wc(i) = 0
          xc(i) = blank4
        endif
      enddo
      do i=1, nclos
        ii = (i-1)*kbox+ib
        y(id,ii) =  yc(i)
        w(id,ii) =  wc(i)
        x(id,ii) =  xc(i)
      enddo
    enddo
  enddo
  n_boxes = nclos*kbox
  !
  ! reset the variables
  call sub_resetvar(nd,n_boxes,x,y,w,error)
  return
1005 format('/VARIABLE X_DATA[',i4.4,'] Y_DATA[',i4.4,']')
end subroutine sub_clos_amp
!
subroutine sub_sub_plot(md,mb,x_data,y_data,w_data,   &
    i_data,b_data,j_data,argum,ident, colour, pen_plot, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_index
  use clic_title
  !---------------------------------------------------------------------
  ! This version for GREG1 Version 7.2 and later : call list of
  ! GR4_HISTO was incorrect previously.
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: i_data(md,mb)             !
  real :: b_data(md,mb)             !
  integer :: j_data(mb)                !
  character(len=*) :: argum         !
  logical :: ident                  !
  logical :: colour                 !
  integer :: pen_plot               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  ! Local
  type(title_v2_t) :: title_save
  integer :: i, l, l1, j, lc, isides, istyle, sides0, style0, k, n
  integer :: nch, ix, iy, nplot, mplot, iscan
  integer :: i_data_dumm(md)
  real :: tick(4)
  character(len=132) :: chain,ch1,ch2,ch3
  character(len=4) :: cloc
  character(len=3) :: italic
  character(len=12) :: answer
  integer :: nch1, nch2, nch3, old_pen, status
  real*4 :: cdef, cdef0, cdef1
  real*4 :: xmax, ymax, size0, sl, sl1, sl2, sl3, bx1, bx2, by1, by2
  real*4 :: xmax1, ymax1, xx1(mbox), xx2(mbox), yy1(mbox), yy2(mbox)
  real*4 :: dummy(1)
  logical :: angle_data, good(mbox)
  logical :: fatm, fwvr, fmon
  !------------------------------------------------------------------------
  save xmax1, ymax1
  data xmax1/0.0/, ymax1/0.0/
  ! 
  ! Save the current short tile
  title_save = title
  !
  ! Code:
  italic = char(92)//char(92)//'i'
  do i=1, n_x
    xx1(i) = 1e30
    xx2(i) = -1e30
  enddo
  do i=1, n_y
    yy1(i) = 1e30
    yy2(i) = -1e30
  enddo
  mplot = 0
  do i=1, n_boxes
    nplot = 0
    do j = 1, n_data(i)
      if (abs(y_data(j,i)-blank4).gt.d_blank4) then
        nplot = nplot + 1
      endif
    enddo
    if (nplot.le.0) then
      write(chain,'(A,I0)') 'No data in box ',i
      call message(8,2,'SUB_PLOT',chain(1:lenc(chain)))
    endif
    good(i) = nplot.gt.0
    mplot = mplot + nplot
  enddo
  if (mplot.le.0) then
    call message(8,2,'SUB_PLOT','No data to plot')
    call gtclear  
    error = .true.
    return
  endif
  call gr8_blanking(blank8,d_blank8)
  if (clear) then
    call gtclear
    error = gr_error()
    if (error) goto 999
    call sic_get_real('CHARACTER_SIZE',cdef0,error)
    call sic_get_real('PAGE_X',xmax,error)
    call sic_get_real('PAGE_Y',ymax,error)
    if (xmax1.ne.0 .and. (xmax.ne.xmax1 .or. ymax.ne.ymax1)) then
      call set_display(error)
      if (error) return
    endif
    xmax1 = xmax
    ymax1 = ymax
    !
    ! Central header should fit in (0.70*xmax,0.08*xmax)
    cdef = 0.08*xmax/3.0       ! space for 3 lines
    !
    ! First line
    call user_info(ch1)
    nch1 = lenc(ch1)
    ch1 = ch1(1:nch1)//'   '   &
      //r_config(1:lenc(r_config))//' '   &
      //r_teles
    nch1 = lenc(ch1)
    call gtg_charlen(nch1,ch1(1:nch1),cdef,sl1,0)
    !
    ! Second and third line -- Old receivers
    if (.not.new_receivers) then
      if ((argum.eq.'ALL' .or. argum.eq.'SAME')   &
        .and. cxnext.gt.2) then
        call rix(cx_ind(1),error)
      else
        call rix(r_xnum,error)
      endif
      call out0('Chain',xmax/2,ymax-cdef,error)
      call titout('Normal',error)
      call out2(ch2,error)
      nch2 = lenc(ch2)
      call gtg_charlen(nch2,ch2(1:nch2),cdef,sl2,0)
      if ((argum.eq.'ALL' .or. argum.eq.'SAME')   &
        .and. cxnext.gt.2) then
        call rix(cx_ind(cxnext-1),error)
        call out0('Chain',xmax/2,ymax-cdef,error)
        call titout('Normal',error)
        call out2(ch3,error)
        nch3 = lenc(ch3)
        call gtg_charlen(nch3,ch3(1:nch3),cdef,sl3,0)
      else
        sl3 = 0
      endif
    !
    ! Second and third lines -- New receivers
    else
      call rix(cx_ind(1),error)
      call out0('Chain',xmax/2,ymax-cdef,error)
      call titout('Plot',error)
      call out2(ch2,error)
      nch2 = lenc(ch2)
      call gtg_charlen(nch2,ch2(1:nch2),cdef,sl2,0)
      !
      call rix(cx_ind(cxnext-1),error)
      call out0('Chain',xmax/2,ymax-cdef,error)
      call titout('Blot',error)
      call out2(ch3,error)
      nch3 = lenc(ch3)
      call gtg_charlen(nch3,ch3(1:nch3),cdef,sl3,0)
    endif
    !
    ! Now print the two or three lines
    sl = max(sl1, sl2, sl3)
    if (sl.gt.0.70*xmax) then
      cdef = cdef*0.70*xmax/sl
    endif
    write (chain,'(A,F7.3)') 'SET CHAR ',cdef
    call gr_exec1 (chain(1:lenc(chain)))
    call grelocate(xmax*0.5-0.1*cdef,ymax)
    call gr_labe_cent(2)
    call gr_labe(italic//ch1(1:nch1))
    call grelocate(xmax/2.,ymax-cdef)
    call gr_labe_cent(2)
    call gr_labe(ch2(1:nch2))
    if (sl3.gt.0) then
      call grelocate(xmax/2.,ymax-cdef*2)
      call gr_labe_cent(2)
      call gr_labe(ch3(1:nch3))
    endif
    !
    ! Corner labels (Left)
    cdef = 0.08*xmax/4         ! space for 4 lines
    call gtg_charlen(13,'XXXXXXXXXXXXXXXXXX',cdef,sl,0)
    if (sl.gt.0.12*xmax) then
      cdef = cdef*0.12*xmax/sl
    endif
    write (chain,'(A,F7.3)') 'SET CHAR ',cdef
    call gr_exec1 (chain(1:lenc(chain)))
    call grelocate(0.1,ymax)
    call gr_labe_cent(3)
    call gr_labe(italic//'RF:')
    call grelocate(0.1,ymax-cdef)
    call gr_labe_cent(3)
    call gr_labe(italic//'Am:')
    call grelocate(0.1,ymax-2*cdef)
    call gr_labe_cent(3)
    call gr_labe(italic//'Ph:')
    !
    !RF
    if (do_pass) then
      if (do_pass_freq) then
        chain = 'Fr.'
      else
        chain = 'Ch.'
      endif
      if (do_pass_antenna) then
        chain(4:6) = '(A)'
      else
        chain(4:6) = '(B)'
      endif
      if (do_pass_spectrum) then
        iscan = 0
        call get_pass_scan(chain,iscan,error)
      endif
    else
      chain = 'Uncal.'
    endif
    call grelocate(3*cdef,ymax)
    call gr_labe_cent(3)
    call gr_labe(chain(1:10))
    !Amp.
    if (do_amplitude) then
      if (do_amplitude_antenna) then
        chain = 'Rel.(A)'
      else
        chain = 'Rel.(B)'
      endif
    elseif (do_scale) then
      chain = 'Scaled'
    else
      chain = 'Abs.   '
    endif
    call grelocate(3*cdef,ymax-cdef)
    call gr_labe_cent(3)
    call gr_labe(chain(1:7))
    if (do_phase) then
      if (do_phase_antenna) then
        chain = 'Rel.(A)'
      else
        chain = 'Rel.(B)'
      endif
    else
      chain = 'Abs.'
    endif
    fatm = .false.
    do i=1, r_nant
      fatm = fatm.or.do_phase_atm(i)
    enddo
    if (fatm) then
      lc = lenc(chain)
      chain = chain(1:lc)//' Atm.'
    endif
    fwvr = .false.
    do i=1, r_nant
      fwvr = fwvr.or.do_phase_wvr(i)
    enddo
    if (fwvr) then
      lc = lenc(chain)
      chain = chain(1:lc)//' Wvr.'
    endif
    fmon = .false.
    do i=1, r_nant
      fmon = fmon.or.do_phase_mon(i)
    enddo
    if (fmon) then
      lc = lenc(chain)
      chain = chain(1:lc)//' Tot.'
    endif
    if (do_phase_ext) then
      lc = lenc(chain)
      chain = chain(1:lc)//' Ext.'
    endif
    lc = lenc(chain)
    call grelocate(3*cdef,ymax-2*cdef)
    call gr_labe_cent(3)
    call gr_labe(chain(1:lc))
    !
    ! Corner labels (Right)
    ! -- Averaging
    if (i_average.eq.0) then
      chain = 'No Avg.'
    elseif (i_average.eq.1) then
      chain = 'Scan Avg.'
    elseif (i_average.eq.2) then
      write(chain,'(A,F5.0,A)') 'T.Av:',t_average,' s.'
    endif
    call grelocate(xmax,ymax)
    call gr_labe_cent(1)
    nch = lenc(chain)
    call gr_labe(chain(1:nch))
    ! Obsolete ?
    !         IF (J_AVERAGE.EQ.0) THEN
    !            CHAIN = 'Scal.Avg.'
    !         ELSE
    !            CHAIN = 'Vect.Avg.'
    !         ENDIF
    !         CALL GRELOCATE(XMAX,YMAX-CDEF)
    !         CALL GR_LABE_CENT(1)
    !         CALL GR_LABE(CHAIN(1:9))
    !
    ! -- Phys. antenna
    if (phys_base) then
      old_pen = gr_spen(1)
      chain = 'PHYSICAL ant.'
      call grelocate(xmax,ymax-cdef)
      call gr_labe_cent(1)
      call gr_labe(chain(1:13))
      old_pen = gr_spen(old_pen)
      ymax = ymax-cdef
    endif
    !
    ! -- Subbands selection
    if (new_receivers) then
      chain = ' '
      if (do_polar.ne.0.or.each_polar) then
        if (do_polar.eq.1) then
          chain = 'HORIZONTAL pol.'
        elseif (do_polar.eq.2) then
          chain = 'VERTICAL pol.'
        elseif (do_polar.eq.3) then
          chain = 'BOTH polarizations'
        endif
        if (each_polar) then
          chain = 'EACH polar'
        endif
        old_pen = gr_spen(1)
        call grelocate(xmax,ymax-cdef)
        call gr_labe_cent(1)
        call gr_labe(chain(1:lenc(chain)))
        old_pen = gr_spen(old_pen)
      endif
      if (narrow_input.ne.0) then
        if (narrow_input.eq.1) then
          chain = 'Narrow Input 1'
        elseif (narrow_input.eq.2) then
          chain = 'Narrow Input 2'
        elseif (narrow_input.eq.3) then
          chain = 'Narrow Input 1+2'
        endif
        old_pen = gr_spen(1)
        call grelocate(xmax,ymax-cdef)
        call gr_labe_cent(1)
        call gr_labe(chain(1:lenc(chain)))
        old_pen = gr_spen(old_pen)
      endif
      if (widex_unit.ne.0) then
        write (chain,'(a,i1)')'WIDEX Unit ',widex_unit
        old_pen = gr_spen(1)
        call grelocate(xmax,ymax-cdef)
        call gr_labe_cent(1)
        call gr_labe(chain(1:lenc(chain)))
        old_pen = gr_spen(old_pen)
      endif
    endif
  endif
  !
  ! Recover the data if PLOT SAME
  if (argum.eq.'SAME') then
    call getvar(md,mb,x_data,y_data,w_data)
  endif
  !
  ! Global limits (using e.g. "/LIMITS 0 =")
  do i=1, n_boxes
    if (good(i) .and. n_data(i).gt.0) then
      l = lenc(c_limits(i))
      call gr_exec1 (c_limits(i)(1:l))
      error = gr_error()
      if (error) goto 999
      call save_limits(i,error)
      ix = k_x(i)
      if (sm_x1(ix).eq.'=') then
        xx1(ix) = min(xx1(ix),gb1_x(i))
      endif
      if (sm_x2(ix).eq.'=') then
        xx2(ix) = max(xx2(ix),gb2_x(i))
      endif
      iy = k_y(i)
      if (sm_y1(iy).eq.'=') then
        yy1(iy) = min(yy1(iy),gb1_y(i))
      endif
      if (sm_y2(iy).eq.'=') then
        yy2(iy) = max(yy2(iy),gb2_y(i))
      endif
    endif
  enddo
  do i=1, n_boxes
    ix = k_x(i)
    if (sm_x1(ix).eq.'=') then
      gb1_x(i) = xx1(ix)
    elseif (sm_x1(ix).eq.'F') then
      gb1_x(i) = gu1_x(ix)
    endif
    if (sm_x2(ix).eq.'=') then
      gb2_x(i) = xx2(ix)
    elseif (sm_x2(ix).eq.'F') then
      gb2_x(i) = gu2_x(ix)
    endif
    iy = k_y(i)
    if (sm_y1(iy).eq.'=') then
      gb1_y(i) = yy1(iy)
    elseif (sm_y1(iy).eq.'F') then
      gb1_y(i) = gu1_y(iy)
    endif
    if (sm_y2(iy).eq.'=') then
      gb2_y(i) = yy2(iy)
    elseif (sm_y2(iy).eq.'F') then
      gb2_y(i) = gu2_y(iy)
    endif
  enddo
  !
  ! Recompute The positions of boxes if needed
  if (user_box) then
    call position_boxes_user(user_box_nx,user_box_ny,error)
  else
    call position_boxes(error)
  endif
  !
  k = 0
  many_pages = .false.
  do i=1, n_boxes
    k = k + 1
    if (user_box.and.k.gt.user_nboxes) then
      answer=' '
      do while (answer(1:1).ne."C")
        call sic_wprn('I-PLOT, Type C to continue',answer,n)
        if (answer(1:1).eq."Q") return
      enddo
      call gr_execl('CHANGE DIRECTORY')
      do j=1, user_nboxes
         write(chain,'(I4.4)') j
         call gr_execl('DESTROY DIRECTORY BOX'//chain)
      enddo
      many_pages = .true.
      change_display = .true.
      k = 1
    endif
    call gr_execl('CHANGE DIRECTORY')
    error = gr_error()
    if (error) goto 999
    !
    write(chain,'(I4.4)') k
    if (clear) then
      call gr_execl('CREATE DIRECTORY BOX'//chain)
      error = gr_error()
      if (error) goto 999
    endif
    call gr_execl('CHANGE DIRECTORY BOX'//chain)
    error = gr_error()
    if (error) goto 999
    if (n_data(i).le.0.or..not.good(i)) then
      goto 800
    endif
    !
    if (clear) then
      l = lenc(c_setbox(k))
      call gr_exec  (c_setbox(k)(1:l))
      error = gr_error()
      if (error) goto 999
      write(chain,'(A,4(1X,G19.12))') 'LIMITS ',gb1_x(i),   &
        gb2_x(i),gb1_y(i),gb2_y(i)
      l = lenc(chain)
      call gr_exec(chain(1:l))
      error = gr_error()
      if (error) goto 999
      call sic_get_real('BOX_XMIN',bx1,error)
      call sic_get_real('BOX_XMAX',bx2,error)
      call sic_get_real('BOX_YMIN',by1,error)
      call sic_get_real('BOX_YMAX',by2,error)
      cdef = 0.20*(by2-by1)
      cdef1 = min(0.1*(by2-by1),0.1*(bx2-bx1))
      nch1 = lenc(header_1(i))
      nch2 = lenc(header_2(i))
      nch3 = lenc(header_3(i))
      call gtg_charlen(nch1,header_1(i)(1:nch1),cdef,sl1,0)
      call gtg_charlen(nch2,header_2(i)(1:nch2),cdef,sl2,0)
      call gtg_charlen(nch3,header_3(i)(1:nch3),cdef,sl3,0)
      sl = max(2*sl1+sl2,2*sl3+sl2)
      if (sl.gt.0.95*(bx2-bx1)) then
        cdef = cdef*0.95*(bx2-bx1)/sl
      endif
      write (chain,'(A,F7.3)') 'SET CHAR ',cdef1
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      ix = i_x(k_x(i))
      iy = i_y(k_y(i))
      do j=1, 4
        tick (j) = 0.0
      enddo
      ! Convert to degrees if angle data
      if (angle_data(ix)) then
        if (degrees.and.abs(gb1_x(i)-gb2_x(i)).gt.180.) then
          tick(2) = 90.
          do while (abs(gb1_x(i)-gb2_x(i))/tick(2).gt.5)
            tick(2) = tick(2)*2.0
          enddo
          tick(1) = tick(2)/3.0
        endif
      endif
      if (angle_data(iy)) then
        if (degrees.and.abs(gb1_y(i)-gb2_y(i)).gt.180.) then
          tick(4) = 90.
          do while (abs(gb1_y(i)-gb2_y(i))/tick(4).gt.5)
            tick(4) = tick(4)*2.0
          enddo
          tick(3) = tick(4)/3.0
        endif
      endif
      write (chain,'(A,4(1X,F10.1))') 'TICKSPACE ',tick
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      call gr_exec1 ('BOX')
      error = gr_error()
      if (error) goto 999
      ! Box Header
      write (chain,'(A,F7.3)') 'SET CHAR ',cdef
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      l = lenc(header_1(i))
      write(chain,'(A,F4.2,A)') 'DRAW TEXT 0 ',0.2*cdef,' "'
      call gr_exec1 (chain(1:18)//header_1(i)(1:l)//'" 9 /BOX 7')
      error = gr_error()
      if (error) goto 999
      l = lenc(header_2(i))
      call gr_exec1 (chain(1:18)//header_2(i)(1:l)//'" 8 /BOX 8')
      error = gr_error()
      if (error) goto 999
      l = lenc(header_3(i))
      call gr_exec1 (chain(1:18)//header_3(i)(1:l)//'" 7 /BOX 9')
      error = gr_error()
      if (error) goto 999
      write (chain,'(A,F7.3)') 'SET CHAR ',cdef1
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      l = lenc(x_label(i))
      l1 = lenc(y_label(i))
      call gr_exec1 ('LABEL "'//y_label(i)(1:l1)//'  vs.  '   &
        //x_label(i)(1:l)//'" /X')
      error = gr_error()
      if (error) goto 999
    endif
    !
    ! change the pen here for overlay plots
    if (.not.clear) then
      old_pen = gr_spen(pen_plot)
    endif
    !         IF (ARGUM.EQ.'SAME') THEN
    !            CALL GETVAR(MD,MB,X_DATA,Y_DATA,W_DATA)
    !         ENDIF
    if (plot_type.eq.'LINE') then
      call gr_segm('PLOT',error)
      call gr4_connect (n_data(i),   &
        x_data(1,i),y_data(1,i),   &
        blank4,d_blank4)
      call gr_segm_close(error)
    elseif (plot_type.eq.'POIN') then
      if (.not.ident) then
        call gr_segm('PLOT',error)
        call gr4_marker (n_data(i), x_data(1,i),y_data(1,i),   &
          blank4,d_blank4)
        call gr_segm_close(error)
      else
        i_data_dumm(:) = i_data(:,i)
        call gr4_ident (n_data(i), x_data(1,i),y_data(1,i),   &
          i_data_dumm(1),j_data(1),colour,.false.,b_data(1,i),   &
          error)
      endif
    elseif (plot_type.eq.'HIST') then
      call gr_segm('PLOT',error)
      call gr4_histo (n_data(i), x_data(1,i),y_data(1,i),   &
        blank4,d_blank4)
      call gr_segm_close(error)
    elseif (plot_type.eq.'BARS') then
      do j = 1, n_data(i)
        if (w_data(j,i).gt.0.0) then
          b_data(j,i) = 1.0e-3/sqrt(w_data(j,i))
        else
          b_data(j,i) = 0.0
        endif
      enddo
      cloc = 'Y'
      if (.not.ident) then
        call gr_segm('PLOT',error)
        call gr4_bars (cloc,n_data(i),   &
          x_data(1,i),y_data(1,i),b_data(1,i),dummy,   &
          blank4,d_blank4)
        call gr4_marker (n_data(i),x_data(1,i),y_data(1,i),   &
          blank4,d_blank4)
        call gr_segm_close(error)
      else
        i_data_dumm(1:md) = i_data(1:md,i)
        call gr4_ident (n_data(i),x_data(1,i),y_data(1,i),   &
          i_data_dumm(1),j_data(1),colour,.true.,b_data(1,i),   &
          error)
      endif
    endif
    !
    ! reset the pen here for overlay plots
    if (.not.clear) then
      old_pen = gr_spen(old_pen)
    endif
    !
800 continue
  enddo
  !
  if (ident) then
    print *,'Ident ',cdef1
    write (chain,'(A,F7.3)') 'SET CHAR ',cdef1
    call gr_exec1 (chain(1:lenc(chain)))
    error = gr_error()
    if (error) goto 999
    call gr_get_marker(sides0,style0,size0)
    old_pen = gr_spen(0)
    do i=1, n_ident
      isides = (i-1)/4 + 3
      istyle = i - (isides-3)*4 -1
      call gr_set_marker(isides,istyle,size0*3)
      if (colour) then
        status = gr_spen(mod(i-1,7))
      endif
      write (chain,'(A,1PG13.6,1X,1PG13.6,a)') 'DRAW MARKER ',   &
        0.5*cdef1,-i*cdef1,' /BOX 7'
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      write (chain,'(A,1PG13.6,1X,1PG13.6,a)') 'DRAW RELOCATE ',   &
        1.5*cdef1,-i*cdef1,' /BOX 7'
      call gr_exec1 (chain(1:lenc(chain)))
      error = gr_error()
      if (error) goto 999
      lc = lenc(ch_ident(i))
      call gr_labe_cent(6)
      call gr_labe(ch_ident(i)(1:lc))
    enddo
    call gr_set_marker(sides0,style0,size0)
    if (colour) then
      status = gr_spen(old_pen)
    endif
  endif
  call gr_execl('CHANGE DIRECTORY')
  call gr_exec1('LIMITS 0 1 0 1')
  error = gr_error()
  if (error) goto 999
  if (clear) then
    write (chain,'(A,F7.3)') 'SET CHAR ',cdef0
    call gr_exec1 (chain(1:lenc(chain)))
    error = gr_error()
    if (error) goto 999
  endif
999 continue
  !
  ! For consistency of further commands restore saved title
  title = title_save
  clear = .false.
end subroutine sub_sub_plot
!
subroutine save_limits(ibox,error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Save current GREG plot limits into display common, for box # IBOX
  !---------------------------------------------------------------------
  integer :: ibox                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  real :: x1, x2, y1, y2
  !------------------------------------------------------------------------
  ! Code:
  call sic_get_real('USER_XMIN',x1,error)
  call sic_get_real('USER_XMAX',x2,error)
  if (x2.gt.x1) then
    gb1_x(ibox) = x1
    gb2_x(ibox) = x2
  endif
  call sic_get_real('USER_YMIN',y1,error)
  call sic_get_real('USER_YMAX',y2,error)
  if (y2.gt.y1) then
    gb1_y(ibox) = y1
    gb2_y(ibox) = y2
  endif
  return
end subroutine save_limits
!
subroutine gr4_ident (np,x,y,id,ind,colour,bars,b,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Plot data with identifying markers
  !---------------------------------------------------------------------
  integer :: np                     ! number of points
  real :: x(np)                     ! coordinates
  real :: y(np)                     ! coordinates
  integer :: id(np)                 ! identifying numbers
  integer :: ind(np)                ! identifying numbers
  logical :: colour                 !
  logical :: bars                   !
  real :: b(np)                     ! Error bars
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'      ! needed for blank4, ...
  ! Local
  real :: size,dummy(1)
  integer :: i, j, sides, style, idlast, idsave, old_pen
  integer :: status, k, nc
  character(len=48) :: chain
  character(len=4) :: cloc
  !------------------------------------------------------------------------
  ! Code:
  ! first sort data according to ID (indexes in IND)
  call gi4_trie(id,ind,np,error)
  if (error) then
    call message(8,4,'GR4_IDENT','Error in sorting points')
    return
  endif
  !
  if (colour) then
    cloc = 'Y'
    !
    ! Compute maximum number of colours
    nc = 0
    do i=1,np
      nc = max(nc,id(i))
    enddo
    old_pen = gr_spen(0)
    !
    do k=1,nc
      status = gr_spen(mod(k-1,7))
      call gr_segm('IDENT',error)
      do i=1,np
        if (id(i).eq.k) then
          j = ind(i)
          call gr4_marker (1, x(j),y(j),   &
            blank4,d_blank4)
          if (bars) then
            call gr4_bars (cloc,1,   &
              x(j),y(j),b(j),dummy,   &
              blank4,d_blank4)
          endif
        endif
      enddo
      call gr_segm_close(error)
    enddo
    status = gr_spen(old_pen)
  else
    call gr_get_marker(sides,style,size)
    idlast = style + 1 + (sides-3)*4
    idsave = idlast
    call gr_set_marker(sides,style,size*3.0)
    do i=1, np
      if (id(i).ne.idlast) then
        idlast = id(i)
        sides = (idlast-1)/4 + 3
        style = idlast - (sides-3)*4 -1
        call gr_set_marker(sides,style,size*3.0)
        if (colour) then
          status = gr_spen(mod(idlast-1,7))
        endif
      endif
      j = ind(i)
      write(chain,100) x(j),y(j)
      call gr_exec1(chain)
      error = gr_error()
      if (error) return
    enddo
    sides = (idsave-1)/4 + 3
    style = idsave - (sides-3)*4 -1
    call gr_set_marker(sides,style,size)
  endif
  return
  !
100 format('DRAW MARKER ',1pg13.6,1x,1pg13.6,' /USER')
end subroutine gr4_ident
!
subroutine user_info(name)
  use gkernel_interfaces
  character(len=*) :: name          !
  ! Local
  character(len=20) :: date,user
  !------------------------------------------------------------------------
  ! Code:
  call sic_date(date)
  call sic_user(user)
  name = 'CLIC - '//date!//' - '//user
end subroutine user_info
!
subroutine getvar(md,mb,x_data,y_data,w_data)
  use gildas_def
  use gkernel_types
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Load the plot buffers with the contents of related variables
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipnt, nd, nb
  type(sic_descriptor_t) :: descr
  logical :: found
  ! Code ----------------------------------------------------------------
  ! X_DATA
  call sic_descriptor ('X_DATA',descr,found)
  if (.not.found) return
  ipnt = gag_pointer(descr%addr,memory)
  nd = descr%dims(1)
  nb = descr%dims(2)
  call copyvar(memory(ipnt),md,nd,nb,x_data)
  ! Y_DATA
  call sic_descriptor ('Y_DATA',descr,found)
  if (.not.found) return
  ipnt = gag_pointer(descr%addr,memory)
  nd = descr%dims(1)
  nb = descr%dims(2)
  call copyvar(memory(ipnt),md,nd,nb,y_data)
  ! W_DATA
  call sic_descriptor ('W_DATA',descr,found)
  if (.not.found) return
  ipnt = gag_pointer(descr%addr,memory)
  nd = descr%dims(1)
  nb = descr%dims(2)
  call copyvar(memory(ipnt),md,nd,nb,w_data)
  return
end subroutine getvar
!
subroutine reset_time
  use gildas_def
  use classic_api
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_xy_code.inc'
  include 'clic_par.inc'
  ! Local
  integer :: iy,isb,iba,i,k,km(2),kp(2), ir,ipol
  !
  km(1) = -mnant
  kp(1) = -1
  km(2) = 1
  kp(2) = mnbas
  !
  do ir=1, 2
    do iy=1, 2
      do isb=1, 3
        do k=1,2
          do ipol=1,3
            do iba = km(k), kp(k)
              f_spline(iy,isb,ipol,iba,ir) = 0
              n_knots(iy,isb,ipol,iba,ir) = 0
              do i=1, m_spl+4
                c_spline(i,iy,isb,ipol,iba,ir) = 0
              enddo
              do i=1, m_spl+8
                k_spline(i,iy,isb,ipol,iba,ir) = 0
              enddo
              do i=1, m_pol
                c_pol(i,iy,isb,ipol,iba,ir) = 0
              enddo
              n_pol(iy,isb,ipol,iba,ir) = 0
              f_pol(iy,isb,ipol,iba,ir) = 0
            enddo
          enddo
        enddo
      enddo
    enddo
  enddo
  dobs_0 = 0
  change_display = .true.
  call message(6,1,'CLIC_PLOT','Time origin reset')
  call message(6,2,'CLIC_PLOT','Calibration curves reset')
end subroutine reset_time
