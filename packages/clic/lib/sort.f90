subroutine sort_index (error)
  use classic_api
  use clic_index
  logical :: error                  !
  ! Global
  external :: gtt_i,gte_i
  ! Local
  integer(kind=entry_length) :: i
  ! Arrays to sort :
  !	CX_Num
  !	CX_Bloc
  !	CX_Ver
  !	CX_Ind
  !
  ! Assumes in GTT_i and GTE_i that Scan number is primary number,
  ! Or receiver if do_sort_receiver
  !	  Obs number secondary number.
  !
  if (cxnext.le.2) return
  call tri_index (cx_ind,cxnext-1,gtt_i,gte_i,error)
  if (error) return
  do i=1,cxnext-1
    cx_num(i)  = ix_num(cx_ind(i))
    cx_ver(i)  = ix_ver(cx_ind(i))
    cx_bloc(i) = ix_bloc(cx_ind(i))
    cx_word(i) = ix_word(cx_ind(i))
  enddo
  return
end subroutine sort_index
!
function gtt_i  (m,l)
  use clic_index
  logical :: gtt_i                  !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !
  if (do_sort_receiver .and. ix_rece(m).gt.ix_rece(l)) then
    gtt_i  = .true.
  elseif (.not.do_sort_receiver .or. ix_rece(m).eq.ix_rece(l)) then
    if ((mod(ix_scan(m)-ix_scan(1)+15000,10000)   &
      .gt. mod(ix_scan(l)-ix_scan(1)+15000,10000))&
      .and..not.do_sort_num)   &
      then
      !         IF (IX_SCAN(M) .GT. IX_SCAN(L)) THEN
      gtt_i  = .true.
    elseif ((ix_scan(m) .eq. ix_scan(l)).or.do_sort_num) then
      if (ix_num(m) .gt. ix_num(l)) then
        gtt_i  = .true.
      else
        gtt_i  = .false.
      endif
    else
      gtt_i  = .false.
    endif
  else
    gtt_i  = .false.
  endif
end function gtt_i
!
function gte_i (m,l)
  use clic_index
  logical :: gte_i                  !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !
  if (do_sort_receiver .and. ix_rece(m).gt.ix_rece(l)) then
    gte_i  = .true.
  elseif (.not.do_sort_receiver .or. ix_rece(m).eq.ix_rece(l)) then
    !         IF (IX_SCAN(M) .GT. IX_SCAN(L)) THEN
    if ((mod(ix_scan(m)-ix_scan(1)+15000,10000)   &
      .gt. mod(ix_scan(l)-ix_scan(1)+15000,10000)) &
      .and..not.do_sort_num)   &
      then
      gte_i  = .true.
    elseif ((ix_scan(m) .eq. ix_scan(l)).or.do_sort_num) then
      if (ix_num(m) .ge. ix_num(l)) then
        gte_i  = .true.
      else
        gte_i  = .false.
      endif
    else
      gte_i  = .false.
    endif
  else
    gte_i  = .false.
  endif
end function gte_i
!
subroutine tri_index (x,n,gtt,gte,error)
  !---------------------------------------------------------------------
  ! 	Sorting program that uses a quicksort algorithm.
  !	Applies for an input array of integer values, which are SORTED
  !
  !	X	I(*) 	Indexes					Input
  !	N	I	Length of arrays			Input
  !	*	Return	Error return
  !---------------------------------------------------------------------
  integer(kind=8) :: x(*)           !
  integer(kind=8) :: n              !
  external :: gtt                   !
  logical :: gtt                    !
  external :: gte                   !
  logical :: gte                    !
  logical :: error                  !
  ! Local
  integer :: maxstack,nstop
  parameter (maxstack=1000,nstop=15)
  integer*4 :: i, j, k, l1, r1
  integer*4 :: l, r, m, lstack(maxstack), rstack(maxstack), sp
  integer(kind=8) :: temp, key
  logical :: mgtl, lgtr, rgtm
  !
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! Set KEY = median of X(L), X(M), X(R)
  ! No! This is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! To fix this problem, I found (but I cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. P.V.
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  l1=(2*l+r)/3
  r1=(l+2*r)/3
  !
  mgtl = gtt (x(m),x(l))
  rgtm = gtt (x(r),x(m))
  !
  ! Algorithm to select the median key. The original one from MONGO
  ! was completely wrong. P. Valiron, 24-Jan-84 .
  !
  !		   	MGTL	RGTM	LGTR	MGTL.EQV.LGTR	MEDIAN_KEY
  !
  !	KL < KM < KR	T	T	*	*		KM
  !	KL > KM > KR	F	F	*	*		KM
  !
  !	KL < KM > KR	T	F	F	F		KR
  !	KL < KM > KR	T	F	T	T		KL
  !
  !	KL > KM < KR	F	T	F	T		KL
  !	KL > KM < KR	F	T	T	F		KR
  !
  if (mgtl .eqv. rgtm) then
    key = x(m)
  else
    lgtr = gtt (x(l),x(r))
    if (mgtl .eqv. lgtr) then
      key = x(l)
    else
      key = x(r)
    endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
10 if (gte(x(i),key)) goto 11
  i = i + 1
  goto 10
11 continue
  ! Find a small record on the right
20 if (gtt(key,x(j))) goto 21
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  temp = x(i)
  x(i) = x(j)
  x(j) = temp
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! Push the two halves on the stack
2 continue
  if (j-l+1 .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(6,*) 'E-SORT,  Stack overflow ',sp
      error = .true.
      return
    endif
    lstack(sp) = l
    rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(6,*) 'E-SORT,  Stack overflow ',sp
      error = .true.
      return
    endif
    lstack(sp) = j+1
    rstack(sp) = r
  endif
  !
  ! Anything left to process?
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do 110 j = n-1,1,-1
    k = j
    do i = j+1,n
      if (gtt(x(i),x(j))) goto 121
      k = i
    enddo
121 continue
    if (k.eq.j) goto 110
    temp = x(j)
    do i = j+1,k
      x(i-1) = x(i)
    enddo
    x(k) = temp
110 continue
  return
end subroutine tri_index
!
subroutine sort_fluxes(error)
  logical :: error                  !
  ! Global
  external :: gtt_flux,gte_flux
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  integer(kind=8)   :: ind_i8(m_flux), n_flux_i8, i8
  integer(kind=4)   :: ind_i4(m_flux)
  character(len=12) :: cwork (m_flux)
  integer :: iwork(m_flux), i, k
  !-----------------------------------------------------------------------
  if (n_flux.lt.2) return
  call i4toi8(n_flux,n_flux_i8,1,error)
  do i8=1, n_flux_i8
    ind_i8(i8) = i8
  enddo
  call tri_index(ind_i8,n_flux_i8,gtt_flux,gte_flux,error)
  do i=1,n_flux
    call i8toi4_fini(ind_i8(i),ind_i4(i),1,error)
    cwork(i) = c_flux(ind_i4(i))
  enddo
  do i=1,n_flux
    c_flux(i) = cwork(i)
  enddo
  call gr4_sort(flux,iwork,ind_i4,n_flux)
  call gr4_sort(freq_flux,iwork,ind_i4,n_flux)
  call gi4_sort(date_flux,iwork,ind_i4,n_flux)
  call gi4_sort(f_flux,iwork,ind_i4,n_flux)
  ! "~" is the last chain ... (deleted fluxes)
  k = 0
  do i=1,n_flux
    if (c_flux(i).lt.'~') k = k+1
  enddo
  n_flux = k
  return
end subroutine sort_fluxes
!
function gtt_flux (m,l)
  !---------------------------------------------------------------------
  ! Flux record comparison (1-source then 2-freq then 3-date)
  !---------------------------------------------------------------------
  logical :: gtt_flux               !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !-----------------------------------------------------------------------
  if (c_flux(m) .gt. c_flux(l)) then
    gtt_flux = .true.
  elseif (c_flux(m) .eq. c_flux(l)) then
    if (freq_flux(m) .gt. freq_flux(l)) then
      gtt_flux = .true.
    elseif (freq_flux(m) .eq. freq_flux(l)) then
      gtt_flux = (date_flux(m) .gt. date_flux(l))
    else
      gtt_flux = .false.
    endif
  else
    gtt_flux = .false.
  endif
end function gtt_flux
!
function gte_flux (m,l)
  !---------------------------------------------------------------------
  ! Flux record comparison (1-source then 2-freq then 3-date)
  !---------------------------------------------------------------------
  logical :: gte_flux               !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !-----------------------------------------------------------------------
  if (c_flux(m) .gt. c_flux(l)) then
    gte_flux = .true.
  elseif (c_flux(m) .eq. c_flux(l)) then
    if (freq_flux(m) .gt. freq_flux(l)) then
      gte_flux = .true.
    elseif (freq_flux(m) .eq. freq_flux(l)) then
      gte_flux = (date_flux(m) .ge. date_flux(l))
    else
      gte_flux = .false.
    endif
  else
    gte_flux = .false.
  endif
end function gte_flux
!
subroutine sort_spidx(error)
  logical :: error                  !
  ! Global
  external :: gtt_spidx,gte_spidx
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  integer(kind=8)   :: ind_i8(m_spidx), n_spidx_i8, i8
  integer(kind=4)   :: ind_i4(m_spidx)
  character(len=12) :: cwork (m_spidx)
  integer :: iwork(m_spidx), i, k
  !-----------------------------------------------------------------------
  if (n_spidx.lt.2) return
  call i4toi8(n_spidx,n_spidx_i8,1,error)
  do i8=1, n_spidx_i8
    ind_i8(i8) = i8
  enddo
  call tri_index(ind_i8,n_spidx_i8,gtt_spidx,gte_spidx,error)
  do i=1,n_spidx
    call i8toi4_fini(ind_i8(i),ind_i4(i),1,error)
    cwork(i) = c_spidx(ind_i4(i))
  enddo
  do i=1,n_spidx
    c_spidx(i) = cwork(i)
  enddo
  call gr4_sort(spidx,iwork,ind_i4,n_spidx)
  call gr4_sort(freq_spidx,iwork,ind_i4,n_spidx)
  call gi4_sort(date_spidx,iwork,ind_i4,n_spidx)
  call gi4_sort(f_spidx,iwork,ind_i4,n_spidx)
  ! "~" is the last chain ... (deleted spidxes)
  k = 0
  do i=1,n_spidx
    if (c_spidx(i).lt.'~') k = k+1
  enddo
  n_spidx = k
  return
end subroutine sort_spidx
!
function gtt_spidx (m,l)
  !---------------------------------------------------------------------
  ! Spidx record comparison (1-source then 2-freq then 3-date)
  !---------------------------------------------------------------------
  logical :: gtt_spidx               !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !-----------------------------------------------------------------------
  if (c_spidx(m) .gt. c_spidx(l)) then
    gtt_spidx = .true.
  elseif (c_spidx(m) .eq. c_spidx(l)) then
    if (freq_spidx(m) .gt. freq_spidx(l)) then
      gtt_spidx = .true.
    elseif (freq_spidx(m) .eq. freq_spidx(l)) then
      gtt_spidx = (date_spidx(m) .gt. date_spidx(l))
    else
      gtt_spidx = .false.
    endif
  else
    gtt_spidx = .false.
  endif
end function gtt_spidx
!
function gte_spidx (m,l)
  !---------------------------------------------------------------------
  ! Spidx record comparison (1-source then 2-freq then 3-date)
  !---------------------------------------------------------------------
  logical :: gte_spidx               !
  integer(kind=8) :: m                      !
  integer(kind=8) :: l                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !-----------------------------------------------------------------------
  if (c_spidx(m) .gt. c_spidx(l)) then
    gte_spidx = .true.
  elseif (c_spidx(m) .eq. c_spidx(l)) then
    if (freq_spidx(m) .gt. freq_spidx(l)) then
      gte_spidx = .true.
    elseif (freq_spidx(m) .eq. freq_spidx(l)) then
      gte_spidx = (date_spidx(m) .ge. date_spidx(l))
    else
      gte_spidx = .false.
    endif
  else
    gte_spidx = .false.
  endif
end function gte_spidx

