SUBROUTINE WRITE_SDM(LINE,ERROR)
  use gildas_def
  use gkernel_interfaces
  !------------------------------------------------------------------------
  ! Support WRITE file /DATA /NOCAL
  ! Write a SDM data set in ALMA SDM FITS format
  ! write the current index
  !( Actual options TBD)
  !     file   character     file name
  !     /DATA     option        write all data table extensions
  !     /NOCAL    option        write data uncalibrated (not multiplied by Tsys)
  !     /HOLODATA option        write data table in holography format
  !                iant  ref      scanned antennaand reference antenna
  !     /ANTENNAS  a1 a2 a3     use only a subarray ...
  !     /TELESCOPE name         telescope value
  !     /SIMULATE  list        simulate data for ATF
  !------------------------------------------------------------------------
  ! Dummy variables:
  character*(*) line
  logical error
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Local variables:
  integer(kind=address_length) data_in, ip_data
  character*132 argum, argum2
  character c2*2
  character telarg*12
  integer msimvocab
  parameter (msimvocab=3)
  character simvocab(msimvocab)*12, arg*12, simkw*12
  logical :: end, data, nocal, holodata,   &
    telesc, dosubant, switch, simpolar, old, simAddressing
  integer nch, unit, ndata, status, ib, kfich, maxhdu, num,   &
    larg, larg2, holoant, holoref, holobase, nsubant, subant(mnant),   &
    i, narg, nk
  integer lfich, iswitch
  parameter (maxhdu=950)
  save unit
  data unit/-999/
  data simvocab/'SWITCH','POLARIZATION','ADDRESSING'/
  !c      data next_uid/0/, file_name/'EMPTY '/
  data file_name/'EMPTY '/
  !------------------------------------------------------------------------
  ! Code:
  DOSUBANT = .FALSE.
  switch = .FALSE.
  simpolar = .FALSE.
  simAddressing = .false.
  STATUS = 0
  !c unused       DATA = SIC_PRESENT(1,0)
  !c unused       NOCAL = SIC_PRESENT(2,0)
  HOLODATA = SIC_PRESENT(3,0)  ! used.
  TELESC = SIC_PRESENT(5,0)    ! used.
  ! get ATF simulation parameters
  if (SIC_PRESENT(6,0)) then
    i = 1
    do while (i.le.sic_narg(6))
      call sic_ke(line,6,i,arg,narg,.true.,error)
      if (error) return
      call sic_ambigs('WRITE /SIMULATE',arg,simkw,nk,   &
        simvocab,msimvocab,error)
      if (error) return
      i = i+1
      if (simkw.eq.'SWITCH') then
        switch = .true.
        CALL SIC_I4(LINE,6,i,iswitch,.TRUE.,ERROR)
        if (error) return
        i = i+1
      elseif (simkw.eq.'POLARIZATION') then
        simpolar = .true.
      elseif (simkw.eq.'ADDRESSING') then
        simAddressing = .true.
      endif
    enddo
  endif
  !
  ! /ANTENNA
  IF (SIC_PRESENT(4,0)) THEN
    NSUBANT = SIC_NARG(4)
    DO I =1, NSUBANT
      CALL SIC_I4(LINE,4,I,SUBANT(I),.FALSE.,ERROR)
      IF (SUBANT(I).LE.0 .OR. SUBANT(I).GT.MNANT) THEN
        CALL MESSAGE(8,3,'WRITE_SDM',   &
          'Wrong /ANTENNA argument')
      ENDIF
    ENDDO
    DOSUBANT = .TRUE.
  ENDIF
  ! /TELESCOPE
  IF (TELESC) THEN
    CALL SIC_CH(LINE,5,1,TELARG,NARG,.TRUE.,ERROR)
  ENDIF
  !
  ! Verify if any input file
  CALL CHECK_INPUT_FILE(ERROR)
  IF (ERROR) RETURN
  !
  CALL CHECK_INDEX(ERROR)
  IF (ERROR) RETURN
  ! Get file name
  lfich = lenc(file_name)
  if (sic_present(0,1)) then
    ARGUM = 'clicFile'
    CALL SIC_CH(LINE,0,1,ARGUM,NCH,.false.,ERROR)
    IF (ERROR) RETURN
    CALL SIC_PARSEF(ARGUM,file_name,' ','')
    lfich = lenc(file_name)
    old = .false.
    call message(2,1,'WRITE_SDM',   &
      'Creating '//file_name(1:lfich))
  else
    if (file_name(1:lfich).eq.'EMPTY') then
      error = .true.
      CALL MESSAGE(8,3,'WRITE_SDM',   &
        'No open ASDM to extend')
      return
    endif
    old = .true.
    call message(2,1,'WRITE_SDM',   &
      'Extending '//file_name(1:lfich))
  endif
  KFICH = 0
  !
  ! Now loop on current index.
  ! we need a first loop to get the start and end times for the execblock table
  eb_starttime = 9223372036854775807_8
  eb_endtime = 0
  call get_first(.true.,error)
  if (error) goto 99
  call get_data (ndata,data_in,error)
  if (error) goto 99
  ip_data = gag_pointer(data_in,memory)
  call get_eb_times (ndata, memory(ip_data), error)
  if (error) goto 99
  end = .false.
  do while (.not.end)
    
    call get_next(end,error)
    call get_data (ndata,data_in,error)
    if (error) goto 99
    ip_data = gag_pointer(data_in,memory)
    call get_eb_times (ndata, memory(ip_data), error)
  enddo
  
  !
  ! Loop on current index
  call get_first(.true.,error)
  if (error) goto 99
  if (.not.dosubant) then
    nsubant = r_nant
    do i=1, nsubant
      subant(i) = i
    enddo
  endif
  lfich = lenc(file_name)
  file_name = file_name(1:lfich)//char(0)//' '
  call sdm_init(file_name(1:lfich)//char(0),error)
  if (.not.old) then
    call system('mkdir '//file_name(1:lfich))
    call system('mkdir '//file_name(1:lfich)//'/ASDMBinary')
    ! define uids for containers:
    call sdm_getUid(Project_Uid)
    call sdm_getUid(ObsUnitSet_Uid)
    call sdm_getUid(SchedBlock_Uid)
    call sdm_getUid(ExecBlock_Uid)
    r_execblocknum = 1
  endif
  ! reset all identifiers
  call init_sdm
  if (error) goto 99
  end = .false.
  !     This makes sure the time_start is set to the date of the first
  !     scan (this is used for sources and the like)
  time_start = -1
  do while (.not.end)
    !
    ! Get storage and data
    call get_data (ndata,data_in,error)
    if (error) goto 99
    ip_data = gag_pointer(data_in,memory)
    ! 
    !
    if (telesc) r_teles = telarg
    ! Specific Holography setup:
    if (holodata) then
      !c            ntch = r_nband
      r_nband = 1
      r_lband = 1
      r_lnsb = 1
      r_nsb = 2
      call write_sdm_holotables (ndata, memory(ip_data), error)
    else
      call write_sdm_tables (ndata, memory(ip_data),   &
        switch, iswitch, simAddressing, error)
    endif
    if (error) goto 99
    if (sic_ctrlc()) then
      error = .true.
      goto 99
    endif
    call get_next(end,error)
  enddo
  call write_sdm_scan(error)
  if (error) return
  call sdm_write()
  call sdm_close()
  call message(2,1,'Write_sdm','Data set '//file_name(1:lfich)//   &
    ' written.')
  return
  !
  !
99 call message(8,4,'Write_sdm','SDM Writing Failed')
  return
end subroutine WRITE_SDM
!
subroutine get_eb_times(ldata, data, error)
  integer :: ldata, data(ldata)
  logical :: error
  INCLUDE 'clic_parameter.inc'
  INCLUDE 'clic_par.inc'
  include 'sdm_identifiers.inc'
  integer :: ir, k, h_offset

  ir = r_ndump+1
  k = 1+ h_offset(ir)
  call decode_header (data(k))
  call get_time_interval
  eb_starttime = min(time_interval(1),eb_starttime)
  eb_endtime = max(time_interval(1)+time_interval(2), eb_endtime)
end subroutine get_eb_times
