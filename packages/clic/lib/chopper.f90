subroutine chopper (ip, iant, freqs, freqi, tcals, tcali, error)
  use atm_interfaces_public
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !	compute TCAL, TSYS, TREC, from the chop and atm values observed
  !	according to the usual chopper method (Ulich and Haas)
  !
  !	Double side band operation
  !
  !	If r.cmode(ir) is 'AU' and a cold load is available, or if r.cmode(ir)
  !	is 'TR', derive the sky emission TEMI from the receiver
  !	temperature TREC, and then H2OMM from the best model fitting.
  !	Else, if r.cmode(ir) is 'AU', the sky emission is computed from
  !	the given H2OMM, and if r.cmode(ir) is 'MA', the sky emission is
  !	computed from the given opacities and atmospheric temperatures.
  !
  !  	Uses a curved atmosphere, equivalent height 5.5 KM
  !
  ! Input:
  !       ip              i       calibration number (almost logical)
  !       iant            i       antenna number (logical)
  !	FREQS, FREQI	R	Frequencies signal image in GHz (!!!)
  !	Calibration section (-25)
  ! Output:
  !	R.TSYSS 	Signal SB system temperature (K)
  !	R.TSYSI		Image SB system temperature (K)
  !	R.TREC		receiver dsb temperature (K)
  !
  ! Call Tree
  !			READ_DATA,
  !	READ_SPEC,	SUB_READ_SPEC,	SUB_SPECTRUM,	DO_CALIB
  !			CLIC_SPECTRUM,
  !---------------------------------------------------------------------
  integer :: ip                   !
  integer :: iant                   !
  real*4 :: freqs                   !
  real*4 :: freqi                   !
  real*4 :: tcals                   !
  real*4 :: tcali                   !
  logical :: error                  !
  ! Global
  real*8 :: air_mass
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  real :: em, dem, airmas, tatm, taut, dwater, err, deltaw
  real :: atts, atti, temis, temii, tsky, temi, t1, t2
  integer :: k, mode_au, mode_tr, mode_no, ier
  character(len=25) :: mess(3)
  character(len=60) :: msg
  !
  ! Data
  data mess /'Zero atmospheric opacity',   &
    'No oxygen in atmosphere','No water in atmosphere'/
  !------------------------------------------------------------------------
  !
  ! Check for bad atmosphere
  ier = 0
  if (r_csky(ip,iant).ge.r_cchop(ip,iant)) then
    call message (8,3,'CHOPPER','Bad atmosphere')
    return
  endif
  if (r_ccold(ip,iant).ge.r_cchop(ip,iant)) then
    call message (8,3,'CHOPPER',   &
      'Signal stronger on COLD than on CHOPPER')
    return
  endif
  !
  call chtoby('AU  ',mode_au,4)
  call chtoby('TR  ',mode_tr,4)
  call chtoby('NO  ',mode_no,4)
  if (r_cmode(ip,iant).eq.mode_no) return
  airmas = air_mass(r_el)
  !
  ! AUTO mode with cold load or fixed TREC : minimize for water vapor
  ! content
  if ((r_cmode(ip,iant).eq.mode_au .and. r_ccold(ip,iant).gt.0)   &
    .or. r_cmode(ip,iant).eq.mode_tr) then
    if (atm_interpolate) then
       call atm_atmosp_i (r_tamb, r_pamb, r_alti) ! press at 2550m
    else
       call atm_atmosp (r_tamb, r_pamb, r_alti) 
    endif
    ! use a reasonable starting value
    r_h2omm(ip,iant) = min(10.,max(0.5,r_h2omm(ip,iant)))
    !
    ! ANY mode with cold load : compute TREC
    if (r_ccold(ip,iant).ne.0) then
      r_trec(ip,iant) = (r_tcold(ip,iant)*r_cchop(ip,iant) -   &
        r_tchop(ip,iant)*r_ccold(ip,iant) ) /   &
        (r_ccold(ip,iant)-r_cchop(ip,iant))
    endif
    !
    ! Compute TEMI
    temi = ((r_ceff(ip,iant)*r_tchop(ip,iant)+r_trec(ip,iant)   &
      )*r_csky(ip,iant)/r_cchop(ip,iant)-r_trec(ip,iant) )   &
      /(1.-r_csky(ip,iant)/r_cchop(ip,iant)*(1.-r_ceff(ip   &
      ,iant)))
    tsky = (temi - (1.-r_feff(ip,iant))*r_tamb) / r_feff(ip   &
      ,iant)
    r_h2omm(ip,iant) = min(10.,max(0.5,r_h2omm(ip,iant)))
    err = 1e10
    dwater = .02
    k = 0
10  continue
    k = k + 1
    if (atm_interpolate) then
      call atm_transm_i(r_h2omm(ip,iant), airmas, freqs, temis,   &
        r_tatms(ip,iant), t1, t2, taut, ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm_i(r_h2omm(ip,iant),airmas, freqi, temii,   &
        r_tatmi(ip,iant), t1, t2, taut, ier)
      r_taui(ip,iant) = t1 + t2
      em = (temis + temii*r_gim(ip,iant)) / (1.+r_gim(ip,iant))
      call atm_transm_i(r_h2omm(ip,iant)+dwater, airmas, freqs,   &
        temis, tatm, t1, t2, taut,ier)
      call atm_transm_i(r_h2omm(ip,iant)+dwater, airmas, freqi,   &
        temii, tatm, t1, t2, taut,ier)
    else
      call atm_transm(r_h2omm(ip,iant), airmas, freqs, temis,   &
        r_tatms(ip,iant), t1, t2, taut, ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm(r_h2omm(ip,iant),airmas, freqi, temii,   &
        r_tatmi(ip,iant), t1, t2, taut, ier)
      r_taui(ip,iant) = t1 + t2
      em = (temis + temii*r_gim(ip,iant)) / (1.+r_gim(ip,iant))
      call atm_transm(r_h2omm(ip,iant)+dwater, airmas, freqs,   &
        temis, tatm, t1, t2, taut,ier)
      call atm_transm(r_h2omm(ip,iant)+dwater, airmas, freqi,   &
        temii, tatm, t1, t2, taut,ier)
    endif
    dem = (temis + temii*r_gim(ip,iant)) / (1.+r_gim(ip,iant))   &
      - em
    err = em - tsky
    if (abs(err).ge.0.1 .and. dem.ne.0 .and. k.le.10) then
      deltaw = dwater * err / dem
      if(abs(dwater).ge.abs(deltaw/10.)) dwater = deltaw/10.
      ! limit optical depth to ~20
      r_h2omm(ip,iant) = max(0.01,min(r_h2omm(ip,iant)-deltaw,   &
        20.*r_h2omm(ip,iant)/max(r_taus(ip,iant)   &
        ,r_taui(ip,iant))/airmas))
      goto 10
    endif
    if (atm_interpolate) then
      call atm_transm_i (r_h2omm(ip,iant),airmas,freqs,   &
        temis,r_tatms(ip,iant),t1,t2,taut,ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm_i (r_h2omm(ip,iant),airmas,freqi,   &
        temii,r_tatmi(ip,iant),t1,t2,taut,ier)
      r_taui(ip,iant) = t1 + t2
    else
      call atm_transm (r_h2omm(ip,iant),airmas,freqs,   &
        temis,r_tatms(ip,iant),t1,t2,taut,ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm (r_h2omm(ip,iant),airmas,freqi,   &
        temii,r_tatmi(ip,iant),t1,t2,taut,ier)
      r_taui(ip,iant) = t1 + t2
    endif
    if (ier.ne.0) then
      msg = 'Stupid calibration '//mess(ier)
      call message(6,2,'CHOPPER',msg)
    endif
  elseif (r_cmode(ip,iant) .eq. mode_au) then
    !
    ! Auto Mode without cold load, or when the water vapor content is known
    if (atm_interpolate) then
      call atm_atmosp_i (r_tamb, r_pamb, r_alti) ! press at 2550m
      call atm_transm_i (r_h2omm(ip,iant),airmas,freqs,   &
        temis,r_tatms(ip,iant),t1,t2,taut,ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm_i (r_h2omm(ip,iant),airmas,freqi,   &
        temii,r_tatmi(ip,iant),t1,t2,taut,ier)
      r_taui(ip,iant) = t1 + t2
    else
      call atm_atmosp (r_tamb, r_pamb, r_alti) ! press at 2550m
      call atm_transm (r_h2omm(ip,iant),airmas,freqs,   &
        temis,r_tatms(ip,iant),t1,t2,taut,ier)
      r_taus(ip,iant) = t1 + t2
      call atm_transm (r_h2omm(ip,iant),airmas,freqi,   &
        temii,r_tatmi(ip,iant),t1,t2,taut,ier)
      r_taui(ip,iant) = t1 + t2
    endif
    if (ier.ne.0) then
      msg = 'Stupid calibration '//mess(ier)
      call message(6,2,'CHOPPER',msg)
    endif
    em = (temis + temii*r_gim(ip,iant)) / (1.+r_gim(ip,iant))
    temi = (1.-r_feff(ip,iant))*r_tamb + r_feff(ip,iant)*em
  !
  ! Manual Mode
  else
    temis = r_tatms(ip,iant) * (1.-exp(-r_taus(ip,iant)*airmas)   &
      )
    temii = r_tatmi(ip,iant) * (1.-exp(-r_taui(ip,iant)*airmas)   &
      )
    em = (temis + temii*r_gim(ip,iant)) / (1.+r_gim(ip,iant))
    temi = (1.-r_feff(ip,iant))*r_tamb + r_feff(ip,iant)*em
  endif
  !
  ! Compute TCAL at zenith
  tcals = (r_tchop(ip,iant)-temi) * (1.+ r_gim(ip,iant)) /   &
    r_beef(ip,iant)* r_ceff(ip,iant)
  tcali = (r_tchop(ip,iant)-temi) * (1.+ 1./r_gim(ip,iant)) /   &
    r_beef(ip,iant)* r_ceff(ip,iant)
  !
  ! Compute TSYS and TCAL (TSYS at current elevation)
  atts = exp (-r_taus(ip,iant) * airmas)
  atti = exp (-r_taui(ip,iant) * airmas)
  if (atts.ne.0) then
    r_tsyss(ip,iant) = tcals*r_csky(ip,iant) / (r_cchop(ip   &
      ,iant)-r_csky(ip,iant))/atts
  endif
  if (atti.ne.0) then
    r_tsysi(ip,iant) = tcali*r_csky(ip,iant) / (r_cchop(ip   &
      ,iant)-r_csky(ip,iant))/atti
  endif
  r_tsyss(ip,iant) = max(min(1e10,r_tsyss(ip,iant)),0.)
  r_tsysi(ip,iant) = max(min(1e10,r_tsysi(ip,iant)),0.)
  !
  ! Compute TREC
  if (r_cmode(ip,iant).ne.mode_tr) then
    r_trec(ip,iant) = (temi*(r_csky(ip,iant)*(1-r_ceff(ip   &
      ,iant))- r_cchop(ip,iant))+ r_csky(ip,iant)   &
      *r_ceff(ip,iant)*r_tchop(ip,iant))/ (r_cchop(ip,iant   &
      )-r_csky(ip,iant))
  endif
  return
end subroutine chopper
!
function air_mass(el)
  !---------------------------------------------------------------------
  ! clic
  !	Computes airmass content corresponding to elevation
  !	EL
  !---------------------------------------------------------------------
  real*8 :: air_mass                !
  real :: el                        !
  ! Local
  real*8 :: r0,h0,delev,eps,gamma,hz
  parameter (h0= 5.5d0, r0= 6370.d0)
  !
  delev = el
  eps= asin(r0/(r0+h0)*cos(delev))
  gamma= delev + eps
  hz = r0*r0 + (r0+h0)**2 - 2.d0*r0*(r0+h0)*sin(gamma)
  hz = sqrt (max(h0**2,hz))
  hz = hz/h0
  hz = min (hz, 20.d0)
  air_mass = hz
end function air_mass
