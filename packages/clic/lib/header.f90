subroutine clic_header(line,error)
  use gkernel_interfaces
  use astro_interfaces_public
  use clic_title
  use classic_api
  use astro_types
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local copies of astro global variables
  ! From aline.inc
  real*8 :: flo1
  integer :: plot_mode
  integer :: sky
  logical :: narrow_def
  integer :: narrow_input(6)
  integer :: nunit
  parameter (nunit=12)
  logical :: unit_def(nunit)
  integer :: unit_band(nunit)
  real :: unit_cent(nunit)
  integer :: unit_wind(nunit)
  character(len=132) :: label
  type(astro_noemasetup_t) :: clic
  ! From astro.inc
  real*8 :: fshift
  integer :: i, quarter, isb, icorrmode(mrlband)
  character(len=4) :: lock
  character(len=3) :: band
  character(len=12) :: arg
  integer :: harm, irec, mult, narg, mvocab, nkey
  real*8 :: fcent, vsys, eps,hh, fmin, fmax
  parameter(mvocab=3)
  character(len=6) :: vocab(mvocab), keyword
  data vocab/'LINE','NARROW','WIDEX'/
  !------------------------------------------------------------------------
  ! Code
  call out0('Terminal',0,0,error)
  call titout('Full',error)
  call out1(error)
  !
  ! HEADER /PLOT
  !
  if ((sic_present(1,0).or.sic_present(2,0)).and.(.not.error)) then
    if (title%kind.ne.5) then
      call message(2,2,'HEADER',   &
        ' /PLOT option invalid for old receivers (<2007)')
    else
      if (r_lnch(1).eq.2032) then   ! Polyfix
        isb = (3-r_isb)/2
        call astro_noemasetup_receiver('CLIC',clic%rec,r_flo1,r_fif1,isb,&
              r_line,r_sourc,real(r_doppl,kind=8),error)
        !
        ! Astro needs matching corrmode for spw pertaining to a bb
        icorrmode = 1
        if (r_nbb.lt.r_lband) then 
          do i=r_nbb+1, r_lband
            if (abs(r_lfres(i)).eq.0.250) then
              icorrmode(i) = 3
              icorrmode(r_bb(i)) = 3
            endif
          enddo
        endif
        do i=1, r_lband
          fmin = r_lfcen(i)+(1-r_lcench(i))*r_lfres(i)
          fmax = r_lfcen(i)+(r_lnch(i)-r_lcench(i))*r_lfres(i)
          fmax = min(fmax,3870d0)
          call astro_noemasetup_spw('CLIC',clic%spw,fmin,fmax, &
              real(r_lfres(i),kind=8), r_bbname(r_bb(i)),r_lnam(1,i),&
              icorrmode(i),error)
        enddo
        if (sic_present(1,0)) then
          call astro_noemasetup_plot('CLIC',clic,pm_receiver,freqax_if1, &
                .true.,error)
        elseif (sic_present(2,0)) then
          call astro_noemasetup_list('CLIC',clic,.true.,error)
        endif
      else
        ! 
        ! plot narrow or plot widex
       !
        plot_mode = 2 ! default is NARROW plot
        if (sic_present(1,1)) then
          call sic_ke(line,1,1,arg,narg,.true.,error)
           if (error) return
           call sic_ambigs('HEADER',arg,keyword,nkey,vocab,mvocab,error)
           if (error) return
           plot_mode = nkey
        endif
        !
        ! Define parameters needed by ASTRO commands
        !
        call pdbi_plot_def(error)
        if (error) return
        !
        ! Frequency
        !
        !            FLO1 = R_FLO1-R_DOPPL*R_RESTF ! Also correct
        flo1 = r_restf-r_isb*r_fif1
        sky = r_isb
        fshift = 1.
        !
        ! Spectral units
        !
        narrow_def = .true.
        do i = 1,r_lband
          unit_def(i) = .true.
          unit_band(i) = r_lnch(i)*abs(r_lfres(i))
          unit_cent(i) = r_lfcen(i)
          unit_wind(i) = r_bb(i)
          narrow_input(unit_wind(i)) =   &
            quarter(r_flo2(i),r_flo2bis(i))
        enddo
        !
        ! Plot label (LINE command)
        !
        if (r_isb.gt.0) then
          band = 'USB'
        else
          band = 'LSB'
        endif
        if (r_lock.gt.0) then
          lock = 'HIGH'
        else
          lock = 'LOW'
        endif
        fcent = r_fif1
        irec = r_nrec
        mult = r_nrec
        eps = 100.25d0
        hh = (flo1/mult+r_lock*eps)/r_flo1_ref
        harm = int(hh)
        if (hh.ne.harm) then
          call message(2,2,'HEADER',' Non-integer HARM number?')
        endif
        vsys = r_veloc
        write(label,1000)   &
          r_line(1:lenc(r_line)),   &
          r_restf*1d-3/fshift, band, lock, fcent, harm,   &
          '  /RECEIVER',irec,'   [V= ',vsys,' km/s]'
        !
        ! Synchronize local with astro-global variables
        !
        call pdbi_plot_sync(flo1,plot_mode,sky,narrow_def,   &
          narrow_input,unit_def,unit_band,unit_cent,   &
          unit_wind,fshift,label)
        !
        ! Now do the plot
        !
        call pdbi_plot_line(error)
        if (error) return
        !
      endif
    endif
  endif
1000 format('LINE ',a,1x,f11.6,1x,a,1x,a,1x,f7.2,1x,i2,a,i2,a,f6.1,a)
!
end subroutine clic_header
