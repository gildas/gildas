subroutine clic_show(line,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC 	Support routine for command
  !	SHOW Something
  ! Arguments :
  !	LINE	C*(*)	Command line		Input
  !	ERROR	L	Logical error flag	Output
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_constant.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  ! Local
  logical :: all, general, display, criteria
  character(len=12) :: argum
  integer :: nch
  integer :: nkey, mvoc
  parameter (mvoc=79)
  character(len=12) :: kw, voc(mvoc)
  data voc/'ALL',   'AMPLITUDE',    'ANGLE',        'ANTENNAS',      &
    'ASPECT_RATIO', 'ATMOSPHERE',   'AVERAGING',    'BANDS',         &
    'BASELINES',    'BINNING',      'CLOSURE',      'COPY',          &
    'CORRECTION',   'CRITERIA',     'DATA',         'DISPLAY',       &
    'DROP_FRACTIO', 'ERRORS',       'EXTENSION',    'FILE',          &
    'FIND',         'FLUX',         'FORMAT',       'FREQUENCY',     &
    'GAIN',         'GENERAL',      'GIBBS',        'HOUR_ANGLE',    &
    'IC',           'LEVEL',        'LIMITS',       'LINE',          &
    'MODE',         'NARROW_INPUT', 'NUMBER',       'OBSERVED_DAT',  &
    'OFFSETS',      'PHASES',       'PLANET',       'PLOT',          &
    'POLARIZATION', 'PROCEDURE',                                     &
    'PROJECT',      'QUALITY',      'RANGE',        'RECEIVER',      &
    'RECORD',       'REDUCED_DATE', 'REFERENCE',    'RF_PASSBAND',   &
    'SCANS',        'SOURCE',       'TELESCOPE',    'TYPE',          &
    'SELECTION',    'SCALING',      'SORT',         'SPECTRUM',      &
    'STEP',         'SUB_BANDS',    'TREC',         'TRIANGLES',     &
    'UV_RANGE',     'VIRTUAL',      'WEIGHTS',      'X',             &
    'Y',            'WATER',        'WIDEX_UNIT',   'BBAND',         &
    'IF',           'BLANK',        'SKIP',         'EXCLUDE',       &
    'SPIDX',        'VERSION',      'SPW',          'STOKES',        &
    'FINDPOL'/
  !------------------------------------------------------------------------
  ! Code:
  !
  error = .true.
  argum = '  '
  call sic_ke(line,0,1,argum,nch,.true.,error)
  if (error) goto 99
  call sic_ambigs('SHOW',argum,kw,nkey,voc,mvoc,error)
  if (error) goto 99
  all = kw.eq.'ALL'
  general = (kw.eq.'GENERAL') .or. all
  display = (kw.eq.'DISPLAY') .or. all
  criteria = (kw.eq.'CRITERIA') .or. all
  !
  call show_criteria(kw,criteria,error)
  call show_general(kw,general,line,error)
  call show_display(kw,display,error)
  if (error) write (6,*) 'Unknown SHOW Argument :',argum
  !
  error = .false.
99 return
end subroutine clic_show
!
subroutine show_criteria(argum,criteria,error)
  use gkernel_interfaces
  use gildas_def
  use clic_find
  !---------------------------------------------------------------------
  !     Display all or part of selection criteria
  !---------------------------------------------------------------------
  character(len=*) :: argum         !
  logical :: criteria               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_constant.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=12) :: argum1, argum2, argum3, argum4
  character(len=3) :: state(-1:1)
  character(len=12) :: proced(6:29)
  character(len=12) :: type(1:3)
  integer :: i
  data state /'ANY','NO','YES'/
  data proced/'Skydip',4*'?',   &
    'Cross-corr.','Auto-corr.','Gain-Ratio','Delay',   &
    'Focus','Pointing','Calibrate','IF-passband','On-Off',   &
    'Holography','Five-Point','Pseudo-Point','Flux-Meas.',   &
    'Stability','Calib.WVR','VLBI','VLBI(Gain)','On the fly',&
    'Bandpass'/
  data type /'Object','Phase_Cal','Bandpass_Cal'/
  !------------------------------------------------------------------------
  ! Code:
  if (len(argum).lt.4) return
  !
  ! SHOW NUMBER
  if (criteria .or. argum.eq. 'NUMBER') then
    if (snume2.lt.2147483647) then
      write(6,*) 'Observation Numbers : ',   &
        snume1,' to ',snume2
    else
      write(6,*) 'Observation Numbers : ',snume1,' to *'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW SCANS
  if (criteria .or. argum.eq. 'SCANS') then
    do i=1, snscan
      if (sscan2(i).lt.2147483647) then
        write(6,*) 'Scan Numbers : ',   &
          sscan1(i),' to ',sscan2(i)
      else
        write(6,*) 'Scan Numbers : ',sscan1(i),' to *'
      endif
    enddo
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW HOUR_ANGLE
  if (criteria .or. argum.eq. 'HOUR_ANGLE') then
    write(6,*) 'Hour Angles: ',shoura1*12./pi,' to ',   &
      shoura2*12./pi,' hours'
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW OBSERVED_DATE
  if (criteria .or. argum.eq.'OBSERVED_DATE') then
    call datec(sobse1,argum1,error)
    call datec(sobse2,argum2,error)
    write(6,100) 'Observation Date : ',   &
      argum1(1:lenc(argum1)),' to ',   &
      argum2(1:lenc(argum2))
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW OFFSET-RANGE
  if (criteria .or. argum.eq. 'OFFSETS'   &
    .or. argum.eq. 'RANGE') then
    call offsec(soffs1,argum1,error)
    call offsec(soffl1,argum2,error)
    call offsec(soffs2,argum3,error)
    call offsec(soffl2,argum4,error)
    write(6,100) 'Offset range : ',argum1(1:lenc(argum1)),   &
      ' to ',argum2(1:lenc(argum2)),' and ',   &
      argum3(1:lenc(argum3)),' to ',argum4(1:lenc(argum4))
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW REDUCED_DATE
  if (criteria .or. argum(1:3).eq. 'REDUCED_DATE') then
    call datec(sredu1,argum1,error)
    call datec(sredu2,argum2,error)
    write(6,100) 'Reduction Date : ',   &
      argum1(1:lenc(argum1)),' to ',   &
      argum2(1:lenc(argum2))
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW SOURCE
  if (criteria .or. argum.eq. 'SOURCE') then
    if (nsour.eq.0) then
      write(6,100) 'Source: Any'
    else
      write(6,100) 'Source: ',(sssour(i),i=1,nsour)
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW TYPE
  if (criteria .or. argum.eq. 'TYPE') then
    if (sitype.gt.0) then
      write(6,100) 'Source Type: ',type(sitype)
    else
      write(6,100) 'Source Type: Any'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW PROCEDURE
  if (criteria .or. argum.eq. 'PROCEDURE') then
    if (nproc .gt. 0) then
      write(6,100) 'Procedure: ',(proced(ssproc(i)),i=1,nproc)
    else
      write(6,100) 'Procedure: Any'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW EXCLUDE
  if (criteria .or. argum.eq. 'EXCLUDE') then
    if (nexcl .gt. 0) then
      write(6,100) 'Excluded Procedure: ',(proced(ssexcl(i)),i=1,nexcl)
    else
      write(6,100) 'Excluded procedure: None'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW LINE
  if (criteria .or. argum.eq. 'LINE') then
    write(6,100) 'Molecular transition ',sline
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW TELESCOPE
  if (criteria .or. argum.eq. 'TELESCOPE') then
    write(6,100) 'Telescope ',steles
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW PROJECT
  if (criteria .or. argum.eq.'PROJECT') then
    write(6,*) 'Project is ',sproject
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW IC
  if (criteria .or. argum.eq.'IC') then
    write(6,100) 'IC state is ',state(setic)
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW BPC
  if (criteria .or. argum.eq.'BPC') then
    write(6,100) 'BPC state is ',state(sbpc)
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW FIND
  if (criteria .or. argum.eq.'FIND') then
    if (find_update) then
      write(6,100) 'FIND mode is UPDATE'
    else
      write(6,100) 'FIND mode is NOUPDATE'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW RECEIVER
  if (criteria .or. argum.eq.'RECEIVER') then
    if (srecei.ge.0) then
      write(6,101) 'Selected receiver is ',srecei
    else
      write(6,100) 'ALL receivers selected'
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW QUALITY
  if (criteria .or. argum.eq.'QUALITY') then
    if (squal.ge.0 .and. squal.le.9) then
      write(6,101) 'Selected data quality is ', squal,   &
        ' ('//quality(squal)(1:lenc(quality(squal)))//')'
    else
      write(6,100) 'Wrong data quality ',squal
    endif
    error = .false.
    if (.not.criteria) return
  endif
  !
  ! SHOW SKIP
  if (criteria .or. argum.eq.'SKIP') then
    if (iskip.eq.1) then
      write(6,100) 'SKIP autocorrelation in cross-correlation mode' 
    else
      write(6,100) 'Do not SKIP autocorrelation in cross-correlation mode' 
    endif
  endif
  !
  ! SHOW FINDPOL
  if (criteria .or. argum.eq.'FINDPOL') then
    if (snpol.gt.0) then
      do i=1, snpol
        write(6,*) 'FINDPOL ',spol(i)
      enddo
    else
      write(6,*) 'FINDPOL ','*'
    endif
  endif
  !
  return
100 format(20(1x,a))
101 format(1x,a,i3,a)
end subroutine show_criteria
!
subroutine show_general(argum,general,line,error)
  use gkernel_interfaces
  use gildas_def
  use clic_index
  use clic_file, except=>i
  use clic_virtual
  character(len=*) :: argum         !
  logical :: general                !
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_constant.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_title.inc'
  include 'clic_display.inc'
  ! Local
  integer :: nch, j, i, ier, l
  character(len=800) :: chain
  character(len=24) :: argum2, catm, cnoatm, cwvr, cnowvr, cwvre, cnowvre
  character(len=24) :: cmon, cnomon, ccor, cnocor
  character(len=11) :: dch, dfr
  real :: ipb_size,size
  character(len=12) :: vpol(4)
  data vpol/'OFF','HORIZONTAL','VERTICAL','BOTH'/
  !------------------------------------------------------------------------
  ! Code:
  if (len(argum).lt.4) return
  !
  ! SHOW ANGLE
  if (general .or. argum.eq. 'ANGLE') then
    write(6,*) 'Angle unit is '//sangle
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW CORRECTION
  if (general .or. argum.eq. 'CORRECTION') then
    write(6,*) 'Phase correction factors:'
    write(6,*) (phase_corr(1,i),i=1, mnant)
    write(6,*) (phase_corr(2,i),i=1, mnant)
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW DATA
  if (general .or. argum.eq. 'DATA') then
    write(6,'(1X,A,I6,A,I6,A)') 'Displaying ',m_data,   &
      ' points in each of ',m_boxes,' boxes '
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW EXTENSION
  if (general .or. argum.eq. 'EXTENSION') then
    write(6,*) 'Default Extensions ', (defext(i),i=1, ndefext)
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW FILE [NB: there is no SET FILE command, see FILE]
  if (general .or. argum.eq. 'FILE') then
    call info(error)
    !         print *, 'ipb_size ',ipb_size(0)
    error = .false.
    if (.not.general) return
  endif
  !
  !
  ! SHOW VERSION 
  if (general .or. argum.eq. 'VERSION') then
    write(6,*) 'Current observation version is ', version_current
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW FORMAT
  if (general .or. argum.eq. 'FORMAT') then
    if (sheade.eq.'b') then
      write(6,*) 'Header format BRIEF '
    else
      write(6,*) 'Header format LONG, information written:'
      nch = 1
      if (t_interfero) then
        chain(nch:) = 'Interferometer'
        nch = nch + 19
      endif
      if (t_position) then
        chain(nch:) = 'Position'
        nch = nch + 13
      endif
      if (t_quality) then
        chain(nch:) = 'Quality'
        nch = nch+12
      endif
      if (t_spectral) then
        chain(nch:) = 'Spectral'
        nch = nch+12
      endif
      if (t_calibration) then
        chain(nch:) = 'Calibration'
        nch = nch+12
      endif
      if (t_status) then
        chain(nch:) = 'Status'
        nch = nch+12
      endif
      if (t_pointing) then
        chain(nch:) = 'Pointing'
        nch = nch+12
      endif
      if (t_atmosphere) then
        chain(nch:) = 'Atmosphere'
        nch = nch+12
      endif
      if (nch.gt.1) write(6,*) chain(1:nch-1)
    endif
    error = .false.
    if (.not.general) return
  endif
  !
  ! SHOW LEVEL
  if (general .or. argum .eq. 'LEVEL') then
    i = 1
    call message_level(i)
    j = i
    call message_level(i)
    write(6,*) 'Message display level is set to ',j
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW STEP
  if (general .or. argum .eq. 'STEP') then
    write(6,*) 'Time step for splines is ',t_step,' hours.'
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW FLUX
  if (general .or. argum .eq. 'FLUX') then
    if (n_flux.le.0) then
      write(6,*) 'No list of fluxes'
    else
      argum2 = '*'
      call sic_ch(line,0,2,argum2,nch,.false.,error)
      if (error) goto 999
      do i=1, n_flux
        if (argum2.eq.'*' .or. c_flux(i).eq.argum2) then
          if (date_flux(i).gt.-99999) then
            call datec (date_flux(i),dch,error)
          else
            dch = ' [any date]'
          endif
          if (freq_flux(i).gt.0) then
            write(dfr,'(f7.1,a)') freq_flux(i)/1000.,' GHz'
          else
            dfr = '[any freq.]'
          endif
          if (f_flux(i)) then
            write(6,1002) c_flux(i), dfr,   &
              dch, flux(i), ' Jy FIXED'
          else
            write(6,1002) c_flux(i), dfr,   &
              dch, flux(i), ' Jy FREE'
          endif
        endif
      enddo
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW SPIDX
  if (general .or. argum .eq. 'SPIDX') then
    if (n_spidx.le.0) then
      write(6,*) 'No list of fluxes'
    else
      argum2 = '*'
      call sic_ch(line,0,2,argum2,nch,.false.,error)
      if (error) goto 999
      do i=1, n_spidx
        if (argum2.eq.'*' .or. c_spidx(i).eq.argum2) then
          if (date_spidx(i).gt.-99999) then
            call datec (date_spidx(i),dch,error)
          else
            dch = ' [any date]'
          endif
          if (freq_spidx(i).gt.0) then
            write(dfr,'(f7.1,a)') freq_spidx(i)/1000.,' GHz'
          else
            dfr = '[any freq.]'
          endif
          if (f_spidx(i)) then
            write(6,1002) c_spidx(i), dfr,   &
              dch, spidx(i), ' FIXED'
          else
            write(6,1002) c_spidx(i), dfr,   &
              dch, spidx(i), ' FREE'
          endif
        endif
      enddo
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW CLOSURE
  if (general .or. argum .eq. 'CLOSURE') then
    write(6,*) 'Weights for closure are '
    do i = 1, mnbas
      write(6,*) ' _ baseline ',cbas(i),' w= ',w_c(i)
    enddo
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW ATMOS
  if (general .or. argum.eq.'ATMOSPHERE' .or. argum.eq.'TREC'   &
    .or. argum.eq.'WATER') then
    write(6,*) 'Atmospheric calibration modes:'
    do i=1, mnant
      if (atm_mode(i).eq.'AUTO') then
        if (atm_water.gt.0) then
          write(6,*) 'Ant ',i,' mode AUTO, w(H2O)= ',atm_water
        elseif (atm_water.eq.0) then
          write(6,*) 'Ant ',i,   &
            ' mode AUTO, w(H2O) from humidity'
        elseif (atm_water.lt.0) then
          write(6,*) 'Ant ',i,   &
            ' mode AUTO, w(H2O) from file or TREC antennas'
        endif
      elseif (atm_mode(i).eq.'TREC') then
        if (atm_trec(i).gt.0) then
          write(6,*) 'Ant ',i,' mode TREC, Trec= ',atm_trec(i)
        else
          write(6,*) 'Ant ',i,   &
            ' mode TREC, Trec value taken from file'
        endif
      elseif (atm_mode(i).eq.'FILE') then
        write(6,*) 'Ant ',i,' mode FILE: take mode from data'
      else
        write(6,*) 'Ant ',i,' mode ',atm_mode(i)
      endif
    enddo
    if (atm_interpolate) then
      write(6,*) 'Atmospheric model interpolated from table'
    else
      write(6,*) 'Atmospheric model version: ', atmmodel
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW GAIN
  if (general .or. argum .eq. 'GAIN') then
    write(6,*) 'Receiver Gain Ratios'
    do i=1, mnant
      if (atm_gim(i).gt.0) then
        write(6,*) 'Ant ',i,' I/S= ',atm_gim(i)
      else
        write(6,*) 'Ant ',i,' I/S - take from data'
      endif
    enddo
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW REFERENCE
  if (general .or. argum .eq. 'REFERENCE') then
    write(6,*) 'Reference antenna is antenna ',ref_ant
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW AMPLITUDE
  if (general .or. argum.eq.'AMPLITUDE') then
    if (do_amplitude) then
      write(6,*) 'Amplitudes are relative to calibrator amplitude'
    else
      write(6,*) 'Amplitudes are absolute'
    endif
    if (do_amplitude_antenna) then
      write(6,*) 'Amplitude Calibration is antenna-based'
    else
      write(6,*) 'Amplitude Calibration is baseline-based'
    endif
    if (do_size) write(6,*)   &
      'Amplitudes are corrected for calibrator model size'
    if (do_scale) write(6,*)   &
      'Amplitudes are divided by assumed calibrator flux'
    if (do_flux) then
      write(6,*) 'Amplitudes are expressed in janskys'
    elseif (.not.do_raw) then
      write(6,*) 'Amplitudes are expressed in kelvins'
    else
      write(6,*) 'Amplitudes are Raw'
    endif
    if (do_spidx) then
      write(6,*) 'Scaling by spectral index is ON'
    else
      write(6,*) 'Scaling by spectral index is OFF'
    endif
    error = .false.
  endif
  !
  ! SHOW PHASE
  if (general .or. argum.eq.'PHASES') then
    if (do_phase) then
      write(6,*) 'Phases are relative to calibrator phase'
    else
      write(6,*) 'Phases are absolute'
    endif
    if (do_phase_antenna) then
      write(6,*) 'Phase Calibration is antenna-based'
    else
      write(6,*) 'Phase Calibration is baseline-based'
    endif
    if (do_phase_ext) then
      write(6,*) 'Phase reference is external (other receiver)'
    else
      write(6,*) 'Phase reference is internal (same receiver)'
    endif
    write(6,*) ' '
    catm = ' '
    cnoatm = ' '
    cwvr = ' '
    cnowvr = ' '
    cwvre = ' '
    cnowvre = ' '
    cmon = ' '
    cnomon = ' '
    ccor = ' '
    cnocor = ' '
    do i=1, mnant
      if (do_phase_atm(i)) then
        l = lenc(catm)
        catm = catm(1:l)//' '//char(ichar('0')+i)
      else
        l = lenc(cnoatm)
        cnoatm = cnoatm(1:l)//' '//char(ichar('0')+i)
      endif
      if (do_phase_wvr(i)) then
        l = lenc(cwvr)
        cwvr = cwvr(1:l)//' '//char(ichar('0')+i)
      else
        l = lenc(cnowvr)
        cnowvr = cnowvr(1:l)//' '//char(ichar('0')+i)
      endif
      if (do_phase_wvr_e(i)) then
        l = lenc(cwvre)
        cwvre = cwvre(1:l)//' '//char(ichar('0')+i)
      else
        l = lenc(cnowvre)
        cnowvre = cnowvre(1:l)//' '//char(ichar('0')+i)
      endif
      if (do_phase_mon(i)) then
        l = lenc(cmon)
        cmon = cmon(1:l)//' '//char(ichar('0')+i)
      else
        l = lenc(cnomon)
        cnomon = cnomon(1:l)//' '//char(ichar('0')+i)
      endif
      if (do_phase_mon(i).or.do_phase_wvr(i)) then
        l = lenc(ccor)
        ccor = ccor(1:l)//' '//char(ichar('0')+i)
      else
        l = lenc(cnocor)
        cnocor = cnocor(1:l)//' '//char(ichar('0')+i)
      endif
    enddo
    if (lenc(catm).gt.0) then
      write(6,*) 'Using real-time atmospheric phase correction,'   &
        //' antennas '//catm
      if (do_phase_nofile) then
        write(6,*)   &
          '  (ignoring invalidation by STORE CORRECTION)'
      else
        write(6,*)   &
          '  (according to validation by STORE CORRECTION)'
      endif
    endif
    if (lenc(cnoatm).gt.0) then
      write(6,*) 'Using no real-time atmospheric phase'   &
        //' correction, antennas '//cnoatm
    endif
    if (lenc(ccor).gt.0) then
      write(6,*) 'Using off-line atmospheric phase correction'
      if (lenc(cwvr).gt.0) then
        write(6,*) '  based on WVR, antennas '//cwvr
        if (lenc(cwvre).gt.0) then
          write(6,*) '  (WVR correction is empirical,'   &
            //' antennas '//cwvre//')'
          if (lenc(cnowvre).gt.0) then
            write(6,*) '  (WVR correction is model-based,'   &
              //' antennas '//cnowvre//')'
          endif
        endif
      endif
      if (lenc(cmon).gt.0) then
        write(6,*) '  based on total power monitor,'   &
          //' antennas '//cmon
      endif
    endif
    if (lenc(cnocor).gt.0) then
      write(6,*) 'Using no off-line atmospheric phase correction,'   &
        //' antennas '//cnocor
    endif
    write(6,*) ' '
    error = .false.
  endif
  !
  ! SHOW ERROR
  if (general .or. argum.eq.'ERRORS') then
    if (do_bar_atm) then
      write(6,*)   &
        'Atmospheric fluctuations are included in phase error bars'
    else
      write(6,*) 'No atmospheric fluctuations are '//   &
        'included in phase error bars'
    endif
    error = .false.
  endif
  !
  ! SHOW PLANET
  if (general .or. argum .eq. 'PLANET') then
    if (planet_model.eq.-1) then
      write(6,*) 'Planet model: elliptical disk'
    elseif (planet_model.eq.-2) then
      write(6,*) 'Planet model: '//   &
        'circular disk with primary beam attenuation'
    else
      write(6,*) 'Planet model: '//   &
        'Special spline file'
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW RF_PASSBAND
  if (general .or. argum.eq.'RF_PASSBAND') then
    if (do_pass) then
      write(6,*) 'RF Passband Calibration is applied'
    else
      write(6,*) 'RF Passband Calibration is not applied'
    endif
    if (do_pass_spectrum) then
      write(6,*) 'RF Passband Calibration is interpolated from spectrum'
    else
      if (do_pass_freq) then
        write(6,*) 'RF Passband Calibration is frequency dependent'
      else
        write(6,*) 'RF Passband Calibration is channel dependent'
      endif
    endif
    if (do_pass_antenna) then
      write(6,*) 'RF Passband Calibration is antenna-based'
    else
      write(6,*) 'RF Passband Calibration is baseline-based'
    endif
    if (do_pass_memory) then
      write(6,*) 'RF Passband Calibration from program memory'
    else
      write(6,*) 'RF Passband Calibration from input file'
    endif
    error = .false.
  endif
  !
  ! SHOW WEIGHTS
  if (general .or. argum .eq. 'WEIGHTS') then
    if (weight_tsys) then
      write(6,*) 'Weighting by 1/TSYS**2 applied in TABLE'
    else
      write(6,*) 'Weighting by 1/TSYS**2 NOT applied in TABLE'
    endif
    if (weight_cal) then
      write(6,*)   &
        'Weighting by instrumental func. applied in TABLE'
    else
      write(6,*)   &
        'Weighting by instrumental func. NOT applied in TABLE'
    endif
    if (weight_pass) then
      write(6,*)   &
        'Weighting by bandpass applied in TABLE'
    else
      write(6,*)   &
        'Weighting by bandpass NOT applied in TABLE'
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW SCALING
  if (general .or. argum .eq. 'SCALING') then
    if (do_scale) then
      write(6,*) 'Scaling is ON'
    else
      write(6,*) 'Scaling is OFF'
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW SPECTRUM
  if (general .or. argum .eq. 'SPECTRUM') then
    if (do_spec) then
      write(6,*) 'Spectrum processing is ON'
    else
      write(6,*) 'Spectrum processing is OFF'
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW FREQUENCY
  if (general .or. argum .eq. 'FREQUENCY') then
    write(6,*) 'Selected frequencies:'
    write(6,1000) ' C   USB ',c_line(1),c_freq(1),   &
      ' C   LSB ',c_line(2),c_freq(2)
    do j=1, mrlband
      write(6,1001) ' L',j,' USB ',l_line(j,1),l_freq(j,1),   &
        ' L',j,' LSB ',l_line(j,2),l_freq(j,2)
    enddo
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW VIRTUAL
  if (general .or. argum .eq. 'VIRTUAL') then
    size = 16  ! Default used in 'get_memory'
    ier = sic_ramlog('SPACE_CLIC',size)
    write(6,*) 'Default memory space is ', size, ' Mbytes'
    error = .false.
    if (.not. general) return
  endif
  !
  ! SHOW SELECTION
  if (general .or. argum .eq. 'SELECTION') then
    if (all_select) then
      if (widex_select) then
        chain = 'WIDEX, from all units'
      else
        chain = 'from all line subbands'
      endif
    else
      write(chain,*) (csub(isubb_out(i))//' ',i=1,lsubb_out)
    endif
    nch = lenc(chain)
    if (cont_select) then
      if (isideb_out.lt.3) then
        write(6,*) 'Selection is CONTINUUM, '//cband(isideb_out)   &
          //', '//chain(1:nch)
      elseif(isideb_out.eq.3) then
        write(6,*) 'Selection is CONTINUUM, DSB, '//chain(1:nch)
        if (freq_out.eq.0) then
          write(6,*) 'U and V will refer to L.O.1 frequency.'
        else
          write(6,'(a,f10.3,a)') ' U and V will refer to ',   &
            freq_out,'  MHz'
        endif
      elseif (isideb_out.eq.4) then
        write(6,*) 'Selection is CONTINUUM, automatic receiver rejection detection, '//chain(1:nch)
      else
        write(6,*) 'Selection is CONTINUUM, SSB (automatic sideband determination), '//chain(1:nch)
      endif
    else
      if (isideb_out.lt.3) then
        write(6,*) 'Selection is LINE, '//cband(isideb_out)   &
          //', '//chain(1:nch)
      else
        write(6,*) 'Selection is LINE, '//chain(1:nch)
        write(6,*) '(Sideband selection is automatical)'
      endif
    endif
    if (nwin_out.gt.0) then
      write(6,*)    'Frequency windows: '
      do i=1, nwin_out
        if (window_out(1,i).gt.1.   &
          .and. window_out(2,i).lt.9.99e5) then
          write(6,'(a,i2,a,2(f10.3,a))') '  # ',i,' ',   &
            window_out(1,i),' to ', window_out(2,i), '  MHz'
        elseif (window_out(1,i).le.1.) then
          write(6,'(a,i2,a,f10.3,a)') '  # ',i,   &
            ' lower than    ', window_out(2,i), '  MHz'
        elseif (window_out(2,i).ge.9.99e5) then
          write(6,'(a,i2,a,f10.3,a)') '  # ',i,   &
            ' higher than   ', window_out(1,i), '  MHz'
        endif
      enddo
    else
      write(6,*) 'All frequencies selected.'
    endif
    error = .false.
    if (.not. general) return
  endif
  !
  !  SHOW SORT
  if (general .or. argum.eq.'SORT') then
    if (do_sort_receiver) then
      write(6,*) 'Scan lists sorted by receiver'
    elseif (do_sort_num) then
      write(6,*) 'Scan lists sorted by observation number'
    else
      write(6,*) 'Scan lists sorted by scan number'
    endif
  endif
  !
  !  SHOW COPY
  if (general .or. argum.eq.'COPY') then
    if (do_write_data) then
      write(6,*) 'Data are written in ouput file by COPY'
    else
      write(6,*) 'Headers only are written in ouput file by COPY'
    endif
  endif
  !
  !  SHOW NARROW_INPUT
  if (general .or. argum.eq.'NARROW_INPUT') then
    if (narrow_input.gt.0) then
      write(6,*) 'Narrow-band correlator input : ',narrow_input
    else
      write(6,*) 'No narrow-band correlator input selected'
    endif
  endif
  !
  !  SHOW POLARIZATION
  if (general .or. argum.eq.'POLARIZATION') then
    dch = vpol(do_polar+1)
    l = lenc(dch)
    write(6,*) 'Polarization mode: '//dch(1:l)
  endif
  !
  !  SHOW WIDEX_UNIT
  if (general .or. argum.eq.'WIDEX_UNIT') then
    if (widex_unit.gt.0) then
      write(6,*) 'WIDEX unit number : ',widex_unit
    else
      write(6,*) 'No WIDEX unit selected'
    endif
  endif
  !
  !  SHOW SPIDX
  if (general .or. argum.eq.'SPIDX_CORR') then
  endif
  !
  return
999 error = .true.
  return
1000 format(2(a,a12,1x,f10.3))
1001 format(2(a,i2.2,a,a12,1x,f10.3))
1002 format(1x,a12,4x,a11,3x,a11,1x,f10.4,1x,a)
end subroutine show_general
!
subroutine show_display(argum,display,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  use gio_params
  character(len=*) :: argum         !
  logical :: display                !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  include 'clic_stations.inc'
  include 'clic_clic.inc'
  ! Local
  character(len=40) :: ch1, ch2
  character(len=255) :: chain
  integer :: i, l, j, a1, a2, ii
  integer :: phys_log(mnant)
  real :: bl(mnbas)
  !------------------------------------------------------------------------
  ! Code:
  if (len(argum).lt.4) return
  !
  ! CONVERSION PHYSICAL TO LOGICAL ANTENNAS
  do i=1,mnant
    phys_log(i) = mnant+1
  enddo
  do i=1,r_nant
    phys_log(r_kant(i)) = i
  enddo
  !
  ! SHOW AVERAGING
  if (display .or. argum .eq. 'AVERAGING') then
    if (i_average.eq.0) then
      if (j_average.eq.0) then
        write(6,*) 'No Time Averaging, method Scalar'
      else
        write(6,*) 'No Time Averaging, method Vector'
      endif
    elseif (i_average.eq.1) then
      if (j_average.eq.0) then
        write(6,*) 'Scan Averaging, method Scalar'
      else
        write(6,*) 'Scan Averaging, method Vector'
      endif
      if (s_average.gt.0) then
        write(6,*) 'Averaging ',s_average, &
            ' scans together' 
      endif
    elseif (i_average.eq.2) then
      if (j_average.eq.0) then
        write(6,*) 'Time Averaging by ',   &
          t_average,', method Scalar'
      else
        write(6,*) 'Time Averaging by ',   &
          t_average,', method Vector'
      endif
    endif
    if (k_average.eq.0) then
      write(6,*) 'Side-band average computed with continuum spectral windows'
    else
      write(6,*) 'Side-band average computed with line spectral windows'
    endif
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW ANTENNAS
  if (display .or. argum.eq. 'ANTENNAS') then
    if (i_base(1).gt.0) then
      !
      ! Baseline-based plot
      !
      if (.not.display) then
        write(6,*)   &
          'Caution: plot is baseline based (use SHOW BASELINE)'
        if (r_nant.gt.0) then
          write(6,*) 'Antennas available in data:'
          write(6,1010)
          do i=1,r_nant
            write(6,1011) i, r_kant(i),r_config(4*i-3:4*i)
          enddo
        endif
      endif
    else
      !
      ! Antenna-based plot
      !
      if (all_base) then
        write(6,*) 'All antennas selected:'
      else
        write(6,*) 'Antennas selected:'
      endif
      if (phys_base) then
        write(6,1015)
        do i=1,n_base
          if (r_nant.eq.0) then
            write(6,1017) -i_base(i)
          elseif (-i_base(i).gt.mnant) then
            write(6,1018) -i_base(i)-mnant
          else
            ii=-i_base(i)
            write(6,1016) r_kant(ii),-i_base(i),   &
              r_config(4*ii-3:4*ii),r_ant(1,ii)-stat99(1,r_istat(ii)), &
              r_ant(2,ii)-stat99(2,r_istat(ii)), &
              r_ant(3,ii)-stat99(3,r_istat(ii))
          endif
        enddo
      else
        write(6,1010)
        do i=1,n_base
          if (r_nant.eq.0) then
            write(6,1012) -i_base(i)
          elseif (-i_base(i).gt.r_nant) then
            write(6,1013) -i_base(i)
          else
            ii=-i_base(i)
            write(6,1011) -i_base(i), r_kant(ii),   &
              r_config(4*ii-3:4*ii),r_ant(1,ii)-stat99(1,r_istat(ii)), &
              r_ant(2,ii)-stat99(2,r_istat(ii)), &
              r_ant(3,ii)-stat99(3,r_istat(ii))
          endif
        enddo
      endif
    endif
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW BASELINES
  if (display .or. argum.eq. 'BASELINES') then
    if (i_base(1).lt.0) then
      !
      ! Antenna-based plot
      !
      if (.not.display) then
        write(6,*)   &
          'Caution: plot is antenna based (use SHOW ANTENNAS)'
        if (r_nbas.gt.0) then
          write(6,*) 'Baselines available in data:'
          write (6,1000)
          do i=1,r_nbas
            a1=r_iant(i)
            a2=r_jant(i)
            bl(i) = sqrt(r_bas(1,i)**2   &
              +r_bas(2,i)**2   &
              +r_bas(3,i)**2)
            write(6,1002) a1,a2,r_kant(a1),r_kant(a2),bl(i)
          enddo
        endif
      endif
    else
      !
      ! Baseline-based plot
      !
      if (all_base) then
        write(6,*) 'All baselines selected:'
      else
        write(6,*) 'Baselines selected:'
      endif
      write (6,1000)
      !           WRITE (6,1001)
      do i=1,n_base
        ch2=cbas(i_base(i))
        if (mnant.lt.10) then
          a1=iachar(ch2(1:1))-iachar('0')
          a2=iachar(ch2(2:2))-iachar('0')
        else          
          read(ch2,'(i2,1x,i2)') a1, a2
        endif
        if (r_nbas.eq.0) then
          write(6,1003) a1,a2
        elseif (r_kant(a1).eq.0) then
          write(6,1004) a1,a2,a1
        elseif (r_kant(a2).eq.0) then
          write(6,1004) a1,a2,a2
        else
          bl(i) = sqrt(r_bas(1,i_base(i))**2   &
            +r_bas(2,i_base(i))**2   &
            +r_bas(3,i_base(i))**2)
          write(6,1002) a1,a2,r_kant(a1),r_kant(a2),bl(i)
        endif
      enddo
    endif
    !! OLD SHOW TRIANGLE
    !!         ELSEIF (I_BASE(1).GT.MNBAS .AND.
    !!     &   I_BASE(1).LE.MNBAS+MNTRI) THEN
    !!            WRITE(6,*) 'Triangles 	: ',
    !!     &      (CTRI(I_BASE(I)-MNBAS),' 	',I=1, N_BASE), CH1
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW BANDS
  if (display .or. argum .eq. 'BANDS') then
    write(6,*) 'Band(s) 	: ',   &
      (cband(i_band(i)),' 	',i=1, n_band)
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW BINNING
  if (display .or. argum .eq. 'BINNING') then
    if (do_bin) then
      write(6,*) 'Size and position of bins: ',   &
        bin_size, bin_pos
    else
      write(6,*) 'Binning is OFF'
    endif
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW BLANK
  if (display .or. argum .eq. 'BLANK') then
    do i=1,mbands
      if (nmask(i).gt.0) then
        write(6,*) 'Spw ',i,' :'
        do j=1, nmask(i)
           write(6,*) '   Channel range: ',cmask(1,j,i),' to ',cmask(2,j,i)
        enddo
      endif
    enddo
    if (imask.eq.0) then
      write(6,*) 'Blanked channels are blanked'
    else
      write(6,*) 'Blanked channels are interpolated'
    endif
  endif
  !
  ! SHOW DROP_FRACTION
  if (display .or. argum .eq. 'DROP_FRACTIO') then
    write(6,*) 'Fraction of subbands dropped:  ',   &
      fdrop(1), fdrop(2)
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW GIBBS
  if (display .or. argum .eq. 'GIBBS') then
    write(6,*) 'Number of channels dropped (GIBBS effect):  ',   &
      ngibbs
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW BBAND
  if (display .or. argum .eq. 'BBAND') then
    if (bb_select) write(6,*) 'Baseband mode selected'
    if (all_bband) write(6,*) 'All baseband selected'
    do i=1, n_baseband
        write(6,'(a,i2,a,11(1x,i2))') ' Baseband group ',i,   &
          '	: ',(i_baseband(j,i),j=1,l_baseband(i))
    enddo
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW IF
  if (display .or. argum .eq. 'IF') then
    if (if_select) write(6,*) 'IF mode selected'
    do i=1, n_if
        write(6,'(a,i2,a,11(1x,i2))') 'IF group ',i,   &
          '	: ',(i_if(j,i),j=1,l_if(i))
    enddo
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW STOKES
  if (display .or. argum .eq. 'STOKES') then
    do i=1, n_stokes
      if (i_stokes(i).ne.code_stokes_ratio) then
        write(6,'(a,a)') 'Stokes parameter ', &
              gio_stokes_names(i_stokes(i))
      else
        write(6,'(a,a)') 'Stokes parameter [HH-VV]/[HH+VV])'
      endif
    enddo
    error = .false.
    if (do_leakage) write(6,'(a)') 'Computing leakage'
    if (do_corrected) write(6,'(a)') 'Leakages are corrected'

    if (.not. display) return
  endif
  !
  ! SHOW SUB_BANDS
  if (display .or. argum .eq. 'SUB_BANDS'.or. argum.eq.'SPW') then
    if (subb_select) write(6,*) 'Subband mode selected'
    if (all_subb) write(6,*) 'All subbands'
    if (each_subb) write(6,*) 'Each subband'
    if (pol_subb.eq.1) write(6,*) 'HOR polarization subbands'
    if (pol_subb.eq.2) write(6,*) 'VER polarization subbands'
    if (quar_subb.eq.1) write(6,*) 'Q1 (4.2-5.2 GHz) subbands'
    if (quar_subb.eq.2) write(6,*) 'Q2 (5.0-6.0 GHz) subbands'
    if (quar_subb.eq.3) write(6,*) 'Q3 (6.0-7.0 GHz) subbands'
    if (quar_subb.eq.4) write(6,*) 'Q4 (6.8-7.8 GHz) subbands'
    if (nbc_subb.eq.1) write(6,*) 'First corr. entry subbands'
    if (nbc_subb.eq.2) write(6,*) 'Second corr. entry subbands'
    do i=1, n_subb
      if (i_subb(1,i).le.mbands   &
        .or. (sw1(i).eq.'*'.and.sw2(i).eq.'*')) then
        write(6,'(a,i2,11(1x,a))') ' Subband group ',i,   &
          '	: ',(csub(i_subb(j,i)),j=1,l_subb(i))
      else
        write(6,'(a,i2,11(1x,a),$)') ' Subband group ',i,   &
          '	: ',(csub(i_subb(j,i)),j=1,l_subb(i))
        write(6,'(a,i4,a,i4)')   &
          '+   window :',iw1(i),' to ',iw2(i)
      endif
    enddo
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW LIMITS, X, Y
  if (display .or. argum.eq.'LIMITS' .or. argum.eq.'X') then
    do i=1, n_x
      if (sm_x1(i).eq.'F') then
        write(ch1,'(1pg10.3)') gu1_x(i)
      else
        ch1 = sm_x1(i)
      endif
      if (sm_x2(i).eq.'F') then
        write(ch2,'(1pg10.3)') gu2_x(i)
      else
        ch2 = sm_x2(i)
      endif
      write(6,*) 'X axis	 : '//clab(i_x(i))//', '   &
        //ch1(1:lenc(ch1))//' to  '//ch2(1:lenc(ch2))
    enddo
    error = .false.
  endif
  if (display .or. argum.eq.'LI' .or. argum(1:1).eq.'Y') then
    do i=1, n_y
      if (sm_y1(i).eq.'F') then
        write(ch1,'(1pg10.3)') gu1_y(i)
      else
        ch1 = sm_y1(i)
      endif
      if (sm_y2(i).eq.'F') then
        write(ch2,'(1pg10.3)') gu2_y(i)
      else
        ch2 = sm_y2(i)
      endif
      write(6,*) 'Y axis	 : '//clab(i_y(i))//', '   &
        //ch1(1:lenc(ch1))//' to  '//ch2(1:lenc(ch2))
    enddo
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW ASPECT_RATIO
  if (display .or. argum .eq. 'ASPECT_RATIO') then
    if (aratio.gt.0) then
      write(6,*) 'Box aspect ratio is X/Y = ',aratio,' (EXACT)'
    else
      write(6,*) 'Box aspect ratio is X/Y = ',abs(aratio),   &
        ' (AUTO)'
    endif
    error = .false.
    if (.not. display) return
  endif
  !
  ! SHOW MODE
  if (display .or. argum.eq. 'MODE') then
    write (6,*) 'Plot mode is ',plot_mode
    write (6,*) 'Lowres mode is ',lowres
    error = .false.
    if (.not.display) return
  endif
  !
  ! SHOW PLOT
  if (display .or. argum.eq. 'PLOT') then
    write (6,*) 'Plot type is ',plot_type
    error = .false.
    if (do_physical) then
      write (6,*) 'Physical antennas'
    else
      write (6,*) 'Logical antennas'
    endif
    if (.not.display) return
  endif
  !
  ! SHOW PHASES
  if (display .or. argum.eq. 'PHASES') then
    if (degrees) then
      chain = 'Degrees'
    else
      chain = 'Radians'
    endif
    l = 7
    if (continuous) then
      chain(l+1:) = ' Continuous'
      l = l + 11
      write(chain(l+1:l+3),'(1x,i2)') n_cont
      l = l + 3
    else
      chain(l+1:) = ' Jumpy'
      l = l + 6
    endif
    write (6,*) 'Phases are ',chain(1:l)
    error = .false.
  endif
  !
  ! SHOW RECORD
  if (display .or. argum.eq. 'RECORD') then
    l = 1
    do i=1, n_loop
      if (rec_2(i).ne.rec_1(i)) then
        write(chain(l:),'(i12,a,i12,a,i12)')   &
          rec_1(i),' TO ',rec_2(i),' BY ',rec_3(i)
      else
        write(chain(l:),*) rec_1(i)
      endif
      l = lenc(chain)
      call sic_blanc(chain,l)
      chain = chain(1:l)//' '
      l = l + 2
    enddo
    write(6,*) 'Records: '//chain(1:l)
    error = .false.
    if (.not.display) return
  endif
  !
  ! SHOW UV_RANGE
  if (display .or. argum.eq. 'UV_RANGE') then
    write(6,*) 'UV range is from ',uv_range(1),' to ',   &
      uv_range(2),' m.'
  endif
  !
  return
  !
1000 format(' Logical | Physical | Length (m)')
1001 format('--------------------------------')
1002 format(i4,i3,'  |',i4,i3,'   |',2x,f8.3)
1003 format(i4,i3,'  |',' Undefined: no data read')
1004 format(i4,i3,'  |',' Warning: antenna ',i2,' not in the array')
  !
1010 format(' Logical | Physical | Station  |  X Offset  |  Y Offset  |  Z Offset /99')
1011 format(1x,i4,4x,'|'1x,i4,5x'|',3x,a,3x,'|',f11.6,1x,'|',f11.6,1x,'|',f11.6)
1012 format(1x,i4,4x,'| Undefined: no data read')
1013 format(1x,i4,4x,'| Warning: antenna not in the array')
1015 format(' Physical | Logical | Station |  X Offset  |  Y Offset  |  Z Offset /99')
1016 format(1x,i4,5x,'|'1x,i4,4x'|',3x,a,3x,'|',f11.6,1x,'|',f11.6,1x,'|',f11.6)
1017 format(1x,i4,5x,'| Undefined: no data read')
1018 format(1x,i4,5x,'| Warning: antenna not in the array')
!
end subroutine show_display
