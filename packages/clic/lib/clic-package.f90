!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the CLIC package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine clic_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Public
  !---------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack  !
  ! Global
  external :: ephem_pack_set
  external :: clic_pack_init
  ! external :: clic_pack_clean
  !
  pack%name='clic'
  pack%ext='.clic'
  pack%depend(1:2) = (/ locwrd(ephem_pack_set), locwrd(greg_pack_set) /)
  pack%init=locwrd(clic_pack_init)
  ! pack%clean=locwrd(clic_pack_clean)
  pack%authors="V.Pietu, A.Castro-Carrizo, R.Lucas"
  !
end subroutine clic_pack_set
!
subroutine clic_pack_init(gpack_id,error)
  use gkernel_interfaces
  use sic_def
  !---------------------------------------------------------------------
  ! Private
  !---------------------------------------------------------------------
  integer, intent(in) :: gpack_id   !
  logical, intent(inout) :: error   !
  ! Local
  character(len=80) :: comm
  !
  ! Ensure messages first since next calls produce messages
  call gmessage_log_date(.true.)
  call classic_message_set_id(gpack_id)
  call clic_message_set_id(gpack_id)  ! Set library id
  !
  ! Local language
  call init_clic
  call found_variable
  call mth_init               ! Init MATH package
  !
!!!!!!!!!!!!! Unneeded !!!!!!!!!!!!!!!!
!       CALL MESNAME(LOGFILE)
!       MESFILE = LOGFILE
!       NL = LENC(LOGFILE)
!       LOGFILE(NL-3:) = '.log'
!       MESFILE(NL-3:) = '.mes'
! Get rid of older versions
!       STATUS = SIC_PURGE (LOGFILE,2)
!       STATUS = SIC_PURGE (MESFILE,2)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!       CALL SIC_OPT(NAME,LOGFILE,.TRUE.)
!
! Set priorities:
#if defined(SDM)
      CALL MESSAGE_INIT('',2,2)
#else
      CALL MESSAGE_INIT('',1,0)
#endif
!
  comm = 'CLIC'//backslash//'SHOW'
  call sic_setsymbol('SH*OW',comm,error)
  comm = 'CLIC'//backslash//'FIND'
  call sic_setsymbol('FIN*D',comm,error)
!  comm = 'SIC'//backslash//'HELP CLIC'//backslash//' INFO'
!  call siC_setsymbol('INFO',comm,error)
  comm = 'CLIC'//backslash//'HEADER'
  call sic_setsymbol('HEAD*ER',comm,error)
  comm = 'CLIC'//backslash//'SET DEFAULT'
  call exec_program(comm)
  !
end subroutine clic_pack_init
!
! subroutine clic_pack_clean(error)
!   !----------------------------------------------------------------------
!   ! Private
!   !----------------------------------------------------------------------
!   logical, intent(inout) :: error
!   !
! end subroutine clic_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
