subroutine store_wvr(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  use clic_rdata
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: end, do_it
  integer(kind=address_length) :: kh, data_in, kin
  integer(kind=data_length)    :: ldata_in, h_offset
  !      INTEGER LDATA_IN,  OSCAN, SPLINE(MNANT), POL(MNANT),
  !     $LENC, NC1, IA, KR, H_OFFSET
  integer :: oscan, pol(mnant)
  integer :: nc1, ia, kr, ich
  real :: xx, yy
  character(len=80) :: ch2,ch1
  character(len=12) :: typecal(2)
  data typecal /'FPATH','TCAL'/
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  call get_first(.false.,error)    ! to initialise ...
  if (error) goto 999
  call check_output_file(error)
  if (error) goto 999
  call check_equal_file(error)
  if (error) goto 999
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  if (error) goto 999
  !
  ! Check for presence of calibration curve
  do_it = .false.
  ch1 = 'No WVR calibration for '
  nc1 = lenc(ch1)+1
  do ia = 1, r_nant
    ch2 = 'Antenna '//char(ia+ichar('0'))
    if (f_polwvr(ia).gt.0) then
      pol (ia) = f_polwvr(ia)
      do_it = .true.
      call message(2,1,'STORE_WVR',ch2(1:9)//' '   &
        //typecal(pol(ia)))
    else
      pol (ia) = 0
      call message(4,2,'STORE_WVR', ch1(1:nc1)//ch2)
    endif
  enddo
  if (.not.do_it) then
    call message (8,4,'STORE_WVR','No WVR curve to store')
    goto 999
  endif
  !
  ! Loop on current index
  end = .false.
  oscan = 0
  do while (.not.end)
    !
    ! Store calibration curves
    call get_data (ldata_in,data_in,error)
    if (error) goto 999
    !
    ! Average record only
    kin = gag_pointer(data_in,memory)
    call spectral_dump(kr,0,0)
    kh = kin + h_offset(kr)
    call decode_header (memory(kh))
    if (error) return
    xx = dh_utc/3600.   &
      + (mod(dh_obs+32768,65536)-mod(dobs_0+32768,65536))*24.
    !
    do ia = 1, r_nant
      if (pol(ia).eq.1) then
        call get_wvr(ia,1,xx,yy,oscan,error)
        if (error) return
        r_wvrfpath(1,ia) = 1./yy
      elseif (pol(ia).eq.2) then
        do ich=1, mwvrch
          call get_wvr(ia,ich,xx,yy,oscan,error)
          if (error) return
          r_wvrtcal(ich,ia) = yy
        enddo
      endif
    enddo
    !
    ! Update or copy scan
    call write_scan (.false.,error)
    if (error) goto 999
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) goto 999
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_wvr
!
subroutine get_wvr(ia,ich,x,y,oscan,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Returns in Y the value and derivatives of the calibration curve
  ! at time X,
  ! for baseline IB, side band ISB.
  !---------------------------------------------------------------------
  integer :: ia                     !
  integer :: ich                    !
  real :: x                         !
  real :: y                         !
  integer :: oscan                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: ispline, ipol
  real*8 :: xx, yy(4), aa(m_pol)
  integer :: ifail, nk, i
  character(len=80) :: ch
  !------------------------------------------------------------------------
  ! Code:
  xx = x
  error = .false.
  !
  ! Polynomials
  ifail = 1
  if (xx.lt.t_polwvr(1,ia)) then
    if (t_polwvr(1,ia)-xx .gt. 0.1) then
      if (r_scan.ne.oscan) then
        oscan = r_scan
        write(ch,'(a,1pg10.3,a,1pg10.3,a)')   &
          'Time ',xx,' below limit (',   &
          t_polwvr(1,ia),')'
        call message (5,2,'GET_WVR',ch(1:lenc(ch)))
      endif
    endif
    xx = t_polwvr(1,ia)
  elseif (xx.gt.t_polwvr(2,ia)) then
    if (xx-t_polwvr(2,ia).gt.0.1) then
      if (r_scan.ne.oscan) then
        oscan = r_scan
        write(ch,'(a,1pg10.3,a,1pg10.3,a)')   &
          'Time ',xx,' above limit (',   &
          t_polwvr(2,ia),')'
        call message (5,2,'GET_WVR',ch(1:lenc(ch)))
      endif
    endif
    xx = t_polwvr(2,ia)
  endif
  do i=1, n_polwvr(ia)
    aa(i) = c_polwvr(i,ia,ich)
  enddo
  xx = ((xx-t_polwvr(1,ia))-(t_polwvr(2,ia)-xx))   &
    /(t_polwvr(2,ia)-t_polwvr(1,ia))
  xx = max(xx,-1d0)
  xx = min(xx,1d0)
  call mth_getpol ('GET_WVR',n_polwvr(ia),aa,xx,yy,error)
  if (error) return
  y = yy(1)
end subroutine get_wvr
