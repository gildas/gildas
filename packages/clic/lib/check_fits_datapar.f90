subroutine check_fits_datapar(unit,ndata,addrecord, error)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Check the DATAPAR-ALMATI table
  ! Return the length  of words of the data section ...
  !---------------------------------------------------------------------
  integer :: unit                   !
  integer :: ndata                  !
  logical :: addrecord              !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: mf, nrows, tfields, varidat, status, ncorr, nauto, i
  integer :: nholo
  !
  ! 20 fields, 2 data tables per baseband
  integer :: mdatanum, colnum, nullval, r_npol
  integer :: ndatanum, nldump
  integer :: lc
  parameter (mf = 20,mdatanum=3, nullval=-1000)
  character(len=16) :: ttype(mf),tform(mf),tunit(mf), extname
  character(len=80) :: chain,comment
  logical :: anyf, datalog(mdatanum)
  integer :: not_found
  parameter (not_found=202)
  !------------------------------------------------------------------------------
  status = 0
  call ftghbn(unit,mf,nrows,tfields,ttype,tform,tunit,   &
    extname,varidat,status)
  !
  ! Header keywords: compute sixe of data area needed
  call ftgkyj(unit,'NO_CORR',ncorr,comment,status)
  if (status.eq.not_found) then
    ncorr = 0
  elseif (status.gt.0) then
    goto 99
  endif
  status = 0
  call ftgkyj(unit,'NO_AUTO',nauto,comment,status)
  if (status.eq.not_found) then
    nauto = 0
  elseif (status.gt.0) then
    goto 99
  endif
  status = 0
  call ftgkyj(unit,'NO_HOLO',nholo,comment,status)
  if (status.eq.not_found) then
    nholo = 0
  elseif (status.gt.0) then
    goto 99
  endif
  !
  r_nant = 1                   !!! ??? as a default
  call ftgkyj(unit,'NO_ANT',r_nant,comment,status)
  if (r_nant.gt.mnant) then
    call message(8,2,'CHECK_FITS_DATAPAR'   &
      ,'NO_ANT exceeds CLIC capacity')
    write(chain,'(a,i2,a,i2)') 'NO_ANT =',r_nant,' mnant ',mnant
    lc = lenc(chain)
    call message(6,2,'CHECK_FITS_DATAPAR',chain(1:lc))
    call message(8,2,'CHECK_FITS_DATAPAR',   &
      'Using first mnant antennas')
    r_nant = mnant
  endif
  r_nbas = max(r_nant*(r_nant-1)/2,1)
  do i=1, r_nbas
    r_iant(i) = antbas(1,i)
    r_jant(i) = antbas(2,i)
  enddo
  r_ldpar = 21 + (24+mwvrch)*r_nant + 11*r_nbas + 2*mrlband*mnant
  !c      R_LDPAR = 21 + 27*R_NANT + 11*R_NBAS
  call ftgkyj(unit,'NO_POL',r_npol,comment,status)
  call ftgkyj(unit,'NO_BAND',r_nband,comment,status)
  call ftgkyj(unit,'NO_CHAN',r_lntch,comment,status)
  call ftgkyj(unit,'NO_SIDE',r_nsb,comment,status)
  call ftgkyj(unit,'NO_PHCOR',r_ndatl,comment,status)
  r_lband = r_nband
  r_lnsb = r_nsb
  !
  if (ncorr.le.0 .and. nauto.gt.0) then
    if (nauto.gt.mdatanum) goto 98
    ndatanum = nauto
    r_lmode = 2
    r_ldatc = r_nband*r_nant*r_npol
    r_ldatl = r_lntch*r_nant*r_npol
    call ftgcno(unit,.false.,'AUTO',colnum,status)
    if (status.gt.0) goto 99
  elseif (ncorr.gt.0) then
    if (ncorr.gt.mdatanum) goto 98
    ndatanum = ncorr
    r_lmode = 1
    r_ldatc = 2*r_nband*r_nsb*r_nbas*r_npol
    r_ldatl = 2*r_lntch*r_lnsb*r_nbas*r_npol
    call ftgcno(unit,.false.,'CORR',colnum,status)
    if (status.gt.0) goto 99
  elseif (nholo.gt.0) then
    if (ncorr.gt.mdatanum) goto 98
    ndatanum = 1
    ! HOLO tables are treated as correlation in CLIC
    r_nant = 1
    r_nbas = 1
    r_lmode = 1
    ! forced to be DSB for compatibility ...
    r_nsb = 2
    r_lnsb = 2
    r_ldatc = 2*r_nband*r_nsb*r_nbas*r_npol
    r_ldatl = 2*r_lntch*r_lnsb*r_nbas*r_npol
    call ftgcno(unit,.false.,'HOLO',colnum,status)
    if (status.gt.0) goto 99
  else
    ndatanum = ncorr
    r_lmode = 1
    r_ldatc = 2*r_nband*r_nsb*r_nbas*r_npol
    r_ldatl = 2*r_lntch*r_lnsb*r_nbas*r_npol
    colnum = 0
  endif
  r_ldump = r_ldpar+r_ldatc
  !
  ndata = 0
  r_ndump = 0
  nldump = 0
  !
  do i=1, nrows
    if (colnum.ne.0) then
      call ftgcvl(unit,colnum,i,1,ndatanum,nullval,   &
        datalog,anyf,status)
      if (status.gt.0) then
        call fio_printerror('CHECK_FITS_DATAPAR','FTGCVL',   &
          status,error)
        return
      endif
    else
      ! patch
      datalog(1) = .true.
    endif
    ndata = ndata + r_ldpar
    if (datalog(1)) then
      r_ndump = r_ndump + 1
      ndata = ndata + r_ldatc
    elseif (datalog(2) .and. datalog(3)) then
      ndata = ndata + r_ldatc + r_ldatl
      if (r_ndatl.gt.1) then
        ndata = ndata + r_ldpar + r_ldatc + r_ldatl
      endif
      nldump = nldump + 1
    else
      call message(8,4,'CHECK_FITS_DATAPAR',   &
        'Unsupported data configuration')
    endif
  enddo
  !
  ! Force one average record for CLIC
  if (nldump.eq.0) then
    ndata = ndata + r_ldpar
    ndata = ndata + r_ldatc + r_ldatl
    if (r_ndatl.gt.1) then
      ndata = ndata + r_ldpar + r_ldatc + r_ldatl
    endif
    addrecord = .true.
  else
    addrecord = .false.
  endif
  return
  !
98 call message(8,4,'CHECK_FITS_DATAPAR',   &
    'Too many tables per baseband')
  error = .true.
  return
  !
99 call printerror('CHECK_FITS_DATAPAR',status)
  call message(6,2,'CHECK_FITS_DATAPAR',   &
    'Last comment was: '//comment)
  error = .true.
  return
end subroutine check_fits_datapar
