subroutine run_sdm(line,comm,error)
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! ASDM
  !	Main routine
  ! 	Call appropriate subroutine according to COMM
  ! Arguments :
  !	LINE	C*(*)	Command line		Input/Output
  !	COMM	C*(*)	Command name		Input
  !	ERROR	L	Logical error flag	Output
  ! (2003-05-07)
  !----------------------------------------------------------------------
  character*(*) line,comm
  logical error
  integer mvoc1
  character*60 mess
  !
  !
  call message(0,1,'SDM',line(1:lenc(line)))
  !
  !      IF (COMM.EQ.'READ') THEN
  !         CALL READ_SDM(LINE,ERROR)
  !      ELSEIF (COMM.EQ.'WRITE') THEN
  if (comm.eq.'WRITE') then
    call write_sdm(line,error)
  elseif (comm.eq.'READ') then
    call read_sdm(line,error)
  else
    mess = comm//' Not implemented'
    call message(6,1,'RUN_SDM',mess)
  endif
  return
end subroutine run_sdm
