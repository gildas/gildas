subroutine planet_flux(name,date,ut,fnu,flux,fbeam,size,error)
  !---------------------------------------------------------------------
  !	FLUX Command :
  !		character*(*) name 	input
  !		integer date 	        input	CLASS date (DH_OBS)
  !		real*8  ut 	        input	UT time (DH_UTC)
  !		real*8 	FNU		input
  !		real*8	flux		output
  !		REAL*8	SIZE(3)		output
  !		logical error 		in/output
  !---------------------------------------------------------------------
  character(len=12) :: name         !
  integer :: date                   !
  real*8 :: ut                      !
  real*8 :: fnu                     !
  real*8 :: flux                    !
  real*8 :: fbeam                   !
  real*8 :: size(3)                 !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real*8 :: utj
  logical :: found
  integer :: m_body, i_planet
  parameter (m_body = 9)
  character(len=12) :: body (m_body)
  real*8 :: au, helio_dist, distance, tbright, tmb, beam, asize
  real*8 :: lola(2), eq(2), ho(2), sunel, vr
  real*8 :: lola_e(2), eqoff(2), vroff(2), eqtopo(2)
  logical :: visible
  !
  data au/1.49598d8/
  data body/'MOON', 'SUN', 'MERCURY', 'VENUS',   &
    'MARS', 'JUPITER', 'SATURN', 'URANUS', 'NEPTUNE'/
  !------------------------------------------------------------------------
  ! Code :
  !
  found = .false.
  !
  ! Compute real time
  utj = date+2460549.5d0+ut/2d0/pi
  !
  ! new subroutine calls from astro
  call do_astro_time(utj, 0.0d0, 56.184d0, error)
  !
  ! Loop on possible objects
  beam = 41.0*115.0/fnu*pi/180./3600.
  do i_planet = 1, m_body
    if (name .eq. body(i_planet)) then
      found = .true.
      !            BEAM = 0.
      call do_astro_planet('CLIC',i_planet,.false.,lola,eq,eqtopo,   &
        ho,sunel,vr,   &
        distance,helio_dist,size,tbright,fnu,flux,   &
        visible,lola_e,eqoff,vroff,beam,tmb,fbeam,asize,error)
    endif
  enddo
! Note: the routine returns the total flux of the planet,
! not the single-dish flux. Correction for primary beam is
! done in APPARENT_FLUX.
end subroutine planet_flux
!
subroutine apparent_flux (fnu,flux,size,type,uvm)
  !---------------------------------------------------------------------
  ! The Fourier Transform of the elliptical disk is an Elliptical Bessel
  ! function of same position angle (Caution: positive towards East).
  ! Using the similarity theorem, an appropriate scaling of the U,V vector
  ! gives the answer:
  !     U1 =  U*COS(PA) - V*SIN(PA)
  !     V1 =  U*SIN(PA) + V*COS(PA)
  !
  !     U2 = U1
  !     V2 = V1*MAJOR/MINOR
  !
  !     UV = SQRT(U2^2+V2^2)/C*FNU     ! in wavelengths
  !
  ! For type = -1 (Disk)
  !     visi = minor/major * Bessel(2*pi*UV,MAJOR)
  ! For type # 0 (Gaussian)
  !     visi = minor/major * Gauss (2*pi*UV,MAJOR)
  !---------------------------------------------------------------------
  real*8 :: fnu                     ! Observing frequency
  real*8 :: flux                    ! Zero spacing flux
  real*8 :: size(3)                 ! Major, Minor, PA in radians
  integer :: type                   ! Type of source (1 Disk, 2 Gauss, etc...)
  real*4 :: uvm(2)                  ! U,V in meters
  ! Global
  real*8 :: mth_bessj1
  include 'gbl_pi.inc'
  ! Local
  integer :: ifail
  real*8 :: ca,sa,u2,v2,uv,ksi,j1,j2,j(2)
  real*8 :: c, eps, beam
  parameter (c=2.99792458d8)
  !
  ca = dcos(size(3))
  sa = dsin(size(3))
  u2 = uvm(1)*ca - uvm(2)*sa
  ! size (2) is the minor axis !!
  !      V2 = (UVM(1)*SA + UVM(2)*CA)*SIZE(1)/SIZE(2)
  v2 = (uvm(1)*sa + uvm(2)*ca)*size(2)/size(1)
  uv = dsqrt(u2**2+v2**2) * fnu * 1d9 / c  ! Frequency is in GHz
  !
  if (uv.eq.0) return
  !
  if (type.eq.-1) then
    !
    ! Disk with radius
    ksi = size(1) * pi * uv    ! Size is the diameter...
    ifail = 0
    j1 =  2.d0 * mth_bessj1(ksi) / ksi
    j2 = flux * j1 * size(2) / size(1)
  elseif (type.eq.-2) then
    !
    ! Disk with radius and first order correction for primary beam size
    ! using:
    ! (1-(r/a)^2)PI(r/2a)    ---Hankel--->  4*J2(2 pi aq) / pi q^2
    !
    ksi = size(1) * size(2) * (pi * uv) **2 / log(2.0)
    call mth_bessjn(ksi,j,2)
    beam = pi/180/3600*44d0*115d0/fnu
    eps = 1.d0-exp(-log(2.d0)*(size(1)/beam)**2)
    j1 = (1-eps)*2d0*j(1)/ksi + eps*4d0*j(2)/ksi**2
    j2 = flux * j1 * size(2) / size(1)
  !
  ! Gaussian with HWHM
  elseif (type.gt.0) then
    ksi = size(1) * size(2) * (pi * uv) **2 / log(2.0)
    j1 = exp(-ksi)
    j2 = flux * j1
  elseif (type.eq.-3) then
    ! Get spline interpolation
    uv = sqrt(uvm(1)**2+uvm(2)**2)
    call get_fluxspl(uv,j2)
  endif
  !
  !      IF (TYPE.NE.-1 .AND. TYPE.NE.0) THEN
  !         FLUX = J2
  !      ELSEIF (J2.EQ.0) THEN
  !         WRITE(6,100) FLUX,J1,KSI,J2
  !      ELSE
  !         FLUX = J2
  !      ENDIF
  !
  if (j2.eq.0) then
    write(6,100) flux,j1,ksi,j2
  else
    flux = j2
  endif
100 format('Zero FLUX ',0pf8.3,1x,1pg13.6,1x,1pg13.6,1x,0pf8.3)
end subroutine apparent_flux
!
function known_flux (name,le_flux)
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !     Check if a source has a known flux and structure.
  !---------------------------------------------------------------------
  integer :: known_flux             !
  character(len=*) :: name          !
  real*8 :: le_flux                 !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  ! Local
  integer :: npla,noth,i,nl
  parameter (npla=7,noth=2)
  character(len=12) :: known(npla+noth)
  real :: frtol
  parameter (frtol=1000.)
  !
  data known /'MERCURY', 'VENUS',   &
    'MARS', 'JUPITER', 'SATURN', 'URANUS', 'NEPTUNE',   &
    'W3OH', 'MWC349'/
  !
  nl = lenc(name)
  do i=1,npla
    if (name(1:nl).eq.known(i)) then
      known_flux = planet_model
      return
    endif
  enddo
  !
  ! Check if flux has been specified already in the flux list without
  ! frequency
  do i = 1, n_flux
    if (f_flux(i) .and. c_flux(i).eq.name(1:nl)   &
      .and. freq_flux(i).le.0) then
      le_flux = flux(i)
    endif
  enddo
  ! Check if flux has been specified already in the flux list
  ! with the RIGHT frequency
  do i = 1, n_flux
    if (f_flux(i) .and. c_flux(i).eq.name(1:nl)   &
      .and. abs(freq_flux(i)-r_restf).lt.frtol) then
      le_flux = flux(i)
    endif
  enddo
  !
  ! Check if flux already known
  do i=1,noth
    if (name(1:nl).eq.known(i+npla)) then
      known_flux = i
      return
    endif
  enddo
  known_flux = 0
end function known_flux
!
subroutine get_flux(fnu,isou,le_flux,size)
  real*8 :: fnu                     !
  integer :: isou                   !
  real*8 :: le_flux                 !
  real*8 :: size(3)                 !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  integer :: moth,lenc
  parameter (moth=2)
  real :: the_size(5,moth)
  character*150 :: chain
  character*8 :: name(moth)
  !
  ! Major, Minor, PA, Flux(100), Size Index (MWC349)
  data the_size /   &
    1.60, 0.90, 75.0, 3.7,   0.00,   &  ! W3OH flux: 3.7 0.15
    0.11, 0.11, 0.0,  1.20, -0.62    &  ! MWC349
    /
  data name /'W3OH','MWC349'/
  !
  if (isou.gt.moth) return
  ! Do not use it now
  !      LE_FLUX = THE_SIZE(4,ISOU)*(FNU/100.)**THE_SIZE(5,ISOU)
  size(1) = the_size(1,isou)*(fnu/100)**the_size(5,isou)*pi/180.0/3600.0
  size(2) = the_size(2,isou)*(fnu/100)**the_size(5,isou)*pi/180.0/3600.0
  size(3) = the_size(3,isou)*pi/180.0
  write (chain,'(a,f6.3,a,f5.1,a)') 'Using a size of ',&
    size(1)*180*3600/pi, ' arcsecond at ',fnu,' GHz for '//name(isou)
  call message(2,1,'GET_SIZE',chain(1:lenc(chain)))
end subroutine get_flux
!
subroutine set_fluxspl(fich,planet_model)
  use gkernel_interfaces
  character(len=*) :: fich          !
  integer :: planet_model           !
  ! Local
  real*8 :: uvm,flux
  !
  real :: splfk,uv
  !
  integer :: mspl,nspl
  parameter (mspl=64)
  real*4 :: uvo,fluo
  real*4 :: uspl(mspl),fspl(mspl)
  real*4 :: c2(mspl),c3(mspl),c4(mspl)
  common /planet_spline/ uspl,fspl,   &
    c2,c3,c4,uvo,fluo,nspl
  !
  logical :: error                ! not tracked further
  integer :: i,ier,lun
  !
  planet_model = -1
  ier = sic_getlun(lun)
  open(unit=lun,file=fich,status='OLD',iostat=ier)
  if (ier.ne.0) then
    close(unit=lun)
    call message(6,2,'SET PLANET','No such file')
    return
  endif
  !
  do i=1,mspl
    read(lun,*,iostat=ier) uspl(i),fspl(i)
    if (ier.ne.0) goto 10
    nspl = i
  enddo
10 close (unit=lun)
  call sic_frelun(lun)
  if (nspl.lt.3) return
  !
  ! Compute the spline coefficients
  call cubspl(nspl,uspl,fspl,   &
    c2,c3,c4,0,0,error)
  planet_model = -3
  uvo = 0
  return
  !
entry get_fluxspl(uvm,flux)
  !
  uv = uvm
  if (uv.ne.uvo) then
    fluo = splfk(nspl,uspl,fspl,c3,uv,i)
    !         print *,i,uv,fluo
    uvo = uv
  endif
  flux = fluo
end subroutine set_fluxspl
!
