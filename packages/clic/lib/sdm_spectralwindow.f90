subroutine write_sdm_spectralWindow(holodata, error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the spectralWindow SDM table.
  !
  !---------------------------------------------------------------------
  !      use gildas_def
  use sdm_SpectralWindow
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  type(SpectralWindowRow) :: swRow
  type(SpectralWindowKey) :: swKey
  type(SpectralWindowOpt) :: swOpt
  integer ia, is, ic, isb, jsb, i, j, bbcSideband, ln,   &
    AssSpectralWindowId, AssSpectralWindowIdDim, ier, netSB,   &
    sb_conversion(2) , eps
  real*8 :: fLSR, fConv, f1, f2, fmin, fmax, ai
  integer, parameter ::  minalloc=1
  logical :: present
  character csb(2)*3, AssNature*16, sdmTable*16, addname*4
  parameter (sdmTable='SpectralWindow')
  data csb/'USB','LSB'/
  data sb_conversion /1,-1/
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! Two associated natures in general.
  swRow%numChan = 1
  swOpt%numAssocValues = 2
  call allocSpectralWindowOpt(swRow, swOpt, error)
  if (error) return
  !
  !     save the Bure LO1 and LO2 frequencies at start so that the offsets
  !     (time variable parts) can be computed later.
  if (freq_LO1.eq.0) then
    freq_LO1 = r_flo1
  !cc         freq_LO2 = r_flO2
  endif
  !
  !     cnversion factor from topo. freq.  in MHz to LSR frequencies in Hz
  !c      fLSR = 1d6/(1+r_dopplr)
  !     conversion factor to Hz
  fConv = 1d6
  !     Reset the list of indentifiers...
  !      lSpectralWindowId = 0
  AssNature = 'ImageSideBand'
  if (.not. holodata) then
    !
    !     Continuum first (present in all congigurations).................
    !
    !     loop on spectral subbands (=basebands) let's go down, so that we
    !     get the lowest ranked name in case of duplicates (when two corr
    !     units are used for different polarizations).
    !c         do ic = 1, r_lband
    do ic = r_lband, 1, -1
      if (new_receivers) then
        freq_LO2 = r_flo2(ic)
      else
        freq_LO2 = r_flo1_ref
      endif
      !
      !     loop on side bands
      do isb = 1, r_lnsb
        !
        swRow%numchan = 1
        !     *TBD* what to put in refFreq?
        swRow%refFreq = 0
        eps = sb_conversion(isb)
        !     *TBD* is totBandwidth the max delta_nu? the noise equivalent total
        !     bandwidth?
        swRow%totBandwidth = 0
!!!         call allocSpectralWindowRow(swRow, swRow%numChan, error)
!!!         if (error) return

        if (new_receivers) then
          swRow%refFreq = r_restf - r_isb*(r_fif1) + eps*(r_flo2(ic) -   &
               (r_flo2bis(ic) +r_band2bis(ic)* r_cfcen(ic)))
        else
          swRow%refFreq = r_restf + eps*(r_flo1_ref   &
               - r_cfcen(ic))
        endif
        !! swRow%basebandName = BasebandName_BB_1+(ic-1)
        !! force this to NO_BB as otherwise we cannot group the polarizations
        !  the optional name    will contain the original correlatorr units.
        swRow%basebandName = BasebandName_NOBB
        swRow%refFreq =  swRow%refFreq *fconv
        swOpt%chanFreqStart = swRow%refFreq
        swOpt%chanFreqStep = r_cfwid(ic)*fconv
        swOpt%chanWidth = r_cfwid(ic)*fconv
        swOpt%effectiveBw = r_cfwid(ic)*fconv
        swOpt%resolution = r_cfwid(ic)*fconv
        swRow%totBandwidth = r_cfwid(ic)*fconv
        if (error) return
        !!do i=1, swRow%numchan
          !!swRow%chanFreq(i) = swRow%refFreq
          !!swRow%chanWidth(i) = r_cfwid(ic)*fconv
          !!swRow%effectiveBw(i) = r_cfwid(ic)*fconv
          !!swRow%resolution(i) = r_cfwid(ic)*fconv
        !!  swRow%totBandwidth = swRow%totBandwidth +   &
        !!    swRow%chanWidth(i)
        !!enddo
        !
        ! forget about 3rd conversion it is both U and L for wide bandwidth...
        netSB = (-1)**isb*(-1)
        if (netSB.gt.0) then
          swRow%netSideBand = NetSideBand_USB
        else
          swRow%netSideBand = NetSideBand_LSB
        endif
        ! *TBD* this may be the correlator unit number
        !! deleted !! swOpt%ifConvChain = r_iunit(ic)
        swOpt%freqGroup = ic
        write (swOpt%freqGroupName,'(a4,i2.2)') 'CorrUnit'   &
          ,r_iunit(ic)
        !!swRow%flagRow = .false.    ! *TBD* fill in from dh_aflag
        swRow%sideBandProcessingMode = SidebandProcessingMode_PHASE_SWITCH_SEPARATION
        swRow%windowFunction = WindowFunction_WELCH 
        !
        call addSpectralWindowRow(swKey,swRow,error)
        if (error) return
        spectralWindow_Id(isb,ic,1) = swKey%spectralWindowId
        !
        !! mandatory call addspectralWindowBasebandName(swKey, swOpt, error)
        !!  if (error) return
        ! Add the name or extend it
        write(addname,'(a2,i2.2)') '_C',ic
        swOpt%name =''
        call getSpectralWindowName(swKey, swOpt, present,   &
          error)
        if (error) return
        if (present) then
          ln = lenc(swOpt%name)
          if (index(swOpt%name(1:ln),addname).eq.0) then
            swOpt%name(ln+1:) = addname
          endif
        else
          swOpt%name = csb(isb)//addname
        endif
        !print *, 'call addSpectralWindowName '
        call addspectralWindowName(swKey, swOpt, error)
        if (error) return
        ! I assume 0 is REST, 1 is LSRK
        swOpt%measFreqRef = 1
        call addspectralWindowMeasFreqRef(swKey, swOpt, error)
        if (error) return
        ln = lenc(swOpt%name)
        call addspectralWindowchanFreqStart(swKey, swOpt, error)
        call addspectralWindowchanFreqStep(swKey, swOpt, error)
        call addspectralWindowChanWidth(swKey, swOpt, error)
        call addspectralWindowEffectiveBw(swKey, swOpt, error)
        call addspectralWindowResolution(swKey, swOpt, error)
        !print '(a12,i3,f14.9)', swOpt%name (1:ln), swKey%spectralWindowId, swRow%refFreq/1e9

      enddo
    !
    !     Add Associations
    !
    enddo
    !
    !     Then Line .....................................................
    !
    !     loop on spectral  subbands (=basebands)
    do ic = r_lband, 1, -1
      if (new_receivers) then
        freq_LO2 = r_flo2(ic)
      else
        freq_LO2 = r_flo1_ref
      endif
      !         do ic = 1, r_lband
      !
      !     loop on side bands
      do isb = 1, r_lnsb
        swRow%numchan = r_lnch(ic)
        write(swOpt%name,'(a1,i2.2,a3)') 'L',ic,csb(isb)
        eps = sb_conversion(isb)
        swRow%totBandwidth = 0
!!!         call allocSpectralWindowRow(swRow, swRow%numChan, error)
!!!         if (error) return
        !
        ! set this to Sky Frequency. The frequency will only be truly right for the reference channel.
        !
        ai = (r_lnch(ic)+1.)/2
        if (new_receivers) then
          swRow%refFreq = r_restf - r_isb*r_fif1  + eps*(r_flo2(ic) -   &
               (r_flo2bis(ic) +r_band2bis(ic)* (r_lfcen(ic)   &
               +(ai -r_lcench(ic))* r_lfres(ic))))
          
        else
          swRow%refFreq = r_restf - r_isb*r_fif1  + eps*(r_flo1_ref -   &
               (r_lfcen(ic) + (ai-r_lcench(ic))*r_lfres(ic)))
        endif
        swRow%refFreq =  swRow%refFreq *fconv
        swOpt%chanFreqStart = r_lrfoff(isb,ic) + (1.-r_lrch(isb,ic))*r_lrfres(isb,ic)
        if (new_receivers) then
          swOpt%chanFreqStart = r_restf - r_isb*r_fif1  + eps*(r_flo2(ic) -   &
               (r_flo2bis(ic) +r_band2bis(ic)* (r_lfcen(ic)   &
               +(1. -r_lcench(ic))* r_lfres(ic))))
        else
          swOpt%chanFreqStart = r_restf - r_isb*r_fif1  + eps*(r_flo1_ref -   &
               (r_lfcen(ic) + (1.-r_lcench(ic))*r_lfres(ic)))
        endif
        swOpt%chanFreqStart = swOpt%chanFreqStart * fConv
        swOpt%chanFreqStep = abs(r_lrfres(isb,ic)) * fConv
        swOpt%chanWidth = abs(r_lrfres(isb,ic)) * fConv
        swOpt%effectiveBw = abs(r_lrfres(isb,ic)) * fConv
        swOpt%resolution = abs(r_lrfres(isb,ic)) * fConv
        swRow%totBandwidth = swRow%numchan * swOpt%resolution
        !! swRow%basebandName = BasebandName_BB_1+(ic-1)
        !! force this to NO_BB as otherwise we cannot group the polarizations
        !  the optional name    will contain the original correlatorr units.
        swRow%basebandName = BasebandName_NOBB

!!        do i=1, swRow%numchan
!!!           swRow%chanFreq(i) = r_lrfoff(isb,ic) + (i-r_lrch(isb,ic))*r_lrfres(isb,ic)
!!!           if (new_receivers) then
!!!             swRow%chanFreq(i) = r_restf - r_isb*r_fif1  + eps*(r_flo2(ic) -   &
!!!                  (r_flo2bis(ic) +r_band2bis(ic)* (r_lfcen(ic)   &
!!!                  +(i -r_lcench(ic))* r_lfres(ic))))
!!!             
!!!           else
!!!             swRow%chanFreq(i) = r_restf - r_isb*r_fif1  + eps*(r_flo1_ref -   &
!!!                  (r_lfcen(ic) + (i-r_lcench(ic))*r_lfres(ic)))
!!!           endif
!!!           swRow%chanFreq(i) = swRow%chanFreq(i) * fconv
!!          swRow%chanWidth(i) = abs(r_lrfres(isb,ic)) * fConv
          !     *TBD* is effectiveBw the noise bandwidth?
!!          swRow%effectiveBw(i) = abs(r_lrfres(isb,ic)) * fConv
          !     *TBD* is resolution the half-power width ?
!!          swRow%resolution(i) = abs(r_lrfres(isb,ic)) * fConv
!!!           swRow%totBandwidth = swRow%totBandwidth   &
!!!             +swRow%resolution(i)
!!!         enddo
        !     forget about 3rd conversion it is both U and L for wide bandwidth...
        netSB = (-1)**isb*(-1)
        if (netSB.gt.0) then
          swRow%netSideBand = NetSideBand_USB
        else
          swRow%netSideBand = NetSideBand_LSB
        endif
        !     *TBD* this is may be the corr unit number.
        !!swOpt%ifConvChain = r_iunit(ic)
        swOpt%freqGroup = ic
        write (swOpt%freqGroupName,'(a4,i2.2)') 'CorrUnit'   &
          ,r_iunit(ic)
        swRow%sideBandProcessingMode = SidebandProcessingMode_PHASE_SWITCH_SEPARATION
        swRow%windowFunction = WindowFunction_WELCH 
        !!swRow%flagRow = .false.    ! *TBD* fill in from dh_aflag
        !
        !print *, 'call addspectralWindowRow 2'
        call addspectralWindowRow(swKey, swRow, error)
        if (error) return
        spectralWindow_Id(isb,ic,2) = swKey%spectralWindowId
        !
        !     Add the BasebandName
        !! swOpt%BasebandName = ic-1
!!!         call addspectralWindowBasebandName(swKey, swOpt, error)
!!!         if (error) return
        ! Add the name or extend it
        write(addname,'(a2,i2.2)') '_L',ic
        swOpt%name =' '
        !print *, 'call getSpectralWindowName 2'
        call getSpectralWindowName(swKey, swOpt, present,   &
          error)
        if (error) return
        if (present) then
          ln = lenc(swOpt%name)
          if (index(swOpt%name(1:ln),addname).eq.0) then
            swOpt%name(ln+1:) = addname
          endif
        else
          swOpt%name = csb(isb)//addname
        endif
        !print *, 'call addSpectralWindowName 2'
        call addspectralWindowName(swKey, swOpt, error)
        if (error) return
        ! I assume 0 is REST, 1 is LSRK
        swOpt%measFreqRef = 1
        !swOpt%measFreqRef = 0
        call addspectralWindowMeasFreqRef(swKey, swOpt, error)
        if (error) return
        ln = lenc(swOpt%name)
        call addspectralWindowchanFreqStart(swKey, swOpt, error)
        call addspectralWindowchanFreqStep(swKey, swOpt, error)
        call addspectralWindowChanWidth(swKey, swOpt, error)
        call addspectralWindowEffectiveBw(swKey, swOpt, error)
        call addspectralWindowResolution(swKey, swOpt, error)
        !print '(a12,i3,f14.9)', swOpt%name (1:ln), swKey%spectralWindowId, swRow%refFreq/1e9
      enddo
      !
      !     Add Associations
      !
      do isb = 1, r_lnsb
      enddo
    enddo
    !
    !     Now for the continuum detector: (Valid for the current 100-600 MHz
    !     detectors). One spw for USB and one for LSB, they are needed for the
    !     calibration (tsys, ...). The data description refers only to one,
    !     depending on the setup.
    !
    !     loop on side bands
    swRow%numchan = 1
!!!     call allocSpectralWindowRow(swRow, swRow%numChan, error)
!!!     if (error) return
    do isb = 1, r_lnsb
      eps = sb_conversion(isb)
      if (isb.eq.1) then
        swOpt%name = 'TotPowDetector(USB)'
      else
        swOpt%name = 'TotPowDetector(LSB)'
      endif
      !
      swRow%totBandwidth = 0d0
      swOpt%chanFreqStep = 0d0 
      if (new_receivers) then
        fmin = 1e20
        fmax = -1e20
        do ic=1, r_lband
          f1 = r_restf - r_isb*(r_fif1) + eps*(r_flo2(ic) - (r_flo2bis(ic) +   &
               r_band2bis(ic)* (r_lfcen(ic)+(1-r_lcench(ic))   &
               * r_lfres(ic))))
          f2 = r_restf - r_isb*(r_fif1) + eps*(r_flo2(ic) - (r_flo2bis(ic) +   &
               r_band2bis(ic)* (r_lfcen(ic)+(r_lnch(ic)   &
              -r_lcench(ic)) * r_lfres(ic))))
          fmin = min(f1,f2,fmin)
          fmax = max(f1,f2,fmax)
        enddo
        swOpt%chanFreqStart = (fmin+fmax)/2.*fconv
        swOpt%chanWidth = (fmax-fmin)*fconv
      else
        !     this was a hardware detector.
        swOpt%chanFreqStart = (r_restf - r_isb*(r_fif1) +eps*(freq_lo2-350.))   &
             *fconv
        swOpt%chanWidth = 500.d0*fConv
      endif
      swOpt%effectiveBw = swOpt%chanWidth
        !     *TBD* is resolution the half-power width ?
      swOpt%resolution = swOpt%chanWidth
      swRow%totBandwidth = swOpt%chanWidth
       


!!!       do i=1, swRow%numchan
!!!         !
!!!         !     This now comes from the correlator units:
!!!         if (new_receivers) then
!!!           fmin = 1e20
!!!           fmax = -1e20
!!!           do ic=1, r_lband
!!!             f1 = r_restf - r_isb*(r_fif1) + eps*(r_flo2(ic) - (r_flo2bis(ic) +   &
!!!               r_band2bis(ic)* (r_lfcen(ic)+(1-r_lcench(ic))   &
!!!               * r_lfres(ic))))
!!!             f2 = r_restf - r_isb*(r_fif1) + eps*(r_flo2(ic) - (r_flo2bis(ic) +   &
!!!               r_band2bis(ic)* (r_lfcen(ic)+(r_lnch(ic)   &
!!!               -r_lcench(ic)) * r_lfres(ic))))
!!!             fmin = min(f1,f2,fmin)
!!!             fmax = max(f1,f2,fmax)
!!!           enddo
!!!           swRow%chanFreq(i) = (fmin+fmax)/2.*fconv
!!!           swRow%chanWidth(i) = (fmax-fmin)*fconv
!!!         else
!!!           !     this was a hardware detector.
!!!           swRow%chanFreq(i) = (r_restf - r_isb*(r_fif1) +eps*(freq_lo2-350.))   &
!!!             *fconv
!!!           swRow%chanWidth(i) = 500.d0*fConv
!!!         endif
!!!         !     *TBD* is effectiveBw the noise bandwidth?
!!!         swRow%effectiveBw(i) = swRow%chanWidth(i)
!!!         !     *TBD* is resolution the half-power width ?
!!!         swRow%resolution(i) = swRow%chanWidth(i)
!!!         swRow%totBandwidth = swRow%totBandwidth+swRow%chanWidth(i   &
!!!           )
!!!       enddo
      swRow%refFreq = swOpt%chanFreqStart
      swRow%basebandName = BasebandName_NOBB
      !
      netSB = (-1)**isb*(-1)
      if (netSB.gt.0) then
        swRow%netSideBand = NetSideBand_USB
      else
        swRow%netSideBand = NetSideBand_LSB
      endif
      !!swOpt%ifConvChain = 0
      swOpt%freqGroup = ic
      swOpt%freqGroupName =  'TotalPow.'
      swRow%BasebandName = BasebandName_BB_1
      swRow%sideBandProcessingMode = SidebandProcessingMode_NONE
      swRow%windowFunction = WindowFunction_WELCH 
      !!swRow%flagRow = .false.  ! *TBD* fill in from dh_aflag
      !print *, 'call addspectralWindowRow 3'
      call addspectralWindowRow(swKey, swRow, error)
      if (error) return
      TotPowSpectralWindow_Id(isb) = swKey%spectralWindowId
      !
      !     Add the BasebandName
      call addspectralWindowName(swKey, swOpt, error)
      if (error) return
      ! I assume 0 is REST, 1 is LSRK
      swOpt%measFreqRef = 1
      !swOpt%measFreqRef = 0
      call addspectralWindowMeasFreqRef(swKey, swOpt, error)
      if (error) return
      call addspectralWindowchanFreqStart(swKey, swOpt, error)
      call addspectralWindowchanFreqStep(swKey, swOpt, error)
      call addspectralWindowChanWidth(swKey, swOpt, error)
      call addspectralWindowEffectiveBw(swKey, swOpt, error)
      call addspectralWindowResolution(swKey, swOpt, error)
      if (error) return
      ln = lenc(swOpt%name)
      !! print '(a12,i3,f14.9)',swOpt%name (1:ln), swKey%spectralWindowId, swRow%refFreq/1e9
      call addspectralWindowfreqGroup(swKey, swOpt, error)
      call addspectralWindowfreqGroupName(swKey, swOpt, error)
      if (error) return


    enddo
    !
    !     Add Associations
    !
    !
    !     loop on side bands
    do isb = 1, r_lnsb
      jsb = 3-isb
      !     loop on spectral  subbands (=basebands)
      do ic = 1, r_lband
        ! channel average:
        swKey%SpectralWindowId = spectralWindow_Id(isb,ic,1)
        !     The associated SpectralWindowIds are the full resolution spectral and total power ones:
        swOpt%AssocSpectralWindowId(1) = spectralWindow_Id(isb,ic   &
          ,2)
        swOpt%AssocSpectralWindowId(2)   &
          = TotPowSpectralWindow_Id(isb)
        !     And the associated natures:
        swOpt%AssocNature(1) =   &
           SpectralResolutionType_FULL_RESOLUTION
        swOpt%AssocNature(2) =   &
           SpectralResolutionType_BASEBAND_WIDE
        call addSpectralWindowNumAssocValues(swKey, swOpt, error)
        call addSpectralWindowAssocSpectralWindowId(swKey, swOpt, error)
        call addSpectralWindowAssocNature(swKey, swOpt, error)
        !     then the image side band
        swOpt%imageSpectralWindowId = spectralWindow_Id(jsb,ic,1)
        call addSpectralWindowImageSpectralWindowId(swKey, swOpt,   &
          error)
        if (error) return
        !     now the full resolution
        swKey%spectralWindowId = spectralWindow_Id(isb,ic,2)
        !     The associated SpectralWindowIds are the channel average  and total power ones:
        swOpt%AssocSpectralWindowId(1) = spectralWindow_Id(isb,ic   &
          ,1)
        swOpt%AssocSpectralWindowId(2)   &
          = TotPowSpectralWindow_Id(isb)
        !     And the associated natures:
! *** commented for the time being (error in the model issue)
         swOpt%AssocNature(1) =   &
           SpectralResolutionType_CHANNEL_AVERAGE
         swOpt%AssocNature(2) =   &
           SpectralResolutionType_BASEBAND_WIDE
! *** commented for the time being (error in the model issue)
        !
         !print *, 'call addSpectralWindowAssocSpectralWindowId 2'
        call addSpectralWindowNumAssocValues(swKey, swOpt, error)
        call addSpectralWindowAssocSpectralWindowId(swKey, swOpt,   &
          error)
        call addSpectralWindowAssocNature(swKey, swOpt, error)
        swOpt%imageSpectralWindowId = spectralWindow_Id(jsb,ic,2)
        call addSpectralWindowImageSpectralWindowId(swKey, swOpt,   &
          error)
        if (error) return
      enddo
      !     finally the total power (baseband wide)
      !     At this point there is a single total power channel at Bure. I do
      !     not associate all correlator subbands to it. Somee day we may use
      !     the total power per subband (with NGR).
      swKey%SpectralWindowId = TotPowSpectralWindow_Id(isb)
      swOpt%imageSpectralWindowId = TotPowSpectralWindow_Id(jsb)
      call addSpectralWindowImageSpectralWindowId(swKey, swOpt,   &
        error)
      if (error) return
    enddo
  !
  !
  ! Now the special case of holography. A single spectral window.
  else
    swRow%basebandName = BasebandName_NOBB
    swRow%netSideBand = NetSideBand_USB
    swRow%numchan = 1
    swRow%refFreq = (freq_lo1+(freq_lo2-r_cfcen(1)))   &
      *fconv
    !     *TBD* is totBandwidth the max delta_nu? the noise equivalent total
    !     bandwidth?
    swRow%sidebandProcessingMode = SidebandProcessingMode_NONE
!!!     call allocSpectralWindowRow(swRow, swRow%numChan, error)
!!!     if (error) return
    !!swRow%chanFreq(1) = (freq_lo1+(freq_lo2-r_cfcen(1)))   &
    !!  *fconv
    !     *TBD* is effectiveBw the noise bandwidth?
    !     *TBD* is resolution the half-power width ?
    swRow%totBandwidth = r_cfwid(1)*fconv
    swRow%windowFunction = WindowFunction_UNIFORM
    !     *TBD* this is may be the correlator unit number
    !!swOpt%ifConvChain = r_iunit(1)
    swOpt%freqGroup = 1
    write (swOpt%freqGroupName,'(a4,i2.2)') 'Holo'   &
      ,r_iunit(1)
    call addSpectralWindowRow(swKey, swRow, error)
    if (error) return
    TotPowspectralWindow_Id(1) = swKey%spectralWindowId
    swOpt%name = 'Holography'
    swOpt%chanFreqStart = (freq_lo1+(freq_lo2-r_cfcen(1))) *fconv
    swOpt%chanWidth = r_cfwid(1)*fconv
    swOpt%effectiveBw = r_cfwid(1)*fconv
    swOpt%resolution = r_cfwid(1)*fconv
    call addspectralWindowName(swKey, swOpt, error)
    call addspectralWindowchanFreqStart(swKey, swOpt, error)
    call addspectralWindowchanFreqStep(swKey, swOpt, error)
    call addspectralWindowChanWidth(swKey, swOpt, error)
    call addspectralWindowEffectiveBw(swKey, swOpt, error)
    call addspectralWindowResolution(swKey, swOpt, error)
    if (error) return
  endif
  !
  call sdmMessage(1,1,sdmTable ,'Written.')
  !y
  return
end subroutine write_sdm_spectralWindow
!---------------------------------------------------------------------
subroutine get_sdm_spectralWindow(holodata, error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Read the spectralWindow SDM table.
  !
  !---------------------------------------------------------------------
  !      use gildas_def
  use sdm_SpectralWindow
  use sdm_Receiver
  use sdm_FreqOffset
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  !      include 'sdm_types.inc'
  ! Dummy
  logical :: error, holodata
  ! Local
  type(SpectralWindowRow) :: swRow
  type(SpectralWindowKey) :: swKey
  type(SpectralWindowOpt) :: swOpt
  type(ReceiverRow) :: rRow
  type(ReceiverKey) :: rKey
  type(FreqOffsetRow) :: foRow
  type(FreqOffsetKey) :: foKey
  integer ia, is, numchan, ic, isb, jsb, i, j,   &
    AssSpectralWindowId , ilc, ier, fosize, convSign, ivc, ipp
  real*8 fLSR, fConv, fref, fres
  logical present
  character csb(2)*3, AssNature*16, sdmTable*16
  parameter (sdmTable='SpectralWindow')
  data csb/'USB','LSB'/
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call getFreqOffsetTableSize(foSize, error)
  if (error) return
  !
  ! read all Ids in the list.
  !
  !     conversion factor to Hz
  fConv = 1d6
  swRow%numChan = mch
  swOpt%numAssocValues = 1
  call allocSpectralWindowOpt(swRow, swOpt, error)
  if (error) return
  rRow%numLo = 4
  call allocReceiverRow(rRow, error)
  if (error) return

  !
  ! NO we only handle one pol product per spectral window in Bure format for the time being.
  ! the data here will concern num_corr (1 or 2)  bure subbands v_bands(ivc, 1) and v_bands(ivc,2).
  

  !
  do i=1, ddId_Size
    swKey%SpectralWindowId = swId_List(i)
    call getSpectralWindowRow(swKey, swRow, error)
    if (error) goto 99
    !
    ! now look in receiver to get the lo setups:
    !
    do ia = 1, r_nant
      rKey%receiverId = receiverId_List(ia, i)
      if (rKey%receiverId.ge.0) then
        rKey%spectralWindowId = swId_List(i)
        rKey%timeInterval = ArrayTimeInterval(time_Interval(1)   &
          ,time_Interval(2))
        call getReceiverRow(rKey, rRow, error)
        if (error) then
          print *,  'rkey is :', rkey
          goto 99
        endif
        if (foSize.gt.0) then
          foKey%antennaId = antenna_Id(ia)
          foKey%feedId = feed_Id
          foKey%spectralWindowId = swId_List(i)
          foKey%timeInterval = ArrayTimeInterval(time_Interval(1   &
            ),time_Interval(2))
          call getFreqOffsetRow(foKey, foRow, error)
          if (error) then
            print *, 'foKey ', foKey
            goto 99
          endif
        endif
      else
        !
        ! patch in some default values ...
        rRow%sidebandLo(1) = NetSideBand_LSB
        rRow%sidebandLo(2) = NetSideBand_LSB
        rRow%sidebandLo(3) = NetSideBand_LSB
        rRow%sidebandLo(4) = 0
        rRow%freqLo(1) = 100e9
        rRow%freqLo(2) = 10e9
        rRow%freqLo(3) = 4e9
        rRow%freqLo(4) = 0
      endif
    enddo
    !
    ! Assume the baseband number is coded in swOpt%BasebandName :
    if (.not.holodata) then
      bb_list(i) = swRow%BasebandName-BasebandName_BB_1+1
      ivc = bb_list(i)
    else
      ! more patches for holography as there is no polarization table:
      ! we handle a single polarization, but this is not setup from feed now.
      r_npol_rec = 1
      r_lband = 1
      r_nband = r_lband
      r_lpolmode = 1 
      ivc = 1
      bb_list(1) = 1
      num_corr = 1
      v_bands(1,1) = 1
      do ia=1, r_nant
        r_lpolentry(ia,1) = 1
      enddo
    endif

    do ipp = 1, num_corr
      ic = v_bands(ivc, ipp)
      ! r_lband corr units are in almacorrelatorMode.numBaseBand
      if (rRow%sidebandLo(4).eq.NetSideBand_USB) then
        r_band4(ic) = 0
      elseif (rRow%sidebandLo(4).eq.NetSideBand_LSB) then
        r_band4(ic) = 1
        !         elseif (rRow%sidebandLo(4).eq.0) then
        !            r_band4(ic) = 2
        ! set to 0 by default to avoid Gibbs effects in CLIC.
      elseif (rRow%sidebandLo(4).eq.0) then
        r_band4(ic) = 0
      endif
      if (rRow%sidebandLo(1) .eq. NetSideBand_USB) then
        isb = 1
      else
        isb = 2
      endif
      sb_list(i) = isb
      if (rRow%numLO.ge.2) then
        r_flo2(ic) = rRow%freqLo(2)/fconv
      else
        r_flo2(ic) = 0.
      endif
      if (rRow%numLO.ge.3) then
        r_flo3(ic) = rRow%freqLo(3)/fconv
      else
        r_flo3(ic) = 0.
      endif
      if (rRow%numLO.ge.4) then
        r_flo4(ic) = rRow%freqLo(4)/fconv
      else
        r_flo4(ic) = 0.
      endif
      if (rRow%numLO.ge.1) then
        r_flo1 = rRow%freqLo(1)/fconv
      else
        r_flo1 = swRow%refFreq/fconv
      endif
      !
      !     the actual IF2 frequencies r_lfcen(ic), r_cfcen(ic) are only read
      !     when the LO are read in from the receiver table...
      !
      !     channel averages
      if (swRow%numchan.eq.1) then
        !
        !  HOLO PATCH
        !
        call getSpectralWindowChanWidthArray(swKey, swOpt, present, error)
        if (present) then
          r_cfwid(ic) = swOpt%chanWidthArray(1)/fconv
        else
          call getSpectralWindowChanWidth(swKey, swOpt, present, error)
          r_cfwid(ic) = swOpt%chanWidth/fconv
        endif
        if (holodata) then
          r_cfwid(ic) = 10. ! MHz
        endif
        call getSpectralWindowChanFreqArray(swKey, swOpt, present, error)
        if (present) then
          fref = swOpt%chanFreqArray(1) + foRow%offset
        else
          call getSpectralWindowChanFreqStart(swKey, swOpt, present, error)
          fref = swOpt%chanFreqStart + foRow%offset
        endif
        if (holodata) then
          swOpt%chanFreqStart = swRow%refFreq
        endif
        r_lfcen(ic) = convSign(rRow%sidebandLo(2))*(convSign(rRow%sidebandLo(1))*(fref   &
             -rRow%freqLo(1))- rRow%freqLo(2)) / fconv
        cont_list(i) = .true.
        !
        !     spectra
      elseif (swRow%numchan.gt.1) then
        call getSpectralWindowChanWidth(swKey, swOpt, present, error)
        call getSpectralWindowChanFreqStart(swKey, swOpt, present, error)
        call getSpectralWindowChanFreqStep(swKey, swOpt, present, error)
        !  HOLO PATCH
        if (holodata) then
          swOpt%chanWidth = 10e6
          swOpt%chanFreqStart = swRow%refFreq
        endif
        !
        r_lnch(ic) = swRow%numchan
        r_lcench(ic) = (swRow%numchan+1.)/2.
!!!       fres = (swRow%chanFreq(swRow%numchan)-swRow%chanFreq(1))   &
!!!         /(swRow%numchan-1)
        fres = swOpt%chanFreqStep
        !!      fref = swRow%chanFreq(1) - (1-r_lcench(ic))*fres
        fref = swOpt%chanFreqStart - (1-r_lcench(ic))*fres
        fref = fref + foRow%offset
        r_lfcen(ic) = ((fref-rRow%freqLo(1))*convSign(rRow%sidebandLo(1))   &
             -rRow%freqLo(2))*convSign(rRow%sidebandLo(2)) / fconv
        !!r_lfres(ic) = fres*convSign(rRow%sidebandLo(1))*convSign(rRow%sidebandLo(2)) /   &
        !!     fconv
        r_lfres(ic) = fres /   fconv
        r_band2bis(ic) = convSign(rRow%sidebandLo(2))
        cont_list(i) = .false.
      endif
      ! the sideband dependent paremeters remain to be filled in...
      !
      swKey%SpectralWindowId = swId_List(i)
      call getSpectralWindowDopplerId(swKey, swOpt, present, error)
      if (error) goto 99
      if (present) Doppler_Id = swOpt%DopplerId
    enddo
  enddo
  !
  ! count all channels
  r_lntch = 0
  do ic = 1, r_lband
    r_lich(ic) = r_lntch
    r_lntch = r_lntch + r_lnch(ic)
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 continue
end subroutine get_sdm_spectralWindow
!---------------------------------------------------------------------
function convSign(sideBand)
  use sdm_Enumerations
  integer :: convSign
  integer :: sideBand
  if (sideBand .eq. NetSideBand_USB) then
    convSign = +1
  elseif  (sideBand .eq. NetSideBand_LSB) then
    convSign = -1
  endif
  
end function convSign
!---------------------------------------------------------------------
subroutine sdmMessageI(ip,is,pgm,mess,i)
  integer ip, is, i
  character*(*) pgm,mess
  character*13 ci
  write(ci,'(1x,i8)') i
  call message(ip,is,'SDM Table '//pgm,mess//ci)
end subroutine sdmMessageI
!---------------------------------------------------------------------
subroutine sdmMessage(ip,is,pgm,mess)
  integer ip, is
  character*(*) pgm,mess
  call message(ip,is,'SDM Table '//pgm,mess)
end subroutine sdmMessage
