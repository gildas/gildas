subroutine decode_header (iw)
  use gkernel_interfaces
  use phys_const
  use classic_api  
  use clic_file
  use clic_title
  integer :: iw(*)                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_proc_par.inc'
  ! Local
  integer :: k,j, l,m,natfac,temp_dim,ia,ib,ii,mask, ibit
  integer :: saflag(mnant),sbflag(mnbas),dummy
  character(len=12) :: ch
  logical :: error
  real :: tp1(mnant),dellin(m_pol_rec,mnant,2),delay(m_pol_rec,mnant)
  logical, save :: warned = .false.
  !------------------------------------------------------------------------
  ! CODE:
  !      print *, '========================decode header ! r_kind ', r_kind
  call r4tor4 (iw,dh_dump,3)
  k = 4
  !
  !      call r8tor8 (iw(k),dh_utc,4)
  ! Use an R4TOR4 call to avoid mis-aligned problems
  call r4tor4 (iw(k),dh_utc,8)
  k = k + 8
  if (de%version.le.2) then
    call r4tor4 (iw(k),dh_test0,10)
    k = k + 10
  else
    call r4tor4 (iw(k),dh_test0,20)
    k = k + 20
  endif
  call r4tor4 (iw(k),dh_aflag,r_nant)
  k = k + r_nant
  !     RL: first ngrx data had r_npol_rec=2, but only 1 totalpower.
  if (de%version.le.3) then
    if (new_receivers) then
      if (r_dobs.lt.(-6406)) then    ! changed the 13/02/2007
        temp_dim = r_npol_rec + r_nband_widex
      else
        temp_dim = 2 + r_nband_widex
      endif
      do ia=1, r_nant
        call r4tor4 (iw(k),dh_total(1,ia),temp_dim)
        k = k + temp_dim
      enddo
    else
      do ia=1, r_nant
        call r4tor4 (iw(k),dh_total(1,ia),1)
        call r4tor4 (iw(k),dh_total(2,ia),1)
        k = k + 1
      enddo
    endif
  else
    do ia=1, r_nant
      call r4tor4 (iw(k),dh_total(1,ia),r_nbb)
      k = k + r_nbb 
    enddo
  endif
  !      print *, 'crall : dh_total ',dh_total
  !
  !     RL: first ngrx data had only 1 dh_atfac, with no sub-band
  !     dependence; make sure this work with the subbands.
  if (de%version.le.3) then
    if (new_receivers) then
      natfac = max(r_npol_rec, r_lband)
    else
      natfac = 1
    endif
    do ia=1, r_nant
      do j=1, 2
        call r4tor4 (iw(k),dh_atfac(1,j,ia),natfac)
        ! this is needed for old data (RL 2007-03-19)
        if (natfac.eq.1 .and. r_lband.gt.1) then
          do l=2, r_lband
            dh_atfac(l,j,ia) = dh_atfac(1,j,ia)
          enddo
        endif
        k = k + natfac
      enddo
    enddo
  else
    do ia=1, r_nant
      do j=1, 2
        call r4tor4(iw(k),dh_atfac(1,j,ia),r_nbb)
        k = k + r_nbb
      enddo
    enddo
  endif
  call r4tor4 (iw(k),dh_rmspe,2*r_nant)
  k = k + 2*r_nant
  if (de%version.le.2) then
    call r4tor4 (iw(k),dh_delcon,r_nant)
    k = k + r_nant
  endif
  !     RL: first ngrx data had r_npol_rec=2, but only 1 dh_dellin.
  if (de%version.le.3) then
    !
    ! Was coded as (m_pol_rec,mnant,2)
    if (r_nband_widex.ne.0) then
      temp_dim = 2
    else
      temp_dim = 1
    endif
    do ia=1, r_nant
      do j = 1, temp_dim
         call r4tor4 (iw(k),dellin(1,ia,j),r_npol_rec)
         k = k + r_npol_rec
      enddo
    enddo
    ! 
    ! Convert it in v4
    do ia=1, r_nant
      do j=1, r_nbb
        ii = r_mapbb(j)
        l  = r_mappol(ia,j)
        !
        ! BB 1 and 2 connected to first correlator, 3 to 6 to second correlator
        if (ii.le.2) then        
          dh_dellin(ii,ia) = dellin(l,ia,1)
        else
          dh_dellin(ii,ia) = dellin(l,ia,2)
        endif
      enddo
    enddo    
  else 
    do ia=1, r_nant
      call r4tor4(iw(k),dh_dellin(1,ia),r_nbb)
      k = k + r_nbb
    enddo
  endif
  call r4tor4 (iw(k),dh_delayc,r_nant)
  k = k + r_nant
  !     RL: first ngrx data had r_npol_rec=2, but only 1 dh_delay.
  if (de%version.le.3) then
    if (new_receivers) then
      do ia=1, r_nant
        call r4tor4 (iw(k),delay(1,ia),r_npol_rec)
        k = k + r_npol_rec
      enddo
    else
      do ia=1, r_nant
        call r4tor4 (iw(k),delay(1,ia),1)
        call r4tor4 (iw(k),delay(2,ia),1)
        k = k + 1
      enddo
    endif
    ! 
    ! Convert it in v4
    do ia=1, r_nant
      do j=1, r_nbb
        ii = r_mapbb(j)
        l  = r_mappol(ia,j)
        dh_delay(ii,ia) = delay(l,ia)
      enddo
    enddo 
  else
    do ia=1, r_nant
      call r4tor4(iw(k),dh_delay(1,ia),r_nbb)
      k = k + r_nbb
    enddo
  endif
  call r4tor4 (iw(k),dh_phasec,r_nant)
  k = k + r_nant
  call r4tor4 (iw(k),dh_phase,r_nant)
  k = k + r_nant
  call r4tor4 (iw(k),dh_ratec,r_nant)
  k = k + r_nant
  call r4tor4 (iw(k),dh_cable,r_nant)
  k = k + r_nant
  if (de%version.le.2) then
    call r4tor4 (iw(k),dh_gamme,r_nant)
    k = k + r_nant
  endif
  if (de%version.le.2) then
    do ia=1, r_nant
      call r4tor4 (iw(k),dh_test1(1,ia),5)
      k = k + 5
    enddo
  else
    call r4tor4 (iw(k),dh_test1,20*r_nant)
    k = k + 20*r_nant
  endif
  ! RL: first ngrx data had r_npol_rec=2, but only 1 dh_rmsamp/pha.
  if (new_receivers) then
    do ia=1, r_nbas
      do j=1, 2
        call r4tor4 (iw(k),dh_rmsamp(1,j,ia),r_npol_rec)
        k = k + r_npol_rec
      enddo
    enddo
    do ib=1, r_nbas
      do j=1, 2
        call r4tor4 (iw(k),dh_rmspha(1,j,ib),r_npol_rec)
        k = k + r_npol_rec
      enddo
    enddo
  else
    call r4tor4 (iw(k),dh_rmsamp,2*r_nbas)
    k = k + 2*r_nbas
    call r4tor4 (iw(k),dh_rmspha,2*r_nbas)
    k = k + 2*r_nbas
  endif
  call r4tor4 (iw(k),dh_offfoc,r_nant)
  k = k + r_nant
  call r4tor4 (iw(k),dh_offlam,r_nant)
  k = k + r_nant
  call r4tor4 (iw(k),dh_offbet,r_nant)
  k = k + r_nant
  !
  ! Patch for mosaics observed:
  ! - between 05-may-2019 and 21-may-2019
  ! - between 18-sep-2020 and 04-oct-2020
  ! - between 03-may-2021 and 30-jun-2021
  ! - on 03-feb-2022
  ! - between 05-aug-2022 and 21-aug-2022
  ! - between 16-oct-2023 and 24-oct-2023
  if (((r_proc.eq.p_source).and.                       &
       (title%off1.ne.0).or.(title%off2.ne.0))  .and. &
      (((r_dobs.ge.-1941).and.(r_dobs.le.-1925)).or.  & 
       ((r_dobs.ge.-1439).and.(r_dobs.le.-1423)).or.  & 
       ((r_dobs.gt.-1212).and.(r_dobs.lt.-1154)).or.  & 
       ((r_dobs.eq.-936))                       .or.  & 
       ((r_dobs.ge.-753).and.(r_dobs.le.-737))  .or.  &
       ((r_dobs.ge.-316).and.(r_dobs.le.-309)))) then   
     if (.not.warned) then
       call message(8,1,'RRECORD',&
         'Patching mosaic data for a known acquisition problem')
       warned = .true.
     endif
     do ia=1,r_nant
       dh_offlam(ia) = title%off1*sec_per_rad
       dh_offbet(ia) = title%off2*sec_per_rad
     enddo
  endif
  call r4tor4 (iw(k),dh_bflag,r_nbas)
  k = k + r_nbas
  call r4tor4 (iw(k),dh_uvm,2*r_nbas)
  k = k + 2*r_nbas
  if (de%version.le.2) then
    call r4tor4 (iw(k),dh_infac,4*r_nbas)
    k = k + 4*r_nbas
  endif
  call r4tor4 (iw(k),dh_wvr,mwvrch*r_nant)
  k = k + mwvrch*r_nant
  call r4tor4 (iw(k),dh_wvrstat,r_nant)
  k = k + r_nant
  if (new_receivers) then
    if (de%version.le.3) then
      do ii = 1, r_lband
        do j = 1, r_nant
          call r4tor4 (iw(k),dh_actp(1,j,ii),r_npol_rec)
          k = k + r_npol_rec
        enddo
      enddo
    endif
    do j=1, r_nant
      call r4tor4 (iw(k),dh_anttp(1,j),r_nif)
      k = k + r_nif
    enddo
  endif
  if (de%version.le.3) then
    if (r_nband_widex.ne.0) then
        call w4tow4 (iw(k),saflag,r_nant)
        k = k + r_nant
        call w4tow4 (iw(k),sbflag,r_nbas)
        k = k + r_nbas
    else
        ! Copy spectral flags to dh_saflag and clear corresponding bits
        do ia = 1, r_nant
           saflag(ia) = 0
           do ii=1, 10
              mask = 0 
              mask = iand(dh_aflag(ia),ior(mask,ishft(1,ii-1)))
              if (mask.ne.0) then
                saflag(ia) = ibset(saflag(ia),ii-1) 
                dh_aflag(ia) = ibclr(dh_aflag(ia),ii-1)
              endif
           enddo
           do ii=1,8
              mask = 0
              mask = iand(dh_aflag(ia),ior(mask,ishft(1,ii+9)))
              if (mask.ne.0) then
                saflag(ia) = ibset(saflag(ia),ii+mbands-1) 
                dh_aflag(ia) = ibclr(dh_aflag(ia),ii+9) 
              endif
           enddo
        enddo
        do ib = 1, r_nbas
           sbflag(ib) = 0
           do ii=1, 10
              mask = 0
              mask = iand(dh_bflag(ib),ior(mask,ishft(1,ii-1)))
              if (mask.ne.0) then
                sbflag(ib) = ibset(sbflag(ib),ii-1)
                dh_bflag(ib) = ibclr(dh_bflag(ib),ii-1)
              endif
           enddo
           do ii=1,8
              mask = 0
              mask = iand(dh_bflag(ib),ior(mask,ishft(1,ii+9)))
              if (mask.ne.0) then
                sbflag(ib) = ibset(sbflag(ib),ii+mbands-1)
                dh_bflag(ib) = ibclr(dh_bflag(ib),ii+9)
              endif
           enddo
        enddo
    endif
    !
    ! With version le 3, spectral flags on one word
    dh_saflag = .false.
    do ia = 1, r_nant
      is_saflag(ia) =.false.
      do ii = 1, 16
         if (btest(saflag(ia),ii-1)) then
           dh_saflag(ii,ia) = .true.
           is_saflag(ia) = .true.
         endif
         if (btest(saflag(ia),ii+15)) then
           dh_saflag(ii+mbands,ia) = .true.
           is_saflag(ia) = .true.
         endif
      enddo
    enddo
    dh_sbflag = .false.
    do ib = 1, r_nbas
      is_sbflag(ib) =.false.
      do ii = 1, 16
         if (btest(sbflag(ib),ii-1)) then
           dh_sbflag(ii,ib) = .true.
           is_sbflag(ib) =.true.
         endif
         if (btest(sbflag(ib),ii+15)) then
           dh_sbflag(ii+mbands,ib) = .true.
           is_sbflag(ib) =.true.
         endif
      enddo
    enddo
  else
    dh_saflag = .false.
    do ia=1, r_nant
      l = 0
      do j=1, r_nword
        call i4toi4(iw(k),dummy,1)
        k = k+1
        do m=1, 32
          l = l+1
          if (btest(dummy,m-1).and.l.le.r_lband) then
            dh_saflag(l,ia) = .true.
            is_saflag(ia) = .true.
          endif
        enddo
      enddo 
    enddo
    do ia=1, r_nant
      l = 0
      do j=1, r_nword
        call i4toi4(iw(k),dummy,1)
        k = k+1
        do m=1, 32
          l = l+1
          if (btest(dummy,m-1).and.l.le.r_lband) then
            dh_saflag(l+mbands,ia) = .true.
            is_saflag(ia) = .true.
          endif
        enddo
      enddo 
    enddo
    dh_sbflag = .false.
    do ib=1, r_nbas
      l = 0
      do j=1, r_nword
        call i4toi4(iw(k),dummy,1)
        k = k+1
        do m=1, 32
          l = l+1
          if (btest(dummy,m-1).and.l.le.r_lband) then
            dh_sbflag(l,ib) = .true.
            is_sbflag(ib) = .true.
          endif
        enddo
      enddo 
    enddo
    do ib=1, r_nbas
      l = 0
      do j=1, r_nword
        call i4toi4(iw(k),dummy,1)
        k = k+1
        do m=1, 32
          l = l+1
          if (btest(dummy,m-1).and.l.le.r_lband) then
            dh_sbflag(l+mbands,ib) = .true.
            is_sbflag(ib) = .true.
          endif
        enddo
      enddo 
    enddo
  endif
  if (de%version.ge.3) then
    call r4tor4(iw(k),dh_cable_alternate,r_nant)
    k = k + r_nant
    call r4tor4(iw(k),dh_ntime,1)
    k = k + 1 
    if (dh_ntime.gt.0) then
      call r4tor4(iw(k),dh_time_monitoring,dh_ntime)
      k = k + dh_ntime
    endif
    do j = 1, 2
      call r4tor4(iw(k),dh_incli(1,j),r_nant)
      k = k + r_nant
    enddo
    call r4tor4(iw(k),dh_incli_temp,r_nant)
    k = k + r_nant
    do j = 1, 3
      call r4tor4(iw(k),dh_tilfoc(1,j),r_nant)
      k = k + r_nant
    enddo
    do j = 1, 2
      call r4tor4(iw(k),dh_offfoc_xy(1,j),r_nant)
      k = k + r_nant
    enddo    
  else
    do j=1, r_nant
      call r4tor4(dh_test1(3,j),dh_cable_alternate(j),1)
    enddo
    dh_ntime = 4
    call r4tor4(dh_test0,dh_time_monitoring,dh_ntime)
  endif
  if (de%version.ge.6) then
    if (r_nant_vlbi.gt.0) then
      call r4tor4(iw(k),dh_vlbi_phaseff,r_nbb)
      k = k + r_nbb
      do j=1, r_nbb
        call r4tor4(iw(k),dh_vlbi_phase_tot(1,j),r_nant)
        k = k + r_nant
      enddo
      do j=1, r_nbb
        call r4tor4(iw(k),dh_vlbi_phase_inc(1,j),r_nant)
        k = k + r_nant
      enddo
    else
      dh_vlbi_phaseff = 0
      dh_vlbi_phase_inc = 0
      dh_vlbi_phase_tot = 0
    endif
  else
    dh_vlbi_phaseff = 0
    dh_vlbi_phase_inc = 0
    dh_vlbi_phase_tot = 0
  endif
!        print *, '=========== end decode_header  k= ',k
  !
  call datec(dh_obs,dh_date,error)
  call sexag(ch,dh_utc*pi/43200.,24)
  dh_time = ch(1:12)
end subroutine decode_header
!
subroutine encode_header(iw)
  use classic_api  
  use clic_file
  integer :: iw(*)                  !
  ! Global
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: ii,j,k,l,m,natfac,temp_dim,ia,ib
  integer :: saflag(mnant),sbflag(mnbas),dummy,nword
  real :: dellin(m_pol_rec,mnant,2),delay(m_pol_rec,mnant)
  logical :: to_new_receivers
  !------------------------------------------------------------------------
  ! CODE:
  ! We want do use this for format conversion ...
  to_new_receivers = (r_kind.ge.5)
!        print *,'============encode header to_new ',to_new_receivers, &
!            r_kind
  k = 1
  call r4tor4 (dh_dump,iw(k),3)
  k = k + 3
  !
  !      call r8tor8 (dh_utc,iw(k),4)
  ! Use an R4TOR4 call to avoid mis-aligned problems
  call r4tor4 (dh_utc,iw(k),8)
  k = k + 8
  if (de%version.le.2) then
    call r4tor4 (dh_test0,iw(k),10)
    k = k + 10
  else
    call r4tor4 (dh_test0,iw(k),20)
    k = k + 20
  endif
  call r4tor4 (dh_aflag,iw(k),r_nant)
  k = k + r_nant
  if (de%version.le.3) then
    if (to_new_receivers) then
      if (r_dobs.lt.(-6406)) then    ! changed the 13/02/2007
        temp_dim = r_npol_rec + r_nband_widex
      else
        temp_dim = 2 + r_nband_widex
      endif
      do ia=1, r_nant
        call r4tor4 (dh_total(1,ia),iw(k),temp_dim)
        k = k + temp_dim
      enddo
    else
      do ia=1, r_nant
        call r4tor4 (dh_total(1,ia),iw(k),1)
        call r4tor4 (dh_total(2,ia),iw(k),1)
        k = k + 1
      enddo
    endif
  else
    do ia=1, r_nant
      call r4tor4(dh_total(1,ia),iw(k),r_nbb)
      k = k + r_nbb
    enddo
  endif
  ! atfac
  if (de%version.le.3) then
    natfac = max(r_npol_rec, r_lband)
    if (to_new_receivers) then
      natfac = max(r_npol_rec, r_lband)
    else
      natfac = 1
    endif
    do ia=1, r_nant
      do j=1, 2
        call r4tor4 (dh_atfac(1,j,ia),iw(k),natfac)
        k = k + natfac
      enddo
    enddo
  else
    do ia=1, r_nant
      do j=1, 2
        call r4tor4 (dh_atfac(1,j,ia),iw(k),r_nbb)
        k = k + r_nbb
      enddo
    enddo
  endif
  call r4tor4 (dh_rmspe,iw(k),2*r_nant)
  k = k + 2*r_nant
  if (de%version.le.2) then
    call r4tor4 (dh_delcon,iw(k),r_nant)
    k = k + r_nant
  endif
  if (de%version.le.3) then
    do ia=1, r_nant
      do j=1, r_nbb
        l = r_mappol(ia,j)
        if (j.le.2) then
           dellin(l,ia,1) = dh_dellin(j,ia)
        else
           dellin(l,ia,2) = dh_dellin(j,ia)
        endif
      enddo
    enddo
    if (r_nband_widex.ne.0) then
      temp_dim = 2
    else
      temp_dim = 1
    endif
    do ia=1, r_nant
      do j = 1, temp_dim
         call r4tor4 (dellin(1,ia,j),iw(k),r_npol_rec)
         k = k + r_npol_rec
      enddo
    enddo
  else
    do ia=1, r_nant
      call r4tor4 (dh_dellin(1,ia),iw(k),r_nbb)
      k = k + r_nbb
    enddo
  endif 
  call r4tor4 (dh_delayc,iw(k),r_nant)
  k = k + r_nant
  !
  if (de%version.le.3) then
    do ia=1, r_nant
      do j=1, r_nbb
        l = r_mappol(ia,j)
        delay(l,ia) = dh_delay(j,ia)
      enddo
      call r4tor4 (dh_delay(1,ia),iw(k),r_npol_rec)
      k = k + r_npol_rec
    enddo
  else
    do ia=1, r_nant
      call r4tor4 (dh_delay(1,ia),iw(k),r_nbb)
      k = k + r_nbb
    enddo
  endif
  !      CALL R4TOR4 (DH_DELAY,IW(K),R_NPOL_REC*R_NANT)
  !      K = K + R_NPOL_REC*R_NANT
  call r4tor4 (dh_phasec,iw(k),r_nant)
  k = k + r_nant
  call r4tor4 (dh_phase,iw(k),r_nant)
  k = k + r_nant
  call r4tor4 (dh_ratec,iw(k),r_nant)
  k = k + r_nant
  call r4tor4 (dh_cable,iw(k),r_nant)
  k = k + r_nant
  if (de%version.le.2) then
    call r4tor4 (dh_gamme,iw(k),r_nant)
    k = k + r_nant
  endif
  if (de%version.le.2) then
    do ia=1, r_nant
      call r4tor4 (dh_test1(1,ia),iw(k),5)
      k = k + 5
    enddo
  else
    call r4tor4 (dh_test1,iw(k),20*r_nant)
    k = k + 20*r_nant
  endif
  !
  if (to_new_receivers) then
    do ib=1, r_nbas
      do j=1, 2
        call r4tor4 (dh_rmsamp(1,j,ib),iw(k),r_npol_rec)
        k = k + r_npol_rec
      enddo
    enddo
    do ib=1, r_nbas
      do j=1, 2
        call r4tor4 (dh_rmspha(1,j,ib),iw(k),r_npol_rec)
        k = k + r_npol_rec
      enddo
    enddo
  else
    call r4tor4 (dh_rmsamp,iw(k),2*r_nbas)
    k = k + 2*r_nbas
    call r4tor4 (dh_rmspha,iw(k),2*r_nbas)
    k = k + 2*r_nbas
  endif
  !
  call r4tor4 (dh_offfoc,iw(k),2*r_nant)
  k = k + r_nant
  call r4tor4 (dh_offlam,iw(k),r_nant)
  k = k + r_nant
  call r4tor4 (dh_offbet,iw(k),r_nant)
  k = k + r_nant
  call r4tor4 (dh_bflag,iw(k),r_nbas)
  k = k + r_nbas
  call r4tor4 (dh_uvm,iw(k),2*r_nbas)
  k = k + 2*r_nbas
  if (de%version.le.2) then
    call r4tor4 (dh_infac,iw(k),4*r_nbas)
    k = k + 4*r_nbas
  endif
  call r4tor4 (dh_wvr,iw(k),mwvrch*r_nant)
  k = k + mwvrch*r_nant
  call r4tor4 (dh_wvrstat,iw(k),r_nant)
  k = k + r_nant
  if (to_new_receivers) then
    if (de%version.le.3) then
      do ii = 1, r_lband
        do j = 1, r_nant
          call r4tor4 (dh_actp(1,j,ii),iw(k),r_npol_rec)
          k = k + r_npol_rec
        enddo
      enddo
    endif
    do j=1, r_nant
      call r4tor4 (dh_anttp(1,j),iw(k),r_nif)
      k = k + r_nif
    enddo
  endif
  if (de%version.le.3) then
    saflag = 0
    do ia = 1, r_nant
      do ii = 1, 16
         if (dh_saflag(ii,ia))  saflag(ia)= ibset(saflag(ia),ii-1)
         if (dh_saflag(ii+mbands,ia))  saflag(ia)= ibset(saflag(ia),ii+15)
      enddo
    enddo
    sbflag = 0
    do ib = 1, r_nbas
      do ii = 1, 16
         if (dh_sbflag(ii,ib)) sbflag(ib)= ibset(sbflag(ib),ii-1)
         if (dh_sbflag(ii+mbands,ib)) sbflag(ib)= ibset(sbflag(ib),ii+15)
      enddo
    enddo
    if (r_nband_widex.ne.0) then
      call w4tow4 (saflag,iw(k),r_nant)
      k = k + r_nant
      call w4tow4 (sbflag,iw(k),r_nbas)
      k = k + r_nbas
    endif
  else
    nword = int(r_lband/32)+1
    do ia=1, r_nant
      l = 0
      do j=1, nword
        dummy = 0
        do ii= 1, 32
          l=l+1
          if (l.le.r_lband) then
            if (dh_saflag(l,ia)) dummy = ibset(dummy,ii-1)
          endif
        enddo
        call i4toi4(dummy,iw(k),1)
        k = k + 1
      enddo
    enddo
    do ia=1, r_nant
      l = 0
      do j=1, nword
        dummy = 0
        do ii= 1, 32
          l=l+1
          if (l.le.r_lband) then
            if (dh_saflag(l+mbands,ia)) dummy = ibset(dummy,ii-1)
          endif
        enddo
        call i4toi4(dummy,iw(k),1)
        k = k + 1
      enddo
    enddo
    do ib=1, r_nbas
      l = 0
      do j=1, nword
        dummy = 0
        do ii= 1, 32
          l=l+1
          if (l.le.r_lband) then
            if (dh_sbflag(l,ib)) dummy = ibset(dummy,ii-1)
          endif
        enddo
        call i4toi4(dummy,iw(k),1)
        k = k + 1
      enddo
    enddo
    do ib=1, r_nbas
      l = 0
      do j=1, nword
        dummy = 0
        do ii= 1, 32
          l=l+1
          if (l.le.r_lband) then
            if (dh_sbflag(l+mbands,ib)) dummy = ibset(dummy,ii-1)
          endif
        enddo
        call i4toi4(dummy,iw(k),1)
        k = k + 1
      enddo
    enddo
  endif
  if (de%version.ge.3) then
    call r4tor4(dh_cable_alternate,iw(k),r_nant)
    k = k + r_nant
    call r4tor4(dh_ntime,iw(k),1)
    k = k + 1
    if (dh_ntime.gt.0) then
      call r4tor4(dh_time_monitoring,iw(k),dh_ntime)
      k = k + dh_ntime
    endif
    do j = 1, 2
      call r4tor4(dh_incli(1,j),iw(k),r_nant)
      k = k + r_nant
    enddo
    call r4tor4(dh_incli_temp,iw(k),r_nant)
    k = k + r_nant
    do j = 1, 3
      call r4tor4(dh_tilfoc(1,j),iw(k),r_nant)
      k = k + r_nant
    enddo
    do j = 1, 2
      call r4tor4(dh_offfoc_xy(1,j),iw(k),r_nant)
      k = k + r_nant
    enddo    
  endif
  if (de%version.ge.6) then
    if (r_nant_vlbi.gt.0) then
      call r4tor4(dh_vlbi_phaseff,iw(k),r_nbb)
      k = k + r_nbb
      do j=1, r_nbb
        call r4tor4(dh_vlbi_phase_tot(1,j),iw(k),r_nant)
        k = k + r_nant
      enddo
      do j=1, r_nbb
        call r4tor4(dh_vlbi_phase_inc(1,j),iw(k),r_nant)
        k = k + r_nant
      enddo
   endif
   endif
!      print *, '=========== end encode_header  k= ',k
end subroutine encode_header
