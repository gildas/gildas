!-----*-fortran-*------------
!
      integer, parameter ::  msideband= 2, mCorrelationMode = 2, msw =  &
     &     msideband *mrlband*2, mCorrHolo=6
!c     &     msideband *mrlband*2, mCorr=4, mCorrHolo=6
!     Stations: all antennas plus one weather station:      
      integer Weather_Station_Id, Station_Id(mnant)
!     Antennas 
      integer Antenna_Id(mnant)
!     Focus and pointing models antenna based,
      integer FocusModel_Id(mnant), PointingModel_Id(mnant)
!     Polarization description: at this point one for correlator and one
!     for total power; there will be one for each correaltor unit in the
!     future.
      integer Polarization_Id(mrlband), TotPowPolarization_Id
!     Spectral windows (correlator units: one for each sideband, one for
!     each of temporal and spectral data; plus the total powers).
!     bbb_list gives for each data description item the two bure
!     correlation units (one for rach product)
      integer spectralWindow_Id(msideband,mrlband,2),                   &
     &     TotPowSpectralWindow_Id(msideband), swId_List(msw),          &
     &     bb_list(msw), v_lband, v_bands(msw,2), v_ncorr(msw),         &
     &     sb_list(msw), PolarizationId_list(msw), numCorr_list(msw)
! for Bure we have spectral window information  for all radiometers.
      integer :: almaRadiometer_Id, radiometerSpw_Id(mnant)
      integer :: squarelawdetector_Id
      integer ReceiverId_List(mnant,msw)
      logical cont_list(msw) 
!     Correlator mode and processor
      integer CorrelatorMode_Id, Processor_Id, TotPowProcessor_Id, &
     & RadiometerProcessor_Id
!     Beam (not used actually)
      integer Beam_Id
!     Switch cycle (single dish stuff)
      integer SwitchCycle_Id
!     The config descriptions: one for spectral data, one for temporal
!     data, one for total powers:
      integer ConfigDescription_Id, contConfigDescription_Id,           &
     &     totPowConfigDescription_Id
      integer Field_Id, ExecBlock_Id , Ephemeris_Id ,Observation_Id,    &
     &     sourceParameter_Id, History_Id, weather_Id, state_Id,  &
      sbsummary_id
!     
      integer feed_Id, Doppler_Id, Source_Id
!     The data descriptions
      integer dataDescription_Id(msideband,mrlband,2),                  &
     &     totPowDataDescription_Id, ddId_List(msideband*mrlband*2)
      integer Receiver_Id(mnant,msideband,mrlband,2),                   &
     &     totPowReceiver_Id(mnant,msideband)
      integer*8 time_Interval(2), time_Start(2), scan_startTIme,        &
     &     scan_endtime, eb_startTime, eb_endTime
      integer next_Uid
      real scale_Factor(mrlband, mCorrelationMode)
      integer bit_Size(mrlband, mCorrelationMode), ddId_size
      real*8 freq_lo1, freq_lo2, r_antgeo(3,mnant)
      character*128 file_name, execBlock_uid, project_uid, &
           schedBlock_uid, obsUnitSet_uid, field_name
      integer  scan_intent, subscan_intent, data_type
      integer  holo_type(mCorrHolo)
      integer padding
      
!
      common /sdm_ident/ time_Start, time_Interval, Weather_Station_Id, &
     &     Station_Id,Antenna_Id, FocusModel_Id, PointingModel_Id       &
     &     ,SpectralWindow_Id, TotPowSpectralWindow_Id, Polarization_Id &
     &     ,TotPowPolarization_Id, CorrelatorMode_Id, Processor_Id,     &
     &     TotPowProcessor_Id , RadiometerProcessor_Id,                 &
     &     DataDescription_Id,                                          &
     &     totPowDataDescription_Id ,totPowConfigDescription_Id, Beam_Id&
     &     , SwitchCycle_Id ,ConfigDescription_Id, sbsummary_id,        &
     &     contConfigDescription_Id, Field_Id ,ExecBlock_Id,            &
     &     Ephemeris_Id, Observation_Id, feed_Id ,Receiver_Id,          &
     &     totPowReceiver_Id, Doppler_Id, Source_Id, state_Id           &
     &     ,sourceParameter_Id, History_Id, weather_Id, ddId_list,      &
     &     swId_list, ddId_size, bb_list, cont_list, sb_list,           &
     &     ReceiverId_List,  padding,                                   &
     &     r_antgeo, holo_type, v_lband, v_bands,                       &
     &     v_ncorr, PolarizationId_list, numCorr_list, &
     &     almaRadiometer_Id, radiometerSpw_Id, squarelawdetector_Id
      integer Scan_Number, subScan_Number, num_Corr, num_CorrTotPow,    &
     &     num_subscan
      common /sdm_dim/ scan_startTime, scan_endtime, eb_starttime, &
           eb_endtime, Scan_Number,       &
     &     subScan_Number, num_corr,num_CorrTotPow, next_Uid,           &
     &     scale_Factor, bit_Size,num_Subscan, freq_lo1, freq_lo2,      &
     &     scan_intent, subscan_intent, data_type
      common /sdm_char/ file_name, execBlock_uid, project_uid,           &
           schedBlock_uid, obsUnitSet_uid, field_name
