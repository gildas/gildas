subroutine plot_power(error)
  use gkernel_interfaces
  use gildas_def
  use clic_rdata
  !---------------------------------------------------------------------
  ! Plot the power spectra of the variables in Y (X must be TIME)
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: ix, ib, nd, ndt
  integer(kind=address_length) :: ipx, ipy, ipw, ipz, ips
  integer(kind=address_length) :: ipxb, ipyb, ipwb, ipzb, ipsb
  !-----------------------------------------------------------------------
  ! loop on boxes: SET X should be TIME.
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ipz = gag_pointer(data_z,memory)
  ips = gag_pointer(data_s,memory)
  nd = 0
  do ib=1, n_boxes
    ix = i_x(k_x(ib))
    if (ix.ne.xy_time) then
      call message(8,4,'PLOT_POWER','X should be TIME')
      error = .true.
      return
    elseif (do_bin) then
    endif
    ipxb = ipx+(ib-1)*m_data
    ipyb = ipy+(ib-1)*m_data
    ipwb = ipw+(ib-1)*m_data
    ipzb = ipz+(ib-1)*m_data*2
    ipsb = ips+(ib-1)*m_data*2
    ndt = n_data(ib)
    call sub_power(m_data,ndt,memory(ipxb),memory(ipyb),   &
      memory(ipwb),memory(ipzb),memory(ipsb),do_bin,bin_size,   &
      error)
    n_data(ib) = ndt
    nd = max(nd,n_data(ib))
  enddo
  !
  ! reset the variables
  call sub_resetvar(nd,n_boxes,memory(ipx),memory(ipy),   &
    memory(ipw),error)
  return
end subroutine plot_power
!
subroutine sub_power(md,nd,x,y,w,z,work,bin,xbin,error)
  use gildas_def
  integer :: md                     !
  integer :: nd                     !
  real*4 :: x(md)                   !
  real*4 :: y(md)                   !
  real*4 :: w(md)                   !
  complex :: z(md)                  !
  real :: work(md,2)                !
  logical :: bin                    !
  real*4 :: xbin                    !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  integer :: i, ii, j, jj, k, nx, ns, n0, n1, nbin
  real :: xmin, xmax, xstep, ystep, df, s, sx, sx2, sy, sxy, wh, a, b
  !-----------------------------------------------------------------------
  ! (x is already sorted)
  ! skip blanked points
  j = 0
  do i=1, nd
    if (w(i).gt.0 .and. (abs(y(i)).lt.1e10)) then
      j = j+1
      if (j.lt.i) then
        x(j) = x(i)
        w(j) = w(i)
        y(j) = y(i)
      endif
    endif
  enddo
  nd = j
  !
  ! Get min and max of X
  xmin = x(1)
  xmax = x(nd)
  xstep = xmax-xmin
  do i = 2, nd
    xstep = min(xstep,x(i)-x(i-1))
  enddo
  nx = nint((xmax-xmin)/xstep)+1
  if (nx.gt.1) xstep = (xmax-xmin)/(nx-1)
  if (nx.gt.md) then
    call message(8,4,'SUB_POWER','Too many data points')
    error = .true.
    return
  endif
  !
  ! Fill in the holes, starting form the end
  j = nx
  i = nd
  do while (j.gt.i)
    x(j) = x(i)
    y(j) = y(i)
    w(j) = w(i)
    ns = max(j-i+1,nint((x(i)-x(i-1))/xstep))
    if (ns.gt.1) then
      ystep = (y(i)-y(i-1))/ns
      do k=1, ns-1
        x(j-k) = x(i)-k*xstep
        y(j-k) = y(i)-k*ystep
      enddo
    endif
    j = j - ns
    i = i - 1
  enddo
  !
  ! Substract slope and weight (Hanning)
  sxy = 0
  sy = 0
  sx = 0
  sx2 = 0
  s = 0
  do i=1, nx
    if (abs(x(i)).gt.1e5 .or. abs(y(i)).gt.1e5) then
    endif
    s = s+1
    sx = sx+x(i)
    sy = sy+y(i)
    sx2 = sx2+x(i)**2
    sxy = sxy+y(i)*x(i)
  enddo
  a = (s*sxy-sy*sx)/(s*sx2-sx**2)
  b = -(sx*sxy-sy*sx2)/(s*sx2-sx**2)
  do i=1,nx
    wh = (1.-cos(2*pi*(i-1)/(nx-1)))/2.
    y(i) = y(i)-a*x(i)-b
    z(i) = cmplx(y(i))*wh
  enddo
  !
  ! Do fft
  call fourt(z,nx,1,1,0,work)
  !
  ! We drop the zero frequency
  ! the frequency interval in Hz
  df = 1./(3600.*xstep*nx)
  nd = nx/2-1
  do i=1, nd-1
    ii = i+1
    jj = nx+1-ii
    x(i) = (i+1)*df
    y(i) = (abs(z(ii)/nx)**2 + abs(z(jj)/nx)**2)*x(i)
    x(i) = log10(x(i))
    w(i) = 1.
  enddo
  x(nd) = nd*df
  y(nd) = abs(z(nd+1)/nx)**2*x(nd)
  x(nd) = log10(x(nd))
  w(nd) = 1.
  !
  ! Optionnally integrate in bins (not average)
  if (bin) then
    n0 = nint(x(1)/xbin)
    n1 = nint(x(nd)/xbin)
    nbin = n1-n0+1
    do i=1, nbin
      work(i,1) = 0
      work(i,2) = 0
    enddo
    do i=1, nd
      j = nint(x(i)/xbin)-n0+1
      work(j,1) = work(j,1) + y(i)
      work(j,2) = work(j,2) + 1
    enddo
    nd = nbin
    do i=1, nbin
      x(i) = (i+n0-1)*xbin
      w(i) = work(i,2)
      y(i) = work(i,1)
    enddo
  endif
  return
end subroutine sub_power
