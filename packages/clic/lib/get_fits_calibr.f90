!
subroutine get_fits_calibr(unit,ifeed,error)
  use gildas_def
  use classic_api  
  integer :: unit                   !
  integer :: ifeed                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: comment
  real :: f(mnant)
  integer :: status, itest, i, ia, ja, ib, nullval, k
  integer :: num_col, not_found
  parameter (not_found=202)
  logical :: anyf
  integer :: mode_tr
  integer :: mkw
  parameter (mkw=14)
  character(len=8) :: kwmodel(mkw)
  data kwmodel/   &
    !c     &     'IA','CA','NPAE','AN','AW','IE','HECE','REFRACTIO'/
    ! changed 2004-04-18
    'IA','CA','HASA','HACA','HASA2',   &
    'IE','HESE','HECE','HESA','HESA2','NPAE',   &
    'AN','AW','REFRACTIO'/
  !*---------------------------------------------------------------------------
  call chtoby('TR  ',mode_tr,4)
  status = 0
  ! Get  header keywords
  call ftgkyj(unit,'SCAN-NUM',itest,comment,status)
  comment = 'SCAN-NUM'
  if (status .gt. 0) go to 99
  if (itest.ne.r_scan) then
    call message(8,4,'GET_FITS_CALIBR','Wrong scan number')
    error = .true.
    return
  endif
  !
  ! Radiometer
  call ftgkye(unit,'FREQUSRD',r_frs_mon,comment,status)
  comment = 'FREQUSRD'
  if (status .eq. not_found) then
    r_frs_mon = 0
    status = 0
  elseif (status .gt. 0) then
    goto 99
  endif
  r_frs_mon = r_frs_mon/1e6
  call ftgkye(unit,'FREQUIRD',r_fri_mon,comment,status)
  comment = 'FREQUIRD'
  if (status .eq. not_found) then
    r_fri_mon = 0
    status = 0
  elseif (status .gt. 0) then
    goto 99
  endif
  r_fri_mon = r_fri_mon/1e6
  !
  ! There are some more headers that are not used in CLIC for the time being.
  !
  ! Read the columns
  call ftgcno(unit,.false.,'ANTENNID',num_col,status)
  if (status.gt.0) then
    call column_error(status,'ANTENNID',num_col,error)
    goto 99
  endif
  call ftgcvj(unit,num_col,1,1,r_nant,nullval,   &
    r_kant,anyf,status)
  !
  call ftgcno(unit,.false.,'STATIOID',num_col,status)
  if (status.gt.0) then
    call column_error(status,'STATIOID',num_col,error)
    goto 99
  endif
  call ftgcvj(unit,num_col,1,1,r_nant,nullval,   &
    r_istat,anyf,status)
  !
  call ftgcno(unit,.false.,'STABXYZ',num_col,status)
  if (status.gt.0) then
    call column_error(status,'STABXYZ',num_col,error)
    goto 99
  endif
  call ftgcvd(unit,num_col,1,1,3*r_nant,nullval,   &
    r_ant,anyf,status)
  !
  call ftgcno(unit,.false.,'STAXOF',num_col,status)
  if (status.gt.0) then
    call column_error(status,'STAXOF',num_col,error)
    goto 99
  endif
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    r_axes,anyf,status)
  do ib = 1, r_nbas
    ia = r_iant(ib)
    ja = r_jant(ib)
    do k=1, 3
      r_bas(k,ib) = r_ant(k,ja)-r_ant(k,ia)
    enddo
  enddo
  do ia=1, r_nant
    r_cmode_mon(ia) = mode_tr
    r_cmode(1,ia) = mode_tr
    r_ceff(1,ia) = 1.0
  enddo
  ! ETAFSS
  call ftgcno(unit,.false.,'ETAFSS',num_col,status)
  if (status.gt.0) then
    call column_error(status,'ETAFSS',num_col,error)
    goto 99
  endif
  !            CALL FTGCVE(UNIT,TOTPOWER_COL,I,k,1,NULLVAL,
  !     &      DH_TOTAL(IA),ANYF,STATUS)
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_feff(1,ia),anyf,status)
    if (status.gt.0) goto 99
    if (r_teles.eq.'VTX-ALMATI'   &
      .or. r_teles.eq.'AEC-ALMATI') then
      r_feff(1,ia) = 0.90
    endif
  enddo
  ! BEAMEFF
  call ftgcno(unit,.false.,'BEAMEFF',num_col,status)
  if (status.gt.0) then
    call column_error(status,'BEAMEFF',num_col,error)
    goto 99
  endif
  if (status.gt.0) goto 99
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_beef(1,ia),anyf,status)
    if (status.gt.0) goto 99
    if (r_teles.eq.'VTX-ALMATI'   &
      .or. r_teles.eq.'AEC-ALMATI') then
      r_beef(1,ia) = 0.95
    endif
  enddo
  ! ANTGAIN
  call ftgcno(unit,.false.,'ANTGAIN',num_col,status)
  if (status.gt.0) then
    call column_error(status,'ANTGAIN',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_jykel(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! HUMIDITY
  call ftgcno(unit,.false.,'HUMIDITY',num_col,status)
  if (status.gt.0) then
    call column_error(status,'HUMIDITY',num_col,error)
    goto 99
  endif
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    f,anyf,status)
  if (status.gt.0) goto 99
  !      DO I=1, R_NANT
  r_humid = f(1)
  !      ENDDO
  ! TAMBIENT
  call ftgcno(unit,.false.,'TAMBIENT',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TAMBIENT',num_col,error)
    goto 99
  endif
  if (status.gt.0) goto 99
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    f,anyf,status)
  if (status.gt.0) goto 99
  r_tamb = f(1)
  ! PRESSURE into hPa
  call ftgcno(unit,.false.,'PRESSURE',num_col,status)
  if (status.gt.0) then
    call column_error(status,'PRESSURE',num_col,error)
    goto 99
  endif
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    f,anyf,status)
  if (status.gt.0) goto 99
  r_pamb = f(1)/100.
  ! THOT
  call ftgcno(unit,.false.,'THOT',num_col,status)
  if (status.gt.0) then
    call column_error(status,'THOT',num_col,error)
    goto 99
  endif
  do ia = 1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_tchop(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! TCOLD
  call ftgcno(unit,.false.,'TCOLD',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TCOLD',num_col,error)
    goto 99
  endif
  do ia = 1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_tcold(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! PHOT
  call ftgcno(unit,.false.,'PHOT',num_col,status)
  if (status.gt.0) then
    call column_error(status,'PHOT',num_col,error)
    goto 99
  endif
  do ia = 1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_cchop(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! PCOLD
  call ftgcno(unit,.false.,'PCOLD',num_col,status)
  if (status.gt.0) then
    call column_error(status,'PCOLD',num_col,error)
    goto 99
  endif
  do ia = 1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_ccold(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! PSKY
  call ftgcno(unit,.false.,'PSKY',num_col,status)
  if (status.gt.0) then
    call column_error(status,'PSKY',num_col,error)
    goto 99
  endif
  do ia = 1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_csky(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! GAINIMAG
  call ftgcno(unit,.false.,'GAINIMAG',num_col,status)
  if (status.gt.0) then
    call column_error(status,'GAINIMAG',num_col,error)
    do ia = 1, r_nant
      r_gim(1,ia)=0.001
    enddo
  else
    do ia = 1, r_nant
      call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
        r_gim(1,ia),anyf,status)
      if (status.gt.0) goto 99
    enddo
  endif
  ! TRX
  call ftgcno(unit,.false.,'TRX',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TRX',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_trec(1,ia),anyf,status)
    if (status.gt.0) goto 99
    if (r_teles.eq.'VTX-ALMATI'   &
      .or. r_teles.eq.'AEC-ALMATI') then
      if (ifeed.eq.1) then
        r_trec(1,ia) = 87.
      elseif (ifeed.eq.2) then
        r_trec(1,ia) = 80.
      endif
    endif
  enddo
  ! TSYS
  call ftgcno(unit,.false.,'TSYS',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TSYS',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_tsyss(1,ia),anyf,status)
    if (status.gt.0) goto 99
  !         ISB = (3-R_ISB)/2
  !         DH_ATFAC(ISB,IA) = R_TSYSS(IA)
  enddo
  ! TSYSIMAG
  call ftgcno(unit,.false.,'TSYSIMAG',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TSYSIMAG',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_tsysi(1,ia),anyf,status)
    if (status.gt.0) goto 99
  !         ISB = (3+R_ISB)/2
  !         DH_ATFAC(ISB,IA) = R_TSYSI(IA)
  enddo
  ! TAU
  call ftgcno(unit,.false.,'TAU',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TAU',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_taus(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! TAUIMAG
  call ftgcno(unit,.false.,'TAUIMAG',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TAUIMAG',num_col,error)
    goto 99
  endif
  do ia=1, r_nant
    call ftgcve(unit,num_col,ia,ifeed,1,nullval,   &
      r_taui(1,ia),anyf,status)
    if (status.gt.0) goto 99
  enddo
  ! TCABIN
  call ftgcno(unit,.false.,'TCABIN',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TCABIN',num_col,error)
    goto 99
  endif
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    r_tcabin,anyf,status)
  if (status.gt.0) goto 99
  ! TDEWAR
  call ftgcno(unit,.false.,'TDEWAR',num_col,status)
  if (status.gt.0) then
    call column_error(status,'TDEWAR',num_col,error)
    goto 99
  endif
  call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
    f,anyf,status)
  if (status.gt.0) goto 99
  do i=1, r_nant
    r_tdewar(1,i) = f(i)
  enddo
  !
  ! PREWATER
  call ftgcno(unit,.false.,'PREWATER',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      f,anyf,status)
    do i=1, r_nant
      r_h2omm(1,i) = f(i)*1e3
    enddo
  endif
  ! PREWATRD
  call ftgcno(unit,.false.,'PREWATRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      f,anyf,status)
    do i=1, r_nant
      r_h2o_mon(i) = f(i)*1e3
    enddo
  endif
  ! ETAFSRD
  call ftgcno(unit,.false.,'ETAFSRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_feff_mon,anyf,status)
  endif
  ! THOTRD
  call ftgcno(unit,.false.,'THOTRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_tchop_mon,anyf,status)
  endif
  ! TCOLDRD
  call ftgcno(unit,.false.,'TCOLDRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_tcold_mon,anyf,status)
  endif
  ! PHOTRD
  call ftgcno(unit,.false.,'PHOTRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_cchop_mon,anyf,status)
  endif
  ! PCOLDRD
  call ftgcno(unit,.false.,'PCOLDRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_ccold_mon,anyf,status)
  endif
  ! PSKYRD
  call ftgcno(unit,.false.,'PSKYRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_csky_mon,anyf,status)
  endif
  ! TSYSRD
  call ftgcno(unit,.false.,'TSYSRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_tsys_mon,anyf,status)
    if (status.ne.0) goto 99
  endif
  ! TRXRD
  call ftgcno(unit,.false.,'TRXRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_trec_mon,anyf,status)
  endif
  ! GAINIMRD
  call ftgcno(unit,.false.,'GAINIMRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_gim_mon,anyf,status)
  endif
  ! PATHRD
  call ftgcno(unit,.false.,'PATHRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_path_mon,anyf,status)
    do i=1, r_nant
      r_path_mon(i) = r_path_mon(i)*1e3
    enddo
  endif
  ! DPATHRD
  call ftgcno(unit,.false.,'DPATHRD',num_col,status)
  if (status.eq.0) then
    call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
      r_dpath_mon,anyf,status)
    do i=1, r_nant
      r_dpath_mon(i) = r_dpath_mon(i)*1e3
    enddo
  endif
  ! VALIDRD
  call ftgcno(unit,.false.,'VALIDRD',num_col,status)
  if (status.eq.0) then
    call ftgcvl(unit,num_col,1,1,r_nant,nullval,   &
      r_ok_mon,anyf,status)
    ! this is used by CLIC also.
    r_nrec_mon = 2
    r_magic_mon(1) = 1.0
    r_magic_mon(2) = 1.0
  endif
  ! Pointng model
  r_npmodel = mkw
  r_cpmodel = 0
  do k=1, mkw
    status = 0
    call ftgcno(unit,.false.,kwmodel(k),num_col,status)
    if (status.eq.0) then
      call ftgcve(unit,num_col,1,1,r_nant,nullval,   &
        f,anyf,status)
      do i=1, r_nant
        r_cpmodel(k,i) = f(i)*3600.
        ! Keep the 'normal' definition for r_collaz and r_collel...
        ! For CLIC they are pointing corrections, not collimations.
        ! in addition CA at ATF has now the opposite sign!
        if (kwmodel(k).eq.'CA') then
          r_collaz(i) = f(i)*3600.
        !                  print *, k, i, r_cpmodel(k,i), r_collaz(i)
        elseif (kwmodel(k).eq.'IE') then
          r_collel(i) = -f(i)*3600.
        endif
      enddo
    endif
  enddo
  !
  ! Just testing
  !      r_pmodel = pmodel_alma
  !      r_npmodel = 10
  !      do i=1, r_nant
  !         do j=1, r_npmodel
  !            r_cpmodel(j,i) = 0.
  !         enddo
  !      enddo
  !      WRITE(6,*) 'r_pmodel = ',R_pmodel,'   r_npmodel',r_npmodel
  !      do i=1, r_npmodel
  !         write(6,*) 'i, r_cpmodel(i) = ', i,
  !     $        (r_cpmodel(i,j),j=1, r_nant)
  !      enddo
  return
  !
99 call printerror('GET_FITS_CALIBR',status)
  call message(6,2,'GET_FITS_CALIBR',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_calibr
!
subroutine column_error(status,name,icol,error)
  integer :: status                 !
  character(len=*) :: name          !
  integer :: icol                   !
  logical :: error                  !
  !
  if (status.eq.219) then
    status = 0
    call message(6,2,'COLUMN_ERROR',   &
      'Missing column: '//name)
    icol = -1
  elseif(status.ne.0) then
    call fio_printerror('COLUMN_ERROR',   &
      'FTGCNO ['//name//'] ',   &
      status,error)
    return
  endif
  return
end subroutine column_error
