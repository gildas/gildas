!
! Data Structures : Header r_
      INTEGER(KIND=8) R_XNUM             ! number in index (0:no obs, -1:not fro
      LOGICAL R_PRESEC(-MNSEC:0)         ! Present Sections
!
! Section -2 General Information
!
!-- not in general section on disk, copied from entry title ---
      INTEGER(KIND=8) R_NUM              ! Observation number
      INTEGER R_VER                      ! Version number
      INTEGER R_DOBS                     ! Date of observation
      INTEGER R_DRED                     ! Date of last reduction
      INTEGER R_TYPEC                    ! Type of coordinates
      INTEGER R_KIND                     ! Type of data (0=line,1=Continuum)
      INTEGER R_QUAL                     ! Quality flag
      INTEGER R_SCAN                     ! Scan number
      CHARACTER*8 R_PROJECT              ! observing project
      CHARACTER*12 R_TELES               ! Telescope name
!-- in general section on disk---
      REAL*8  R_UT                       ! Universal time
      REAL*8  R_ST                       ! Sidereal time
      REAL*4  R_AZ                       ! Azimuth
      REAL*4  R_EL                       ! Elevation
      REAL*4  R_TAU                      ! Sky opacity
      REAL*4  R_TSYS                     ! System temperature
      REAL*4  R_TIME                     ! Integration time
      INTEGER R_TYPOF                    ! type of offsets
      INTEGER R_PROC                     ! observing procedure
!-- not in data ---
      CHARACTER*12 R_CDOBS               ! date observation
      CHARACTER*12 R_CDRED               ! date reduction
      CHARACTER*12 R_CPROC               ! procedure
      CHARACTER*12 R_CTOBS               ! UT
      CHARACTER*12 R_CQUAL               ! Quality
!
! Section -3 Positions
      CHARACTER*12 R_SOURC               ! Source name
      REAL*4  R_EPOCH                    ! Epoch of observation
      REAL*8  R_LAM                      ! Lambda of center position
      REAL*8  R_BET                      ! Delta  ----
      REAL*4  R_LAMOF                    ! Offset in Lambda
      REAL*4  R_BETOF                    ! Offset in Delta
      INTEGER R_PROJ                     ! Projection type
      REAL*4  R_FOCUS                    ! focus in mm
      REAL*4  R_FLUX                     ! in Janskys, for calibrators
      REAL*4  R_SPIDX                    ! spectral index              
      REAL*4  R_STOKES_I                 ! Source Stokes I
      REAL*4  R_STOKES_Q                 ! Source Stokes Q
      REAL*4  R_STOKES_U                 ! Source Stokes U
      REAL*4  R_STOKES_V                 ! Source Stokes V
      REAL*4  R_SPIDX_STOKES(MNSTOKES)   ! Spectral indexes for the Stokes
!
! Section -21 Interferometer General Information
      INTEGER R_NANT                     ! Number of Antennas (used, e.g. virtua
      INTEGER R_NBAS                     ! Number of baselines ('')
      INTEGER R_ITYPE                    ! Type of interf. obs.
      REAL*4  R_HOURA                    ! Start hour angle
      INTEGER R_KANT(MNANT)              ! Antenna Physical numbers
      INTEGER R_KENT(MNANT)              ! Corelator Entry numbers
      INTEGER R_ISTAT(MNANT)             ! Station numbers
      INTEGER R_IANT(MNBAS)              ! Start virtual antenna for baseline
      INTEGER R_JANT(MNBAS)              ! End virtual antenna for baseline
      REAL*8  R_BAS(3,MNBAS)             ! components of baseline (meters)
      REAL*8  R_ANT(3,MNANT)             ! coordinates of antenna (meters)
      INTEGER R_NTRI                     ! Number of triangles
      INTEGER R_ATRI(3,MNTRI)            ! triang.def. A1 A2 A3 (in r_ntri.ne.0)
      REAL*4  R_AXES(MNANT)              ! offset between azim. and elev. axes (
      REAL*4 R_PHLO1(MNANT)              ! LO1 phase offset
      REAL*4 R_PHLO3(MNANT)              ! LO3 phase offset
      INTEGER R_NBAND_WIDEX              ! Number of WIDEX units
!-- not in data ---
      CHARACTER*48 R_CONFIG              ! extended configuration
!
! Section -22	RF Frequency Setup
      CHARACTER*12 R_LINE                ! Line name (12 chars)
      INTEGER R_ISB                      ! Side band (+1 upper, -1 Lower)
      INTEGER R_LOCK                     ! lock sign (+1 High, -1 Low)
      REAL*8  R_RESTF                    ! Rest Frequency (MHz)
      REAL*8  R_FLO1                     ! LO1 frequency (MHz)
      REAL*8  R_FLO1_REF                 ! LO1 referencefrequency (MHz)
                                         ! = L02 with "old" receivers
      REAL*8  R_FIF1                     ! IF1 frequency (MHz)
      REAL*4 R_VELOC                     ! Source velocity (km/s)
      INTEGER*4 R_TYPEV                  ! type of velocity (LSR / SUN)
      REAL*4 R_DOPPL                     ! doppler veloc. correction (frac. c)
      INTEGER*4 R_NPOL_REC               ! Number of polarisation in receiver
      INTEGER*4 R_QUARTER(2)             ! Selected quarters (IF1, IF2)
      INTEGER*4 R_QWPLATE(MNANT)         ! Quarter-wave plate ON of OFF
      INTEGER*4 R_NIF                    ! Number of IF
      CHARACTER*4 R_IFNAME(MNIF)         ! IF name
      INTEGER*4 R_KIF(MNIF)              ! IF physical number
      REAL*4  R_IFATTN(MNANT,MNIF)       ! IF attenuation (dB scale)
      INTEGER R_NBB                      ! Number of basebands
      CHARACTER*4 R_BBNAME(MNBB)         ! Name of basebands
      INTEGER R_KBB(MNBB)                ! Baseband physical number
      INTEGER R_MAPBB(MNBB)              ! Mapping of entries
      INTEGER R_MAPPOL(MNANT,MNBB)       ! Mapping of pol/entries
      INTEGER R_NSUBPOWER                ! "Sub" Total power
      REAL    R_SUBPOWER_VAL             ! Freq value at ref
      INTEGER R_SUBPOWER_REF             ! ref
      REAL    R_SUBPOWER_INC             ! incr
      INTEGER R_POLSWITCH(MNANT,MNSB)    ! Polarisation switches
!
! Section -23 Continuum setup
      INTEGER R_NSB                      ! Nb. of continuum sidebands (1-2)
      INTEGER R_NBAND                    ! Nb. of continuum sub-bands (ex: 10)
      INTEGER R_NCDAT                    ! Nb. of cmplx values per baseline (ex:
      REAL*4  R_CRCH(2)                  ! Reference channel in each sideband
      REAL*4  R_CVOFF(2)                 ! Vel. in each sideband
      REAL*4  R_CVRES(2)                 ! Vel. resolution in each sideband
      REAL*8  R_CRFOFF(2)                ! Rest Freq in each sideband
      REAL*4  R_CRFRES(2)                ! Freq. resolution in RF(MHz), signed
      CHARACTER*12 R_CNAM(2)             ! line name
      REAL*8  R_CFCEN(MCCH)              ! Center freq (MHz), for each sub-band
      REAL*4  R_CFWID(MCCH)              ! Freq. width (MHz)
      INTEGER R_SBAND(2,MCCH)            ! present side bands (1:USB, 2:LSB, 3:D
!
! Section -24 Line setup
      INTEGER R_LBAND                    ! Number of sub-bands (per baseline)
      INTEGER R_LNTCH                    ! total number of channels (<mlch)
      LOGICAL R_LFOUR                    ! Fourier Transform done
      INTEGER R_LNCH(MRLBAND)            ! Number of channels in sub-band
      INTEGER R_LICH(MRLBAND)            ! Channel offset in corr. (Ch 0)
      REAL*8  R_LFCEN(MRLBAND)           ! center freq (MHz)
      REAL*4  R_LFRES(MRLBAND)           ! Freq. resolution in IF1(MHz), signed
      REAL*4  R_LRCH(2,MRLBAND)          ! Reference channel (in subband) (U,L)
      REAL*4  R_LVOFF(2,MRLBAND)         ! Vel. in RF (MHz)
      REAL*4  R_LVRES(2,MRLBAND)         ! Vel. resolution in RF(MHz)
      REAL*8  R_LRFOFF(2,MRLBAND)        ! Freq. offset in RF (MHz)
      REAL*4  R_LRFRES(2,MRLBAND)        ! Freq. resolution in RF(MHz), signed
      CHARACTER*12 R_LNAM(2,MRLBAND)     ! line name
      INTEGER R_LILEVU(MRLBAND,MNANT)    ! sampling levels (U part)
      INTEGER R_LNSB                     ! number of line sidebands
      INTEGER R_LSBAND(2,MRLBAND)        ! line sidebands
      REAL*4  R_LCENCH(MRLBAND)          ! Channel number of R_LFCEN (NEW, 1/DEF
      REAL*4  R_LDELOFF(MNANT,MRLBAND)   ! delay offsets       (NEW, 0/def)
      INTEGER R_LMODE                    ! old sp.corr = 0, new = 1  (NEW, 0/def
      INTEGER R_LILEVL(MRLBAND,MNANT)    ! sampling levels (L part)
      INTEGER R_IUNIT(MRLBAND)           ! Physical unit number
      INTEGER R_BAND4(MRLBAND)           ! 4th conv. band (0,1,2) = (USB,LSB,DSB)
      INTEGER R_PHSELECT(MNANT,MRLBAND)  ! origin of phase correction (1=1mm,2-22GHz)
      REAL*8  R_FLO2(MRLBAND)            ! LO2 frequency
      INTEGER R_BAND2(MRLBAND)           ! 2bd conv. band (1,-1) = USB, LSB
      REAL*8  R_FLO2BIS(MRLBAND)         ! LO2bis frequency
      INTEGER R_BAND2BIS(MRLBAND)        ! 2nd bis conv. band (0,1) = (USB,LSB)
      REAL*8  R_FLO3(MRLBAND)            ! LO3 frequency
      REAL*8  R_FLO4(MRLBAND)            ! LO4 frequency
      INTEGER R_LPOLMODE(MRLBAND)        ! polar. mode (1/2)
      INTEGER R_LPOLENTRY(MNANT,MRLBAND) ! polarization entry for each antenna
      INTEGER R_BB(MRLBAND)              ! Baseband
      INTEGER R_IF(MRLBAND)              ! IF
      INTEGER R_SB(MRLBAND)              ! SB
      INTEGER R_STACHU(MRLBAND)          ! Starting chunk for spw
      INTEGER R_NCHU(MRLBAND)            ! Number of chunks in spw
      INTEGER R_CODE_STOKES(MNBAS,MRLBAND) ! Measured Stokes Code.
!-- not in data ---
      INTEGER R_NWORD                    ! NOT in data, number of sflag words
      INTEGER LBAND_ORIGINAL             ! Native number of bands
      INTEGER LNTCH_ORIGINAL             ! Native number of channels
      INTEGER*8 LDATL_ORIGINAL           ! Native size of line data
!
! Section -25 Scanning Fonction
      INTEGER R_SCATY                    ! Scan type (1=focus, 2=lambda, 3=beta)
      LOGICAL R_MOBIL(MNANT)             ! .true. in antenna is scanning.
      REAL R_COLLAZ(MNANT)               ! Azimuth collimation
      REAL R_COLLEL(MNANT)               ! elevation collimation
      REAL R_CORFOC(MNANT)               ! focus correction(mm) *NEW*
      integer r_nswitch                  ! number of switching states
      real r_wswitch(mswitch)            ! switching weights
      real r_pswitch(2,mswitch)          ! switching offsets
      integer r_pmodel                   ! pointing model type (1-Bure,2-Alma)
      integer r_npmodel                  ! number of coefficients
      real r_cpmodel(mpmodel,mnant)      ! coefficients

!
! Section -26 Atmospheric Parameters
      INTEGER R_MODE                     ! Cal mode (man, aut, water, trec, ...)
      INTEGER R_NREC                     ! number of stored receivers
      REAL*4  R_PAMB                     ! Ambient pressure
      REAL*4  R_TAMB                     ! Ambient temperature
      REAL*4  R_ALTI                     ! Site altitude (km)
      REAL*4  R_HUMID                    ! % humidity
      REAL*4  R_H2OMM(MNBB,MNANT)   ! mm of precipitable water
      REAL*4  R_TATMS(MNBB,MNANT)   ! Atmospheric temperature
      REAL*4  R_TATMI(MNBB,MNANT)   ! same for image side band
      REAL*4  R_TAUS(MNBB,MNANT)    ! Opacity at Zeinth
      REAL*4  R_TAUI(MNBB,MNANT)    ! same for image side band
      REAL*4  R_BEEF(MNBB,MNANT)    ! beam efficiency
      REAL*4  R_FEFF(MNBB,MNANT)    ! forward efficiency
      REAL*4  R_TCHOP(MNBB,MNANT)   ! Temperature of Chopper
      REAL*4  R_TCOLD(MNBB,MNANT)   ! Temperature of Cold Load
      REAL*4  R_TREC(MNBB,MNANT)    ! Receiver DSB temperature
      REAL*4  R_GIM(MNBB,MNANT)     ! relative gain (image/signal)
      REAL*4  R_CSKY(MNBB,MNANT)    ! Counts on Sky
      REAL*4  R_CCHOP(MNBB,MNANT)   ! Counts on Chopper
      REAL*4  R_CCOLD(MNBB,MNANT)   ! Counts on Cold Load
      REAL*4  R_TSYSS(MNBB,MNANT)   ! temp sys (sig side band)
      REAL*4  R_TSYSI(MNBB,MNANT)   ! temp sys (image side band)
      REAL*4  R_CEFF(MNBB,MNANT)    ! chopper efficiency
      INTEGER  R_CMODE(MNBB,MNANT)  ! cal mode
      REAL*4  R_JYKEL(MNBB,MNANT)   ! Jy to Kelvin factor
      REAL*4  R_AVWIND                   ! Aver. Wind (km/s)
      REAL*4  R_AVWINDIR                 ! Aver. Wind Direction (deg.)
      REAL*4  R_TOPWIND                  ! Top Wind (km/s)
      REAL*4  R_TOPWINDIR                ! Top Wind Direction (deg.)
      REAL*4  R_TCABIN(MNANT)            ! Temperature of Cabin
      REAL*4  R_TDEWAR(3,MNANT)          ! Temperatures of Dewar
      REAL*4  R_TOTSCALE(MNBB,MNANT)! Digital to physical power scale
!
! Section -33 Atmospheric monitoring receiver
      INTEGER R_NREC_MON                 ! rec. number of monitoring receiver
      REAL*4  R_FRS_MON                  ! freq of monitoring receiver
      REAL*4  R_FRI_MON                  ! freq of monitoring receiver(Image)
      REAL*4  R_H2O_MON(MNANT)           ! mm of precipitable water
      REAL*4  R_TATMS_MON(MNANT)         ! Atmospheric temperature
      REAL*4  R_TATMI_MON(MNANT)         ! same for image side band
      REAL*4  R_TAUS_MON(MNANT)          ! Opacity at Zenith
      REAL*4  R_TAUI_MON(MNANT)          ! same for image side band
      REAL*4  R_FEFF_MON(MNANT)          ! forward efficiency
      REAL*4  R_TCHOP_MON(MNANT)         ! Temperature of Chopper
      REAL*4  R_TCOLD_MON(MNANT)         ! Temperature of Cold Load
      REAL*4  R_TREC_MON(MNANT)          ! Receiver DSB temperature
      REAL*4  R_GIM_MON(MNANT)           ! relative gain (image/signal)
      REAL*4  R_CSKY_MON(MNANT)          ! Counts on Sky
      REAL*4  R_CCHOP_MON(MNANT)         ! Counts on Chopper
      REAL*4  R_CCOLD_MON(MNANT)         ! Counts on Cold Load
      REAL*4  R_TSYS_MON(MNANT)          ! temp sys (double side band)
      INTEGER R_CMODE_MON(MNANT)         ! cal mode
      REAL*4  R_PATH_MON(MNANT)          ! mm of path due to H2O (nu_obs)
      REAL*4  R_TEM_MON(MNANT)           ! atmospheric brightness (nu_mon)
      REAL*4  R_DPATH_MON(MNANT)         ! mm of path per deg. of atm emiss (nu_
      REAL*4  R_MAGIC_MON(2)             ! Empirical scale factor on path.
      LOGICAL R_OK_MON(MNANT)            ! Validity of correction
!
! Section -28 Passband Calibration
      INTEGER R_BPC                      ! BPC has been stored (2=IF function)
      INTEGER R_BPCDEG                   ! Degree of interpolating polynomial
      REAL*4  R_BPCCAMP(MNBB,2,MNBAS,MCCH)
      REAL*4  R_BPCCPHA(MNBB,2,MNBAS,MCCH)
      REAL*4  R_BPCLAMP(MNBB,2,MNBAS,MRLBAND,0:MBPCDEG)   ! polynomial coefficients
      REAL*4  R_BPCLPHA(MNBB,2,MNBAS,MRLBAND,0:MBPCDEG)   ! polynomial coefficients
      COMPLEX R_BPCSBA(MNBB,2,MNBAS)          !
      INTEGER R_BPFDEG                   ! Degree of interpolating polynomial
      REAL*4  R_BPFLIM(MNBB,2)       ! IF frequency interval (min, max)
      REAL*4  R_BPFAMP(MNBB,2,MNBAS,0:MBPCDEG)! polynomial coefficients
      REAL*4  R_BPFPHA(MNBB,2,MNBAS,0:MBPCDEG)! polynomial coefficients
!
! Section -29 Instrumental Calibration
      INTEGER R_IC                       ! IC has been stored
      INTEGER R_ICDEG                    ! Degree of interpolating polynomial
      REAL*4 R_ICPHA(M_POL_REC,2,MNBAS,0:MICDEG)   ! polynomial coefficients
      REAL*4 R_ICAMP(M_POL_REC,2,MNBAS,0:MICDEG)   ! polynomial coefficients
      REAL*4 R_ICRMS(M_POL_REC,2,MNBAS,2)          ! amp-phase rms
!
! Section -30 Data Section Descriptor
      INTEGER R_NDUMP                    ! Number of records
      INTEGER R_NDATL                    ! number of line datas (1,2)
      INTEGER(KIND=8) R_LDPAR  ! Length of data header (longwords)
      INTEGER(KIND=8) R_LDATC  ! length of continuum data (")
      INTEGER(KIND=8) R_LDATL  ! length of line data      (")
      INTEGER(KIND=8) R_LDUMP  ! length of record         (")
!
! Section -31 Passband Calibration
      INTEGER R_ABPC                     ! ABPC has been stored (2=IF function)
      INTEGER R_ABPCDEG                  ! Degree of interpolating polynomial
      REAL*4  R_ABPCCAMP(MNBB,2,MNANT,MCCH)
      REAL*4  R_ABPCCPHA(MNBB,2,MNANT,MCCH)
      REAL*4  R_ABPCLAMP(MNBB,2,MNANT,MRLBAND,0:MBPCDEG)  ! polynomial coefficients
      REAL*4  R_ABPCLPHA(MNBB,2,MNANT,MRLBAND,0:MBPCDEG)  ! polynomial coefficients
      COMPLEX R_ABPCSBA(MNBB,2,MNANT)         !
      INTEGER R_ABPFDEG                  ! Degree of interpolating polynomial
      REAL*4  R_ABPFLIM(MNBB,2)      ! IF frequency interval (min, max)
      REAL*4  R_ABPFAMP(MNBB,2,MNANT,0:MBPCDEG)     ! polynomial coefficients
      REAL*4  R_ABPFPHA(MNBB,2,MNANT,0:MBPCDEG)     ! polynomial coefficients
!
! Section -32 Antenna-based Instrumental Calibration
      INTEGER R_AIC                      ! IC has been stored
      INTEGER R_AICDEG                   ! Degree of interpolating polynomial
      REAL*4 R_AICPHA(M_POL_REC,2,MNANT,0:MICDEG)  ! polynomial coefficients
      REAL*4 R_AICAMP(M_POL_REC,2,MNANT,0:MICDEG)  ! polynomial coefficients
      REAL*4 R_AICRMS(M_POL_REC,2,MNBAS,2)         ! amp-phase rms
      COMPLEX R_DX(MNANT,MNBB)                     ! polarisation leakage
      COMPLEX R_DY(MNANT,MNBB)                     ! (")
!
! Section -33 Data File (NEW SECTION)
      INTEGER R_DXNUM                    ! Data File index number of data
      CHARACTER*32 R_DFILE               ! Data file name without extension
!
! Section -34 Data Modifier (NEW SECTION)
      INTEGER R_DMAFLAG(MNANT)                   ! Antenna flags (user)
      INTEGER R_DMBFLAG(MNBAS)                   ! Baseline flags (user)
      REAL*4 R_DMATFAC(MNBB,2,MNANT)             ! Applied atmosph. cal factor(U,L)
      REAL*4 R_DMDELAY(MNBB,MNANT)               ! Delay
      REAL*4 R_DMCAMP(2,MNBAS,MCCH)              ! continuum amplitudes
      REAL*4 R_DMCPHA(2,MNBAS,MCCH)              ! continuum phases
      REAL*4 R_DMLAMP(2,MNBAS,MRLBAND)           ! line amplitudes
      REAL*4 R_DMLPHA(2,MNBAS,MRLBAND)           ! line phases
      REAL*4 R_DMLDPH(2,MNBAS,MRLBAND)           ! line phase slopes
      LOGICAL R_DMSAFLAG(2*MBANDS,MNANT)         ! Antenna spectral flags
      LOGICAL R_DMSBFLAG(2*MBANDS,MNBAS)         ! Baseline spectral flags 
!
! Section -35 Water Vapor Radiometer  (NEW SECTION)
! data input
      INTEGER R_WVRNCH(MNANT)            ! number of channels
! (=3; 0 if antenna not equipped)
      REAL R_WVRFREQ(MWVRCH,MNANT)       ! central frequencies (MHz)
      REAL R_WVRBW(MWVRCH,MNANT)         ! bandwidths (MHz)
      REAL R_WVRTAMB(MNANT)              ! Temp ambient load (K)
      REAL R_WVRTPEL(MNANT)              ! Temp Peltier cooler (K)
      REAL R_WVRTCAL(MWVRCH,MNANT)       ! K/count channel gains
      REAL R_WVRREF(MWVRCH,MNANT)        ! average counts on reference observation
      REAL R_WVRAVER(MWVRCH,MNANT)       ! average counts on current observation
      REAL R_WVRAMB(MWVRCH,MNANT)        ! average counts on last ambient meas.
      REAL R_WVRTREC(MWVRCH,MNANT)       ! receiver temperatures (K)
      REAL R_WVRFEFF(MWVRCH,MNANT)       ! coupling factor to sky
      REAL R_WVRLABTCAL(MWVRCH,MNANT)    ! LAB K/COUNT CHANNEL GAINS
      REAL R_WVRLABTREC(MWVRCH,MNANT)    ! LAB TREC
      REAL R_WVRLABTDIO(MWVRCH,MNANT)    ! LAB DIODE TEMPERATURE
!
! model output
      INTEGER R_WVRMODE(MNANT)           ! calibration mode
      REAL R_WVRH2O(MNANT)               ! prec. h2o (mm)
      REAL R_WVRPATH(MNANT)              ! H2O vap. pathlength (mum)
      REAL R_WVRTSYS(MWVRCH,MNANT)       ! system temperature (K)
      REAL R_WVRDPATH(MWVRCH,MNANT)      ! Kelvin to H2O vap. pathlength
! (model, mum/K)
      REAL R_WVRFPATH(MWVRCH,MNANT)      ! Kelvin to H2O vap. pathlength
! (empirical, mum/K)
      REAL R_WVRLIQ(MWVRCH,MNANT)        ! Kelvin to H2O liquid emission (K/K)
      REAL R_WVRDCLOUD(MWVRCH,MNANT)     ! Kelvin to H2O liq. pathlength (mum/K)
      REAL R_WVRTATM(MNANT)              ! temperature of atmosphere (K)
      INTEGER R_WVRQUAL(MNANT)           ! quality code (usable or not)
      INTEGER R_WVRAVERTIME              ! online count averaging time
!
! Section -36 (ALMA- specific parameters)
!
      REAL R_TRANDIST                    ! transmitter distance (m)
      REAL R_TRANFOCU                    ! transmitter refocus  (mm)
      integer r_subscan                  ! subscan number    (not used at Bure)
      integer r_execBlockNum             ! exec block number (not used at Bure)
      character*256 R_ExecBlock          ! the ExecBlockId from the original data set.
      character*32 R_AntennaName(mnant)  ! the antenna names  (32 first chars).
      character*32 R_AntennaType(mnant)  ! the antenna types  (32 first chars).
      real*4 R_DishDiameter(mnant)       ! the dish diameter in meters.
!
! Section -37 Instrument monitoring (PdBI - specific parameters)
!
      REAL R_ATTENUATION_HIQ(MNANT,2)    ! HiQ Attenuations (dB?)
      REAL R_LASER_LEVEL(MNANT,MNIF)        ! Power in laser Rx (?)
      REAL R_WIDEX_TEMPERATURE(4)        ! Widex temperature (C)
!
! Section -38 Status monitoring (NOEMA status tracker)
!
      CHARACTER*12 R_STATUS              ! Interferometer status
      CHARACTER*12 R_SUBSTATUS           ! Interferometer substatus
      CHARACTER*140 R_COMM
!
! Section -39 VLBI
!
       INTEGER R_REFANT                  ! VLBI reference antenna
       INTEGER R_COMPARISON_ANT          ! Comparison antennna (for phasing)
       INTEGER R_NANT_VLBI               ! Number of antenna within mask
       REAL    R_MASER_GPS_DIFF          ! Maser minus GPS
       REAL    R_EFFTSYS(MNBB)           ! Effective system temperature
       INTEGER R_ANTMASK(MNANT)          ! Antenna mask.

!
      INTEGER R_LASTONE, R_LAST_RF_ANT, R_LAST_RF_BAS
      INTEGER ALIGANT_1(MPADANT),ALIGTRI_1(MPADTRI),ALIG_1,ALIG_2
      INTEGER ALIGANT_2(MPADANT),ALIG_3,ALIG_4,ALIG_5
!
! Not in data
      CHARACTER*12 SPWNAME(MRLBAND)      ! SPW name
      REAL*8       SPWFREQ(MRLBAND)      ! SPW center frequency (source)
      REAL*8       SPWTOPO(MRLBAND)      ! SPW center frequency (topo)
      REAL*4       SPWBW(MRLBAND)        ! SPW bandwidth
      INTEGER      SPWPOL(MRLBAND)       ! SPW pol
      INTEGER      SPWSB(MRLBAND)        ! SPW SB
!
!     Common definition for spw
      COMMON /SPW/                                                       &
     &SPWNAME,SPWFREQ,SPWBW,SPWPOL,SPWSB,SPWTOPO
      SAVE /SPW/
!
!     Common definition for header
!      
      COMMON /R_COM/                                                     &
      !------------------------------------------------------------------
      ! Entry number      
      ! Warning: must stay the first element
      !------------------------------------------------------------------
     &R_XNUM,                                                            &
      !
      !------------------------------------------------------------------
      ! -2 General section
      ! Length: MGEN + MGEN2 + 15 = 11 + 12 + 15 (sect. disk, ind. disk, mem.)
      !                           = 38
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_NUM,R_VER,R_DOBS,R_DRED,R_TYPEC,R_KIND,R_QUAL,R_SCAN,            &
     &R_PROJECT,R_TELES,                                                 &    
     &R_UT,R_ST,R_AZ,R_EL,R_TAU,R_TSYS,R_TIME,R_TYPOF,R_PROC,            &
     &R_CDOBS,R_CDRED,R_CPROC,R_CTOBS,R_CQUAL,                           &
      !
      !------------------------------------------------------------------
      !  -3 Positions section
      !  Length MPOS = 18 + MNSTOKES
      !  Length parity: always odd
      !------------------------------------------------------------------
     &R_SOURC,R_EPOCH,R_LAM,R_BET,R_LAMOF,R_BETOF,R_PROJ,R_FOCUS,R_FLUX, &
     &R_SPIDX,R_STOKES_I,R_STOKES_Q,R_STOKES_U,R_STOKES_V,               &
     &R_SPIDX_STOKES,ALIG_3,                                             &
      !  
      !------------------------------------------------------------------
      ! -21 Interferometer General information
      ! Length MINTERC = MINTERC0 + MINTERC_1 * MNANT + MINTERC_2 * MNBAS
      !                           + MINTERC_3 * MNTRI
      !        MINTERC = 7 + 12 * MNANT + 8 * MNBAS + 3 * MNTRI
      ! Oddity depend on MNTRI
      !------------------------------------------------------------------
     &R_NANT,R_NBAS,R_ITYPE,R_HOURA,R_KANT,R_KENT,R_ISTAT,R_IANT,R_JANT, &
     &R_AXES,R_NTRI,R_BAS,R_ANT,R_ATRI,R_PHLO1,R_PHLO3,R_NBAND_WIDEX,    &
     &R_CONFIG, ALIG_5,                                                  &
     &ALIGTRI_1,                                                         &
      !------------------------------------------------------------------
      ! -22 RF Frequency setup 
      ! Length MRFSET = MRFSET0 + MRFSET1 * MNANT
      !        MRFSET = 25+2*MNIF+3*MNBB + (1+MNBB+MNIF+MNSB) * MNANT
      ! Length parity: odd if MNANT even and v.v.
      !------------------------------------------------------------------
     &R_RESTF,R_FLO1,                                                    &
     &R_FLO1_REF,R_FIF1,R_VELOC,R_TYPEV,                                 &
     &R_DOPPL,R_NPOL_REC,R_QUARTER,R_QWPLATE,                            &
     &R_LINE,R_ISB,R_LOCK,R_NIF,R_IFNAME,R_KIF,ALIG_2,                   &
     &R_NSUBPOWER,R_SUBPOWER_VAL,R_SUBPOWER_REF,R_SUBPOWER_INC,          &
     &R_POLSWITCH,                                                       &
     &ALIGANT_1,                                                         &
      !------------------------------------------------------------------
      ! -23 Continuum setup
      ! Length MCONTSET = 59
      ! Length parity: always odd
      !------------------------------------------------------------------
     &R_CRCH,R_CVOFF,R_CVRES,R_CRFOFF,R_CRFRES,                          &
     &R_CNAM,R_CFCEN,R_CFWID,R_SBAND,                                    &
     &R_NSB,R_NBAND,R_NCDAT,                                             &
      !------------------------------------------------------------------
      ! -24 Line setup                                                   
      ! Length MLINESET = MLINESET_0 + MLINESET_1 * MRLBAND
      !        MLINESET = 8 + (37 + 7 * MNANT) * MRLBAND
      ! Length parity: if MRLBAND even, even
      !------------------------------------------------------------------
     &R_LBAND,R_LNTCH,R_LFOUR,R_LNCH,                                    &
     &R_LICH,R_LFCEN,R_LFRES,R_LRCH,                                     &
     &R_LVOFF,R_LVRES,R_LRFOFF,R_LRFRES,                                 &
     &R_LNAM,R_LILEVU,R_LNSB,R_LSBAND,                                   &
     &R_LCENCH,R_LDELOFF,R_LMODE,R_LILEVL,R_IUNIT,R_BAND4,               &
     &     r_phselect,                                                   &
     &R_FLO2,R_BAND2,                                                    &
     &R_FLO2BIS,R_BAND2BIS,R_FLO3,R_FLO4,R_LPOLMODE,R_LPOLENTRY,         &
     &R_IFATTN,R_BB,R_NBB,R_BBNAME,R_MAPBB,R_KBB,R_MAPPOL,R_IF,R_SB,     &
     &R_STACHU,R_NCHU,R_CODE_STOKES,R_NWORD,LBAND_ORIGINAL,              &
     &LNTCH_ORIGINAL,LDATL_ORIGINAL,ALIG_4,                              &
      !------------------------------------------------------------------
      ! -25 Scanning function
      ! Length MSCANNING =  4 + (4 + MPMODEL) * MNANT + MSWITCH * 3
      ! Length parity: always even (if no change in MPMODEL and MSWITCH)
      !------------------------------------------------------------------
     &R_SCATY,R_MOBIL,R_COLLAZ,R_COLLEL,                                 &
     &R_CORFOC,                                                          &
     &r_pmodel, r_npmodel, r_cpmodel,                                    &
     &r_nswitch, r_wswitch, r_pswitch,                                   &
      !------------------------------------------------------------------
      ! -26 Atmospheric parameters
      ! Length MATMPARM = MATMPARM_0 + MATMPARM_1 * MNANT
      !        MATMPARM = 10 + (4 + 20 * MNBB ) * MNANT
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_MODE,R_NREC,R_PAMB,R_TAMB,R_ALTI,                                &
     &R_HUMID,R_H2OMM,R_TATMS,R_TATMI,                                   &
     &R_TAUS,R_TAUI,R_BEEF,R_FEFF,R_TCHOP,                               &
     &R_TCOLD,R_TREC,R_GIM,R_CSKY,R_CCHOP,                               &
     &R_CCOLD,R_TSYSS,R_TSYSI,R_CEFF,                                    &
     &R_CMODE,R_JYKEL,                                                   &
     &R_AVWIND,R_AVWINDIR,R_TOPWIND,R_TOPWINDIR,                         &
     &R_TCABIN,R_TDEWAR,r_totscale,                                      &
      !------------------------------------------------------------------
      ! -27 Atmospheric monitoring receiver
      ! Length MATMON = 5 + 19 * MNANT
      ! Length parity: odd if MNANT even and v.v.
      !------------------------------------------------------------------
     &R_NREC_MON,R_FRS_MON,R_FRI_MON,R_H2O_MON,R_TATMS_MON,              &
     &R_TATMI_MON,R_TAUS_MON,R_TAUI_MON,R_FEFF_MON,                      &
     &R_TCHOP_MON,R_TCOLD_MON,R_TREC_MON,R_GIM_MON,                      &
     &R_CSKY_MON,R_CCHOP_MON,R_CCOLD_MON,R_TSYS_MON,                     &
     &R_CMODE_MON,R_PATH_MON,R_TEM_MON,R_DPATH_MON,R_MAGIC_MON,          &
     &R_OK_MON,                                                          &
     &ALIGANT_2,                                                         &     
      !------------------------------------------------------------------
      ! -29 (Baseline) Instrumental calibration
      ! Length MIC = 2 + 8 * (MICDEG+2) * MNBAS
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_IC,R_ICDEG,R_ICPHA,                                              &
     &R_ICAMP,R_ICRMS,R_DX,R_DY,                                         &
      !------------------------------------------------------------------
      ! -32 Antenna Instrumental calibration
      ! Length MAIC = 2 + 8 * (MICDEG+1) * MNANT + 4 * MNBAS
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_AIC, R_AICDEG, R_AICPHA, R_AICAMP,R_AICRMS,                      &
      !------------------------------------------------------------------
      ! -30 Data section descriptor
      ! Length MDESCR = 10
      ! Length parity: always even
      !------------------------------------------------------------------
     &ALIG_1,                                                            &
     &R_NDUMP,R_NDATL,R_LDPAR,R_LDATC,R_LDATL,R_LDUMP,                   &
      !------------------------------------------------------------------
      ! -33 Data file
      ! Length MDFILE = 32
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_DXNUM, R_DFILE,                                                  &
      !------------------------------------------------------------------
      ! -34 Data modifier
      !------------------------------------------------------------------
     &R_DMAFLAG, R_DMBFLAG, R_DMATFAC, R_DMDELAY,                        &
     &R_DMCAMP,R_DMCPHA,R_DMLAMP,R_DMLPHA,R_DMLDPH,R_DMSAFLAG,           &
     &R_DMSBFLAG,                                                        &
      !------------------------------------------------------------------
      ! -35 Water Vapor Radiometer
      ! Length MWVR = MNANT * (8_NWVRCH*16)
      ! Length parity: always even
      !------------------------------------------------------------------
     &R_WVRNCH, R_WVRFREQ, R_WVRBW, R_WVRTAMB, R_WVRTPEL,R_WVRTCAL,      &
     &R_WVRREF, R_WVRAVER, R_WVRAMB, R_WVRTREC, R_WVRFEFF,               &
     &R_WVRLABTCAL, R_WVRLABTREC, R_WVRLABTDIO,                          &
     &R_WVRMODE, R_WVRH2O, R_WVRPATH, R_WVRTSYS, R_WVRDPATH, R_WVRFPATH, &
     &R_WVRLIQ, R_WVRDCLOUD, R_WVRTATM, R_WVRQUAL,R_WVRAVERTIME,         &
      !------------------------------------------------------------------
      ! -36 ALMA specific parameters
      ! Length MALMA = 68 + MNANT * 17
      ! Length parity: MNANT parity
      !------------------------------------------------------------------
     &     R_TRANDIST, R_TRANFOCU, r_subscan, r_execblocknum,            &
     &     R_ExecBlock, R_AntennaName,                                   &
     &     R_AntennaType, R_DishDiameter,                                &
      !------------------------------------------------------------------
      ! -37 NOEMA monitoring parameters
      ! Length MMONI = 4 + MNANT * 4
      ! Length parity: MNANT parity
      !------------------------------------------------------------------
     & R_ATTENUATION_HIQ, R_LASER_LEVEL, R_WIDEX_TEMPERATURE,            &
      !------------------------------------------------------------------
      ! -38 NOEMA status monitoring 
      ! Length MSTAT = 41
      ! Length parity: odd
      !------------------------------------------------------------------
     & R_STATUS, R_SUBSTATUS, R_COMM,                                    &
      !------------------------------------------------------------------
      ! -39 VLBI
      ! Length MVLBI = 4+MNANT+MNBB
      ! Length parity: MNANT parity
      !------------------------------------------------------------------
     & R_REFANT, R_COMPARISON_ANT, R_NANT_VLBI, R_MASER_GPS_DIFF,        &
     & R_EFFTSYS, R_ANTMASK,                                             &
      !------------------------------------------------------------------
      ! Present sections -- not in data
      !------------------------------------------------------------------
     &     R_PRESEC,                                                     &
      !------------------------------------------------------------------
      ! To compute common length -- not in data
      !------------------------------------------------------------------
      !------------------------------------------------------------------
      ! -31 Antenna Bandpass calibration
      ! Length MAPBC = 5 + 4 * MNBB*MNANT*MCCH
      !                  + 4 * (MBPCDEG+1) * MNANT * MRLBAND * MNBB
      !                  + 4 * MNANT 
      !                  + 4 * (MBPCDEG+1) * MNANT * MNBB
      ! Length parity: always odd
      !------------------------------------------------------------------
     &R_LAST_RF_ANT,                                                     &
     &R_ABPC,R_ABPCDEG,R_ABPCCAMP,R_ABPCCPHA,R_ABPCLAMP,                 &
     &R_ABPCLPHA,R_ABPCSBA,R_ABPFDEG,R_ABPFLIM,R_ABPFAMP,                &
     &R_ABPFPHA,                                                         &
     &R_LAST_RF_BAS,                                                     &
      !------------------------------------------------------------------
      ! -28 (Baseline) Bandpass calibration
      ! Length MBPC = 5 + 4 * MNBB * MNBAS * MCCH
      !                 + 4 * (MBPCDEG+1) * (MRLBAND+1) * MNBB * MNBAS
      !                 + 4 * MNBAS
      ! Always odd
      !------------------------------------------------------------------
     &R_BPC,R_BPCDEG,R_BPCCAMP,R_BPCCPHA,R_BPCLAMP,                      &
     &R_BPCLPHA,R_BPCSBA,R_BPFDEG,R_BPFLIM,R_BPFAMP,                     &
     &R_BPFPHA,                                                          &
     & R_LASTONE
      !
      ! r_lastone must stay the last one
      !
      SAVE /R_COM/
!-------------------------------------------------------------------------
