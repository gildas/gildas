subroutine run_tifits(line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! TIFITS
  !	Main routine
  ! 	Call appropriate subroutine according to COMM
  ! Arguments :
  !	LINE	C*(*)	Command line		Input/Output
  !	COMM	C*(*)	Command name		Input
  !	ERROR	L	Logical error flag	Output
  ! (2003-05-07)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Local
  integer :: mvoc1
  character(len=60) :: mess
  !
  call message(0,1,'TIFITS',line(1:lenc(line)))
  !
  if (comm.eq.'READ') then
    call read_fitsti(line,error)
  elseif (comm.eq.'WRITE') then
    call write_fitsti(line,error)
  else
    mess = comm//' Not implemented'
    call message(6,1,'RUN_TIFITS',mess)
  endif
  return
end subroutine run_tifits
