subroutine write_sdm_state(error)
  !---------------------------------------------------------------------
  ! Write the State SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_State
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (StateRow) :: sRow
  type (StateKey) :: sKey
  character, parameter ::  sdmTable*8='State'
  integer, parameter :: noLoad=0, coldLoad=1, ambientLoad=2
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     what we put here depends on the integration number in CLIC
  !     (dh_dump)
  !
  !     no single dish here
  sRow%sig = .true.
  sRow%ref = .false.
  if (r_scaty.eq.5) then
    sRow%calDeviceName = CalibrationDevice_NONE
    sRow%onSky = .true.
  !$$$         sRow%obsIntent= 'Sky'
  !$$$         sRow%calloadNum = noLoad
  !$$$         sRow%obsMode = 'Astronomy'
  elseif (r_scaty.eq.7) then
    sRow%calDeviceName = CalibrationDevice_COLD_LOAD
    sRow%onSky = .false.
  !$$$         sRow%obsIntent= 'Cold'
  !$$$         sRow%calloadNum = coldLoad
  !$$$         sRow%obsMode = 'TempScaleCalibration'
  elseif (r_scaty.eq.4) then
    sRow%calDeviceName = CalibrationDevice_AMBIENT_LOAD
    sRow%onSky = .false.
  else
    sRow%calDeviceName = CalibrationDevice_NONE
    sRow%onSky = .true.
  endif
  !
  ! should deal with flags...
!!!   sRow%flagRow = .false.
  !
  !     *TBD* I put 0 for the integrations and 1 to N in sub-integrations
  !-- ShSim suubintegNum moved to Main
  !$$$      sRow%subintegNum = dh_dump
  !$$$      if (dh_integ.gt.1) then
  !$$$         sRow%subintegNum = 0
  !$$$      endif
  call addStateRow(sKey, sRow, error)
  if (error) return
  state_id = sKey%stateId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_state
!---------------------------------------------------------------------
subroutine get_sdm_state(error)
  !---------------------------------------------------------------------
  ! Read the State SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_State
  use sdm_Enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (StateRow) :: sRow
  type (StateKey) :: sKey
  character, parameter ::  sdmTable*8='State'
  integer, parameter :: noLoad=0, coldLoad=1, ambientLoad=2
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     what we put here depends on the integration number in CLIC
  !     (dh_dump)
  !
  !     *TBD* I put 0 for the integrations and 1 to N in sub-integrations
  sRow%sig = .false.
  sRow%ref = .false.
  !!  sRow%flagRow = .false.
  sKey%stateId = state_id
  call getStateRow(sKey, sRow, error)
  if (error) return
  if (sRow%calDeviceName.eq.CalibrationDevice_COLD_LOAD) then
    r_scaty = 7
  elseif (sRow%calDeviceName.eq.CalibrationDevice_AMBIENT_LOAD) then
    r_scaty = 4
  endif
  !-- ShSim: subintegNum moved to Main
  !$$$      dh_dump = sRow%subintegNum
  !
  !     no single dish here
  if (sRow%ref) then
    call sdmMessage(2,4,sdmTable ,'Reference state ignored')
  endif
  !
  ! Should deal with this *TBD*
  !$$$      print *, 'dh_dump, sRow%calloadNum ', dh_dump, sRow%calloadNum
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
!
end subroutine get_sdm_state
!---------------------------------------------------------------------
