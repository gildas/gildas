subroutine crsec (scode,len,error)
  use gbl_constant
  use gbl_convert
  use clic_file
  !---------------------------------------------------------------------
  !	Convert section from File data type to System data type
  !---------------------------------------------------------------------
  integer         :: scode                  ! Section number
  integer(kind=8) :: len                    ! Section length
  logical         :: error                  ! Error flag
  ! Global
  external :: r4tor4,r8tor8,i8toi8
  external :: ier4va,eir4va,eir4ie,ier4ei,var4ie,var4ei
  external :: ier8va,eir8va,eir8ie,ier8ei,var8ie,var8ei
  external ::        eii4va,eii4ie,iei4ei       ,vai4ei
  external ::               eii8ie,iei8ei
  !
  if (error) return
  !
  if (i%conv%code.eq.0) then        ! No conversion
    call scrsec (scode,len,error,r4tor4,r8tor8,r4tor4,i8toi8,r4tor4)
  elseif (i%conv%code.eq.vax_to_ieee) then
    call scrsec (scode,len,error,var4ie,var8ie,r4tor4,i8toi8,r4tor4)
  elseif (i%conv%code.eq.ieee_to_vax) then
    call scrsec (scode,len,error,ier4va,ier8va,r4tor4,i8toi8,r4tor4)
  elseif (i%conv%code.eq.vax_to_eeei) then
    call scrsec (scode,len,error,var4ei,var8ei,vai4ei,i8toi8,r4tor4)
  elseif (i%conv%code.eq.ieee_to_eeei) then
    call scrsec (scode,len,error,ier4ei,ier8ei,iei4ei,iei8ei,r4tor4)
  elseif (i%conv%code.eq.eeei_to_vax) then
    call scrsec (scode,len,error,eir4va,eir8va,eii4va,i8toi8,r4tor4)
  elseif (i%conv%code.eq.eeei_to_ieee) then
    call scrsec (scode,len,error,eir4ie,eir8ie,eii4ie,eii8ie,r4tor4)
  endif
end subroutine crsec
!
subroutine scrsec (scode,len,error,r4,r8,i4,i8,cc)
  use gkernel_interfaces
  use gbl_constant
  use gio_params
  use classic_api
  use clic_file, except=>i
  use clic_index
  use clic_virtual
  !---------------------------------------------------------------------
  !	Convert section from File data type to System data type
  !---------------------------------------------------------------------
  integer :: scode                  !
  integer(kind=8) :: len                    !
  logical :: error                  !
  external :: r4                    !
  external :: r8                    !
  external :: i4                    !
  external :: i8
  external :: cc                    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: ichain(64)
  integer :: iwork(mdata), iwork2(mdescr)
  integer,parameter :: mlwork=4
  integer(kind=8) :: lwork(mlwork)
  integer,parameter :: mdwork=max(12,3*mnbas,3*mnant,2*mrlband,mbands)
  real*8 ::  dwork(mdwork)
  integer :: i,j,l,m,n, temp_dim, mask, temp_dim2, ia, ja 
  integer :: polentry, nbc_entry, lntch, ldl
  integer :: dmsaflag(mnant),dmsbflag(mnbas), dummy, nword
  real    :: dmdelay(m_pol_rec,mnant),dmatfac(m_pol_rec,2,mnant)
  real    :: ifattn(2,mnant,mrlband)
  integer(kind=8) :: k
  equivalence (iwork,data_buffer)
  character(len=80) :: chain
  logical :: pol_switched, found
  !
  if (error) return
  !
  ! General Section
  if (scode.eq.gen_sec) then
    !         CALL R8 (IWORK,R_UT,2)
    call r4tor4(iwork,dwork,4)
    call r8 (dwork,r_ut,2)
    call r4 (iwork(5),r_az,5)
    call i4 (iwork(10),r_typof,2)
    new_receivers = .false.
    if (r_kind.eq.5) then
      new_receivers = .true.
      write (chain,'(A,I2)') 'New receiver data kind is ', r_kind
      l = lenc(chain)
    !            CALL MESSAGE(1,1,'ROBS',CHAIN(1:L))
    endif
  !
  ! Position Section
  elseif (scode.eq.pos_sec) then
    call cc (iwork,ichain,3)
    call bytoch(ichain,r_sourc,12) ! R_SOURC = CHAIN
    if (r_kind.ge.3) then
      call r4 (iwork(4),r_epoch,1)
      !            CALL R8 (IWORK(5),R_LAM,2)
      call r4tor4(iwork(5),dwork,4)
      call r8 (dwork,r_lam,2)
      !
      call r4 (iwork(9),r_lamof,2)
      call i4 (iwork(11),r_proj,1)
      call r4 (iwork(12),r_focus,2)
      if (e%version.gt.3) then
        call r4 (iwork(14),r_spidx,1)
      endif
      if (e%version.gt.5) then
        call r4 (iwork(15),r_stokes_i,8)
      endif 
    else
      call message(8,2,'SCRSEC','Old position format')
      call i4 (iwork(4),r_proj,1)
      call r4 (iwork(5),r_lamof,2)
      call r4 (iwork(7),r_epoch,1)
      !            CALL R8 (IWORK(8),R_LAM,2)
      call r4tor4(iwork(8),dwork,4)
      call r8 (dwork,r_lam,2)
    endif
  !
  ! Spectroscopy Section
  !      ELSEIF (SCODE.EQ.SPEC_SEC) THEN
  !         CALL CC (IWORK,ICHAIN,3)
  !         R_SLINE = CHAIN
  !         CALL R8 (IWORK(4),R_SRESTF,1)
  !         CALL I4 (IWORK(6),R_SNCHAN,1)
  !         CALL R4 (IWORK(7),R_SRCHAN,6)
  !         CALL R8 (IWORK(13),R_SIMAGE,1)
  !         CALL I4 (IWORK(15),R_SVTYPE,1)
  !         CALL R8 (IWORK(16),R_SFOBS,1)
  !
  ! Interferometer Setup Section
  elseif (scode.eq.interc_sec) then
    call i4 (iwork,r_nant,3)
    if (r_nant.le.0) then
      call message(8,4,'RINTERC','NANT should be positive')
      error = .true.
      return
    endif
    if (r_nbas.le.0) then
      call message(8,4,'RINTERC','NBAS should be positive')
      error = .true.
      return
    endif
    k = 5
    call r4 (iwork(4),r_houra,1)
    call i4 (iwork(k),r_kant,r_nant)
    k = k + r_nant
    call i4 (iwork(k),r_kent,r_nant)
    k = k + r_nant
    call i4 (iwork(k),r_istat,r_nant)
    k = k + r_nant
    call i4 (iwork(k),r_iant,r_nbas)
    k = k + r_nbas
    call i4 (iwork(k),r_jant,r_nbas)
    k = k + r_nbas
    !         CALL R8 (IWORK(K),R_BAS,3*R_NBAS)
    call r4tor4(iwork(k),dwork,2*3*r_nbas)
    call r8(dwork,r_bas,3*r_nbas)
    k = k + 6*r_nbas
    if (len.gt.k-1) then
      !            CALL R8 (IWORK(K),R_ANT,3*R_NANT)
      call r4tor4(iwork(k),dwork,2*3*r_nant)
      call r8(dwork,r_ant,3*r_nant)
      k = k + 6*r_nant
      call i4 (iwork(k),r_ntri,1)
      k = k + 1
      if (r_ntri.gt.0) then
        call i4 (iwork(k),r_atri,3*r_ntri)
        k = k + 3*r_ntri
      endif
    else
      r_ntri = 0
    endif
    ! Jan 1996 extension
    if (len.gt.k-1) then
      call r4(iwork(k),r_axes,r_nant)
      k = k + r_nant
    else
      do i=1,r_nant
        r_axes(i) = 0
      enddo
    endif
    ! Nov 1996 extension
    if (len.gt.k-1) then
      call r4(iwork(k),r_phlo1,r_nant)
      k = k + r_nant
      call r4(iwork(k),r_phlo3,r_nant)
      k = k + r_nant
    else
      do i=1,r_nant
        r_phlo1(i) = 0
        r_phlo3(i) = 0
      enddo
    endif
    if (len.gt.k-1) then
      call i4 (iwork(k),r_nband_widex,1)
      k = k + 1
    else
      r_nband_widex = 0
    endif
  !
  ! RF Setup Section
  elseif (scode.eq.rfset_sec) then
    call cc (iwork,ichain,3)
    call bytoch(ichain,r_line,12)  ! R_LINE = CHAIN
    call i4 (iwork(4),r_isb,2)
    if (r_isb.ne.-1)  r_isb = 1    ! Patch bad files
    if (r_lock.ne.-1) r_lock = 1   ! Patch bad files
    !        CALL R8 (IWORK(6),R_RESTF,4)
    call r4tor4(iwork(6),dwork,8)
    call r8 (dwork,r_restf,4)
    call r4 (iwork(14),r_veloc,1)
    call i4 (iwork(15),r_typev,1)
    call r4 (iwork(16),r_doppl,1)
    k = 17
    if (fake_ngrx) then
      r_fif1 = fake_fif1
    endif
    if (r_kind.eq.5) then
      call i4(iwork(k),r_npol_rec,1)
      k = k + 1
      if (e%version.le.3) then
        call i4(iwork(k),r_quarter,2)
        k = k + 2
      endif
      call i4(iwork(k),r_qwplate,r_nant)
      k = k + r_nant
    !            DO I =1, R_NPOL_REC
    !               CALL I4(IWORK(K),R_ATTCOM(1,I),R_NANT)
    !               K = K + R_NANT
    !            ENDDO
    else
      r_npol_rec = 1
    endif
    if (e%version.le.3) then
      r_nif = r_npol_rec
      r_ifname(1) = "H"
      r_kif(1) = 1
      if (r_nif.eq.2) then
        r_ifname(2) = "V"
        r_kif(2)    = 2
      endif
      !
      ! with version < 4, basebands are decoded in the line section
      ! as attenuations.
    else
      call i4(iwork(k),r_nif,1)
      k = k + 1
      do i=1, r_nif
        call cc(iwork(k),ichain,1)
        call bytoch(ichain,r_ifname(i),4)
        k = k + 1     
      enddo
      call i4(iwork(k),r_kif,r_nif)
      k = k + r_nif
      do i = 1, r_nif
        call r4(iwork(k),r_ifattn(1,i),r_nant)
        k = k + r_nant
      enddo
      call i4(iwork(k),r_nbb,1)
      k = k + 1
      do i=1, r_nbb
        call cc(iwork(k),ichain,1)
        call bytoch(ichain,r_bbname(i),4)
        k = k + 1
      enddo
      call i4(iwork(k),r_kbb,r_nbb)
      k = k + r_nbb
      call i4(iwork(k),r_mapbb,r_nbb)
      k = k + r_nbb
      do i = 1, r_nbb
        call i4(iwork(k),r_mappol(1,i),r_nant)
        k = k + r_nant
      enddo
      call i4(iwork(k),r_nsubpower,1)
      k = k + 1
      if (r_nsubpower.ne.0) then
        call r4(iwork(k),r_subpower_val,1)
        k = k + 1
        call i4(iwork(k),r_subpower_ref,1)
        k = k + 1
        call r4(iwork(k),r_subpower_inc,1)
        k = k + 1
      endif
      if (e%version.ge.5) then
        do i = 1, mnsb
          call i4 (iwork(k),r_polswitch(1,i),r_nant)
          k = k + r_nant
        enddo
      endif
    endif
  !
  ! Continuum Setup Section
  elseif (scode.eq.contset_sec) then
    call i4 (iwork,r_nsb,3)
    call r4 (iwork(4),r_crch,6)
    !         CALL R8 (IWORK(10),R_CRFOFF,2)
    call r4tor4(iwork(10),dwork,4)
    call r8 (dwork,r_crfoff,2)
    call r4 (iwork(14),r_crfres,2)
    call cc (iwork(16),ichain,3)
    call bytoch(ichain,r_cnam(1),12)   ! R_CNAM(1) = CHAIN
    call cc (iwork(19),ichain,3)
    call bytoch(ichain,r_cnam(2),12)   ! R_CNAM(2) = CHAIN
    !         CALL R8 (IWORK(22),R_CFCEN,MCCH)
    if (e%version.le.3) then
      if (r_nband_widex.eq.0) then
        temp_dim = 10
      else
        temp_dim = 12
      endif
    else 
      temp_dim = r_nband
    endif
    call r4tor4(iwork(22),dwork,2*temp_dim)
    call r8 (dwork,r_cfcen,temp_dim)
    call r4 (iwork(22+2*temp_dim),r_cfwid,temp_dim)
    if (e%version.le.3) then
      call i4 (iwork(22+3*temp_dim),r_sband,2)
      do i=2,r_nband
        r_sband(1,i) = r_sband(1,1) 
        r_sband(2,i) = r_sband(2,1) 
      enddo 
    else
      k = 22+3*temp_dim
      do i=1, r_nband
        call i4(iwork(k),r_sband(1,i),2)
        k = k + 2
      enddo
    endif
  !
  ! Line Setup Section
  elseif (scode.eq.lineset_sec) then
    call i4 (iwork,r_lband,3)
    k = 4
    r_nword=int(r_lband/32)+1
    lband_original = r_lband
    lntch_original = r_lntch
    if (r_lband.gt.0) then
      call i4 (iwork(k),r_lnch,r_lband)
      k = k + r_lband
      call i4 (iwork(k),r_lich,r_lband)
      k = k + r_lband
      !            CALL R8 (IWORK(K),R_LFCEN,R_LBAND)
      call r4tor4(iwork(k),dwork,2*r_lband)
      call r8 (dwork,r_lfcen,r_lband)
      k = k + 2*r_lband
      call r4 (iwork(k),r_lfres,r_lband)
      k = k + r_lband
      call r4 (iwork(k),r_lrch,2*r_lband)
      k = k + 2*r_lband
      call r4 (iwork(k),r_lvoff,2*r_lband)
      k = k + 2*r_lband
      call r4 (iwork(k),r_lvres,2*r_lband)
      k = k + 2*r_lband
      !            CALL R8 (IWORK(K),R_LRFOFF,2*R_LBAND)
      call r4tor4 (iwork(k),dwork,2*2*r_lband)
      call r8 (dwork,r_lrfoff,2*r_lband)
      k = k + 4*r_lband
      call r4 (iwork(k),r_lrfres,2*r_lband)
      k = k + 2*r_lband
      do i=1,r_lband
        call cc(iwork(k),ichain,3)
        call bytoch(ichain,r_lnam(1,i),12) ! R_LNAM(1,I) = CHAIN
        k = k+3
        call cc(iwork(k),ichain,3)
        call bytoch(ichain,r_lnam(2,i),12) ! R_LNAM(2,I) = CHAIN
        k = k+3
      enddo
      do i=1, r_nant
        call i4 (iwork(k),r_lilevu(1,i),r_lband)
        k = k + r_lband
      enddo
    endif
    if (e%version.le.3) then
      call i4 (iwork(k),r_lnsb,4)
      k = k + 4
      !
      do i=2, r_lband
        r_lsband(1,i) = r_lsband(1,1) 
        r_lsband(2,i) = r_lsband(2,1) 
     enddo 
    else
      call i4(iwork(k),r_lnsb,1)
      k = k +1
      do i = 1, r_lband
        call i4(iwork(k),r_lsband(1,i),r_lnsb)
        k = k + r_lnsb
      enddo
    endif
    !
    if (len.gt.k-1) then
      call r4(iwork(k),r_lcench,r_lband)
      k = k + r_lband
      do i=1, r_nant
        call r4 (iwork(k),r_ldeloff(1,i),r_lband)
        k = k + r_lband
      enddo
      call i4(iwork(k),r_lmode,1)
      ! add r_lilevl and r_iunit 15-sep-1992 RL
      k = k + 1
      if (len.gt.k-1) then
        do i=1, r_nant
          call i4 (iwork(k),r_lilevl(1,i),r_lband)
          k = k + r_lband
        enddo
        call i4 (iwork(k),r_iunit(1),r_lband)
        k = k + r_lband
      endif
      if (len.gt.k-1) then
        call i4 (iwork(k),r_band4,r_lband)
        k = k + r_lband
      else
        do i=1, r_lband
          if (abs(r_lfres(i)).gt. 0.1) then
            r_band4(i)  = 2
          else
            r_band4(i) = 1
          endif
        enddo
      endif
    else
      r_lmode = 0
      do i=1, r_lband
        r_lcench(i) = 1.0
      enddo
    endif
    ! add phase correction origin (2004-06-18)
    if (len.gt.k-1) then
      r_phselect = 0
      do i=1, r_lband
        call i4 (iwork(k), r_phselect(1,i), r_nant)
        k = k + r_nant
      enddo
    ! default to 1mm continuum
    else
      r_phselect = 0
      do i=1, r_lband
        do j=1, r_nant
          r_phselect(j,i) = 1
        enddo
      enddo
    endif
    ! parameters for the new generation receivers
    if (r_kind.eq.5) then
      call r4tor4 (iwork(k),dwork,2*r_lband)
      k = k + 2*r_lband
      call r8(dwork,r_flo2,r_lband)
      if (e%version.gt.3) then
        call r4tor4 (iwork(k),r_band2,r_lband)
        k = k + r_lband
      else
        do i=1, r_lband
          r_band2(i) = -1
        enddo
      endif
      call r4tor4 (iwork(k),dwork,2*r_lband)
      k = k + 2*r_lband
      call r8(dwork,r_flo2bis,r_lband)
      call i4(iwork(k),r_band2bis,r_lband)
      k = k + r_lband
      call r4tor4 (iwork(k),dwork,2*r_lband)
      call r8(dwork,r_flo3,r_lband)
      k = k + 2*r_lband
      call r4tor4 (iwork(k),dwork,2*r_lband)
      call r8(dwork,r_flo4,r_lband)
      k = k + 2*r_lband
      call i4(iwork(k),r_lpolmode,r_lband)
      k = k + r_lband
      do i = 1, r_lband
        call i4 (iwork(k), r_lpolentry(1,i),r_nant)
        k = k + r_nant
      enddo
      !
      ! For later version, this is read in RF section
      if (e%version.le.3) then
        do i = 1, r_lband
          call r4 (iwork(k),ifattn,2*r_nant)
          k = k + 2*r_nant
        enddo
        do i = 1, r_nant
          do j = 1, r_nif
            call r4 (ifattn(j,i,1),r_ifattn(i,j),1)
          enddo
        enddo
      endif
      !
      if (e%version.gt.3) then
        call i4(iwork(k),r_bb,r_lband)
        k = k + r_lband
        call i4(iwork(k),r_if,r_lband)
        k = k + r_lband
        call i4(iwork(k),r_sb,r_lband)
        k = k + r_lband
        call i4(iwork(k),r_stachu,r_lband)
        k = k + r_lband
        call i4(iwork(k),r_nchu,r_lband)
        k = k + r_lband
      else 
        ! --- Not in data with v<4,  need line section to be set
        !
        ! Find out to what correlator entry units are connected
        !
        pol_switched = .false.
        do i =1, r_lband
           ! Check polarization status on Widex 1 ant 1 
           ! (connected to H (=1) if not switched)
           if (r_iunit(i).eq.9) then
              if (r_lpolentry(1,i).eq.2)  pol_switched = .true.
           endif
        enddo
        !
        ! Now determines entry from ant 1 
        do i = 1, r_lband
           if (pol_switched) then
             polentry = 3-r_lpolentry(1,i)
           else
             polentry = r_lpolentry(1,i)
           endif
           r_bb(i) = nbc_entry(i,r_iunit(i),r_flo2(i), &
              r_flo2bis(i),1,polentry)
           r_if(i) = polentry
           r_sb(i) = r_isb
        enddo
        do i = 1, mnbb
           r_mapbb(i) = 0
        enddo
        r_nbb = 1
        r_mapbb(1) = r_bb(1)
        do l = 1, r_nant
           r_mappol(l,1) = r_lpolentry(l,1)
        enddo
        do i = 2, r_lband
           found = .true.
           do j =1, r_nbb
              if (r_bb(i).eq.r_mapbb(j)) found = .false.
           enddo
           if (found) then
              r_nbb = r_nbb + 1
              r_mapbb(r_nbb) = r_bb(i)
              do l = 1, r_nant
                 r_mappol(l,r_nbb) = r_lpolentry(l,i)
              enddo
           endif
        enddo
        do i=1, r_nbb
          if (r_mapbb(i).eq.1) r_bbname(i)="N1"
          if (r_mapbb(i).eq.2) r_bbname(i)="N2"
          if (r_mapbb(i).eq.3) r_bbname(i)="W1"
          if (r_mapbb(i).eq.4) r_bbname(i)="W2"
          if (r_mapbb(i).eq.5) r_bbname(i)="W3"
          if (r_mapbb(i).eq.6) r_bbname(i)="W4"
        enddo
      endif
    elseif (fake_ngrx) then
      do i=1, r_lband
        r_flo2(i) = fake_flo2(i)
        r_flo2bis(i) = fake_flo2bis(i)
        r_band2bis(i) = fake_band2bis(i)
        r_lpolmode(i) = fake_lpolmode(i)
        do j=1, r_nant
          r_lpolentry(j,i) = fake_lpolentry(j,i)
        enddo
      enddo
    else
      !
      ! Filling default for "ogr"
      do i = 1, r_lband
         r_flo2(i) =  r_flo1_ref      ! r_flo1_ref = r_flo2 for ogr
         r_flo2bis(i) = 0.0           ! no band2bis conversion
         r_band2bis(i) = 1            ! equivalent USB
         r_band2(i) = -1              ! was LSB for ogr
         r_flo3(i) = 0.0              ! unknown, filled to 0
         r_flo4(i) = 0.0              ! unkwown, filled to 0
         r_bb(i) = 1                  ! default
         r_if(i) = 1
         r_lpolmode(i) = 1            ! default
         do l = 1, r_nant
            r_lpolentry(l,i) = 1
         enddo
      enddo
      r_nbb = 1
      r_nif = 1
      r_mapbb(1) = 1
      do l = 1, r_nant
         r_mappol(l,1) = 1
      enddo
      do i = 2, mnbb
         r_mapbb(i) = 0
      enddo
    endif
    !
    ! Observation version 6
    if (e%version.ge.6) then
      do i=1, r_lband
        call i4(iwork(k),r_code_stokes(1,i),r_nbas)
        k = k + r_nbas
      enddo
    else 
      do j=1, r_nbas
        ia = r_iant(j)
        ja = r_jant(j)
        do i=1, r_lband
          if ((r_lpolentry(ia,i).eq.code_polar_h).and. &
              (r_lpolentry(ja,i).eq.code_polar_h)) then
            r_code_stokes(j,i) = code_stokes_hh
          endif
          if ((r_lpolentry(ia,i).eq.code_polar_h).and. &
              (r_lpolentry(ja,i).eq.code_polar_v)) then
            r_code_stokes(j,i) = code_stokes_hv
          endif
          if ((r_lpolentry(ia,i).eq.code_polar_v).and. &
              (r_lpolentry(ja,i).eq.code_polar_h)) then
            r_code_stokes(j,i) = code_stokes_vh
          endif
          if ((r_lpolentry(ia,i).eq.code_polar_v).and. &
              (r_lpolentry(ja,i).eq.code_polar_v)) then
            r_code_stokes(j,i) = code_stokes_vv
          endif
        enddo
      enddo
    endif
    !
    ! Now fill in SPW structure
    do i = 1, r_lband
      spwname(i) = r_lnam(1,i)
      spwtopo(i) = r_flo1+r_sb(i)*    &
                (r_flo2(i)+r_band2(i)*(r_flo2bis(i)+r_band2bis(i)*r_lfcen(i)))
      spwbw(i) = r_lnch(i)*r_lfres(i)
      spwpol(i) = r_lpolentry(1,i)
      spwsb(i)  = r_sb(i)
    enddo
    spwfreq = spwtopo*(1-r_doppl)
    !
    ! Lowres mode
    if (lowres.and.r_presec(file_sec).and.r_lmode.eq.1) then
      r_lband = r_nbb
      r_lntch = 0
      do i=1, r_nbb
        r_lntch = r_lntch+r_lnch(i)
      enddo
    endif
  !
  ! Scanning Section
  elseif (scode.eq.scanning_sec) then
    call i4 (iwork,r_scaty,r_nant+1)
    k = 2 + r_nant
    call r4 (iwork(k),r_collaz,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_collel,r_nant)
    k = k + r_nant
    ! March 1996 extension
    if (len.gt.k-1) then
      call r4 (iwork(k),r_corfoc,r_nant)
      k = k + r_nant
    else
      do i=1, r_nant
        r_corfoc(i) = 0
      enddo
    endif
    ! November 2003 extension
    if (len.gt.k-1) then
      call i4(iwork(k),r_pmodel,2) ! r_pmodel and r_npmodel
      k = k+2
      do i=1, r_nant
        call i4 (iwork(k),r_cpmodel(1,i),r_npmodel)
        k = k + r_npmodel
      enddo
      call i4(iwork(k),r_nswitch,1)
      k = k + 1
      if (r_nswitch.gt.0) then
        call r4(iwork(k),r_wswitch,r_nswitch)
        k = k + r_nswitch
        call r4(iwork(k),r_pswitch,2*r_nswitch)
        k = k + 2*r_nswitch
      endif
    else
      r_pmodel = pmodel_none
      r_npmodel = 1
      do i=1, mnant
        r_cpmodel(1,i) = 0
      enddo
      r_nswitch = 0
    endif
  !
  ! Atmospheric Calibration Section
  elseif (scode.eq.atparm_sec) then
    call i4 (iwork,r_mode,2)   ! CC??
    ! by safety, for older observations.
    r_nrec = min(mnrec,max(1,r_nrec))
    call r4 (iwork(3),r_pamb,4)
    k = 7
    ! Change of atmospheric calibration on 13-feb-2007
    if (e%version.le.3) then
      if (r_dobs.lt.(-6406)) then
        temp_dim = r_npol_rec 
      else
        temp_dim = mnbcinput + r_nband_widex
      endif
    else
      temp_dim = r_nbb
    endif
    do i=1, r_nant
      call r4 (iwork(k),r_h2omm(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tatms(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tatmi(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_taus(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_taui(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_beef(1,i),temp_dim)
      k = k+temp_dim
    enddo
    ! New generation receiver ipb files had twice feff before 21 Mar 2007
    if ((r_kind.eq.5).and.(r_dobs.lt.-6369)) then
      do i=1, r_nant
        call r4 (iwork(k),r_feff(1,i),temp_dim)
        k = k+temp_dim
      enddo
    endif
    do i=1, r_nant
      call r4 (iwork(k),r_feff(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tchop(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tcold(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_trec(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_gim(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_csky(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_cchop(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_ccold(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tsyss(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_tsysi(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call r4 (iwork(k),r_ceff(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      call cc (iwork(k),r_cmode(1,i),temp_dim)
      k = k+temp_dim
    enddo
    do i=1, r_nant
      if (r_nband_widex.ne.0.or.e%version.gt.3) then
        call r4 (iwork(k),r_jykel(1,i),temp_dim)
        k = k+temp_dim
      else
        call r4 (iwork(k),r_jykel(1,i),r_npol_rec)
        k = k+r_npol_rec
      endif
    enddo
    ! 21-Aug-1997 extension
    if (len.gt.k-1) then
      call r4 (iwork(k),r_avwind,4)
      k = k + 4
    else
      r_avwind = 0
      r_avwindir = 0
      r_topwind = 0
      r_topwindir = 0
    endif
    ! 24-jan-1999 extension
    if (len.gt.k-1) then
      call r4 (iwork(k),r_tcabin,r_nant)
      k = k+r_nant
      call r4 (iwork(k),r_tdewar,3*r_nant)
      k = k+3*r_nant
    else
      do i=1, r_nant
        r_tcabin(i) = 0
        do j=1, 3
          r_tdewar(j,i) = 0
        enddo
      enddo
    endif
    ! 01-dec-2006 extension
    if (len.gt.k-1) then
      ! Change of atmospheric calibration on 13-feb-2007
      if (e%version.le.3) then
        if (r_dobs.lt.(-6406)) then
          temp_dim = r_npol_rec
        else
          temp_dim = mnbcinput + r_nband_widex
        endif
      else
        temp_dim = r_nbb
      endif
      do i=1, r_nant
        call r4 (iwork(k),r_totscale(1,i),temp_dim)
        k = k+temp_dim
      enddo
    else
      do i=1, r_nant
        r_totscale(1,i) = 1
        r_totscale(2,i) = 1
      enddo
    endif
  !
  ! Atmospheric Monitor Section
  elseif (scode.eq.atmon_sec) then
    call i4 (iwork,r_nrec_mon,1)
    call r4 (iwork(2),r_frs_mon,2)
    k = 4
    call r4 (iwork(k),r_h2o_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tatms_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tatmi_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_taus_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_taui_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_feff_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tchop_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tcold_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_trec_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_gim_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_csky_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_cchop_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_ccold_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tsys_mon,r_nant)
    k = k + r_nant
    call cc (iwork(k),r_cmode_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_path_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_tem_mon,r_nant)
    k = k + r_nant
    call r4 (iwork(k),r_dpath_mon,r_nant)
    k = k + r_nant
    ! Aug-1997 extension (SG)
    if (len.gt.k-1) then
      call r4 (iwork(k),r_magic_mon,2)
      k = k + 2
    else
      r_magic_mon(1) = 1.0     ! Empirical scale factor used in real-time
      r_magic_mon(2) = 1.0     !                             a posteriori
    endif
    ! 12-jan-1999 extension
    if (len.gt.k-1) then
      !            CALL CC (IWORK(K),R_OK_MON,R_NANT)
      call i4 (iwork(k),r_ok_mon,r_nant)
      k = k + r_nant
    else
      do i=1, mnant
        r_ok_mon(i) = r_nrec_mon.gt.0
      enddo
    endif
  !
  ! BandPass Calibration Section
  elseif (scode.eq.bpcal_sec) then
    temp_dim = 1
    if (e%version.le.3) then
      if (new_receivers) then
        temp_dim = mnbcinput + r_nband_widex
      endif
      if (r_nband_widex.eq.0) then
        temp_dim2 = 10
      else
        temp_dim2 = 12
      endif
    else
      temp_dim = r_nbb
      temp_dim2 = r_nband
    endif
    call i4 (iwork,r_bpc,2)
    k = 3
    do i=1,temp_dim2
      do j=1,r_nbas
        do l=1,2
          do m=1,temp_dim
            call r4 (iwork(k),r_bpccamp(m,l,j,i),1)
            k = k + 1
          enddo
        enddo
      enddo
    enddo
    do i=1,temp_dim2
      do j=1,r_nbas
        do l=1,2
          do m=1,temp_dim
            call r4 (iwork(k),r_bpccpha(m,l,j,i),1)
            k = k+1
          enddo
        enddo
      enddo
    enddo
    do i=0,r_bpcdeg
      do j=1,lband_original
        do l=1,r_nbas
          do m=1,2
            do n=1,temp_dim
              call r4 (iwork(k),r_bpclamp(n,m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do i=0,r_bpcdeg
      do j=1,lband_original
        do l=1,r_nbas
          do m=1,2
            do n=1,temp_dim
              call r4(iwork(k),r_bpclpha(n,m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    if (len.gt.k-1) then
      do i=1,r_nbas
	do j = 1, 2
	  do l =1, temp_dim
	    call r4(iwork(k),r_bpcsba(l,j,i),2) ! complex
	    k = k + 2  
	  enddo
	enddo
      enddo
    else	
      do i=1,2
        do j=1,r_nbas
	  do l =1, temp_dim
            r_bpcsba(l,i,j) = 1.
          enddo
        enddo
      enddo
    endif 
    if (len.gt.k-1) then
      call i4 (iwork(k),r_bpfdeg,1)
      k = k+1
      do i=1,temp_dim
        call r4 (iwork(k),r_bpflim(i,1),1)
        k = k+1
        call r4 (iwork(k),r_bpflim(i,2),1)
        k = k+1
      enddo
      do i=0,r_bpfdeg
        do j=1,r_nbas
          do l=1,2
            do m=1,temp_dim
              call r4 (iwork(k),r_bpfamp(m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
      do i=0,r_bpfdeg
        do j=1,r_nbas
          do l=1,2
            do m=1,temp_dim
              call r4 (iwork(k),r_bpfpha(m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    else
      r_bpfdeg = -1
    endif
  !
  ! Antenna BandPass Calibration Section
  elseif (scode.eq.abpcal_sec) then
    temp_dim = 1
    if (e%version.le.3) then
      if (new_receivers) then
        temp_dim = mnbcinput + r_nband_widex
      endif
      if (r_nband_widex.eq.0) then
        temp_dim2 = 10
      else
        temp_dim2 = 12
      endif
    else
      temp_dim = r_nbb
      temp_dim2 = r_nband
    endif
    call i4 (iwork,r_abpc,2)
    k = 3
    do i=1,temp_dim2
      do j=1,r_nant
        do l=1,2
          do m=1,temp_dim
            call r4 (iwork(k),r_abpccamp(m,l,j,i),1)
            k = k + 1
          enddo
        enddo
      enddo
    enddo
    do i=1,temp_dim2
      do j=1,r_nant
        do l=1,2
          do m=1,temp_dim
            call r4 (iwork(k),r_abpccpha(m,l,j,i),1)
            k = k+1
          enddo
        enddo
      enddo
    enddo
    do i=0,r_abpcdeg
      do j=1,lband_original
        do l=1,r_nant
          do m=1,2
            do n=1,temp_dim
              call r4 (iwork(k),r_abpclamp(n,m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do i=0,r_abpcdeg
      do j=1,lband_original
        do l=1,r_nant
          do m=1,2
            do n=1,temp_dim
              call r4(iwork(k),r_abpclpha(n,m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    if (len.gt.k-1) then
      do i=1,r_nant
	do j = 1, 2
	  do l =1, temp_dim
	    call r4(iwork(k),r_abpcsba(l,j,i),2) ! complex
	    k = k + 2  
	  enddo
	enddo
      enddo
    else	
      do i=1,2
        do j=1,r_nant
	  do l =1, temp_dim
            r_abpcsba(l,i,j) = 1.
          enddo
        enddo
      enddo
    endif 
    if (len.gt.k-1) then
      call i4 (iwork(k),r_abpfdeg,1)
      k = k+1
      do i=1,temp_dim
        call r4 (iwork(k),r_abpflim(i,1),1)
        k = k+1
        call r4 (iwork(k),r_abpflim(i,2),1)
        k = k+1
      enddo
      do i=0,r_abpfdeg
        do j=1,r_nant
          do l=1,2
            do m=1,temp_dim
              call r4 (iwork(k),r_abpfamp(m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
      do i=0,r_abpfdeg
        do j=1,r_nant
          do l=1,2
            do m=1,temp_dim
              call r4 (iwork(k),r_abpfpha(m,l,j,i),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    else
      r_abpfdeg = -1
    endif
  !
  ! "Instrumental" Calibration Section
 elseif (scode.eq.ical_sec) then
    call i4 (iwork(1),r_ic,2)
    k = 3
    if (new_receivers) then
       do i=1,r_npol_rec
          do j=1,2
             do l=1,r_nbas
                do m=0,r_icdeg
                   call r4 (iwork(k),r_icamp(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_icdeg
          do l=1,r_nbas
             do j=1,2
                do i=1,r_npol_rec
                   call r4 (iwork(k),r_icamp(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do i=1,r_npol_rec
          do j=1,2
             do l=1,r_nbas
                do m=0,r_icdeg
                   call r4 (iwork(k),r_icpha(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_icdeg
          do l=1,r_nbas
             do j=1,2
                do i=1,r_npol_rec
                   call r4 (iwork(k),r_icpha(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (len.gt.k-1) then
       if (new_receivers) then
          do i=1,r_npol_rec
             do j=1,2
                do l=1,r_nbas
                   call r4(iwork(k),r_icrms(i,j,l,1),1)
                   k = k + 1
                   call r4(iwork(k),r_icrms(i,j,l,2),1)
                   k = k + 1
                enddo
             enddo
          enddo
       else
          do l=1,r_nbas
             do j=1,2
                do i=1,r_npol_rec
                   call r4(iwork(k),r_icrms(i,j,l,1),1)
                   k = k + 1
                   call r4(iwork(k),r_icrms(i,j,l,2),1)
                   k = k + 1
                enddo
             enddo
          enddo
       endif
    endif
  !
  ! Antenna "Instrumental" Calibration Section
 elseif (scode.eq.aical_sec) then
    call i4 (iwork(1),r_aic,2)
    k = 3
    if (new_receivers) then
       do i=1,r_npol_rec
          do j=1,2
             do l=1,r_nant
                do m=0,r_aicdeg
                   call r4 (iwork(k),r_aicamp(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_aicdeg
          do l=1,r_nant
             do j=1,2
                do i=1,r_npol_rec
                   call r4 (iwork(k),r_aicamp(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do i=1,r_npol_rec
          do j=1,2
             do l=1,r_nant
                do m=0,r_aicdeg
                   call r4 (iwork(k),r_aicpha(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_aicdeg
          do l=1,r_nant
             do j=1,2
                do i=1,r_npol_rec
                   call r4 (iwork(k),r_aicpha(i,j,l,m),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (len.gt.k-1) then
       if (new_receivers) then
          do i=1,r_npol_rec
             do j=1,2
                do l=1,r_nant
                   call r4(iwork(k),r_aicrms(i,j,l,1),1)
                   k = k + 1
                   call r4(iwork(k),r_aicrms(i,j,l,2),1)
                   k = k + 1
                enddo
             enddo
          enddo
       else
          do l=1,r_nant
             do j=1,2
                do i=1,r_npol_rec
                   call r4(iwork(k),r_aicrms(i,j,l,1),1)
                   k = k + 1
                   call r4(iwork(k),r_aicrms(i,j,l,2),1)
                   k = k + 1
                enddo
             enddo
          enddo
       endif
    endif
    if (e%version.ge.5) then
      do i=1,r_nbb
        call r4(iwork(k),r_dx(1,i),2*r_nant)
        k = k + 2*r_nant
      enddo
      do i=1,r_nbb
        call r4(iwork(k),r_dy(1,i),2*r_nant)
        k = k + 2*r_nant
      enddo
    endif
  !
  ! Data Descriptor Section
  elseif (scode.eq.descr_sec) then
    if (e%version.eq.1) then
      call i4 (iwork,iwork2,len)        ! I*4 and in that order
      r_ndump = iwork2(1)               ! R_NDUMP
      r_ldpar = int(iwork2(2),kind=8)   ! R_LDPAR
      r_ldatc = int(iwork2(3),kind=8)   ! R_LDATC
      r_ldatl = int(iwork2(4),kind=8)   ! R_LDATL
      r_ldump = int(iwork2(5),kind=8)   ! R_LDUMP
      r_ndatl = iwork2(6)               ! R_NDATL
      ! the default is r_ndatl=0, only uncorrected spectral data present.
      if (len.lt.6) r_ndatl=0
   elseif(e%version.ge.2) then
      call i4(iwork,r_ndump,2)
      call w4tow4(iwork(3),lwork,8)
      call i8(lwork,r_ldpar,4)
   endif
   ldatl_original = r_ldatl
   if (lowres.and.r_lmode.eq.1.and.r_presec(file_sec)) then
     lntch = 0
     do j=1, r_nbb
       lntch = lntch+r_lnch(j)
     enddo
     ldl = 2*lntch*r_lnsb
     r_ldatl = ldl*r_nbas
  endif
  !
  ! Data modifier section
  elseif (scode.eq.modify_sec) then
    call r4(iwork,r_dmaflag,r_nant)
    k = 1+r_nant
    call r4(iwork(k),r_dmbflag,r_nbas)
    k = k+r_nbas
    ! r_dmatfac
    if (e%version.le.3) then
      ! r_dmatfac
      do i=1, r_nant
        do j=1, 2
          call r4 (iwork(k),dmatfac(1,j,i),r_npol_rec)
          k = k + r_npol_rec
          do l=1, r_nbb
            m = r_mapbb(l)
            n = r_mappol(i,l)        
            r_dmatfac(m,j,i) = dmatfac(n,j,i)
          enddo 
        enddo
      enddo
    else
      do i=1, r_nant
        do j=1, 2
          call r4(iwork(k),r_dmatfac(1,j,i),r_nbb)
          k = k + r_nbb
        enddo     
      enddo
    endif
    ! r_dmdelay
    if (e%version.le.3) then
      do i=1, r_nant
        call r4 (iwork(k),dmdelay(1,i),r_npol_rec)
        k = k + r_npol_rec
      enddo
      ! Fill it to v4
      do i=1, r_nant
        do j=1, r_nbb
          m = r_mappol(i,j)
          l = r_mapbb(j)
          r_dmdelay(l,i) = dmdelay(m,i)
        enddo
      enddo
    else
      do i=1, r_nant
        call r4(iwork(k),r_dmdelay(1,i),r_nbb)
        k = k + r_nbb
      enddo
    endif
    !
    do i=1, r_nband
      call r4(iwork(k),r_dmcamp(1,1,i),2*r_nbas)
      k = k+2*r_nbas
      call r4(iwork(k),r_dmcpha(1,1,i),2*r_nbas)
      k = k+2*r_nbas
    enddo
    do i=1, lband_original
      call r4(iwork(k),r_dmlamp(1,1,i),2*r_nbas)
      k = k+2*r_nbas
      call r4(iwork(k),r_dmlpha(1,1,i),2*r_nbas)
      k = k+2*r_nbas
      call r4(iwork(k),r_dmldph(1,1,i),2*r_nbas)
      k = k+2*r_nbas
    enddo
    ! Apr-2010 extension
    if (e%version.le.3) then
      if (len.gt.k-1.and.r_nband_widex.ne.0) then
        call i4 (iwork(k),dmsaflag,r_nant)
        k = k + r_nant
        call i4 (iwork(k),dmsbflag,r_nbas)
        k = k + r_nbas
        r_dmsaflag = .false.
        do i= 1, r_nant
          do j = 1, 16 
            if (btest(dmsaflag(i),j-1)) r_dmsaflag(j,i) = .true.
            if (btest(dmsaflag(i),j+15)) r_dmsaflag(j+mbands,i) = .true.
          enddo
        enddo
        r_dmsbflag = .false.
        do i = 1, r_nbas
          do j = 1, 16
            if (btest(dmsbflag(i),j-1)) r_dmsbflag(j,i) = .true.
            if (btest(dmsbflag(i),j+15)) r_dmsbflag(j+mbands,i) = .true.
          enddo
        enddo
      endif
    else
      nword = int(lband_original/32)+1
      r_dmsaflag = .false.
      do i = 1, r_nant
        l = 0
        do j=1, nword
          call i4(iwork(k),dummy,1)
          k = k + 1
          do m=1, 32
            l = l + 1
            if (btest(dummy,m-1).and.l.le.lband_original) &
                     r_dmsaflag(l,i) = .true.
          enddo
        enddo
      enddo
      do i = 1, r_nant
        l = 0
        do j=1, nword
          call i4(iwork(k),dummy,1)
          k = k + 1
          do m=1, 32
            l = l + 1
            if (btest(dummy,m-1).and.l.le.lband_original) &
               r_dmsaflag(l+mbands,i) = .true.
          enddo
        enddo
      enddo
      r_dmsbflag = .false.
      do i = 1, r_nbas
        l = 0
        do j=1, nword
          call i4(iwork(k),dummy,1)
          k = k + 1
          do m=1, 32
            l = l + 1
            if (btest(dummy,m-1).and.l.le.lband_original) &
                r_dmsbflag(l,i) = .true.
          enddo
        enddo
      enddo
      do i = 1, r_nbas
        l = 0
        do j=1, nword
          call i4(iwork(k),dummy,1)
          k = k + 1
          do m=1, 32
            l = l + 1
            if (btest(dummy,m-1).and.l.le.lband_original) &
               r_dmsbflag(l+mbands,i) = .true.
          enddo
        enddo
      enddo
    endif
  !
  ! Data file section
  elseif (scode.eq.file_sec) then
    call i4(iwork,r_dxnum,1)
    k = 2
    call cc(iwork(k),ichain,len-k+1)
    call bytoch(ichain,r_dfile,32) ! R_DFILE = CHAIN
    k = len+1
  !
  ! Water vapor radiometer section
  elseif (scode.eq.wvr_sec) then
    call i4(iwork,r_wvrnch,r_nant)
    k = 1+r_nant
    do i=1, r_nant
      call r4(iwork(k),r_wvrfreq(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrbw(1,i),mwvrch)
      k = k+mwvrch
    enddo
    call r4(iwork(k),r_wvrtamb,r_nant)
    k = k+r_nant
    call r4(iwork(k),r_wvrtpel,r_nant)
    k = k+r_nant
    do i=1, r_nant
      call r4(iwork(k),r_wvrtcal(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrref(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvraver(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvramb(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrtrec(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrfeff(1,i),mwvrch)
      k = k+mwvrch
      !*!
      call r4(iwork(k),r_wvrlabtcal(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrlabtrec(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrlabtdio(1,i),mwvrch)
      k = k+mwvrch             !*!
    enddo
    call i4(iwork(k),r_wvrmode,r_nant)
    k = k+r_nant
    call r4(iwork(k),r_wvrh2o,r_nant)
    k = k+r_nant
    call r4(iwork(k),r_wvrpath,r_nant)
    k = k+r_nant
    do i=1, r_nant
      call r4(iwork(k),r_wvrtsys(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrdpath(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrfpath(1,i),mwvrch)
      k = k+mwvrch
      call r4(iwork(k),r_wvrliq(1,i),mwvrch)
      k = k+mwvrch
      !*!
      call r4(iwork(k),r_wvrdcloud(1,i),mwvrch)
      k = k+mwvrch
    enddo
    call r4(iwork(k),r_wvrtatm,r_nant)
    k = k+r_nant
    call i4(iwork(k),r_wvrqual,r_nant)
    k = k+r_nant
    if (e%version.ge.3) then
      call i4(iwork(k),r_wvravertime,1)
      k = k+1
    endif
  !
  ! S.D.Holo. section
  elseif (scode.eq.alma_sec) then
    call r4 (iwork,r_trandist,2)
    k = 3
    ! 2007-02-01  extension
    if (len.gt.k-1) then
      call i4(iwork(k),r_subscan,2)
      k = k+2
      call cc(iwork(k),ichain,64)
      call bytoch(ichain,r_execblock,256)
      k = k+64
      do i=1, r_nant
        call cc(iwork(k),ichain,8)
        call bytoch(ichain,r_antennaname(i),32)
        k = k+8
      enddo
      do i=1, r_nant
        call cc(iwork(k),ichain,8)
        call bytoch(ichain,r_antennatype(i),32)
        k = k+8
      enddo
      call r4tor4(iwork(k), r_dishdiameter, r_nant)
      k = k+r_nant
    endif
  elseif (scode.eq.monitor_sec) then
    k = 1
    do i=1, 2
      call r4 (iwork(k), r_attenuation_hiq(1,i), r_nant)
      k = k+r_nant
    enddo
    do i=1, r_nif
      call r4 (iwork(k), r_laser_level(1,i), r_nant)
      k = k+r_nant
    enddo
    if (e%version.le.3) then
      call r4 (iwork(k), r_widex_temperature, 4)
      k = k+4
    endif
  elseif (scode.eq.status_sec) then
    k = 1
    call cc(iwork(k),ichain,3)
    call bytoch(ichain,r_status,12) 
    k = k+3
    call cc(iwork(k),ichain,3)
    call bytoch(ichain,r_substatus,12) 
    k = k+3
    call cc(iwork(k),ichain,35)
    call bytoch(ichain,r_comm,140) 
    k = k+35
  elseif (scode.eq.vlbi_sec) then
    ! No observation version as version 5 was never written
    k = 1
    call i4(iwork(k),r_refant,1)
    k = k+1
    call i4(iwork(k),r_comparison_ant,1)
    k = k+1
    call i4(iwork(k),r_nant_vlbi,1)
    k = k+1
    call r4(iwork(k),r_maser_gps_diff,1)
    k = k+1
    call r4(iwork(k),r_efftsys,r_nbb)
    k = k+r_nbb
    call i4(iwork(k),r_antmask,r_nant_vlbi)
    k = k+r_nant_vlbi
  endif
end subroutine scrsec
