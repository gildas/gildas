subroutine read_sdm_holoTables(error)
  use gkernel_interfaces
  !------------------------------------------------------------------------
  ! Support READ FILE
  ! read  a SDM data set in ALMA SDM format
  !------------------------------------------------------------------------
  use gildas_def
  use gkernel_types
  use classic_api
  use sdm_State
  use sdm_TotalPower
  use sdm_Pointing
  use sdm_scan
  use sdm_subscan
  use sdm_enumerations
  ! Dummy variables:
  logical :: error
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Local variables:
  !      type (TotalPowerKey), allocatable  :: tpmKeyList(:)
  !      type (TotalPowerKey) :: tpmKey
  !      type (TotalPowerRow) :: tpmRow
  !      type (TotalPowerRow), allocatable :: tpmRowList(:)
  !      type (TotalPowerOpt) :: tpmOpt
  type (TotalPower) :: tpm
  type (ScanRow) :: sRow
  type (ScanKey) :: sKey
  type (scanKey), allocatable :: sKeyList(:)
  type (SubscanKey) :: ssKey
  type (subscanKey), allocatable :: ssKeyList(:)
  type (SubscanRow), allocatable :: ssRowList(:)
  type (Pointing) :: p
  !
  integer, allocatable :: scanNumberList(:), subScanNumberList(:),   &
    integrationNumberList(:),   &
    subIntegNumList(:), dataArray(:), irow(:)
  integer*8, allocatable :: timeList(:), intervalList(:)
  integer :: ier, tpmSize, pSize, r_npol, nch, ndata, ndata1, num,   &
    lenc, i,j, nobs, k, kc, klu, klc, pointingSize, focusSize,   &
    scanSize,subscanSize, oldSubscan, iss, TpRow, numChannel,   &
    numPoly, numAntenna, ip, itpm, ic, ia
  integer(kind=address_length) data_in, ip_data
  character :: dataOid*512, chain*50, done*7
  logical :: end, first, check, present,   &
    holodata, isOutside, almaCorr
  integer :: lfich, ir
  integer(kind=data_length) :: h_offset, c_offset, l_offset
  integer*8 :: dayStart, dayTime
  real :: minfoc(mnant), maxfoc(mnant)
  integer, parameter :: scan_focu = 1, scan_lamb = 2, scan_beta = 3,   &
    scan_five = 6, scan_other = 8, maxNumberIntegration=3000
  character :: dattim*20
  integer  phasecalIntent, rasterIntent
  !      character :: phasecalIntent*256, rasterIntent*256
  integer, allocatable :: subscanTpRows(:,:), TPFloatdataDim(:,:),   &
    cId(:), fId(:), TPuvwDim(:,:) !!!, pRow(:,:)
  integer*8, allocatable :: tId(:)
  real, allocatable :: TPfloatData(:,:,:,:)
  real*8, allocatable :: TPuvw(:,:,:)
  !
  integer*8  :: timeRowStart, trueInterval
  type(ArrayTimeInterval) :: tmp, timeIntervalSubscan
  type(ArrayTimeInterval), allocatable :: timeIntervalRecord(:)
  integer istart, maxIntent
  integer testBuffer(1000)
  type(cputime_t) :: time
  external :: dummyConvert
  !------------------------------------------------------------------------
  ! Code:
  k = 0
  holodata = .true.
  call reset_times(time)
  !
  ! Read the scan table to get the number of subscans
  call getScanTableSize(scanSize, error)
  if (error) return
  print *, 'scanSize ', scanSize
  allocate (sKeyList(scanSize), stat=ier)
  ! now allocate
  sRow%numIntent = 10
  call allocScanRow(sRow, error)
  if (error) return
  if (ier.ne.0) goto 97
  call getScanKeys(scanSize, sKeyList, error)
  if (error) return
  !
  ! Take first scan (should do a scan loop).
  !      print *, 'call getScanRow '
  call getScanRow(sKeyList(1), sRow,error)
  if (error) return
  !      print *, sRow%numIntent, sRow%numIntent
  ! +PATCH
  !c      sRow%scanIntent(1)  = 'HOLOGRAPHY'
  !c      sRow%scanIntent = 'HOLOGRAPHY'
  !$$$      print *, 'sRow%scanIntent ', sRow%scanIntent
  ! -PATCH
  nobs = sRow%numSubscan
  print *, 'This will create ',nobs   &
    ,' observations in CLIC data file.'
  !
  ! Read the subscan Table
  call getSubscanTableSize(subscanSize, error)
  if (error) return
  !! print *, 'subscanSize ', subscanSize
  allocate (ssKeyList(subscanSize), ssRowList(subscanSize), stat=ier   &
    )
  if (ier.ne.0) goto 97
  call getSubscanKeys(subscanSize, ssKeyList, error)
  if (error) return
  allocate(subscanTpRows(maxNumberIntegration,subscanSize),stat =ier   &
    )
  if (ier.ne.0) goto 97
  !
  !     Loop over subscans
  do iss = 1, subscanSize
    ssRowList(iss)%numberIntegration = maxNumberIntegration
    call allocSubScanRow(ssRowList(iss), error)
    if (error) return
    call getSubscanRow(ssKeyList(iss), ssRowList(iss), error)
    if (error) return
    ! +PATCH the intents...
!!!     if (iss.eq.1) then
!!!       PhasecalIntent = ssRowList(iss)%subscanIntent
!!!       ssRowList(iss)%subscanIntent =   &
!!!         SubscanIntent_SCANNING
!!!     elseif  (iss.eq.2) then
!!!       RasterIntent = ssRowList(iss)%subscanIntent
!!!       ssRowList(iss)%subscanIntent =   &
!!!         SubscanIntent_SCANNING
!!!     elseif (ssRowList(iss)%subscanIntent.eq.PhasecalIntent) then
!!!       ssRowList(iss)%subscanIntent =   &
!!!         SubscanIntent_REFERENCE
!!!     elseif (ssRowList(iss)%subscanIntent.eq.RasterIntent) then
!!!       ssRowList(iss)%subscanIntent =   &
!!!         SubscanIntent_REFERENCE
!!!     endif
  !$$$         print *, 'Subscan ',iss, ' ', ssRowList(iss)%subscanIntent
  ! -PATCH
  enddo
  call message_times(time,'Scan&Subscan ')
  !
  ! Read the Total Power  Table
  call getTotalPowerTableSize(tpmSize, error)
  !! print *, 'TotalPower size ',tpmsize
  !
  ! Allocate to one antenna only
  numChannel = 1
  num_corr = 6
  allocate(tpm%floatData(num_corr, numchannel, 1, tpmSize),   &
    tpm%configDescriptionId(tpmSize), tpm%FieldId(tpmSize),   &
    tpm%StateId(mnant,tpmSize), tpm%ExecBlockId(tpmSize)   &
    ,tpm%time(tpmsize), tpm%interval(tpmsize)   &
    ,tpm%subscannumber(tpmsize), tpm%scanNumber(tpmsize)   &
    ,tpm%integrationNumber(tpmsize), tpm%flagRow(tpmsize),   &
    tpm%flagAnt(1,tpmsize), tpm%flagPol(num_corr, 1,tpmsize),   &
    stat =ier)
  if (ier.ne.0) goto 97
  !$$$<<<<<<< read_sdm_holotables.f
  call getTotalPowerFloatDataColumn(tpm, error)
  call getTotalPowerConfigDescriptionIdColumn (tpm ,error)
  call getTotalPowerFieldIdColumn (tpm ,error)
  call getTotalPowerStateIdColumn (tpm ,error)
  call getTotalPowerExecBlockIdColumn (tpm ,error)
  call getTotalPowerTimeColumn (tpm ,error)
  call getTotalPowerIntervalColumn (tpm ,error)
  ! patch
!!!   do i=1, tpmSize
!!!     tpm%interval(i) = 12000000
!!!   enddo
  call getTotalPowerScanNumberColumn (tpm ,error)
  call getTotalPowerSubScanNumberColumn (tpm ,error)
  call getTotalPowerIntegrationNumberColumn (tpm ,error)
  call getTotalPowerFlagRowColumn (tpm ,error)
  call getTotalPowerFlagAntColumn (tpm ,error)
  call getTotalPowerFlagPolColumn (tpm ,error)
  if (error) return
  !
  ! now index it by subscan
  !$$$      do i=1, tpmSize
  !$$$         subscanTpRows(tpm%integrationNumber(i), tpm%subscanNumber(i)) =
  !$$$     &        i
  !$$$      enddo
  !$$$      call message_times(time,'TotalPowerMonitoring Read')
  !
  ! re-calculate number of integrations not needed...
  j = 0
  do i=1, tpmSize
    if (tpm%subscanNumber(i).ne.j) then
      k = 1
    else
      k = k+1
    endif
    tpm%integrationNumber(i) = k
    j = tpm%subscanNumber(i)
    ssRowList(j)%numberIntegration = k
    subscanTpRows(tpm%integrationNumber(i), tpm%subscanNumber(i)) =   &
      i
  enddo
  call message_times(time,'TotalPower Read')
  !
  ! Read the Pointing Table
  call getPointingTableSize(pSize, error)
  !! print *, 'Pointing size ', psize
  !
  ! Allocate to ONE ANTENNA only
  !numPoly = 1
  !numAntenna = 1
  ! allocate(p%timeInterval(pSize), p%pointingDirection(2,numPoly   &
  !   ,pSize), p%target(2,numPoly,pSize), p%encoder(2,pSize),   &
  !   p%antennaId(pSize), pRow(numAntenna, tpmsize), stat=ier)
  !allocate(p%timeInterval(pSize), p%pointingDirection(2,numPoly   &
  !  ,pSize), p%target(2,numPoly,pSize), p%encoder(2,numPoly, pSize),   &
  !  p%antennaId(pSize), pRow(numAntenna, tpmsize), stat=ier)
  !if (ier.ne.0) goto 97
  !call getPointingTimeIntervalColumn(p, error)
  !call getPointingPointingDirectionColumn(p, error)
  !call getPointingTargetColumn(p, error)

  !allocate (p%encoder(2, 1420, pSize))
  !call message_times(time,'getPointingEncoderColumn start')
  !call getPointingEncoderColumn(p, error)
  !call message_times(time,'getPointingEncoderColumn')

  !call getPointingAntennaIdColumn(p, error)
  !call message_times(time,'Pointing Read')
  !$$$ctimeInterval
  !$$$c====  PATCH!!!     recalculate the times in the Pointing Table !!!
  !$$$      trueInterval = 48000000 ! 48 ms.
  !$$$      timeRowStart = p%timeInterval(1)%time
  !$$$      istart = 1
  !$$$      do i=1, pSize
  !$$$c         print *, 'old ',i, p%timeInterval(i)%time, p%timeInterval(i
  !$$$c     &        )%interval, p%timeInterval(i)%time-timeRowStart
  !$$$         tmp%interval = trueInterval
  !$$$         tmp%time = timeRowStart + (i-istart)*trueInterval
  !$$$* last point in row
  !$$$         if (p%timeInterval(i)%interval .gt. 24000000) then
  !$$$            timeRowStart = p%timeInterval(i)%time +p%timeInterval(i
  !$$$     &           )%interval
  !$$$            istart = i+1
  !$$$            p%timeInterval(i)%interval = p%timeInterval(i)%interval
  !$$$     &           + (p%timeInterval(i)%time-tmp%time)
  !$$$            p%timeInterval(i)%time = tmp%time
  !$$$         else
  !$$$            p%timeInterval(i) = tmp
  !$$$         endif
  !$$$c         print *, 'new ',i, p%timeInterval(i)%time, p%timeInterval(i
  !$$$c     &        )%interval, p%timeInterval(i)%time-timeRowStart
  !$$$
  !$$$      enddo
  !$$$c===  END PATCH !!!
  !$$$c
  !
  !     now index it to Total Power... will work if both are ordered.
  !
  !     For this we need the antenna_Id list from the CondifDescription.
  !
  !     We ASSUME there a SINGLE CONFIGDESCRIPTION and get it from the
  !     first row in the TotalPower Table.
  configDescription_Id = tpm%configDescriptionId(1)
  !print *, ' configDescription_Id should be zero ',  configDescription_Id
  r_nant = 0
  r_lmode = 1
  call get_sdm_ConfigDescription(error)
  if (error) return
  !$$$      print *, ' r_nant, antenna_Id (should be 1, 0) ',
  !$$$     &     r_nant, (antenna_Id(ia), ia=1, r_nant)
  !<<<<<<< read_sdm_holotables.f
  !      ip = 0
  !=======
!!!      ip = 0
!!!   ip = 1
!!!   !>>>>>>> 1.4.4.5
!!!   do itpm = 1, tpmSize
!!!     !<<<<<<< read_sdm_holotables.f
!!!     !         ip = mod(ip, pSize) + 1
!!!     !=======
!!!     !!!!         ip = mod(ip, pSize) + 1
!!!     ip = mod(ip-1 , pSize) +1
!!!     !>>>>>>> 1.4.4.5
!!!     if (tpm%configDescriptionId(itpm).ne.0) then
!!!       print *, 'bad tpm%configDescriptionId itpm ', itpm,   &
!!!         'tpm%configDescriptionId(itpm) '   &
!!!         ,tpm%configDescriptionId(itpm)
!!!     endif
!!!     ic = 1
!!!     do while (isOutside(tpm%time(itpm), p%timeInterval(ip)) .and.   &
!!!         ic.le.pSize+1)
!!!       ip = mod(ip, pSize) + 1
!!!       ic = ic+1
!!!     enddo
!!!     if (ic.le.pSize+1) then
!!!       do ia =1, r_nant

!!!         if ( p%antennaId(ip) .eq. antenna_Id(ia)) then
!!!           pRow(ia,itpm) = ip
!!!         endif
!!!       enddo
!!!     !            if (itpm .le. 5000) print *, 'itpm ',itpm, ' ip ',ip, ' ic '
!!!     !     &           ,ic
!!!     !c            print *, 'itpm ',itpm, ' end   ip ',ip, ' ic '
!!!     !c     &           ,ic
!!!     else
!!!       !$$$            call message(8,3,'Read_sdm','Times do not match')
!!!       !$$$            error = .true.
!!!       !$$$            return
!!!       call message(8,2,'Read_sdm','Times do not match')
!!!       print *, ip, p%timeInterval(1)%time, p%timeInterval(psize   &
!!!         )%time, p%timeInterval(1)%time + p%timeInterval(psize   &
!!!         )%interval
!!!     endif
!!!     if (pRow(1,itpm).le.0 .or. pRow(1,itpm).gt.pSize) then
!!!       print *, 'itpm, pRow(1,itpm) ',itpm, pRow(1,itpm)
!!!       print *, 'tpm%time(itpm), ic, ip, p%timeInterval(ip) ',   &
!!!         'p%timeInterval(ip)%time+p%timeInterval(ip)%interval'
!!!       print *, tpm%time(itpm), ic, ip, p%timeInterval(ip),   &
!!!         p%timeInterval(ip)%time+p%timeInterval(ip)%interval
!!!     endif
!!!   enddo
!!!   call message_times(time,'Pointing Indexed to TotalPower')
  !
  !     Loop over subscans
  do iss = 1, subscanSize
    r_ndump = ssRowList(iss)%numberIntegration
    !! print *, 'iss, r_ndump ', iss, r_ndump 
    allocate(timeIntervalRecord(r_ndump))
    !
    ! subscan based stuff: get from 1st integration
    TpRow = subscanTpRows(1,iss)
    !
    r_nant = 0                 ! for a check...
    time_Interval = (/tpm%time(TpRow) - tpm%interval(TpRow)/2,   &
      tpm%interval(TpRow) *r_ndump/)
    !! print *, 'time_Interval ', time_Interval
    if (time_start(1).lt.0) then
      time_start(1) = time_interval(1)
      time_start(2) = -1
    endif
    timeIntervalSubscan = ArrayTimeInterval(tpm%time(TpRow)-tpm%interval(TpRow)/2, &
         tpm%interval(TpRow)*r_ndump )
    !! print *, 'timeIntervalSubscan  ', timeIntervalSubscan
    ! set times in header
    call setDataHeaderTimes(timeIntervalSubscan)

!!!     dayTime = tpm%time(TpRow)
!!!     dayStart = dayTime/1000000000/86400
!!!     r_dobs = dayStart-60549.0d0
!!!     dh_obs = r_dobs
    ! seconds since 00:00 UT:
!!!     dh_utc = dayTime-dayStart*1000000000*86400
!!!     dh_utc = dh_utc/1d9
!!!     r_ut = dh_utc/86400.*pi
!!!     dh_integ = time_interval(2)/1.d9
    Field_Id = tpm%fieldId(TpRow)
    configDescription_Id = tpm%configDescriptionId(TpRow)
    if (tpm%configDescriptionId(TpRow).ne.0) then
      print *, 'bad tpm%configDescriptionId TpRow ', TpRow,   &
        'tpm%configDescriptionId(TpRow) '   &
        ,tpm%configDescriptionId(TpRow)
    endif
    ! caution here
    r_nsb = 2
    r_lnsb = 2
    !
    ! get the number of basebands (=correlator units).
    ! configDescriptionId -> processor_Id -> CorrelatorMode_Id -> numBaseBand
    !! print *, ' configDescription_Id ' , configDescription_Id
    call get_sdm_ConfigDescription(error)
    if (error) goto 99
    !
    ! Get the totalpower   now that the number of antennas is known...
    ExecBlock_Id = tpm%ExecBlockId(TpRow)
    Scan_Number = tpm%scanNumber(TpRow)
    subScan_Number = tpm%subScanNumber(TpRow)
    state_Id = tpm%stateId(1,TpRow)
    !$$$         print *, 'ExecBlock_Id, Scan_Number, subScan_Number'
    !$$$         print *, ExecBlock_Id, Scan_Number, subScan_Number
    !
    call get_sdm_Processor(almaCorr, error)
    if (error) goto 99
    if (correlatorMode_Id .ge. 0) then
      call get_sdm_SquareLawDetector(error)
      if (error) goto 99
    else
      r_lband = 1
    endif
    !
    !     look into the data description (to get data sizes...)
    call get_sdm_DataDescription(error)
    if (error) goto 99
    !
    !     Feed Table (needs Feed_Id BUT having more that one feed is an error!) OK 90
    call get_sdm_Feed(error)
    if (error) return
    !
    !     Spectral window table
    call get_sdm_spectralWindow(holodata,  error)
    if (error) goto 99
    !
    !     get the relevant weather information
    call get_sdm_weather(error)
    if (error) goto 99
    !     antenna table
    call get_sdm_antenna(error)
    if (error) goto 99
    !
    !     station table
    call get_sdm_station(error)
    if (error) goto 99
    !
    !
    !$$$c     Polarization Table OK
    !$$$         call get_sdm_Polarization(error)
    !$$$         if (error) return
    !
    !     Holography Table
    call get_sdm_Holography(error)
    if (error) return
    !
    !     SwitchCycle Table ++  + 90
    call get_sdm_SwitchCycle(error)
    if (error) return
    !
    !     Field Table ++   + 90
    call get_sdm_Field(error)
    if (error) return
    !
    !     Source Table ++  +
    call get_sdm_Source(error)
    if (error) return
    source_Id = 0
    !c
    !     ExecBlock Table ++  +
    call get_sdm_ExecBlock(error)
    if (error) return
    !
    !     History Table (irrelevant?) ++  +
    call get_sdm_History(error)
    if (error) return
    !
    !     Scan and subscan Tables ++  +
    !$$$         call get_sdm_Scan(error)
    !$$$         if (error) return
    !$$$         call get_intent(srow%scanIntent, ssRowList(iss)%subscanIntent)
    call get_intent(srow%numIntent, srow%scanIntent,   &
      ssRowList(iss)%subScanIntent)
    !
    !     SBSummary Table (not needed?)
    !
    !     calculate size parameters
    !
    !     HOLO tables are treated as correlation in CLIC
    R_NANT = 1
    R_LMODE = 1  

    r_npol = 1
    if (r_nant.eq.1) r_nbas = 1
    r_iant(1) = 1
    r_jant(1) = 1
    !
    ! forced to be DSB for compatibility ...
    r_nsb = 2
    r_lnsb = 2
    R_LDATC = 2*R_NBAND*R_NSB*R_NBAS*R_NPOL
    R_LDATL = 2*R_LNTCH*R_LNSB*R_NBAS*R_NPOL
    !     note: the critical parameters are r_lntch and r_nband; they are
    !     obtained by get_sdm_spectralWindow above.
    ! this is based on rrecord routine  (minimal size)
    ! [added last term after new detectors Aug 2006]
    !$$$         r_ldpar = 21 + (24+MWVRCH)*r_nant + 11*r_nbas + 2*r_lband
    !$$$     &        *r_nant
    !$$$         print *,' ==== r_ldpar, r_nant, r_nbas, r_lband , r_npol',
    !$$$     &        r_ldpar,  r_nant, r_nbas, r_lband , r_npol
    r_ldpar = 10000000
    call convert_dhsub(testbuffer, r_ldpar, r_nant, r_nbas, r_lband   &
      , r_npol, dummyconvert, dummyconvert, dummyconvert,   &
      dummyconvert)
    !$$$         print *,' ====>>>> r_ldpar ', r_ldpar
    !
    r_ldatc = 2*r_nband*r_nsb*r_nbas*r_npol
    r_ldatl = 2*r_lntch*r_lnsb*r_nbas*r_npol
    r_ldump = r_ldpar + r_ldatc
    r_ndump = ssRowList(iss)%numberIntegration
    ndata = r_ndump*(r_ldpar+r_ldatc) + r_ldpar+r_ldatc+r_ldatl
    if (r_ndatl.gt.1) then
      ndata = ndata + r_ldpar+r_ldatc+r_ldatl
    endif
    if (allocated(dataArray)) then
      if (ndata.ne.size(dataArray)) then
        if (size(dataArray).gt.0) deallocate(dataArray, stat   &
          =ier)
        if (ier.ne.0) goto 98
        allocate(dataArray(ndata), stat=ier)
        if (ier.ne.0) goto 97
      endif
    else
      allocate(dataArray(ndata), stat=ier)
      if (ier.ne.0) goto 97
    endif
    !
    !     data header for the average record
    k = 1 + h_offset(r_ndump+1)
    call encode_header(dataArray(k))
    if (r_ndatl.gt.1) then
      k = 1+ h_offset(r_ndump+2)    
      call encode_header(dataArray(k))
    endif
    !     Binary data for average record 
    kc = 1 + c_offset(r_ndump+1)
    klu = 1 + l_offset(r_ndump+1)
    klc = 1 + l_offset(r_ndump+2)
    !         print *, 'kc, klu, klc, ndata'
    !         print *, kc, klu, klc, ndata
    !
    ! Now loop on subintegrations
    do j = 1, r_nant
!!!       minlam(j) = 1e20
!!!       minbet(j) = 1e20
      minfoc(j) = 1e20
!!!       maxlam(j) = -1e20
!!!       maxbet(j) = -1e20
      maxfoc(j) = -1e20
      r_mobil(j) = .false.
    enddo
    first = .true.
    ! print *, 'r_ndump = ', r_ndump
    do ir = 1, r_ndump
      ! print *, ir
      TpRow = subscanTpRows(ir,iss)
      time_Interval = (/tpm%time(TpRow) - tpm%interval(TpRow)/2,   &
           tpm%interval(TpRow)/)
      timeIntervalRecord(ir) = ArrayTimeInterval(tpm%time(TpRow)-tpm%interval(TpRow)/2, &
           tpm%interval(TpRow) )
      !
      call setDataHeaderTimes(timeIntervalRecord(ir))
      !
!!!       dayTime = tpm%time(TpRow)
!!!       dayStart = dayTime/1000000000/86400
!!!       !     seconds since 00:00 UT:
!!!       dh_utc = dayTime-dayStart*1000000000*86400
!!!       dh_utc = dh_utc/1d9
!!!       dh_obs = dayStart-60549.0d0
!!!       dh_integ = time_interval(2)/1d9
      Field_Id = tpm%fieldId(TpRow)
      configDescription_Id = tpm%configDescriptionId(TpRow)
      kc = 1 + c_offset(ir)
      !print *, 'get totalPower'
      call get_sdm_totalPowerHoloData(tpm, tpRow,   &
        dataArray(kc), error)
      !            print *, dataArray(kc), dataArray(kc+1), dataArray(kc+2),
      !     &           dataArray(kc+3)
      if (error) return
      !     Get the data from the BIG pointing table
      !     pRow has the list of relevant pointing rows (for each ant


      ! replace by normal calls to pointing table


      !            if (.false.) then
      !call getPointingTableSize(pointingSize, error)
      !if (error) return
      ! do pointign the normal way
      !if (pointingSize.gt.0) then
      !  call get_sdm_Pointing(first, error)
      !  if (error) return
      !endif 
      !
      call getFocusTableSize(FocusSize, error)
      if (error) return
      if (FocusSize.gt.0) then
        !print *, 'get focus'
        call get_sdm_Focus(first, error) 
        if (error) return
      endif
      !print *, 'get state'
      call get_sdm_State(error)
      if (error) return
      !     scanning stuff
      do j=1, r_nant
        minfoc(j) = min(minfoc(j),dh_offfoc(j))
        maxfoc(j) = max(maxfoc(j),dh_offfoc(j))
!!!         minlam(j) = min(minlam(j),dh_offlam(j)*pi/180./3600.)
!!!         maxlam(j) = max(maxlam(j),dh_offlam(j)*pi/180./3600.)
!!!         minbet(j) = min(minbet(j),dh_offbet(j)*pi/180./3600.)
!!!         maxbet(j) = max(maxbet(j),dh_offbet(j)*pi/180./3600.)
      enddo
      !            endif
      !
      k = 1 + h_offset(ir)
      call encode_header(dataArray(k))
      if (ir.gt.r_ndump .and. r_ndatl.gt.1) then
        k = 1+ h_offset(ir+1)
        call encode_header(dataArray(k))
      endif
    enddo
    !!call message_times(time,'TotalPower Read')    
    !
    !! print *, 'timeIntervalSubscan  ', timeIntervalSubscan
    call get_sdm_pointingHoloData(ndata, dataArray, timeIntervalSubscan, timeIntervalRecord, error)
    if (error) return
    !!call message_times(time,'Pointing Read')    
    !
    !
    ! final checks for focus motion
    do j=1, r_nant
      if (abs(maxfoc(j)).gt.0.01) then
        r_mobil(j) = .true.
      endif
!!!       if (maxlam(j)-minlam(j).gt.0.1*pi/180./3600.) then
!!!         r_mobil(j) = .true.
!!!         if (r_scaty.le.3) r_scaty = scan_lamb
!!!       endif
!!!       if (maxbet(j)-minbet(j).gt.0.1*pi/180./3600.) then
!!!         r_mobil(j) = .true.
!!!         if (r_scaty.le.3) r_scaty = scan_beta
!!!       endif
    enddo
    !
    !     Write the observation in IPB file:
    !
    new_receivers = .true.
    r_kind = 5
    call ipb_write(write_mode,check,error)
    if (error) goto 99
    call wdata (ndata,dataArray,.true.,error)
    if (error) goto 99
    call ipb_close(error)
    if (error) goto 99
    write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
      ' '//done(write_mode)//' (',r_ndump,' records)'
    call message(4,1,'READ_SDM',chain(1:50))
    deallocate(timeIntervalRecord, stat=ier)
    if (ier.ne.0) goto 98
  enddo
  !$$$      deallocate (tpmKeyList, intervalList, scanNumberList,
  !$$$     &     subScanNumberList, subIntegNumList, integrationNumberList,
  !$$$     &     stat=ier)
  deallocate (subscanTpRows, stat=ier)
  if (ier.ne.0) goto 98
  call sdm_close()
  call message_times(time,'After writing clic file')
  return
  !
97 call sdmmessageI (8,3,'READ_SDM','Allocate error', ier)
  goto 99
98 call sdmmessageI (8,3,'READ_SDM','Dellocate error', ier)
  goto 99
  !
  !
99 call message(8,4,'Read_sdm','SDM Read Failed')
  error = .true.
  return
end subroutine read_sdm_holoTables
!
subroutine dummyConvert(i,j,n)
  integer :: i,n,j
end subroutine dummyConvert
!p
subroutine setDataHeaderTimes(timeInterval)
  use gildas_def
  use sdm_types
  include 'gbl_pi.inc'
  ! Dummy variables:
  type (ArrayTimeInterval) :: timeInterval
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  integer*8 :: dayTime, daySTart
  dayTime = timeInterval%time + timeInterval%interval/2
  dayStart = dayTime/1000000000/86400
  r_dobs = dayStart-60549.0d0
  dh_obs = r_dobs
  ! seconds since 00:00 UT:
  dh_utc = dayTime-dayStart*1000000000*86400
  dh_utc = dh_utc/1d9
  r_ut = dh_utc/86400.*pi*2
  dh_integ = timeInterval%interval/1.d9
  return
end subroutine setDataHeaderTimes




!$$$      if (timeInterval%interval .ge.0) then
!$$$         isOutside = time.lt.timeInterval%time
!$$$     &        .or. time.ge.timeInterval%time+timeInterval%interval
!$$$      else
!$$$         isOutside = time.lt.timeInterval%time
!$$$      endif
!      print *, time, timeInterval%time-timeInterval%interval/2-time,
!     &     timeInterval%time+timeInterval%interval/2-time, isOutside
!$$$      end
