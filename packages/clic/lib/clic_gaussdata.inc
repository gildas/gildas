!
! GAUSSDATA.INC
      INTEGER MGDATA
      PARAMETER (MGDATA=2048)
      INTEGER NDATA
      REAL*4 RDATAX(MGDATA),RDATAY(MGDATA)
      COMMON /CRGAUSS/ NDATA,RDATAX,RDATAY
!
      SAVE /CRGAUSS/
!-------------------------------------------------------------------------
