subroutine write_fits_dobs (unit, error)
  use classic_api  
  implicit none
  integer :: unit                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: status, date(7)
  character(len=40) :: chain
  !
  ! Write scan number, obs number and date in a HDU.
  status = 0
  call ftpkyj(unit,'SCAN-NUM',r_scan,'Scan number',status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'OBS-NUM',r_num,'Observation number',status)
  if (status .gt. 0) goto 99
  call jjdate (r_dobs+2460549.5d0+r_ut/pi/2,date)
  call fttm2s(date(1),date(2),date(3),date(4),date(5),   &
    date(6)+date(7)*0.001d0,   &
    3,chain,status)
  if (status .gt. 0) goto 99
  call ftpkys(unit,'DATE-OBS',chain,'Date',status)
  if (status .gt. 0) goto 99
  return
  ! error return
99 call printerror('WRITE_FITS_DOBS',status)
  error = .true.
end subroutine write_fits_dobs
