!
function base_number(i,j)
  !---------------------------------------------------------------------
  !  Number of base line from antenna I to antenna J
  !---------------------------------------------------------------------
  integer :: base_number            !
  integer :: i                      !
  integer :: j                      !
  !------------------------------------------------------------------------
  ! Code:
  if (i.ge.j) then
    base_number = 0
  else
    base_number = (j-1)*(j-2)/2 + i
  endif
  return
end function base_number
!
subroutine base_to_n(c,n,error)
  !---------------------------------------------------------------------
  !  Number of base line from antenna C
  !---------------------------------------------------------------------
  character(len=2) :: c             !
  integer :: n                      !
  logical :: error                  !
  ! Global
  integer :: base_number
  ! Local
  integer :: i, j
  !------------------------------------------------------------------------
  ! Code:
  read (c,'(i2,a1,i2)',err=99) i,j
  n = base_number(i,j)
  if (n.ne.0) return
99 error = .true.
  return
end subroutine base_to_n
!
function base(i,j)
  !---------------------------------------------------------------------
  ! Returns the number of baseline I,J (not oriented)
  !---------------------------------------------------------------------
  integer :: base                   !
  integer :: i                      !
  integer :: j                      !
  !------------------------------------------------------------------------
  ! Code:
  if (i.lt.j) then
    base = (j-1)*(j-2)/2 + i
  else
    base = (i-1)*(i-2)/2 + j
  endif
end function base
