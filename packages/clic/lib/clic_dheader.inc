!
! Data structures : Data Header 
!
      INTEGER DH_VOID                   ! dummy alignment word
      INTEGER DH_DUMP                   ! dump number
      INTEGER DH_OBS                    ! MJ date
      REAL DH_INTEG                     ! integration time
      REAL*8 DH_UTC                     ! UTC (sec.)
      REAL*8 DH_SVEC(3)                 ! source direction (H-DELTA coords)
      REAL DH_TEST0(20)                 ! test parameters (global)
      INTEGER DH_AFLAG(MNANT)           ! antenna flags
! added M_POL_REC dimension to DH_TOTAL (RL 2006-10-31)
      REAL DH_TOTAL(MNBB,MNANT)         ! total power for each antenna
!
! NGRX 11.10.06 added M_POL_REC dimension to DH_DELLIN, 
!               DH_DELAY, DH_RMSAMP, DH_RMSPHA
! added mrlband dimension to DH_ATFAC (RL 2006-10-31)
      REAL DH_ATFAC(mrlband,2,MNANT)    ! applied atmosph. cal factor 
      REAL DH_RMSPE(2,MNANT)            ! rms pointing error (az,el)
      REAL DH_DELCON(MNANT)             ! delay used for continuum (ns) **OBSOLETE**
      REAL DH_DELLIN(MNBB,MNANT)        ! delay used for line (ns)
      REAL DH_DELAYC(MNANT)             ! delay computed (ns)
      REAL DH_DELAY(MNBB,MNANT)         ! delay offset (ns)
      REAL DH_PHASEC(MNANT)             ! 1st LO phase, computed (radians)
      REAL DH_PHASE(MNANT)              ! 1st LO phase rotation offset (radians)
      REAL DH_RATEC(MNANT)              ! 1st LO rate, computed (radians/sec)
      REAL DH_CABLE(MNANT)              ! measured cable length (radians)
      INTEGER DH_GAMME(MNANT)           ! phasemeter gamme (by antenna) **OBSOLETE**
      REAL DH_TEST1(20,MNANT)           ! test parameters (by antenna)
      REAL DH_RMSAMP(M_POL_REC,2,MNBAS) ! rms amplitude (0 if no aver)
      REAL DH_RMSPHA(M_POL_REC,2,MNBAS) ! rms phase (rad, 0 if no aver)
      REAL DH_OFFFOC(MNANT)             ! focus offset (mm)
      REAL DH_OFFLAM(MNANT)             ! lambda offset (rad)
      REAL DH_OFFBET(MNANT)             ! beta offset (rad)
      INTEGER DH_BFLAG(MNBAS)           ! baseline flags
      REAL DH_UVM(2,MNBAS)              ! base line components (meters)
      COMPLEX DH_INFAC(2,MNBAS)         ! applied instrum. cal factor (U,L) ** OBSOLETE**
!
! WVR 11.06.01 added new parameters
      REAL DH_WVR(MWVRCH,MNANT)         ! counts of each WVR channel
      INTEGER DH_WVRSTAT(MNANT)         ! status word  1 = noise tube on
                                        !              2 = ambient load on (commanded)
                                        !              4 = ambient load on (sensor)
                                        !              8 = Peltier Alarm on
!
! NGRX 22.08.06 added new parameter
      REAL DH_ACTP(M_POL_REC,MNANT,MRLBAND)     ! total power measured by auto-correlato*
      REAL DH_ANTTP(MNIF,MNANT)         ! total power measured at antenna
      LOGICAL DH_SAFLAG(2*MBANDS,MNANT) ! antenna spectral flags
      LOGICAL DH_SBFLAG(2*MBANDS,MNBAS) ! baseline spectral flags
      LOGICAL IS_SAFLAG(MNANT)          ! at least one saflag -- not in data
      LOGICAL IS_SBFLAG(MNBAS)          ! at least one sbflag -- not in data
! Version 3 observation
      REAL DH_CABLE_ALTERNATE(MNANT)    ! cable phase for other HiQ
      INTEGER DH_NTIME                  ! Number of time values
      REAL DH_TIME_MONITORING(MNTIME)   ! bure1 times
      REAL DH_INCLI(MNANT,2)            ! inclinometer values
      REAL DH_INCLI_TEMP(MNANT)         ! inclinometer temperature
      REAL DH_TILFOC(MNANT,3)           ! secondary tilt      
      REAL DH_OFFFOC_XY(MNANT,2)        ! focus offset X, Y (mm) 
! Version 6 observation
      REAL DH_VLBI_PHASEFF(MNBB)         ! Phasing efficiency
      REAL DH_VLBI_PHASE_INC(MNANT,MNBB) ! Incremental INCA phase correction
      REAL DH_VLBI_PHASE_TOT(MNANT,MNBB) ! Total INCA phase correction

! NOT IN DATA 
      CHARACTER*12 DH_TIME              ! time 
      CHARACTER*12 DH_DATE              ! date dd-mm-yyy 
      COMPLEX DH_AVERAG(2,MNBAS)        ! average visibility
      REAL ALIG                         ! dummy alignement
!
 ! COMMON definition
      COMMON /DH_COM/                                                   &
     &DH_VOID,DH_DUMP,DH_OBS,DH_INTEG,DH_UTC,                           &
     &DH_SVEC,DH_TEST0,DH_AFLAG,DH_TOTAL,                               &
     &DH_ATFAC,DH_RMSPE,DH_DELCON,DH_DELLIN,                            &
     &DH_DELAYC,DH_DELAY,DH_PHASEC,DH_PHASE,                            &
     &DH_RATEC,DH_CABLE,DH_GAMME,DH_TEST1,                              &
     &DH_RMSAMP,DH_RMSPHA,DH_OFFFOC,DH_OFFLAM,                          &
     &DH_OFFBET,DH_BFLAG,DH_UVM,                                        &
     &ALIG,DH_INFAC,DH_WVR,DH_WVRSTAT,DH_ACTP,DH_ANTTP,                 &
     &DH_SAFLAG, DH_SBFLAG,                                             &
     &DH_CABLE_ALTERNATE,DH_NTIME,DH_TIME_MONITORING,                   &
     &DH_INCLI,DH_INCLI_TEMP,DH_TILFOC,DH_OFFFOC_XY,                    &
     &DH_VLBI_PHASEFF,DH_VLBI_PHASE_INC,DH_VLBI_PHASE_TOT,              &
     &DH_TIME,DH_DATE,DH_AVERAG,IS_SAFLAG,IS_SBFLAG
!
      SAVE /DH_COM/
!------------------------------------------------------------------------
