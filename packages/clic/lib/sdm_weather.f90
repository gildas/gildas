subroutine write_sdm_weather(error)
  !---------------------------------------------------------------------
  !     Write the Weather SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_weather
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical error
  ! Local
  type (WeatherRow) :: wRow
  type (WeatherKey) :: wKey
  character, parameter :: sdmTable*8 ='Weather'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! Assume everything works ...
  ! Pressure in Pa
  wRow%pressure = r_pamb*100.
  wRow%pressureFlag = .false.
  wRow%relHumidity = r_humid
  wRow%relHumidityFlag = .false.
  wRow%temperature = r_tamb
  wRow%temperatureFlag = .false.
  wRow%windDirection = r_avwindir*pi/180.
  wRow%windDirectionFlag = .false.
  wRow%windSpeed = r_avwind
  wRow%windSpeedFlag = .false.
  wRow%windMax = r_topwind
  wRow%windMaxFlag = .false.
  wKey%stationId = Weather_Station_Id
  wKey%timeInterval = ArrayTimeInterval(time_Interval(1),   &
    time_Interval(2))
  call addWeatherRow(wKey, wRow, error)
  if (error) return
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine write_sdm_weather
!---------------------------------------------------------------------
subroutine get_sdm_weather(error)
  !---------------------------------------------------------------------
  !     Get the Weather SDM table.
  !
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_weather
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  ! Dummy
  logical error
  ! Local
  type (WeatherRow) :: wRow
  type (WeatherKey), allocatable :: wKeyList(:)
  integer :: i, weatherSize, nw, ier, k
  integer*8 :: timeDiff
  real*8 pressure, relHumidity, temperature, dewPoint, windDirection   &
    , windSpeed, windMax, spressure, srelHumidity, stemperature,   &
    swindDirection , swindSpeed, swindMax, swpressure,   &
    swrelHumidity, swtemperature, swwindDirection , swwindSpeed,   &
    swwindMax, w
  character, parameter :: sdmTable*8 ='Weather'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     get the table size
  !
  call getWeatherTableSize(weatherSize, error)
  if (error) return
  !
  !     get the keys
  !
  if (weatherSize.gt.0) then
    if (allocated(wKeyList)) then
      deallocate(wKeyList, stat=ier)
      if (ier.ne.0) then
        call sdmmessageI(8,4,sdmTable   &
          ,'Deallocation error, ier= ',ier)
        error = .true.
        return
      endif
    endif
    allocate (wKeyList(weatherSize), stat=ier)
    if (ier.ne.0) then
      call sdmmessageI(8,4,sdmTable   &
        ,'Allocation error, ier= ',ier)
      error = .true.
      return
    endif
    call getWeatherKeys(weatherSize, wKeyList, error)
    if (error) return
    !
    !     we average everything that matches time_interval (time intervals
    !     and weather stations)
    !
    nw = 0
    spressure = 0
    srelHumidity = 0
    stemperature = 0
    swindDirection = 0
    swindSpeed = 0
    swindMax = 0
    timeDiff = 1e18
    k = -1
    do i=1, weatherSize
      ! 
      !     matching condition could be different...
      !     note: we keep only one Weather_Station_Id.
      !
      ! patch the interval which has a stupid value (48ms).
      !if (i.lt.weatherSize) then
      !  wKeyList(i)%timeInterval%interval = wKeyList(i+1)%timeInterval%time &
      !       - wKeyList(i)%timeInterval%time 
      !else
      !  wKeyList(i)%timeInterval%interval = 3600*1e9 ! use 1 hour
      !endif
      !
      !! print *, i, wKeyList(i)%stationId, wKeyList(i)%timeInterval%time, wKeyList(i)%timeInterval%interval
      if ((weather_Station_Id .eq. wKeyList(i)%stationId) &
           .and.(wKeyList(i)%timeInterval%interval .gt.0)) then
        
        if (abs(time_interval(1)-wKeyList(i)%timeInterval%time).le.timeDiff) then
          k = i
          timeDiff = abs(time_interval(1)-wKeyList(i)%timeInterval%time)
          !!!
          !! print *, k, timeDiff, wKeyList(i)%timeInterval%time, wKeyList(i)%timeInterval%interval
        endif
      endif
    enddo
    if (k.gt.0) then
      call getWeatherRow(wKeyList(k), wRow, error)
      if (error) return
      if (.not.wRow%pressureFlag) then
        spressure  = wRow%pressure
      endif
      if (.not.wRow%relHumidityFlag) then
        srelHumidity = wRow%relHumidity
      endif
      if (.not.wRow%temperatureFlag) then
        stemperature = wRow%temperature
      endif
      if (.not.wRow%windDirectionFlag) then
        swindDirection = wRow%windDirection
      endif
      if (.not.wRow%windSpeedFlag) then
        swindSpeed = wRow%windSpeed
      endif
      if (.not.wRow%windMaxFlag) then
        swindMax = wRow%windMax
      endif    ! in hPa for Bure
    endif
    r_pamb = spressure/100.
    r_humid = srelHumidity
    r_tamb = stemperature
    r_avwindir = swindDirection *180./pi
    r_avwind = swindSpeed
    r_topwind = swindMax
  endif
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_weather
