subroutine clic_mask(line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC	Command CLIC\MASK f1 f2 ... [/ANTENNA] [/BASELINE] [/RESET]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, j, am(mnant), bm(mnbas)
  logical :: sam(2*mbands,mnant), sbm(2*mbands,mnbas)
  logical :: reset
  character(len=1024) :: out
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  if (sic_narg(0).le.0.and.sic_narg(4).lt.0.and.sic_narg(5).lt.0) then
    call list_flags(mnant,mnbas,ant_mask,bas_mask,san_mask,sba_mask,out)
    call message(6,1,'CLIC_MASK','Masked '//out(1:lenc(out)))
    return
  endif
  call get_flags(line,1,1,2,3,4,5,am,bm,sam,sbm,reset,error)
  if (error) return
  do i=1, mnant
    if (am(i).ne.0) then
      if (reset) then
        ant_mask(i) = iand(ant_mask(i),not(am(i)))
      else
        ant_mask(i) = ior(ant_mask(i),am(i))
      endif
    endif
    do j=1, 2*mbands
      if (sam(j,i)) then
        if (reset) then
          san_mask(j,i) = .false.
        else
          san_mask(j,i) = .true.
        endif
      endif
    enddo
  enddo
  do i=1, mnbas
    if (bm(i).ne.0) then
      if (reset) then
        bas_mask(i) = iand(bas_mask(i),not(bm(i)))
      else
        bas_mask(i) = ior(bas_mask(i),bm(i))
      endif
    endif
    do j=1, 2*mbands
      if (sbm(j,i)) then
        if (reset) then
          sba_mask(j,i) = .false.
        else
          sba_mask(j,i) = .true.
        endif
      endif
    enddo
  enddo
  call list_flags(mnant,mnbas,ant_mask,bas_mask,san_mask,sba_mask,out)
  call message(6,1,'CLIC_MASK','Masked - '//out(1:lenc(out)))
  !
  return
end subroutine clic_mask
