subroutine write_sdm_CorrelatorMode(error)
  !---------------------------------------------------------------------
  ! Write the CorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_CorrelatorMode
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (CorrelatorModeKey) :: cmKey
  type (CorrelatorModeRow) :: cmRow
  character sdmTable*20
  parameter (sdmTable='CorrelatorMode')
  integer ier, i
  integer, parameter :: maxAxes=10, maxunit=8
  integer :: burebasebandNames(maxunit)
  data burebasebandNames/ BasebandName_BB_1, BasebandName_BB_2, &
       BasebandName_BB_3, BasebandName_BB_4, BasebandName_BB_5, BasebandName_BB_6, &
       BasebandName_BB_7, BasebandName_BB_8 /
  !---------------------------------------------------------------------
  ier = 0
  call sdmMessage(1,1,sdmTable ,'starting...')
  cmRow%numBaseband = v_lband
  cmRow%numAxes = 6
  call  allocCorrelatorModeRow(cmRow, error)
  if (error) return
  cmRow%axesOrderArray = (/ AxisName_BAL, AxisName_BAB, AxisName_SPW, AxisName_APC,&
       AxisName_SPP, AxisName_POL /)
  cmRow%basebandConfig = 0
  do i=1, cmRow%numBaseband
    cmRow%basebandNames(i) = burebasebandNames(r_iunit(i))
    ! there is no specific configuration number (?)
    cmRow%basebandConfig(i) = 0
  enddo
  cmRow%correlatorName = CorrelatorName_IRAM_PDB 
  cmRow%accumMode = 0
  cmRow%binMode = 0
  cmRow%filterMode = 0
  call addCorrelatorModeRow(cmKey, cmRow, error)
  if (error) return
  !
  ! was not changed...
  CorrelatorMode_Id = cmKey%correlatorModeId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_CorrelatorMode
!---------------------------------------------------------------------
subroutine get_sdm_CorrelatorMode(error)
  !---------------------------------------------------------------------
  ! Get the CorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_CorrelatorMode
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (CorrelatorModeKey) :: cmKey
  type (CorrelatorModeRow) :: cmRow
  integer ier, ia, ic
  integer, parameter :: maxBaseband = 8, maxAxes=10
  character  :: sdmTable*20
  parameter (sdmTable='CorrelatorMode')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! very much *TBD* at this stage. Think of better filling this in...
  !$$$      print *, 'CorrelatorMode_Id ', CorrelatorMode_Id
  call  allocCorrelatorModeRow(cmRow, maxBaseband, maxAxes, error   &
    )
  if (error) return
  cmKey%CorrelatorModeId  = CorrelatorMode_Id
  call getCorrelatorModeRow(cmKey, cmRow, error)
  if (error) return
  if (cmRow%numBaseband.gt.mrlband) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Wrong numBaseband',cmRow%numBaseband)
    goto 99
  endif
  !
  ! ### Note: with current PDB-NG  format, if there are two polarization products (Polarization.numCorr = 2)
  ! we have to double  double the subbands. SO we should also read the POlarization table.
  ! keep num_Baseband as a global item?
  v_lband = cmRow%numBaseband
  !
  !     baseband index ???
  !
  !     ignore accumMode, binMode, quantization, windowFunction,
  !     axesOrderArray for the time being
  !
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_CorrelatorMode
!---------------------------------------------------------------------
!!! subroutine allocCorrelatorModeRow(row, numBaseband, numAxes, error   &
!!!     )
!!!   !---------------------------------------------------------------------
!!!   !$$$    integer, allocatable :: basebandNames(:)
!!!   !$$$    integer, allocatable :: basebandConfig(:)
!!!   !$$$    logical*1, allocatable :: quantization(:)
!!!   !$$$    integer, allocatable :: windowFunction(:)
!!!   !$$$    integer, allocatable :: axesOrderArray(:)
!!!   !$$$    logical*1, allocatable :: oversampling(:)
!!!   !$$$    integer, allocatable :: correlationBit(:)
!!!   !$$$    integer, allocatable :: filterMode(:)
!!!   !C <<< A ETENDRE AUX NOUVEAUX ITEMS>>>
!!!   !---------------------------------------------------------------------
!!!   use sdm_CorrelatorMode
!!!   type(CorrelatorModeRow) :: row
!!!   integer :: numBaseband, numAxes, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'calReduction'
!!!   !
!!!   ! BasebandNames
!!!   if (allocated(row%BasebandNames)) then
!!!     deallocate(row%BasebandNames, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%BasebandNames(numBaseband), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! basebandConfig
!!!   if (allocated(row%basebandConfig)) then
!!!     deallocate(row%basebandConfig, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%basebandConfig(numBaseband), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! quantization
!!!   !$$$      if (allocated(row%quantization)) then
!!!   !$$$         deallocate(row%quantization, stat=ier)
!!!   !$$$         if (ier.ne.0) goto 98
!!!   !$$$      endif
!!!   !$$$      allocate(row%quantization(numBaseband), stat=ier)
!!!   !$$$      if (ier.ne.0) goto 99
!!!   !
!!!   ! windowFunction
!!!   !$$$      if (allocated(row%windowFunction)) then
!!!   !$$$         deallocate(row%windowFunction, stat=ier)
!!!   !$$$         if (ier.ne.0) goto 98
!!!   !$$$      endif
!!!   !$$$      allocate(row%windowFunction(numBaseband), stat=ier)
!!!   !$$$      if (ier.ne.0) goto 99
!!!   !
!!!   ! oversampling
!!!   !$$$      if (allocated(row%oversampling)) then
!!!   !$$$         deallocate(row%oversampling, stat=ier)
!!!   !$$$         if (ier.ne.0) goto 98
!!!   !$$$      endif
!!!   !$$$      allocate(row%oversampling(numBaseband), stat=ier)
!!!   !$$$      if (ier.ne.0) goto 99
!!!   !
!!!   ! correlationBit
!!!   !$$$      if (allocated(row%correlationBit)) then
!!!   !$$$         deallocate(row%correlationBit, stat=ier)
!!!   !$$$         if (ier.ne.0) goto 98
!!!   !$$$      endif
!!!   !$$$      allocate(row%correlationBit(numBaseband), stat=ier)
!!!   !$$$      if (ier.ne.0) goto 99
!!!   !
!!!   ! filterMode
!!!   if (allocated(row%filterMode)) then
!!!     deallocate(row%filterMode, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%filterMode(numBaseband), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! axesOrderArray (dimension: numAxes)
!!!   if (allocated(row%axesOrderArray)) then
!!!     deallocate(row%axesOrderArray, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%axesOrderArray(numAxes), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
