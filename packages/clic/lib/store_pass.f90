subroutine store_pass(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  use clic_rdata
  !---------------------------------------------------------------------
  !	Stores a passband calibration in the source observations,
  !	assumed to be in the current index.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_proc_par.inc'
  include 'gbl_pi.inc'
  include 'clic_work.inc'
  ! Local
  logical :: end, first
  logical :: multiple, overwrite
  integer :: isb, ibb, lband, lntch, iscan
  integer(kind=8) :: ldatl
  !------------------------------------------------------------------------
  ! Code:
  multiple = .false.
  overwrite  = .false.
  !
  if (sic_present(10,0)) then  ! /SCAN
    if (sic_present(7,0)) then ! /POLARISATION
      !!> here allocate passcpol passlpol
      if (lowres) then
        !
        ! Compute HR version in LR mode
        lband = r_lband
        lntch = r_lntch
        ldatl = r_ldatl
        r_lband = lband_original
        r_lntch = lntch_original
        r_ldatl = ldatl_original
        do iscan=1, mnpolscan
          do ibb=1, r_nbb 
             isb = (3-r_sb(ibb))/2
             call set_pass_spectrum_ant_pol(isb,ibb,r_nband,r_lnsb,&
                              r_nbas+r_ntri,r_lntch,passc,passl,iscan,error)
          enddo 
          bp_spectrum_memory_pol(iscan) = .true.
          call store_pass_scan_pol(iscan,.false.,error)
        enddo
        !
        ! And put back LR version in memory
        r_lband = lband
        r_lntch = lntch
        r_ldatl = ldatl
      endif
      do iscan=1, mnpolscan
        do ibb=1, r_nbb
          isb = (3-r_sb(ibb))/2
       !!> use passcpol and passlpol as argument
          call set_pass_spectrum_ant_pol(isb,ibb,r_nband,r_lnsb,&
                           r_nbas+r_ntri,r_lntch,passc,passl,iscan,error)
        enddo
        bp_spectrum_memory_pol(iscan) = .true.
       !!> add routine, make it work on passcpol and passlpol
        call store_pass_scan_pol(iscan,lowres,error)
      enddo
      return   ! End for /POL /SCAN
    endif
    !
    ! Now /SCAN and .not. /POL
    if (.not.bp_spectrum_memory) then 
      call set_pass (r_nsb,r_nband,r_nbas+r_ntri,   &
         r_lntch, passc, passl, error)
       if (error) return
    endif
    call message(8,1,'STORE_PASS','Storing RF in a scan')
    multiple = .false.
    if (sic_present(11,0)) multiple = .true.  ! /MULTIPLE
    if (sic_present(12,0)) overwrite = .true. ! /OVERWRITE
    if (lowres) then
      !
      ! Compute HR version in LR mode
      lband = r_lband
      lntch = r_lntch
      ldatl = r_ldatl
      r_lband = lband_original
      r_lntch = lntch_original
      r_ldatl = ldatl_original
      do ibb=1, r_nbb 
         isb = r_sb(ibb)
         if (isb.eq.-1) isb = 2
         if (do_pass_antenna) then
           call set_pass_spectrum_ant(isb,ibb,r_nband,r_lnsb,r_nbas+r_ntri, &
                                   r_lntch,passc,passl,error)
         else
           call set_pass_spectrum(isb,ibb,r_nband,r_lnsb,r_nbas+r_ntri, &
                                   r_lntch,passc,passl,error)
         endif
      enddo 
      bp_spectrum_memory = .true.
      call store_pass_scan(multiple,overwrite,.false.,error)
      !
      ! And put back LR version in memory
      r_lband = lband
      r_lntch = lntch
      r_ldatl = ldatl
      do ibb=1, r_nbb
         isb = r_sb(ibb)
         if (isb.eq.-1) isb = 2
         if (do_pass_antenna) then
           call set_pass_spectrum_ant(isb,ibb,r_nband,r_lnsb,r_nbas+r_ntri, &
                                   r_lntch,passc,passl,error)
         else
           call set_pass_spectrum(isb,ibb,r_nband,r_lnsb,r_nbas+r_ntri, &
                                   r_lntch,passc,passl,error)
         endif
      enddo
    endif
    call store_pass_scan(multiple,overwrite,lowres,error)
    if (error) call message(8,3,'STORE_PASS','Problem in store_pass_scan')
    return ! End of /SCAN and .not. /POL
  endif
  !
  ! Now "classical" RF, i.e, .not. /SCAN
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  call check_output_file(error)
  if (error) goto 999
  call check_equal_file(error)
  if (error) goto 999
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  !
  ! Loop on current index
  first = .true.
  call get_first(.false.,error)
  if (error) goto 999
  end = .false.
  do while (.not.end)
    if (r_proc.ne.p_pass .and. r_proc.ne.p_skydip   &
      .and. r_proc.ne.p_sky .and. r_proc.ne.p_cal   &
      .and. r_proc.ne.p_onoff .and. r_proc.ne.p_pseudo) then
      !
      ! Store calibration curves
      if (do_pass_antenna) then
        call sub_store_pass_ant(first,error)
        if (error) goto 999
        r_presec(abpcal_sec) = .true.
      else
        call sub_store_pass(first,error)
        if (error) goto 999
        r_presec(bpcal_sec) = .true.
      endif
    endif
    !
    ! Update or copy scan
    call write_scan (.false.,error)
    if (error) goto 999
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) goto 999
    first = .false.
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_pass
!
subroutine sub_store_pass_ant(first,error)
  use gildas_def
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  !	Stores a passband calibration in the observations currently in
  !	memory
  !       Antenna based mode.
  !---------------------------------------------------------------------
  logical :: first                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_work.inc'
  ! Local
  logical :: do_line, do_cont, do_freq, my_do_rf
  integer :: i, ib, isb, ic, ia, k, ir, inbc, isb1, isb2
  character(len=2) :: ch
  integer ::  pc_yes, pc_last
  parameter (pc_yes=1, pc_last=2)
  !------------------------------------------------------------------------
  ! Code:
  !
  ir = r_nrec
  ! Store calibration curves
  do_cont = .false.
  do_line = .false.
  do_freq = .false.
  ! loop on basebands
  do inbc=1,mnbb
    ! loop on side bands.
    !
    do isb=1, 2
      ! loop on antennas in the current observation
      do ib = -r_nant, -1
        ! search for the corresponding physical antenna in the
        ! memorised passband.
        ia = 0
        !
        do k = 1, bp_nant(inbc)
          if (bp_phys(k,inbc).eq.r_kant(-ib)) then
            ia = bp_ant(k,inbc)
          endif
        enddo
        if (ia.eq.0) then
          if (my_do_rf(-ib,inbc)) then  
            write(ch,'(i2)') -ib
            call message(6,3,'SUB_STORE_PASS',   &
              'No passband available for antenna '//ch(1:2))
            error = .true.
            return
          else
            cycle
          endif
        endif
        if (sba(isb,-ia,ir).ne.0) then
          r_abpcsba(1,isb,-ib) =   &
            sba(isb,-ia,ir)/abs(sba(isb,-ia,ir))
        endif
        !1- continuum
        if (fbp_camp(isb,inbc,-ia,ir)) then
          do i=1,r_nband
            r_abpccamp(inbc,isb,-ib,i)=   &
              bp_camp(isb,inbc,-ia,i,ir)
          enddo
          do_cont = .true.
        endif
        if (fbp_cpha(isb,inbc,-ia,ir)) then
          do i=1,r_nband
            r_abpccpha(inbc,isb,-ib,i)=   &
              bp_cpha(isb,inbc,-ia,i,ir)
          enddo
          do_cont = .true.
        endif
        ! 2- line
        do ic=1, r_lband
          if (fbp_lamp(isb,inbc,-ia,ic,ir)) then
            do i=0, bpc_deg(isb,inbc,-ia,ic,ir)
              r_abpclamp(inbc,isb,-ib,ic,i) =   &
                bp_lamp(isb,inbc,-ia,ic,i,ir)
            enddo
            if (r_abpcdeg.gt.bpc_deg(isb,inbc,-ia,ic,ir)) then
              do i=bpc_deg(isb,inbc,-ia,ic,ir)+1,r_abpcdeg
                r_abpclamp(inbc,isb,-ib,ic,i) = 0
              enddo
            else
              r_abpcdeg = bpc_deg(isb,inbc,-ia,ic,ir)
            endif
            do_line = .true.
          endif
          if (fbp_lpha(isb,inbc,-ia,ic,ir)) then
            do i=0, bpc_deg(isb,inbc,-ia,ic,ir)
              r_abpclpha(inbc,isb,-ib,ic,i) =   &
                bp_lpha(isb,inbc,-ia,ic,i,ir)
            enddo
            if (r_abpcdeg.gt.bpc_deg(isb,inbc,-ia,ic,ir)) then
              do i=bpc_deg(isb,inbc,-ia,ic,ir)+1,r_abpcdeg
                r_abpclpha(inbc,isb,-ib,ic,i) = 0
              enddo
            else
              r_abpcdeg = bpc_deg(isb,inbc,-ia,ic,ir)
            endif
            do_line = .true.
          endif
        enddo
        ! 3 - frequency
        if (fbp_famp(isb,inbc,-ia,ir)) then
          do i=0,bpf_deg(isb,inbc,-ia,ir)
            r_abpfamp(inbc,isb,-ib,i) =   &
              bp_famp(isb,inbc,-ia,i,ir)
          enddo
          if (r_abpfdeg.gt.bpf_deg(isb,inbc,-ia,ir)) then
            do i=bpf_deg(isb,inbc,-ia,ir)+1,r_abpfdeg
              r_abpfamp(inbc,isb,-ib,i) = 0
            enddo
          else
            r_abpfdeg = bpf_deg(isb,inbc,-ia,ir)
          endif
          do_freq = .true.
          r_abpflim(inbc,1) = bp_flim(isb,inbc,-ia,1,ir)
          r_abpflim(inbc,2) = bp_flim(isb,inbc,-ia,2,ir)
        endif
        if (fbp_fpha(isb,inbc,-ia,ir)) then
          do i=0,bpf_deg(isb,inbc,-ia,ir)
            r_abpfpha(inbc,isb,-ib,i) =   &
              bp_fpha(isb,inbc,-ia,i,ir)
          enddo
          if (r_abpfdeg.gt.bpf_deg(isb,inbc,-ia,ir)) then
            do i=bpf_deg(isb,inbc,-ia,ir)+1,r_abpfdeg
              r_abpfpha(inbc,isb,-ib,i) = 0
            enddo
          else
            r_abpfdeg = bpf_deg(isb,inbc,-ia,ir)
          endif
          do_freq = .true.
          r_abpflim(inbc,1) = bp_flim(isb,inbc,-ia,1,ir)
          r_abpflim(inbc,2) = bp_flim(isb,inbc,-ia,2,ir)
        endif
      enddo
    enddo
  enddo
  !
  if (do_line) then
    if (bpc_band.ne.r_lband) then
      call message(6,2,'STORE_PASS',   &
        'Incompatible Correlator Configurations')
    endif
  !*         R_ABPCDEG = BPC_DEG
  elseif (r_abpc.eq.0) then
    r_abpcdeg = 5              ! to recover space
  endif
  if (.not.do_cont .and. .not.do_line .and. .not.do_freq) then
    call message (6,3,'STORE_PASS','No RF Passband in Memory')
    goto 999
  endif
  r_abpc = ior(r_abpc,pc_yes+pc_last)
  r_bpc = iand(r_bpc,not(pc_last))
  !      R_ABPC = 1                   ! not yet applied, but stored
  if (first) then
    if (.not.do_cont) then
      call message (5,2,'STORE_PASS',   &
        'No Channel Continuum RF Passband in Memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Channel Continuum RF Passband')
    endif
    if (.not.do_line) then
      call message (5,2,'STORE_PASS',   &
        'No Channel Line RF Passband in Memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Channel Line RF Passband')
    endif
    if (.not.do_freq) then
      call message (5,2,'STORE_PASS',   &
        'No Frequency RF Passband in memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Frequency RF Passband')
    endif
  endif
  return
  !
999 error = .true.
  return
end subroutine sub_store_pass_ant
!
subroutine sub_store_pass(first,error)
  use gildas_def
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  !	Stores a passband calibration in the observations currently in
  !	memory
  !---------------------------------------------------------------------
  logical :: first                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_work.inc'
  include 'clic_number.inc'
  ! Local
  logical :: do_line, do_cont, do_freq, my_do_rf
  integer :: i, ib, isb, ic, jb, k, l, ir, inbc, isb1, isb2
  character(len=2) :: ch
  integer ::  pc_yes, pc_last
  parameter (pc_yes=1, pc_last=2)
  !
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Store calibration curves
  do_cont = .false.
  do_line = .false.
  do_freq = .false.
  ir = r_nrec
  ! loop on basebands
  do inbc=1,mnbb
    ! loop on side bands
    !
    do isb=1, 2
      ! loop on baselines in the current observation
      do ib = 1, r_nbas
        ! search for the corresponding physical baseline in the
        ! memorised passband.
        jb = 0
        do k=1, bp_nant(inbc)
          do l=1, bp_nant(inbc)
            if ((bp_phys(k,inbc).eq.r_kant(r_iant(ib))).and.   &
              (bp_phys(l,inbc).eq.r_kant(r_jant(ib)))) then
              jb = basant(k,l)
            endif
          enddo
        enddo
        if (jb.eq.0) then
          if (my_do_rf(r_iant(ib),inbc).and.my_do_rf(r_jant(ib),inbc)) then
            write(ch,'(i2)') ib
            call message(6,3,'SUB_STORE_PASS',   &
              'No passband available for baseline '//ch(1:2))
            error = .true.
            return
          else
            cycle
          endif
        endif
        if (sba(isb,ib,ir).ne.0) then
          r_bpcsba(1,isb,ib) =   &
            sba(isb,jb,ir)/abs(sba(isb,jb,ir))
        endif
        ! 1- continuum
        if (fbp_camp(isb,inbc,jb,ir)) then
          do i=1,r_nband
            r_bpccamp(inbc,isb,ib,i)= bp_camp(isb,inbc,jb,i,ir)
          enddo
          do_cont = .true.
        endif
        if (fbp_cpha(isb,inbc,jb,ir)) then
          do i=1,r_nband
            r_bpccpha(inbc,isb,ib,i)= bp_cpha(isb,inbc,jb,i,ir)
          enddo
          do_cont = .true.
        endif
        ! 2- line
        do ic=1, r_lband
          if (fbp_lamp(isb,inbc,jb,ic,ir)) then
            do i=0,bpc_deg(isb,inbc,jb,ic,ir)
              r_bpclamp(inbc,isb,ib,ic,i) =   &
                bp_lamp(isb,inbc,jb,ic,i,ir)
            enddo
            if (r_bpcdeg.gt.bpc_deg(isb,inbc,jb,ic,ir)) then
              do i=bpc_deg(isb,inbc,jb,ic,ir)+1,r_bpcdeg
                r_bpclamp(inbc,isb,ib,ic,i) = 0
              enddo
            else
              r_bpcdeg = bpc_deg(isb,inbc,jb,ic,ir)
            endif
            do_line = .true.
          endif
          if (fbp_lpha(isb,inbc,jb,ic,ir)) then
            do i=0,bpc_deg(isb,inbc,jb,ic,ir)
              r_bpclpha(inbc,isb,ib,ic,i) =   &
                bp_lpha(isb,inbc,jb,ic,i,ir)
            enddo
            if (r_bpcdeg.gt.bpc_deg(isb,inbc,jb,ic,ir)) then
              do i=bpc_deg(isb,inbc,jb,ic,ir)+1,r_bpcdeg
                r_bpclpha(inbc,isb,ib,ic,i) = 0
              enddo
            else
              r_bpcdeg = bpc_deg(isb,inbc,jb,ic,ir)
            endif
            do_line = .true.
          endif
        enddo
        ! 3 - frequency
        if (fbp_famp(isb,inbc,jb,ir)) then
          do i=0,bpf_deg(isb,inbc,jb,ir)
            r_bpfamp(inbc,isb,ib,i) = bp_famp(isb,inbc,jb,i,ir)
          enddo
          if (r_bpfdeg.gt.bpf_deg(isb,inbc,jb,ir)) then
            do i=bpf_deg(isb,inbc,jb,ir)+1,r_bpfdeg
              r_bpfamp(inbc,isb,ib,i) = 0
            enddo
          else
            r_bpfdeg = bpf_deg(isb,inbc,jb,ir)
          endif
          do_freq = .true.
          r_bpflim(inbc,1) = bp_flim(isb,inbc,jb,1,ir)
          r_bpflim(inbc,2) = bp_flim(isb,inbc,jb,2,ir)
        endif
        if (fbp_fpha(isb,inbc,jb,ir)) then
          do i=0,bpf_deg(isb,inbc,jb,ir)
            r_bpfpha(inbc,isb,ib,i) = bp_fpha(isb,inbc,jb,i,ir)
          enddo
          if (r_bpfdeg.gt.bpf_deg(isb,inbc,jb,ir)) then
            do i=bpf_deg(isb,inbc,jb,ir)+1,r_bpfdeg
              r_bpfpha(inbc,isb,ib,i) = 0
            enddo
          else
            r_bpfdeg = bpf_deg(isb,inbc,jb,ir)
          endif
          do_freq = .true.
          r_bpflim(inbc,1) = bp_flim(isb,inbc,jb,1,ir)
          r_bpflim(inbc,2) = bp_flim(isb,inbc,jb,2,ir)
        endif
      enddo
    enddo
  enddo
  !
  if (do_line) then
    if (bpc_band.ne.r_lband) then
      call message(6,2,'STORE_PASS',   &
        'Incompatible correlator configurations')
    endif
  elseif (r_bpc.eq.0) then
    r_bpcdeg = 5               ! to recover space
  endif
  if (.not.do_cont .and. .not.do_line .and. .not.do_freq) then
    call message (6,3,'STORE_PASS','No RF passband in memory')
    goto 999
  endif
  r_bpc = ior(r_bpc,pc_yes+pc_last)
  r_abpc = iand(r_abpc,not(pc_last))
  !      R_BPC = 1                    ! not yet applied, but stored
  if (first) then
    if (.not.do_cont) then
      call message (5,2,'STORE_PASS',   &
        'No Channel Continuum RF passband in memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Channel Continuum RF passband')
    endif
    if (.not.do_line) then
      call message (5,2,'STORE_PASS',   &
        'No Channel Line RF passband in memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Channel Line RF passband')
    endif
    if (.not.do_freq) then
      call message (5,2,'STORE_PASS',   &
        'No Frequency RF passband in memory')
    else
      call message (4,1,'STORE_PASS',   &
        'Storing Frequency RF passband')
    endif
  endif
  return
  !
999 error = .true.
  return
end subroutine sub_store_pass
!
subroutine store_pass_scan(multiple,overwrite,lowre,error)
  use gkernel_interfaces
  use classic_api
  use clic_find
  use clic_file
  use clic_rdata
  use clic_bpc
  !
  logical :: multiple
  logical :: overwrite
  logical :: lowre
  logical :: error
  ! 
  include 'clic_parameter.inc'
  include 'clic_proc_par.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  !
  integer :: nnn
  integer(kind=entry_length) :: ifind(max_rf_file)
  integer(kind=address_length) :: kin, ipk, data_out, ndata_out
  character(len=23) :: datetime
  character(len=256) :: chain
  real(kind=8)      :: mjd
  !
  if (.not.bp_spectrum_memory) then
    call message(8,3,'STORE_PASS_SCAN','No RF solution in memory')
    error = .true.
    return
  endif
  !
  call check_output_file(error)
  if (error) return 
  !
  ! Check if already present
  nnn = max_rf_file
  fscan = .true.
  fnum = .false.
  fver = .false.
  fut = .false.
  xrecei = -1
  xnscan = 1
  xscan(1) = scan_offset
  yscan(1) = scan_offset+max_rf_file
  if (lowre) then
    xscan(1) = scan_offset+max_rf_file+1
    yscan(1) = xscan(1)+max_rf_file
  endif
  !
  call fox(nnn,ifind(1),error)
  if (nnn.gt.0.and..not.overwrite) then
    if (.not.multiple) then
      call message(8,3,'STORE_PASS_SCAN','File already contains a BANDPASS')
      error = .true. 
      return
    elseif(nnn.ge.max_rf_file) then
      call message(8,3,'STORE_PASS_SCAN','Maximum number of solutions reached')
      error = .true. 
      return
    endif
  endif
  !
  ! Update title
  r_sourc = bp_source
  r_proc = p_band 
  if (overwrite) then
    if (nnn.eq.0) nnn = 1
    r_scan = nnn + scan_offset
  else
    r_scan = nnn +  scan_offset + 1
  endif
  if (lowre) r_scan = r_scan+max_rf_file
  if (bp_memory_antenna) then
    r_line(1:2) = 'A.'
  else
    r_line(1:2) = 'B.'
  endif
  if (bp_memory_origin.eq.bp_memory_spec) then
    r_line(3:12) = ' Gain'
  elseif (bp_memory_origin.eq.bp_memory_freq) then
    r_line(3:7)  = ' Pol'
    if (bp_memory_antenna) then
      if (r_abpc.ne.0) then
        write(r_line(8:12),'(a,i2)') ' ',r_abpfdeg
      endif
    else
      if (r_bpc.ne.0) then
        write(r_line(8:12),'(a,i2)') ' ',r_bpfdeg
      endif
    endif
  endif
  call sic_isodate (datetime)
  call gag_isodate2mjd(datetime,mjd,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem with isodate2mjd')
    return
  endif
  call gag_mjd2gagut(mjd,r_dobs,r_ut,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem with mjd2gagut')
    return
  endif
  r_itype = 3
  ! Data descriptor
  r_ndump = 0 
  r_ldatc = mdatac*2    ! 2 complex value      
  r_ldump = r_ldpar+r_ldatc
  r_ndatl = 1           ! one spectrum
  r_ldatl = mdatal*2    ! 2 complex value
  ndata_out = (r_ndump+r_ndatl)*r_ldump +r_ndatl*r_ldatl
  !
  ! Sections
  r_presec(bpcal_sec) = .false.
  r_presec(abpcal_sec) = .false.
  r_presec(ical_sec) = .false.
  r_presec(aical_sec) = .false.
  r_presec(wvr_sec) = .false.
  r_presec(file_sec) = .false.
  if (overwrite) then
    r_num = ifind(nnn)
  else
    r_num = o%desc%xnext ! Guarantee increase even if file both
  endif
  !
  ! Get some memory
  call get_memory(ndata_out,data_out,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problems with getting memory')
    return
  endif
  !
  kin = gag_pointer(data_out,memory)
  call encode_header(memory(kin))
  ipk = kin + r_ldpar
  call w4tow4(passc(1),memory(ipk),r_ldatc)
  ipk = ipk + r_ldatc
  call w4tow4(passl(1),memory(ipk),r_ldatl)
  call ipb_open(error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Cannot open file')
    return
  endif
  if (overwrite) then
    call ipb_write('UPDATE',.false.,error)
    call wgen(error)
    if (error) return
    call wpos(error)           ! position
    if (error) return
    call winterc(error)
    if (r_presec(rfset_sec)) then
      call wrfset(error)
      if (error) return
    endif
    !
    ! This is never written
    !         IF (R_PRESEC(SPEC_SEC)) THEN
    !            CALL WSPEC(ERROR)
    !            IF (ERROR) RETURN
    !         ENDIF
    !
    if (r_presec(contset_sec)) then
      call wcontset(error)
      if (error) return
    endif
    if (r_presec(lineset_sec)) then
      call wlineset(error)
      if (error) return
    endif
    call wdescr(error)
  else
    call ipb_write('NEW',.false.,error)
  endif
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem writing observation')
    return
  endif
  call wdata(ndata_out,memory(kin),.false.,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem writing data')
    return
  endif
  call ipb_close(error)
  if (overwrite) then
    write (chain,'(a,i0,a)') 'Solution #',mod(r_scan,scan_offset),' updated'
    call message(8,1,'STORE_PASS_SCAN',chain(1:lenc(chain)))
  else
    write (chain,'(a,i0,a)') 'Solution #',mod(r_scan,scan_offset),' written'
    call message(8,1,'STORE_PASS_SCAN',chain(1:lenc(chain)))
  endif
end subroutine store_pass_scan
!
subroutine store_pass_scan_pol(iscan,lowre,error)
  use gkernel_interfaces
  use classic_api
  use clic_find
  use clic_file
  use clic_rdata
  use clic_bpc
  !
  integer :: iscan
  logical :: lowre
  logical :: error
  ! 
  include 'clic_parameter.inc'
  include 'clic_proc_par.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  !
  integer :: nnn
  integer(kind=entry_length) :: ifind(max_rf_file)
  integer(kind=address_length) :: kin, ipk, data_out, ndata_out
  character(len=23) :: datetime
  real(kind=8)      :: mjd
  !
  if (.not.bp_spectrum_memory_pol(iscan)) then
    call message(8,3,'STORE_PASS_SCAN_POL',&
        'No RF solution in memory for this subscan')
    error = .true.
    return
  endif
  !
  call check_output_file(error)
  if (error) return 
  !
  ! Check if already present
  nnn = max_rf_file
  fscan = .true.
  xnscan = 1
  xscan(1) = scan_offset+iscan+1
  yscan(1) = xscan(1)
  if (lowre) then
    xscan(1) = xscan(1)+max_rf_file
    yscan(1) = yscan(1)+max_rf_file
  endif
  !
  call fox(nnn,ifind(1),error)
  if (nnn.gt.0) then
    call message(8,3,'STORE_PASS_SCAN_POL',&
     'File already contains a BANDPASS for this subscan')
    error = .true. 
    return
  endif
  !
  ! Update title
  r_sourc = bp_source
  r_proc = p_band 
  r_scan = scan_offset + iscan + 1
  if (lowre) r_scan = r_scan+max_rf_file
  if (bp_memory_antenna) then
    r_line(1:2) = 'A.'
  else
    r_line(1:2) = 'B.'
  endif
  if (bp_memory_origin.eq.bp_memory_spec) then
    r_line(3:8) = ' Gain'
    write (r_line(9:12),'(a,i2)') 'S',iscan
  elseif (bp_memory_origin.eq.bp_memory_freq) then
    r_line(3:7)  = ' Pol'
    if (bp_memory_antenna) then
      if (r_abpc.ne.0) then
        write(r_line(8:12),'(a,i2)') ' ',r_abpfdeg
      endif
    else
      if (r_bpc.ne.0) then
        write(r_line(8:12),'(a,i2)') ' ',r_bpfdeg
      endif
    endif
  endif
  call sic_isodate (datetime)
  call gag_isodate2mjd(datetime,mjd,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN_POL','Problem with isodate2mjd')
    return
  endif
  call gag_mjd2gagut(mjd,r_dobs,r_ut,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN_POL','Problem with mjd2gagut')
    return
  endif
  r_itype = 3
  ! Data descriptor
  r_ndump = 0 
  r_ldatc = mdatac      
  r_ldump = r_ldpar+r_ldatc
  r_ndatl = 1           ! one spectrum
  r_ldatl = mdatal
  ndata_out = (r_ndump+r_ndatl)*r_ldump +r_ndatl*r_ldatl
  !
  ! Sections
  r_presec(bpcal_sec) = .false.
  r_presec(abpcal_sec) = .false.
  r_presec(ical_sec) = .false.
  r_presec(aical_sec) = .false.
  r_presec(wvr_sec) = .false.
  r_presec(file_sec) = .false.
  r_num = o%desc%xnext ! Guarantee increase even if file both
  !
  ! Get some memory
  call get_memory(ndata_out,data_out,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN_POL','Problems with getting memory')
    return
  endif
  !
  kin = gag_pointer(data_out,memory)
  call encode_header(memory(kin))
  ipk = kin + r_ldpar
  call w4tow4(passc(1),memory(ipk),r_ldatc)
  ipk = ipk + r_ldatc
  call w4tow4(passl(1),memory(ipk),r_ldatl)
  call ipb_open(error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Cannot open file')
    return
  endif
  call ipb_write('NEW',.false.,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem writing observation')
    return
  endif
  call wdata(ndata_out,memory(kin),.false.,error)
  if (error) then
    call message(8,3,'STORE_PASS_SCAN','Problem writing data')
    return
  endif
  call ipb_close(error)
  call message(6,1,'STORE_PASS_SCAN','Subscan stored as a new observation')
end subroutine store_pass_scan_pol
!
subroutine get_pass_scan(rf_chain,iscan,error)
  use gkernel_interfaces
  use classic_api
  use clic_find
  use clic_file
  use clic_rdata
  !
  character :: rf_chain*12
  integer :: iscan
  logical :: error
  !
  include 'clic_proc_par.inc'
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  !
  integer :: nnn,isol,ier
  integer(kind=address_length) :: kin, ipk, ndata, data
  integer, allocatable :: saved_header(:)
  integer(kind=entry_length) :: ifind(max_rf_file)
  character(len=128) :: chain
  !
  nnn = max_rf_file
  !
  ! First save current header
  allocate(saved_header(m_header),stat=ier)
  if (failed_allocate('GET_PASS_SCAN','saved_header',ier,error))  return
  call i4toi4(r_xnum,saved_header,m_header)
  !
  fscan = .true.
  fnum = .false.
  fver = .false.
  fut = .false.
  xrecei = -1
  xnscan = 1
  if (iscan.ne.0) then
    xscan(1) = scan_offset+iscan+1
    yscan(1) = scan_offset+iscan+1
  else
    xscan(1) = scan_offset
    yscan(1) = scan_offset+max_rf_file
  endif
  if (lowres) then
    xscan(1) = xscan(1)+max_rf_file
    yscan(1) = yscan(1)+max_rf_file
  endif
  call fix_bp(nnn,ifind,error) 
  if (nnn.gt.0) then
    write (chain,'(a,i0,a)') 'Found ',nnn,' scans'
    call message(8,1,'GET_PASS_SCAN',chain(1:lenc(chain)))
  else 
    call message(8,3,'GET_PASS_SCAN','No BANDPASS scan found')
    error  = .true.
    return
  endif 
  if (nnn.gt.1) then
    if (bp_file_id.gt.0) then
      if (bp_file_id.le.nnn) then
        write (chain,'(a,i0)') 'Selecting passband solution #',bp_file_id
        call message(6,1,'GET_PASS_SCAN',chain(1:lenc(chain)))
        isol = bp_file_id
      else
        write (chain,'(a,i0,a)') 'Passband solution #',bp_file_id,' not found'
        call message(8,3,'GET_PASS_SCAN',chain(1:lenc(chain)))
        error = .true.
        return
      endif
    else
      call message(8,3,'GET_PASS_SCAN','More than one solution stored')
      call message(8,3,'GET_PASS_SCAN','Please select one with SET RF')
      error = .true.
      return
    endif
  else
    isol = 1
  endif
  call get_it(ifind(isol),error)
  write (chain,'(a,i0)') 'Got solution #',mod(r_scan,scan_offset)
  call message(8,1,'GET_PASS_SCAN',chain(1:lenc(chain)))
  if (error) return
  call get_data(ndata,data,error)
  if (error) then
    call message(8,3,'GET_PASS_SCAN','Problems reading data')
    return
  endif
  kin = gag_pointer(data,memory)
  call decode_header(memory(kin))
  ipk = kin+r_ldpar
  call w4tow4(memory(ipk),passc(1),r_ldatc)
  ipk = ipk+r_ldatc
  call w4tow4(memory(ipk),passl(1),r_ldatl)
  !
  ! Restore header
  rf_chain = r_line
  call i4toi4(saved_header,r_xnum,m_header)
end subroutine get_pass_scan

