subroutine find (line,error)
  use gkernel_interfaces
  use gildas_def
  use clic_title
  use clic_index
  use clic_find
  !---------------------------------------------------------------------
  ! SAS	Support routine for command
  !	FIND
  !	Find a set of observations according to setup searh rules
  !	and command options.
  ! Arguments :
  !	LINE	C*(*)	Command line			Input
  !	ERROR	L	Logical error flag		Output
  ! (5-mar-1985)
  ! #1	Modified	13-Nov-1985	S.Guilloteau
  !	Use of "...*"	for source, line and telescope names
  ! 	and FIND/OFFSET as a restriction over the range defined in
  !	SET RANGE or SET OFFSET
  ! #2	Modified	20-Mar-1986	S.Guilloteau
  !	No modification indeed but FIX behaviour has changed,
  !	and IFIND is now dummy
  ! #3	Modified	11-Nov-1986	S.Guilloteau
  !	Support for continuum
  ! #4	Number of observations in index
  ! #5	Quality flag /REDUCED option suppressed.
  ! #6	Modified 	05-Oct-1987	R.Lucas
  !	FIND NEWDATA Option
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  type (title_v2_t) :: title_save
  real :: fourpi
  parameter (fourpi = 4.0*pis)
  character(len=10) :: nombre
  integer*4 :: k,lsave(32)
  character(len=14) :: argum, argum1, argum2
  logical :: new_wanted, upd_wanted, append
  integer :: mvocab
  parameter (mvocab=3)
  character(len=12) :: vocab(mvocab),keywor
  integer :: nkey, nnn, i, j, narg, jopt
  integer(kind=8) :: ifind, cxnext0
  character(len=12) :: arg
  integer :: mvoc1, mvoc2
  parameter (mvoc1=4, mvoc2=22)
  character(len=12) :: kw, voc1(mvoc1), voc2(mvoc2)
  data voc1/'OBJECT','PHASE','RF_PASSBAND','*'/
  data voc2/'CORRELATION','AUTOCORR','GAIN','DELAY','FOCUS',   &
    'POINTING','CALIBRATE','IFPB','ONOFF','HOLOGRAPHY',   &
    'FIVE_POINT','PSEUDO_CONT','FLUX','STABILITY','CWVR',   &
    'VLBI','VLBG','OTF','BANDPASS','MONITORING',   &
    ! les nouveaux mots s'ajoutent ci-dessus
    'SKYDIP','*'/
  data vocab/'APPEND','NEW_DATA','UPDATE'/
  integer, parameter :: mmod=2
  character(len=6) :: vmod(mmod)
  data vmod /'CROSS','AUTO'/
  integer, parameter :: mload=3
  character(len=6) :: vload(mload)
  data vload /'COLD','HOT','SKY'/
  !
  ! Save the current short title
  title_save = title
  !
  !		determine the search rules
  !      YKIND = 3                          ! old format
  xkind = 3                    ! oldest format
  ykind = 5                    ! newest format
  last=.true.
  fver=.false.
  !
  ! /TYPE Type
  if (sic_present(1,0)) then
    arg = '*'
    call clic_kw(line,1,1,arg,nkey,voc1,mvoc1,.false.,error,.true.)
    if (error) return
    if (arg.eq.'OBJECT') then
      xitype = 1
    elseif (arg.eq.'PHASE') then
      xitype = 2
    elseif (arg.eq.'RF_PASSBAND') then
      xitype = 3
    else
      xitype = -1
    endif
  else
    xitype = sitype
  endif
  !
  !		line name
  ! /LINE
  if (sic_present(2,0)) then
    xcline ='*'
    !         CALL SIC_CH(LINE,2,1,XCLINE,NARG,.FALSE.,ERROR)
    call sic_ke(line,2,1,xcline,narg,.false.,error)
    if (error) return
  ! SET LINE
  else
    xcline = sline
  endif
  iline = index(xcline,'*')-1
  if (iline.lt.0) iline=12
  fline = iline.ne.0
  !
  !		obs. number
  !
  fnum=.false.
  xnum=0
  ynum=2147483647
  ! /NUMBER
  if (sic_present(3,0)) then
    argum1='*'
    call sic_ke(line,3,1,argum1,narg,.false.,error)
    if (error) return
    argum2=' '
    call sic_ke(line,3,2,argum2,narg,.false.,error)
    if (error) return
    if (argum1(1:1).eq.'*') then
      xnum=1
    else
      call sic_i4 (line,3,1,xnum,.false.,error)
      if (error) return
      fnum=.true.
    endif
    if (argum2(1:1).eq.' ') then
      ynum=xnum
    elseif (argum2.ne.'*') then
      call sic_i4(line,3,2,ynum,.false.,error)
      if (error) return
      fnum=.true.
    endif
  ! SET NUMBER
  elseif (snume1.ne.0 .or. snume2.ne.2147483647) then
    fnum=.true.
    xnum=snume1
    ynum=snume2
  endif
  !
  !		obs_date
  !
  ! SET OBSERVED
  if (sobse1.ne.-32768 .or. sobse2.ne.32767) then
    fdobs=.true.
    xdobs=sobse1
    ydobs=sobse2
  else
    fdobs=.false.
    xdobs=-32768
    ydobs=32767
  endif
  !
  !		offset
  !
  foff1=.false.
  foff2=.false.
  xoff1=fourpi
  xoff2=fourpi
  yoff1=fourpi
  yoff2=fourpi
  !
  ! SET OFFSET	or 	SET RANGE
  if (.not.foff1 .and.   &
    (soffs1.ne.fourpi .or. soffl1.ne.fourpi)) then
    foff1=.true.
    xoff1=soffs1
    if (xoff1.ne.fourpi) xoff1 = xoff1-stole
    yoff1=soffl1
    if (yoff1.ne.fourpi) yoff1 = yoff1+stole
  endif
  if (.not.foff2 .and.   &
    (soffs2.ne.fourpi .or. soffl2.ne.fourpi)) then
    foff2=.true.
    xoff2=soffs2
    if (xoff2.ne.fourpi) xoff2 = xoff2-stole
    yoff2=soffl2
    if (yoff2.ne.fourpi) yoff2 = yoff2+stole
  endif
  !
  ! /OFFSET
  if (sic_present(5,0)) then
    argum1='*'
    call sic_ke(line,5,1,argum1,narg,.false.,error)
    if (error) return
    if (argum1(1:1).ne.'*') then
      call sic_r4(line,5,1,xoff1,.true.,error)
      if (error) return
      foff1=.true.
      xoff1=xoff1/fangle
      yoff1=xoff1+stole
      xoff1=xoff1-stole
    endif
    argum2='*'
    call sic_ke(line,5,2,argum2,narg,.false.,error)
    if (error) return
    if (argum2(1:1).ne.'*') then
      call sic_r4(line,5,2,xoff2,.true.,error)
      if (error) return
      foff2=.true.
      xoff2=xoff2/fangle
      yoff2=xoff2+stole
      xoff2=xoff2-stole
    endif
  endif
  !
  ! source
  ! /SOURCE Source
  if (sic_present(6,0)) then
    xnsour = sic_narg(6)
    if (xnsour.gt.m_sour) then
      write(nombre,'(I0)') m_sour
      call message(6,2,'CLIC_FIND','Only '//nombre//' sources retained')
      xnsour = min(xnsour,m_sour)
    endif
    do i=1,xnsour
      !            CALL SIC_CH(LINE,6,I,ARGUM1,NARG,.TRUE.,ERROR)
      call sic_ke(line,6,i,argum1,narg,.true.,error)
      if (error) return
      xisour(i) = index(argum1,'*')-1
      if (xisour(i).ne.0) then
        if (xisour(i).lt.0) xisour(i) = 12
        xssour(i) = argum1
      endif
    enddo
    do i=1,xnsour
      if (xisour(i).eq.0) xnsour = 0
    enddo
  ! SET SOURCE
  else
    do i=1,nsour
      xssour(i) = sssour(i)
      xisour(i) = sisour(i)
    enddo
    xnsour = nsour
  endif
  fsourc = xnsour.ne.0
  fitype = xitype.ge.0
  !
  ! Set proc
  if (sic_present(7,0)) then
    xnproc = sic_narg(7)
    if (xnproc.gt.m_proc) then
      write(nombre,'(I0)') m_proc
      call message(6,2,'CLIC_FIND','Only '//nombre//' procedures retained')
      xnproc = min(xnproc,m_proc)
    endif
    do i=1,xnproc
      argum = ' '
      call clic_kw(line,7,i,argum1,nkey,voc2,mvoc2,.true.,error,.true.)
      if (error) then
        xsproc(i) = 0
      endif
      fproc = .true.
      if (nkey.le.mvoc2-2) then
        xsproc(i) = nkey+10
      elseif (argum1.eq.'SKYDIP') then
        xsproc(i) = 6
      elseif (argum1.eq.'*') then
        fproc = .false.
      endif
    enddo
  else
    fproc = nproc.gt.0
    do i=1,nproc
      xsproc(i) = ssproc(i)
    enddo
    xnproc = nproc
  endif
  !
  ! SET EXCLUDE
  if (sic_present(15,0)) then
    xnexcl = sic_narg(15)
    if (xnexcl.gt.m_excl) then
      write(nombre,'(I0)') m_excl
      call message(6,2,'CLIC_FIND','Only '//nombre//' exclusions retained')
      xnexcl = min(xnexcl,m_excl)
    endif
    do i=1,xnexcl
      argum = ' '
      call clic_kw(line,15,i,argum1,nkey,voc2,mvoc2,.true.,error,.true.)
      if (error) then
        xsexcl(i) = 0
      endif
      fexcl = .true.
      if (nkey.le.mvoc2-2) then
        xsexcl(i) = nkey+10
      elseif (argum1.eq.'SKYDIP') then
        xsexcl(i) = 6
      elseif (argum1.eq.'*') then
        fexcl = .false.
      endif
    enddo
  else
    fexcl = nexcl.gt.0
    do i=1,nexcl
      xsexcl(i) = ssexcl(i)
    enddo
    xnexcl = nexcl
  endif
  !
  ! Quality
  !
  xqual = squal
  call sic_i4(line,8,1,xqual,.false.,error)
  if (error) return
  if (xqual.lt.0 .or. xqual.gt.9) then
    call message(6,3,'FIND','Quality out of range 0-9')
    error = .true.
    return
  endif
  !
  !		reduction_date
  !
  if (sredu1.ne.-32768 .or. sredu2.ne.32767) then
    fdred=.true.
    xdred=sredu1
    ydred=sredu2
  else
    fdred=.false.
    xdred=-32768
    ydred=32767
  endif
  !
  ! /SCAN
  fscan=.true.
  xnscan = 1
  xscan(1)=0
  yscan(1)=2147483647
  if (sic_present(4,0)) then
    xnscan = 0
    do k=1, sic_narg(4), 2
      argum1='*'
      call sic_ke(line,4,k,argum1,narg,.false.,error)
      if (error) return
      argum2=' '
      call sic_ke(line,4,k+1,argum2,narg,.false.,error)
      if (error) return
      xnscan = xnscan+1
      if (argum1(1:1).eq.'*') then
        xscan(xnscan) = 0
      else
        call sic_i4 (line,4,k,xscan(xnscan),.false.,error)
        if (error) return
        fscan=.true.
      endif
      if (argum2(1:1).eq.' ') then
        yscan(xnscan)=xscan(xnscan)
      elseif (argum2.ne.'*') then
        call sic_i4(line,4,k+1,yscan(xnscan),.false.,error)
        if (error) return
        fscan=.true.
      endif
    enddo
  ! SET SCAN
  elseif (sscan1(1).ne.0 .or. sscan2(1).ne.2147483647) then
    fscan=.true.
    do k=1, snscan
      xscan(k) = sscan1(k)
      yscan(k) = sscan2(k)
    enddo
    xnscan = snscan
  endif
  !
  ! telescope
  ! /TELESCOPE
  if (sic_present(9,0)) then
    xctele='*'
    call sic_ke(line,9,1,xctele,narg,.false.,error)
    if (error) return
  else
    xctele = steles
  endif
  iteles = index(xctele,'*')-1
  if (iteles.lt.0) iteles=12
  fteles = iteles.ne.0
  !
  ! Polarization
  jopt = 10
  if (sic_present(jopt,0)) then
    fpol = .true.
    xnpol = 0
    do k=1,sic_narg(jopt)
      xnpol = xnpol + 1
      call sic_ke(line,jopt,k,argum1,narg,.false.,error)
      if (argum1(1:1).eq.'*') then
        fpol = .false.
      else
        call sic_i4(line,jopt,k,xpol(xnpol),.true.,error)
        if (error) return
        if (xpol(xnpol).lt.0.or.xpol(xnpol).gt.mnpolscan) then
          call message(6,3,'FIND','Invalid polarisation subscan')
          error = .true.
          return
        endif
      endif
    enddo
  elseif(snpol.gt.0) then
    fpol = .true.
    xnpol=snpol
    do k=1, snpol
      xpol(k) = spol(k)
    enddo
  else
    fpol = .false.
  endif
  !
  ! Receiver
  jopt = 11
  if (sic_present(jopt,0)) then
    xrecei = -1
    call sic_ke(line,jopt,1,argum1,narg,.true.,error)
    if (error) return
    if (argum1.eq.'*') then
      xrecei = -1
    else
      call sic_i4(line,jopt,1,xrecei,.true.,error)
      if (error) return
    endif
  else
    xrecei = srecei
  endif
  !
  ! Correlator mode
  jopt = 12
  if (sic_present(jopt,0)) then
    fmod = .true.
    call clic_kw(line,jopt,1,argum1,xmod,vmod,mmod,.false.,error,.true.)
    if (error) return
  else
    fmod = .false.
  endif
  !
  ! Load
  jopt = 13
  if (sic_present(jopt,0)) then
    fload = .true.
    call clic_kw(line,jopt,1,argum1,xload,vload,mload,.false.,error,.true.)
    if (xload.eq.1) xload = 7
    if (xload.eq.2) xload = 4  
    if (error) return
  else
    fload = .false.
  endif
  !
  ! Skip autocorrelations
  fskip = .false.
  if (sic_present(14,0).or.iskip.ne.0) fskip = .true.
  !
  ! Project
  xcproject = sproject
  iproject = index(xcproject,'*')-1
  if (iproject.lt.0) iproject = 8
  fproject = iproject.ne.0
  !
  ! Hour Angle		(no specific option)
  if (shoura1.gt.-pi .or. shoura2.lt.pi) then
    fhoura = .true.
    xhoura = shoura1
    yhoura = shoura2
  else
    fhoura = .false.
  endif
  !
  ! UT		(no specific option)
  if (sut1.gt.0 .or. sut2.lt.2*pi) then
    fut = .true.
    xut = sut1
    yut = sut2
  else
    fut = .false.
  endif
  !
  ! Calibration status
  if (sbpc.ge.0) then
    xbpc = sbpc
    fbpc = .true.
  else
    fbpc = .false.
  endif
  if (setic.ge.0) then
    xic = setic
    fic = .true.
  else
    fic = .false.
  endif
  !
  ! APPEND NEW arguments
  append = .false.
  new_wanted = .false.
  upd_wanted = .false.
  do i=1,sic_narg(0)
    argum = ' '
    call clic_kw(line,0,i,argum,nkey,vocab,mvocab,.false.,error,.true.)
    if (error) return
    if (nkey.eq.1) then
      append = .true.
    elseif (nkey.eq.2) then
      new_wanted = .true.
    else
      upd_wanted = .true.
    endif
  enddo
  !
  ! if not FIND NEW, check if default is UPDATE or not
  if (.not.new_wanted) then
    upd_wanted = find_update.or.upd_wanted
  elseif (upd_wanted) then
    call message(6,3,'FIND','Conflicting arguments NEW and UPDATE')
    error = .true.
    return
  endif
  if (upd_wanted) then
    call nix(error)
    if (error) goto 999
  endif
  !
  ! Zeroes the current index by default, except if APPEND
  if (.not.append) then
    cxnext = 1
    knext = 0
  endif
  cxnext0 = cxnext
  nnn = m_cx - cxnext0 + 1
  !
  ! IFIND is dummy
  error = .false.
  call fix(nnn,ifind,.true.,new_wanted,error)
  if (error) goto 999
  !
  ! Output messages
  if (nnn.ne.0) then
    if (title%kind.eq.5) then
      call message(1,1,'FIND','New generation receivers data')
      new_receivers = .true.
    elseif (title%kind.eq.4) then
      call message(5,1,'FIND','Old receivers data (<2007)')
      new_receivers = .false.
    else
      call message(5,1,'FIND','Single-dish data')
      new_receivers = .false.
    endif
  endif
  if (nnn.eq.1) then
    call message(5,1,'FIND','1 observation found')
    nombre = '1'
  elseif (nnn.ne.0) then
    write(nombre,'(I10)') nnn
    call message(5,1,'FIND',nombre//' observations found')
  else
    call message(6,2,'FIND','Nothing found')
    goto 999
  endif
  !
  ! Compress the current index if needed
  if (cxnext0.gt.1 .and. cxnext.gt.cxnext0) then
    k = cxnext0
    do i=cxnext0, cxnext-1
      do j=1, cxnext0-1
        if (cx_ind(j).eq.cx_ind(i)) goto 100
      enddo
      cx_ind (k) = cx_ind(i)
      cx_num (k) = cx_num(i)
      cx_ver (k) = cx_ver(i)
      cx_bloc(k) = cx_bloc(i)
      cx_word(k) = cx_word(i)
      k = k+1
100 enddo
    cxnext = k
    !
    ! Do not update NINDEX here , otherwise INDEX_VARIABLE
    ! routine will not update CX_NUM
    ! NINDEX = CXNEXT-1               ! #4
    ! WRITE(NOMBRE,'(I10)') NINDEX
    !
    write(nombre,'(I10)') cxnext-1
    call message(5,1,'FIND',nombre//' observations in index')
  endif
  !
  ! Sort index
  call sort_index(error)
  !
  ! Restore the current short title
999 title = title_save
  call index_variable
  !
  ! Force reading data
  change_display = .true.
  find_needed = .false.
  return
end subroutine find
!
subroutine nix(error)
  use gkernel_interfaces
  use gbl_format
  use gbl_constant
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! LAS
  !	Examine input index, to see if any change occured.
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: ier, errcount
  integer(kind=8) :: ii
  character(len=80) :: mess
  !------------------------------------------------------------------------
  ! Code
  errcount = 0
10 continue
  !
  call classic_filedesc_read(i,error)
  if (error) goto 11
  !
  ! New data
  if (old_ixnext.lt.i%desc%xnext) then
    close(unit=i%lun)
    open (unit=i%lun,file=i%spec(1:i%nspec),access='DIRECT',   &
      recl=facunf*i%desc%reclen,status='OLD',iostat=ier)

    if (ier.ne.0) goto 20
    call classic_filedesc_read(i,error)
    if (error) goto 11
    ! Enforce re-reading of "index" buffer
    call classic_recordbuf_nullify(ibufi)
    ! Enforce re-reading of observation buffer
    ! Needed from V2, since 2 observations can share a common record
    call classic_recordbuf_nullify(ibuff)
    ! 
    do ii = max(1,old_ixnext), i%desc%xnext-1
      call rix (ii,error)
      if (error) return
      old_ixnext  = ii+1
      ix_num(ii)  = title%num
      ix_bloc(ii) = title%bloc
      ix_word(ii) = title%word
      ix_ver(ii)  = title%ver
      ix_kind(ii) = title%kind
      ix_qual(ii) = title%qual
    enddo
  endif
  return
  !
  ! Various errors
11 if (errcount.eq.nint(interval)) then
    error = .true.
    mess = 'Read error file '//i%spec(1:i%nspec)
    call message(8,4,'NIX',mess)
    call messios(8,4,'NIX',ier)
    return
  endif
  errcount = errcount+1
  call sic_wait(1.0)
  goto 10
20 error = .true.
  call messios(6,3,'NIX',ier)
  call message(6,2,'NIX','No input file opened')
end subroutine nix
