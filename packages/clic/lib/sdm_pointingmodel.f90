subroutine write_sdm_pointingModel(error)
  !---------------------------------------------------------------------
  !     Write the Poinitng Model SDM table.
  !     (very much *TBD* at present) but required soon
  !     int sdm_addPointingModelRow ()
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  integer sdm_addPointingModelRow, ireturn
  integer ia
  character sdmTable*16
  parameter (sdmTable='PointingModel')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  do ia = 1, r_nant
    ireturn =  sdm_addPointingModelRow()
    if (ireturn.lt.0) then
      call sdmMessageI(8,3,sdmTable   &
        ,'Error in sdm_addPointingModelRow',ireturn)
      goto 99
    endif
    PointingModel_Id(ia) = ireturn
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_pointingModel
!---------------------------------------------------------------------
