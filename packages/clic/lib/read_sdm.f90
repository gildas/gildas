subroutine read_sdm(line,error)
  use gkernel_interfaces
  !------------------------------------------------------------------------
  ! Support READ FILE
  ! read  a SDM data set in ALMA SDM format
  !------------------------------------------------------------------------
  use gildas_def
  use gkernel_types
  use sdm_Main
  use sdm_State
  use sdm_TotalPower
  use sdm_configDescription
  ! Dummy variables:
  character*(*) line
  logical error
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Local variables:
  integer :: ier, mainSize, tpmSize, hSize, nch, num, i, j, cdSize
  character (len=filename_length) :: fich
  logical :: first, check, present
  integer :: lfich
  type (ConfigDescriptionKey) :: cdKey
  type (ConfigDescriptionRow) :: cdRow
  type (cputime_t) :: time
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Verify if any output file
  call check_output_file(error)
  if (error) return
  write_mode = 'NEW'
  check = .true.
  r_presec(rfset_sec)= .true.
  r_presec(contset_sec)= .true.
  r_presec(lineset_sec)= .true.
  r_presec(atparm_sec)= .true.
  r_presec(atmon_sec)= .true.
  r_presec(scanning_sec)= .true.
  r_presec(atparm_sec)= .true.
  r_presec(alma_sec)= .true.
  r_presec(abpcal_sec)= .true.
  r_presec(aical_sec)= .true.
  r_presec(bpcal_sec)= .true.
  r_presec(ical_sec)= .true.
  r_icdeg = 3
  r_aicdeg = 3
  !
  ! Get file name
  fich = 'clicFile' 
  call sic_ch(line,0,1,fich,nch,.false.,error)
  if (error) return
  call sic_parse_file(fich,'','',fich)
  !
  lfich = lenc(fich)
  file_name = fich(1:lfich)//char(0) 
  call reset_times(time)
  call sdm_init(fich(1:lfich)//char(0), error)
  if (error) goto 99
  call message_times(time,'Sdm_init')
  !
  next_Uid = 0
  ! reset all local identifiers
  call init_sdm
  !
  !     Scan the main table
  !
  !     should als check ths scan table to discard the calDMs which have
  !     no scan table
  call getMainTableSize(mainSize, error)
  ! print *,'mainSize ', mainSize
  call getTotalPowerTableSize(tpmSize, error)
  ! print *, 'tpmSize ', tpmSize
  !
  if (mainSize.gt.0) then
    call read_sdm_Tables(error)
  elseif (tpmSize.gt.0) then
    call getHolographyTableSize(hSize, error)
    if (hSize.gt.0) then
      call message(2,1,'Read_sdm'   &
        ,'This is a Single-dish Holography Data Set')
      call read_sdm_HoloTables(error)
    else
      call message(8,4,'Read_sdm'   &
        ,'Unidentified non-correlator mode !')
      error = .true.
    endif
  endif
99 return
end subroutine read_sdm
!--------------------------------------------------------------------
subroutine message_times(time,where)
  use gkernel_interfaces
  use gkernel_types
  type(cputime_t), intent(inout) :: time
  character ::  chain*160, where*(*)
  integer :: l
  !
  call gag_cputime_get(time)
  !
  write (chain,1000) time%diff%user + time%diff%system,  &
                     time%curr%user + time%curr%system,  &
                     time%diff%elapsed,                  &
                     time%curr%elapsed
1000 format ('CPU ',f8.2,'/',f8.2,' s. Elapsed ',f8.2, '/',f8.2   &
    ,' s: ')
  l = lenc(chain)
  chain = chain(1:l)//' '//where
  l = lenc(chain)
  call message(2,1,'Timing',chain(1:l))
  return
end subroutine message_times
!--------------------------------------------------------------------
subroutine reset_times(time)
  use gkernel_interfaces
  use gkernel_types
  type(cputime_t), intent(inout) :: time
  !
  call gag_cputime_init(time)
  !
end subroutine reset_times
