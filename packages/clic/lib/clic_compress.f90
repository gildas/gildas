subroutine clic_compress(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_constant.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: data_in, vm, ip_data, ipvm, avm
  integer :: nout, kvm, lvm, ktri, narg, mvoc
  integer(kind=data_length) :: ldata_in, ndata
  integer :: iarg, nkey
  logical :: end, triangle
  real :: time_max, uv_max
  character(len=80) :: ch
  logical :: switch
  parameter (mvoc=2)
  character(len=12) :: arg,kw,voc(mvoc)
  data voc/'SWITCH','TIME'/
  !------------------------------------------------------------------------
  save lvm, avm
  ! Code:
  call check_input_file(error)
  if (error) return
  call check_output_file(error)
  if (error) return
  call check_index(error)
  if (error) return
  triangle = sic_present(1,0)
  if (.not.do_write_data) then
    call message (8,4,'COMPRESS',   &
      'COMPRESS needs the SET WRITE DATA mode')
    error = .true.
    return
  endif
  !
  ! Initialize
  arg = 'TIME'
  lvm = 0
  time_max = 60.               ! seconds
  uv_max = 7.5                 ! meters
  switch = .false.
  iarg = 1
  call sic_ke(line,0,1,arg,narg,.false.,error)
  if (ichar(arg(1:1)).gt.ichar('9')) then
    call sic_ambigs('COMPRESS',arg,kw,nkey,voc,mvoc,error)
    if (error) return
    if (kw.eq.'SWITCH') then
      iarg = 2
      switch =.true.
      time_max = -1.
    endif
  endif
  call sic_r4(line,0,iarg,time_max,.false.,error)
  if (error) return
  call sic_r4(line,0,iarg+1,uv_max,.false.,error)
  if (error) return
  !
  ! Loop on current index
  call get_first(.true.,error)
  if (error) return
  end = .false.
  do while (.not.end)
    !
    ! Get input data
    call get_data (ldata_in,data_in,error)
    if (error) goto 999
    !
    ! Compute triangle products
    ktri = r_nant*(r_nant-1)*(r_nant-2)/6
    if (triangle .and. ktri.gt.0 .and. r_lmode.eq.1) then
      if (ktri.gt.r_ntri) then
        kvm = r_ndump*r_ldump/(r_nbas+r_ntri)*(r_nbas+ktri)
        if (kvm.gt.lvm) then
          if (lvm.ne.0) call free_vm(lvm,avm)
          lvm = 0
          if (sic_getvm(kvm,avm).ne.1) then
            error = .true.
            return
          endif
          lvm = kvm
          vm = avm
        endif
      else
        vm = data_in
      endif
      ip_data = gag_pointer(data_in,memory)
      ipvm = gag_pointer(vm,memory)
      call triangles (memory(ip_data),memory(ipvm),error)
      if (error) goto 999
    else
      vm = data_in
      ipvm = gag_pointer(vm,memory)
    endif
    !
    ! Compress
    if (time_max.ne.0) then
      call compress(time_max, uv_max, memory(ipvm),nout,   &
        error)
      if (error) goto 999
      r_ndump = nout
    endif
    ! r_ndump temporal data + (1 or 2) spectral  data
    ndata = r_ndump*r_ldump + max(1,r_ndatl)*(r_ldump+r_ldatl)
    !
    ! Write
    r_bpcdeg = 20              !
    r_presec(bpcal_sec) = .true.   !
    r_icdeg = 3                !
    r_presec(ical_sec) = .true.    !
    r_abpcdeg = 20             !
    r_presec(abpcal_sec) = .true.  !
    r_aicdeg = 3               !
    r_presec(aical_sec) = .true.   !
    call ipb_write('NEW',.true.,error)
    call wdata (ndata,memory(ipvm),.false.,error)
    if (error) goto 999
    call ipb_close(error)
    if (error) goto 200
    call loose_data
    write(ch,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
      ' written (',r_ndump,') records'
    call message(4,1,'COMPRESS',ch(1:lenc(ch)))
    !
    ! Next observation
200 if (sic_ctrlc()) then
      goto 999
    endif
    call get_next(end,error)
    if (error) goto 999
  enddo
  if (lvm.gt.0) call free_vm(lvm,vm)
  return
  !
999 error = .true.
  if (lvm.gt.0) call free_vm(lvm,vm)
  return
end subroutine clic_compress
!
subroutine triangles (in,out,error)
  use classic_api
  !---------------------------------------------------------------------
  ! Call Tree
  !	CLIC_SPECTRUM
  !	CLIC_COMPRESS
  !---------------------------------------------------------------------
  real :: in(*)                     !
  real :: out(*)                    !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_number.inc'
  include 'clic_par.inc'
  ! Local
  integer :: nt, kin, kout, k2, i, j, ic, it, ir
  integer :: kb1, kb2, kb3
  complex :: z
  !------------------------------------------------------------------------
  ! Code:
  r_ntri = r_nant*(r_nant-1)*(r_nant-2)/6
  do i=1, r_ntri
    do j=1, 3
      r_atri(j,i) = anttri(j,i)
    enddo
  enddo
  !
  ! Loop on records (including average record)
  kin = 1
  kout = 1
  do ir = 1, r_ndump + 1
    ! data header
    call r4tor4(in(kin), out(kout), r_ldpar)
    kin = kin + r_ldpar
    kout = kout + r_ldpar
    ! continuum
    nt = 2*r_nband * 2         ! number of channels/baseline
    call r4tor4(in(kin), out(kout), r_nbas*nt)
    kin = kin + r_nbas*nt
    kout = kout + r_nbas*nt
    ! triangle products
    do it = 1, r_ntri
      k2 = kout+r_nbas*nt
      kb1 = kin + nt*(bastri(1,it)-1)
      kb2 = kin + nt*(bastri(2,it)-1)
      kb3 = kin + nt*(bastri(3,it)-1)
      do ic=1, nt, 2
        z = cmplx(in(kb1),in(kb1+1))   &
          * cmplx(in(kb2),-in(kb2+1)) * cmplx(in(kb3),in(kb3+1))
        out(k2) = real(z)
        out(k2+1) = aimag(z)
        k2 = k2 + 2
        kb1 = kb1 + 2
        kb2 = kb2 + 2
        kb3 = kb3 + 2
      enddo
    enddo
    kin = kin + r_nbas*nt
    kout = kout + (r_nbas+r_ntri)*nt
  enddo
  r_ldatc = (r_nbas+r_ntri)*r_nband*2*2
  r_ldump = r_ldpar + r_ldatc
  ! Line
  nt = 2*r_lntch * 2           ! number of channels/baseline
  call r4tor4(in(kin), out(kout), r_nbas*nt)
  kin = kin + r_nbas*nt
  kout = kout + r_nbas*nt
  ! triangle products
  do it = 1, r_ntri
    k2 = kout+r_nbas*nt
    kb1 = kin + nt*(bastri(1,it)-1)
    kb2 = kin + nt*(bastri(2,it)-1)
    kb3 = kin + nt*(bastri(3,it)-1)
    do ic=1, nt, 2
      z = cmplx(in(kb1),in(kb1+1))   &
        * cmplx(in(kb2),-in(kb2+1)) * cmplx(in(kb3),in(kb3+1))
      out(k2) = real(z)
      out(k2+1) = aimag(z)
      k2 = k2 + 2
      kb1 = kb1 + 2
      kb2 = kb2 + 2
      kb3 = kb3 + 2
    enddo
  enddo
  kin = kin + r_nbas*nt
  kout = kout + (r_nbas+r_ntri)*nt
  r_ldatl = (r_nbas+r_ntri)*r_lntch*2*2
  return
end subroutine triangles
!
subroutine compress(time_max, uv_max, in, nout, error)
  use classic_api
  !---------------------------------------------------------------------
  !	Compress data records (temporal data only)
  ! IN	Input and output data
  ! NOUT  Output number of records
  !       time_max = -1. -> compress beam switch...
  !
  ! Call Tree
  !	CLIC_SPECTRUM
  !	CLIC_COMPRESS
  !	CLIC_TABLE
  !---------------------------------------------------------------------
  real :: time_max                  !
  real :: uv_max                    !
  real :: in(*)                     !
  integer :: nout                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'clic_work.inc'
  include 'clic_dcomp.inc'
  ! Local
  real*4 :: aveamp(2,mnbas), sigamp(2,mnbas), avepha(2,mnbas)
  real*4 :: oldpha(2,mnbas), wswpos
  common /averag/ aveamp,avepha,oldpha, sigamp, wswpos
  save /averag/
  !
  integer :: kin, i, ir, kout, fltime, iant, isw
  real :: wsw
  logical :: change,do_flag
  ! Work space
  real :: buffer(4*mnbas*mcch)
  equivalence (buffer,data_buffer)
  real :: old_utc,old_integ, test
  !------------------------------------------------------------------------
  ! Code:
  nout = 0
  !
  ! Initialize first output record
  call zero_record (r_ldatc, buffer)
  !
  ! Loop on records
  do_flag = .false.
  do ir = 1, r_ndump
    !
    ! Next record
    kin = (ir-1)*r_ldump
    call decode_header (in(kin+1))
    !
    ! Check for time discontinuity:
    if (ir.gt.1) then
      test = dh_utc-dh_integ/2.-old_utc-old_integ/2.
      test = mod(test+129600.,86400.)-43200.
      !C
      !C Forced to zero, since COMPRESS is not used for compressing
      !C data but only in real time; then we do'nt want TIME flags
      !C to occur just because of VETOed dumps.
      !C
      test = 0
      !C
      if (abs(test).gt.0.1) then
        if (test.gt.0) then
          call message(6,2,'COMPRESS','Time reset by ONEMN,'   &
            //' previous scan may be wrong')
        endif
        fltime = 0
        do iant=1,r_nant
          fltime = ior(fltime,dh_aflag(iant))
        enddo
        fltime = iand(fltime,ishft(1,26-1))
        if (fltime.eq.0) then
          call message(8,4,'COMPRESS','Time discontinuity'   &
            //' detected: setting TIME flag')
          do iant=1,r_nant
            dcomp_aflag(iant) = ior(dcomp_aflag(iant),   &
              ishft(1,26-1))
          enddo
          do_flag = .true.
        else
          call message(6,2,'COMPRESS','Time discontinuity'   &
            //' detected & TIME flag set: proceeding...')
        endif
      endif
    endif
    !
    old_utc = dh_utc
    old_integ = dh_integ
    !
    ! Check for change
    if (r_nswitch.gt.0) then
      isw = dh_test0(1)
      wsw = r_wswitch(isw)
    else
      isw = 0
      wsw = 0
    endif
    change = dcomp_integ .ge. abs(time_max)
    if (dcomp_integ.gt.0) then
      if (.not.change) then
        do i=1, r_nant
          !c                  CHANGE = CHANGE .OR. DCOMP_AFLAG(I).NE.DH_AFLAG(I)
          change = change .or. dcomp_gamme(i).ne.dh_gamme(i)
          change = change .or.   &
            abs(dcomp_offfoc(i)/dcomp_integ-dh_offfoc(i)).gt.0.1   !mm
          ! Changed to 5 arc seconds RL 14-sep-1992
          ! ignore reference offsets of beam switch (the weight is negative)
          ! (RL 2003-09-24)
          ! force a change on first state of beam switch ...
          if (time_max.lt.0) then
            change = change .or. isw.eq.1
          endif
          if (wsw.ge.0) then
            change = change .or.   &
              abs(dcomp_offlam(i)/wswpos-dh_offlam(i))   &
              .gt.5            !sec
            change = change .or.   &
              abs(dcomp_offbet(i)/wswpos-dh_offbet(i))   &
              .gt.5            !sec
          endif
          change = change .or.   &
            abs(dcomp_delay(1,i)/dcomp_integ-dh_delay(1,i))   &
            .gt.0.05           !ns
          change = change .or.   &
            abs(dcomp_delay(2,i)/dcomp_integ-dh_delay(2,i))   &
            .gt.0.05           !ns
          change = change .or.   &
            abs(dcomp_phase(i)/dcomp_integ-dh_phase(i)).gt.0.017   ! rad
        enddo
      endif
      !c            IF (.NOT.CHANGE) THEN
      !c               DO I=1, R_NBAS
      !c                  CHANGE = CHANGE .OR. DCOMP_BFLAG(I).NE.DH_BFLAG(I)
      !c               ENDDO
      !c            ENDIF
      if (.not.change) then
        do i=1, r_nbas
          change = change .or.   &
            ((dcomp_uvm(1,i)/dcomp_integ-dh_uvm(1,i))**2   &
            +(dcomp_uvm(2,i)/dcomp_integ-dh_uvm(2,i))**2)   &
            .gt.uv_max**2
        enddo
      endif
    endif
    !
    ! New output record
    if (change .and. dcomp_integ .gt.0) then
      !
      kout = nout*r_ldump + r_ldpar + 1
      call output_record (r_ldatc, in(kout), buffer, nout)
      kout = kout - r_ldpar
      call r4tor4 (dcomp_void, dh_void, m_dh)
      call encode_header (in(kout))
      !
      ! Initialise next record and recover header
      call zero_record (r_ldatc, buffer)
      call decode_header (in(kin+1))
    endif
    !
    ! Increment
    call add_record (r_ldatc,in(kin+r_ldpar+1), buffer)
  enddo
  !
  ! Last output record
  kout = nout*r_ldump + r_ldpar + 1
  if (dcomp_integ.gt.0) then
    call output_record (r_ldatc,in(kout), buffer, nout)
    kout = kout - r_ldpar
    call r4tor4 (dcomp_void, dh_void, m_dh)
    call encode_header (in(kout))
  endif
  !
  ! Line data
  if (nout.ne.r_ndump) then
    ! uncorrected:
    kin = r_ndump*r_ldump+1
    call decode_header (in(kin))
    call r4tor4 (dcomp_rmsamp,dh_rmsamp,2*r_nbas)
    call r4tor4 (dcomp_rmspha,dh_rmspha,2*r_nbas)
    kout = nout*r_ldump+1
    ! Check for high amplitude data points:
    call check_data  (in(kin+r_ldpar),r_ldatc+r_ldatl)
    if (do_flag) then
      do iant=1,r_nant
        dh_aflag(iant) = ior(dh_aflag(iant),ishft(1,26-1))
      enddo
      do_flag = .false.
    endif
    call r4tor4 (in(kin+r_ldpar),in(kout+r_ldpar),r_ldatc+r_ldatl)
    call encode_header (in(kout))
    !
    ! Corrected:
    if (r_ndatl.gt.1) then
      kin = (r_ndump+1)*r_ldump+r_ldatl+1
      call decode_header (in(kin))
      call r4tor4 (dcomp_rmsamp,dh_rmsamp,2*r_nbas)
      call r4tor4 (dcomp_rmspha,dh_rmspha,2*r_nbas)
      kout = (nout+1)*r_ldump+r_ldatl+1
      ! Check for high amplitude data points:
      call check_data  (in(kin+r_ldpar),r_ldatc+r_ldatl)
      if (do_flag) then
        do iant=1,r_nant
          dh_aflag(iant) = ior(dh_aflag(iant),ishft(1,26-1))
        enddo
        do_flag = .false.
      endif
      call r4tor4 (in(kin+r_ldpar),in(kout+r_ldpar),   &
        r_ldatc+r_ldatl)
      call encode_header (in(kout))
    endif
  endif
  return
end subroutine compress
!
subroutine check_data (data, qdata)
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Check data and flag subbands if absolute value greater than vmax
  !---------------------------------------------------------------------
  integer :: qdata                  !
  real :: data(qdata)               !
  ! Global
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_dcomp.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: aflag(mnant), bflag(mnbas), iant, ibas, isub, isb, j, k
  integer :: ic, mask
  character(len=255) :: out
  logical :: flagged
  real :: vmax
  ! Data
  data vmax/1e10/
  !------------------------------------------------------------------------
  ! Code:
  ! Correlation data:
  if (r_lmode.eq.1) then
    ! Continuum data:
    k = 1
    do iant = 1, r_nant
      aflag(iant) = 0
    enddo
    !
    do ibas = 1, r_nbas
      bflag(ibas) = 0
      do isb = 1, 2
        do isub = 1, r_nband
          mask = ishft(1,isub-1)
          do j=1, 2
            if (data(k).gt.vmax) then
              bflag(ibas) = ior(bflag(ibas),mask)
            elseif (data(k).lt.-vmax) then
              bflag(ibas) = ior(bflag(ibas),mask)
            endif
            k = k+1
          enddo
        enddo
      enddo
    enddo
    if (k.lt.qdata) then
      ! Line data:
      do ibas = 1, r_nbas
        do isb = 1, 2
          do isub = 1, r_lband
            mask = ishft(1,isub+10-1)
            do ic = 1, r_lnch(isub)
              do j=1, 2
                if (data(k).gt.vmax) then
                  bflag(ibas) = ior(bflag(ibas),mask)
                elseif (data(k).lt.-vmax) then
                  bflag(ibas) = ior(bflag(ibas),mask)
                endif
                k = k+1
              enddo
            enddo
          enddo
        enddo
      enddo
    endif
    ! Message
    flagged = .false.
    do ibas = 1, r_nbas
      if (bflag(ibas).ne.0) then
        dh_bflag(ibas) = ior(dh_bflag(ibas),bflag(ibas))
        flagged = .true.
      endif
    enddo
    if (flagged) then
      call list_flags(r_nant, r_nbas, aflag, bflag,out)
      call message(6,3,'CHECK_DATA','Flagging '//out(1:lenc(out)))
    endif
  ! Autocorrelation data:
  elseif (r_lmode.eq.2) then
    ! Continuum data:
    k = 1
    do ibas = 1, r_nbas
      bflag(ibas) = 0
    enddo
    !
    do iant = 1, r_nant
      aflag(iant) = 0
      do isub = 1, r_nband
        mask = ishft(1,isub-1)
        if (data(k).gt.vmax) then
          aflag(iant) = ior(aflag(iant),mask)
        elseif (data(k).lt.-vmax) then
          aflag(iant) = ior(aflag(iant),mask)
        endif
        k = k+1
      enddo
    enddo
    if (k.lt.qdata) then
      ! Line data:
      do iant = 1, r_nant
        do isub = 1, r_lband
          mask = ishft(1,isub+10-1)
          do ic = 1, r_lnch(isub)
            if (data(k).gt.vmax) then
              aflag(iant) = ior(aflag(iant),mask)
            elseif (data(k).lt.-vmax) then
              aflag(iant) = ior(aflag(iant),mask)
            endif
            k = k+1
          enddo
        enddo
      enddo
    endif
    ! Message
    flagged = .false.
    do iant = 1, r_nant
      if (aflag(iant).ne.0) then
        dh_aflag(iant) = ior(dh_aflag(iant),aflag(iant))
        flagged = .true.
      endif
    enddo
    if (flagged) then
      call list_flags(r_nant, r_nant, aflag, aflag,out)
      call message(6,3,'CHECK_DATA','Flagging '//out(1:lenc(out)))
    endif
  endif
end subroutine check_data
!
subroutine add_record (qdata, data, buffer)
  use classic_api
  !---------------------------------------------------------------------
  ! Call Tree
  !	CLIC_SPECTRUM
  !	CLIC_COMPRESS	COMPRESS
  !	CLIC_TABLE
  !---------------------------------------------------------------------
  integer :: qdata                  !
  real :: data(qdata)               !
  real :: buffer(qdata)             !
  ! Global
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_dcomp.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  real*4 :: aveamp(2,mnbas), sigamp(2,mnbas), avepha(2,mnbas)
  real*4 :: oldpha(2,mnbas), wswpos
  common /averag/ aveamp,avepha,oldpha, sigamp, wswpos
  save /averag/
  integer :: i, j, k, ia, ja, ipol, isb, ic, isig, isw
  logical :: down_channel
  complex :: z(2)
  real :: faz, newpha, wc, sigma, wsw, w
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  ! Put data flag on all antennas if integration time was zero:
  if (dh_integ.le.0) then
    dh_integ = 1.0
    do i=1, mnant
      dh_aflag(i) = ior(dh_aflag(i),ishft(1,24))
    enddo
  endif
  dcomp_integ  = dcomp_integ + dh_integ
  isw = dh_test0(1)
  if (r_nswitch.gt.0 .and. isw.gt.0 .and. isw.le.r_nswitch) then
    wsw = r_wswitch(isw)
    if (wsw.gt.0) then
      wswpos = wswpos + dh_integ
    endif
  else
    wsw = 0
  endif
  !
  !      if (dh_test0(2).ge.0) then
  !         dcomp_test0(2) = dcomp_test0(2) + DH_INTEG
  !      endif
  dcomp_utc = dcomp_utc + ((dh_obs-r_dobs)*86400.+dh_utc)*dh_integ
  dcomp_ntime = dcomp_ntime+dh_ntime*dh_integ
  do i=1,mntime
    dcomp_time_monitoring(i) = dcomp_time_monitoring(i) +   &
       dh_time_monitoring(i)*dh_integ
  enddo
  do i=1, 3
    dcomp_svec(i) = dcomp_svec(i) + dh_svec(i)*dh_integ
  enddo
  do i=1, r_nbb
    dcomp_vlbi_phaseff(i) = dcomp_vlbi_phaseff(i) + &
      dh_vlbi_phaseff(i)*dh_integ
  enddo
  do i=1, r_nant
    if (dh_dump.eq.1) then
      dcomp_aflag(i) = dh_aflag(i)
    else
      dcomp_aflag(i) = iand(dh_aflag(i),dcomp_aflag(i))
    endif
    do j=1, 2*mbands
      if (dh_dump.eq.1) then
        dcomp_saflag(j,i) = dh_saflag(j,i)
      else
        dcomp_saflag(j,i) = dh_saflag(j,i).or.dcomp_saflag(j,i)
      endif
    enddo
    do k = 1,mnbb
      if (wsw.ne.0) then
        dcomp_total(k, i) = dcomp_total(k, i) +   &
          dh_total(k, i)*dh_integ*wsw
      else
        dcomp_total(k, i) = dcomp_total(k, i) + dh_total(k, i)   &
          *dh_integ
      endif
    enddo
    !c      print *, isw, wsw, wswpos, DCOMP_TOTAL(1)
    do j=1,2
      do k=1,mnbb
        dcomp_atfac(k,j,i) = dcomp_atfac(k,j,i) +   &
          dh_atfac(k,j,i)*dh_integ
      enddo
    enddo
    ! do differently for beam switch (only average)
    if (r_nswitch.gt.0) then
      do j=1,2
        dcomp_rmspe(j,i) = dcomp_rmspe(j,i) +   &
          dh_rmspe(j,i)*dh_integ
      enddo
    elseif  (r_nswitch.le.0) then
      do j=1,2
        dcomp_rmspe(j,i) = dcomp_rmspe(j,i) +   &
          dh_rmspe(j,i)**2*dh_integ
      enddo
    endif
    dcomp_delcon(i) = dcomp_delcon(i) + dh_delcon(i)*dh_integ
    do j=1, mnbb ! Number of bband
       dcomp_dellin(j,i) = dcomp_dellin(j,i) + dh_dellin(j,i)*dh_integ
       dcomp_delay(j,i) = dcomp_delay(j,i) + dh_delay(j,i)*dh_integ
    enddo
    dcomp_delayc(i) = dcomp_delayc(i) + dh_delayc(i)*dh_integ
    dcomp_phasec(i) = dcomp_phasec(i) + dh_phasec(i)*dh_integ
    dcomp_phase(i) = dcomp_phase(i) + dh_phase(i)*dh_integ
    dcomp_ratec(i) = dcomp_ratec(i) + dh_ratec(i)*dh_integ
    dcomp_cable(i) = dcomp_cable(i) + dh_cable(i)*dh_integ
    dcomp_gamme(i) = dh_gamme(i)
    do k=1, 20
      dcomp_test1(k,i) = dcomp_test1(k,i) +   &
        dh_test1(k,i)*dh_integ
    enddo
    dcomp_offfoc(i) = dcomp_offfoc(i) + dh_offfoc(i)*dh_integ
    ! do not average postions with reference state of beam switching
    ! RL ATF 2003-09-24
    !         if (dh_test0(2).ge.0) then
    if (wsw.ge.0) then
      dcomp_offlam(i) = dcomp_offlam(i) + dh_offlam(i)*dh_integ
      dcomp_offbet(i) = dcomp_offbet(i) + dh_offbet(i)*dh_integ
    endif
    do k=1, r_wvrnch(i)
      dcomp_wvr(k,i) = dcomp_wvr(k,i) + dh_wvr(k,i)*dh_integ
    enddo
    dcomp_wvrstat(i) = ior(dcomp_wvrstat(i),dh_wvrstat(i))
    do j=1,mrlband
      dcomp_actp(1,i,j) = dcomp_actp(1,i,j) +   &
        dh_actp(1,i,j)*dh_integ
      dcomp_actp(2,i,j) = dcomp_actp(2,i,j) +   &
        dh_actp(2,i,j)*dh_integ
    enddo
    do k=1, r_nif
      dcomp_anttp(k,i) =  dcomp_anttp(k,i) +   &
        dh_anttp(k,i)*dh_integ
    enddo
    dcomp_cable_alternate(i) = dcomp_cable_alternate(i) +  &
      dh_cable_alternate(i)*dh_integ
    do j=1, 2 ! Inclinometer outputs
      dcomp_incli(i,j) = dcomp_incli(i,j) +  &
        dh_incli(i,j)*dh_integ
    enddo
    dcomp_incli_temp(i) = dcomp_incli_temp(i) +  &
      dh_incli_temp(i)*dh_integ
    do j=1, 3 ! Subref tilt axis
      dcomp_tilfoc(i,j) = dcomp_tilfoc(i,j) +  &
        dh_tilfoc(i,j)*dh_integ
    enddo
    do j=1, 2 ! Subref X/Y axis
      dcomp_offfoc_xy(i,j) = dcomp_offfoc_xy(i,j) +  &
        dh_offfoc_xy(i,j)*dh_integ
    enddo
    do j=1, r_nbb
      dcomp_vlbi_phase_inc(i,j) = dcomp_vlbi_phase_inc(i,j) + &
        dh_vlbi_phase_inc(i,j)*dh_integ
      dcomp_vlbi_phase_tot(i,j) = dcomp_vlbi_phase_tot(i,j) + &
        dh_vlbi_phase_tot(i,j)*dh_integ
    enddo
  enddo
  
  if (r_lmode.eq.1) then
    k = 1
    do i=1, r_nbas
      ia = r_iant(i)
      ja = r_jant(i)
      if (dh_dump.eq.1) then
        dcomp_bflag(i) = dh_bflag(i)
      else
        dcomp_bflag(i) = iand(dh_bflag(i),dcomp_bflag(i))
      endif
      do j=1, 2*mbands
        if (dh_dump.eq.1) then
          dcomp_sbflag(j,i) = dh_sbflag(j,i)
        else
          dcomp_sbflag(j,i) = dh_sbflag(j,i).or.dcomp_sbflag(j,i)
        endif
      enddo
      do j=1,2
        dcomp_uvm(j,i) = dcomp_uvm(j,i)+dh_uvm(j,i)*dh_integ
        dcomp_infac(j,i) = dcomp_infac(j,i)+   &
          dh_infac(j,i)*dh_integ
        z(j) = 0
      enddo
      wc = 0
      isig = (3-r_isb)/2
      do ic = 1, r_nband
        ipol = r_lpolentry(ia,ic)
        if (.not.down_channel(i,ic)) then
          do isb = 1, 2
            w = 1.0
            !                     IF (ISB.EQ.ISIG) THEN
            !                        w = 2*DH_INTEG*WC*1E6*R_CFWID(IC) /(R_TSYSS(ipol
            !     $                       ,ia)*R_TSYSS(ipol,ia))
            !                     else
            !                        w = 2*DH_INTEG*WC*1E6*R_CFWID(IC) /(R_TSYSI(ipol
            !     $                       ,ia)*R_TSYSI(ipol,ja))
            !                     endif
            z(isb) = z(isb) + cmplx(data(k),data(k+1))*w
            k = k + 2
          enddo
          wc = wc + w
        else
          k = k + 4
        endif
      enddo
      do isb = 1, 2
        if (wc.gt.0 .and. z(isb).ne.0) then
          ! sigma is the thoretical noise squared
          sigma = 1./wc
          sigamp(isb,i) = sigamp(isb,i) + sigma*dh_integ
          z(isb) = z(isb)/wc
          newpha = faz(z(isb))
          newpha = mod(newpha-oldpha(isb,i)+11*pi,2*pi)   &
            -pi+oldpha(isb,i)
          dcomp_rmspha(1,isb,i) = dcomp_rmspha(1,isb,i) +   &
            newpha**2*dh_integ
          dcomp_rmsamp(1,isb,i) = dcomp_rmsamp(1,isb,i) +   &
            abs(z(isb))**2*dh_integ
          avepha(isb,i) = avepha(isb,i)+newpha*dh_integ
          aveamp(isb,i) = aveamp(isb,i)+abs(z(isb))*dh_integ
          oldpha(isb,i) = avepha(isb,i)/dcomp_integ
        endif
      enddo
    enddo
  endif
  do i=1, qdata
    buffer(i) = buffer(i) + data(i)*dh_integ
  enddo
  return
end subroutine add_record
!
subroutine zero_record (qdata,buffer)
  use classic_api
  !---------------------------------------------------------------------
  ! Call Tree
  !	CLIC_SPECTRUM
  !	CLIC_COMPRESS	COMPRESS
  !	CLIC_TABLE
  !---------------------------------------------------------------------
  integer :: qdata                  !
  real :: buffer(qdata)             !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dcomp.inc'
  ! Local
  real*4 :: aveamp(2,mnbas), sigamp(2,mnbas), avepha(2,mnbas)
  real*4 :: oldpha(2,mnbas), wswpos
  common /averag/ aveamp,avepha,oldpha, sigamp, wswpos
  save /averag/
  integer :: i, k, j
  !------------------------------------------------------------------------
  ! Code:
  dcomp_integ = 0
  !      dcomp_test0(2) = 0
  wswpos = 0
  dcomp_utc = 0
  dcomp_ntime = 0
  do i=1,mntime
    dcomp_time_monitoring(i) = 0
  enddo
  do i=1, 3
    dcomp_svec(i) = 0
  enddo
  do i=1, mnbb
    dcomp_vlbi_phaseff(i) = 0
  enddo
  do i=1, r_nant
    do k = 1, mnbb                ! MNBCINPUT
      dcomp_total(k,i) = 0
    enddo
    dcomp_aflag(i) = 0
    do j=1, 2*mbands
      dcomp_saflag(j,i) = .false.
    enddo
    dcomp_rmspe(1,i) = 0
    dcomp_rmspe(2,i) = 0
    do j=1,mrlband
      dcomp_atfac(j,1,i) = 0
      dcomp_atfac(j,2,i) = 0
    enddo
    dcomp_delcon(i) = 0
    do j=1, mnbb ! Number of bband
       dcomp_dellin(j,i) = 0
       dcomp_delay(j,i) = 0
    enddo
    dcomp_delayc(i) = 0
    dcomp_phasec(i) = 0
    dcomp_phase(i) = 0
    dcomp_ratec(i) = 0
    dcomp_cable(i) = 0
    dcomp_gamme(i) = 0
    do k=1, 20
      dcomp_test1(k,i) = 0
    enddo
    dcomp_offfoc(i) = 0
    dcomp_offlam(i) = 0
    dcomp_offbet(i) = 0
    do k=1, r_wvrnch(i)
      dcomp_wvr(k,i) = 0
    enddo
    dcomp_wvrstat(i) = 0
    do j=1,mrlband
      dcomp_actp(1,i,j) = 0
      dcomp_actp(2,i,j) = 0
    enddo
    do j=1, mnif
      dcomp_anttp(j,i) =  0
    enddo
    dcomp_cable_alternate(i) = 0
    do j=1, 2 ! Inclinometer outputs
      dcomp_incli(i,j) = 0
    enddo
    dcomp_incli_temp(i) = 0
    do j=1, 3 ! Subref tilt axis
      dcomp_tilfoc(i,j) = 0
    enddo
    do j=1, 2 ! Subref X/Y axis
      dcomp_offfoc_xy(i,j) = 0
    enddo
    do j=1, mnbb ! Number of bband
      dcomp_vlbi_phase_inc(i,j) = 0
      dcomp_vlbi_phase_tot(i,j) = 0
    enddo
  enddo
  if (r_lmode.eq.1) then
    do i=1, r_nbas
      do j=1, 2
        dcomp_uvm(j,i) = 0
        dcomp_infac(j,i) = 0
        dcomp_rmspha(1,j,i) = 0
        dcomp_rmsamp(1,j,i) = 0
        dcomp_rmspha(2,j,i) = 0
        dcomp_rmsamp(2,j,i) = 0
        sigamp(j,i) = 0
        avepha(j,i) = 0
        aveamp(j,i) = 0
        oldpha(j,i) = 0
      enddo
    enddo
  endif
  do i=1, qdata
    buffer(i) = 0
  enddo
  return
end subroutine zero_record
!
subroutine output_record (qdata, data, buffer, nout)
  use classic_api
  !---------------------------------------------------------------------
  ! Call Tree
  !	CLIC_SPECTRUM
  !	CLIC_COMPRESS	COMPRESS
  !	CLIC_TABLE
  !---------------------------------------------------------------------
  integer :: qdata                  !
  real :: data(qdata)               !
  real :: buffer(qdata)             !
  integer :: nout                   !
  ! Global
  real :: phrms,amrms
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_dcomp.inc'
  ! Local
  real*4 :: aveamp(2,mnbas), sigamp(2,mnbas), avepha(2,mnbas)
  real*4 :: oldpha(2,mnbas), wswpos
  common /averag/ aveamp,avepha,oldpha, sigamp, wswpos
  save /averag/
  integer :: i, j, k, isb
  real :: sigma, rmsphn, rmsamn, ampk
  !------------------------------------------------------------------------
  ! Code:
  dcomp_utc = dcomp_utc/dcomp_integ
  dcomp_obs = r_dobs+int(dcomp_utc/86400.d0+100000.d0)-100000
  dcomp_utc = dcomp_utc-(dcomp_obs-r_dobs)*86400.
  dcomp_ntime = dcomp_ntime/dcomp_integ
  do i=1,mntime
    dcomp_time_monitoring(i) = dcomp_time_monitoring(i)/dcomp_integ
  enddo
  do i=1, 3
    dcomp_svec(i) = dcomp_svec(i)/dcomp_integ
  enddo
  do i=1, r_nbb
    dcomp_vlbi_phaseff(i) = dcomp_vlbi_phaseff(i)/dcomp_integ
  enddo
  do i=1, r_nant
    !         DCOMP_TOTAL(I) = DCOMP_TOTAL(I)/DCOMP_INTEG
    !         if (dcomp_test0(2).ge.0) then
    !            DCOMP_TOTAL(I) = DCOMP_TOTAL(I)/DCOMP_test0(2)
    do k = 1, mnbb
      if (wswpos.gt.0) then
        dcomp_total(k,i) = dcomp_total(k,i)/wswpos
      else
        dcomp_total(k,i) = dcomp_total(k,i)/dcomp_integ
      endif
    enddo
    do j=1,2
      do k=1,mrlband
        dcomp_atfac(k,j,i) = dcomp_atfac(k,j,i)/dcomp_integ
      enddo
    enddo
    ! do differently for beam switch (only average)
    if  (r_nswitch.gt.0) then
      do j=1,2
        dcomp_rmspe(j,i) = dcomp_rmspe(j,i)/dcomp_integ
      enddo
    elseif  (r_nswitch.le.0) then
      do j=1,2
        dcomp_rmspe(j,i) = sqrt(dcomp_rmspe(j,i)/dcomp_integ)
      enddo
    endif
    dcomp_delcon(i) = dcomp_delcon(i)/dcomp_integ
    do j=1, mnbb ! Number of bband
       dcomp_dellin(j,i) = dcomp_dellin(j,i)/dcomp_integ
       dcomp_delay(j,i) = dcomp_delay(j,i)/dcomp_integ
    enddo
    dcomp_delayc(i) = dcomp_delayc(i)/dcomp_integ
    dcomp_phasec(i) = dcomp_phasec(i)/dcomp_integ
    dcomp_phase(i) = dcomp_phase(i)/dcomp_integ
    dcomp_ratec(i) = dcomp_ratec(i)/dcomp_integ
    dcomp_cable(i) = dcomp_cable(i)/dcomp_integ
    do k=1, 20
      dcomp_test1(k,i) = dcomp_test1(k,i)/dcomp_integ
    enddo
    dcomp_offfoc(i) = dcomp_offfoc(i) /dcomp_integ
    !         if (dcomp_test0(2).ge.0) then
    !            DCOMP_OFFLAM(I) = DCOMP_OFFLAM(I) /dcomp_test0(2)
    !            DCOMP_OFFBET(I) = DCOMP_OFFBET(I) /dcomp_test0(2)
    if (wswpos.gt.0) then
      dcomp_offlam(i) = dcomp_offlam(i) /wswpos
      dcomp_offbet(i) = dcomp_offbet(i) /wswpos
    else
      dcomp_offlam(i) = dcomp_offlam(i) /dcomp_integ
      dcomp_offbet(i) = dcomp_offbet(i) /dcomp_integ
    endif
    do k=1, r_wvrnch(i)
      dcomp_wvr(k,i) = dcomp_wvr(k,i)/dcomp_integ
    enddo
    do j=1,mrlband
      dcomp_actp(1,i,j) =  dcomp_actp(1,i,j)/dcomp_integ
      dcomp_actp(2,i,j) =  dcomp_actp(2,i,j)/dcomp_integ
    enddo
    do j=1, r_nif
      dcomp_anttp(j,i) =  dcomp_anttp(j,i) / dcomp_integ
    enddo
    dcomp_cable_alternate(i) = dcomp_cable_alternate(i) / dcomp_integ
    do j=1, 2 ! Inclinometer outputs
      dcomp_incli(i,j) = dcomp_incli(i,j) / dcomp_integ
    enddo
    dcomp_incli_temp(i) = dcomp_incli_temp(i) / dcomp_integ
    do j=1, 3 ! Subref tilt axis
      dcomp_tilfoc(i,j) = dcomp_tilfoc(i,j) / dcomp_integ
    enddo
    do j=1, 2 ! Subref X/Y axis
      dcomp_offfoc_xy(i,j) = dcomp_offfoc_xy(i,j) / dcomp_integ
    enddo
    do j=1, r_nbb
      dcomp_vlbi_phase_inc(i,j) = dcomp_vlbi_phase_inc(i,j)/dcomp_integ
      dcomp_vlbi_phase_tot(i,j) = dcomp_vlbi_phase_tot(i,j)/dcomp_integ
    enddo
  enddo
  if (r_lmode.eq.1) then
    do i=1, r_nbas
      do j=1, 2
        dcomp_uvm(j,i) = dcomp_uvm(j,i)/dcomp_integ
        dcomp_infac(j,i) = dcomp_infac(j,i)/dcomp_integ
      enddo
      !
      do isb = 1, 2
        aveamp(isb,i) = aveamp(isb,i)/dcomp_integ
        avepha(isb,i) = avepha(isb,i)/dcomp_integ
        dcomp_rmspha(1,isb,i) = sqrt(abs(dcomp_rmspha(1,isb,i)   &
          /dcomp_integ - avepha(isb,i)**2))
        dcomp_rmsamp(1,isb,i) = sqrt(abs(dcomp_rmsamp(1,isb,i)   &
          /dcomp_integ - aveamp(isb,i)**2))
        !		ampk = r_flux/sqrt(r_jykel(r_iant(i))*r_jykel(r_jant(i)))
        ! ampk is the theoretical amplitude on this baseline
        ! Note that this is a biased (over)estimate of the actual visibility amplitude.
        ! we thus underestimate the noise fluctuations
        ampk = aveamp(isb,i)
        sigma = sqrt(sigamp(isb,i)/dcomp_integ)
        ! noise contributions to the rms of phase and amplitude:
        if (sigma.ne.0 .and. ampk.gt.0) then
          rmsphn = phrms(ampk/sigma)
          rmsamn = amrms(ampk/sigma)*sigma
        else
          rmsphn = 0
          rmsamn = 0
        endif
        if (dcomp_rmspha(1,isb,i).gt.rmsphn) then
          dcomp_rmspha(1,isb,i) =   &
            sqrt(dcomp_rmspha(1,isb,i)**2-rmsphn**2)
        else
          dcomp_rmspha(1,isb,i) = 0
        endif
        if (dcomp_rmsamp(1,isb,i).gt.rmsamn) then
          dcomp_rmsamp(1,isb,i) =   &
            sqrt(dcomp_rmsamp(1,isb,i)**2-rmsamn**2)
        else
          dcomp_rmsamp(1,isb,i) = 0
        endif
      enddo
    enddo
  endif
  dcomp_dump = nout + 1
  do i = 1, qdata
    data(i) = buffer(i)/dcomp_integ
  enddo
  nout = dcomp_dump
  return
end subroutine output_record
!
function phrms(v)
  real :: phrms                     ! rms in phase (radians)
  real :: v                         ! visibility modulus, in terms of sigma
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real :: w1
  logical :: no_number
  !------------------------------------------------------------------------
  ! Code:
  phrms = 0.0
  if (no_number(v)) return
  if (v.lt.0.75) then
    w1 = 1.0
  elseif (v.lt.1.25) then
    w1 = 2.5 - 2.0*v
  else
    w1 = 0.0
  endif
  if (v.ne.0) then
    phrms = w1 * pi/sqrt(3.)*(1.-v*sqrt(4.5/pi**3))   &
      + (1.-w1)*1./v
  endif
  return
end function phrms
!
function amrms(v)
  real :: amrms                     ! rms in amplitude, in terms of sigma
  real :: v                         ! visibility modulus, in terms of sigma
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real :: w1
  logical :: no_number
  !------------------------------------------------------------------------
  ! Code:
  amrms = 0.0
  if (no_number(v)) return
  if (v.lt.0.75) then
    w1 = 1.0
  elseif (v.lt.1.25) then
    w1 = 2.5 - 2.0*v
  else
    w1 = 0.0
  endif
  if (v.ne.0.0) then
    amrms = w1 * sqrt(2.-pi/2.) * (1.+v**2/4.)   &
      + (1.-w1) * (1.-1./8/v**2)
  else
    amrms = 0.0
  endif
  return
end function amrms
