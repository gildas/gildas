subroutine file(line,error)
  use gkernel_interfaces
  use clic_file
  !---------------------------------------------------------------------
  ! SAS	Support routine for command
  !	FILE 	I[N] 	File_name
  !		O[UT]	File_name	[NEW]
  !               RAW	[PROJ] [DATE] [/WINDOW] [/DIRECTORY]
  ! Arguments :
  !	LINE	C*(*)	Command line			Input
  !	ERROR	L	Logical error flag		Output
  ! (5-mar-1985)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_windowfile.inc'
  ! Local
  logical :: new,ok, window, err, directory, noext
  integer :: lfile,flen,lch, ic, itry, lc, nl, nkey1, mvocab1
  integer :: mvocab2, nkey2, ii, ls, ll, ier
  integer(kind=entry_length) :: lsize
  parameter (mvocab1=5, mvocab2=2)
  character(len=256) :: chain,chainsave,fich,short_file
  character(len=14) :: ch,shain
  character(len=300) :: mess
  character(len=32) :: ftype
  character(len=8) :: chproj
  character(len=1) :: ousep,separ,insep
  character(len=11) :: chdate
  character(len=12) :: key1,vocab1(mvocab1),key2,vocab2(mvocab2)
  data vocab1/'IN','OUT','BOTH','RAW','SELECTED'/
  data vocab2/'OLD','NEW'/
  save short_file
  !
  call gag_separ(insep,ousep,separ)
  directory = sic_present(2,0)
  if (directory) then
    if (sic_present(2,1)) then
      call file_directory_do
      if (.not.sic_present(0,1)) return
    else
      call file_directory(line,error)
      return
    endif
  endif
  !
  ch = 'IN'
  call sic_ke (line,0,1,ch,lch,.false.,error)
  if (error) return
  call sic_ambigs('FILE',ch,key1,nkey1,vocab1,mvocab1,error)
  if (error) return
  !
  ! FILE RAW [project] [date]
  if (key1.eq.'RAW') then
    chproj = '*'
    call sic_ke (line,0,2,chproj,lch,.false.,error)
    if (error) return
    chdate = '*'
    call sic_ke (line,0,3,chdate,lch,.false.,error)
    if (error) return
    window = sic_present(1,0)
    call file_raw(window,chproj,chdate,chain,error)
    if (error .or. window) return
    new = .false.
    code = 'IN'
  !
  ! KEY1 = 'SELECTED' means we recover the selection from FILE /WINDOW
  elseif (key1.eq.'SELECTED') then
    chain = datafile(ifile)
    new = .false.
  !
  ! FILE IN|OUT|BOTH
  elseif (key1.eq.'IN' .or. key1.eq.'OUT'   &
    .or. key1.eq.'BOTH') then
    code = key1
    !
    ! Input file name
    chain = '*.*pb'
    call sic_ch (line,0,2,chain,lch,.false.,error)
    if (error) return
    ! [OLD|NEW]
    new = .false.
    ch = 'OLD'
    do ii = 3, 4
      call sic_ke (line,0,3,ch,lch,.false.,error)
      if (error) return
      call sic_ambigs('FILE',ch,key2,nkey2,vocab2,mvocab2,error)
      if (error) return
      if (key2.eq.'NEW') new = .true.
    enddo
    ! /WINDOW
    window = sic_present(1,0)
    if (window) then
      call sic_parsef(chain,fich,' ',' ')
      call file_in(fich,error)
      return
    else
      if (.not.sic_present(0,2)) then
        call message(6,3,'FILE','No default is provided '//   &
          'for the file name')
        error = .true.
        return
      endif
    endif
  !      ELSE
  !         CALL MESSAGE(6,3,'FILE','IN RAW OUT or BOTH please')
  !         ERROR = .TRUE.
  !         RETURN
  endif
  !
  ! Bookkeeping
  ic = index(chain,'.')        ! Was an extension given?
  noext = .false.
  if (ic.eq.0) then
    noext = .true.
  endif
  chainsave = chain            ! To keep the input directory path, if any
  !
  ! Try to open the file
  ok = .false.
  call sic_parsef(chain,fich,' ',defext(1))
  call get_file_type (fich, ftype, flen, new)
  call check_extension (ftype, flen, error)
  if (error) return
  lfile = lenc(fich)
  !
  ! FILE IN
  !
  if (code.eq.'IN') then
    inquire (file=fich(1:lfile),exist=ok)
    if (ok) then
      mess = 'Found file '//fich(1:lfile)
      call message(6,1,'FILE',mess)
      call input(fich,lfile,error)
    else
      mess = fich(1:lfile)//' not found'
      call message(6,2,'FILE',mess)
    endif
    !
    ! Check dd-mmm-yyyy-proj equivalent name  YMDDPROJ.IPB;1
    !         IF (.NOT.OK .AND. LENC(CHAIN).EQ.16 .AND.
    !     &      FTYPE.EQ.'.IPB') THEN
    !            CALL ENCRYPT(CHAIN,SHAIN,ERROR)
    !            IF (.NOT.ERROR) THEN
    !
    ! Find location of file name
    !               CALL SIC_UPPER(SHAIN)
    !               IC = INDEX(FICH,CHAIN(1:16))
    !               FICH(IC:) = SHAIN(1:8)//'.IPB'
    !               LFILE = LENC(FICH)
    !               INQUIRE (FILE=FICH(1:LFILE),EXIST=OK)
    !               IF (.NOT.OK) THEN
    !                  MESS = FICH(1:LFILE)//' not found'
    !                  CALL MESSAGE(6,2,'FILE',MESS)
    !                  FICH(IC:) = SHAIN(1:8)//'.IPB;1'
    !                  LFILE = LENC(FICH)
    !                  INQUIRE (FILE=FICH(1:LFILE),EXIST=OK)
    !                  IF (.NOT.OK) THEN
    !                     MESS = FICH(1:LFILE)//' not found'
    !                     CALL MESSAGE(6,2,'FILE',MESS)
    !                  ENDIF
    !               ENDIF
    !               IF (OK) CALL INPUT(FICH,LFILE,ERROR)
    !            ENDIF
    !         ENDIF
    !
    ! No file found - try other extensions?
    if ((.not.ok).and.noext) then
      call message(6,1,'FILE','Trying all file extensions')
      itry = 1
      do while (.not.ok .and. itry.le.ndefext)
        chain = chainsave      ! To recover the input directory path, if any
        call sic_parsef(chain,fich,' ',defext(itry))
        call get_file_type (fich, ftype, flen, new)
        call check_extension (ftype, flen, error)
        if (error) return
        lfile = lenc(fich)
        !
        inquire (file=fich(1:lfile),exist=ok)
        if (ok) then
          mess = '  Found file '//fich(1:lfile)
          call message(6,1,'FILE',mess)
          call input(fich,lfile,error)
        else
          mess = '  '//fich(1:lfile)//' not found'
          call message(6,2,'FILE',mess)
        endif
        !
        ! Check dd-mmm-yyyy-proj equivalent name  YMDDPROJ.IPB;1
        if (.not.ok .and. (lenc(chain).eq.16.or.lenc(chain).eq.20) & 
            .and.  ftype.eq.'.IPB') then
          if (lenc(chain).eq.16) then
             ls = 8
             ll = 16
          else
             ls = 14
             ll = 20
          endif   
          call encrypt(chain,shain,error)
          if (.not.error) then
            !
            ! Find location of file name
            call sic_upper(shain)
            ic = index(fich,chain(1:ll))
            fich(ic:) = shain(1:ls)//'.IPB'
            lfile = lenc(fich)
            inquire (file=fich(1:lfile),exist=ok)
            if (.not.ok) then
              mess = '  '//fich(1:lfile)//' not found'
              call message(6,2,'FILE',mess)
              fich(ic:) = shain(1:ls)//'.IPB;1'
              lfile = lenc(fich)
              inquire (file=fich(1:lfile),exist=ok)
              if (.not.ok) then
                mess = '  '//fich(1:lfile)//' not found'
                call message(6,2,'FILE',mess)
              endif
            endif
            if (ok) then
              mess = '  Found file '//fich(1:lfile)
              call message(6,1,'FILE',mess)
              call input(fich,lfile,error)
            endif
          endif
        endif
        itry = itry+1
      enddo
    endif
    !
    ! A file has been found
    !
    if (ok) then
      if (error) return
      find_needed = .true.
      call new_file
    endif
  !
  ! FILE OUT
  !
  elseif (code.eq.'OUT') then
    if (new) then
      lsize = 10000_8
      ier = sic_getlog('CLIC_IDX_SIZE',lsize) 
      call init(fich,lfile,lsize,error)
    else
      call output(fich,lfile,error)
    endif
    ok = .not.error
  !
  ! FILE BOTH
  !
  elseif (code.eq.'BOTH') then
    call input(fich,lfile,error)
    if (error) return
    find_needed = .true.
    call new_file
    call output(fich,lfile,error)
    ok = .not.error
  endif
  !
  ! No file has been open
  !
  error = .not.ok
  if (error) return
  !
  ! A file has been open
  !
  if (i%lun.ne.o%lun) then
    write_mode = 'NEW '
  else
    write_mode = 'UPDA'
  endif
  !
  call sic_delvariable ('SHORT_FILE',.false.,err)
  short_file = i%spec
  ic = 1
  lc = lenc(short_file)
  do while (ic.gt.0)
    ic = index(short_file,separ)
    lc = lenc(short_file)
    short_file = short_file(ic+1:lc)//' '
  enddo
  lc = lenc(short_file)
  ic = index(short_file,'.')
  if (ic.ne.0) short_file(ic:lc) = ' '
  call sic_def_char('SHORT_FILE',short_file,.false.,error)
  !
  nl = index(line,' ')+1
  if (code.eq.'IN' .or. code.eq.'BOTH') then
    line(nl:) = code//' '//'"!'//i%spec(1:i%nspec)//'"'
  elseif (code(1:1).eq.'OUT') then
    line(nl:) = code//' '//'"!'//o%spec(1:o%nspec)//'"'
  endif
  !
  return
end subroutine file
!
subroutine check_extension (ftype, flen, error)
  character(len=*) :: ftype         !
  integer :: flen                   !
  logical :: error                  !
  ! Local
  integer :: i,next
  parameter (next=8)
  character(len=4) :: kext(next)
  character(len=12) :: fext
  character(len=60) :: mess
  data kext /'raw', 'comp', 'rf', 'calp',   &
    'cala', 'cal', 'tmp','data'/
  !
  error = .false.
  if (flen.gt.4) then
    if (ftype(1:5).eq.'.ipb-') then
      fext = ftype(6:flen)
      do i = 1,next
        if (fext.eq.kext(i)) return
      enddo
    elseif (ftype(1:5).eq.'.IPB;') then
      return
    endif
  elseif (flen.eq.4) then
    if (ftype.eq.'.IPB' .or. ftype.eq.'.HPB') return
    if (ftype.eq.'.ipb' .or. ftype.eq.'.hpb') return
  endif
  mess = 'Invalid file extension '//ftype
  call message(6,3,'FILE',mess)
  error = .true.
end subroutine check_extension
!
subroutine file_raw(window,proj,date,name,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Get the name of data file with stardard name
  !     - corresponding with optional PROJ and DATE
  !     - found in the IPB_DATAi: logical directory list
  ! if not WINDOW, the first file is selected to be opened;
  ! if WINDOW, a window is opened to select the name in the available
  ! choices in the list.
  !---------------------------------------------------------------------
  logical :: window                 !
  character(len=*) :: proj          !
  character(len=*) :: date          !
  character(len=*) :: name          !
  logical :: error                  !
  ! Global
  include 'clic_windowfile.inc'
  ! Local
  logical :: err
  integer :: mlogs, jd, jp, lch, i, lcl
  integer :: ier, lchoix, ll, l0
  parameter (mlogs=10)
  character(len=12) :: logs(mlogs)
  character(len=8) :: tproj
  character(len=11) :: tdate
  character(len=4096) :: com
  character(len=256) :: ch
  character(len=4096) :: choix
  character(len=12) :: vdate,udate
  character(len=9) :: uproj,vproj
  !
  integer :: nf, j, iext, lname, lcom, mext, next
  parameter (mext=2)
  character(len=256), allocatable :: ff(:)
  character(len=4) :: ext(mext)
  character(len=4) :: cgui,csic
  character(len=1) :: bsl
  character(len=8) :: os, long_name
  logical :: check_long_name
  !
  character(len=20) :: key(mfile), long
  !
  data ext/'.ipb','.IPB'/
  data logs/'ipb_data:','ipb_data1:','ipb_data2:',   &
    'ipb_data3:','ipb_data4:','ipb_data5:',   &
    'ipb_data6:','ipb_data7:','ipb_data8:','ipb_data9:'/
  !---------------------------------------------------------------------
  ! Code
  bsl = char(92)
  udate = date(1:lenc(date))//'*'
  uproj = proj(1:lenc(proj))//'*'
  jd = index(udate,'*')
  jp = index(uproj,'*')
  call sic_delvariable('NFILE',.false.,error)
  call sic_delvariable('IFILE',.false.,error)
  call sic_delvariable('DATAFILE',.false.,error)
  call sic_delvariable('LONGFILE',.false.,error)
  call sic_delvariable('HFILEMODE',.false.,error)
  call sic_delvariable('HFILE',.false.,error)
  call sic_delvariable('HFILEDEF',.false.,error)
  hfiledef = .true.
  error = .false.
  call sic_def_logi('HFILEDEF',hfiledef,.false.,error)
  if (error) return
  call sic_def_inte('NFILE',nfile,0,1,.false.,error)
  if (error) return
  call sic_def_inte('IFILE',ifile,0,1,.false.,error)
  if (error) return
  call sic_def_char('HFILE',hfile,.false.,error)
  if (error) return
  call sic_def_char('HFILEMODE',hfilemode,.false.,error)
  if (error) return
  nfile = 0
  !
  ! WIN32 is not case sensitive ...
  call gag_os(os)
  if (os.eq.'WIN32') then
    next = 1
  else
    next = mext
  endif
  !
  ! Special cases: check both nomenclature
  check_long_name = .false.
  if (uproj(1:4).eq."HOLO".or. &
      uproj(1:4).eq."POIN".or. &
      uproj(1:4).eq."FLUX".or. &
      uproj(1:4).eq."BASE".or. &
      uproj(1:4).eq."VLBI") then
    check_long_name = .true.
    long_name = 'T'//uproj(1:4)//'___'
  endif
  !
  ! Create file list; the second * in the search filter is there for the .IPB;1
  do i=1, mlogs
    ch = logs(i)
    ier = sic_getlog(ch)
    lch = len_trim(ch)
    if (ch.ne.logs(i) .and. lch.ne.0.and.gag_inquire(ch,lch).eq.0) then
      do iext=1,next
        call gag_directory(ch(1:lch),'*'//ext(iext)//'*',ff,nf,error)
        if (error) return
        if (nf.gt.0) then
          do j=1, nf
            lname = len_trim(ff(j))
            if (ff(j)(lname-4:lname).eq.".SHA1") cycle
            call get_file_name(ff(j),name,lname)
            call check_raw_name(name,ext(iext),tproj,   &
              tdate,long,err)
            if (err) goto 90
            vproj(1:lenc(tproj)) = tproj
            vproj(jp:) = '*'
            vdate(1:11) = tdate
            vdate(jd:) = '*'
            if ((vproj(1:jp).eq.uproj(1:jp).or. &
                 (check_long_name.and.tproj.eq.long_name)).and.   &
              vdate(1:jd).eq.udate(1:jd)) then
              nfile = nfile+1
              datafile(nfile) = ch(1:lch)//ff(j)
              longfile(nfile) = long
              !                        print *, nfile, DATAFILE(NFILE)
              if (window) then
                if (jd.lt.12 .and. jp.lt.9) then
                  key(nfile)(1:) = tproj(1:lenc(tproj))//'_'   &
                    //tdate(1:2)//tdate(4:6)   &
                    //tdate(8:11)
                elseif (jd.eq.12) then
                  key(nfile)(1:) = tproj
                elseif (jp.eq.9) then
                  key(nfile)(1:) = tdate(1:2)   &
                    //tdate(4:6)//tdate(8:11)
                endif
              endif
            endif
            if (nfile.ge.mfile) goto 100
90          continue  ! Never put a label on an ENDDO.
          enddo
        endif
      enddo
      if (allocated(ff)) deallocate(ff)
    endif
  enddo
  !
100 continue
  !
  if (nfile.eq.0) then
    if (.not.window) then
      call message(6,3,'FILE_RAW','No raw data file')
      error = .true.
      return
    endif
    choix = ' '
    lchoix = 0
  else
    !
    name = datafile(1)
    ifile = 1
    call sic_def_charn('DATAFILE',datafile,1,nfile,.true.,error)
    call sic_def_charn('LONGFILE',longfile,1,nfile,.true.,error)
    code = 'IN'
    if (.not.window) return
    !
    ! Dialog window
    lchoix=0
    ll = 0
    do i=1, nfile
      lchoix = lchoix+1+lenc(key(i))
    enddo
    if (lchoix.gt.len(choix) ) then
      ll = 4
    endif
    lchoix=0
    choix = ' '
    do i=1, nfile
      l0 = lenc(key(i))
      choix(lchoix+1:)  = key(i)(1:l0-ll)//' '
      lchoix = lchoix+1+l0-ll
    enddo
    !
  endif
  csic = 'SIC'//char(92)
  cgui = 'GUI'//char(92)
  !
  com = cgui//'PANEL "Open a raw data file" gag_pro:x_raw_file.hlp'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'SAY "Project: '//proj//'  Date: '//date//'   "''nfile''" files"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  if (jd.lt.12 .and. jp.lt.9) then
    com=csic//'LET  IFILE ''ifile'' /PROMPT "Project_Date ?" /INDEX '//  &
      choix(1:lchoix)
  elseif (jd.eq.12) then
    com=csic//'LET  IFILE ''ifile'' /PROMPT "Project ?" /INDEX '//  &
      choix(1:lchoix)
  elseif (jp.eq.9) then
    com=csic//'LET  IFILE ''ifile'' /PROMPT "Date  ?" /INDEX '//  &
      choix(1:lchoix)
  endif
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'LET HFILEDEF '//"no"
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'LET HFILE ''hfile'' /PROMPT "Header file name:"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'LET HFILEMODE ''hfilemode'' /PROMPT "Mode ?"'//   &
    ' /CHOICE OLD NEW'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com=cgui//'BUTTON "file raw /window /direc  " "RAW DATA DIRECTORIES"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com=cgui//'BUTTON "@ create_header DEF"  "OPEN and FIND"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com=cgui//'BUTTON "@ create_header YES" "CREATE HEADER FILE"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = cgui//'GO "FILE SELECTED"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  return
end subroutine file_raw
!
subroutine check_raw_name(name,ext,tproj,tdate,long,err)
  use gkernel_interfaces
  character(len=*) :: name          !
  character(len=*) :: ext           !
  character(len=*) :: tproj         !
  character(len=*) :: tdate         !
  character(len=*) :: long          !
  logical :: err                    !
  ! Local
  integer :: lname, ip, i
  !
  err = .false.
  lname = lenc(name)
  !      IF (LNAME.GT.8 .AND. EXT.EQ.'.ipb') THEN
  if (lname.eq.16 .and. ext.eq.'.ipb') then
    long = name(1:16)
  elseif  (lname.eq.20 .and. ext.eq.'.ipb') then
    long = name(1:20)
  elseif (lname.eq.8 .and. ext.eq.'.ipb') then
    call sic_upper(name)
    call decrypt(name(1:8),long,err)
    if (err) return
  elseif (lname.eq.8 .and. ext.eq.'.IPB') then
    call decrypt(name(1:8),long,err)
    if (err) return
  elseif (lname.eq.14 .and. ext.eq.'.ipb') then
    call sic_upper(name)
    call decrypt(name(1:14),long,err)
    if (err) return
  elseif (lname.eq.14 .and. ext.eq.'.IPB') then
    call decrypt(name(1:14),long,err)
    if (err) return
  else
    err = .true.
    return
  endif
  lname = lenc(long)
  tdate = long(1:11)
  tproj = long(13:lname)
  call sic_upper(tproj)
  call sic_upper(tdate)
  ip = 7
  call message_level(ip)
  call cdate(tdate,i,err)
  call message_level(ip)
  return
end subroutine check_raw_name
!
subroutine file_in(name,error)
  use gkernel_interfaces
  character(len=*) :: name          !
  logical :: error                  !
  ! Global
  include 'clic_windowfile.inc'
  ! Local
  character(len=120) :: com
  integer :: lcom
  character(len=4) :: cgui, csic
  !-----------------------------------------------------------------------
  !
  ! Dialog window
  call sic_delvariable('NFILE',.false.,error)
  call sic_delvariable('IFILE',.false.,error)
  call sic_delvariable('DATAFILE',.false.,error)
  call sic_delvariable('CODE',.false.,error)
  call sic_def_inte('NFILE',nfile,0,1,.false.,error)
  call sic_def_inte('IFILE',ifile,0,1,.false.,error)
  call sic_def_char('CODE',code,.false.,error)
  ifile = 1
  nfile = 1
  call sic_def_charn('DATAFILE',datafile,1,nfile,.false.,error)
  cgui = 'GUI'//char(92)
  csic = 'SIC'//char(92)
  !
  com = cgui//'PANEL "Open a data file for input" gag_pro:x_in_file.hlp'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'LET datafile ''datafile'' /PROMPT "File name ?"'//   &
    ' /FILE "'//name(1:lenc(name))//'"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = csic//'LET code ''code'' /PROMPT "Mode ?" /CHOICE IN OUT BOTH'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  com = cgui//'GO "FILE SELECTED"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  return
end subroutine file_in
!
subroutine decrypt(short,long,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Decrypt short name into long one.  Only correct if the
  ! long name has the format of dd-mm-yyyy-proj
  !---------------------------------------------------------------------
  character(len=*) :: short         !
  character(len=*) :: long          !
  logical :: error                  !
  ! Local
  integer :: im, id, iy, ip
  character(len=3) :: cm(12)
  character(len=12) :: sm
  logical :: old_format
  data cm/'JAN','FEB','MAR','APR','MAY','JUN',   &
    'JUL','AUG','SEP','OCT','NOV','DEC'/
  data sm/'123456789ABC'/
  !-----------------------------------------------------------------------
  old_format = lenc(short).eq.8
  if (old_format) then
    im = index(sm,short(2:2))
    if (im.eq.0) then
      error = .true.
      return
    endif
    iy = 1990 + ichar(short(1:1)) -ichar('A')
    write(long,1000) short(3:4), cm(im), iy, short(5:8)
  else
    im = 10*(ichar(short(3:3))-ichar('0')) &
           +(ichar(short(4:4))-ichar('0'))
    write(long,1001) short(5:6), cm(im), short(1:2), short(7:14)
  endif 
1000 format(a2,'-',a3,'-',i4.4,'-',a4)
1001 format(a2,'-',a3,'-20',a2,'-',a8)
  call sic_lower(long)
  ip = 7
  call message_level(ip)
  call cdate(long,id,error)
  call message_level(ip)
end subroutine decrypt
!
subroutine encrypt(long,short,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Encrypt long name into short one.  Only correct if the
  ! long name has the format of dd-mm-yyyy-proj
  !---------------------------------------------------------------------
  character(len=*) :: long          !
  character(len=*) :: short         !
  logical :: error                  !
  ! Local
  integer :: im, id, iy
  character(len=1) :: chm,chy
  character(len=3) :: cm(12)
  character(len=4) :: chw
  character(len=12) :: sm
  logical :: old_format
  data cm/'JAN','FEB','MAR','APR','MAY','JUN',   &
    'JUL','AUG','SEP','OCT','NOV','DEC'/
  data sm/'123456789ABC'/
  !-----------------------------------------------------------------------
  short = 'TOUTFAUX'
  old_format = lenc(long).eq.16
  call adate(long(1:11),id,im,iy,error)
  if (error) return
  if (old_format) then
    chy = char(iy-1990+ichar('A'))
    chm = sm(im:im)
    write(short,'(A1,A1,I2.2,A4)') chy, chm, id, long(13:16)
  else
    if (iy.lt.2000.or.iy.gt.2099) then
      write (chw,'(i4)') iy
      call message(8,3,'ENCRYPT','Format cannot accomodate year'//chw)
    else
      iy = iy-2000
    endif
    write(short,'(i2.2,i2.2,i2.2,a8)') iy, im, id, long(13:20)
  endif
  call sic_upper(short)
end subroutine encrypt
!
subroutine file_directory(line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Interactively edit the default raw data file directories.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  ! INCLUDE 'clic_windowfile.inc'
  ! Local
  integer :: i, ier, lcom
  character(len=256) :: test,ipbdata(0:9),com
  character(len=10) :: logs(0:9)
  character(len=4) :: csic,cgui
  !
  data logs/'ipb_data:','ipb_data1:','ipb_data2:',   &
    'ipb_data3:','ipb_data4:','ipb_data5:',   &
    'ipb_data6:','ipb_data7:','ipb_data8:','ipb_data9:'/
  save ipbdata, logs
  !---------------------------------------------------------------------
  ! Code
  !
  error = .false.
  do i=0, 9
    test = logs(i)
    call sic_resolve_log(test)
    if (test.ne.logs(i)) then
      ipbdata(i) = test
    else
      ipbdata(i) = ' '
    endif
    test = 'IPB_DATA'//char(ichar('0')+i)
    call sic_delvariable(test,.false.,error)
    call sic_def_char(test,ipbdata(i),.false.,error)
  enddo
  if (error) return
  !
  ! Dialog window
  cgui = 'GUI'//char(92)
  csic = 'SIC'//char(92)
  !
  com =   &
    cgui//'PANEL "Edit raw data file directories" gag_pro:x_raw_dir.hlp'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  !
  do i=0, 9
    write(com,1000) i,i,logs(i)(1:10)
1000 format('LET IPB_DATA',i1,' ''IPB_DATA',i1,''' /PROMPT "',   &
      a10,' ?"')
    com = csic//com
    lcom = lenc(com)
    call exec_command(com(1:lcom),error)
    if (error)  return
  enddo
  !
  i = index(line,'/DIREC')
  com = cgui//'GO "'//line(1:i-1)//' /DIRECTORY DO"'
  lcom = lenc(com)
  call exec_command(com(1:lcom),error)
  if (error)  return
  return
  !
entry file_directory_do
  do i=0, 9
    if (lenc(ipbdata(i)).gt.0) then
      ier = sic_setlog (logs(i),ipbdata(i))
    else
      test = logs(i)
      call sic_resolve_log(test)
      if (test.eq.logs(i)) then
        ier = sic_setlog (logs(i),' ')
      endif
    endif
  enddo
  return
end subroutine file_directory
