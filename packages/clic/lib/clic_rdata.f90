module clic_rdata
      complex(kind=4), allocatable :: datac(:), datal(:)
      complex(kind=4), allocatable :: passc(:), passl(:)
      complex(kind=4), allocatable :: factc(:), factl(:)
      complex(kind=4), allocatable :: spidc(:), spidl(:)
      real(kind=4), allocatable    :: campmaxu(:),lampmaxu(:), &
                                      campmaxl(:),lampmaxl(:), &
                                      campminu(:),lampminu(:), &
                                      campminl(:),lampminl(:), &
                                      ampfacu(:),ampfacl(:)
      complex(kind=4), allocatable :: gainc(:,:,:), gainl(:,:,:)
      real(kind=4), allocatable    :: wgainc(:,:,:), wgainl(:,:,:)
      integer(kind=4) :: gnumc, gdumpc, gnuml, gdumpl
end module

