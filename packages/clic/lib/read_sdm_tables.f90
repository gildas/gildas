subroutine read_sdm_tables(error)
  use gkernel_interfaces
  !------------------------------------------------------------------------
  ! Support READ FILE
  ! read  a SDM data set in ALMA SDM format
  !------------------------------------------------------------------------
  use gildas_def
  use classic_api
  use sdm_Main
  use sdm_State
  use sdm_Enumerations
  !$$$  use sdm_TotalPower
  !     Dummy variables:
  logical error
  !     Global variables:
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  !     Local variables:
  type (MainKey), allocatable  :: mKeyList(:)
  type (MainKey) :: mKey
  type (MainRow) :: mRow
  !
  integer, allocatable :: scanNumberList(:), subScanNumberList(:),   &
    integrationNumberList(:), subIntegNumList(:), dataArray(:),   &
    nmodeList(:), modeList(:,:)
  integer*8, allocatable :: timeList(:), intervalList(:)
  logical, allocatable :: tpList(:), assocCorrList(:)
  integer :: ier, mainSize, tpmSize, r_npol, nch, ndata, num,   &
    i, j,irow(1000), nobs, k, kc, klu, klc, pointingSize,   &
    focusSize, imode, mcorr, nmode, mode(2),   &
    assocFullResolutionID, assocBasebandWideID,   &
    assocChannelAverageID , numIntegration
  integer(kind=address_length) data_in, ip_data
  character :: dataOid*512, uid*256, chain*50, done*7
  logical :: end, first, check, present,   &
    holoData, ALmaCorr, type
  integer(data_length) ::  h_offset, c_offset, l_offset
  integer :: lfich, ir, l, lenz, k2, jobs
  integer*8 :: dayStart, dayTime
  real :: minfoc(mnant), maxfoc(mnant), minlam(mnant), maxlam(mnant)   &
    , minbet(mnant), maxbet(mnant)
  integer, parameter :: scan_focu = 1, scan_lamb = 2, scan_beta = 3,   &
    scan_five = 6, scan_other = 8, maxNumberIntegration=3000,   &
    maxIntent=10
  type(ArrayTimeInterval) :: tmp, timeIntervalSubscan
  type(ArrayTimeInterval), allocatable :: timeIntervalRecord(:)
  integer*8 :: timeIntervalObs(2)
  external :: dummyConvert
  integer testBuffer(1000)
  !------------------------------------------------------------------------
  !     Code:
  holoData = .false.
  new_receivers = .true.

  call getMainTableSize(mainSize, error)
  allocate (mKeyList(mainSize), intervalList(mainSize),   &
    scanNumberList(mainSize), subScanNumberList(mainSize),   &
    subintegNumList(mainSize), integrationNumberList(mainSize),   &
    nmodeList(mainSize), modeList(2,mainSize), tpList(mainSize),   &
    assocCorrList(mainSize), stat =ier)
  if (ier.ne.0) goto 97
  call getMainKeys(mainSize, mKeyList, error)
  if (error) return
  !
  mCorr = 4
  call  allocMainRow(mRow, error)
  if (error) return
  nobs = 0
  do i=1, mainSize
    call getMainRow(mKeyList(i), mRow, error)
    if (error) return
    intervalList(i) = mRow%interval
    numIntegration = mRow%numIntegration
    !
    !     check for auto/corr/both
    configDescription_Id = mKeyList(i)%configDescriptionId
    call get_sdm_ConfigDescriptionModes(nmode, mode,   &
      assocFullResolutionID, assocBasebandWideID,   &
      assocChannelAverageID, error)
    !print *, 'assocFullResolutionID, assocBasebandWideID, assocChannelAverageID ' 
    !print *, assocFullResolutionID, assocBasebandWideID, assocChannelAverageID  
    if (error) return
    call get_sdm_Processor(AlmaCorr, error)
    !print *, 'AlmaCorr ', AlmaCorr
    !
    !     ignore rows with *associated* (full spectral) correlator rows. We
    !     will use these to make bure observations. Thus we use in this loop
    !     only the full resolution correlator rows ...
    !
    if (assocFullResolutionID.lt.0) then
      tpList(i) = .not.AlmaCorr
      !     ### should also loop on integrations if there is more than one
      if (tpList(i)) then
        nobs = 1
      else
        nobs = numIntegration
      endif
      !print *, 'numIntegration ', numIntegration
      do jobs = 1, nobs
        do imode = 1, nmode
          !print *, 'jobs, imode ', jobs, imode
          r_lmode = mode(imode)
          r_nant = 0           ! for a check...
          !
          !     Get the main table data size information contents for this observation
          time_Interval = (/mKeyList(i)%time - mRow%interval/2,   &
            mRow%interval/)

          if (time_start(1).lt.0) then
            time_start(1) = time_interval(1)
            time_start(2) = -1
          endif
          dayTime = mKeyList(i)%time
          timeIntervalObs = time_Interval
          if (nobs.gt.1) then
             timeIntervalObs(1) = time_Interval(1)+(jobs-1)*time_interval(2)/nobs
             timeIntervalObs(2) = time_interval(2)/nobs
             dayTime = dayTime - mRow%interval/2 + (jobs-1)   &
              *mRow%interval/nobs + mRow%interval/nobs/2
          endif
          timeIntervalSubscan = ArrayTimeInterval(time_Interval(1), &
               time_Interval(2))

          call setDataHeaderTimes(timeIntervalObs) 
          Field_Id = mKeyList(i)%fieldId
          configDescription_Id = mKeyList(i)%configDescriptionId
          !     caution here
          r_nsb = 2
          r_lnsb = 2
          !
          !     get the number of basebands (=correlator units).
          !     configDescriptionId -> processor_Id -> CorrelatorMode_Id -> numBaseBand
          call get_sdm_ConfigDescription(error)
          if (error) goto 99
          !
          !     Get the main now that the number of antennas is known...
          call get_sdm_Main(uid, error)
          if (error) return
          call get_sdm_Processor(type, error)
          if (error) goto 99
          if (correlatorMode_Id .ge. 0) then
            call get_sdm_CorrelatorMode(error)
            if (error) goto 99
          else
            r_lband = 1
          endif
          !
          !     look into the data description (to get data sizes...)
          call get_sdm_DataDescription(error)
          if (error) goto 99
          !
          !     Polarization Table .... defines r_lband, and r_lpolentry
          call get_sdm_Polarization(error)
          if (error) return
          !
          !     Feed Table (needs Feed_Id BUT having more that one feed is an error!) OK 90
          call get_sdm_Feed(error)
          if (error) return
          !
          !     Spectral window tables

          call get_sdm_spectralWindow(holodata, error)
          if (error) goto 99
          !
          !
          !     antenna table
          call get_sdm_antenna(error)
          if (error) goto 99
          !
          !     station table 
          !call get_sdm_station(error) ! now read by antenna...
          !if (error) goto 99
          !
          !     get the relevant weather information
          call get_sdm_weather(error)
          if (error) goto 99
          !
          !     Receiver Table (needs receiver_Id from FeedTable) OK
          call get_sdm_Receiver(error)
          if (error) return
          !
          !     FreqOffset Table OK
          !     call get_sdm_FreqOffset(error)
          !     if (error) return
          !
          !     Beam Table ++
          !     call write_sdm_Beam (error)
          !     if (error) return
          !
          !
          !     SwitchCycle Table ++  + 90
          call get_sdm_SwitchCycle(error)
          if (error) return
          !
          !     Field Table ++   + 90
          call get_sdm_Field(error)
          if (error) return
          !
          !     --- Source information:
          !
          !     Source Table ++  +
          call get_sdm_Source(error)
          if (error) return
          !
          !     Source Parameter Table (??) ++  +
          !cc   call get_sdm_SourceParameter(error)
          !cc   if (error) return
          !
          !     Doppler Table ++  +
          call get_sdm_Doppler (error)
          if (error) return
          !
          !     Ephemeris Table ++  +
          !     c      call write_sdm_Ephemeris(error)
          !     c      if (error) return
          !
          !     ExecBlock Table ++  +
          call get_sdm_ExecBlock(error)
          if (error) return
          !
          !     History Table (irrelevant?) ++  +
          call get_sdm_History(error)
          if (error) return
          !
          !     SBSummary Table (not needed?)
          !     Scan and subscan Tables ++  +
          call get_sdm_Scan(error)
          if (error) return
          !     Seeing Table (deprecated)
          !     SquareLawDetector Table (??)
          !
          !     Calibration Tables:
          !     write_sdm_SysCal (deread_sdm_tables.f90precated)
          !     write_sdm_WVMCal (deprecated)
          !     write_sdm_CalAmpli
          !
          !--------- SKIP as not correctly written--------------------------------
          ! 
          !call get_sdm_calDevice(error)
          !if (error) return
          !--------- SKIP as not correctly written--------------------------------
          !     Atmosphere Table  ++ +
          !! call get_sdm_calAtmosphere(error)
          !! if (error) return
          !
          !     calculate size parameters
          !
          !     note: the critical parameters are r_lntch and r_nband; they are
          !     obtained by get_sdm_spectralWindow above.
          r_nbas = max(r_nant*(r_nant-1)/2,1)
          !     new receivers:
          r_ldpar = 10000000
          r_npol = 1
          call convert_dhsub(testbuffer, r_ldpar, r_nant, r_nbas,   &
            r_lband, r_npol, dummyconvert, dummyconvert,   &
            dummyconvert, dummyconvert)
          !
          if (r_lmode.eq.2) then
            r_ldatc = r_nband*r_nant*r_npol
            r_ldatl = r_lntch*r_nant*r_npol
          elseif (r_lmode.eq.1) then
            r_ldatc = 2*r_nband*r_nsb*r_nbas*r_npol
            r_ldatl = 2*r_lntch*r_lnsb*r_nbas*r_npol
          endif
          r_ldump = r_ldpar + r_ldatc
          !
          ndata = r_ndump*(r_ldpar+r_ldatc) + r_ldpar+r_ldatc   &
            +r_ldatl
          if (r_ndatl.gt.1) then
            ndata = ndata + r_ldpar+r_ldatc+r_ldatl
          endif
          if (allocated(dataArray)) then
            if (ndata.ne.size(dataArray)) then
              deallocate(dataArray, stat=ier)
              if (ier.ne.0) goto 98
              allocate(dataArray(ndata), stat=ier)
              if (ier.ne.0) goto 97
            endif
          else
            allocate(dataArray(ndata), stat=ier)
            if (ier.ne.0) goto 97
          endif
          !
          !     Binary data
          if (.not.tpList(i)) then
            !
            call get_sdm_CorrelatorData   &
              (SpectralResolutionType_FULL_RESOLUTION, ndata   &
              , dataArray, uid, numIntegration, jobs, error)
            if (error) then
              print *, 'Error in get_sdm_CorrelatorData  main Row ',i, ' FULL_RESOLUTION ', uid
              goto 99
            endif
            !
            !     find the row for channel average data
            mKey = mKeyList(i)
            mKey%configdescriptionId = assocChannelAverageID
            call getMainRow(mKey, mRow, error)
            if (error) then
              print *, 'Error in Main row: mKey ',mKey
              return 
            endif
            l = lenz(mRow%DataOid)
            k = index(mRow%DataOid(1:l),'"uid:')
            k2 = index(mRow%DataOid(k+1:),'"')
            uid =  mRow%DataOid(k+1:k+k2-1)
            !
            configDescription_Id = mKey%configdescriptionId
            call get_sdm_ConfigDescription(error)
            if (error) goto 99
            !
            call get_sdm_DataDescription(error)
            if (error) goto 99
            !
            call get_sdm_spectralWindow(holodata, error)
            if (error) goto 99
            !
            call get_sdm_CorrelatorData   &
              (SpectralResolutionType_CHANNEL_AVERAGE, ndata   &
              , dataArray, uid, numIntegration, jobs, error)
            !
            if (assocBasebandWideID.ge.0) then
              mKey%configdescriptionId = assocBasebandWideID
              call getMainRow(mKey, mRow, error)
              if (error) then
                print *, 'Error in Main Row:  mKey ',mKey
                return
              endif
              if (error) return
              l = lenz(mRow%DataOid)
              k = index(mRow%DataOid(1:l),'"uid:')
              k2 = index(mRow%DataOid(k+1:),'"')
              uid =  mRow%DataOid(k+1:k+k2-1)
              call get_sdm_TotalPowerData(ndata, dataArray,   &
                uid, numIntegration, 1, error)
            endif
          !
          !     still to be done: here read Binary Total Power data if present
          !
          else
            l = lenz(mRow%DataOid)
            k = index(mRow%DataOid(1:l),'"uid:')
            k2 = index(mRow%DataOid(k+1:),'"')
            uid =  mRow%DataOid(k+1:k+k2-1)
            call get_sdm_TotalPowerData(ndata, dataArray,   &
              uid, numIntegration, jobs, error)
          !
          endif
          !
          !     Now loop on subintegrations
          do j = 1, r_nant
            minlam(j) = 1e20
            minbet(j) = 1e20
            minfoc(j) = 1e20
            maxlam(j) = -1e20
            maxbet(j) = -1e20
            maxfoc(j) = -1e20
            r_mobil(j) = .false.
          enddo
          first = .true.
          !     Loop on records to read tables that vary faster then integration
          !     (pointing, focus, ...
          allocate(timeIntervalRecord(r_ndump))
          do ir = 1, r_ndump
            !     now we always calculate time_interval by formula.
            k = 1 + h_offset(ir)
            call decode_header(dataArray(k))
            time_Interval = (/ timeIntervalObs(1) &
            + (ir-1)*timeIntervalObs(2)/r_ndump,  &
            timeIntervalObs(2) /r_ndump /)
            timeIntervalRecord(ir) = ArrayTimeInterval(time_Interval(1), time_Interval(2))
            call setDataHeaderTimes(time_interval)
            configDescription_Id = mKey%configDescriptionId
            field_Id = mKey%fieldId
            dh_aflag = 0
            dh_bflag = 0
            !     now not needed
            dh_dump = ir
            !     should now be re-calculated...
            dh_uvm = 0
            !     read the pointing table
            call getPointingTableSize(pointingSize, error)
            if (error) return
            !if (pointingSize.gt.0) then
            !  call get_sdm_Pointing(first, error)
            !  if (error) return
            !endif
            call getFocusTableSize(FocusSize, error)
            if (error) return
            if (FocusSize.gt.0) then
              call get_sdm_Focus(first, error)
              if (error) return
            endif
            !     ### not supposed to change inside a subscan...
            call get_sdm_State(error)
            if (error) return
            !
            !     c               call get_sdm_TotalPower(holoData, tpmSize,
            !     c     &              tpmKeyList,error)
            !     c               if (error) return
            k = 1 + h_offset(ir)
            call encode_header(dataArray(k))
          !
          enddo
          !
          call get_sdm_pointing(ndata, dataArray, timeIntervalSubscan, &
               timeIntervalRecord, error)
          if (error) return
          !
          !     final checks
          do ir = 1, r_ndump
            !     now we always calculate time_interval by formula.
            k = 1 + h_offset(ir)
            call decode_header(dataArray(k))
            !     scanning stuff
            do j=1, r_nant
              minfoc(j) = min(minfoc(j),dh_offfoc(j))
              maxfoc(j) = max(maxfoc(j),dh_offfoc(j))
              minlam(j) = min(minlam(j),dh_offlam(j)*pi/180.   &
                /3600.)
              maxlam(j) = max(maxlam(j),dh_offlam(j)*pi/180.   &
                /3600.)
              minbet(j) = min(minbet(j),dh_offbet(j)*pi/180.   &
                /3600.)
              maxbet(j) = max(maxbet(j),dh_offbet(j)*pi/180.   &
                /3600.)
              !! print *, 'ir, j, minlam(j), maxlam(j) ',ir, j, minlam(j), maxlam(j)
            enddo
 
          enddo


          do j=1, r_nant
            !! print *, 'j, minlam(j), maxlam(j) ',j, minlam(j), maxlam(j)
            if (abs(maxfoc(j)).gt.0.01) then
              r_mobil(j) = .true.
            endif
            !if (maxlam(j)-minlam(j).gt.2.5*pi/180./3600.) then
            if ((abs(maxlam(j)).gt.2.5*pi/180./3600.).or.(abs(minlam(j)).gt.2.5*pi/180./3600.)) then
              r_mobil(j) = .true.
              if (r_scaty.le.3) r_scaty = scan_lamb
            endif
            !if (maxbet(j)-minbet(j).gt.2.5*pi/180./3600.) then
            if ((abs(maxbet(j)).gt.2.5*pi/180./3600.).or.(abs(minbet(j)).gt.2.5*pi/180./3600.)) then
              r_mobil(j) = .true.
              if (r_scaty.le.3) r_scaty = scan_beta
            endif
          enddo
          !
          !     Write the observation in IPB file:
          !
          r_kind = 5
          call ipb_write(write_mode,check,error)
          if (error) goto 99
          call wdata (ndata,dataArray,.true.,error)
          if (error) goto 99
          call ipb_close(error)
          if (error) goto 99
          write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
            ' '//done(write_mode)//' (',r_ndump,' records)'
          call message(4,1,'READ_SDM',chain(1:50))
        !     end loop on modes
          if (sic_ctrlc()) then
             error = .true.
             goto 99
          endif
          deallocate(timeIntervalRecord, stat=ier)
        enddo
      !     end loop on observations
      enddo
    endif
  !     end loop on Main Rows
  enddo
  !
  call sdm_close()
  return
  !
97 call sdmmessageI (8,3,'READ_SDM','Allocate error', ier)
  goto 99
98 call sdmmessageI (8,3,'READ_SDM','Deallocate error', ier)
  goto 99
  !
  !
99 call message(8,4,'Read_sdm','SDM Read Failed')
  error = .true.
  return
end subroutine read_sdm_tables
!--------------------------------------------------------------------
