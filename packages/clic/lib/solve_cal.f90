subroutine solve_cal(line,error)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: m_cal
  integer(kind=address_length) :: ipx,ipy,ipw,ips,ipcal,data_cal
  !------------------------------------------------------------------------
  ! Code:
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ips = gag_pointer(data_s,memory)
  !
  ! just in case m_data has changed
  call check_index(error)
  if (error) return
  call get_first(.true.,error)
  if (error) return
  !
  m_cal = m_data*8
  error = sic_getvm(m_cal,data_cal).ne.1
  if (error) goto 999
  ipcal = gag_pointer(data_cal,memory)
  call sub_solve_cal(m_data,m_boxes,memory(ipx),   &
    memory(ipy),memory(ipw),   &
    memory(ips), memory(ipcal+6*m_data),   &
    memory(ipcal),   &
    memory(ipcal+2*m_data), memory(ipcal+4*m_data),   &
    line,error)
  call free_vm(m_cal,data_cal)
  return
  !
999 error = .true.
  return
end subroutine solve_cal
!
subroutine sub_solve_cal(md,mb,x_data,y_data,w_data,   &
    ind,wk1,xxx,yyy,www,line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  !	Computes a calibration curve, based on the phase calibrators,
  !	assumed to be in the current index.
  !	Amplitudes should be also checked.
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: ind(md)                !
  real*8 :: wk1(3*md)               !
  real*8 :: xxx(md)                 !
  real*8 :: yyy(md)                 !
  real*8 :: www(md)                 !
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: nk, iy, isb, iba, ib, i, l, l1, l2, l3, nn, nd,ipola
  integer :: n_br, k_br(10), ikn, nkn, j, ir, ia, ja, iarg
  real*8 :: cs(m_spl+4), ks(m_spl+8), rss, ss, sss
  real*8 :: xx(10*m_spl), yy(10*m_spl), xx1
  real*8 :: wk2(4*(m_spl+8)), wk3(2*m_pol)
  real*8 :: w0, xold, xnew
  real*8 :: apol(m_pol,m_pol), spol(m_pol), aa(m_pol)
  real :: x4, x2, y2, x3, y3, t_br(10), tkn(40), step
  logical :: plot, reset, poly, weight
  character(len=132) :: chain
  integer :: mvx, nkey, ipol, nt, old_pen, new_pen, nch
  parameter (mvx=2)
  character(len=12) :: kw,vx(mvx),argum
  logical :: solve_amp, solve_phase
  real :: rms_err(mbox)
  save rms_err
  ! Data
  data vx /'AMPLITUDE','PHASE'/
  !------------------------------------------------------------------------
  ! Code:
  if (do_polar.eq.0) then
    ipola = 1
  elseif (do_polar.gt.0.and.do_polar.le.3) then
    ipola = do_polar
  else
    call message(4,3,'SOLVE_CAL','Do not know this polarisation')
    error = .true.
    return
  endif
  old_pen = -1
  if (i_base(1).gt.mnbas) then
    call message(8,4,'SOLVE_PASS','Triangle mode not supported')
    error = .true.
  endif
  solve_amp = .false.
  solve_phase = .false.
  nd = min(2,sic_narg(0))
  do i=1,nd
    call sic_ke(line,0,i,argum,nch,.false.,error)
    if (error) goto 999
    call sic_ambigs('SOLVE_CAL',argum,kw,nkey,vx,mvx,error)
    if (error) goto 999
    if (nkey.eq.1) then
      solve_amp = .true.
    elseif (nkey.eq.2) then
      solve_phase = .true.
    endif
  enddo
  !
  poly = sic_present(9,0)
  ipol = 0
  if (poly) then
    call sic_i4(line,9,1,ipol,.false.,error)
    if (error) goto 999
    ipol = max(min(ipol+1,m_pol),1)    ! degree+1
  endif
  reset = sic_present(4,0)
  if (reset) then
    call reset_time
  endif
  !
  n_br = 0
  if (sic_present(8,0)) then
    i = 0
    do while (i.lt.sic_narg(8))
      n_br = n_br+1
      call sic_i4(line,8,i+1,k_br(n_br),.true.,error)
      if (error) goto 999
      k_br(n_br) = min(max(k_br(n_br),0),3)
      call sic_r4(line,8,i+2,t_br(n_br),.true.,error)
      if (error) goto 999
      i = i + 2
    enddo
  endif
  !
  save_scale = do_scale
  save_flux = do_flux
  save_amplitude = do_amplitude
  save_phase = do_phase
  if (solve_amp .and. (do_amplitude .or. .not.do_scale   &
    .or. do_flux)) then
    do_scale = .true.
    do_amplitude = .false.
    do_flux = .false.
    change_display = .true.
  endif
  if (solve_phase .and. do_phase) then
    do_phase = .false.
    change_display = .true.
  endif
  call show_general('AMPLITUDE',.false.,line,error)
  if (change_display) then
    ! plot all scans in index, sorted, plot buffer re-initialized.
    call read_data('ALL',.true.,.true., error)
    plotted = .false.
    if (error) goto 999
    change_display = .false.
  else
    plotted = .true.
  endif
  plot = sic_present(1,0)
  weight = sic_present(2,0)
  if (plot) then
    clear = .true.
    if (.not. plotted) call sub_plot('A',.false.,.false.,0,error)
    if (error) goto 999
    plotted = .true.
    if (sic_narg(1).ge.1) then
      call sic_i4(line,1,1,iarg,.false.,error)
      if (iarg.lt.0.or.iarg.gt.15) then
        call message(6,3,'SOLVE_CAL','Invalid pencil number')
        error = .true.
        return
      endif
      new_pen = iarg
    else
      new_pen = 1
    endif
    old_pen = gr_spen (new_pen)
    call gr_segm('FIT',error)
  endif
  !
  ! Loop on boxes
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    isb = i_band(k_band(ib))
    iba = i_base(k_base(ib))
    if (n_data(ib).eq.0) then
      call message(4,3,'SOLVE_CAL','No data in that box')
      goto 200
    elseif (f_sres(ib)) then
      l1 = lenc(header_1(ib))
      call message(4,2,'SOLVE_CAL','Baseline '   &
        //header_1(ib)(1:l1)//' ignored (residuals)')
      goto 200
    elseif (i_x(k_x(ib)).ne. 9) then
      call message(4,2,'SOLVE_CAL','X axis should be Time')
      goto 200
    elseif (isb.gt.3) then
      call message(4,2,'SOLVE_CAL',   &
        'Band should Upper, Lower, or DSB')
      goto 200
    elseif ((isb.eq.3)   &
      .and. .not.do_pass .and. iy.eq.xy_phase) then
      call message(4,2,'SOLVE_CAL',   &
        'Please apply RF passband to solve '//   &
        'for DSB phase calibration')
      goto 200
    elseif (iy.gt.xy_phase) then
      call message(4,2,'SOLVE_CAL',   &
        'Y axis should be Amplitude or Phase')
      goto 200
    elseif (iy.eq.xy_ampli .and. .not.solve_amp) then
      call message(4,2,'SOLVE_CAL',   &
        'Y axis should be Phase')
      goto 200
    elseif (iy.eq.xy_phase .and. .not.solve_phase) then
      call message(4,2,'SOLVE_CAL',   &
        'Y axis should be Amplitude')
      goto 200
    elseif (iba.le.0) then
      call message(4,2,'SOLVE_CAL',   &
        'Use a baseline plot (SET BASELINE)')
      goto 200
    endif
    nkn = 0
    if (n_br.gt.0) then
      do i=1, n_br
        do j=1,4-k_br(i)
          nkn = nkn + 1
          tkn(nkn) = t_br(i)
        enddo
      enddo
    endif
    w0 = 0
    !
    ! Load arrays for NAG, ignoring blanked values.
    nd = 0
    do i=1, n_data(ib)
      if (y_data(i,ib) .ne. blank4) then
        nd = nd+1
        xxx(nd) = x_data(i,ib)
        yyy(nd) = y_data(i,ib)
        ! The MTH_FITPOL routine requires 1/sigma values
        if (w0.eq.0) w0 = sqrt(w_data(i,ib))
        ! Use ALWAYS Weights for amplitudes:
        if (weight .or. iy.eq.xy_ampli) then
          www(nd) = sqrt(w_data(i,ib))
        else
          www(nd) = w0
        endif
      endif
    enddo
    if (nd.eq.0) goto 200
    !
    if (.not.poly .and. nd.eq.3) then
      y2 = yyy(2)-yyy(1)
      y3 = yyy(3)-yyy(1)
      x2 = xxx(2)-xxx(1)
      x3 = xxx(3)-xxx(1)
      xxx(4) = (xxx(3)+xxx(2))/2
      x4 = xxx(4)-xxx(1)
      yyy(4) = yyy(1) + x4**2*(y2/x2-y3/x3)/(x2-x3)   &
        + x4*(y3*x2/x3-y2*x3/x2)/(x2-x3)
      www(4) = (www(3)+www(2))/2
      nd = 4
    elseif (.not. poly .and. nd.eq.2) then
      y2 = yyy(2)-yyy(1)
      x2 = xxx(2)-xxx(1)
      xxx(3) = xxx(2)-x2/3
      xxx(4) = xxx(1)+x2/3
      yyy(3) = yyy(2)-y2/3
      yyy(4) = yyy(1)+y2/3
      www(3) = www(2)
      www(4) = www(1)
      nd = 4
    elseif (nd.eq.1) then
      xxx(2) = xxx(1)+0.01
      xxx(3) = xxx(1)+0.02
      xxx(4) = xxx(1)-0.01
      yyy(2) = yyy(1)
      yyy(3) = yyy(1)
      yyy(4) = yyy(1)
      www(2) = www(1)
      www(3) = www(1)
      www(4) = www(1)
      nd = 4
    endif
    sss = 0
    do i=1, nd
      sss = sss + www(i)**2
    enddo
    ! Sort if required
    if (gr8_random(xxx,nd)) then
      call gr8_trie(xxx,ind,nd,error)
      if (error) goto 999
      call gr8_sort(yyy,wk1,ind,nd)
      call gr8_sort(www,wk1,ind,nd)
    endif
    ! Set knots
    if (.not. poly) then
      xold = xxx(1)
      ikn = 0
      nk = 0
      do while (xold.lt.xxx(nd))
        if (ikn.eq.nkn) then
          xnew = xxx(nd)
        elseif (nk.lt.m_spl) then
          ikn = ikn + 1
          xnew = tkn(ikn)
          nk = nk+1
          ks(nk+4) = tkn(ikn)
        else
          call message(8,3,'SOLVE_CAL',   &
            'Too many knots for this spline')
          error = .true.
          return
        endif
        nt = (xnew-xold)/t_step
        nt = min(nt, m_spl-nk, nd-4)
        if (nt.gt.0) then
          step = (xnew-xold)/(nt+1)
          if (nk+nt.gt.m_spl) then
            call message(8,3,'SOLVE_CAL',   &
              'Too many knots for this spline')
            error = .true.
            return
          endif
          do i=1, nt
            ks(nk+i+4) = xold + i*step
          enddo
          nk = nk + nt
        endif
        xold = xnew
      enddo
      if (nk.gt.0) call gr8_trie(ks(5:),ind,nk,error)
      if (error) goto 999
      !		write(*,*) 'knots ',nk
      !		if (nk.gt.0) write(*,*) (ks(i),i=5,nk+4)
      call mth_fitspl ('SOLVE_CAL', nd, nk+8, xxx,   &
        yyy, www, ks, wk1, wk2, cs, ss, error)
      if (error) goto 999
      rss = sqrt(ss/sss)
    else
      ipol = min(ipol,nd)
      call mth_fitpol('SOLVE_CAL',nd,ipol,m_pol,   &
        xxx,yyy,www,wk1,wk3,apol,spol,error)
      if (error) goto 999
      rss = sqrt(sss/nd)       ! rms obtenu
      rss = spol(ipol)/rss
    endif
    ! display results
100 l = min(lenc(y_label(ib)),5)
    l1 = lenc(header_1(ib))
    l2 = lenc(header_2(ib))
    l3 = lenc(header_3(ib))
    rms_err(ib) = rss
    write(chain,1000) y_label(ib)(1:3), header_1(ib)(1:l1),   &
      header_2(ib)(1:l2), header_3(ib)(1:l3), rss
1000 format(a,'. ',a,1x,' ',a,' ',a,1x,   &
      'rms: ',1pg10.3)
    call message(6,1,'SOLVE_CAL',chain(1:lenc(chain)))
    if (plot) then
      nn = 10.*m_spl
      step = (gb2_x(ib)-gb1_x(ib)) / (nn-1)
      if (step.eq.0) step = 0.1
      if (poly) then
        do i=1, ipol
          aa(i) = apol(ipol,i)
        enddo
      endif
      do i=1, nn
        xx(i) = gb1_x(ib) + (i-1) * step
        if (.not.poly) then
          xx1 = min(xx(i),ks(nk+5))
          xx1  = max(xx1,ks(4))
          call mth_getspl('SOLVE_CAL',   &
            nk+8, ks, cs, xx1, yy(i), error)
          if (error) goto 999
        else
          xx1 = min(max(xx(i),xxx(1)),xxx(nd))
          xx1 = ((xx1-xxx(1))-(xxx(nd)-xx1))   &
            /(xxx(nd)-xxx(1))
          call mth_getpol('SOLVE_CAL',   &
            ipol,aa,xx1,yy(i),error)
          if (error) goto 999
        endif
        if (iy.eq.xy_ampli .and. iba.lt.0) then
          yy(i) = yy(i)**2
        endif
      enddo
      write(chain,'(I4.4)') ib
      call gr_execl('CHANGE DIRECTORY BOX'//chain)
      error = gr_error()
      if (error) goto 999
      !            L = LENC(C_SETBOX(IB))
      !            CALL GR_EXEC(C_SETBOX(IB)(1:L))
      !            WRITE(CHAIN,'(a,4(1x,g19.12))') 'LIMITS ',
      !     $      GB1_X(IB),GB2_X(IB),GB1_Y(IB), GB2_Y(IB)
      !            L = LENC(CHAIN)
      !            CALL GR_EXEC(CHAIN(1:L))
      call gr8_connect (nn,xx,yy,0.0d0,-1.0d0)
      call gr_execl('CHANGE DIRECTORY')
    endif
    !
    ! Save results
    ir = r_nrec
    if (iy.eq.xy_phase .and. degrees) rss = rss*pi/180.
    if (.not.poly) then
      n_knots(iy,isb,ipola,iba,ir) = nk
      do i=1, nk+8
        k_spline(i,iy,isb,ipola,iba,ir) = ks(i)    ! in hours
      enddo
      do i=1, nk+4
        if (iy.eq.xy_phase .and. degrees) then
          c_spline(i,iy,isb,ipola,iba,ir) = cs(i)*pi/180.d0
        else
          c_spline(i,iy,isb,ipola,iba,ir) = cs(i)
        endif
      enddo
      rms_spline_bas(iy,isb,ipola,iba,ir) = rss
      if (do_phase_ext) then
        f_spline(iy,isb,ipola,iba,ir) = 3
      else
        f_spline(iy,isb,ipola,iba,ir) = 1
      endif
      f_pol(iy,isb,ipola,iba,ir) = 0
      ph_fac(1,ipola,iba,ir) = r_flo1+r_fif1
      ph_fac(2,ipola,iba,ir) = r_flo1-r_fif1
      ph_fac(3,ipola,iba,ir) = r_flo1
    else
      n_pol(iy,isb,ipola,iba,ir) = ipol
      do i=1, ipol
        if (iy.eq.xy_phase .and. degrees) then
          c_pol(i,iy,isb,ipola,iba,ir) =   &
            apol(ipol,i)*pi/180.d0
        else
          c_pol(i,iy,isb,ipola,iba,ir) = apol(ipol,i)
        endif
      enddo
      t_pol(1,iy,isb,ipola,iba,ir) = xxx(1)
      t_pol(2,iy,isb,ipola,iba,ir) = xxx(nd)
      rms_pol_bas(iy,isb,ipola,iba,ir) = rss
      if (do_phase_ext) then
        f_pol(iy,isb,ipola,iba,ir) = 3
      else
        f_pol(iy,isb,ipola,iba,ir) = 1
      endif
      f_spline(iy,isb,ipola,iba,ir) = 0
      ph_fac(1,ipola,iba,ir) = r_flo1+r_fif1
      ph_fac(2,ipola,iba,ir) = r_flo1-r_fif1
      ph_fac(3,ipola,iba,ir) = r_flo1
    endif
    ! side band averages
    if (isb.gt.2) then
      icsba(isb,iba,ir) = 1.0
    elseif (do_pass_memory) then
      icsba(isb,iba,ir) = sba(isb,iba,ir)
    elseif (r_bpc.ne.0) then
      icsba(isb,iba,ir) = r_bpcsba(1,isb,iba)
    elseif (r_abpc.ne.0) then
      ia = r_iant(iba)
      ja = r_jant(iba)
      icsba(isb,iba,ir) = r_abpcsba(1,isb,ja)/r_abpcsba(1,isb,ia)
    else
      call message(8,2,'SOLVE_CAL','No RF Passband available')
    endif
    !
200 continue
  enddo
  call sic_delvariable ('RMS_ERR',.false.,error)
  error = .false.
  call sic_def_real ('RMS_ERR',rms_err,1,n_boxes,.false.,error)
  error = .false.
998 continue
  if (old_pen.ne.-1)  then
    call gr_segm_close(error)
    i = gr_spen(old_pen)
  endif
  do_scale = save_scale
  do_flux = save_flux
  do_amplitude =  save_amplitude
  do_phase =  save_phase
  return
  !
999 error = .true.
  goto 998
end subroutine sub_solve_cal
!
subroutine nag_fail (fac,prog,ifail,error)
  use gkernel_interfaces
  character(len=*) :: fac           !
  character(len=*) :: prog          !
  integer :: ifail                  !
  logical :: error                  !
  ! Local
  character(len=60) :: chain
  !
  if (ifail.eq.0) return
  write(chain,'(A,A,A,I4)') 'ERROR in ',prog,', ifail = ',ifail
  call message(8,4,fac,chain(1:lenc(chain)))
  error = .true.
  return
end subroutine nag_fail
!
function gr8_random (x,n)
  logical :: gr8_random             !
  integer :: n                      !
  real*8 :: x(n)                    !
  ! Local
  integer :: i
  !
  do i=1,n-1
    if (x(i).gt.x(i+1)) then
      gr8_random = .true.
      return
    endif
  enddo
  gr8_random = .false.
end function gr8_random
