subroutine titout(type,error)
  use gkernel_interfaces
  use gbl_constant
  use gildas_def
  use clic_title
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC	Internal routine
  !	Write a title
  ! Arguments :
  !	TYPE	C*(*)	Type of title to be displayed:
  !
  !      FULL = all informations - used by HEADER command
  !      LONG = part/all informations, depending on SET FORMAT  - used by
  !             LIST /LONG command
  !      SHORT/ZHORT = first/last scans on same line - used by LIST /SHORT
  !             command; NB: SHORT = first pass, ZHORT = 2nd pass
  !      NORMAL = summary information on one line - used by LIST, PLOT,
  !             CURSOR commands
  !      PLOT/BLOT = special for PLOT command (NGRx); NB: PLOT = first
  !             pass, BLOT = 2nd pass
  !---------------------------------------------------------------------
  character(len=*) :: type          !
  logical :: error                  !
  ! Global
  character(len=4) :: procname
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_title.inc'
  include 'clic_display.inc'
  include 'clic_dheader.inc'
  ! Local
  integer :: mg
  parameter (mg=1000)
  integer :: ng, sg(2,mg)
  common/scan_group/ng, sg
  save /scan_group/
  character(len=60) :: pad
  character(len=255) :: ch,ch1,ch2
  character(len=2) :: systext(0:6)
  character(len=5) :: veltext(0:4)
  integer :: i, l_ifpb, lch, npad, npad1, ier
  logical :: long, full, short, plot
  character(len=1) :: chartype(0:3), ctype
  character(len=4) :: cproc
  integer :: msourc(3),mline(3),mteles(3), mscan, nscan, mproject(2), mnum
  character(len=11) :: mch1
  character(len=4) :: mctype,mcproc
  character(len=5) :: mch2
  real :: nhoura, mhoura
  character(len=11) :: mdate
  real*8 :: lut8
  character(len=12) :: aline
  character(len=8) :: aproject
  character(len=12) :: ateles,asourc
  integer :: qq, quarter, corr_input, intw
  character(len=36) :: narrow(2)
  character(len=3) :: pol, fre
  !
  data systext /'Un','Un','Eq','Ga','Ho','Pl','Ea'/
  data veltext /'Unkn.','LSR  ','Hel. ','Obs. ','Null '/
  data chartype /'*','O','P','R'/
  data mscan/0/,mproject/2*0/,mhoura/0/,nscan/0/,nhoura/0/,mnum/0/
  data msourc,mline,mteles/3*0,3*0,3*0/
  !------------------------------------------------------------------------
  save mscan, mproject, mhoura, nscan, nhoura,   &
    msourc, mline, mteles, mcproc, mctype, mdate, mnum, mch1, mch2
  ! Code:
  if (title%num.le.0) then
    call message(6,3,'TITOUT','No header in memory')
    error = .true.
    return
  endif
  !
  call chtoby('IFPB',l_ifpb,4)
  !
  full = type(1:1).eq.'F'
  long = type(1:1).eq.'L'
  short = (type(1:1).eq.'S').or.(type(1:1).eq.'Z')
  plot = (type(1:1).eq.'P').or.(type(1:1).eq.'B')
  !
  ! Long title -------------------------------------------------------------
  ! CALL TITOUT(LONG) or CALL TITOUT(FULL)
  !
  if (long .or. full) then
    !
    ! First line:
    call outlin(' ',1)
    call datec (r_dobs,ch1,error)
    if (r_itype.ge.0 .and. r_itype.le.3) then
      ctype = chartype(r_itype)
    else
      ctype = '?'
    endif
    cproc = procname(r_proc)
    lut8 = title%ut
    call sexag(ch2,lut8,24)
    call bytoch(title%project,aproject,8)
    call bytoch(title%sourc,asourc,12)
    call bytoch(title%teles,ateles,12)
    write(ch,210,iostat=ier) mod(title%num,100000),   &
      mod(title%scan,10000),aproject,asourc,ctype,cproc,   &
      ateles, ch1(1:11),ch2(2:6),title%houra*12./pi
    call outlin(ch,80)
    !
    ! Position
    if (t_position .or. full) then
      call outlin(' ',1)
      if (abs(r_typec).eq.type_eq .or. r_typec.eq.0) then
        call sexag(ch2,r_lam,24)
        ch = 'RA:'//ch2(1:13)
        lch = 17
        call sexag(ch2,r_bet,360)
        ch(lch+1:) = 'Dec:'//ch2(1:13)
        lch = lch+18
        write(ch(lch+1:),103) r_epoch
        lch=lch+8
      else
        write(ch,104) 180.*r_lam/pi,180.*r_bet/pi
        lch = 43
      endif
      ch(lch+2:) = 'Offs: '
      lch = lch+6
      call offsec(r_lamof,ch(lch+1:),error)
      lch = lch+7
      call offsec(r_betof,ch(lch+1:),error)
      lch = lch+7
      if (r_typec .lt. 0) then
        ch(lch+1:) = 'Ho'
      else
        ch(lch+1:)= systext(r_typec)
      endif
      lch = lch+3
      write(ch(lch+1:),'(a6,f6.1)') 'Flux: ', r_flux
      lch = lch+12
      call outlin(ch,lch)
    endif
    !
    ! Noise information
    if (t_quality .or. full) then
      call outlin(' ',1)
      ch1 = 'Unflagged'
      do i=1, r_nant
        if (dh_aflag(i).ne.0) ch1='FLAGGED'
      enddo
      do i=1, r_nbas
        if (dh_bflag(i).ne.0) ch1='FLAGGED'
      enddo
      write(ch,102) quality(r_qual), ch1, r_nrec, r_ndump,   &
        r_az*180./pi, r_el*180./pi
      call outlin(ch,80)
    endif
    !
    ! Radio Frequency
    if (t_status .or. full) then
      call outlin(' ',1)
      ch='Line: '//r_line
      lch = 19
      write(ch(lch+1:),'(a5,f10.3)') 'Fr.: ',r_restf
      lch = lch+16
      if (r_isb.gt.0) then
        ch(lch+1:) = 'USB'
      else
        ch(lch+1:) = 'LSB'
      endif
      lch = lch+4
      if (r_lock.gt.0) then
        ch(lch+1:) = 'HIGH'
      else
        ch(lch+1:) = 'LOW'
      endif
      lch = lch+5
      write(ch(lch+1:),'(a5,e12.5)') 'Dop: ',r_doppl
      lch = lch+18
      write(ch(lch+1:),'(a5,f6.1)') 'Vel: ',r_veloc
      lch = lch+12
      ch(lch+1:) = veltext(r_typev)
      lch = lch+5
      call outlin(ch,lch)
    endif
    !
    ! Correlator information
    if (t_spectral.or.full) then   !
      call outlin(' ',1)
      if (new_receivers) then
        ch = 'Narrow-band correlator:'
        call outlin(ch,23)
        do i=1, r_lband
          ch = ' '
          lch = 6
          qq = quarter(r_flo2(i),r_flo2bis(i))
          corr_input = r_bb(i) 
          if (r_lpolmode(i).ne.1) then
            pol = 'Mix'
          else
            if (r_lpolentry(1,i).eq.1) then
              pol = 'HOR'
            else
              pol = 'VER'
            endif
          endif
          intw = r_lnch(i)*abs(r_lfres(i))
          write(ch(lch+1:),108) 'L',i,intw, r_lfcen(i),   &
            r_iunit(i),qq,pol,corr_input
          !                  LCH = LENC(CH)
          if (r_iunit(i).lt.9) then
             call outlin(ch,lch)
          endif
        enddo
        if (r_nband_widex.ne.0) then
          ch = 'Wide-band correlator:'
          call outlin(ch,23)
          do i=1, r_lband
             if (r_iunit(i).gt.8) then
                ch = ' '
                lch = 6
                if (r_lpolmode(i).ne.1) then
                   pol = 'Mix'
                else
                   if (r_lpolentry(1,i).eq.1) then
                      pol = 'HOR'
                   else
                      pol = 'VER'
                   endif
                endif
                if (r_flo2(i).lt.9000) then
                   fre = 'Low'
                else
                   fre = 'Up '
                endif
                write(ch(lch+1:),109) 'L',   &
                       i, r_iunit(i),pol,fre
                call outlin(ch,lch)
             endif
          enddo
        endif
      else                     ! Old receivers
        ch = 'Corr: '
        lch = 6
        do i=1, r_lband
          write(ch(lch+1:),107) 'L',i,r_lnch(i)*abs(r_lfres(i)),   &
            r_lfcen(i),r_iunit(i)
          lch = lch+38
          if (lch.gt.60) then
            call outlin(ch,lch)
            ch = ' '
            lch = 6
          endif
        enddo
        if (lch.gt.20) call outlin(ch,lch)
      endif
    endif
    !
    ! Interferometer
    ! (ca tient jusqu'a 8 antennes...)
    pad = '  '
    npad = 67/r_nant - 7
    npad1 = 67-r_nant*(npad+7)+1
    if (t_interfero .or. full) then
      call outlin(' ',1)
      write(ch,'(a10,a,12(a,i7))') 'Antennas: ',pad(1:npad1),   &
        (pad(1:npad),r_kant(i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,'(a10,a,12(a,3x,a4))') 'Stations: ',pad(1:npad1),   &
        (pad(1:npad),r_config(4*i-3:4*i),i=1,r_nant)
      call outlin(ch,79)
    endif
    !
    ! Calibration information
    if (t_pointing .or. full) then
      call outlin(' ',1)
      write(ch,113) 'Cor.Az:   ',pad(1:npad1),   &
        (pad(1:npad),r_collaz(i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,113) 'Cor.El:   ',pad(1:npad1),   &
        (pad(1:npad),r_collel(i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,113) 'Cor.Fo:   ',pad(1:npad1),   &
        (pad(1:npad),r_corfoc(i),i=1,r_nant)
      call outlin(ch,79)
    endif
    ! NGRX: TODO ... better than using the first polar only here.
    if (t_calibration .or. full) then
      call outlin(' ',1)
      write(ch,113) 'Trec:     ',pad(1:npad1),   &
        (pad(1:npad),r_trec(1,i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,113) 'G.Im.:    ',pad(1:npad1),   &
        (pad(1:npad),r_gim(1,i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,113) 'Water:    ',pad(1:npad1),   &
        (pad(1:npad),r_h2o_mon(i),i=1,r_nant)
      call outlin(ch,79)
      write(ch,113) 'Tsys:     ',pad(1:npad1),   &
        (pad(1:npad),r_tsyss(1,i),i=1,r_nant)
      call outlin(ch,79)
    endif
  !
  ! Brief title --------------------------------------------------------------
  ! CALL TITOUT(NORMAL) or CALL TITOUT(SHORT) or CALL TITOUT(ZHORT)
  ! or CALL TITOUT(PLOT) or CALL TITOUT(BLOT)
  !
  else
    call datec(title%dobs,ch1,error)
    if (title%itype.ge.0 .and. title%itype.le.3) then
      ctype = chartype(title%itype)
    else
      ctype = '?'
    endif
    cproc = procname(title%proc)
    if (short) then
      !
      ! CALL TITOUT(SHORT) or CALL TITOUT(ZHORT)
      !
      if (mscan.ne.0 .and.   &
        (type(1:1).eq.'Z' .or. msourc(1).ne.title%sourc(1)   &
        .or. msourc(2).ne.title%sourc(2) .or. msourc(3).ne.title%sourc(3)   &
        .or. mctype.ne.ctype   &
        .or. mcproc.ne.cproc   &
        .or. mline(1).ne.title%line(1)   &
        .or. mline(2).ne.title%line(2)   &
        .or. mline(3).ne.title%line(3)   &
        .or. mproject(1).ne.title%project(1)  &
        .or. mproject(2).ne.title%project(2)  &
        .or. mteles(1).ne.title%teles(1)   &
        .or. mteles(2).ne.title%teles(2)   &
        .or. mteles(3).ne.title%teles(3)   &
        .or. mdate.ne.ch1(1:11))) then
        if (mcproc.ne.'IFPB'.and.mcproc.ne.'CALI') then
          call bytoch(mproject,aproject,8)
          call bytoch(msourc,asourc,12)
          call bytoch(mline,aline,12)
          call bytoch(mteles,ateles,12)
          write(ch,301,iostat=ier) mod(nscan,10000),   &
            mod(mscan,10000), aproject, asourc, mctype, mcproc,   &
            aline(1:12), ateles, mdate, nhoura*12/pi,   &
            mhoura*12/pi
          ng = ng + 1
          if (ng.gt.mg) then
            call message(8,4,'TITOUT','Too many lines')
            error = .true.
            return
          endif
          sg(1,ng) = mod(nscan,10000)
          sg(2,ng) = mod(mscan,10000)
          call outlin(ch,80)
          nscan = title%scan
        endif
        if (title%proc.ne.l_ifpb) nhoura = title%houra
      endif
      do i=1, 3
        msourc(i) = title%sourc(i)
        mline(i) = title%line(i)
        mteles(i) = title%teles(i)
      enddo
      mdate = ch1(1:11)
      mscan = title%scan
      if (nscan.le.0) then
        nscan = title%scan
        nhoura = title%houra
      endif
      if (type(1:1).eq.'Z') then
        mscan = 0
        nscan = 0
      endif
      mhoura = title%houra
      mproject(1) = title%project(1)
      mproject(2) = title%project(2)
      mctype = ctype
      mcproc = cproc
    elseif (plot) then
      if (type(1:1).eq.'P') then
        !
        ! CALL TITOUT(PLOT)
        !
        call bytoch(title%project,aproject,8)
        call bytoch(title%line,aline,12)
        call decode_backend(narrow)
        write(ch,202,iostat=ier) aproject, aline,   &
          nint(r_restf)/1000., r_nrec, narrow(1), narrow(2)
        call outlin(ch,80)
        !
        ! prepare for second line of plot
        !
        mnum = title%num
        mscan = title%scan
        mctype = ctype
        mcproc = cproc
        mch1 = ch1(1:11)
        lut8 = title%ut
        call sexag(ch2,lut8,24)
        mch2 = ch2(2:6)
        mhoura = title%houra
      else
        !
        ! CALL TITOUT(BLOT)
        !
        lut8 = title%ut
        call sexag(ch2,lut8,24)
        write(ch,203,iostat=ier)   &
          !     &         MNUM, LNUM, MSCAN, LSCAN, MCTYPE, MCPROC, CTYPE, CPROC,
          mnum, mscan, mctype, mcproc, title%num, title%scan, ctype, cproc,   &
          mch1, mch2, ch2(2:6)
        call outlin(ch,80)
        mscan = 0
        nscan = 0
      endif
    else
      !
      ! CALL TITOUT(NORMAL)
      !
      lut8 = title%ut
      call sexag(ch2,lut8,24)
      call bytoch(title%project,aproject,8)
      call bytoch(title%sourc,asourc,12)
      call bytoch(title%line,aline,12)
      call bytoch(title%teles,ateles,12)
      lch = lenc(ateles)
      write(ch,201,iostat=ier) mod(title%num,100000)   &
        ,mod(title%scan,10000),aproject,asourc,ctype,cproc   &
        ,aline(1:8),ateles(1:lch), ch1(1:11),ch2(2:6),title%houra*12./pi
      call outlin(ch,80)
    endif
  endif
  !
301 format(i4,'_',i4,1x,a,1x,a,a1,1x,a4,1x,a,1x,a,1x,a11,   &
    1x,f5.1,'_',f5.1)
  !201   FORMAT(I4,1X,I4,1X,A4,1X,3A4,1X,A1,1X,A4,
  !     &1X,2A4,1X,3A4,1X,A11,1X,A5,F5.1)
201 format(i5,1x,i4,1x,a,1x,a,1x,a1,1x,a,   &
    1x,a,1x,a,1x,a,1x,a,f5.1)
202 format(a,1x,a,1x,f7.3,'GHz B',i1,1x,a,1x,a)
203 format('(',i5,1x,i4,1x,a1,1x,a4,')-(',i5,1x,i4,1x,a1,1x,a4,')',   &
    1x,a11,1x,a5,'-',a5)
  !210   FORMAT(I4,1X,I4,1X,A4,1X,3A4,1X,A1,1X,A4,1X,3A4,1X,A11,
  !     &1X,'UT: ',A5,' HA:',F5.1)
210 format(i5,1x,i4,1x,a,1x,a,1x,a1,1x,a4,1x,a,1x,a11,   &
    1x,'UT: ',a5,' HA:',f5.1)
100 format(i5,';',i2,1x,a,1x,a1,1x,a4,1x,a,1x,a,1x,a,1x,a)
102 format('Quality: ',a10,2x,a10,'  Rec.: ',i1,'  Ndump: ',i3,   &
    '  Az:',f7.2,2x,'  El: ',f6.2)
103 format('(',f6.1,')')
104 format('l: ',1pg13.6,2x,'b: ',1pg13.6,2x)
105 format(a1,i2.2,a,'  N: ',i3,'  Line: ',a12,'  I0: ',1pg11.4)
107 format(a1,i2.2,'  W: ',f4.0,'  Cen.: ',f6.2,' Unit: ',i1)
108 format(a1,i2.2,'  W: ',i3,'  Cen.: ',f7.2,'  Unit: ',i1,   &
    '  Quarter: Q',i1,'  Pol: ',a3,'  Input:',i1)
109 format (a1,i2.2,' Unit: ',i2,' Pol: ',a3, ' ',a3)
113 format(a10,a,12(a,f7.2))
end subroutine titout
!
function procname (nproc)
  character(len=4) :: procname      !
  integer :: nproc                  !
  ! Local
  integer :: mproc,iproc
  parameter (mproc=30)
  character(len=4) :: charproc(0:mproc)
  data charproc /'?...',5*'----','SKYD',4*'----',   &
    'CORR','AUTO','GAIN','DELA','FOCU','POIN','CALI','IFPB',   &
    'ONOF','HOLO','FIVE','PSEU','FLUX','STAB','CWVR',   &
    'VLBI','VLBG','OTF','BAND','MONI'/
  !
  iproc = max(0,nproc)
  procname = charproc(iproc)
end function procname
!
subroutine vartitout(short,scan,nv,type,addr,form,end,error)
  use gkernel_interfaces
  use gildas_def
  use gbl_format
  logical :: short                          !
  integer :: scan                           !
  integer :: nv                             !
  integer(kind=address_length) :: type(nv)  !
  integer(kind=address_length) :: addr(nv)  !
  character(len=12) :: form(nv)             !
  logical :: end                            !
  logical :: error                          !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=256) :: chain,old_chain
  character(len=80) :: ch
  character(len=10) :: ch1
  integer :: lch, lchain, iv, lcv, ll, l1, l2
  integer(kind=address_length) :: ipv, ipc, add
  logical :: err
  integer :: first_scan, last_scan
  data first_scan/-1/
  save first_scan, last_scan, old_chain
  integer :: mg
  parameter (mg=1000)
  integer :: ng, sg(2,mg)
  real :: x
  common/scan_group/ng, sg
  save /scan_group/
  !-----------------------------------------------------------------------
  lchain = 1
  chain = ' '
  do iv = 1, nv
    ipv = gag_pointer(addr(iv),memory)
    err = .false.
    if (type(iv).gt.0) then
      lcv = type(iv)
      l1 = index(form(iv),'A')
      if (l1.gt.0) then
        l2 = index(form(iv),')')
        lch = l2-l1-1
        if (lch.gt.0) then
          read(form(iv)(l1+1:l2-1),'(i3)') ll
          lcv = min(ll,lcv)
        endif
      endif
      lch = lcv
      add = locstr(ch)
      ipc = bytpnt(add,membyt)
      call bytoby(memory(ipv),membyt(ipc),lcv)
    elseif (type(iv).eq.fmt_r4) then
      call r4form(memory(ipv),ch,lch,form(iv),err)
    elseif (type(iv).eq.fmt_r8) then
      call r8form(memory(ipv),ch,lch,form(iv),err)
    elseif (type(iv).eq.fmt_i4) then
      call i4form(memory(ipv),ch,lch,form(iv),err)
    elseif (type(iv).eq.fmt_i8) then
      call i8form(memory(ipv),ch,lch,form(iv),err)
    elseif (type(iv).eq.fmt_l) then
      call lform(memory(ipv),ch,lch,form(iv),err)
    else
      call message(8,3,'VARTITOUT','Not implemented for this type')
      error = .true.
      return
    endif
    if (.not.err .and. lchain+lch+1.le.256) then
      chain(lchain:) = ' '//ch(1:lch)
      lchain = lchain+lch+1
    endif
  enddo
  ! on ecrit
  if (short) then
    if (first_scan.gt.0   &
      .and. (end .or. chain.ne.old_chain   &
      .or. scan.lt.last_scan)) then
      write(ch1,'(i4,a,i4,1x)') first_scan, '_', last_scan
      call outlin(ch1//old_chain,lchain+10)
      ng = ng + 1
      if (ng.gt.mg) then
        call message(8,4,'TITOUT','Too many lines')
        error = .true.
        return
      endif
      sg(1,ng) = mod(first_scan,10000)
      sg(2,ng) = mod(last_scan,10000)
      first_scan = scan
    endif
    old_chain = chain
    last_scan = scan
    if (first_scan.le.0) first_scan = scan
    if (end) then
      first_scan = -1
      last_scan = -1
    endif
  else
    call outlin(chain,lchain)
  endif
  return
end subroutine vartitout
!
subroutine i4form(iv,ch,lch,form,error)
  use gkernel_interfaces
  integer :: iv                     !
  character(len=*) :: ch            !
  integer :: lch                    !
  character(len=12) :: form         !
  logical :: error                  !
  ! Local
  real*4 :: r4v
  real*8 :: r8v
  logical :: lv
  integer*8 :: i8v
  integer :: ier
  !-----------------------------------------------------------------------
  !
  if (form.eq.'*') then
    write(ch,'(i8)',iostat=ier) iv
  else
    write(ch,form,iostat=ier) iv
  endif
  goto 10
  !
entry i8form(i8v,ch,lch,form,error)
  if (form.eq.'*') then
    write(ch,'(i8)',iostat=ier) i8v
  else
    write(ch,form,iostat=ier) i8v
  endif
  goto 10
  !
entry lform(lv,ch,lch,form,error)
  if (form.eq.'*') then
    write(ch,'(l1)',iostat=ier) lv
  else
    write(ch,form,iostat=ier) lv
  endif
  goto 10
  !
entry r4form(r4v,ch,lch,form,error)
  if (form.eq.'*') then
    write(ch,'(1pg12.5)',iostat=ier) r4v
  else
    write(ch,form,iostat=ier) r4v
  endif
  goto 10
  !
entry r8form(r8v,ch,lch,form,error)
  if (form.eq.'*') then
    write(ch,'(1pg12.5)',iostat=ier) r8v
  else
    write(ch,form,iostat=ier) r8v
  endif
  !
10 if (ier.ne.0) then
    call message(6,3,'CFORM','Invalid format '//form)
    error  = .true.
  else
    lch = lenc(ch)
  endif
end subroutine i4form
!
subroutine decode_backend(narrow)
  use gkernel_interfaces
  use classic_api  
  character(len=36) :: narrow(2)    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  character(len=36) :: ch
  character(len=3) :: tmp
  character(len=1) :: pol(6)
  integer :: i, k, n, q(2), j(2), s(8,2)
  integer :: quarter
  !
  j(1) = 0
  j(2) = 0
  k = 3
  do i=1,r_lband
    if (r_iunit(i).lt.9) then
       n = r_bb(i)
       q(n) = quarter(r_flo2(i),r_flo2bis(i))
       j(n) = j(n)+1
       s(j(n),n) = r_lnch(i)*abs(r_lfres(i))
       if (r_lpolentry(1,i).eq.1) then
         pol(n) = 'H'
       else
         pol(n) = 'V'
       endif
    endif
  enddo
  !
  do n=1,2
    write(ch,'(A,I1,A)') 'Q',q(n),'('
    do i=1,j(n)
      if (s(i,n).lt.100) then
        write(tmp,'(I2)') s(i,n)
      else
        write(tmp,'(I3)') s(i,n)
      endif
      ch = ch(1:lenc(ch))//tmp(1:lenc(tmp))
      if (i.ne.j(n)) ch = ch(1:lenc(ch))//','
    enddo
    narrow(n) = ch(1:lenc(ch))//')'//pol(n)
  enddo
!
end subroutine decode_backend
