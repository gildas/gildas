subroutine solve_cal_ant(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  !	Computes a calibration curve, based on the phase calibrators,
  !	assumed to be in the current index.
  !	Amplitudes should be also checked.
  ! Antenna based.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: iy, isb, iba, i, nd, n_br, k_br(10), nkn, j
  integer :: m_cal, nd_work, iarg
  integer :: mvx, nkey, ipol, old_pen, new_pen,nysave, nch
  real :: t_br(10), tkn(40), faz
  parameter (mvx=2)
  character(len=12) :: kw,vx(mvx),argum
  logical :: plot, reset, poly, weight, vm_requested, saved
  logical :: solve_amp, solve_phase
  complex :: z(mnbas), zant(mnant)
  real :: w(mnbas), want(mnant)
  !
  integer(kind=address_length) :: ipx,ipy,ipw
  integer(kind=address_length) :: data_cal, x_cal, w_cal, y_cal, iwork1, iwork0
  !
  data vx /'AMPLITUDE','PHASE'/
  !------------------------------------------------------------------------
  ! Code:
  old_pen = -1
  vm_requested = .false.
  saved = .false.
  if (i_base(1).gt.mnbas) then
    call message(8,4,'SOLVE_PASS','Triangle mode not supported')
    error = .true.
  endif
  solve_amp = .false.
  solve_phase = .false.
  nd = min(2,sic_narg(0))
  do i=1,nd
    call sic_ke(line,0,i,argum,nch,.false.,error)
    if (error) goto 999
    call sic_ambigs('SOLVE_CAL',argum,kw,nkey,vx,mvx,error)
    if (error) goto 999
    if (nkey.eq.1) then
      solve_amp = .true.
    elseif (nkey.eq.2) then
      solve_phase = .true.
    endif
  enddo
  !
  poly = sic_present(9,0)
  ipol = 0
  if (poly) then
    call sic_i4(line,9,1,ipol,.false.,error)
    if (error) goto 999
    ipol = max(min(ipol+1,m_pol),1)    ! degree+1
  endif
  reset = sic_present(4,0)
  if (reset) then
    call reset_time
  endif
  !
  n_br = 0
  if (sic_present(8,0)) then
    i = 0
    do while (i.lt.sic_narg(8))
      n_br = n_br+1
      call sic_i4(line,8,i+1,k_br(n_br),.true.,error)
      if (error) goto 999
      k_br(n_br) = min(max(k_br(n_br),0),3)
      call sic_r4(line,8,i+2,t_br(n_br),.true.,error)
      if (error) goto 999
      i = i + 2
    enddo
  endif
  !
  ! reset some defaults for plotting
  call check_index(error)
  if (error) goto 999
  call get_first(.false.,error)
  if (error) goto 999
  ! Check for less than 3 antennas:
  if (r_nant.le.2) then
    call message(6,3,'SOLVE_CAL_ANT',   &
      'Antenna mode cannot be used for 2 antennas only')
  endif
  ! Time in X
  if (n_x.ne.1) then
    n_x = 1
    change_display = .true.
  endif
  if (i_x(1) .ne. xy_time) then
    i_x(1) = xy_time
    sm_x1(1) = '*'
    sm_x2(1) = '*'
    do_bin = .false.
    change_display = .true.
  endif
  !
  ! Y variables: amplitude and/or phase
  nysave = n_y
  n_y = 0
  if (solve_amp) then
    n_y = n_y + 1
    if (i_y(n_y).ne. xy_ampli) then
      i_y(n_y) = xy_ampli
      sm_y1(n_y) = 'F'
      gu1_y(n_y) = 0.
      sm_y2(n_y) = '*'
      change_display = .true.
    endif
  endif
  if (solve_phase) then
    n_y = n_y + 1
    if (i_y(n_y).ne. xy_phase) then
      i_y(n_y) = xy_phase
      sm_y1(n_y) = '*'
      sm_y2(n_y) = '*'
      continuous = .true.
      change_display = .true.
    endif
  endif
  if (nysave.ne.n_y) then
    change_display = .true.
  endif
  ! 1 subband, 1 band at a time:
  if (n_subb.gt.1 .or. n_band.gt.1) then
    call message(4,2,'SOLVE_CAL',   &
      'Only ONE band and ONE group of subbands at a time')
    goto 999
  endif
  !
  isb = i_band(1)
  if (isb.gt.3) then
    call message(4,2,'SOLVE_CAL',   &
      'Band should Upper, Lower, or DSB')
    goto 999
  endif
  ! all available baselines:
  if (n_base.ne.r_nbas) then
    n_base = r_nbas
    change_display = .true.
  endif
  do i=1, n_base
    if (i_base(i).ne.i) then
      i_base(i) = i
      change_display = .true.
    endif
  enddo
  call set_display(error)
  if (error) return
  !
  save_scale = do_scale
  save_flux = do_flux
  save_amplitude = do_amplitude
  save_phase = do_phase
  saved = .true.
  if (solve_amp .and. (do_amplitude .or. .not.do_scale   &
    .or. do_flux)) then
    do_scale = .true.
    do_amplitude = .false.
    do_flux = .false.
    change_display = .true.
  endif
  if (solve_phase .and. do_phase) then
    do_phase = .false.
    change_display = .true.
  endif
  call show_general('AMPLITUDE',.false.,line,error)
  call show_general('PHASE',.false.,line,error)
  if (change_display) then
    ! plot all scans in index, sorted, plot buffer re-initialized.
    call read_data('ALL',.true.,.true., error)
    plotted = .false.
    if (error) goto 999
    change_display = .false.
    if (n_base.ne.r_nbas) then
      call message(8,3,'SOLVE_CAL',   &
        'Number of antennas has changed')
      goto 999
    endif
  else
    plotted = .true.
  endif
  plot = sic_present(1,0)
  weight = sic_present(2,0)
  if (plot) then
    clear = .true.
    if (.not. plotted) call sub_plot('A',.false.,.false.,0,error)
    if (error) goto 999
    plotted = .true.
    if (sic_narg(1).ge.1) then
      call sic_i4(line,1,1,iarg,.false.,error)
      if (iarg.lt.0.or.iarg.gt.15) then
        call message(6,3,'SOLVE_CAL_ANT','Invalid pencil number')
        error = .true.
        return
      endif  
      new_pen = iarg
    else
      new_pen = 1
    endif
    old_pen = gr_spen (new_pen)
    call gr_segm('FIT',error)
  endif
  !
  ! additional knots
  nkn = 0
  if (n_br.gt.0) then
    do i=1, n_br
      do j=1,4-k_br(i)
        nkn = nkn + 1
        tkn(nkn) = t_br(i)
      enddo
    enddo
  endif
  ! Get storage:
  nd = n_data(1)
  nd_work = nd
  if (nd.eq.0) goto 999
  if (nd_work.lt.4) nd_work=4 ! To be able to fit splines
  m_cal = nd_work*(12*n_base+3)
  error = sic_getvm(m_cal,data_cal).ne.1
  if (error) goto 999
  vm_requested = .true.
  x_cal = gag_pointer(data_cal,memory)
  w_cal = x_cal+nd_work*2
  y_cal = w_cal+nd_work*n_base*2
  iwork1 = y_cal+nd_work*n_base*2
  iwork0 = iwork1+nd_work*n_base*8
  !
  ! Do amplitudes first
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  if (solve_amp) then
    call sub_solve_cal_ant(xy_ampli,nd,nd_work,n_base,r_nant,   &
      memory(x_cal),memory(y_cal),   &
      memory(w_cal), memory(iwork0), memory(iwork1),   &
      m_data,m_boxes,memory(ipx),memory(ipy),memory(ipw),   &
      nkn,tkn,weight,poly,ipol,plot,error)
    if (error) goto 999
  endif
  if (solve_phase) then
    call sub_solve_cal_ant(xy_phase,nd,nd_work,n_base,r_nant,   &
      memory(x_cal),memory(y_cal),   &
      memory(w_cal), memory(iwork0), memory(iwork1),   &
      m_data,m_boxes,memory(ipx),memory(ipy),memory(ipw),   &
      nkn,tkn,weight,poly,ipol,plot,error)
    if (error) goto 999
    !
    ! save the side band averages in icsba, for other receiver
    do iba = -r_nant, -1
      if (isb.gt.2) then
        icsba(isb,iba,r_nrec) = 1.0
      elseif (do_pass_memory) then
        icsba(isb,iba,r_nrec) = sba(isb,iba,r_nrec)
      elseif (r_abpc.ne.0) then
        icsba(isb,iba,r_nrec) = r_abpcsba(1,isb,-iba)
      elseif (r_bpc.ne.0) then
        do i=1,r_nbas
          z(i) = r_bpcsba(1,isb,i)
          w(i) = 1.
        enddo
        call antgain(z,w,zant,want)
        icsba(isb,iba,r_nrec) = zant(-iba)
      else
        call message(8,2,'SOLVE_CAL_ANT',   &
          'No RF Passband available')
      endif
    enddo
  endif
  !
998 continue
  if (old_pen.ne.-1)  then
    call gr_segm_close(error)
    i = gr_spen(old_pen)
  endif
  if (saved) then
    do_scale = save_scale
    do_flux = save_flux
    do_amplitude =  save_amplitude
    do_phase =  save_phase
  endif
  if (vm_requested) then
    call free_vm(m_cal,data_cal)
  endif
  return
  !
999 error = .true.
  goto 998
end subroutine solve_cal_ant
!
subroutine sub_solve_cal_ant(ly,nd,nd_work,nbas,nant,xxx,yyy,www,   &
    ind,wk1,   &
    md,mb,x_data,y_data,w_data,nkn,tkn,weight,poly,ipol,plot,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  integer :: ly                     !
  integer :: nd                     !
  integer :: nd_work                    !
  integer :: nbas                   !
  integer :: nant                   !
  real*8 :: xxx(nd_work)                 !
  real*8 :: yyy(nd_work,nbas)            !
  real*8 :: www(nd_work,nbas)            !
  integer :: ind(nd_work)                !
  real*8 :: wk1(nd_work,nbas,4)          !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nkn                    !
  real :: tkn(nkn)                  !
  logical :: weight                 !
  logical :: poly                   !
  integer :: ipol                   !
  logical :: plot                   !
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: ib, iy, iba, i, ikn, nt, ifail, nk, isb, ia, nn, ipola
  integer :: ja, l, l1, l2, l3, ir
  real*8 :: cs((m_spl+4)*mnant), ks(m_spl+8), sss
  real*8 :: xx(10*m_spl), yy(10*m_spl), yya(10*m_spl,mnant), x1, xx1
  real*8 :: ss(mnbas), w0, xold, xnew, x2, x3, x4, step, y2, y3
  !! Original code was using static arrays
  !!      REAL*8  wss(mnbas)
  !!      REAL*8  WK2((M_SPL+8)**2*MNANT*MNANT)
  !!      REAL*8  WK3((M_SPL+8)*MNANT),
  !! New code uses virtual memory
  integer :: nmad, size           ! NMAD = (IK+8)*NANT or IPOL*NANT
  integer(kind=address_length) :: ipwk2   ! Size 2 * NMAD**2
  integer(kind=address_length) :: ipwk3   ! Size 2 * NMAD
  integer(kind=address_length) :: ipwss   ! Size 2 * MNBAS
  integer(kind=address_length) :: addr
  integer :: ier
  integer(kind=index_length) :: dim(4)
  !
  real :: turn, rmss
  character(len=255) :: chain
  character(len=20) :: cunit
  ! Real variable for RMS
  real :: rms_err(mbox),pol_coeff(mnant,m_pol)
  save rms_err,pol_coeff
  !------------------------------------------------------------------------
  ! Code:
  if (do_polar.eq.0) then
    ipola = 1
  elseif (do_polar.gt.0.and.do_polar.le.3) then
    ipola = do_polar
  else
    call message(4,3,'SOLVE_CAL','Do not know this polarisation')
    error = .true.
    return
  endif
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      isb = i_band(k_band(ib))
      iba = i_base(k_base(ib))
      if (n_data(ib).eq.0) then
        call message(4,3,'SOLVE_CAL','No data in that box')
        error = .true.
        return
      endif
      w0 = 0
      !
      ! Load arrays for calibration
      do i=1, nd
        xxx(i) = x_data(i,ib)
        ! fit logarithm of amplitudes for antenna-based amplitudes
        !
        ! attention a w0=0 si le premier point a un poids nul !!
        if (ly.eq.1) then
          yyy(i,iba) = log(dble(y_data(i,ib)))
        elseif (ly.eq.2) then
          yyy(i,iba) = y_data(i,ib)
          if (degrees) then
            yyy(i,iba) = pi/180d0*yyy(i,iba)
          endif
        endif
        if (w0.eq.0) w0 = w_data(i,ib)
        ! attention ce test a l'air a l'envers !!!
        if (weight .or. w_data(i,ib).le.0) then
          www(i,iba) = w_data(i,ib)
        elseif (ly.eq.1) then
          www(i,iba) = w0*y_data(i,ib)**2
        else
          www(i,iba) = w0
        endif
      enddo
    endif
  enddo
  !
  if (nd.eq.3) then
    x2 = xxx(2)-xxx(1)
    x3 = xxx(3)-xxx(1)
    xxx(4) = (xxx(3)+xxx(2))/2
    x4 = xxx(4)-xxx(1)
    do iba=1,nbas
      y2 = yyy(2,iba)-yyy(1,iba)
      y3 = yyy(3,iba)-yyy(1,iba)
      yyy(4,iba) = yyy(1,iba) + x4**2*(y2/x2-y3/x3)/(x2-x3)   &
        + x4*(y3*x2/x3-y2*x3/x2)/(x2-x3)
      www(4,iba) = (www(3,iba)+www(2,iba))/2
    enddo
    nd = 4
  elseif (nd.eq.2) then
    x2 = xxx(2)-xxx(1)
    xxx(3) = xxx(2)-x2/3
    xxx(4) = xxx(1)+x2/3
    do iba=1,nbas
      y2 = yyy(2,iba)-yyy(1,iba)
      yyy(3,iba) = yyy(2,iba)-y2/3
      yyy(4,iba) = yyy(1,iba)+y2/3
      www(3,iba) = www(2,iba)
      www(4,iba) = www(1,iba)
    enddo
    nd = 4
  elseif (nd.eq.1) then
    xxx(2) = xxx(1)+0.01
    xxx(3) = xxx(1)+0.02
    xxx(4) = xxx(1)-0.01
    do iba=1,nbas
      yyy(2,iba) = yyy(1,iba)
      yyy(3,iba) = yyy(1,iba)
      yyy(4,iba) = yyy(1,iba)
      www(2,iba) = www(1,iba)
      www(3,iba) = www(1,iba)
      www(4,iba) = www(1,iba)
    enddo
    nd = 4
  endif
  sss = 0
  do i=1, nd
    sss = sss + www(i,iba)**2
  enddo
  ! Sort if required
  if (gr8_random(xxx,nd)) then
    call gr8_trie(xxx,ind,nd,error)
    if (error) goto 999
    do iba=1,n_base
      call gr8_sort(yyy(1,iba),wk1,ind,nd)
      call gr8_sort(www(1,iba),wk1,ind,nd)
    enddo
  endif
  if (r_nant.lt.10) then
    if (ly.eq.1) call check_amp_clos(nd,nant,yyy,www)
    if (ly.eq.2) call check_pha_clos(nd,nant,yyy,www)
  endif
  ! Set knots, and sort them.
  ir = r_nrec
  if (.not. poly) then
    !
    xold = xxx(1)
    ikn = 0
    nk = 0
    do while (xold.lt.xxx(nd))
      if (ikn.eq.nkn) then
        xnew = xxx(nd)
      elseif (nk.lt.m_spl) then
        ikn = ikn + 1
        xnew = tkn(ikn)
        nk = nk+1
        ks(nk+4) = tkn(ikn)
      else
        call message(8,3,'SOLVE_CAL',   &
          'Too many knots for this spline')
        error = .true.
        return
      endif
      nt = (xnew-xold)/t_step
      nt = min(nt, m_spl-nk, nd-4)
      if (nt.gt.0) then
        step = (xnew-xold)/(nt+1)
        if (nk+nt.gt.m_spl) then
          call message(8,3,'SOLVE_CAL',   &
            'Too many knots for this spline')
          error = .true.
          return
        endif
        do i=1, nt
          ks(nk+i+4) = xold + i*step
        enddo
        nk = nk + nt
      endif
      xold = xnew
    enddo
    if (nk.gt.0) then
      call  gr8_trie(ks(5:),ind,nk,error)
      if (error) goto 999
    endif
    !
    ifail = 1
    !      do i=1, nd
    !         write(6,*), i, 'xxx ',xxx(i)
    !         write(6,*) 'yyy ', (yyy(i,j),j=1,nbas)
    !         write(6,*) 'www ', (www(i,j),j=1,nbas)
    !      enddo
    !
    ! Actual size of workspace is
    !     SIZE = 4*NANT*(NK+8)*NANT + (NK+8)*NANT
    ! Real*8 numbers
    nmad = (nk+8)*nant
    size = 2 * (4*nant*nmad + nmad)
    ier = sic_getvm(size, addr)
    if (ier.ne.1) then
      call message(8,3,'SOLVE','Memory allocation failure')
      error = .true.
      goto 999
    endif
    !
    ipwk3 = gag_pointer(addr,memory)
    ipwk2 = ipwk3 + 2 * nmad
    call splinant(ly, nd, nbas, r_iant, r_jant, ref_ant,   &
      nk+8, nant, xxx, yyy, www, ks, wk1,   &
      memory(ipwk2), memory(ipwk3), ss, cs, error)
    call free_vm(size,addr)
    if (error) goto 999
    !
    do ia = 1, nant
      n_knots(ly,isb,ipola,-ia,ir) = nk
      do i=1, nk+8
        k_spline(i,ly,isb,ipola,-ia,ir) = ks(i)    ! in hours
      enddo
      do i=1, nk+4
        c_spline(i,ly,isb,ipola,-ia,ir) = cs(ia+nant*(i-1))
      enddo
      if (do_phase_ext) then
        f_spline(ly,isb,ipola,-ia,ir) = 3
      else
        f_spline(ly,isb,ipola,-ia,ir) = 1
      endif
      f_pol(ly,isb,ipola,-ia,ir) = 0
      ph_fac(1,ipola,-ia,ir) = r_flo1+r_fif1
      ph_fac(2,ipola,-ia,ir) = r_flo1-r_fif1
      ph_fac(3,ipola,-ia,ir) = r_flo1
    enddo
    do iba=1, nbas
      rms_spline_ant(ly,isb,ipola,iba,ir) = ss(iba)
    enddo
  !
  else
    ipol = min(ipol, nd)
    !
    ! Actual size of Work space is
    !     SIZE = NBAS + IPOL*NANT + (IPOL*NANT)**2
    ! Real*8 numbers
    nmad = ipol*nant
    size = 2 * (nbas + nmad + nmad*nmad)
    ier = sic_getvm(size, addr)
    if (ier.ne.1) then
      call message(8,3,'SOLVE','Memory allocation failure')
      error = .true.
      goto 999
    endif
    ipwss = gag_pointer(addr,memory)
    ipwk3 = ipwss + 2 * nbas
    ipwk2 = ipwk3 + 2 * nmad
    call  polyant(ly, nd, nbas, r_iant, r_jant, ref_ant,   &
      ipol, nant, xxx, yyy, www,   &
      wk1, memory(ipwk2), memory(ipwk3), ss,   &
      memory(ipwss), cs, error)
    call free_vm(size,addr)
    if (error) goto 999
    !
    do ia = 1, nant
      n_pol(ly,isb,ipola,-ia,ir) = ipol
      t_pol(1,ly,isb,ipola,-ia,ir) = xxx(1)
      t_pol(2,ly,isb,ipola,-ia,ir) = xxx(nd)
      do i=1, ipol
        c_pol(i,ly,isb,ipola,-ia,ir) = cs(ia+nant*(i-1))
        pol_coeff(ia,i) = c_pol(i,ly,isb,ipola,-ia,ir)
      enddo
      if (do_phase_ext) then
        f_pol(ly,isb,ipola,-ia,ir) = 3
      else
        f_pol(ly,isb,ipola,-ia,ir) = 1
      endif
      f_spline(ly,isb,ipola,-ia,ir) = 0
      ph_fac(1,ipola,-ia,ir) = r_flo1+r_fif1
      ph_fac(2,ipola,-ia,ir) = r_flo1-r_fif1
      ph_fac(3,ipola,-ia,ir) = r_flo1
    enddo
    do iba=1, nbas
      rms_pol_ant(ly,isb,ipola,iba,ir) = ss(iba)
    enddo
    call sic_delvariable ('POL_COEFF',.false.,error)
    error = .false.
    dim(1) = mnant
    dim(2) = ipol
    call sic_def_real ('POL_COEFF',pol_coeff,2,dim,.false.,error)
    error = .false.
  endif
  !
  ! Give rms
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      iba = i_base(k_base(ib))
      l1 = lenc(header_1(ib))
      l2 = lenc(header_2(ib))
      l3 = lenc(header_3(ib))
      if (iy.eq.2 .and. degrees) then
        rmss = ss(iba)*180d0/pi
        cunit = 'deg.'
      elseif (iy.eq.2) then
        rmss = ss(iba)
        cunit = 'rad.'
      elseif (iy.eq.1) then
        rmss = ss(iba)*100.
        cunit = '%'
      endif
      rms_err(ib) = rmss
      write(chain,1000) y_label(ib)(1:3), header_1(ib)(1:l1),   &
        header_2(ib)(1:l2), header_3(ib)(1:l3),   &
        rmss, cunit
1000  format(a,'. ',a,2x,a,1x,a,' rms: ',f7.2,1x,a)
      call message(6,1,'SOLVE_CAL',chain(1:lenc(chain)))
    endif
  enddo
  call sic_delvariable ('RMS_ERR',.false.,error)
  error = .false.
  call sic_def_real ('RMS_ERR',rms_err,1,n_boxes,.false.,error)
  error = .false.
  !
  ! display results
  if (plot) then
    nn = 10.*m_spl
    x1 = 1e30
    x2 = -1e30
    do ib = 1, n_boxes
      iy = i_y(k_y(ib))
      if (iy.eq.ly) then
        x1 = min(x1,dble(gb1_x(ib)))
        x2 = max(x2,dble(gb2_x(ib)))
      endif
    enddo
    step = (x2-x1)/(nn-1)
    if (step.eq.0) step = 0.1
    if (.not. poly) then
      do ia=1, nant
        nk = n_knots(ly,isb,ipola,-ia,ir)
        do i=1, nk+8
          ks(i) = k_spline(i,ly,isb,ipola,-ia,ir)
        enddo
        do i=1, nk+4
          cs(i) = c_spline(i,ly,isb,ipola,-ia,ir)
        enddo
        do i=1, nn
          xx(i) = x1 + (i-1) * step
          ifail = 1
          xx1 = min(xx(i),ks(nk+5))
          xx1  = max(xx1,ks(4))
          call mth_getspl('SOLVE_CAL',   &
            nk+8, ks, cs, xx1, yya(i,ia), error)
          if (error) goto 999
        enddo
      enddo
    else
      do ia=1, nant
        ipol = n_pol(ly,isb,ipola,-ia,ir)
        do i=1, ipol
          cs(i) = c_pol(i,ly,isb,ipola,-ia,ir)
        enddo
        do i=1, nn
          xx(i) = x1 + (i-1) * step
          ifail = 1
          x2 = t_pol(1,ly,isb,ipola,-ia,ir)
          x3 = t_pol(2,ly,isb,ipola,-ia,ir)
          xx1 = min(max(xx(i),x2),x3)
          xx1 = ((xx1-x2)-(x3-xx1))/(x3-x2)
          call mth_getpol('SOLVE_CAL',   &
            ipol, cs, xx1, yya(i,ia), error)
          if (error) goto 999
        enddo
      enddo
    endif
    do ib = 1, n_boxes
      call gr_execl('CHANGE DIRECTORY')
      iy = i_y(k_y(ib))
      if (iy.eq.ly) then
        iba = i_base(k_base(ib))
        ia = r_iant(iba)
        ja = r_jant(iba)
        if (ly.eq.xy_ampli) then
          do i=1, nn
            yy(i) = exp(yya(i,ia)+yya(i,ja))
          enddo
        elseif (ly.eq.xy_phase) then
          do i=1, nn
            yy(i) = yya(i,ja)-yya(i,ia)
            if (degrees) then
              yy(i) = yy(i)*180d0/pi
            endif
          enddo
        endif
        write(chain,'(I4.4)') ib
        call gr_execl('CHANGE DIRECTORY BOX'//chain)
        error = gr_error()
        if (error) goto 999
        if (ly.eq.xy_phase) then
          turn = 2d0*pi
          if (degrees) turn = 360d0
          call phi_plot(gb1_y(ib),gb2_y(ib),   &
            nn,xx,yy, turn)
        else
          call gr8_connect (nn,xx,yy,0.0d0,-1.0d0)
        endif
        call gr_execl('CHANGE DIRECTORY')
      endif
    enddo
  endif
  return
  !
999 error = .true.
  return
end subroutine sub_solve_cal_ant
!
subroutine check_amp_clos(np,nant,y,w)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Check available amplitude closures
  !---------------------------------------------------------------------
  integer :: np                     !
  integer :: nant                   !
  real*8 :: y(np,*)                 !
  real*8 :: w(np,*)                 !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_number.inc'
  ! Local
  real :: c0,c1,c2,c1av,c2av,c1av2,c2av2,c3,c3av,c3av2, ww
  integer :: nq, iq, i, i12, i13, i23, i14, i24, i34, k
  character(len=132) :: chain
  !-----------------------------------------------------------------------
  if (nant.lt.4) return
  nq = nant*(nant-1)*(nant-2)*(nant-3)/24
  do iq=1, nq
    i12 = basquad(1,iq)
    i13 = basquad(2,iq)
    i23 = basquad(3,iq)
    i14 = basquad(4,iq)
    i24 = basquad(5,iq)
    i34 = basquad(6,iq)
    c1av= 0
    c2av= 0
    c3av= 0
    c1av2= 0
    c2av2= 0
    c3av2= 0
    k = 0
    do i=1, np
      ww = w(i,i12)*w(i,i13)*w(i,i23)*w(i,i14)*w(i,i24)*w(i,i34)
      if (ww.gt.0) then
        c0 = y(i,i12)+y(i,i34)
        c1 = y(i,i13)+y(i,i24)-c0
        c2 = y(i,i14)+y(i,i23)-c0
        c3 = c1-c2
        c1av  = c1av+c1
        c1av2 = c1av+c1**2
        c2av  = c2av+c2
        c2av2 = c2av+c2**2
        c3av  = c3av+c3
        c3av2 = c3av+c3**2
        k = k+1
      endif
    enddo
    if (k.ne.0) then
      c1av = c1av/k
      c2av = c2av/k
      c3av = c3av/k
      c1av2 = sqrt (abs(c1av2/k-c1av**2))
      c2av2 = sqrt (abs(c2av2/k-c2av**2))
      c3av2 = sqrt (abs(c3av2/k-c3av**2))
    endif
    write(chain,1000)   &
      antquad(1,iq),antquad(3,iq),antquad(2,iq),antquad(4,iq),   &
      exp(c1av)*100., c1av2*100.
    call message(6,1,'CHECK_AMP_CLOS',chain(1:lenc(chain)))
    write(chain,1000)   &
      antquad(1,iq),antquad(4,iq),antquad(2,iq),antquad(3,iq),   &
      exp(c2av)*100., c2av2*100.
    call message(6,1,'CHECK_AMP_CLOS',chain(1:lenc(chain)))
    write(chain,1000)   &
      antquad(1,iq),antquad(2,iq),antquad(4,iq),antquad(3,iq),   &
      exp(c3av)*100., c3av2*100.
    call message(6,1,'CHECK_AMP_CLOS',chain(1:lenc(chain)))
  enddo
  return
1000 format('Amp.Cl.(',4i1,') av.= ',f7.2,' rms= ',f7.2,' %')
end subroutine check_amp_clos
!
subroutine check_pha_clos(np,nant,y,w)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Check available amplitude closures,
  !---------------------------------------------------------------------
  integer :: np                     !
  integer :: nant                   !
  real*8 :: y(np,*)                 !
  real*8 :: w(np,*)                 !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'clic_display.inc'
  ! Local
  real :: c1,c1av,c1av2, ww
  integer :: nt, it, i, i12, i13, i23, k
  character(len=132) :: chain
  !-----------------------------------------------------------------------
  if (nant.lt.3) return
  nt = nant*(nant-1)*(nant-2)/6
  do it=1, nt
    i12 = bastri(1,it)
    i13 = bastri(2,it)
    i23 = bastri(3,it)
    c1av= 0
    c1av2= 0
    k = 0
    do i=1, np
      ww = w(i,i12)*w(i,i13)*w(i,i23)
      if (ww.gt.0) then
        c1 = y(i,i12)+y(i,i23)-y(i,i13)
        c1 = mod(c1+11*pi,2*pi)-pi
        c1av  = c1av+c1
        c1av2 = c1av+c1**2
        k = k+1
      endif
    enddo
    if (k.gt.0) then
      c1av = c1av/k
      c1av2 = sqrt (abs(c1av2/k-c1av**2))
    endif
    if (degrees) then
      write(chain,1000) anttri(1,it),anttri(2,it),anttri(3,it),   &
        c1av*180/pi, c1av2*180/pi, 'deg.'
    else
      write(chain,1000) anttri(1,it),anttri(2,it),anttri(3,it),   &
        c1av, c1av2, 'rad.'
    endif
    call message(6,1,'CHECK_PHA_CLOS',chain(1:lenc(chain)))
  enddo
  return
1000 format('Phas.Cl.(',3i1,') av.= ',f7.2,' rms= ',f7.2,1x,a)

end subroutine check_pha_clos
