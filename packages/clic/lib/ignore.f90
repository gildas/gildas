subroutine tagout(line,error)
  use gkernel_interfaces
  use gkernel_types
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! CLASS	Support routine for command
  !	TAG Quality List_of_scans
  ! Argument
  !	LINE	C*(*)	Command line
  !	ERROR	L	Error flag
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  character(len=40) :: mess
  character(len=10) :: arg,qual
  integer :: mlist, narg
  parameter (mlist=15)
  integer(kind=entry_length) :: if
  integer :: j,ic,iv,nqual,save(32),rr
  type(sic_listi4_t) :: list
  type(title_v2_t) :: title_save
  !
  call sic_ke (line,0,1,arg,narg,.true.,error)
  if (error) return
  call get_quality(arg,nqual,qual,error)
  if (error) return
  !
  if (sic_present(0,2)) then
    call check_output_file(error)
    if (error) return
    ic = index(line,' ')+1
    ic = index(line(ic:),' ')+ic
    iv = lenc(line)
    call sic_parse_listi4 ('TAG',line(ic:iv),list,mlist,error)
    if (error) return
  else
    title%qual = nqual
    r_qual = nqual
    return
  endif
  !
  title_save = title
  do j=1,list%nlist
    do 20 rr=list%i1(j),list%i2(j),list%i3(j)
      error = .false.
      do 10 if=1,o%desc%xnext-1
        if (rr.ne.ox_num(if)) goto 10
        call rox(if,error)
        title%qual = nqual
        call mox(if,error)
        goto 20
10    continue
      write(mess,100) rr
      call message(6,2,'TAG',mess)
20  continue
  enddo
  title = title_save
  return
  !
entry ignore(line,error)
  !----------------------------------------------------------------------
  ! CLASS	Support routine for command
  !	IGNORE List_of_scans
  ! Argument
  !	LINE	C*(*)	Command line
  !	ERROR	L	Error flag
  !----------------------------------------------------------------------
  ic = index(line,' ')+1
  iv = lenc(line)
  if (iv.lt.ic) return
  call sic_parse_listi4 ('TAG',line(ic:iv),list,mlist,error)
  if (error) return
  !
  title_save = title  
  do j=1,list%nlist
    do 40 rr=list%i1(j),list%i2(j),list%i3(j)
      error = .false.
      do 30 if=1,i%desc%xnext-1
        if (rr.ne.ix_num(if)) goto 30
        ix_qual(if) = 9
        if (ix_ver(if).gt.0) goto 40
30    continue
40  continue
  enddo
  title = title_save
  return
  !
100 format(1x,'Observation ',i6,' not found')
end subroutine tagout
