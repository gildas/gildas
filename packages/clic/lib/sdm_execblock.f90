subroutine write_sdm_execblock(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Source ExecBlock  table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_ExecBlock
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (ExecBlockKey) :: ebKey
  type (ExecBlockRow) :: ebRow
  real*8 :: releaseDays, lonlat(2), altitude
  integer :: numObservingLog, numSchedulerMode, ia, l
  character, parameter :: sdmTable*12 = 'ExecBlock'
  save ebRow
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ebRow%numAntenna = r_nant
  numObservingLog = 1
  !numSchedulerMode = 1
!!!   call allocExecBlockRow(ebRow, ebRow%numAntenna, numObservingLog,   &
!!!     numSchedulerMode, error)
  call allocExecBlockRow(ebRow, error)
  if (error) return
  do ia=1, r_nant
    ebRow%antennaId(ia) = antenna_Id(ia)
  enddo
  !!!!! TEMPORARY !!!!! MUST HANDLE THE SB SUMMARY
  ebRow%sbSummaryId = sbsummary_Id
  ebRow%startTime = eb_startTime
  ebRow%endTime = eb_endTime
  ebRow%sessionReference = 'Plateau de Bure ObservingSession'

  
  !
  ! This has to be IRAM_PDB for casa.
  !ebRow%telescopeName = r_teles
  ebRow%telescopeName = 'IRAM_PDB'
  if (r_nant.gt.1) then
    ebRow%configName = r_config(1:lenc(r_config))
    call get_config(ebRow%baseRangeMin, ebRow%baseRangeMax,   &
      ebRow%baseRmsMinor,ebRow%baseRmsMajor, ebRow%basePa)
  else
    ebRow%configName = 'Single Dish'
    ebRow%baseRangeMin = 0
    ebRow%baseRangeMax = 0
    ebRow%baseRmsMinor = 0
    ebRow%baseRmsMajor = 0
    ebRow%basePa = 0
  endif
  ebRow%observerName = 'N.N., Astronomer.'
!  ebRow%observingLog(1) = 'nn.log'
  ebRow%observingLog = 'nn.log'
  ebRow%schedulerMode = 'testMe'
  l = lenc(project_uid)
  ebRow%projectId= '<EntityRef '// 'entityId="' //   &
    project_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="ObsProject" '//   &
    'documentVersion="1"/>'//char(0)
  l = lenc(execBlock_uid)
  ebRow%execBlockUID = '<EntityRef entityId="' //   &
    execBlock_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="ExecBlock" '//   &
    'documentVersion="1"/>'//char(0)
  ebRow%sbSummary = '<EntityRef entityId="' //   &
    execBlock_uid(1:l)//   &
    '" partId="X00000000" entityTypeName="SchedBlock" '//   &
    'documentVersion="1"/>'//char(0)
  !
  ! Make this into a subroutine ?
  if (r_teles.eq.'VTX-ALMATI') then
    call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    altitude = 2.13542d0*1000d0
  elseif (r_teles.eq.'AEC-ALMATI') then
    call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    altitude = 2.13542d0*1000.d0
  else
    call sic_sexa ('05:54:28.5',10,lonlat(1),error)
    call sic_sexa ('44:38:02.0',10,lonlat(2),error)
    altitude = 2.560d0*1000.d0
  endif
  !
  ebRow%siteLongitude = lonlat(1)*pi/180d0
  ebRow%siteLatitude = lonlat(2)*pi/180d0
  ebRow%siteAltitude = altitude
  !
  ! OPTIONAL in SS2 ... releaseDays = time_interval(1)/1.d9/84600.+365.*1.5
  ! OPTIONAL in SS2 ... ebRow%releaseDate = int(releaseDays)
  ! OPTIONAL in SS2 ... ebRow%releaseDate = ebRow%releaseDate *84600 *1000000000
!!!   ebRow%flagRow = .false.
  !
  call addExecBlockRow(ebKey, ebRow, error)
  if (error) return
  !
  ExecBlock_Id = ebKey%execBlockId
  deallocate(ebRow%antennaId)
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_execblock
!---------------------------------------------------------------------
subroutine get_sdm_execblock(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Read  the ExecBlock  table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_ExecBlock
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (ExecBlockKey) :: ebKey
  type (ExecBlockRow) :: ebRow
  real*8 :: releaseDays
  integer :: numObservingLog, numSchedulerMode, ia, lenz, l1,   &
    l2, l
  character, parameter :: sdmTable*12 = 'ExecBlock'
  integer, parameter :: maxObservingLog = 10, maxSchedulerMode = 10
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  numObservingLog = maxObservingLog
  !numSchedulerMode = maxSchedulerMode
  call allocExecBlockRow(ebRow, error)
  if (error) return
  ebKey%execBlockId = execBlock_Id
  call getExecBlockRow(ebKey, ebRow, error)
  if (error) return
  r_alti = ebRow%siteAltitude/1000.
  r_config = ebRow%configName
  r_teles = ebRow%telescopeName
  l = lenz(ebRow%execBlockUID)
  l1 = index(ebRow%execBlockUID,'"uid://')
  l2 = index(ebRow%execBlockUID(l1+1:),'"')
  r_execBlock = ebRow%execBlockUID(l1+1:l1+l2-1)
  ExecBlock_uid = r_execBlock
  call unslash(r_execBlock,'/')
  call unslash(r_execBlock,':')
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_execblock
!---------------------------------------------------------------------
subroutine unslash(chain,c)
  use gkernel_interfaces
  character*(*) chain, c
  integer l
  l = index(chain,c)
  do while (l.ne.0)
    chain(l:l) = '_'
    l = index(chain,c)
  enddo
  l = lenc(chain)
end subroutine unslash
!---------------------------------------------------------------------
subroutine  get_config(baseRangeMin, baseRangeMax, baseRmsMinor,   &
    baseRmsMajor, basePa)
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Dummy
  real*8 baseRangeMin, baseRangeMax, baseRmsMinor, baseRmsMajor,   &
    basePa
  ! Local
  real*8 length, lat, sx2, sxy, sy2, s, xx, yy
  parameter (lat=44.63389D0*pi/180d0)
  integer ib, k
  baseRangeMin = 1d10
  baseRangeMax = 0
  sx2 = 0
  sy2 = 0
  sxy = 0
  s = 0
  if (r_nbas.gt.1) then
    do ib = 1, r_nbas
      length = 0
      do k = 1, 3
        length = length + r_bas(k,ib)**2
      enddo
      length = sqrt(length)
      !     horizontal component
      xx = r_bas(1,ib)*sin(lat)-r_bas(3,ib)*cos(lat)
      yy = r_bas(2,ib)
      sx2 = sx2 + xx**2
      sxy = sy2 + yy**2
      sxy = sxy + xx*yy
      s = s + 1
      baseRangeMin = min(baseRangeMin, length)
      baseRangeMax = max(baseRangeMax, length)
    enddo
    sx2 = sx2 / s
    sy2 = sy2 / s
    sxy = sxy / s
  else
    baseRangeMin = 0
    baseRangeMax = 1
  endif
! compute rms values and pa using TBD formula
end subroutine get_config
!---------------------------------------------------------------------
!!! subroutine allocExecBlockRow(row, numAntenna, numObservingLog,   &
!!!     numSchedulerMode, error)
!!!   !---------------------------------------------------------------------
!!!   !     integer, allocatable :: antennaId(:)
!!!   !     character*256, allocatable :: observingLog(:)
!!!   !     character*256, allocatable :: schedulerMode(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_ExecBlock
!!!   type(ExecBlockRow) :: row
!!!   integer :: ier, numAntenna, numObservingLog, numSchedulerMode
!!!   logical :: error
!!!   character, parameter :: sdmTable*12 = 'ExecBlock'
!!!   !
!!!   ! antennaId(numAntenna)
!!!   if (allocated(row%antennaId)) then
!!!     deallocate(row%antennaId, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%antennaId(numAntenna), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! ObservingLog(numObservingLog)
!!!   if (allocated(row%ObservingLog)) then
!!!     deallocate(row%ObservingLog, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%ObservingLog(numObservingLog), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! SchedulerMode(numSchedulerMode)
!!!   if (allocated(row%SchedulerMode)) then
!!!     deallocate(row%SchedulerMode, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%SchedulerMode(numSchedulerMode), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!!! !---------------------------------------------------------------------
