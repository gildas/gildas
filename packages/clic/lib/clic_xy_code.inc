!-----------------------------------------------------------------------
! Definition of the X and Y axes 
!   - XY_* codes
!   - MVX = keywords known by SET X|Y command
!   - CLAB = labels for PLOT 
!-----------------------------------------------------------------------
!
      INTEGER XY_AMPLI
      PARAMETER (XY_AMPLI=1)
      INTEGER XY_PHASE
      PARAMETER (XY_PHASE=2)
      INTEGER XY_REAL
      PARAMETER (XY_REAL =3)
      INTEGER XY_IMAG
      PARAMETER (XY_IMAG =4)
!
      INTEGER XY_U
      PARAMETER (XY_U    =5)
      INTEGER XY_V
      PARAMETER (XY_V    =6)
      INTEGER XY_RADIU
      PARAMETER (XY_RADIU=7)
      INTEGER XY_ANGLE
      PARAMETER (XY_ANGLE=8)
      INTEGER XY_TIME
      PARAMETER (XY_TIME =9)
      INTEGER XY_SCAN
      PARAMETER (XY_SCAN =10)
      INTEGER XY_HOUR
      PARAMETER (XY_HOUR =11)
      INTEGER XY_DELAY
      PARAMETER (XY_DELAY=12)
      INTEGER XY_CABLE
      PARAMETER (XY_CABLE=13)
      INTEGER XY_DECLI
      PARAMETER (XY_DECLI=14)
      INTEGER XY_LO_PH
      PARAMETER (XY_LO_PH=15)
      INTEGER XY_LO_RA
      PARAMETER (XY_LO_RA=16)
      INTEGER XY_CHANN
      PARAMETER (XY_CHANN=17)
      INTEGER XY_VELOC
      PARAMETER (XY_VELOC=18)
      INTEGER XY_I_FRE
      PARAMETER (XY_I_FRE=19)
      INTEGER XY_SKY_F
      PARAMETER (XY_SKY_F=20)
!
      INTEGER XY_T00
      PARAMETER (XY_T00  =21)
      INTEGER XY_T01
      PARAMETER (XY_T01  =22)
      INTEGER XY_T02
      PARAMETER (XY_T02  =23)
      INTEGER XY_T03
      PARAMETER (XY_T03  =24)
      INTEGER XY_T04
      PARAMETER (XY_T04  =25)
      INTEGER XY_T05
      PARAMETER (XY_T05  =26)
      INTEGER XY_T06
      PARAMETER (XY_T06  =27)
      INTEGER XY_T07
      PARAMETER (XY_T07  =28)
      INTEGER XY_T08
      PARAMETER (XY_T08  =29)
      INTEGER XY_T09
      PARAMETER (XY_T09  =30)
      INTEGER XY_T10
      PARAMETER (XY_T10  =31)
      INTEGER XY_T11
      PARAMETER (XY_T11  =32)
      INTEGER XY_T12
      PARAMETER (XY_T12  =33)
      INTEGER XY_T13
      PARAMETER (XY_T13  =34)
      INTEGER XY_T14
      PARAMETER (XY_T14  =35)
      INTEGER XY_T15
      PARAMETER (XY_T15  =36)
      INTEGER XY_T16
      PARAMETER (XY_T16  =37)
      INTEGER XY_T17
      PARAMETER (XY_T17  =38)
      INTEGER XY_T18
      PARAMETER (XY_T18  =39)
      INTEGER XY_T19
      PARAMETER (XY_T19  =40)
!
      INTEGER XY_ATM
      PARAMETER (XY_ATM  =41)
      INTEGER XY_ATM_POW
      PARAMETER (XY_ATM_POW =42)
      INTEGER XY_ATM_REF
      PARAMETER (XY_ATM_REF =43)
      INTEGER XY_ATM_PHA
      PARAMETER (XY_ATM_PHA  =44)
      INTEGER XY_ATM_UNC
      PARAMETER (XY_ATM_UNC  =45)
      INTEGER XY_ATM_COR
      PARAMETER (XY_ATM_COR  =46)
      INTEGER XY_CAL_PHA
      PARAMETER (XY_CAL_PHA  =47)
      INTEGER XY_CAL_AMP
      PARAMETER (XY_CAL_AMP  =48)
      INTEGER XY_RF_PHA
      PARAMETER (XY_RF_PHA  =49)
      INTEGER XY_RF_AMP
      PARAMETER (XY_RF_AMP  =50)
      INTEGER XY_WINDAV
      PARAMETER (XY_WINDAV  =51)
      INTEGER XY_WINDIRAV
      PARAMETER (XY_WINDIRAV =52)
      INTEGER XY_WINTOP
      PARAMETER (XY_WINTOP  =53)
      INTEGER XY_WINDIRTOP
      PARAMETER (XY_WINDIRTOP  =54)
      INTEGER XY_ATM_VAL
      PARAMETER (XY_ATM_VAL  =55)
      INTEGER XY_AZERR
      PARAMETER (XY_AZERR  =56)
      INTEGER XY_ELERR
      PARAMETER (XY_ELERR  =57)
      INTEGER XY_AZPH
      PARAMETER (XY_AZPH  =58)
      INTEGER XY_ELPH
      PARAMETER (XY_ELPH  =59)
      INTEGER XY_PARANG
      PARAMETER (XY_PARANG  =60)
!
! Data header
      INTEGER XY_GAMME
      PARAMETER (XY_GAMME=61)
      INTEGER XY_FLUX
      PARAMETER (XY_FLUX =62)
      INTEGER XY_LO1_F
      PARAMETER (XY_LO1_F=63)
      INTEGER XY_DOPPL
      PARAMETER (XY_DOPPL=64)
      INTEGER XY_AZ_CO
      PARAMETER (XY_AZ_CO=65)
      INTEGER XY_EL_CO
      PARAMETER (XY_EL_CO=66)
      INTEGER XY_FOC_C
      PARAMETER (XY_FOC_C=67)
      INTEGER XY_GAIN
      PARAMETER (XY_GAIN =68)
      INTEGER XY_OPACI
      PARAMETER (XY_OPACI=69)
      INTEGER XY_GROUNDEM
      PARAMETER (XY_GROUNDEM=70)
      INTEGER XY_EMIS
      PARAMETER (XY_EMIS=71)
      INTEGER XY_AIRMASS
      PARAMETER (XY_AIRMASS=72)
      INTEGER XY_TDEWAR1
      PARAMETER (XY_TDEWAR1=73)
      INTEGER XY_TDEWAR2
      PARAMETER (XY_TDEWAR2=74)
      INTEGER XY_TDEWAR3
      PARAMETER (XY_TDEWAR3=75)
      INTEGER XY_LAMBD
      PARAMETER (XY_LAMBD=76)
      INTEGER XY_BETA
      PARAMETER (XY_BETA =77)
      INTEGER XY_FOCUS
      PARAMETER (XY_FOCUS=78)
      INTEGER XY_TOTAL
      PARAMETER (XY_TOTAL=79)
      INTEGER XY_I_ATM
      PARAMETER (XY_I_ATM=80)
      INTEGER XY_J_ATM
      PARAMETER (XY_J_ATM=81)
      INTEGER XY_TREC
      PARAMETER (XY_TREC =82)
      INTEGER XY_TCABIN
      PARAMETER (XY_TCABIN =83)
      INTEGER XY_TCHOP
      PARAMETER (XY_TCHOP =84)
      INTEGER XY_TSYS
      PARAMETER (XY_TSYS =85)
      INTEGER XY_PAMB
      PARAMETER (XY_PAMB =86)
      INTEGER XY_TAMB
      PARAMETER (XY_TAMB =87)
      INTEGER XY_HUMID
      PARAMETER (XY_HUMID=88)
      INTEGER XY_NUMBE
      PARAMETER (XY_NUMBE=89)
      INTEGER XY_QUALI
      PARAMETER (XY_QUALI=90)
      INTEGER XY_AZIMU
      PARAMETER (XY_AZIMU=91)
      INTEGER XY_ELEVA
      PARAMETER (XY_ELEVA=92)
      INTEGER XY_WATER
      PARAMETER (XY_WATER=93)
      INTEGER XY_TCOLD
      PARAMETER (XY_TCOLD =94)
      INTEGER XY_95
      PARAMETER (XY_95   =95)
      INTEGER XY_RECOR
      PARAMETER (XY_RECOR=96)
      INTEGER XY_AUTO
      PARAMETER (XY_AUTO =97)
      INTEGER XY_RMS_A
      PARAMETER (XY_RMS_A=98)
      INTEGER XY_RMS_P
      PARAMETER (XY_RMS_P=99)
!
! WVR
      INTEGER XY_WVRTAMB
      PARAMETER (XY_WVRTAMB=100)
      INTEGER XY_WVRTPEL
      PARAMETER (XY_WVRTPEL=101)
      INTEGER XY_WVRTCAL1
      PARAMETER (XY_WVRTCAL1=102)
      INTEGER XY_WVRTCAL2
      PARAMETER (XY_WVRTCAL2=103)
      INTEGER XY_WVRTCAL3
      PARAMETER (XY_WVRTCAL3=104)
      INTEGER XY_WVRREF1
      PARAMETER (XY_WVRREF1=105)
      INTEGER XY_WVRREF2
      PARAMETER (XY_WVRREF2=106)
      INTEGER XY_WVRREF3
      PARAMETER (XY_WVRREF3=107)
      INTEGER XY_WVRAVER1
      PARAMETER (XY_WVRAVER1=108)
      INTEGER XY_WVRAVER2
      PARAMETER (XY_WVRAVER2=109)
      INTEGER XY_WVRAVER3
      PARAMETER (XY_WVRAVER3=110)
      INTEGER XY_WVRAMB1
      PARAMETER (XY_WVRAMB1=111)
      INTEGER XY_WVRAMB2
      PARAMETER (XY_WVRAMB2=112)
      INTEGER XY_WVRAMB3
      PARAMETER (XY_WVRAMB3=113)
      INTEGER XY_WVRTREC1
      PARAMETER (XY_WVRTREC1=114)
      INTEGER XY_WVRTREC2
      PARAMETER (XY_WVRTREC2=115)
      INTEGER XY_WVRTREC3
      PARAMETER (XY_WVRTREC3=116)
      INTEGER XY_WVRFEFF1
      PARAMETER (XY_WVRFEFF1=117)
      INTEGER XY_WVRFEFF2
      PARAMETER (XY_WVRFEFF2=118)
      INTEGER XY_WVRFEFF3
      PARAMETER (XY_WVRFEFF3=119)
      INTEGER XY_WVRMODE
      PARAMETER (XY_WVRMODE=120)
      INTEGER XY_WVRH2O
      PARAMETER (XY_WVRH2O=121)
      INTEGER XY_WVRPATH
      PARAMETER (XY_WVRPATH=122)
      INTEGER XY_WVRTSYS1
      PARAMETER (XY_WVRTSYS1=123)
      INTEGER XY_WVRTSYS2
      PARAMETER (XY_WVRTSYS2=124)
      INTEGER XY_WVRTSYS3
      PARAMETER (XY_WVRTSYS3=125)
      INTEGER XY_WVRDPATH1
      PARAMETER (XY_WVRDPATH1=126)
      INTEGER XY_WVRDPATH2
      PARAMETER (XY_WVRDPATH2=127)
      INTEGER XY_WVRDPATH3
      PARAMETER (XY_WVRDPATH3=128)
      INTEGER XY_WVRFPATH1
      PARAMETER (XY_WVRFPATH1=129)
      INTEGER XY_WVRFPATH2
      PARAMETER (XY_WVRFPATH2=130)
      INTEGER XY_WVRFPATH3
      PARAMETER (XY_WVRFPATH3=131)
      INTEGER XY_WVRLIQ1
      PARAMETER (XY_WVRLIQ1=132)
      INTEGER XY_WVRLIQ2
      PARAMETER (XY_WVRLIQ2=133)
      INTEGER XY_WVRLIQ3
      PARAMETER (XY_WVRLIQ3=134)
      INTEGER XY_WVRDCLOUD1
      PARAMETER (XY_WVRDCLOUD1=135)
      INTEGER XY_WVRDCLOUD2
      PARAMETER (XY_WVRDCLOUD2=136)
      INTEGER XY_WVRDCLOUD3
      PARAMETER (XY_WVRDCLOUD3=137)
      INTEGER XY_WVRTATM
      PARAMETER (XY_WVRTATM=138)
      INTEGER XY_WVRQUAL
      PARAMETER (XY_WVRQUAL=139)
      INTEGER XY_DH_WVR1
      PARAMETER (XY_DH_WVR1=140)
      INTEGER XY_DH_WVR2
      PARAMETER (XY_DH_WVR2=141)
      INTEGER XY_DH_WVR3
      PARAMETER (XY_DH_WVR3=142)
      INTEGER XY_DH_WVRSTAT
      PARAMETER (XY_DH_WVRSTAT=143)
      INTEGER XY_WVR_PHA_M
      PARAMETER (XY_WVR_PHA_M=144)
      INTEGER XY_WVR_PHA_E
      PARAMETER (XY_WVR_PHA_E=145)
      INTEGER XY_WVR_PHA_C
      PARAMETER (XY_WVR_PHA_C=146)
      INTEGER XY_WVR_TRIPLE
      PARAMETER (XY_WVR_TRIPLE=147)
      INTEGER XY_TATM_S
      PARAMETER (XY_TATM_S=148)
      INTEGER XY_TATM_I
      PARAMETER (XY_TATM_I=149)
      INTEGER XY_WVR_DT1
      PARAMETER (XY_WVR_DT1=150)
      INTEGER XY_WVR_DT2
      PARAMETER (XY_WVR_DT2=151)
      INTEGER XY_WVR_DT3
      PARAMETER (XY_WVR_DT3=152)
      INTEGER XY_WVR_DTRI
      PARAMETER (XY_WVR_DTRI=153)
!
! Misc.
      INTEGER XY_I_FRE1
      PARAMETER (XY_I_FRE1=154)
      INTEGER XY_I_FRE2
      PARAMETER (XY_I_FRE2=155)
      INTEGER XY_I_FRE3
      PARAMETER (XY_I_FRE3=156)
      INTEGER XY_DH_ACTP1
      PARAMETER (XY_DH_ACTP1=157)
      INTEGER XY_DH_ACTP2
      PARAMETER (XY_DH_ACTP2=158)
      INTEGER XY_DH_ACTP3
      PARAMETER (XY_DH_ACTP3=159)
      INTEGER XY_DH_ACTP4
      PARAMETER (XY_DH_ACTP4=160)
      INTEGER XY_DH_ACTP5
      PARAMETER (XY_DH_ACTP5=161)
      INTEGER XY_DH_ACTP6
      PARAMETER (XY_DH_ACTP6=162)
      INTEGER XY_DH_ACTP7
      PARAMETER (XY_DH_ACTP7=163)
      INTEGER XY_DH_ACTP8
      PARAMETER (XY_DH_ACTP8=164)
      INTEGER XY_DH_ANTTP
      PARAMETER (XY_DH_ANTTP=165)
      INTEGER XY_ATTENUATION
      PARAMETER (XY_ATTENUATION=166)
      INTEGER XY_LILEVL1
      PARAMETER (XY_LILEVL1=167)
      INTEGER XY_LILEVL2
      PARAMETER (XY_LILEVL2=168)
      INTEGER XY_LILEVL3
      PARAMETER (XY_LILEVL3=169)
      INTEGER XY_LILEVL4
      PARAMETER (XY_LILEVL4=170)
      INTEGER XY_LILEVL5
      PARAMETER (XY_LILEVL5=171)
      INTEGER XY_LILEVL6
      PARAMETER (XY_LILEVL6=172)
      INTEGER XY_LILEVL7
      PARAMETER (XY_LILEVL7=173)
      INTEGER XY_LILEVL8
      PARAMETER (XY_LILEVL8=174)
      INTEGER XY_LILEVU1
      PARAMETER (XY_LILEVU1=175)
      INTEGER XY_LILEVU2
      PARAMETER (XY_LILEVU2=176)
      INTEGER XY_LILEVU3
      PARAMETER (XY_LILEVU3=177)
      INTEGER XY_LILEVU4
      PARAMETER (XY_LILEVU4=178)
      INTEGER XY_LILEVU5
      PARAMETER (XY_LILEVU5=179)
      INTEGER XY_LILEVU6
      PARAMETER (XY_LILEVU6=180)
      INTEGER XY_LILEVU7
      PARAMETER (XY_LILEVU7=181)
      INTEGER XY_LILEVU8
      PARAMETER (XY_LILEVU8=182)
      INTEGER XY_SCALE
      PARAMETER (XY_SCALE=183)
      INTEGER XY_REFC1
      PARAMETER (XY_REFC1=184)
      INTEGER XY_REFC2
      PARAMETER (XY_REFC2=185)
      INTEGER XY_REFC3
      PARAMETER (XY_REFC3=186)
      INTEGER XY_REFCOR
      PARAMETER (XY_REFCOR=187)
      INTEGER XY_LILEVL9
      PARAMETER (XY_LILEVL9=188)
      INTEGER XY_LILEVL10
      PARAMETER (XY_LILEVL10=189)
      INTEGER XY_LILEVL11
      PARAMETER (XY_LILEVL11=190)
      INTEGER XY_LILEVL12
      PARAMETER (XY_LILEVL12=191)
      INTEGER XY_LO1_REF
      PARAMETER (XY_LO1_REF=192)
      INTEGER XY_CABLE_ALTERNATE
      PARAMETER (XY_CABLE_ALTERNATE=193)
      INTEGER XY_TIM_01
      PARAMETER (XY_TIM_01  =194)
      INTEGER XY_TIM_02
      PARAMETER (XY_TIM_02  =195)
      INTEGER XY_TIM_03
      PARAMETER (XY_TIM_03  =196)
      INTEGER XY_TIM_04
      PARAMETER (XY_TIM_04  =197)
      INTEGER XY_LASER_LEVEL
      PARAMETER (XY_LASER_LEVEL = 198)
      INTEGER XY_HIQ_ATTEN1
      PARAMETER (XY_HIQ_ATTEN1 = 199)
      INTEGER XY_HIQ_ATTEN2
      PARAMETER (XY_HIQ_ATTEN2 = 200)
      INTEGER XY_WIDEX_TEMP1
      PARAMETER (XY_WIDEX_TEMP1 = 201)
      INTEGER XY_WIDEX_TEMP2
      PARAMETER (XY_WIDEX_TEMP2 = 202)
      INTEGER XY_WIDEX_TEMP3
      PARAMETER (XY_WIDEX_TEMP3 = 203)
      INTEGER XY_WIDEX_TEMP4
      PARAMETER (XY_WIDEX_TEMP4 = 204)
      INTEGER XY_INCLIX
      PARAMETER (XY_INCLIX = 205)
      INTEGER XY_INCLIY
      PARAMETER (XY_INCLIY = 206)
      INTEGER XY_INCLI_TEMP
      PARAMETER (XY_INCLI_TEMP = 207)
      INTEGER XY_T20
      PARAMETER (XY_T20  = 208)
      INTEGER XY_T21
      PARAMETER (XY_T21  = 209)
      INTEGER XY_T22
      PARAMETER (XY_T22  = 210)
      INTEGER XY_T23
      PARAMETER (XY_T23  = 211)
      INTEGER XY_T24
      PARAMETER (XY_T24  = 212)
      INTEGER XY_T25
      PARAMETER (XY_T25  = 213)
      INTEGER XY_T26
      PARAMETER (XY_T26  = 214)
      INTEGER XY_T27
      PARAMETER (XY_T27  = 215)
      INTEGER XY_T28
      PARAMETER (XY_T28  = 216)
      INTEGER XY_T29
      PARAMETER (XY_T29  = 217)
!
! VLBI
      INTEGER XY_VLBI_PHASEFF
      PARAMETER (XY_VLBI_PHASEFF = 218)
      INTEGER XY_VLBI_PHASE_INC
      PARAMETER (XY_VLBI_PHASE_INC = 219)
      INTEGER XY_VLBI_PHASE_TOT
      PARAMETER (XY_VLBI_PHASE_TOT = 220)
      INTEGER XY_VLBI_MASER
      PARAMETER (XY_VLBI_MASER = 221)
!
! SET X|Y keywords
!
      INTEGER MXY
      PARAMETER (MXY=221)
      CHARACTER*12 VXY(MXY)
!
      DATA VXY/'AMPLITUDE','PHASE','REAL','IMAGINARY','U_COORD',        &
     &'V_COORD','RADIUS','ANGLE','TIME','SCAN',                         &
     &'HOUR_ANGLE','DELAY','CABLE_PHASE','DECLINATION','LO_PHASE',      &
     &'LO_RATE',                                                        &
     &'CHANNEL','VELOCITY','I_FREQUENCY','SKY_FREQUENC',                & !17-20
     &'T00','T01','T02','T03','T04','T05','T06','T07','T08','T09',      & !21-30
     &'T10','T11','T12','T13','T14','T15','T16','T17','T18','T19',      & !31-40
     &'ATM_EMISSION','ATM_POWER','ATM_REF','ATM_PHASE','ATM_UNCORRPH',  & !41-45
     &'ATM_CORRPH','CAL_PHASE','CAL_AMPLI','RF_PHASE','RF_AMPLI',       & !46-50
     &'WIND_AVERAGE','WIND_DIR_AV','WIND_TOP','WIND_DIR_TOP',           & !51-54
     &'ATM_VALIDITY','AZ_ERR','EL_ERR',                                 & !55-57
     &'AZ_PH','EL_PH','PARAL_ANGLE',                                    & !58-60
     &'GAMME','FLUX','LO1_FREQ','DOPPLER','AZ_CORR',                    & !61-65
     &'EL_CORR','FOC_CORR','GAIN_RATIO','OPACITY',                      & !66-69
     &'GROUND_EMIS','EMISSION','AIR_MASS',                              & !70-72
     &'TDEWAR1','TDEWAR2','TDEWAR3',                                    & !73-75
     &'LAMBDA','BETA','FOCUS',                                          & !76-78
     &'TOTAL_POWER','I_ATM','J_ATM',                                    & !79-81
     &'TREC','TCABIN','TCHOP',                                          & !82-84
     &'TSYS','PAMB','TAMB','HUMIDITY',                                  & !85-88
     &'NUMBER','QUALITY','AZIMUTH','ELEVATION',                         & !89-92
     &'WATER','TCOLD','95',                                             & !93-95
     &'RECORD','AUTOCORREL',                                            & !96-97
     &'RMS_AMPLITUD','RMS_PHASE',                                       & !98,99
     &'WVRTAMB','WVRTPEL',                                              & !100,101
     &'WVRTCAL1','WVRTCAL2','WVRTCAL3',                                 & !102,104
     &'WVRREF1','WVRREF2','WVRREF3',                                    & !105,107
     &'WVRAVER1','WVRAVER2','WVRAVER3',                                 & !108,110
     &'WVRAMB1','WVRAMB2','WVRAMB3',                                    & !111,113
     &'WVRTREC1','WVRTREC2','WVRTREC3',                                 & !114,116
     &'WVRFEFF1','WVRFEFF2','WVRFEFF3',                                 & !117,119
     &'WVRMODE','WVRH2O','WVRPATH',                                     & !120,122
     &'WVRTSYS1','WVRTSYS2','WVRTSYS3',                                 & !123,125
     &'WVRDPATH1','WVRDPATH2','WVRDPATH3',                              & !126,128
     &'WVRFPATH1','WVRFPATH2','WVRFPATH3',                              & !129,131
     &'WVRLIQ1','WVRLIQ2','WVRLIQ3',                                    & !132,134
     &'WVRDCLOUD1','WVRDCLOUD2','WVRDCLOUD3',                           & !135,137
     &'WVRTATM', 'WVRQUAL',                                             & !138,139
     &'DH_WVR1', 'DH_WVR2', 'DH_WVR3',                                  & !140,142
     &'DH_WVRSTAT','WVRPHA_M','WVRPHA_E','WVRPHA_C',                    & !143,146
     &'WVRTRIPLE','TATM_S','TATM_I',                                    & !147,149
     &'WVRDT1','WVRDT2','WVRDT3','WVRDTRI',                             & !150,153
     &'IF1','IF2','IF3',                                                & !154-156
     &'DH_ACTP1','DH_ACTP2','DH_ACTP3','DH_ACTP4',                      & !157-160
     &'DH_ACTP5','DH_ACTP6','DH_ACTP7','DH_ACTP8',                      & !161-164
     &'DH_ANTTP','ATTENUATION',                                         & !165-166
     &'LILEVL01','LILEVL02','LILEVL03','LILEVL04',                      & !167-170
     &'LILEVL05','LILEVL06','LILEVL07','LILEVL08',                      & !171-174
     &'LILEVU01','LILEVU02','LILEVU03','LILEVU04',                      & !175-178
     &'LILEVU05','LILEVU06','LILEVU07','LILEVU08',                      & !179-182
     &'TOT_SCALE','REFC1','REFC2','REFC3','REFRACTION',                 & !183-187
     &'LILEVL09','LILEVL10','LILEVL11','LILEVL12',                      & !188-191
     &'LO1_REF','CABLE_ALTERN',                                         & !192-293
     &'TIM01','TIM02','TIM03','TIM04',                                  & !194-197
     &'LASER_LEVEL','HIQ_ATTEN1','HIQ_ATTEN2',                          & !198-200
     &'WIDEX_TEMP1','WIDEX_TEMP2','WIDEX_TEMP3','WIDEX_TEMP4',          & !201-204
     &'INCLIX','INCLIY','INCLI_TEMP',                                   & !205-207
     &'T20','T21','T22','T23','T24','T25','T26','T27','T28','T29',      & !208-217
     &'VLBI_PHASEFF', 'VLBI_INC_PHA', 'VLBI_TOT_PHA','VLBI_MASER' /       !218-221
!
! Labels for plots
!
      CHARACTER*20 CLAB(MXY)
!
      DATA CLAB /                                                       &
     &'Amplitude','Phase','Real','Imaginary',                           & !1-4
     &'U','V','sqrt(U\u2+V\u2)','arctg(V/U)',                           & !5-8 By default, no double \\
     &'Time','Scan','Hour Angle',                                       & !9-11
     &'Delay','Cable Ph.','Declination','LO Phase','LO Rate',           & !12-16
     &'Channel','Velocity','Interm. Freq.','Sky Frequency',             & !17-20
     &'T00','T01','T02','T03','T04','T05','T06','T07','T08','T09',      & !21-30
     &'T10','T11','T12','T13','T14','T15','T16','T17','T18','T19',      & !31-40
     &'Atm.Emiss.','Atm.Power','Atm.Ref.','Atm.Phase','Unc. Phase',     & !41-45
     &'Corr.Phase','Cal.Pha.','Cal.Amp.','R.F.Pha.','R.F.Amp.',         & !46-50
     &'Aver.Wind (m/s)','Dir. Av.Wind','Max.Wind (m/s)','Dir. Max.Wind',& ! 51-54
     &'Atm.Corr.Val.','Azim.Err.','Elev.Err.',                          & ! 55-57
     &'Azim.(ph.trk.)','Elev.(ph.trk.)','Parall.Ang.',                  & !58-60
     &'Gamme','Flux','LO1-Freq.','Doppler Corr.','Azim. Corr.',         & !61-65
     &'Elev. Corr.','Focus Corr.','Gain Ratio','Opacity',               & !66-69
     &'Ground Em.','Emission','AirMass',                                & !70-73
     &'T.Dewar(1)','T.Dewar(2)','T.Dewar(3)',                           & !74-76
     &'Lambda','Beta','Focus',                                          & !76-78
     &'Tot.Pow','I_Atm.Em.','J_Atm.Em.',                                & !79-81
     &'Trec','T.Cabin','T.Chopp.',                                      & !82-84
     &'Tsys', 'P.Amb','T.Amb','Humidity',                               & !85-88
     &'Number','Quality','Azimuth(ant.)','Elevation(ant.)',             & !89-92
     &'Water','T.Cold','Unused',                                        & !93-95
     &'Record','Autocorrelation',                                       & !96-97
     &'R.M.S.Amplitude','R.M.S. Phase',                                 & !98-99
     &'WVR_TAMB(K)','WVR_TPEL(K)',                                      & !100,101
     &'WVR_TCAL_1(K/CTS)','WVR_TCAL_2(K/CTS)','WVR_TCAL_3(K/CTS)',      & !102,104
     &'WVR_REF_1(CTS)','WVR_REF_2(CTS)','WVR_REF_3(CTS)',               & !105,107
     &'WVR_AVER_1(CTS)','WVR_AVER_2(CTS)','WVR_AVER_3(CTS)',            & !108,110
     &'WVR_AMB_1(CTS)','WVR_AMB_2(CTS)','WVR_AMB_3(CTS)',               & !111,113
     &'WVR_TREC_1(K)','WVR_TREC_2(K)','WVR_TREC_3(K)',                  & !114,116
     &'WVR_FEFF_1','WVR_FEFF_2','WVR_FEFF_3',                           & !117,119
     &'WVR_MODE','WVR_H2O(MM)','WVR_PATH(MUM)',                         & !120,122
     &'WVR_TSYS_1(K)','WVR_TSYS_2(K)','WVR_TSYS_3(K)',                  & !123,125
     &'WVR_DPATH_1(MUM/K)','WVR_DPATH_2(MUM/K)','WVR_DPATH_3(MUM/K)',   & !126,128
     &'WVR_FPATH_1(MUM/K)','WVR_FPATH_2(MUM/K)','WVR_FPATH_3(MUM/K)',   & !129,131
     &'WVR_LIQ_1(K/K)','WVR_LIQ_2(K/K)','WV_RLIQ_3(K/K)',               & !132,134
     &'WVR_DCLOUD_1(MUM/K)','WVR_DCLOUD_2(MUM/K)',                      & !135,136
     &'WVR_DCLOUD_3(MUM/K)',                                            & !137,
     &'WVR_TATM(K)', 'WVR_QUAL',                                        & !138,139
     &'DH_WVR1 (CTS)','DH_WVR2 (CTS)','DH_WVR3 (CTS)',                  & !140,142
     &'DH_WVRSTAT','WVR_PHA_M','WVR_PHA_E','WVR_PHA_C',                 & !143,146
     &'WVR_TRIPLE','Tatm_s','Tatm_i',                                   & ! 147,149
     &'WVR_DT1', 'WVR_DT2', 'WVR_DT3', 'WVR_DTTRIPLE',                  & ! 150, 153
     &'IF1', 'IF2', 'IF3',                                              & ! 154, 156
     &'DH_ACTP1','DH_ACTP2','DH_ACTP3','DH_ACTP4',                      & ! 157, 160
     &'DH_ACTP5','DH_ACTP6','DH_ACTP7','DH_ACTP8',                      & ! 161, 164
     &'DH_ANTTP','ATTENUATION',                                         & ! 165, 166
     &'LILEVL1','LILEVL2','LILEVL3','LILEVL4',                          & ! 167-170
     &'LILEVL5','LILEVL6','LILEVL7','LILEVL8',                          & ! 171-174
     &'LILEVU1','LILEVU2','LILEVU3','LILEVU4',                          & ! 175-178
     &'LILEVU5','LILEVU6','LILEVU7','LILEVU8',                          & ! 179-182
     &'T. Pow Scale (K/CTS)','Refc1','Refc2','Refc3','Refraction',      & ! 183-187
     &'LILEVL9','LILEVL10','LILEVL11','LILEVL12',                       & ! 188-191
     &'LO1_REF-freq','Cable Pha. Alt',                                  & ! 192-193
     &'UT Sec (s)','NTP second (s)','NTP decimal (ms)',                 & ! 194-196
     &'\gdt(NTP-PPS) (ms)',                                             & ! 197 
     &'Laser Level (dBm)','Hi-Q atten. A (dB)',                         & ! 198-199
     &'Hi-Q atten. B (dB)',                                             & ! 200
     &'Temp. Widex 1 (C)','Temp. Widex 2 (C)',                          & ! 201-202
     &'Temp. Widex 3 (C)','Temp. Widex 4 (C)',                          & ! 203-204
     &'Inclino. X (mrad)','Inclino. Y (mrad)',                          & ! 205-206
     &'Inclino. temp (C)',                                              & ! 207
     &'T20','T21','T22','T23','T24','T25','T26','T27','T28','T29',      & ! 208-217
     &'Phasing eff','Inc. Pha. Cor.' ,'Tot. Pha. Cor.',                 & ! 218-220
     &'Maser GPS diff'/
