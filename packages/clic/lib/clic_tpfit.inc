      integer(kind=address_length) ipx, ipy, ipw
      integer np, nb, mpar, npar, mdim, ndim
      parameter (mpar = 6, mdim=2)
      integer ibx(mdim)
      real*8 pars(mpar), beam, fbeam
      logical variable(mpar)
      common /tpfit/ pars, beam, fbeam, ndim, variable, np, npar, nb,   &
     &  ibx, ipx, ipy, ipw
      save /tpfit/
