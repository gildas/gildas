subroutine write_sdm_CorrelatorData(SpectralResolution, ndata,   &
    dataArray, uid, simAddressing, error)
  use gildas_def
  use classic_api
  use gkernel_interfaces
  use sdm_Enumerations
  !---------------------------------------------------------------------
  !
  !     Write Data for subintegrationsand integrations
  !
  !     SpectralResolution: channel-Average or Full-Resolution
  !     ndata = data length (words)
  !     dataArray = data array
  !     uid = data identification
  !     error = error return
  !
  !     simAddressing: set scaling factors to 1, and replace data by their location
  !     in the binary attachments
  !
  !---------------------------------------------------------------------
  !     Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  !
  !     Dummy
  integer :: ndata, SpectralResolution
  real :: dataArray(ndata)
  character*(*) :: uid
  logical :: error, simAddressing
  !
  !     Local
  integer(kind=data_length) :: h_offset, c _offset, l_offset
  integer, parameter :: maxSpW=2, maxPP=2
  real*4 :: scaleFactor(maxSpW, mrlband)
  integer :: crossPolProducts(maxPP, maxSpW, mrlband),   &
    SdPolProducts(maxPP, maxSpW, mrlband), correlationMode, is,   &
    sdm_writebinaryDataHeader, sdm_writebinaryDataSubset, ireturn   &
    , sdm_closeBinaryDataWriter , md , ia, ja, ib, ic, isb, nsb,   &
    iapc, ki, kd, j , numSpectralWindow(mrlband),   &
    numSpectralPoint(maxSpW ,mrlband) , numCrossPolProduct(maxSpW   &
    ,mrlband), numSdPolProduct(maxSpW ,mrlband), numAPC,   &
    numBin(maxSpW,mrlband), kzl, ir, kh, kc, kcc,  &
    corrDataType, l, ll, kt, ier,   &
    indexBure_CrossCont, indexBure_AutoCont, indexBure_CrossSpec,   &
    indexBure_AutoSpec, idd, numDataDescription, recNUms(2), i ,   &
    kr, klu, kdp(2), ivc, k, ip, kiold
  ! + ACACORR additions
  integer :: imageSpectralWindow(maxSpW,mrlband), netSideband   &
    (maxSpW,mrlband)
  integer :: ProcessorType
  ! - ACACORR additions
  !     2 vis components, 2 apcs, 2 sidebands, mlch channels, mnbas baselines...
  integer*4 numActualTimes, numActualDurations
  real*4 dmax, fact
  integer numBasebandFlags, numCrossData, numAutoData
  integer numZeroLags
  !
  !     make these allocatable...
  parameter (md=mnbas*mlch*2*2*2)
  integer, allocatable ::  crossData(:), basebandFlags(:)
  real*4, allocatable ::  zeroLags(:), autoData(:)
  integer*8, allocatable :: actualTimes(:), actualDurations(:)
  character sdmTable*16
  parameter (sdmTable='CorrelatorData')
  character fileName*256, uidFile*256
  integer :: apc(2), basebandNames(mrlband), timeSampling
  integer :: products(4)
  data products/StokesParameter_XX, StokesParameter_YY,   &
    StokesParameter_XY, StokesParameter_YX/
  integer jp
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  if ( SpectralResolution .eq.   &
    SpectralResolutionType_FULL_RESOLUTION) then
    timeSampling = TimeSampling_INTEGRATION
  else
    timeSampling = TimeSampling_SUBINTEGRATION
  endif
  processorType = ProcessorType_CORRELATOR
  !
  call sdm_getUid(uid)
  uidFile = uid
  call uidToFilename(uidFile)
  l = lenc(file_name)
  ll = lenc(uid)
  fileName = file_name(1:l)//'/ASDMBinary/'//uidFile(1:ll)//char(0)
  if (r_ndatl.ge.2) then
    kcc = c_offset(r_ndump+2)-c_offset(r_ndump+1)
  else
    kcc = 0
  endif
  !
  !     scale factors (auto is made of unscaled real numbers)
  !     for the binaries we need one scale factor for each spw in each baseband
  !     thus one for usb and one for lsb in each corr unit. They are the same.
  numBin = 1
  numCrossPolProduct = 1       !???
  numSdPolProduct = 1          !???
  ! Translate APC modes from Bure:
  if ( SpectralResolution .eq.   &
    SpectralResolutionType_FULL_RESOLUTION) then
    if (r_ndatl.eq.0) then
      numApc = 1
      apc = AtmPhaseCorrection_AP_UNCORRECTED
      else if (r_ndatl.eq.1) then
      numApc = 1
      apc = AtmPhaseCorrection_AP_CORRECTED
      !c do a test ignoring phase correction:
      !$$$         else
      !$$$            numApc = 1
      !$$$            apc = AtmPhaseCorrection_AP_UNCORRECTED
      ! the true thing
      else if (r_ndatl.eq.2) then
      numApc = 2
      apc =(/ AtmPhaseCorrection_AP_UNCORRECTED,   &
        AtmPhaseCorrection_AP_CORRECTED/)
    end if
  else
    numApc = 1
    apc = AtmPhaseCorrection_AP_UNCORRECTED
  endif
  numActualTimes = 0
  numActualDurations = 0
  numZeroLags = 0
  numBasebandFlags = 0
  numCrossData = 0
  numAutoData = 0
  nsb = r_lnsb
  do ivc = 1, v_lband
    basebandNames(ivc) = ivc ! should do better, ... works.
  enddo
  ! correlation specific
  if (r_lmode.eq.1) then
    correlationMode = CorrelationMode_CROSS_ONLY
    do ivc = 1, v_lband
      ic = v_bands(ivc,1)
      numSpectralWindow(ivc) = nsb
      do is = 1, numSpectralWindow(ivc)
        if (is.eq.1) then
          netSideband(is,ivc) = NetSideband_USB
          imageSpectralWindow(is,ivc) = 2
        else
          netSideband(is,ivc) = NetSideband_LSB
          imageSpectralWindow(is,ivc) = 1
        endif
        numCrossPolProduct(is,ivc) = v_ncorr(ivc)
        do k=1, numCrossPolProduct(is,ivc)
          crossPolProducts(k,is,ivc) = products(k) ! check (NGR)
        enddo
        if ( SpectralResolution .eq.   &
          SpectralResolutionType_FULL_RESOLUTION) then
          if (simAddressing) then
            scaleFactor(is,ivc) = 1.0
          else
            scaleFactor(is,ivc) = 2.04 *sqrt( dh_integ*1e6   &
              *r_lfres(ic))
          endif
          numSpectralPoint(is,ivc) = max(1,r_lnch(ic))
          !     ZeroLags is always antenna-based.
          numZeroLags = numZeroLags + numBin(is,ivc)   &
            *numSpectralWindow(ivc)*numSdPolProduct(is,ivc)   &
            * r_nant
        else
          if (simAddressing) then
            scaleFactor(is,ivc) = 1.0
          else
            scaleFactor(is,ivc) = 2.04 *sqrt( dh_integ*1e6   &
              *r_cfwid(ic))
          endif
          numSpectralPoint(is,ivc) = 1
        endif
        numActualDurations = numActualDurations + r_nbas   &
          * numCrossPolProduct(is,ivc) * numBin(is,ivc)
        numActualTimes = numActualTimes + r_nbas   &
          * numCrossPolProduct(is,ivc) * numBin(is,ivc)
        numBasebandFlags = numBasebandFlags + (r_nbas+r_nant)   &
          * numCrossPolProduct(is,ivc)  * numBin(is,ivc)
        numCrossData = numCrossData + 2*numApc*r_nbas   &
          *numCrossPolProduct(is,ivc)*numSpectralPoint(is,ivc)
      enddo
    enddo
  ! autocorrelation specific
  else
    correlationMode = CorrelationMode_AUTO_ONLY
    do ivc = 1, v_lband
      ic = v_bands(ivc,1)
      numSpectralWindow(ivc) = 1
      numSpectralPoint(1,ivc) = 1
      is = 1
      numSdPolProduct(is,ivc) = v_ncorr(ivc)
      do k=1, numSdPolProduct(is,ivc)
        sdPolProducts(k,is,ivc) = products(k)  ! check (NGR)
      enddo
      if ( SpectralResolution .eq.   &
        SpectralResolutionType_FULL_RESOLUTION) then
        !     ZeroLags is always antenna-based.
        numZeroLags = numZeroLags + numBin(1,ivc)   &
          *numSpectralWindow(ivc)*numSdPolProduct(1,ivc)   &
          * r_nant
        numSpectralPoint(1,ivc) = max(1,r_lnch(ic))
      else
        numSpectralPoint(1,ivc) = 1
      endif
      numActualDurations = numActualDurations + numSdPolProduct(1   &
        ,ivc) * r_nant
      numActualTimes = numActualTimes + numSdPolProduct(1,ivc)   &
        *r_nant
      numBasebandFlags = numBasebandFlags + r_nant   &
        * numSdPolProduct(is,ivc) * numBin(1,ivc)
      if (numSdPolProduct(1,ivc).le.2) then
        numAutoData = numAutoData + r_nant*numSdPolProduct(1,ivc)   &
          * numSpectralPoint(1,ivc)
      elseif (numSdPolProduct(1,ivc).eq.3) then
        numAutoData = numAutoData + r_nant * 4   &
          * numSpectralPoint(1,ivc)
      endif
    enddo
  endif

  ! we do not fill in the flags at this time...
  numBasebandFlags = 0
  ! we do not blank at Bure
  numActualTimes = 0
  numActualDurations = 0
  ireturn = sdm_writeBinaryDataHeader(fileName, uid(1:ll)//char(0),   &
    execBlock_Uid, time_interval(1),time_interval(1),   &
    time_interval(2), 1, scan_number,subscan_number   &
    ,r_execblocknum, r_nant, v_lband ,correlationMode,   &
    SpectralResolution , processorType, &
    numApc, apc, basebandNames, maxSpW,   &
    numSpectralWindow, numBin, netSideband, imageSpectralWindow,   &
    numSpectralPoint, scaleFactor, maxPP, numSdPolProduct   &
    ,sdPolProducts, numCrossPolProduct, crossPolProducts   &
    ,numActualTimes, numActualDurations, numZeroLags   &
    ,numBasebandFlags, numCrossData, numAutoData)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Error in sdm_writeBinaryDataHeader', ireturn)
    goto 99
  endif
  
  ier = 0
  if (numActualTimes.gt.0) allocate(ActualTimes(numActualTimes),   &
    stat=ier)
  if (numActualDurations.gt.0)   &
    allocate(ActualDurations(numActualDurations), stat=ier)
  if (numZeroLags.gt.0) allocate(ZeroLags(numZeroLags), stat=ier)
  if (numBasebandFlags.gt.0)   &
    allocate(BasebandFlags(numBasebandFlags), stat=ier)
  if (numAutoData.gt.0) allocate(AutoData(numAutoData), stat=ier)
  if (numCrossData.gt.0) allocate(CrossData(numCrossData), stat=ier)
  if (ier.ne.0) goto 98
  !
  !     integration range:
  recNums  = 1
  if (SpectralResolution .eq. SpectralResolutionType_FULL_RESOLUTION   &
    ) then
    recNums = r_ndump+1
  elseif (SpectralResolution .eq.   &
    SpectralResolutionType_CHANNEL_AVERAGE) then
    recNums = (/ 1, r_ndump /)
  endif
  !
  ! Loop over subintegrations
  kr = 1
  do ir = recNums(1), recNums(2)
    !     get data header:
    kh = 1+ h_offset(ir)
    call decode_header (dataArray(kh))
    call get_time_interval
!!!     if (error) return
!!!     dh_utc = nint(dh_utc/.001d0)*0.001d0
!!!     time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-0.5d0   &
!!!       *dh_integ)*1d9
!!!     ! Patch for TAI-UTC valin in 2006,2007, 2008; replace by correct software!
!!!     time_interval(1) = time_interval(1) + 33.*1d9
!!!     time_interval(2) = dh_integ*1d9
    !     pointer for data:
    kc = 1+ c_offset(ir)
    klu = 1+ l_offset(ir)
    !
    !     check DataType for correlation
    corrDataType =  PrimitiveDataType_INT32_TYPE
    !         ntch = 1
    if (r_lmode.eq.1) then
      dmax = 0.
      do ib = 1, r_nbas
        ia = r_iant(ib)
        ja = r_jant(ib)
        do ivc = 1, v_lband
          ic = v_bands(ivc,1)
          do isb = 1, nsb
            if (SpectralResolution .eq.   &
              SpectralResolutionType_FULL_RESOLUTION) then
              kd = klu-1 + indexBure_CrossSpec(ib, ic, isb, 1)
              do iapc = 1, numApc
                do ip = 1, numCrossPolProduct(isb,ivc)
                  fact = sqrt(dh_atfac(ip,isb,ia)   &
                    *dh_atfac(ip,isb,ja))   &
                    /scaleFactor(isb,ivc)
                  kdp(ip) = klu-1 + indexBure_CrossSpec(ib,   &
                    v_bands(ivc,ip), isb, 1)
                  if (apc(iapc).eq.   &
                    AtmPhaseCorrection_AP_CORRECTED)   &
                    then
                    kdp(ip) = kdp(ip) + kcc
                  endif
                  call sdm_scale (2*r_lnch(ic),   &
                    dataArray(kdp(ip)),fact, dmax)
                enddo
              enddo
            elseif (SpectralResolution .eq.   &
              SpectralResolutionType_CHANNEL_AVERAGE)   &
              then
              do ip = 1, numCrossPolProduct(isb,ivc)
                fact = sqrt(dh_atfac(ip,isb,ia)   &
                  *dh_atfac(ip,isb,ja))   &
                  / scaleFactor(isb,ivc)
                kdp(ip) = kc-1 + indexBure_CrossCont(ib,   &
                     v_bands(ivc,ip), isb)
                call sdm_scale (2, dataArray(kdp(ip)), fact, dmax)
              enddo
            endif
          enddo
        enddo
      enddo
      if (dmax.lt.32768.) then
        corrDataType = PrimitiveDataType_INT16_TYPE
      endif
      !
      ! fill in the data
      !
      !     Here we loop on data description list.
      !     new loop over cross products; for now there can be only 2 at Bure.
      !     each one comes from a different correlator unit
      ki = 1
      kt = 1
      do ib = 1, r_nbas
        do ivc = 1, v_lband
          do isb=1, r_lnsb
            if (SpectralResolution .eq.   &
              SpectralResolutionType_FULL_RESOLUTION) then
              do iapc = 1, numApc
                do ip = 1, numCrossPolProduct(isb,ivc)
                  kdp(ip) = klu-1 + indexBure_CrossSpec(ib,   &
                    v_bands(ivc,ip), isb, 1)
                  if (apc(iapc).eq.   &
                    AtmPhaseCorrection_AP_CORRECTED)   &
                    then
                    kdp(ip) = kdp(ip) + kcc
                  endif
                enddo
                do i = 1, r_lnch(v_bands(ivc,1))
                  do ip = 1, numCrossPolProduct(isb,ivc)
                    !     real and imaginary parts
                    do j=1, 2
                      crossData(ki) =nint(dataArray(kdp(ip   &
                        )))
                      if (simAddressing) then
                        crossData(ki) = ki
                      endif

                      ki = ki+1
                      kdp(ip) = kdp(ip)+1
                    enddo
                  enddo
                enddo
              !     Actual Times and durations : no blanking at Bure
              enddo
              
              if (numActualTimes.gt.0) ActualTimes(kt) = time_interval(1)
              if (numActualDurations.gt.0) ActualDurations(kt) = time_interval(2)
              kt = kt+1
            elseif (SpectralResolution .eq.   &
              SpectralResolutionType_CHANNEL_AVERAGE)   &
              then
              kiold = ki
              do iapc = 1, numApc
                do ip = 1, numCrossPolProduct(isb,ivc)
                  kdp(ip) = kc-1 + indexBure_CrossCont(ib,   &
                    v_bands(ivc,ip), isb)
                  !     real and imaginary parts
                  do j=1, 2
                    crossData(ki) = nint(dataArray(kdp(ip))   &
                      )
                    if (simAddressing) then
                      crossData(ki) = ki
                    endif
                    ki = ki+1
                    kdp(ip) = kdp(ip)+1
                  enddo
                enddo
              enddo
              !     Actual Times and durations : no blanking at Bure
              if (numActualTimes.gt.0) ActualTimes(kt) = time_interval(1)
              if (numActualDurations.gt.0) ActualDurations(kt) = time_interval(2)
              kt = kt+1
            endif
          enddo
        enddo
      enddo
    !     autocorrelation is easy
    !     new loop over cross products; for now there can be only 2 at Bure.
    !     each one comes from a different correlator unit, these are
    !     bb_list(idd,1) and bb_list(ibb,2)
    else
      ki = 1
      numCrossData = 0
      corrDataType =  PrimitiveDataType_INT32_TYPE
      kt = 1
      do ia = 1, r_nant
        do ivc = 1, v_lband
          if (r_isb .eq.1) then
            isb = 1
          else
            isb = 2
          endif
          if (SpectralResolution .eq.   &
            SpectralResolutionType_FULL_RESOLUTION) then
            do ip = 1, numSdPolProduct(isb,ivc)
              kdp(ip) = klu-1 + indexBure_AutoSpec(ia,   &
                v_bands(ivc,ip), 1)
            enddo
            !
            !     We normalize the spectral window to unity and keep the 'channel zero' (no more?)
            do ip = 1, numSdPolProduct(isb,ivc)
              fact = 0
              do i = 1, r_lnch(v_bands(ivc,1))
                fact = fact + dataArray(kdp(ip)+i-1)
              enddo
            enddo
            !     if (fact.gt.0) then
            do i = 1, r_lnch(v_bands(ivc,1))
              do ip = 1, numSdPolProduct(isb,ivc)
                autoData(ki) = dataArray(kdp(ip)+i-1)
                if (simAddressing) then
                  autoData(ki) = ki
                endif
                ki = ki+1
              enddo
            enddo
            !     Actual Times and durations : no blanking at Bure
            if (numActualTimes.gt.0) ActualTimes(kt) = time_interval(1)+time_interval(2)   &
              /2
            if (numActualDurations.gt.0) ActualDurations(kt) = time_interval(2)
            kt = kt+1
          elseif (SpectralResolution .eq.   &
            SpectralResolutionType_CHANNEL_AVERAGE) then
            do ip = 1, numSdPolProduct(isb,ivc)
              kd = kc-1 + indexBure_AutoCont(ia,   &
                v_bands(ivc,ip))
              fact = dataArray(kd)
              autoData(ki) = dataArray(kd)
              if (simAddressing) then
                autoData(ki) = ki
              endif
              ki = ki+1
            enddo
            !$$$  kzl = kzl+1
            !$$$  zeroLags(kzl) = fact
            !     Actual Times and durations : no blanking at Bure
            if (numActualTimes.gt.0) ActualTimes(kt) = time_interval(1)+time_interval(2)   &
              /2
            if (numActualDurations.gt.0) ActualDurations(kt) = time_interval(2)
            kt = kt+1
          endif
        enddo
      enddo
    endif
    if (numBasebandFlags.gt.0) BasebandFlags= 0
    if (numZeroLags.gt.0) ZeroLags= 1.0
    ireturn = sdm_writeBinaryDataSubset(1, kr,   &
      timeSampling, time_interval(1)+time_interval(2)/2,   &
      time_interval(2), corrDataType, numBasebandFlags,   &
      basebandFlags, numActualTimes, actualTimes,   &
      numActualDurations, actualDurations, numZeroLags, zeroLags   &
      , numCrossData, crossData, numAutoData, autoData)
    if (ireturn.lt.0) then
      call sdmMessageI(8,3,sdmTable   &
        ,'Error in sdm_writeBinaryDataSubset', ireturn)
      goto 99
    endif
    kr = kr + 1
  enddo
  ireturn = sdm_closeBinaryDataWriter()
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Error in sdm_closeBinaryData', ireturn)
    goto 99
  endif
  ! Reset to the subscan-based Time_interval...
  kh = 1+ h_offset(r_ndump+1)
  call decode_header (dataArray(kh))
  call get_time_interval
!!!   dh_utc = nint(2*dh_utc)*0.5d0
!!!   time_interval(1) = ((dh_obs+60549.0d0)*86400.d0+dh_utc-0.5d0   &
!!!     *dh_integ)*1d9
!!!   ! Patch for TAI-UTC valin in 2006,2007, 2008; replace by correct software!
!!!   time_interval(1) = time_interval(1) + 33.*1d9
!!!   !
!!!   time_interval(2) = dh_integ*1d9
  !
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
  error = .true.
  return
  !
99 error = .true.
end subroutine write_sdm_CorrelatorData
!---------------------------------------------------------------------
!---------------------------------------------------------------------
subroutine write_sdm_TotalPowerData(ndata, data, uid, switch,   &
    iswitch, error)
  use gildas_def
  use gkernel_interfaces
  use sdm_Enumerations
  !---------------------------------------------------------------------
  !
  !     Write Total Power Data for subintegrations
  !
  !     ndata = data length (words)
  !     data = data array
  !     uid = data identification
  !     error = error return
  !
  !     in the 'switch' case we use only one switch state (e.q. 1 or 2).
  !
  !---------------------------------------------------------------------
  !     Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  !
  !     Dummy
  integer :: ndata, iswitch
  real :: data(ndata)
  character*(*) :: uid
  logical :: error, switch
  !
  !     Local
  integer, parameter :: maxSpW=2, maxPP=2
  real*4 :: scaleFactor(maxSpW, mrlband)
  integer :: SdPolProducts(maxPP), is, sdm_writeTpBinary, ireturn ,   &
    ia, ja, ib, ic, isb, nsb , iapc, ki, kd, j , numSdPolProduct,   &
    numBin(maxSpW, mrlband), ir, kh, kc , h_offset,   &
    c_offset, l, ll, kt, ier, ip, i, numInt, ivc
  integer numAutoData, numActualTimes, numActualDurations, numFlags
  !
  !     make these allocatable...
  integer, allocatable ::  flags(:)
  real*4, allocatable ::  autoData(:)
  integer*8, allocatable :: actualTimes(:), actualDurations(:)
  character sdmTable*12
  parameter (sdmTable='TPBinaryData')
  character fileName*256, uidFile*256
  integer :: basebandNames(mrlband)
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  call sdm_getUid(uid)
  uidFile = uid
  call uidToFilename(uidFile)
  l = lenc(file_name)
  ll = lenc(uid)
  fileName = file_name(1:l)//'/ASDMBinary/'//uidFile(1:ll)//char(0)
  !
  numBin = 1
  numInt = r_ndump
  if (r_nswitch.ne.0) then
    !     get only one state:
    numInt = r_ndump/r_nswitch
    !     get all switch states in:
    if (iswitch.eq.0) then
      numBin = r_nswitch
    endif
  endif
  !
  numSdPolProduct = r_npol_rec
  numAutoData = 0
  numFlags = 0
  numActualTimes = 0
  numActualDurations = 0
  nsb = r_lnsb
  do ic = 1, r_lband
    basebandNames(ic) = ic  ! should do better
  enddo
  sdPolProducts(1) = PolarizationType_X    ! check (NGR)
  sdPolProducts(2) = PolarizationType_Y    ! check (NGR)
  !c      do ic = 1, r_lband
  do ivc = 1, v_lband
    numAutoData = numAutoData + r_nant *   &
      numSdPolProduct*numInt
    numFlags = numFlags + r_nant   &
      * numSdPolProduct * numBin(1,ivc)*numInt
    numActualDurations = numActualDurations +numSdPolProduct*r_nant   &
      *numInt
    numActualTimes = numActualTimes + numSdPolProduct*r_nant*numInt
  enddo

  ! we do not fill in the flags at this time...
  numFlags = 0
  ! we do not blank at Bure
  numActualTimes = 0
  numActualDurations = 0

  ier = 0
  if (numAutoData.gt.0) allocate(AutoData(numAutoData), stat=ier)
  if (numFlags.gt.0) allocate(Flags(numFlags), stat=ier)
  if (numActualDurations.gt.0)   &
    allocate(ActualDurations(numActualDurations), stat=ier)
  if (numActualTimes.gt.0) allocate(ActualTimes(numActualTimes),   &
    stat=ier)
  if (ier.ne.0) goto 98
  !
  !     Loop over subintegrations
  ki = 0
  do ir = 1, r_ndump
    !     get data header:
    kh = 1+ h_offset(ir)
    call decode_header (data(kh))
    if (.not.switch .or. (dh_test0(1).eq.iswitch)) then
      !
      !     fill in the data; we use the 2 polarizations...
      !     pointer for data:
      do ia = 1, r_nant
        do ivc = 1, v_lband
          do ip=1, r_npol_rec
            ki = ki+1
            autoData(ki) = dh_total(ip,ia)
            if (r_totscale(ip,ia).ne.0) then
              autoData(ki) = autoData(ki) * r_totscale(ip,ia)
            endif
          enddo
        enddo
      enddo
    endif
  enddo
  ! be conservative ...
  if (numActualTimes.gt.0) actualTimes = time_interval(1) +time_interval(2)/2
  if (numActualDurations.gt.0) actualDurations = time_interval(2)
  if (numFlags.gt.0) flags = 0
  !
  ireturn = sdm_writeTpBinary(fileName, uid(1:ll)//char(0),   &
    execBlock_Uid, time_interval(1),time_interval(1)   &
    +time_interval(2)/2, time_interval(2), numInt, scan_number   &
    ,subscan_number ,r_execblocknum, r_nant, r_lband   &
    ,basebandNames, numBin, numSdPolProduct ,sdPolProducts,   &
    numFlags, flags, numActualTimes, actualTimes,   &
    numActualDurations, actualDurations, numAutoData, autoData)
  if (ireturn.lt.0) then
    call sdmMessageI(8,3,sdmTable   &
      ,'Error in sdm_writeTpBinary', ireturn)
    goto 99
  endif
  ! not needed?
  !      if (allocated(AutoData)) deallocate(AutoData, stat=ier)
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
  error = .true.
  return
  !
99 error = .true.
end subroutine write_sdm_TotalPowerData
!---------------------------------------------------------------------
subroutine get_sdm_CorrelatorData(SpectralResolution, ndata,   &
    dataArray, uid, numIntegration, integrationNumber, error)
  use gildas_def
  use gkernel_interfaces
  use sdm_Enumerations
  !---------------------------------------------------------------------
  !     Get the binary data header and the binary data for one of the
  !     integrations. Put it where appropriate in the Plateau de Bure Data
  !     Array.
  !---------------------------------------------------------------------
  !     Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  !
  !     Dummy
  integer :: SpectralResolution, ndata, integrationNumber,   &
    sdm_getBinaryDataHeader, sdm_getBinaryDataSubset, &
    sdm_closeBinaryDataReader
  real :: dataArray(ndata)
  character*(*) :: uid
  logical :: error
  !---------------------------------------------------------------------
  integer, parameter :: maxSpW=2, maxPP=1
  integer :: l, ll, scanNum, subscanNum, execBlockNum,   &
    numAntenna,  correlationMode, numAPC, apc(2),   &
    numSpectralWindow(mrlband), numBin(maxSpW,mrlband),   &
    numSpectralPoint(maxSpW,mrlband), numCrossPolProduct(maxSpW   &
    ,mrlband), numSdPolProduct(maxSpW ,mrlband),   &
    crossPolProducts(maxPP, maxSpW, mrlband), SdPolProducts(maxPP   &
    , maxSpW, mrlband), maxActualDurations, maxActualTimes,   &
    maxBasebandFlags, maxZeroLags, maxCrossData, maxAutoData,   &
    numActualDurations, numActualTimes, numBasebandFlags,   &
    numZeroLags, numCrossData, numAutoData, ier, numDataSubset,   &
    ir, ireturn, intNUm, intNums(2), kh, kc, klu, klc, kcc,   &
    numBaseBand, ki , numSubIntegration, numIntegration, c_offset   &
    , h_offset, l_offset, ia, ja, idd, kd(2), iapc, ib, isb, ic, ivc, i,   &
    j, kzl, ip , indexBure_CrossCont, indexBure_AutoCont,   &
    indexBure_CrossSpec, indexBure_AutoSpec, lenz, testCrossData
  integer :: conjugate(2) 
  data conjugate /1, -1/
  integer :: numCrossPP(msw), numSDPP(msw)
  real :: scaleF(msw)
  integer :: kd1, k, ispw, ibb
  integer*8 startTime, time, interval, dayTime, dayStart
  integer, allocatable ::  crossData(:), basebandFlags(:)
  real*4, allocatable ::  zeroLags(:), autoData(:)
  integer*8, allocatable :: actualTimes(:), actualDurations(:)
  real*4 :: scaleFactor(maxSpW, mrlband), fact, aver
  character :: uidFile*256, fileName*256, execBlockUid*256
  character, parameter ::  sdmTable*16='CorrelatorData'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ll = lenc(uid)
  uidFile = uid(1:ll)
  call uidToFilename(uidFile)
  l = lenc(file_name)
  fileName = file_name(1:l)//'/ASDMBinary/'//uidFile(1:ll)//char(0)
  ireturn = sdm_getBinaryDataHeader(fileName, execBlockUid,   &
    startTime, scanNum, subscanNum, execBlockNum, numAntenna ,   &
    correlationMode, numAPC, apc, mrlband, numBaseband,   &
    maxSpW, numSpectralWindow, numBin, numSpectralPoint,   &
    scaleFactor, maxPP, numCrossPolProduct, crossPolProducts,   &
    numSdPolProduct, sdPolProducts, maxActualDurations,   &
    maxActualTimes, maxBasebandFlags, maxZeroLags, maxCrossData,   &
    maxAutoData, numDataSubset)
  l = lenz(execBlockuid)
  if (execBlockuid(1:l).ne.execBlock_uid(1:l)) then
    call sdmMessage(2,2,sdmTable   &
      ,'Wrong ExecBlockUID : '//execBlockuid(1:l))
  endif
  if (correlationMode.eq.CorrelationMode_CROSS_AND_AUTO) then
    if (r_lmode.eq.1) then
      call sdmMessage(1,1,sdmTable   &
        ,'Correlation code CROSS_AND_AUTO;' //   &
        ' ignoring autocorrelation')
    elseif (r_lmode.eq.2) then
      call sdmMessage(1,1,sdmTable   &
        ,'Correlation code CROSS_AND_AUTO;' //   &

        ' ignoring correlation')
    endif
  elseif ((correlationMode.eq.CorrelationMode_CROSS_ONLY .and.   &
    r_lmode.eq.2) .or.(correlationMode.eq.   &
    CorrelationMode_AUTO_ONLY .and. r_lmode.eq.1) .or.   &
    (correlationMode.gt.2) .or. (correlationMode.lt. 0)) then
    call sdmMessageI(8, 3, sdmTable,'Wrong Correlation mode '   &
      , correlationMode)
    error = .true.
    return
  endif
  if (numBaseband.ne.v_lband) then
    call sdmMessageI(8, 3, sdmTable,'Wrong numBaseband '   &
      , numBaseband)
    call sdmMessageI(8, 3, sdmTable,'... expecting '   &
      , r_lband)
    error = .true.
    return
  endif
  ! at this point Bure has at most 2 spw per baseband, one for each sideband.
  ! we reassign the polproducts to the sideband number
  idd = 1
  testCrossData = 0
  do i=1, numBaseband
    do j=1, numSpectralWindow(i)
      if (numBin(j,i).ne.1) then
        call sdmMessageI(8, 3, sdmTable,'Wrong numBin ' ,   &
          numBin(j,i))
        call sdmMessageI(8, 3, sdmTable,'... using only ', 1)
      endif
      if (numCrossPolProduct(j,i).gt.2) then
        call sdmMessageI(8, 2, sdmTable   &
          ,'NumCrossPolProduct too large :'   &
          ,numCrossPolProduct(j ,i))
        call sdmMessageI(8, 2, sdmTable,'... using ', 1)
        numCrossPP(idd) = 1
      else
        numCrossPP(idd) = numCrossPolProduct(j, i)
      endif
      testCrossData = testCrossData + numCrossPolProduct(j,i)*numSpectralPoint(j,i)
      if (numSdPolProduct(j,i).gt.2) then
        call sdmMessageI(8, 2, sdmTable   &
          ,'NumSdPolProduct too large: ',numSdPolProduct(j,i))
        call sdmMessageI(8, 2, sdmTable,'... using ', 1)
        numSDPP(idd) = 1 
      else
        numSDPP(idd) = numSdPolProduct(j, i)
      endif
      scaleF(idd) = scaleFactor(j, i)
      idd = idd+1
    enddo
  enddo
  !     for safety!!
  if (r_isb.ne.1 .and. r_isb.ne. -1) then
    r_isb = 1
  endif
  !
  !     *** Set the tsys scaling factors to 1.0 (mostly to avoid trouble
  !     with this for the time being)
  do ia=1, r_nant
    do ic = 1, r_lband
      ip = r_lpolentry(ia,ic)
      if (r_tsyss(ip,ia).le.0) r_tsyss(ip,ia)=1.
      if (r_tsysi(ip,ia).le.0) r_tsysi(ip,ia)=1.
      isb = (3-r_isb)/2
      dh_atfac(ic,isb,ia) = r_tsyss(ip,ia)
      isb = (3+r_isb)/2
      dh_atfac(ic,isb,ia) = r_tsysi(ip,ia)
    enddo
  enddo
  !
  !     allocate the required arrays
  ier = 0
  if (maxActualTimes.gt.0) allocate(ActualTimes(maxActualTimes),   &
    stat=ier)
  if (maxActualDurations.gt.0)   &
    allocate(ActualDurations(maxActualDurations), stat=ier)
  if (maxZeroLags.gt.0) allocate(ZeroLags(maxZeroLags), stat=ier)
  if (maxBasebandFlags.gt.0)   &
    allocate(BasebandFlags(maxBasebandFlags), stat=ier)
  if (maxAutoData.gt.0) allocate(AutoData(maxAutoData), stat=ier)
  if (maxCrossData.gt.0) allocate(CrossData(maxCrossData), stat=ier)
  if (ier.ne.0) goto 98
  ! 
  intNums  = integrationNumber
  ir = r_ndump+1
  if (SpectralResolution .eq. SpectralResolutionType_FULL_RESOLUTION   &
    ) then
    ir = r_ndump+1
  !     integration range:
  elseif (SpectralResolution .eq.   &
    SpectralResolutionType_CHANNEL_AVERAGE) then
    !
    !     number of subintegrations (data subsets per integration)
    !
    numSubIntegration = numDataSubset/numIntegration
    !     should match
    intNums = (/ numSubIntegration *(IntegrationNumber-1) + 1,   &
      numSubIntegration *IntegrationNumber /)
    ir = 1
  endif
  !
  do intNum = intNums(1), intNums(2)
    ireturn = sdm_getBinaryDataSubset(intNum, numActualDurations,   &
      maxActualDurations, actualDurations, numActualTimes,   &
      maxActualTimes, actualTimes, numBasebandFlags,   &
      maxBasebandFlags, basebandFlags, numZeroLags, maxZeroLags,   &
      zeroLags, numCrossData, maxCrossData, crossData,   &
      numAutoData, maxAutoData, autoData)
    if (ireturn.lt.0)    goto 99
    !     pointer for data:
    kc = 1+ c_offset(ir)
    klu = 1+ l_offset(r_ndump+1)
    klc = 1+ l_offset(r_ndump+2)
    !     get data header:
    kh = 1+ h_offset(ir)
    ! not needed in principle.
    ! call decode_header (dataArray(kh))
    if (error) return
    dh_dump = ir
    !
    !     *** Set the tsys scaling factors to 1.0 (mostly to avoid trouble
    !     with this for the time being)
    do ia=1, r_nant
      do ic = 1, r_lband
        ip = r_lpolentry(ia,ic)
        if (r_tsyss(ip,ia).le.0) r_tsyss(ip,ia)=1.
        if (r_tsysi(ip,ia).le.0) r_tsysi(ip,ia)=1.
        isb = (3-r_isb)/2
        dh_atfac(ic,isb,ia) = r_tsyss(ip,ia)
        isb = (3+r_isb)/2
        dh_atfac(ic,isb,ia) = r_tsysi(ip,ia)
      enddo
    enddo
    !
    if (r_lmode .eq. 1) then
      if (numCrossData.le.0) then
        call sdmMessageI(8,3,sdmTable   &
          ,'Wrong crossData binary length ',numCrossData)
        goto 99
      endif
      ki = 1
      !     kt = 0
      do ib = 1, r_nbas
        ia = r_iant(ib)
        ja = r_jant(ib)
        do idd = 1, ddId_size
          isb = sb_list(idd)
          ivc = bb_list(idd)
          
          if (SpectralResolution .eq.   &
            SpectralResolutionType_FULL_RESOLUTION) then
            do iapc = 1, numApc
              do ip = 1, numCrossPP(idd)
                ic = v_bands(ivc, ip)
                kd(ip) = klu-1 + indexBure_CrossSpec(ib, ic, isb, 1)
                if (apc(iapc).eq.AtmPhaseCorrection_AP_CORRECTED) then
                  kd(ip) = kd(ip) + klc-klu
                endif
              enddo
              do i = 1, r_lnch(ic)
                do ip = 1, numCrossPP(idd)
                  ! corresponding bure subband
                  ic = v_bands(ivc, ip)
                  
                  fact = sqrt(dh_atfac(ic,isb,ia)   &
                       *dh_atfac(ic,isb,ja)) / scaleF(idd)
                  !     real and imaginary parts
                  do j=1, 2
                    if (kd(ip) .gt.ndata) goto 97
                    ! We reverse phases, to get the right orientation of maps after
                    ! complex conjugation has been applied in the correlator
                    dataArray(kd(ip)) = conjugate(j) * crossData(ki)*fact
                    ki = ki+1
                  enddo
                enddo
              enddo
            !     Actual Times and durations : no blanking at Bure - ignore
            !     kt = kt+1
            !     ActualTimes(kt) = time_interval(1)
            !     ActualDurations(kt) = time_interval(2)
            enddo
          else                 ! (SpectralResolutionType_CHANNEL_AVERAGE)
            do ip = 1, numCrossPP(idd)
              ic = v_bands(ivc, ip)
              kd(ip) = kc-1 + indexBure_CrossCont(ib, ic, isb)
              !kd1 = kd
            enddo
            do iapc = 1, numApc
              !     real and imaginary parts; Bure files have only uncorrected data for channel averages.
              !     (... so far only one correlation product ... the others are ignored.)
              do ip = 1, numCrossPP(idd)
              !do ip = 1, numCrossPolProduct(isb,ic)
                ic = v_bands(ivc, ip)
                fact = sqrt(dh_atfac(ic,isb,ia)   &
                     *dh_atfac(ic,isb,ja)) / scaleF(idd)
                do j=1, 2
                  !if (ip.eq.1 .and. apc(iapc).eq.   &
                  if (apc(iapc).eq.   &
                    AtmPhaseCorrection_AP_UNCORRECTED)   &
                    then
                    if (kd(ip).gt.ndata) goto 97
                    ! We reverse phases, to get the right orientation of maps after
                    ! complex conjugation has been applied in the correlator
                    dataArray(kd(ip)) = conjugate(j) * crossData(ki)*fact
                    kd(ip) = kd(ip)+1
                  endif
                  ki = ki+1
                enddo
              enddo
            enddo
          endif
        enddo                  ! idd
      enddo                    ! ib
    !     autocorrelation is easy 
    elseif (r_lmode .eq. 2) then
      if (numAutoData.le.0) then
        call sdmMessageI(8,3,sdmTable   &
          ,'Wrong autoData binary length ',numAutoData)
        goto 99
      endif
      ki = 1
      kzl = 0
      !     kt = 0
      do ia = 1, r_nant
        do idd = 1, ddId_size
          ivc = bb_list(idd)
          !
          !     for autocorrelation isb is 1
          isb = 1
          !$$$                  isb = sb_list(idd)
          !     not used?            af = dh_atfac(1,ia)
          if (SpectralResolution .eq.   &
            SpectralResolutionType_FULL_RESOLUTION) then
            do ip = 1, numSdPP(idd)
              ic = v_bands(ivc, ip)
              kd(ip) = klu-1 + indexBure_AutoSpec(ia, ic, 1)
            enddo
            do i = 1, r_lnch(ic)
              do ip = 1, numSdPP(idd)
                if (kd(ip)+i-1.gt.ndata) goto 96
                dataArray(kd(ip)+i-1) = autoData(ki)
                ki = ki+1
              enddo
            enddo
            kzl = kzl+1
            fact = zeroLags(kzl)
          !     Actual Times and durations : no blanking at Bure, ignore
          !     kt = kt+1
          !     ActualTimes(kt) = time_interval(1)
          !     ActualDurations(kt) = time_interval(2)
          else                 ! (SpectralResolutionType_CHANNEL_AVERAGE)
            do ip = 1, numSdPP(idd)
              ic = v_bands(ivc, ip)
              kd(ip) = kc-1 + indexBure_AutoCont(ia, ic)
              if (kd(ip).gt.ndata) goto 96
              dataArray(kd(ip)) = autodata(ki)
              ki = ki+1
            enddo
          endif
        enddo                  ! idd
      enddo                    ! ia
    endif
    if (kh.gt.ndata) goto 95
    call encode_header (dataArray(kh))
    if (error) return
    ir = ir+1                  ! the next one (Chann aver only)
  enddo                        ! loop on  data subsets
  !
  ireturn = sdm_closeBinaryDataReader()
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
97 call sdmmessage(8,4,sdmTable ,'Too much binary (cross) data, ignoring data subset')
  ! error = .true.
  return
96 call sdmmessage(8,4,sdmTable ,'Too much binary (auto) data, ignoring data subset')
  ! error = .true.
  return
95 call sdmmessage(8,4,sdmTable ,'data header addressing error')
  ! error = .true.
  return
  ! 
98 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
  error = .true.
  return
  !
99 error = .true.
  ireturn = sdm_closeBinaryDataReader()
end subroutine get_sdm_CorrelatorData
!---------------------------------------------------------------------
subroutine get_sdm_TotalPowerData(ndata, dataArray, uid,   &
    numIntegration, IntegrationNumber, error)
  use gildas_def
  use gkernel_interfaces
  use sdm_Enumerations
  !---------------------------------------------------------------------
  !     Get the binary data header and the binary data for a set of
  !     subintegrations. Put it where appropriate in the Plateau de Bure
  !     Data Array.
  !---------------------------------------------------------------------
  !     Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  include 'gbl_pi.inc'
  !
  !     Dummy
  integer :: ndata, integrationNumber, sdm_getBinaryDataHeader,   &
    sdm_getBinaryDataSubset, numIntegration, sdm_closeBinaryDataReader
  integer numSubIntegration, numTime, sdm_getTpBinary
  real :: dataArray(ndata)
  character*(*) :: uid
  logical :: error
  !---------------------------------------------------------------------
  integer, parameter :: maxSpW=2, maxPP=1
  integer :: l, ll, scanNum, subscanNum, execBlockNum,   &
    numAntenna,  correlationMode, numAPC, apc(2),   &
    numSpectralWindow(mrlband), numBin(maxSpW,mrlband),   &
    numSpectralPoint(maxSpW,mrlband), numCrossPolProduct(maxSpW   &
    ,mrlband), numSdPolProduct(maxSpW ,mrlband),   &
    crossPolProducts(maxPP, maxSpW, mrlband), SdPolProducts(maxPP   &
    , maxSpW, mrlband), maxAutoData, numAutoData, ier,   &
    numDataSubset, ir, ireturn, kh, kc, ki, ic, ia, ip, h_offset,   &
    c_offset, lenz
  integer*8 startTime
  integer, allocatable ::  crossData(:), basebandFlags(:)
  real*4, allocatable ::  zeroLags(:), autoData(:)
  integer*8, allocatable :: actualTimes(:), actualDurations(:)
  real*4 :: scaleFactor(maxSpW, mrlband)
  character :: uidFile*256, fileName*256, execBlockUid*256
  character, parameter ::  sdmTable*24='BinaryTotalPowerData'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  uidFile = uid
  call uidToFilename(uidFile)
  l = lenc(file_name)
  ll = lenc(uid)
  fileName = file_name(1:l)//'/ASDMBinary/'//uidFile(1:ll)//char(0)
  ier = 0
  !     2 polarizations max
  ir = 1
  maxAutoData = r_ndump*r_npol_rec*r_lband*r_nant   &
    *numIntegration
  if (maxAutoData.gt.0) allocate(AutoData(maxAutoData), stat=ier)
  if (ier.ne.0) goto 98
  numAutoData = 0
  ireturn = sdm_getTpBinary(fileName, execBlockUid, startTime,   &
    numTime, scanNum,subscanNum, execBlockNum, numAntenna,   &
    mrlband, r_lband, numBin, maxPP, numSdPolProduct,   &
    sdPolProducts, numAutoData,maxAutoData, autodata)
  l = lenc(execBlock_uid)
  if (execBlockuid(1:l).ne.execBlock_uid(1:l)) then
    call sdmMessage(2,2,sdmTable   &
      ,'Wrong ExecBlockUID : '//execBlockuid(1:l))
  endif
  !
  ! number of subintegrations (data subsets per integration)
  numSubIntegration = numTime / numIntegration
  !
  !
  !     Loop over subintegrations
  ki = numSubIntegration*(IntegrationNumber-1)
  do ir = 1, r_ndump
    !     get data header:
    kh = 1+ h_offset(ir)
    call decode_header (dataArray(kh))
    if (error) return
    !
    !     fill in the data; we use the 2 polarizations...
    !     pointer for data:
    kc = 1+ c_offset(ir)
    do ia = 1, r_nant
      do ic = 1, r_lband
        do ip=1, r_npol_rec
          ki = ki+1
          dh_total(ip,ia) = autoData(ki)
        enddo
      enddo
    enddo
    call encode_header (dataArray(kh))
    if (error) return
  enddo
  r_totscale = 1.
  ireturn = sdm_closeBinaryDataReader()
  !call sdm_closeBinaryDataReader()
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
98 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
  ireturn = sdm_closeBinaryDataReader()
  error = .true.
  return
  !
99 error = .true.
end subroutine get_sdm_TotalPowerData
!---------------------------------------------------------------------
!
subroutine sdm_scale(n, d, a, dmax)
  !---------------------------------------------------------------------
  ! Rescale array D dividing by A
  ! update maximum absolute value
  !---------------------------------------------------------------------
  include 'clic_parameter.inc'
  integer n, i
  real d(n), a, dmax
  do i=1, n
    if (ABS(d(I)-BLANK4).ge.D_BLANK4) then
      d(i) = d(i) / a
      dmax = max(dmax,abs(d(i)))
    endif
  enddo
end subroutine sdm_scale
!---------------------------------------------------------------------
subroutine sdm_getUid(uid)
  !
  !     format a Uid string
  !
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'sdm_identifiers.inc'
  data next_Uid/0/
  character*(*) uid
  write (uid,'(a,i5.5,a1)') 'uid://X1/X1/X',next_Uid   &
    ,char(0)
  next_Uid = next_Uid+1
  return
end subroutine sdm_getUid
!
subroutine uidToFilename(uid)
  use gkernel_interfaces
  !
  !     re format a Uid string to a file name
  !
  integer l, i
  character*(*) uid
  l = lenc(uid)
  do i=1, l
    if (uid(i:i).eq.':' .or. uid(i:i).eq.'/') then
      uid(i:i) = '_'
    endif
  enddo
  return
end subroutine uidToFilename
!
integer function indexBure_AutoCont(ia, ic)
  ! Bure data indexing
  ! auto correlation
  ! ia = antenna number
  ! ic = subband number
  integer ia, ic, ntch
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !
  ntch = r_nband
  indexBure_AutoCont = ic + (ia-1)*ntch
  return
end function indexBure_AutoCont
!
integer function indexBure_CrossCont(ib, ic, isb)
  !
  !     Bure data indexing (real part)
  !     correlation, cont subband
  !     ib = baseline number
  !     isb = sideband number
  !     ic = subband number
  !
  integer ib, ic, isb, ich, ntch, nsb
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !
  ntch = r_nband
  nsb = r_nsb
  ich = ic - 1
  indexBure_CrossCont = (ich+(isb-1+(ib-1)*nsb)*ntch)*2+1
  return
end function indexBure_CrossCont
!
integer function indexBure_AutoSpec(ia, ic, i)
  !
  !     Bure data indexing, auto correlation, spectral data
  !     ia = antenna number
  !     ic = subband number
  !     i = channel number
  !
  integer ia, ic, ich, ntch, i
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !
  ntch =  r_lntch
  ich = r_lich(ic)
  indexBure_AutoSpec = i+ich + (ia-1)*ntch
  return
end function indexBure_AutoSpec
!
integer function indexBure_CrossSpec(ib, ic, isb, i)
  !
  !     Bure data indexing (real part)
  !     correlation, , spectral data
  !     ib = baseline number
  !     isb = sideband number
  !     ic = subband number
  !     i = channel number
  !
  integer ib, ic, isb, ich, i, ntch, nsb
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !
  ntch =  r_lntch
  ich = r_lich(ic)
  nsb = r_lnsb
  indexBure_CrossSpec = 1+ (i+ich-1 + (isb-1 + (ib-1)*nsb)*ntch)*2
  return
end function indexBure_CrossSpec
