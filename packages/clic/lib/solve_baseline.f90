subroutine solve_baseline(line,error)
  use gildas_def
  use gkernel_types
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !     Interface routine for SOLVE BASELINE/SEARCH
  ! This has been modified to allocate the outpput variables arrays dynamically
  ! What remains to be done: move alloptions/arguments recovery
  ! in the main subroutine before calls to sic_libr,
  ! so that it is not necessary to reanalyse the command line by a call to
  ! sic_analyse.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: mtry
  parameter(mtry=20)
  integer :: ntry, ktry
  real*8 :: f_to_k                ! (2*pi/clight)*1d6
  parameter (f_to_k = 2*pi/299792458.d-6)
  ! Local variables:
  integer :: ier, b_dim
  logical :: search, found
  real :: search_box, search_interval, ddb, kw
  integer(kind=address_length) :: ipx,ipy,ipw
  integer(kind=address_length) :: i_solution, i_start, i_rmss, i_dist
  type(sic_descriptor_t) :: desc
  integer :: nsize, nline
  character(len=64) :: csize
  character(len=12) :: comm
  !
  !------------------------------------------------------------------------
  ! Code:
  ! Verify if any input file
  call check_input_file(error)
  if (error) return
  !
  call check_index(error)
  if (error) return
  !  
  if (change_display) then
     call read_data('ALL',.false.,.true.,error)
     if (error) return
  endif
  !
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ! just in case m_data has changed
  call get_first(.true.,error)
  if (error) return
  !
  ! Determine NTRY
  search = sic_present(7,0)
  if (search) then
    search_box = 0.01          ! 1 cm default
    search_interval = 0.0      ! according to freq.
    call sic_r4(line,7,1,search_box,.false.,error)
    if (error) return
    call sic_r4(line,7,2,search_interval,.false.,error)
    if (error) return
  endif
  !
  if (search) then
    if (search_interval.eq.0) then
      kw = r_flo1*f_to_k
      ddb = pi/kw
    else
      ddb = search_interval
    endif
    ntry = min(mtry, int(search_box/ddb + 1.))
    ktry = ntry**3
  else
    ntry = 1
    ktry = 1
  endif
  !
  ! Delete the previous SIC variables...
  call sic_delvariable('B_SOLUTION',.false.,error)
  call sic_delvariable('B_START',.false.,error)
  call sic_delvariable('B_RMS',.false.,error)
  call sic_delvariable('B_DIST',.false.,error)
  error = .false.
  !
  ! Ask SIC to define the variables, and recover their addresses
  write(csize,'(I12,A,I12)') ktry,',',abs(n_base)
  nsize = lenc(csize)
  call sic_black(csize,nsize)
  b_dim =  ktry*abs(n_base)
  !
  call sic_libr('DEFINE REAL B_SOLUTION[3,'//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('B_SOLUTION', desc, found)
  if (.not. found) goto 999
  i_solution = gag_pointer(desc%addr,memory)
  !
  call sic_libr('DEFINE REAL B_START[3,'//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('B_START', desc, found)
  if (.not. found) goto 999
  i_start = gag_pointer(desc%addr,memory)
  call sic_libr('DEFINE REAL B_RMS['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('B_RMS', desc, found)
  if (.not. found) goto 999
  i_rmss = gag_pointer(desc%addr,memory)
  call sic_libr('DEFINE REAL B_DIST['//csize(1:nsize)//   &
    '] /GLOBAL',error)
  call sic_descriptor ('B_DIST', desc, found)
  if (.not. found) goto 999
  i_dist = gag_pointer(desc%addr,memory)
  !
  ! Reanalyze the command line, because we overwrote it...
  nline = lenc(line)
  call sic_find(comm,line,nline,error)
  !
  ! Solve for Baseline
  call sub_solve_baseline(m_data,m_boxes,memory(ipx),   &
    memory(ipy),memory(ipw),line,error,   &
    b_dim,   &
    memory(i_solution), memory(i_start),   &
    memory(i_rmss), memory(i_dist) )
  !
  return
  !
999 call message(8,3,'SOLVE BASELINE',   &
    'Cannot allocate display variables')
  call sic_delvariable('B_SOLUTION',.false.,error)
  call sic_delvariable('B_START',.false.,error)
  call sic_delvariable('B_RMS',.false.,error)
  call sic_delvariable('B_DIST',.false.,error)
  error = .true.
end subroutine solve_baseline
!
subroutine sub_solve_baseline(md,mb,x_data,y_data,   &
    w_data,line,error,b_dim,b_solution, b_start, b_rmss, b_dist)
  use gildas_def
  use gkernel_interfaces, no_interface=>gr4_sort
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !     Interface routine for SOLVE BASELINE/SEARCH
  !     Solves for baseline components errors.
  !     SOLVE BASELINE [DIFF] /OFFSET base1 dx dy dz ...
  ! Arguments
  !     MD       I     Number of data
  !     MB       I     Number of boxes
  !     X_DATA   R*4   X coordinates of data in boxes
  !     Y_DATA   R*4   Y coordinates of data in boxes
  !     W_DATA   R*4   Weights of data in boxes
  !     LINE     C*(*) Command line
  !     ERROR    L     Logical error flag
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  character(len=*) :: line          !
  logical :: error                  !
  integer :: b_dim                  !
  real :: b_solution(3,b_dim)       ! x, y, z
  real :: b_start(3,b_dim)          ! x0, y0, z0
  real :: b_rmss(b_dim)             ! rms
  real :: b_dist(b_dim)             ! rms
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_stations.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: mtry,mtry3
  parameter (mtry=200,mtry3=mtry*mtry*mtry)
  !$$$      REAL B_SOLUTION(3,MTRY3*MNBAS)     ! x, y, z
  !$$$      REAL B_START(3,MTRY3*MNBAS)        ! x0, y0, z0
  !$$$      REAL B_RMSS(MTRY3*MNBAS)           ! rms
  !$$$      REAL B_DIST(MTRY3*MNBAS)           ! rms
  integer :: j, nd, i0, nch,ier
  integer :: iha, ide, iph, ipha, iphd, ibase, ich, iband, i, l, l1, l2, l3
  integer :: k ,ib, ntry, i1, i2, i3, iba, base, iti, ipht, iph0, ix, iy
  integer :: dim(3), ktry, iel, iphe, iph1
  integer, allocatable :: index(:),iwork(:)
  real, allocatable :: solution(:,:)  ! x, y, z
  real, allocatable :: start(:,:)     ! x0, y0, z0
  real, allocatable :: rms(:)         ! rms
  real*8 :: kw
  logical :: plot, offset, search, reset
  real*8 :: f_to_k                ! (2*pi/clight)*1d6
  parameter (f_to_k = 2*pi/299792458.d-6)
  real*4 :: boff(3,mnbas)
  real :: search_box, db(3), bmes(8), bmin(8), ddb, sig, sigmin
  real :: savedb(3), search_interval
  character(len=132) :: chain
  integer :: m_base
  integer(kind=address_length) :: data_base, ipd
  logical :: diff
  !
  integer :: mvoc1, nkw
  parameter (mvoc1=2)
  character(len=12) :: argum, kw1, voc1(mvoc1)
  data voc1/'ABSOLUTE','DIFFERENCES'/
  !      call sic_ambigs('SOLVE_BASELINE',argum,kw1,nkw,voc1,mvoc1,error)
  data m_base/0/
  !------------------------------------------------------------------------
  save m_base, data_base
  ! Code:
  !
  allocate(index(mtry3),iwork(mtry3),solution(mtry3,3),start(mtry3,3),rms(mtry3),stat=ier)
  if (failed_allocate('SOLVE_BASELINE','MTRY3 buffers',ier,error))  return
  !
  !!      CALL SIC_DELVARIABLE('B_SOLUTION',.FALSE.,ERROR)
  !!      CALL SIC_DELVARIABLE('B_START',.FALSE.,ERROR)
  !!      CALL SIC_DELVARIABLE('B_RMS',.FALSE.,ERROR)
  !!      CALL SIC_DELVARIABLE('B_DIST',.FALSE.,ERROR)
  error = .false.
  argum = 'ABSOLUTE'
  call sic_ke(line,0,2,argum,nch,.false.,error)
  if (error) goto 999
  call sic_ambigs('SOLVE_BASELINE',argum,kw1,nkw,voc1,mvoc1,error)
  if (error) goto 999
  diff = kw1.eq.'DIFFERENCES'
  if (diff) call message(1,2,'SOLVE_BASELINE','Using differences')
  !
  ! Offset values:
  reset = sic_present(4,0)
  offset = sic_present(5,0)
  search = sic_present(7,0)
  sinele = sic_present(16,0)
  !
  do ib=1, mnbas
    do i=1, 3
      boff (i,ib) = 0
    enddo
  enddo
  if (search) then
    search_box = 0.01          ! 1 cm default
    search_interval = 0.0      ! according to freq.
    call sic_r4(line,7,1,search_box,.false.,error)
    if (error) goto 999
    call sic_r4(line,7,2,search_interval,.false.,error)
    if (error) goto 999
  endif
  if (reset) then
    do i=1, mnant
      f_afit(i) = .false.
    enddo
    do i=1, mnbas
      f_bfit(i) = .false.
    enddo
  endif
  if (offset) then
    i = 1
    do while(sic_present(5,i))
      if (i_base(1).gt.0) then
        call sic_ke(line,5,i,argum,nch,.true.,error)
        if (error) goto 999
        call base_to_n(argum,j,error)
        if (error) then
          call message(8,4,'SOLVE_BASELINE',   &
            'Invalid baseline '//argum)
          goto 999
        endif
      else
        call sic_i4(line,5,i,j,.true.,error)
        if (error) goto 999
      endif
      do k=1, 3
        call sic_r4(line,5,i+k,boff(k,j),.true.,error)
        if (error) goto 999
      enddo
      i = i+4
    enddo
  endif
  !
  if (diff) then
    if (i_average.ne.1) then
      i_average = 1
      change_display = .true.
    endif
  endif
  if (change_display .or. sorted) then
    ! plot all scans in index, unsorted/X, and re-initialize the plot.
    call read_data('ALL',.false.,.true.,error)
    if (error) return
    change_display = .true.
    plotted = .false.
  endif
  ideg = 0
  if (sic_present(9,0)) then   ! /POL degree (<=3)
    call sic_i4(line,9,1,ideg,.false.,error)
    if (error) goto 999
    ideg = max(0,min(3,ideg))
  endif
  plot = sic_present(1,0)
  if (plot .and. .not.plotted) then
    clear = .true.
    call sub_plot('All',.false.,.false.,0,error)
    if (error) return
  endif
  ! Check data columns for HA, SINDEC : boxes iha, ide.
  iha = 0
  ide = 0
  iti = 0
  iph = 0
  iel = 0
  do ib=1, n_boxes
    ix = i_x(k_x(ib))
    iy = i_y(k_y(ib))
    if (ix.eq.xy_hour) iha = ib
    if (ix.eq.xy_decli) ide = ib
    if (ix.eq.xy_time) iti = ib
    if (ix.eq.xy_eleva) iel = ib
    if (iy.eq.xy_phase) iph = ib
  enddo
  if (iel.eq.0) then
    ielev = 0
  else
    ielev = 1
  endif
  error = .false.
  if (iha.eq.0) then
    call message(6,3,'SOLVE_BASELINE',   &
      'Hour Angle must be selected as one X axis')
    error = .true.
  endif
  if (ide.eq.0) then
    call message(6,3,'SOLVE_BASELINE',   &
      'Declination must be selected as one X axis')
    error = .true.
  endif
  if (iph.eq.0) then
    call message(6,3,'SOLVE_BASELINE',   &
      'Phase must be selected as one Y axis')
    error = .true.
  endif
  if (error) return
  if (iti.ne.0) then
    write(chain,*) 'Using a polynomial of degree ',ideg,   &
      ' for time dependence'
    call message(4,1,'SOLVE_BASELINE',chain(1:lenc(chain)))
  endif
  !
  ! Loop on : baselines, bands, subbs (La Triple Boucle)
  if (search) then
    if (search_interval.eq.0) then
      kw = r_flo1*f_to_k
      ddb = pi/kw
    else
      ddb = search_interval
    endif
    ntry = min(mtry, int(search_box/ddb + 1.))
  else
    ntry = 1
  endif
  !
  do ibase = 1, n_base
    iba = abs(i_base(ibase))
    do ich = 1, n_subb
      do iband = 1, n_band
        if (i_band(iband).eq.1) then
          kw = (r_flo1+r_fif1)*f_to_k
        elseif (i_band(iband).eq.2) then
          kw = (r_flo1-r_fif1)*f_to_k
        elseif (i_band(iband).eq.3) then
          kw = r_flo1*f_to_k
        elseif (i_band(iband).eq.4) then !USB
          kw = -2*r_isb*r_fif1*f_to_k
        else
          goto 100
        endif
        ipha = 0
        iphd = 0
        ipht = 0
        iphe = 0
        do ib = 1, n_boxes
          if (ibase.eq.k_base(ib) .and. ich.eq.k_subb(ib)   &
            .and. iband.eq.k_band(ib)) then
            ix = i_x(k_x(ib))
            iy = i_y(k_y(ib))
            if (iy.eq.xy_phase) then
              if (ix.eq.xy_hour) then
                ipha = ib
              elseif (ix.eq.xy_decli) then
                iphd = ib
              elseif (ix.eq.xy_time) then
                ipht = ib
              elseif (ix.eq.xy_eleva) then
                iphe = ib
              endif
            endif
          endif
        enddo
        if (f_bres(ipha) .or. f_bres(iphd)) then
          call message(6,3,'SOLVE_BASELINE',   &
            'Residuals - not fitted')
          goto 100
        endif
        nd = min(n_data(ipha),n_data(iphd))
        if (ipht.ne.0) nd = min(nd,n_data(ipht))
        if (iphe.ne.0) nd = min(nd,n_data(iphe))
        !
        ! Get auxiliary storage
        ! 18 means 2*(mvar+1), where mvar is 3*  coords + 4 time var + 1 elev = 8
        if (nd*18.gt.m_base) then
          if (m_base .gt.0) then
            call free_vm(m_base,data_base)
            m_base = 0
          endif
          error = (sic_getvm(nd*18,data_base).ne.1)
          if (error) return
          m_base = nd*18
        endif
        ipd = gag_pointer(data_base,memory)
        !
        ! No search option:
        if (ipht.eq.0) then
          iph0 = ipha
        else
          iph0 = ipht
        endif
        if (iphe.eq.0) then
          iph1 = ipha
        else
          iph1 = iphe
        endif
        if (.not. search) then
          call solve_bas_sub (nd,kw,x_data(1,iphd),   &
            x_data(1,ipha), x_data(1,iph0), x_data(1,iph1),   &
            y_data(1,ipha), w_data(1,ipha),   &
            boff(1,iba), bmin,   &
            sigmin , memory(ipd+2*nd), memory(ipd), error,   &
            diff)
          do i=1, 3
            solution(1,i) = bmin(i)
            start(1,i) = 0
          enddo
          rms(1) = sigmin
          ktry = 1
        else
1011      format('Search range in ',a1,': [',f8.6,','f8.6,']')
          write (chain,1011) 'X', boff(1,iba)-search_box/2.,   &
            boff(1,iba)-search_box/2.+ddb*ntry
          call message(6,1,'SUB_SOLVE_BASELINE',chain)
          write (chain,1011) 'Y', boff(2,iba)-search_box/2.,   &
            boff(2,iba)-search_box/2.+ddb*ntry
          call message(6,1,'SUB_SOLVE_BASELINE',chain)
          write (chain,1011) 'Z', boff(3,iba)-search_box/2.,   &
            boff(3,iba)-search_box/2.+ddb*ntry
          call message(6,1,'SUB_SOLVE_BASELINE',chain)
          sigmin = 1e20
          db(1) = boff(1,iba)-search_box/2.-ddb
          ktry = 0
          do i1 = 1, ntry
            db(1) = db(1)+ddb
            db(2) = boff(2,iba)-search_box/2.-ddb
            do i2 = 1, ntry
              db(2) = db(2)+ddb
              db(3) = boff(3,iba)-search_box/2.-ddb
              do i3 = 1, ntry
                db(3) = db(3)+ddb
                ktry = ktry+1
                do i=1, 3
                  start(ktry,i) = db(i)
                enddo
                call solve_bas_sub (nd,kw,x_data(1,iphd),   &
                  x_data(1,ipha), x_data(1,iph0),   &
                  x_data(1,iph1),   &
                  y_data(1,ipha), w_data(1,ipha), db ,bmes,   &
                  sig,  memory(ipd+2*nd), memory(ipd),   &
                  error, diff)
                do i=1, 3
                  solution(ktry,i) = bmes(i)
                enddo
                rms(ktry) = sig
                if (sig.lt.sigmin   &
                  .and.   &
                  abs(bmes(1)-boff(1,iba)).lt.search_box/2   &
                  .and.   &
                  abs(bmes(2)-boff(2,iba)).lt.search_box/2   &
                  .and.   &
                  abs(bmes(3)-boff(3,iba)).lt.search_box/2)   &
                  then
                  do i=1, 4+ideg+ielev
                    bmin(i) = bmes(i)
                  enddo
                  sigmin = sig
                  savedb(1) = db(1)
                  savedb(2) = db(2)
                  savedb(3) = db(3)
                endif
              enddo
            enddo
          enddo
        endif
        if (i_base(ibase).gt.0) then
          do i=1,3
            b_fit(i,iba) = bmin(i) + r_bas(i,iba)
          enddo
          do i=4,4+ideg + ielev
            b_fit(i,iba) = bmin(i)
          enddo
          f_bfit(iba) = .true.
          do i=1, mnant
            f_afit(i) = .false.
          enddo
        else
          do i=1,3
            a_fit(i,iba) = bmin(i) + stat99(i,r_istat(1))
            if (iba.ne.1) a_fit(i,iba) =  a_fit(i,iba)   &
              + r_bas(i,base(1,iba))
          enddo
          do i=4,4+ideg + ielev
            a_fit(i,iba) = bmin(i)
            a_fit(i,1) = 0
          enddo
          f_afit(iba) = .true.
          do i=1, mnbas
            f_bfit(i) = .false.
          enddo
        endif
        !
        ! Display results
        l = lenc(y_label(ipha))
        l1 = lenc(header_1(ipha))
        l2 = lenc(header_2(ipha))
        l3 = lenc(header_3(ipha))
        write(chain,1000) header_1(ipha)(1:l1),   &
          header_2(ipha)(1:l2), header_3(ipha)(1:l3), sigmin
        call message(6,1,'SOLVE_BASELINE',   &
          chain(1:lenc(chain)))
        write(chain,1001) (bmin(i), i=1,3)
        call message(6,1,'SOLVE_BASELINE',   &
          chain(1:lenc(chain)))
        if (i_base(ibase).gt.0) then
          write(chain,1002) (b_fit(i,iba), i=1,3)
          call message(6,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          call message(2,1,'SOLVE_BASELINE',   &
            'Offsets from standard values :')
          write(chain,1003) (b_fit(i,iba)   &
            -stat99(i,r_istat(r_jant(iba)))   &
            +stat99(i,r_istat(r_iant(iba))), i=1,3)
          call message(6,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          write(chain,1004) ideg, (b_fit(i,iba),i=4,4+ideg)
          call message(5,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          if (ielev.gt.0) then
            if (sinele) then
              write(chain,1007) b_fit(5+ideg,iba)
              call message(5,1,'SOLVE_BASELINE',   &
                chain(1:lenc(chain)))
            else
              write(chain,1005) b_fit(5+ideg,iba)
              call message(5,1,'SOLVE_BASELINE',   &
                chain(1:lenc(chain)))
            endif
          endif
          b_rms(iba) = sigmin
        else
          write(chain,1002) (a_fit(i,iba), i=1,3)
          call message(6,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          call message(4,1,'SOLVE_BASELINE',   &
            'Offsets from standard values :')
          write(chain,1003) (a_fit(i,iba)   &
            -stat99(i,r_istat(iba)), i=1,3)
          call message(6,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          write(chain,1004) ideg, (a_fit(i,iba),i=4,4+ideg)
          call message(5,1,'SOLVE_BASELINE',   &
            chain(1:lenc(chain)))
          if (ielev.gt.0) then
            if (sinele) then
              write(chain,1008) a_fit(5+ideg,iba),   &
                a_fit(5+ideg,iba)+r_axes(iba)
              call message(5,1,'SOLVE_BASELINE',   &
                chain(1:lenc(chain)))
            else
              write(chain,1006) a_fit(5+ideg,iba),   &
                a_fit(5+ideg,iba)+r_axes(iba)
              call message(5,1,'SOLVE_BASELINE',   &
                chain(1:lenc(chain)))
            endif
          endif
          a_rms(iba) = sigmin
        endif
        f_bres(ipha) = .false.
        f_bres(iphd) = .false.
        if (ipht.ne.0) f_bres(ipht) = .false.
        if (iphe.ne.0) f_bres(iphe) = .false.
        if (search) then
          call solve_bas_sub (nd,kw,x_data(1,iphd),   &
            x_data(1,ipha), x_data(1,iph0), x_data(1,iph1),   &
            y_data(1,ipha), w_data(1,ipha),   &
            savedb, bmin,   &
            sigmin ,  memory(ipd+2*nd), memory(ipd),   &
            error, diff)
        endif
        if (i_base(ibase).gt.0) then
          call show_errors(nd, memory(ipd+2*nd),   &
            kw,b_err(1,iba))
        else
          call show_errors(nd, memory(ipd+2*nd),   &
            kw,a_err(1,iba))
        endif
100     continue
        ! Sort solutions by increasing RMS
        call gr4_trie (rms,index,ktry,error)
        if (error) return
        do i=1, 3
          call gr4_sort(solution(1,i),iwork,index,ktry)
          call gr4_sort(start(1,i),iwork,index,ktry)
        enddo
        ! Transpose into b_solution (note only b_dim best values are kept).
        i0 = ktry*(ibase-1)
        do i = 1, ktry
          if ((i0+i).le.b_dim) then
            do j = 1, 3
              b_solution(j,i0+i) = solution(i,j)
              b_start(j,i0+i) = start(i,j)
            enddo
            b_rmss(i0+i) = rms(i)
            b_dist(i0+i) = sqrt(solution(i,1)**2+   &
              solution(i,2)**2+solution(i,3)**2)
          endif
        ! End loops
        enddo
      enddo
    enddo
  enddo
  !
  ! check closure in baseline mode
  if (i_base(1).gt.0) then
    call check_baseline_closure
  endif
  !
  if (ktry.ne.ntry**3) then
    print *,'Logic error: KTRY = ',ktry,'  # NTRY = ',ntry
  endif
  !!      DIM(1) = 3
  !!      DIM(2) = KTRY
  !!      DIM(3) = N_BASE
  !!      CALL SIC_DEF_REAL('B_SOLUTION',B_SOLUTION,3,DIM,.FALSE.,ERROR)
  !!      CALL SIC_DEF_REAL('B_START',B_START,3,DIM,.FALSE.,ERROR)
  !!      DIM(1) = KTRY
  !!      DIM(2) = N_BASE
  !!      CALL SIC_DEF_REAL('B_RMS',B_RMSS,2,DIM,.FALSE.,ERROR)
  !!      CALL SIC_DEF_REAL('B_DIST',B_DIST,2,DIM,.FALSE.,ERROR)
  write(chain,*)   &
    'Intermediate results are available in variables '//   &
    ' B_SOLUTION and B_RMS; starting values in B_START'
  call message(6,1,'SOLVE_BASELINE',chain(1:lenc(chain)))
  write(chain,*)   &
    'Command PRINT BASELINE will create a procedure '//   &
    ' for updating station offsets in OBS.'
  call message(6,1,'SOLVE_BASELINE',chain(1:lenc(chain)))
  return
  !
999 error = .true.
  return
1000 format(a,1x,a,1x,a,' rms ',1pg11.4)
1001 format(' dx= ',f12.6,' dy= ',f12.6,' dz= ',f12.6)
1002 format('  X= ',f12.6,'  Y= ',f12.6,'  Z= ',f12.6)
1003 format(' DX= ',f12.6,' DY= ',f12.6,' DZ= ',f12.6)
1004 format(' Time degree ',i1,' coefficients: ',4(1pg10.3))
1005 format(' Axes offset: ',f12.6,' m')
1006 format(' Axes offset: ',f12.6,' m (rel), ',f12.6,' m (abs)')
1007 format(' Focus offset: ',f12.6,' m')
1008 format(' Focus offset: ',f12.6,' m (rel), ',f12.6,' m (abs)')
end subroutine sub_solve_baseline
!
subroutine show_errors(nd,a,kw,err)
  use gildas_def
  use gkernel_interfaces
  integer :: nd                     !
  real*8 :: a(nd,8)                 !
  real*8 :: kw                      !
  real :: err(4)                    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=132) :: chain
  integer :: i, j, ier, imax
  real :: fac
  !------------------------------------------------------------------------
  ! Code:
  fac = 1e6                    ! to convert in square millimeters
  imax = 4+ideg+ielev
  do i=1, 3
    do j=1, 3
      a(i,j) = a(i,j)*fac
    enddo
    err(i) = sqrt(a(i,i))
  enddo
  err(4) = sqrt(a(imax,imax)*fac)
  call message(2,1,'SHOW_ERRORS','RMS Errors (mm)')
  if (ielev.eq.0) then
    write(chain,1000,iostat=ier) (err(i),i=1,3)
    call message(4,1,'SHOW_ERRORS',chain(1:lenc(chain)))
  else
    write(chain,1010,iostat=ier) (err(i),i=1,4)
    call message(4,1,'SHOW_ERRORS',chain(1:lenc(chain)))
  endif
  call message(2,1,'SHOW_ERRORS','Variances (mm2)')
  write(chain,1001) a(1,1), a(2,2), a(3,3)
  call message(2,1,'SHOW_ERRORS',chain(1:lenc(chain)))
  write(chain,1002) a(1,2), a(1,3), a(2,3)
  call message(2,1,'SHOW_ERRORS',chain(1:lenc(chain)))
  !
1000 format('rx= ',1pg12.5,' ry= ',1pg12.5,' rz= ',1pg12.5)
1010 format('rx= ',1pg12.5,' ry= ',1pg12.5,' rz= ',1pg12.5,   &
    ' rd= ',1pg12.5)
1001 format('xx= ',1pg12.5,' yy= ',1pg12.5,' zz= ',1pg12.5)
1002 format('xy= ',1pg12.5,' xz= ',1pg12.5,' yz= ',1pg12.5)
end subroutine show_errors
!
subroutine resid_baseline(line,error)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy
  !------------------------------------------------------------------------
  ! Code:
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  call sub_resid_baseline(m_data,m_boxes,memory(ipx),   &
    memory(ipy),line,error)
end subroutine resid_baseline
!
subroutine sub_resid_baseline(md,mb,x_data,y_data,line,error)
  use gildas_def
  use classic_api  
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_stations.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: mvar, j, nd, ipha, iphd, iband, ibase
  integer :: ich, i, iant, k, base, ipht, ix, iy, ib, iphe
  parameter (mvar=8)
  real*4 :: a(mvar), b, kw, cd
  real*4 :: f_to_k                ! (2*PI/CLIGHT)*1D6
  parameter (f_to_k = 2.0*pis/299792458.e-6)
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Loop on : baselines, bands, subbs (La Triple Boucle)
  do ibase = 1, n_base
    do ich = 1, n_subb
      do iband = 1, n_band
        if (i_band(iband).eq.1) then
          kw = (r_flo1+r_fif1)*f_to_k
        elseif (i_band(iband).eq.2) then
          kw = (r_flo1-r_fif1)*f_to_k
        elseif (i_band(iband).eq.3) then
          kw = r_flo1*f_to_k
        elseif (i_band(iband).eq.4) then !USB
          kw = -2*r_isb*r_fif1*f_to_k
        else
          goto 100
        endif
        ipha = 0
        iphd = 0
        ipht = 0
        iphe = 0
        do ib = 1, n_boxes
          if (ibase.eq.k_base(ib) .and. ich.eq.k_subb(ib)   &
            .and. iband.eq.k_band(ib)) then
            ix = i_x(k_x(ib))
            iy = i_y(k_y(ib))
            if (iy.eq.xy_phase) then
              if (ix.eq.xy_hour) then
                ipha = ib
              elseif (ix.eq.xy_decli) then
                iphd = ib
              elseif (ix.eq.xy_time) then
                ipht = ib
              elseif (ix.eq.xy_eleva) then
                iphe = ib
              endif
            endif
          endif
        enddo
        if (ideg.gt.0 .and. ipht.eq.0) then
          write(6,*) 'E-RESIDUALS, Time plot needed'
          goto 999
        endif
        if (ielev.gt.0 .and. iphe.eq.0) then
          write(6,*) 'E-RESIDUALS, Elevation plot needed'
          goto 999
        endif
        nd = min(n_data(ipha),n_data(iphd))
        do i=1, nd
          cd = cos(x_data(i,iphd)*pis/180.)
          a(1) = + cd * cos(x_data(i,ipha)*pis/12.)
          a(2) = - cd * sin(x_data(i,ipha)*pis/12.)
            a(3) = + cos(x_data(i,iphd)*pis/180.)
            a(3) = + sin(x_data(i,iphd)*pis/180.)
          a(4) = 1.0
          if (ipht.ne.0.and.ideg.gt.0) then
            do k=1, ideg
              a(4+k) = (x_data(i,ipht)-x_data(1,ipht))**k
            enddo
          endif
          if (iphe.ne.0) then
            if (sinele) then
              a(4+ideg+1) = sin(x_data(i,iphe)*pis/180.)
            else
              a(4+ideg+1) = cos(x_data(i,iphe)*pis/180.)
            endif
          endif
          b = 0
          if (i_base(1).gt.0) then
            do j=1, 3
              b = b + (b_fit(j,i_base(ibase))   &
                -r_bas(j,i_base(ibase)))*a(j)
            enddo
            if (iphe.ne.0) then
              b = b +   &
                a(4+ideg+1) * b_fit(4+ideg+1,i_base(ibase))
            endif
            if (.not.degrees) then
              b = b * kw
            else
              b = b * kw * 180./pis
            endif
            do k=0, ideg
              b = b + a(4+k) * b_fit(4+k,i_base(ibase))
            enddo
          else
            iant = abs(i_base(ibase))
            do j=1,3
              b = b+(a_fit(j,iant)-stat99(j,r_istat(1)) )*a(j)
              if (iant.ne.1) b=b-r_bas(j,base(1,iant))*a(j)
            enddo
            if (iphe.ne.0) then
              b = b +   &
                a(4+ideg+1) * a_fit(4+ideg+1,iant)
            endif
            if (.not.degrees) then
              b = b * kw
            else
              b = b * kw * 180./pis
            endif
            do k=0, ideg
              b = b + a(4+k) * a_fit(4+k,iant)
            enddo
          endif
          if (.not.f_bres(ipha)) then
            y_data(i,ipha) = y_data(i,ipha) - b
          endif
          if (.not.f_bres(iphd)) then
            y_data(i,iphd) = y_data(i,iphd) - b
          endif
          if (ipht.ne.0 .and. .not.f_bres(ipht)) then
            y_data(i,ipht) = y_data(i,ipht) - b
          endif
          if (iphe.ne.0 .and. .not.f_bres(iphe)) then
            y_data(i,iphe) = y_data(i,iphe) - b
          endif
        enddo
        f_bres(ipha) = .true.
        f_bres(iphd) = .true.
        if (ipht.ne.0) f_bres(ipht) = .true.
        if (iphe.ne.0) f_bres(iphe) = .true.
100     continue
      enddo
    enddo
  enddo
  !
  ! Process phases
  call reset_phases
  !
  ! Redimension variables
  call resetvar(error)
  if (error) goto 999
  clear = .true.
  call sub_plot('All',.false.,.false.,0,error)
  return
  !
999 error = .true.
  return
end subroutine sub_resid_baseline
!
subroutine check_baseline_closure
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_number.inc'
  ! Local
  real*8 :: close(3)
  integer :: wside(3), w, j, itri, ib, iside, ntri
  character(len=132) :: chain
  integer :: lctri
  ! Data
  data wside /1,-1,1/
  ! Code:
  ntri = r_nant*(r_nant-1)*(r_nant-2)/6
  if (ntri.le.0) return
  lctri = lenc(ctri(1))
  do itri=1, ntri
    do j=1, 3
      close(j) = 0
    enddo
    w = 0
    do iside=1, 3
      ib = bastri(iside,itri)
      if (f_bfit(ib)) then
        do j=1,3
          close(j) = close(j)+wside(iside)*b_fit(j,ib)
        enddo
        w = w + 1
      endif
    enddo
    if (w.ge.3) then
      write (chain,'(3a,3f12.3)') 'Closure ',   &
        ctri(itri)(1:lctri),' (mm): ',(close(j)*1000.,j=1,3)
      call message(6,1,'SOLVE_BASELINE',   &
        chain(1:lenc(chain)))
    endif
  enddo
  return
end subroutine check_baseline_closure
!
subroutine solve_bas_sub (nd,kw,x_dec,   &
    x_ha, x_tim, x_elev, y_pha,   &
    w, boff, bmes, sig, a, b, error, diff)
  use gildas_def
  use gkernel_interfaces
  integer, parameter :: mvar=8
  integer :: nd                     !
  real*8 :: kw                      !
  real :: x_dec(nd)                 !
  real :: x_ha(nd)                  !
  real :: x_tim(nd)                 !
  real :: x_elev(nd)                !
  real :: y_pha(nd)                 !
  real :: w(nd)                     !
  real :: boff(3)                   !
  real :: bmes(mvar)                !
  real :: sig                       !
  real*8 :: a(nd,mvar)              !
  real*8 :: b(nd)                   !
  logical :: error                  !
  logical :: diff                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: nvar, i, j, k, ndd, ii
  real*8 :: sigma, cd
  logical :: first
  ! Data
  data first /.true./, sigma/0.0d0/
  save first,sigma
  !------------------------------------------------------------------------
  ! Code:
  if (first) then
    call sic_def_dble ('SIGMA',sigma,0,1,.false.,error)
    first = .false.
  endif
  nvar = 4 + ideg + ielev
  if (nd.lt.nvar) then
    call message(8,4,'SOLVE_BAS_SUB','Too few data points')
    error = .true.
    return
  endif
  i = 0
  do ii=1, nd
    if (w(ii).gt.0) then
      i = i+1
      cd = cos(x_dec(ii)*pis/180.)
      a(i,1) = + cd * cos(x_ha(ii)*pis/12.)
      a(i,2) = - cd * sin(x_ha(ii)*pis/12.)
      a(i,3) = + sin(x_dec(ii)*pis/180.)
      a(i,4) = 1
      if (ideg.gt.0) then
        do k=1, ideg
          a(i,4+k) = (x_tim(ii)-x_tim(1))**k
        enddo
      endif
      if (ielev.gt.0) then
        if (sinele) then
          a(i,4+ideg+1) = sin(x_elev(ii)*pis/180.)
        else
          a(i,4+ideg+1) = cos(x_elev(ii)*pis/180.)
        endif
      endif
      if (.not.degrees) then
        b(i) = y_pha(ii)/kw
      else
        b(i) = y_pha(ii)/180.*pis/kw
      endif
      do j=1, 3
        b(i) = b(i)-boff(j)*a(i,j)
      enddo
      do while ((b(i)*kw).le.-pis)
        b(i) = b(i)+2*pis/kw
      enddo
      do while ((b(i)*kw).gt.pis)
        b(i) = b(i)-2*pis/kw
      enddo
      if (diff .and. i.gt.1) then
        do k=1, 4
          a(i-1,k) = a(i-1,k)-a(i,k)
        enddo
        b(i-1) = b(i-1)-b(i)
        do while ((b(i-1)*kw).le.-pis)
          b(i-1) = b(i-1)+2*pis/kw
        enddo
        do while ((b(i-1)*kw).gt.pis)
          b(i-1) = b(i-1)-2*pis/kw
        enddo
      endif
    endif
  enddo
  ndd = i
  if (diff) then
    ndd = i-1
    nvar = 3
  endif
  !
  ! Call fitting routine
  call mth_fitlin ('SOLVE_BASE',ndd,nvar,a,b,nd,sigma)
  ! display results
  if (.not.degrees) then
    sig = sigma*kw
  else
    sig = sigma*kw*180./pis
  endif
  do i=1, 3
    bmes(i) = b(i)+boff(i)
  enddo
  if (.not.degrees) then
    do i=4, 4+ideg+ielev
      bmes(i) = b(i)*kw        ! in rad, rad/h, rad/h**2,  ..
    enddo
  else
    do i=4, 4+ideg+ielev
      bmes(i) = b(i)*kw*180./pis   ! in deg, deg/h, deg/h**2 ...
    enddo
  endif
  if (ielev.gt.0) then
    bmes(5+ideg) = b(5+ideg)   ! in meters
  endif
  return
end subroutine solve_bas_sub
