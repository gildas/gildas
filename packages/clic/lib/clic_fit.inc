      INTEGER NTOT,NVAR
      PARAMETER (NTOT=24, NVAR=20)
!
      REAL*8 X(NVAR), XT(NVAR), DIRIN(NVAR)
      REAL*8 U(NTOT), WERR(NTOT), ALIM(NTOT), BLIM(NTOT)
      REAL*8 V(NVAR,NVAR)
      REAL*8 XS(NVAR), XTS(NVAR), WTS(NVAR)
      REAL*8 Y(NVAR+1)
      REAL*8 G(NTOT), G2(NTOT)
      REAL*8 PSTAR(NVAR), PSTST(NVAR), PBAR(NVAR), PRHO(NVAR)
      REAL*8 P(NVAR,NVAR), VT(NVAR,NVAR)
      REAL*8 EPSI, APSI, VTEST
      REAL*8 AMIN, UP, SIGMA
!
      INTEGER MAXINT, NPAR
      INTEGER MAXEXT, NU, LCODE(NTOT), LCORSP(NTOT), LIMSET
      INTEGER IPFIX(NVAR),NPFIX
      INTEGER JH, JL
      INTEGER  ISYSRD ,ISYSWR ,ISYSPU, ISW(7), NBLOCK
      INTEGER NSTEPQ, NFCN, NFCNMX
      INTEGER NEWMIN, ITAUR
!
      LOGICAL BAVARD
!
      COMMON /FITCOM/ X,XT,DIRIN, U,WERR,ALIM,BLIM, V,                  &
     &XS,XTS,WTS, Y, G,G2, P,PSTAR,PSTST,PBAR,PRHO,                     &
     &VT, EPSI,APSI,VTEST, AMIN,UP,SIGMA,                               &
     &MAXINT,NPAR,                                                      &
     &MAXEXT, NU, LCODE, LCORSP, LIMSET,                                &
     &IPFIX,NPFIX,                                                      &
     &JH,JL,                                                            &
     &ISYSRD,ISYSWR,ISYSPU,ISW,NBLOCK,                                  &
     &NSTEPQ,NFCN,NFCNMX,                                               &
     &NEWMIN,ITAUR, BAVARD
!
      SAVE /FITCOM/
