      INTEGER V_UNKNOWN,V_LSR,V_HELIO,V_EARTH,V_NULL
! Parameter specific to CLIC as defined in OBS
      PARAMETER(V_UNKNOWN = 0)       ! Unsupported referential (planetary...)
      PARAMETER(V_LSR = 1)           ! LSR velocity
      PARAMETER(V_HELIO = 2)         ! Heliocentric velocity
      PARAMETER(V_EARTH = 3)         ! Earth
      PARAMETER(V_NULL = 4)          ! No doppler tracking at all
