!------------------------------------------------------------------------
! FLAG.INC
!------------------------------------------------------------------------
      INTEGER MAF, MBF, MSF, FATAL
      PARAMETER (MAF=32, MBF=32, MSF=2*136, FATAL=25)
      CHARACTER AF(MAF)*8, BF(MBF)*8, SF(MSF)*8
!
      COMMON /FLAGS/ AF, BF, SF
