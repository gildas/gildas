subroutine init_alma_fits(unit,fich,error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Initialise an ALMA FITS file.
  !---------------------------------------------------------------------
  integer :: unit                   !
  character(len=*) :: fich          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  character(len=20) :: version
  !
  integer :: status, blocksize, bitpix, naxis, naxes(2)
  logical :: simple, extend
  !------------------------------------------------------------------------
  include 'clic_version.inc'
  !
  status = 0
  !  Get an unused Logical Unit Number to use to open the FITS file.
  if (unit.eq.-999) then
    call ftgiou(unit,status)
  endif
  if (status .gt. 0) goto 99
  !  Create the new empty FITS file.  The blocksize parameter is a
  !  historical artifact and the value is ignored by FITSIO.
  blocksize = 1
  call ftinit(unit, fich, blocksize, status)
  if (status .gt. 0) goto 99
  !  Initialize parameters about the FITS image.
  simple=.true.
  bitpix=32
  naxis=2
  naxes(1) = 0
  naxes(2) = 0
  extend=.true.
  !  Write the required header keywords to the file
  call ftphpr(unit,simple,bitpix,naxis,naxes,0,0,extend,status)
  if (status .gt. 0) goto 99
  call ftpcom(unit,   &
    'This file conforms to the ALMA-TestInterferometer standard',   &
    status)
  if (status .gt. 0) goto 99
  call ftpcom(unit,   &
    'v1.0, 2001-07-03 see Lucas and Glendenning, 2001',status)
  call ftpcom(unit,   &
    'http://www.alma.nrao.edu/development/computing/docs/memos/',   &
    status)
  !
  if (status .gt. 0) goto 99
  !      CALL FTPKYJ(UNIT,'PCOUNT',0,'Size of special data area',STATUS)
  !      IF (STATUS .GT. 0) GOTO 99
  !      CALL FTPKYJ(UNIT,'GCOUNT',0,'Data groups',STATUS)
  !      CALL FTPKYL(UNIT,'GROUPS',.TRUE.,'...',STATUS)
  call ftpkys(unit,'ORIGIN','ALMA'//version,   &
    'Organization or Institution',status)
  call ftpkys(unit,'CREATOR','CLIC '//version,   &
    'Program name and version',status)
  call ftpkys(unit,'TELESCOP','(simulated)',   &
    'Telescope name',status)
  if (status .gt. 0) goto 99
  return
99 call printerror('INIT_ALMA_FITS',status)
  error = .true.
  return
end subroutine init_alma_fits
