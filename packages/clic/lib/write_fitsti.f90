subroutine write_fitsti(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! Support WRITE file /DATA /NOCAL
  ! Write a FITS file in ALMA-TI FITS format
  ! write the current index
  !     file   character     file name
  !     /DATA     option        write all data table extensions
  !     /NOCAL    option        write data uncalibrated (not multiplied by Tsys)
  !     /HOLODATA option        write data table in holography format
  !                iant  ref      scanned antennaand reference antenna
  !     /ANTENNAS  a1 a2 a3     use only a subarray ...
  !     /TELESCOPE name         telescope value
  !     /SIMULATE  list        simulate data for ATF
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_number.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: data_in, ip_data
  character(len=132) :: fich, fich2, argum, argum2
  character(len=2) :: c2
  character(len=12) :: telarg
  integer :: msimvocab
  parameter (msimvocab=2)
  character(len=12) :: arg,simvocab(msimvocab),simkw
  logical :: end, data, nocal, holodata
  logical :: telesc, dosubant, switch, simpolar
  integer :: nch, unit, ndata, status, ib, kfich, maxhdu, num
  integer :: larg, larg2, holoant, holoref, holobase, nsubant, subant(mnant)
  integer :: i, ntch, narg, nk, ier
  parameter (maxhdu=950)
  save unit
  data unit/-999/
  data simvocab/'SWITCH','POLARIZATION'/
  !------------------------------------------------------------------------
  ! Code:
  dosubant = .false.
  switch = .false.
  simpolar = .false.
  status = 0
  data = sic_present(1,0)
  nocal = sic_present(2,0)
  holodata = sic_present(3,0)
  telesc = sic_present(5,0)
  ! get ATF simulation parameters
  if (sic_present(6,0)) then
    i = 1
    do while (i.le.sic_narg(6))
      call sic_ke(line,6,i,arg,narg,.true.,error)
      if (error) return
      call sic_ambigs('WRITE /SIMULATE',arg,simkw,nk,   &
        simvocab,msimvocab,error)
      if (error) return
      i = i+1
      if (simkw.eq.'SWITCH') then
        switch = .true.
      elseif (simkw.eq.'POLARIZATION') then
        simpolar = .true.
      endif
    enddo
  endif
  !
  if (holodata) then
    nsubant = 1
    call sic_i4(line,3,1,subant(1),.false.,error)
    holoant = subant(1)
    call sic_i4(line,3,2,holoref,.false.,error)
    print *, 'holography antenna ',holoant, ' ref ',holoref
    holobase = basant (holoref,holoant)
    print *, 'baseline ',holobase
    dosubant = .true.
  endif
  ! /ANTENNA
  if (sic_present(4,0)) then
    nsubant = sic_narg(4)
    do i =1, nsubant
      call sic_i4(line,4,i,subant(i),.false.,error)
      if (subant(i).le.0 .or. subant(i).gt.mnant) then
        call message(8,3,'WRITE_FITSTI',   &
          'Wrong /ANTENNA argument')
      endif
    enddo
    dosubant = .true.
  endif
  ! /TELESCOPE
  if (telesc) then
    call sic_ch(line,5,1,telarg,narg,.true.,error)
  endif
  !
  ! Verify if any input file
  call check_input_file(error)
  if (error) return
  !
  call check_index(error)
  if (error) return
  ! Get file name
  argum = 'clicFile'
  call sic_ch(line,0,1,argum,nch,.false.,error)
  if (error) return
  call sic_parsef(argum,fich,' ','.fits')
  kfich = 0
  !
  ! Now loop on current index.
  !
  ! Loop on current index
  call get_first(.true.,error)
  if (error) goto 99
  if (.not.dosubant) then
    nsubant = r_nant
    do i=1, nsubant
      subant(i) = i
    enddo
  endif
  call init_alma_fits(unit,fich,error)
  if (error) goto 99
  end = .false.
  do while (.not.end)
    !
    ! Get storage and data
    call get_data (ndata,data_in,error)
    if (error) goto 99
    ip_data = gag_pointer(data_in,memory)
    ! Write the DATAPAR table.
    !
    ! Specific Holography setup:
    if (holodata) then
      ntch = r_nband
      r_nband = 1
      r_lband = 1
      r_lnsb = 1
      r_nsb = 2
    endif
    if (telesc) r_teles = telarg
    call ftthdu(unit,num,status)
    if (num .gt. maxhdu) then
      call ftclos(unit, status)
      kfich = kfich+1
      write(c2,'(i2.2)') kfich
      larg = lenc(argum)
      argum2 = argum(1:larg)//'-'//c2
      call sic_parsef(argum2,fich2,' ','.fits')
      larg = lenc(fich)
      larg2 = lenc(fich2)
      ier = gag_system('mv '//fich(1:larg)//' '//fich2(1:larg2)//char(0))
      call init_alma_fits(unit,fich,error)
      if (error) goto 99
    endif
    call write_fits_datapar (unit,  nsubant, subant,   &
      ndata,memory(ip_data), holodata, switch, simpolar, error)
    if (error) goto 99
    ! Write the CALIBRATION table.
    call write_fits_calibr (unit, nsubant, subant, nocal,   &
      simpolar, error)
    if (error) goto 99
    ! write gain calibration
    if (r_aic.ne.0 .and. .not.nocal) then
      call write_fits_gain (unit, nsubant, subant, error)
    endif
    ! Write passband calibration
    if (r_abpc.ne.0 .and. .not.nocal) then
      call write_fits_passband (unit, nsubant, subant,  error)
    endif
    if (data) then
      ! Write the HOLODATA for project HOLO
      if (holodata) then
        ib = 1                 ! only one subband ...
        if (r_lmode.eq.1) then
          call write_fits_holodata   &
            (unit, ib, ntch,  holobase, ndata,   &
            memory(ip_data), error)
          if (error) goto 99
        endif
      else
        do ib = 1, r_nband
          ! Write the CORRDATA/AUTODATA table(s) for `continuum' .
          call write_fits_data   &
            (unit, ib, 'CT', nsubant, subant,   &
            ndata, memory(ip_data),switch,error)
          if (error) goto 99
          ! Write the CORRDATA/AUTODATA table(s) for `continuum/average' .
          call write_fits_data   &
            (unit, ib, 'CA', nsubant, subant,   &
            ndata, memory(ip_data),switch,error)
          if (error) goto 99
          ! Write the CORRDATA/AUTODATA table(s) for `line/average' .
          call write_fits_data   &
            (unit, ib, 'LA', nsubant, subant,   &
            ndata, memory(ip_data),switch,error)
          if (error) goto 99
        enddo
      endif
    endif
    !
    if (sic_ctrlc()) then
      error = .true.
      goto 99
    endif
    call get_next(end,error)
    if (error) goto 99
    if (.not.dosubant) then
      nsubant = r_nant
      do i=1, nsubant
        subant(i) = i
      enddo
    endif
  enddo
99 status = 0
  call ftclos(unit, status)
  call ftfiou(unit, status)
  if (kfich.ne.0) then
    kfich = kfich+1
    write(c2,'(i2.2)') kfich
    larg = lenc(argum)
    argum2 = argum(1:larg)//'-'//c2
    call sic_parsef(argum2,fich2,' ','.fits')
    larg = lenc(fich)
    larg2 = lenc(fich2)
    ier =  gag_system('mv '//fich(1:larg)//' '//fich2(1:larg2)//char(0))
  endif
  if (status .gt. 0) then
    call printerror('CLIC_FITS_TI',status)
    error = .true.
  endif
end subroutine write_fitsti
