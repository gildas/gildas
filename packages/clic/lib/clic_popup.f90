subroutine clic_popup (line,error)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! CLIC
  !     Pop-up the box pointed out by the cursor, and
  !     then call the cursor again to get "local" user coordinates.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  ! Local
  character(len=1) :: code,argum
  integer :: i, ib, l, ko, nch, jb
  real*4 :: gx1(mbox), gx2(mbox), gy1(mbox), gy2(mbox)
  real*4 :: x4, y4
  real*8 :: xu, yu
  !
  ! Get box corners
  do i=1, n_boxes
    l = lenc(c_setbox(i))
    call gr_exec  (c_setbox(i)(1:l))
    call sic_get_real ('BOX_XMIN',gx1(i),error)
    call sic_get_real ('BOX_XMAX',gx2(i),error)
    call sic_get_real ('BOX_YMIN',gy1(i),error)
    call sic_get_real ('BOX_YMAX',gy2(i),error)
  enddo
  !
  call gr_execl('CHANGE DIRECTORY')
  do while (.true.)
    error = .false.
    call gr_curs (xu, yu, x4, y4, code)
    if (code.eq.'E' .or. code.eq.'e') then
      call resetvar(error)
      return
    endif
    ib = 0
    jb = 0
    do while (ib.eq.0)
      jb = jb+1
      if (jb.gt.n_boxes) then
        ib = -1
      else
        if ((x4-gx1(jb))*(x4-gx2(jb)) .le. 0) then
          if ((y4-gy1(jb))*(y4-gy2(jb)) .le. 0) then
            ib = jb
          endif
        endif
      endif
    enddo
    !
    if (ib.lt.0) then
      write(6,*) ' Cursor (',x4,',',y4,') is not in any box'
    else
      call sub_popup(ib)
    endif
  enddo
end subroutine clic_popup
!
subroutine sub_popup(ib)
  integer :: ib                     !
  ! Local
  character(len=4) :: chain
  !
  write(chain,'(I4.4)') ib
  call gr_execl('CHANGE DIRECTORY BOX'//chain)
  call gr_execl('CREATE WINDOW')
  call gr_exec1('DRAW')
  call gr_execl('DESTROY WINDOW')
  call gr_execl('CHANGE DIRECTORY')
end subroutine sub_popup
