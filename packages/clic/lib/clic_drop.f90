subroutine clic_drop(line,error)
  use gkernel_interfaces
  use clic_index
  !---------------------------------------------------------------------
  ! CLIC 	Support routine for command
  !	DROP Number [Version]
  !	Deletes an observation from current index
  ! Arguments :
  !	LINE	C*(*)	Command line		Input
  ! (5-mar-1985)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  logical :: lst,found
  integer :: versio
  character(len=20) :: nombre
  integer :: tver
  integer(kind=entry_length) :: i, number
  !------------------------------------------------------------------------
  ! Code:
  call check_index(error)
  if (error) return
  if (sic_present(0,1)) then
    call sic_i8(line,0,1,number,.true.,error)
    if (error) return
    versio = 0
    call sic_i4(line,0,2,versio,.false.,error)
    if (error) return
    lst = versio .eq. 0
  else
    number=r_num
    versio=abs(r_ver)
    lst = .false.
  endif
  found = .false.
  do 200 i=1,cxnext-1
    if (.not.found) then
      if (lst) then
        found = cx_num(i).eq.number .and. cx_ver(i).ge.0
      else
        tver = abs(cx_ver(i))
        found = cx_num(i).eq.number .and. tver.eq.versio
      endif
      if (found) then
        if (knext .ge. i) knext = knext-1
      endif
    else
      cx_ind(i-1)  = cx_ind(i)
      cx_num(i-1)  = cx_num(i)
      cx_ver(i-1)  = cx_ver(i)
      cx_bloc(i-1) = cx_bloc(i)
      cx_word(i-1) = cx_word(i)
    endif
200 continue
  if (found) then
    cxnext = cxnext - 1
  else
    write(nombre,1001) number,versio
    call message(8,1,'DROP',nombre//' not in current index')
    error = .true.
    return
  endif
  call index_variable
  return
1001 format(i10,';',i6)
end subroutine clic_drop
