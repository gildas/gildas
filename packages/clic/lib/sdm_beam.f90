subroutine write_sdm_Beam(error)
  !---------------------------------------------------------------------
  ! Write the Beam SDM table.
  !     int sdm_addBeamRow ()
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  integer sdm_addBeamRow, ireturn
  !---------------------------------------------------------------------
  !     This table is entirely *TBD*.
  ireturn = sdm_addBeamRow()
  if (ireturn.lt.0) then
    call message(8,3,'write_sdm_Beam'   &
      ,'Error in sdm_addBeamRow')
    goto 99
  endif
  beam_Id = ireturn
  return
  !
99 error = .true.
end subroutine write_sdm_Beam
!---------------------------------------------------------------------
