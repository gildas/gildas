subroutine write_sdm_SourceParameter(error)
  !---------------------------------------------------------------------
  ! Write the SourceParameter SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_SourceParameter
  use sdm_Source
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(SourceParameterRow) :: spRow
  type(SourceParameterKey) :: spKey
  type(SourceOpt) :: sOpt
  type(SourceKey) :: sKey
  integer :: ic, isb, ilc, numFreq, numStokes, numDep
  character sdmTable*16
  parameter (sdmTable='SourceParameter')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  spRow%numFreq = 1
  spRow%numStokes = 1
  !!spRow%numDep = 1
  call allocSourceParameterRow(spRow, error)
  spRow%flux = r_flux
  spRow%fluxErr = 0
  spRow%frequency = r_restf * 1d6
  ! need to be refined at some point
  spRow%frequencyInterval = 500d6
  spRow%stokesParameter = 0
  ! OPTIONAL IN SS2 ...      spRow%size = 0
  ! OPTIONAL IN SS2 ...      spRow%sizeErr = 0
  ! OPTIONAL IN SS2 ...      spRow%positionAngle = 0
  ! OPTIONAL IN SS2 ...      spRow%positionAngleErr = 0
  ! OPTIONAL IN SS2 ...      spRow%depSourceParameterId = -1
  !
  spKey%sourceId = source_Id
  spKey%timeInterval = ArrayTimeInterval(time_interval(1)   &
    ,time_interval(2))
  call addSourceParameterRow(spKey, spRow, error)
  if (error) return
  SourceParameter_Id  = spKey%sourceParameterId
  !
  ! Add this in SOurce Table
  !
  do ic = 1, r_lband
    do isb = 1, 2
      do ilc = 1, 2
        sKey%sourceId = source_Id
        sKey%spectralWindowId = spectralWindow_Id(isb,ic,ilc)
        !
        ! Use time_start for SOurce rows (to avoid writing unnecesasary rows)
        sKey%timeInterval = ArrayTimeInterval(time_start(1),   &
          time_start(2))
        sOpt%sourceParameterId = sourceParameter_Id
        call addSourceSourceParameterId(sKey, sOpt, error)
        if (error) return
      enddo
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_SourceParameter
!---------------------------------------------------------------------
!!! subroutine allocSourceParameterRow(row, numFreq, numStokes,   &
!!!     numDep, error)
!!!   !---------------------------------------------------------------------
!!!   !     integer, allocatable :: stokesParameter(:)
!!!   !     real*8, allocatable :: flux(:,:)
!!!   !     real*8, allocatable :: size(:,:)
!!!   !     real*8, allocatable :: positionAngle(:)
!!!   !     real*8, allocatable :: frequency(:)
!!!   !     real*8, allocatable :: frequencyInterval(:)
!!!   !     real*8, allocatable :: fluxErr(:,:)
!!!   !     real*8, allocatable :: sizeErr(:,:)
!!!   !     real*8, allocatable :: positionAngleErr(:)
!!!   !     integer, allocatable :: depSourceParameterId(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_SourceParameter
!!!   type(SourceParameterRow) :: row
!!!   integer :: ier, numFreq, numStokes, numDep
!!!   logical :: error
!!!   character, parameter :: sdmTable*16 = 'SourceParameter'
!!!   !
!!!   ! stokeParameter(numStokes)
!!!   if (allocated(row%stokesParameter)) then
!!!     deallocate(row%stokesParameter, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%stokesParameter(numStokes), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! flux(numStokes, numFreq)
!!!   if (allocated(row%flux)) then
!!!     deallocate(row%flux, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%flux(numStokes, numFreq), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! size(2, numFreq)
!!!   ! OPTIONAL IN SS2 ...      if (allocated(row%size)) then
!!!   ! OPTIONAL IN SS2 ...         deallocate(row%size, stat=ier)
!!!   ! OPTIONAL IN SS2 ...         if (ier.ne.0) goto 98
!!!   ! OPTIONAL IN SS2 ...      endif
!!!   ! OPTIONAL IN SS2 ...      allocate(row%size(2, numFreq), stat=ier)
!!!   ! OPTIONAL IN SS2 ...      if (ier.ne.0) goto 99
!!!   !
!!!   ! positionAngle(numFreq)
!!!   ! OPTIONAL IN SS2 ...      if (allocated(row%positionAngle)) then
!!!   ! OPTIONAL IN SS2 ...         deallocate(row%positionAngle, stat=ier)
!!!   ! OPTIONAL IN SS2 ...         if (ier.ne.0) goto 98
!!!   ! OPTIONAL IN SS2 ...      endif
!!!   ! OPTIONAL IN SS2 ...      allocate(row%positionAngle(numFreq), stat=ier)
!!!   ! OPTIONAL IN SS2 ...      if (ier.ne.0) goto 99
!!!   !
!!!   ! frequency(numFreq)
!!!   if (allocated(row%frequency)) then
!!!     deallocate(row%frequency, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%frequency(numFreq), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! frequencyInterval(numFreq)
!!!   if (allocated(row%frequencyInterval)) then
!!!     deallocate(row%frequencyInterval, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%frequencyInterval(numFreq), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! fluxErr(numStokes, numFreq)
!!!   if (allocated(row%fluxErr)) then
!!!     deallocate(row%fluxErr, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%fluxErr(numStokes, numFreq), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! sizeErr(2, numFreq)
!!!   ! OPTIONAL IN SS2 ...      if (allocated(row%sizeErr)) then
!!!   ! OPTIONAL IN SS2 ...         deallocate(row%sizeErr, stat=ier)
!!!   ! OPTIONAL IN SS2 ...         if (ier.ne.0) goto 98
!!!   ! OPTIONAL IN SS2 ...      endif
!!!   ! OPTIONAL IN SS2 ...      allocate(row%sizeErr(2, numFreq), stat=ier)
!!!   ! OPTIONAL IN SS2 ...      if (ier.ne.0) goto 99
!!!   !
!!!   ! positionAngleErr(numFreq)
!!!   ! OPTIONAL IN SS2 ...      if (allocated(row%positionAngleErr)) then
!!!   ! OPTIONAL IN SS2 ...         deallocate(row%positionAngleErr, stat=ier)
!!!   ! OPTIONAL IN SS2 ...         if (ier.ne.0) goto 98
!!!   ! OPTIONAL IN SS2 ...      endif
!!!   ! OPTIONAL IN SS2 ...      allocate(row%positionAngleErr(numFreq), stat=ier)
!!!   ! OPTIONAL IN SS2 ...      if (ier.ne.0) goto 99
!!!   !
!!!   ! depSourceParameterId(numDep)
!!!   ! OPTIONAL IN SS2 ...      if (allocated(row%depSourceParameterId)) then
!!!   ! OPTIONAL IN SS2 ...         deallocate(row%depSourceParameterId, stat=ier)
!!!   ! OPTIONAL IN SS2 ...         if (ier.ne.0) goto 98
!!!   ! OPTIONAL IN SS2 ...      endif
!!!   ! OPTIONAL IN SS2 ...      allocate(row%depSourceParameterId(numDep), stat=ier)
!!!   ! OPTIONAL IN SS2 ...      if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!!! !---------------------------------------------------------------------
