subroutine clic_derive(gg,gg2,fcn)
  !---------------------------------------------------------------------
  ! SAS	Internal routine
  !	Calculates the first derivatives of FCN (GG),
  !	either by finite differences or by transforming the user-
  !	supplied derivatives to internal coordinates,
  !	according to whether ISW(3) is zero or one.
  !	if ISW(3) = 0, an error estimate GG2  is available
  !
  !	External routine FCN added to allow use of various
  !	minimization functions.
  !
  ! Arguments :
  !	GG	R*8 (*)	First derivatives of function FCN	Output
  !	GG2	R*8 (*)	Error estimate for variables		Output
  !	FCN	Ext	Function to minimise			Input
  !---------------------------------------------------------------------
  real*8 ::   gg                    !
  real*8 ::   gg2                   !
  external :: fcn                   !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  iflag,i,lc
  real*8 ::  eps,xtf,fs1,fs2,dd
  real*8 ::   gy
  dimension   gg(ntot),gg2(nvar),gy(ntot)
  !
  if (isw(3) .eq. 1)  go to 100
  iflag = 4
  do 46  i=1,npar
    eps = 0.1d0 * dabs(dirin(i))
    if (isw(2) .ge. 1) eps = eps + 0.005d0*dsqrt(v(i,i)*up)
    !
    ! Precision dependent implementation
    !
    if (eps .lt.  1.0d-5*dabs(x(i)))  eps = 1.0d-5*x(i)
    xtf = x(i)
    x(i) = xtf + eps
    call clic_intoex(x)
    call fcn(npar,gy,fs1,u ,iflag)
    nfcn=nfcn+1
    x(i) = xtf - eps
    call clic_intoex(x)
    call fcn(npar,gy,fs2,u,iflag)
    nfcn=nfcn+1
    ! First derivative
    gg(i)= (fs1-fs2)/(2.0d0*eps)
    ! Error on first derivative
    gg2(i)= (fs1+fs2-2.0d0*amin)/(2.0d0*eps)
    x(i) = xtf
46 continue
  call clic_intoex(x)
  go to 200
  ! Derivatives calculated by FCN
100 do 150 i= 1, nu
    lc=lcorsp(i)
    if (lc .lt. 1)  go to 150
    if (lcode(i) .gt. 1)  go to 120
    gg(lc)=gg(i)
    go to 150
120 dd = (blim(i)-alim(i))*0.5d0 *dcos(x(lc))
    gg(lc)=gg(i)*dd
150 continue
200 return
end subroutine clic_derive
!
subroutine clic_extoin(pint)
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Transforms the external parameter values X  to internal
  !	values in the dense array PINT.   Function PINTF is used.
  ! Arguments :
  !	PINT	R*8 (2)	Dense array
  !---------------------------------------------------------------------
  real*8 ::   pint                  !
  ! Global
  real*8 ::  clic_pintf
  include 'clic_fit.inc'
  ! Local
  integer ::  i,j
  dimension  pint(2)
  !
  limset=0
  do 100  i= 1, nu
    j = lcorsp(i)
    if ( j )  100,100,50
50  pint(j) = clic_pintf(u(i),i)
100 continue
  return
end subroutine clic_extoin
!
subroutine clic_fixpar(i2,kode,ilax)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Removes parameter I2 from the internal (variable) parameter
  !	list, and arranges the rest of the list to fill the hole.
  !	If KODE=0, I2 is an external number, otherwise internal.
  !	ILAX is returned as the external number of the parameter.
  ! Arguments :
  !	I2	I	Internal or external parameter number	Input
  !	KODE	I	Indicates Internal (1) or External (0)	Input
  !	ILAX	I	External number				Output
  !---------------------------------------------------------------------
  integer ::  i2                    !
  integer ::  kode                  !
  integer ::  ilax                  !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  lc,it,ik,iq,kon,kon2,mpar,i,j
  real*8 ::   eps
  real*8 ::   v1,yy
  character(len=255) :: ch
  dimension   v1(2),yy(nvar)
  equivalence (v(1,1) , v1(1))
  !
  if (kode)  250,50,150
  ! Ext param no. specified
50 i = i2
  if (i .gt. nu)  go to 70
  if (i .lt. 1)  go to 70
60 if (lcorsp(i))  70,70,80
  ! Error return   param already fixed
70 ilax = 0
  write (ch,500)  i
  call message (2,1,'FIXPAR',ch(1:lenc(ch)))
  go to 300
80 lc = lcorsp(i)
  it = lc
  lcorsp(i) = 0
  ilax = i
  npar = npar - 1
  npfix = npfix + 1
  ipfix(npfix) = i
  xs(npfix) = x(lc)
  xts(npfix) = xt(lc)
  eps = dabs(dirin(lc)) * 10.d0
  if (isw(2) .ge. 1)  eps = eps + dsqrt(dabs(v(lc,lc))*up)
  ! Precision dependent
  if (eps .lt. 1.0d-5*dabs(x(lc)))  eps = 1.0d-5*x(lc)
  wts(npfix) = eps*0.1d0
  do 100  ik= i, nu
    if (lcorsp(ik))  100,100,85
85  lc = lcorsp(ik) - 1
    lcorsp(ik) = lc
    x(lc) = x(lc+1)
    xt(lc) = xt(lc+1)
    dirin(lc) = dirin(lc+1)
100 continue
  if (isw(2) .gt. 1)  go to 250
  isw(2) = 0
  go to 300
  ! Internal parameter number specified
150 continue
  do 200  iq= 1, nu
    if (lcorsp(iq) .ne. i2)  go to 200
    i = iq
    go to 60
200 continue
  go to 70
  ! Remove one row and one column from variance matrix
250 kon = 0
  if (npar .le. 0)  go to 300
  kon2 = 0
  mpar = npar + 1
  do 260 i= 1, mpar
260 yy(i)=v(i,it)
  do 294 i= 1, mpar
    if (i.eq.it)  go to 294
    kon2 = kon2 + 1
    do 292 j= 1, mpar
      if (j .eq. it)  go to 292
      kon = kon + 1
      v1(kon)=v(j,i) - yy(j)*yy(i)/yy(it)
292 continue
    kon = maxint*kon2
294 continue
  ! Check for well-behaved final matrix
  do 295 i= 1, npar
    if (v(i,i) .le. 0.d0)  go to 296
    do 295 j= 1, npar
      if (i .eq. j)  go to 295
      if (v(i,j)**2 .ge. v(i,i)*v(j,j))  v(i,j) = 0.d0
295 continue
  go to 300
296 isw(2) = 0
  call message(8,3,'FIXPAR',   &
    'Covariance matrix ill-conditioned, Destroyed')
300 return
500 format ('Parameter ',i3,' was not variable')
end subroutine clic_fixpar
!
subroutine clic_hesse(fcn)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Calculates the full second-derivative matrix of FCN
  !	by taking finite differences.   Includes some safeguards
  !	against non-positive-definite matrices, and it may set
  !	 off-diagonal elements to zero in attempt to force
  !	positiveness.
  ! Arguments :
  !	FCN	Ext	Function to minimise
  !---------------------------------------------------------------------
  external :: fcn                   !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  ifaut1,ifaut2,iflag,npfn,npard,mdiag,id,i,j,icyc,nparm1
  integer ::  ip1,ifail,ifix
  real*8 ::   d,xtf,yy,gy,fs1,fs2,df,chan,xti,xtj,elem,r
  dimension yy(nvar),gy(ntot)
  real*8 :: dfwant,dfzero,dfmin,dfmax
  character(len=255) :: ch
  !
  data   dfwant, dfzero,       dfmin,   dfmax   &
    / 0.01d0, 0.00000001d0, 0.001d0, 0.1d0 /
  !
  ifaut2=0
  ifaut1=0
  iflag = 4
  npfn = nfcn
  npard = npar
  !                                        . . . . . . DIAGONAL ELEMENTS .
  mdiag = 0
  do 100 id= 1, npard
    i = id + npar - npard
    d = 0.02d0* dabs(dirin(i))
    if (isw(2) .ge. 1)  d = 0.02d0* dsqrt(dabs(v(i,i))*up)
    ! Precision dependent constant
    if (d .lt.  1.0d-5 *dabs(x(i)))  d = 1.0d-5 * dabs(x(i))
    do 20 j= 1, npar
20  v(i,j) = 0.d0
    icyc = 0
40  dirin(i) = d
    xtf = x(i)
    x(i) = xtf + d
    call clic_intoex(x)
    call fcn(npar, gy, fs1, u, iflag)
    nfcn = nfcn + 1
    x(i) = xtf - d
    call clic_intoex(x)
    call fcn (npar, gy, fs2, u, iflag)
    nfcn = nfcn + 1
    x(i) = xtf
    ! Check if step sizes appropriate
    icyc = icyc + 1
    if (icyc .ge. 4)  go to 55
    df = dmax1(dabs(fs1-amin),dabs(fs2-amin))/up
    if (df .gt. dfmin)  go to 45
    if (df .gt. dfzero)  go to 50
    d = d*1000.d0
    go to 40
45  if (df .lt. dfmax)  go to 55
50  chan = dsqrt(dfwant/df)
    if (chan .lt. 0.001d0)  chan = 0.001d0
    d = d*chan
    go to 40
55  continue
    ! Get first and second derivative
    g(i) = (fs1-fs2)/(2.0d0 * d)
    g2(i) = (fs1 + fs2 - 2.0d0*amin) / d**2
    yy(i) = fs1
    if (dabs(g(i))+ dabs(g2(i)).gt. 1.0d-30)  go to 80
    ! Fix a parameter if  G = G2 = 0.0 .
    if (itaur .ge. 1)  go to 85
    isw(2) = 0
    call clic_fixpar(i, 1, ifix)
    write(ch,460) ifix, g(i), g2(i)
    call message(2,2,'HESSE',ch(1:lenc(ch)))
    if (npar .eq. 0)  mdiag = 1
    go to 100
80  if (g2(i) .gt. 1.0d-30)  go to 90
85  mdiag = 1
    ifaut2=ifaut2+1
90  v(i,i) = g2(i)
100 continue
  call clic_intoex(x)
  if (mdiag .eq. 1)  go to 390
  isw(2) = 1
  ! Off-diagonal elements
  if (npar .eq. 1)  go to 214
  nparm1 = npar - 1
  do 200 i= 1, nparm1
    ip1 = i + 1
    do 180 j= ip1, npar
      if (nfcnmx-nfcn+npfn .lt. npar)  go to 210
      xti = x(i)
      xtj = x(j)
      x(i) = xti + dirin(i)
      x(j) = xtj + dirin(j)
      call clic_intoex(x)
      call fcn(npar, gy, fs1, u, iflag)
      nfcn = nfcn + 1
      x(i) = xti
      x(j) = xtj
      elem = (fs1+amin-yy(i)-yy(j)) / (dirin(i)*dirin(j))
      if (elem**2 .lt. g2(i)*g2(j)) go to 170
      elem = 0.d0
      ifaut1=ifaut1+1
170   v(i,j) = elem
      v(j,i) = elem
180 continue
200 continue
  go to 214
210 j = j - 1
  write (ch, 490)  i,j
  call message(6,3,'HESSE',ch(1:lenc(ch)))
214 call clic_intoex(x)
  call clic_vermin(v,maxint,maxint,npar,ifail)
  if (ifail .lt. 1)  go to 222
  call message(6,3,'HESSE','Matrix inversion fails')
  ! Diagonal matrix only
216 call message(4,2,'HESSE','Only diagonal matrix produced')
  isw(2) = 1
  do 220 i= 1, npar
    do 218 j= 1, npar
218 v(i,j) = 0.d0
220 v(i,i) = 1.0d0/g2(i)
  mdiag = 0
  go to 223
222 call message(2,1,'HESSE','Second derivative matrix inverted')
  isw(2) = 2
  ! Calculate  Estimated Distance to Mimimum
223 do 225 i= 1, npar
    do 225 j= 1, npar
225 v(i,j) = 2.0d0 * v(i,j)
  sigma = 0.d0
  do 250 i= 1, npar
    if (v(i,i) .gt. 0.d0)  go to 228
    ifaut2=ifaut2+1
    mdiag = 1
228 r = 0.d0
    do 240 j= 1, npar
      if (i .eq. j)  go to 230
      if (v(i,j)**2 .lt. dabs(v(i,i)*v(j,j)))  go to 230
      ifaut1=ifaut1+1
      v(i,j) = 0.d0
      v(j,i) = 0.d0
230   continue
240 r = r + v(i,j) * g(j)
250 sigma = sigma + 0.5d0 *r *g(i)
  if (mdiag .eq. 1)  go to 390
  if (sigma .gt. 0.d0)  go to 400
  call message(4,2,'HESSE','Matrix not positive-definite')
  go to 216
390 isw(2) = 0
400 if (ifaut1.ne.0) then
    write(ch,600) ifaut1
    call message(6,3,'HESSE',ch(1:lenc(ch)))
  endif
  if (ifaut2.ne.0) then
    write(ch,610) ifaut2
    call message(6,3,'HESSE',ch(1:lenc(ch)))
  endif
  return
460 format ('Parameter',i3,' has been fixed',   &
    'First derivative ',e11.3,' Second derivative ',e11.3)
470 format (1x,'HESSE Covariance matrix not positive-definite.',   &
    'Faulty element in position',2i3)
480 format (1x,'HESSE Second derivative matrix inverted')
490 format ('Call limit. Off-diagonal elements calculated',   &
    ' only up to ',2i3)
510 format (1x,'HESSE Diagonal element',i5,' is zero or negative')
520 format (1x,'HESSE Matrix inversion fails')
540 format (1x,'HESSE Only diagonal matrix produced')
600 format (1x,'HESSE Covariance matrix not positive-definite.',i4,   &
    ' Faulty elements')
610 format (1x,'HESSE',i4,' Non-positive diagonal elements ')
end subroutine clic_hesse
!
subroutine clic_intoex(pint)
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Transforms from internal coordinates (PINT) to external
  !	parameters (U).   The minimizing routines which work in
  !	internal coordinates call this routine before calling FCN.
  ! Arguments :
  !	PINT	R*8 (2)	Dense array			Input
  !---------------------------------------------------------------------
  real*8 ::  pint                   !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer :: i,j
  real*8 ::  al
  dimension pint(1)
  !
  do 100 i= 1, nu
    j = lcorsp(i)
    if ( j )  100,100,50
50  continue
    if (lcode(i) .eq. 1)  go to 80
    al = alim(i)
    u(i) = al + 0.5d0 *(dsin(pint(j)) +1.0d0) * (blim(i) -al)
    go to 100
80  u(i) = pint(j)
100 continue
  return
end subroutine clic_intoex
!
subroutine clic_migrad(fcn,ier)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Performs a local function minimization using basically the
  !	method of DAVIDON-FLETCHER-POWELL as modified by FLETCHER
  !	Ref. -- FLETCHER, COMP.J. 13,317 (1970)   "Switching method"
  ! Arguments :
  !	FCN	Ext	Function to minimise			Input
  !	IER 	I	Error condition flag			Output
  !---------------------------------------------------------------------
  external :: fcn                   !
  integer :: ier                    !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer :: ifaut1,ifaut2,iswtr,npfn,iflag,npard,i,ntry,negg2
  integer :: id,kg,nf,ns,j,iter,npargd,matgd
  real*8 ::  gs,r,xxs,flnu,vg,vii
  real*8 ::  parn,rho2,rostop,trace,fs,d,xtf,fs1,fs2,xbeg,f
  real*8 ::  ri,gdel,denom,slam,f2,aa,bb,cc,tlam,f3,gvg,delgam,vgi
  real*8 ::  gami
  !
  ! ERROR	Logique d'erreur pour HESSE	S. Guilloteau 1-12-1983
  ! FCN external function			S. Guilloteau 6-09-1984
  !
  dimension gs(ntot) , r(nvar),xxs(nvar),   &
    flnu(nvar), vg(nvar), vii(nvar)
  real*8 :: slamin,slamax,tlamin,tlamax
  character(len=255) :: ch
  !
  data slamin,slamax,tlamin,tlamax/0.2d0, 3.0d0, 0.05d0, 6.0d0/
  !
  ifaut1=0
  ifaut2=0
  if (npar .le. 0)  return
  iswtr = isw(5) - itaur
  npfn = nfcn
  parn=npar
  rho2 = 10.d0*apsi
  ! Precision dependent constant
  rostop = 1.0d-2 * apsi
  trace=1.d0
  iflag=4
  if (isw(3) .eq. 1)  iflag = 2
  fs = amin
  go to 2
1 ifaut1=ifaut1+1
  !
  ! Step sizes dirin
2 npard = npar
  do  3 i= 1, npar
    d = 0.02d0* dabs(dirin(i))
    if (isw(2) .ge. 1)  d = 0.02d0* dsqrt(dabs(v(i,i))*up)
    ! Precision dependent
    if (d .lt.  1.0d-5 *dabs(x(i)))  d = 1.0d-5 * x(i)
3 dirin(i) = d
  !
  ! Starting gradient
  ntry = 0
4 negg2 = 0
  do 10  id= 1, npard
    i = id + npar - npard
    d = dirin(i)
    xtf = x(i)
    x(i) = xtf + d
    call clic_intoex(x)
    call fcn(npar,g,fs1,u,4)
    nfcn = nfcn + 1
    x(i) = xtf - d
    call clic_intoex(x)
    call fcn(npar,g,fs2,u,4)
    nfcn = nfcn + 1
    x(i) = xtf
    gs(i) = (fs1-fs2)/(2.0d0 * d)
    g2(i) = (fs1 + fs2 - 2.0d0*amin) / d**2
    if (g2(i) .gt. 1.0d-30)  go to 10
    !
    ! Search if G2 .LE. 0.
    ifaut1=ifaut1+1
    negg2 = negg2 + 1
    ntry = ntry + 1
    if (ntry .gt. 4)  go to 230
    d = 50.d0*dabs(dirin(i))
    xbeg = xtf
    if (gs(i) .lt. 0.d0)  dirin(i) = -dirin(i)
    kg = 0
    nf = 0
    ns = 0
5   x(i) = xtf + d
    call clic_intoex(x)
    call fcn(npar,g,f,u,4)
    nfcn = nfcn + 1
    if (f .le. amin)  go to 6
    !
    ! Failure
    if (kg .eq. 1)  go to 8
    kg = -1
    nf = nf + 1
    d = -0.4d0*d
    if (nf .lt. 10)  go to 5
    d = 1000.d0*d
    go to 7
    !
    ! Success
6   xtf = x(i)
    d = 3.0d0*d
    amin = f
    kg = 1
    ns = ns + 1
    if (ns .lt. 10)  go to 5
    if (amin .lt. fs)  go to 8
    d = 0.001d0*d
7   xtf = xbeg
    g2(i) = 1.0d0
    negg2 = negg2 - 1
8   x(i) = xtf
    dirin(i) = 0.1d0*d
    fs = amin
10 continue
  if (negg2 .ge. 1)  go to 4
  ntry = 0
  matgd = 1
  !
  ! Diagonal matrix
  if (isw(2) .gt. 1)  go to 15
11 ntry = 1
  matgd = 0
  do 13 i= 1, npar
    do 12 j= 1, npar
12  v(i,j) = 0.d0
13 v(i,i) = 2.0d0/g2(i)
  !
  ! Get sigma and set up loop
15 sigma = 0.d0
  do 18 i= 1, npar
    if (v(i,i) .le. 0.d0)  go to 11
    ri = 0.d0
    do 17 j= 1, npar
      xxs(i) = x(i)
17  ri= ri+ v(i,j) * gs(j)
18 sigma = sigma + gs(i) *ri *0.5d0
  if (sigma .ge. 0.d0)  go to 20
  ifaut1=ifaut1+1
  if (ntry.eq.0)  go to 11
  isw(2) = 0
  go to 230
20 isw(2) = 1
  iter = 0
  call clic_intoex(x)
  !
  ! Start main loop
24 continue
  gdel = 0.d0
  do 30  i=1,npar
    ri = 0.d0
    do 25 j=1,npar
25  ri = ri + v(i,j) *gs(j)
    dirin(i) = -0.5d0*ri
    gdel = gdel + dirin(i)*gs(i)
  !
  ! Linear search along -VG
30 x(i) =xxs(i) + dirin(i)
  call clic_intoex(x)
  call fcn (npar, g, f, u, 4)
  nfcn=nfcn+1
  !
  ! Quadrupole interpolation using slope GDEL
  denom = 2.0d0*(f-amin-gdel)
  if (denom .le. 0.d0)  go to 35
  slam = -gdel/denom
  if (slam .gt. slamax)  go to 35
  if (slam .lt. slamin)  slam=slamin
  go to 40
35 slam = slamax
40 if (dabs(slam-1.0d0) .lt. 0.1d0)  go to 70
  do 45 i= 1, npar
45 x(i) =xxs(i) + slam*dirin(i)
  call clic_intoex(x)
  call fcn(npar,g,f2,u,4)
  nfcn = nfcn + 1
  !
  ! Quadrupole interpolation using 3 points
  aa = fs/slam
  bb = f/(1.0d0-slam)
  cc = f2/ (slam*(slam-1.0d0))
  denom = 2.0d0*(aa+bb+cc)
  if (denom .le. 0.d0)  go to 48
  tlam = (aa*(slam+1.0d0) + bb*slam + cc)/denom
  if (tlam .gt. tlamax)  go to 48
  if (tlam .lt. tlamin)  tlam=tlamin
  go to 50
48 tlam = tlamax
50 continue
  do 51 i= 1, npar
51 x(i) = xxs(i)+tlam*dirin(i)
  call clic_intoex(x)
  call fcn(npar,g,f3,u,4)
  nfcn = nfcn + 1
  if (f.ge.amin .and. f2.ge.amin .and. f3.ge.amin) go to 200
  if (f .lt. f2 .and. f .lt. f3)  go to 61
  if (f2 .lt. f3)  go to 58
55 f = f3
  slam = tlam
  go to 65
58 f = f2
  go to 65
61 slam = 1.0d0
65 do 67 i= 1, npar
    dirin(i) = dirin(i)*slam
67 x(i) = xxs(i) + dirin(i)
70 amin = f
  isw(2) = 2
  if (sigma+fs-amin .lt. rostop)  go to 170
  if (sigma+rho2+fs-amin .gt. apsi)  go to 75
  if (trace .lt. vtest)  go to 170
75 continue
  if (nfcn-npfn .ge. nfcnmx)  go to 190
  iter = iter + 1
  !
  ! Get gradient and sigma
  if (isw(3) .ne. 1)  go to 80
  call clic_intoex(x)
  call fcn(npar,g,amin,u,iflag)
  nfcn = nfcn + 1
80 call clic_derive(g,g2,fcn)
  rho2 = sigma
  sigma = 0.d0
  gvg = 0.d0
  delgam = 0.d0
  do 100 i= 1, npar
    ri = 0.d0
    vgi = 0.d0
    do 90 j= 1, npar
      vgi = vgi + v(i,j)*(g(j)-gs(j))
90  ri = ri + v(i,j) *g (j)
    r(i) = ri * 0.5d0
    vg(i) = vgi*0.5d0
    gami = g(i) - gs(i)
    gvg = gvg + gami*vg(i)
    delgam = delgam + dirin(i)*gami
100 sigma = sigma + g(i)*r(i)
  if (sigma .lt. 0.d0)  go to 1
  if (gvg .le. 0.d0)  go to 105
  if (delgam .le. 0.d0)  go to 105
  go to 107
105 if (sigma .lt. 0.1d0*rostop)  go to 170
  go to 1
107 continue
  !
  ! Update covariance matrix
  trace=0.d0
  do 120 i= 1, npar
    vii(i) = v(i,i)
    do  120  j=1,npar
      d = dirin(i)*dirin(j)/delgam - vg(i)*vg(j)/gvg
120 v(i,j) = v(i,j) + 2.0d0*d
  if (delgam .le. gvg)  go to 135
  do 125 i= 1, npar
125 flnu(i) = dirin(i)/delgam - vg(i)/gvg
  do 130 i= 1, npar
    do 130 j= 1, npar
130 v(i,j) = v(i,j) + 2.0d0*gvg*flnu(i)*flnu(j)
135 continue
  do 140 i= 1, npar
140 trace = trace + ((v(i,i)-vii(i))/(v(i,i)+vii(i)))**2
  trace = dsqrt(trace/parn)
  call ucopy(x,xxs,npar)
  call ucopy(g,gs,npar)
  fs = f
  go to 24
  !
  ! End main loop
170 call message(2,1,'MIGRAD','Minimization has converged')
  isw(2) = 3
  iswtr = iswtr - 3*itaur
  if (itaur .gt. 0)  go to 435
  if (matgd .gt. 0)  go to 435
  npargd = npar*(npar+5)/2
  if (nfcn-npfn .ge. npargd)  go to 435
  call message(6,2,'MIGRAD','Covariance matrix inaccurate.'//   &
    ' HESSE will recalculate')
  !
  ! CALL HESSE fait par FITGAU si IER = 1
  if (isw(2) .ge. 2)  isw(2) = 3
  ier = 1
  go to 435
190 isw(1) = 1
  go to 230
200 call message(4,2,'MIGRAD','Fails to find improvement')
  call ucopy(xxs,x,npar)
  isw(2) = 1
  if (sigma .lt. rostop)  go to 170
  if (matgd .gt. 0)  go to 2
230 call message(6,3,'MIGRAD','Terminated without convergence')
  call  clic_intoex(x)
  iswtr = isw(5) - itaur*3
  ier = 3
435 if (ifaut1.ne.0) then
    write(ch,700) ifaut1
    call message(4,2,'MIGRAD',ch(1:lenc(ch)))
  endif
  return
700 format (1x,'MIGRAD Covariance matrix is not positive-definite ('   &
    ,i3,') times.')
end subroutine clic_migrad
!
function clic_pintf(pexti,i)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Calculates the internal parameter value PINTF corresponding
  !	to the external value PEXTI for parameter I.
  ! Arguments :
  !	PEXTI	R*8	External value			Input
  !	I	I	Parameter number		Input
  !	PINTF	R*8	Internal value			Output
  !---------------------------------------------------------------------
  real*8 ::  clic_pintf                  !
  real*8 ::  pexti                  !
  integer :: i                      !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer :: igo
  real*8 ::  yy,alimi,blimi,a
  character(len=2550) :: ch
  !
  real*8 ::  big, small
  data big,small/ 1.570796326794897d0, -1.570796326794897d0/
  !
  igo = lcode(i)
  go to (100,200,300,400),igo
  !
  ! IGO = 1  means no limits
100 clic_pintf = pexti
  go to 800
200 continue
300 continue
  !
  ! IGO = 4  means there are two limits
400 alimi = alim(i)
  blimi = blim(i)
  if (pexti-alimi)  440,500,460
440 a = small
450 clic_pintf = a
  pexti = alimi + 0.5d0* (blimi-alimi) *(dsin(a) +1.0d0)
  limset=1
  write (ch,241)  i
  call message(4,2,'PINTF',ch(1:lenc(ch)))
  go to 800
460 if (blimi-pexti)  470,520,480
470 a = big
  go to 450
480 yy=2.0d0*(pexti-alimi)/(blimi-alimi) - 1.0d0
  clic_pintf = datan(yy/dsqrt(1.d0- yy**2) )
  go to 800
500 clic_pintf = small
  go to 800
520 clic_pintf = big
800 return
241 format ('Variable ',i3,' has been brought back inside limits')
end function clic_pintf
!
subroutine clic_razzia (ynew,pnew,ier)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Called only by SIMPLEX (and IMPROV) to add a new point
  !	and remove an old one from the current simplex, and get the
  !	estimated distance to minimum.
  ! Arguments :
  !	YNEW	R*8
  !	PNEW	R*8 (*)
  !	ERROR	L	Logical error flag
  !---------------------------------------------------------------------
  real*8 ::  ynew                   !
  real*8 ::  pnew                   !
  integer :: ier                    !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer :: i,j,nparp1
  real*8 ::  us,pbig,plit
  dimension pnew(nvar)
  character(len=255) :: ch
  !
  do 10 i=1,npar
10 p(i,jh)=pnew(i)
  y(jh)=ynew
  if(ynew.ge.amin) go to 18
  do 15 i=1,npar
15 x(i)=pnew(i)
  call clic_intoex(x)
  amin=ynew
  jl=jh
18 continue
  jh=1
  nparp1=npar+1
20 do 25 j=2,nparp1
    if (y(j) .gt. y(jh))  jh = j
25 continue
  sigma = y(jh) - y(jl)
  if (sigma .le. 0.d0)  go to 45
  us = 1.0d0/sigma
  do 35 i= 1, npar
    pbig = p(i,1)
    plit = pbig
    do 30 j= 2, nparp1
      if (p(i,j) .gt. pbig)  pbig = p(i,j)
      if (p(i,j) .lt. plit)  plit = p(i,j)
30  continue
    dirin(i) = pbig - plit
    if (itaur .lt. 1 )  v(i,i) = 0.5d0*(v(i,i) +us*dirin(i)**2)
35 continue
40 return
45 write (ch, 1000)  npar
  call message(8,4,'RAZZIA',ch(1:lenc(ch)))
  ier = 4
  go to 40
1000 format ('Function value does not seem to depend ',   &
    'on any of the ',i3,' variable parameters. ',   &
    'Check input parameters and try again.')
end subroutine clic_razzia
!
subroutine clic_restor(k)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Restores a fixed parameter to variable status
  !	by inserting it into the internal parameter list at the
  !	appropriate place.
  !
  !       K = 0 means restore all parameters
  !       K = 1 means restore the last parameter fixed
  !       K = -I means restore external parameter I (if possible)
  !       IQ = fix-location where internal parameters were stored
  !       IR = external number of parameter being restored
  !       IS = internal number of parameter being restored
  ! Arguments :
  !	K	I	Code for operation			Input
  !----------------------------------------------------------------------
  ! S. Guilloteau Patch
  !---------------------------------------------------------------------
  integer ::  k                     !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  ka,ik,ipsav,ikp1,i,ir,is,ij,lc,iq
  real*8 ::   xssav,xtssav,wtssav
  character(len=255) :: ch
  !
  if (npfix .lt. 1) call message(4,2,'RESTORE',   &
    'There are no more fixed parameters')
  if (k.eq.1 .or. k.eq.0)  go to 50
  !
  ! Release parameter with specified external number
  ka = iabs(k)
  if (lcorsp(ka) .eq. 0)  go to 15
  return
15 if (npfix .lt. 1)  go to 21
  do 20 ik= 1, npfix
    if (ipfix(ik) .eq. ka)  go to 24
20 continue
21 call message(6,3,'RESTORE',   &
    'Parameter specified has never been variable')
  return
24 if (ik .eq. npfix)  go to 50
  !
  ! Move specified parameter to end of list
  ipsav = ipfix(ik)
  xssav = xs(ik)
  xtssav = xts(ik)
  wtssav = wts(ik)
  ikp1 = ik + 1
  do 30 i= ikp1,npfix
    ipfix(i-1) = ipfix(i)
    xs(i-1) = xs(i)
    xts(i-1) = xts(i)
30 wts(i-1) = wts(i)
  ipfix(npfix) = ipsav
  xs(npfix) = xssav
  xts(npfix) = xtssav
  wts(npfix) = wtssav
  !
  ! Restore last parameter in list  -- IPFIX(NPFIX)
50 continue
  if (npfix .lt. 1)  go to 300
  ir = ipfix(npfix)
  is = 0
  do 100 ij= ir, nu
    ik = nu + ir - ij
    if (lcorsp(ik))  100,100,85
85  lc = lcorsp(ik) + 1
    is = lc - 1
    lcorsp(ik) = lc
    x(lc) = x(lc-1)
    xt(lc) = xt(lc-1)
    dirin(lc) = dirin(lc-1)
100 continue
  npar = npar + 1
  if (is .eq. 0)   is = npar
  lcorsp(ir) = is
  iq = npfix
  x(is) = xs(iq)
  xt(is) = xts(iq)
  dirin(is) = wts(iq)
  npfix = npfix - 1
  isw(2) = 0
  if (itaur .lt. 1) then
    write(ch,520) ir
    call message(1,1,'RESTORE',ch(1:lenc(ch)))
  endif
  if (k.eq.0)  go to 50
300 return
520 format ('Parameter ',i4,' restored to variable.')
end subroutine clic_restor
!
subroutine clic_simplx(fcn,ier)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Performs a minimization using the SIMPLEX method of NELDER
  !	and MEAD (REF. -- COMP. J. 7,308 (1965)).
  ! Arguments :
  !	FCN	Ext	Function to minimize		Input
  !	IER	I	Error condition flag		Output
  !----------------------------------------------------------------------
  ! S. Guilloteau Patch
  !---------------------------------------------------------------------
  external :: fcn                   !
  integer ::  ier                   !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  npfn,nparp1,iflag,i,j,k,kg,ns,nf,ignal,ncycl,jhold
  real*8 ::   rho1,rho2,wg,ynpp1,absmin,aming,bestx,sig2,pb
  real*8 ::   ystar,ystst,y1,y2,rho,yrho,ypbar,f
  real*8 ::  alpha, beta, gamma, rhomin, rhomax
  character(len=255) :: ch
  !
  data   alpha, beta,  gamma, rhomin, rhomax   &
    / 1.0d0, 0.5d0, 2.0d0, 4.0d0,  8.0d0 /
  !
  if (npar .le. 0)  return
  npfn=nfcn
  nparp1=npar+1
  rho1 = 1.0d0 + alpha
  rho2 = rho1 + alpha*gamma
  wg = 1.0d0/dble(npar)
  iflag=4
  do 2 i= 1, npar
    if (isw(2) .ge. 1)  dirin(i) = dsqrt(v(i,i)*up)
    if (dabs(dirin(i)) .lt. 1.0d-5*dabs(x(i))) dirin(i)=1.0d-5*x(i)
    if(itaur.lt. 1)  v(i,i) =    dirin(i)**2/up
2 continue
  if (itaur .lt. 1)  isw(2) = 1
  !
  ! Choose the initial simplex using single-parameter searches
1 continue
  ynpp1 = amin
  jl = nparp1
  y(nparp1) = amin
  absmin = amin
  do 10 i= 1, npar
    aming = amin
    pbar(i) = x(i)
    bestx = x(i)
    kg = 0
    ns = 0
    nf = 0
4   x(i) = bestx + dirin(i)
    call clic_intoex(x)
    call fcn(npar,g, f, u, 4)
    nfcn = nfcn + 1
    if (f .le. aming)  go to 6
    !
    ! Failure
    if (kg .eq. 1)  go to 8
    kg = -1
    nf = nf + 1
    dirin(i) = dirin(i) * (-0.4d0)
    if (nf .lt. 3)  go to 4
    ns = 6
    !
    ! Success
6   bestx = x(i)
    dirin(i) = dirin(i) * 3.0d0
    aming = f
    kg = 1
    ns = ns + 1
    if (ns .lt. 6)  go to 4
    !
    ! Local minimum found in Ith direction
8   y(i) = aming
    if (aming .lt. absmin)  jl = i
    if (aming .lt. absmin)  absmin = aming
    x(i) = bestx
    do 9 k= 1, npar
9   p(k,i) = x(k)
10 continue
  jh = nparp1
  amin=y(jl)
  call clic_razzia(ynpp1,pbar,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  do 20 i= 1, npar
20 x(i) = p(i,jl)
  call clic_intoex(x)
  sigma = sigma * 10.d0
  sig2 = sigma
  ignal = 0
  ncycl=0
  !
  ! Start main loop
50 continue
  if (ignal .ge. 10)  go to 1
  if (sig2 .lt. epsi .and. sigma.lt.epsi)     go to 76
  sig2 = sigma
  if ((nfcn-npfn) .gt. nfcnmx)  go to 78
  !
  ! Calculate new point * by reflection
  do 60 i= 1, npar
    pb = 0.d0
    do 59 j= 1, nparp1
59  pb = pb + wg * p(i,j)
    pbar(i) = pb - wg * p(i,jh)
60 pstar(i)=(1.d0+alpha)*pbar(i)-alpha*p(i,jh)
  call clic_intoex(pstar)
  call fcn(npar,g,ystar,u,4)
  nfcn=nfcn+1
  if(ystar.ge.amin) go to 70
  !
  ! Point * better than JL, calculate new point **
  do 61 i=1,npar
61 pstst(i)=gamma*pstar(i)+(1.d0-gamma)*pbar(i)
  call clic_intoex(pstst)
  call fcn(npar,g,ystst,u,4)
  nfcn=nfcn+1
  !
  ! Try a parabola through PH, PSTAR, PSTST.  min = PRHO
  y1 = (ystar-y(jh)) * rho2
  y2 = (ystst-y(jh)) * rho1
  rho = 0.5d0 * (rho2*y1 -rho1*y2) / (y1 -y2)
  if (rho .lt. rhomin)  go to 66
  if (rho .gt. rhomax)  rho = rhomax
  do 64 i= 1, npar
64 prho(i) = rho*pstar(i) + (1.d0 -rho)*p(i,jh)
  call clic_intoex(prho)
  call fcn(npar, g, yrho, u, 4)
  nfcn = nfcn + 1
  if (yrho .lt. y(jl) .and. yrho .lt. ystst)  go to 65
  if (ystst .lt. y(jl))  go to 67
  if (yrho .gt. y(jl))  go to 66
  !
  ! Accept minimum point of parabola, PRHO
65 call clic_razzia (yrho,prho,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  ignal = max(ignal-2, 0)
  go to 68
66 if (ystst .lt. y(jl))  go to 67
  ignal = max(ignal-1, 0)
  call clic_razzia(ystar,pstar,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  go to 68
67 ignal = max(ignal-2, 0)
675 call clic_razzia(ystst,pstst,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
68 ncycl=ncycl+1
  go to 50
  !
  ! Point * is not as good as JL
70 if (ystar .ge. y(jh))  go to 73
  jhold = jh
  call clic_razzia(ystar,pstar,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  if (jhold .ne. jh)  go to 50
  !
  ! Calculate new point **
73 do 74 i=1,npar
74 pstst(i)=beta*p(i,jh)+(1.d0-beta)*pbar(i)
  call clic_intoex (pstst)
  call fcn(npar,g,ystst,u,4)
  nfcn=nfcn+1
  if(ystst.gt.y(jh)) go to 1
  !
  ! Point ** is better than JH
  if (ystst .lt. amin)  go to 675
  ignal = ignal + 1
  call clic_razzia(ystst,pstst,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  go to 50
  !
  ! End main loop
76 call message(2,1,'SIMPLEX','Minimization has converged')
  go to 80
78 call message(8,3,'SIMPLEX','Terminates without convergence')
  isw(1) = 1
80 do 82 i=1,npar
    pb = 0.d0
    do 81 j=1,nparp1
81  pb = pb + wg * p(i,j)
82 pbar(i) = pb - wg * p(i,jh)
  call clic_intoex(pbar)
  call fcn(npar,g,ypbar,u,iflag)
  nfcn=nfcn+1
  if (ypbar .lt. amin)  call clic_razzia(ypbar,pbar,ier)
  !
  ! Error Patch in Case of bad behaved Function
  if (ier.ne.0) return
  call clic_intoex(x)
  if (nfcnmx+npfn-nfcn .lt. 3*npar)  go to 90
  if (sigma .gt. 2.0d0*epsi)  go to 1
90 return
end subroutine clic_simplx
!
subroutine ucopy(a,b,n)
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Copies N words from A to B
  !---------------------------------------------------------------------
  real*8 ::  a                      !
  real*8 ::  b                      !
  integer :: n                      !
  ! Local
  integer :: i
  dimension a(*),b(*)
  !
  if (n .le. 0)  return
  do 10 i= 1, n
    b(i) = a(i)
10 continue
  return
end subroutine ucopy
!
subroutine clic_vermin(a,l,m,n,ifail)
  !---------------------------------------------------------------------
  ! MINUIT	Internal routine
  !	Inverts a symmetric matrix.   Matrix is first scaled to
  !	have all ones on the diagonal (equivalent to change of units)
  !	but no pivoting is done since matrix is positive-definite.
  !
  ! Arguments :
  !	A	R*8(L,M)	Matrix to be inverted	Input/Output
  !	L	I		First dimension		Input
  !	M	I		Second dimension	Input
  !	N	I
  !	IFAIL	I		Error code		Output
  !----------------------------------------------------------------------
  ! S. Guilloteau Patch
  !---------------------------------------------------------------------
  real*8 ::  a                      !
  integer ::  l                     !
  integer ::  m                     !
  integer ::  n                     !
  integer ::  ifail                 !
  ! Global
  include 'clic_fit.inc'
  ! Local
  integer ::  i,j,k,km1,kp1
  real*8 ::  si,s,q,pp
  !
  dimension a(l,m) , pp(nvar) , q(nvar) , s(nvar)
  ifail=0
  if (n .lt. 1)  go to 100
  if (n .gt. maxint)  go to 100
  !
  ! Scale matrix by sqrt of diagonal elements
  do 8  i=1,n
    si = a(i,i)
    if (si) 100,100,8
8 s(i) = 1.0d0/dsqrt(si)
  do 20 i= 1, n
    do 20 j= 1, n
20 a(i,j) = a(i,j) *s(i)*s(j)
  !
  ! Start main loop
  do 65 i=1,n
    k = i
    !
    ! Preparation for elimination step1
    q(k)=1.d0/a(k,k)
    pp(k) = 1.0d0
    a(k,k)=0.0d0
    kp1=k+1
    km1=k-1
    if(km1)100,50,40
40  do 49 j=1,km1
      pp(j)=a(j,k)
      q(j)=a(j,k)*q(k)
49  a(j,k)=0.d0
50  if(k-n)51,60,100
51  do 59 j=kp1,n
      pp(j)=a(k,j)
      q(j)=-a(k,j)*q(k)
59  a(k,j)=0.0d0
    !
    ! Elimination proper
60  do 65 j=1,n
      do 65 k=j,n
65 a(j,k)=a(j,k)+pp(j)*q(k)
  !
  ! Elements of left diagonal and unscaling
  do 70 j= 1, n
    do 70 k= 1, j
      a(k,j) = a(k,j) *s(k)*s(j)
70 a(j,k) = a(k,j)
  return
  !
  ! Failure return
100 ifail=1
  return
end subroutine clic_vermin
