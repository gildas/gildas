subroutine write_sdm_Source(holodata, error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Source SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Source
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  type (SourceRow) :: sRow
  type (SourceKey) :: sKey
  type (SourceOpt) :: sOpt
  integer :: ic, isb, ilc, l
  character, parameter :: sdmTable*6 = 'Source'
  real*8 s_5(2), x_5(3), x_2(3), s_2(2), trfm_52(3,3), psi, the, phi   &
    , lonlat(2), altitude
  real*8, parameter :: radius=6367.435d3   ! the Earth radius
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call allocSourceRow(sRow, error)
  if (error) return
  !!!!sRow%numLines = 1
  sOpt%numLines = 1
  call allocSourceOpt(sRow, sOpt, error)
  if (error) return
  sRow%sourceName = r_sourc
  !
  if (r_itype .eq. 1) then
    sRow%code = 'Target'
  elseif (r_itype .eq. 2) then
    sRow%code = 'Calibrator'
  else
    sRow%code = 'Undefined'
  endif
  !CC NOW OPTIONAL IN SS2 ...      sRow%calibrationGroup = 0
  !
  ! Special case of Tower Holography at ATF...
  !
  if (holodata) then
    call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    altitude = 2.13542*1000.
    lonlat = lonlat*pi/180d0
    ! Euler angles to go from horiz (2) to geocentric (5)
    psi = pi/2                 ! 90 degrees
    the = pi/2-lonlat(2)       ! 90 degrees minus latitude
    phi = pi/2-lonlat(1)       ! 90 degrees minus longitude
    call eulmat (psi, the, phi,trfm_52)
    !
    ! The tower is at 314 m from Vertex antenna, Az. 150 deg, el. 9 deg
    s_2 = (/ 150d0*pi/180d0, 9d0*pi/180d0 /)
    call rect(s_2, x_2)
    x_2(2) = -x_2(2)
    x_2 = x_2* 314.d0
    call matvec (x_2, trfm_52, x_5)
    sOpt%position = x_5
    !     now add the Vertex Antenna position
    x_2 = (/0d0, 0d0, radius+altitude+6.5d0/)
    call matvec (x_2, trfm_52, x_5)
    sOpt%position = sOpt%position  + x_5
    sRow%direction = 0
    sRow%properMotion = 0
    !     There is a single spectral window
    sKey%spectralWindowId = TotPowspectralWindow_Id(1)
    !
    ! Use time_start for SOurce rows (to avoid writing unnecesasary rows)
    sKey%timeInterval = ArrayTimeInterval(time_start(1),   &
      time_start(2))
    call addSourceRow(sKey, sRow, error)
    if (error) return
    Source_Id = sKey%sourceId
    call addSourcePosition(sKey, sOpt, error)
    if (error) return
    sOpt%numLines = 0
    call addSourceNumLines(sKey, sOpt, error)
    if (error) return
  !
  !     Normal case
  !
  else
    ! *TBD* need a way to code epoch, system.
    sRow%direction(1) = r_lam
    sRow%direction(2) = r_bet
    sRow%properMotion = 0
    ! Loop on spectral windows
    do isb = 1, 2
      do ic = 1, r_lband
        do ilc = 1, 2
          sKey%spectralWindowId = spectralWindow_Id(isb,ic,ilc)
          !
          ! Use time_start for SOurce rows (to avoid writing unnecesasary rows)
          sKey%timeInterval = ArrayTimeInterval(time_start(1),   &
            time_start(2))
          call addSourceRow(sKey, sRow, error)
          if (error) return
          Source_Id = sKey%sourceId
          if (r_sourc.eq.'TOWER') then
            call addSourcePosition(sKey, sOpt, error)
            if (error) return
          endif
          !
          !     Add optional parameters
          !
          sOpt%transition(1) = r_lnam(isb,ic)(1:11)
          l = lenc(sOpt%transition(1))
          if (l.gt.0) then
            call addSourceTransition(sKey, sOpt, error)
            if (error) return
            sOpt%restFrequency(1) = r_lrfoff(isb,ic)*1d6
            call addSourceRestFrequency(sKey, sOpt, error)
            if (error) return
            sOpt%sysVel(1) = r_lvoff(isb,ic)
            call addSourceSysVel(sKey, sOpt, error)
            if (error) return
            sOpt%numLines = 1
          else
            sOpt%numLines = 0
          endif
          !
          ! For a test
          sOpt%sourceModel = SourceModel_POINT
          call addSourceSourceModel(sKey, sOpt, error)
          if (error) return
          call addSourceNumLines(sKey, sOpt, error)
          if (error) return
        enddo
      enddo
      sKey%spectralWindowId = totPowSpectralWindow_Id(isb)
      sKey%timeInterval = ArrayTimeInterval(time_start(1)   &
        ,time_start(2))
      call addSourceRow(sKey, sRow, error)
    enddo
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_Source
!---------------------------------------------------------------------
subroutine get_sdm_Source(error)
  !---------------------------------------------------------------------
  ! Write the Source SDM table.
  !
  ! Key: (sourceId, spectralWindowId, timeInterval)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Source
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (SourceRow) :: sRow
  type (SourceKey) :: sKey
  type (SourceOpt) :: sOpt
  integer :: ic, isb, ilc, l, lenz, i, ia, k
  integer, parameter :: mlines = 12
  character, parameter :: sdmTable*6 = 'Source'
  logical present
  real*8 :: sourcePosition(3), d
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call allocSourceRow(sRow, error)
  if (error) return
  sOpt%numLines = mLines
  call allocSourceOpt(sRow, sOpt, error)
  if (error) return
  !      calibrationGroup = 0
  ! *TBD* need a way to code epoch, system.
  !      positionExists = .false.
  ! may be otherwise for holography transmitter
  ! Loop on spectral windows
  !     loops on spectral windows:
  !      timeInterval = (/time_Interval(1), time_Interval(2)-1/)
  sKey%timeInterval = ArrayTimeInterval(time_Interval(1),   &
    time_Interval(2)-1)
  do i=1, ddId_Size
    !         numLines = 1
    !               print *, 'Source: time_start ',time_start
    !
    ! Use time_start for SOurce rows (to avoid writing unnecesasary rows)
    sKey%sourceId = Source_Id
    sKey%spectralWindowId = swId_List(i)
    !$$$         print *, 'sKey%timeInterval ',sKey%timeInterval
    !$$$         print *,'sKey%sourceId, sKey%spectralWindowId'
    !$$$         print *, sKey%sourceId, sKey%spectralWindowId
    !               print *, 'Source: time_start ',time_start
    call getSourceRow(sKey, sRow, error)
    if (error) then
      call sdmMessageI(1,1,sdmTable ,   &
        'Spectral Window with no source info ',   &
        sKey%spectralWindowId)
      error= .false.
    else
      r_lam = sRow%direction(1)
      r_bet = sRow%direction(2)
      l = lenz(sRow%sourceName)
      r_sourc = sRow%sourceName(1:l)
      !
      !     Get optional parameters
      l = lenz(sRow%code)
      if (sRow%code(1:l).eq.'Target') then
        r_itype = 1
      elseif (sRow%code(1:l).eq.'Calibrator') then
        r_itype = 2
      endif
      !     Get optional parameters
      call getSourceTransition(sKey, sOpt, present, error)
      !$$$            print *, 'getSourceTransition present, error ',present,
      !$$$     &           error
      if (error) return
      isb = sb_list(i)
      ic = bb_list(i)
      if (present) then
        l = lenz(sOpt%transition(1))
        r_lnam(isb,ic) =  sOpt%transition(1)(1:l)
      else
        r_lnam(isb,ic) = 'None'
      endif
      !
      !         restFrequencyDim = 1
      call getSourceRestFrequency(sKey, sOpt, present, error)
      !$$$            print *, 'getSourceRestFrequency present, error ',present,
      !$$$     &           error
      if (error) return
      if (present) then
        r_lrfoff(isb,ic) = sOpt%restFrequency(1)
      else
        r_lrfoff(isb,ic) = 0
      endif
      !
      call getSourceSysVel(sKey, sOpt, present, error)
      !$$$            print *, 'getSourceSysVel present, error ',present, error
      if (error) return
      if (present) then
        r_lvoff(isb,ic) = sOpt%sysVel(1)
      else
        r_lvoff(isb,ic) = 0
      endif
      !
      call getSourcePosition(sKey, sOpt, present, error)
      if (error) return
      if (present) then
        sourcePosition = sOpt%position
        !               print *, 'sourcePosition ', sourcePosition
        do ia = 1, r_nant
          d = 0
          do k=1, 3
            d = d + (sourcePosition(k)-r_antgeo(k,ia))**2
          enddo
          d = sqrt(d)
        !c                  print *, '=== ia, d ', ia, d
        enddo
      else
        sourcePosition = 0
      endif
    endif
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
end subroutine get_sdm_Source
!!! !---------------------------------------------------------------------
!!! subroutine allocSourceRow(row, error)
!!!   !---------------------------------------------------------------------
!!!   !    real*8, allocatable :: direction(:)
!!!   !    real*8, allocatable :: properMotion(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Source
!!!   type(SourceRow) :: row
!!!   integer :: ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*6 = 'Source'
!!!   !
!!!   ! direction(2)
!!!   if (allocated(row%direction)) then
!!!     deallocate(row%direction, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%direction(2), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! properMotion(2)
!!!   if (allocated(row%properMotion)) then
!!!     deallocate(row%properMotion, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%properMotion(2), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!!! !---------------------------------------------------------------------
!!! subroutine allocSourceOpt(Opt, numLines, error)
!!!   !---------------------------------------------------------------------
!!!   !     real*8, allocatable :: position(:)
!!!   !     character*256, allocatable :: transition(:)
!!!   !     real*8, allocatable :: restFrequency(:)
!!!   !     real*8, allocatable :: sysVel(:)
!!!   !     real*8, allocatable :: rangeVel(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Source
!!!   type(SourceOpt) :: opt
!!!   integer :: numLines, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*6 = 'Source'
!!!   !
!!!   ! position(3)
!!!   if (allocated(opt%position)) then
!!!     deallocate(opt%position, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%position(3), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! transition(numLines)
!!!   if (allocated(opt%transition)) then
!!!     deallocate(opt%transition, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%transition(numLines), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! restFrequency(numLines)
!!!   if (allocated(opt%restFrequency)) then
!!!     deallocate(opt%restFrequency, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%restFrequency(numLines), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! sysVel(numLines)
!!!   if (allocated(opt%sysVel)) then
!!!     deallocate(opt%sysVel, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%sysVel(numLines), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! rangeVel(2)
!!!   if (allocated(opt%rangeVel)) then
!!!     deallocate(opt%rangeVel, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%rangeVel(2), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!---------------------------------------------------------------------
