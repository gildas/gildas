module clic_buffers
!
! Calibration buffers.
!
      real(kind=4),allocatable :: tcal_u(:,:), tcal_l(:,:)! (at zenith)
      real(kind=4),allocatable :: tcal(:,:,:)
!
      integer(kind=4),parameter :: chop =  1
      integer(kind=4),parameter :: cold  = 2 
      integer(kind=4),parameter :: sky   = 3
!
! S_lev contains the TP levels for the last atmospheric calibration
! C_lev contains the current TP levels.
      real(kind=4),allocatable :: c_lev(:,:,:,:,:) 
      real(kind=4),allocatable :: s_lev(:,:,:,:,:)
      real(kind=4),allocatable :: c_c(:,:,:,:,:) 
      real(kind=4),allocatable :: c_l(:,:,:,:,:)
      integer(kind=4),allocatable ::  h_atm(:,:)
      integer(kind=4),allocatable :: h_mon(:,:)
      integer(kind=4),allocatable :: h_line(:,:)
      integer(kind=4),allocatable :: h_inter(:,:)
      integer(kind=4),allocatable :: sky_flags(:,:)
      integer(kind=4),allocatable :: sky_scan(:)
      integer(kind=4) :: mon_scan
      real(kind=4),allocatable :: integ(:,:,:,:,:)
      integer end_crband
end module
