subroutine write_sdm_calDevice(error)
  !---------------------------------------------------------------------
  ! Write the calDevice SDM table.
  !
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_caldevice
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(CalDeviceRow) :: cdRow
  type(CalDeviceKey) :: cdKey
  type(CalDeviceOpt) :: cdOpt
  character sdmTable*12
  parameter (sdmTable='CalDevice')
  integer :: ic, ia, isb, ilc, numReceptors
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  ! Bure has 2 loads
  cdRow%numCalLoad = 2
  call allocCalDeviceRow(cdRow, error)
  if (error) return
  cdOpt%numReceptor = 1
  call allocCalDeviceOpt(cdRow, cdOpt, error)
  if (error) return
  ! open-ended interval
  cdKey%feedId = feed_Id
  !
  !     antennas
  do ia = 1, r_nant
    cdKey%antennaId = antenna_Id(ia)
    !
    !     load 1 is cold, load 2 is ambient...  NOTE: At this point I
    !     neglect the load temperature difference between the two polars. In
    !     fact ASDM can hae also a coupling corefficient depending on polar.
    cdRow%calLoadNames(1) = CalibrationDevice_COLD_LOAD
    cdOpt%temperatureLoad(1) = r_tcold(1,ia)
    cdRow%calLoadNames(2) = CalibrationDevice_AMBIENT_LOAD
    cdOpt%temperatureLoad(2) = r_tchop(1,ia)
    !     loop on side bands
    do isb = 1, r_lnsb
      !     loop on correlator units
      do ic = 1, r_lband
        !     line/continuum
        do ilc = 1, 2
          cdKey%timeInterval = ArrayTimeInterval(time_Interval(1   &
            ),time_Interval(2))
          cdKey%spectralWindowId = spectralWindow_Id(isb, ic   &
            ,ilc)
          call addCalDeviceRow(cdKey, cdRow, error)
          if (error) return
          !     add 1ns to the time so that we do not add the load T to the
          !     previous subscan ...
          cdKey%timeInterval = ArrayTimeInterval(time_Interval(1   &
            )+1,time_Interval(2))
          call addCalDeviceTemperatureLoad(cdKey, cdOpt, error)
          if (error) return
        enddo
      enddo
      cdKey%timeInterval = ArrayTimeInterval(time_Interval(1)   &
        ,time_Interval(2))
      cdKey%spectralWindowId = totPowSpectralWindow_Id(isb)
      call addCalDeviceRow(cdKey, cdRow, error)
      if (error) return
      !     add 1ns to the time so that we do not add the load T to the
      !     previous subscan ...
      cdKey%timeInterval = ArrayTimeInterval(time_Interval(1)+1   &
        ,time_Interval(2))
      call addCalDeviceTemperatureLoad(cdKey, cdOpt, error)
      if (error) return
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
!
end subroutine write_sdm_calDevice
!---------------------------------------------------------------------
! TODO Get CalDevice
subroutine get_sdm_calDevice(error)
  !---------------------------------------------------------------------
  ! Get the calDevice SDM table.
  ! Keys has timeInterval, AnntennaId, SpectralWindowId ...
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_caldevice
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(CalDeviceRow) :: cdRow
  type(CalDeviceKey) :: cdKey
  type(CalDeviceOpt) :: cdOpt
  character sdmTable*12
  parameter (sdmTable='CalDevice')
  integer :: i, ia, numReceptors, k
  real*8  :: t
  logical :: present
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call getCalDeviceTableSize(i, error)
  if (i.le.0) then
    call sdmMessage(1,1,sdmTable ,'Empty.')
    return
  endif
  cdRow%numCalLoad = 2
  call allocCalDeviceRow(cdRow, error)
  if (error) return
  !
  !     loop on spectral windows
  do i=1, ddId_Size
    do ia = 1, r_nant
      cdKey%antennaId = antenna_Id(ia)
      cdKey%timeInterval = ArrayTimeInterval(time_Interval(1)   &
        ,time_Interval(2))
      cdkey%spectralWindowId = swId_List(i)
      cdKey%feedId = 0
      call getCalDeviceRow (cdKey, cdRow, error)
      if (error) return
      ! Bure has a single polarization ... for the time being.
      numReceptors = 1
      call allocCalDeviceOpt(cdOpt, cdRow%numCalLoad, numReceptors   &
        , error)
      if (error) return
      call getCalDeviceTemperatureLoad(cdKey, cdOpt, present,   &
        error)
      if (error) return 
      if (present) then
        !     Bure stores a single Cal temperature for all spectral windows
        do k = 1, cdRow%numCalLoad
          t = cdOpt%temperatureLoad(k)
          !     cold or ambient is only a matter of temperature...
          if (cdRow%calLoadNames(k).eq.   &
            CalibrationDevice_COLD_LOAD) then
            r_tcold(1,ia) = t
            r_tcold(2,ia) = t
          elseif (cdRow%calLoadNames(k).eq.   &
            CalibrationDevice_AMBIENT_LOAD) then
            r_tchop(1,ia) = t
            r_tchop(2,ia) = t
          else
            call sdmMessageI(2,2,sdmTable   &
              ,'Ignored Calibration Load #',k)
            print *, 'i, k, temperature  ', i, k, t
          endif
        enddo
      endif
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine get_sdm_calDevice
!---------------------------------------------------------------------
!!! subroutine allocCalDeviceRow(row, numCalload,error)
!!!   !---------------------------------------------------------------------
!!!   !---------------------------------------------------------------------
!!!   use sdm_CalDevice
!!!   type(CalDeviceRow) :: row
!!!   integer :: ier, numCalload
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'CalDevice'
!!!   !
!!!   ! calLoadName(numCalload)
!!!   if (allocated(row%calLoadName)) then
!!!     deallocate(row%calLoadName, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%calLoadName(numCalload), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!---------------------------------------------------------------------
!!! subroutine allocCalDeviceOpt(opt, numCalload, numReceptors, error)
!!!   !---------------------------------------------------------------------
!!!   !    real*8, allocatable :: noiseCal(:)
!!!   !    real*8, allocatable :: temperatureLoad(:)
!!!   !    real, allocatable :: calEff(:,:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_CalDevice
!!!   type(CalDeviceOpt) :: opt
!!!   integer :: ier, numCalload, numReceptors
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'CalDevice'
!!!   !
!!!   ! noiseCal(numCalload)
!!!   if (allocated(opt%noiseCal)) then
!!!     deallocate(opt%noiseCal, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%noiseCal(numCalload), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! temperatureLoad(numCalload)
!!!   if (allocated(opt%temperatureLoad)) then
!!!     deallocate(opt%temperatureLoad, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%temperatureLoad(numCalload), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! calEff(numReceptors, numCalload)
!!!   if (allocated(opt%calEff)) then
!!!     deallocate(opt%calEff, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%calEff(numReceptors, numCalload), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
