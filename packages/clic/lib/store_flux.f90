subroutine store_flux(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  !	Stores internal fluxes and efficiencies
  !	into observations from current index
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  logical :: end
  integer :: i, if, j
  character(len=80) :: chain
  integer :: dtol
  real :: frtol
  parameter (frtol=1e3, dtol=7)    ! 1 GHz ... 1 week ...
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) return
  call check_index(error)
  if (error) return
  call check_output_file(error)
  if (error) return
  call check_equal_file(error)
  if (error) return
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  if (error) return
  !
  ! Check the flux list
  call sort_fluxes(error)
  if (error) return
  do i = 1, n_flux
    if (abs(flux(i)).gt.1e6 .or. abs(flux(i)).lt.1e-6) then
      write (chain,'(a,1pg12.5)')   &
        'Unlikely flux for '//c_flux(i)//' : ',flux(i)
      call message (8,3,'STORE_FLUX',chain(1:lenc(chain)))
      error = .true.
      return
    endif
  enddo
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) return
  end = .false.
  do while (.not.end)
    !
    ! Store Fluxes
    ! Fluxes with undetermined date or frequency come first in the list.
    ! so they are overridden by values with precise frequency or date.
    if = 0
    !
    !Give priority to FIXED fluxes.
    ! exact frequency or date ? (FRTOL and DTOL should be tunable parameters)
    do i = 1, n_flux
      if (f_flux(i) .and. r_sourc.eq.c_flux(i) .and.   &
        ((freq_flux(i).le.0)  .or.   &
        abs(r_restf-freq_flux(i)).lt.frtol) .and.   &
        ((date_flux(i).lt.-100000)   &
        .or. (abs(date_flux(i)-r_dobs).le.dtol))) then
        if = i
      endif
    enddo
    ! or any frequency,
    if (if.eq.0) then
      do i = 1, n_flux
        if (f_flux(i) .and. c_flux(i).eq.r_sourc) then
          if = i
        endif
      enddo
    endif
    ! now free fluxes
    ! exact frequency or date ? (FRTOL and DTOL should be tunable parameters)
    if (if.eq.0) then
      do i = 1, n_flux
        if (r_sourc.eq.c_flux(i) .and.   &
          ((freq_flux(i).le.0)  .or.   &
          abs(r_restf-freq_flux(i)).lt.frtol) .and.   &
          ((date_flux(i).lt.-100000)   &
          .or. (abs(date_flux(i)-r_dobs).le.dtol))) then
          if = i
        endif
      enddo
    endif
    ! or any frequency,
    if (if.eq.0) then
      do i = 1, n_flux
        if (c_flux(i).eq.r_sourc) then
          if = i
        endif
      enddo
    endif
    if (if.ne.0) then
      if (f_flux(if)) then
        r_flux = abs(flux(if))
      else
        r_flux = -abs(flux(if))
      endif
    endif
    ! Store efficiencies
    !         DO J=1, R_NANT
    !            R_JYKEL(J) = JY_TO_KEL(J)
    !         ENDDO
    !
    ! Update scan
    call write_scan (.false.,error)    ! data not changed
    if (error) return
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) return
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_flux
!
subroutine store_spidx(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  !	Stores internal spidx
  !	into observations from current index
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  logical :: end
  integer :: i, if, j
  character(len=80) :: chain
  integer :: dtol
  real :: frtol
  parameter (frtol=1e3, dtol=7)    ! 1 GHz ... 1 week ...
  !------------------------------------------------------------------------
  ! Code:
  call check_input_file(error)
  if (error) return
  call check_index(error)
  if (error) return
  call check_output_file(error)
  if (error) return
  call check_equal_file(error)
  if (error) return
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  if (error) return
  !
  ! Check the spidx list
  call sort_spidx(error)
  if (error) return
  do i = 1, n_spidx
    if (abs(spidx(i)).gt.100) then
      write (chain,'(a,1pg12.5)')   &
        'Unlikely spidx for '//c_spidx(i)//' : ',spidx(i)
      call message (8,3,'STORE_SPIDX',chain(1:lenc(chain)))
      error = .true.
      return
    endif
  enddo
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) return
  end = .false.
  do while (.not.end)
    !
    ! Store Spidx
    ! Spidx with undetermined date or frequency come first in the list.
    ! so they are overridden by values with precise frequency or date.
    if = 0
    !
    !Give priority to FIXED spidxes.
    ! exact frequency or date ? (FRTOL and DTOL should be tunable parameters)
    do i = 1, n_spidx
      if (f_spidx(i) .and. r_sourc.eq.c_spidx(i) .and.   &
        ((freq_spidx(i).le.0)  .or.   &
        abs(r_restf-freq_spidx(i)).lt.frtol) .and.   &
        ((date_spidx(i).lt.-100000)   &
        .or. (abs(date_spidx(i)-r_dobs).le.dtol))) then
        if = i
      endif
    enddo
    ! or any frequency,
    if (if.eq.0) then
      do i = 1, n_spidx
        if (f_spidx(i) .and. c_spidx(i).eq.r_sourc) then
          if = i
        endif
      enddo
    endif
    ! now free spidxes
    ! exact frequency or date ? (FRTOL and DTOL should be tunable parameters)
    if (if.eq.0) then
      do i = 1, n_spidx
        if (r_sourc.eq.c_spidx(i) .and.   &
          ((freq_spidx(i).le.0)  .or.   &
          abs(r_restf-freq_spidx(i)).lt.frtol) .and.   &
          ((date_spidx(i).lt.-100000)   &
          .or. (abs(date_spidx(i)-r_dobs).le.dtol))) then
          if = i
        endif
      enddo
    endif
    ! or any frequency,
    if (if.eq.0) then
      do i = 1, n_spidx
        if (c_spidx(i).eq.r_sourc) then
          if = i
        endif
      enddo
    endif
    if (if.ne.0) then
      r_spidx = spidx(if)
    endif
    ! Store efficiencies
    !         DO J=1, R_NANT
    !            R_JYKEL(J) = JY_TO_KEL(J)
    !         ENDDO
    !
    ! Update scan
    call write_scan (.false.,error)    ! data not changed
    if (error) return
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) return
  enddo
  return
  !
999 error = .true.
  return
end subroutine store_spidx
!
subroutine store_stokes(line,error)
  use gildas_def
  use gkernel_interfaces
  use gkernel_types
  !---------------------------------------------------------------------
  !	Stores Stokes parameters
  !	into observations from current index
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: iarg,narg
  real :: arg(4)
  logical :: end
  ! Code:
  error = .false.
  call check_input_file(error)
  if (error) return
  call check_index(error)
  if (error) return
  call check_output_file(error)
  if (error) return
  call check_equal_file(error)
  if (error) return
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  if (error) return
  !
  !
  narg = sic_narg(0)
  if (narg.lt.4) then
    call message(8,3,'STORE_STOKES','At least 3 Stokes parameter required')
    error = .true.
    return
  endif
  if (narg.gt.5) return
  !
  do iarg=1, narg
    call sic_r4(line,0,iarg+1,arg(iarg),.false.,error)
    if (error) then 
      call message(8,3,'STORE_STOKES','Error reading input parameters')
      error = .true.
      return
    endif
  enddo
  r_stokes_i = arg(1)
  r_stokes_q = arg(2)
  r_stokes_u = arg(3)
  if (narg.eq.4) r_stokes_v = arg(4)
print *,"arg",arg
  !
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) return
  end = .false.
  do while (.not.end)
    r_stokes_i = arg(1)
    r_stokes_q = arg(2)
    r_stokes_u = arg(3)
    if (narg.eq.5) r_stokes_v = arg(4)
    ! Update scan
    call write_scan (.false.,error)    ! data not changed
    if (error) return
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) return
  enddo

  return
999 error = .true.
  return
end subroutine store_stokes
!
subroutine store_dterm(line,error)
  use gildas_def
  use gkernel_interfaces
  use gkernel_types
  !---------------------------------------------------------------------
  !	Stores "D terms" (polarisation)
  !	into observations from current index
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: iarg,narg,iopt,i,j,k,ibb
  real :: arg(4),dum(4,mnant),real, aimag
  logical :: end,dterm(mnant)
  ! Code:
  dterm = .false.
  error = .false.
  call check_input_file(error)
  if (error) return
  call check_index(error)
  if (error) return
  call check_output_file(error)
  if (error) return
  call check_equal_file(error)
  if (error) return
  if (write_mode.eq.'UPDA') call check_equal_file(error)
  if (error) return
  !
  !
  iopt = 0
  !
  ! /BBAND option
  if (sic_present(13,0)) then
    call sic_i4(line,13,1,ibb,.true.,error)
  else
    call message(8,3,'STORE_DTERM','/BBAND option is mandatory')
    error = .true.
    return
  endif
  !
  i = 2
  do while(sic_present(iopt,i))
    call sic_i4(line,iopt,i,j,.true.,error)
    if (error) return
    if (j.gt.0.and.j.le.mnant) then
      dterm(j) = .true.
    else
      call message(8,3,'STORE_DTERM','Wrong antenna number')
      error = .true.
      return
    endif
    do k=1, 4
      call sic_r4(line,iopt,i+k,dum(k,j),.true.,error)
      if (error) return
    enddo
    i = i+5
  enddo
  !
  ! Loop on current index
  call get_first(.false.,error)
  if (error) return
  if (ibb.lt.1.or.ibb.gt.r_nbb) then
    call message(8,3,'STORE_DTERM','Wrong baseband number')
    error = .true.
    return
  endif
  end = .false.
  do while (.not.end)
    do i=1, mnant
      if (dterm(i)) then
        r_dx(i,ibb) = cmplx(dum(1,i),dum(2,i))
        r_dy(i,ibb) = cmplx(dum(3,i),dum(4,i))
      endif
    enddo
    ! Update scan
    call write_scan (.false.,error)    ! data not changed
    if (error) return
    !
    ! Next observation
    if (sic_ctrlc()) goto 999
    call get_next(end,error)
    if (error) return
  enddo

  return
999 error = .true.
  return
end subroutine store_dterm

