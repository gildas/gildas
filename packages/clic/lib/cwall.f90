subroutine cwsec (scode,len,error)
  use gbl_constant
  use gbl_convert
  use clic_file
  !---------------------------------------------------------------------
  !	Convert section from System data type to File data type
  !---------------------------------------------------------------------
  integer         :: scode                  ! Section number
  integer(kind=8) :: len                    ! Section length
  logical         :: error                  ! Error flag
  ! Global
  external :: r4tor4,r8tor8,i8toi8
  external :: ier4va,eir4va,eir4ie,ier4ei,var4ie,var4ei
  external :: ier8va,eir8va,eir8ie,ier8ei,var8ie,var8ei
  external ::        eii4va,eii4ie,iei4ei       ,vai4ei
  external ::               eii8ie,iei8ei
  !
  if (error) return
  !
  if (o%conv%code.eq.0) then        ! No conversion
    call scwsec (scode,len,error,r4tor4,r8tor8,r4tor4,i8toi8,r4tor4)
  elseif (o%conv%code.eq.-ieee_fr_vax) then
    call scwsec (scode,len,error,ier4va,ier8va,r4tor4,i8toi8,r4tor4)
  elseif (o%conv%code.eq.-vax_fr_ieee) then
    call scwsec (scode,len,error,var4ie,var8ie,r4tor4,i8toi8,r4tor4)
  elseif (o%conv%code.eq.-eeei_fr_vax) then
    call scwsec (scode,len,error,eir4va,eir8va,eii4va,i8toi8,r4tor4)
  elseif (o%conv%code.eq.-eeei_fr_ieee) then
    call scwsec (scode,len,error,eir4ie,eir8ie,eii4ie,eii8ie,r4tor4)
  elseif (o%conv%code.eq.-vax_fr_eeei) then
    call scwsec (scode,len,error,var4ei,var8ei,vai4ei,i8toi8,r4tor4)
  elseif (o%conv%code.eq.-ieee_fr_eeei) then
    call scwsec (scode,len,error,ier4ei,ier8ei,iei4ei,iei8ei,r4tor4)
  endif
end subroutine cwsec
!
subroutine scwsec (scode,len,error,r4,r8,i4,i8,cc)
  use gkernel_interfaces
  use gbl_constant
  use clic_file
  !---------------------------------------------------------------------
  !	Convert section from System data type to File data type
  !---------------------------------------------------------------------
  integer        ,intent(in)    :: scode  ! Section number
  integer(kind=8),intent(inout) :: len    ! Section lenght (In/Out)
  logical        ,intent(out)   :: error  ! Error flag
  external                      :: r4     ! real*4 conversion routine
  external                      :: r8     ! real*8 conversion routine
  external                      :: i4     ! integer*4 conversion routine
  external                      :: i8     ! integer*8 conversion routine
  external                      :: cc     ! character conversion routine
  ! Global
  include 'clic_parameter.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  real*4 :: ichain(64)
  integer(kind=8) :: k,lensec
  integer,parameter :: mlwork=4
  integer(kind=8) :: lwork(mlwork)
  integer,parameter :: mdwork=max(12,3*mnbas,3*mnant,2*mrlband,mbands)
  real*8 ::  dwork(mdwork)
  integer :: iwork(mdata),ii,j,l,m,n, kk, temp_dim, mask, temp_dim2,i4value
  integer :: dmsaflag(mnant), dmsbflag(mnbas), dummy, nword
  real    :: dmdelay(m_pol_rec,mnant),dmatfac(m_pol_rec,2,mnant)
  real    :: ifattn(2,mnant,mrlband)
  equivalence (iwork,data_buffer)
  logical :: test
  character(len=80) :: chain
  ! Code
  if (error) return
  !
  k = len+1
  !
  ! General Section
  if (scode.eq.gen_sec) then
    !         CALL R8 (R_UT,IWORK,2)
    call r8 (r_ut,dwork,2)
    call r4tor4(dwork,iwork,4)
    call r4 (r_az,iwork(5),5)
    call i4 (r_typof,iwork(10),2)
    if (r_kind.eq.5) then
      write (chain,'(A,I2)') 'New receiver data kind is ', r_kind
      l = lenc(chain)
    !           CALL MESSAGE(1,1,'WOBS',CHAIN(1:L))
    endif
  !
  ! Position Section
  elseif (scode.eq.pos_sec) then
    call chtoby(r_sourc,ichain,12) ! CHAIN = R_SOURC
    call cc (ichain,iwork,3)
    call r4 (r_epoch,iwork(4),1)
    !         CALL R8 (R_LAM,IWORK(5),2)
    call r8 (r_lam,dwork,2)
    call r4tor4(dwork,iwork(5),4)
    call r4 (r_lamof,iwork(9),5)
    call i4 (r_proj,iwork(11),1)
    k = 14
    if (e%version.gt.3) then
      call r4 (r_spidx,iwork(14),1)
      k = k + 1
    endif
    if (e%version.gt.5) then
      call r4 (r_stokes_i,iwork(15),8)
      k = k + 8
    endif
  !
  ! Spectral Setup Section: never used.
  !      ELSEIF (SCODE.EQ.SPEC_SEC) THEN
  !         CHAIN = R_SLINE
  !         CALL CC (ICHAIN,IWORK,3)
  !         CALL R8 (R_SRESTF,IWORK(4),1)
  !         CALL I4 (R_SNCHAN,IWORK(6),1)
  !         CALL R4 (R_SRCHAN,IWORK(7),6)
  !         CALL R8 (R_SIMAGE,IWORK(13),1)
  !         CALL I4 (R_SVTYPE,IWORK(15),1)
  !         CALL R8 (R_SFOBS,IWORK(16),1)
  !
  ! Interferometer Section
  elseif (scode.eq.interc_sec) then
    call i4 (r_nant,iwork,3)
    if (r_nant.le.0) then
      call message(8,4,'WINTERC','NANT should be positive')
      error = .true.
      return
    endif
    if (r_nbas.le.0) then
      call message(8,4,'WINTERC','NBAS should be positive')
      error = .true.
      return
    endif
    k = 5
    call r4 (r_houra,iwork(4),1)
    call i4 (r_kant,iwork(k),r_nant)
    k = k + r_nant
    call i4 (r_kent,iwork(k),r_nant)
    k = k + r_nant
    call i4 (r_istat,iwork(k),r_nant)
    k = k + r_nant
    call i4 (r_iant,iwork(k),r_nbas)
    k = k + r_nbas
    call i4 (r_jant,iwork(k),r_nbas)
    k = k + r_nbas
    !         CALL R8 (R_BAS,IWORK(K),3*R_NBAS)
    call r8 (r_bas,dwork,3*r_nbas)
    call r4tor4(dwork,iwork(k),2*3*r_nbas)
    k = k + 6*r_nbas
    ! It's better NOT to modify older observations, this can lead to
    ! unpredictable results, for instance with MODIFY AXES ...
    !         IF (.NOT.(MODIFY .AND. K.GT.LENSEC(INTERC_SEC))) THEN
    !         CALL R8 (R_ANT,IWORK(K),3*R_NANT)
    call r8 (r_ant,dwork,3*r_nant)
    call r4tor4(dwork,iwork(k),2*3*r_nant)
    k = k + 6*r_nant
    call i4 (r_ntri,iwork(k),1)
    k = k + 1
    if (r_ntri.gt.0) then
      call i4 (r_atri,iwork(k),3*r_ntri)
      k = k + 3*r_ntri
    endif
    !         ENDIF
    !         IF (.NOT.(MODIFY .AND. K.GT.LENSEC(INTERC_SEC))) THEN
    call r4 (r_axes,iwork(k),r_nant)
    k = k + r_nant
    !         ENDIF
    !         IF (.NOT.(MODIFY .AND. K.GT.LENSEC(INTERC_SEC))) THEN
    call r4 (r_phlo1,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_phlo3,iwork(k),r_nant)
    k = k + r_nant
    if (.not.modify .or. k.le.lensec(interc_sec)) then
      call i4 (r_nband_widex, iwork(k),1)
      k = k + 1
   endif
  !         ENDIF
  !
  ! RF Setup Section
  elseif (scode.eq.rfset_sec) then
    call chtoby(r_line,ichain,12)  ! CHAIN = R_LINE
    call cc (ichain,iwork,3)
    call i4 (r_isb,iwork(4),2)
    !         CALL R8 (R_RESTF,IWORK(6),4)
    call r8 (r_restf,dwork,4)
    call r4tor4(dwork,iwork(6),4*2)
    call r4 (r_veloc,iwork(14),1)
    call i4 (r_typev,iwork(15),1)
    call r4 (r_doppl,iwork(16),1)
    k = 17
    if (r_kind.eq.5) then
      call i4 (r_npol_rec,iwork(k),1)
      k = k + 1
      if (e%version.le.3) then
        call i4 (r_quarter,iwork(k),2)
        k = k + 2
      endif 
      call i4 (r_qwplate,iwork(k),r_nant)
      k = k + r_nant
      if (e%version.ge.4) then
        call i4(r_nif,iwork(k),1)
        k = k + 1
        do ii =1, r_nif
          call chtoby(r_ifname(ii),ichain,4)
          call cc(ichain,iwork(k),1)
          k = k + 1
        enddo
        call i4(r_kif,iwork(k),r_nif)
        k = k + r_nif
        do ii = 1, r_nif
          call r4(r_ifattn(1,ii),iwork(k),r_nant)
          k = k + r_nant
        enddo
        call i4(r_nbb,iwork(k),1)
        k = k + 1
        do ii=1, r_nbb
          call chtoby(r_bbname(ii),ichain,4)
          call cc(ichain,iwork(k),1)
          k = k + 1
        enddo
        call i4(r_kbb,iwork(k),r_nbb)
        k = k + r_nbb
        call i4(r_mapbb,iwork(k),r_nbb)
        k = k + r_nbb
        do ii = 1, r_nbb
          call i4(r_mappol(1,ii),iwork(k),r_nant)
          k = k + r_nant
        enddo
        call i4(r_nsubpower,iwork(k),1)
        k = k +1
        if (r_nsubpower.ne.0) then
          call r4(r_subpower_val,iwork(k),1)
          k = k + 1
          call i4(r_subpower_ref,iwork(k),1)
          k = k + 1
          call r4(r_subpower_inc,iwork(k),1)
          k = k + 1
        endif
      endif
      if (e%version.ge.5) then
        do ii = 1, mnsb
          call i4 (r_polswitch(1,ii),iwork(k),r_nant)
          k = k + r_nant
        enddo
      endif 
    !            DO I = 1, R_NPOL_REC
    !               CALL I4 (R_ATTCOM(1,I),IWORK(K),R_NANT)
    !               K = K + R_NANT
    !            ENDDO
    endif
  !
  ! Continuum Setup Section
  elseif (scode.eq.contset_sec) then
    call i4 (r_nsb,iwork,3)
    call r4 (r_crch,iwork(4),6)
    !         CALL R8 (R_CRFOFF,IWORK(10),2)
    call r8 (r_crfoff,dwork,2)
    call r4tor4(dwork,iwork(10),4)
    call r4 (r_crfres,iwork(14),2)
    call chtoby(r_cnam(1),ichain,12)
    call cc (ichain,iwork(16),3)
    call chtoby(r_cnam(2),ichain,12)
    call cc (ichain,iwork(19),3)
    !         CALL R8 (R_CFCEN,IWORK(22),MCCH)
    k = 22
    if (e%version.le.3) then
      if (r_nband_widex.eq.0) then
        temp_dim = 10
      else
        temp_dim = 12
      endif
    else
      temp_dim = r_nband
    endif
    call r8 (r_cfcen,dwork,temp_dim)
    call r4tor4(dwork,iwork(22),2*temp_dim)
    call r4 (r_cfwid,iwork(22+2*temp_dim),temp_dim)
    k = k + 3*temp_dim
    if (e%version.le.3) then
      call i4 (r_sband,iwork(22+3*temp_dim),2)
      k = k + 2
    else
      do ii=1, r_nband
        call i4 (r_sband(1,ii),iwork(k),2)
        k = k + 2
      enddo
    endif
  !
  ! Line Setup Section
  elseif (scode.eq.lineset_sec) then
    call i4 (r_lband,iwork,3)
    k = 4
    if (r_lband.gt.0) then
      call i4 (r_lnch,iwork(k),r_lband)
      k = k + r_lband
      call i4 (r_lich,iwork(k),r_lband)
      k = k + r_lband
      !            CALL R8 (R_LFCEN,IWORK(K),R_LBAND)
      call r8 (r_lfcen,dwork,r_lband)
      call r4tor4(dwork,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call r4 (r_lfres,iwork(k),r_lband)
      k = k + r_lband
      call r4 (r_lrch,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call r4 (r_lvoff,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call r4 (r_lvres,iwork(k),2*r_lband)
      k = k + 2*r_lband
      !            CALL R8 (R_LRFOFF,IWORK(K),R_LBAND)
      call r8 (r_lrfoff,dwork,2*r_lband)
      call r4tor4(dwork,iwork(k),2*2*r_lband)
      k = k + 4*r_lband
      call r4 (r_lrfres,iwork(k),2*r_lband)
      k = k + 2*r_lband
      do ii=1,r_lband
        call chtoby(r_lnam(1,ii),ichain,12)
        call cc(ichain,iwork(k),3)
        k = k+3
        call chtoby(r_lnam(2,ii),ichain,12)
        call cc(ichain,iwork(k),3)
        k = k+3
      enddo
      do ii=1, r_nant
        call i4 (r_lilevu(1,ii),iwork(k),r_lband)
        k = k + r_lband
      enddo
    endif
    if (e%version.le.3) then
      call i4 (r_lnsb,iwork(k),4)
      k = k + 4
    else 
      call i4 (r_lnsb,iwork(k),1)
      k = k + 1
      do ii=1, r_lband
        call i4(r_lsband(1,ii),iwork(k),r_lnsb)
        k = k + r_lnsb
      enddo
    endif
    call r4(r_lcench,iwork(k),r_lband)
    k = k + r_lband
    do ii=1, r_nant
      call r4 (r_ldeloff(1,ii),iwork(k),r_lband)
      k = k + r_lband
    enddo
    call i4(r_lmode,iwork(k),1)
    k = k+1
    ! add r_lilevl and r_iunit 15-sep-1992 RL
    do ii=1, r_nant
      call i4 (r_lilevl(1,ii),iwork(k),r_lband)
      k = k + r_lband
    enddo
    call i4 (r_iunit(1),iwork(k),r_lband)
    k = k + r_lband
    ! add r_band4 if possible
    if (.not.modify .or. k.le.lensec(lineset_sec)) then
      call r4 (r_band4,iwork(k),r_lband)
      k = k + r_lband
    endif
    ! add r_phselect if possible
    if (.not.modify .or. k.le.lensec(lineset_sec)) then
      do ii=1, r_lband
        call i4 (r_phselect(1,ii),iwork(k),r_nant)
        k = k + r_nant
      enddo
    endif
    ! add new receivers parameters if possible
    if (.not.modify.or.k.le.lensec(lineset_sec)) then
      call r8(r_flo2,dwork,r_lband)
      call r4tor4 (dwork,iwork(k),2*r_lband)
      k = k + 2*r_lband
      if (e%version.gt.3) then
        call r4tor4 (r_band2,iwork(k),r_lband)
        k = k + r_lband
      endif
      call r8(r_flo2bis,dwork,r_lband)
      call r4tor4 (dwork,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call i4(r_band2bis,iwork(k),r_lband)
      k = k + r_lband
      call r8(r_flo3,dwork,r_lband)
      call r4tor4 (dwork,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call r8(r_flo4,dwork,r_lband)
      call r4tor4 (dwork,iwork(k),2*r_lband)
      k = k + 2*r_lband
      call i4(r_lpolmode,iwork(k),r_lband)
      k = k + r_lband
      do ii = 1, r_lband
        call i4 (r_lpolentry(1,ii), iwork(k), r_nant)
        k = k + r_nant
      enddo
      if (e%version.le.3) then
        do ii = 1, r_nant
          do j = 1, r_nif
            call r4(r_ifattn(ii,j),ifattn(j,ii,1),1)
          enddo
        enddo
        do ii = 1, r_lband
          call r4 (ifattn,iwork(k),2*r_nant)
          k = k + 2*r_nant
        enddo
      else
        call i4(r_bb,iwork(k),r_lband)
        k = k + r_lband
        call i4(r_if,iwork(k),r_lband)
        k = k + r_lband
        call i4(r_sb,iwork(k),r_lband)
        k = k + r_lband
        call i4(r_stachu,iwork(k),r_lband)
        k = k + r_lband
        call i4(r_nchu,iwork(k),r_lband)
        k = k + r_lband
      endif
      if (e%version.ge.6) then
        do ii = 1, r_lband
          call i4(r_code_stokes(1,ii),iwork(k),r_nbas)
          k = k + r_nbas
        enddo
      endif
    endif
  !
  ! Scanning Section
  elseif (scode.eq.scanning_sec) then
    call i4 (r_scaty,iwork,1)
    k = 2
    call i4 (r_mobil,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_collaz,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_collel,iwork(k),r_nant)
    k = k + r_nant
    !
    if (.not.modify .or. k.le.lensec(scanning_sec)) then
      call r4 (r_corfoc,iwork(k),r_nant)
      k = k + r_nant
    endif
    ! add Pointing Model if possible
    if (.not.modify .or. k.le.lensec(scanning_sec)) then
      call i4(r_pmodel,iwork(k),2) ! r_pmodel and r_npmodel
      k = k+2
      do ii=1, r_nant
        call i4 (r_cpmodel(1,ii),iwork(k),r_npmodel)
        k = k + r_npmodel
      enddo
    endif
    ! add Switching table if possible
    if (.not.modify .or. k.le.lensec(scanning_sec)) then
      call i4(r_nswitch,iwork(k),1)
      k = k + 1
      if (r_nswitch.gt.0) then
        call r4(r_wswitch,iwork(k),r_nswitch)
        k = k + r_nswitch
        call r4(r_pswitch,iwork(k),2*r_nswitch)
        k = k + 2*r_nswitch
      endif
    endif
  !
  ! Atmospheric Calibration Section
  elseif (scode.eq.atparm_sec) then
    call i4 (r_mode,iwork,2)
    call r4 (r_pamb,iwork(3),4)
    k = 7
    if (e%version.le.3) then
      if (r_dobs.lt.(-6406)) then
        temp_dim = r_npol_rec
      else
        temp_dim = mnbcinput + r_nband_widex
      endif
    else
      temp_dim = r_nbb
    endif
    do ii=1, r_nant
      call r4 (r_h2omm(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_tatms(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_tatmi(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_taus(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_taui(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_beef(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_feff(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    ! New generation receiver ipb files had twice feff before 21 Mar 2007
    if ((r_kind.eq.5).and.(r_dobs.lt.-6369)) then
      do ii=1, r_nant
        call r4 (r_feff(1,ii),iwork(k),temp_dim)
        k = k+temp_dim
      enddo
    endif
    do ii=1, r_nant
      call r4 (r_tchop(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_tcold(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_trec(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_gim(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_csky(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_cchop(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_ccold(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_tsyss(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_tsysi(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call r4 (r_ceff(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      call cc (r_cmode(1,ii),iwork(k),temp_dim)
      k = k+temp_dim
    enddo
    do ii=1, r_nant
      if (r_nband_widex.ne.0.or.e%version.gt.3) then
        call r4 (r_jykel(1,ii),iwork(k),temp_dim)
        k = k+temp_dim
      else
        call r4 (r_jykel(1,ii),iwork(k),r_npol_rec)
        k = k+r_npol_rec
      endif
    enddo
    !
    ! Addition aug. 1997
    if (.not.modify .or. k.le.lensec(atparm_sec)) then
      call r4 (r_avwind,iwork(k),4)
      k = k + 4
    endif
    ! 24-jan-1999 extension
    if (.not.modify .or. k.le.lensec(atparm_sec)) then
      call r4 (r_tcabin,iwork(k),r_nant)
      k = k+r_nant
      call r4 (r_tdewar,iwork(k),3*r_nant)
      k = k+3*r_nant
    endif
    ! 01-dec-2006 extension
    if (.not.modify .or. k.le.lensec(atparm_sec)) then
      ! Change of atmospheric calibration on 13-feb-2007
      if (e%version.le.3) then
        if (r_dobs.lt.(-6406)) then
          temp_dim = r_npol_rec
        else
          temp_dim = mnbcinput + r_nband_widex
        endif
      else
        temp_dim = r_nbb
      endif
      do ii=1, r_nant
        call r4 (r_totscale(1,ii),iwork(k),temp_dim)
        k = k+temp_dim
      enddo
    endif
  !
  ! Atmospheric Monitor Section
  elseif (scode.eq.atmon_sec) then
    call i4 (r_nrec_mon,iwork,1)
    call r4 (r_frs_mon,iwork(2),2)
    k = 4
    call r4 (r_h2o_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tatms_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tatmi_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_taus_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_taui_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_feff_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tchop_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tcold_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_trec_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_gim_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_csky_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_cchop_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_ccold_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tsys_mon,iwork(k),r_nant)
    k = k + r_nant
    call cc (r_cmode_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_path_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_tem_mon,iwork(k),r_nant)
    k = k + r_nant
    call r4 (r_dpath_mon,iwork(k),r_nant)
    k = k + r_nant
    if (.not.modify .or. k.le.lensec(atmon_sec)) then
      call r4 (r_magic_mon,iwork(k),2)
      k = k + 2
    endif
    ! Addition 12-jan-1999
    if (.not.modify .or. k.le.lensec(atmon_sec)) then
      call i4 (r_ok_mon,iwork(k),r_nant)
      !c            CALL CC (R_OK_MON,IWORK(K),R_NANT)
      k = k + r_nant
    ! for old files, store correction the old way ...
    else
      test = .true.
      do ii=1, mnant
        test = test.and.(r_ok_mon(ii))
      enddo
      if (.not.test) then
        r_nrec_mon = - iabs(r_nrec_mon)
        call i4 (r_nrec_mon,iwork,1)
      endif
    endif
  !
  ! Bandpass Calibration Section
  elseif (scode.eq.bpcal_sec) then
    temp_dim = 1
    if (e%version.le.3) then
      if (new_receivers) then
        temp_dim = mnbcinput + r_nband_widex
      endif
      if (r_nband_widex.eq.0) then
        temp_dim2 = 10
      else
        temp_dim2 = 12
      endif
    else
      temp_dim = r_nbb
      temp_dim2 = r_nband
    endif
    call i4 (r_bpc,iwork,2)
    k = 3
    do ii=1,temp_dim2
      do j=1,r_nbas
        do l=1,2
          do m=1,temp_dim
            call r4 (r_bpccamp(m,l,j,ii),iwork(k),1)
            k = k + 1
          enddo
        enddo
      enddo
    enddo
    do ii=1,temp_dim2
      do j=1,r_nbas
        do l=1,2
          do m=1,temp_dim
            call r4 (r_bpccpha(m,l,j,ii),iwork(k),1)
            k = k+1
          enddo
        enddo
      enddo
    enddo
    do ii=0,r_bpcdeg
      do j=1,r_lband
        do l=1,r_nbas
          do m=1,2
            do n=1,temp_dim
              call r4 (r_bpclamp(n,m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do ii=0,r_bpcdeg
      do j=1,r_lband
        do l=1,r_nbas
          do m=1,2
            do n=1,temp_dim
              call r4(r_bpclpha(n,m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do ii=1,r_nbas
	do j = 1, 2
	   do l =1, temp_dim
	      call r4 (r_bpcsba(l,j,ii),iwork(k),2) ! complex
	      k = k + 2 
	   enddo
	enddo
    enddo
    if (r_bpfdeg.ge.0) then
      call i4 (r_bpfdeg,iwork(k),1)
      k = k+1
      do ii=1,temp_dim
        call r4 (r_bpflim(ii,1),iwork(k),1)
        k = k+1
        call r4 (r_bpflim(ii,2),iwork(k),1)
        k = k+1
      enddo
      do ii=0,r_bpfdeg
        do j=1,r_nbas
          do l=1,2
            do m=1,temp_dim
              call r4 (r_bpfamp(m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
      do ii=0,r_bpfdeg
        do j=1,r_nbas
          do l=1,2
            do m=1,temp_dim
              call r4 (r_bpfpha(m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    endif
  !
  ! Antenna Bandpass Calibration Section
  elseif (scode.eq.abpcal_sec) then
    temp_dim = 1
    if (e%version.le.3) then
      if (new_receivers) then
        temp_dim = mnbcinput + r_nband_widex
      endif
      if (r_nband_widex.eq.0) then
        temp_dim2 = 10
      else
        temp_dim2 = 12
      endif
    else
      temp_dim = r_nbb
      temp_dim2 = r_nband
    endif
    call i4 (r_abpc,iwork,2)
    k = 3
    do ii=1,temp_dim2
      do j=1,r_nant
        do l=1,2
          do m=1,temp_dim
            call r4 (r_abpccamp(m,l,j,ii),iwork(k),1)
            k = k + 1
          enddo
        enddo
      enddo
    enddo
    do ii=1,temp_dim2
      do j=1,r_nant
        do l=1,2
          do m=1,temp_dim
            call r4 (r_abpccpha(m,l,j,ii),iwork(k),1)
            k = k+1
          enddo
        enddo
      enddo
    enddo
    do ii=0,r_abpcdeg
      do j=1,r_lband
        do l=1,r_nant
          do m=1,2
            do n=1,temp_dim
              call r4 (r_abpclamp(n,m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do ii=0,r_abpcdeg
      do j=1,r_lband
        do l=1,r_nant
          do m=1,2
            do n=1,temp_dim
              call r4(r_abpclpha(n,m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    enddo
    do ii=1,r_nant
	do j = 1, 2
	   do l =1, temp_dim
	      call r4 (r_abpcsba(l,j,ii),iwork(k),2) ! complex
	      k = k + 2 
	   enddo
	enddo
    enddo
    if (r_abpfdeg.ge.0) then
      call i4 (r_abpfdeg,iwork(k),1)
      k = k+1
      do ii=1,temp_dim
        call r4 (r_abpflim(ii,1),iwork(k),1)
        k = k+1
        call r4 (r_abpflim(ii,2),iwork(k),1)
        k = k+1
      enddo
      do ii=0,r_abpfdeg
        do j=1,r_nant
          do l=1,2
            do m=1,temp_dim
              call r4 (r_abpfamp(m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
      do ii=0,r_abpfdeg
        do j=1,r_nant
          do l=1,2
            do m=1,temp_dim
              call r4 (r_abpfpha(m,l,j,ii),iwork(k),1)
              k = k+1
            enddo
          enddo
        enddo
      enddo
    endif
  !
  ! "Instrumental" calibration section
 elseif (scode.eq.ical_sec) then
    call i4 (r_ic,iwork,2)
    k = 3
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nbas
                do m=0,r_icdeg
                   call r4 (r_icamp(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_icdeg
          do l=1,r_nbas
             do j=1,2
                do ii=1,r_npol_rec
                   call r4 (r_icamp(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nbas
                do m=0,r_icdeg
                   call r4 (r_icpha(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_icdeg 
          do l=1,r_nbas 
             do j=1,2
                do ii=1,r_npol_rec
                   call r4 (r_icpha(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nbas
                call r4(r_icrms(ii,j,l,1),iwork(k),1)
                k = k + 1
                call r4(r_icrms(ii,j,l,2),iwork(k),1)
                k = k + 1
             enddo
          enddo
       enddo
    else
       do l=1,r_nbas
          do j=1,2
             do ii=1,r_npol_rec
                call r4(r_icrms(ii,j,l,1),iwork(k),1)
                k = k + 1
                call r4(r_icrms(ii,j,l,2),iwork(k),1)
                k = k + 1
             enddo
          enddo
       enddo
    endif
  !
  ! Antenna "Instrumental" Calibration Section
 elseif (scode.eq.aical_sec) then
    call i4 (r_aic,iwork,2)
    k = 3
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nant
                do m=0,r_aicdeg
                   call r4 (r_aicamp(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_aicdeg
          do l=1,r_nant
             do j=1,2
                do ii=1,r_npol_rec
                   call r4 (r_aicamp(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nant
                do m=0,r_aicdeg
                   call r4 (r_aicpha(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    else
       do m=0,r_aicdeg
          do l=1,r_nant
             do j=1,2
                do ii=1,r_npol_rec
                   call r4 (r_aicpha(ii,j,l,m),iwork(k),1)
                   k = k + 1
                enddo
             enddo
          enddo
       enddo
    endif
    if (new_receivers) then
       do ii=1,r_npol_rec
          do j=1,2
             do l=1,r_nant
                call r4(r_aicrms(ii,j,l,1),iwork(k),1)
                k = k + 1
                call r4(r_aicrms(ii,j,l,2),iwork(k),1)
                k = k + 1
             enddo
          enddo
       enddo
    else
       do l=1,r_nant
          do j=1,2
             do ii=1,r_npol_rec
                call r4(r_aicrms(ii,j,l,1),iwork(k),1)
                k = k + 1
                call r4(r_aicrms(ii,j,l,2),iwork(k),1)
                k = k + 1
             enddo
          enddo
       enddo
    endif
    if (e%version.ge.5) then
      do ii=1, r_nbb
        call r4(r_dx(1,ii),iwork(k),2*r_nant)
        k = k + 2*r_nant
      enddo
      do ii=1, r_nbb
        call r4(r_dy(1,ii),iwork(k),2*r_nant)
        k = k + 2*r_nant
      enddo
    endif
  !
  ! Data description Section
  elseif (scode.eq.descr_sec) then
    if (e%version.eq.1) then
      ! r_ndump
      k = 1          
      call i4(r_ndump,iwork(k),1)
      ! r_ldpar
      call i8toi4_fini(r_ldpar,i4value,1,error)
      if (error) then
         write (chain, '(A,I0)') 'Header data length too large for '// &
           'v1: r_ldpar = ',r_ldpar
         l = lenc(chain)
         call message(8,3,'WSEC',chain(1:l))
         error = .true.
         return
      endif
      k = k+1
      call i4(i4value,iwork(k),1)
      ! r_ldatc
      call i8toi4_fini(r_ldatc,i4value,1,error)
      if (error) then
         write (chain, '(A,I0)') 'Continuum data length too large for '// &
           'v1: r_ldatc = ',r_ldatc
         l = lenc(chain)
         call message(8,3,'WSEC',chain(1:l))
         error = .true.
         return
      endif
      k = k+1
      call i4(i4value,iwork(k),1)
      ! r_ldatl
      call i8toi4_fini(r_ldatl,i4value,1,error)
      if (error) then
         write (chain, '(A,I0)') 'Header data length too large for '// &
           'v1: r_ldatl = ',r_ldatl
         l = lenc(chain)
         call message(8,3,'WSEC',chain(1:l))
         error = .true.
         return
      endif
      k = k+1
      call i4(i4value,iwork(k),1)
      ! r_ldump
      call i8toi4_fini(r_ldump,i4value,1,error)
      if (error) then
         write (chain, '(A,I0)') 'Header data length too large for v1: '// &
           'v1: r_ldump = ',r_ldump
         l = lenc(chain)
         call message(8,3,'WSEC',chain(1:l))
         error = .true.
         return
      endif
      k = k+1
      call i4(i4value,iwork(k),1)
      ! r_ndatl
      k = k+1
      call i4 (r_ndatl,iwork(k),1)    
      k = k+1
    elseif (e%version.ge.2) then
      call i4(r_ndump,iwork,2)
      call i8(r_ldpar,lwork,4)
      call w4tow4(lwork,iwork(3),8) 
    endif
  !
  ! Data modifier section
  elseif (scode.eq.modify_sec) then
    call r4(r_dmaflag,iwork,r_nant)
    k = 1+r_nant
    call r4(r_dmbflag,iwork(k),r_nbas)
    k = k+r_nbas
    ! r_dmatfac
    if (e%version.le.3) then
      do ii=1, r_nant
        do j=1, 2
          do l=1, r_nbb
            m = r_mapbb(l)
            n = r_mappol(ii,l)
            dmatfac(n,j,ii) = r_dmatfac(m,j,ii)
          enddo
          call r4 (dmatfac(1,j,ii),iwork(k),r_npol_rec)
          k = k + r_npol_rec
        enddo
      enddo
    else
      do ii=1, r_nant
        do j=1, 2
          call r4(r_dmatfac(1,j,ii),iwork(k),r_nbb)
          k = k + r_nbb
        enddo
      enddo
    endif
    ! r_dmdelay
    if (e%version.le.3) then
      do ii=1, r_nant
        do j=1, r_nbb
          m = r_mappol(ii,j)
          dmdelay(m,ii) = r_dmdelay(j,ii)
        enddo
        call r4 (dmdelay(1,ii),iwork(k),r_npol_rec)
        k = k + r_npol_rec
      enddo
    else
      do ii=1, r_nant
        call r4(r_dmdelay(1,ii),iwork(k),r_nbb)
        k = k + r_nbb
      enddo
    endif
    !
    do ii=1, r_nband
      call r4(r_dmcamp(1,1,ii),iwork(k),2*r_nbas)
      k = k+2*r_nbas
      call r4(r_dmcpha(1,1,ii),iwork(k),2*r_nbas)
      k = k+2*r_nbas
    enddo
    do ii=1, r_lband
      call r4(r_dmlamp(1,1,ii),iwork(k),2*r_nbas)
      k = k+2*r_nbas
      call r4(r_dmlpha(1,1,ii),iwork(k),2*r_nbas)
      k = k+2*r_nbas
      call r4(r_dmldph(1,1,ii),iwork(k),2*r_nbas)
      k = k+2*r_nbas
    enddo
    ! Apr-2010 extension
    if (e%version.le.3) then
      dmsaflag = 0
      do j= 1, r_nant
        do ii = 1, 16
           if (r_dmsaflag(ii,j))  dmsaflag(j)= ibset(dmsaflag(j),ii-1)
           if (r_dmsaflag(ii+mbands,j))  dmsaflag(j)= ibset(dmsaflag(j),ii+15)
        enddo
      enddo
      dmsbflag = 0
      do j = 1, r_nbas
        do ii = 1, 16
           if (r_dmsbflag(ii,j)) dmsbflag(j)= ibset(dmsbflag(j),ii-1)
           if (r_dmsbflag(ii+mbands,j)) dmsbflag(j)= ibset(dmsbflag(j),ii+15)
        enddo
      enddo
      if (.not.modify .or. k.le.lensec(modify_sec).and.r_nband_widex.ne.0) then
        call i4 (dmsaflag,iwork(k),r_nant)
        k = k + r_nant
        call i4 (dmsbflag,iwork(k),r_nbas)
        k = k + r_nbas
      else
        ! Copy spectral flags to r_dmaflag 
        do j = 1, r_nant
           do ii=1, 10 ! Ignore spectral flags greater than C10 (no room)
              mask = 0
              mask = iand(dmsaflag(j),ior(mask,ishft(1,ii-1)))
              if (mask.ne.0) then
                r_dmaflag(j) = ibset(r_dmaflag(j),ii-1)
                dmsaflag(j) = ibclr(dmsaflag(j),ii-1)
              endif
           enddo
           do ii=1,8 ! Ignore spectral flags greater than L08 (no room)
              mask = 0
              mask = iand(dmsaflag(j),ior(mask,ishft(1,ii-1+mbands)))
              if (mask.ne.0) then
                r_dmaflag(j) = ibset(r_dmaflag(j),ii+9)
                dmsaflag(j) = ibclr(dmsaflag(j),ii-1+mbands)
              endif
           enddo
        enddo
        call r4(r_dmaflag,iwork,r_nant)
        ! ... and the same for baselines.
        do j = 1, r_nbas
           do ii=1, 10
              mask = 0
              mask = iand(dmsbflag(j),ior(mask,ishft(1,ii-1)))
              if (mask.ne.0) then
                r_dmbflag(j) = ibset(r_dmbflag(j),ii-1)
                dmsbflag(j) = ibclr(dmsbflag(j),ii-1)
              endif
           enddo
           do ii=1,8
              mask = 0
              mask = iand(dmsbflag(j),ior(mask,ishft(1,ii-1+mbands)))
              if (mask.ne.0) then
                r_dmbflag(j) = ibset(r_dmbflag(j),ii+9)
                dmsbflag(j) = ibclr(dmsbflag(j),ii-1+mbands)
              endif
           enddo
        enddo
        call r4(r_dmbflag,iwork(1+r_nant),r_nbas)
      endif
    else
      nword= int(r_lband/32)+1
      do ii=1, r_nant
        l = 0
        do j=1, nword
          dummy = 0
          do m=1, 32
            l = l+1
            if (l.le.r_lband) then
              if (r_dmsaflag(l,ii)) dummy = ibset(dummy,m-1)
            endif
          enddo
          call i4(dummy,iwork(k),1)
          k = k+1
        enddo
      enddo
      do ii=1, r_nant
        l = 0
        do j=1, nword
          dummy = 0
          do m=1, 32
            l = l+1
            if (l.le.r_lband) then
              if (r_dmsaflag(l+mbands,ii)) dummy = ibset(dummy,m-1)
            endif
          enddo
          call i4(dummy,iwork(k),1)
          k = k+1
        enddo
      enddo
      do ii=1, r_nbas
        l = 0
        do j=1, nword
          dummy = 0
          do m=1, 32
            l = l+1
            if (l.le.r_lband) then
              if (r_dmsbflag(l,ii)) dummy = ibset(dummy,m-1)
            endif
          enddo
          call i4(dummy,iwork(k),1)
          k = k+1
        enddo
      enddo
      do ii=1, r_nbas
        l = 0
        do j=1, nword
          dummy = 0
          do m=1, 32
            l = l+1
            if (l.le.r_lband) then
              if (r_dmsbflag(l+mbands,ii)) dummy = ibset(dummy,m-1)
            endif
          enddo
          call i4(dummy,iwork(k),1)
          k = k+1
        enddo
      enddo
    endif
  !
  ! Data file section
  elseif (scode.eq.file_sec) then
    call i4(r_dxnum,iwork,1)
    k = 2
    kk= 32                     ! KK = LENC(R_DFILE)
    call chtoby(r_dfile,ichain,kk)
    kk= 8                      ! KK = INT(KK/4)+1   ! words
    call cc(ichain,iwork(k),kk)
    k = k+kk
  !
  ! Water vapor radiometer section
  elseif (scode.eq.wvr_sec) then
    call i4(r_wvrnch,iwork,r_nant)
    k = 1+r_nant
    do ii=1, r_nant
      call r4(r_wvrfreq(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrbw(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
    enddo
    call r4(r_wvrtamb,iwork(k),r_nant)
    k = k+r_nant
    call r4(r_wvrtpel,iwork(k),r_nant)
    k = k+r_nant
    do ii=1, r_nant
      call r4(r_wvrtcal(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrref(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvraver(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvramb(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrtrec(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrfeff(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      !*!
      call r4(r_wvrlabtcal(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrlabtrec(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrlabtdio(1,ii),iwork(k),mwvrch)
      k = k+mwvrch             !*!
    enddo
    call i4(r_wvrmode,iwork(k),r_nant)
    k = k+r_nant
    call r4(r_wvrh2o,iwork(k),r_nant)
    k = k+r_nant
    call r4(r_wvrpath,iwork(k),r_nant)
    k = k+r_nant
    do ii=1, r_nant
      call r4(r_wvrtsys(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrdpath(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrfpath(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      call r4(r_wvrliq(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
      !*!
      call r4(r_wvrdcloud(1,ii),iwork(k),mwvrch)
      k = k+mwvrch
    enddo
    call r4(r_wvrtatm,iwork(k),r_nant)
    k = k+r_nant
    call i4(r_wvrqual,iwork(k),r_nant)
    k = k+r_nant
    if (e%version.ge.3) then
      call i4(r_wvravertime,iwork(k),1)
      k = k+1
    endif
  !
  ! S.D.Holo. section
  elseif (scode.eq.alma_sec) then
    call r4 (r_trandist,iwork,2)
    k = 3
    call i4 (r_subscan,iwork(k),2)
    k = k+2
    call chtoby(r_execblock,ichain,256)
    call cc(ichain,iwork(k),64)
    k = k+64
    do ii=1, r_nant
      call chtoby(r_antennaname(ii),ichain,32)
      call cc(ichain,iwork(k),8)
      k = k+8
    enddo
    do ii=1, r_nant
      call chtoby(r_antennatype(ii),ichain,32)
      call cc(ichain,iwork(k),8)
      k = k+8
    enddo
    call r4tor4(r_dishdiameter,iwork(k),r_nant)
    k = k+r_nant
  elseif (scode.eq.monitor_sec) then
    k = 1
    do ii=1, 2
      call r4 (r_attenuation_hiq(1,ii), iwork(k), r_nant)
      k = k + r_nant
    enddo
    do ii=1, r_nif
      call r4 (r_laser_level(1,ii), iwork(k),  r_nant)
      k = k+r_nant
    enddo
    if (e%version.le.3) then
      call r4 (r_widex_temperature, iwork(k), 4)
      k = k+4
    endif
  elseif (scode.eq.status_sec) then
    k = 1
    call chtoby(r_status,ichain,12)
    call cc(ichain,iwork(k),3)
    k = k+3
    call chtoby(r_substatus,ichain,12)
    call cc(ichain,iwork(k),3)
    k = k+3
    call chtoby(r_comm,ichain,140)
    call cc(ichain,iwork(k),35)
    k = k+35
  elseif (scode.eq.vlbi_sec) then
    ! No observation version as version 5 was never written
    k = 1
    call i4 (r_refant,iwork(k),1)
    k = k+1
    call i4 (r_comparison_ant,iwork(k),1)
    k = k+1
    call i4 (r_nant_vlbi,iwork(k),1)
    k = k+1
    call r4 (r_maser_gps_diff,iwork(k),1)
    k = k+1
    call r4 (r_efftsys,iwork(k),r_nbb)
    k = k+r_nbb
    call i4 (r_antmask,iwork(k),r_nant_vlbi)
    k = k+r_nant_vlbi
  endif
  !
  ! Now write it
!  print *,"cwall section ",scode," section length ",k-1
  len = k-1
end subroutine scwsec
