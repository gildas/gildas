subroutine splinant(iy, m, nbas, iant, jant, iref,   &
    ncap7, nant, x, y, w,   &
    k, wk1, wk2, wk3, ss, c, error)
  !---------------------------------------------------------------------
  !     splinant computes a weighted least-squares approximation
  !     to an arbitrary set of data points, either amplitude or phase.
  !     with knots prescribed by the user.
  ! parameters:
  !     iy            I   Input    1 for log(amplitude), 2 for phase
  !     m             I   Input    the number of data points for each baseline
  !     nbas          I   Input    the number of baselines
  !     iant(nbas)    I   Input    start antenna for each baseline
  !     jant(nbas)    I   Input    end  antenna for each baseline
  !     iref          i   Input    Reference antenna for phases
  !     ncap7         I   Input    number of knots for splines (see e02baf)
  !     nant          I   Input    the number of antennas
  !     X(m)          R8  Input    the data abscissae
  !     Y(m,nbas)     R8  Input    the data values
  !     W(m,mbas)     R8  Input    weights
  !     k(ncap7)      R8  Input    knots for the splines (inners + 4 at each end)
  !     wk1(4,m)    R8  Output   wk space
  !     wk2(4*nant,ncap7*nant)
  !                   R8  Output   work space
  !     wk3(ncap7*nant)
  !                   R8  Output   work space
  !     ss(nbas)      R8  Output   rms of fit for each baseline
  !     c(nant,ncap7) R8  Output   the spline coefficients (ncap3 values)
  !---------------------------------------------------------------------
  integer :: iy                     !
  integer :: m                      !
  integer :: nbas                   !
  integer :: iant(nbas)             !
  integer :: jant(nbas)             !
  integer :: iref                   !
  integer :: ncap7                  !
  integer :: nant                   !
  real*8 :: x(m)                    !
  real*8 :: y(m,nbas)               !
  real*8 :: w(m,nbas)               !
  real*8 :: k(ncap7)                !
  real*8 :: wk1(4,m)                !
  real*8 :: wk2(4*nant,ncap7*nant)  !
  real*8 :: wk3(ncap7*nant)         !
  real*8 :: ss(nbas)                !
  real*8 :: c(nant,ncap7)           !
  logical :: error                  !
  ! Global
  integer :: zant
  include 'gbl_pi.inc'
  ! Local
  real*8 :: norm, tol
  parameter (tol=1e-14)
  integer :: i, ia, ib, jj, nantm1, iter, ja, ibd, icol, nbd
  integer :: kn, kp, j, jold,  l, ncap3, ncap, ncapm1
  real*8 ::  d4, d5, d6, d7,  d8, d9,  e2, e3, e4, e5, k1, k2, k3
  real*8 ::  k4, k5, k6, n1, n2, n3, wi, xi, wn, ww, www, test, yi
  !------------------------------------------------------------------------
  ! Code
  !
  call  spline_check(m, ncap7, x, k, wk1, error)
  if (error) return
  !
  ! Check that the weights are (strictly) positive.
  !
  do  i=1,m
    do ib = 1, nbas
      if (w(i,ib).lt.0.0d0) then
        call message(6,4,'SPLINANT','Weights not positive')
        error = .true.
        return
      endif
    enddo
  enddo
  ncap = ncap7 - 7
  ncapm1 = ncap - 1
  ncap3 = ncap + 3
  nbd = 4*nant
  nantm1 = nant-1
  !
  ! First loop on data abscissae to compute the spline values in wk1
  !
  j = 0
  jold = 0
  do i = 1, m
    !
    ! for the data point  (x(i), y(i,ib))  determine an interval
    ! k(j + 3) .le. x .lt. k(j + 4)  containing  x(i).  (in the
    ! case  j + 4 .eq. ncap  the second equality is relaxed to
    ! include equality).
    !
    xi = x(i)
    do while (xi.ge.k(j+4) .and. j.le.ncapm1)
      j = j + 1
    enddo
    if (j.ne.jold) then
      !
      ! set certain constants relating to the interval
      ! k(j + 3) .le. x .le. k(j + 4)
      ! (i.e. the jth non vanishing interval)
      !
      k1 = k(j+1)
      k2 = k(j+2)
      k3 = k(j+3)
      k4 = k(j+4)
      k5 = k(j+5)
      k6 = k(j+6)
      d4 = 1.0d0/(k4-k1)
      d5 = 1.0d0/(k5-k2)
      d6 = 1.0d0/(k6-k3)
      d7 = 1.0d0/(k4-k2)
      d8 = 1.0d0/(k5-k3)
      d9 = 1.0d0/(k4-k3)
      jold = j
    endif
    !
    ! compute and store in  wk1(l,i) (l = 1, 2, 3, 4)  the values
    ! of the four normalized cubic b-splines which are non-zero at
    ! x=x(i), i.e. the splines of indexes j, j+1, j+2, j+3.
    !
    e5 = k5 - xi
    e4 = k4 - xi
    e3 = xi - k3
    e2 = xi - k2
    n1 = e4*d9*d7
    n2 = e3*d9*d8
    n3 = e3*n2*d6
    n2 = (e2*n1+e5*n2)*d5
    n1 = e4*n1*d4
    wk1(4,i) = e3*n3
    wk1(3,i) = e2*n2 + (k6-xi)*n3
    wk1(2,i) = (xi-k1)*n1 + e5*n2
    wk1(1,i) = e4*n1
  enddo
  !
  ! Amplitude case is simple...
  !
  if (iy.eq.1) then
    nbd = 4*nant
    do i=1,nant*ncap3
      do l=1,nbd
        wk2(l,i) = 0.0d0
      enddo
      wk3(i) = 0.0d0
    enddo
    j = 0
    do i=1, m
      xi = x(i)
      do while (xi.ge.k(j+4) .and. j.le.ncapm1)
        j = j + 1
      enddo
      !
      ! store the upper-triangular part of the normal equations in wk2
      !
      !            write(6,*) i,j,(wk1(kn,i),kn=1,4)
      do kn=0, 3
        wn = wk1(kn+1,i)
        do kp = kn, 3
          icol = (j+kp-1)*nant
          ibd = nbd-(kp-kn)*nant
          ww = wn*wk1(kp+1,i)
          do ib=1,nbas
            wi = w(i,ib)
            if (wi.gt.0) then
              ia = iant(ib)
              ja = jant(ib)
              www = wi*ww
              wk2(ibd,icol+ia) = wk2(ibd,icol+ia)+www
              wk2(ibd,icol+ja) = wk2(ibd,icol+ja)+www
              wk2(ibd+ia-ja,icol+ja) =   &
                wk2(ibd+ia-ja,icol+ja) + www
              if (kp.gt.kn) then
                wk2(ibd+ja-ia,icol+ia) =   &
                  wk2(ibd+ja-ia,icol+ia) + www
              endif
            endif
          enddo
        enddo
        do ib=1, nbas
          ia = iant(ib)
          ja = jant(ib)
          wi = w(i,ib)*wn*y(i,ib)
          jj = (j+kn-1)*nant
          wk3(ia+jj) = wk3(ia+jj) + wi
          wk3(ja+jj) = wk3(ja+jj) + wi
        enddo
      enddo
    enddo
    !
    ! Solve the system of normal equations by first computing the Cholesky
    ! factorization
    call mth_dpbtrf ('SPLINANT',   &
      'U',ncap3*nant,nbd-1,wk2,4*nant,error)
    if (error) return
    call mth_dpbtrs ('SPLINANT',   &
      'U',ncap3*nant,nbd-1,1,wk2,4*nant,   &
      wk3,ncap3*nant,error)
    if (error) return
    do j=1,ncap3
      do i=1,nant
        c(i,j) = wk3(i+(j-1)*nant)
      enddo
    enddo
  ! Phase is more complicated ...
  elseif (iy.eq.2) then
    nbd = 4*nantm1
    do i=1,nant
      do j=1,ncap3
        c(i,j) = 0.0d0
      enddo
    enddo
    !     start iterating
    norm = 1e10
    iter = 0
    do while (norm.gt.tol .and. iter.lt.100)
      iter = iter+1
      do i=1,nantm1*ncap3
        do l=1,nbd
          wk2(l,i) = 0.0d0
        enddo
        wk3(i) = 0.0d0
      enddo
      j = 0
      i = 0
      xi = x(1)
      do i=1, m
        xi = x(i)
        do while (xi.ge.k(j+4) .and. j.le.ncapm1)
          j = j + 1
        enddo
        !
        ! store the upper-triangular part of the normal equations in wk2
        !
        do kn=0, 3
          wn = wk1(kn+1,i)
          do kp = kn, 3
            icol = (j+kp-1)*nantm1
            ibd = nbd-(kp-kn)*nantm1
            ww = wn*wk1(kp+1,i)
            do ib=1,nbas
              wi = w(i,ib)
              if (wi.gt.0) then
                www = wi*ww
                ia = zant(iant(ib),iref)
                ja = zant(jant(ib),iref)
                if (ia.ne.0) then
                  wk2(ibd,icol+ia) = wk2(ibd,icol+ia)+www
                endif
                if (ja.ne.0) then
                  wk2(ibd,icol+ja) = wk2(ibd,icol+ja)+www
                endif
                if (ia.ne.0 .and. ja.ne.0) then
                  wk2(ibd+ia-ja,icol+ja) =   &
                    wk2(ibd+ia-ja,icol+ja) - www
                  if (kp.gt.kn) then
                    wk2(ibd+ja-ia,icol+ia) =   &
                      wk2(ibd+ja-ia,icol+ia) - www
                  endif
                endif
              endif
            enddo
          enddo
        enddo
        do ib=1, nbas
          if (w(i,ib).gt.0) then
            ia = iant(ib)
            ja = jant(ib)
            yi = y(i,ib)
            if (j.le.ncap3) then
              do kn = 0, 3
                wn = wk1(kn+1,i)
                if (j+kn.le.ncap3) then
                  yi = yi+c(ia,j+kn)*wn
                  yi = yi-c(ja,j+kn)*wn
                endif
              enddo
            else
              yi = 0
            endif
            if (norm.lt.1e9) then
              yi = sin(yi)
            endif
            ia = zant(iant(ib),iref)
            ja = zant(jant(ib),iref)
            do kn = 0, 3
              wn = wk1(kn+1,i)
              wi = w(i,ib)*wn*yi
              jj = (j+kn-1)*nantm1
              if (ia.ne.0) then
                wk3(ia+jj) = wk3(ia+jj) - wi
              endif
              if (ja.ne.0) then
                wk3(ja+jj) = wk3(ja+jj) + wi
              endif
            enddo
          endif
        enddo
      enddo
      !
      ! Solve the system of normal equations by first computing the Cholesky
      ! factorization
      call mth_dpbtrf ('SPLINANT',   &
        'U',ncap3*nantm1,nbd-1,wk2,4*nant,error)
      if (error) return
      call mth_dpbtrs ('SPLINANT',   &
        'U',ncap3*nantm1,nbd-1,1,wk2,4*nant,   &
        wk3,ncap3*nant,error)
      if (error) return
      !
      ! Add the result to c:
      norm = 0
      do j=1,ncap3
        do ia=1,nant
          i = zant(ia,iref)
          if (i.ne.0) then
            ww = wk3(i+(j-1)*nantm1)
            c(ia,j) = c(ia,j)+ww
            norm = norm+ww**2
          endif
        enddo
      enddo
    !            write(6,*) 'norm' , norm
    enddo
  !         write(6,*) 'ncap3, j,(c(i,j),i=1, nant)'
  !         do j=1,ncap3
  !            write(6,*) ncap3, j,(c(i,j),i=1, nant)
  !         enddo
  endif
  !
  ! loop over data points to get rms
  do i=1, nbas
    ss(i) = 0
    wk2(i,1) = 0
  enddo
  j = 0
  do i = 1, m
    xi = x(i)
    do while (xi.ge.k(j+4) .and. j.le.ncapm1)
      j = j + 1
    enddo
    do ib=1,nbas
      if (w(i,ib).gt.0) then
        ia = iant(ib)
        ja = jant(ib)
        test = 0
        do kn = 0, 3
          wn = wk1(kn+1,i)
          if (iy.eq.1) then
            test = test+(c(ia,j+kn)+c(ja,j+kn))*wn
          else
            test = test+(-c(ia,j+kn)+c(ja,j+kn))*wn
          endif
        enddo
        test = y(i,ib)-test
        test = mod(test+11*pi,2*pi)-pi
        ss(ib) = ss(ib)+w(i,ib)*test**2
        wk2(ib,1) = wk2(ib,1)+w(i,ib)
      endif
    enddo
  enddo
  do ib=1, nbas
    if (wk2(ib,1).gt.0) then
      ss(ib) = sqrt(ss(ib)/wk2(ib,1))
    else
      ss(ib) = 0
    endif
  enddo
  !
  return
end subroutine splinant
!
subroutine spline_check(m, ncap7, x, k, wk, error)
  !---------------------------------------------------------------------
  !     splinant computes a weighted least-squares approximation
  !     to an arbitrary set of data points by a cubic spline
  !     with knots prescribed by the user.
  !---------------------------------------------------------------------
  integer :: m                      !
  integer :: ncap7                  !
  real*8 :: x(m)                    !
  real*8 :: k(ncap7)                !
  real*8 :: wk(m)                   !
  logical :: error                  !
  ! Local
  integer :: i, j, l, ncap3, ncap, ncapm1, r
  real*8 :: k0, k4
  !------------------------------------------------------------------------
  ! Code
  !
  ! check that the values of  m  and  ncap7  are reasonable
  if (ncap7.lt.8 .or. m.lt.ncap7-4) go to 991
  ncap = ncap7 - 7
  ncapm1 = ncap - 1
  ncap3 = ncap + 3
  !
  ! In order to define the full b-spline basis, augment the
  ! prescribed interior knots by knots of multiplicity four
  ! at each end of the data range.
  !
  do  j=1,4
    i = ncap3 + j
    k(j) = x(1)
    k(i) = x(m)
  enddo
  !
  ! test the validity of the data.
  !
  ! check that the knots are ordered and are interior
  ! to the data interval.
  !
  if (k(5).le.x(1) .or. k(ncap3).ge.x(m)) then
    call message(6,4,'SPLINE_CHECK','Knots outside range')
    error = .true.
    return
  else
    do  j=4,ncap3
      if (k(j).gt.k(j+1)) then
        call message(6,4,'SPLINE_CHECK','Knots non increasing')
        error = .true.
        return
      endif
    enddo
  endif
  !
  ! check that the data abscissae are ordered, then form the
  ! array  wk  from the array  x.  the array  wk  contains
  ! the set of distinct data abscissae.
  !
  wk(1) = x(1)
  j = 2
  do i=2,m
    if (x(i).lt.wk(j-1)) then
      call message(6,4,'SPLINE_CHECK',   &
        'Data abscissae not ordered')
      error = .true.
      return
    elseif (x(i).gt.wk(j-1)) then
      wk(j) = x(i)
      j = j + 1
    endif
  enddo
  r = j - 1
  !
  ! check that there are sufficient distinct data abscissae for
  ! the prescribed number of knots.
  !
  if (r.lt.ncap3) goto 991
  !
  ! check the first  s  and the last  s  Schoenberg-Whitney
  ! conditions ( s = min(ncap - 1, 4) ).
  !
  do j=1,4
    if (j.ge.ncap) return
    i = ncap3 - j + 1
    l = r - j + 1
    if (wk(j).ge.k(j+4) .or. k(i).ge.wk(l)) goto 991
  enddo
  !
  ! check all the remaining schoenberg-whitney conditions.
  !
  if (ncap.gt.5) then
    r = r - 4
    i = 3
    do j= 5, ncapm1
      k0 = k(j+4)
      k4 = k(j)
      do while (wk(i).le.k4)
        i = i + 1
      enddo
      if (i.gt.r .or. wk(i).ge.k0) then
         call message(6,4,'SPLINE_CHECK', 'No data between two successive knots')
         goto 991
      endif
    enddo
  endif
  return
991 call message(6,4,'SPLINE_CHECK', 'Too many knots')
  write(6,*) 'Data abscissae: ',(wk(i),i=1,r)
  write(6,*) 'Knots: ',(k(i),i=1,ncap7)
  error = .true.
  return
end subroutine spline_check
!
function zant(i,r)
  integer :: zant                   !
  integer :: i                      !
  integer :: r                      !
  if (i.eq.r) then
    zant = 0
  elseif (i.gt.r) then
    zant = i-1
  else
    zant = i
  endif
  return
end function zant
