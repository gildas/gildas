! parameters
      INTEGER ANT_TYPE, NRING, MRING, MPAN, MM, TYPE_BURE, TYPE_BURE2,  &
     &     TYPE_VX12M, TYPE_AEC12M, TYPE_MELCO12M_1, TYPE_MELCO12M_2,   &
     &     TYPE_MELCO12M_3, TYPE_MELCO12M_4, TYPE_MELCO_7M,           &
     &     TYPE_IRAM30M, MBLOCK, mhole, maxPol,                      &
     &     numPol	
      PARAMETER (MRING=12, MPAN=48, MM=5, TYPE_BURE=1, TYPE_BURE2=2,    &
     &     TYPE_VX12M=3, TYPE_AEC12M=4, TYPE_MELCO12M_1=5,              &
     &     TYPE_MELCO12M_2=6, TYPE_MELCO12M_3=7, TYPE_MELCO12M_4=8,     &
     &     TYPE_MELCO_7M=9, TYPE_IRAM30M=10,                            &
     &     MBLOCK=12,	mhole=10, maxPol=100)
      INTEGER NPAN(MRING), NSCR(MRING), KXSCR(MM,MRING),                &
     &     KYSCR(MM,MRING),   MMODE(MRING),  NBLOCK, I4, nhole,         &
           modes(mm,mring), mPol(maxpol), nPol(maxpol)
      REAL RAY(0:MRING), DIAMETER, FOCUS, C2,                           &
     &     XSCR(MM,MRING), YSCR(MM,MRING), HBLOCK(MBLOCK),              &
     &     RBLOCK(MBLOCK), rhole(mhole), xhole(mhole), yhole(mhole),    &
     &     coeffpol(maxpol)
      logical vblock, front_view
      COMMON /PANEL/ ANT_TYPE, DIAMETER, FOCUS, NPAN, NRING, RAY, C2,   &
     &     NSCR, XSCR, YSCR, KXSCR, KYSCR, MMODE, NBLOCK, RBLOCK,       &
     &     HBLOCK, I4, nhole, rhole, xhole, yhole,                      &
     &     vblock, front_view, modes, numpol, mpol, npol, coeffpol
