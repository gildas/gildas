subroutine write_sdm_Feed(holodata,error)
  !---------------------------------------------------------------------
  ! Write the Feed SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_enumerations
  use sdm_feed
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical holodata, error
  ! Local
  type (FeedRow) :: fRow
  type (FeedKey) :: fKey
  type (FeedOpt) :: fOpt
  integer ia, ic, isb, ilc, ier
  integer, parameter :: minalloc = 1
  character, parameter  :: sdmTable*4 = 'Feed'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !     normal case
  !
  if (.not.holodata) then
    !
    !         fRow%numReceptors = 2
    ! one only for CURRENT Bure data !!!
    ! now get from r_npol_rec
    !cc         fRow%numReceptors = 1
    fRow%numReceptor = r_npol_rec
    call allocFeedRow(frow, fRow%numReceptor,  error)
    if (error) return
    !     NOW OPTIONAL IN SS2 fRow%feedNum = 0
    !     loop on antennas
    do ia=1, r_nant
      ! *TBD* *Check*
      !     These should be in Bure data for the new receivers. They are not
      !     at this time.
      fRow%beamOffset = 0.     !! (/ 0.d0, 0.d0  /)
      !     *TBD* What is Focus Reference?
      !     Useless entry I guess ...
      fRow%focusReference = 0.0d0
      !     *TBD* It's the set of illumOffset, illumOffsetPa which is optional.
      !     *TBD* the frame system for illumOffsetPA is not obvious.
      !     May be this is crossed...
      fRow%polarizationTypes(1) = PolarizationType_X
      if (fRow%numReceptor.ge.2) fRow%polarizationTypes(2) =   &
        PolarizationType_Y
      !            fRow%polarizationType(2) = 'Y'//char(0)
      !     *TBD* meaning not understood (relative complex gains of products?)
      !     Then this is a derived quantity from calibration... and probably
      !     very much frequency dependent.
      fRow%polResponse = 0
      !     *TBD* Do not understand the meaning of these numbers. Coordinate
      !     system should be precised
      !     NOW OPTIONAL IN SS2             fRow%xPosition = 0
      !     NOW OPTIONAL IN SS2             fRow%yPosition = 0
      !     NOW OPTIONAL IN SS2             fRow%zPosition = 0
      !     *TBD* With respect to what??
      !            fRow%receptorAngle = (/ 0.d0, (pi/2.d0) /)
      fRow%receptorAngle = 0.d0
      !     loops on spectral windows:
      !     loop on side bands, line/continuum
      do isb = 1, r_lnsb
        !     loop on line subbands
        do ic = 1, r_lband
          do ilc = 1, 2
            fKey%antennaId = antenna_Id(ia)
            fKey%spectralWindowId = spectralWindow_Id(isb,ic   &
              ,ilc)
            fKey%timeInterval = ArrayTimeInterval(time_Start(1)   &
              ,time_Start(2))
            !     NOW OPTIONAL IN SS2                      fRow%beamId = beam_Id
            fRow%receiverId = receiver_Id(ia, isb, ic, ilc)
            call addFeedRow(fKey, fRow, error)
            if (error) return
            feed_Id = fKey%feedId
            if (feed_Id.ne.0) then
              call sdmMessageI(6,3,sdmTable   &
                ,'FeedId is not zero: ',feed_Id)
              print *, 'fKey ', fKey
            !     c                     goto 99
            endif
          enddo
        enddo
        !     Total Power
        fKey%spectralWindowId = TotPowSpectralWindow_Id(isb)
        fRow%receiverId = TotPowReceiver_Id(ia, isb)
        call addFeedRow(fKey, fRow, error)
        if (error) return
        feed_Id = fKey%feedId
        if (feed_Id.ne.0) then
          call sdmMessageI(6,3,sdmTable   &
            ,'- FeedId is not zero: ',feed_Id)
          print *, 'fKey ', fKey
        !     c                     goto 99
        endif
      enddo
    enddo
  !
  !     Holography case
  !
  else
    fRow%numReceptor = 1
    call allocFeedRow(frow, fRow%numReceptor,  error)
    if (error) return
    !     NOW OPTIONAL IN SS2          fRow%feedNum = 0
    !     loop on antennas
    do ia=1, r_nant
      fRow%beamOffset = 0.     !! (/ 0.d0, 0.d0  /)
      fRow%polarizationTypes(1) = PolarizationType_X   ! 'X'//char(0)
      fRow%polResponse = 0
      !     NOW OPTIONAL IN SS2             fRow%xPosition = 0
      !     NOW OPTIONAL IN SS2             fRow%yPosition = 0
      !     NOW OPTIONAL IN SS2             fRow%zPosition = 0
      fRow%receptorAngle = (/ 0.d0, (pi/2.d0) /)
      fRow%focusReference = 0.0d0
      fKey%antennaId = antenna_Id(ia)
      fKey%spectralWindowId = totPowSpectralWindow_Id(1)
      fKey%timeInterval = ArrayTimeInterval(time_Start(1),   &
        time_Start(2))
      !     NOW OPTIONAL IN SS2             fRow%beamId = beam_Id
      fRow%receiverId = totPowReceiver_Id(ia, 1)
      !$$$            print *, 'feed time_Interval ', time_Interval
      !$$$            print *, 'feed time_start ', time_start
      call addFeedRow(fKey, fRow, error)
      if (error) return
      feed_Id = fKey%feedId
      if (feed_Id.ne.0) then
        call sdmMessageI(6,3,sdmTable   &
          ,'FeedId is not zero: ',feed_Id)
      endif
    enddo
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_Feed
!---------------------------------------------------------------------
subroutine get_sdm_Feed(error)
  !---------------------------------------------------------------------
  ! Read the Feed SDM table.
  ! Key: antennaId spectralWindowId timeInterval  ...
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_feed
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (FeedRow) :: fRow
  type (FeedKey) :: fKey
  type (FeedOpt) :: fOpt
  integer mreceptors
  parameter (mreceptors = 2)
  integer ia, ic, isb, ilc, i, ier
  character, parameter  :: sdmTable*4 = 'Feed'
  !      integer, parameter :: minalloc = 1
  !---------------------------------------------------------------------
  ier = 0
  call sdmMessage(1,1,sdmTable ,'starting...')
  fRow%numReceptor = mreceptors
  call allocFeedRow(frow, error)
  if (error) return
  !      numReceptors = 2
  !      feedNum = 0
  !     loop on antennas
  do ia=1, r_nant
    ! *TBD* *Check* the dimensions of BeamId and ReceiverId...
    !     loops on spectral windows:
    do i=1, ddId_Size
      ! semble necessaire...
      feed_Id = 0
      fKey%feedId = feed_Id
      fKey%antennaId = antenna_Id(ia)
      fKey%spectralWindowId = swId_List(i)
      fKey%timeInterval = ArrayTimeInterval(time_Interval(1),   &
        time_Interval(2))
      call getFeedRow(fKey, fRow, error)
      if (error) then
        print *, 'fKey: ',fKey
      endif
      receiverId_List(ia, i) = fRow%receiverId(1)
      ! numReceptor is now set to 2 in ALMA. 
      ! we need the actual number of polarizations whuch is in the 
      ! CorrelatorMode table.
      ! r_npol_rec = fRow%numReceptor
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
end subroutine get_sdm_Feed
!---------------------------------------------------------------------
!!! subroutine allocFeedRow(row, numReceptor,  error)
!!!   !---------------------------------------------------------------------
!!!   !$$$    integer, allocatable :: beamId(:)
!!!   !$$$    integer, allocatable :: receiverId(:)
!!!   !$$$    real*8, allocatable :: beamOffset(:,:)
!!!   !$$$    character*256, allocatable :: polarizationType(:)
!!!   !$$$    Complex, allocatable :: polResponse(:,:)
!!!   !$$$    real*8, allocatable :: receptorAngle(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Feed
!!!   type(FeedRow) :: row
!!!   integer :: numReceptor, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*4 = 'Feed'
!!!   !
!!!   ! beamId
!!!   !     NOW OPTIONAL IN SS2       if (allocated(row%beamId)) then
!!!   !     NOW OPTIONAL IN SS2          deallocate(row%beamId, stat=ier)
!!!   !     NOW OPTIONAL IN SS2          if (ier.ne.0) goto 98
!!!   !     NOW OPTIONAL IN SS2       endif
!!!   !     NOW OPTIONAL IN SS2       allocate(row%beamId(numReceptor), stat=ier)
!!!   !     NOW OPTIONAL IN SS2       if (ier.ne.0) goto 99
!!!   !
!!!   ! receiverId
!!!   if (allocated(row%receiverId)) then
!!!     deallocate(row%receiverId, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%receiverId(numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! beamOffset
!!!   if (allocated(row%beamOffset)) then
!!!     deallocate(row%beamOffset, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%beamOffset(2, numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! focusReference
!!!   if (allocated(row%focusReference)) then
!!!     deallocate(row%focusReference, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%focusReference(3, numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! polarizationType
!!!   if (allocated(row%polarizationTypes)) then
!!!     deallocate(row%polarizationTypes, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%polarizationTypes(numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! polResponse
!!!   if (allocated(row%polResponse)) then
!!!     deallocate(row%polResponse, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%polResponse(numReceptor, numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! receptorAngle
!!!   if (allocated(row%receptorAngle)) then
!!!     deallocate(row%receptorAngle, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%receptorAngle(numReceptor), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
