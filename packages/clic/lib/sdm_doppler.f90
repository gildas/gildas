subroutine write_sdm_Doppler(error)
  !---------------------------------------------------------------------
  ! Write the Doppler SDM table.
  !
  !     int sdm_addDopplerRow (int * sourceId, int * transitionIndex,
  !     double * velDef)
  !
  !     int sdm_addSpectralWindowDopplerId(int * spectralWindowId, int *
  !     dopplerId)
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_SpectralWindow
  use sdm_Doppler
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (DopplerRow) :: dRow
  type (DopplerKey) :: dKey
  type (SpectralWindowKey) :: swKey
  type (SpectralWindowOpt) :: swOpt
  !      integer transitionIndex, ic, isb, ilc, ireturn
  integer ic, isb, ilc, dSize
  !      integer sdm_addDopplerRow, sdm_addSpectralWindowDopplerId
  character sdmTable*12
  parameter (sdmTable='Doppler')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !     Loops on the source_Ids... get the Doppler_Id and add it into the
  !     SpectralWindow Table.
  dKey%SourceId = Source_Id
  dRow%transitionIndex = 0
  dRow%velDef = 0
  call addDopplerRow(dKey, dRow, error)
  if (error) return
  Doppler_Id = dKey%DopplerId
  do isb = 1, r_lnsb
    do ic = 1, r_lband
      do ilc = 1, 2
        if  (spectralWindow_Id(1,1,ilc).ge.0) then
          swKey%SpectralWindowId = SpectralWindow_Id(isb,ic,ilc)
          swOpt%DopplerId = Doppler_Id
          call addSpectralWindowDopplerId(swkey, swopt, error)
          if (error) return
        endif
      enddo
    enddo
    if  (TotPowSpectralWindow_Id(isb).ge.0) then
      swKey%SpectralWindowId = TotPowSpectralWindow_Id(isb)
      swOpt%DopplerId = Doppler_Id
      call addSpectralWindowDopplerId(swkey, swopt, error)
      if (error) return
    endif
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_Doppler
!---------------------------------------------------------------------
subroutine get_sdm_Doppler(error)
  !---------------------------------------------------------------------
  ! Read  the Doppler SDM table.
  !
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Doppler
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (DopplerRow) :: dRow
  type (DopplerKey) :: dKey
  integer ic, isb, ilc, dSize
  character sdmTable*12
  parameter (sdmTable='Doppler')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call getDopplerTableSize(dSize, error)
  if (error) return
  if (dSize.gt.0) then
    if (source_id.ge.0) then
      dKey%DopplerId = Doppler_Id
      dKey%SourceId = Source_Id
      call getDopplerRow(dKey, dRow, error)
      if (error) then
        print *, 'Doppler Key ', dKey
        goto 99
      endif
    endif
  endif
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
  return
end subroutine get_sdm_Doppler
!---------------------------------------------------------------------
