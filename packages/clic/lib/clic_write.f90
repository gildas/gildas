subroutine ipb_write(chain,check,error)
  use classic_api
  !---------------------------------------------------------------------
  !  Write a scan
  ! Argument:
  !	CHAIN	C*(*)	Write status ('NEW', 'EXTEND', 'UPDATE'
  !       CHECK	L	Check for duplicate write
  !	ERROR	L	Error return
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  logical :: check                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer(kind=entry_length) :: ix
  logical :: new
  ! maximum number of sections in an observation.
  integer :: max_sections
  parameter (max_sections=32)
  !------------------------------------------------------------------------
  ! Code:
  new = .false.
  error = .false.
10 if (new .or. chain(1:1).eq.'N') then   ! New
    call iobs(max_sections,ix,check,error)
    if(error) return
    call wgen(error)           ! section generale
    if (error) return
    call wpos(error)           ! position
    if (error) return
    call winterc(error)
    if (r_presec(rfset_sec)) then
      call wrfset(error)
      if (error) return
    endif
    !
    ! This is never written
    !         IF (R_PRESEC(SPEC_SEC)) THEN
    !            CALL WSPEC(ERROR)
    !            IF (ERROR) RETURN
    !         ENDIF
    !
    if (r_presec(contset_sec)) then
      call wcontset(error)
      if (error) return
    endif
    if (r_presec(lineset_sec)) then
      call wlineset(error)
      if (error) return
    endif
    if (r_presec(atparm_sec)) then
      call watparm(error)
      if (error) return
    endif
    if (r_presec(atmon_sec)) then
      call watmon(error)
      if (error) return
    endif
    if (r_presec(wvr_sec)) then
      call wwvr(error)
      if (error) return
    endif
    if (r_presec(scanning_sec)) then
      call wscanning(error)
      if (error) return
    endif
    if (r_presec(alma_sec)) then
      call walma(error)
      if (error) return
    endif
    if (r_presec(monitor_sec)) then
      call wmoni(error)
      if (error) return
    endif
    if (r_presec(status_sec)) then
      call wstatus(error)
      if (error) return
    endif
    if (r_presec(vlbi_sec)) then
      call wvlbi(error)
      if (error) return
    endif
    if (r_presec(bpcal_sec)) then
      call wbpcal(error)
      if (error) return
    endif
    if (r_presec(abpcal_sec)) then
      call wabpcal(error)
      if (error) return
    endif
    if (r_presec(ical_sec)) then
      call wical(error)
      if (error) return
    endif
    if (r_presec(aical_sec)) then
      call waical(error)
      if (error) return
    endif
    call wdescr(error)
    if (error) return
  elseif (chain(1:1).eq.'E') then  ! Extend
    call xobs(error)
    if (error) then
      error = .false.
      new = .true.
      goto 10
    endif
  elseif (chain(1:1).eq.'U') then  ! Update
    call mobs(error)
    if (error) then
      error = .false.
      new = .true.
      goto 10
    endif
  endif
  return
end subroutine ipb_write
!
subroutine ipb_close(error)
  use clic_file
  use gbl_format
  !---------------------------------------------------------------------
  ! 	Close an observation. Reopens the output file.
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: ktype, ier
  !------------------------------------------------------------------------
  ! Code:
  ktype = r_typec
  error = .false.
  call cobs(ktype,error)       ! ferme l'observation
  if (error) return
  !
  if (o%lun.eq.0) return
  close (unit=o%lun)
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',recl=facunf*o%desc%reclen,iostat=ier)
  if (ier.eq.0)  return
  !
  ! Error message
  call message(6,3,'CLOSE','Error re-opening file '   &
    //o%spec(1:o%nspec))
  call messios(6,3,'CLOSE',ier)
  error = .true.
  return
end subroutine ipb_close
!
subroutine ipb_done (error)
  use classic_api
  !---------------------------------------------------------------------
  !	Close an observation, but not the file
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: ktype
  !------------------------------------------------------------------------
  ! Code:
  ktype = r_typec
  call cobs(ktype,error)       ! ferme l'observation
end subroutine ipb_done
!
subroutine ipb_open (error)
  use gbl_format
  use clic_file
  !---------------------------------------------------------------------
  !	Reopen the output file.
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: ier
  !------------------------------------------------------------------------
  ! Code:
  if (o%lun.eq.0) return
  open (unit=o%lun,file=o%spec(1:o%nspec),access='DIRECT',   &
    status='OLD',recl=o%desc%reclen*facunf,iostat=ier)
  if (ier.eq.0) return
  
  ! 
  ! Error message
  call message(6,3,'OPEN','Error re-opening file '   &
    //o%spec(1:o%nspec))
  call messios(6,3,'OPEN',ier)
  error = .true.
  return
  !
  ! Close the output file
entry ipb_free(error)
  error = .false.
  if (o%lun.ne.0) close (unit=o%lun)
end subroutine ipb_open
!
! Close the output file
function ipb_size(dummy)
  use clic_file
  real :: ipb_size                  !
  integer :: dummy                  !
  !
  ipb_size = (o%desc%nextrec-1)*o%desc%reclen/1048576.
end function ipb_size
!
function done(w)
  !---------------------------------------------------------------------
  !	Simple-minded message for IPB_WRITE
  !---------------------------------------------------------------------
  character(len=7) :: done          !
  character(len=*) :: w             !
  !------------------------------------------------------------------------
  ! Code:
  if (w(1:1).eq.'U') then
    done = 'Updated'
  else
    done = 'Written'
  endif
end function done
!
subroutine write_scan (do_data,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! Call Tree
  !	STORE_FLUX
  !	CLIC_MODIFY
  !	STORE_CAL
  !	STORE_FLAG
  !	STORE_PASS
  !---------------------------------------------------------------------
  logical :: do_data                !
  logical :: error                  !
  ! Global
  character(len=7) :: done
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=data_length)    :: ndata
  integer(kind=address_length) :: data_in, ip_data
  integer :: lband,lntch,ldatl
  logical :: do_it
  character(len=80) :: chain
  character(len=4) :: procname, cproc
  !------------------------------------------------------------------------
  ! Code:
  do_it = do_data .or. write_mode.eq.'NEW'
  error = .false.
  if (do_it.and..not.r_presec(file_sec)) call get_data (ndata,data_in,error)
  if (error) goto 999
  !
  ! In lowres mode, get the original version of line and dd sections
  if (lowres.and.r_presec(file_sec).and.r_lmode.eq.1) then
    lband = r_lband
    lntch = r_lntch
    ldatl = r_ldatl
    r_lband = lband_original
    r_lntch = lntch_original
    r_ldatl = ldatl_original
  endif
  call ipb_write(write_mode,.true.,error)
  if (error) goto 999
  if (write_mode.eq.'UPDA') then
    call wgen(error)           ! section generale
    if (error) goto 999
    call wpos(error)           ! position
    if (error) goto 999
    call winterc(error)
    if (r_presec(rfset_sec)) then
      call wrfset(error)
      if (error) goto 999
    endif
    !
    ! This is never written
    !         IF (R_PRESEC(SPEC_SEC)) THEN
    !            CALL WSPEC(ERROR)
    !            IF (ERROR) GOTO 999
    !         ENDIF
    !
    if (r_presec(contset_sec)) then
      call wcontset(error)
      if (error) goto 999
    endif
    if (r_presec(lineset_sec)) then
      call wlineset(error)
      if (error) goto 999
    endif
    if (r_presec(atparm_sec)) then
      call watparm(error)
      if (error) goto 999
    endif
    if (r_presec(atmon_sec)) then
      call watmon(error)
      if (error) goto 999
    endif
    if (r_presec(wvr_sec)) then
      call wwvr(error)
      if (error) return
    endif
    if (r_presec(scanning_sec)) then
      call wscanning(error)
      if (error) goto 999
    endif
    if (r_presec(alma_sec)) then
      call walma(error)
      if (error) return
    endif
    if (r_presec(monitor_sec)) then
      call wmoni(error)
      if (error) return
    endif
    if (r_presec(status_sec)) then
      call wstatus(error)
      if (error) return
    endif
    if (r_presec(bpcal_sec)) then
      call wbpcal(error)
      if (error) goto 999
    endif
    if (r_presec(ical_sec)) then
      call wical(error)
      if (error) goto 999
    endif
    if (r_presec(abpcal_sec)) then
      call wabpcal(error)
      if (error) goto 999
    endif
    if (r_presec(aical_sec)) then
      call waical(error)
      if (error) goto 999
    endif
    call wdescr(error)
    if (error) goto 999
    call update_header
  endif
  if (do_it) then
    ip_data = gag_pointer(data_in,memory)
    call wdata (ndata,memory(ip_data),.false.,error)
  else
    if (r_presec(modify_sec)) then
      call wdmodif(error)
      if (error) goto 999
    endif
  endif
  if (error) goto 999
  ! in the following case,
  ! data has been changed in memory, but not in the input file...
  if (do_it .and. (write_mode.eq.'NEW')) call loose_data
  call ipb_close(error)
  if (error) goto 999
  !
  cproc = procname(r_proc)
  write(chain,'(a4,1x,i6,a,i6,a)') cproc ,r_num,   &
    ' '//done(write_mode)//' (',r_ndump,' records)'
  call message(4,1,'WRITE_SCAN',chain(1:50))
  if (lowres.and.r_presec(file_sec).and.r_lmode.eq.1) then
    r_lband = lband
    r_lntch = lntch
    r_ldatl = ldatl
    call update_header
  endif
  return
  !
999 error = .true.
  if (lowres.and.r_presec(file_sec).and.r_lmode.eq.1) then
    r_lband = lband
    r_lntch = lntch
    r_ldatl = ldatl
  endif
  return
end subroutine write_scan
