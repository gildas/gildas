subroutine write_sdm_AlmaRadiometer(error)
  !---------------------------------------------------------------------
  ! Write the CorrelatorMode SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_AlmaRadiometer
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (AlmaRadiometerKey) :: arKey
  type (AlmaRadiometerRow) :: arRow
  character sdmTable*20
  parameter (sdmTable='AlmaRadiometer')
  integer i
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !arOpt%numAntenna = r_nant
  !call  allocAlmaRadiometerOpt(arRow, arOpt, error)
  !if (error) return
  !do i=1, r_nant
  !  arOpt%spectralWindowId(i) = radiometerSpw_Id(i)
  !enddo
  call addAlmaRadiometerRow(arKey, arRow, error)
  if (error) return
  !
  ! was not changed...
  almaRadiometer_Id = arKey%almaRadiometerId
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_AlmaRadiometer
!---------------------------------------------------------------------
! get routine TBW
!---------------------------------------------------------------------
