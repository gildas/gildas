subroutine write_sdm_field(error)
  !---------------------------------------------------------------------
  ! Write the Source Field  table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Field
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (FieldRow) :: fRow
  type (FieldKey) :: fKey
  type (FieldOpt) :: fOpt
  integer :: i, ier, numAssocFieldId, k, l
  integer , parameter  :: mPoly=3
  real*8 :: dir(2)
  character sdmTable*12, chain*80
  parameter (sdmTable='Field')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  fRow%numPoly = 1
!!  call allocFieldRow(fRow, fRow%numPoly,  error)
  call allocFieldRow(fRow, error)
  if (error) return
  if (r_lamof.eq.0.0 .and. r_betof.eq.0.0) then
    fRow%fieldName = r_sourc
  else
    write (chain,1000) r_sourc, r_lamof, r_betof
1000 format(a,'(',f6.1,',',f6.1,')')
    call noir(chain, fRow%fieldName, 256)
  endif
  ! save for scan table
  field_Name = fRow%fieldName
  fRow%code = '-'
  ! now optional ! fRow%time = 0
  !
  ! By default at Bure we keep the phase center on the pointing center.
  dir(2) = r_bet + r_betof
  dir(1) = r_lam + r_lamof/cos(dir(2))
  do k = 1, fRow%numPoly
    do l = 1, 2
      fRow%delayDir(l, k) = dir(l)
    enddo
  enddo
  fRow%phaseDir = fRow%delayDir
  fRow%referenceDir = fRow%delayDir
  !     *TBD* the actual way this is specified (put in the actual 2000
  !     ra and dec coordinates?)
  ! deleted ! fRow%flagRow = .false.
  !
  call addFieldRow(fKey, fRow, error)
  if (error) return
  Field_Id = fKey%FieldId
  !
  ! add optional parameters (sourceId)
  !
  fOpt%sourceId = source_Id
  call addFieldSourceId(fKey, fOpt, error)
  if (error) return
  call sdmMessage(1,1,sdmTable ,'Written.')
  !
  return
end subroutine write_sdm_field
!---------------------------------------------------------------------
subroutine get_sdm_field(error)
  !---------------------------------------------------------------------
  ! Read the Source Field  table.
  !
  ! Key: fieldId
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Field
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (FieldRow) :: fRow
  type (FieldKey) :: fKey
  type (FieldOpt) :: fOpt
  logical :: present
  integer :: ier
  integer , parameter  :: mPoly=3
  character sdmTable*12
  parameter (sdmTable='Field')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call allocFieldRow(fRow, mPoly,  error)
  if (error) return
  !
  fKey%FieldId = Field_Id
  call getFieldRow(fKey, fRow, error)
  if (error) return
  ! Note the offsets must be in phaseDir ???
  !
  ! add optional parameters (sourceId)
  call getFieldSourceId(fKey, fOpt, present, error)
  if (error) return
  !c      print *, 'fOpt%sourceId ', fOpt%sourceId
  source_Id = fOpt%sourceId
  call sdmMessage(1,1,sdmTable ,'Read.')
  !
  return
end subroutine get_sdm_field
!---------------------------------------------------------------------
!!! subroutine allocFieldRow(row, numPoly,  error)
!!!   !---------------------------------------------------------------------
!!!   !$$$    real*8, allocatable :: delayDir(:,:)
!!!   !$$$    real*8, allocatable :: phaseDir(:,:)
!!!   !$$$    real*8, allocatable :: referenceDir(:,:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Field
!!!   type(FieldRow) :: row
!!!   integer :: numPoly, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'Field'
!!!   !
!!!   ! delayDir
!!!   if (allocated(row%delayDir)) then
!!!     deallocate(row%delayDir, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%delayDir(2,numPoly+1), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! phaseDir
!!!   if (allocated(row%phaseDir)) then
!!!     deallocate(row%phaseDir, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%phaseDir(2,numPoly+1), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   ! referenceDir
!!!   if (allocated(row%referenceDir)) then
!!!     deallocate(row%referenceDir, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(row%referenceDir(2,numPoly+1), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
!!! !---------------------------------------------------------------------
!!! subroutine allocFieldOpt(opt, numAssocFieldId,  error)
!!!   !---------------------------------------------------------------------
!!!   !     integer, allocatable :: assocFieldId(:)
!!!   !---------------------------------------------------------------------
!!!   use sdm_Field
!!!   type(FieldOpt) :: opt
!!!   integer :: numAssocFieldId, ier
!!!   logical :: error
!!!   character, parameter :: sdmTable*8 = 'Field'
!!!   !
!!!   ! assocFieldId
!!!   if (allocated(opt%assocFieldId)) then
!!!     deallocate(opt%assocFieldId, stat=ier)
!!!     if (ier.ne.0) goto 98
!!!   endif
!!!   allocate(opt%assocFieldId(numAssocFieldId), stat=ier)
!!!   if (ier.ne.0) goto 99
!!!   !
!!!   return
!!!   !
!!! 98 call sdmmessageI(8,4,sdmTable ,'Deallocation error ier ',ier)
!!!   error = .true.
!!!   return
!!!   !
!!! 99 call sdmmessageI(8,4,sdmTable ,'Allocation error ier ',ier)
!!!   error = .true.
!!!   return
!!! end
