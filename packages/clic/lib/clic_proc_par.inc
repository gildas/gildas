      INTEGER P_PASS, P_SOURCE, P_CAL, P_ONOFF, P_SKY, P_DELAY, P_GAIN, &
     &P_FOCUS, P_POINT, P_SKYDIP, P_HOLOG, P_FIVE, P_PSEUDO, P_FLUX,    &
     &P_STAB, P_CWVR, P_VLBI, P_VLBG, P_OTF, P_BAND, P_MONI
      PARAMETER(P_SKYDIP = 6)     ! skydip scan : table or sky
      PARAMETER(P_SOURCE = 11)    ! standard source or calibrator scan (corr-sk)
      PARAMETER(P_SKY    = 12)    ! sky scan : 4s auto-sky
      PARAMETER(P_GAIN   = 13)    ! gain scan : as source
      PARAMETER(P_DELAY  = 14)    ! delay scan : as source
      PARAMETER(P_FOCUS  = 15)    ! focus scan : as source
      PARAMETER(P_POINT  = 16)    ! point scan : as source
      PARAMETER(P_CAL    = 17)    ! cal scan : 4s auto-table, 4s auto-sky
      PARAMETER(P_PASS   = 18)    ! pass source scan : 4s auto, 4s corr.
      PARAMETER(P_ONOFF  = 19)    ! on - off
      PARAMETER(P_HOLOG  = 20)    ! Holography scan : as source
      PARAMETER(P_FIVE   = 21)    ! Five Point : as source
      PARAMETER(P_PSEUDO = 22)    ! Pseudo-Continuum: as on - off
      PARAMETER(P_FLUX   = 23)    ! Flux estimation : as source
      PARAMETER(P_STAB   = 24)    ! stability
      PARAMETER(P_CWVR   = 25)    ! wvr calibration scan
      PARAMETER(P_VLBI   = 26)    ! VLBI observation
      PARAMETER(P_VLBG   = 27)    ! VLBI phasing scan
      PARAMETER(P_OTF    = 28)    ! OTF scan
      PARAMETER(P_BAND   = 29)    ! Bandpass pseudo-scan
      PARAMETER(P_MONI   = 30)    ! Monitoring pseudo-scan
