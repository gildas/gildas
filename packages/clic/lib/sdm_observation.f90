subroutine write_sdm_observation(error)
  !---------------------------------------------------------------------
  ! Write the Source ExecBlock  table.
  ! Row Contents:
  ! observation table is *TBD*
  !
  ! calling:
  !
  !     int sdm_addObservationRow ()
  !
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  integer sdm_addObservationRow, ireturn
  !---------------------------------------------------------------------
  ireturn = sdm_addObservationRow()
  if (ireturn .lt.0) then
    call message(8,3,'write_sdm_observation'   &
      ,'Error  in sdm_addObservationRow')
    goto 99
  else
    Observation_Id  = ireturn
  endif
  return
99 error = .true.
end subroutine write_sdm_observation
!---------------------------------------------------------------------
subroutine get_sdm_observation(error)
  !---------------------------------------------------------------------
  ! Read the Source ExecBlock  table.
  ! Row Contents:
  ! observation table is *TBD*
  !
  ! calling:
  !
  !     int sdm_addObservationRow ()
  !
  !---------------------------------------------------------------------
  use gildas_def
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  integer sdm_getObservationRow, ireturn
  !---------------------------------------------------------------------
  Observation_Id = 0
  ireturn = sdm_getObservationRow(Observation_Id)
  if (ireturn .lt.0) then
    call message(8,3,'get_sdm_observation'   &
      ,'Error  in sdm_getObservationRow')
    goto 99
  endif
  return
99 error = .true.
end subroutine get_sdm_observation
!---------------------------------------------------------------------
