module clic_xypar
  use image_def
  !---------------------------------------------------------------------
  ! Support module for old X and Y commons which have been replaced
  ! by derived types private to Clic
  !---------------------------------------------------------------------
  type(gildas), save :: xima,yima
  !
end module clic_xypar

module clic_rf_solution
  include 'clic_parameter.inc'
  !
  ! Antenna----------------------------
  !      Frequency
  !
  type rf_ant_freq_t
    integer(kind=4) :: deg
    real(kind=4)    :: lim(mnbb,2)
    real(kind=4)    :: amp(mnbb,2,mnant,0:mbpcdeg)
    real(kind=4)    :: pha(mnbb,2,mnant,0:mbpcdeg)
  end type rf_ant_freq_t
  !
  !      Channel
  type rf_ant_chan_t
    integer(kind=4) :: deg
    real(kind=4)    :: camp(mnbb,2,mnant,mcch)
    real(kind=4)    :: cpha(mnbb,2,mnant,mcch)
    real(kind=4)    :: lamp(mnbb,2,mnant,mrlband,0:mbpcdeg)
    real(kind=4)    :: lpha(mnbb,2,mnant,mrlband,0:mbpcdeg)
  end type rf_ant_chan_t 
  !
  type rf_ant_t
    integer             :: nant           ! Number of ant
    integer             :: nc             ! Number of cont spw
    integer             :: nl             ! Number of line spw
    integer             :: nbb            ! Number of basebands
    integer             :: ibb(mrlband)   ! BB connected to spw
    integer             :: nsb            ! Number of sidebands
    integer             :: isb(mrlband)   ! Sideband present
    type(rf_ant_chan_t) :: chan
    type(rf_ant_freq_t) :: freq
  end type rf_ant_t
  !
  ! Baseline----------------------------
  !      Frequency
  !
  type rf_bas_freq_t
    integer(kind=4) :: deg
    real(kind=4)    :: lim(mnbb,2)
    real(kind=4)    :: amp(mnbb,2,mnbas,0:mbpcdeg)
    real(kind=4)    :: pha(mnbb,2,mnbas,0:mbpcdeg)
  end type rf_bas_freq_t
  !
  !      Channel
  type rf_bas_chan_t
    integer(kind=4) :: deg
    real(kind=4)    :: camp(mnbb,2,mnbas,mcch)
    real(kind=4)    :: cpha(mnbb,2,mnbas,mcch)
    real(kind=4)    :: lamp(mnbb,2,mnbas,mrlband,0:mbpcdeg)
    real(kind=4)    :: lpha(mnbb,2,mnbas,mrlband,0:mbpcdeg)
  end type rf_bas_chan_t 
  !
  type rf_bas_t
    integer             :: nbas           ! Number of baselines
    integer             :: nc             ! Number of cont spw
    integer             :: nl             ! Number of line spw
    integer             :: nbb            ! Number of basebands
    integer             :: ibb(mrlband)   ! BB connected to spw
    integer             :: nsb            ! Number of sidebands
    integer             :: isb(mrlband)   ! Sideband present
    type(rf_bas_chan_t) :: chan
    type(rf_bas_freq_t) :: freq
  end type rf_bas_t
  !
  type rf_t
    logical        :: antenna
    logical        :: freq
    type(rf_ant_t) :: ant
    type(rf_bas_t) :: bas
  end type rf_t
  !
  type(rf_t), save :: rf
end module clic_rf_solution
