!
subroutine get_fits_datapar(unit,ndata,data,addrecord,   &
    ifeed,error)
  use gildas_def
  use classic_api
  use gbl_constant
  integer :: unit                   !
  integer(kind=data_length) :: ndata                  !
  integer :: data(ndata)            !
  logical :: addrecord              !
  integer :: ifeed                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=data_length) :: h_offset
  character(len=4) :: procname
  character(len=80) :: chain,comment
  real*8 :: dd, uvw(3, mnant), doff(mrlband), second, ifval
  real :: total(mrlband)
  real :: ed
  integer :: status, integnum_col,integtim_col
  integer :: mjd_col, iutc
  integer :: uuvvww_col, azelerr_col, sourdir_col
  integer :: delaygeo_col, delayoff_col
  integer :: phasegeo_col, phaseoff_col
  integer :: rategeo_col,  rateoff_col
  integer :: focusoff_col
  integer :: latoff_col,longoff_col,totpower_col,windspee_col
  integer :: winddire_col,rdpower_col
  integer :: flag_col, iswitch_col, wswitch_col
  integer :: i, j, ia, ja, ib, nullval, k
  integer :: date(7), nldump, nswitch, nfeed, isb2, nlo
  logical :: anyf
  real :: minfoc(mnant), maxfoc(mnant), minlam(mnant), maxlam(mnant)
  real :: minbet(mnant), maxbet(mnant), testlamof(mnant), testbetof(mnant)
  real :: minlamof, maxlamof, minbetof, maxbetof, sx, ww, sw
  integer :: scan_focu, scan_lamb, scan_beta, scan_five, scan_other
  parameter (scan_focu=1, scan_lamb=2, scan_beta=3,   &
    scan_five=6, scan_other=8)
  !---------------------------------------------------------------------------
  status = 0
  ! Get  header keywords
  !      call ftgkyj(unit,'EXTVER',1,extname//' version',status)
  call ftgkys(unit,'TELESCOP',chain,comment,status)
  r_teles = chain(1:12)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','TELESCOP',   &
      status,error)
    return
  endif
  call ftgkyj(unit,'SCAN-NUM',r_scan,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','SCAN-NUM',   &
      status,error)
    return
  endif
  call ftgkyj(unit,'OBS-NUM',r_num,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','OBS-NUM',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  call ftgkys(unit,'DATE-OBS',chain,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','DATE-OBS',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  call fts2tm(chain,date(1),date(2),date(3),date(4),date(5),second,   &
    status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','FTS2TM',   &
      status,error)
    status = 0
  !         RETURN
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  date(6) = int(second)
  date(7) = nint((second-date(6))*1000.)
  call datejj(date,dd)
  dd = dd-2460549.5d0
  r_dobs = int(dd+100000)-100000
  dd = dd-r_dobs
  !
  ! NOTE: Here do something with TIMESYS keyword (convert to UTC if not ?)
  !
  r_ut = dd*2d0*pi
  call ftgkyd(unit,'LST',dd,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','LST',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_st = dd/43200d0*pi
  call ftgkys(unit,'OBSMODE',chain,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','OBSMODE',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  ! By default, no switching
  r_nswitch = 0
  call ftgkyj(unit,'NO_SWITC',r_nswitch,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','NO_SWITC',   &
      status,error)
    return
  !c      else
  !c PATCH
  !c         r_nswitch = 2
  endif
  !
  nfeed = 1
  call ftgkyj(unit,'NO_FEED',nfeed,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','NO_FEED',   &
      status,error)
    return
  endif
  !
  ! Get default frequency information
  ! (used for total power description if no data table is present)
  nlo = 2                      ! by default
  call ftgkyj(unit,'NO_LO',nlo,comment,status)
  ! (don't care if not there)
  status = 0
  if (nlo.ge.1) then
    call ftgkyd(unit,'FREQLO1',r_flo1,comment,status)
    r_flo1 = r_flo1/1d6
    call ftgkyj(unit,'SIDEBLO1',r_isb,comment,status)
  endif
  if (nlo.ge.2) then
    call ftgkyj(unit,'SIDEBLO2',isb2,comment,status)
    call ftgkyd(unit,'FREQLO2',r_flo1_ref,comment,status)
  endif
  r_flo1_ref = r_flo1_ref/1d6
  r_flo2 = r_flo1_ref
  r_fif1 = r_flo1_ref-350.d0   ! by definition in CLIC
  call ftgkyd(unit,'INTERFRE',ifval,comment,status)
  ifval = ifval/1d6
  !      IF (STATUS .GT. 0) GO TO 99
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','NO_LO',   &
      status,error)
    status = 0
  !         RETURN
  endif
  if (nlo.ge.2) then
    r_restf = r_flo1+r_isb*(ifval*isb2+r_flo1_ref)
  elseif (nlo.ge.1) then
    r_restf = r_flo1+r_isb*ifval
  endif
  write (r_line,'(a1,i3.3,a1,i1)') 'F',nint(r_restf/1d3),'P',ifeed
  status = 0
  !
  ! add ON, OFF and others
  ! chain contains 'OBSMODE' FITS keyword
  ! CALIBRATION
  if (chain(1:4).eq.'COLD') then
    chain = 'CALI'
    r_scaty = 7
  elseif (chain(1:4).eq.'CSKY') then   ! Calibration, Sky
    chain = 'CALI'
    r_scaty = 5
  elseif (chain(1:4).eq.'CALI') then   ! Calibration, load
    r_scaty = 4
  ! SKYDIP
  elseif (chain(1:4).eq.'SKYC') then   ! Skydip, load
    chain = 'SKYD'
    r_scaty = 4
  elseif (chain(1:4).eq.'SKYD') then   ! Skydip, sky
    r_scaty = 5
  ! FOCUS Z
  elseif (chain(1:4).eq.'FOCU') then
    r_scaty = 1
  ! FOCUS X
  elseif (chain(1:4).eq.'FOCX') then
    chain = 'FOCU'
    r_scaty = 11
  ! FOCUS Y
  elseif (chain(1:4).eq.'FOCY') then
    chain = 'FOCU'
    r_scaty = 12
  ! POINTING
  elseif (chain(1:4).eq.'POIN') then
    r_scaty = 2
  endif
  !
  r_proc = 23
  do while (r_proc.gt.0 .and. procname(r_proc).ne.chain(1:4))
    r_proc = r_proc-1
  enddo
  call ftgkys(unit,'PROJID',chain,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','PROJID',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_project = chain(1:4)
  call ftgkyj(unit,'FRONTEND',r_nrec,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','FRONTEND',   &
      status,error)
    return
  endif
  r_nrec = max(1, min(mnrec,r_nrec))
  !      IF (STATUS .GT. 0) GO TO 99
  call ftgkyd(unit,'AZIMUTH',dd,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','AZIMUTH',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_az = dd/180d0*pi
  call ftgkyd(unit,'ELEVATIO',dd,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','ELEVATIO',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_el = dd/180d0*pi
  call ftgkys(unit,'LONGSYS',chain,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','LONGSYS',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  if (chain(1:4).eq.'AZIM') then
    r_typof = type_ho
  elseif  (chain(1:4).eq.'RA--') then
    r_typof = type_eq
  elseif  (chain(1:4).eq.'GLON') then
    r_typof = type_ga
  else
    r_typof = type_un
  endif
  call ftgkye(unit,'EXPOSURE',ed,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','EXPOSURE',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_time = ed
  call ftgkyd(unit,'VFRAME',dd,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','VFRAME',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_doppl = dd/299792458d0
  !
  call ftgkys(unit,'SOURCE',chain,comment,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','SOURCE',   &
      status,error)
    return
  endif
  !      IF (STATUS .GT. 0) GO TO 99
  r_sourc = chain(1:12)
  call ftgkys(unit,'CALCODE',chain,comment,status)
  if (status.eq.202) then
    chain = ' '
    status = 0
    call message(6,2,'GET_FITS_DATAPAR',   &
      'Missing keyword: '//'CALCODE')
  elseif (status.ne.0) then
    call fio_printerror('GET_FITS_DATAPAR','CALCODE',   &
      status,error)
    return
  endif
  if (chain(1:3).eq.'OBJ') then
    r_itype = 1
  elseif (chain(1:4).eq.'PHAS') then
    r_itype = 2
  else
    r_itype = 0
  endif
  call ftgkyd(unit,'RA',dd,comment,status)
  if (status.eq.0) then
    r_lam = dd*pi/180d0
    call ftgkyd(unit,'DEC',dd,comment,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR','DEC',   &
        status,error)
      return
    endif
    !     IF (STATUS .GT. 0) GO TO 99
    r_bet = dd*pi/180d0
    call ftgkye(unit,'EQUINOX',r_epoch,comment,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR','EQUINOX',   &
        status,error)
      return
    endif
    !     IF (STATUS .GT. 0) GO TO 99
    r_typec = type_eq
  else
    status = 0
    call ftgkyd(unit,'GLON',dd,comment,status)
    if (status.eq.0) then
      r_lam = dd*pi/180d0
      call ftgkyd(unit,'GLAT',dd,comment,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR','GLAT',   &
          status,error)
        return
      endif
      !            IF (STATUS .GT. 0) GO TO 99
      r_bet = dd*pi/180d0
      r_typec = type_ga
    else
      call ftgkyd(unit,'AZIM-FIX',dd,comment,status)
      if (status.eq.0) then
        r_lam = dd*pi/180d0
        call ftgkyd(unit,'ELEV-FIX',dd,comment,status)
        if (status.gt.0) then
          call fio_printerror('GET_FITS_DATAPAR','ELEV-FIX',   &
            status,error)
          return
        endif
        !               IF (STATUS .GT. 0) GO TO 99
        r_bet = dd*pi/180d0
        r_typec = type_ho
      else
        status = 0
        r_typec = type_un
        call ftgkyd(unit,'LON',dd,comment,status)
        if (status.eq.0) then
          status = 0
          r_lam = dd*pi/180d0
          call ftgkyd(unit,'LAT',dd,comment,status)
          if (status.gt.0) then
            call fio_printerror('GET_FITS_DATAPAR','LAT',   &
              status,error)
            return
          endif
          !                  IF (STATUS .GT. 0) GO TO 99
          r_bet = dd*pi/180d0
        else
          status = 0
        endif
      endif
    endif
  endif
  call ftgkys(unit,'VELTYP',chain,comment,status)
  if (status.eq.202) then
    chain = ' '
    status = 0
    call message(6,2,'GET_FITS_DATAPAR',   &
      'Missing keyword: '//'VELTYP')
  elseif (status.ne.0) then
    call fio_printerror('GET_FITS_DATAPAR','VELTYP',   &
      status,error)
    return
  endif
  if (chain(1:8).eq.'VELO-LSR') then
    r_typev = 1
  elseif (chain(1:8).eq.'VELO-HEL') then
    r_typev = 2
  elseif (chain(1:8).eq.'VELO-OBS') then
    r_typev = 3
  elseif (chain(1:8).eq.'VELO-EAR') then
    r_typev = 4
  else
    r_typev = 0
  endif
  if (status .gt. 0) go to 99
  call ftgkye(unit,'OBS-ELEV',r_alti,comment,status)
  if (status .gt. 0) go to 99
  r_alti = r_alti/1000.
  !
  ! Read the columns
  call ftgcno(unit,.false.,'INTEGNUM',integnum_col,status)
  if (status.gt.0) then
    call fio_printerror('GET_FITS_DATAPAR','FTGCNO[INTEGNUM]',   &
      status,error)
    return
  endif
  call ftgcno(unit,.false.,'INTEGTIM',integtim_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'MJD',mjd_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'UUVVWW',uuvvww_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'AZELERR',azelerr_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'SOURDIR',sourdir_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'FOCUSOFF',focusoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'LATOFF',latoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'LONGOFF',longoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'TOTPOWER',totpower_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'WINDSPEE',windspee_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'WINDDIRE',winddire_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'FLAG',flag_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'DELAYGEO',delaygeo_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'DELAYOFF',delayoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'PHASEGEO',phasegeo_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'PHASEOFF',phaseoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'RATEGEO',rategeo_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'RATEOFF',rateoff_col,status)
  if (status.gt.0) goto 99
  call ftgcno(unit,.false.,'RDPOWER',rdpower_col,status)
  if (status.eq.219) then
    status = 0
    call message(6,2,'GET_FITS_DATAPAR',   &
      'Missing column: '//'RDPOWER')
    rdpower_col = -1
  elseif(status.ne.0) then
    call fio_printerror('GET_FITS_DATAPAR','FTGCNO [RDPOWER] ',   &
      status,error)
    return
  endif
  !
  ! Pathc for new receivers
  r_npol_rec =1
  r_lpolmode = 1
  r_lpolentry = 1
  r_totscale = 1.0
  !
  ! only read those if NO_SWITCH is non zero
  if (r_nswitch.gt.0) then
    call ftgcno(unit,.false.,'ISWITCH',iswitch_col,status)
    if (status.gt.0) goto 99
    call ftgcno(unit,.false.,'WSWITCH',wswitch_col,status)
    if (status.gt.0) goto 99
    do i=1, r_nswitch
      r_wswitch(i) = 0
      r_pswitch(1,i) = 0
      r_pswitch(2,i) = 0
    enddo
  else
    iswitch_col = -1
    wswitch_col = -1
  endif
  !
  do i=1, r_nant
    minlam(i) = 1e20
    minbet(i) = 1e20
    minfoc(i) = 1e20
    maxlam(i) = -1e20
    maxbet(i) = -1e20
    maxfoc(i) = -1e20
    r_mobil(i) = .false.
  enddo
  nldump = 0
  if (.not.addrecord) nldump = 1
  do i=1, r_ndump + nldump
    status = 0
    error = .false.
    call ftgcvj(unit,integnum_col,i,1,1,nullval,   &
      dh_dump,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVJ [INTEGNUM]', status,error)
      return
    endif
    call ftgcve(unit,integtim_col,i,1,1,nullval,   &
      dh_integ,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [INTEGTIM]', status,error)
      return
    endif
    ! MJD = JD - 2400000.5
    call ftgcvd(unit,mjd_col,i,1,1,nullval,   &
      dh_utc,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVD [DH_UTC]', status,error)
      return
    endif
    !         IF (STATUS.GT.0) GOTO 99
    iutc = int(dh_utc)
    dh_obs = iutc - 60549
    dh_utc = (dh_utc-iutc)*86400.d0
    !         IF (STATUS.GT.0) GOTO 99
    call ftgcvd(unit,uuvvww_col,i,1,3*r_nant,nullval,   &
      uvw,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR','FTGCVD[UUVVWW]',   &
        status,error)
      return
    endif
    if (status.gt.0) goto 99
    call ftgcve(unit,azelerr_col,i,1,2*r_nant,nullval,   &
      dh_rmspe,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [AZELERR_COL] ', status, error)
      return
    endif
    do j=1, r_nant
      do k=1, 2
        dh_rmspe(k,j) = dh_rmspe(k,j)*3600.
      enddo
    enddo
    if (status.gt.0) goto 99
    call ftgcvd(unit,sourdir_col,i,1,3,nullval,   &
      dh_svec,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVD [DH_SVEC] ', status, error)
      return
    endif
    if (status.gt.0) goto 99
    ! geometrical delay and phase tracking
    call ftgcve(unit,delaygeo_col,i,1,r_nant,nullval,   &
      dh_delayc,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [DH_DELAYC] ', status, error)
      return
    endif
    if (status.gt.0) goto 99
    call ftgcve(unit,phasegeo_col,i,1,r_nant,nullval,   &
      dh_phasec,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [PHASEGEO_COL] ', status, error)
      return
    endif
    if (status.gt.0) goto 99
    call ftgcve(unit,rategeo_col,i,1,r_nant,nullval,   &
      dh_ratec,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [RATEGEO_COL] ', status, error)
      return
    endif
    if (status.gt.0) goto 99
    ! offsets in delay and phase tracking
    k = 1
    do ia=1, r_nant
      call ftgcve(unit,delayoff_col,i,k,r_lband,nullval,   &
        doff,anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVE [DELAYGEO_COL] ', status, error)
        return
      endif
      dh_delay(1,ia) = doff(1)
      call ftgcve(unit,phaseoff_col,i,k,r_lband,nullval,   &
        doff,anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVE [PHASEOFF_COL] ', status, error)
        return
      endif
      dh_phase(ia) = doff(1)
      call ftgcve(unit,rateoff_col,i,k,r_lband,nullval,   &
        doff,anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVE [RATEOFF_COL] ', status, error)
        return
      endif
      k = k + r_lband
      !     mis dans delcon ... pour le moment.
      dh_delcon(ia) = doff(1)
    enddo
    !
    call ftgcve(unit,focusoff_col,i,1,r_nant,nullval,   &
      dh_offfoc,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [FOCUSOFF_COL] ', status, error)
      return
    endif
    call ftgcve(unit,longoff_col,i,1,r_nant,nullval,   &
      dh_offlam,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [LONGOFF_COL] ', status, error)
      return
    endif
    call ftgcve(unit,latoff_col,i,1,r_nant,nullval,   &
      dh_offbet,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE[LATOFF_COL]', status, error)
      return
    endif
    ! Note: CLIC takes only one total power (OK for ALMA-TI)
    ! RL 2006-10-30: now we may have two, but let's sort this out later ...
    !
    k = ifeed
    do ia = 1, r_nant
      !c            CALL FTGCVE(UNIT,TOTPOWER_COL,I,K,R_LBAND,NULLVAL,
      !c     &      TOTAL,ANYF,STATUS)
      !c            print *, i, k
      call ftgcve(unit,totpower_col,i,k,1,nullval,   &
        dh_total(1,ia),anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVE [TOTPOWER_COL] ', status, error)
        return
      endif
      if (r_teles.eq.'VTX-ALMATI'   &
        .or. r_teles.eq.'AEC-ALMATI') then
        dh_total(1,ia) = dh_total(1,ia)/50.
      endif
      k = k+nfeed
    enddo
    call ftgcve(unit,windspee_col,i,1,1,nullval,   &
      r_avwind,anyf,status)
    if (status.gt.0) goto 99
    call ftgcve(unit,winddire_col,i,1,1,nullval,   &
      r_avwindir,anyf,status)
    if (status.gt.0) then
      call fio_printerror('GET_FITS_DATAPAR',   &
        'FTGCVE [WINDDIRE_COL] ', status,error)
      return
    endif
    if (status.gt.0) goto 99
    do ia=1, r_nant
      ! get only the first one as CLIC has only one
      !            DO J=1, R_LBAND
      !            print *, 'FLAG_COL'
      call ftgcvj(unit,flag_col,i,(ia-1)*r_lband+1,1,nullval,   &
        dh_aflag(ia),anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVJ [FLAG_COL] ', status,error)
        return
      endif
    !            ENDDO
    enddo
    !         CALL FTGCVJ(UNIT,FLAG_COL,I,1,R_NANT,NULLVAL,
    !     &   DH_AFLAG,ANYF,STATUS)
    if (status.gt.0) goto 99
    if (rdpower_col.gt.0) then
      ants: do ia=1, r_nant
        !c            print *, 'RDPOWER_COL ant ',ia
        call ftgcve(unit,rdpower_col,i,ia,1,nullval,   &
          dh_test1(2,ia),anyf,status)
        if (status.gt.0) then
          call fio_printerror('GET_FITS_DATAPAR',   &
            'FTGCVJ [RDPOWER] ', status,error)
          status = 0
          error = .false.
          exit ants
        endif
      enddo ants
    endif
    !
    ! SWITCH columns
    if (iswitch_col.gt.0) then
      call ftgcvj(unit,iswitch_col,i,1,1,nullval,   &
        ia,anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVJ [ISWITCH_COL] ', status,error)
        ! (ignore this column for this table...)
        iswitch_col = 0
        status = 0
        error = .false.
      endif
      dh_test0(1) = ia
    endif
    if (wswitch_col.gt.0) then
      call ftgcve(unit,wswitch_col,i,1,1,nullval,   &
        dh_test0(2),anyf,status)
      if (status.gt.0) then
        call fio_printerror('GET_FITS_DATAPAR',   &
          'FTGCVJ [WSWITCH_COL]', status,error)
        status = 0
        error = .false.
      endif
      ia = dh_test0(1)
      if (r_wswitch(ia) .eq. 0) then
        r_wswitch(ia) = dh_test0(2)
        r_pswitch(1,ia) = dh_offlam(1)*3600.
        r_pswitch(2,ia) = dh_offbet(1)*3600.
      endif
    endif
    !
    do ib = 1, r_nbas
      ia = r_iant(ib)
      ja = r_jant(ib)
      dh_uvm(1,ib) = uvw(1,ja)-uvw(1,ia)
      dh_uvm(2,ib) = uvw(2,ja)-uvw(2,ia)
      dh_bflag(ib) = 0
    enddo
    k = 1+ h_offset(i)
    do j=1, r_nant
      dh_offlam(j) = dh_offlam(j)*3600.
      dh_offbet(j) = dh_offbet(j)*3600.
      minfoc(j) = min(minfoc(j),dh_offfoc(j))
      maxfoc(j) = max(maxfoc(j),dh_offfoc(j))
      minlam(j) = min(minlam(j),dh_offlam(j)*pis/180./3600.)
      maxlam(j) = max(maxlam(j),dh_offlam(j)*pis/180./3600.)
      minbet(j) = min(minbet(j),dh_offbet(j)*pis/180./3600.)
      maxbet(j) = max(maxbet(j),dh_offbet(j)*pis/180./3600.)
    enddo
    ! these defaults are overriden by get_fits_calibr
    do ib=1, r_nbas
      dh_atfac(1,1,ib) = 1.0
      dh_atfac(1,2,ib) = 1.0
    enddo
    call encode_header(data(k))
    if (i.gt.r_ndump .and. r_ndatl.gt.1) then
      k = 1+ h_offset(i+1)
      call encode_header(data(k))
    endif
  enddo
  ! FOCUS / POINTING Specifics
  ! not sure about the 0.1 as maxlam and minlam are in radians now.
  minlamof = 1e10
  minbetof = 1e10
  maxlamof = -1e10
  maxbetof = -1e10
  do i=1, r_nant
    if (abs(maxfoc(i)).gt.0.01) then
      r_mobil(i) = .true.
    endif
    if (maxlam(i)-minlam(i).gt.0.1*pi/180./3600.) then
      r_mobil(i) = .true.
      if (r_scaty.le.3) r_scaty = scan_lamb
    endif
    if (maxbet(i)-minbet(i).gt.0.1*pi/180./3600.) then
      r_mobil(i) = .true.
      if (r_scaty.le.3) r_scaty = scan_beta
    endif
    testlamof(i) = (maxlam(i)+minlam(i))/2.
    testbetof(i) = (maxbet(i)+minbet(i))/2.
    minlamof = min (minlamof,(maxlam(i)+minlam(i))/2.)
    maxlamof = max (maxlamof,(maxlam(i)+minlam(i))/2.)
    minbetof = min (minbetof,(maxbet(i)+minbet(i))/2.)
    maxbetof = max (maxbetof,(maxbet(i)+minbet(i))/2.)
  enddo
  if ((maxlamof-minlamof).lt.0.001) then
    r_lamof = (maxlamof+minlamof)/2.
    r_betof = (maxbetof+minbetof)/2.
  endif
  r_lfour = .true.
  !
  ! fill in switch and pointing model
  if (r_nswitch.gt.0) then
    do i=1,2
      sx = 0
      sw = 0
      do j=1, r_nswitch
        ww = r_wswitch(j)
        if (ww .gt. 0) then
          sx = sx + r_pswitch(i,j)*ww
          sw = sw + ww
        endif
      enddo
      sx = sx / sw
      do j=1, r_nswitch
        r_pswitch(i,j) = r_pswitch(i,j) - sx
      enddo
    enddo
  endif
  r_pmodel = pmodel_alma
  r_npmodel = 13
  do i=1, r_nant
    do j=1, r_npmodel
      r_cpmodel(j,i) = 0.
    enddo
  enddo
  return
  !
99 call printerror('GET_FITS_DATAPAR',status)
  call message(6,2,'GET_FITS_DATAPAR',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_datapar
