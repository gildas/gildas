subroutine get_fits_passband(unit,error)
  use gildas_def
  use classic_api  
  integer :: unit                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: comment
  integer :: status, itest, i, j, nullval, num_col, not_found
  parameter (not_found=202)
  logical :: anyf
  !---------------------------------------------------------------------------
  status = 0
  ! Get  header keywords
  call ftgkyj(unit,'SCAN-NUM',itest,comment,status)
  if (status .gt. 0) go to 99
  if (itest.ne.r_scan) then
    call message(8,4,'GET_FITS_PASSBAND','Wrong scan number')
    error = .true.
    return
  endif
  !
  ! Read keywords
  call ftgkyj(unit,'PASSDEGR',r_abpfdeg,comment,status)
  if (status .gt. 0) go to 99
  call ftgkye(unit,'IFREQMIN',r_abpflim(1,1),comment,status)
  if (status .gt. 0) go to 99
  call ftgkye(unit,'IFREQMAX',r_abpflim(1,2),comment,status)
  if (status .gt. 0) go to 99
  !
  ! Read the columns
  call ftgcno(unit,.false.,'ANTENNID',num_col,status)
  if (status.gt.0) goto 99
  call ftgcvj(unit,num_col,1,1,r_nant,nullval,   &
    r_kant,anyf,status)
  !
  call ftgcno(unit,.false.,'AMPLPASS',num_col,status)
  if (status.gt.0) goto 99
  do i=1, r_nant
    do j=0, r_abpfdeg
      call ftgcve(unit,num_col,i,1+2*j,2,nullval,   &
        r_abpfamp(1,1,i,j),anyf,status)
    enddo
  enddo
  !
  call ftgcno(unit,.false.,'PHASPASS',num_col,status)
  if (status.gt.0) goto 99
  do i=1, r_nant
    do j=0, r_abpfdeg
      call ftgcve(unit,num_col,i,1+2*j,2,nullval,   &
        r_abpfpha(1,1,i,j),anyf,status)
    enddo
  enddo
  !
  call ftgcno(unit,.false.,'SBAVERAG',num_col,status)
  if (status.gt.0) goto 99
  do i=1, r_nant
    call ftgcve(unit,num_col,i,1,4,nullval,   &
      r_abpcsba(1,1,i),anyf,status)
  enddo
  r_abpc = 1
  !
  return
  !
99 call printerror('GET_FITS_GAIN',status)
  call message(6,2,'GET_FITS_GAIN',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_passband
!
