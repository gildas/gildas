subroutine feedcorrection(nx,ny,p,error)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  ! Perform the Focal Feed phase correction
  ! The co-polar complex gain of the feed in direction theta, phi is:
  ! - Circular symmetry of the feed is assumed.
  ! G_co(th,ph)= sin^2(ph) G_E(th) + cos^2(ph) G_H(th)
  !              [Kilda, Foundations of Antennas - A unified approach, p 56]
  ! - G_E(th) and G_H(th) are either modelled or measured.
  !   and developped as:
  !   G_E(th) = sigma { ge(k) * cos(k*th) } k=1, nk
  !   G_H(th) = sigma { gh(k) * cos(k*th) } k=1, nk
  !
  !
  ! Input
  !     nx      integer          number of X points
  !     ny      integer          number of Y points
  !     p       real    nx,ny    raw phases
  ! Output
  !     p       real    nx,ny    corrected phases
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  real :: p(nx,ny)                  !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  integer :: ix, iy, k, nk, nf
  real :: xx, yy, r2, t2, t, costh, phi, phfeed
  parameter (nk=10)
  complex :: ge(nk), gh(nk), zt, zz, geth, ghth, gc
  ! computed by fitPhase.greg from FF79_AT_CMI_Lin_Phi00.prn and
  ! FF79_AT_CMI_Lin_Phi90.prn on 2002-11-22.
  ! (i.e. these are the model data).
  ! (ge is obtained by scanning at phi=90 and gh by scanning at phi=0)
  data gh/   &
    (  13.33004,   -0.03155), (  -1.27077,    0.00656),   &
    (   0.38349,   -0.17755), (   0.78041,   -0.11238),   &
    (  -0.54821,    0.16739), (  -0.68021,    0.11472),   &
    (   1.05341,   -0.01921), (  -0.80119,    0.06443),   &
    (   0.36258,   -0.01845), (  -0.07905,    0.00515)/
  data ge/   &
    (  12.79400,    2.27305), (   1.06279,   -0.56235),   &
    (  -1.92694,   -1.72309), (   1.79152,   -0.08008),   &
    (   0.09406,    0.46197), (  -2.82441,   -1.06010),   &
    (   2.77077,    1.02349), (  -1.74437,   -0.45956),   &
    (   0.62276,    0.11504), (  -0.11176,    0.01616)/
  !-------------------------------------------------------------------
  do iy=1, ny
    yy = (yima%gil%val(2)+(iy-yima%gil%ref(2))*yima%gil%inc(2))
    do ix= 1, nx
      xx = (yima%gil%val(1)+(ix-yima%gil%ref(1))*yima%gil%inc(1))
      if (abs(p(ix,iy)-yima%gil%bval).gt.yima%gil%eval) then
        r2 = xx**2 + yy**2
        t2 = r2 /4/focus**2
        ! t is tg(theta/2)
        t = sqrt(t2)
        ! we have zt = cos(theta)+i*sin(theta)
        zt = cmplx(1-t2,2*t)/(1+t2)
        zz = 1.
        geth = 0.
        ghth = 0.
        do k=1, nk
          zz = zz * zt
          costh = real(zz)
          geth = geth + ge(k)*costh
          ghth = ghth + gh(k)*costh
        enddo
        ! make sure the polarization is right !!!
        ! phi= 0    means theta scanning perpendicular to the E vector of the feed.
        ! phi= pi/2 means theta scanning parallel to the E vector of the feed.
        ! the following thus assumes the E field is in the Y direction (vertical)
        phi = atan2(yy,xx)
        gc = geth * sin(phi)**2 + ghth * cos(phi)**2
        phfeed = atan2(aimag(gc),real(gc))
        ! make sure the sign is right !!!
        ! Bernard says:
        ! " a positive increment of pathlength corresponds to
        !   a negative increment of the measured feed phase."
        ! which means that a POSITIVE phfeed corresponds to an artificial
        ! BUMP in the surface.
        p(ix,iy) = p(ix,iy) + phfeed 
      ! rotated               P(Iy,Ix) = P(Iy,Ix) + PHFEED
!                   p(ix,iy) = phfeed
      endif
    enddo
  enddo
  return
end subroutine feedcorrection
!
subroutine feedcorrection30m(nx,ny,p,error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  ! Perform the Focal Feed phase correction
  ! The co-polar complex gain of the feed in direction theta, phi is:
  ! - Circular symmetry of the feed is assumed.
  ! G_co(th,ph)= sin^2(ph) G_E(th) + cos^2(ph) G_H(th)
  !              [Kilda, Foundations of Antennas - A unified approach, p 56]
  ! - G_E(th) and G_H(th) are either modelled or measured.
  !   and developped as:
  !   G_E(th) = sigma { ge(k) * cos(k*th) } k=1, nk
  !   G_H(th) = sigma { gh(k) * cos(k*th) } k=1, nk
  !
  !
  ! Input
  !     nx      integer          number of X points
  !     ny      integer          number of Y points
  !     p       real    nx,ny    raw phases
  ! Output
  !     p       real    nx,ny    corrected phases
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  real :: p(nx,ny)                  !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  integer :: ix, iy, k, nk, nf, ier
  real :: xx, yy, r2, t2, t, costh, phi, phfeed
  real :: feed_angle
  parameter (nk=10)
  complex :: ge(nk), gh(nk), zt, zz, geth, ghth, gc
  ! computed by fitPhase.greg from FF79_AT_CMI_Lin_Phi00.prn and
  ! FF79_AT_CMI_Lin_Phi90.prn on 2002-11-22.
  ! (i.e. these are the model data).
  ! (ge is obtained by scanning at phi=90 and gh by scanning at phi=0)
      data ge/ &  
     & (  15.17699,   -1.42436), (  -3.42197,    0.73084),  &
     & (   1.93289,   -1.34948), (   0.07599,    0.57349),  &
     & (  -1.41472,    1.47833), (   0.73452,   -0.69789),  &
     & (   0.26901,    0.75635), (  -0.40234,   -0.09285),  &
     & (   0.35072,   -0.06541), (  -0.09002,    0.06682  )/ 
      data gh/  & 
     & (  12.52141,   -1.95618), (  -1.04963,    0.96664),  &
     & (   0.88352,    0.20061), (   0.63994,    0.15655),  &
     & (  -0.16381,    0.60636), (  -0.06513,    0.09193),  &
     & (   0.50606,   -0.15601), (  -0.12665,    0.21847),  &
     & (   0.02385,   -0.16633), (   0.04846,    0.05414 )/  
  !-------------------------------------------------------------------
  feed_angle = 45
  ier = sic_getlog('FEED_ANGLE',feed_angle)
  print *,"Correcting feed at angle ",feed_angle
  do iy=1, ny
    yy = (yima%gil%val(2)+(iy-yima%gil%ref(2))*yima%gil%inc(2))
    do ix= 1, nx
      xx = (yima%gil%val(1)+(ix-yima%gil%ref(1))*yima%gil%inc(1))
      if (abs(p(ix,iy)-yima%gil%bval).gt.yima%gil%eval) then
        r2 = xx**2 + yy**2
        t2 = r2 /4/focus**2
        ! t is tg(theta/2)
        t = sqrt(t2)
        ! we have zt = cos(theta)+i*sin(theta)
        zt = cmplx(1-t2,2*t)/(1+t2)
        zz = 1.
        geth = 0.
        ghth = 0.
        do k=1, nk
          zz = zz * zt
          costh = real(zz)
          geth = geth + ge(k)*costh
          ghth = ghth + gh(k)*costh
        enddo
        ! make sure the polarization is right !!!
        ! phi= 0    means theta scanning perpendicular to the E vector of the feed.
        ! phi= pi/2 means theta scanning parallel to the E vector of the feed.
        ! the following thus assumes the E field is in the Y direction (vertical)
        phi = atan2(yy,xx)+feed_angle*pi/180
        gc = geth * sin(phi)**2 + ghth * cos(phi)**2
        phfeed = atan2(aimag(gc),real(gc))
        ! make sure the sign is right !!!
        ! Bernard says:
        ! " a positive increment of pathlength corresponds to
        !   a negative increment of the measured feed phase."
        ! which means that a POSITIVE phfeed corresponds to an artificial
        ! BUMP in the surface.
        p(ix,iy) = p(ix,iy) + phfeed 
      ! rotated               P(Iy,Ix) = P(Iy,Ix) + PHFEED
!                   p(ix,iy) = phfeed
      endif
    enddo
  enddo
  return
end subroutine feedcorrection30m
!
subroutine fft(x,kx,ky,y,nx,ny,w)
  integer :: kx                     !
  integer :: ky                     !
  complex :: x(kx,ky)               !
  integer :: nx                     !
  integer :: ny                     !
  complex :: y(nx,ny)               !
  complex :: w(nx,ny)               !
  ! Local
  integer :: i, j, ii, jj, nn(2)
  nn(1) = nx
  nn(2) = ny
  !
  do i=1, nx
    do j=1, ny
      w(i,j) = 0
    enddo
  enddo
  !
  do j=1, ky
    jj = mod(j+ny-ky/2-1,ny)+1
    do i=1, kx
      ii = mod(i+nx-kx/2-1,nx)+1
      w(ii,jj) = x(i,j)
    enddo
  enddo
  !
  call fourt(w,nn,2,1,1,y)
  !
  do j=1, ny
    jj = mod(j+ny/2-1,ny)+1
    do i=1, nx
      ii = mod(i+nx/2-1,nx)+1
      y(ii,jj) = w(i,j)
    enddo
  enddo
!
end subroutine fft
!
subroutine field_extend(x,n1,n2,y,m1,m2)
  !---------------------------------------------------------------------
  ! extend input map into output map by zeroes
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  integer :: m1                     !
  integer :: m2                     !
  complex :: y(m1,m2)               !
  ! Local
  integer :: i1, i2, j1, j2, k1, k2
  !-------------------------------------------------------------------
  k1 = m1/2 - n1/2
  k2 = m2/2 - n2/2
  do j2 = 1, m2
    do j1 = 1, m1
      y(j1,j2) = 0
    enddo
  enddo
  do i2 = 1, (n2/2)*2
    j2 = i2 + k2
    do i1 = 1, (n1/2)*2
      j1 = i1 + k1
      if (j1.gt.0 .and. j1.le.m1 .and. j2.gt.0 .and. j2.le.m2)   &
        then
        y(j1,j2) = x(i1,i2)
      endif
    enddo
!!!            if (j2.eq.m2/2) then
!!!               print *, (j1, abs(y(j1,j2)), j1=1, m1)
!!!            endif
  enddo
end subroutine field_extend
!
subroutine field_extract(x,n1,n2,y,m1,m2)
  !---------------------------------------------------------------------
  ! extract input map from extended map
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  integer :: m1                     !
  integer :: m2                     !
  complex :: y(m1,m2)               !
  ! Local
  integer :: i1, i2, j1, j2, k1, k2
  complex :: scale
  !-------------------------------------------------------------------
  do j2 = 1, n2
    do j1 = 1, n1
      x(j1,j2) = 0
    enddo
  enddo
  k1 = m1/2 - n1/2
  k2 = m2/2 - n2/2
  do i2 = 1, (n2/2)*2
    j2 = i2 + k2
    do i1 = 1, (n1/2)*2
      j1 = i1 + k1
      x(i1,i2) = y(j1,j2)
    enddo
  enddo
  scale = abs(x(n1/2,n2/2))
  do i2 = 1, (n2/2)*2
    do i1 = 1, (n1/2)*2
      x(i1,i2) = x(i1,i2) / scale
    enddo
  enddo
end subroutine field_extract
!
subroutine do_apodize(x,n1,n2)
  !---------------------------------------------------------------------
  ! extend input map into output map by zeroes
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  ! Local
  integer :: i1, i2, j1, j2, k1, k2
  real :: f1, f2
  !-------------------------------------------------------------------
  print *, 'Apodising input map ...'
  do j2 = 1, n2
    f2 = 4.*(j2-n2-1.)*(j2-1.)/n2**2
    do j1 = 1, n1
      f1 = 4.*(j1-n1-1)*(j1-1.)/n1**2
      x(j1,j2) = x(j1,j2)*f1*f2
    enddo
  !$$$         if (j2.eq.n2/2+1) then
  !$$$            print *, (j1, x(j1,j2), j1=1, n1)
  !$$$         endif
  enddo
end subroutine do_apodize
subroutine do_test1(x,n1,n2)
  !---------------------------------------------------------------------
  ! extend input map into output map by zeroes
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  ! Local
  integer :: i1, i2, j1, j2, k1, k2
  real :: f1, f2
  !-------------------------------------------------------------------
  do j2 = 1, n2
    if (j2.eq.n2/2+1) then
      print *, (j1, abs(x(j1,j2)), j1=1, n1)
    endif
  enddo
end subroutine do_test1
!
subroutine field_to_aperture(n1,n2,x,dx,y,dy,w,distance,fresnel,   &
    dfocus,cheat)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  ! Include non fresnel terms.
  !     X: input field map,
  !     Y: input aperture  map,
  !     W: work space (two complex planes)
  !     d: distance of field
  !     fresnel: use fresnel approximation (otherwise use add. terms)
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  real *8 :: dx                     !
  complex :: y(n1,n2)               !
  real *8 :: dy                     !
  complex :: w(n1,n2,2)             !
  real :: distance                  !
  logical :: fresnel                !
  real :: dfocus                    !
  complex :: cheat                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  real *8 :: ref1, ref2
  real :: wvl, d2, xr(2), xi(2), yr(2), yi(2)
  real :: xx, yy, uu, vv, r2, t2, z, dp, zm, dp1
  complex :: factor
  integer :: it, i1, i2, j1, j2, nn(2), nt, im1, im2
  real :: maxa, aa
  !-------------------------------------------------------------------
  d2 = distance**2
  nn(1) = n1
  nn(2) = n2
  ref1 = n1/2 + 1
  ref2 = n2/2 + 1
  !
  wvl = dx * dy * n1
  factor = cmplx(0.,2*pi/wvl)
  if (fresnel) then
    nt = 1
  else
    nt = 6
  endif
  !
  do it = 1, nt
    do i2 = 1, n2
      do i1 = 1, n1
        w(i1,i2,1) = 0
      enddo
    enddo
    !
    maxa = 0
    im1 = 0
    im2 = 0
    do i2 = 1, n2
      vv = (i2-ref2)*dx
      j2 = mod(i2+n2/2-1,n2)+1
      do i1 = 1, n1
        uu = (i1-ref1)*dx
        j1 = mod(i1+n1/2-1,n1)+1
        if (it.eq.1) then
          w(j1,j2,1) = x(i1,i2)
        elseif (it.eq.2) then
          w(j1,j2,1) = x(i1,i2)*uu
        elseif (it.eq.3) then
          w(j1,j2,1) = x(i1,i2)*vv
        elseif (it.eq.4) then
          w(j1,j2,1) = x(i1,i2)*uu**2
        elseif (it.eq.5) then
          w(j1,j2,1) = x(i1,i2)*vv**2
        elseif (it.eq.6) then
          w(j1,j2,1) = x(i1,i2)*uu*vv
        endif
        if (abs(w(j1,j2,1)).gt.maxa) then
          maxa = abs(w(j1,j2,1))
          im1 = i1
          im2 = i2
        endif
      enddo
    enddo
    !
    call fourt(w(1,1,1),nn,2,1,1,w(1,1,2))
    !
    do i2 = 1, n2
      j2 = mod(i2+n2/2-1,n2)+1
      yy = (j2-ref2)*dy
      do i1 = 1, n1
        j1 = mod(i1+n1/2-1,n1)+1
        xx = (j1-ref1)*dy
        if (it.eq.1) then
          y(j1,j2) = w(i1,i2,1)
        elseif (it.eq.2) then
          y(j1,j2) = y(j1,j2)   &
            + w(i1,i2,1)*xx*(xx**2+yy**2) /2/d2*factor
        elseif (it.eq.3) then
          y(j1,j2) = y(j1,j2)   &
            + w(i1,i2,1)*yy*(xx**2+yy**2) /2/d2*factor
        elseif (it.eq.4) then
          y(j1,j2) = y(j1,j2)   &
            - w(i1,i2,1)*xx**2 /2/distance*factor
        elseif (it.eq.5) then
          y(j1,j2) = y(j1,j2)   &
            - w(i1,i2,1)*yy**2 /2/distance*factor
        elseif (it.eq.6) then
          y(j1,j2) = y(j1,j2)   &
            - w(i1,i2,1)*xx*yy /distance*factor
        endif
        if (abs(w(i1,i2,1)).gt.maxa) then
          maxa = abs(w(i1,i2,1))
          im1 = i1
          im2 = i2
        endif
      enddo
    enddo
  enddo
  ! do the gridding correction (affecting only the amplitudes).
  call grid_corr(n1,n2,y)
  !
  ! Correct the phases in aperture.
  if (distance.gt.0) then
    ! z at parabola edge
    zm = (diameter/2.)**2/4/focus
    do i2 = 1, n2
      yy = (i2-ref2)*dy
      do i1 = 1, n1
        xx = (i1-ref1)*dy
        r2 = xx**2 + yy**2
        z = r2 /4 /focus
        dp = r2 /2/distance - r2**2 /8/distance**3   &
          +  sqrt(r2+(focus+dfocus-z)**2) - (focus+z +dfocus)
        ! do the correction
        y(i1,i2) = y(i1,i2) * exp(factor * dp)
      !            y(i1,i2) = exp(factor * dp)
      enddo
    enddo
  endif
end subroutine field_to_aperture
!
subroutine corr_aperture(n1, n2,   &
    amp, pha, aperture)
  integer :: n1                     !
  integer :: n2                     !
  real :: amp(n1, n2)               !
  real :: pha(n1, n2)               !
  complex :: aperture(n1, n2)       !
  ! Local
  integer :: i1, i2
  !
  do i1 = 1, n2
    do i2 = 1, n1
      if (pha(i1,i2).ne.-1000.) then
        aperture(i1,i2) = 10.**(amp(i1,i2)/10.)   &
          * exp(cmplx(0.,pha(i1,i2)))
      else
        aperture(i1,i2) = 10.**(amp(i1,i2)/10.)
      endif
    enddo
  enddo
end subroutine corr_aperture
subroutine corr_aperture_perfect(n1, n2,   &
    amp, pha, aperture)
  integer :: n1                     !
  integer :: n2                     !
  real :: amp(n1, n2)               !
  real :: pha(n1, n2)               !
  complex :: aperture(n1, n2)       !
  ! Local
  integer :: i1, i2
  !
  do i1 = 1, n2
    do i2 = 1, n1
      if (pha(i1,i2).ne.-1000.) then
        aperture(i1,i2) = 10.**(amp(i1,i2)/10.)
      else
        aperture(i1,i2) = 0.
      endif
    enddo
  enddo
end subroutine corr_aperture_perfect
!
subroutine aperture_to_field(n1,n2,x,dx,y,dy,w,distance,fresnel,   &
    dfocus)
  !---------------------------------------------------------------------
  ! Include non fresnel terms.
  !     X: output field map
  !     Y: input  aperture map.
  !     d: distance of field
  !     fresnel: use fresnel approximation (otherwise use add. terms)
  !     DIAMETER       antenna diameter
  !     Focus          focus distance
  !     DFocus         focus offset
  !---------------------------------------------------------------------
  integer :: n1                     !
  integer :: n2                     !
  complex :: x(n1,n2)               !
  real*8 :: dx                      !
  complex :: y(n1,n2)               !
  real*8 :: dy                      !
  complex :: w(n1,n2,2)             !
  real :: distance                  !
  logical :: fresnel                !
  real :: dfocus                    !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  real*8 :: ref1, ref2
  real :: wvl, d2, xr(2), xi(2), yr(2), yi(2)
  real :: xx, yy, uu, vv,  z, r2, t2, dp
  complex :: factor
  integer :: it, i1, i2, j1, j2, nn(2), nt
  !-------------------------------------------------------------------
  d2 = distance**2
  nn(1) = n1
  nn(2) = n2
  ref1 = n1/2 + 1
  ref2 = n2/2 + 1
  !
  wvl = dx * dy * n1
  factor = cmplx(0.,2*pi/wvl)
  if (fresnel) then
    nt = 1
  else
    nt = 6
  endif
  !
  ! Correct the phases in aperture.
  do i2 = 1, n2
    yy = (i2-ref2)*dy
    do i1 = 1, n1
      xx = (i1-ref1)*dy
      r2 = xx**2 + yy**2
      t2 = r2 /4/focus**2
      z = focus*t2
      dp = r2 /2/distance - r2**2 /8/distance**3   &
        +  sqrt(r2+(focus+dfocus-z)**2) - (focus+z +dfocus)
      y(i1,i2) = y(i1,i2) * exp(factor * dp)
    enddo
  enddo
  !
  do it = 1, nt
    do i2 = 1, n2
      do i1 = 1, n1
        w(i1,i2,1) = 0
      enddo
    enddo
    !
    do i2 = 1, n2
      xx = (i2-ref2)*dy
      j2 = mod(i2+n2/2-1,n2)+1
      do i1 = 1, n1
        yy = (i1-ref1)*dy
        j1 = mod(i1+n1/2-1,n1)+1
        if (it.eq.1) then
          w(j1,j2,1) = y(i1,i2)
        elseif (it.eq.2) then
          w(j1,j2,1) = y(i1,i2)*xx*(xx**2+yy**2)
        elseif (it.eq.3) then
          w(j1,j2,1) = y(i1,i2)*yy*(xx**2+yy**2)
        elseif (it.eq.4) then
          w(j1,j2,1) = y(i1,i2)*xx**2
        elseif (it.eq.5) then
          w(j1,j2,1) = y(i1,i2)*yy**2
        elseif (it.eq.6) then
          w(j1,j2,1) = y(i1,i2)*xx*yy
        endif
      enddo
    enddo
    !
    call fourt(w(1,1,1),nn,2,-1,1,w(1,1,2))
    !
    do i2 = 1, n2
      j2 = mod(i2+n2/2-1,n2)+1
      uu = (j2-ref2)*dx
      do i1 = 1, n1
        j1 = mod(i1+n1/2-1,n1)+1
        vv = (j1-ref1)*dx
        if (it.eq.1) then
          x(j1,j2) = w(i1,i2,1)
        elseif (it.eq.2) then
          x(j1,j2) = y(j1,j2)   &
            + w(i1,i2,1)*uu /2/d2*factor
        elseif (it.eq.3) then
          x(j1,j2) = y(j1,j2)   &
            + w(i1,i2,1)*vv /2/d2*factor
        elseif (it.eq.4) then
          x(j1,j2) = y(j1,j2)   &
            - w(i1,i2,1)*uu**2 /2/distance*factor
        elseif (it.eq.5) then
          x(j1,j2) = y(j1,j2)   &
            - w(i1,i2,1)*vv**2 /2/distance*factor
        elseif (it.eq.6) then
          x(j1,j2) = y(j1,j2)   &
            + w(i1,i2,1)*uu*vv /distance*factor
        endif
      enddo
    enddo
  enddo
!
end subroutine aperture_to_field
!
subroutine phases(x,nx,ny,y,npm,pm,error)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  ! Compute the phase aperture distribution.
  !     X     Input Complex array (X slot)
  !     NX    Input complex array X dimension
  !     NY    Input complex array Y dimension
  !     Y     output array (NX,NY,2) :
  !           in 1 = amplitudes in dB (max 0dB)
  !           in 2 = phases in radians
  !     NPM   number of masked panels
  !     PM    masked panels
  !     ERROR error return
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  complex :: x(nx,ny)               !
  real :: y(nx,ny,2)                !
  integer :: npm                    !
  character(len=5) :: pm(200)       !
  logical :: error                  !
  ! Global
  external :: mask
  logical :: mask
  include 'gbl_pi.inc'
  ! Local
  real :: faz, edge
  integer :: ix, iy, ixmax, iymax
  real :: xx, yy, amax
  !-------------------------------------------------------------------------
  !
  amax = -1e10
  do iy=1, ny
    yy = (yima%gil%val(2)+(iy-yima%gil%ref(2))*yima%gil%inc(2))
    do ix= 1, nx
      xx = (yima%gil%val(1)+(ix-yima%gil%ref(1))*yima%gil%inc(1))
      y(ix,iy,1) = abs(x(ix,iy))
      !
      ! Mask Phase beyond edges and 4-pod., optionally some panels
      edge = 0.1 ! 10 cm...
      if (.not.mask(xx,yy,edge,npm,pm)) then
        if (y(ix,iy,1) .gt. amax) then
          ixmax = ix
          iymax = iy        
          amax = y(ix,iy,1)
        endif
      endif
      if (y(ix,iy,1) .gt.0) then
        y(ix,iy,2) = -faz(x(ix,iy))
      else
        y(ix,iy,2) = 0
      endif
    enddo
  enddo
  if (amax.le.0) then
    call message(8,4,'PHASES','Zero data')
    error = .true.
    return
  endif
  !
  ! Relative decibels
  yima%gil%bval = -1000.
  yima%gil%eval = 1.
  do iy=1, ny
    yy = (yima%gil%val(2)+(iy-yima%gil%ref(2))*yima%gil%inc(2))
    do ix= 1, nx
      xx = (yima%gil%val(1)+(ix-yima%gil%ref(1))*yima%gil%inc(1))
      y(ix,iy,1) = 10.*alog10(y(ix,iy,1)/amax)
      !
      ! Mask Phase beyond edges and 4-pod., optionally some panels
      edge = 0.1 ! 10 cm...
      if (mask(xx,yy,edge,npm,pm)) then
        y(ix,iy,2) = yima%gil%bval
      endif
    enddo
  enddo
  return
end subroutine phases
!
function mask(x,y,d,npm,pm)
  !---------------------------------------------------------------------
  ! returns .true. if point (x,y) is within d of blockage area
  !---------------------------------------------------------------------
  logical :: mask                   !
  real :: x                         !
  real :: y                         !
  real :: d                         !
  integer :: npm                    !
  character(len=5) :: pm(npm)       !
  ! Global
  include 'clic_panels.inc'
  ! Local
  character(len=5) :: cpanel
  integer :: nsector, iring, ninsector, ipanel, i, i1
  real :: xx, yy, r1, rq, r, dr, h1, dh, h, edge
  !
  ! the quadrupod shadow is a rectangle of 1/2 width q1 for r<r1,
  ! plus a trapeze  of maximum 1/2 width q2 (r=rout)
  !                 and minimum 1/2 width q1 (r=r1)
  !-----------------------------------------------------------------------
  !      dd = .1 ! half a resolution element
  mask = .false.
  r = sqrt(x**2+y**2)
  ! mask first  outer and inner edges
  if (r.gt.ray(nring)-d .or. r.lt.ray(0)+d) goto 100
  !
  ! rq = distance to the closest 4pod arm
  if (vblock) then
    rq = min(abs(x),abs(y))    ! plus type
  else
    rq = min(abs(x+y),abs(x-y))/sqrt(2.)   ! multiply type
  endif
  i1 = 1
  do i=1, nblock-1
    if (rblock(i).lt.r) then
      i1 = i
    endif
  enddo
  r1 = rblock(i1)
  dr = rblock(i1+1)-r1
  h1 = hblock(i1)
  dh = hblock(i1+1)-h1
  h = h1+(r-r1)/dr*dh
  if (rq.lt. d+h) goto 100
  !
  ! optical telescope
  if (nhole.ge.1) then
    do i = 1, nhole
      r1 = (x-xhole(i))**2+(y-yhole(i))**2
      if (r1.lt.(rhole(i)+d)**2) goto 100
    enddo
  endif
  !
  ! Get panel number and relative coordinates in panel
  call xypanel(x,y,iring,ipanel,xx,yy)
  if (npm.gt.0) then
    do i=1, npm
      if (pm(i).eq.cpanel(iring,ipanel)) goto 100
    enddo
  endif
  return
  !
100 mask = .true.
  return
end function mask
!
subroutine check_holo(iant, beta1, beta2, betam,   &
    lambda1, lambda2, lambdam, error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !	Check if data is a Holo scan
  !       Check if demanded antenna was moved.
  !---------------------------------------------------------------------
  integer :: iant                   !
  real :: beta1                     !
  real :: beta2                     !
  real :: betam                     !
  real :: lambda1                   !
  real :: lambda2                   !
  real :: lambdam                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_proc_par.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: k, data_in, ipk
  integer(kind=data_length)    :: ldata_in, h_offset
  integer :: m, ir
  ! Code:
  if (r_proc.ne.p_holog .and. r_proc.ne.p_source .and.r_proc.ne.p_otf ) then
    call message(6,3,'CHECK_HOLO',   &
      'Scan is not a holography or boresight measurement')
    error = .true.
    return
  endif
  if (iant.lt.1 .or. iant.gt.r_nant) then
    call message(6,3,'CHECK_HOLO',   &
      'Please select a valid antenna with SET ANTENNA i')
    error = .true.
    return
  endif
  if (.not.r_mobil(iant)) then
    call message(6,3,'CHECK_HOLO','Antenna was not moved')
    error = .true.
    return
  endif
  !
  ! Select band UPPER LOWER or AVERAGE
  if (n_band.ne.1) then
    call message(6,2,'CHECK_HOLO',   &
      'Using only first selected Band')
  endif
  if (i_band(1).eq.4) then
    call message(6,2,'CHECK_HOLO',   &
      'Sideband Ratio invalid in this context')
    error = .true.
    return
  endif
  call get_data (ldata_in,data_in,error)
  if (error) goto 999
  !
  ! Read records to get positions:
  lambdam = 0
  betam = 0
  lambda1 = 1e10
  beta1 = 1e10
  lambda2 = -1e10
  beta2 = -1e10
  k = data_in
  ipk = gag_pointer(k,memory)
  m = 0
  do ir = 1, r_ndump
    call decode_header (memory(ipk+h_offset(ir)))
    if (max(abs(dh_rmspe(1,iant)),abs(dh_rmspe(2,iant))).lt.30)   &
      then
      if (abs(dh_offlam(iant)).lt.18000. .and. abs(dh_offbet(iant)   &
        ).lt.18000.) then
        if (ir.gt.r_ndump/10) then
          lambdam = lambdam + dh_offlam(iant)-dh_rmspe(1,iant)
          betam = betam + dh_offbet(iant)-dh_rmspe(2,iant)
          lambda1 = min(lambda1,dh_offlam(iant))
          beta1 = min(beta1,dh_offbet(iant))
          lambda2 = max(lambda2,dh_offlam(iant))
          beta2 = max(beta2,dh_offbet(iant))
          m = m+1
        endif
      endif
    endif
  enddo
  lambdam = lambdam/m
  betam = betam/m
  if (r_proc.eq.p_source ) then
    lambdam = 0
    betam = 0
    lambda1 = 0
    beta1 = 0
    lambda2 = 0
    beta2 = 0
  endif
1000 format(6(1pg11.4))
  return
999 error = .true.
  return
end subroutine check_holo
!
subroutine open_map(nx,ny,dx,dy,rx,ry,iband,iant,   &
    nxmap,nymap,distance,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  use classic_api  
  use clic_xypar
  !---------------------------------------------------------------------
  ! Open map GDF files
  ! input:
  !     NX, NY number of pixel on each axis for beam map
  !     DX, DY coordinate increments, in radians!
  !     RX, RY pixel numbers for center (0,0)
  !     IBAND  side band used 1=USB, 2=LSB, 3=DSB (for freq.)
  !     IANT   physical number of antenna
  !     NXMAP, NYMAP number of pixel on each axis for antenna surface map
  !     ERROR  error return
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  real :: dx                        !
  real :: dy                        !
  real :: rx                        !
  real :: ry                        !
  integer :: iband                  !
  integer :: iant                   !
  integer :: nxmap                  !
  integer :: nymap                  !
  real :: distance                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
!  integer(kind=index_length) :: blc(4),trc(4)
  integer :: nc, ndim
  real :: wvl, factor, scale, zm
  character(len=255) :: fname
  ! Data
!  data blc/4*0/, trc/4*0/
  !-----------------------------------------------------------------------
  error = .false.
  call gildas_null(xima)
  !
  call datec (r_dobs,xima%file(1:11),error)
  call sic_lower(xima%file(1:11))
  if (iant.gt.100) then
    call sic_get_char('SHORT_FILE',xima%file,nc,error)
    xima%file(nc+1:) = '.beam'
  !     only for old data...
  elseif (r_teles.eq.'VTX-ALMATI') then
    write(xima%file(12:28),'(a,i4.4,a5)')   &
      '-Vertex-',r_scan,'.beam'
  elseif (r_teles.eq.'AEC-ALMATI') then
    write(xima%file(12:25),'(a,i4.4,a5)')   &
      '-AEC-',r_scan,'.beam'
  else
    write(xima%file(12:24),'(a3,i0,a2,i0,a5)')   &
      '-an',iant,'-r',r_nrec,'.beam'
  endif
  xima%gil%blan_words = 2
  ! reset the extrema section as they are not yet computed.
  xima%gil%extr_words = 0
  xima%gil%desc_words = 18
  xima%gil%posi_words = 12
  xima%gil%proj_words = 9
  xima%gil%spec_words = 12
  xima%gil%reso_words = 3
  xima%gil%bval = -1000.
  xima%gil%eval = 0.1
  !
  xima%gil%dim(1) = 2
  xima%gil%ref(1) = 0
  xima%gil%val(1) = 0
  xima%gil%inc(1) = 1
  xima%gil%dim(2) = nx
  xima%gil%ref(2) = rx
  xima%gil%val(2) = 0
  xima%gil%inc(2) = dx
  xima%gil%dim(3) = ny
  xima%gil%ref(3) = ry
  xima%gil%val(3) = 0
  xima%gil%inc(3) = dy
  xima%gil%dim(4) = 1
  xima%gil%ndim = 3
  xima%char%code(2) = 'DEC'
  xima%char%code(1) = 'RA'
  xima%char%unit = 'Jy'
  xima%char%syst = 'EQUATORIAL'
  xima%char%name = r_sourc
  xima%gil%ra = r_az
  xima%gil%dec = r_el
  xima%gil%epoc = r_epoch
  xima%gil%ptyp = p_azimuthal
  xima%gil%a0 = xima%gil%ra
  xima%gil%d0 = xima%gil%dec
  xima%gil%pang = 0.0
  xima%gil%xaxi = 0
  xima%gil%yaxi = 0
  xima%char%line = r_line
  if (iband.eq.1) then
    xima%gil%freq = r_flo1+r_fif1
  elseif (iband.eq.2) then
    xima%gil%freq = r_flo1-r_fif1
  else
    xima%gil%freq = r_flo1
  endif
  xima%loca%size = xima%gil%dim(1) * xima%gil%dim(2) *xima%gil%dim(3)
  xima%gil%form = fmt_r4
  xima%char%type = 'GILDAS_IMAGE'
  !
 ! call gdf_geis (xima%loca%islo,error)
 ! if (error) return
 ! call gdf_writ (xima,xima%loca%islo,error)
 ! if (error) return
 ! call gdf_cris (xima%loca%islo,xima%char%type,xima%file,xima%gil%form,xima%loca%size,error)
 ! if (error) return
 ! call gdf_gems (xima%loca%mslo,xima%loca%islo,blc,trc,xima%loca%addr,xima%gil%form,error)
 ! if (error) return
  call gdf_create_image (xima, error)
  if (error) return
  ndim = xima%gil%ndim
  call gdf_allocate (xima, error)
  if (error) return
  xima%gil%ndim = ndim
  xima%loca%addr = locwrd (xima%r3d)
  !
  ! Prepare output map
  call gildas_null(yima)
  call gdf_copy_header(xima,yima,error)
!  call gdf_geis (yima%loca%islo,error)
  if (error) return
  ! twice the resolution ...
  yima%gil%dim(1) = nxmap
  yima%gil%dim(2) = nymap
  ! 4 planes, to be used as workspace ...
  ! 5 planes, now: last for normal errors in micrometers
  ! 7 planes, now: last for normal errors in micrometers
  yima%gil%dim(3) = 7
  yima%gil%dim(4) = 1
  wvl = 299.792458/xima%gil%freq
  !      FACTOR = WVL*3600.*180./PI
  yima%gil%ref(1) = yima%gil%dim(1)/2+1
  yima%gil%val(1) = 0
  !      yima%gil%inc(1) = FACTOR/xima%gil%inc(2)/yima%gil%dim(1)
  yima%gil%inc(1) = wvl/xima%gil%inc(2)/yima%gil%dim(1)
  yima%gil%ref(2) = yima%gil%dim(2)/2+1
  yima%gil%val(2) = 0
  yima%gil%inc(2) = wvl/xima%gil%inc(3)/yima%gil%dim(2)
  yima%gil%ref(3) = 0
  yima%gil%val(3) = 0
  yima%gil%inc(3) = 1
  yima%file = ' '
  yima%gil%bval = -1000.
  call datec (r_dobs,yima%file(1:11),error)
  call sic_lower(yima%file(1:11))
  if (iant.gt.100) then
    call sic_get_char('SHORT_FILE',yima%file,nc,error)
    yima%file(nc+1:) = '.map'
  !     only for old data ...
  elseif (r_teles.eq.'VTX-ALMATI') then
    write(yima%file(12:27),'(a,i4.4,a4)') '-Vertex-',r_scan,'.map'
  elseif (r_teles.eq.'AEC-ALMATI') then
    write(yima%file(12:24),'(a,i4.4,a4)') '-AEC-',r_scan,'.map'
  !     Bure case
  else
    write(yima%file(12:23),'(a3,i0,a2,i1,a4)') '-an', iant, '-r',   &
      r_nrec,'.map'
  endif
 ! yima%loca%size = yima%gil%dim(1) * yima%gil%dim(2) * yima%gil%dim(3)
 ! call gdf_writ (yima,yima%loca%islo,error)
 ! if (error) return
 ! call gdf_cris (yima%loca%islo,yima%char%type,yima%file,yima%gil%form,yima%loca%size,error)
 ! if (error) return
 ! call gdf_gems (yima%loca%mslo,yima%loca%islo,blc,trc,yima%loca%addr,yima%gil%form,error)
 ! if (error) return
  call gdf_create_image (yima, error)
  if (error) return
  ndim = yima%gil%ndim
  call gdf_allocate (yima, error)
  yima%gil%ndim = ndim
  if (error) return
  yima%loca%addr = locwrd(yima%r3d)
  return
end subroutine open_map
!
subroutine create_aperture_map(nxmap,nymap,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use clic_xypar
  !---------------------------------------------------------------------
  ! Create an aperture map data file
  !---------------------------------------------------------------------
  integer :: nxmap                  !
  integer :: nymap                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
!  integer(kind=index_length) :: blc(4),trc(4)
  integer :: iant
  real :: wvl, factor
  ! Data
!  data blc/4*0/, trc/4*0/
  !-----------------------------------------------------------------------
  !
  ! Prepare output map
  call gildas_null(yima)
  call gdf_copy_header(xima,yima,error)
!  call gdf_geis (yima%loca%islo,error)
  if (error) return
  ! twice the resolution ...
  yima%gil%dim(1) = nxmap
  yima%gil%dim(2) = nymap
  ! 4 planes, to be used as workspace ...
  ! 5 planes, now: last for normal errors in micrometers
  yima%gil%dim(3) = 7
  yima%gil%dim(4) = 1
  wvl = 299.792458/xima%gil%freq
  !      FACTOR = WVL*3600.*180./PI
  yima%gil%ref(1) = yima%gil%dim(1)/2+1
  yima%gil%val(1) = 0
  !      yima%gil%inc(1) = FACTOR/xima%gil%inc(2)/yima%gil%dim(1)
  yima%gil%inc(1) = wvl /xima%gil%inc(2)/yima%gil%dim(1)
  yima%gil%ref(2) = yima%gil%dim(2)/2+1
  yima%gil%val(2) = 0
  !      yima%gil%inc(2) = FACTOR/xima%gil%inc(3)/yima%gil%dim(2)
  yima%gil%inc(2) = wvl /xima%gil%inc(3)/yima%gil%dim(2)
  yima%gil%ref(3) = 0
  yima%gil%val(3) = 0
  yima%gil%inc(3) = 1
  yima%file = ' '
  yima%gil%bval = -1000.
  call datec (r_dobs,yima%file(1:11),error)
  call sic_lower(yima%file(1:11))
  write(yima%file(12:22),'(a3,i0,a2,i1,a4)')   &
    '-an',0,'-r',r_nrec,'.map'
!  yima%loca%size = yima%gil%dim(1) * yima%gil%dim(2) * yima%gil%dim(3)
!  call gdf_writ (yima,yima%loca%islo,error)
!  if (error) return
!  call gdf_cris (yima%loca%islo,yima%char%type,yima%file,yima%gil%form,yima%loca%size,error)
!  if (error) return
!  call gdf_gems (yima%loca%mslo,yima%loca%islo,blc,trc,yima%loca%addr,yima%gil%form,error)
!  if (error) return
  call gdf_create_image (yima, error)
  if (error) return
  call gdf_allocate (yima, error)
  if (error) return
  yima%loca%addr = locwrd(yima%r3d)
end subroutine create_aperture_map
!
subroutine close_map(error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  logical :: error                  !
  ! Global
  call gdf_write_data (xima, xima%r3d, error)
  call gdf_write_data (yima, yima%r3d, error)
  call gdf_close_image (xima, error)
  call gdf_close_image (yima, error)
!  call gdf_fris (xima%loca%islo,error)
!  call gdf_fris (yima%loca%islo,error)
end subroutine close_map
!
subroutine plot_holo(iant,kw,sigu,sigw,rmsu,rmsw,eff,   &
    rms_ring,s_ring,z_ring,bmes,rmsast,taperx,tapery,x0, y0, h1, h2,   &
    basu,nbasu, distance, rigging, riggingElevation, & 
    tempBias, biasTemperature, temperature,fixedAst)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  include 'clic_parameter.inc'
  include 'clic_panels.inc'
  integer :: iant                   !
  real*8 :: kw                      !
  real :: sigu                      !
  real :: sigw                      !
  real :: rmsu                      !
  real :: rmsw, rmsast                      !
  real :: eff(8)                    !
  real :: rms_ring(mring)           !
  real :: s_ring(mring)             !
  real :: z_ring(mring)             !
  real*8 :: bmes(8)                 !
  real :: taperx                    !
  real :: tapery                    !
  real :: x0                        !
  real :: y0 , distance , riggingElevation(2), redeg(2), biasTemperature, temperature
  logical :: rigging, tempBias, fixedAst
  character(len=*) :: h1            !
  character(len=*) :: h2            !
  integer :: basu(mnbas)            !
  integer :: nbasu                  !
  ! Global
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: i
  real :: cdef, cdef0, xmax, ymax, xx, yy, rms, sl
  logical :: error
  integer :: nch1, nch2, j,ipol, nfilename, l
  character(len=1) :: bksl
  character(len=180) :: ch2
  character(len=256) :: chain,filename
  character(len=100) :: ch1
  real*8 :: frghz, st, astamp, astdir
  character(len=3) :: italic
  character(len=5) :: eta
  italic = char(92)//char(92)//'i'
  eta=char(92)//'gh'//char(92)//'d'
  !----------------------------------------------------------------------
  error = .false.
  bksl = char(92)
  call sic_get_char('SHORT_FILE',filename,nfilename,error)
  !
  ! Gasp... This is recursive programming
  !     CALL EXEC_CLIC('SIC GREG2'//BKSL//' ON')
  ! Just use GR_EXEC2 when needed: it does not matter that GREG2\
  ! language is OFF in such a case...
  !
  call gtclear
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  cdef0 = cdef
  cdef = 0.5
  write (ch1,'(A,F5.3)') 'SET CHAR ',cdef
  call gr_exec1 (ch1(1:lenc(ch1)))
  call sic_get_real('PAGE_X',xmax,error)
  call sic_get_real('PAGE_Y',ymax,error)
  call gr_exec('SET BLANKING -1000 1')
  yy = ymax - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  nch1 = lenc(h1)
  ch1  = italic//h1(1:nch1)
  call gr_labe(ch1(1:nch1+3))
  ! add file name...
  chain = filename(1:nfilename)
  if (r_presec(alma_sec)) then
    l = lenc(r_execblock)
    chain = filename(1:nfilename)//' - '//r_execblock(1:l)
    l = lenc(chain)
    if (distance.gt.0) then
      write(chain(l+1:),'(a,f7.3)') ' D=',distance
    else
      write(chain(l+1:),'(a)') ' Far Field'
    endif
    l = lenc(chain)
    if (rigging) then
      !reg = riggingElevation*180./pi
      write(chain(l+1:),'(a,2f5.1)') ' Grav:', riggingElevation*180./pi
    else
      write(chain(l+1:),'(a)') ' No Grav'
    endif
    l = lenc(chain)
    if (tempBias) then
      !reg = riggingElevation*180./pi
      write(chain(l+1:),'(a,f5.1,a,f5.1,a)') ' Temp:', biasTemperature, &
           '(', temperature,')'
    else
      write(chain(l+1:),'(a)') ' No Temp'
    endif
  endif
  l = lenc(chain)
  call grelocate(xmax/2,yy+0.5)
  call gr_labe_cent(2)
  call gr_labe(chain(1:l))
  !
  nch2 = lenc(h2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(h2(1:nch2))
  !
  write (ch2,1000) 'Edge taper = ', taperx,'x',tapery,   &
    ' dB - offset X=',x0,' Y=',y0,' m'
  nch2 = lenc(ch2)
  yy = yy - 1.5*cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
1001 format(a,3f6.2,a,f7.1,a, f7.1, a)
  astamp = sqrt(bmes(7)**2+bmes(8)**2)
  astdir = 180.-mod(atan2(bmes(8), bmes(7))*90./pi+180d0, 180d0)
  if (fixedAst) then
    write (ch2,1001) 'Focus offsets (X,Y,Z) = ',   &
      bmes(5)*1000., bmes(6)*1000., bmes(4)*1000.,   &
      ' mm; Astigmatism = ',rmsast,' '//char(92)//'gmm (Fixed to ',&
      astdir, 'deg.)'
  else
    write (ch2,1001) 'Focus offsets (X,Y,Z) = ',   &
      bmes(5)*1000., bmes(6)*1000., bmes(4)*1000.,   &
      ' mm; Astigmatism = ',rmsast,' '//char(92)//'gmm ( ',&
      astdir, 'deg.)'
  endif
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
1000 format(a,4(f7.2,a))
1002 format(a,8(f7.3,a))
1003 format(a,9(f7.3,a))
1004 format(a,f7.3,a,3(f6.3,a))
1005 format(a,2(f5.3,a),f7.3,a,3(f5.3,a))
  write (ch2,1002) 'Phase rms (unweighted)= ',   &
    sigu,' (weighted)=',sigw,' radians'
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  !      WRITE (CH2,1000) 'Surface rms (unweighted)= ',
  !     &RMSU*1E6,'  - (weighted)=',RMSW*1E6,' '//CHAR(92)//'gmm'
  write (ch2,1000) 'Surface rms (unweighted)= ',   &
    rmsu,'  - (weighted)=',rmsw,' '//char(92)//'gmm'
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  frghz = kw/2/pi*0.299792458d0
  write (ch2,1004) eta//'A(',frghz,' GHz) =',eff(1),';  '   &
    //eta//'A(230.0 GHz) = ',eff(2),';  '   &
    //eta//'A(345.0 GHz) = ',eff(7)
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  st = 2*1.38e3/pi/(diameter**2/4.)    ! S/T for perfect antenna.
  write (ch2,1003) 'S/T(', frghz,' GHz)=',st/eff(1),   &
    ' Jy/K;  S/T(230GHz)=',st/eff(2),' Jy/K; S/T(345 GHz)='   &
    ,st/eff(7),' Jy/K'
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  write (ch2,1005) eta//'I=', eff(3),   &
    '  -'//eta//'S=', eff(4),   &
    '  -'//eta//'P(', frghz,' GHz)=', eff(5),   &
    '  -'//eta//'P(230 GHz)=', eff(6),   &
    '  -'//eta//'P(345 GHz)=', eff(8)
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  write(ch2,'(A,8(1PG10.3))') 'Rms/ring: ',   &
    (rms_ring(i),i=1,nring)
  nch2 = lenc(ch2)
  yy = yy - cdef
  call grelocate(xmax/2.,yy)
  call gr_labe_cent(2)
  call gr_labe(ch2(1:nch2))
  !
  ! Corner labels (Left)
  cdef = 0.08*xmax/4           ! space for 4 lines
  call gtg_charlen(13,'XXXXXXXXXXXXX',cdef,sl,0)
  if (sl.gt.0.12*xmax) then
    cdef = cdef*0.12*xmax/sl
  endif
  write (ch1,'(A,F5.3)') 'SET CHAR ',cdef
  call gr_exec1 (ch1(1:lenc(ch1)))
  yy = ymax-cdef
  call grelocate(0.1,yy)
  call gr_labe_cent(3)
  call gr_labe(italic//'RF:')
  call grelocate(0.1,yy-cdef)
  call gr_labe_cent(3)
  call gr_labe(italic//'Am:')
  call grelocate(0.1,yy-2*cdef)
  call gr_labe_cent(3)
  call gr_labe(italic//'Ph:')
  !RF
  if (do_pass) then
    if (do_pass_freq) then
      ch1 = 'Fr.'
    else
      ch1 = 'Ch.'
    endif
    if (do_pass_antenna) then
      ch1(4:6) = '(A)'
    else
      ch1(4:6) = '(B)'
    endif
  else
    ch1 = 'Uncal.'
  endif
  call grelocate(3*cdef,yy)
  call gr_labe_cent(3)
  call gr_labe(ch1(1:10))
  !Amp.
  if (do_amplitude) then
    if (do_amplitude_antenna) then
      ch1 = 'Rel.(A)'
    else
      ch1 = 'Rel.(B)'
    endif
  elseif (do_scale) then
    ch1 = 'Scaled'
  else
    ch1 = 'Abs.   '
  endif
  call grelocate(3*cdef,yy-cdef)
  call gr_labe_cent(3)
  call gr_labe(ch1(1:7))
  if (do_phase) then
    if (do_phase_antenna) then
      ch1 = 'Rel.(A)'
    else
      ch1 = 'Rel.(B)'
    endif
  else
    ch1 = 'Abs.'
  endif
  call grelocate(3*cdef,yy-2*cdef)
  call gr_labe_cent(3)
  call gr_labe(ch1(1:7))
  !
  ! Rms calibration
  !      YY = YY-4*CDEF
  yy = yy-3*cdef
  xx = 0                       !cdef
  if (do_amplitude.or.do_phase) then
    call grelocate(xx,yy)
    call gr_labe_cent(3)
    call gr_labe('rms')
    xx = xx+2*cdef
  endif
  if (do_phase) then
    call grelocate(xx, yy)
    call gr_labe_cent(3)
    call gr_labe('Pha.')
    xx = xx+7*cdef
  endif
  j = i_band(1)
  if (nbasu.eq.0) then
    do i=1, r_nbas
      if (r_kant(r_iant(i)).eq.iant .or.  &
          r_kant(r_jant(i)).eq.iant) then
        yy = yy-cdef
        call gtg_charlen(5,cbas(i),cdef,sl,0)
        xx = sl
        if (do_amplitude.or.do_phase) then
          call grelocate(xx,yy)
          call gr_labe_cent(1)
          call gr_labe(cbas(i))
          xx = xx+sl
        endif
        if (do_phase) then
          ipol = do_polar
          if (ipol.eq.1.or.ipol.eq.2) then
            if (do_phase_antenna) then
              rms = r_aicrms(ipol,j,i,2)*180./pi
            else
              rms = r_icrms(ipol,j,i,2)*180./pi
            endif
          elseif(ipol.eq.3) then
            if (do_phase_antenna) then
              rms = r_aicrms(1,j,i,2)*90./pi +   &
                r_aicrms(2,j,i,2)*90./pi
            else
              rms = r_icrms(1,j,i,2)*90./pi +   &
                r_icrms(2,j,i,2)*90./pi
            endif
          endif
          write (ch1,'(f6.2)') rms
          call grelocate(xx, yy)
          call gr_labe_cent(1)
          call gr_labe(ch1(1:lenc(ch1)))
          xx = xx+6*cdef
        endif
      endif
    enddo
  else
    do i=1,nbasu
      yy = yy-cdef
      call gtg_charlen(5,cbas(basu(i)),cdef,sl,0)
      xx = sl
      if (do_amplitude.or.do_phase) then
        call grelocate(xx,yy)
        call gr_labe_cent(1)
        call gr_labe(cbas(basu(i)))
        xx = xx+sl
      endif
      if (do_phase) then
        if (do_polar.eq.2) then
          ipol = 2
        else
          ipol = 1
        endif
        if (do_phase_antenna) then
          rms = r_aicrms(ipol,j,basu(i),2)*180./pi
        else
          rms = r_icrms(ipol,j,basu(i),2)*180./pi
        endif
        write (ch1,'(f6.2)') rms
        call grelocate(xx, yy)
        call gr_labe_cent(1)
        call gr_labe(ch1(1:lenc(ch1)))
        xx = xx+6*cdef
      endif
    enddo
  endif
  write (ch1,'(A,F5.3)') 'SET CHAR ',cdef0
  call gr_exec1 (ch1(1:lenc(ch1)))
  !
end subroutine plot_holo
!
subroutine plot_maps(v1,v2,nx,scale1,scale2)
  use gildas_def
  use classic_api  
  use clic_xypar
  !---------------------------------------------------------------------
  ! Plot the two maps V1 and V2
  !---------------------------------------------------------------------
  integer :: nx                     !
  real :: v1(nx,nx)                 !
  real :: v2(nx,nx)                 !
  real :: scale1(3)                 !
  real :: scale2(3)                 !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_panels.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=80) :: ch1
  character(len=14) :: ch2,ch3
  real*8 :: conv(6)
  !----------------------------------------------------------------------
  conv(1) = yima%gil%ref(1)
  conv(2) = yima%gil%val(1)
  conv(3) = yima%gil%inc(1)
  conv(4) = yima%gil%ref(2)
  conv(5) = yima%gil%val(2)
  conv(6) = yima%gil%inc(2)
  call gr_exec('SET BLANKING -1000 1')
  !
  ! first plot
  call gr_exec('SET BOX .5 12.5 .5 12.5')
  call gr4_rgive(nx,nx,conv,v1)
  write  (ch2,'(2f7.2)') -diameter/2.,diameter/2.
  write  (ch3,'(2f7.2)') diameter/2.,-diameter/2.
  !* ATF Special View from front
  !!      CALL GR_EXEC1('LIMITS '//CH2//' '//CH2)
  !!!      CALL GR_EXEC1('LIMITS '//CH3//' '//CH2)
  if (front_view) then
    call gr_exec1('LIMITS '//ch3//' '//ch2)
  else
    call gr_exec1('LIMITS '//ch2//' '//ch2)
  endif
  write (ch1,'(2f13.5)') scale1(1),scale1(2)
  call gr_exec2('PLOT /SCALING LINEAR '//ch1(1:26))
  call gr_exec2('wedge')
  !
  ! second plot
  call gr_exec('SET BOX 15.5 27.5 .5 12.5')
  call gr4_rgive(nx,nx,conv,v2)
  !* ATF Special View from front
  !!      CALL GR_EXEC1('LIMITS '//CH2//' '//CH2)
  !!!      CALL GR_EXEC1('LIMITS '//CH3//' '//CH2)
  if (front_view) then
    call gr_exec1('LIMITS '//ch3//' '//ch2)
  else
    call gr_exec1('LIMITS '//ch2//' '//ch2)
  endif
  write (ch1,'(2f13.5)') scale2(1),scale2(2)
  call gr_exec2('PLOT /SCALING LINEAR '//ch1(1:26))
  call gr_exec2('wedge')
end subroutine plot_maps
!
subroutine plot_ctrs(v1,cs1,v2,cs2,nx,scale1,scale2,beam,number   &
    ,iant,pen_col)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use clic_xypar
  !---------------------------------------------------------------------
  ! Plot the contour maps of V1 and V2, as well as the Boxes and labels.
  !---------------------------------------------------------------------
  integer :: nx                     !
  real :: v1(nx,nx)                 !
  character(len=*) :: cs1           !
  real :: v2(nx,nx)                 !
  character(len=*) :: cs2           !
  real :: scale1(3)                 !
  real :: scale2(3)                 !
  real*8 :: beam                    !
  logical :: number                 !
  integer :: iant                   !
  integer :: pen_col                !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_panels.inc'
  include 'gbl_pi.inc'
  ! Local
  character(len=80) :: ch1,ch2
  character(len=13) :: view
  character(len=20) :: ch4,ch3
  real*8 :: conv(6)
  integer :: l, nch2
  !----------------------------------------------------------------------
  !
  ! first plot
  if (front_view) then
    view = ' (front view)'
  else
    view = ' (back view)'
  endif
  conv(1) = yima%gil%ref(1)
  conv(2) = yima%gil%val(1)
  conv(3) = yima%gil%inc(1)
  conv(4) = yima%gil%ref(2)
  conv(5) = yima%gil%val(2)
  conv(6) = yima%gil%inc(2)
  call gr_exec('SET BOX .5 12.5 .5 12.5')
  l = lenc(cs1)
  ch1 = 'DRAW TEXT 0 1 "'//cs1(1:l)//view//'" 8 /BOX 8'
  l = lenc(ch1)
  call gr_exec1(ch1(1:l))
  call gr4_rgive(nx,nx,conv,v1)
  write  (ch3,'(2f10.2)') -diameter/2.,diameter/2.
  write  (ch4,'(2f10.2)') diameter/2.,-diameter/2.
  !* ATF Special View from front
  if (front_view) then
    call gr_exec1('LIMITS '//ch4//' '//ch3)
  else
    call gr_exec1('LIMITS '//ch3//' '//ch3)
  endif
  !!
  write(ch2,1000) 'Levels ',scale1(1),' to ',-scale1(3),   &
    ' by ',scale1(3)
1000 format(a,4(f10.3,a))
  nch2 = lenc(ch2)
  call gr_exec2(ch2(1:nch2))
  call gr_exec2('RGMAP QUIET /PENS')
  write(ch2,1000) 'Levels ',scale1(3),' to ',scale1(2),   &
    ' by ',scale1(3)
  nch2 = lenc(ch2)
  call gr_exec2(ch2(1:nch2))
  write(ch2,1000) 'Levels ',scale1(1),' to ',scale1(2),   &
    ' by ',scale1(3)
  nch2 = lenc(ch2)
  call gr_exec1('DRAW TEXT 0 0.5 "'//ch2(8:nch2)//'" 8 /BOX 8')
  call gr_exec2('RGMAP QUIET /PENS')
  call draw_panels(beam,number,pen_col)
  if (ant_type.eq.type_bure) then
    call draw_new_bure_panels(iant)
  endif
  !
  ! second plot
  call gr_exec('SET BOX 15.5 27.5 .5 12.5')
  l = lenc(cs2)
  ch1 = 'DRAW TEXT 0 1 "'//cs2(1:l)//view//'" 8 /BOX 8'
  l = lenc(ch1)
  call gr_exec(ch1(1:l))
  call gr4_rgive(nx,nx,conv,v2)
  !* ATF Special View from front
  if (front_view) then
    call gr_exec1('LIMITS '//ch4//' '//ch3)
  else
    call gr_exec1('LIMITS '//ch3//' '//ch3)
  endif
  !!
  write(ch2,1000) 'Levels ',scale2(1),' to ', -scale2(3),   &
    ' by ',scale2(3)
  nch2 = lenc(ch2)
  call gr_exec2(ch2(1:nch2))
  call gr_exec2('RGMAP QUIET /PENS')
  write(ch2,1000) 'Levels ',scale2(3),' to ',scale2(2),   &
    ' by ',scale2(3)
  nch2 = lenc(ch2)
  call gr_exec2(ch2(1:nch2))
  write(ch2,1000) 'Levels ',scale2(1),' to ',scale2(2),   &
    ' by ',scale2(3)
  nch2 = lenc(ch2)
  call gr_exec1('DRAW TEXT 0 0.5 "'//ch2(8:nch2)//'" 8 /BOX 8')
  call gr_exec2('RGMAP QUIET /PENS')
  call draw_panels(beam,number,pen_col)
  if (ant_type.eq.type_bure) then
    call draw_new_bure_panels(iant)
  endif
end subroutine plot_ctrs
!
subroutine draw_panels(beam,number,pen_col)
  use gkernel_interfaces
  real*8 :: beam                    !
  logical :: number                 !
  integer :: pen_col                !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  integer :: i, j, lc
  real :: theta,  x(2), y(2), t1, t2, rr, rc, tori
  character(len=80) :: chain
  character(len=10) :: ch2
  character(len=5) :: ch5,cpanel
  logical :: error
  !----------------------------------------------------------------------
  ! draw panels
  error = .false.
  write(ch2,'(f10.7)') diameter/2.
  call gr_exec('ELLIPSE '//ch2//' /USER 0 0')
  call gr_exec('SET ORIENT 0')
  do j=1, nring
    write(ch2,'(f10.7)') ray(j-1)
    call gr_exec('ELLIPSE '//ch2//' /USER 0 0')
    do i=1, npan(j)
      call gr_segm('PANELS',error)
      if (ant_type.ge.type_melco12m_1 .and. ant_type.le.type_melco12m_4) then
        theta = pi/2.+(i-1)*2*pi/npan(j)
      elseif (ant_type.eq.type_iram30m) then
        theta = pi/2-(i-1)*2*pi/npan(j)
      else
        theta = pi-(i-1)*2*pi/npan(j)
      endif
      x(1) = ray(j-1)*cos(theta)
      y(1) = ray(j-1)*sin(theta)
      x(2) = x(1)*ray(j)/ray(j-1)
      y(2) = y(1)*ray(j)/ray(j-1)
      call gr4_connect(2,x,y,100.,0.)
      call gr_segm_close(error)
    enddo
  enddo
  if (number) then
    call gr_exec('SET EXPAND 0.3')
    write (chain,1001) 'PEN',pen_col,'/COL', pen_col
1001 format (a,1x,i2,1x,a,1x,i2)
!    call gr_exec('PEN 7 /COL 7')
    lc = lenc(chain)
    call gr_exec(chain(1:lc))
    do j=1, nring
      do i=1, npan(j)
        ch5 = cpanel(j,i)
        rc = (ray(j-1)+ray(j))/2
        if (ant_type.ge.type_melco12m_1 .and. ant_type.le.type_melco12m_4) then
          theta = pi/2.+(i-0.5)*2*pi/npan(j)
        elseif (ant_type.eq.type_iram30m) then
          theta = pi/2+(i-0.5)*2*pi/npan(j)
        else
          theta = pi-(i-0.5)*2*pi/npan(j)
        endif
        !* ATF Special View from front
        if (front_view) then
          tori = mod(-theta/pi*180.d0+270.d0,180.d0)-90.d0
        else
          tori = mod(theta/pi*180.d0+270.d0,180.d0)-90.d0
        endif
        write(chain,1000) 'DRAW TEXT ',   &
          rc*cos(theta), rc*sin(theta),   &
          ch5//' 5 ', tori, ' /USER'
1000    format(a,2f10.3,1x,a,f5.0,a)
        lc = lenc(chain)
        call gr_exec(chain(1:lc))
      enddo
    enddo
  endif
  !
  call gr_exec('PEN 0')
  call gr_exec('SET EXPAND 1.0')
  write(chain,'(A,F5.2,A,f7.1,1x,f7.1)') 'ELLIPSE ',beam/2.,   &
    ' /USER ',-diameter*0.45, -diameter*0.45
  lc = lenc(chain)
  call gr_exec(chain(1:lc))
  return
end subroutine draw_panels
!
subroutine draw_new_bure_panels(iant)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! draw darker the new panels set in clic_new_bure_panels.inc
  !---------------------------------------------------------------------
  integer :: iant                   !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  include 'clic_parameter.inc'
  ! INCLUDE 'clic_new_bure_panels.inc'
  ! Local
  integer :: i, j
  real :: theta,  x(2), y(2)
  logical :: error
  integer :: olun, ier
  character(len=256) :: file_npb
  integer :: newpanel(32,6,6),ipanel,iring,iante  ! panel,ring,antenna
  !
  ! old panel aluminium+hostaflon     : newpanel=0
  ! new panel aluminium+white painted : newpanel=1
  ! new panel aluminium               : newpanel=2
  !----------------------------------------------------------------------
  ! Open new panels file
  if (.not.sic_query_file('bure_panels','data#dir:','.dat',   &
    file_npb)) then
    call message(8,4,'HOLO_SUB','bure_panels not found')
    call sysexi(fatale)
  endif
  ier = sic_getlun(olun)
  ier = sic_open(olun,file_npb,'OLD',.false.)
  if (ier.ne.0) then
    call gagout('E-HOLO_SUB, Error opening '   &
      //file_npb(1:lenc(file_npb)))
    goto 997
  endif
  !
  ! read panels type
  do iante=1,6
    do iring=1,6
998   read(olun,*,err=998,end=997)   &
        (newpanel(ipanel,iring,iante),ipanel=1,32)
    enddo
  enddo
  !
  error = .false.
  call gr_exec('SET ORIENT 0')
  do j=1, nring
    do i=1, npan(j)
      if (newpanel(i,j,iant).eq.1) then
        call gr_exec('PEN  /WEIGHT 3')
        call gr_segm('PANELS',error)
        !
        theta = pi-(i-1)*2*pi/npan(j)
        x(1) = ray(j-1)*cos(theta)
        y(1) = ray(j-1)*sin(theta)
        x(2) = x(1)*ray(j)/ray(j-1)
        y(2) = y(1)*ray(j)/ray(j-1)
        call gr4_connect(2,x,y,100.,0.)
        !
        theta = pi-(i)*2*pi/npan(j)
        x(1) = ray(j-1)*cos(theta)
        y(1) = ray(j-1)*sin(theta)
        x(2) = x(1)*ray(j)/ray(j-1)
        y(2) = y(1)*ray(j)/ray(j-1)
        call gr4_connect(2,x,y,100.,0.)
        !
        theta = pi-(i-1)*2*pi/npan(j)
        x(1) = ray(j-1)*cos(theta)
        y(1) = ray(j-1)*sin(theta)
        theta = pi-(i)*2*pi/npan(j)
        x(2) = ray(j-1)*cos(theta)
        y(2) = ray(j-1)*sin(theta)
        call gr4_connect(2,x,y,100.,0.)
        !
        x(1) = x(1)*ray(j)/ray(j-1)
        y(1) = y(1)*ray(j)/ray(j-1)
        x(2) = x(2)*ray(j)/ray(j-1)
        y(2) = y(2)*ray(j)/ray(j-1)
        call gr4_connect(2,x,y,100.,0.)
        call gr_segm_close(error)
      endif
    enddo
  enddo
  call gr_exec('PEN /DEF')
997 close (unit=olun)
  call sic_frelun (olun)
  return
end subroutine draw_new_bure_panels
!
subroutine sub_grid(nvis,vis,sd_beam,nd,thres,error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  !     Cloned from SUB_GRID (author S.G.)
  !     Creation of a "well behaved" map from the spectra.
  !
  !     The "well behaved" map is obtained by linear  operations  (convolutions)
  !     from  the original spectra, in an optimum way from signal to noise point
  !     of view.
  !     nd = first dimension of VIS,
  !     x, y, and weights in columns 1 2 3,
  !     the remaining (nd-3) columns are data to be gridded.
  !
  !     thres is the weight threshold under which pixels are blanked
  !
  !     iscan is the column used for scanning (1 for X, 2 for Y)
  !---------------------------------------------------------------------
  integer :: nvis                   !
  integer :: nd                     !
  real :: vis(nd,nvis)              !
  real :: sd_beam                   !
  real :: thres                     !
  logical :: error                  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  real :: ubias,vbias,ubuff(4096),vbuff(4096)
  common /conv/ ubias,vbias,ubuff,vbuff
  integer :: nc, iscan
  real :: wmin
  !
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real*8 :: pi
  parameter (pi=3.14159265358979323846d0)
  integer :: nx,ny,np, i
  integer :: ctypx,ctypy
  integer(kind=address_length) :: ipxc, ipyc, ipx, ipw, ipwe
  integer(kind=address_length) :: addrw, addre, addrc
  integer :: lenge, lengw, lengc
  save lenge, lengw, lengc, addrw, addre, addrc
  integer :: xcol,ycol,ocol,lcol,wcol
  real :: smooth
  real :: maxw,xparm(10),yparm(10),support(2),cell(2)
  !
  character(len=2) :: weight_mode
  character(len=12) :: ctype(5)
  ! Data
  data ctype /'BOX','EXP','SINC','EXP*SINC','SPHEROIDAL'/
  !------------------------------------------------------------------------
  ! Code:
  xcol = 1
  ycol = 2
  wcol = 3
  weight_mode = 'NA'
  np = nvis
  ocol = wcol
  !      LCOL = 5 ! not used
  nc = nd - ocol
  !      ND = 2 + ocol
  !
  ! Tolerance is one arc second
  !      TOLE = PI/180./3600.
  !
  ! Recompute offsets if needed
  if (lengw.lt. 2*max(np,nd)) then
    if (lengw.gt.0) call free_vm(lengw,addrw)
    lengw = 2*max(np,nd)
    if (sic_getvm(lengw,addrw).ne.1) goto 998
  endif
  ipw = gag_pointer(addrw,memory)
  yima%gil%ptyp = p_azimuthal             ! Set the SINus type projection
  !
  ! Check order
  call dosor (vis,nd,np,memory(ipw),ycol)
  !
  ! Compute weights
  call dowei (vis,nd,np,memory(ipw),wcol)
  !
  ! Compute gridding function
  ! the output beam map is complex : in order (2,X,Y)
  ! SD_BEAM/3.0 is definitely too small.
  smooth = sd_beam/2.0 
  ctypx = 2
  ctypy = 2
  !      SUPPORT(1) = 2*SMOOTH              ! Go far enough...
  !      SUPPORT(2) = 2*SMOOTH
  support(1) = 4*smooth        ! Go far enough...
  support(2) = 4*smooth
  xparm(1) = support(1)/abs(xima%gil%inc(2))
  yparm(1) = support(2)/abs(xima%gil%inc(3))
  xparm(2) = smooth/(2*sqrt(log(2.0)))/abs(xima%gil%inc(2))
  yparm(2) = smooth/(2*sqrt(log(2.0)))/abs(xima%gil%inc(3))
  xparm(3) = 2
  yparm(3) = 2
  call grdflt (ctypx, ctypy, xparm, yparm)
  call convfn (ctypx, xparm, ubuff, ubias)
  call convfn (ctypy, yparm, vbuff, vbias)
  cell(1) = xima%gil%inc(2)
  cell(2) = xima%gil%inc(3)
  nx = xima%gil%dim(2)
  ny = xima%gil%dim(3)
  ipx = gag_pointer(xima%loca%addr,memory)
  !
  ! Get the workspace for intermediate smoothing
  if (lenge.lt. 2*nx*ny) then
    if (lenge.gt.0) call free_vm(lenge,addre)
    lenge = 2*nx*ny
    if (sic_getvm(lenge,addre).ne.1) goto 998
  endif
  ipwe = gag_pointer(addre,memory)
  !
  ! Convolve or grid
  if (lengc.lt.nx+ny) then
    if (lengc.gt.0) call free_vm(lengc,addrc)
    lengc = nx+ny
    if (sic_getvm(lengc,addrc).ne.1) goto 998
  endif
  ipxc = gag_pointer(addrc,memory)
  ipyc = ipxc + nx
  call docoor (nx,xima%gil%ref(2),xima%gil%val(2),xima%gil%inc(2),memory(ipxc))
  call docoor (ny,xima%gil%ref(3),xima%gil%val(3),xima%gil%inc(3),memory(ipyc))
  call doconv (   &
    nd,np,   &                 ! Number of input points
    vis,   &                   ! Input Values
    xcol,ycol,ocol,   &        ! Pointers to special values
    memory(ipw),   &           ! Weights
    memory(ipwe),   &          ! Gridded weights
    nc,nx,ny,   &              ! Cube size
    memory(ipx),   &           ! RAW Cube
    memory(ipxc),memory(ipyc),   & ! Cube coordinates
    support,cell,maxw)
  !
  ! Mask
  wmin = thres*maxw
  call domask(nc,nx*ny,memory(ipx),memory(ipwe),wmin,xima%gil%bval)
  !
  ! Transpose to the LMV order
  !      WRITE(6,*) 'I-SUB_GRID,  Transposing '
  !      CALL DOTRANS(MEMORY(IPRA),MEMORY(IPX),NC,NX*NY)
  !      CALL GDF_FRIS (xima%loca%islo,ERROR)
  !
  return
  !
998 call message(8,4,'SUB_GRID','Problems getting memory')
999 error = .true.
  return
end subroutine sub_grid
!
subroutine docoor (n,xref,xval,xinc,x)
  integer :: n                      !
  real*8 :: xref                    !
  real*8 :: xval                    !
  real*8 :: xinc                    !
  real :: x(n)                      !
  ! Local
  integer :: i
  do i=1,n
    x(i) = (dble(i)-xref)*xinc+xval
  enddo
end subroutine docoor
!
subroutine doconv (nd,np,visi,jx,jy,jo,we,gwe,   &
    nc,nx,ny,map,mapx,mapy,sup,cell,maxw)
  integer :: nd                     ! Number of "visibilities"
  integer :: np                     ! number of values
  real :: visi(nd,np)               ! values
  integer :: jx                     ! X coord, Y coord location in VISI
  integer :: jy                     ! X coord, Y coord location in VISI
  integer :: jo                     ! offset for data in VISI
  real :: we(np)                    ! Weights
  integer :: nx                     ! map size
  integer :: ny                     ! map size
  real :: gwe(nx,ny)                ! gridded weights
  integer :: nc                     ! number of channels
  real :: map(nc,nx,ny)             ! gridded values
  real :: mapx(nx)                  ! Coordinates of grid
  real :: mapy(ny)                  ! Coordinates of grid
  real :: sup(2)                    ! Supp.of convolving fct in User Units
  real :: cell(2)                   ! cell size in User Units
  real :: maxw                      ! Maximum weight
  ! Local
  integer :: ifirs,ilast          ! Range to be considered
  integer :: ix,iy,ic,i
  real :: result,weight
  real :: u,v,du,dv,um,up,vm,vp
  !
  maxw = 0.0
  !
  ! Loop on Y rows
  ifirs = 1
  do iy=1,ny
    v = mapy(iy)
    vm = v-sup(2)
    vp = v+sup(2)
    !
    ! Optimized dichotomic search, taking into account the fact that
    !	MAPY is an ordered array
    !
    call findr (np,nd,jy,visi,vm,ifirs)
    ilast = ifirs
    call findr (np,nd,jy,visi,vp,ilast)
    ilast = ilast-1
    !
    ! Initialize X column
    do ix=1,nx
      do ic=1,nc
        map(ic,ix,iy) = 0.0
      enddo
    enddo
    !
    ! Loop on X cells
    if (ilast.ge.ifirs) then
      do ix=1,nx
        u = mapx(ix)
        um = u-sup(1)
        up = u+sup(1)
        weight = 0.0
        !
        ! Do while in X cell
        do i=ifirs,ilast
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            du = (u-visi(jx,i))/cell(1)
            dv = (v-visi(jy,i))/cell(2)
            call convol (du,dv,result)
            result = result*we(i)
            weight = weight + result
            do ic=1,nc
              map (ic,ix,iy) = map (ic,ix,iy) +   &
                visi(ic+jo,i)*result
            enddo
          endif
        enddo
        !
        ! Normalize weight only in cells where some data exists...
        gwe(ix,iy) = weight
        maxw = max(maxw,weight)
        if (weight.gt.2e-30) then
          weight = 1.0/weight
          do ic=1,nc
            map (ic,ix,iy) = map(ic,ix,iy)*weight
          enddo
        endif
      enddo
    endif
  enddo
end subroutine doconv
!
subroutine dowei (visi,nd,np,we,iw)
  integer :: nd                     !
  integer :: np                     !
  real :: visi(nd,np)               !
  real :: we(np)                    !
  integer :: iw                     !
  ! Local
  integer :: i
  if (iw.le.0 .or. iw.gt.nd) then
    do i=1,np
      we(i) = 1.0
    enddo
  else
    do i=1,np
      we(i) = visi(iw,i)
    enddo
  endif
end subroutine dowei
!
subroutine findr (nv,nc,ic,xx,xlim,nlim)
  !---------------------------------------------------------------------
  ! GILDAS	Internal routine
  !	Find NLIM such as
  !	 	XX(IC,NLIM-1) < XLIM < XX(IC,NLIM)
  !	for input data ordered, retrieved from memory
  !	Assumes NLIM already preset so that XX(IC,NLIM-1) < XLIM
  !---------------------------------------------------------------------
  integer :: nv                     !
  integer :: nc                     !
  integer :: ic                     !
  real :: xx(nc,nv)                 !
  real :: xlim                      !
  integer :: nlim                   !
  ! Local
  integer :: ninf,nsup,nmid
  !
  if (nlim.gt.nv) return
  if (xx(ic,nlim).gt.xlim) then
    return
  elseif (xx(ic,nv).lt.xlim) then
    nlim = nv+1
    return
  endif
  !
  ninf = nlim
  nsup = nv
  do while(nsup.gt.ninf+1)
    nmid = (nsup + ninf)/2
    if (xx(ic,nmid).lt.xlim) then
      ninf = nmid
    else
      nsup = nmid
    endif
  enddo
  nlim = nsup
end subroutine findr
!
subroutine convol (du,dv,resu)
  real :: du                        !
  real :: dv                        !
  real :: resu                      !
  ! Local
  real :: ubias,vbias,ubuff(4096),vbuff(4096)
  common /conv/ ubias,vbias,ubuff,vbuff
  integer :: iu,iv
  !
  iu = nint(100.0*du+ubias)
  iv = nint(100.0*dv+vbias)
  resu = ubuff(iu)*vbuff(iv)
end subroutine convol
!
subroutine convfn (type, parm, buffer, bias)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! 	CONVFN computes the convolving functions and stores them in
  !   	the supplied buffer. Values are tabulated every 1/100 cell.
  ! Arguments :
  !	TYPE		I*4	Convolving function type
  ! 	PARM(10)        R*4  	Convolving function parameters.
  !				PARM(1) = radius of support in cells
  !	BUFFER(4096)	R*4  	Work buffer.
  !	BIAS		R*4	Center of convolution
  !---------------------------------------------------------------------
  integer :: type                   !
  real*4 ::  parm(10)               !
  real*4 ::  buffer(4096)           !
  real*4 ::  bias                   !
  ! Local
  integer :: lim, i, im, ialf, ier, ibias
  real*4 ::  pi, p1, p2, u, umax, absu, eta, psi
  data pi /3.1415926536/
  !
  ! Number of rows
  i = int( max (parm(1)+0.995 , 1.0) )
  i = i * 2 + 1
  lim = i * 100 + 1
  if (lim.gt.1.5*4096) then
    write(6,*) 'E-CONVFN,  Work buffer insufficient ',lim
    call sysexi(fatale)
  elseif (lim.gt.4096) then
    lim = 4096
    bias = 2049
  else
    bias = 50.0 * i + 1.0
  endif
  umax = parm(1)
  !                                       Check function types.
  goto (100,200,300,400,500) type
  !
  ! Type defaulted or not implemented, use Default = EXP * SINC
  type = 4
  parm(1) = 3.0
  parm(2) = 1.55
  parm(3) = 2.52
  parm(4) = 2.00
  goto 400
  !
  ! Pill box
100 continue
  do i = 1,lim
    u = (i-bias) * 0.01
    absu = abs (u)
    if (absu.lt.umax) then
      buffer(i) = 1.0
    elseif (absu.eq.umax) then
      buffer(i) = 0.5
    else
      buffer(i) = 0.0
    endif
  enddo
  return
  !
  ! Exponential function.
200 continue
  p1 = 1.0 / parm(2)
  do i = 1,lim
    u = (i-bias) * 0.01
    absu = abs (u)
    if (absu.gt.umax) then
      buffer(i) = 0.0
    else
      buffer(i) = exp (-((p1*absu) ** parm(3)))
    endif
  enddo
  return
  !
  ! Sinc function.
300 continue
  p1 = pi / parm(2)
  do i = 1,lim
    u = (i-bias)*0.01
    absu = abs (u)
    if (absu.gt.umax) then
      buffer(i) = 0.0
    elseif (absu.eq.0.0) then
      buffer(i) = 1.0
    else
      buffer(i) = sin (p1*absu) / (p1*absu)
    endif
  enddo
  return
  !
  ! EXP * SINC convolving function
400 continue
  p1 = pi / parm(2)
  p2 = 1.0 / parm(3)
  do i = 1,lim
    u = (i-bias)*0.01
    absu = abs (u)
    if (absu.gt.umax) then
      buffer(i) = 0.0
    elseif (absu.lt.0.01) then
      buffer(i) = 1.0
    else
      buffer(i) = sin(u*p1) / (u*p1) *   &
        exp (-((absu * p2) ** parm(4)))
    endif
  enddo
  return
  !
500 continue
  do i = 1,lim
    buffer(i) = 0.0
  enddo
  ialf = 2.0 * parm(2) + 1.1
  im = 2.0 * parm(1) + 0.1
  ialf = max (1, min (5, ialf))
  im = max (4, min (8, im))
  lim = parm(1) * 100.0 + 0.1
  ibias = bias
  do i = 1,lim
    eta = float (i-1) / float (lim-1)
    call sphfn (ialf, im, 0, eta, psi, ier)
    buffer(ibias+i-1) = psi
  enddo
  lim = ibias-1
  do i = 1,lim
    buffer(ibias-i) = buffer(ibias+i)
  enddo
  return
!
end subroutine convfn
!
subroutine sphfn (ialf, im, iflag, eta, psi, ier)
  !---------------------------------------------------------------------
  !     SPHFN is a subroutine to evaluate rational approximations to se-
  !  lected zero-order spheroidal functions, psi(c,eta), which are, in a
  !  sense defined in VLA Scientific Memorandum No. 132, optimal for
  !  gridding interferometer data.  The approximations are taken from
  !  VLA Computer Memorandum No. 156.  The parameter c is related to the
  !  support width, m, of the convoluting function according to c=
  !  pi*m/2.  The parameter alpha determines a weight function in the
  !  definition of the criterion by which the function is optimal.
  !  SPHFN incorporates approximations to 25 of the spheroidal func-
  !  tions, corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
  !  and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
  !
  !  Input:
  !    IALF    I*4   Selects the weighting exponent, alpha.  IALF =
  !                  1, 2, 3, 4, and 5 correspond, respectively, to
  !                  alpha = 0, 1/2, 1, 3/2, and 2.
  !    IM      I*4   Selects the support width m, (=IM) and, correspond-
  !                  ingly, the parameter c of the spheroidal function.
  !                  Only the choices 4, 5, 6, 7, and 8 are allowed.
  !    IFLAG   I*4   Chooses whether the spheroidal function itself, or
  !                  its Fourier transform, is to be approximated.  The
  !                  latter is appropriate for gridding, and the former
  !                  for the u-v plane convolution.  The two differ on-
  !                  by a factor (1-eta**2)**alpha.  IFLAG less than or
  !                  equal to zero chooses the function appropriate for
  !                  gridding, and IFLAG positive chooses its F.T.
  !    ETA     R*4   Eta, as the argument of the spheroidal function, is
  !                  a variable which ranges from 0 at the center of the
  !                  convoluting function to 1 at its edge (also from 0
  !                  at the center of the gridding correction function
  !                  to unity at the edge of the map).
  !
  !  Output:
  !    PSI      R*4  The function value which, on entry to the subrou-
  !                  tine, was to have been computed.
  !    IER      I*4  An error flag whose meaning is as follows:
  !                     IER = 0  =>  No evident problem.
  !                           1  =>  IALF is outside the allowed range.
  !                           2  =>  IM is outside of the allowed range.
  !                           3  =>  ETA is larger than 1 in absolute
  !                                     value.
  !                          12  =>  IALF and IM are out of bounds.
  !                          13  =>  IALF and ETA are both illegal.
  !                          23  =>  IM and ETA are both illegal.
  !                         123  =>  IALF, IM, and ETA all are illegal.
  !---------------------------------------------------------------------
  integer*4 :: ialf                 !
  integer*4 :: im                   !
  integer*4 :: iflag                !
  real*4 :: eta                     !
  real*4 :: psi                     !
  integer*4 :: ier                  !
  ! Local
  integer*4 :: j
  real*4 :: alpha(5), eta2, x
  real*4 :: p4(5,5), q4(2,5), p5(7,5), q5(5), p6l(5,5), q6l(2,5)
  real*4 :: p6u(5,5), q6u(2,5), p7l(5,5), q7l(2,5), p7u(5,5)
  real*4 :: q7u(2,5), p8l(6,5), q8l(2,5), p8u(6,5), q8u(2,5)
  ! Data
  data alpha / 0., .5, 1., 1.5, 2. /
  data p4 /   &
    1.584774e-2, -1.269612e-1,  2.333851e-1, -1.636744e-1,   &
    5.014648e-2,  3.101855e-2, -1.641253e-1,  2.385500e-1,   &
    -1.417069e-1,  3.773226e-2,  5.007900e-2, -1.971357e-1,   &
    2.363775e-1, -1.215569e-1,  2.853104e-2,  7.201260e-2,   &
    -2.251580e-1,  2.293715e-1, -1.038359e-1,  2.174211e-2,   &
    9.585932e-2, -2.481381e-1,  2.194469e-1, -8.862132e-2,   &
    1.672243e-2 /
  data q4 /   &
    4.845581e-1,  7.457381e-2,  4.514531e-1,  6.458640e-2,   &
    4.228767e-1,  5.655715e-2,  3.978515e-1,  4.997164e-2,   &
    3.756999e-1,  4.448800e-2 /
  data p5 /   &
    3.722238e-3, -4.991683e-2,  1.658905e-1, -2.387240e-1,   &
    1.877469e-1, -8.159855e-2,  3.051959e-2,  8.182649e-3,   &
    -7.325459e-2,  1.945697e-1, -2.396387e-1,  1.667832e-1,   &
    -6.620786e-2,  2.224041e-2,  1.466325e-2, -9.858686e-2,   &
    2.180684e-1, -2.347118e-1,  1.464354e-1, -5.350728e-2,   &
    1.624782e-2,  2.314317e-2, -1.246383e-1,  2.362036e-1,   &
    -2.257366e-1,  1.275895e-1, -4.317874e-2,  1.193168e-2,   &
    3.346886e-2, -1.503778e-1,  2.492826e-1, -2.142055e-1,   &
    1.106482e-1, -3.486024e-2,  8.821107e-3 /
  data q5 /   &
    2.418820e-1,  2.291233e-1,  2.177793e-1,  2.075784e-1,   &
    1.983358e-1 /
  data p6l /   &
    5.613913e-2, -3.019847e-1,  6.256387e-1, -6.324887e-1,   &
    3.303194e-1,  6.843713e-2, -3.342119e-1,  6.302307e-1,   &
    -5.829747e-1,  2.765700e-1,  8.203343e-2, -3.644705e-1,   &
    6.278660e-1, -5.335581e-1,  2.312756e-1,  9.675562e-2,   &
    -3.922489e-1,  6.197133e-1, -4.857470e-1,  1.934013e-1,   &
    1.124069e-1, -4.172349e-1,  6.069622e-1, -4.405326e-1,   &
    1.618978e-1 /
  data q6l /   &
    9.077644e-1,  2.535284e-1,  8.626056e-1,  2.291400e-1,   &
    8.212018e-1,  2.078043e-1,  7.831755e-1,  1.890848e-1,   &
    7.481828e-1,  1.726085e-1 /
  data p6u /   &
    8.531865e-4, -1.616105e-2,  6.888533e-2, -1.109391e-1,   &
    7.747182e-2,  2.060760e-3, -2.558954e-2,  8.595213e-2,   &
    -1.170228e-1,  7.094106e-2,  4.028559e-3, -3.697768e-2,   &
    1.021332e-1, -1.201436e-1,  6.412774e-2,  6.887946e-3,   &
    -4.994202e-2,  1.168451e-1, -1.207733e-1,  5.744210e-2,   &
    1.071895e-2, -6.404749e-2,  1.297386e-1, -1.194208e-1,   &
    5.112822e-2 /
  data q6u /   &
    1.101270e+0,  3.858544e-1,  1.025431e+0,  3.337648e-1,   &
    9.599102e-1,  2.918724e-1,  9.025276e-1,  2.575336e-1,   &
    8.517470e-1,  2.289667e-1 /
  data p7l /   &
    2.460495e-2, -1.640964e-1,  4.340110e-1, -5.705516e-1,   &
    4.418614e-1,  3.070261e-2, -1.879546e-1,  4.565902e-1,   &
    -5.544891e-1,  3.892790e-1,  3.770526e-2, -2.121608e-1,   &
    4.746423e-1, -5.338058e-1,  3.417026e-1,  4.559398e-2,   &
    -2.362670e-1,  4.881998e-1, -5.098448e-1,  2.991635e-1,   &
    5.432500e-2, -2.598752e-1,  4.974791e-1, -4.837861e-1,   &
    2.614838e-1 /
  data q7l /   &
    1.124957e+0,  3.784976e-1,  1.075420e+0,  3.466086e-1,   &
    1.029374e+0,  3.181219e-1,  9.865496e-1,  2.926441e-1,   &
    9.466891e-1,  2.698218e-1 /
  data p7u /   &
    1.924318e-4, -5.044864e-3,  2.979803e-2, -6.660688e-2,   &
    6.792268e-2,  5.030909e-4, -8.639332e-3,  4.018472e-2,   &
    -7.595456e-2,  6.696215e-2,  1.059406e-3, -1.343605e-2,   &
    5.135360e-2, -8.386588e-2,  6.484517e-2,  1.941904e-3,   &
    -1.943727e-2,  6.288221e-2, -9.021607e-2,  6.193000e-2,   &
    3.224785e-3, -2.657664e-2,  7.438627e-2, -9.500554e-2,   &
    5.850884e-2 /
  data q7u /   &
    1.450730e+0,  6.578685e-1,  1.353872e+0,  5.724332e-1,   &
    1.269924e+0,  5.032139e-1,  1.196177e+0,  4.460948e-1,   &
    1.130719e+0,  3.982785e-1 /
  data p8l /   &
    1.378030e-2, -1.097846e-1,  3.625283e-1, -6.522477e-1,   &
    6.684458e-1, -4.703556e-1,  1.721632e-2, -1.274981e-1,   &
    3.917226e-1, -6.562264e-1,  6.305859e-1, -4.067119e-1,   &
    2.121871e-2, -1.461891e-1,  4.185427e-1, -6.543539e-1,   &
    5.904660e-1, -3.507098e-1,  2.580565e-2, -1.656048e-1,   &
    4.426283e-1, -6.473472e-1,  5.494752e-1, -3.018936e-1,   &
    3.098251e-2, -1.854823e-1,  4.637398e-1, -6.359482e-1,   &
    5.086794e-1, -2.595588e-1 /
  data q8l /   &
    1.076975e+0,  3.394154e-1,  1.036132e+0,  3.145673e-1,   &
    9.978025e-1,  2.920529e-1,  9.617584e-1,  2.715949e-1,   &
    9.278774e-1,  2.530051e-1 /
  data p8u /   &
    4.290460e-5, -1.508077e-3,  1.233763e-2, -4.091270e-2,   &
    6.547454e-2, -5.664203e-2,  1.201008e-4, -2.778372e-3,   &
    1.797999e-2, -5.055048e-2,  7.125083e-2, -5.469912e-2,   &
    2.698511e-4, -4.628815e-3,  2.470890e-2, -6.017759e-2,   &
    7.566434e-2, -5.202678e-2,  5.259595e-4, -7.144198e-3,   &
    3.238633e-2, -6.946769e-2,  7.873067e-2, -4.889490e-2,   &
    9.255826e-4, -1.038126e-2,  4.083176e-2, -7.815954e-2,   &
    8.054087e-2, -4.552077e-2 /
  data q8u /   &
    1.379457e+0,  5.786953e-1,  1.300303e+0,  5.135748e-1,   &
    1.230436e+0,  4.593779e-1,  1.168075e+0,  4.135871e-1,   &
    1.111893e+0,  3.744076e-1 /
  !
  ! Code
  ier = 0
  if (ialf.lt.1 .or. ialf.gt.5) ier = 1
  if (im.lt.4 .or. im.gt.8) ier = 2+10*ier
  if (abs(eta).gt.1.) ier = 3+10*ier
  if (ier.ne.0) then
    write(6,*) 'E-SPHEROIDAL,  Error ',ier
    return
  endif
  eta2 = eta**2
  j = ialf
  !
  ! Support width = 4 cells:
  if (im.eq.4) then
    x = eta2-1.
    psi = (p4(1,j)+x*(p4(2,j)+x*(p4(3,j)+x*(p4(4,j)+x*p4(5,j)))))   &
      / (1.+x*(q4(1,j)+x*q4(2,j)))
  !
  ! Support width = 5 cells:
  elseif (im.eq.5) then
    x = eta2-1.
    psi = (p5(1,j)+x*(p5(2,j)+x*(p5(3,j)+x*(p5(4,j)+x*(p5(5,j)   &
      +x*(p5(6,j)+x*p5(7,j)))))))   &
      / (1.+x*q5(j))
  !
  ! Support width = 6 cells:
  elseif (im.eq.6) then
    if (abs(eta).le..75) then
      x = eta2-.5625
      psi = (p6l(1,j)+x*(p6l(2,j)+x*(p6l(3,j)+x*(p6l(4,j)   &
        +x*p6l(5,j))))) / (1.+x*(q6l(1,j)+x*q6l(2,j)))
    else
      x = eta2-1.
      psi = (p6u(1,j)+x*(p6u(2,j)+x*(p6u(3,j)+x*(p6u(4,j)   &
        +x*p6u(5,j))))) / (1.+x*(q6u(1,j)+x*q6u(2,j)))
    endif
  !
  ! Support width = 7 cells:
  elseif (im.eq.7) then
    if (abs(eta).le..775) then
      x = eta2-.600625
      psi = (p7l(1,j)+x*(p7l(2,j)+x*(p7l(3,j)+x*(p7l(4,j)   &
        +x*p7l(5,j))))) / (1.+x*(q7l(1,j)+x*q7l(2,j)))
    else
      x = eta2-1.
      psi = (p7u(1,j)+x*(p7u(2,j)+x*(p7u(3,j)+x*(p7u(4,j)   &
        +x*p7u(5,j))))) / (1.+x*(q7u(1,j)+x*q7u(2,j)))
    endif
  !
  ! Support width = 8 cells:
  elseif (im.eq.8) then
    if (abs(eta).le..775) then
      x = eta2-.600625
      psi = (p8l(1,j)+x*(p8l(2,j)+x*(p8l(3,j)+x*(p8l(4,j)   &
        +x*(p8l(5,j)+x*p8l(6,j)))))) / (1.+x*(q8l(1,j)+x*q8l(2,j)))
    else
      x = eta2-1.
      psi = (p8u(1,j)+x*(p8u(2,j)+x*(p8u(3,j)+x*(p8u(4,j)   &
        +x*(p8u(5,j)+x*p8u(6,j)))))) / (1.+x*(q8u(1,j)+x*q8u(2,j)))
    endif
  endif
  !
  ! Normal return:
  if (iflag.gt.0 .or. ialf.eq.1 .or. eta.eq.0.) return
  if (abs(eta).eq.1.) then
    psi = 0.0
  else
    psi = (1.-eta2)**alpha(ialf)*psi
  endif
end subroutine sphfn
!
subroutine grdflt (ctypx, ctypy, xparm, yparm)
  !---------------------------------------------------------------------
  !     GRDFLT determines default parameters for the convolution functions
  !     If no convolving type is chosen, an Spheroidal is picked.
  !     Otherwise any unspecified values ( = 0.0) will be set to some
  !     value.
  ! Arguments:
  !     CTYPX,CTYPY           I  Convolution types for X and Y direction
  !                                1 = pill box
  !                                2 = exponential
  !                                3 = sinc
  !                                4 = expontntial * sinc
  !                                5 = spheroidal function
  !     XPARM(10),YPARM(10)   R*4  Parameters for the convolution fns.
  !                                (1) = support radius (cells)
  !---------------------------------------------------------------------
  integer :: ctypx                  !
  integer :: ctypy                  !
  real*4 ::    xparm(10)            !
  real*4 ::    yparm(10)            !
  ! Local
  character(len=12) :: chtyps(5)
  integer :: numprm(5), i, k
  data numprm /1, 3, 2, 4, 2/
  data chtyps /'Pillbox','Exponential','Sin(X)/(X)',   &
    'Exp*Sinc','Spheroidal'/
  !
  ! Default = type 5
  if ((ctypx.le.0) .or. (ctypx.gt.5)) ctypx = 5
  goto (110,120,130,140,150) ctypx
  !
  ! Pillbox
110 continue
  if (xparm(1).le.0.0) xparm(1) = 0.5
  go to 500
  ! Exponential.
120 continue
  if (xparm(1).le.0.0) xparm(1) = 3.0
  if (xparm(2).le.0.0) xparm(2) = 1.00
  if (xparm(3).le.0.0) xparm(3) = 2.00
  go to 500
  ! Sinc.
130 continue
  if (xparm(1).le.0.0) xparm(1) = 3.0
  if (xparm(2).le.0.0) xparm(2) = 1.14
  go to 500
  ! Exponential * sinc
140 continue
  if (xparm(1).le.0.0) xparm(1) = 3.0
  if (xparm(2).le.0.0) xparm(2) = 1.55
  if (xparm(3).le.0.0) xparm(3) = 2.52
  if (xparm(4).le.0.0) xparm(4) = 2.00
  go to 500
  ! Spheroidal function
150 continue
  if (xparm(1).le.0.0) xparm(1) = 3.0
  if (xparm(2).le.0.0) xparm(2) = 1.0
  go to 500
  !
  ! Put default cheking here for further function types
500 continue
  ! Check Y convolution defaults
  if ((ctypy.gt.0) .and. (ctypy.le.5)) go to (610,620,630,   &
    640,650) ctypy
  ! Use X values
  ctypy = ctypx
  do i = 1,10
    yparm(i) = xparm(i)
  enddo
  goto 900
  ! Pillbox
610 continue
  if (yparm(1).le.0.0) yparm(1) = 0.5
  go to 900
  ! Exponential
620 continue
  if (yparm(1).le.0.0) yparm(1) = 3.0
  if (yparm(2).le.0.0) yparm(2) = 1.0
  if (yparm(3).le.0.0) yparm(3) = 2.0
  go to 900
  ! Sinc
630 continue
  if (yparm(1).le.0.0) yparm(1) = 3.0
  if (yparm(2).le.0.0) yparm(2) = 1.14
  go to 900
  ! Exponential * sinc
640 continue
  if (yparm(1).le.0.0) yparm(1) = 3.0
  if (yparm(2).le.0.0) yparm(2) = 1.55
  if (yparm(3).le.0.0) yparm(3) = 2.52
  if (yparm(4).le.0.0) yparm(4) = 2.00
  go to 900
  ! Spheroidal function
650 continue
  if (yparm(1).le.0.0) yparm(1) = 3.0
  if (yparm(2).le.0.0) yparm(2) = 1.0
  go to 900
  !
  ! Put default checking for new types here.
  ! Print parameters chosen.
900 continue
  write(6,1001) 'I-GRDFLT, X',chtyps(ctypx),(xparm(k),k=1   &
    ,numprm(ctypx))
  write(6,1001) 'I-GRDFLT, Y',chtyps(ctypy),(yparm(k),k=1   &
    ,numprm(ctypy))
1001 format(1x,a,' Convolution ',a,' Par.=',5f8.4)
end subroutine grdflt
!
subroutine dosor  (visi,nd,np,we,iy)
  use gkernel_interfaces
  use gildas_def
  integer :: nd                     !
  integer :: np                     !
  real :: visi(nd,np)               ! Corrected 22-Mar-1995
  real :: we(nd)                    ! Corrected 22-Mar-1995
  integer :: iy                     !
  ! Global
  integer :: trione
  ! Local
  integer :: i,ier
  do i=1,np-1
    if (visi(iy,i).gt.visi(iy,i+1)) then
      write(6,*) 'I-DOSOR,  Sorting input table'
      ier = trione (visi,nd,np,iy,we)
      if (ier.ne.1) call sysexi (fatale)
      return
    endif
  enddo
  write(6,*) 'I-DOSOR,  Input table is Sorted'
end subroutine dosor
!
function trione (x,nd,n,ix,work)
  !---------------------------------------------------------------------
  ! 	Sorting program that uses a quicksort algorithm.
  !	Sort on one row
  !	X	R*4(*)	Unsorted array				Input
  !	ND	I	First dimension of X 			Input
  !	N	I	Second dimension of X			Input
  !	IX	I	X(IX,*) is the key for sorting		Input
  !	WORK	R*4(ND)	Work space for exchange			Input
  !---------------------------------------------------------------------
  integer :: trione                 !
  integer :: nd                     !
  integer :: n                      !
  real*4 :: x(nd,n)                 !
  integer :: ix                     !
  real*4 :: work(nd)                !
  ! Local
  integer :: maxstack,nstop
  parameter (maxstack=1000,nstop=15)
  integer*4 :: i, j, k, l1, r1
  integer*4 :: l, r, m, lstack(maxstack), rstack(maxstack), sp
  real*4 :: key
  logical :: mgtl, lgtr, rgtm
  !
  trione = 1
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! Set KEY = median of X(L), X(M), X(R)
  ! No! This is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! To fix this problem, I found (but I cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. P.V.
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  l1=(2*l+r)/3
  r1=(l+2*r)/3
  !
  mgtl = x(ix,m) .gt. x(ix,l)
  rgtm = x(ix,r) .gt. x(ix,m)
  !
  ! Algorithm to select the median key. The original one from MONGO
  ! was completely wrong. P. Valiron, 24-Jan-84 .
  !
  !		   	MGTL	RGTM	LGTR	MGTL.EQV.LGTR	MEDIAN_KEY
  !
  !	KL < KM < KR	T	T	*	*		KM
  !	KL > KM > KR	F	F	*	*		KM
  !
  !	KL < KM > KR	T	F	F	F		KR
  !	KL < KM > KR	T	F	T	T		KL
  !
  !	KL > KM < KR	F	T	F	T		KL
  !	KL > KM < KR	F	T	T	F		KR
  !
  if (mgtl .eqv. rgtm) then
    key = x(ix,m)
  else
    lgtr = x(ix,l) .gt. x(ix,r)
    if (mgtl .eqv. lgtr) then
      key = x(ix,l)
    else
      key = x(ix,r)
    endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
10 if (x(ix,i).ge.key) goto 11
  i = i + 1
  goto 10
11 continue
  ! Find a small record on the right
20 if (x(ix,j).le.key) goto 21
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  call r4tor4 (x(1,i),work,nd)
  call r4tor4 (x(1,j),x(1,i),nd)
  call r4tor4 (work,x(1,j),nd)
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! Push the two halves on the stack
2 continue
  if (j-l+1 .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(6,*) 'E-SORT,  Stack overflow ',sp
      trione = 0
      return
    endif
    lstack(sp) = l
    rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(6,*) 'E-SORT,  Stack overflow ',sp
      trione = 0
      return
    endif
    lstack(sp) = j+1
    rstack(sp) = r
  endif
  !
  ! Anything left to process?
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do 110 j = n-1,1,-1
    k = j
    do i = j+1,n
      if (x(ix,j).le.x(ix,i)) goto 121
      k = i
    enddo
121 continue
    if (k.eq.j) goto 110
    call r4tor4 (x(1,j),work,nd)
    do i = j+1,k
      call r4tor4 (x(1,i),x(1,i-1),nd)
    enddo
    call r4tor4 (work,x(1,k),nd)
110 continue
end function trione
!
subroutine domask (nc, n, x, w, wm, bval)
  !---------------------------------------------------------------------
  !     mask points with weights lower than wm
  !---------------------------------------------------------------------
  integer :: nc                     !
  integer :: n                      !
  real :: x(nc, n)                  !
  real :: w(n)                      !
  real :: wm                        !
  real :: bval                      !
  ! Local
  integer :: i, j
  !
  do i = 1, n
    if (w(i) .le. wm) then
      do j = 1, nc
        x(j,i) = 0             ! bval
      enddo
    endif
  enddo
end subroutine domask
!
subroutine  nearfield(nx,ny,p,kw,df,distance,diameter,f)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  !     Correct phase map for near field effects.
  !     This means geometrical pathlength variation due to
  !     1)- finite distance of transmitter (DISTANCE in m)
  !       dp1 = r^2/(2*R) - r^4 / (8*R^3)
  !     2)- compensating displacement of receiver DF (in m) from prime focus.
  !       dp2 = DF* (1- DF * cos(alpha) + DF^2 * sin(alpha)^2 /(2*rho)
  ! where
  !     r = radius in aperture plane                     r   = 2*f*t
  !     alpha = angle between ray from focus and  axis:  t   = tan(alpha/2)
  !     rho = ray length between focus and parabola:     rho = f*(1+t^2)
  !     kw = wave vector (2*pi/lambda) in rad / m
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  real :: p(nx,ny)                  !
  real*8 :: kw                      !
  real :: df                        !
  real :: distance                  !
  real :: diameter                  !
  real :: f                         !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  logical :: error
  integer :: ix, iy
  real :: xx, yy, t, dp, r2, t2, z
  !-------------------------------------------------------------------------
  do iy=1, ny
    yy = (yima%gil%val(2)+(iy-yima%gil%ref(2))*yima%gil%inc(2))
    do ix= 1, nx
      xx = (yima%gil%val(1)+(ix-yima%gil%ref(1))*yima%gil%inc(1))
      r2 = xx**2 + yy**2
      t2 = r2 /4/f**2
      z = f*t2
      dp = r2 /2/distance - r2**2 /8 /distance**3   &
        +  sqrt(r2+(f+df-z)**2) - (f+z +df)
      if (abs(p(ix,iy)-yima%gil%bval).gt.yima%gil%eval) then
        p(ix,iy) = p(ix,iy) + kw * dp
      endif
    enddo
  enddo
end subroutine nearfield
!
subroutine load_x_image(name,type,error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  !---------------------------------------------------------------------
  !     open and read X image header
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  character(len=*) :: type          !
  logical :: error                  !
  ! Local
  integer :: n
!  integer(kind=index_length) :: blc(4), trc(4)
!  data blc/0,0,0,0/, trc /0,0,0,0/
  !
  n = lenc(name)
  if (n.eq.0) then
    call gagout('F-GDF_READ_X,  Invalid name')
    error = .true.
    return
  endif
  call gildas_null(xima)
  xima%gil%ndim = 3 ! This is what we expect
  call gdf_read_gildas (xima, name, type, error)
  xima%loca%addr = locwrd (xima%r3d)
!  call sic_parsef(name,xima%file,' ',type)
!  call gdf_geis (xima%loca%islo,error)
!  if (error) return
!  call gdf_reis (xima%loca%islo,xima%char%type,xima%file,xima%gil%form,xima%loca%size,error)
!  if (error) return
!  call gdf_read (xima,xima%loca%islo,error)
!  if (error) return
!  call gdf_gems (xima%loca%mslo,xima%loca%islo,blc,trc,xima%loca%addr,xima%gil%form,error)
  return
end subroutine load_x_image
!
subroutine reference_feed(nx,ny,x,kw, center)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  !     do reference feed correction
  !     correct for the phase effect due to the motion of the reference feed
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  complex :: x(nx,ny)               !
  real :: kw                        !
  real :: center                    !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  real :: df
  !
  real :: u2, v2, dp, rad, wvl
  complex :: factor
  integer :: ix, iy
  !
  wvl = 299.792458/xima%gil%freq
  factor = cmplx(0.,2*pi/wvl)  ! apparently no !!!
  rad = center+focus
  !
  write(6,*) 'correction for reference feed motion rad ',rad
  write(6,*) 'x_val2, x_ref2, x_inc2'
  write(6,*) 'x_val1, x_ref1, x_inc1'
  write(6,*) xima%gil%convert(:,2)
  write(6,*) xima%gil%convert(:,3)
  !
  do iy=1, ny
    v2 = (xima%gil%val(3)+(iy-xima%gil%ref(3))*xima%gil%inc(3))**2
    do ix= 1, nx
      u2 = (xima%gil%val(2)+(ix-xima%gil%ref(2))*xima%gil%inc(2))**2
      dp = rad*(u2+v2)/2
      if (abs(x(ix,iy)-xima%gil%bval).gt.xima%gil%eval) then
        x(ix,iy) = x(ix,iy) * exp(factor * dp)
      !$$$               if (ix.eq.124) print *, iy, dp
      endif
    enddo
  enddo
end subroutine reference_feed

subroutine grid_corr(nx, ny, data)
  ! dummy
  integer :: nx, ny
  real ::  data(2, nx, ny)
  ! Local
  integer :: i, j, k
  real, allocatable :: corx(:), cory(:)
  real :: ubias,vbias,ubuff(4096),vbuff(4096)
  common /conv/ ubias, vbias, ubuff, vbuff
  real :: x

  allocate (corx(nx), cory(ny))
  call grdtab (nx, ubuff, ubias, corx)
  call grdtab (ny, vbuff, vbias, cory)
  x = corx(nx/2+1)*cory(ny/2+1)
  do j=1,ny
    do i=1,nx
      if (corx(i).ge.1e-30.and.cory(j).ge.1e-30) then
        do k = 1, 2
          data (k, i,j) = data(k, i,j) * x/(corx(i)*cory(j))
        enddo
      endif
    enddo
  enddo
      !
end subroutine grid_corr
!
subroutine grdtab (n, buff, bias, corr)
  !---------------------------------------------------------------------
  ! GILDAS	UVMAP
  ! Compute fourier transform of gridding function
  !	N	I*4	Number of pixels, assuming center on N/2+1
  !	BUFF	R*4(*)	Gridding function, tabulated every 1/100 cell
  !	BIAS	R*4	Center of gridding function
  !	CORR	R*4(N)	Gridding correction FT
  !---------------------------------------------------------------------
  integer :: n                      !
  real :: buff(*)                   !
  real :: bias                      !
  real :: corr(n)                   !
  ! Local
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real*8 :: pi
  parameter (pi=3.14159265358979323846d0)
  integer :: i,j,m,l
  real :: kw,kx
  !
  do j=1,n
    corr(j) = 0.0
  enddo
  m = n/2+1
  kw = 0.01 * pi / m
  l = 2*bias+1
  do i=1,l
    if (buff(i).ne.0.0) then
      kx  = kw*(float(i)-bias)
      do j=1,n
        corr(j) = corr(j) + buff(i) * cos(kx*float(j-m))
      enddo
    endif
  enddo
end subroutine grdtab
