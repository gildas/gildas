subroutine write_sdm_history(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the History  table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_History
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (HistoryRow) :: hRow
  type (HistoryKey) :: hKey
  character :: sdmTable*12, uname*80
  integer :: l, count
  data count/ 0/
  save count
  parameter (sdmTable='History')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !     (no array to allocate)
  !
  !     very much *TBD*
  !
  hKey%time = time_interval(1)
  hKey%execBlockId = execBlock_Id
  call user_info(uname)
  l = lenc(uname)
  hRow%message = uname(1:l)
  call sdmMessage(1,1,sdmTable , 'Message: '//hRow%message(1:l))
  hRow%priority = '-'
  hRow%origin = '-'
  hRow%application = 'CLIC'
  hRow%cliCommand = 'WRITE '
  hRow%appParms = '-'
  hRow%objectId = 'whateverObjectId...'
  if (count.ne.r_scan) then
    count = r_scan
    call addHistoryRow(hKey, hRow, error)
    if (error) return
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
!
end subroutine write_sdm_history
!---------------------------------------------------------------------
subroutine get_sdm_history(error)
  !---------------------------------------------------------------------
  ! Read the History table
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_History
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (HistoryRow) :: hRow
  type (HistoryKey), allocatable :: hKeyList(:)
  integer :: historySize, lenz, i, l, ier
  character*12 , parameter :: sdmTable='History'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  call getHistoryTableSize(historySize, error)
  if (error) return
  !
  if (historySize.gt.0) then
    if (allocated(hKeyList)) then
      deallocate (hKeyList, stat = ier)
      if (ier.ne.0) then
        call sdmMessageI(8,4,sdmTable, 'Deallocate error: ',ier)
        error = .true.
        return
      endif
    endif
    allocate (hKeyList(historySize), stat = ier)
    if (ier.ne.0) then
      call sdmMessageI(8,4,sdmTable, 'Allocate error: ',ier)
      error = .true.
      return
    endif
    call  getHistoryKeys(historySize, hKeyList, error)
    !
    do i=1, historySize
      if (hKeyList(i)%time.ge.time_interval(1) .and. hKeyList(i   &
        )%time.le.time_interval(1)+time_interval(2)) then
        call getHistoryRow(hKeyList(i), hRow, error)
        if (error) return
        l = lenz(hRow%Message)
        call sdmMessage(1,1,sdmTable, 'History: '//hRow%Message(1:l))
      endif
    enddo
  endif
  call sdmMessage(1,1,sdmTable ,'Read.')
!
end subroutine get_sdm_history
!---------------------------------------------------------------------
