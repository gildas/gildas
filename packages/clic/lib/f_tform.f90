!
subroutine f_tform(ch,i,code)
  implicit none
  character(len=*) :: ch            !
  integer :: i                      !
  character(len=1) :: code          !
  ! Local
  character(len=10) :: c10
  integer :: k
  !
  write(c10,'(i10)') i
  k = 1
  do while (index(c10(k:10),' ').ne.0)
    k = k+1
  enddo
  ch = c10(k:10)//code//char(0)
end subroutine f_tform
