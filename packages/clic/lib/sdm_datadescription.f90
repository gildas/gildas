subroutine write_sdm_DataDescription(holodata, error)
  !---------------------------------------------------------------------
  ! Write the dataDescription SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_DataDescription
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical :: holodata, error
  ! Local
  type (DataDescriptionRow) :: ddRow
  type (DataDescriptionKey) :: ddKey
  integer ilc, isb, ic, ivc
  character sdmTable*16
  parameter (sdmTable='DataDescription')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !     This assumes that we have a single polarisation configuration at Bure
  !     (to be changed in the future ...)
  !
  !cc OPTIONAL IN SS2 ...      ddRow%flagRow = .false.
  !
  !     Holography case:
  !
  if (holodata) then
    ddRow%polOrHoloId = polarization_Id(1)
    ddRow%spectralWindowId = totPowSpectralWindow_Id(1)
    call addDataDescriptionRow(ddKey, ddRow, holodata, error)
    if (error) return
    totPowDataDescription_Id = ddKey%DataDescriptionId
  !
  !     Normal case (correlator)
  !
  else
    !
    !     loop on cont/line subbands
    do ilc = 1, 2
      !
      !     loop on spectral subbands ('virtual' basebands)
      do ivc = 1, v_lband
        !
        !     loop on side bands
        do isb = 1, r_lnsb
          ddRow%polOrHoloId = polarization_Id(ivc)
          ddRow%spectralWindowId = spectralWindow_Id(isb,   &
            v_bands(ivc,1), ilc)
          call addDataDescriptionRow(ddKey, ddRow, holodata,   &
            error)
          if (error) return
          DataDescription_Id(isb,ivc,ilc) =   &
            ddKey%DataDescriptionId
        !$$$                  print *, isb, ivc, ilc, v_bands(ivc,1),
        !$$$     &                 spectralWindow_Id(isb,v_bands(ivc,1),ilc),
        !$$$     &                 DataDescription_Id(isb,ivc,ilc)
        enddo
      enddo
    enddo
    !
    ! Data description for continuum detector
    ! the sideband to use:
    ddRow%polOrHoloId = totPowPolarization_Id
    if (r_isb.eq.1) then
      ddRow%spectralWindowId = totPowSpectralWindow_Id(1)  ! USB
    else
      ddRow%spectralWindowId = totPowSpectralWindow_Id(2)  ! LSB
    endif
    call addDataDescriptionRow(ddKey, ddRow, holodata, error)
    if (error) return
    totPowDataDescription_Id = ddKey%DataDescriptionId
  endif
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_DataDescription
!---------------------------------------------------------------------
subroutine get_sdm_DataDescription(error)
  !---------------------------------------------------------------------
  ! Read the dataDescription SDM table.
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_DataDescription
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (DataDescriptionRow) :: ddRow
  type (DataDescriptionKey) :: ddKey
  integer i
  character sdmTable*16
  parameter (sdmTable='DataDescription')
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  ! do this for all the data description Ids...
  !
  do i = 1, ddId_size
    ddKey%DataDescriptionId = ddId_List(i)
    call getDataDescriptionRow(ddKey, ddRow, error)
    if (polarization_Id(1).ge.0 .and. ddRow%polOrHoloId.ne.   &
        polarization_Id(1)) then
      call sdmMessage(6,2,sdmTable   &
        ,'A single polarization_Id is allowed'//   &
        ' for each Bure observation')
      goto 99
    else
      polarization_Id(1) = ddRow%polOrHoloId
    endif
    swId_List(i) = ddRow%spectralWIndowId
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_DataDescription
!---------------------------------------------------------------------
