subroutine clic_variable(line,error)
  !---------------------------------------------------------------------
  ! CLIC command VARIABLE group ON|OFF
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  integer :: mvocab, mvoc2, nkey, nkw
  parameter (mvocab=23, mvoc2=2)
  character(len=12) :: argum2, vocab(mvocab), keywor, voc2(mvoc2)
  logical :: off
  !
  data voc2/'ON','OFF'/
  data vocab/       &
    'READ_WRITE',   &   ! 01  read-write variables
    'GENERAL',      &   ! 02
    'POSITION',     &   ! 03
    'CONFIG',       &   ! 04
    'RF_SETUP',     &   ! 05
    'CONTINUUM',    &   ! 06
    'LINE',         &   ! 07
    'SCANNING',     &   ! 08
    'ATMOSPHERE',   &   ! 09
    'DESCRIPTOR',   &   ! 10
    'DATA_HEADER',  &   ! 11
    'DATA_RECORD',  &   ! 12
    'MONITOR',      &   ! 13
    'INDEX',        &   ! 14
    'WVR',          &   ! 15
    'ALMA',         &   ! 16
    'AIC',          &   ! 17
    'SIZE',         &   ! 18
    'INSTRUMENT',   &   ! 19
    'SPW',          &   ! 20
    'STATUS',       &   ! 21
    'VLBI',         &   ! 22
    'MODIFIERS' /       ! 23
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  keywor = ' '
  call clic_kw(line,0,1,keywor,nkey,vocab,mvocab,.true.,error,.true.)
  if (error) return
  argum2 = 'ON'
  call clic_kw(line,0,2,argum2,nkw,voc2,mvoc2,.false.,error,.true.)
  if (error) return
  off = argum2.eq.'OFF'
  !
  goto  (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23) nkey
  call message(6,3,'CLIC_VARIABLE',keywor//' not implemented')
999 error = .true.
  return
  !
1 call rw_variables(off)
  return
2 call general_variables(off)
  return
3 call position_variables(off)
  return
4 call config_variables(off)
  return
5 call rf_variables(off)
  return
6 call contin_variables(off)
  return
7 call line_variables(off)
  return
8 call scanning_variables(off)
  return
9 call atmos_variables(off)
  return
10 call descr_variables(off)
  return
11 call datah_variables(off)
  return
12 call data_variables(off)
  return
13 call monitor_variables(off)
  return
14 call inds_variables(off)
  return
15 call wvr_variables(off)
  return
16 call alma_variables(off)
  return
17 call aic_variables(off)
  return
18 call size_variables(off)
  return
19 call instrument_variables(off)
  return
20 call spw_variables(off)
  return
21 call status_variables(off)
  return
22 call vlbi_variables(off)
  return
23 call modifier_variables(off)
  return
end subroutine clic_variable
!
subroutine index_variable
  use gildas_def
  use gkernel_interfaces
  use clic_title
  use clic_index
  ! Local
  logical :: error,off
  integer(kind=index_length) :: dim(4)
  !
  if (cxnext-1.eq.nindex) return
  call sic_delvariable ('CX_NUM',.false.,error)
  call sic_delvariable ('CX_BLOC',.false.,error)
  nindex = cxnext-1
  if (nindex.eq.0) return
  dim(1) = nindex
  call sic_def_long ('CX_NUM', cx_num,1,dim,.false.,error)
  call sic_def_long ('CX_BLOC', cx_bloc,1,dim,.false.,error)
  return
  !
entry found_variable
  call sic_def_long ('FOUND',nindex,0,0,.false.,error)
  return
  !
entry inds_variables(off)
  !
  ! Data
  if (off) then
    call sic_delvariable('L_INDEX',.false.,error)
  endif
  dim(1) = lind_v2
  call sic_def_inte('L_INDEX',tbuf,1,dim,.false.,error)
  return
!
end subroutine index_variable
!
subroutine rw_variables(off)
  use gildas_def
  use gkernel_interfaces
  use clic_file
  use clic_index
  !---------------------------------------------------------------------
  ! CLIC 	Initialisation routine
  !	defines all RW variables
  !---------------------------------------------------------------------
  logical :: off                    !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  logical :: error
  integer(kind=index_length) :: dim(4)
  !------------------------------------------------------------------------
  ! General
  if (off) then
    call sic_delvariable ('IN_FILE',.false.,error)
    call sic_delvariable ('OUT_FILE',.false.,error)
    call  sic_delvariable ('IDATA',.false.,error)
    call  sic_delvariable ('INSEC',.false.,error)
    call  sic_delvariable ('INEXT',.false.,error)
    call  sic_delvariable ('ONEXT',.false.,error)
    call  sic_delvariable ('IXNUM',.false.,error)
    call  sic_delvariable ('ISEC',.false.,error)
    call  sic_delvariable ('ILEN',.false.,error)
    call  sic_delvariable ('IADD',.false.,error)
    call  sic_delvariable ('IVERSION',.false.,error)
    call  sic_delvariable ('DVERSION',.false.,error)
    call  sic_delvariable ('IXNEXT',.false.,error)
    call  sic_delvariable ('OXNEXT',.false.,error)
    call  sic_delvariable ('CXNEXT',.false.,error)
    call  sic_delvariable ('INWORD',.false.,error)
    call  sic_delvariable ('IDATAL',.false.,error)
    call  sic_delvariable ('MODIFY',.false.,error)
    call  sic_delvariable ('SHARE',.false.,error)
    call  sic_delvariable ('IX_NUM',.false.,error)
    call  sic_delvariable ('IX_BLOC',.false.,error)
    call  sic_delvariable ('OX_NUM',.false.,error)
    call  sic_delvariable ('OX_BLOC',.false.,error)
    return
  endif
  !
  call sic_def_char ('IN_FILE',i%spec,.false.,error)
  call sic_def_char ('OUT_FILE',o%spec,.false.,error)
  call sic_def_long ('IDATA',e%adata,0,dim,.false.,error)
  call sic_def_inte ('INSEC',e%nsec,0,dim,.false.,error)
  call sic_def_long ('INEXT',i%desc%nextrec,0,dim,.false.,error)
  call sic_def_long ('ONEXT',o%desc%nextrec,0,dim,.false.,error)
  call sic_def_long ('IXNUM',e%xnum,0,dim,.false.,error)
  dim(1) = e%nsec
  call sic_def_inte ('ISEC',e%seciden,1,dim,.false.,error)
  call sic_def_long ('ILEN',e%secleng,1,dim,.false.,error)
  call sic_def_long ('IADD',e%secaddr,1,dim,.false.,error)
  call sic_def_inte ('IVERSION',e%version,0,dim,.false.,error)
  call sic_def_inte ('DVERSION',de%version,0,dim,.false.,error)
  call sic_def_long ('IXNEXT',i%desc%xnext,0,dim,.false.,error)
  call sic_def_long ('OXNEXT',o%desc%xnext,0,dim,.false.,error)
  call sic_def_long ('CXNEXT',cxnext,0,dim,.false.,error)
  call sic_def_long ('INWORD',e%nword,0,dim,.false.,error)
  call sic_def_long ('IDATAL',e%ldata,-1,dim,.false.,error)
  call sic_def_logi ('MODIFY',modify,.false.,error)
!  call sic_def_logi ('SHARE',share,.false.,error)
  !SG!	call sic_def_inte ('FOUND',nindex,0,dim,.false.,error)
  dim(1) = m_ix
  call sic_def_long ('IX_NUM',ix_num,1,dim,.false.,error)
  call sic_def_long ('IX_BLOC',ix_bloc,1,dim,.false.,error)
  dim(1) = m_ox
  call sic_def_long ('OX_NUM',ox_num,1,dim,.false.,error)
  call sic_def_long ('OX_BLOC',ox_bloc,1,dim,.false.,error)
!SG!	dim(1) = m_cx
!SG!	call sic_def_inte ('CX_NUM',cx_num,1,dim,.false.,error)
!SG!	call sic_def_inte ('CX_BLOC',cx_bloc,1,dim,.false.,error)
end subroutine rw_variables
!
subroutine general_variables(off)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC 	Initialisation routine
  !	defines all SIC variables
  !---------------------------------------------------------------------
  logical :: off                    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  ! Local
  logical :: error
  integer(kind=index_length) :: dim(4)
  real*8 :: air_mass
  real*8,save :: airmass
  character(len=*), parameter :: struct='SPW'
  !
  !------------------------------------------------------------------------
  ! General
  if (off) then
    call sic_delvariable('RECEIVER',.false.,error)
    call sic_delvariable('TELESCOPE',.false.,error)
    call sic_delvariable('CONFIGURATION',.false.,error)
    call sic_delvariable('NUMBER',.false.,error)
    call sic_delvariable('VERSION',.false.,error)
    call sic_delvariable('DOBS',.false.,error)
    call sic_delvariable('DATE_OBSERVED',.false.,error)
    call sic_delvariable('DRED',.false.,error)
    call sic_delvariable('DATE_REDUCED',.false.,error)
    call sic_delvariable('DATATYPE',.false.,error)
    call sic_delvariable('IQUAL',.false.,error)
    call sic_delvariable('QUALITY',.false.,error)
    call sic_delvariable('SCAN',.false.,error)
    call sic_delvariable('UTOBS',.false.,error)
    call sic_delvariable('TIME_OBSERVED',.false.,error)
    call sic_delvariable('LSTOBS',.false.,error)
    call sic_delvariable('AZIMUTH',.false.,error)
    call sic_delvariable('ELEVATION',.false.,error)
    call sic_delvariable('AIR_MASS',.false.,error)
    call sic_delvariable('TSYS',.false.,error)
    call sic_delvariable('TIME',.false.,error)
    call sic_delvariable('PROCEDURE',.false.,error)
    call sic_delvariable('PROC',.false.,error)
    call sic_delvariable('PROJECT',.false.,error)
    call sic_delvariable('TYPE_OFF',.false.,error)
    call sic_delvariable('N_BOXES',.false.,error)
    call sic_delvariable('NEW_RECEIVERS',.false.,error)
    return
  endif
  call sic_def_char ('TELESCOPE',r_teles,.false.,error)
  call sic_def_inte ('RECEIVER',r_nrec,0,0,.false.,error)
  call sic_def_char ('CONFIGURATION',r_config,.false.,error)
  call sic_def_long ('NUMBER',r_num,0,0,.false.,error)
  call sic_def_inte ('VERSION',r_ver,0,dim,.false.,error)
  call sic_def_inte ('DOBS',r_dobs,0,dim,.false.,error)
  call sic_def_char ('DATE_OBSERVED',r_cdobs,.false.,error)
  call sic_def_char ('DATE_REDUCED',r_cdred,.false.,error)
  call sic_def_inte ('DRED',r_dred,0,dim,.false.,error)
  call sic_def_inte ('DATATYPE',r_kind,0,dim,.false.,error)
  call sic_def_inte ('IQUAL',r_qual,0,dim,.false.,error)
  call sic_def_char ('QUALITY',r_cqual,.false.,error)
  call sic_def_inte ('SCAN',r_scan,0,dim,.false.,error)
  call sic_def_dble ('UTOBS',r_ut,0,dim,.false.,error)
  call sic_def_char ('TIME_OBSERVED',r_ctobs,.false.,error)
  call sic_def_dble ('LSTOBS',r_st,0,dim,.false.,error)
  call sic_def_real ('AZIMUTH',r_az,0,dim,.false.,error)
  call sic_def_real ('ELEVATION',r_el,0,dim,.false.,error)
  airmass = air_mass(r_el)
  call sic_def_dble ('AIR_MASS',airmass,0,dim,.false.,error)
  call sic_def_real ('TSYS',r_tsys,0,dim,.false.,error)
  call sic_def_real ('TIME',r_time,0,dim,.false.,error)
  call sic_def_char ('PROJECT',r_project,.false.,error)
  call sic_def_inte ('PROC',r_proc,0,dim,.false.,error)
  call sic_def_char ('PROCEDURE',r_cproc,.false.,error)
  call sic_def_inte ('TYPE_OFF',r_typof,0,dim,.false.,error)
  call sic_def_inte ('N_BOXES',n_boxes,0,dim,.false.,error)
  call sic_def_logi ('NEW_RECEIVERS',new_receivers,.true.,error)
  return
  !<FF>
entry position_variables(off)
  !
  ! Position
  if (off) then
    call sic_delvariable('SOURCE',.false.,error)
    call sic_delvariable('LAMBDA',.false.,error)
    call sic_delvariable('BETA',.false.,error)
    call sic_delvariable('OFF_LAMBDA',.false.,error)
    call sic_delvariable('OFF_BETA',.false.,error)
    call sic_delvariable('EPOCH',.false.,error)
    call sic_delvariable('PROJECTION',.false.,error)
    call sic_delvariable('FOCUS',.false.,error)
    call sic_delvariable('FLUX',.false.,error)
    call sic_delvariable('SPIDX',.false.,error)
    call sic_delvariable('STOKES_I',.false.,error)
    call sic_delvariable('STOKES_Q',.false.,error)
    call sic_delvariable('STOKES_U',.false.,error)
    call sic_delvariable('STOKES_V',.false.,error)
    call sic_delvariable('STOKES_SPIDX',.false.,error)
    call sic_delvariable('TYPE_COORD',.false.,error)
    return
  endif
  call sic_def_char ('SOURCE',r_sourc,.false.,error)
  call sic_def_dble ('LAMBDA',r_lam,0,dim,.false.,error)
  call sic_def_dble ('BETA',r_bet,0,dim,.false.,error)
  call sic_def_real ('OFF_LAMBDA',r_lamof,0,dim,.false.,error)
  call sic_def_real ('OFF_BETA',r_betof,0,dim,.false.,error)
  call sic_def_real ('EPOCH',r_epoch,0,dim,.false.,error)
  call sic_def_inte ('PROJECTION',r_proj,0,dim,.false.,error)
  call sic_def_real ('FOCUS',r_focus,0,dim,.false.,error)
  call sic_def_real ('FLUX',r_flux,0,dim,.false.,error)
  call sic_def_real ('SPIDX',r_spidx,0,dim,.false.,error)
  call sic_def_real ('STOKES_I',r_stokes_i,0,dim,.false.,error)
  call sic_def_real ('STOKES_Q',r_stokes_q,0,dim,.false.,error)
  call sic_def_real ('STOKES_U',r_stokes_u,0,dim,.false.,error)
  call sic_def_real ('STOKES_V',r_stokes_v,0,dim,.false.,error)
  dim(1) = mnstokes
  call sic_def_real ('STOKES_SPIDX',r_spidx_stokes,1,dim,.false.,error)
  call sic_def_inte ('TYPE_COORD',r_typec,0,dim,.false.,error)
  return
  !<FF>
entry config_variables(off)
  !
  ! Configuration
  if(off) then
    call sic_delvariable('NANT',.false.,error)
    call sic_delvariable('NBAS',.false.,error)
    call sic_delvariable('ITYPE',.false.,error)
    call sic_delvariable('HOUR_ANGLE',.false.,error)
    call sic_delvariable('STATION',.false.,error)
    call sic_delvariable('PHYS_ANT',.false.,error)
    call sic_delvariable('CORR_INPUT',.false.,error)
    call sic_delvariable('START_ANTENNA',.false.,error)
    call sic_delvariable('END_ANTENNA',.false.,error)
    call sic_delvariable('BASELINE',.false.,error)
    call sic_delvariable('ANTENNA',.false.,error)
    call sic_delvariable('AXES_OFFSET',.false.,error)
    call sic_delvariable('PHLO1',.false.,error)
    call sic_delvariable('PHLO3',.false.,error)
    call sic_delvariable('NBAND_WIDEX',.false.,error)
    return
  endif
  call sic_def_inte ('NANT',r_nant,0,dim,.false.,error)
  call sic_def_inte ('NBAS',r_nbas,0,dim,.false.,error)
  call sic_def_inte ('ITYPE',r_itype,0,dim,.false.,error)
  call sic_def_real ('HOUR_ANGLE',r_houra,0,dim,.false.,error)
  dim(1) = mnant
  call sic_def_inte ('STATION',r_istat,1,dim,.false.,error)
  call sic_def_inte ('PHYS_ANT',r_kant,1,dim,.false.,error)
  call sic_def_inte ('CORR_INPUT',r_kent,1,dim,.false.,error)
  dim(1) = mnbas
  call sic_def_inte ('START_ANTENNA',r_iant,1,dim,.false.,error)
  call sic_def_inte ('END_ANTENNA',r_jant,1,dim,.false.,error)
  dim(1) = 3
  dim(2) = mnbas
  call sic_def_dble ('BASELINE',r_bas,2,dim,.false.,error)
  dim(2) = mnant
  call sic_def_dble ('ANTENNA',r_ant,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real ('AXES_OFFSET',r_axes,1,dim,.false.,error)
  call sic_def_real ('PHLO1',r_phlo1,1,dim,.false.,error)
  call sic_def_real ('PHLO3',r_phlo3,1,dim,.false.,error)
  call sic_def_inte ('NBAND_WIDEX',r_nband_widex,0,dim,.false.,error)
  return
  !<FF>
entry rf_variables(off)
  !
  ! RF Setup
  if(off) then
    call sic_delvariable('LINE',.false.,error)
    call sic_delvariable('FREQUENCY',.false.,error)
    call sic_delvariable('ISB',.false.,error)
    call sic_delvariable('LOCK',.false.,error)
    call sic_delvariable('FLO1',.false.,error)
    call sic_delvariable('FLO1_REF',.false.,error)
    call sic_delvariable('FIF1',.false.,error)
    call sic_delvariable('VELOCITY',.false.,error)
    call sic_delvariable('TYPE_VEL',.false.,error)
    call sic_delvariable('DOPPLER',.false.,error)
    if (new_receivers) then
      call sic_delvariable('NPOL_REC',.false.,error)
      call sic_delvariable('QUARTER',.false.,error)
      call sic_delvariable('QWPLATE',.false.,error)
    endif
    call sic_delvariable('N_IF',.false.,error)
    call sic_delvariable('IFNAME',.false.,error)
    call sic_delvariable('KIF',.false.,error)
    call sic_delvariable('R_IFATTN',.false.,error)
    call sic_delvariable('R_NBB',.false.,error)
    call sic_delvariable('R_BBNAME',.false.,error)
    call sic_delvariable('R_KBB',.false.,error)
    call sic_delvariable('R_MAPBB',.false.,error)
    call sic_delvariable('R_MAPPOL',.false.,error)
    call sic_delvariable('R_POLSWITCH',.false.,error)
    return
  endif
  call sic_def_char ('LINE',r_line,.false.,error)
  call sic_def_dble ('FREQUENCY',r_restf,0,dim,.false.,error)
  call sic_def_inte ('ISB',r_isb,0,dim,.false.,error)
  call sic_def_inte ('LOCK',r_lock,0,dim,.false.,error)
  call sic_def_dble ('FLO1',r_flo1,0,dim,.false.,error)
  call sic_def_dble ('FLO1_REF',r_flo1_ref,0,dim,.false.,error)
  call sic_def_dble ('FIF1',r_fif1,0,dim,.false.,error)
  call sic_def_real ('VELOCITY',r_veloc,0,dim,.false.,error)
  call sic_def_inte ('TYPE_VEL',r_typev,0,dim,.false.,error)
  call sic_def_real ('DOPPLER',r_doppl,0,dim,.false.,error)
  if (new_receivers) then
    call sic_def_inte ('NPOL_REC',r_npol_rec,0,dim,.false.,error)
    dim(1) = 2
    call sic_def_inte ('QUARTER',r_quarter,1,dim,.false.,error)
    dim(1) = mnant
    call sic_def_inte ('QWPLATE',r_qwplate,1,dim,.false.,error)
  endif
  call sic_def_inte ('N_IF',r_nif,0,dim,.false.,error)
  dim(1) = mnif
  call sic_def_charn ('IFNAME',r_ifname,1,dim,.false.,error)
  dim(1) = mnif
  call sic_def_inte ('KIF',r_kif,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnif
  call sic_def_real('R_IFATTN',r_ifattn,2,dim,.false.,error)
  call sic_def_inte('R_NBB',r_nbb,0,dim,.false.,error)
  dim(1) = mnbb
  call sic_def_charn('R_BBNAME',r_bbname,1,dim,.false.,error)
  call sic_def_inte('R_KBB',r_kbb,1,dim,.false.,error)
  call sic_def_inte('R_MAPBB',r_mapbb,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnbb
  call sic_def_inte('R_MAPPOL',r_mappol,2,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnsb
  call sic_def_inte('R_POLSWITCH',r_polswitch,2,dim,.false.,error)
  return
  !<FF>
entry contin_variables(off)
  !
  ! Continuum Setup
  if(off) then
    call sic_delvariable('N_SIDEBANDS',.false.,error)
    call sic_delvariable('N_SUB_BANDS',.false.,error)
    call sic_delvariable('NCDAT',.false.,error)
    call sic_delvariable('CRCH',.false.,error)
    call sic_delvariable('CVOFF',.false.,error)
    call sic_delvariable('CVRES',.false.,error)
    call sic_delvariable('CRFOFF',.false.,error)
    call sic_delvariable('CRFRES',.false.,error)
    call sic_delvariable('CNAM_U',.false.,error)
    call sic_delvariable('CNAM_L',.false.,error)
    call sic_delvariable('CFCEN',.false.,error)
    call sic_delvariable('CFWID',.false.,error)
    return
  endif
  call sic_def_inte ('N_SIDEBANDS',r_nsb,0,dim,.false.,error)
  call sic_def_inte ('N_SUB_BANDS',r_nband,0,dim,.false.,error)
  call sic_def_inte ('NCDAT',r_ncdat,0,dim,.false.,error)
  dim(1) = 2
  call sic_def_real ('CRCH',r_crch,1,dim,.false.,error)
  call sic_def_real ('CVOFF',r_cvoff,1,dim,.false.,error)
  call sic_def_real ('CVRES',r_cvres,1,dim,.false.,error)
  call sic_def_dble ('CRFOFF',r_crfoff,1,dim,.false.,error)
  !      call sic_def_inte ('crfoff',r_crfoff,1,dim,.false.,error)
  call sic_def_real ('CRFRES',r_crfres,1,dim,.false.,error)
  call sic_def_char ('CNAM_U',r_cnam(1),.false.,error)
  call sic_def_char ('CNAM_L',r_cnam(2),.false.,error)
  dim(1) = mcch
  call sic_def_dble ('CFCEN',r_cfcen,1,dim,.false.,error)
  call sic_def_real ('CFWID',r_cfwid,1,dim,.false.,error)
  return
  !<FF>
entry line_variables(off)
  !
  ! Line Setup
  if(off) then
    call sic_delvariable('N_LINE_BANDS',.false.,error)
    call sic_delvariable('FOURIER',.false.,error)
    call sic_delvariable('TOTAL_CHANNELS',.false.,error)
    call sic_delvariable('N_CHANNELS',.false.,error)
    call sic_delvariable('FIRST_CHANNEL',.false.,error)
    call sic_delvariable('LFCEN',.false.,error)
    call sic_delvariable('LFRES',.false.,error)
    call sic_delvariable('LNAME',.false.,error)
    call sic_delvariable('REF_CHANNEL',.false.,error)
    call sic_delvariable('F_RESOLUTION',.false.,error)
    call sic_delvariable('F_OFFSET',.false.,error)
    call sic_delvariable('V_RESOLUTION',.false.,error)
    call sic_delvariable('V_OFFSET',.false.,error)
    call sic_delvariable('R_LILEVL',.false.,error)
    call sic_delvariable('R_LILEVU',.false.,error)
    call sic_delvariable('BAND4',.false.,error)
    call sic_delvariable('REF_NUM',.false.,error)
    call sic_delvariable('UNIT_NUMBER',.false.,error)
    call sic_delvariable('PHSELECT',.false.,error)
    call sic_delvariable('R_FLO2',.false.,error)
    call sic_delvariable('R_BAND2',.false.,error)
    call sic_delvariable('R_FLO2BIS',.false.,error)
    call sic_delvariable('R_BAND2BIS',.false.,error)
    call sic_delvariable('R_FLO3',.false.,error)
    call sic_delvariable('R_FLO4',.false.,error)
    call sic_delvariable('R_LPOLMODE',.false.,error)
    call sic_delvariable('R_LPOLENTRY',.false.,error)
    call sic_delvariable('R_BB',.false.,error)
    call sic_delvariable('R_IF',.false.,error)
    call sic_delvariable('R_SB',.false.,error)
    call sic_delvariable('R_STARTING_CHUNK',.false.,error)
    call sic_delvariable('R_NB_CHUNK',.false.,error)
    call sic_delvariable('R_CODE_STOKES',.false.,error)
    return
  endif
  call sic_def_inte ('N_LINE_BANDS',r_lband,0,dim,.false.,error)
  call sic_def_logi ('FOURIER',r_lfour,.false.,error)
  call sic_def_inte ('TOTAL_CHANNELS',r_lntch,0,dim,.false.,error)
  dim(1) = mrlband
  call sic_def_inte ('N_CHANNELS',r_lnch,1,dim,.false.,error)
  call sic_def_inte ('FIRST_CHANNEL',r_lich,1,dim,.false.,error)
  call sic_def_dble ('LFCEN',r_lfcen,1,dim,.false.,error)
  call sic_def_real ('LFRES',r_lfres,1,dim,.false.,error)
  call sic_def_inte ('BAND4',r_band4,1,dim,.false.,error)
  call sic_def_real ('REF_NUM',r_lcench,1,dim,.false.,error)
  call sic_def_inte ('UNIT_NUMBER',r_iunit,1,dim,.false.,error)
  dim(1) = 2
  dim(2) = mrlband
  call sic_def_charn('LNAME',r_lnam,2,dim,.false.,error)
  call sic_def_real ('REF_CHANNEL',r_lrch,2,dim,.false.,error)
  call sic_def_real ('F_RESOLUTION',r_lrfres,2,dim,.false.,error)
  call sic_def_dble ('F_OFFSET',r_lrfoff,2,dim,.false.,error)
  call sic_def_real ('V_RESOLUTION',r_lvres,2,dim,.false.,error)
  call sic_def_real ('V_OFFSET',r_lvoff,2,dim,.false.,error)
  dim(1) = mrlband
  dim(2) = mnant
  call sic_def_inte('R_LILEVU',r_lilevu,2,dim,.false.,error)
  call sic_def_inte('R_LILEVL',r_lilevl,2,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mrlband
  call sic_def_inte('PHSELECT',r_phselect,2,dim,.false.,error)
  dim(1) = mrlband
  call sic_def_dble('R_FLO2',r_flo2,1,dim,.false.,error)
  call sic_def_inte('R_BAND2',r_band2,1,dim,.false.,error)
  call sic_def_dble('R_FLO2BIS',r_flo2bis,1,dim,.false.,error)
  call sic_def_inte('R_BAND2BIS',r_band2bis,1,dim,.false.,error)
  call sic_def_dble('R_FLO3',r_flo3,1,dim,.false.,error)
  call sic_def_dble('R_FLO4',r_flo4,1,dim,.false.,error)
  call sic_def_inte('R_LPOLMODE',r_lpolmode,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mrlband
  call sic_def_inte('R_LPOLENTRY',r_lpolentry,2,dim,.false.,error)
  dim(1) = mrlband
  call sic_def_inte('R_BB',r_bb,1,dim,.false.,error)
  call sic_def_inte('R_IF',r_if,1,dim,.false.,error)
  call sic_def_inte('R_SB',r_sb,1,dim,.false.,error)
  call sic_def_inte('R_STARTING_CHUNK',r_stachu,1,dim,.false.,error)
  call sic_def_inte('R_NB_CHUNK',r_nchu,1,dim,.false.,error)
  dim(1) = mnbas
  dim(2) = mrlband
  call sic_def_inte('R_CODE_STOKES',r_code_stokes,2,dim,.false.,error)
  !<FF>
entry scanning_variables(off)
  !
  ! Scanning
  if(off) then
    call sic_delvariable('SCAN_TYPE',.false.,error)
    call sic_delvariable('MOBIL',.false.,error)
    call sic_delvariable('COLL_AZ',.false.,error)
    call sic_delvariable('COLL_EL',.false.,error)
    call sic_delvariable('COR_FOC',.false.,error)
    return
  endif
  call sic_def_inte ('SCAN_TYPE',r_scaty,0,dim,.false.,error)
  ! Strictly speaking, this is a logical array...
  dim(1) = mnant
  call sic_def_inte_addr ('MOBIL',r_mobil,1,dim,.false.,error)
  call sic_def_real ('COLL_AZ',r_collaz,1,dim,.false.,error)
  call sic_def_real ('COLL_EL',r_collel,1,dim,.false.,error)
  call sic_def_real ('COR_FOC',r_corfoc,1,dim,.false.,error)
  call sic_def_inte ('NSWITCH',r_nswitch,1,dim,.false.,error)
  dim(1) = mswitch
  call sic_def_real ('WSWITCH',r_wswitch,1,dim,.false.,error)
  dim(1) = 2
  dim(2) = mswitch
  call sic_def_real ('PSWITCH',r_pswitch,2,dim,.false.,error)
  return
  !<FF>
entry atmos_variables(off)
  !
  ! Atmospheric parameters
  if(off) then
    call sic_delvariable('PRESSURE',.false.,error)
    call sic_delvariable('AMBIANT_T',.false.,error)
    call sic_delvariable('ALTITUDE',.false.,error)
    call sic_delvariable('HUMIDITY',.false.,error)
    call sic_delvariable('WATER',.false.,error)
    call sic_delvariable('TAU_S',.false.,error)
    call sic_delvariable('TAU_I',.false.,error)
    call sic_delvariable('TATM_S',.false.,error)
    call sic_delvariable('TATM_I',.false.,error)
    call sic_delvariable('BEAM_EFF',.false.,error)
    call sic_delvariable('FORWARD_EFF',.false.,error)
    call sic_delvariable('T_CHOPPER',.false.,error)
    call sic_delvariable('T_COLD',.false.,error)
    call sic_delvariable('T_REC',.false.,error)
    call sic_delvariable('GAIN_IMAGE',.false.,error)
    call sic_delvariable('CHOPPER_COUNTS',.false.,error)
    call sic_delvariable('SKY_COUNTS',.false.,error)
    call sic_delvariable('COLD_COUNTS',.false.,error)
    call sic_delvariable('T_SYS_S',.false.,error)
    call sic_delvariable('T_SYS_I',.false.,error)
    call sic_delvariable('CHOPPER_EFF',.false.,error)
    call sic_delvariable('CAL_MODE',.false.,error)
    call sic_delvariable('JY_TO_KEL',.false.,error)
    call sic_delvariable('TOTAL_SCALE',.false.,error)
    call sic_delvariable('T_CABIN',.false.,error)
    call sic_delvariable('T_DEWAR',.false.,error)
    return
  endif
  call sic_def_real ('PRESSURE',r_pamb,0,dim,.false.,error)
  call sic_def_real ('AMBIANT_T',r_tamb,0,dim,.false.,error)
  call sic_def_real ('ALTITUDE',r_alti,0,dim,.false.,error)
  call sic_def_real ('HUMIDITY',r_humid,0,dim,.false.,error)
  dim(1) = mnbb 
  dim(2) = mnant
  call sic_def_real ('WATER',r_h2omm,2,dim,.false.,error)
  call sic_def_real ('TAU_S',r_taus,2,dim,.false.,error)
  call sic_def_real ('TAU_I',r_taui,2,dim,.false.,error)
  call sic_def_real ('TATM_S',r_tatms,2,dim,.false.,error)
  call sic_def_real ('TATM_I',r_tatmi,2,dim,.false.,error)
  call sic_def_real ('BEAM_EFF',r_beef,2,dim,.false.,error)
  call sic_def_real ('FORWARD_EFF',r_feff,2,dim,.false.,error)
  call sic_def_real ('T_CHOPPER',r_tchop,2,dim,.false.,error)
  call sic_def_real ('T_COLD',r_tcold,2,dim,.false.,error)
  call sic_def_real ('T_REC',r_trec,2,dim,.false.,error)
  call sic_def_real ('GAIN_IMAGE',r_gim,2,dim,.false.,error)
  call sic_def_real ('CHOPPER_COUNTS',r_cchop,2,dim,.false.,error)
  call sic_def_real ('SKY_COUNTS',r_csky,2,dim,.false.,error)
  call sic_def_real ('COLD_COUNTS',r_ccold,2,dim,.false.,error)
  call sic_def_real ('T_SYS_S',r_tsyss,2,dim,.false.,error)
  call sic_def_real ('T_SYS_I',r_tsysi,2,dim,.false.,error)
  call sic_def_real ('CHOPPER_EFF',r_ceff,2,dim,.false.,error)
  call sic_def_inte ('CAL_MODE',r_cmode,2,dim,.false.,error)
  call sic_def_real ('JY_TO_KEL',r_jykel,2,dim,.false.,error)
  call sic_def_real ('TOTAL_SCALE',r_totscale,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real ('T_CABIN',r_tcabin,1,dim,.false.,error)
  dim(1) = 3
  dim(2) = mnant
  call sic_def_real ('T_DEWAR',r_tdewar,2,dim,.false.,error)
  
  return
  !<FF>
entry monitor_variables(off)
  !
  ! Atmospheric monitor parameters
  if (off) then
    call sic_delvariable('NREC_MON',.false.,error)
    call sic_delvariable('FRS_MON',.false.,error)
    call sic_delvariable('FRI_MON',.false.,error)
    call sic_delvariable('WATER_MON',.false.,error)
    call sic_delvariable('TAU_S_MON',.false.,error)
    call sic_delvariable('TAU_I_MON',.false.,error)
    call sic_delvariable('TATM_S_MON',.false.,error)
    call sic_delvariable('TATM_I_MON',.false.,error)
    call sic_delvariable('FORWARD_EFF_MON',.false.,error)
    call sic_delvariable('T_CHOPPER_MON',.false.,error)
    call sic_delvariable('T_COLD_MON',.false.,error)
    call sic_delvariable('T_REC_MON',.false.,error)
    call sic_delvariable('GAIN_IMAGE_MON',.false.,error)
    call sic_delvariable('CHOP_COUNTS_MON',.false.,error)
    call sic_delvariable('SKY_COUNTS_MON',.false.,error)
    call sic_delvariable('COLD_COUNTS_MON',.false.,error)
    call sic_delvariable('T_SYS_MON',.false.,error)
    call sic_delvariable('CAL_MODE_MON',.false.,error)
    call sic_delvariable('PATH_MON',.false.,error)
    call sic_delvariable('TEM_MON',.false.,error)
    call sic_delvariable('DPATH_MON',.false.,error)
    call sic_delvariable('MAGIC_MON',.false.,error)
    call sic_delvariable('OK_MON',.false.,error)
    return
  endif
  call sic_def_inte ('NREC_MON',r_nrec_mon,0,dim,.false.,error)
  call sic_def_real ('FRS_MON',r_frs_mon,0,dim,.false.,error)
  call sic_def_real ('FRI_MON',r_fri_mon,0,dim,.false.,error)
  call sic_def_real ('MAGIC_MON',r_magic_mon,1,2,.false.,error)
  dim(1) = mnant
  call sic_def_real ('WATER_MON',r_h2o_mon,1,dim,.false.,error)
  call sic_def_real ('TAU_S_MON',r_taus_mon,1,dim,.false.,error)
  call sic_def_real ('TAU_I_MON',r_taui_mon,1,dim,.false.,error)
  call sic_def_real ('TATM_S_MON',r_tatms_mon,1,dim,.false.,error)
  call sic_def_real ('TATM_I_MON',r_tatmi_mon,1,dim,.false.,error)
  call sic_def_real   &
    ('FORWARD_EFF_MON',r_feff_mon,1,dim,.false.,error)
  call sic_def_real   &
    ('T_CHOPPER_MON',r_tchop_mon,1,dim,.false.,error)
  call sic_def_real ('T_COLD_MON',r_tcold_mon,1,dim,.false.,error)
  call sic_def_real ('T_REC_MON',r_trec_mon,1,dim,.false.,error)
  call sic_def_real ('GAIN_IMAGE_MON',r_gim_mon,1,dim,.false.,error)
  call sic_def_real   &
    ('CHOP_COUNTS_MON',r_cchop_mon,1,dim,.false.,error)
  call sic_def_real   &
    ('SKY_COUNTS_MON',r_csky_mon,1,dim,.false.,error)
  call sic_def_real   &
    ('COLD_COUNTS_MON',r_ccold_mon,1,dim,.false.,error)
  call sic_def_real ('T_SYS_MON',r_tsys_mon,1,dim,.false.,error)
  call sic_def_inte ('CAL_MODE_MON',r_cmode_mon,1,dim,.false.,error)
  call sic_def_real ('PATH_MON',r_path_mon,1,dim,.false.,error)
  call sic_def_real ('TEM_MON',r_tem_mon,1,dim,.false.,error)
  call sic_def_real ('DPATH_MON',r_dpath_mon,1,dim,.false.,error)
  call sic_def_login ('OK_MON',r_ok_mon,1,dim,.false.,error)
  return
  !<FF>
entry wvr_variables(off)
  !
  ! Water Vapor Radiometer  parameters
  if (off) then
    call sic_delvariable('wvrnch',.false.,error)
    call sic_delvariable('wvrfreq',.false.,error)
    call sic_delvariable('wvrbw',.false.,error)
    call sic_delvariable('wvrtamb',.false.,error)
    call sic_delvariable('wvrtpel',.false.,error)
    call sic_delvariable('wvrtcal',.false.,error)
    call sic_delvariable('wvrref',.false.,error)
    call sic_delvariable('wvraver',.false.,error)
    call sic_delvariable('wvramb',.false.,error)
    call sic_delvariable('wvrtrec',.false.,error)
    call sic_delvariable('wvrlabtrec',.false.,error)
    call sic_delvariable('wvrlabtcal',.false.,error)
    call sic_delvariable('wvrlabtdio',.false.,error)
    call sic_delvariable('wvrfeff',.false.,error)
    call sic_delvariable('wvrmode',.false.,error)
    call sic_delvariable('wvrh2o',.false.,error)
    call sic_delvariable('wvrpath',.false.,error)
    call sic_delvariable('wvrtsys',.false.,error)
    call sic_delvariable('wvrdpath',.false.,error)
    call sic_delvariable('wvrfpath',.false.,error)
    call sic_delvariable('wvrliq',.false.,error)
    call sic_delvariable('wvrdcloud',.false.,error)
    call sic_delvariable('wvrtatm',.false.,error)
    call sic_delvariable('wvrqual',.false.,error)
    call sic_delvariable('wvravertime',.false.,error)
    return
  endif
  !
  !data input
  dim(1) = mnant
  call sic_def_inte ('wvrnch',r_wvrnch,1,dim,.false.,error)
  dim(1) = mwvrch
  dim(2) = mnant
  call sic_def_real ('wvrfreq',r_wvrfreq,2,dim,.false.,error)
  call sic_def_real ('wvrbw',r_wvrbw,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real ('wvrtamb',r_wvrtamb,1,dim,.false.,error)
  call sic_def_real ('wvrtpel',r_wvrtpel,1,dim,.false.,error)
  dim(1) = mwvrch
  dim(2) = mnant
  call sic_def_real ('wvrtcal',r_wvrtcal,2,dim,.false.,error)
  call sic_def_real ('wvrref',r_wvrref,2,dim,.false.,error)
  call sic_def_real ('wvraver',r_wvraver,2,dim,.false.,error)
  call sic_def_real ('wvramb',r_wvramb,2,dim,.false.,error)
  call sic_def_real ('wvrtrec',r_wvrtrec,2,dim,.false.,error)
  call sic_def_real ('wvrfeff',r_wvrfeff,2,dim,.false.,error)
  call sic_def_real ('wvrlabtrec',r_wvrlabtrec,2,dim,.false.,error)
  call sic_def_real ('wvrlabtcal',r_wvrlabtcal,2,dim,.false.,error)
  call sic_def_real ('wvrlabtdio',r_wvrlabtdio,2,dim,.false.,error)
  !
  !model output
  dim(1) = mnant
  call sic_def_inte ('wvrmode',r_wvrmode,1,dim,.false.,error)
  call sic_def_real ('wvrh2o',r_wvrh2o,1,dim,.false.,error)
  call sic_def_real ('wvrpath',r_wvrpath,1,dim,.false.,error)
  dim(1) = mwvrch
  dim(2) = mnant
  call sic_def_real ('wvrtsys',r_wvrtsys,2,dim,.false.,error)
  call sic_def_real ('wvrdpath',r_wvrdpath,2,dim,.false.,error)
  call sic_def_real ('wvrfpath',r_wvrfpath,2,dim,.false.,error)
  call sic_def_real ('wvrliq',r_wvrliq,2,dim,.false.,error)
  call sic_def_real ('wvrdcloud',r_wvrdcloud,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real ('wvrtatm',r_wvrtatm,1,dim,.false.,error)
  call sic_def_inte ('wvrqual',r_wvrqual,1,dim,.false.,error)
  !
  call sic_def_inte ('wvravertime',r_wvravertime,0,dim,.false.,error)
  return
  !<FF>
entry alma_variables(off)
  !
  ! Single Dish Holography (ALMA) variables
  if (off) then
    call sic_delvariable('TRANS_DIST',.false.,error)
    call sic_delvariable('TRANS_FOCUS',.false.,error)
    call sic_delvariable('EXECBLOCKNUM',.false.,error)
    call sic_delvariable('SUBSCAN',.false.,error)
    call sic_delvariable('EXECBLOCK',.false.,error)
    call sic_delvariable('ANTENNA_NAME',.false.,error)
    call sic_delvariable('ANTENNA_TYPE',.false.,error)
    call sic_delvariable('DISH_DIAMETER',.false.,error)
    return
  endif
  !
  !data input
  call sic_def_real ('TRANS_DIST',r_trandist,0,dim,.false.,error)
  call sic_def_real ('TRANS_FOCUS',r_tranfocu,0,dim,.false.,error)
  call sic_def_inte ('EXECBLOCKNUM',r_execblocknum,0,dim,.false.   &
    ,error)
  call sic_def_inte ('SUBSCAN',r_execblocknum,0,dim,.false.,error)
  call sic_def_char ('EXECBLOCK',r_execblock,.false.,error)
  dim(1) = mnant
  call sic_def_charn ('ANTENNA_NAME',r_antennaname,1,dim,.false.   &
    ,error)
  call sic_def_charn ('ANTENNA_TYPE',r_antennatype,1,dim,.false.   &
    ,error)
  call sic_def_real ('DISH_DIAMETER',r_dishdiameter,1,dim,.false.   &
    ,error)
  return
  !<FF>
entry descr_variables(off)
  !
  ! Data Section Descriptor
  if(off) then
    call sic_delvariable('N_DUMPS',.false.,error)
    call sic_delvariable('HEADER_LENGTH',.false.,error)
    call sic_delvariable('CONT_LENGTH',.false.,error)
    call sic_delvariable('LINE_LENGTH',.false.,error)
    return
  endif
  call sic_def_inte('N_DUMPS',r_ndump,0,dim,.false.,error)
  call sic_def_long('HEADER_LENGTH',r_ldpar,0,dim,.false.,error)
  call sic_def_long('CONT_LENGTH',r_ldatc,0,dim,.false.,error)
  call sic_def_long('LINE_LENGTH',r_ldatl,0,dim,.false.,error)
  return
  !<FF>
entry aic_variables(off)
  !
  ! Antenna Instrumental Calibration Section
  if(off) then
    call sic_delvariable('AIC',.false.,error)
    call sic_delvariable('AICDEG',.false.,error)
    call sic_delvariable('AICPHA',.false.,error)
    call sic_delvariable('AICAMP',.false.,error)
    call sic_delvariable('DX',.false.,error)
    call sic_delvariable('DY',.false.,error)
    return
  endif
  call sic_def_inte('AIC',r_aic,0,dim,.false.,error)
  call sic_def_inte('AICDEG',r_aicdeg,0,dim,.false.,error)
  dim(1) = m_pol_rec
  dim(2) = 2
  dim(3) = mnant
  dim(4) = micdeg
  call sic_def_real('AICPHA',r_aicpha,4,dim,.false.,error)
  call sic_def_real('AICAMP',r_aicamp,4,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnbb
  call sic_def_cplx('DX',r_dx,2,dim,.false.,error)
  call sic_def_cplx('DY',r_dy,2,dim,.false.,error)
  return
  !<FF>
entry datah_variables(off)
  !
  ! Data header
  if(off) then
    call sic_delvariable('DH_DUMP',.false.,error)
    call sic_delvariable('DH_OBS',.false.,error)
    call sic_delvariable('DH_DATE',.false.,error)
    call sic_delvariable('DH_TIME',.false.,error)
    call sic_delvariable('DH_INTEG',.false.,error)
    call sic_delvariable('DH_SVEC',.false.,error)
    call sic_delvariable('DH_AFLAG',.false.,error)
    call sic_delvariable('DH_TOTAL',.false.,error)
    call sic_delvariable('DH_TEST0',.false.,error)
    call sic_delvariable('DH_TEST1',.false.,error)
    call sic_delvariable('DH_DELCON',.false.,error)
    call sic_delvariable('DH_DELLIN',.false.,error)
    call sic_delvariable('DH_DELAYC',.false.,error)
    call sic_delvariable('DH_DELAY',.false.,error)
    call sic_delvariable('DH_PHASEC',.false.,error)
    call sic_delvariable('DH_PHASE',.false.,error)
    call sic_delvariable('DH_RATEC',.false.,error)
    call sic_delvariable('DH_CABLE',.false.,error)
    call sic_delvariable('DH_GAMME',.false.,error)
    call sic_delvariable('DH_OFFFOC',.false.,error)
    call sic_delvariable('DH_OFFLAM',.false.,error)
    call sic_delvariable('DH_OFFBET',.false.,error)
    call sic_delvariable('DH_UVM',.false.,error)
    call sic_delvariable('DH_RMSPHA',.false.,error)
    call sic_delvariable('DH_RMSAMP',.false.,error)
    call sic_delvariable('DH_RMSPE',.false.,error)
    call sic_delvariable('DH_ATFAC',.false.,error)
    call sic_delvariable('DH_UTC',.false.,error)
    call sic_delvariable('DH_BFLAG',.false.,error)
    call sic_delvariable('DH_INFAC',.false.,error)
    call sic_delvariable('AMPFAC_L',.false.,error)
    call sic_delvariable('AMPFAC_U',.false.,error)
    call sic_delvariable('DH_WVR',.false.,error)
    call sic_delvariable('DH_WVRSTAT',.false.,error)
    call sic_delvariable('DH_ACTP',.false.,error)
    call sic_delvariable('DH_ANTTP',.false.,error)
    call sic_delvariable('DH_SAFLAG',.false.,error)
    call sic_delvariable('DH_SBFLAG',.false.,error)
    call sic_delvariable('DH_CABLE_ALTERNATE',.false.,error)
    call sic_delvariable('DH_NTIME',.false.,error)
    call sic_delvariable('DH_TIME_MONITORING',.false.,error)
    call sic_delvariable('DH_INCLI',.false.,error)
    call sic_delvariable('DH_INCLI_TEMP',.false.,error)
    call sic_delvariable('DH_TILFOC',.false.,error)
    call sic_delvariable('DH_OFFFOC_XY',.false.,error)
    call sic_delvariable('DH_VLBI_PHASEFF',.false.,error)
    call sic_delvariable('DH_VLBI_PHASE_INC',.false.,error)
    call sic_delvariable('DH_VLBI_PHASE_TOT',.false.,error)
    call sic_delvariable('IS_SAFLAG',.false.,error)
    call sic_delvariable('IS_SBFLAG',.false.,error)
    return
  endif
  call sic_def_inte ('DH_DUMP',dh_dump,0,dim,.false.,error)
  call sic_def_dble('DH_UTC',dh_utc,0,dim,.false.,error)
  call sic_def_char ('DH_TIME',dh_time,.false.,error)
  call sic_def_char ('DH_DATE',dh_date,.false.,error)
  call sic_def_inte('DH_OBS',dh_obs,0,dim,.false.,error)
  call sic_def_real('DH_INTEG',dh_integ,0,dim,.false.,error)
  dim(1) = 3
  call sic_def_dble('DH_SVEC',dh_svec,1,dim,.false.,error)
  dim(1) = 20
  call sic_def_real('DH_TEST0',dh_test0,1,dim,.false.,error)
  dim(1) = mnant
  call sic_def_inte('DH_AFLAG',dh_aflag,1,dim,.false.,error)
  dim(1) = mnbb
  dim(2) = mnant
  call sic_def_real('DH_TOTAL',dh_total,2,dim,.false.,error)
  !c      DIM(1) = M_POL_REC RL 2006-11-02
  dim(1) = mrlband
  dim(2) = 2
  dim(3) = mnant
  call sic_def_real('DH_ATFAC',dh_atfac,3,dim,.false.,error)
  dim(1) = 2
  dim(2) = mnant
  call sic_def_real('DH_RMSPE',dh_rmspe,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real('DH_DELCON',dh_delcon,1,dim,.false.,error)
  call sic_def_real('DH_DELAYC',dh_delayc,1,dim,.false.,error)
  dim(1) = mnbb
  dim(2) = mnant
  call sic_def_real('DH_DELLIN',dh_dellin,2,dim,.false.,error)
  call sic_def_real('DH_DELAY',dh_delay,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real('DH_PHASEC',dh_phasec,1,dim,.false.,error)
  call sic_def_real('DH_PHASE',dh_phase,1,dim,.false.,error)
  call sic_def_real('DH_CABLE',dh_cable,1,dim,.false.,error)
  call sic_def_inte('DH_GAMME',dh_gamme,1,dim,.false.,error)
  call sic_def_real('DH_RATEC',dh_ratec,1,dim,.false.,error)
  call sic_def_real('DH_OFFFOC',dh_offfoc,1,dim,.false.,error)
  call sic_def_real('DH_OFFLAM',dh_offlam,1,dim,.false.,error)
  call sic_def_real('DH_OFFBET',dh_offbet,1,dim,.false.,error)
  dim(1) = 2
  dim(2) = mnbas
  call sic_def_real('DH_UVM',dh_uvm,2,dim,.false.,error)
  dim(1) = m_pol_rec
  dim(2) = 2
  dim(3) = mnbas
  call sic_def_real('DH_RMSPHA',dh_rmspha,2,dim,.false.,error)
  call sic_def_real('DH_RMSAMP',dh_rmsamp,2,dim,.false.,error)
  dim(1) = 20
  dim(2) = mnant
  call sic_def_real('DH_TEST1',dh_test1,2,dim,.false.,error)
  dim(1) = mnbas
  call sic_def_inte('DH_BFLAG',dh_bflag,1,dim,.false.,error)
  dim(1) = 2                   ! real, imag
  dim(2) = 2                   ! upper, lower
  dim(3) = mnbas
  call sic_def_real_addr('DH_INFAC',dh_infac,3,dim,.false.,error)
  call sic_def_real('AMPFAC_L',ampfacl,1,dim,.false.,error)
  call sic_def_real('AMPFAC_U',ampfacu,1,dim,.false.,error)
  dim(1) = mwvrch
  dim(2) = mnant
  call sic_def_real ('DH_WVR',dh_wvr,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_inte ('DH_WVRSTAT',dh_wvrstat,1,dim,.false.,error)
  dim(1) = 2
  dim(2) = mnant
  dim(3) = mrlband
  call sic_def_real('DH_ACTP',dh_actp,3,dim,.false.,error)
  dim(1) = mnif
  dim(2) = mnant
  call sic_def_real('DH_ANTTP',dh_anttp,2,dim,.false.,error)
  dim(1) = 2*mbands
  dim(2) = mnant
  call sic_def_login('DH_SAFLAG',dh_saflag,2,dim,.false.,error)
  dim(1) = 2*mbands
  dim(2) = mnbas
  call sic_def_login('DH_SBFLAG',dh_sbflag,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_login('IS_SAFLAG',is_saflag,1,dim,.false.,error)
  dim(1) = mnbas
  call sic_def_login('IS_SBFLAG',is_sbflag,1,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real('DH_CABLE_ALTERNATE',dh_cable_alternate,1,dim,.false.,error)
  call sic_def_inte('DH_NTIME',dh_ntime,0,dim,.false.,error)
  dim(1) = dh_ntime
  call sic_def_real('DH_TIME_MONITORING',dh_time_monitoring,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = 2
  call sic_def_real('DH_INCLI',dh_incli,2,dim,.false.,error)
  dim(1) = mnant
  call sic_def_real('DH_INCLI_TEMP',dh_incli_temp,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = 3
  call sic_def_real('DH_TILFOC',dh_tilfoc,2,dim,.false.,error)
  dim(1) = mnant
  dim(2) = 2
  call sic_def_real('DH_OFFFOC_XY',dh_offfoc_xy,2,dim,.false.,error)
  dim(1) = mnbb
  call sic_def_real('DH_VLBI_PHASEFF',dh_vlbi_phaseff,1,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnbb
  call sic_def_real('DH_VLBI_PHASE_INC',dh_vlbi_phase_inc,2,dim,.false.,error)
  call sic_def_real('DH_VLBI_PHASE_TOT',dh_vlbi_phase_tot,2,dim,.false.,error)
  return
  !<FF>
entry data_variables(off)
  !
  ! Data
  if (off) then
    call sic_delvariable('DATA_C',.false.,error)
    call sic_delvariable('DATA_L',.false.,error)
    call sic_delvariable('C_AMPMAX_U',.false.,error)
    call sic_delvariable('C_AMPMAX_L',.false.,error)
    call sic_delvariable('L_AMPMAX_U',.false.,error)
    call sic_delvariable('L_AMPMAX_L',.false.,error)
    call sic_delvariable('C_AMPMIN_U',.false.,error)
    call sic_delvariable('C_AMPMIN_L',.false.,error)
    call sic_delvariable('L_AMPMIN_U',.false.,error)
    call sic_delvariable('L_AMPMIN_L',.false.,error)
    return
  endif
  dim(1) = 2                   ! real, imag
  dim(2) = max(r_nband,1)
  dim(3) = max(r_nsb,1)        ! sideband
  dim(4) = max(r_nbas,1)
  call sic_def_real_addr('DATA_C',datac,4,dim,.false.,error)
  dim(1) = 2                   ! real, imag
  dim(2) = max(r_lntch,1)
  dim(3) = max(r_lnsb,1)       ! sideband
  dim(4) = max(r_nbas,1)
  call sic_def_real_addr('DATA_L',datal,4,dim,.false.,error)
  dim(1) = mnbas
  call sic_def_real('C_AMPMAX_U',campmaxu,1,dim,.false.,error)
  call sic_def_real('C_AMPMAX_L',campmaxl,1,dim,.false.,error)
  call sic_def_real('L_AMPMAX_U',lampmaxu,1,dim,.false.,error)
  call sic_def_real('L_AMPMAX_L',lampmaxl,1,dim,.false.,error)
  call sic_def_real('C_AMPMIN_U',campminu,1,dim,.false.,error)
  call sic_def_real('C_AMPMIN_L',campminl,1,dim,.false.,error)
  call sic_def_real('L_AMPMIN_U',lampminu,1,dim,.false.,error)
  call sic_def_real('L_AMPMIN_L',lampminl,1,dim,.false.,error)
  return
  !<FF>
entry size_variables(off)
  ! CLIC parameters
  if (off) then
    call sic_delvariable('MAXANT',.false.,error)
    call sic_delvariable('MAXBAS',.false.,error)
    call sic_delvariable('NSFLAG',.false.,error)
    call sic_delvariable('MDATAL',.false.,error)
    return
  endif
  call sic_def_inte('MAXANT',mnant,0,dim,.false.,error)
  call sic_def_inte('MAXBAS',mnbas,0,dim,.false.,error)
  call sic_def_inte('NSFLAG',mbands,0,dim,.false.,error)
  call sic_def_inte('MDATAL',mdatal,0,dim,.false.,error)
  return
  !

entry instrument_variables(off)
  ! instrument monitoring
  if (off) then
    call sic_delvariable('HIQ_ATTEN',.false.,error)
    call sic_delvariable('LASER_LEVEL',.false.,error)
    call sic_delvariable('WIDEX_TEMPERATURE',.false.,error)
    return
  endif
  dim(1) = mnant
  dim(2) = 2
  call sic_def_real('HIQ_ATTEN',r_attenuation_hiq,2,dim,.false.,error)
  dim(1) = mnant
  dim(2) = mnif 
  call sic_def_real('LASER_LEVEL',r_laser_level,2,dim,.false.,error)
  dim(1) = 4
  call sic_def_real('WIDEX_TEMPERATURE',r_widex_temperature,1,dim,.false.,error)
   return

entry spw_variables(off)
  ! spw mapping (for user)
  if (off) then
    call sic_delvariable(struct,.false.,error)
    return
  endif
  call sic_defstructure(struct,.true.,error)
  call sic_def_inte(struct//'%NSPW', r_lband,0,dim,.false.,error)
  dim(1) = r_lband
  call sic_def_charn (struct//'%NAME',spwname,1,dim,.false.,error)
  call sic_def_dble(struct//'%TOPO',spwtopo,1,dim,.false.,error)
  call sic_def_dble(struct//'%FREQ',spwfreq,1,dim,.false.,error)
  call sic_def_real(struct//'%BW',spwbw,1,dim,.false.,error)
  call sic_def_inte(struct//'%POL',spwpol,1,dim,.false.,error)
  call sic_def_inte(struct//'%SB',spwsb,1,dim,.false.,error)
  return

entry status_variables(off)
  ! status monitoring
  if (off) then
    call sic_delvariable('STATUS',.false.,error)
    call sic_delvariable('SUBSTATUS',.false.,error)
    return
  endif
  call sic_def_charn ('STATUS',r_status,0,dim,.false.,error)
  call sic_def_charn ('SUBSTATUS',r_substatus,0,dim,.false.,error)
  return

entry vlbi_variables(off)
  ! vlbi 
  if (off) then
    call sic_delvariable('R_REFANT',.false.,error)
    call sic_delvariable('R_COMPANT',.false.,error)
    call sic_delvariable('R_NANT_VLBI',.false.,error)
    call sic_delvariable('R_ANTMASK',.false.,error)
    call sic_delvariable('R_MASER_GPS_DIFF',.false.,error)
    call sic_delvariable('R_EFFTSYS',.false.,error)
    return
  endif
  call sic_def_inte('R_REFANT',r_refant,0,dim,.false.,error)
  call sic_def_inte('R_COMPANT',r_comparison_ant,0,dim,.false.,error)
  call sic_def_inte('R_NANT_VLBI',r_nant_vlbi,0,dim,.false.,error)
  dim(1) = mnant
  call sic_def_inte('R_ANTMASK',r_antmask,1,dim,.false.,error)
  call sic_def_real('R_MASER_GPS_DIFF',r_maser_gps_diff,0,dim,.false.,error)
  dim(1) = mnbb
  call sic_def_real('R_EFFTSYS',r_efftsys,1,dim,.false.,error)
  return

entry modifier_variables(off)
  ! dm
  if (off) then
    call sic_delvariable('R_DMLPHA',.false.,error)
    call sic_delvariable('R_DMLDPH',.false.,error)
    call sic_delvariable('r_DMLAMP',.false.,error)
    return
  endif
  dim(1) = 2
  dim(2) = mnbas
  dim(3) = mrlband
  call sic_def_real('R_DMLPHA',r_dmlpha,3,dim,.false., error)
  call sic_def_real('R_DMLDPH',r_dmldph,3,dim,.false., error)
  call sic_def_real('R_DMLAMP',r_dmlamp,3,dim,.false., error)
  return
end subroutine general_variables
!
subroutine functions
  use gkernel_interfaces
  ! Global
  real(kind=8), external :: mth_bessj1,mth_bessj0
  real(kind=4), external :: s_bessj1,s_bessj0
  ! Local
  logical :: error
  ! Define user functions
  call sic_def_func('J_1',s_bessj1,mth_bessj1,1,error)
  if (error) call message(8,3,'FUNCTIONS','Error defining J_1')
  call sic_def_func('J_0',s_bessj0,mth_bessj0,1,error)
  if (error) call message(8,3,'FUNCTIONS','Error defining J_0')
  return
end subroutine functions
!
function s_bessj1(x)
  real :: s_bessj1                  !
  real :: x                         !
  ! Global
  real*8 :: mth_bessj1
  s_bessj1 = mth_bessj1(dble(x))
  return
end function s_bessj1
!
function s_bessj0(x)
  real :: s_bessj0                  !
  real :: x                         !
  ! Global
  real*8 :: mth_bessj0
  s_bessj0 = mth_bessj0(dble(x))
  return
end function s_bessj0
