subroutine get_fits_gain(unit,error)
  use gildas_def
  use classic_api  
  integer :: unit                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: comment
  integer :: status, itest, i, j,   nullval
  integer :: num_col, not_found
  parameter (not_found=202)
  logical :: anyf
  !---------------------------------------------------------------------------
  status = 0
  ! Get  header keywords
  call ftgkyj(unit,'SCAN-NUM',itest,comment,status)
  if (status .gt. 0) go to 99
  if (itest.ne.r_scan) then
    call message(8,4,'GET_FITS_GAIN','Wrong scan number')
    error = .true.
    return
  endif
  !
  ! Read keywords
  !
  ! Read the columns
  call ftgcno(unit,.false.,'ANTENNID',num_col,status)
  if (status.gt.0) goto 99
  call ftgcvj(unit,num_col,1,1,r_nant,nullval,   &
    r_kant,anyf,status)
  !
  call ftgcno(unit,.false.,'AMPLGAIN',num_col,status)
  if (status.gt.0) goto 99
  r_aicdeg = 2
  do i=1, r_nant
    do j=0, r_aicdeg
      call ftgcve(unit,num_col,i,1+2*j,2,nullval,   &
        r_aicamp(1,1,i,j),anyf,status)
    enddo
  enddo
  !
  call ftgcno(unit,.false.,'PHASGAIN',num_col,status)
  if (status.gt.0) goto 99
  r_aicdeg = 2
  do i=1, r_nant
    do j=0, r_aicdeg
      call ftgcve(unit,num_col,i,1+2*j,2,nullval,   &
        r_aicpha(1,1,i,j),anyf,status)
    enddo
  enddo
  r_aic = 1
  !
  return
  !
99 call printerror('GET_FITS_GAIN',status)
  call message(6,2,'GET_FITS_GAIN',   &
    'Last comment was: '//comment)
  status = 0
  error = .true.
  return
end subroutine get_fits_gain
!
