subroutine aver(ib,vt,wt,it,ut,rt,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Put average value of a visibility-like information into display
  !     buffers
  !     IB    INTEGER    Box number
  !     UT    INTEGER    Observation number
  !     IT    INTEGER    Identification number (source number)
  !     RT    INTEGER    Record number
  !     ZT    COMPLEX    Visibility
  !     XT    REAL       X value
  !     YT    REAL       Y value
  !     WT    REAL       Weight
  !     T     REAL       Integration time
  !     WPT   REAL       Positive weight
  !---------------------------------------------------------------------
  integer :: ib                     !
  real*4 :: vt(4,3)                 !
  real*4 :: wt(4,3)                 !
  integer :: it                     !
  integer :: ut                     !
  integer :: rt                     !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy,ipz,ipw,ipi,ipu
  integer(kind=address_length) :: ipr
  !------------------------------------------------------------------------
  ! Code:
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipz = gag_pointer(data_z,memory)
  ipw = gag_pointer(data_w,memory)
  ipi = gag_pointer(data_i,memory)
  ipu = gag_pointer(data_u,memory)
  ipr = gag_pointer(data_r,memory)
  call sub_aver(ib,vt,wt,it,ut,rt,   &
    m_data,m_boxes,memory(ipx),memory(ipy),memory(ipz),   &
    memory(ipw), memory(ipi), memory(ipu), memory(ipr), error)
  return
  error = .true.
  return
end subroutine aver
!
subroutine sub_aver(ib,vt,wt,it,ut,rt,   &
    md, mb, x_data,y_data,z_data,w_data,   &
    i_data,u_data,r_data,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !     Put average value of a visibility-like information into display
  !     buffers
  !     IB    INTEGER    Box number
  !     UT    INTEGER    Observation number
  !     IT    INTEGER    Identification number (source number)
  !     RT    INTEGER    Record number
  !     ZT    COMPLEX    Visibility
  !     XT    REAL       X value
  !     YT    REAL       Y value
  !     WT    REAL       Weight
  !     T     REAL       Integration time
  !     WPT   REAL       Positive weight
  !     MD      INTEGER        Number of points per box
  !     MB      INTEGER        Number of boxes
  !     X_DATA  REAL(MB,MD)    X coordinates
  !     Y_DATA  REAL(MB,MD)    Y values
  !     W_DATA  REAL(MB,MD)    Weight values (for error bars)
  !     I_DATA  REAL(MB,MD)    Identification number
  !     U_DATA  REAL(MB,MD)    Observation number of point
  !     Z_DATA  COMPLEX(MB,MD) Visibility of point used to compute X and Y
  !     A_DATA  REAL(MB,MD)    "Amplitude" of point
  !     ARG     C*(*)
  !     ERROR   L              Logical error flag
  !---------------------------------------------------------------------
  integer :: ib                     !
  real*4 :: vt(4,3)                 !
  real*4 :: wt(4,3)                 !
  integer :: it                     !
  integer :: ut                     !
  integer :: rt                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  complex :: z_data(md,mb)          !
  real :: w_data(md,mb)             !
  integer :: i_data(md,mb)          !
  integer :: u_data(md,mb)          !
  integer :: r_data(md,mb)          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: iv, im
  integer :: nd, ix, iy, lch
  real :: faz
  logical :: angle_data, corr_data
  character(len=80) :: ch
  complex :: zt
  !------------------------------------------------------------------------
  ! Code:
  ix = i_x(k_x(ib))
  iy = i_y(k_y(ib))
  nd = n_data(ib)+1
  if (nd.gt.md) then
    call message(8,4,'AVER','Too many data points')
    call message(8,1,'AVER',   &
      'Use command "SET DATA npoints nboxes"')
    error = .true.
    return
  endif
  do iv=1,4
    do im=1,3
      if (wt(iv,im).gt.0) vt(iv,im) = vt(iv,im)/wt(iv,im)
    enddo
  enddo
  !
  ! A bad point ?
  if (max(abs(vt(3,2)),abs(vt(4,2))).gt.1e20) then
    write (ch,*) vt(3,2), vt(4,2)
    lch = lenc(ch)
    call message(8,2,'AVER',   &
      'High amplitude data point '//ch(1:lch))
    wt(3,2) = 0
    wt(4,2) = 0
  endif
  !
  ! Good correlator data: Use tsys weighting (2), flagging included.
  if (corr_data(ix,iy) .and. wt(3,2).gt.0) then
    zt = cmplx(vt(3,2),vt(4,2))
    if (ix.le.xy_phase .or. iy.le.xy_phase) then
      z_data(nd,ib) = zt
    endif
    if (j_average.eq.1 .and. ix.eq.xy_ampli) then
      ! in case of vector averaging
      x_data(nd,ib) = abs(zt)
    elseif (ix.eq.xy_phase) then
      ! phase always vector
      x_data(nd,ib) = faz(zt)
    elseif (ix.eq.xy_real) then 
      x_data(nd,ib) = real(zt)
    elseif (ix.eq.xy_imag) then 
      x_data(nd,ib) = aimag(zt)
    else
      x_data(nd,ib) = vt(1,2)
    endif
    if (j_average.eq.1 .and. iy.eq.xy_ampli) then
      y_data(nd,ib) = abs(zt)
    elseif (iy.eq.xy_phase) then
      y_data(nd,ib) = faz(zt)
    elseif (iy.eq.xy_real) then 
      y_data(nd,ib) = real(zt)
    elseif (iy.eq.xy_imag) then 
      y_data(nd,ib) = aimag(zt)
    else
      y_data(nd,ib) = vt(2,2)
    endif
    !
    ! Error bars
    if (iy.eq.xy_phase) then
      if (do_bar_atm) then
        w_data(nd,ib) = 1./(1./wt(2,3)+1./wt(2,2)/abs(zt)**2)
      else
        w_data(nd,ib) = wt(2,2)*abs(zt)**2
      endif
    else
      w_data(nd,ib) = wt(2,2)
    endif
  ! Otherwise use time-weighting (1):
  else
    w_data(nd,ib) = wt(2,2)
    if (ix.gt.xy_imag .and. wt(1,1).gt.0) then
      x_data(nd,ib) = vt(1,1)
    endif
    if (iy.gt.xy_imag .and. wt(2,1).gt.0) then
      y_data(nd,ib) = vt(2,1)
    endif
  endif
  !
  i_data(nd,ib) = it
  u_data(nd,ib) = ut
  r_data(nd,ib) = rt
  n_data(ib) = nd
  if (ix.eq.xy_recor) then
    x_data(nd,ib) = nd
  elseif (iy.eq.xy_recor) then
    y_data(nd,ib) = nd
  endif
  if (angle_data(iy) .and. iy.ne.xy_lo_ra) then
    call cvphase(1,y_data(nd,ib),w_data(nd,ib),degrees,.false.)
  endif
  !
  ! Reset counters
  do iv=1,4
    do im=1,3
      wt(iv,im) = 0
      vt(iv,im) = 0
    enddo
  enddo
end subroutine sub_aver
!
subroutine resetvar(error)
  use gildas_def
  use gkernel_interfaces
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy,ipw
  !------------------------------------------------------------------------
  ! Code:
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  call sub_resetvar(m_data,m_boxes,   &
    memory(ipx),memory(ipy),memory(ipw),error)
  return
end subroutine resetvar
!
subroutine sub_resetvar(md,mb,x_data,y_data,w_data,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Redefine SIC X_DATA,Y_DATA and W_DATA variables when number of
  !     boxes/points as changed. Get their content from the X_DATA
  !     Y_DATA and W_DATA arrays.
  !
  !     MD   INTEGER    Number of points per box
  !     MB   INTEGER    Number of boxes
  !     X_DATA  REAL    X_DATA array
  !     Y_DATA  REAL    Y_DATA array
  !     W_DATA  REAL    W_DATA array
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=index_length) :: dim(4)
  integer :: lsic, i, nd, j
  integer(kind=address_length) :: addr, sicx, sicy, sicw
  !
  save addr, lsic, nd
  data lsic/0/
  !------------------------------------------------------------------------
  ! Code:
  !      type *,'SUB_RESETVAR, md, mb ',md,mb
  call sic_delvariable ('X_DATA',.false.,error)
  call sic_delvariable ('Y_DATA',.false.,error)
  call sic_delvariable ('W_DATA',.false.,error)
  call sic_delvariable ('N_DATA',.false.,error)
  call sic_delvariable ('N_BOXES',.false.,error)
  call sic_delvariable ('X_DATA_MIN',.false.,error)
  call sic_delvariable ('Y_DATA_MIN',.false.,error)
  call sic_delvariable ('X_DATA_MAX',.false.,error)
  call sic_delvariable ('Y_DATA_MAX',.false.,error)
  if (lsic.ne.0) then
    call free_vm(lsic,addr)
  endif
  nd = 0
  do i=1,n_boxes
    nd = max(nd,n_data(i))
  enddo
  do i=1,n_boxes
    if (n_data(i).lt.nd) then
      do j=n_data(i)+1, nd
        y_data(j,i) = blank4
      enddo
    endif
    do j=1, n_data(i)
      if (abs(y_data(j,i)).gt.1e10) then
        y_data(j,i) = blank4
        w_data(j,i) = 0
      elseif (abs(x_data(j,i)).gt.1e10) then
        x_data(j,i) = blank4
        w_data(j,i) = 0
      elseif (w_data(j,i).le.0) then
        y_data(j,i) = blank4
      endif
    !            if (i.le.4 .and. j.le.10) type *, j,Y_DATA(J,I),W_DATA(J,I)
    enddo
  enddo
  lsic = nd * n_boxes
  if (lsic.gt.0) then
    error = sic_getvm(3*lsic,addr).ne.1
    if (error) return
    sicx = gag_pointer(addr,memory)
    sicy = sicx+lsic
    sicw = sicy+lsic
    call copyvar(x_data,nd,m_data,n_boxes,memory(sicx))
    call copyvar(y_data,nd,m_data,n_boxes,memory(sicy))
    call copyvar(w_data,nd,m_data,n_boxes,memory(sicw))
    dim(1) = nd
    dim(2) = n_boxes
    call sic_def_real_addr('X_DATA',memory(sicx),2,dim,.false.,error)
    call sic_def_real_addr('Y_DATA',memory(sicy),2,dim,.false.,error)
    call sic_def_real_addr('W_DATA',memory(sicw),2,dim,.false.,error)
  endif
  dim(1) = n_boxes
  call sic_def_inte('N_DATA',n_data,1,dim,.false.,error)
  call sic_def_real('X_DATA_MIN',gb1_x,1,dim,.false.,error)
  call sic_def_real('Y_DATA_MIN',gb1_y,1,dim,.false.,error)
  call sic_def_real('X_DATA_MAX',gb2_x,1,dim,.false.,error)
  call sic_def_real('Y_DATA_MAX',gb2_y,1,dim,.false.,error)
  call sic_def_inte('N_BOXES',n_boxes,0,dim,.false.,error)
  return
end subroutine sub_resetvar
!
subroutine copyvar(in,nout1,nin1,n2,out)
  !---------------------------------------------------------------------
  ! CLIC
  !     Utility routine: copy a 2-d array in another of different
  !     dimensions
  !---------------------------------------------------------------------
  integer :: nin1                   !
  integer :: n2                     !
  real*4 :: in(nin1,n2)             !
  integer :: nout1                  !
  real*4 :: out(nout1,n2)           !
  ! Local
  integer :: j, n1
  !------------------------------------------------------------------------
  ! Code:
  n1 = min(nin1,nout1)
  do j=1,n2
    call r4tor4 (in(1,j),out(1,j),n1)
  enddo
end subroutine copyvar
!
subroutine zrecord (qsb, qband, qbt, qntch,   &
    datac, datal, passc, passl, ibase, iband,   &
    nch, ich, jw1, jw2,  z, fw, fwp,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !     Get a visibility like information from a record
  !
  !     QSB     INTEGER   Number of sidebands (2)
  !     QBAND   INTEGER   Number of temporal data
  !     QBT     INTEGER   Number of baseline+triangles
  !     QNTCH   INTEGER   Number of channels (in total per baseline)
  !     DATAC(QBAND,QSB,QBT)  COMPLEX  Temporal data
  !     DATAL(QNTCH,QSB,QBT)  COMPLEX  Spectral data
  !     PASSC(QBAND,QSB,QBT)  COMPLEX  PassBand of temporal data
  !     PASSL(QNTCH,QSB,QBT)  COMPLEX  PassBand of spectral data
  !     IBASE   INTEGER   Baseline number
  !     IBAND   INTEGER   Subband number
  !     ICH1    INTEGER   First subband
  !     ICH2    INTEGER   Last subband
  !     JW1,JW2 INTEGER   Lowest and Highest channel in window subband
  !     JW3     INTEGER   NUMBER of GIBSS Channels
  !     WP      REAL      Positive weight
  !     FW      REAL      (full) Total weight
  !     Z       COMPLEX   Complex output
  ! Call Tree
  !	...		...		ARECORD
  !	...		READ_DATA	BRECORD
  !---------------------------------------------------------------------
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbt                    !
  integer :: qntch                  !
  complex :: datac(qband,qsb,qbt)   !
  complex :: datal(qntch,qsb,qbt)   !
  complex :: passc(qband,qsb,qbt)   !
  complex :: passl(qntch,qsb,qbt)   !
  integer :: ibase                  !
  integer :: iband                  !
  integer :: nch                    !
  integer :: ich(nch)               !
  integer :: jw1                    !
  integer :: jw2                    !
  complex :: z                      !
  real*4 :: fw                      !
  real*4 :: fwp                     !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! include 'clic_sba.inc'
  ! Local
  ! REAL*4 TSYS(2), FFW(2), A, P, AFAC, FWM, UVR
  real*4 :: tsys_b, ffw(2), a, p, afac, fwm, uvr, mean_afac
  complex :: zz(2), zzz, av(2), zm
  integer :: i, il, j, n, ii, ii1, ii2, k, iw, is, isp, ibnd
  integer :: ibdata
  integer :: jw3
  logical :: down_channel, present_sb
  integer :: jok(mch)
  !------------------------------------------------------------------------
  ! Code:
  if (qsb.eq.1) then ! Check available sideband
    ibnd = 1
  else
    ibnd = 0
  endif
  !
  call get_cont_average (av,ibase)
  !
  ! Eliminate data out of UV Range:
  uvr = sqrt(dh_uvm(1,ibase)**2+dh_uvm(2,ibase)**2)
  if ((uvr.lt.uv_range(1)).or.(uvr.gt.uv_range(2))) return
  !
  z = 0
  a = 0
  p = 0
  n = 0
  fw = 0
  fwm = 0
  if (iband.le.2) then
    ii1 = iband
    ii2 = iband
  else
    ii1 = 1
    ii2 = 2
  endif
  do j=1, nch
    i = ich(j)
    il = i
    if (i.gt.mbands) il = i-mbands
    if (i.gt.mbands) then          ! GET number of GIBBS channels
      ! now use r_band4 for compatibility with new correlator (is 2 for DSB)
      if (r_band4(il).eq.2) then
        jw3 = ngibbs
      else
        jw3 = 0
      endif
    endif
    do ii = ii1, ii2           ! loop on sidebands
      ffw(ii) = 0
      zz(ii) = 0
      afac = 1
      mean_afac = 0
      present_sb =  .true.
      if (qsb.eq.1.and.r_lsband(1,il).ne.ii) present_sb = .false.
      if (.not.down_channel(ibase,i).and.present_sb) then
        ! Temporal data
        if (ibnd.ne.0) then
          ibdata = ibnd 
        else
          ibdata = ii
        endif
        if (i.le.mbands) then
          zz(ii) = datac(i,ibdata,ibase)
          !
          ffw(ii) = 2*r_cfwid(i)/tsys_b(i,ii,ibase)
          if (do_pass.or.do_spidx) then
            zz(ii) = zz(ii)*passc(i,ii,ibase)
          endif
          call scaling (il,ii,ibase,zz(ii),mean_afac,error)
          if (error) then
            call message(8,4,'ZRECORD',   &
              'Invalid calibration factor for '//cband(ii))
            error = .true.
            return
          endif
        else
          ! Spectral line data
          iw = 0
          zz(ii) = 0
          is = i-mbands
          call jlimits(r_lnch(is),jw1,jw2,jw3,jok)
          call jmask(r_lnch(is),nmask(is),cmask(:,:,is),jok)
          do isp = 1, r_lnch(is)   ! scan line window
            k = r_lich(is) + isp
            zzz = datal(k,ibdata,ibase)
            if (zzz.eq.0) jok(isp) = 0
            if (do_pass.or.do_spidx) then
              if (passl(k,ii,ibase).ne.blankc) then
                zzz = zzz * passl(k,ii,ibase)
              else
                 jok(isp) = 0
              endif
            endif
            call scaling (il,ii,ibase,zzz,afac,error)
            if (error) then
              call message(8,4,'ZRECORD',   &
                'Invalid calibration factor for '//cband(ii))
              error = .true.
              return
            endif
            zz(ii) = zz(ii) + zzz*jok(isp)
            mean_afac =  mean_afac + afac*jok(isp)
            iw = iw + jok(isp)
          enddo
          if (iw.ne.0) then
            zz(ii) = zz(ii) / iw
            mean_afac = mean_afac/iw
            !
            ffw(ii) = 2*abs(r_lfres(is))/tsys_b(is,ii,ibase)*iw
          endif
        endif
      endif
      if (mean_afac.gt.0) ffw(ii) = ffw(ii)/mean_afac**2
    enddo
    !        
    call mixband(iband,r_sb(i),zz,ffw,av,zm,fwm)
    z = z + fwm * zm
    if (j_average.ne.1) then
      a = a + fwm * abs(zm)
    endif
    fw = fw + fwm
  enddo
  if (fw.le.0) return
  z = z / fw
  ibnd = min(2,iband)
  if (ibase.le.mnbas .and. dh_rmspha(1,ibnd,ibase).gt.0) then
    fwp = (1e-3/dh_rmspha(1,ibnd,ibase))**2*(1./4.)
  else
    fwp = 0
  endif
  if (j_average.ne.1.and.abs(z).ne.0) then
    z = z / abs(z) * a/fw
  endif
  return
  error = .true.
  return
end subroutine zrecord
!
subroutine irecord (qband, qant, qntch,   &
    datac, datal, iant, iband, nch, ich, jw1, jw2, z, fw, wp)
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !     Get an auto-correlation like information from a record
  !
  !     QANT    INTEGER   Number of antennas
  !     QBAND   INTEGER   Number of temporal data
  !     QNTCH   INTEGER   Number of channels (in total per baseline)
  !     DATAC(QBAND,QANT) REAL     Temporal data
  !     DATAL(QNTCH,QANT) REAL     Spectral data
  !     IANT    INTEGER   Antenna number
  !     IBAND   INTEGER   Subband number
  !     ICH1    INTEGER   First subband
  !     ICH2    INTEGER   Last subband
  !     JW1,JW2 INTEGER   Lowest and Highest channel in window subband
  !     WP      REAL      Positive weight
  !     FW      REAL      (full) Total weight
  !     Z       COMPLEX   Complex output (yes, complex for compatibility)
  ! Call Tree
  !	...		...		ARECORD
  !	...		READ_DATA	BRECORD
  !---------------------------------------------------------------------
  integer :: qband                  !
  integer :: qant                   !
  integer :: qntch                  !
  real :: datac(qband,qant)         !
  real :: datal(qntch,qant)         !
  integer :: iant                   !
  integer :: iband                  !
  integer :: nch                    !
  integer :: ich(nch)               !
  integer :: jw1                    !
  integer :: jw2                    !
  complex :: z                      !
  real*4 :: fw                      !
  real*4 :: wp                      !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  real*4 :: tsys_a, ffw, a, p, aa, afac
  integer :: i, j, n, k, iw, is, jok(mch), isp, jw3
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  z = 0
  a = 0
  p = 0
  n = 0
  fw = 0
  do j = 1, nch
    i = ich(j)                 ! loop on subband range
    afac = 1
    if (i.le.mbands) then          ! continuum subband
      aa = datac(i,iant)
      call ascaling (iant,i,aa,afac,error)
      ffw = 2*r_cfwid(i)/tsys_a(i,iband,iant)/afac**2
    else
      ! now use r_band4 for compatibility with new correlator (is 2 for DSB)
      if (r_band4(i-mbands).eq.2) then
        jw3 = ngibbs
      else
        jw3 = 0
      endif
      iw = 0
      aa = 0
      is = i-mbands
      call jlimits(r_lnch(is),jw1,jw2,jw3,jok)
      call jmask(r_lnch(is),nmask(is),cmask(:,:,is),jok)
      do isp = 1, r_lnch(is)   ! scan line window
        k = r_lich(is) + isp
        aa = aa + datal(k,iant)*jok(isp)
        iw = iw + jok(isp)
      enddo
      if (iw.ne.0) then
        aa = aa / iw
        call ascaling (iant,is,aa,afac,error)
        ffw = 2*abs(r_lfres(is))/tsys_a(is,iband,iant)*iw/afac**2
      else
        ffw = 0
      endif
    endif
    z = z + aa*ffw
    fw = fw + ffw
  enddo
  if (fw.le.0) return
  z = z / fw
  wp = 0
  return
end subroutine irecord
!
subroutine value(ix,z,ibase,iband,kpol,kbb,kif,x,w,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !     Returns value of the specified variable
  !     IX    INTEGER   Type of desired value
  !     IBASE INTEGER   Baseline/Antenna number
  !     IBAND INTEGER   Sideband number
  !     Z     COMPLEX   Complex visibility
  !     X     REAL      Returned value
  !     W     REAL      Returned weight (0 or 1)
  !---------------------------------------------------------------------
  integer :: ix                     !
  complex :: z                      !
  integer :: ibase                  !
  integer :: iband                  !
  integer :: kpol                   ! Polarization
  integer :: kbb                    ! Baseband group
  integer :: kif                    ! IF group
  real*4 :: x                       !
  real*4 :: w                       !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'gbl_pi.inc'
  include 'clic_display.inc'
  include 'clic_constant.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_xy_code.inc'
  include 'clic_proc_par.inc'
  ! Local
  integer :: ia, ja, dobs, ic
  logical :: base, tri
  real :: faz, tsys(2), afac, a, phase_tmp, phacor, refr(3), tan_el
  real*8 :: psi, the, phi, x_2(3), v_2(3), n_4(3), n_2(3), den, p_2(3), s_2
  real*8 :: trfm_24(3,3), lat, ho(2)
  parameter (lat=(44+38d0/60d0+2d0/3600d0)*pi/180d0)
  ! cc is the velocity of light im mm x MHz:
  real :: cc
  parameter (cc=299792.458)
  real :: c1, c2, b, tsky(3)
  integer :: ip1, ip2, ib1, ipol, ibb, nx
  integer :: n_bb, i_bb(mnbb), nif, iif(mnif), jif
  !------------------------------------------------------------------------
  ! Code:
  !     for polar averaging:
  if (kpol.le.r_npol_rec) then
    ip1 = max(kpol,1)
    ip2 = max(kpol,1)
  else
    ip1 = 1
    ip2 = r_npol_rec
  endif
  if (l_baseband(kbb).ne.0) then
    n_bb = l_baseband(kbb) 
    do ibb =1, n_bb
      i_bb(ibb) = i_baseband(ibb,kbb)
    enddo
  else
    n_bb = r_nbb
    do ibb =1, r_nbb
      i_bb(ibb) = r_mapbb(ibb)
    enddo
  endif
  if (l_if(kif).ne.0) then
    nif = l_if(kif)
    do jif=1, nif
      iif(jif) = i_if(jif,kif)
    enddo
  else
    nif = r_nif
    do jif =1, r_nif
       iif(jif) = r_if(jif)
    enddo
  endif
  if (.not.new_receivers) then
    ip1 = 1
    ip2 = 1
    r_nbb = 1
    i_bb = 1
  endif
  w = 1
  base = ibase.gt.0 .and. ibase.le.mnbas
  tri = ibase.gt.mnbas
  if (base) then
    ia = r_iant(ibase)
    ja = r_jant(ibase)
  elseif (.not. tri) then
    ia = abs(ibase)
  else
    ! This should be an error, since no antenna is assigned on the triangles
    ! but it is not yet used
    ia = 1
  endif
  x = 0
  if (ix.eq.xy_ampli) then
    x = abs(z)
  elseif (ix.eq.xy_auto) then
    x = real(z)
  elseif (ix.eq.xy_phase) then
    x = faz(z)
  elseif (ix.eq.xy_real) then
    x = real(z)
  elseif (ix.eq.xy_imag) then
    x = aimag(z)
  elseif (ix.eq.xy_u) then
    if (base) then
      x = dh_uvm(1,ibase)
    endif
  elseif (ix.eq.xy_v) then
    if (base) then
      x = dh_uvm(2,ibase)
    endif
  elseif (ix.eq.xy_radiu) then
    if (base) then
      x = sqrt(dh_uvm(1,ibase)**2+dh_uvm(2,ibase)**2)
    endif
  elseif (ix.eq.xy_angle) then
    if (base) then
      if (dh_uvm(1,ibase).ne.0) then
        x = atan2(dh_uvm(2,ibase),dh_uvm(1,ibase))
      endif
    endif
  elseif (ix.eq.xy_time) then
    dobs = mod(dh_obs+32768,65536)-32768
    if (dobs_0.eq.0) then
      dobs_0 = dobs
    endif
    x = dh_utc/3600.+(dobs-dobs_0)*24.
    if (abs(x).gt.1e5) then
      write(*,*)  'x, dh_utc, dobs, dobs_0'
      write(*,*)  x, dh_utc, dobs, dobs_0
      w = 0
    endif
  elseif (ix.eq.xy_scan) then
    x = r_scan
  elseif (ix.eq.xy_hour) then
    if (dh_svec(1).ne.0) then
      x = atan2(-dh_svec(2),dh_svec(1))*12./pi
    else
      call message(6,3,'VALUE','Error in hour-angle')
    endif
  elseif (ix.eq.xy_delay) then
    x = 0
    nx = 0
    do ib1=1,n_bb
      ibb =i_bb(ib1)
      if (base) then
        x = x + dh_delay(ibb,ia) - dh_delay(ibb,ja)
      else
        x = x + dh_delay(ibb,ia)
      endif
      nx = nx + 1
    enddo
    if (nx.ne.0) x = x / nx
  elseif (ix.eq.xy_cable) then
    if (base) then
      x = 2*pi*(dh_cable(ia) - dh_cable(ja))
    else
      x = 2*pi*dh_cable(ia)
    endif
  elseif (ix.eq.xy_decli) then
    if (abs(dh_svec(3)).le.1.) then
      x = asin(dh_svec(3))*180./pi
    else
      call message(6,3,'VALUE','Error in declination')
      x = blank4
    endif
  elseif (ix.eq.xy_lo_ph) then
    if (base) then
      x = (dh_phasec(ia) - dh_phasec(ja))*2*pi
    else
      x = dh_phasec(ia)*2*pi
    endif
  elseif (ix.eq.xy_lo_ra) then
    if (base) then
      x = dh_ratec(ia) - dh_ratec(ja)
    else
      x = dh_ratec(ia)
    endif
  elseif (ix.eq.xy_atm) then
    !
    ! RL 2006-10-30:
    ! this is provisional, in the near future we have trec for each polar.
    ! and we can catculate the atm emission correctly.
    !
    if (.not.r_presec(atparm_sec)) return
    x = 0
    nx = 0
    if (base) then
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ja).gt.0 .and. r_feff(ibb,ja).gt.0 .and.  &
            r_cchop(ibb,ia).gt.0 .and. r_feff(ibb,ia).gt.0)       &
          then
          x = x + dh_total(ibb,ja)/r_cchop(ibb,ja)   &
            * (r_trec(ibb,ja)+r_tchop(ibb,ja))   &
            -(r_trec(ibb,ja)+(1-r_feff(ibb,ja))*r_tamb)   &
            /r_feff(ibb,ja) -dh_total(ibb,ia)/r_cchop(ibb   &
            ,ia)* (r_trec(ibb,ia) +r_tchop(ibb,ia))   &
            +(r_trec(ibb,ia)+(1-r_feff(ibb,ia))*r_tamb)
          nx = nx + 1
        endif
      enddo
    else
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ia).gt.0 .and. r_feff(ibb,ia).gt.0)   &
          then
          x = x + dh_total(ibb,ia)/r_cchop(ibb,ia)   &
            * (r_trec(ibb,ia)+r_tchop(ibb,ia))   &
            -(r_trec(ibb,ia)+(1-r_feff(ibb,ia))*r_tamb)
          nx = nx + 1
        endif
      enddo
    endif
    if (nx.gt.0) x = x /nx
  elseif (ix.eq.xy_emis) then
    if (.not.r_presec(atparm_sec)) return
    x = 0
    nx = 0
    if (base) then
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ja).gt.0 .and. r_cchop(ibb,ia).gt.0)   &
          then
          a = 1/sin(r_el)
          x = x + dh_total(ibb,ia)/r_cchop(ibb,ia)   &
            * (r_trec(ibb,ia)+r_tchop(ibb,ia))-r_trec(ibb   &
            ,ia) - dh_total(ibb,ja)/r_cchop(ibb,ja)   &
            * (r_trec(ibb,ja)+r_tchop(ibb,ja))-r_trec(ibb   &
            ,ja)
          nx = nx + 1
        endif
      enddo
    else
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ia).gt.0) then
          x = x + dh_total(ibb,ia)/r_cchop(ibb,ia)   &
            * (r_trec(ibb,ia)+r_tchop(ibb,ia))   &
            -r_trec(ibb,ia)
          nx = nx+1
        endif
      enddo
    endif
    if (nx.gt.0) x = x /nx
  elseif (ix.eq.xy_groundem) then
    if (.not.r_presec(atparm_sec)) return
    x = 0
    nx = 0
    if (base) then
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ja).gt.0 .and. r_cchop(ibb,ia).gt.0)   &
          then
          a = 1/sin(r_el)
          x = dh_total(ibb,ia)/r_cchop(ibb,ia)* (r_trec(ibb   &
            ,ia)+r_tchop(ibb,ia))-r_trec(ibb,ia)   &
            -r_feff(ibb,ia)*((1.-exp(-a*r_taus(ibb,ia)))   &
            *r_tatms(ibb,ia) +r_gim(ibb,ia)*(1.-exp(-a   &
            *r_taui(ibb,ia)))*r_tatmi(ibb,ia)) -   &
            dh_total(ibb,ja)/r_cchop(ibb,ja)* (r_trec(ibb   &
            ,ja)+r_tchop(ibb,ja))-r_trec(ibb,ja)   &
            -r_feff(ibb,ja)*((1.-exp(-a*r_taus(ibb,ja)))   &
            *r_tatms(ibb,ja) +r_gim(ibb,ja)*(1.-exp(-a   &
            *r_taui(ibb,ja)))*r_tatmi(ibb,ja))
          nx = nx+1
        endif
      enddo
    else
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (r_cchop(ibb,ia).gt.0) then
          a = 1/sin(r_el)
          x = dh_total(ibb,ia)/r_cchop(ibb,ia)*(r_trec(ibb,ia   &
            )+r_tchop(ibb,ia))-r_trec(ibb,ia)  -   &
            r_feff(ibb,ia)/(1+r_gim(ibb,ia))*((1.-exp(-a   &
            *r_taus(ibb,ia)))*r_tatms(ibb,ia)+r_gim(ibb,ia   &
            )*(1.-exp(-a*r_taui(ibb,ia)))*r_tatmi(ibb,ia))
          nx = nx+1
        endif
      enddo
    endif
    if (nx.gt.0) x = x /nx
  elseif (ix.eq.xy_atm_pha) then
    if (.not.r_presec(atmon_sec)) return
    if (base) then
      x = phase_corr(iband,ja)*(dh_test1(2,ja)-r_csky_mon(ja))   &
        - phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
    else
      x = phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
    endif
  elseif (ix.eq.xy_atm_unc) then
    x = faz(z)
    if (base) then
      if (do_phase_atm(ia) .and. do_phase_atm(ja)) then
        x = x   &
          - phase_corr(iband,ja)*(dh_test1(2,ja)-r_csky_mon(ja))   &
          + phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
      endif
    else
      if (do_phase_atm(ia)) then
        x = x   &
          - phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
      endif
    endif
  elseif (ix.eq.xy_atm_cor) then
    x = faz(z)
    if (base) then
      if (.not.do_phase_atm(ia) .and. .not.do_phase_atm(ja)) then
        x = x   &
          + phase_corr(iband,ja)*(dh_test1(2,ja)-r_csky_mon(ja))   &
          - phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
      endif
    else                       ! Antenna
      if (do_phase_atm(ia)) then
        x = x   &
          + phase_corr(iband,ia)*(dh_test1(2,ia)-r_csky_mon(ia))
      endif
    endif
  elseif (ix.eq.xy_atm_pow) then
    if (base) then
      x = dh_test1(2,ja)-dh_test1(2,ia)
    else
      x = dh_test1(2,ia)
    endif
    if (r_proc.eq.p_cal) w = 0
  elseif (ix.eq.xy_atm_val) then
    if (base) then
      if (r_ok_mon(ia) .and. r_ok_mon(ja)) then
        x = 1.0
      else
        x = 0.0
      endif
    else
      if (r_ok_mon(ia)) then
        x = 1.0
      else
        x = 0.0
      endif
    endif
  elseif (ix.eq.xy_atm_ref) then
    if (base) then
      x = r_csky_mon(ja)-r_csky_mon(ia)
    else
      x = r_csky_mon(ia)
    endif
  elseif (ix.eq.xy_i_atm) then
    if (base) then
      x = dh_test1(2,ia)
    else
      x = dh_test1(2,ia)
    endif
  elseif (ix.eq.xy_j_atm) then
    if (base) then
      x = dh_test1(2,ja)
    else
      x = dh_test1(2,ia)
    endif
  elseif (ix.eq.xy_cal_pha) then
    if (do_phase_antenna) then
      if (base) then
        if (do_polar.ne.2) then
          x = r_aicpha(1,iband,ja,0) - r_aicpha(1,iband,ia,0)
        else
          x = r_aicpha(2,iband,ja,0) - r_aicpha(2,iband,ia,0)
        endif
      else
        if (do_polar.ne.2) then
          x = r_aicpha(1,iband,ia,0)
        else
          x = r_aicpha(2,iband,ia,0)
        endif
      endif
    else
      if (base) then
        if (do_polar.ne.2) then
          x = r_icpha(1,iband,ibase,0)
        else
          x = r_icpha(2,iband,ibase,0)
        endif
        if (x.eq.0 .and. dh_infac(iband,ibase).ne.0) then
          x = -faz(dh_infac(iband,ibase))
        endif
      else
        x = 0
      endif
    endif
  elseif (ix.eq.xy_cal_amp) then
    if (do_amplitude_antenna) then
      if (base) then
        if (do_polar.ne.2) then
          x = exp(r_aicamp(1,iband,ja,0)+r_aicamp(1,iband,ia,0))
        else
          x = exp(r_aicamp(2,iband,ja,0)+r_aicamp(2,iband,ia,0))
        endif
      else
        if (do_polar.ne.2) then
          x = r_aicamp(1,iband,ia,0)
        else
          x = r_aicamp(2,iband,ia,0)
        endif
      endif
    else
      if (base) then
        if (do_polar.ne.2) then
          x = r_icamp(1,iband,ibase,0)
        else
          x = r_icamp(2,iband,ibase,0)
        endif
        if (x.lt.1e-20) then
          if (dh_infac(iband,ibase).eq.0) then
            x = 0
          else
            x = 1./abs(dh_infac(iband,ibase))
          endif
        endif
      else                     ! not available ...
        x = 0
      endif
    endif
  ! more work needed here
  elseif (ix.eq.xy_rf_pha) then
    x = 0
  elseif (ix.eq.xy_rf_amp) then
    x = 0
  elseif (ix.ge.xy_t00 .and. ix.le.xy_t08) then
    x = dh_test0(ix-xy_t00+1)
  elseif (ix.eq.xy_t09) then   ! T09 -> delay recalculated
    if (base) then
      x = (r_bas(1,ibase)*dh_svec(1)+r_bas(2,ibase)*dh_svec(2)+   &
        r_bas(3,ibase)*dh_svec(3))/0.299792458d0
    endif
  elseif (ix.ge.xy_t10 .and. ix.le.xy_t19) then
    x = dh_test1(ix-xy_t10+1,ia)
  elseif (ix.ge.xy_t20 .and. ix.le.xy_t29) then
    x = dh_test1(ix-xy_t20+11,ia)
  elseif (ix.eq.xy_gamme) then
    if (base) then
      x = (dh_gamme(ia) - dh_gamme(ja))
    else
      x = dh_gamme(ia)
    endif
  elseif (ix.eq.xy_flux) then
    x = abs(r_flux)
  elseif (ix.eq.xy_lo1_f) then
    x = r_flo1
  elseif (ix.eq.xy_lo1_ref) then
    x = r_flo1_ref
  elseif (ix.eq.xy_doppl) then
    x = r_doppl*299792.458     ! in km/s
  elseif (ix.eq.xy_az_co) then
    x = r_collaz(ia)
  elseif (ix.eq.xy_el_co) then
    x = r_collel(ia)
  elseif (ix.eq.xy_azerr) then
    x = dh_rmspe(1,ia)
  elseif (ix.eq.xy_elerr) then
    x = dh_rmspe(2,ia)
  elseif (ix.eq.xy_foc_c) then
    x = r_corfoc(ia)
  elseif (ix.eq.xy_gain) then
    x = 0
    nx = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = x + r_gim(ibb, ia)
      nx = nx+1
    enddo
    if (nx.gt.0) x = x /nx
  elseif (ix.eq.xy_opaci) then
    x = 0
    nx = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      if (iband.eq.1 .or. iband.eq.2) then
        if (r_sb(ibb).eq.1) then
          if (iband.eq.1) then
            x = x+r_taus(ibb,ia)
            nx = nx+1
          else
            x = x+r_taui(ibb,ia)
            nx = nx+1
          endif
        else
          if (iband.eq.1) then
            x = x+r_taui(ibb,ia)
            nx = nx+1
          else
            x = x+r_taus(ibb,ia)
            nx = nx+1
          endif
        endif
      elseif(iband.eq.3) then
        x = x+ (r_taus(ibb,ia)+r_taui(ibb,ia)) / 2
        nx = nx+1
      elseif (iband.eq.4) then
        x = x+ r_taus(ibb,ia)/r_taui(ibb,ia)
        nx = nx+1
      endif
    enddo
    if (nx.gt.0) x = x /nx
  elseif (ix.eq.xy_lambd) then
    if (base) then
      x = dh_offlam(ia)
      if (x.eq.0) x = dh_offlam(ja)
    else
      x = dh_offlam(ia)
    endif
  elseif (ix.eq.xy_beta) then
    if (base) then
      x = dh_offbet(ia)
      if (x.eq.0) x = dh_offbet(ja)
    else
      x = dh_offbet(ia)
    endif
  elseif (ix.eq.xy_focus) then
    if (base) then
      x = dh_offfoc(ia)
      if (x.eq.0) x = dh_offfoc(ja)
    else
      x = dh_offfoc(ia)
    endif
  elseif (ix.eq.xy_total) then
    x = 0
    nx = 0
    if (.not. base) then
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        if (physical_power) then
          x = x+dh_total(ibb,ia)*r_totscale(ibb,ia)
        else
          x = x+dh_total(ibb,ia)
        endif
        nx = nx + 1
      enddo
    endif
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_trec) then
    x = 0
    nx = 0
    if (.not. base) then
      do ib1 = 1, n_bb
        ibb = i_bb(ib1)
        x = x + r_trec(ibb, ia)
        nx = nx+1
      enddo
    endif
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_tsys) then
    x = 0
    nx = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      if (base) then
        if (r_sb(ibb).eq.1) then
          tsys(1) = sqrt(r_tsyss(ibb,ia)*r_tsyss(ibb,ja))
          tsys(2) = sqrt(r_tsysi(ibb,ia)*r_tsysi(ibb,ja))
        else
          tsys(1) = sqrt(r_tsysi(ibb,ia)*r_tsysi(ibb,ja))
          tsys(2) = sqrt(r_tsyss(ibb,ia)*r_tsyss(ibb,ja))
        endif
      else
        if (r_sb(ibb).eq.1) then
          tsys(1) = r_tsyss(ibb,ia)
          tsys(2) = r_tsysi(ibb,ia)
        else
          tsys(2) = r_tsyss(ibb,ia)
          tsys(1) = r_tsysi(ibb,ia)
        endif
      endif
      if (iband.eq.1 .or. iband.eq.2) then
        x = x + tsys(iband)
        nx = nx+1
      elseif(iband.eq.3) then
        x = x + (tsys(1)+tsys(2)) / 2
        nx = nx+1
      elseif (iband.eq.4) then
        x = x + tsys(1)/tsys(2)
        nx = nx+1
      endif
    enddo
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_pamb) then
    x = r_pamb
  elseif (ix.eq.xy_tamb) then
    x = r_tamb
  elseif (ix.eq.xy_humid) then
    x = r_humid
  elseif (ix.eq.xy_windav) then
    x = r_avwind
  elseif (ix.eq.xy_windirav) then
    x = r_avwindir*180.0/pi
  elseif (ix.eq.xy_wintop) then
    x = r_topwind
  elseif (ix.eq.xy_windirtop) then
    x = r_topwindir*180.0/pi
  elseif (ix.eq.xy_numbe) then
    x = r_num
  elseif (ix.eq.xy_quali) then
    x = r_qual
  elseif (ix.eq.xy_azimu) then
    x = r_az*180./pi + dh_offlam(ia)/3600.
  elseif (ix.eq.xy_eleva) then
    x = r_el*180./pi + dh_offbet(ia)/3600.
  elseif (ix.eq.xy_airmass) then
    x = r_el + dh_offbet(ia)*pi/180./3600.
    if (x.ne.0) x = 1/sin(x)
  elseif (ix.eq.xy_water) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = x + r_h2omm(ibb,ia)
      nx = nx+1
    enddo
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_tcold) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = x + r_tcold(ibb,ia)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_tchop) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = x + r_tchop(ibb,ia)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x / nx
  elseif (ix.eq.xy_tcabin) then
    x = r_tcabin(ia)
  elseif (ix.eq.xy_tdewar1) then
    x = r_tdewar(1,ia)
  elseif (ix.eq.xy_tdewar2) then
    x = r_tdewar(2,ia)
  elseif (ix.eq.xy_tdewar3) then
    x = r_tdewar(3,ia)
  elseif (ix.eq.xy_rms_a .and. iband.le.2) then
    if (base) then
      if (.not.new_receivers) then
        x = dh_rmsamp(1,iband, ibase)
        z = x
        call scaling (1,iband,ibase,z,afac,error)
        x = abs(z)
      endif
    endif
  elseif (ix.eq.xy_rms_p .and. iband.le.2) then
    if (base) then
      x = min(pis,dh_rmspha(1,iband, ibase))
    endif
  elseif (ix.eq.xy_parang .or. ix.eq.xy_azph   &
    .or. ix.eq.xy_elph) then
    ! parallactic angle
    ! dh_svec is the source vector in hour-angle, dec coordinates (4)
    ! n_4 is the normal to the source meridian plane
    ! TRFM_24 goes from (4) to (2)
    ! n_2 is the normal to the source meridian plane, in horiz coords.(2)
    ! x_2 is the source vector in in horiz coords.
    ! v_2 is the intersection of the vertical plane of the source and
    ! the plane of the sky
    ! p_2 is the vector product of v_2 and n_2
    ! s_2 is the scalar product of p_2 and x_2
    psi=pi/2                   ! 90 degrees
    the=pi/2-lat               ! 90 degrees minus latitude
    phi=-pi/2                  ! -90 degrees
    call eulmat (psi,the,phi,trfm_24)
    call matvec (dh_svec, trfm_24, x_2)
    call spher(x_2, ho)
    if (ix.eq.xy_azph) then
      ho(1) = mod(ho(1)+3d0*pi,2d0*pi)-pi
      x = -ho(1)*180d0/pi
    elseif (ix.eq.xy_elph) then
      ho(2) = mod(ho(2)+3d0*pi,2d0*pi)-pi
      x = ho(2)*180d0/pi
    else
      den = sqrt(dh_svec(1)**2+dh_svec(2)**2)
      n_4(1) = -dh_svec(2)/den
      n_4(2) = dh_svec(1)/den
      n_4(3) = 0
      call matvec (n_4, trfm_24, n_2)
      den = sqrt(x_2(1)**2+x_2(2)**2)
      v_2(1) = x_2(1)*x_2(3) / den
      v_2(2) = x_2(2)*x_2(3) / den
      v_2(3) = -den
      p_2(1) = v_2(2)*n_2(3)-v_2(3)*n_2(2)
      p_2(2) = v_2(3)*n_2(1)-v_2(1)*n_2(3)
      p_2(3) = v_2(1)*n_2(2)-v_2(2)*n_2(1)
      s_2 = p_2(1)*x_2(1)+p_2(2)*x_2(1)+p_2(3)*x_2(3) 
      x = -pi/2+sign(acos(v_2(1)*n_2(1)+v_2(2)*n_2(2)+v_2(3)*n_2(3)),s_2)
    endif
  !
  !wvr
  elseif (ix.eq.xy_wvrtamb) then
    x = r_wvrtamb(ia)
  elseif (ix.eq.xy_wvrtpel) then
    x = r_wvrtpel(ia)
  elseif (ix.eq.xy_wvrtcal1) then
    x = r_wvrtcal(1,ia)
  elseif (ix.eq.xy_wvrtcal2) then
    x = r_wvrtcal(2,ia)
  elseif (ix.eq.xy_wvrtcal3) then
    x = r_wvrtcal(3,ia)
  elseif (ix.eq.xy_wvrref1) then
    x = r_wvrref(1,ia)
  elseif (ix.eq.xy_wvrref2) then
    x = r_wvrref(2,ia)
  elseif (ix.eq.xy_wvrref3) then
    x = r_wvrref(3,ia)
  elseif (ix.eq.xy_wvraver1) then
    x = r_wvraver(1,ia)
  elseif (ix.eq.xy_wvraver2) then
    x = r_wvraver(2,ia)
  elseif (ix.eq.xy_wvraver3) then
    x = r_wvraver(3,ia)
  elseif (ix.eq.xy_wvramb1) then
    x = r_wvramb(1,ia)
  elseif (ix.eq.xy_wvramb2) then
    x = r_wvramb(2,ia)
  elseif (ix.eq.xy_wvramb3) then
    x = r_wvramb(3,ia)
  elseif (ix.eq.xy_wvrtrec1) then
    x = r_wvrtrec(1,ia)
  elseif (ix.eq.xy_wvrtrec2) then
    x = r_wvrtrec(2,ia)
  elseif (ix.eq.xy_wvrtrec3) then
    x = r_wvrtrec(3,ia)
  elseif (ix.eq.xy_wvrfeff1) then
    x = r_wvrfeff(1,ia)
  elseif (ix.eq.xy_wvrfeff2) then
    x = r_wvrfeff(2,ia)
  elseif (ix.eq.xy_wvrfeff3) then
    x = r_wvrfeff(3,ia)
  elseif (ix.eq.xy_wvrmode) then
    x = r_wvrmode(ia)
  elseif (ix.eq.xy_wvrh2o) then
    x = r_wvrh2o(ia)
  elseif (ix.eq.xy_wvrpath) then
    x = r_wvrpath(ia)
  elseif (ix.eq.xy_wvrtsys1) then
    x = r_wvrtsys(1,ia)
  elseif (ix.eq.xy_wvrtsys2) then
    x = r_wvrtsys(2,ia)
  elseif (ix.eq.xy_wvrtsys3) then
    x = r_wvrtsys(3,ia)
  elseif (ix.eq.xy_wvrdpath1) then
    x = r_wvrdpath(1,ia)
  elseif (ix.eq.xy_wvrdpath2) then
    x = r_wvrdpath(2,ia)
  elseif (ix.eq.xy_wvrdpath3) then
    x = r_wvrdpath(3,ia)
  elseif (ix.eq.xy_wvrfpath1) then
    x = r_wvrfpath(1,ia)
  elseif (ix.eq.xy_wvrfpath2) then
    x = r_wvrfpath(2,ia)
  elseif (ix.eq.xy_wvrfpath3) then
    x = r_wvrfpath(3,ia)
  elseif (ix.eq.xy_wvrliq1) then
    x = r_wvrliq(1,ia)
  elseif (ix.eq.xy_wvrliq2) then
    x = r_wvrliq(2,ia)
  elseif (ix.eq.xy_wvrliq3) then
    x = r_wvrliq(3,ia)
  elseif (ix.eq.xy_wvrdcloud1) then
    x = r_wvrdcloud(1,ia)
  elseif (ix.eq.xy_wvrdcloud2) then
    x = r_wvrdcloud(2,ia)
  elseif (ix.eq.xy_wvrdcloud3) then
    x = r_wvrdcloud(3,ia)
  elseif (ix.eq.xy_wvrtatm) then
    x = r_wvrtatm(ia)
  elseif (ix.eq.xy_wvrqual) then
    x = r_wvrqual(ia)
  elseif (ix.eq.xy_dh_wvr1) then
    x = dh_wvr(1,ia)
  elseif (ix.eq.xy_dh_wvr2) then
    x = dh_wvr(2,ia)
  elseif (ix.eq.xy_dh_wvr3) then
    x = dh_wvr(3,ia)
  elseif (ix.eq.xy_dh_wvrstat) then
    x = dh_wvrstat(ia)
  elseif (ix.eq.xy_wvr_pha_m) then
    if (iband.eq.1) then
      phacor = 2*pi*(r_flo1+r_fif1)/cc
    else
      phacor = 2*pi*(r_flo1-r_fif1)/cc
    endif
    if (base) then
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ja)-r_wvrref(ic,ja))   &
          *r_wvrtcal(ic,ja)*r_wvrdpath(ic,ja)
      enddo
      ! note dpath in mum/K
      x = - phacor*phase_tmp/1000.
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdpath(ic,ia)
      enddo
      x = x + phacor*phase_tmp/1000.
    !         ELSEIF (DO_PHASE_WVR(IA)) THEN
    else
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdpath(ic,ia)
      enddo
      x = phacor*phase_tmp/1000.
    endif
  elseif (ix.eq.xy_wvr_pha_e) then
    if (iband.eq.1) then
      phacor = 2*pi*(r_flo1+r_fif1)/cc
    else
      phacor = 2*pi*(r_flo1-r_fif1)/cc
    endif
    !         IF (BASE .AND. DO_PHASE_WVR(IA) .AND. DO_PHASE_WVR(JA) ) THEN
    if (base) then
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ja)-r_wvrref(ic,ja))   &
          *r_wvrtcal(ic,ja)*r_wvrdpath(ic,ja)
      enddo
      x = - phacor*phase_tmp/1000.*r_wvrfpath(1,ja)
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdpath(ic,ia)
      enddo
      x = x + phacor*phase_tmp/1000.*r_wvrfpath(1,ia)
    else
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdpath(ic,ia)*r_wvrfpath(1,ia)
      enddo
      x = phacor*phase_tmp/1000.
    endif
  elseif (ix.eq.xy_wvr_pha_c) then
    if (iband.eq.1) then
      phacor = 2*pi*(r_flo1+r_fif1)/cc
    else
      phacor = 2*pi*(r_flo1-r_fif1)/cc
    endif
    if (base) then
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ja)-r_wvrref(ic,ja))   &
          *r_wvrtcal(ic,ja)*r_wvrdcloud(ic,ja)*r_wvrliq(ic,ja)
      enddo
      x = - phacor*phase_tmp/1000.
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdcloud(ic,ia)*r_wvrliq(ic,ia)
      enddo
      x = x + phacor*phase_tmp/1000.
    else
      phase_tmp = 0.0
      do ic=1,mwvrch
        phase_tmp = phase_tmp + (dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
          *r_wvrtcal(ic,ia)*r_wvrdcloud(ic,ia)*r_wvrliq(ic,ia)
      enddo
      x = phacor*phase_tmp/1000.
    endif
  elseif (ix.eq.xy_wvr_triple) then
    if (base) then
      if (r_wvrnch(ia).eq.3) then
        c1 = (r_wvrfreq(2,ia) / r_wvrfreq(1,ia))**2
        c2 = (r_wvrfreq(3,ia) / r_wvrfreq(2,ia))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          if (r_wvrmode(ia).ge.5) then
            tsky(ic)=(dh_wvr(ic,ia)-r_wvrtrec(ic,ia))   &
              *r_wvrtcal(ic,ia)
          else
            tsky(ic)=(dh_wvr(ic,ia)*r_wvrtcal(ic,ia)   &
              -r_wvrtrec(ic,ia)   &
              -(1.-r_wvrfeff(ic,ia))*r_wvrtamb(ia))   &
              /r_wvrfeff(ic,ia)
          endif
        enddo
        if (r_wvrmode(ia).eq.6) then
           x = tsky(2)-tsky(3)*c2
        else
           x= b*(tsky(2)-tsky(3)*c2) - (tsky(1)-tsky(2)*c1)
        endif
      endif
      if (r_wvrnch(ja).eq.3) then
        c1 = (r_wvrfreq(2,ja) / r_wvrfreq(1,ja))**2
        c2 = (r_wvrfreq(3,ja) / r_wvrfreq(2,ja))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          if (r_wvrmode(ja).ge.5) then
            tsky(ic)=(dh_wvr(ic,ja)-r_wvrtrec(ic,ja))   &
              *r_wvrtcal(ic,ja)
          else
            tsky(ic)=(dh_wvr(ic,ja)*r_wvrtcal(ic,ja)   &
              -r_wvrtrec(ic,ja)   &
              -(1.-r_wvrfeff(ic,ja))*r_wvrtamb(ja))   &
              /r_wvrfeff(ic,ja)
          endif
        enddo

        if (r_wvrmode(ia).eq.6) then
           x = x - (tsky(2)-tsky(3)*c2)
        else
           x= x - b*(tsky(2)-tsky(3)*c2) + (tsky(1)-tsky(2)*c1)
        endif

      endif
    else
      if (r_wvrnch(ia).eq.3) then
        c1 = (r_wvrfreq(2,ia) / r_wvrfreq(1,ia))**2
        c2 = (r_wvrfreq(3,ia) / r_wvrfreq(2,ia))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          if (r_wvrmode(ia).ge.5) then
            tsky(ic)=(dh_wvr(ic,ia)-r_wvrtrec(ic,ia))   &
              *r_wvrtcal(ic,ia)
          else
            tsky(ic)=(dh_wvr(ic,ia)*r_wvrtcal(ic,ia)   &
              -r_wvrtrec(ic,ia)   &
              -(1.-r_wvrfeff(ic,ia))*r_wvrtamb(ia))   &
              /r_wvrfeff(ic,ia)
          endif
        enddo
        if (r_wvrmode(ia).eq.6) then
           x = tsky(2)-tsky(3)*c2
        else
           x= b*(tsky(2)-tsky(3)*c2) - (tsky(1)-tsky(2)*c1)
        endif
  
      endif
    endif
  elseif (ix.eq.xy_tatm_s) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = x + r_tatms(ibb,ia)
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_tatm_i) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x = r_tatmi(ibb,ia)
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_wvr_dt1) then
    if (base) then
      x = (dh_wvr(1,ja)-r_wvrref(1,ja))*r_wvrtcal(1,ja)   &
        - (dh_wvr(1,ia)-r_wvrref(1,ia))*r_wvrtcal(1,ia)
    else
      x = (dh_wvr(1,ia)-r_wvrref(1,ia))*r_wvrtcal(1,ia)
    endif
  elseif (ix.eq.xy_wvr_dt2) then
    if (base) then
      x = (dh_wvr(2,ja)-r_wvrref(2,ja))*r_wvrtcal(2,ja)   &
        - (dh_wvr(2,ia)-r_wvrref(2,ia))*r_wvrtcal(2,ia)
    else
      x = (dh_wvr(2,ia)-r_wvrref(2,ia))*r_wvrtcal(2,ia)
    endif
  elseif (ix.eq.xy_wvr_dt3) then
    if (base) then
      x = (dh_wvr(3,ja)-r_wvrref(3,ja))*r_wvrtcal(3,ja)   &
        - (dh_wvr(3,ia)-r_wvrref(3,ia))*r_wvrtcal(3,ia)
    else
      x = (dh_wvr(3,ia)-r_wvrref(3,ia))*r_wvrtcal(3,ia)
    endif
  elseif (ix.eq.xy_wvr_dtri) then
    if (base) then
      if (r_wvrnch(ja).eq.3) then
        c1 = (r_wvrfreq(2,ja) / r_wvrfreq(1,ja))**2
        c2 = (r_wvrfreq(3,ja) / r_wvrfreq(2,ja))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          tsky(ic)=(dh_wvr(ic,ja)-r_wvrref(ic,ja))   &
            *r_wvrtcal(ic,ja)/r_wvrfeff(ic,ja)
        enddo
        if (r_wvrmode(ja).eq.6) then
           x = tsky(2)-tsky(3)*c2
        else
           x= b*(tsky(2)-tsky(3)*c2) - (tsky(1)-tsky(2)*c1)
        endif
  
      endif
      if (r_wvrnch(ia).eq.3) then
        c1 = (r_wvrfreq(2,ia) / r_wvrfreq(1,ia))**2
        c2 = (r_wvrfreq(3,ia) / r_wvrfreq(2,ia))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          tsky(ic)=(dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
            *r_wvrtcal(ic,ia)/r_wvrfeff(ic,ia)
        enddo

        if (r_wvrmode(ia).eq.6) then
           x = x - (tsky(2)-tsky(3)*c2)
        else
           x= x - b*(tsky(2)-tsky(3)*c2) + (tsky(1)-tsky(2)*c1)
        endif

      endif
    else
      if (r_wvrnch(ia).eq.3) then
        c1 = (r_wvrfreq(2,ia) / r_wvrfreq(1,ia))**2
        c2 = (r_wvrfreq(3,ia) / r_wvrfreq(2,ia))**2
        b = (1.-c1)/(1.-c2)
        do ic = 1, mwvrch
          tsky(ic)=(dh_wvr(ic,ia)-r_wvrref(ic,ia))   &
            *r_wvrtcal(ic,ia)/r_wvrfeff(ic,ia)
        enddo
        if (r_wvrmode(ia).eq.6) then
           x = tsky(2)-tsky(3)*c2
        else
           x= b*(tsky(2)-tsky(3)*c2) - (tsky(1)-tsky(2)*c1)
        endif
      endif
    endif
  elseif (ix.eq.xy_dh_actp1) then
    x = dh_actp(1,ia,1)
  elseif (ix.eq.xy_dh_actp2) then
    x = dh_actp(1,ia,2)
  elseif (ix.eq.xy_dh_actp3) then
    x = dh_actp(1,ia,3)
  elseif (ix.eq.xy_dh_actp4) then
    x = dh_actp(1,ia,4)
  elseif (ix.eq.xy_dh_actp5) then
    x = dh_actp(1,ia,5)
  elseif (ix.eq.xy_dh_actp6) then
    x = dh_actp(1,ia,6)
  elseif (ix.eq.xy_dh_actp7) then
    x = dh_actp(1,ia,7)
  elseif (ix.eq.xy_dh_actp8) then
    x = dh_actp(1,ia,8)
  elseif (ix.eq.xy_dh_anttp) then
    nx = 0
    x = 0
    do jif = 1, nif
      ipol = iif(jif)
      x = x + dh_anttp(ipol,ia)
      nx = nx + 1 
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_attenuation) then
    nx = 0
    x = 0
    do jif = 1, nif
      ipol = iif(jif)
      x = x + r_ifattn(ia,ipol)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_lilevl1) then
    x = r_lilevl(1,ia)
  elseif (ix.eq.xy_lilevl2) then
    x = r_lilevl(2,ia)
  elseif (ix.eq.xy_lilevl3) then
    x = r_lilevl(3,ia)
  elseif (ix.eq.xy_lilevl4) then
    x = r_lilevl(4,ia)
  elseif (ix.eq.xy_lilevl5) then
    x = r_lilevl(5,ia)
  elseif (ix.eq.xy_lilevl6) then
    x = r_lilevl(6,ia)
  elseif (ix.eq.xy_lilevl7) then
    x = r_lilevl(7,ia)
  elseif (ix.eq.xy_lilevl8) then
    x = r_lilevl(8,ia)
  elseif (ix.eq.xy_lilevl9) then
    x = r_lilevl(9,ia)
  elseif (ix.eq.xy_lilevl10) then
    x = r_lilevl(10,ia)
  elseif (ix.eq.xy_lilevl11) then
    x = r_lilevl(11,ia)
  elseif (ix.eq.xy_lilevl12) then
    x = r_lilevl(12,ia)
  elseif (ix.eq.xy_lilevu1) then
    x = r_lilevu(1,ia)
  elseif (ix.eq.xy_lilevu2) then
    x = r_lilevu(2,ia)
  elseif (ix.eq.xy_lilevu3) then
    x = r_lilevu(3,ia)
  elseif (ix.eq.xy_lilevu4) then
    x = r_lilevu(4,ia)
  elseif (ix.eq.xy_lilevu5) then
    x = r_lilevu(5,ia)
  elseif (ix.eq.xy_lilevu6) then
    x = r_lilevu(6,ia)
  elseif (ix.eq.xy_lilevu7) then
    x = r_lilevu(7,ia)
  elseif (ix.eq.xy_lilevu8) then
    x = r_lilevu(8,ia)
  elseif (ix.eq.xy_scale) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x =  x + r_totscale(ibb,ia)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_cable_alternate) then
    if (base) then
      x = 2*pi*(dh_cable_alternate(ia) - dh_cable_alternate(ja))
    else
      x = 2*pi*dh_cable_alternate(ia)
    endif
  elseif (ix.ge.xy_tim_01 .and. ix.le.xy_tim_04) then
    x = dh_time_monitoring(ix-xy_tim_01+1)
  elseif (ix.eq.xy_laser_level) then
    do jif = 1, nif
       ipol = iif(jif)
       x = x + r_laser_level(ia,ipol)
       nx = nx +1
    enddo 
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_hiq_atten1) then
    x = r_attenuation_hiq(ia,1)
  elseif (ix.eq.xy_hiq_atten2) then
    x = r_attenuation_hiq(ia,2)
  elseif (ix.eq.xy_widex_temp1) then
    x = r_widex_temperature(1)
  elseif (ix.eq.xy_widex_temp2) then
    x = r_widex_temperature(2)
  elseif (ix.eq.xy_widex_temp3) then
    x = r_widex_temperature(3)
  elseif (ix.eq.xy_widex_temp4) then
    x = r_widex_temperature(4)
  elseif (ix.eq.xy_inclix) then
    x = dh_incli(ia,1)
  elseif (ix.eq.xy_incliy) then
    x = dh_incli(ia,2)
  elseif (ix.eq.xy_incli_temp) then
    x = dh_incli_temp(ia)
  elseif (ix.eq.xy_refc1) then
    call refrac(refr)
    x = refr(1)*180.0/pi*3600
  elseif (ix.eq.xy_refc2) then
    call refrac(refr)
    x = refr(2)*180.0/pi*3600
  elseif (ix.eq.xy_refc3) then
    call refrac(refr)
    x = refr(3)*180.0/pi*3600
  elseif (ix.eq.xy_refcor) then
    call refrac(refr)
    if (r_el.ne.0) then
       tan_el = tan(r_el)       
       x = refr(1)/tan_el+refr(2)/tan_el**3+refr(3)/tan_el**5
       x = x*180.0/pi*3600
    endif
  elseif (ix.eq.xy_vlbi_phaseff) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x =  x + dh_vlbi_phaseff(ibb)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_vlbi_phase_inc) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x =  x + dh_vlbi_phase_inc(ia,ibb)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_vlbi_phase_tot) then
    nx = 0
    x = 0
    do ib1 = 1, n_bb
      ibb = i_bb(ib1)
      x =  x + dh_vlbi_phase_tot(ia,ibb)
      nx = nx + 1
    enddo
    if (nx.gt.0) x = x/nx
  elseif (ix.eq.xy_vlbi_maser) then
    x =  r_maser_gps_diff
  endif
  return
!999  error = .true.
!     RETURN
end subroutine value
!
subroutine bin(error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Bin data in plot arrays
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: ib, nh, m_bin, nbh, nout
  !
  integer(kind=address_length) :: data_bin, ipx,ipy,ipz,ipw,ipd,ko
  data m_bin/0/
  save m_bin, data_bin
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Get auxiliary storage
  !      NH = M_DATA
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipz = gag_pointer(data_z,memory)
  ipw = gag_pointer(data_w,memory)
  nh = 0
  do ib=1, n_boxes
    ko = (ib-1)*m_data
    call get_nbins(memory(ipx+ko),memory(ipw+ko),n_data(ib),bin_size,nbh)
    nh = max(nbh,nh)
  enddo
  if (nh*4.gt.m_bin) then
    if (m_bin .gt.0) then
      call free_vm(m_bin,data_bin)
    endif
    m_bin = nh*4
    error = sic_getvm(m_bin,data_bin).ne.1
    if (error) return
  endif
  !
  ipd = gag_pointer(data_bin,memory)
  do ib=1, n_boxes
    ko = (ib-1)*m_data
    call binning(i_y(k_y(ib)),j_average,   &
      memory(ipx+ko), memory(ipy+ko), memory(ipz+2*ko),   &
      memory(ipw+ko),   &
      n_data(ib), nh, nout, blank4, d_blank4,degrees,   &
      bin_pos,bin_size,memory(ipd))
    !         N_DATA(IB) = NH
    n_data(ib) = min(nout,m_data)
  enddo
  return
end subroutine bin
!
subroutine get_nbins(x,w,nx,bs,nb)
  integer :: nx                     !
  real :: x(nx)                     !
  real :: w(nx)                     !
  real :: bs                        !
  integer :: nb                     !
  ! Local
  real :: xmin, xmax
  integer :: i
  !-----------------------------------------------------------------------
  xmin = 1e37
  xmax = -1e37
  do i=1, nx
    if (w(i).gt.0) then
      xmin = min(xmin,x(i))
      xmax = max(xmax,x(i))
    endif
  enddo
  nb = (xmax-xmin)/abs(bs)+2
  return
end subroutine get_nbins
!
subroutine binning(iy,ja,x,y,z,w,nin,nh,nout,bval,eval,degrees,   &
    hmin,hstep,work)
  !---------------------------------------------------------------------
  ! CLIC	Do data binning in X for data values in Y
  ! Arguments
  ! Input/Output:
  !       IY      Integer Type of value
  !	X,Y,W	R*4(*)	Input arrays (size>max(np,nh))
  !	Z	C*8(*)	...
  !				Output data
  ! Input:
  !	NIN 	I	Number of input points
  !	NH	I	Number of bins (<NIN)
  !	HMIN	R*4	Minimum of first bin
  !	HSTEP	R*4	Bin size
  !	BVAL	R*4	Blanking values
  !	EVAL	R*4	Tolerance on blanking
  ! 	WORK	R*4	Space of size 4*NH
  !---------------------------------------------------------------------
  integer :: iy                     !
  integer :: ja                     !
  real :: x(*)                      !
  real :: y(*)                      !
  complex :: z(*)                   !
  real :: w(*)                      !
  integer :: nin                    !
  integer :: nh                     !
  integer :: nout                   !
  real :: bval                      !
  real :: eval                      !
  logical :: degrees                !
  real :: hmin                      !
  real :: hstep                     !
  real :: work(4,nin)               !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: i, n
  real :: xmin, xmax, faz
  !------------------------------------------------------------------------
  ! Code:
  xmin = 1e37
  xmax = -1e37
  do i=1, nin
    if (w(i).gt.0) then
      xmin = min(xmin,x(i))
      xmax = max(xmax,x(i))
    endif
  enddo
  xmin = hmin + hstep*nint((xmin-hmin)/hstep-1.)
  !      NH = MIN(Nh, NINT((XMAX-XMIN)/HSTEP+2.))
  do i=1, nh
    work(1,i) = 0.
    work(2,i) = 0.
    work(3,i) = 0.
    work(4,i) = 0.
  enddo
  do i=1, nin
    if (abs(y(i)-bval).gt.eval) then
      if (iy.eq.xy_phase) then
        call cvphase(1,y(i),w(i),.false.,degrees)
        w(i) = w(i)/abs(z(i))**2
      endif
      n = nint((x(i)-xmin)/hstep + 1.0)
      if (n.ge.1 .and. n.le.nh) then
        work(1,n) = work(1,n) + w(i)
        work(2,n) = work(2,n) + y(i)*w(i)
        if (iy.le.xy_phase) then
          work(3,n) = work(3,n) + real(z(i))*w(i)
          work(4,n) = work(4,n) + aimag(z(i))*w(i)
        endif
      endif
    endif
  enddo
  nout = 0
  do i=1, nh
    if (work(1,i).gt.0) then
      nout = nout+1
      x(nout) = xmin + (i-1)*hstep
      if (iy.eq.xy_phase) then
        w(nout) =   &
          1./work(1,i)*abs(cmplx(work(3,i),work(4,i)))**2
      else
        w(nout) = work(1,i)
      endif
      if (ja.eq.1 .and. iy.eq.xy_ampli) then
        y(nout) = abs(cmplx(work(3,i),work(4,i))/work(1,i))
      elseif (iy.eq.xy_phase) then
        y(nout) = faz(cmplx(work(3,i),work(4,i)))
        call cvphase(1,y(nout),w(nout),degrees,.false.)
      else
        y(nout) = work(2,i)/work(1,i)
      endif
    else
      nout = nout+1
      x(nout) = xmin + (i-1)*hstep
      w(nout) = 0
      y(nout) =  blank4
    endif
  enddo
  return
end subroutine binning
!
subroutine jlimits(nch,jw1,jw2,jw3,jok)
  use gildas_def
  !---------------------------------------------------------------------
  ! CLIC: compute actual channel limits of subband being displayed
  !---------------------------------------------------------------------
  ! number of channels, limits from SET SUBB /WINDOW, SET GIBBS:
  integer :: nch                    !
  integer :: jw1                    !
  integer :: jw2                    !
  integer :: jw3                    !
  integer :: jok(nch)               !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  ! Local
  integer :: i, jstart, jend
  !------------------------------------------------------------------------
  ! Code:
  do i=1, nch
    jok(i) = 1
  enddo
  if (jw1.eq.0) then
    jstart = nint(fdrop(1)*nch)
  else
    jstart = jw1
  endif
  if (jstart.gt.1) then
    do i=1, jstart-1
      jok(i) = 0
    enddo
  endif
  if (jw2.eq.0) then
    jend = nint((1.-fdrop(2))*nch)
  else
    jend = min(nch,jw2)
  endif
  if (jend.lt.nch) then
    do i=jend+1, nch
      jok(i) = 0
    enddo
  endif
  ! Gibbs
  if (jw3.gt.0) then
    do i=nch/2-jw3+1, nch/2+jw3
      jok(i) = 0
    enddo
  endif
  return
end subroutine jlimits
!
subroutine set_corr(error)
  use classic_api
  !---------------------------------------------------------------------
  !     compute the phase corrections factors in radians/unit of 
  !     tot pow monitor or WVR
  !     these are used in the scaling routine
  !---------------------------------------------------------------------
  logical :: error                  
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: ia
  ! cc is the velocity of light im mm x MHz:
  real :: cc
  parameter (cc=299792.458)
  !-----------------------------------------------------------------------
  ! 
  do ia=1, r_nant
     if (do_phase_mon(ia)) then
        !
        ! TOT POWER MONITOR
        !
        if (r_csky_mon(ia).ne.0) then
           phase_corr(1,ia) = r_tsys_mon(ia)/r_csky_mon(ia)   &
                *2*pi*r_dpath_mon(ia)*(r_flo1+r_fif1)/cc   &
                * r_magic_mon(1)
           phase_corr(2,ia) = r_tsys_mon(ia)/r_csky_mon(ia)   &
                *2*pi*r_dpath_mon(ia)*(r_flo1-r_fif1)/cc   &
                * r_magic_mon(1)
        else
           phase_corr(1,ia) = 0
           phase_corr(2,ia) = 0
        endif
     elseif (do_phase_wvr(ia)) then
        !
        ! WVR
        !
        phase_corr(1,ia) = 2*pi*(r_flo1+r_fif1)/cc
        phase_corr(2,ia) = 2*pi*(r_flo1-r_fif1)/cc
     else
        phase_corr(1,ia) = 0
        phase_corr(2,ia) = 0
     endif
     if (abs(phase_corr(1,ia)).gt.1000) phase_corr(1,ia) = 0
     if (abs(phase_corr(2,ia)).gt.1000) phase_corr(2,ia) = 0
  enddo
  return
end subroutine set_corr
!
function h_offset(idump)
  use classic_api
  !---------------------------------------------------------------------
  ! gives the offset of the data header in the data section in words ,
  ! for dump idump (1 to r_ndump+2)
  !---------------------------------------------------------------------
  integer(kind=data_length) :: h_offset               !
  integer :: idump                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !-----------------------------------------------------------------------
  if (idump .le. r_ndump+1) then
    h_offset = (idump-1)*r_ldump
  elseif (idump .eq. r_ndump+2) then
    h_offset = (idump-1)*r_ldump + r_ldatl
  endif
  return
end function h_offset
!
function c_offset(idump)
  use classic_api
  !---------------------------------------------------------------------
  ! gives the offset of the continuum data in the data section in words ,
  ! for dump idump (1 to r_ndump+2)
  !---------------------------------------------------------------------
  integer(kind=data_length) :: c_offset               !
  integer :: idump                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !-----------------------------------------------------------------------
  if (idump .le. r_ndump+1) then
    c_offset = (idump-1)*r_ldump + r_ldpar
  elseif (idump .eq. r_ndump+2) then
    c_offset = (idump-1)*r_ldump + r_ldatl + r_ldpar
  endif
  return
end function c_offset
!
function l_offset(idump)
  use classic_api
  !---------------------------------------------------------------------
  ! gives the offset of the data header in the data section in words ,
  ! for dump idump (1 to r_ndump+2)
  !---------------------------------------------------------------------
  integer(kind=data_length) :: l_offset               !
  integer :: idump                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !-----------------------------------------------------------------------
  if (idump .le. r_ndump+1) then
    l_offset = (r_ndump+1)*r_ldump
  elseif (idump .eq. r_ndump+2) then
    l_offset = (r_ndump+2)*r_ldump + r_ldatl
  endif
  return
end function l_offset
!
real function my_total(dopolar,ia)
  use classic_api
  !---------------------------------------------------------------------
  ! average total powers according to dopolar (1,2,3=average)
  !---------------------------------------------------------------------
  integer :: dopolar                !
  integer :: ia                     !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  real :: x
  integer :: k
  !
  x = 0.
  if (dopolar.gt.0 .and. dopolar.le. r_npol_rec) then
    x = dh_total(dopolar,ia)
  elseif (dopolar .gt. r_npol_rec) then
    x = 0.
    do k=1, r_npol_rec
      x = x+ dh_total(k,ia)
    enddo
    x = x/r_npol_rec
  endif
  my_total = x
end function my_total
!
real function my_total2(jinput,ia,error)
  use classic_api
  !---------------------------------------------------------------------
  ! average total powers according to jinput (1,2,0=average)
  !---------------------------------------------------------------------
  integer :: jinput                 !
  integer :: ia                     !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  real :: x
  integer :: k, ninput
  !
  error = .false.
  ninput = r_nbb
  if (error) return
  if (jinput.gt.0 .and. jinput.le. ninput) then
    x = dh_total(jinput,ia)
  elseif (jinput .eq.0) then
    x = 0.
    do k=1, ninput
      x = x+ dh_total(k,ia)
    enddo
    x = x/ninput
  else
    x = dh_total(jinput,ia)
  endif
  my_total2 = x
end function my_total2
!
subroutine jmask(nch,nmask,mask,jok)
  use gildas_def
  !---------------------------------------------------------------------
  ! CLIC: compute mask from BLANK command
  !---------------------------------------------------------------------
  integer :: nch                    !
  integer :: nmask                  !
  integer :: mask(2,nmask)          !
  integer :: jok(nch)               !
  ! Local
  integer :: i,j
  !
  if (nmask.eq.0) return
  do i=1, nmask
    do j=mask(1,i),mask(2,i)
      jok(j) = 0
    enddo
  enddo 
  return
end subroutine jmask
