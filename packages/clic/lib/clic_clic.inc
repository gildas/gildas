!------------------------------------------------------------------------
! CLIC.INC
!-----------------------------------------------------------------------
!
      CHARACTER*12 SLINE                 ! Line Name
      CHARACTER*12 SSOURC                ! Source Name
      CHARACTER*12 STELES                ! Telescope Name
      CHARACTER*12 DEFEXT(5)             ! Default Extensions
      CHARACTER*12 AtmModel              ! ATM Model used (1985|2003)
      CHARACTER*1  SANGLE                ! Angle unit
      CHARACTER*1  SHEADE                ! Header type
      CHARACTER*2  SALIG0                ! Alignment Patch
      REAL*4 SOFFS1,SOFFS2               ! Offset Min Max (E-W)
      REAL*4 SOFFL1,SOFFL2               ! Offset Min Max (N-S)
      REAL*4 STOLE                       ! Tolerance on position checks
      REAL*8 FANGLE                      ! Angle conversion factor
      INTEGER*4 SNUME1, SNUME2           ! Observation Numbers
      INTEGER*4 SKIND                    ! 0 Line, 1 Continuum ...
      INTEGER*4 SOBSE1,SOBSE2            ! Observing dates
      INTEGER*4 SREDU1,SREDU2            ! Reduction dates
      INTEGER*4 SCOORD                   ! Coordinate Type
      INTEGER*4 SVELOC                   ! Velocity Type
      INTEGER*4 SQUAL                    ! Quality of data
      INTEGER*4 SPROC                    ! Observing procedure ...
      INTEGER*4 SRECEI                   ! Receiver number
!
      INTEGER SAPC, SBPC, SETIC, SITYPE, NDEFEXT
      CHARACTER*8 SPROJECT
      INTEGER ANT_MASK(MNANT), BAS_MASK(MNBAS)
      LOGICAL SAN_MASK(2*MBANDS,MNANT), SBA_MASK(2*MBANDS,MNBAS)
      INTEGER ANT_KILL(MNANT), BAS_KILL(MNBAS)
      LOGICAL SAN_KILL(2*MBANDS,MNANT), SBA_KILL(2*MBANDS,MNBAS)
      REAL*4 SHOURA1, SHOURA2
      REAL*4 SUT1, SUT2
      LOGICAL FIND_NEEDED,FIND_UPDATE
!
! Fitted Splines
      REAL*8 T_STEP                      ! time step for splines
      INTEGER M_SPL
      PARAMETER (M_SPL=20)
      INTEGER F_SPLINE (2,3,3,-MNANT:MNBAS,MNREC)! 0, 1 (int), 2 (ext)

! (amp/pha, U/L/DSB,HOR/VER/BOTH, baseline)
      INTEGER N_KNOTS(2,3,3,-MNANT:MNBAS,MNREC)  ! number of interior knots
      REAL*8 C_SPLINE(M_SPL+4,2,3,3,-MNANT:MNBAS,MNREC)! coefficients
      REAL*8 K_SPLINE(M_SPL+8,2,3,3,-MNANT:MNBAS,MNREC)! knots
      REAL*4 RMS_SPLINE_ANT(2,3,3,MNBAS,MNREC)
      REAL*4 RMS_SPLINE_BAS(2,3,3,MNBAS,MNREC)
!
! Fitted splines for atm monitor
      REAL*8 T_SPLMON                    ! time step for splines
      INTEGER F_SPLMON (MNANT)           ! 0, 1
      INTEGER N_SPLMON(MNANT)            ! number of interior knots
      REAL*8 C_SPLMON(M_SPL+4,MNANT)     ! coefficients
      REAL*8 K_SPLMON(M_SPL+8,MNANT)     ! knots
      REAL*4 RMS_SPLMON(MNANT)
!
! Fitted splines for atm WVR
      REAL*8 T_SPLWVR                    ! time step for splines
      INTEGER F_SPLWVR (MNANT)           ! 0, 1
      INTEGER N_SPLWVR(MNANT)            ! number of interior knots
      REAL*8 C_SPLWVR(M_SPL+4,MNANT)     ! coefficients
      REAL*8 K_SPLWVR(M_SPL+8,MNANT)     ! knots
      REAL*4 RMS_SPLWVR(MNANT)
!
! Fitted polynomials
      INTEGER M_POL
      PARAMETER (M_POL=M_SPL+7)
      INTEGER F_POL(2,3,3,-MNANT:MNBAS,MNREC)    ! 0, 1 (int), 2 (ext)
      INTEGER N_POL(2,3,3,-MNANT:MNBAS,MNREC)    ! polyn degree +1
      REAL*8 C_POL(M_POL,2,3,3,-MNANT:MNBAS,MNREC)     ! coefficients
      REAL*8 T_POL(2,2,3,3,-MNANT:MNBAS,MNREC)   ! time range limits
      REAL*4 RMS_POL_ANT(2,3,3,MNBAS,MNREC)
      REAL*4 RMS_POL_BAS(2,3,3,MNBAS,MNREC)
      REAL*4 PH_FAC(3,3,MNBAS,MNREC)       ! the freq in MHz,
! used to convert one rec to other,
      COMPLEX ICSBA(2,-MNANT:MNBAS,MNREC)! side-band averages (cont)
!
! Polynomials for atmospheric monitor
      INTEGER F_POLMON(MNANT)            ! 0, 1 (int), 2 (ext)
      INTEGER N_POLMON(MNANT)            ! polyn degree +1
      REAL*8 C_POLMON(M_POL,MNANT)       ! coefficients
      REAL*8 T_POLMON(2,MNANT)           ! time range limits
      REAL*4 RMS_POLMON(MNBAS)
!
! Polynomials for atm WVR 
      INTEGER F_POLWVR(MNANT)            ! 1, 2, 3, ...
      INTEGER N_POLWVR(MNANT)            ! polyn degree +1
      REAL*8 C_POLWVR(M_POL,MNANT,MWVRCH)! coefficients
      REAL*8 T_POLWVR(2,MNANT)           ! time range limits
      REAL*4 RMS_POLWVR(MNBAS,MWVRCH)
!
! Fitted spectral indexes
      INTEGER M_SPIDX, N_SPIDX           ! number of sources
      PARAMETER (M_SPIDX=400)
      CHARACTER*12 C_SPIDX(M_SPIDX)      ! source name
      LOGICAL F_SPIDX(M_SPIDX)           ! fixed or not
      REAL*4 SPIDX(M_SPIDX)              ! value (Jy)
      REAL*4 JY_TO_KEL_IF(MNIF,MNANT)     
      REAL*4 FREQ_SPIDX(M_SPIDX)         ! frequency of spidx measurements. (MHz)
      INTEGER DATE_SPIDX(M_SPIDX)        ! date of flux measurements. CLASS ft.
!
! Fitted fluxes
      INTEGER M_FLUX, N_FLUX             ! number of sources
      PARAMETER (M_FLUX=400)
      CHARACTER*12 C_FLUX(M_FLUX)        ! source name
      LOGICAL F_FLUX(M_FLUX)             ! fixed or not
      REAL*4 FLUX(M_FLUX)                ! value (Jy)
      REAL*4 JY_TO_KEL(MNANT)
      REAL*4 FREQ_FLUX(M_FLUX)           ! frequency of flux measurements. (MHz)
      INTEGER DATE_FLUX(M_FLUX)          ! date of flux measurements. CLASS ft.
!
! Reference antenna
      INTEGER REF_ANT
!
! Bandpass calibration:
      COMPLEX SBA(2,-MNANT:MNBAS,MNREC)  ! side-band averages (cont)
      LOGICAL BP_SPECTRUM_READY          ! Spectrum in memory
      LOGICAL BP_SPECTRUM_MEMORY         ! and in bandpass
      LOGICAL BP_SPECTRUM_MEMORY_POL(MNPOLSCAN)
      INTEGER BP_MEMORY_ORIGIN_POL(MNPOLSCAN)
      INTEGER BP_MEMORY_ORIGIN           ! How it was computed
      LOGICAL BP_MEMORY_ANTENNA          ! Antenna or baseline
      INTEGER BP_MEMORY_SPEC             ! From spectrum
      INTEGER BP_MEMORY_FREQ             ! From polynom fit
      INTEGER BP_MEMORY_FILE             ! Read from file
      PARAMETER (BP_MEMORY_SPEC = -1)
      PARAMETER (BP_MEMORY_FREQ = -2)
      PARAMETER (BP_MEMORY_FILE = -3)
! Fitted Baselines
      LOGICAL F_BFIT(MNBAS)              ! something fitted
      REAL*4 B_RMS(MNBAS)
      REAL*4 B_ERR(3,MNBAS)
      REAL*8 B_FIT(8,MNBAS)              ! baseline fitted 1:4 = (m,m,m,rad)
      LOGICAL F_AFIT(MNANT)              ! something fitted
      REAL*4 A_RMS(MNBAS)
      REAL*4 A_ERR(3,MNBAS)
! baseline fitted 1:4 = (m,m,m,rad,rad/h)
      REAL*8 A_FIT(8,MNANT)
! + pol degree 4 max
      INTEGER IDEG
! + elevation fil
      INTEGER IELEV
      LOGICAL SINELE
! Fitted delays (by baseband or IF)
      LOGICAL F_CROSSED
      LOGICAL F_REFERENCE
      LOGICAL F_DELAYBASE
      LOGICAL F_BBDELAYFIT(MNANT,MNBB)
      LOGICAL F_BBDELAYOFF(MNANT,MNBB)     ! by physical antenna, for OBS
      REAL*4 BBDELAYFIT (MNANT,MNBB)
      REAL*4 BBDELAYOFF (MNANT,MNBB)       ! by physical antenna, for OBS
      REAL*4 BBDELAYRMS (MNANT,MNBB)
      REAL*4 BBDELAYFITBASE (MNBAS,MNBB)
      LOGICAL F_IFDELAYFIT(MNANT,MNIF)
      LOGICAL F_IFDELAYOFF(MNANT,MNIF)     ! by physical antenna, for OBS
      REAL*4 IFDELAYFIT (MNANT,MNIF)
      REAL*4 IFDELAYOFF (MNANT,MNIF)       ! by physical antenna, for OBS
      REAL*4 IFDELAYRMS (MNANT,MNIF)
! Closure weights
      REAL*4 W_C(MNBAS)                  ! weights for closure phase
! Phase Atmospheric Emission correction (USB,LSB)
      REAL*4 PHASE_CORR(2,MNANT)         ! radian of phase per unit of tot power
! Atmospheric calibration
      CHARACTER*4 ATM_MODE(MNANT)
      REAL*4 ATM_WATER
      REAL*4 ATM_TREC(MNANT)
      REAL*4 ATM_GIM(MNANT)
      LOGICAL ATM_INTERPOLATE
! H|V phase difference
      REAL*4 ph_pol_saved(mnant)
!
! Weighting mode mode for TABLE
      LOGICAL WEIGHT_CAL, WEIGHT_TSYS, WEIGHT_PASS
! Writing policy
      CHARACTER*4 WRITE_MODE
! Reading data policy
      LOGICAL SKIP_FLAG
! Frequencies
      CHARACTER*12 C_LINE(2), L_LINE(MRLBAND,2)! continuum, 6 l. subb. U/LSB
      REAL*8 C_FREQ(2),L_FREQ(MRLBAND,2) ! continuum, 6 l. subb. U/LSB
! Selected output
      LOGICAL SELECT                     ! selection active
      LOGICAL CONT_SELECT                ! continuum output
      LOGICAL ALL_SELECT                 ! select all subbands.
      LOGICAL WIDEX_SELECT               ! select widex correlator
      INTEGER ISUBB_OUT(MBANDS)          ! subbands output
      INTEGER LSUBB_OUT                  ! # of subbands output
      INTEGER ISIDEB_OUT                 ! output sideband
      REAL FREQ_OUT                      ! output freq for U and V
      INTEGER NWIN_OUT                   ! number of windows
      INTEGER MWIN_OUT                   ! max. number of windows
      PARAMETER (MWIN_OUT=20)            !
      REAL WINDOW_OUT(2,MWIN_OUT)        ! windows for continuum output
! serting policy for listings
      LOGICAL DO_SORT_RECEIVER           ! separate receivers on listings
      LOGICAL DO_SORT_NUM                ! sort by obs num
!
! Calibration policy
      LOGICAL DO_PASS                    ! apply bandpass cal'n on plot
      LOGICAL DO_PASS_FREQ               ! RF PB cal'n is frequency  dep.
      LOGICAL DO_PASS_ANTENNA            ! RF PB cal'n is antenna based
      LOGICAL DO_PASS_MEMORY             ! RF PB cal'n is in memory/file
      LOGICAL DO_PASS_SPECTRUM           ! RF PB is a spectrum
      INTEGER BP_FILE_ID                 ! Solution number from file
      LOGICAL DO_AMPLITUDE               ! apply rel. ampl. cal'n on plot
      LOGICAL DO_AMPLITUDE_ANTENNA       ! AMP cal'n antenna based
      LOGICAL DO_PHASE                   ! apply rel. phase. cal on plot
      LOGICAL DO_PHASE_ANTENNA           ! PHASE cal'n antenna based
!     LOGICAL DO_PHASE_ATM               ! PHASE Corrected from atmospheric em.
      LOGICAL DO_PHASE_ATM(MNANT)        ! use PHASE correction done at Bure 
      LOGICAL DO_PHASE_WVR(MNANT)        ! PHASE Corrected from wvr
      LOGICAL DO_PHASE_WVR_E(MNANT)      ! PHASE Corrected from wvr, Empirical
      LOGICAL DO_PHASE_MON(MNANT)        ! PHASE Corrected from atmospheric emission 
      LOGICAL DO_SCALE                   ! scale by flux on plot
      LOGICAL DO_SIZE                    ! correct for calibrator size
      LOGICAL DO_FLUX                    ! go to janskys
      LOGICAL DO_SPEC                    ! do spectrum processing
      LOGICAL DO_RAW                     ! undo atmospheric calibration
      LOGICAL DO_BAR_ATM                 ! atm fluct. in ph. err bars.
      LOGICAL DO_PHASE_EXT               ! use external phase reference
      LOGICAL DO_PHASE_NOFILE            ! Do not Use File info to switchc
      LOGICAL DO_SPIDX                   ! Scale by spdix w.r.t. reference frequency
      LOGICAL DO_LEAKAGE                 ! Plot leakage
      LOGICAL DO_CORRECTED               ! Correct for leakage
! phase corr on/off
      LOGICAL SAVE_PASS                  ! apply bandpass cal. on plot
      LOGICAL SAVE_AMPLITUDE             ! apply rel. ampl. cal on plot
      LOGICAL SAVE_PHASE                 ! apply rel. phase. cal on plot
      LOGICAL SAVE_SCALE                 ! scale by flux on plot
      LOGICAL SAVE_FLUX                  ! go to janskys
      LOGICAL SAVE_SPEC                  ! do spectrum processing
      LOGICAL SAVE_RAW                   ! undo atmospheric calibration
! antenna gain solver old/new
      LOGICAL NEW_ANT_GAIN
! old/new receivers (winter 2006 change)
      LOGICAL NEW_RECEIVERS              ! fall 2006 change
      LOGICAL FAKE_NGRX                  ! to produce "new" data out of old
      REAL FAKE_FIF1
      REAL FAKE_FLO2(8)
      REAL FAKE_FLO2BIS(8)
      REAL FAKE_BAND2BIS(8)
      INTEGER FAKE_LPOLMODE(8)
      INTEGER FAKE_LPOLENTRY(MNANT,8)
      INTEGER DO_POLAR                   ! use no/hor/ver/both polar 
      LOGICAL PHYSICAL_POWER             ! plot raw/physical total power
      INTEGER NARROW_INPUT               ! narrow-band corr. input (for RF)
      INTEGER WIDEX_UNIT                 ! widex unit (for RF, atm.)
      LOGICAL ALLOW_POL                  ! temp fix to allow "expert" mode
      LOGICAL LOWRES                     ! low-resolution mode
      LOGICAL TABLE_PROTOTYPE            ! activate prototype mode
!
      INTEGER PLANET_MODEL               ! Type of model for Planet flux
!
      LOGICAL DO_WRITE_DATA              ! actually write data sections
!
      COMMON /SELECTION/ SELECT, CONT_SELECT,                           &
     &ISUBB_OUT, LSUBB_OUT, ISIDEB_OUT, NWIN_OUT, FREQ_OUT,             &
     &WINDOW_OUT, ALL_SELECT, WIDEX_SELECT
!
      COMMON /SASSET/                                                   &
     &C_FREQ, L_FREQ,                                                   &
     &SOFFS1,SOFFS2,SOFFL1,SOFFL2,                                      &
     &SNUME1,SNUME2,                                                    &
     &SOBSE1,SOBSE2,SREDU1,SREDU2,STOLE,                                &
     &SKIND,SCOORD,SVELOC,                                              &
     &FANGLE,SQUAL,                                                     &
     &SPROJECT, SAPC, SBPC, SETIC, SITYPE, SPROC, SRECEI,               &
     &SHOURA1, SHOURA2,                                                 &
     &SUT1, SUT2,                                                       &
     &REF_ANT,                                                          &
     &FIND_NEEDED,                                                      &
     &BAS_MASK, ANT_MASK, BAS_KILL, ANT_KILL,                           &
     &SBA_MASK, SAN_MASK, SAN_KILL, SBA_KILL,                           &
     &WEIGHT_CAL, WEIGHT_TSYS, WEIGHT_PASS,                             &
     &ATM_WATER, ATM_TREC, ATM_GIM,ATM_INTERPOLATE,ph_pol_saved,        &
     &DO_SORT_RECEIVER,DO_SORT_NUM,BP_MEMORY_ORIGIN,BP_MEMORY_ANTENNA,  &
     &BP_SPECTRUM_MEMORY_POL,BP_MEMORY_ORIGIN_POL,                      &
     &DO_PASS, DO_PASS_FREQ, DO_PASS_ANTENNA, BP_FILE_ID,DO_SPIDX,      &
     &DO_PASS_MEMORY,DO_PASS_SPECTRUM,DO_AMPLITUDE, DO_PHASE, DO_SCALE, &
     &DO_SIZE ,DO_PHASE_ANTENNA, DO_AMPLITUDE_ANTENNA,                  &
     &DO_PHASE_ATM, DO_PHASE_WVR, DO_PHASE_WVR_E, DO_PHASE_MON,         &
     &DO_PHASE_EXT, DO_PHASE_NOFILE,DO_POLAR,PHYSICAL_POWER,            &
     &NARROW_INPUT, WIDEX_UNIT,                                         &
     &DO_SPEC, DO_FLUX, DO_RAW, DO_BAR_ATM,                             &
     &SAVE_PASS, SAVE_AMPLITUDE, SAVE_PHASE, SAVE_SCALE,                &
     &SAVE_SPEC, SAVE_FLUX, SAVE_RAW, FIND_UPDATE,                      &
     &PLANET_MODEL, PHASE_CORR,                                         &
     &DO_WRITE_DATA, NDEFEXT,NEW_ANT_GAIN,NEW_RECEIVERS,                &
     &FAKE_NGRX,FAKE_FLO2,FAKE_FLO2BIS,FAKE_BAND2BIS,FAKE_FIF1,         &
     &FAKE_LPOLMODE,FAKE_LPOLENTRY,ALLOW_POL,LOWRES,                    &
     &TABLE_PROTOTYPE,DO_LEAKAGE,DO_CORRECTED
!
      COMMON /SASCET/ SLINE,SSOURC,STELES,                              &
     &SANGLE,SHEADE,SALIG0,                                             &
     &DEFEXT,                                                           &
     &ATM_MODE, AtmModel,                                               &
     &WRITE_MODE,SKIP_FLAG,                                             &
     &C_LINE, L_LINE
!
      COMMON /FITS/                                                     &
     &C_POLMON, T_POLMON, T_SPLMON, C_SPLMON, K_SPLMON,                 &
     &C_POLWVR, T_POLWVR, T_SPLWVR, C_SPLWVR, K_SPLWVR,                 &
     &SBA, A_FIT, B_FIT,                                                &
     &T_STEP, N_KNOTS, F_SPLINE, C_SPLINE, K_SPLINE,                    &
     &RMS_SPLINE_ANT,RMS_SPLINE_BAS,                                    &
     &F_POL, N_POL, C_POL, T_POL,RMS_POL_ANT,RMS_POL_BAS,               &
     &PH_FAC,                                                           &
     &F_BFIT, B_RMS, B_ERR, W_C,                                        &
     &F_AFIT, A_RMS, A_ERR, IDEG, IELEV, SINELE,                        &
     &F_BBDELAYFIT, BBDELAYFIT, BBDELAYRMS,BBDELAYFITBASE,F_DELAYBASE,  &
     &F_IFDELAYFIT, IFDELAYFIT, IFDELAYRMS,                             &
     &N_FLUX, C_FLUX, FLUX, F_FLUX, JY_TO_KEL, ICSBA,                   &
     &N_SPIDX, C_SPIDX, SPIDX, F_SPIDX, JY_TO_KEL_IF,                   &
     &F_SPLMON,N_SPLMON,RMS_SPLMON,F_POLMON,N_POLMON,RMS_POLMON,        &
     &F_SPLWVR,N_SPLWVR,RMS_SPLWVR,F_POLWVR,N_POLWVR,RMS_POLWVR,        &
     &FREQ_FLUX, DATE_FLUX, BBDELAYOFF, F_BBDELAYOFF,                   &
     &FREQ_SPIDX, DATE_SPIDX,                                           &
     &IFDELAYOFF, F_IFDELAYOFF, BP_SPECTRUM_READY,BP_SPECTRUM_MEMORY,   &
     &F_REFERENCE, F_CROSSED         
!
      SAVE /SELECTION/, /SASSET/, /SASCET/, /FITS/
