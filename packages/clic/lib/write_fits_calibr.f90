subroutine write_fits_calibr(unit, nsubant, subant, nocal,   &
    simpolar, error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Write the ARRAY_GEOMETRY  fits table.
  ! assumes NO_POL=1
  !---------------------------------------------------------------------
  integer :: unit                   !
  integer :: nsubant                !
  integer :: subant(nsubant)        ! selected antennas in table
  logical :: nocal                  !
  logical :: simpolar               !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: naxis, naxes(4), mf, status, nrows, tfields, varidat
  integer :: i, j, r_npol, r_nfeed, r_nside, mdigit2, mdigit1, isub
  parameter(r_npol = 1, r_nside=2, mdigit2=13, mdigit1=8)
  real :: rr
  parameter (mf = 40)
  character(len=16) :: ttype(mf),tform(mf),tunit(mf), extname
  logical :: ltest
  data ttype/   &
    'ANTENNID',   'STATIOID',   'STABXYZ',  'STAXOF',   &
    'POLTY',     'POLA',   &
    'APEREFF',  'BEAMEFF',  'ETAFSS',   'ANTGAIN',   &
    'HUMIDITY', 'TAMBIENT', 'PRESSURE', 'THOT',     'TCOLD',   &
    'PHOT',     'PCOLD',    'PSKY',     'GAINIMAG', 'TRX',   &
    'TSYS',     'TSYSIMAG', 'TAU',      'TAUIMAG',  'TCABIN',   &
    'TDEWAR',   'PREWATER', 'PREWATRD', 'ETAFSRD',  'THOTRD',   &
    'TCOLDRD',  'PHOTRD',   'PCOLDRD',  'PSKYRD',   'TSYSRD',   &
    'TRXRD',    'GAINIMRD', 'PATHRD',   'DPATHRD',  'VALIDRD'/
  data tform/   &
    '1J',       '1J',      '3D',       '1E',   &
    '1A',       '1E',   &
    '*E',      '*E',       '*E',      '*E',   &
    '1E',       '1E',      '1E',       '1E',      '1E',   &
    '*E',       '*E',      '*E',       '*E',      '*E',   &
    '*E',       '*E',      '*E',       '*E',      '1E',   &
    '1E',       '1E',      '1E',       '1E',      '1E',   &
    '1E',       '1E',      '1E',       '1E',      '1E',   &
    '1E',       '1E',      '1E',       '1E',      '1L'/
  data tunit/   &
    ' ',        ' ',       'm',        'm',   &
    ' ', 'degr',   &
    ' ',        ' ',        ' ',    'K/Jy',   &
    ' ',        'K',       'Pa',      'L',        'K',   &
    'adu',        'adu',        'adu',        ' ',        'K',   &
    'K',        'K',        ' ',        ' ',        'K',   &
    'K',        'm',        'm',        ' ',        'K',   &
    'K',        'adu',      'adu',      'adu',      'K',   &
    'K',        ' ',        'm',        'm',        ' '/
  !---------------------------------------------------------------------
  r_nfeed = 1
  if (simpolar) then
    r_nfeed = 1
  endif
  !
  !  Append/create a new empty HDU onto the end of the file and move to it.
  status = 0
  call ftcrhd(unit,status)
  if (status .gt. 0) goto 99
  !
  !  Define parameters for the binary table (see the above data statements)
  !      NROWS = R_NANT
  nrows = nsubant
  tfields = mf
  extname = 'CALIBR-ALMATI'
  varidat = 0
  ! columns in datapar
  call f_tform(tform(5),r_nfeed,'A')
  call f_tform(tform(6),r_nfeed,'E')
  do i=7, 10
    call f_tform(tform(i),r_nfeed*r_lband,'E')
  enddo
  do i=14, 24
    call f_tform(tform(i),r_nfeed*r_lband,'E')
  enddo
  call ftphbn(unit,nrows,tfields,ttype,tform,tunit,   &
    extname,varidat,status)
  if (status .gt. 0) goto 99
  do i=7, 10
    naxis = 2
    naxes(1) = r_nfeed
    naxes(2) = r_lband
    call ftptdm(unit, i, naxis, naxes, status)
  enddo
  do i=14, 24
    naxis = 2
    naxes(1) = r_nfeed
    naxes(2) = r_lband
    call ftptdm(unit, i, naxis, naxes, status)
  enddo
  ! Other header keywords
  call ftpkys(unit,'TABLEREV','v1.0 2001-07-03',   &
    extname//' release',status)
  if (status .gt. 0) goto 99
  !      CALL FTPKYJ(UNIT,'NO_ANT',R_NANT,'Number of Antennas',STATUS)
  call ftpkyj(unit,'NO_ANT',nsubant,'Number of Antennas',status)
  if (status .gt. 0) goto 99
  !      CALL FTPKYJ(UNIT,'SCAN-NUM',R_SCAN,'Scan number',STATUS)
  !      IF (STATUS .GT. 0) GOTO 99
  !      CALL FTPKYJ(UNIT,'OBS-NUM',R_NUM,'Observation number',STATUS)
  !      IF (STATUS .GT. 0) GOTO 99
  call write_fits_dobs(unit,error)
  if (error) return
  call ftpkyj(unit,'NO_BAND',r_lband,'Number of basebands',status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'NO_POL',r_npol,'Number of polar. products',   &
    status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'NO_FEED',r_nfeed,'Number of feeds.',status)
  if (status .gt. 0) goto 99
  ! Radiometer
  call ftpkye(unit,'FREQUSRD',r_frs_mon*1e6,mdigit1,   &
    'Radiometer Signal Frequency',status)
  if (status .gt. 0) goto 99
  call ftpkye(unit,'FREQUSRI',r_fri_mon*1e6,mdigit1,   &
    'Radiometer Image  Frequency',status)
  if (status .gt. 0) goto 99
  !
  status = 0
  !
  ! Fill in the columns.
  !
  !      DO I=1, R_NANT
  do isub = 1, nsubant
    i = subant(isub)
    ! ANNAME
    call ftpclj(unit,1,i,1,1,r_kant(i),status)
    if (status .gt. 0) goto 99
    ! NOSTA
    call ftpclj(unit,2,i,1,1,r_istat(i),status)
    if (status .gt. 0) goto 99
    ! STABXYZ
    call ftpcld(unit,3,i,1,3,r_ant(1,i),status)
    if (status .gt. 0) goto 99
    ! STAXOF
    call ftpcle(unit,4,i,1,1,r_axes(i),status)
    if (status .gt. 0) goto 99
    ! POLTY
    do j=1, r_nfeed
      call ftpcls(unit,5,i,j,1,'X',status)
    enddo
    if (status .gt. 0) goto 99
    ! POLA (at Bure, may be 0 or 90, in fact, diff with 3mm/1mm).
    call ftpcle(unit,6,i,1,1,0.,status)
    if (r_nfeed.gt.1) then
      call ftpcle(unit,6,i,2,1,90.,status)
    endif
    if (status .gt. 0) goto 99
    ! APEREFF
    do j=1, r_lband*r_nfeed
      rr = 2*1.38e3/(pi*15**2/4)/r_jykel(1,i)
      call ftpcle(unit,7,i,j,1,rr,status)
      if (status .gt. 0) goto 99
      ! BEAMEFF
      call ftpcle(unit,8,i,j,1,r_beef(1,i),status)
      if (status .gt. 0) goto 99
      ! ETAFSS
      call ftpcle(unit,9,i,j,1,r_feff(1,i),status)
      if (status .gt. 0) goto 99
      ! ANTGAIN
      call ftpcle(unit,10,i,j,1,r_jykel(1,i),status)
      if (status .gt. 0) goto 99
    enddo
    ! HUMIDITY
    call ftpcle(unit,11,i,1,1,r_humid,status)
    if (status .gt. 0) goto 99
    ! TAMBIENT
    call ftpcle(unit,12,i,1,1,r_tamb,status)
    if (status .gt. 0) goto 99
    ! PRESSURE in Pa
    call ftpcle(unit,13,i,1,1,r_pamb*100.,status)
    if (status .gt. 0) goto 99
    ! THOT
    do j=1, r_lband*r_nfeed
      call ftpcle(unit,14,i,j,1,r_tchop(1,i),status)
      if (status .gt. 0) goto 99
      ! TCOLD
      call ftpcle(unit,15,i,j,1,r_tcold(1,i),status)
      if (status .gt. 0) goto 99
      ! PHOT
      call ftpcle(unit,16,i,j,1,r_cchop(1,i),status)
      if (status .gt. 0) goto 99
      ! PCOLD
      call ftpcle(unit,17,i,j,1,r_ccold(1,i),status)
      if (status .gt. 0) goto 99
      ! PSKY
      call ftpcle(unit,18,i,j,1,r_csky(1,i),status)
      if (status .gt. 0) goto 99
      ! GAINIMAG
      call ftpcle(unit,19,i,j,1,r_gim(1,i),status)
      if (status .gt. 0) goto 99
      ! TRX
      call ftpcle(unit,20,i,j,1,r_trec(1,i),status)
      if (status .gt. 0) goto 99
      if (nocal) then
        call ftpcle(unit,21,i,j,1,0.0,status)
        if (status .gt. 0) goto 99
        call ftpcle(unit,22,i,j,1,0.0,status)
        if (status .gt. 0) goto 99
        call ftpcle(unit,23,i,j,1,0.0,status)
        if (status .gt. 0) goto 99
        call ftpcle(unit,24,i,j,1,0.0,status)
        if (status .gt. 0) goto 99
      else
        ! TSYS
        call ftpcle(unit,21,i,j,1,r_tsyss(1,i),status)
        if (status .gt. 0) goto 99
        ! TSYSIMAG
        call ftpcle(unit,22,i,j,1,r_tsysi(1,i),status)
        if (status .gt. 0) goto 99
        ! TAU
        call ftpcle(unit,23,i,j,1,r_taus(1,i),status)
        if (status .gt. 0) goto 99
        ! TAUIMAG
        call ftpcle(unit,24,i,j,1,r_taui(1,i),status)
        if (status .gt. 0) goto 99
      endif
    enddo
    ! TCABIN
    call ftpcle(unit,25,i,1,1,r_tcabin(i),status)
    if (status .gt. 0) goto 99
    ! TDEWAR
    call ftpcle(unit,26,i,1,1,r_tdewar(1,i),status)
    if (status .gt. 0) goto 99
    ! PREWATER
    if (nocal) then
      call ftpcle(unit,27,i,1,1,0.0,status)
      if (status .gt. 0) goto 99
      ! PREWATRD
      call ftpcle(unit,28,i,1,1,0.0,status)
      if (status .gt. 0) goto 99
    else
      ! PREWATER
      call ftpcle(unit,27,i,1,1,r_h2omm(1,i)/1e3,status)
      if (status .gt. 0) goto 99
      ! PREWATRD
      call ftpcle(unit,28,i,1,1,r_h2o_mon(i)/1e3,status)
      if (status .gt. 0) goto 99
    endif
    ! ETAFSRD
    call ftpcle(unit,29,i,1,1,r_feff_mon(i),status)
    if (status .gt. 0) goto 99
    ! THOTRD
    call ftpcle(unit,30,i,1,1,r_tchop_mon(i),status)
    if (status .gt. 0) goto 99
    ! TCOLDRD
    call ftpcle(unit,31,i,1,1,r_tcold_mon(i),status)
    if (status .gt. 0) goto 99
    ! PHOTRD
    call ftpcle(unit,32,i,1,1,r_cchop_mon(i),status)
    if (status .gt. 0) goto 99
    ! PCOLDRD
    call ftpcle(unit,33,i,1,1,r_ccold_mon(i),status)
    if (status .gt. 0) goto 99
    ! PSKYRD
    call ftpcle(unit,34,i,1,1,r_csky_mon(i),status)
    if (status .gt. 0) goto 99
    ! TSYSRD
    if (nocal) then
      call ftpcle(unit,35,i,1,1,0.0,status)
      if (status .gt. 0) goto 99
    else
      call ftpcle(unit,35,i,1,1,r_tsys_mon(i),status)
      if (status .gt. 0) goto 99
    endif
    ! TRXRD
    call ftpcle(unit,36,i,1,1,r_trec_mon(i),status)
    if (status .gt. 0) goto 99
    ! GAINIMRD
    call ftpcle(unit,37,i,1,1,r_gim_mon(i),status)
    if (status .gt. 0) goto 99
    !
    if (nocal) then
      call ftpcle(unit,38,i,1,1,0.0,status)
      if (status .gt. 0) goto 99
      call ftpcle(unit,39,i,1,1,0.0,status)
      if (status .gt. 0) goto 99
    else
      ! PATHRD
      call ftpcle(unit,38,i,1,1,r_path_mon(i)/1e3,status)
      if (status .gt. 0) goto 99
      ! DPATHRD
      call ftpcle(unit,39,i,1,1,r_dpath_mon(i)/1e3,status)
      if (status .gt. 0) goto 99
    endif
    ! VALIDRD
    if (r_ok_mon(i)) ltest = .true.
    call ftpcll(unit,40,i,1,1,ltest,status)
    if (status .gt. 0) goto 99
  enddo
  return
  !
99 call printerror('WRITE_FITS_CALIBR',     status)
  error = .true.
end subroutine write_fits_calibr
