!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CLIC as a master
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program clic
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Main clic program
  !----------------------------------------------------------------------
  external :: clic_pack_set
  !
  call gmaster_set_prompt('CLIC')
  call gmaster_run(clic_pack_set)
  !
end program clic
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
