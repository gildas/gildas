program datatype
  !
  logical :: header_data, corr_data, auto_data, ante_data, base_data, angle_data
  integer :: i
  character*1 :: h,c,a,an,b,ang
  ! 
  include 'clic_xy_code.inc'
  write (6,'(a)') '  i  SET Y         Label                | H | C | A | An| Ba| A |'
  write (6,'(a)') '-----------------------------------------------------------------'
  do i=1, mxy
    h =' '
    c =' '
    a =' ' 
    an=' '
    b= ' '
    ang=' '
    if (header_data(i))   h = 'X'
    if (corr_data(i,i))   c = 'X'
    if (auto_data(i,i))   a = 'X'
    if (ante_data(i))     an ='X'
    if (base_data(i))     b = 'X'
    if (angle_data(i))    ang = 'X'
    write(6,'(i3,2x,a,2x,a,a,a,a,a,a,a,a,a,a,a,a,a,a)') i,vxy(i),clab(i), &
                   ' | ',h,' | ',c,' | ',a,' | ',an,' | ',b,' | ',ang,' |'
  enddo

end program 
