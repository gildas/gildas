import xml.dom.minidom
import sys
import xml.parsers.expat
import string
import os
##import myUtil
import traceback

__version__ = "2019-04-03"
__author__ = "as"

dbgLevel = 0
##dbg = myUtil.MyDebug(level = dbgLevel, name = "mySmallXml")
msgName = ""
msgAttributes = {}

def start_element(name, attrs):
    """Process start elements of messages."""
    global msgName, msgAttributes
    ##dbg.out ("#. name, attrs:", name, attrs)
    msgName = name
    msgAttributes.update(attrs)

def attrsToDict(attrs):
    """Return xml attributes as dictionary."""
    dict = {}
    for attr in list(attrs.keys()):
        dict[attr] = attrs.getValue(attr)
    return dict
    
def getAllAttributes(line):
    """Get all attributes of xml line."""
    
    global msgData, msgAttributes

    ##dbg.out("#. getAllAttrs:", line)
    msgAttributes = {}
    p = xml.parsers.expat.ParserCreate()
    p.StartElementHandler = start_element
    try:
        p.Parse(line)
#        print 'msgAttributes ',msgAttributes
        return msgAttributes
    except:
        ##dbg.out("#! parse error", line, level = 0)
        return {}
    

if __name__=="__main__":

    msgName = ""

    while 1:
        line = sys.stdin.readline()
        print("#. line: ", line)
        line = line [string.find(line,"<"):]
        attrs = getAllAttrs(line)
        print("#. attrs:", attrs)
