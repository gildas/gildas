!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage PAKO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module pako_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: pako_message_id = gpack_global_id  ! Default value for startup message
  !
end module pako_message_private
!
subroutine pako_message_set_id(id)
  use pako_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  pako_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',pako_message_id
  call pako_message(seve%d,'pako_message_set_id',mess)
  !
end subroutine pako_message_set_id
!
subroutine pako_message(mkind,procname,message)
  use pako_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(pako_message_id,mkind,procname,message)
  !
end subroutine pako_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
