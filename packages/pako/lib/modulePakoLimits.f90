!
!     Id: modulePakoLimits.f90,v 1.2.3 2014-05-20 Hans Ungerechts
!     Id: modulePakoLimits.f90,v 1.1.3 2010-08-04 Hans Ungerechts
!     
!----------------------------------------------------------------------
!     PAKO 
!
! <DOCUMENTATION name="moduleGlobalVariables">
!
! 
! 
!
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
! 
!
! 
!
! </DOCUMENTATION> <!-- name="moduleGlobalVariables" -->
!
Module modulePakoLimits
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  !
  Implicit None
  Save
  Private
  !
  Public :: checkNofParameters
  Public :: checkI4
  Public :: checkI4co
  Public :: checkR4, checkR4angle
  Public :: checkR8
  Public :: checkR4co
  Public :: checkR8co
  Public :: checkR8options
  !
  Type  ::  pakoLimitsGVType                                                     ! !! TBD 2014: values from global parameters
     !
     Character(len=24)           ::  privilege     =  "none"                     ! !! 24 = lenCh2
     !
     Character(len=24)           ::  userLevel     =  "none"
     Integer                     ::  iUserLevel    =   0
     !
     Character(len=24)           ::  limitCheck    =  "none"
     Integer                     ::  iLimitCheck   =   0
     !
  End type pakoLimitsGVType
  !
  Type (pakoLimitsGVType)  ::  pakoLimitsGV
  !
  Public :: pakoLimitsGVType, pakoLimitsGV
  !
  !
Contains
!!!
!!!
  Subroutine checkNofParameters(command,  &
    &     minNo,  &
    &     maxNo,  &
    &     nArguments,  &
    &     error)
    !
    Character   command*12
    Integer     minNo, maxNo
    Integer     nArguments
    Logical     error
    !
    Integer     SIC_NARG
    !
    Character  messageText*128
    !
    error = .False.
    !
    nArguments = SIC_NARG(0)
    !
    If (minNo.Eq.0 .And. maxNo.Eq.0 &
       &     .And. nArguments.Gt.maxNo) Then
       messageText = "no parameters allowed"
       Call PakoMessage(6,3,command,messageText)
       error = .True.
    Else If (nArguments.Lt.minNo) Then
       If (minNo.Eq.1) Then
          Write (messageText,*)  &
          &           "at least ","one"," parameter required"
       Else If (minNo.Ge.2) Then
          Write (messageText,*)  &
          &           "at least ",minNo," parameters required"
       End If
       Call PakoMessage(6,3,command,messageText)
       error = .True.
    Else If (nArguments.Gt.maxNo) Then
       If (maxNo.Eq.1) Then
          Write (messageText,*)  &
          &           "no more then ","one"," parameter allowed"
       Else If (maxNo.Ge.2) Then
          Write (messageText,*)  &
          &           "no more then ",maxNo," parameters allowed"
       End If
       Call PakoMessage(6,3,command,messageText)
       error = .True.
    End If
    !
    Return
  End Subroutine checkNofParameters
!!!
!!!
  Subroutine checkI4(command,vIn,  &
    &     variable,  &
    &     error)
    !
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Integer     vIn                                   
    Integer     variable(nDimValuesI)
    Integer     vStdMin, vStdMax, vLimitMin, vLimitMax
    Integer                       vXlimitMin, vXlimitMax
    Real                          vXlimitFactor
    !
    Include 'inc/ranges/checkLimits.inc'
    !
    Return
  End Subroutine checkI4
!!!
!!!
  Subroutine checkI4co(command,option,vIn,  &
    &     variable,  &
    &     error)
    !
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Integer     vIn                                   
    Integer     variable(nDimValuesI)
    Integer     vStdMin, vStdMax, vLimitMin, vLimitMax
    Integer                       vXlimitMin, vXlimitMax
    Real                          vXlimitFactor
    !
    Include 'inc/ranges/checkLimits.inc'
    !
    Return
  End Subroutine checkI4co
!!!
!!!
  Subroutine checkR4(command,vIn,  &
    &     variable,  &
    &     error)
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Real(Kind=4)      vIn                                   
    Real(Kind=4)      variable(nDimValuesR)
    Real(Kind=4)      vStdMin, vStdMax, vLimitMin, vLimitMax
    !
    !D    Write (6,*) "   --> SR checkR4: "
    !
    Include 'inc/ranges/checkLimitsNoOption.inc'
    !
    Return
  End Subroutine checkR4
!!!
!!!
  Subroutine checkR4angle(command,vIn,  &
    &     variable,  &
    &     angleFactor,   &
    &     error)
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Real(Kind=4)     angleFactor
    !
    Real(Kind=4)      vIn                                   
    Real(Kind=4)      variable(nDimValuesR)
    Real(Kind=4)      vStdMin, vStdMax, vLimitMin, vLimitMax
    !
    !D    Write (6,*) "   --> SR checkR4angle: "
    !D    Write (6,*) "          angleFactor = ", angleFactor
    !
    vIn = vIn*angleFactor
    !
    Include 'inc/ranges/checkLimitsAngle.inc'
    !
    !D    Write (6,*) "          angleFactor = ", angleFactor
    !
    Return
  End Subroutine checkR4angle
!!!
!!!
  Subroutine checkR8(command,vIn, &
    &     variable, &
    &     error)
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Real(Kind=8)        vIn                                   
    Real(Kind=8)        variable(nDimValuesR)
    Real(Kind=8)        vStdMin, vStdMax, vLimitMin, vLimitMax
    Real(Kind=8)                          vXlimitMin, vXlimitMax
    Real                                  vXlimitFactor
    !
    Include 'inc/ranges/checkLimits.inc'
    !
    Return
  End Subroutine checkR8
!!!
!!!
  Subroutine checkR4co(command,option,vIn,  &
    &     variable,  &
    &     error)
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Real(Kind=4)      vIn                                   
    Real(Kind=4)      variable(nDimValuesR)
    Real(Kind=4)      vStdMin, vStdMax, vLimitMin,  vLimitMax
    Real(Kind=4)                        vXlimitMin, vXlimitMax
    Real                                vXlimitFactor
    !
    !D     Write (6,*) "      --> checkR4co   "
    !D     Write (6,*) "          pakoLimitsGV%privilege:     ", pakoLimitsGV%privilege
    !
    Include 'inc/ranges/checkLimits.inc'
    !
    Return
  End Subroutine checkR4co
!!!
!!!
  Subroutine checkR8co(command,option,vIn,  &
    &     variable,  &
    &     error)
    !
    Include 'inc/ranges/headerForCheckLimits.inc'
    !
    Real(Kind=8)        vIn                                   
    Real(Kind=8)        variable(nDimValuesR)
    Real(Kind=8)        vStdMin, vStdMax, vLimitMin, vLimitMax
    Real(Kind=8)                          vXlimitMin, vXlimitMax
    Real                                  vXlimitFactor
    !
    Include 'inc/ranges/checkLimits.inc'
    !
    Return
  End Subroutine checkR8co
!!!
!!!
  Subroutine checkR8options(command,vIn,                                         &
       &               variable,                                                 &
       &               option,                                                   &
       &               description,                                              &
       &               formatNumber,                                             &
       &               iSeverityLimit,                                           &
       &               iSeverityStd,                                             &
       &               errorLimit,                                               &
       &               errorStd         )                                        !
    !
    Include 'inc/variables/headerForVariables.inc'
    !
    Character(len=*),  Intent(IN)             ::  command
    Real(Kind=8),      Intent(IN)             ::  vIn                                   
    Real(Kind=8),      Intent(IN)             ::  variable(nDimValuesR)
    Character(len=*),  Intent(IN)             ::  option
    Character(len=*),  Intent(IN)             ::  description
    Character(len=*),  Intent(IN)             ::  formatNumber
    Integer,           Intent(IN)             ::  iSeverityLimit
    Integer,           Intent(IN)             ::  iSeverityStd
    Logical,           Intent(OUT)            ::  errorLimit
    Logical,           Intent(OUT)            ::  errorStd
    !     
    ! TBD:     Character(len=*),  Intent(IN),  Optional  ::  option
    ! TBD:     Character(len=*),  Intent(IN),  Optional  ::  description
    ! TBD:     Character(len=*),  Intent(IN),  Optional  ::  formatNumber
    ! TBD:     Integer,           Intent(IN),  Optional  ::  iSeverityLimit
    ! TBD:     Integer,           Intent(IN),  Optional  ::  iSeverityStd
    ! TBD:     Logical,           Intent(OUT), Optional  ::  errorLimit
    ! TBD:     Logical,           Intent(OUT), Optional  ::  errorStd
    !     
    Character(len=128) messageText
    !
    Character(len=128) format
    !
    Real(Kind=8)        vStdMin, vStdMax, vLimitMin, vLimitMax
    !
    errorLimit = .False.
    errorStd   = .False.
    !
    !D     Write (6,*) "   --> checkR8options : "
    !D     !
    !D     Write (6,*) "       option :         ", option
    !D     Write (6,*) "       description :    ", description
    !D     Write (6,*) "       formatNumber :   ", formatNumber
    !D     Write (6,*) "       iSeverityLimit : ", iSeverityLimit
    !D     Write (6,*) "       iSeverityStd :   ", iSeverityStd
    !D     Write (6,*) "       errorLimit :     ", errorLimit
    !D     Write (6,*) "       errorStd :       ", errorStd
    !
    vStdMin   = Min(variable(iStd1),  variable(iStd2))
    vStdMax   = Max(variable(iStd1),  variable(iStd2))     
    vLimitMin = Min(variable(iLimit1),variable(iLimit2))
    vLimitMax = Max(variable(iLimit1),variable(iLimit2))
    !
    !D     write (6,*) " vIn         = ",vIn
    !D     write (6,*) " vStdMin     = ",vStdMin
    !D     write (6,*) " vStdMax     = ",vStdMax
    !D     write (6,*) " vLimitMin   = ",vLimitMin
    !D     write (6,*) " vLimitMax   = ",vLimitMax
    !
    format = "(A,"//formatNumber//",A,"//formatNumber//",A,"//formatNumber//")"
    !
    !
    If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
       errorLimit = .True.
       Write (messageText,format) 'value ', vIn,                                 &
            &        ' outside limits ', variable(iLimit1),                      &
            &        ' to ', variable(iLimit2)                                   !
       Call PakoMessage(iSeverityLimit,iSeverityLimit,command,                   &
            &          description//" "//messageText)                            !
       ! TBD:        Call PakoMessage(iSeverityLimit,iSeverityLimit,
       !               command//"/"//option,messageText)
    Else If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
       errorStd = .True.
       Write (messageText,format) 'value ', vIn,                                 &
            &           ' outside standard range ', variable(iStd1),             &
            &           ' to ', variable(iStd2)                                  !
       Call PakoMessage(iSeverityStd,iSeverityStd,command,                       &
            &           description//" "//messageText)                           !
       ! TBD:           Call PakoMessage(iSeverityStd,iSeverityStd,   
       !                command//"/"//option,messageText)
    Endif
    !
    Return
  End Subroutine checkR8options
!!!
End Module modulePakoLimits
