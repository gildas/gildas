!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoDisplay">
!   <VERSION>
!                Id: modulePakoDisplay.f90,v 1.2.3 2014-02-10 Hans Ungerechts
!                based on
!                Id: modulePakoDisplay.f90,v 0.9 2005-09-22 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                
!   </FAMILY>
!   <SIBLINGS>
!   </SIBLINGS>        
!
!   Pako module for command: DISPLAY
!
! </DOCUMENTATION> <!-- name="modulePakoDisplay" -->
!
!----------------------------------------------------------------------
!
! handle Display command.
!
! </DOCUMENTATION>
!
Module modulePakoDisplay
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  Use modulePakoCatalog
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  Use modulePakoSource
  Use modulePakoCalibrate
  Use modulePakoFocus
  Use modulePakoOnOff
  Use modulePakoOtfMap
  Use modulePakoPointing
  Use modulePakoTip
  Use modulePakoTrack
  Use modulePakoVlbi
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  !
  ! *** Subroutines
  !
  Public :: pakoDisplay
  !
!!!
!!!
Contains
!!!
!!!
  Subroutine pakoDisplay(programName,LINE,COMMAND,ERROR)
    !
    !**   arguments:   ***
    Character(len=lenPro),        Intent(in)    ::  programName
    Character(len=lenLineDouble), Intent(in)    ::  LINE
    Character(len=lenCo),         Intent(in)    ::  COMMAND
    Logical,                      Intent(out)   ::  ERROR
    !
    !**   specific local variables:    ***
    Character(len=lenCh)  :: whatToDo
    !
    !**   functions called:   ***
    Integer  ::  SIC_NARG
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Error = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Display, v 1.2.3 2014-02-10 ---> SR: Display ")             ! trace execution
    !
    !D     Write (6,*) "   --> pakoDisplay"
    !
    !**   read Parameters, Options   ***
    !
    Include 'inc/parameters/parametersDisplay.inc'
    !
    If (.Not. Error) Then
       !
       If (whatToDo.Eq."redo") Then
          !
          Call displaySource
          Call displayOffsets
          Call displaySet
          Call displayCatalog
          Call displayReceiver
          Call displayBackend
          !
          If      (GV%observingMode.Eq.OM%Cal) Then
             Call displayCalibrate
          Else If (GV%observingMode.Eq.OM%Focus) Then
             Call displayFocus
          Else If (GV%observingMode.Eq.OM%OnOff) Then
             Call displayOnOff
          Else If (GV%observingMode.Eq.OM%OtfMap) Then
             Call displayOtfMap
          Else If (GV%observingMode.Eq.OM%Pointing) Then
             Call displayPointing
          Else If (GV%observingMode.Eq.OM%Tip) Then
             Call displayTip
          Else If (GV%observingMode.Eq.OM%Track) Then
             Call displayTrack
          Else If (GV%observingMode.Eq.OM%VLBI) Then
             Call displayVLBI
          End If
          !
          If      (GV%switchingMode.Eq.swMode%Total) Then
             Call displaySwTotalPower
          Else If (GV%switchingMode.Eq.swMode%Beam) Then
             Call displaySwBeam
          Else If (GV%switchingMode.Eq.swMode%Freq) Then
             Call displaySwFrequency
          Else If (GV%switchingMode.Eq.swMode%Wobb) Then
             Call displaySwWobbler
          End If
          !
       End If
       !
    End If
    !
    Return
!
  End Subroutine pakoDisplay
!!!
!!!
End Module modulePakoDisplay



