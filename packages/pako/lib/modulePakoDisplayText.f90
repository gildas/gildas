!
!  Id: modulePakoDisplayText.f90,v 1.0.7 2007-08-14 Hans Ungerechts
!
Module modulePakoDisplayText
  !
  Implicit None
  Save
  Private
  !
  !     display* subroutines are public:
  Public :: displayTextSetOutputUnit
  Public :: displayTextC, displayTextClong
  Public :: displayTextI
  Public :: displayTextR, displayText2R
  Public :: displayTextL
  Public :: xmlOut, xmlOutBlank
  !
  ! *** Variables  for Display XML ***
  !
  Integer, Parameter :: unitDefault = 23
  Integer            :: outputUnitDisplay = unitDefault
  !
  !     length in characters of fields for different types
  Integer, Parameter :: lenName  = 24
  Integer, Parameter :: lenS     = 24
  Integer, Parameter :: lenSlong = 100
  Integer, Parameter :: lenI     = 12
  Integer, Parameter :: lenR     = 12
  Integer, Parameter :: lenL     = 12
  !
  Integer            :: l
!!!
Contains
!!!
!!!
  Subroutine displayTextSetOutputUnit(iUnit, error)
    Integer,  Intent(IN),  Optional :: iUnit
    Logical,  Intent(OUT), Optional :: error              ! errorFlag
    ! <DOCUMENTATION name="displyTextSetOutputUnit">
    ! set unit number for Display XML.
    ! all Display XML output will be written to the file attached to iUnit
    ! </DOCUMENTATION>
    !
    if (Present(error)) error = .False.
    !
    If (Present(iUnit)) Then
       outputUnitDisplay = iUnit
    Else
       outputUnitDisplay = unitDefault
    End If
    !
  End Subroutine displayTextSetOutputUnit
!!!
!!!
  Subroutine displayTextC                                          &
       &     (command, row, column, variableName, variableC,             &
       &      messageID, messageName, messageType, dataType)
    !
    !     display character variable in (short) length: lenS
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Character(len=*), Intent(in) :: variableC
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName) nameText 
    Character(len=lenS)    valueText
    !
    Integer :: len_trim
    !
    l = Min(lenName,Len_trim(variableName))
    nameText = variableName(1:l)
    !
    l = Min(lenS,Len_trim(variableC))
    valueText = variableC(1:l)
    !
    Call xmlOut(row,column   , nameText(1:Len_trim(nameText)) )
    Call xmlOut(row,column+22,valueText(1:Len_trim(valueText)))
    !
    Return
  End Subroutine displayTextC
!!!
  !D      Write (6,*) " --> moduleDisplayText: SR displayTextC "
  !D      l = len_trim(variableName)
  !D      Write (6,*) " variableName: ->",variableName(1:l),"<-"
  !D      l = len_trim(variableC)
  !D      Write (6,*) " variableC:    ->",variableC(1:l),"<-"
  !D!
  !D      l = Min(lenName,len_trim(variableName))
  !D      Write (nameText, '(a)') variableName(1:l)
  !D      Write (6,*) " nameText:    ->",nameText(1:l),"<-"
  !D      l = Min(lenS,len_trim(variableC))
  !D      Write (valueText,'(a)') variableC(1:l)
  !D      Write (6,*) " valueText:   ->",valueText(1:l),"<-"
!!!
  Subroutine displayTextClong                                      &
       &     (command, row, column, variableName, variableC,             &
       &      messageID, messageName, messageType, dataType)
    !
    !     display character variable in (long) length: lenSlong
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Character(len=*), Intent(in) :: variableC
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName)  nameText 
    Character(len=lenSlong) valueText
    !
    !D      write (6,*) '   moduleDisplayText: SR displayTextClong '
    !D      write (6,*) '      variableName ->',variableName,'<-'
    !D      write (6,*) '      variableC    ->',variableC,'<-'
    !
    l = Min(lenName,Len_trim(variableName))
    nameText  = variableName(1:l)
    !
    l = Min(lenSlong,Len_trim(variableC))
    valueText = variableC(1:l)
    !
    !D      write (6,*) '      nameText     ->',nameText(1:len_trim(nameText)),'<-'
    !D      write (6,*) '      valueText    ->',valueText(1:len_trim(valueText)),'<-'
    !
    Call xmlOut(row,  column, nameText(1:Len_trim(nameText)) )
    Call xmlOut(row,  column+15,valueText(1:Len_trim(valueText)))
    !
    Return
  End Subroutine displayTextClong
!!!
  Subroutine displayTextI                                          &
       &     (command, row, column, variableName, variableI,             &
       &      messageID, messageName, messageType, dataType)
    !
    !     display integer variable in length: lenI
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Integer         , Intent(in) :: variableI
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName) nameText 
    Character(len=lenI)    valueText
    !
    Write (nameText, '(a)'  ) variableName
    Write (valueText,'(i12)') variableI
    !
    Call xmlOut(row,column   ,variableName)
    Call xmlOut(row,column+22,valueText)
    !
    Return
  End Subroutine displayTextI
!!!
  Subroutine displayTextR                                          &
       &     (command, row, column, variableName, variableR,             &
       &      messageID, messageName, messageType, dataType)
    !
    !     display real variable in length: lenR
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Real            , Intent(in) :: variableR
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName) nameText 
    Character(len=lenR)    valueText
    !
    Write (nameText, '(a)'    ) variableName
    Write (valueText,'(f12.3)') variableR
    !
    Call xmlOut(row,column   ,variableName)
    Call xmlOut(row,column+22,valueText)
    !
    Return
  End Subroutine displayTextR
!!!
  Subroutine displayText2R                                         &
       &     (command, row, column, variableName,                        &
       &      variableR1, variableR2,                                    &
       &      messageID, messageName, messageType, dataType)
    !
    !     display 2 real variables in 2 * length: lenR
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Real            , Intent(in) :: variableR1, variableR2
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName) nameText 
    Character(len=lenR)    valueText
    !
    Write (nameText, '(a)'    ) variableName
    Write (valueText,'(f12.3)') variableR1
    !
    Call xmlOut(row,column   ,variableName)
    Call xmlOut(row,column+22,valueText)
    !
    Write (valueText,'(f12.3)') variableR2
    !
    Call xmlOut(row,column+22+14,valueText)
    !
    Return
  End Subroutine displayText2R
!!!
  Subroutine displayTextL                                          &
       &     (command, row, column, variableName, variableL,             &
       &      messageID, messageName, messageType, dataType)
    !
    !     display logical variable in length: lenL
    !
    Integer         , Intent(in) :: row, column
    Character(len=*), Intent(in) :: command, variableName
    Logical         , Intent(in) :: variableL
    !
    Character(len=*), Intent(in), Optional :: messageID
    Character(len=*), Intent(in), Optional :: messageName
    Character(len=*), Intent(in), Optional :: messageType
    Character(len=*), Intent(in), Optional :: dataType 
    !
    Character(len=lenName) nameText 
    Character(len=lenL)    valueText
    !
    Write (nameText,'(a)') variableName
    Write (valueText,*) variableL
    l = Len_trim(valueText)
    !
    Call xmlOut(row,column   ,variableName)
    Call xmlOut(row,column+32,valueText(1:l))
    !
    Return
  End Subroutine displayTextL
!!!
  Subroutine xmlOut(r, c, text, style)
    !
    Integer         , Intent(in) :: r, c
    Character(len=*), Intent(in) :: text
    !     
    Character(len=*), Intent(in), Optional :: style
    !     
    Integer  ::  lt, ls, ioerr
    !
    !D    Write (6,*) "   --> moduleDisplayText: xmlOut "
    !D    Write (6,*) "   text(1:len_trim(text)):  -->",text(1:len_trim(text)),"<--"
    !
    lt = Len_trim(text)
    !
    If (Present(style)) Then
       ls = len_trim(style)
       !D         Write (6,*) "      xmlOut: style = ->", style(1:ls), "<-"
       Write (outputUnitDisplay,'(1h ,a,i3,a,i3,a,a,a,a,a)', IOSTAT=ioerr)       &
            &      '<text r="',r,'" c="',c,'" text="', text(1:lt),               &
            &      '" bg="', style(1:ls), '"/>'                                  !
    Else
       !D         Write (6,*) "      xmlOut: no style "
       Write (outputUnitDisplay,'(1h ,a,i3,a,i3,a,a,a)',     IOSTAT=ioerr)      &
            &      '<text r="',r,'" c="',c,'" text="', text(1:lt), '"/>'        !
    End If
    !
    Return
  End Subroutine xmlOut
!!!
  Subroutine xmlOutBlank(r, c, text, style)
    !
    Integer         , Intent(in) :: r, c
    Character(len=*), Intent(in) :: text
    !     
    Character(len=*), Intent(in), Optional :: style
    !     
    Integer  ::  lt, ls, ioerr
    !
    !D    Write (6,*) "   --> moduleDisplayText: xmlOutBlank "
    !D    Write (6,*) "   text:                -->",text,"<--"
    !D    Write (6,*) "   text(1:len_trim(text)):  -->",text(1:len_trim(text)),"<--"
    !
    lt = Len(text)
    !
    If (Present(style)) Then
       ls = len_trim(style)
       !D         Write (6,*) "      xmlOutBlank: style = ->", style(1:ls), "<-"
       Write (outputUnitDisplay,'(1h ,a,i3,a,i3,a,a,a,a,a)', IOSTAT=ioerr)       &
            &      '<text r="',r,'" c="',c,'" text="', text(1:lt),               &
            &      '" bg="', style(1:ls), '"/>'                                  !
    Else
       !D         Write (6,*) "      xmlOutBlank: no style "
       Write (outputUnitDisplay,'(1h ,a,i3,a,i3,a,a,a)',     IOSTAT=ioerr)       &
            &      '<text r="',r,'" c="',c,'" text="', text(1:lt), '"/>'         !
    End If
    !
    Return
  End Subroutine xmlOutBlank
!!!
End Module modulePakoDisplayText
!!!
!D      write (6,*) " variableName: ->",variableName,"<-"
!D      write (6,*) " variableC:    ->",variableC,"<-"
!
!D      Write (6,*) " --> moduleDisplayText: SR displayTextC "
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) " --> moduleDisplayText: SR displayTextClong "
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) " --> moduleDisplayText: SR displayTextI "
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) " --> moduleDisplayText: SR displayTextR "
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) " --> moduleDisplayText: SR displayText2R"
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) " --> moduleDisplayText: SR displayTextL "
!D      Include 'displayOptionalArgumentsDebug.f90'
!D!
!D      Write (6,*) "   --> moduleDisplayText: SR xmlOut "
!D      Write (6,*) "     text ->",text(1:l),"<-"
!D!






