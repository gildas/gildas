!     
!  Id: modulePakoStart.f90,v 1.2.4 2015-06-08 Hans Ungerechts
!  Id: modulePakoStart.f90,v 1.2.4 2015-06-01 Hans Ungerechts
!  Id: modulePakoStart.f90,v 1.2.3 2014-10-23 Hans Ungerechts
!  Id: modulePakoStart.f90,v 1.2.3 2014-10-17 Hans Ungerechts
!  Id: modulePakoStart.f90,v 1.2.3 2014-05-13 Hans Ungerechts
!                                  patch for check # of backends
!                                  2014-04-29 Hans Ungerechts
!                                  comment debug write(6,*)
!  Id: modulePakoStart.f90,v 1.2.2 2013-02-06 Hans Ungerechts
!  Id: modulePakoStart.f90,v 1.2.1 2012-08-20 Hans Ungerechts
!    
! <DOCUMENTATION name="moduleStart">
!
! handle Start command.
!
! </DOCUMENTATION>
!
! <DEV> 
! TBD: - more documentation
! TBD: put file backup mechanism in utility SR
! TBD: put file operations like cp in utility SR
! <\DEV> 
!
Module modulePakoStart
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  Use modulePakoReceiver
!!$  Use modulePakoReceiver, Only: listRX, varReceiver, queryReceiver
  Use modulePakoCalibrate
  Use modulePakoFocus
  Use modulePakoOnOff
  Use modulePakoLissajous
  Use modulePakoOtfMap
  Use modulePakoPointing
  Use modulePakoDIYlist
  Use modulePakoSubscanList
  Use modulePakoTip
  Use modulePakoTrack
  Use modulePakoVlbi
  !
  Use gbl_message
  !
  Implicit None
  Save
!!$  Private
!!$  !
!!$  !
  ! *** Subroutines
  !
  Public :: pakoStart
  !
!!!
!!!
Contains
!!!
!!!
  Subroutine pakoStart(programName,LINE,COMMAND,ERROR)
    !
    !**   arguments:   ***
    Character(len=lenPro),        Intent(in)    ::  programName
    Character(len=lenLineDouble), Intent(in)    ::  LINE
    Character(len=lenCo),         Intent(in)    ::  COMMAND
    Logical,                      Intent(out)   ::  ERROR
    !
    !**   specific local variables:    ***
    Character(len=lenCh)          ::  commandToStart
    Character(len=lenVar)         ::  baseFileName
    Character(len=lenVar)         ::  startFile, startFileOLD
    Character(len=lenVar)         ::  queueFile, queueFileOLD
    Character(len=lenVar)         ::  htmlFile,  htmlFileOLD
    Character(len=lenVAr)         ::  htmlQFile, htmlQFileOLD
    Character(len=lenLineDouble)  ::  shellCommand
    Integer                       ::  ier, priorityIlocal
    !
    !**   functions called:   ***
    Integer  ::  SIC_NARG
    Integer  ::  Rename
    Integer  ::  System
    !
    Logical  ::  receiverIsHolo
    Logical  ::  isConnected, errorS
    !
    Integer  ::  iUnit2
    Integer  ::  Job
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Start, v 1.2.4 2015-06-08---> SR: Start ")                  ! this allows to trace execution
    !
    priorityIlocal  = 1
    !
    errorL = .False.
    errorS = .False.
    !
    commandToStart = GV%observingModePako
    !
    !**   read Parameters, Options   ***
    !
    Include 'inc/parameters/parametersCommandToStart.inc'
    !
    !! Call SIC_LOWER(commandToStart)
    !
    Call pakoUmatchKey (                                                         &
         &              keys=OMChoicesPako,                                      &
         &              key=commandToStart,                                      &
         &              command='START',                                         &
         &              howto='Start Upper',                                     &
         &              iMatch=iMatch,                                           &
         &              nMatch=nMatch,                                           &
         &              error=errorC,                                            &
         &              errorCode=errorCode                                      &
         &             )                                                         !
    !
    If (.Not. errorC) Then
       commandToStart = OMChoicesPako(iMatch)
    End If
    !
    !D     Write (6,*) '      commandToStart:         ',  commandToStart   , '<'
    !D     Write (6,*) "      gv%observingMode:       ",  gv%observingMode    
    !D     Write (6,*) "      gv%observingModePako:   ",  gv%observingModePako
    !
    If ( .Not. commandToStart.Eq.gv%observingModePako ) Then
       errorL = .True.
       messageText =                                                             &
            &        "can not start "//commandToStart                            !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &   "use START [ "//gv%observingModePako//" ]"                       !
       Call pakoMessage(priorityI,severityI,command,messageText)
    End If
    !
    ERROR = ERROR .Or. errorC .Or. errorL
    !
    If (ERROR) Then
       messageText =                                                             &
            &        "NO OBSERVATIONS STARTED"                                   !
       Call pakoMessage(priorityE,severityE,command,messageText)
       RETURN                                                                    !  !!  ERROR --> do not START 
    End If
    !
    baseFileName = commandToStart(1:lenc(commandToStart))
    Call sic_lower(baseFileName)
    !
    startFile    = baseFileName(1:lenc(baseFileName))//".xml"
    startFileOLD = startFile(1:lenc(startFile))//"~"
    !
    htmlFile     = baseFileName(1:lenc(baseFileName))//".html"
    htmlFileOLD  = htmlFile(1:lenc(htmlFile))//"~"
    !
    queueFile    = "XML/"//startFile(1:lenc(startFile))
    queueFileOLD = queueFile(1:lenc(queueFile))//"~"
    !
    htmlQFile    = "XML/"//htmlFile(1:lenc(htmlFile))
    htmlQFileOLD = htmlQFile(1:lenc(htmlQFile))//"~"
    !
    !D     Write (6,*) "  startFile    ->", startFile(1:lenc(startFile)),"<-"
    !D     Write (6,*) "  startFileOld ->", startFileOld(1:lenc(startFileOld)),"<-"
    !D     Write (6,*) "  htmlFile     ->", htmlFile(1:lenc(htmlFile)),"<-"
    !D     Write (6,*) "  htmlFileOld  ->", htmlFileOld(1:lenc(htmlFileOld)),"<-"
    !D     Write (6,*) "  queueFile    ->", queueFile(1:lenc(queueFile)),"<-"
    !D     Write (6,*) "  queueFileOld ->", queueFileOld(1:lenc(queueFileOld)),"<-"
    !D     Write (6,*) "  htmlQFile    ->", htmlQFile(1:lenc(htmlQFile)),"<-"
    !D     Write (6,*) "  htmlQFileOld ->", htmlQFileOld(1:lenc(htmlQFileOld)),"<-"
    !
    ier = 0
    !
!!$!!OLD    ier = Rename(startFile, startFileOLD)
!!$!!OLD    If (ier.Eq.0) Then
!!$!!OLD       Write (messageText,*)                                                     &
!!$!!OLD            &           'renamed previous start file: "',                        &
!!$!!OLD            &           startFile(1:lenc(startFile)),                            &
!!$!!OLD            &           '"'
!!$!!OLD       Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD       Write (messageText,*)                                                     &
!!$!!OLD            &           '   to: "',                                              &
!!$!!OLD            &           startFileOLD(1:lenc(startFileOLD)),                      &
!!$!!OLD            &           '"'
!!$!!OLD       Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD    Else
!!$!!OLD       ier = 0
!!$!!OLD    End If
!!$!!OLD    !
    !
    If (GV%privilege.Eq.setPrivilege%VLBI .And.                                  &
         &   GV%sessionMode.Eq.om%vlbi .And. GV%doSubmit ) Then                  !
       !
       !! for VLBI sessions: send "done" for running VLBI scan
       shellCommand =                                                            &
            &  'ncsMessageSend.py sync operator:scan:done > /dev/null  2>&1 '    !  ! to work
!!OLD       &  'ncsMessageSend.py sync     test:scan:done > /dev/null  2>&1 '    !  ! for tests
       ier = system(shellCommand)                                                
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &           'any running VLBI scan will be stopped.'              !
          Call pakoMessage(priorityW,severityW,command,messageText)
       Else
          Write (messageText,*)                                                  &
               &           'ERROR', ier,                                         &
               &           '  shellCommand: ', shellCommand                      !
          Call pakoMessage(priorityE,severityE,command,messageText)
       End If
       !
    End If
    !
    iunit =  ioUnit%start
    !
    !D     Write (6,*) "         unit: iunit = ", iunit
    !
    ! !!  unix convention ier.Eq.0 --> OK !!
    If (ier.Eq.0) Then  !!  If OK ...
       !
       Open (unit=iUnit,file=startFile,recl=512,status='unknown')
       !
       ! *** call the requested start method:   ***
       !
       If (.False.) Then
       Else If (commandToStart .Eq. OMpako%Cal) Then
          Call startCalibrate(programName,LINE,commandToStart,                   &
               &            iUnit, errorL)                                       !
       Else If (commandToStart .Eq. OMpako%Focus) Then
          Call startFocus(programName,LINE,commandToStart,                       &
               &            iUnit, errorL)                                       !
          !
!!$!!OLD       Else If (commandToStart .Eq. 'OTFCIRCLE') Then
!!$!!OLD          Call startOtfCircle(programName,LINE,commandToStart,                &
!!$!!OLD               &            iUnit, errorL)
!!$!!OLD       Else If (commandToStart .Eq. 'OTFLINEAR') Then
!!$!!OLD          Call startOtfLinear(programName,LINE,commandToStart,                &
!!$!!OLD               &            iUnit, errorL)
          !
       Else If (commandToStart .Eq. OMpako%LISSAJOUS) Then
          Call startLISSAJOUS(programName,LINE,commandToStart,                   &
               &             iUnit, errorL)                                      !
       Else If (commandToStart .Eq. OMpako%otfmap) Then
          Call startOtfMap(programName,LINE,commandToStart,                      &
               &             iUnit, errorL)                                      !
       Else If (commandToStart .Eq. OMpako%onoff) Then
          Call startOnOff(programName,LINE,commandToStart,                       &
               &           iUnit, errorL)
       Else If (commandToStart .Eq. OMpako%Pointing) Then
          Call startPointing(programName,LINE,commandToStart,                    &
               &           iUnit, errorL)                                        !
          !
!!$!!TBD       Else If (commandToStart .Eq. 'SEGMENT') Then
!!$!!TBD          Write (messageText,*)'START SEGMENT is not possible'
!!$!!TBD          Call pakoMessage(priorityW,severityW,command,messageText)
!!$!!TBD          Write (messageText,*)'try: START subscanList'
!!$!!TBD          Call pakoMessage(priorityI,severityI,command,messageText)
!!$!!TBD          errorL = .True.
          !
       Else If (commandToStart .Eq. OM%DIY) Then
          !D           Write (6,*) " --> modulePakoStart: Calling startDIYlist"
          Call startDIYlist(programName,LINE,commandToStart,iUnit, errorL)                              
       Else If (commandToStart .Eq. OM%subscanList) Then
          !D           Write (6,*) " --> modulePakoStart: Calling startSubscanList"
          Call startSubscanList(programName,LINE,commandToStart,iUnit, errorL)                              
       Else If (commandToStart .Eq. OMpako%tip) Then
          Call startTip(programName,LINE,commandToStart,                         &
               &             iUnit, errorL)                                      !
       Else If (commandToStart .Eq. OMpako%track) Then
          Call startTrack(programName,LINE,commandToStart,                       &
               &             iUnit, errorL)                                      !
       Else If (commandToStart .Eq. OMpako%vlbi ) Then
          If (GV%privilege.Eq.setPrivilege%VLBI ) Then
             Call startVlbi(programName,LINE,commandToStart,iUnit, errorL)         
          Else
             errorL = .True.
             messageText =                                                       &
                  &   " NOT privileged to start and submit VLBI"                 !
             Call pakoMessage(priorityE,severityE,command,messageText)
          End If
       Else
          errorL = .True.
          errorS = .True. 
       End If
       !
       Close(unit=iunit)
       !
    Else
       errorL = .True.
    End If
    !
    ERROR = ERROR .Or. errorL
    !
    ! *** Enforce rules about command interaction:
    !
    ! **  Source must be specified 
    !
    If     (.Not. GV%sourceSet) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify SOURCE "                                  !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  At least one receiver must be specified 
    !
    If     (.Not. GV%receiverSet) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify at least 1 RECEIVER "                     !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  
    !
    Call queryReceiver(RxHolography, receiverIsHolo)
    Call queryReceiver(rec%bolo, isConnected)
    If     (GV%observingModePako.Ne.OMpako%VLBI                                  &
         &        .And. .Not.receiverIsHolo                                      &
         &        .And. .Not.isConnected ) Then                                  !                                                                   
       ! **  At least one backend must be specified 
       !
       If (.Not. GV%backendSet) Then
          messageText =                                                          &
               &           " CAN NOT start and submit "//commandToStart          !
          Call pakoMessage(priorityE,severityE,command,messageText)
          messageText =                                                          &
               &           " at least 1 BACKEND must be connected "              !
          Call pakoMessage(priorityE,severityE,command,messageText)
          ERROR = .True.
       End If
    End If
    !
!!$!!OLD    Call queryReceiver(rec%bolo, isConnected)
!!$!!OLD    If     (GV%observingModePako.Ne.OMpako%VLBI .And. .Not.isConnected ) Then
!!$!!OLD       ! **  At least one backend must be specified 
!!$!!OLD       !
!!$!!OLD       If (.Not. GV%backendSet) Then
!!$!!OLD          messageText =                                                          &
!!$!!OLD               &           " CAN NOT start and submit "//commandToStart          !
!!$!!OLD          Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD          messageText =                                                          &
!!$!!OLD               &           " at least 1 BACKEND must be connected "              !
!!$!!OLD          Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD          ERROR = .True.
!!$!!OLD       End If
!!$!!OLD    End If
    !
    ! **  Backends must be specified after Receivers
    !
    If     (GV%notReadyRXafterBE) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify BACKENDs after RECEIVERs "                !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  OM Tip must be specified after Receivers
    !
    If     ( GV%observingMode.Eq.OM%tip .And. GV%notReadyRXafterTip ) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify observing mode TIP after RECEIVERs "      !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  2ndary Rotation must be specified after Receivers
    !
    If     ( GV%secondaryRotation.Ne.0.0 .And. GV%notReadyRXafterSecondary ) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify SET 2ndRotation after RECEIVERs "         !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  observing mode must be specified after 2ndary Rotation
    !
    If     ( GV%notReadySecondaryRafterOM ) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify observing mode "//commandToStart//        &
            &           " after SET 2ndRotation "                                !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  observing mode must be specified after switching mode:
    !
    If     ( GV%notReadySWafterOM .And.(      commandToStart.Eq.OMpako%Focus     &
         &                               .Or. commandToStart.Eq.OMpako%OnOff     &
         &                               .Or. commandToStart.Eq.OMpako%OtfMap    &
         &                               .Or. commandToStart.Eq.OMpako%Pointing  &
         &                               .Or. commandToStart.Eq.OMpako%Track  )  &
         & )    Then                                                             !
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " MUST specify observing mode "//commandToStart//        &
            &           " after switching mode "                                 !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  observing mode must be specified after OFFSETS:
    !
    If (GV%notReadyOffsetsAfterOnOff .And. commandToStart.Eq.OMpako%OnOff) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " Observing mode "//commandToStart//                     &
            &           " has to be specified AFTER OFFSETS"                     !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  OFFSETS /System Nasmyth and RX Array offsets must be consistent
    !
    If (GV%notReadyReceiverOffsets) Then
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText = "OFFSETS /SYSTEM Nasmyth and "//                            &
            &        "RX Array offsets are inconsistent"                         !
       Call pakoMessage(priorityE,severityE,command,messageText)
       ERROR = .True.
    End If
    !
    ! **  other conditions:
    !
    If     (commandToStart .Ne. GV%observingModePako .And.                       & 
         &    ( GV%notReadyFocus    .And. commandToStart.Eq.OMpako%Focus         &
         & .Or. GV%notReadyOnOff    .And. commandToStart.Eq.OMpako%OnOff         &
         & .Or. GV%notReadyOtfMap   .And. commandToStart.Eq.OMpako%OtfMap        &
         & .Or. GV%notReadyPointing .And. commandToStart.Eq.OMpako%Pointing      &
         & .Or. GV%notReadyTrack    .And. commandToStart.Eq.OMpako%Track         &
         &    )                                                                  &
         & )    Then                                                             !
       messageText =                                                             &
            &           " CAN NOT start and submit "//commandToStart             !
       Call pakoMessage(priorityE,severityE,command,messageText)
       messageText =                                                             &
            &           " CHECK switching and observing modes "                  !
       Call pakoMessage(priorityI,severityI,command,messageText)
       ERROR = .True.
    End If
    !
!!$!!OLD    ! **  other and more specific rules: (de-activated 2006-10-16)
!!$!!OLD    !
!!$!!OLD    If     (GV%switchingMode.Eq.swMode%Freq .And.                                &
!!$!!OLD         &    ( GV%notReadyOtfMap   .And. commandToStart.Eq.OMpako%OtfMap )      &
!!$!!OLD         & )    Then
!!$!!OLD       messageText =                                                             &
!!$!!OLD            &           " CAN NOT start and submit "//commandToStart
!!$!!OLD       Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       messageText =                                                             &
!!$!!OLD            &           " Observing mode "//commandToStart//                     &
!!$!!OLD            &           " has to be specified AFTER SWFREQUENCY "
!!$!!OLD       Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       ERROR = .True.
!!$!!OLD    End If
!!$!!OLD    !
!!$!!OLD    If     (GV%switchingMode.Eq.swMode%Wobb .And.                                &
!!$!!OLD         &    ( GV%notReadyFocus    .And. commandToStart.Eq.OMpako%Focus         &
!!$!!OLD         & .Or. GV%notReadyPointing .And. commandToStart.Eq.OMpako%Pointing      &
!!$!!OLD         & .Or. GV%notReadyOnOff    .And. commandToStart.Eq.OMpako%OnOff         &
!!$!!OLD         & .Or. GV%notReadyOtfMap   .And. commandToStart.Eq.OMpako%OtfMap )      &
!!$!!OLD         & )    Then
!!$!!OLD       messageText =                                                             &
!!$!!OLD            &           " CAN NOT start and submit "//commandToStart
!!$!!OLD       Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       messageText =                                                             &
!!$!!OLD            &           " Observing mode "//commandToStart//                     &
!!$!!OLD            &           " has to be specified AFTER SWWOBBLER"
!!$!!OLD       Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       ERROR = .True.
!!$!!OLD    End If
    !
    !D     Write (6,*) "      ERROR:   ", ERROR
    !
    If (.Not. ERROR) Then
       !     
       messageText =                                                             &
            &           " OK. wrote "//                                          &
            &           commandToStart(1:lenc(commandToStart))//                 &
            &           " to file "//startFile(1:lenc(startFile))                !
       Call pakoMessage(priorityIlocal,severityI,command,messageText)
       !
       ier = Rename(queueFile, queueFileOLD)
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &           'renamed previous queue file: "',                     &
               &           queueFile(1:lenc(queueFile)),                         &
               &           '"'                                                   !
          !D      Call pakoMessage(priorityIlocal,severityI,command,messageText)
          Write (messageText,*)                                                  &
               &           '   to: "',                                           &
               &           queueFileOLD(1:lenc(queueFileOLD)),                   &
               &           '"'                                                   !
          !D      Call pakoMessage(priorityIlocal,severityI,command,messageText)
       End If
       !
!!$!!OLD       ier = Rename(htmlFile, htmlFileOLD)
!!$!!OLD       If (ier.Eq.0) Then
!!$!!OLD          Write (messageText,*)                                                    &
!!$!!OLD               &           'renamed previous html file: "',                        &
!!$!!OLD               &           htmlFile(1:lenc(htmlFile)),                             &
!!$!!OLD               &           '"'
!!$!!OLD          Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD          Write (messageText,*)                                                    &
!!$!!OLD               &           '   to: "',                                             &
!!$!!OLD               &           htmlFileOLD(1:lenc(htmlFileOLD)),                       &
!!$!!OLD               &           '"'
!!$!!OLD          Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD       End If
!!$!!OLD       !
       !!
       !!
       !!  *** SUBMIT to Observing Queue
       !!
       shellCommand = "submitObservingBlock "//startFile(1:lenc(startFile))      &
            &       //" 2>/dev/null"                                             !
       shellCommand = "submitObservingBlock "//startFile(1:lenc(startFile))      &
            &       //" > .submitResult"                                         !
       ier =  0                                                                  !   0 to run
       !ier = -1                                                                 !  -1 to test
       !
       If (ier.Eq.0 .And. GV%doSubmit) Then
          !
          ier = system(shellCommand)
          !D          Write (6,*) "   after System(shellCommand): iostat=ier: ", ier
          !
!!OLD       Write (6,*) "   cat .submitResult:   "
!!OLD       shellCommand2 = "cat .submitResult"    
!!OLD       ier2 = system(shellCommand2)
          !
          iUnit2 = ioUnit%Read
          !
          If (ier.Eq.0) Then
             Open (unit=iUnit2,file=".submitResult",status='unknown',iostat=ier)
             !D             Write (6,*) "   after Open: iostat=ier: ", ier
          End If
          !
          If (ier.Eq.0) Then       
             Read (iUnit2,*,iostat=ier)  Job
             !D             Write (6,*) "   after Read: iostat=ier: ", ier
          End If
          !
          Close (unit=iUnit2)
          !
          If (ier.Eq.0) Then
             Write (messageText,*)                                               &
                  & ' Observation submitted to observing queue with job number ',&
                  & Job                                                          !
             Call pakoMessage(priorityI,severityI,command,messageText)
          End If
          !
          If (ier.Ne.0) Then
             Error = .True.
             Write (messageText,*)                                               &
                  &           'ERROR', ier,                                      &
                  &           " Couldn't submit observation to Queue "           !
             Call pakoMessage(priorityE,severityE,command,messageText)
          End If
          !
       End If
       !
       If (.Not.GV%doSubmit) Then
          Write (messageText,*)                                                  &
               & 'doSubmit to observation queue is NO / .False.'                 !
          Call pakoMessage(priorityW,severityW,command,messageText)
          Write (messageText,*)                                                  &
               & 'Observation would be submitted to queue with command:'         !
          Call pakoMessage(priorityI,severityI,command,messageText)
          Call pakoMessage(priorityI,severityI,command,shellcommand)
       End If
       !
       !!  *** MOVE to XML directory
       !!
       !! TBD: shellCommand = 'mv '//                                            &
       shellCommand = 'cp '//                                                    &
            &        startFile(1:lenc(startFile))//                              &
            &        ' '//                                                       &
            &        queueFile(1:lenc(queueFile))                                !
       ier = system(shellCommand)
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &           'saved start file: "',                                &
               &           startFile(1:lenc(startFile)),                         &
               &           '"'                                                   !
          !D          Call pakoMessage(priorityI,severityI,command,messageText)
       Else
          Write (messageText,*)                                                  &
               &           'ERROR', ier,                                         &
               &           ' on save of start file: "',                          &
               &           startFile(1:lenc(startFile))                          !
          !D          Call pakoMessage(priorityE,severityE,command,messageText)
       End If
       Write (messageText,*)                                                     &
            &        '   in directory: "',                                       &
            &        queueFile(1:lenc(queueFile)),                               &
            &        '"'                                                         !
       !D       Call pakoMessage(priorityI,severityI,command,messageText)
       !
       !!
!!$!!OLD       shellCommand = 'obsXMLtoHTML.py < '//                                    &
!!$!!OLD            &        startFile(1:lenc(startFile))//                             &
!!$!!OLD            &        ' > '//                                                    &
!!$!!OLD            &        htmlFile(1:lenc(htmlFile))
!!$!!OLD       ier = 1
!!$!!OLD       ier = system(shellCommand)
!!$!!OLD       If (ier.Eq.0) Then
!!$!!OLD          Write (messageText,*)                                                 &
!!$!!OLD               &           'transformed XML file: "',                           &
!!$!!OLD               &           startFile(1:lenc(startFile)),                        &
!!$!!OLD               &           '"'
!!$!!OLD          Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD       Else
!!$!!OLD          Write (messageText,*)                                                 &
!!$!!OLD               &           'ERROR', ier,                                        &
!!$!!OLD               &           ' on transform of XML file: "',                      &
!!$!!OLD               &           startFile(1:lenc(startFile))
!!$!!OLD          Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       End If
!!$!!OLD       Write (messageText,*)                                                    &
!!$!!OLD            &        '   to HTML file: "',                                      &
!!$!!OLD            &        htmlFile(1:lenc(htmlFile)),                                &
!!$!!OLD            &        '"'
!!$!!OLD       Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD       !
!!$!!OLD       shellCommand = 'cp '//                                                   &
!!$!!OLD            &        htmlFile(1:lenc(htmlFile))//                               &
!!$!!OLD            &        ' '//                                                      &
!!$!!OLD            &        htmlQFile(1:lenc(htmlQFile))
!!$!!OLD       ier = system(shellCommand)
!!$!!OLD       If (ier.Eq.0) Then
!!$!!OLD          Write (messageText,*)                                                 &
!!$!!OLD               &           'copied html file: "',                               &
!!$!!OLD               &           htmlFile(1:lenc(htmlFile)),                          &
!!$!!OLD               &           '"'
!!$!!OLD          Call pakoMessage(priorityIlocal,severityI,command,messageText)
!!$!!OLD       Else
!!$!!OLD          Write (messageText,*)                                                 &
!!$!!OLD               &           'ERROR', ier,                                        &
!!$!!OLD               &           ' on copy of html file: "',                          &
!!$!!OLD               &           htmlFile(1:lenc(htmlFile))
!!$!!OLD          Call pakoMessage(priorityE,severityE,command,messageText)
!!$!!OLD       End If
!!$!!OLD       Write (messageText,*)                                                    &
!!$!!OLD            &        '   to: "',                                                &
!!$!!OLD            &        htmlQFile(1:lenc(htmlQFile)),                              &
!!$!!OLD            &        '"'
!!$!!OLD       Call pakoMessage(priorityIlocal,severityI,command,messageText)
       !
    Else If (errorS) Then
       !     
       messageText =                                                             &
            &          programName(1:lenc(programName))//                        &
            &        " does not know (yet) how to  start "//                     &
            &          commandToStart(1:lenc(commandToStart))
       Call pakoMessage(priorityE,severityE,command,messageText)
       !
    Else If (ERROR) Then
       !     
       messageText =                                                             &
            &          programName(1:lenc(programName))//                        &
            &        " could not start "//                                       &
            &          commandToStart(1:lenc(commandToStart))//                  &
            &        " to file "//startFile(1:lenc(startFile))
       Call pakoMessage(priorityE,severityE,command,messageText)
       !
    End If   !!   If (.Not. ERROR) Then
    !
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Start, <--- SR: Start ")                                    ! this allows to trace execution
    !
    Return
    !
  End Subroutine pakoStart
!!!
!!!
End Module modulePakoStart



