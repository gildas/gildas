!
! --------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoSwTotalPower">
!   <VERSION>
!                Id: modulePakoSwTotalPower.f90,v 1.2.2 2013-02-14 Hans Ungerechts
!                    reviewed w/r/t paKo                2012-11-30
!                Id: modulePakoSwTotalPower.f90,v 1.2.1 2012-08-16 Hans Ungerechts
!                based on
!                Id: modulePakoSwTotalPower.f90,v 1.1.6 2011-06-30 Hans Ungerechts                
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Switchinging Modes
!   </FAMILY>
!   <SIBLINGS>
!                SwBeam
!                SwFrequency
!                SwTotalPower
!                SwWobbler
!   </SIBLINGS>        
!
!   Pako module for command: SWTOTAL
!
! </DOCUMENTATION> <!-- name="modulePakoSwTotalPower" -->
!
! --------------------------------------------------------------------------------
!
Module modulePakoSwTotalPower
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  !
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: swTotalPower, displaySwTotalPower, saveSwTotalPower, writeXMLswTotalPower
  Public :: saveSwitching
  !     
  ! *** Variables for swTotalPower ***
  Include 'inc/variables/variablesSwTotalPower.inc'
  !
Contains
!!!
!!!
  Subroutine swTotalPower(programName,line,command,error)
    !
    ! *** Arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Real(Kind=kindSingle)   ::  rr
    !
    ! 
    Include 'inc/ranges/rangesSwTotalPower.inc'
    !
    error = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module swTotalPower,v 1.2.1 2012-08-16 ---> SR: swTotalPower ")  ! trace execution
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    Call checkNofParameters(command,                                             &
         &     0, 0,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/options/readOptionsSwTotalPower.inc'
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/options/readOptionsSwTotalPower.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** derived parameters                              ***
    !
    If (.Not.ERROR) Then
       !
       rr                     =     vars(iIn)%tPhase                             &
            &                     * vars(iIn)%nPhases                            &
            &                     * vars(iIn)%nCycles                            !
       !
       Write (messageText,*)                                                     &
            &                 " number of phases = nPhases =  ",                 &
            &                 vars(iIn)%nPhases                                  !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            &                 " time per record = tRecord = " //                 &
            &                 " tPhase*nPhases*nCycles = " ,                     &
            &                 rr                                                 !
       If      (rr.Gt.vars(iLimit2)%tRecord) Then
          Call PakoMessage(priorityE,severityE,command,messageText)
       Else If (rr.Gt.vars(iStd2)%tRecord) Then
          Call PakoMessage(priorityW,severityW,command,messageText)
       Else 
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
       !
       Call checkR4("SWT tRecord ",rr,                                           &
            &           vars%tRecord,                                            &
            &           errorR)                                                  !
       !
       If (errorR) Then
          Write (messageText,*)                                                  &
               &         " tRecord = tPhase*nPhases*nCycles "//                  &
               &         " is outside limits "                                   !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       ERROR = ERROR .or. errorR
       !
    End If
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       !
       errorInconsistent = .False.
       !
       !D        Write (6,*) "          vars(iIn)%tPhase:  ", vars(iIn)%tPhase
       !D        Write (6,*) "          GV%backendFTS:     ", GV%backendFTS
       !
       If (GV%backendFTS .And. vars(iIn)%tPhase.Le.0.1-0.001) Then
          errorInconsistent = .True.
          Write (messageText, '(a,F8.2,a,a,a,a)')                                &
                  &   " /tPhase ",            vars(iIn)%tPhase,                  &
                  &   " in ",                 swModePako%total,                  &
                  &   " too small for ",      bac%FTS                            !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from Temp into persistent variables   ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
    End If
    !
    ! *** display values ***
    If (.Not.ERROR) Then
       Call displaySwTotalPower
    End If
    !
    ! *** set "selected" switching mode ***
    If (.Not.error) Then
       GV%switchingMode     = swMode%Total
       GV%tPhase            = vars(iValue)%tPhase
       GV%tRecord           = vars(iValue)%tRecord
       GV%notReadySWafterOM = .True.
       GV%notReadyFocus     = .True.
       GV%notReadyOnOff     = .True.
       GV%notReadyOtfMap    = .True.
       GV%notReadyPointing  = .True.
       GV%notReadyTrack     = .True.
       !
       Include 'inc/messages/messageSwModeSelected.inc'
       !
       swTotalPowerCommand = line(1:lenc(line))
    End If
    !
    !D    Write (6,*) "      GV%tRecord:  ", GV%tRecord
    !
    Return
  End Subroutine swTotalPower
!!!
!!!
  Subroutine displaySwTotalPower
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    ! 
    Include 'inc/display/commandDisplaySwTotalPower.inc'
    !
  End Subroutine displaySwTotalPower
!!!
!!!
  Subroutine saveSwitching(programName,LINE,commandToSave, iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    error = .False.
    !
    !D    Write (6,*) "   --> SR saveSwitching "
    !D    Write (6,*) "       switchingMode:  >", GV%switchingMode,"<"
    !D    Write (6,*) "                       >", swMode%Beam,"<"
    !D    Write (6,*) "                       >", swMode%Freq,"<"
    !D    Write (6,*) "                       >", swMode%Wobb,"<"
    !D    Write (6,*) "                       >", swMode%Total,"<"
    !
    Select Case (GV%switchingMode)
    Case(swMode%Beam)
       Call saveSwBeam(programName,LINE,commandToSave,iUnit,ERROR)
    Case(swMode%Freq)
       Call saveSwFrequency(programName,LINE,commandToSave,iUnit,ERROR)
    Case(swMode%Wobb)
       Call saveSwWobbler(programName,LINE,commandToSave,iUnit,ERROR)
    Case(swMode%Total)
       Call saveSwTotalPower(programName,LINE,commandToSave,iUnit,ERROR)
    Case default
       Call saveSwTotalPower(programName,LINE,commandToSave,iUnit,ERROR)
    End Select
    !
    Return
  End Subroutine saveSwitching
!!!
!!!
  Subroutine saveSwTotalPower(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    error = .False.
    !
    contC = contNN
    !
    B = '\'   !   '
    S = ' '
    CMD =  programName//B//"SWTOTAL"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    Write (iUnit,*) CMD(1:lCMD), " -"
    !
    contC = contCN
    !
    Include 'inc/options/saveTphase.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveSwTotalPower
!!!
!!!
  Subroutine writeXMLswTotalPower(programName,LINE,commandToSave,      &
       &        iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh) :: valueC
    Real                  :: value
    Logical               :: errorXML
    !
    Character(len=lenLine) :: messageText
    !
    error = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module SwTotalPower, v 1.2.1  --> SR: writeXMLswTotalPower ")      ! trace execution
    !D Write (6,*)  "      modulePakoSwTotalPower --> SR: writeXMLswTotalPower "
    !
    Include 'inc/startXML/swTotalPower.inc'
    !
    Return
  End Subroutine writeXMLswTotalPower
!!!
!!!
End Module modulePakoSwTotalPower
!!!
!!!








