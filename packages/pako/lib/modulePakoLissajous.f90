!
!--------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoLissajous.f90">
!   <VERSION>
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-12-15 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-10-20 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-09-25 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-08-28 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-07-17 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.3  2014-07-08 Hans Ungerechts
!                Id: modulePakoLissajous.f90,v 1.2.2  2013-02-06 Hans Ungerechts
!                based on:
!                Id: modulePakoLissajous.f90,v 1.1.12 2012-03-12 Hans Ungerechts
!   </VERSION>
!   <HISTORY>
!                2012-03-12: new for GISMO run
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>

!   </SIBLINGS>        
!
!   Pako module for command: LISSAJOUS
!
! </DOCUMENTATION> <!-- name="modulePakoLissajous.f90" -->
!
!--------------------------------------------------------------------------------
!
Module modulePakoLissajous
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  Use modulePakoDIYlist
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: Lissajous
  Public :: saveLissajous
  Public :: startLissajous
  !     
  ! *** Variables for Lissajous ***
  Include 'inc/variables/variablesLissajous.inc'
!!!   
!!!   
Contains
!!!
!!!
  Subroutine LISSAJOUS(programName,line,command,error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
!! OLD !!$      Integer              ::  iMatch = 0 , nMatch = 0
!! OLD !!$      Character (len=128)  ::  errorCode = 'none'
!! OLD !!$      Logical              ::  errorM = .False.
    !
    ! *** special working variables:   ***
    Logical               :: optionSpeedIsPresent, optionTotfIsPresent
    Logical, Dimension(3) :: alternative
    !
    Real                  :: vLissMaxX,   vLissMaxY,   aLissMaxX,   aLissMaxY,   vLissLimit, aLissLimit
    Real                  :: vLissFacX,   vLissFacY,   aLissFacX,   aLissFacY
    Real                  :: vLissMaxXel, vLissMaxYel, aLissMaxXel, aLissMaxYel, lissMaxEl
    !
    Logical               :: isConnected   = .False.
    !
    Integer               :: ierr
    !
    Real, Dimension(2)    :: rr 
    !
    Logical :: receiverIsBolo
    Character (len=lenCh) :: bolometerName
    !
    Type(conditionMinMax)  ::  condElevation                                     &
         &                     = conditionMinMax("elevation",                    &
         &                                        0.0, Pi/2.0,                   &
         &                                       "rad",                          &
         &                                       .False.         )               ! 
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module Lissajous, 1.2.3 2014-12-15 ---> SR: Lissajous ")         ! trace execution
    !
    ERROR = .False.
    !
    optionSpeedIsPresent = .False.
    optionTotfIsPresent  = .False.
    alternative          = .False.
    !
    !D     Write (6,*) "   modulePakoLissajous: --> SR Lissajous "
    !
    Include 'inc/ranges/rangesLissajous.inc'
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    Call checkNofParameters(command,                                             &
         &     0, 2,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    Call queryReceiver(rec%bolo, receiverIsBolo,                                 &
         &       bolometerName = bolometerName)                                  !
    !
    !D     Write (6,*) "      receiverIsBolo:   ", receiverIsBolo
    !D     Write (6,*) "      bolometerName:    ", bolometerName
    !
    If (   .Not.                                                                 &
         & (     receiverIsBolo.And.bolometerName.Eq.bolo%GISMO                  &
         &  .Or. receiverIsBolo.And.bolometerName.Eq.bolo%NIKA                   &
         &  .Or. GV%privilege.Eq.setPrivilege%ncsTeam                            &
         & )    ) Then                                                           !
       ERROR = .True.
       Write (messageText, *) " works only with ", bolo%GISMO, " or ", bolo%NIKA
       Call pakoMessage(priorityE,severityE,command,                             &
            &           messageText)                                             !
    End If
    !
!!$!! OLD !!$    If (   .Not.                                                                 &
!!$!! OLD !!$         & (     GV%privilege.Eq.setPrivilege%GISMO                              &
!!$!! OLD !!$         &  .Or. GV%privilege.Eq.setPrivilege%ncsTeam                            &
!!$!! OLD !!$         &  .Or. GV%privilege.Eq.setPrivilege%staff                              &
!!$!! OLD !!$         & )    ) Then                                                           !
!!$!! OLD !!$       ERROR = .True.
!!$!! OLD !!$       Write (messageText, *) " requires privilege"
!!$!! OLD !!$       Call pakoMessage(priorityE,severityE,command,                             &
!!$!! OLD !!$            &           messageText)                                             !
!!$!! OLD !!$    End If
    !
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    ! *** pako (error) messages are turned ON                 ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersAmplitudeXY.inc'
       Include 'inc/options/readOptionsLissajous.inc'
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
    !
    ! *** Read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    ! *** pako (error) messages are turned OFF                ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersAmplitudeXY.inc'
       Include 'inc/options/readOptionsLissajous.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then    !!   check consistency
       !
       errorInconsistent = .False.
       !
       If (vars(iIn)%tOtf .Lt. 5*GV%tRecord) Then
          If (vars(iIn)%tOtf .Lt. 3*GV%tRecord) Then
             errorInconsistent = .True.
             Write (messageText,*)    "must be at least 3 times "                &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityE,severityE,                               &
                  &           command(1:l)//"/"//"tOtf",messageText)             !
          Else
             Write (messageText,*)    "should be at least 5 times "              &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityW,severityW,                               &
                  &           command(1:l)//"/"//"tOtf",messageText)             !
          End If
       End If
       !
       If (vars(iIn)%doReference                                                 &
            &  .And.   vars(iIn)%tReference .Lt. 5*GV%tRecord) Then              !
          If (vars(iIn)%tReference .Lt. 3*GV%tRecord) Then
             errorInconsistent = .True.
             Write (messageText,*)    "must be at least 3 times "                &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityE,severityE,                               &
                  &           command(1:l)//"/"//"tReference",messageText)       !
          Else
             Write (messageText,*)    "should be at least 5 times "              &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityW,severityW,                               &
                  &           command(1:l)//"/"//"tReference",messageText)       !
          End If
       End If
       !
       If (vars(iIn)%doReference                                                 &
            &  .And. vars(iIn)%systemNameRef.Ne.vars(iIn)%systemName) Then
          errorInconsistent = .True.
          GV%notReadyLissajous = .True.
          Write (messageText,*) "different values for /SYSTEM ",                 &  
               &  "and /REFERENCE are not yet allowed "   
          Call pakoMessage(priorityE,severityE,                                  &
               &           command//"/"//"REFERENCE",messageText)
       End If
       !
       If (    .Not. (GV%privilege.Eq.setPrivilege%staff                         &
            &  .Or.   GV%privilege.Eq.setPrivilege%ncsTeam)) Then                !
       If (GV%switchingMode.Eq.swMode%Beam) Then
          errorInconsistent = .True.
          GV%notReadyLissajous = .True.
          Write (messageText,*)  "will not work with switching mode ",           &
               &                 swModePako%Beam
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       End If
       !
       If (        vars(iIn)%doReference .And. GV%switchingMode.Eq.swMode%Freq   &
            & .Or. vars(iIn)%doReference .And. GV%switchingMode.Eq.swMode%Wobb ) &
            & Then
          errorInconsistent = .True.
          GV%notReadyLissajous = .True.
          Write (messageText,*)  "will not work with switching mode ",           &
               &                 GV%switchingMode
          Call PakoMessage(priorityE,severityE,command//" /reference",           &
               &                 messageText)
          Write (messageText,*)  "consider to use: /reference no" 
          Call PakoMessage(priorityI,severityI,command,                          &
               &                 messageText)
       End If
       !
       If (        vars(iIn)%doFocus .And. vars(iIn)%doPointing ) Then
          errorInconsistent = .True.
          Write (messageText,*)  "can not have both /pointing and /focus"
          Call PakoMessage(priorityE,severityE,command,                          &
               &                 messageText)                                    !
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If   !!   check consistency
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    If (.Not.error) Then   !!   check Lissajous
       !
       vLissMaxX  =   vars(iIn)%amplitude%x*GV%angleUnit * vars(iIn)%omega%x
       vLissMaxY  =   vars(iIn)%amplitude%y*GV%angleUnit * vars(iIn)%omega%y
       !
       aLissMaxX  =   vars(iIn)%amplitude%x*GV%angleUnit * vars(iIn)%omega%x**2
       aLissMaxY  =   vars(iIn)%amplitude%y*GV%angleUnit * vars(iIn)%omega%y**2
       !
       vLissLimit = 300.0*arcsec
       aLissLimit = 108.0*arcsec
       !
       !D        Write (6,*) "      vLissMaxX:  ", vLissMaxX
       !D        Write (6,*) "      vLissMaxY:  ", vLissMaxY
       !D        Write (6,*) "      aLissMaxX:  ", aLissMaxX
       !D        Write (6,*) "      aLissMaxY:  ", aLissMaxY
       !D        Write (6,*) "      vLissLimit: ", vLissLimit
       !D        Write (6,*) "      aLissLimit: ", aLissLimit
       !
       vLissFacX  = vLissMaxX/vLissLimit
       vLissFacY  = vLissMaxY/vLissLimit
       aLissFacX  = aLissMaxX/aLissLimit
       aLissFacY  = aLissMaxY/aLissLimit
       !
       If (vLissMaxX.Gt.vLissLimit) Then
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("x max. speed = amplitude x [",    F12.1,                    &
               &    " ] * frequency x [",            F12.4,                      &
               &    " ] is too large by factor: ",  F12.3 )' )                   &
               & vars(iIn)%amplitude%x,                                          &
               & vars(iIn)%frequency%x,                                          &
               & vLissFacX                                                       !
          Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("to correct his, try frequency x:  ", F12.4                  &
               &                                          )' )                   &
               & 0.999*vars(iIn)%frequency%x/(vLissFacX)                         !
          Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
          errorInconsistent = .True.
       End If
       !
       If (vLissMaxY.Gt.vLissLimit) Then
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("y max. speed = amplitude y [",    F12.1,                    &
               &    " ] * frequency y [",            F12.4,                      &
               &    " ] is too large by factor: ",  F12.3 )' )                   &
               & vars(iIn)%amplitude%y,                                          &
               & vars(iIn)%frequency%y,                                          &
               & vLissFacY                                                       !
          Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("to correct his, try frequency y:  ", F12.4                  &
               &                                          )' )                   &
               & 0.999*vars(iIn)%frequency%y/(vLissFacY)                         !
          Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
          errorInconsistent = .True.
       End If
       !
       If (aLissMaxX.Gt.aLissLimit) Then
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("x max. acceleration = amplitude y [",  F12.1,               &
               &    " ] * frequency x [",                 F12.4,                 &
               &    " ] ^2 is too large by factor: ",    F12.3 )' )              &
               & vars(iIn)%amplitude%x,                                          &
               & vars(iIn)%frequency%x,                                          &
               & aLissFacX                                                       !
          Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("to correct his, try: frequency x  ", F12.4                  &
               &                                          )' )                   &
               & 0.999*vars(iIn)%frequency%x/sqrt(aLissFacX)                     !
          Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
          errorInconsistent = .True.
       End If
       !
       If (aLissMaxY.Gt.aLissLimit) Then
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("y max. acceleration = amplitude y [",  F12.1,               &
               &    " ] * frequency y [",                 F12.4,                 &
               &    " ] ^2 is too large by factor ",     F12.3 )' )              &
               & vars(iIn)%amplitude%y,                                          &
               & vars(iIn)%frequency%y,                                          &
               & aLissFacY                                                       !
          Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
          Write (messageText, iostat=ierr, fmt =                                 &
               &  '("to correct his, try: frequency y  ", F12.4                  &
               &                                          )' )                   &
               & 0.999*vars(iIn)%frequency%y/sqrt(aLissFacY)                     !
          Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
          errorInconsistent = .True.
       End If
       !
!! TBD:          !D           Write (6,*) "   vars(iIn)%doRampUp   ", vars(iIn)%doRampUp
!! TBD:          !D           Write (6,*) "   vars(iIn)%doRampDown ", vars(iIn)%doRampDown
!! TBD:          !
!! TBD:          If (.Not.vars(iIn)%doRampUp) Then
!! TBD:             Write (messageText,*) "/ramp up [tRamp] is REQUIRED for Lissajous"
!! TBD:             Call pakoMESSAGE(priorityE,severityE,"SUBSCAN",messageText)
!! TBD:             errorInconsistent = .True.
!! TBD:          End If
       !
       !  ** calculate maximum allowed elevation:
       !
       If (.Not.errorInconsistent) Then   !!  calculate maximum allowed elevation
          !
          vLissMaxXel = Acos(vLissMaxX/(2*vLissLimit))
          vLissMaxYel = Acos(vLissMaxY/(2*vLissLimit))
          !
          aLissMaxXel = Acos(aLissMaxX/(2*aLissLimit))  ! <-- corrected aLissLimit on 2010-03-18 
          aLissMaxYel = Acos(aLissMaxY/(2*aLissLimit))  ! <-- corrected aLissLimit on 2010-03-18 
          !
          If (         vars(iIn)%systemName.Eq.offsPako%tru                      &
               &  .Or. vars(iIn)%systemName.Eq.offsPako%hor) Then                !
             lissMaxEl   = min(vLissMaxXel,aLissMaxXel)                          ! use only "x" offsets (Azimuth) for el. condition
          Else
             lissMaxEl   = min(vLissMaxXel,vLissMaxYel,                          &
                  &            aLissMaxXel,aLissMaxYel)                          !
          End If
          !
          !D           Write (6,*) " max el vLissMaxXel: ", vLissMaxXel/deg
          !D           Write (6,*) " max el vLissMaxYel: ", vLissMaxYel/deg
          !D           Write (6,*) " max el aLissMaxXel: ", aLissMaxXel/deg
          !D           Write (6,*) " max el aLissMaxYel: ", aLissMaxYel/deg
          !
          !D           Write (6,*) " max el  lissMaxEl:  ", lissMaxEl/deg
          !
          lissMaxEl  =  lissMaxEl-2.0*deg                                        !!  2 deg safety marging
          !
          lissMaxEl  =  min(lissMaxEl,85.0*deg)                                  !!  max. elevatio: 85 deg
          !D           Write (6,*) "  "
          !D           Write (6,*) " max el  lissMaxEl:  ", lissMaxEl/deg
          !
          !!             If (lissMaxEl.Ge.60.0*deg) Then
          !
          condElevation%isSet     = .True.
          condElevation%maximum   = lissMaxEl
!! OLD !!$          If (condElevation%isSet) Then                                          ! maximum elevation: minimum over subsans
!! OLD !!$             condElevation%maximum   = min(condElevation%maximum,lissMaxEl)
!! OLD !!$          Else
!! OLD !!$             condElevation%isSet     = .True.
!! OLD !!$             condElevation%maximum   = lissMaxEl
!! OLD !!$          End If
          !
          !D           Write (6,*) " condElevation%isSet:   ", condElevation%isSet
          !D           Write (6,*) " condElevation%maximum: ", condElevation%maximum/deg
          !
          Write (messageText, fmt =                                              &
               &  '("WARNING--CONDITION: Elevation must be less than ",          &
               &     F10.2,                                                      &
               &    " [deg] ")' )                                                &
               & condElevation%maximum/deg                                       !
          Call pakoMESSAGE(priorityW,severityW,"LISSAJOUS",messageText)
          !
       End If   !!   calculate maximum allowed elevation
       !
    End If   !!   check Lissajous
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue)  = vars(iTemp)
    End If
    !
    ! *** set "selected" observing mode & plot ***
    If (.Not.error) Then
       GV%observingMode     = OM%Lissajous
       GV%observingModePako = OMpako%Lissajous
       !D        Write (6,*) "   GV%observingMode: ", GV%observingMode
       GV%omSystemName      = vars(iValue)%systemName 
       GV%condElevation     = condElevation
       !D        Write (6,*) "   GV%condElevation: ", GV%condElevation                                                                               
       !
       GV%notReadySWafterOM = .False.
       GV%notReadySecondaryRafterOM = .False.
       !
       GV%notReadyLissajous    = .False.
       LissajousCommand        = line(1:lenc(line))
       Call analyzeLissajous (errorA)
!!$       Call plotLissajous (errorP)
       Call plotDIYlist(errorP)
    End If
    !
    ! *** display values ***
    If (.Not. ERROR) Then
       Call displayLissajous
       Call listDIYlist (error)
    End If
    !
    Return
  End Subroutine LISSAJOUS
!!!
!!!
  Subroutine displayLissajous
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Integer            :: larg
    !
    Include 'inc/display/commandDisplayLissajous.inc'
    !
  End Subroutine displayLissajous
!!!
!!!
  Subroutine analyzeLissajous (errorA)
    !
    ! TBD:  support of several reference positions
    !
    ! <DOCUMENTATION name="analyzeLissajous">
    !
    ! </DOCUMENTATION> <!-- name="analyzeLissajous" -->
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer                :: nScan 
    Character (len=lenCh)  :: aUnit, sUnit
    Integer                :: iSS           = 0     ! counts subcans/segment
    !
    Integer                :: iQuery
    Character(len=lenCh)   :: nameQuery
    Logical                :: isSet
    Type(xyPointType)      :: pointResult
    !
    Integer                :: nSub, nSeg
    Integer                :: nFocus
    !
!!$!! OLD !!$    Integer              :: nScan 
!!$!! OLD !!$    Character (len=12)   :: aUnit, sUnit
!!$!! OLD !!$    Integer              :: iCRO          = 0     ! counts through CRO loop
!!$!! OLD !!$    Integer              :: iCROnext      = 0     !        next iCRO
!!$!! OLD !!$    Integer              :: iSS           = 0     ! counts subcans/segment
!!$!! OLD !!$    Integer              :: iOTF          = 0     ! counts OTF subscans
!!$!! OLD !!$    !
!!$!! OLD !!$    Character (len=1)    :: cCroII        = ''    ! stores previous CRO code    
!!$    !
    !
    !! TBD: fix need for explicit "paKo"
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Lissajous, 1.2.3 2014-08-27 ---> SR: analyzeLissajous ")  ! trace execution
    !
    errorA = .False.
    !
    !D     Write (6,*) " --> modulePakoLissajous: SR analyzeLissajous "
    !
    !D     Write (6,*) "     segList(1): ", segList(1) 
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "      vars(iValue)%doTune :      ", vars(iValue)%doTune
       Write (6,*) "      vars(iValue)%tTune :       ", vars(iValue)%tTune
       Write (6,*) "      vars(iValue)%doFocus :     ", vars(iValue)%doFocus
       Write (6,*) "      vars(iValue)%nFocus :      ", vars(iValue)%nFocus
       Write (6,*) "      vars(iValue)%focusList :   "
       Write (6,*)        vars(iValue)%focusList
       Write (6,*) "   <- vars(iValue)%focusList :   "
    End If
    !
    segList(:) = segDefault                          !!  reset list of segments
    !
    Call countSubscans(value=0)                      !!  reset subscan counter
    Call countSegments(value=0)                      !!  reset segment counter
    !
    ii        = 0
    jj        = 0
    iSS       = 1
    nScan     = 1111
    aUnit     = GV%angleUnitC                        ! 
    sUnit     = GV%speedUnitC                        ! 
    !
!! OLD !!$    nLissajous = 0
!! OLD !!$    !
!! OLD !!$    condElevation%isSet     = .False.
!! OLD !!$    GV%condElevation%isSet  = .False.
    !
    Call getCountSubscans(nSub)
    !D     Write (6,*) "     nSub:   ", nSub
    !
    Call getCountSegments(nSeg)
    !D     Write (6,*) "     nSeg:   ", nSeg
    !
    If (.Not. errorA) Then
       !

       If (vars(ivalue)%doTune) Then
          !
          If (GV%doDebugMessages) Then
             Write (6,*) "  --> Tune Subscan "
             Write (6,*) "      vars(iValue)%doTune : ", vars(iValue)%doTune
             Write (6,*) "      vars(iValue)%tTune :  ", vars(iValue)%tTune
          End If

          Call countSubscans(ii)
          Call countSegments(jj)
          !

   
          If (vars(ivalue)%doRampUp) Then
             !
             !D           Write (6,*) "            Ramp Up Segment"
             !D           Write (6,*) "            tRampUp   ", vars(ivalue)%tRampUp
             !D           Write (6,*) "            tRampDown ", vars(ivalue)%tRampDown
             !
             pStart%x =   vars(iValue)%pCenter%x                                    &
                  &     + vars(iValue)%amplitude%x * sin(vars(iValue)%phase%x)      !
             pStart%y =   vars(iValue)%pCenter%y                                    &
                  &     + vars(iValue)%amplitude%y * sin(vars(iValue)%phase%y)      !
             vStart%x =   vars(iValue)%omega%x                                      & 
                  &     * vars(iValue)%amplitude%x * cos(vars(iValue)%phase%x)      !
             vStart%y =   vars(iValue)%omega%y                                      &   
                  &     * vars(iValue)%amplitude%y * cos(vars(iValue)%phase%y)      !
             vStart%speed = sqrt(vStart%x**2+vStart%y**2)
             !
             accelMax = 0.03*degree                                       !  maximum aceleration in [rad/s^2]
             tAccel   = Int(vStart%speed*GV%angleUnit/accelMax) + 1.0     !  TBD: use max accel. from globals
             !
             If (tAccel.Lt.vars(ivalue)%tRampUp) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to ", vars(ivalue)%tRampUp
                tAccel = vars(ivalue)%tRampUp
             End If
             !
             If (tAccel.Lt.5) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to 5 [sec]"
                tAccel = 5
             End If
             !
             pAccel%x = pStart%x -0.5*vstart%x*tAccel
             pAccel%y = pStart%y -0.5*vstart%y*tAccel
             !
          End If   !!!   If (vars(ivalue)%doRampUp) Then

          segList(jj)%newSubscan =  .True.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  1
          segList(jj)%ssType     =  ss%track
          segList(jj)%segType    =  seg%track
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
          segList(jj)%flagOn     =  .True.
          segList(jj)%flagRef    =  .False.
          !
          segList(jj)%flagDropin =  .False.
          !
          segList(jj)%flagTune   =  vars(iIn)%doTune
          !
          If (vars(ivalue)%doRampUp) Then                                        !  !!  replaced by vars(iValue)%offsetTune
             !
             segList(jj)%pStart     =  pAccel
             !
          Else
             !
             segList(jj)%pStart     =  pStart
             !
          End If                                                                 !
          !
          segList(jj)%pStart     =  vars(iValue)%offsetTune
          !
          segList(jj)%pEnd       =  segList(jj)%pStart
          segList(jj)%speedStart =  0.0
          segList(jj)%speedEnd   =  0.0
          segList(jj)%systemName =  vars(iValue)%systemName
          segList(jj)%altOption  =  'tSegment'
          segList(jj)%tSegment   =  vars(iValue)%tTune
          segList(jj)%tRecord    =  0.1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          segList(jj)%upDateDoppler = .False.
          !
          If (GV%doDebugMessages) Then
             Write (6,*) "        jj, segList(jj): ", jj
             Write (6,*)                          segList(jj)
             Write (6,*) "  <-- Tune Subscan "
          End If

       End If





       If (vars(iValue)%doReference) Then
          !
          !!  ** 
          !D           Write (6,*) "      -> REF 1 before Lissajous "
          !D           Write (6,*) "         vars(iValue)%doReference:    ", vars(iValue)%doReference
          !D           Write (6,*) "         vars(iValue)%offsetR:        ", vars(iValue)%offsetR
          !D           Write (6,*) "         vars(iValue)%systemNameRef:  ", vars(iValue)%systemNameRef
          !D           Write (6,*) "         vars(iValue)%tReference:     ", vars(iValue)%tReference
          !
          Call countSubscans(ii)
          Call countSegments(jj)
          !
          !D           Write (6,*) "       ii: ", ii
          !D           Write (6,*) "       jj: ", jj
          !
          segList(jj)%newSubscan =  .True.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  1
          segList(jj)%ssType     =  ss%track
          segList(jj)%segType    =  seg%track
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
!! OLD !!$          If      (vars(iValue)%croFlag.Eq."O") Then
!! OLD !!$             segList(jj)%flagOn     =  .True.
!! OLD !!$             segList(jj)%flagRef    =  .False.
!! OLD !!$          Else If (vars(iValue)%croFlag.Eq."R") Then
          segList(jj)%flagOn     =  .False.
          segList(jj)%flagRef    =  .True.
!! OLD !!$          Else
!! OLD !!$             segList(jj)%flagOn     =  .True.
!! OLD !!$             segList(jj)%flagRef    =  .False.
!! OLD !!$          End If
          !
          segList(jj)%flagDropin =  .False.
          !
          segList(jj)%pStart     =  vars(iValue)%offsetR
          segList(jj)%pEnd       =  segList(jj)%pStart
          segList(jj)%speedStart =  0.0
          segList(jj)%speedEnd   =  0.0
          segList(jj)%systemName =  vars(iValue)%systemNameRef
          segList(jj)%altOption  =  'tSegment'
          segList(jj)%tSegment   =  vars(iValue)%tReference
          segList(jj)%tRecord    =  0.1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          segList(jj)%upDateDoppler = .False.
          !
          !D           Write (6,*) "        jj, segList(jj): ", jj
          !D           Write (6,*)                          segList(jj)
          !
       End If
       !
       !
       If (  .Not.vars(iValue)%doFocus ) Then
          nFocus = 1
       Else
          nFocus = vars(iValue)%nFocus
       End If
       !
       !D        Write (6,*) "      nFocus:         ",  nFocus
       !
       !D        Write (6,*) "         "
       !D        Write (6,*) "      -> OTF Lissajous "
       !

       Do kk = 1, nFocus, 1

          !!  ** 
          Call countSubscans(ii)
          Call countSegments(jj)
          !
          !D        Write (6,*) "       ii: ", ii
          !D        Write (6,*) "       jj: ", jj
          ! derived parameters positions in GV%angleUnit, speeds in GV%angleUnit/[s]
          ! derived parameters all other in [s], [rad], [rad/s], [rad/s^2]
          !
          !D        Write (6,*) "            doRampUp   ", vars(ivalue)%doRampUp
          !D        Write (6,*) "            doRampDown ", vars(ivalue)%doRampDown
          !
          !! NOTE: following code adapted from modulePakoDIYlist, SR analyzeSubscan
          !
          If (vars(ivalue)%doRampUp) Then
             !
             !D           Write (6,*) "            Ramp Up Segment"
             !D           Write (6,*) "            tRampUp   ", vars(ivalue)%tRampUp
             !D           Write (6,*) "            tRampDown ", vars(ivalue)%tRampDown
             !
             pStart%x =   vars(iValue)%pCenter%x                                    &
                  &     + vars(iValue)%amplitude%x * sin(vars(iValue)%phase%x)      !
             pStart%y =   vars(iValue)%pCenter%y                                    &
                  &     + vars(iValue)%amplitude%y * sin(vars(iValue)%phase%y)      !
             vStart%x =   vars(iValue)%omega%x                                      & 
                  &     * vars(iValue)%amplitude%x * cos(vars(iValue)%phase%x)      !
             vStart%y =   vars(iValue)%omega%y                                      &   
                  &     * vars(iValue)%amplitude%y * cos(vars(iValue)%phase%y)      !
             vStart%speed = sqrt(vStart%x**2+vStart%y**2)
             !
             accelMax = 0.03*degree                                       !  maximum aceleration in [rad/s^2]
             tAccel   = Int(vStart%speed*GV%angleUnit/accelMax) + 1.0     !  TBD: use max accel. from globals
             !
             If (tAccel.Lt.vars(ivalue)%tRampUp) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to ", vars(ivalue)%tRampUp
                tAccel = vars(ivalue)%tRampUp
             End If
             !
             If (tAccel.Lt.5) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to 5 [sec]"
                tAccel = 5
             End If
             !
             pAccel%x = pStart%x -0.5*vstart%x*tAccel
             pAccel%y = pStart%y -0.5*vstart%y*tAccel
             !
             segList(jj)%newSubscan =  .True.
             segList(jj)%scanNumber =  nScan
             segList(jj)%ssNumber   =  jj
             segList(jj)%segNumber  =  1
             segList(jj)%ssType     =  ss%otf
             segList(jj)%segType    =  seg%linear
             segList(jj)%angleUnit  =  aUnit
             segList(jj)%speedUnit  =  sUnit
             !
             !! OLD !!$             If      (vars(iValue)%croFlag.Eq."O") Then
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
             !! OLD !!$             Else If (vars(iValue)%croFlag.Eq."R") Then
             !! OLD !!$                segList(jj)%flagOn     =  .False.
             !! OLD !!$                segList(jj)%flagRef    =  .True.
             !! OLD !!$             Else
             !! OLD !!$                segList(jj)%flagOn     =  .True.
             !! OLD !!$                segList(jj)%flagRef    =  .False.
             !! OLD !!$       End If
             !
             segList(jj)%flagDropin    =  .True.
             !
             segList(jj)%pStart     =  pAccel
             segList(jj)%pEnd       =  pStart
             segList(jj)%lengthOtf  =  Sqrt(                                        &
                  &                 (segList(jj)%pEnd%x-segList(jj)%pStart%x)**2    &
                  &                +(segList(jj)%pEnd%y-segList(jj)%pStart%y)**2 )  !
             segList(jj)%speedStart =  0.0
             segList(jj)%speedEnd   =  1.0001*vStart%speed      !  IMPORTANT: needed to get smoothly "into" Lissajous
             !                                                  !  TBD:       check /optimize
             segList(jj)%systemName =  vars(iValue)%systemName
             segList(jj)%altOption  =  'SPEED'                  !  TBD:       debug/generalize
             segList(jj)%tSegment   =  tAccel
             segList(jj)%tRecord    =  0.1
             !
             !D           Write (6,*) "        jj, segList(jj): ", jj
             !D           Write (6,*)                          segList(jj)
             ! TBD: clever /proper handling of Doppler 
             segList(jj)%upDateDoppler = .False.
             !

             If   (vars(iValue)%doFocus) Then
                segList(jj)%focusOffset     =  vars(iValue)%focusList(kk)
                segList(jj)%directionFocus  =  vars(iValue)%directionFocus
             Else
                segList(jj)%focusOffset     =  0.0
                segList(jj)%directionFocus  =  GPnone
             End If

             !
             Call countSegments(jj)
             !
          End If   !!   vars(ivalue)%doRampUp
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !D       Write (6,*) "       jj: ", jj
          !
          !D        Write (6,*) "             Lissajous Segment"
          !
          segList(jj)%newSubscan =  .False.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  2
          segList(jj)%ssType     =  ss%otf
          segList(jj)%segType    =  seg%lissajous
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
          !! OLD !!$       If      (vars(iValue)%croFlag.Eq."O") Then
          segList(jj)%flagOn     =  .True.
          segList(jj)%flagRef    =  .False.
          !! OLD !!$       Else If (vars(iValue)%croFlag.Eq."R") Then
          !! OLD !!$          segList(jj)%flagOn     =  .False.
          !! OLD !!$          segList(jj)%flagRef    =  .True.
          !! OLD !!$       Else
          !! OLD !!$          segList(jj)%flagOn     =  .True.
          !! OLD !!$          segList(jj)%flagRef    =  .False.
          !! OLD !!$       End If
          !
          segList(jj)%flagDropin    =  .False.
          !
          segList(jj)%pCenter    =  vars(iValue)%pCenter
          segList(jj)%xAmplitude =  vars(iValue)%amplitude%x
          segList(jj)%yAmplitude =  vars(iValue)%amplitude%y
          segList(jj)%omegaX     =  vars(iValue)%omega%x    
          segList(jj)%omegaY     =  vars(iValue)%omega%y    
          segList(jj)%phiX       =  vars(iValue)%phase%x      
          segList(jj)%phiY       =  vars(iValue)%phase%y      
          !
          segList(jj)%systemName =  vars(iValue)%systemName
          segList(jj)%altOption  =  'TOTF'
          segList(jj)%tSegment   =  vars(iValue)%tOtf
          segList(jj)%tRecord    =  0.1
          !
          ! TBD: clever /proper handling of Doppler 
          segList(jj)%upDateDoppler = .False.
          !
          !! OLD !!$       nLissajous = nLissajous+1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !D        Write (6,*) "        jj, segList(jj): ", jj
          !D        Write (6,*)                          segList(jj)
          !

       End Do   !!   1, nFocus, 1

          !
       If (vars(iValue)%doReference) Then
          !
          !!  ** 
          !D           Write (6,*) "      -> REF 2 after Lissajous "
          !D           Write (6,*) "         vars(iValue)%doReference:    ", vars(iValue)%doReference
          !D           Write (6,*) "         vars(iValue)%offsetR:        ", vars(iValue)%offsetR
          !D           Write (6,*) "         vars(iValue)%systemNameRef:  ", vars(iValue)%systemNameRef
          !D           Write (6,*) "         vars(iValue)%tReference:     ", vars(iValue)%tReference
          !
          Call countSubscans(ii)
          Call countSegments(jj)
          !
          !D           Write (6,*) "       ii: ", ii
          !D           Write (6,*) "       jj: ", jj
          !
          segList(jj)%newSubscan =  .True.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  1
          segList(jj)%ssType     =  ss%track
          segList(jj)%segType    =  seg%track
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
!! OLD !!$          If      (vars(iValue)%croFlag.Eq."O") Then
!! OLD !!$             segList(jj)%flagOn     =  .True.
!! OLD !!$             segList(jj)%flagRef    =  .False.
!! OLD !!$          Else If (vars(iValue)%croFlag.Eq."R") Then
          segList(jj)%flagOn     =  .False.
          segList(jj)%flagRef    =  .True.
!! OLD !!$          Else
!! OLD !!$             segList(jj)%flagOn     =  .True.
!! OLD !!$             segList(jj)%flagRef    =  .False.
!! OLD !!$          End If
          !
          segList(jj)%flagDropin =  .False.
          !
          segList(jj)%pStart%x   =  -vars(iValue)%offsetR%x        !!!  note - !!!
          segList(jj)%pStart%y   =  -vars(iValue)%offsetR%y        !!!  note - !!!
          segList(jj)%pEnd       =  segList(jj)%pStart
          segList(jj)%speedStart =  0.0
          segList(jj)%speedEnd   =  0.0
          segList(jj)%systemName =  vars(iValue)%systemNameRef
          segList(jj)%altOption  =  'tSegment'
          segList(jj)%tSegment   =  vars(iValue)%tReference
          segList(jj)%tRecord    =  0.1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          segList(jj)%upDateDoppler = .False.
          !
          !D           Write (6,*) "        jj, segList(jj): ", jj
          !D           Write (6,*)                          segList(jj)
          !
       End If   !!   doReference
       !
    End If    !!  not errorA
    !
    Call getCountSegments(nSegments)
    !
    If (GV%doDebugMessages) Then
       Do ii = 1, nSegments, 1
          !
          Write (6,*) "     ii, segList(ii): ", ii, segList(ii) 
          !
       End Do
    End If
    !
    Return 
    !
  End Subroutine analyzeLissajous
!!!
!! OLD !!$    from analyzeOtf:
!! OLD !!$    nScan     = 1111
!! OLD !!$    aUnit     = GV%angleUnitC
!! OLD !!$    sUnit     = GV%speedUnitC
!! OLD !!$    !
!! OLD !!$    iCRO = 0
!! OLD !!$    iSS  = 0
!! OLD !!$    iOTF = 0
!! OLD !!$    !
!! OLD !!$    cCroII = ''
!! OLD !!$    !
!! OLD !!$    Do While (iOTF .Lt. vars(iValue)%nOtf)
!! OLD !!$       !
!! OLD !!$       iCRO     = iCRO+1
!! OLD !!$       iCROnext = iCRO+1
!! OLD !!$       !
!! OLD !!$       !D           write (6,*) "   ++iCRO: ", iCRO
!! OLD !!$       !
!! OLD !!$       iCRO = Modulo(iCRO,vars(iValue)%croCodeCount)
!! OLD !!$       If (iCRO.Eq.0) iCRO = vars(iValue)%croCodeCount
!! OLD !!$       iCROnext = Modulo(iCROnext,vars(iValue)%croCodeCount)
!! OLD !!$       If (iCROnext.Eq.0) iCROnext = vars(iValue)%croCodeCount
!! OLD !!$       !
!! OLD !!$       !D           write (6,*) "     iCRO:     ", iCRO
!! OLD !!$       !D           write (6,*) "      CRO(",iCRO,"):   ->",                      &
!! OLD !!$       !D     &                 vars(iValue)%croCodeBites(iCRO:iCRO),"<-"
!! OLD !!$       !D           write (6,*) "     iCROnext: ", iCROnext
!! OLD !!$       !D           write (6,*) "      CRO(",iCROnext,"):   ->",                  &
!! OLD !!$       !D     &                 vars(iValue)%croCodeBites(iCROnext:iCROnext),"<-"
!! OLD !!$       !D
!! OLD !!$       !D           write (6,*) "     iSS:  ", iSS
!! OLD !!$       !D           write (6,*) "     iOTF: ", iOTF
!! OLD !!$       !D!
!! OLD !!$       !D           write (6,*) " is C? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."C"
!! OLD !!$       !D           write (6,*) " is c? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."c"
!! OLD !!$       !D           write (6,*) " is R? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."R"
!! OLD !!$       !D           write (6,*) " is r? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."r"
!! OLD !!$       !D           write (6,*) " is O? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."O"
!! OLD !!$       !D           write (6,*) " is o? ", vars(iValue)%croCodeBites(iCRO:iCRO).eq."o"
!! OLD !!$       !
!! OLD !!$       !D           write (6,*) '      cCroII: ', cCroII
!! OLD !!$       !
!! OLD !!$       If (vars(iValue)%croCodeBites(iCRO:iCRO).Eq."C") Then
!! OLD !!$          !
!! OLD !!$          iSS = iSS+1
!! OLD !!$          segList(iSS)            =  segDefault                    ! CAL COLD
!! OLD !!$          segList(iSS)%newSubscan =  .True.
!! OLD !!$          segList(iSS)%scanNumber =  nScan
!! OLD !!$          segList(iSS)%ssNumber   =  iSS
!! OLD !!$          segList(iSS)%segNumber  =  1
!! OLD !!$          segList(iSS)%ssType     =  ss%cc
!! OLD !!$          segList(iSS)%segType    =  seg%track
!! OLD !!$          segList(iSS)%angleUnit  =  aUnit
!! OLD !!$          segList(iSS)%speedUnit  =  sUnit
!! OLD !!$          segList(iSS)%flagOn     =  .False.
!! OLD !!$          segList(iSS)%flagRef    =  .False.
!! OLD !!$          If (vars(iValue)%doReference) Then
!! OLD !!$             segList(iSS)%pStart     =  vars(iValue)%offsetR
!! OLD !!$          Else If (iOTF.Eq.0) Then
!! OLD !!$             segList(iSS)%pStart     =  vars(iValue)%pStart
!! OLD !!$          Else If (vars(iValue)%croCodeBites(iCROnext:iCROnext).Eq."C") Then
!! OLD !!$             segList(iSS)%pStart%x   =  segOtfII%pEnd%x
!! OLD !!$             segList(iSS)%pStart%y   =  segOtfII%pEnd%y
!! OLD !!$          Else If (vars(iValue)%doZigzag) Then
!! OLD !!$             segList(iSS)%pStart%x   =  segOtfII%pEnd%x+vars(iValue)%delta%x
!! OLD !!$             segList(iSS)%pStart%y   =  segOtfII%pEnd%y+vars(iValue)%delta%y
!! OLD !!$          Else
!! OLD !!$             segList(iSS)%pStart%x   =  segOtfII%pStart%x+vars(iValue)%delta%x
!! OLD !!$             segList(iSS)%pStart%y   =  segOtfII%pStart%y+vars(iValue)%delta%y
!! OLD !!$          End If
!! OLD !!$          segList(iSS)%pEnd       =  segList(iSS)%pStart
!! OLD !!$          segList(iSS)%speedStart =  0.0
!! OLD !!$          segList(iSS)%speedEnd   =  0.0
!! OLD !!$          ! TBD: systemName in case of REF sysem .NE. OTF system
!! OLD !!$          segList(iSS)%systemName =  vars(iValue)%systemName
!! OLD !!$          segList(iSS)%altOption  =  'tSegment'
!! OLD !!$          ! TBD: time from CALIBRATE
!! OLD !!$          segList(iSS)%tSegment   =  5.0
!! OLD !!$          segList(iSS)%tRecord    =  vars(iValue)%tRecord
!! OLD !!$          Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$               &           segList(iSS)%ssType
!! OLD !!$          Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$          !
!! OLD !!$          iSS = iSS+1
!! OLD !!$          segList(iSS)            =  segList(iSS-1)                ! CAL AMBIENT
!! OLD !!$          segList(iSS)%ssNumber   =  iSS
!! OLD !!$          segList(iSS)%ssType     =  ss%ca
!! OLD !!$          Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$               &           segList(iSS)%ssType
!! OLD !!$          Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$          !
!! OLD !!$          iSS = iSS+1
!! OLD !!$          segList(iSS)            =  segList(iSS-1)                ! CAL SKY
!! OLD !!$          segList(iSS)%ssNumber   =  iSS
!! OLD !!$          segList(iSS)%ssType     =  ss%cs
!! OLD !!$          Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$               &           segList(iSS)%ssType, segList(iSS)%pStart
!! OLD !!$          Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$          !
!! OLD !!$          cCroII = vars(iValue)%croCodeBites(iCRO:iCRO)
!! OLD !!$          !
!! OLD !!$       End If
!! OLD !!$       !
!! OLD !!$       If (vars(iValue)%doReference .And.                            &
!! OLD !!$            &         vars(iValue)%croCodeBites(iCRO:iCRO).Eq."R") Then
!! OLD !!$          !
!! OLD !!$          iSS = iSS+1
!! OLD !!$          segList(iSS)            =  segDefault                    ! REFERENCE
!! OLD !!$          segList(iSS)%newSubscan =  .True.
!! OLD !!$          segList(iSS)%scanNumber =  nScan
!! OLD !!$          segList(iSS)%ssNumber   =  iSS
!! OLD !!$          segList(iSS)%segNumber  =  1
!! OLD !!$          segList(iSS)%ssType     =  ss%ref
!! OLD !!$          segList(iSS)%segType    =  seg%track
!! OLD !!$          segList(iSS)%angleUnit  =  aUnit
!! OLD !!$          segList(iSS)%speedUnit  =  sUnit
!! OLD !!$          segList(iSS)%flagOn     =  .False.
!! OLD !!$          segList(iSS)%flagRef    =  .True.
!! OLD !!$          segList(iSS)%pStart     =  vars(iValue)%offsetR
!! OLD !!$          segList(iSS)%pEnd       =  vars(iValue)%offsetR
!! OLD !!$          ! TBD: system name for REF
!! OLD !!$          segList(iSS)%systemName =  vars(iValue)%systemName
!! OLD !!$          segList(iSS)%altOption  =  'tSegment'
!! OLD !!$          segList(iSS)%speedStart =  0.0
!! OLD !!$          segList(iSS)%speedEnd   =  0.0
!! OLD !!$          segList(iSS)%tSegment   =  vars(iValue)%tReference
!! OLD !!$          segList(iSS)%tRecord    =  vars(iValue)%tRecord
!! OLD !!$          Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$               &           segList(iSS)%ssType, segList(iSS)%pStart
!! OLD !!$          Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$          !
!! OLD !!$          cCroII = vars(iValue)%croCodeBites(iCRO:iCRO)
!! OLD !!$          !
!! OLD !!$       End If
!! OLD !!$       !
!! OLD !!$       If (vars(iValue)%croCodeBites(iCRO:iCRO).Eq."O") Then
!! OLD !!$          !
!! OLD !!$          iSS  = iSS+1
!! OLD !!$          iOTF = iOTF+1
!! OLD !!$          If (iOTF.Eq.1) Then
!! OLD !!$             segOtfII            =  segDefault                    ! first OTF
!! OLD !!$             segList(iSS)%newSubscan =  .True.
!! OLD !!$             segOtfII%scanNumber =  nScan
!! OLD !!$             segOtfII%ssNumber   =  iSS
!! OLD !!$             segOtfII%segNumber  =  1
!! OLD !!$             segOtfII%ssType     =  ss%otf
!! OLD !!$             segOtfII%segType    =  seg%linear
!! OLD !!$             segOtfII%angleUnit  =  aUnit
!! OLD !!$             segOtfII%speedUnit  =  sUnit
!! OLD !!$             segOtfII%flagOn     =  .True.
!! OLD !!$             segOtfII%flagRef    =  .False.
!! OLD !!$             segOtfII%pStart     =  vars(iValue)%pStart  
!! OLD !!$             segOtfII%pEnd       =  vars(iValue)%pEnd  
!! OLD !!$             segOtfII%lengthOtf  =  vars(iValue)%lengthOtf
!! OLD !!$             segOtfII%systemName =  vars(iValue)%systemName
!! OLD !!$             segOtfII%altOption  =  vars(iValue)%altOption
!! OLD !!$             segOtfII%speedStart =  vars(iValue)%speedStart
!! OLD !!$             segOtfII%speedEnd   =  vars(iValue)%speedEnd
!! OLD !!$             segOtfII%tSegment   =  vars(iValue)%tOtf
!! OLD !!$             segOtfII%tRecord    =  vars(iValue)%tRecord
!! OLD !!$             segList(iSS)        =  segOtfII
!! OLD !!$          Else                                                 ! More OTF
!! OLD !!$             segOtfII%ssNumber   =  iSS
!! OLD !!$             If (vars(iValue)%doZigzag) Then
!! OLD !!$                pII              =  segOtfII%pStart
!! OLD !!$                segOtfII%pStart  =  segOtfII%pEnd
!! OLD !!$                segOtfII%pEnd    =  pII
!! OLD !!$             End If
!! OLD !!$             segOtfII%pStart%x   =  segOtfII%pStart%x+vars(iValue)%delta%x
!! OLD !!$             segOtfII%pStart%y   =  segOtfII%pStart%y+vars(iValue)%delta%y
!! OLD !!$             segOtfII%pEnd%x     =  segOtfII%pEnd%x  +vars(iValue)%delta%x
!! OLD !!$             segOtfII%pEnd%y     =  segOtfII%pEnd%y  +vars(iValue)%delta%y
!! OLD !!$             segList(iSS)        =  segOtfII
!! OLD !!$          End If
!! OLD !!$          !
!! OLD !!$          Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$               &           segList(iSS)%ssType, segList(iSS)%pStart, " to ",       &
!! OLD !!$               &           segList(iSS)%pEnd
!! OLD !!$          Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$          !
!! OLD !!$          cCroII = vars(iValue)%croCodeBites(iCRO:iCRO)
!! OLD !!$          !
!! OLD !!$       End If
!! OLD !!$       !
!! OLD !!$    End Do
!! OLD !!$    !
!! OLD !!$    If (iSS.Ge.1) Then
!! OLD !!$       If (vars(iValue)%systemName.Eq.offs%pro) Then
!! OLD !!$          segList(ii)%updateDoppler = .True.
!! OLD !!$          segList(ii)%pDoppler      = vars(iValue)%offset
!! OLD !!$       Else If (ii.Eq.1) Then
!! OLD !!$       segList(1)%updateDoppler = .True.
!! OLD !!$       segList(1)%pDoppler      = xyPointType(0.0,0.0)
!! OLD !!$    End If
!! OLD !!$    !
!! OLD !!$    nSegments = iSS
!! OLD !!$    !
!! OLD !!$    ii = vars(iValue)%croCodeCount
!! OLD !!$    If (vars(iValue)%doReference                                  &
!! OLD !!$         &         .And.((vars(iValue)%croCodeBites(ii:ii).Eq."R"            &      
!! OLD !!$         &                   .And. (segList(nSegments)%ssType.Eq.ss%otf))    &
!! OLD !!$         &               .Or.                                                &
!! OLD !!$         &               (vars(iValue)%croCodeBites(ii-1:ii).Eq."RC"         &      
!! OLD !!$         &                   .And. (segList(nSegments)%ssType.Eq.ss%otf))    &
!! OLD !!$         &              )                                                    &
!! OLD !!$         &        ) Then
!! OLD !!$       !
!! OLD !!$       iSS  = iSS+1
!! OLD !!$       segList(iSS)            =  segDefault                    ! add REFERENCE at end
!! OLD !!$       segList(iSS)%newSubscan =  .True.
!! OLD !!$       segList(iSS)%scanNumber =  nScan
!! OLD !!$       segList(iSS)%ssNumber   =  iSS
!! OLD !!$       segList(iSS)%segNumber  =  1
!! OLD !!$       segList(iSS)%ssType     =  ss%ref
!! OLD !!$       segList(iSS)%segType    =  seg%track
!! OLD !!$       segList(iSS)%angleUnit  =  aUnit
!! OLD !!$       segList(iSS)%speedUnit  =  sUnit
!! OLD !!$       segList(iSS)%flagOn     =  .False.
!! OLD !!$       segList(iSS)%flagRef    =  .True.
!! OLD !!$       segList(iSS)%pStart     =  vars(iValue)%offsetR  
!! OLD !!$       segList(iSS)%pEnd       =  vars(iValue)%offsetR  
!! OLD !!$       segList(iSS)%systemName =  vars(iValue)%systemName
!! OLD !!$       segList(iSS)%altOption  =  'tSegment'
!! OLD !!$       segList(iSS)%speedStart =  0.0
!! OLD !!$       segList(iSS)%speedEnd   =  0.0
!! OLD !!$       segList(iSS)%tSegment   =  vars(iValue)%tReference
!! OLD !!$       segList(iSS)%tRecord    =  vars(iValue)%tRecord
!! OLD !!$       Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$            &           segList(iSS)%ssType, segList(iSS)%pStart
!! OLD !!$       Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$       !
!! OLD !!$       cCroII = vars(iValue)%croCodeBites(iCRO:iCRO)
!! OLD !!$       !
!! OLD !!$    End If
!! OLD !!$    !
!! OLD !!$    nSegments = iSS
!! OLD !!$    !
!! OLD !!$    ii = vars(iValue)%croCodeCount
!! OLD !!$    If ( vars(iValue)%croCodeBites(ii:ii).Eq."C"                  &      
!! OLD !!$         &         .And. .Not. (segList(nSegments)%ssType.Eq.ss%cs)          &
!! OLD !!$         &        ) Then
!! OLD !!$       !                                                                      ! add CAL at end
!! OLD !!$       iSS  = iSS+1
!! OLD !!$       segList(iSS)            =  segDefault                    ! CAL COLD
!! OLD !!$       segList(iSS)%newSubscan =  .True.
!! OLD !!$       segList(iSS)%scanNumber =  nScan
!! OLD !!$       segList(iSS)%ssNumber   =  iSS
!! OLD !!$       segList(iSS)%segNumber  =  1
!! OLD !!$       segList(iSS)%ssType     =  ss%cc
!! OLD !!$       segList(iSS)%segType    =  seg%track
!! OLD !!$       segList(iSS)%angleUnit  =  aUnit
!! OLD !!$       segList(iSS)%speedUnit  =  sUnit
!! OLD !!$       segList(iSS)%flagOn     =  .False.
!! OLD !!$       segList(iSS)%flagRef    =  .False.
!! OLD !!$       If (vars(iValue)%doReference) Then
!! OLD !!$          segList(iSS)%pStart  =  vars(iValue)%offsetR  
!! OLD !!$       Else
!! OLD !!$          segList(iSS)%pStart  =  segOtfII%pEnd  
!! OLD !!$       End If
!! OLD !!$       segList(iSS)%pEnd       =  segList(iSS)%pStart  
!! OLD !!$       segList(iSS)%speedStart =  0.0
!! OLD !!$       segList(iSS)%speedEnd   =  0.0
!! OLD !!$       ! TBD: case ref system
!! OLD !!$       segList(iSS)%systemName =  vars(iValue)%systemName
!! OLD !!$       segList(iSS)%altOption  =  'tSegment'
!! OLD !!$       ! TBD: time from CALIBRATE
!! OLD !!$       segList(iSS)%tSegment   =  5.0
!! OLD !!$       segList(iSS)%tRecord    =  vars(iValue)%tRecord
!! OLD !!$       Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$            &           segList(iSS)%ssType
!! OLD !!$       Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$       !
!! OLD !!$       iSS = iSS+1
!! OLD !!$       segList(iSS)            =  segList(iSS-1)                ! CAL AMBIENT
!! OLD !!$       segList(iSS)%ssType     =  ss%ca
!! OLD !!$       Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$            &           segList(iSS)%ssType
!! OLD !!$       Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$       !
!! OLD !!$       iSS = iSS+1
!! OLD !!$       segList(iSS)            =  segList(iSS-1)                ! CAL SKY
!! OLD !!$       segList(iSS)%ssType     =  ss%cs
!! OLD !!$       Write (messageText,*)  "  subscan #:   ", iSS,        &
!! OLD !!$            &           segList(iSS)%ssType, segList(iSS)%pStart
!! OLD !!$       Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!! OLD !!$       !
!! OLD !!$       cCroII = vars(iValue)%croCodeBites(iCRO:iCRO)
!! OLD !!$       !
!! OLD !!$    End If
!! OLD !!$    !
!! OLD !!$    nSegments = iSS
!! OLD !!$    !
!! OLD !!$    Write (messageText,*)  "   number of Subscans/Segments: ", nSegments
!! OLD !!$    Call pakoMessage(priorityI,severityI,"LISSAJOUS "//" Analyze",messageText)
!!!
!!!
  Subroutine plotLissajous (errorP)
    !
    !**   Variables  for Plots   ***
    Include 'inc/variables/headerForPlotMethods.inc'
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Lissajous, 1.2.3 2014-10-20 ---> SR: plotLissajous ")     ! trace execution
    !
    errorP = .False.
    !
    Call configurePlots
    !
    Call getCountSegments(nSegments)
    !
    !D     Write (messageText,*)  "  subscans #: 1 to ", nSegments
    !
    If (nSegments.Ge.1) Then
       !
       Do ii = 1, nSegments, 1
          !
          !D           Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType
          !
          If      (segList(ii)%ssType.Eq.ss%otf                                  &  
               &   .And. segList(ii)%segType.Eq.seg%linear) Then                 !  !!  OTF linear
             !
             If (segList(ii)%flagDropin) Then
                Call plotOtfLinearP(                                             &
                     &  segList(ii)%pStart,                                      &
                     &  segList(ii)%pEnd,                                        &
                     &  errorP,                                                  &
                     &  speedStart= segList(ii)%speedStart,                      &
                     &  speedEnd  = segList(ii)%speedEnd,                        &
                     &  tSegment  = segList(ii)%tSegment,                        &
                     &  tSample   = 1/GV%slowRateAMD,                            &
                     &  number    = ii,                                          &
                     &  Type      = "dropin"                                     &
                     &  )                                                        !
             Else
                Call plotOtfLinearP(                                             &
                     &  segList(ii)%pStart,                                      &
                     &  segList(ii)%pEnd,                                        &
                     &  errorP,                                                  &
                     &  speedStart= segList(ii)%speedStart,                      &
                     &  speedEnd  = segList(ii)%speedEnd,                        &
                     &  tSegment  = segList(ii)%tSegment,                        &
                     &  tSample   = 1/GV%slowRateAMD,                            &
                     &  number    = ii)                                          !
             End If   !!   segList(ii)%flagDropin
             !
          Else If      (segList(ii)%ssType.Eq.ss%otf                             &
               &   .And. segList(ii)%segType.Eq.seg%lissajous) Then              !  !!  OTF Lissajous
             !
             !D              Write (6,*) "   --> plotDIYlist: about to call plotOtfLissajous"
             !D              Write (6,*) "                GV%slowRateAMD: ", GV%slowRateAMD
             !
             Call plotOtfLissajousP(                                             &
                  &  segList(ii)%pCenter,                                        &
                  &  segList(ii)%xAmplitude,                                     &
                  &  segList(ii)%yAmplitude,                                     &
                  &  segList(ii)%omegaX,                                         &
                  &  segList(ii)%omegaY,                                         &
                  &  segList(ii)%phiX,                                           &
                  &  segList(ii)%phiY,                                           &
                  &  segList(ii)%tSegment,                                       &
                  &  errorP,                                                     &
                  &  tSample=1/GV%slowRateAMD,                                   &
                  &  number=ii)                                                  !
             !
          Else If (segList(ii)%ssType .Eq.ss%track                               &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagOn) Then                                !  !!  Track ON
             !
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%on)                                        !
             !
          Else If (segList(ii)%ssType .Eq.ss%track                               &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagRef) Then                               !  !!  Track REF
             !
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%ref)                                       !
             !
          End If   !!   segList(ii)%ssType.Eq.ss%otf.And. segList(ii)%segType.Eq.seg%linear
          !        !!   Else ... other segment types
          !
       End Do   !!   ii = 1, nSegments, 1
       !
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'DIY',messageText)
       !
    End If                      !!   nSegments.Ge.1
    !
    If (errorP)                                                                  &
         &        Call pakoMessage(priorityE,severityE,                          &
         &        'Lissajous',' could not plot ')                                !
    !
    Return
    !
  End Subroutine plotLissajous
!!!
!!!
  Subroutine saveLissajous(programName,LINE,commandToSave, iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Lissajous, v 1.2.3 2014-08-28 --> SR: saveLissajous ")      ! trace execution
    !
    ERROR = .False.
    !
    contC = contNN
    !
    Include 'inc/commands/saveCommand.inc'
    !
    doContinue = .True.
    !
    contC = contCC
    !
    Include 'inc/parameters/saveAmplitudeXY.inc'
    !
    Include 'inc/options/saveCenter.inc'
    Include 'inc/options/saveFrequencyXY.inc'
    Include 'inc/options/savePhasesXY.inc'
    Include 'inc/options/saveSystem.inc'
    !
    Include 'inc/options/saveFocus.inc'
    Include 'inc/options/savePointing.inc'
    !
!! TBD    Include 'inc/options/saveReference.inc'
!! TBD    Include 'inc/options/saveTreference.inc'
    !
    If (.Not. vars(iValue)%doTuneSet) Then
       doContinue = .False.
       contC = contCN
    End If
    Include 'inc/options/saveTotf.inc'
    !
    If (vars(iValue)%doTuneSet) Then
       If ( vars(iValue)%doTune) Then
          Include 'inc/options/saveTuneOffsets.inc'
          doContinue = .False.
          contC = contCN
          Include 'inc/options/saveTuneTime.inc'
       Else 
          doContinue = .False.
          contC = contCN
          Include 'inc/options/saveTuneOffsets.inc'
       End If
    End If
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveLissajous
!!!
!!!
  Subroutine startLissajous(programName,LINE,commandToSave, iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    !
    Integer                 :: ii
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Logical                 :: errorL, errorXML
    !
    Logical                 :: inOtfSubscan
    ! 
    ERROR = .False.
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Lissajous, 1.2.2 2013-02-06 ---> SR: startLissajous ")    ! trace execution
    ! 
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",                       &
         &                         comment="save from pako",                     &
         &                         error=errorXML)                               !
    Call pakoXMLwriteStartElement("DESCRIPTION",                                 &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call saveLissajous(programName,LINE,commandToSave, iUnit, errorL)
    !                                                                       
    Call pakoXMLwriteEndElement("DESCRIPTION", error=errorXML)                                             
    Call pakoXMLwriteEndElement("RESOURCE", "pakoScript",error=errorXML)
    !
    Call writeXMLantenna(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLbackend(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/switchingMode.inc'
    !
    Call writeXMLsource(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/generalScanHead.inc'
    !
    Include 'inc/startXML/conditions.inc'
    !
    ! **  Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       Call writeXMLLissajous(programName,LINE,commandToSave,                    &
            &         iUnit, ERROR)                                              !
    End If
    !
    ! **  subscan list to XML
    Include 'inc/startXML/LissajousSubscansXML.inc'
    !   
    Include 'inc/startXML/generalTail.inc'
    !
    Return
    !
  End Subroutine startLissajous
!!!
!!!
  Subroutine writeXMLLissajous(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: ii, iMatch, nMatch
    Character (len=lenCh)   :: valueC, errorCode
    Character (len=lenLine) :: valueComment
    Logical                 :: errorM, errorXML
    Character (len=lenCh)   :: systemName
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Lissajous, 1.2.3 2014-10-14 ---> SR: writeXMLLissajous ") ! trace execution
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/parametersAmplitudeXY.inc'
    !
    Include 'inc/startXML/optionSystemXML.inc'
    Include 'inc/startXML/optionCenterXML.inc'
    Include 'inc/startXML/optionFrequencyXML.inc'
    Include 'inc/startXML/optionPhaseXML.inc'
    !
    Include 'inc/startXML/optionTotfXML.inc'
    !
    Include 'inc/startXML/optionReferenceXML.inc'
    Include 'inc/startXML/optionTreferenceXML.inc'
    !
    systemName = vars(iValue)%systemName
    If (vars(iValue)%doTune) Then 
       Include 'inc/startXML/optionTuneXML.inc'
       Include 'inc/startXML/optionTtuneXML.inc'
    End If
    !
    If (vars(iValue)%doFocus) Then 
       Include 'inc/startXML/optionFocusXML.inc'
    End If
    !
    Include 'inc/startXML/lissajousAnnotationsXML.inc'
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLLissajous
!!!
!!!
End Module modulePakoLissajous
