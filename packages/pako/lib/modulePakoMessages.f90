!     
! Id: modulePakoMessages.f90, v 1.2.4    2015-05-25 Hans Ungerechts
!     <-- patch from          v 1.1.15.1 2014-05-15 on hu-lenny
!         for project names with mixed/upper case, e.g., D015-14 
!         Note: pakoMessage_init may be obsolete anyhow ?
! 
! Id: modulePakoMessages.f90, v 1.2.3   2014-02-17 Hans Ungerechts
! Id: modulePakoMessages.f90, v 1.2.2.1 2014-01-23 Hans Ungerechts
! Id: modulePakoMessages.f90, v 1.2.2   2013-02-15 Hans Ungerechts
! Id: modulePakoMessages.f90, v 1.2.0   2012-02-07 Hans Ungerechts
!    
! <DOCUMENTATION name="modulePakoUtilities">
!
! Pako Utilitiy to handle I W E F "messages".
!
! </DOCUMENTATION>
!
! <DEV> 
! TBD: - more documentation
! <\DEV> 
!
Module modulePakoMessages
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  ! *** Subroutines
  !
  Public :: pakoMessage
  Public :: pakoMessage_init
  Public :: pakoMessage_level
  Public :: pakoMessageSwitch
  !
  Integer :: iPstdio = 0 , iPfile = 0, iUnit = 0
  !
  Integer, Parameter :: lenLine = 256
  !
  Integer, Parameter :: lSeverityCode    = 1
  Integer, Parameter :: nDimSeverityCode = 4
  Character(len=lSeverityCode), Dimension(nDimSeverityCode) :: &
       & sC = (/ 'I','W','E','F' /)
  !
  Logical  ::  pakoMessageOn   =  .True.
  Logical  ::  pakoMessageOff  =  .False.

  Character*12  :: programName = "pako" 

!!!
!!!
Contains
!!!
!!!
  Subroutine pakoMessage (iPriority, iSeverity, package, messageText)
    !
    !   handle message 
    !
    Integer, Intent(in)          :: iPriority   ! "priority"
    Integer, Intent(in)          :: iSeverity   ! "severity" I W E F
    Character(len=*), Intent(in) :: package     ! name of sw package
    Character(len=*), Intent(in) :: messageText ! text of message
    !
    Integer, Parameter   :: lOut = 160
    Character(len=lOut)  :: messageTextOut
    !
    Integer :: iS, nl
    !
    Integer :: length
    !
    length = Len_Trim(messageText)
    !
    !D     Write (6,*) "  "
    !
    !D     Call pako_message(seve%t,programName,                                        &
    !D          &  " module Messages, v 1.2.3 2014-02-17  --> SR: pakoMessage ")        ! trace execution
    !
    !!  If (GV%doDebugMessages) Then
    !D     Write (6,*) "              iPriority:   ", iPriority
    !D     Write (6,*) "              iSeverity:   ", iSeverity
    !D     Write (6,*) "              package: -->", package(1:len_trim(package)), "<--"
    !D     Write (6,*) "              messageText(1:length): -->", messageText(1:length), "<--"
    !D     Write (6,*) "  "
    !D     Write (6,*) "              iPstdio:     ", iPstdio
    !D     Write (6,*) "              iPfile:      ", iPfile
    !!  End If
    !
    iS = Min(Max(iSeverity,1),nDimSeverityCode)
    nl = Min(Len_trim(messageText),lOut)
    !
    messageTextOut = messageText(1:nl)
    nl             = Len(messageTextOut)
    Call Sic_noir(messageTextOut,nl)
    !
    length = Len_Trim(messageTextOut)
    !
    !D     Write (6,*) "              messageTextOut(1:length): -->", messageTextOut(1:length)
    !D     Write (6,*) sC(iS),package,messageTextOut(1:nl)
    !D     Write (6,*) iPriority,sC(iS),package,messageTextOut(1:nl)
    !D     Write (6,*) "using format 101:"
    !D     Write (6,101) iPriority,sC(iS),package,messageTextOut(1:nl)
    !
    ! new 2013-02-06: support turning messages On (for 1st evalutaion)
    !                                     and Off (for 2nd evalutaion)
    If ( pakoMessageOn .And. iPstdio.Le.iPriority ) Then
       !
       !  new 2012-02-07, using general SIC scheme:
       !
       If      (iSeverity.Eq.1) Then
          Call pako_message(seve%i,package,messageTextOut(1:nl)) 
       Else If (iSeverity.Eq.2) Then
          Call pako_message(seve%w,package,messageTextOut(1:nl)) 
       Else If (iSeverity.Eq.3) Then
          Call pako_message(seve%e,package,messageTextOut(1:nl)) 
       Else If (iSeverity.Eq.4) Then
          Call pako_message(seve%f,package,messageTextOut(1:nl)) 
       End If
       !
    End If
    !
!!$    OLD, before v1.2
!!$    If (iPstdio.Le.iPriority) Then
!!$          Write (6,*)  "screen: -->",iPriority,sC(iS),package,messageTextOut(1:nl),"<--"
!!$       Write(6,100) sC(iS),package,messageTextOut(1:nl)
!!$    Endif
!!$    If (iPfile.Le.iPriority) Then
!!$          Write (6,*)  "file:   -->",iPriority,sC(iS),package,messageTextOut(1:nl),"<--"
!!$       Write(iUnit,101) iPriority,sC(iS),package,messageTextOut(1:nl)
!!$    Endif
!!$    !
!!$100 Format(a,'-',a,',  ',a)
!!$101 Format(i2,'-',a,'-',a,',  ',a)
    !
    !D     Call pako_message(seve%t,programName,                                        &
    !D          &  " module Messages,                     <-- SR: pakoMessage ")        ! trace execution    
    !
    Return
  End Subroutine pakoMessage
!!!
!!!
  Subroutine pakoMessage_level(setPstdio,setPfile)
    !
    !   set minimum "level" for messages to write to std I/O and file
    !
    Integer, Intent(in)           :: setPstdio ! set minumum priority for std. I/O
    Integer, Intent(in), Optional :: setPfile  ! set minimum priority for file
    !
    Integer :: iP
    !
!D    Write (6,*) '   --> message_level: '
    !
    iP = Min(Max(setPstdio,0),9)
    iPstdio = iP
    !
    If (Present(setPfile)) iPfile = setPfile
    !
!D    Write (6,*) '       iPstdio: ', iPstdio
!D    Write (6,*) '       iPfile:  ', iPfile
    !
  End Subroutine pakoMessage_level
!!!
!!!
  Subroutine pakoMessage_init(messageFile,iniPstdo,iniPfile)
    !
    ! OBSOLETE (?) with v1.2.0 2012-02-07
    !
    ! open file for messages
    !
    Character(len=*)   :: messageFile  !  name of message file
    Integer            :: iniPstdo     !  init minumum priority for std. I/O
    Integer            :: iniPfile     !  init minimum priority for file
    !
    Integer            :: sic_getlun
    !
    Character(len=256) :: messageFileOLD
    Integer            :: ier
    Integer            :: rename
    !
    Write (6,*)                                                                  &
         &    " module messages, 1.2.4 2015-05-25 ---> SR: pakoMessage_init "    ! trace execution
    Write (6,*)                                                                  &
         &    " --> messageFile: ->"//messageFile(1:Len_trim(messageFile))//"<-" !
    !
!!$    Call pako_message(seve%t,programName,                                        &
!!$         &    " module messages, 1.2.4 2015-05-25 ---> SR: pakoMessage_init ")   ! trace execution
!!$    Call pako_message(seve%d,programName,                                        &
!!$         &    " --> messageFile: ->"//messageFile(1:Len_trim(messageFile))//"<-")!
    !
    !!    Call sic_lower(messageFile)                                            ! !! 2014-05-14   
    !!                                                                           ! !! patch from pako on lenny
    !
    messageFileOLD = messageFile(1:Len_trim(messageFile))//"~"
    !
    ier = Rename(messageFile, messageFileOLD)
    If (ier.Eq.0) Then
!!OLD       Write (6,*)  &
!!OLD            &           'I-message, renamed previous message file: "',  &
!!OLD            &           messageFile(1:Len_trim(messageFile)), &
!!OLD            &           '"'
!!OLD       Write (6,*)  &
!!OLD            &           'I-message, to: "',  &
!!OLD            &           messageFileOLD(1:Len_trim(messageFileOLD)), &
!!OLD            &           '"'
    End If
    !
    ier = sic_getlun(iUnit)
!
!D      Write (6,*) "         unit: iunit = ", iunit
!D      Write (6,*) "         unit: ier =   ", ier
!
    If (ier.Eq.1) Then
       Open (unit=iUnit, file=messageFile,  recl=lenLine, & 
            &           status='unknown')
!D       Write (6,*) 'I-message, opened  message file "', messageFile(1:Len_trim(messageFile)), '"'
    Else
       Write (6,*) 'F-message, error opening message file "', messageFile(1:Len_trim(messageFile)), '"'
    End If
    !
    iPfile  = iniPfile
    iPstdio = iniPstdo
    !
  End Subroutine pakoMessage_init
!!!
!!!
  Subroutine pakoMessageSwitch (setOn, setOff, queryOn)
    !
    Logical, Intent(in),  optional           :: setOn    !  switch pako messages ON
    Logical, Intent(in),  optional           :: setOff   !  switch pako messages OFF
    Logical, Intent(out), optional           :: queryOn  !  query if ON
    !
    pakoMessageOn   =  .True.
    pakoMessageOff  =  .False.
    !
    !D     Write (6,*) "    --> pakoMessageSwitch "
    !
    If (Present(setOff) .And. setOff) Then
       !D        Write (6,*) "         setOff: ", setOff
       pakoMessageOn   =  .False.
       pakoMessageOff  =  .True.
    End If
    If (Present(setOn) .And. setOn) Then
       !D        Write (6,*) "         setOn:  ", setOn
       pakoMessageOn   =  .True.
       pakoMessageOff  =  .False.
    End If
    !
    If (Present(queryOn)) Then
       queryOn = pakoMessageOn
    End If
    !
    !D     Write (6,*) "    <-- pakoMessageSwitch "
    !D     Write (6,*) "       pakoMessageOn:  ", pakoMessageOn
    !D     Write (6,*) "       pakoMessageOff: ", pakoMessageOff
    !
    Return
    !
  End Subroutine pakoMessageSwitch
!!!
!!!
End Module modulePakoMessages



