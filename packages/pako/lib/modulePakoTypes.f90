!
!  Id: modulePakoTypes.f90,v 1.1.2 2010-02-25 Hans Ungerechts
!     
!----------------------------------------------------------------------
!     PAKO 
!
! <DOCUMENTATION name="moduleTypes">
!
! </DOCUMENTATION> <!-- name="moduleTypes" -->
!
Module modulePakoTypes
  !
  Implicit None
  Save
  Private
  !
  Public :: xyPointType, varOffsets, xyVelocityType, conditionMinMax
  !
  Include 'inc/parameters/sizes.inc'
  !
  ! ***   type for xy offset points
  Type    :: xyPointType
     Real              :: x = 0.0
     Real              :: y = 0.0
  End Type xyPointType
  !
  Type :: varOffsets
     !
     Logical                :: isSet        = .False.      ! flag if this offset is set
     Character(len=lenCh)   :: systemName   = "none"       ! system 
     Type(xyPointType)      :: point                       !
     Integer                :: channel      =  0           ! of multibeam
     Real                   :: elevation    = 0.0          ! for NASMYTH
     !
  End Type varOffsets
  !
  ! ***   type for xy offset velocity and speed
  Type    :: xyVelocityType
     Real              :: x     = 0.0
     Real              :: y     = 0.0
     Real              :: speed = 0.0
  End Type xyVelocityType
  !
  !  ***  type for "conditions" (pre-conditions for execution)
  !
  Type     ::  conditionMinMax
     Character(len=lenCh)    ::   parameter  =  " "      !  name of parameter
     Real(kind=4)            ::   minimum    =  0.0
     Real(kind=4)            ::   maximum    =  0.0
     Character(len=lenCh)    ::   unit       =  "--"     !  unit for XML
     Logical                 ::   isSet      = .False.   !  flag if this condition is set (required)
  End Type conditionMinMax
!
!!!
End Module modulePakoTypes





