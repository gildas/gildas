!
! -------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoSwFrequency">
!   <VERSION>
!                Id: modulePakoSwFrequency.f90,v 1.2.3 2014-02-10 Hans Ungerechts
!                Id: modulePakoSwFrequency.f90,v 1.2.2 2013-02-15 Hans Ungerechts
!                    reviewed w/r/t paKo               2012-11-30
!                Id: modulePakoSwFrequency.f90,v 1.2.0 2012-06-14 Hans Ungerechts
!                based on
!                Id: modulePakoSwFrequency.f90,v 1.1.6 2011-06-30 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Switchinging Modes
!   </FAMILY>
!   <SIBLINGS>
!                SwBeam
!                SwFrequency
!                SwTotalPower
!                SwWobbler
!   </SIBLINGS>        
!
!   Pako module for command: SWFREQUENCY
!
!   <NOTES>
!     - to be realized with 2nd reference synthesizer (SYN2)
!     - value of SYN2 is added to LO frequency by PLL
!     - SYN2 is normally set fixed to:        100 MHz
!     - SYN2                    range:  90 to 120 MHz 
!            except for A100 and C150:  80 to 130 MHz
!     - therefore +/- 9 MHz is in range for all SYN2s
!     - LO frequency, and therefore SYN2 is multiplied
!          by 1 for 3mm     --> +/-  9 MHz  (limits in old CS)
!             2 for 2mm     --> +/- 18 MHz
!             3 for 1.3mm   --> +/- 27 MHz
!     - in NCS v++ we could have some what larger FSW  by
!       adjusting first synthesizer (which is also used to 
!                 realize the Doppler correction)
!   </NOTES>
!
! </DOCUMENTATION> <!-- name="modulePakoSwFrequency" -->
!
! -------------------------------------------------------------------------------
!
Module modulePakoSwFrequency
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoReceiver
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: swFrequency
  Public :: displaySwFrequency
  Public :: saveSwFrequency
  Public :: writeXMLswFrequency
  !     
  ! *** Variables for swFrequency ***
  Include 'inc/variables/variablesSwFrequency.inc'
  !     
Contains
!!!
!!!
  Subroutine swFrequency(programName,line,command,error)
    !
    ! *** Arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Real(Kind=kindSingle)   ::  rr
    !
    Include 'inc/ranges/rangesSwFrequency.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module SwFrequency, v 1.2.0 2012-06-14 ---> SR: SwFrequency ")     ! trace execution
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/swFrequencySetDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    Call checkNofParameters(command,                                             &
         &     0, 2,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/options/optionSwfReceiver.inc'
       Include 'inc/parameters/parametersFoffsets.inc'      
       Include 'inc/options/readOptionsSwFrequency.inc'
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/swFrequencySetDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/options/optionSwfReceiver.inc'
       Include 'inc/parameters/parametersFoffsets.inc'
       Include 'inc/options/readOptionsSwFrequency.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** derived parameters                              ***
    !
    If (.Not.ERROR) Then
       !
       rr                     =     vars(iIn)%tPhase                             &
            &                     * vars(iIn)%nPhases                            &
            &                     * vars(iIn)%nCycles                            !
       !
       Write (messageText,*)                                                     &
            &                 " number of phases = nPhases =  ",                 &
            &                 vars(iIn)%nPhases                                  !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            &                 " time per record = tRecord = " //                 &
            &                 " tPhase*nPhases*nCycles = " ,                     &
            &                 rr                                                 !
       If      (rr.Gt.vars(iLimit2)%tRecord) Then
          Call PakoMessage(priorityE,severityE,command,messageText)
       Else If (rr.Gt.vars(iStd2)%tRecord) Then
          Call PakoMessage(priorityW,severityW,command,messageText)
       Else 
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
       !
       Call checkR4("SWF tRecord ",rr,                                           &
            &           vars%tRecord,                                            &
            &           errorR)                                                  !
       !
       If (errorR) Then
          Write (messageText,*)                                                  &
               &         " tRecord = tPhase*nPhases*nCycles "//                  &
               &         " is outside limits "                                   !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       ERROR = ERROR .or. errorR
       !
    End If
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       !
       errorInconsistent = .False.
       !
       Include 'inc/options/checkConsistentSwfReceiver.inc'
       !
       If (GV%backendFTS .And. vars(iIn)%tPhase.Le.0.1-0.001) Then
          errorInconsistent = .True.
          Write (messageText, '(a,F8.2,a,a,a,a)')                                &
                  &   " /tPhase ",            vars(iIn)%tPhase,                  &
                  &   " in ",                 swModePako%Freq,                   &
                  &   " too small for ",      bac%FTS                            !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from Temp into persistent variables   ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
    End If
    !
    ! *** display values  ***
    !     (Note: this is done independantly of the Error status)
    Call displaySwFrequency
    !
    ! *** set "selected" switching mode ***
    If (.Not.error) Then
       GV%switchingMode     = swMode%Freq
       GV%tPhase            = vars(iValue)%tPhase
       GV%tRecord           = vars(iValue)%tRecord
       GV%notReadySWafterOM = .True.
       GV%notReadyFocus     = .True.
       GV%notReadyOnOff     = .True.
       GV%notReadyOtfMap    = .True.
       GV%notReadyPointing  = .True.
       GV%notReadyTrack     = .True.
       !
       Include 'inc/messages/messageSwModeSelected.inc'
       !
       swFrequencyCommand = line(1:lenc(line))
    End If
    !
    Return
  End Subroutine swFrequency
!!!
!!!
  Subroutine displaySwFrequency
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    ! 
    Include 'inc/display/commandDisplaySwFrequency.inc'
    !
  End Subroutine displaySwFrequency
!!!
!!!
  Subroutine saveSwFrequency(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical  ::  isConnected
    !
    contC = contNN
    !
    B = '\'   !  '
    S = ' '
    CMD =  programName//B//"SWFREQUENCY"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    !
    contC = contNN
    !
    Include 'inc/parameters/saveFoffsets.inc'
    !
    contC = contNN
    !
    Include 'inc/options/saveNcycles.inc'
    Include 'inc/options/saveTphase.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveSwFrequency
!!!
!!!
  Subroutine writeXMLswFrequency(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)  :: valueC
    Character (len=lenVar) :: valueComment
    Real                   :: value
    Logical                :: errorXML
    Logical                :: isConnected
    !
    ERROR = .False.
    !
    Include 'inc/startXML/swFrequency.inc'
    !
    Return
  End Subroutine writeXMLswFrequency
!!!
!!!
End Module modulePakoSwFrequency
