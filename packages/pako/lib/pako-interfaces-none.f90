module pako_interfaces_none
  interface
    subroutine pako_pack_set(pack)
      use gpack_def
      !
      type(gpack_info_t), intent(out) :: pack
    End Subroutine pako_pack_set
  end interface
  !
  interface
    Subroutine pako_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! 
      !----------------------------------------------------------------------
      Integer :: gpack_id
      Logical :: error
    End Subroutine pako_pack_init
  end interface
  !
  interface
    Subroutine pako_pack_clean(error)
      !----------------------------------------------------------------------
      ! Called at end of session. Might clean here for example global buffers
      ! allocated during the session
      !----------------------------------------------------------------------
      Logical :: error
    End Subroutine pako_pack_clean
  end interface
  !
  interface
    subroutine pako_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine pako_message_set_id
  end interface
  !
  interface
    subroutine pako_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine pako_message
  end interface
  !
  interface
    Subroutine pako_load
      !
      Use gbl_message
      Use modulePakoGlobalParameters    !  to get pakoVersion string
      Use modulePakoMessages            !
      !
      !---------------------------------------------------------------------
      ! Define and load languages of the package.
      !---------------------------------------------------------------------
      ! Global
    End Subroutine pako_load
  end interface
  !
  interface
    Subroutine pako_run(line,comm,error)
      !---------------------------------------------------------------------
      ! Support routine for the language PAKO.
      ! This routine is able to call the routines associated to each
      ! command of the language PAKO
      !---------------------------------------------------------------------
      !
      Use modulePakoGlobalVariables
      Use modulePakoDisplayText
      Use modulePakoReceiver
      Use modulePakoBackend
      Use modulePakoSwBeam
      Use modulePakoSwFrequency
      Use modulePakoSwWobbler
      Use modulePakoSwTotalPower
      Use modulePakoCatalog
      Use modulePakoSource
      Use modulePakoCalibrate
      Use modulePakoDIYlist
      Use modulePakoFocus
      Use modulePakoOnOff
      Use modulePakoLissajous
      Use modulePakoOtfMap
      Use modulePakoPointing
      Use modulePakoTip
      Use modulePakoTrack
      Use modulePakoVlbi
      Use modulePakoDisplay
      Use modulePakoStart
      Use modulePakoStop
      Use modulePakoSave
      !
    !!   from run*.f < 2011:
    !!$ !!$OLD      Use modulePakoMessages
    !!$ !!$OLD      Use modulePakoGlobalParameters
    !!$ !!$OLD      Use modulePakoGlobalVariables
    !!$ !!$OLD      Use modulePakoDisplayText
    !!$ !!$OLD      Use modulePakoReceiver
    !!$ !!$OLD      Use modulePakoBackend
    !!$ !!$OLD      Use modulePakoSwBeam
    !!$ !!$OLD      Use modulePakoSwFrequency
    !!$ !!$OLD      Use modulePakoSwWobbler
    !!$ !!$OLD      Use modulePakoSwTotalPower
    !!$ !!$OLD      Use modulePakoCatalog
    !!$ !!$OLD      Use modulePakoSource
    !!$ !!$OLD      Use modulePakoCalibrate
    !!$ !!$OLD      Use modulePakoDIYlist
    !!$ !!$OLD      Use modulePakoFocus
    !!$ !!$OLD      Use modulePakoOnOff
    !!$ !!$OLD      Use modulePakoOtfMap
    !!$ !!$OLD      Use modulePakoPointing
    !!$ !!$OLD      Use modulePakoTip
    !!$ !!$OLD      Use modulePakoTrack
    !!$ !!$OLD      Use modulePakoVlbi
    !!$ !!$OLD      Use modulePakoDisplay
    !!$ !!$OLD      Use modulePakoStart
    !!$ !!$OLD      Use modulePakoStop
    !!$ !!$OLD      Use modulePakoSave
      !
      Use gbl_message
      !
      Character(len=*),  Intent(in)    :: line   ! Command line to send to support routines
      Character(len=12), Intent(in)    :: comm   ! Command resolved by the SIC interpreter
      Logical,           Intent(inout) :: error  ! Logical error flag
    End Subroutine pako_run
  end interface
  !
  interface
    Function pako_error()
      !---------------------------------------------------------------------
      ! Support routine for the error status of the library. Does nothing
      ! here.
      !---------------------------------------------------------------------
      Logical :: pako_error
    End Function pako_error
  end interface
  !
  interface
          SUBROUTINE indexCommmandOption                                             &
               (command,option,commandFull,optionFull,                               &
                commandIndex,optionIndex,iCommand,iOption,                           &
                errorNotFound,errorNotUnique)                                        !
    !
    !     <doc>  
    !     SR searches the SIC vocabulary INCLUDED below for 
    !        a unique combination of command and option
    !     INPUT:   command       (substring of) command
    !              option        (substring of) option
    !     OUTPUT:  commandFull   full command as in vocab
    !              optionFull    full option as in vocab
    !              comandIndex   index of command in vocab
    !              optionIndex   index of option in vocab
    !              iCommand      sequential number of command
    !              iOption       number of option
    !                            as required by SIC_I4, SIC_R4 etc.
    !              errorNotFound 
    !              errorNotUnique
    !     <\doc>
    !     <dev>
    !     TBD:    vocabulary should be passed as parameter
    !     TBD:    entry for call with minimal set of parameters
    !     TBD:    replace use of (1:noBlanks(...)) with more general feature
    !     <\dev>
    !
          END SUBROUTINE indexCommmandOption
  end interface
  !
  interface
          INTEGER FUNCTION noBlanks(text)
    !
    !<DOC>  
    !     F  counts the number of non-blank characters in text
    !        starting from the first
    !     INPUT:   text          string
    !     OUTPUT:  noBlanks      
    !<\DOC>
    !<DEV>
    !      TBD:    should also delete blanks at beginning of text
    !      TBD:    should retrun 1 and flag error if there are 
    !              only blanks in text
    !<\DEV>
    !
          END FUNCTION noBlanks
  end interface
  !
end module pako_interfaces_none
