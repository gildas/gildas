!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoSwBeam">
!   <VERSION>
!                Id: modulePakoSwBeam.f90,v 1.2.3 2014-02-10 Hans Ungerechts
!                Id: modulePakoSwBeam.f90,v 1.2.2 2013-02-14 Hans Ungerechts
!                    checked w/r/t pako           2012-11-30
!                Id: modulePakoSwBeam.f90,v 1.2.0 2012-08-14 Hans Ungerechts
!                based on
!                Id: modulePakoSwBeam.f90,v 1.1.6 2011-06-30 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Switchinging Modes
!   </FAMILY>
!   <SIBLINGS>
!                SwBeam
!                SwFrequency
!                SwTotalPower
!                SwWobbler
!   </SIBLINGS>        
!
!   Pako module for command: SWBEAM
!
! </DOCUMENTATION> <!-- name="modulePakoSwBeam" -->
!
!----------------------------------------------------------------------
!
Module modulePakoSwBeam
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: swBeam
  Public :: displaySwBeam
  Public :: saveSwBeam
  Public :: writeXMLswBeam
  !     
  ! *** Variables for swBeam ***
  Include 'inc/variables/variablesSwBeam.inc'
  !
!!!
Contains
!!!
!!!
  Subroutine swBeam(programName,line,command,error)
    !
    ! *** Arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Real(Kind=kindSingle)   ::  rr
    !
    Include 'inc/ranges/rangesSwBeam.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module swBeam, v 1.2.0 2012-08-14 ---> SR: swBeam ")               ! trace execution
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    Call checkNofParameters(command,                                             &
         &     0, 0,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/options/readOptionsSwBeam.inc'
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/options/readOptionsSwBeam.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** derived parameters                              ***
    !
    If (.Not.ERROR) Then
       !
       rr                     =     vars(iIn)%tPhase                             &
            &                     * vars(iIn)%nPhases                            &
            &                     * vars(iIn)%nCycles                            !
       !
       Write (messageText,*)                                                     &
            &                 " number of phases = nPhases =  ",                 &
            &                 vars(iIn)%nPhases                                  !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            &                 " time per record = tRecord = " //                 &
            &                 " tPhase*nPhases*nCycles = " ,                     &
            &                 rr                                                 !
       If      (rr.Gt.vars(iLimit2)%tRecord) Then
          Call PakoMessage(priorityE,severityE,command,messageText)
       Else If (rr.Gt.vars(iStd2)%tRecord) Then
          Call PakoMessage(priorityW,severityW,command,messageText)
       Else 
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
       !
       Call checkR4("SWB tRecord ",rr,                                           &
            &           vars%tRecord,                                            &
            &           errorR)                                                  !
       !
       If (errorR) Then
          Write (messageText,*)                                                  &
               &         " tRecord = tPhase*nPhases*nCycles "//                  &
               &         " is outside limits "                                   !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       ERROR = ERROR .or. errorR
       !
    End If
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       !
       errorInconsistent = .False.
       !
       If (GV%backendFTS .And. vars(iIn)%tPhase.Le.0.1-0.001) Then
          errorInconsistent = .True.
          Write (messageText, '(a,F8.2,a,a,a,a)')                                &
                  &   " /tPhase ",            vars(iIn)%tPhase,                  &
                  &   " in ",                 swModePako%beam,                   &
                  &   " too small for ",      bac%FTS                            !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from Temp into persistent variables   ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
    End If
    !
    ! *** display values ***
    If (.Not. ERROR) Then
       Call displaySwBeam
    End If
    !
    ! *** set "selected" switching mode ***
    If (.Not.error) Then
       GV%switchingMode     = swMode%Beam
       GV%tPhase            = swBeamTphase
       GV%tRecord           = vars(iValue)%tRecord
       GV%notReadySWafterOM = .True.
       GV%notReadyFocus     = .True.
       GV%notReadyOnOff     = .True.
       GV%notReadyOtfMap    = .True.
       GV%notReadyPointing  = .True.
       GV%notReadyTrack     = .True.
       !
       Include 'inc/messages/messageSwModeSelected.inc'
       !
       swBeamCommand = line(1:lenc(line))
    End If
    !
    Return
  End Subroutine swBeam
!!!
!!!
  Subroutine displaySwBeam
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    ! 
    Include 'inc/display/commandDisplaySwBeam.inc'
    !
  End Subroutine displaySwBeam
!!!
!!!
  Subroutine saveSwBeam(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    contC = contNN
    !
    B = '\'  ! '
    S = ' '
    CMD =  programName//B//"SWBEAM"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    Write (iUnit,*) CMD(1:lCMD)
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveSwBeam
!!!
!!!
  Subroutine writeXMLswBeam(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)  :: valueC
    Real                   :: value
    Logical                :: errorXML
    !
    ERROR = .False.
    !
    Include 'inc/startXML/swBeam.inc'
    !
    Return
  End Subroutine writeXMLswBeam
!!!
!!!
End Module modulePakoSwBeam








