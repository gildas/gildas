!
!--------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoPlots.f90">
!   <VERSION>
!
!                Id: modulePakoPlots.f90,v 1.2.3  2014-10-21 Hans Ungerechts
!                Id: modulePakoPlots.f90,v 1.1.13 2012-10-18 Hans Ungerechts
!
!   </VERSION>
!   <HISTORY>
!                
!                
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Plots
!   </FAMILY>
!   <SIBLINGS>

!   </SIBLINGS>        
!
!   Pako module for plots
!
! </DOCUMENTATION> <!-- name="modulePakoPlots.f90" -->
!
!--------------------------------------------------------------------------------
!     
!     
Module modulePakoPlots
  !     
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoGlobalVariables
  Use modulePakoMessages
  Use modulePakoReceiver
  !
  Use gbl_message
  !                                                                                                                                                    Implicit None
  !
  Save
  Private
  !
  Public :: configurePlots
  Public :: plotOtfCurveP,  plotOtfLissajousP,  plotOtfLinearP,    plotTrackP 
  Public :: plotOnOffStart, plotOtfMapStart,    plotPointingStart, plotTrackStart
  Public :: pakoGregExec
  Public :: plotCalSky, plotTrack               !  DEPRECATED
  Public :: plotOtfCircle, plotOtfLinear        !  DEPRECATED
  !     
  ! TBD: all SRs should have a version using Type xyPointType
  !      these versions are indicated by names ...P()
  !      e.g., plotOtfCurveP()
  !
  ! *** Variables  for Plots ***
  Include 'inc/variables/headerForPlotMethods.inc'
  !
  !  ** For plotting curves **
  Integer, Parameter       :: nPoints = 4567
  Real, Dimension(nPoints) :: xCurve,  yCurve
  !
Contains
!!!
!!!
  Subroutine configurePlots
    !     
    Include 'inc/variables/configurePlotMethods.inc'
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: configurePlots ")        ! trace execution
    !     
  End Subroutine configurePlots
!!!
!!!
  Subroutine plotOtfCurveP(pStart,                                               &
       &                   pEnd,                                                 &
       &                   CPstart,                                              &
       &                   CPend,                                                &
       &                   errorP,                                               &
       &                   number, name,   Type)                                 !
    !     
    Type(xyPointType), Intent(IN)          :: pStart 
    Type(xyPointType), Intent(IN)          :: pEnd
    Type(xyPointType), Intent(IN)          :: CPstart 
    Type(xyPointType), Intent(IN)          :: CPend 
    !
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Real :: t
    Real :: xID, yID
    !     
    Real, Dimension(100) :: x, y
    !     
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Integer :: lenc
    !
    !     loop variables:
    Integer :: ii=1
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfCurveP ")         ! trace execution
    !     
    errorP = .False.
    !     
    !D      WRITE (6,*) ' ---> SR plotOtfCurveP (in modulePlots)'
    !
    !D    WRITE (6,*) 'pStart%x = ', pStart%x
    !D    WRITE (6,*) 'pStart%y = ', pStart%y
    !D    WRITE (6,*) 'pEnd%x   = ', pEnd%x
    !D    WRITE (6,*) 'pEnd%y   = ', pEnd%y
    !D    IF (Present(number)) WRITE (6,*) 'number = ', number
    !D    IF (Present(name))   WRITE (6,*) 'name   = ', name
    !D    IF (Present(type))   WRITE (6,*) 'type   = ', type
    !     
    ! ***    Control Points
    !
    Write (pPandO,*)    pCpNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !     
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !     
    Write (pPandO,*)    pCpCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    Write (pPandO,*)    pCpMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Write (pPandO,*)                                                             &
         &        pStart%x ,                                                     &
         &        pStart%y ,                                                     &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        CPstart%x,                                                     &
         &        CPStart%y,                                                     &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !
    Write (pPandO,*)    CPstart%x, CPstart%y,                                    &
         &                       ' ', pCpChar(1:lenc(pCpChar))//'start',         &
         &                       ' 5 /user'                                      !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    Write (pPandO,*)                                                             &
         &        CPend%x,                                                       &
         &        CPEnd%y,                                                       &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        pEnd%x ,                                                       &
         &        pEnd%y ,                                                       &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !
    Write (pPandO,*)    CPend%x, CPend%y,                                        &
         &                       ' ', pCpChar(1:lenc(pCpChar))//'end',           &
         &                       ' 5 /user'                                      !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    ! ***    OTF     
    !
    Write (pPandO,*)    pOtfNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !     
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !     
    Write (pPandO,*)    pOtfCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    Write (pPandO,*)    pOtfMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !     
    !     
    Do ii = 1,100,1
       t     = (Real(ii)-1.0)/(100.0-1.0)
       ! TBD: check math
       x(ii) =       (1.0-t)**3.0       *pStart%x                                &
            &              + 3.0*(1.0-t)**2.0*t     *CPstart%x                   &
            &              + 3.0*(1.0-t)     *t**2.0*CPend%x                     &
            &              +                  t**3.0*pEnd%x                      !
       y(ii) =       (1.0-t)**3.0       *pStart%y                                &
            &              + 3.0*(1.0-t)**2.0*t     *CPstart%y                   &
            &              + 3.0*(1.0-t)     *t**2.0*CPend%y                     &
            &              +                  t**3.0*pEnd%y                      !
       !D          WRITE (6,*) ii, x(ii), y(ii)
    End Do
    !     
    If (Present(number)) Then
       xId    =  x(1)-5*(x(2)-x(1))
       yId    =  y(1)-5*(y(2)-y(1))
       !D       WRITE (6,*) ' xId         = ', xId
       !D       WRITE (6,*) ' yId         = ', yId
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        number,                                                     &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(Type)) Then
       xId    =  x(1)-10*(x(2)-x(1))
       yId    =  y(1)-10*(y(2)-y(1))
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        Type,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(name)) Then
       xId    =  x(1)-15*(x(2)-x(1))
       yId    =  y(1)-15*(y(2)-y(1))
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        name,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    Call gr4_give('X',100,x)
    Call gr4_give('Y',100,y)
    Write (pPandO,*) ' '
    Call pakoGregExec( 'connect'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        x(99),                                                         &
         &        y(99),                                                         &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        x(100),                                                        &
         &        y(100),                                                        &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !     
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !     
    errorP =  errorP .Or. gr_error()
    !     
    If (errorP)                                                                  &
         &        Call pakoMessage(6,2,'plotOtfCurveP',' could not plot ')       !
    !     
    Return
  End Subroutine plotOtfCurveP
!!!
!!!
  Subroutine plotOtfLissajousP(                                                  &
       &  pCenter,                                                               &
       &  xAmplitude,                                                            &
       &  yAmplitude,                                                            &
       &  omegaX,                                                                &
       &  omegaY,                                                                &
       &  phiX,                                                                  &
       &  phiY,                                                                  &
       &  tSegment,                                                              &
       &  errorP,                                                                &
       &  tSample,                                                               &
       &  number, name, Type)                                                    !
    !     
    Type(xyPointType), Intent(IN)          :: pCenter
    Real             , Intent(IN)          :: xAmplitude, yAmplitude
    Real             , Intent(IN)          :: omegaX, omegaY
    Real             , Intent(IN)          :: phiX, phiY
    Real             , Intent(IN)          :: tSegment
    !
    Logical,          Intent(OUT)          :: errorP
    !
    Real            , Intent(IN), Optional :: tSample
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Real :: t
    Real :: xID, yID
    !
    Integer                  :: nSample
    !    
    Real                     :: xS, yS
    !     
    Character(len=lenLine)  ::  messageText
    !
    ! *** functions called: ***
    Logical :: gr_error
    Integer :: lenc
    !
    !     loop variables:
    Integer :: ii=1
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfLissajousP ")     ! trace execution
    !     
    errorP = .False.
    !     
    !D     Write (6,*) ' ---> SR plotOtfLissajousP (in modulePlots)'
    !
    ! ***    OTF Lissajous
    !
    Do ii = 1,nPoints,1
       !
       t = (Real(ii)-1.0)/(nPoints-1.0)*tSegment
       !
       xCurve(ii) = pCenter%x + xAmplitude*sin(omegaX*t+phiX)
       yCurve(ii) = pCenter%y + yAmplitude*sin(omegaY*t+phiY)
       !
    End Do
    !     
    !  **    Control Points
    !
    If (.Not.(gv%plotStyle.Eq.setPlot%simple.Or.gv%plotStyle.Eq.setPlot%off)) Then
       !
       Write (pPandO,*)    pCpNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       !     
       Write (pPandO,*)   'd'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       !     
       Write (pPandO,*)    pCpCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
       !     
       Write (pPandO,*)    pCpMarker
       Call pakoGregExec( 'set marker'     , pPandO, errorP)
       !
       Write (pPandO,*)    pCenter%x, pCenter%y, ' /user'
       Call pakoGregExec( 'draw marker'    , pPandO, errorP)
       !
    End If
    !     
    !  **    Curve
    !
    If (.Not.gv%plotStyle.Eq.setPlot%off) Then
       !
       Write (pPandO,*)    pOtfNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       !     
       Write (pPandO,*)   'd'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       !     
       Write (pPandO,*)    pOtfCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
       !     
       Write (pPandO,*)    pOtfMarker
       Call pakoGregExec( 'set marker'     , pPandO, errorP)
       !     
       Call gr4_give('X',nPoints,xCurve)
       Call gr4_give('Y',nPoints,yCurve)
       Write (pPandO,*) ' '
       Call pakoGregExec( 'connect'  , pPandO, errorP)
    !     
    End If
    !
    !  **    Sample Points
    !
    If (.Not.(gv%plotStyle.Eq.setPlot%simple.Or.gv%plotStyle.Eq.setPlot%off)) Then
       !
       If (Present(tSample)) Then
          nSample = Int(tSegment/tSample)+1
       Else
          nSample = Int(tSegment)+1
       End If
       !
       Write (pPandO,*) pCurveMarker
       Call pakoGregExec( 'set marker'  , pPandO, errorP)
       !
       Do ii = 1,nSample,1
          !
          t = (Real(ii)-1.0)/(nSample-1.0)*tSegment
          !
          xS = pCenter%x + xAmplitude*sin(omegaX*t+phiX)
          yS = pCenter%y + yAmplitude*sin(omegaY*t+phiY)
          !
          If (ii.Le.5.Or.ii.Ge.nSample-5) Then
             Write (messageText, *) "step ",ii,": t x y ", t, xS, yS
             Call pakoMessage(priorityI,severityI,                               &
                  &  "plotOtfLissajous",messageText)                             !
          End If
          !
          If (ii.Eq.2.Or.ii.Eq.(nSample)) Then
             Write (pPandO,*)                                                    &
                  &        xS,                                                   &
                  &        yS,                                                   &
                  &        ' /user'                                              !
             Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
          End If
          !
          Write (pPandO,*)                                                       &
               &        xS,                                                      &
               &        yS,                                                      &
               &        ' /user'                                                 !
          Call pakoGregExec( 'draw marker'  , pPandO, errorP)
          !
       End Do
       !
    End If
    !     
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !     
    errorP =  errorP .Or. gr_error()
    !     
    If (errorP)                                                                  &
         &        Call pakoMessage(6,2,'plotOtfCurve',' could not plot ')        !
    !     
    Return
  End Subroutine plotOtfLissajousP
!!!
!!!
  Subroutine plotOtfLinearP (                                                    &
       &  pStart,                                                                &
       &  pEnd,                                                                  &
       &  errorP,                                                                &
       &  speedStart,                                                            &
       &  speedEnd,                                                              &
       &  tSegment,                                                              &
       &  tSample,                                                               &
       &  number, name,   Type)                                                  !
    !     
    Type(xyPointType), Intent(IN)           :: pStart
    Type(xyPointType), Intent(IN)           :: pEnd
    Logical,           Intent(OUT)          :: errorP
    Real             , Intent(IN), Optional :: speedStart
    Real             , Intent(IN), Optional :: speedEnd
    Real             , Intent(IN), Optional :: tSegment
    Real             , Intent(IN), Optional :: tSample
    Integer,           Intent(IN), Optional :: number
    Character(len=*),  Intent(IN), Optional :: name, Type
    !     
    Real :: t
    Real :: xID, yID
    !
    Integer                  :: nSample
    !    
    Real                     :: length, cosOTF, sinOTF, accel
    Real                     :: xS, yS
    !     
    Character(len=lenLine)  ::  messageText
    !
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Logical       :: testResult1, testResult2
    Real          :: cosDer, sinDer
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfLinearP ")        ! trace execution
    !     
    errorP = .False.
    !     
    !D     Write (6,*) ' ---> SR plotOtfLinearP (in modulePlots)'
    !D     Write (6,*) 'pStart = ', pStart
    !D     Write (6,*) 'pEnd   = ', pEnd
    !
    !D     If (Present(speedStart)) Write (6,*) 'speedStart = ', speedStart
    !D     If (Present(speedEnd))   Write (6,*) 'speedEnd   = ', speedEnd
    !D     If (Present(tSegment))   Write (6,*) 'tSegment   = ', tSegment
    !D     If (Present(tSample))    Write (6,*) 'tSample    = ', tSample
    !
    !D     If (Present(number)) Write (6,*) 'number = ', number
    !D     If (Present(name))   Write (6,*) 'name   = ', name
    !D     If (Present(Type))   Write (6,*) 'type   = ', Type
    !     
    Write (pPandO,*)    pOtfNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    Write (pPandO,*)    pOtfCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    If (Present(Type) .And. Type.Eq.'pointing') Then
       Write (pPandO,*)    pPointNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       Write (pPandO,*)   's'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       Write (pPandO,*)    pPointCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
    End If
    !
    ! Obsolete:     
    If (Present(Type) .And. Type.Eq.'extra') Then
       Write (pPandO,*)    6
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       Write (pPandO,*)   's'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
    End If
    !     
    If (Present(Type) .And. Type.Eq.'dropin') Then
       Write (pPandO,*)    pDropNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
    End If
    !     
    If (Present(number)) Then
       Write (pPandO,*)                                                          &
            &        pStart%x -0.1*(pEnd%x -pStart%x ),                          &
            &        pStart%y -0.1*(pEnd%y -pStart%y ),                          &
            &        number,                                                     &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(Type).And.Type.Ne.'pointing'.And.Type.Ne.'extra') Then
       Write (pPandO,*)                                                          &
            &        pStart%x -0.15*(pEnd%x -pStart%x ),                         &
            &        pStart%y -0.15*(pEnd%y -pStart%y ),                         &
            &        ' ',Type,                                                   &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(name)) Then
       Write (pPandO,*)                                                          &
            &        pStart%x -0.25*(pEnd%x -pStart%x ),                         &
            &        pStart%y -0.25*(pEnd%y -pStart%y ),                         &
            &        name,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    Write (pPandO,*)                                                             &
         &        pStart%x ,                                                     &
         &        pStart%y ,                                                     &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        pEnd%x ,                                                       &
         &        pEnd%y ,                                                       &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !     
    ! ** for HERA
    ! TBD: handle angle units, check derotator system
    !
    Call queryReceiver(rec%HERA1,testResult1)
    Call queryReceiver(rec%HERA2,testResult2)
    If (testResult1) Then
       cosder = Cos(listRX(iHERA1)%derotatorAngle/360.0*2*Pi)
       sinder = Sin(listRX(iHERA1)%derotatorAngle/360.0*2*Pi)
    Else If (testResult2) Then
       cosder = Cos(listRX(iHERA2)%derotatorAngle/360.0*2*Pi)
       sinder = Sin(listRX(iHERA2)%derotatorAngle/360.0*2*Pi)
    End If
    !D Write (6,*) "   cosder = ", cosder
    !D Write (6,*) "   sinder = ", sinder
    !
    If (testResult1 .Or. testResult2) Then
       !
!!$       Do ii = 1,nDimHeraPixel/2,1
!!$          !
!!$          Write (pPandO,*)                                                             &
!!$               &        pStart%x+heraPixel(ii)%offsets%x*cosder                        &
!!$               &                +heraPixel(ii)%offsets%y*sinder ,                      &
!!$               &        pStart%y-heraPixel(ii)%offsets%x*sinder                        &
!!$               &                +heraPixel(ii)%offsets%y*cosder ,                      &
!!$               &        ' /user'
!!$          Call pakoGregExec( 'draw marker'  , pPandO, errorP)
!!$          Write (pPandO,*)                                                             &
!!$               &        pEnd%x  +heraPixel(ii)%offsets%x*cosder                        &
!!$               &                +heraPixel(ii)%offsets%y*sinder ,                      &
!!$               &        pEnd%y  -heraPixel(ii)%offsets%x*sinder                        &
!!$               &                +heraPixel(ii)%offsets%y*cosder ,                      &
!!$               &        ' /user'
!!$          Call pakoGregExec( 'draw line'  , pPandO, errorP)
!!$          !
!!$       End Do
       !
    End If
    !
    !  **    Sample Points
    !
    If (   .Not.(gv%plotStyle.Eq.setPlot%simple.Or.gv%plotStyle.Eq.setPlot%off)  &
         & .And.Present(speedStart)                                              &
         & .And.Present(speedEnd)                                                &
         & .And.Present(tSegment)                                                &
         & ) Then                                                                !
       !
       If (Present(tSegment).And.Present(tSample)) Then
          nSample = Int(tSegment/tSample)+1
       Else If (Present(tSegment)) Then
          nSample = Int(tSegment)+1
       Else
          nSample = 0
       End If
       !
       !D        Write (6,*) "      nSample: ", nSample
       !
       Write (pPandO,*) pCurveMarker
       Call pakoGregExec( 'set marker'  , pPandO, errorP)
       !
       length = sqrt((pEnd%x-pStart%x)**2 + (pEnd%y-pStart%y)**2)
       cosOTF = (pEnd%x-pStart%x)/length
       sinOTF = (pEnd%y-pStart%y)/length
       accel  = (speedEnd-speedStart)/tSegment
       !
       Do ii = 1,nSample,1
          !
          t = (Real(ii)-1.0)/(nSample-1.0)*tSegment
          !
          xS = pStart%x + cosOTF* 0.5*accel*t**2
          yS = pStart%y + sinOTF* 0.5*accel*t**2
          !
          If (ii.Le.3.Or.ii.Ge.nSample-3) Then
             Write (messageText, *) "step ",ii,": t x y ", t, xS, yS
             Call pakoMessage(priorityI,severityI,                               &
                  &  "plotOtfLinear",messageText)                                !
          End If
          !
          If (ii.Eq.2.Or.ii.Eq.(nSample)) Then
             Write (pPandO,*)                                                    &
                  &        xS,                                                   &
                  &        yS,                                                   &
                  &        ' /user'                                              !
             Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
          End If
          !
          Write (pPandO,*)                                                       &
               &        xS,                                                      &
               &        yS,                                                      &
               &        ' /user'                                                 !
          Call pakoGregExec( 'draw marker'  , pPandO, errorP)
          !
       End Do
       !
    End If
    !
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !     
    errorP =  errorP .Or. GR_ERROR()
    !     
    If (errorP) Call pakoMessage(priorityW,severityW,                            &
         &                      'plotOtfLinearP',' could not plot ')             !
    !     
    Return
  End Subroutine plotOtfLinearP
!!!
!!!
  Subroutine plotTrackP   (pStart,                                               &
       &                   errorP,                                               &
       &                   number, name,   Type)                                 !
    !     
    Type(xyPointType), Intent(IN)           :: pStart 
    Logical,           Intent(OUT)          :: errorP
    Integer,           Intent(IN), Optional :: number
    Character(len=*),  Intent(IN), Optional :: name, Type
    !     
    ! *** functions called: ***
    Logical       :: gr_error
    Integer       :: lenc
    !
    Logical       :: testResult1, testResult2
    Real          :: cosDer, sinDer
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotTrackP ")            ! trace execution
    !     
    errorP = .False.
    !     
    If (Type.Eq.ss%on) Then
       !     
       Write (pPandO,*)   'd'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       !
       Write (pPandO,*)    pOnNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       Write (pPandO,*)    pOnCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
       Write (pPandO,*)    pOnMarker
       Call pakoGregExec( 'set marker'     , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y,                                   &
            &                       ' ', pOnChar, ' 5 /user'                     !
       Call pakoGregExec( 'draw text'      , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y, ' /user'
       Call pakoGregExec( 'draw marker'    , pPandO, errorP)
       !
       ! ** for HERA
       ! TBD: handle angle units, check derotator system
       !
       Call queryReceiver(rec%HERA1,testResult1)
       Call queryReceiver(rec%HERA2,testResult2)
       If (testResult1) Then
          cosder = Cos(listRX(iHERA1)%derotatorAngle/360.0*2*Pi)
          sinder = Sin(listRX(iHERA1)%derotatorAngle/360.0*2*Pi)
       Else If (testResult2) Then
          cosder = Cos(listRX(iHERA2)%derotatorAngle/360.0*2*Pi)
          sinder = Sin(listRX(iHERA2)%derotatorAngle/360.0*2*Pi)
       End If
       !D Write (6,*) "   cosder = ", cosder
       !D Write (6,*) "   sinder = ", sinder
       !
       If (testResult1 .Or. testResult2) Then
          !
          Write (pPandO,*)    pOnNpen
          Call pakoGregExec( 'pen'            , pPandO, errorP)
          Write (pPandO,*)    pPixCharSize
          Call pakoGregExec( 'set character'  , pPandO, errorP)
          Write (pPandO,*)    pPixMarker
          Call pakoGregExec( 'set marker'     , pPandO, errorP)
          !
!!$          Do ii = 1,nDimHeraPixel/2,1
!!$             Write (pPandO,*) pStart%x+heraPixel(ii)%offsets%x*cosder            &
!!$                  &                   +heraPixel(ii)%offsets%y*sinder,           &
!!$                  &           pStart%y-heraPixel(ii)%offsets%x*sinder            &
!!$                  &                   +heraPixel(ii)%offsets%y*cosder, ' /user'
!!$             Call pakoGregExec( 'draw marker'    , pPandO, errorP)
!!$          End Do
          !
       End If
       !
    End If
    !
    If (Type.Eq.ss%ref) Then
       !     
       Write (pPandO,*)    pRefNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       !
       Write (pPandO,*)   'd'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       !
       Write (pPandO,*)    pRefCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
       !
       Write (pPandO,*)    pRefMarker
       Call pakoGregExec( 'set marker'     , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y,                                   &
            &                       ' ', pRefChar, ' 5 /user'                    !
       Call pakoGregExec( 'draw text'      , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y, ' /user'
       Call pakoGregExec( 'draw marker'    , pPandO, errorP)
       !
    End If
    !
    If (Type.Eq.ss%tune) Then
       !
       !D  Write (6,*) "        -> plot Track /Tune "
       !
       Write (pPandO,*)    pTuneNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       !
       Write (pPandO,*)   'd'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       !
       Write (pPandO,*)    pTuneCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
       !
       Write (pPandO,*)    pTuneMarker
       !D  Write (6,*)        "    pPandO: ->", pPandO, "<-"
       Call pakoGregExec( 'set marker'     , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y,                                   &
            &                       ' ', pTuneChar, ' 5 /user'                   !
       Call pakoGregExec( 'draw text'      , pPandO, errorP)
       !
       Write (pPandO,*)    pStart%x, pStart%y, ' /user'
       Call pakoGregExec( 'draw marker'    , pPandO, errorP)
       !
    End If
    !
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !
    errorP =  errorP .Or. GR_ERROR()
    !
    If (errorP)                                                                  &
         &    Call PakoMessage(priorityW,severityW,'Track',' could not plot ')   !
    !
    Return
    !
  End Subroutine plotTrackP
!!!
!!!
  Subroutine plotOnOffStart(                                                     &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: OnOffStart ")            ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pOnNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    pOnCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pOnMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Return
    !
  End Subroutine plotOnOffStart
!!!
!!!
  Subroutine plotOtfMapStart(                                                    &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfMapStart ")       ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pOtfNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    pOtfCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pOtfMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Return
    !
  End Subroutine plotOtfMapStart
!!!
!!!
  Subroutine plotPointingStart(                                                  &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotPointingStart ")     ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pPointNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    0.5
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
!!$         write (6,*) "pPointChar ", pPointChar
    !
    Write (pPandO,*)    0.0, 0.0,                                                &
         &                       ' ', pPointChar, ' 5 /user'                     !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    Write (pPandO,*)    pPointCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pPointMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Return
    !
  End Subroutine plotPointingStart
!!!
!!!
  Subroutine plotTrackStart(                                                     &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotTrackStart ")        ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pOnNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    pOnCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pOnMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Return
    !
  End Subroutine plotTrackStart
!!!
!!!
  Subroutine pakoGregExec(plotCommand, plotPandO, errorPlot)
    !     
    Character(len=*),  Intent(IN)   :: plotCommand, plotPandO
    Logical,           Intent(OUT)  :: errorPlot
    !     
    ! *** functions called: ***
    Logical       :: gr_error
    Integer       :: lenc
    !     
    ! *** local variables: ***
    Integer       :: lc, lp, lcp
    Character(len=512) :: plotCPandO
    !     
    !D     Call pako_message(seve%t,"paKo",                                             &
    !D          &    " module Plots, 1.2.3 2014-10-20 ---> SR: pakoGregExec ")          ! trace execution
    !     
    lc = lenc(plotCommand)
    lp = lenc(plotPandO)
    !     
    plotCPandO = plotCommand
    Write (plotCPandO(lc+2:),*) plotPandO(1:lp)
    lcp = lenc(plotCPandO)
    !     
    !D    write (6,*) " ---> SR pakoGregExec (in modulePlots): "
    !D    write (6,*) "   ->",plotCPandO(1:lcp),"<-  "
    !     
    Call gr_exec(plotCPandO(1:lcp))
    !     
    errorPlot =  errorPlot .Or. gr_error()
    !     
    Return
  End Subroutine pakoGregExec
!!!
!!! The remaining Subroutines are DEPRECATED
!!!
  Subroutine plotCalSky(    xStart, yStart,                                      &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    ! DEPRECATED
    !
    Real,             Intent(IN)           :: xStart, yStart
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotCalSky DEPRECATED ") ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pCalNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    0.5
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pCalMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Write (pPandO,*)    xStart, yStart,                                          &
         &                       ' ', pCalChar, ' 5 /user'                       !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    Write (pPandO,*)    xStart, yStart, ' /user'
    Call pakoGregExec( 'draw marker'    , pPandO, errorP)
    !
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !
    errorP =  errorP .Or. GR_ERROR()
    !
    If (errorP)                                                                  &
         &        Call PakoMessage(6,2,'Calibrate',' could not plot ')           !
    !
    Return
    !
  End Subroutine plotCalSky
!!!
!!!
  Subroutine plotOtfCircle (xStart, yStart,                                      &
       &                          xEnd,   yEnd,   turn, errorP,                  &
       &                          number, name,   Type)                          !
    !
    ! DEPRECATED
    !     
    Real,             Intent(IN)           :: xStart, yStart
    Real,             Intent(IN)           :: xEnd, yEnd
    Real,             Intent(IN)           :: turn
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    Real :: xId, yId
    Real :: xDelta, yDelta 
    Real :: xDistance, xRadius
    Real :: alpha, lambda, aStart, aEnd, xcen, ycen 
    !     
    Real, Dimension(100) :: x, y
    !     
    !     loop variables:
    Integer :: ii=1
    !
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Integer :: lenc
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfCircle "          &
         & // " DEPRECATED ")                                                    ! trace execution
    !     
    errorP = .False.
    !     
    !D    WRITE (6,*) ' ---> SR plotOtfCircle (in modulePlots)'
    !D    WRITE (6,*) 'xStart = ', xStart
    !D    WRITE (6,*) 'yStart = ', yStart
    !D    WRITE (6,*) 'xEnd   = ', xEnd
    !D    WRITE (6,*) 'yEnd   = ', yEnd
    !D    WRITE (6,*) 'turn   = ', turn
    !D    IF (Present(number)) WRITE (6,*) 'number = ', number
    !D    IF (Present(name))   WRITE (6,*) 'name   = ', name
    !D    IF (Present(type))   WRITE (6,*) 'type   = ', type
    !     
    !
    !     
    xDelta = xEnd-xStart
    yDelta = yEnd-yStart
    !     
    !D       WRITE (6,*) ' xEnd   = ', xEnd
    !D       WRITE (6,*) ' yEnd   = ', yEnd
    !D       WRITE (6,*) ' xDelta = ', xDelta
    !D       WRITE (6,*) ' yDelta = ', yDelta
    !     
    !!  we need lambda = the argument of (xEnd-xStart)+i*(yEnd-yStart)
    !!  in Mathematica this is ARcTan[xEnd-xStart,yEnd-yStart]
    !!  but with the simple-minded Atan(y/x) we have to be careful:
    !     
    If (xDelta.Gt.0.0.And.yDelta.Ge.0) Then
       lambda = Atan(yDelta/xDelta)
    End If
    !     
    If (xDelta.Gt.0.0.And.yDelta.Lt.0) Then
       lambda = 2*Pi+Atan(yDelta/xDelta)
    End If
    !     
    If (xDelta.Eq.0.0.And.yDelta.Gt.0) Then
       lambda = Pi/2
    End If
    !     
    If (xDelta.Eq.0.0.And.yDelta.Lt.0) Then
       lambda = 3*Pi/2
    End If
    !     
    If (xDelta.Lt.0.0) Then
       lambda = Pi+Atan(yDelta/xDelta)
    End If
    !     
    alpha     = turn*degs
    xDistance = Sqrt((xEnd-xStart)**2+(yEnd-yStart)**2)
    xRadius   = (xDistance/2.0)/Sin(turn/2.0*degs)
    aStart    = lambda-Pi/2.0-alpha/2.0
    aEnd      = lambda-Pi/2.0+alpha/2.0
    xcen      = xStart-xRadius*Cos(aStart)
    ycen      = yStart-xRadius*Sin(aStart)
    !     
    ! ***    Control Points: Center
    !
    Write (pPandO,*)    pCpNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !     
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !     
    Write (pPandO,*)    pCpCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    Write (pPandO,*)    pCpMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Write (pPandO,*)    xCen, yCen,                                              & 
         &                       ' ', pCpChar(1:lenc(pCpChar))//'center',        &
         &                       ' 5 /user'                                      !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    Write (pPandO,*)    xCen, yCen, ' /user'
    Call pakoGregExec( 'draw marker'    , pPandO, errorP)
    !
    Write (pPandO,*)                                                             &
         &        xStart ,                                                       &
         &        yStart ,                                                       &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        xCen,                                                          &
         &        yCen,                                                          &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw line'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        xEnd,                                                          &
         &        yEnd,                                                          &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw line'  , pPandO, errorP)
    !
    !D       WRITE (6,*) ' lambda   = ', lambda
    !D       WRITE (6,*) ' turn     = ', turn
    !D       WRITE (6,*) ' degs     = ', degs
    !D       WRITE (6,*) ' turn     = ', turn
    !D       WRITE (6,*) ' alpha    = ', alpha
    !D       WRITE (6,*) ' xDistance   = ', xDistance
    !D       WRITE (6,*) ' xRadius     = ', xRadius  
    !D       WRITE (6,*) ' aStart      = ', aStart
    !D       WRITE (6,*) ' aEnd        = ', aEnd
    !D       WRITE (6,*) ' xcen         = ', xcen
    !D       WRITE (6,*) ' ycen         = ', ycen
    !     
    ! ***    OTF     
    !
    Do ii = 1,100,1
       x(ii) = xcen+xRadius*Cos(aStart+((ii-1.)/99.)*alpha)
       y(ii) = ycen+xRadius*Sin(aStart+((ii-1.)/99.)*alpha)
       !D          WRITE (6,*) ii, x(ii), y(ii)
    End Do
    !     
    Write (pPandO,*)    pOtfNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !     
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !     
    Write (pPandO,*)    pOtfCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    Write (pPandO,*)    pOtfMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    If (Present(number)) Then
       xId    =  x(1)-5*(x(2)-x(1))
       yId    =  y(1)-5*(y(2)-y(1))
       !D       WRITE (6,*) ' xId         = ', xId
       !D       WRITE (6,*) ' yId         = ', yId
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        number,                                                     &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(Type)) Then
       xId    =  x(1)-10*(x(2)-x(1))
       yId    =  y(1)-10*(y(2)-y(1))
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        Type,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(name)) Then
       xId    =  x(1)-15*(x(2)-x(1))
       yId    =  y(1)-15*(y(2)-y(1))
       Write (pPandO,*)                                                          &
            &        xId,                                                        &
            &        yId,                                                        &
            &        name,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    Call gr4_give('X',100,x)
    Call gr4_give('Y',100,y)
    Write (pPandO,*) ' '
    Call pakoGregExec( 'connect'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        x(99),                                                         &
         &        y(99),                                                         &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        x(100),                                                        &
         &        y(100),                                                        &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !     
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !     
    errorP =  errorP .Or. gr_error()
    !     
    If (errorP)                                                                  &
         &        Call pakoMessage(6,2,'plotOtfCircle',' could not plot ')       !
    !     
    Return
  End Subroutine plotOtfCircle
!!!
!!!
  Subroutine plotOtfLinear (xStart, yStart,                                      &
       &                          xEnd,   yEnd,   errorP,                        &
       &                          number, name,   Type)                          !
    !
    ! DEPRECATED
    !     
    Real,             Intent(IN)           :: xStart, yStart
    Real,             Intent(IN)           :: xEnd, yEnd
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotOtfLinear "          &
         & // " DEPRECATED ")                                                    ! trace execution
    !     
    errorP = .False.
    !     
    !D    WRITE (6,*) ' ---> SR plotOtfLinear (in modulePlots)'
    !D    WRITE (6,*) 'xStart = ', xStart
    !D    WRITE (6,*) 'yStart = ', yStart
    !D    WRITE (6,*) 'xEnd   = ', xEnd
    !D    WRITE (6,*) 'yEnd   = ', yEnd
    !D    IF (Present(number)) WRITE (6,*) 'number = ', number
    !D    IF (Present(name))   WRITE (6,*) 'name   = ', name
    !D    IF (Present(type))   WRITE (6,*) 'type   = ', type
    !     
    Write (pPandO,*)    pOtfNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    Write (pPandO,*)    pOtfCharSize
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !     
    If (Present(Type) .And. Type.Eq.'pointing') Then
       Write (pPandO,*)    pPointNpen
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       Write (pPandO,*)   's'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
       Write (pPandO,*)    pPointCharSize
       Call pakoGregExec( 'set character'  , pPandO, errorP)
    End If
    !     
    If (Present(Type) .And. Type.Eq.'extra') Then
       Write (pPandO,*)    6
       Call pakoGregExec( 'pen'            , pPandO, errorP)
       Write (pPandO,*)   's'
       Call pakoGregExec( 'set font'       , pPandO, errorP)
    End If
    !     
!!$          WRITE (pPandO,*)    0.0, 0.0,
!!$      &                       ' ', pOtfChar, ' 5 /user'
!!$          CALL pakoGregExec( 'draw text'      , pPandO, errorP)
!!$          WRITE (pPandO,*)    pOtfMarker
!!$          CALL pakoGregExec( 'set marker'     , pPandO, errorP)
!!$!     
    If (Present(number)) Then
       Write (pPandO,*)                                                          &
            &        xStart -0.1*(xEnd -xStart ),                                &
            &        yStart -0.1*(yEnd -yStart ),                                &
            &        number,                                                     &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(Type).And.Type.Ne.'pointing'.And.Type.Ne.'extra') Then
       Write (pPandO,*)                                                          &
            &        xStart -0.15*(xEnd -xStart ),                               &
            &        yStart -0.15*(yEnd -yStart ),                               &
            &        ' ',Type,                                                   &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    If (Present(name)) Then
       Write (pPandO,*)                                                          &
            &        xStart -0.25*(xEnd -xStart ),                               &
            &        yStart -0.25*(yEnd -yStart ),                               &
            &        name,                                                       &
            &        ' /user'                                                    !
       Call pakoGregExec( 'draw text'  , pPandO, errorP)
    End If
    !     
    Write (pPandO,*)                                                             &
         &        xStart ,                                                       &
         &        yStart ,                                                       &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw relocate'  , pPandO, errorP)
    Write (pPandO,*)                                                             &
         &        xEnd ,                                                         &
         &        yEnd ,                                                         &
         &        ' /user'                                                       !
    Call pakoGregExec( 'draw arrow'  , pPandO, errorP)
    !     
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !     
    errorP =  errorP .Or. GR_ERROR()
    !     
    If (errorP)                                                                  &
         &        Call pakoMessage(6,2,'plotOtfLinear',' could not plot ')       !
    !     
    Return
  End Subroutine plotOtfLinear
!!!
!!!
  Subroutine plotTrack(    xStart, yStart,                                       &
       &                          errorP,                                        &
       &                          number, name,   Type)                          !
    !
    ! DEPRECATED
    !
    Real,             Intent(IN)           :: xStart, yStart
    Logical,          Intent(OUT)          :: errorP
    Integer,          Intent(IN), Optional :: number
    Character(len=*), Intent(IN), Optional :: name, Type
    !     
    ! *** functions called: ***
    Logical       :: gr_error
    !
    Call pako_message(seve%t,"paKo",                                             &
         &    " module Plots, 1.2.3 2014-10-20 ---> SR: plotTrack "              &
         & // " DEPRECATED ")                                                    ! trace execution
    !     
    errorP = .False.
    !     
    Write (pPandO,*)    pRefNpen
    Call pakoGregExec( 'pen'            , pPandO, errorP)
    !
    Write (pPandO,*)   'd'
    Call pakoGregExec( 'set font'       , pPandO, errorP)
    !
    Write (pPandO,*)    0.5
    Call pakoGregExec( 'set character'  , pPandO, errorP)
    !
    Write (pPandO,*)    pRefMarker
    Call pakoGregExec( 'set marker'     , pPandO, errorP)
    !
    Write (pPandO,*)    xStart, yStart,                                          &
         &                       ' ', pRefChar, ' 5 /user'                       !
    Call pakoGregExec( 'draw text'      , pPandO, errorP)
    !
    Write (pPandO,*)    xStart, yStart, ' /user'
    Call pakoGregExec( 'draw marker'    , pPandO, errorP)
    !
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !
    errorP =  errorP .Or. GR_ERROR()
    !
    If (errorP)                                                                  &
         &        Call PakoMessage(6,2,'Track',' could not plot ')               !
    !
    Return
    !
  End Subroutine plotTrack
!!!
!!!
End Module modulePakoPlots
