!
!
! Id: modulePakoBackend.f90,v 1.3.0  2017-09-06 Hans Ungerechts
!                                    pulsar sampling
! Id: modulePakoBackend.f90,v 1.2.3  2014-10-23 Hans Ungerechts
! Id: modulePakoBackend.f90,v 1.2.3  2014-04-29 Hans Ungerechts
!                                    holography
!
! Id: modulePakoBackend.f90,v 1.2.3  2013-08-27, 28 Hans Ungerechts
!                                    upgrade of E150 to H&V 2SB LO LI UI UO
!
! Id: modulePakoBackend.f90,v 1.2.3  2013-03-01 Hans Ungerechts
!     review against pako            2012-11-20
!     modulePakoBackend.f90,v 1.1.12 2012-02-20 Hans Ungerechts
!
!     modulePakoBackend.f90,v 1.2.2  2012-08-22 Hans Ungerechts
!     modulePakoBackend.f90,v 1.2.1  2012-06-13 Hans Ungerechts
!     based on:
!     modulePakoBackend.f90,v 1.1.12 2012-02-20 Hans Ungerechts
!     
! <DOCUMENTATION name="modulePakoBackend">
!   <PROGRAM>
!                Pako
!   <\PROGRAM>
!   <FAMILY>
!                Backend
!   </FAMILY>
!   <SIBLINGS>
!
!   </SIBLINGS>        
!
!   Pako module for command: Backend
!
! </DOCUMENTATION> <!-- name="modulePakoBackend" -->
!
Module modulePakoBackend
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoGlobalVariables
  Use modulePakoLimits
  Use modulePakoDisplayText
  Use modulePakoUtilities
  Use modulePakoReceiver,                                                        &
       &  Only: queryReceiver,                                                   &
       &        listRX, varReceiver,                                             &
       &        nEsbIF, EMIRsbType, EMIRsubBands,                                &
       &        nEMIRsbCoax, esbCoax                                             !
  Use modulePakoXML
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: backend, displayBackend, saveBackend, writeXMLbackend
  !     
  ! *** Variables for backend ***
  Include 'inc/variables/variablesBackend.inc'
!!!   
!!!   
Contains
!!!   
!!!
  Subroutine Backend(programName,line,command,ERROR)
    !
    ! *** arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer               :: iOption2, iOption3
    Integer               :: countBE
    Logical               :: errorBE
    !
    Logical               :: doCCD              !  do clear, connect, or disconnect
    Logical               :: doClear
    Logical               :: doConnect          !  do connect based on /CONNECT or implicitly
    Logical               :: doConnectExplicit  !  explicit option /CONNECT .True.
    Logical               :: doDisconnect
    Logical               :: doDefaults         !  option /DEFAULTS is present
    Logical               :: fineFTS            !  option /FINEFTS is present
    Logical               :: doLineName         !  option /LINENAMAE is present
    Logical               :: doMode             !  option /MODE is present
    Logical               :: doPercentage       !  option /PERCENTAGE is present
    Logical               :: doReceiver         !  option /RECEIVER is present
    Logical               :: shortSyntax
    !
    Logical               :: hasOptions
    !
    Logical               :: doImplicitConnect
    !
    Real(Kind=kindSingle) ::      percentUsed
    Real(Kind=kindSingle) :: VESPApercentUsed
    !
    Logical               :: isConnectedRX      !  when checking if receiver is connected
    !
    Real(Kind=kindSingle) :: fShiftForced
    !
    Logical               :: receiverIsEMIR
    Logical               :: receiverIsHERA
    !
    Integer               :: length
    !
    Include 'inc/ranges/rangesBackend.inc'
    !
    ERROR       = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &          " module Backend v1.3.0 2017-09-13 ---> SR: Backend ")       ! this allows to trace execution
    !
    doCCD         = .False.
    doClear       = .False.
    doConnect     = .False.
    doDisconnect  = .False.
    doDefaults    = .False.
    fineFTS       = .False.
    doLineName    = .False.
    doMode        = .False.
    doPercentage  = .False.
    doReceiver    = .False.
    shortSyntax   = .False.
    !
    hasOptions    = .False.
    !
    doImplicitConnect   = .False. 
    !
    !D     Write (6,*) " BBC    samples ", vars(iValue,iBBC)%nSamples,   vars(iValue,iBBC)%samplesIsSet 
    !D     Write (6,*) " VESPA  samples ", vars(iValue,iVESPA)%nSamples, vars(iValue,iVESPA)%samplesIsSet 
    !D     Write (6,*) " WILMA  samples ", vars(iValue,iWILMA)%nSamples, vars(iValue,iWILMA)%samplesIsSet 
    !D     Write (6,*) "                                 "
    !D     Write (6,*) " GV%pulsarMode: ", GV%pulsarMode
    !D     Write (6,*) "                                 "
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    Call queryReceiver("HERA", receiverIsHERA)
    !D     Write (6,*) "      receiverIsEMIR: ", receiverIsEMIR
    !
!!$!!OLD    Write (6,*) "     backendChoices: ", backendChoices
!!$!!OLD    Write (6,*) "     bac%Continuum:  ", bac%Continuum
!!$!!OLD    Write (6,*) "     bac%FB4MHz      ", bac%FB4MHz  
!!$!!OLD    Write (6,*) "     bac%FB1MHz      ", bac%FB1MHz  
!!$!!OLD    Write (6,*) "     bac%FB100kHz    ", bac%FB100kHz
!!$!!OLD    Write (6,*) "     bac%FTS         ", bac%FTS     
!!$!!OLD    Write (6,*) "     bac%WILMA       ", bac%WILMA   
!!$!!OLD    Write (6,*) "     bac%VESPA       ", bac%VESPA   
!!$!!OLD    Write (6,*) "     bac%ABBA        ", bac%ABBA    
!!$!!OLD    Write (6,*) "     bac%USB         ", bac%USB     
!!$
    ! *** Initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/backendSetDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn,:) = vars(iValue,:)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    Call checkNofParameters(command,                                             &
         &     0, 8,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !    
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersBackend.inc'                            
       Include 'inc/options/readOptionsBackend.inc'          
    End If
    !
!!$    !D     Write (6,*) "         after 1st parametersBackend.inc readOptionsBackend.inc "
!!$    !D     Write (6,*) "         vars(iIn,iBack): ", vars(iIn,iBack)
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          If (receiverIsEMIR) Then
             Include 'inc/variables/backendSetDefaults.inc'
          Else
             error = .True.
             messageText = " /defaults only works with receiver EMIR"
             Call pakoMessage(priorityE,severityE,command,messageText)
          End If
       End If
    End If
    !
    If (GV%doDebugMessages) Then
       Write (6,*)    "     ---> SR Backend after backendSetDefaults.inc"
       Do jj = 1,8,1 
          Write (6,*) "     listBE(iBBC,jj): ", listBE(iBBC,jj)
       End Do
!!$       Do jj = 1,4,1 
!!$          Write (6,*) "     listBE(iNBC,jj): ", listBE(iNBC,jj)
!!$       End Do
       Do jj = 1,4,1 
          Write (6,*) "     listBE(iContinuum,jj): ", listBE(iContinuum,jj)
       End Do
    End If
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersBackend.inc'
       Include 'inc/options/readOptionsBackend.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** check consistency and                           ***                    
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       errorInconsistent = .False.
       Include 'inc/parameters/checkConsistentBackend.inc'
       If (.Not. errorInconsistent) Then
          vars(iTemp,iBack) = vars(iIn,iBack)
       End If
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    If (.Not.ERROR) Then         !!    enforce special and fixed values
       !
       ! *** special default settings for VESPA * * 640 * * : 
       !     percentage = 82, if not specified explicitly
       !
       If (               iBack.Eq.iVESPA                                        &   
            & .And. .Not. doPercentage                                           &
            &    ) Then                                                          !
          If (vars(iTemp,iBack)%bandWidth.Ge.512.) Then                          
               vars(iTemp,iBack)%percentage = 82.0
          Else If (vars(iTemp,iBack)%percentage .Eq. 82.0) Then
               !  ** set back to "normal default"
               vars(iTemp,iBack)%percentage = vars(iDefault,iBack)%percentage
          End If
       End If   !!   VESPA * * 640 * *
       !
       ! **  EMIR: enforce formally correct fOffset (relative to VESPA)
       !           if it is fixed by hardware and user requests 0.0 ("no shift")
       !
       If     (     receiverIsEMIR                                               &
            & .And. (     iBack.Eq.iWILMA                                        &
            &        .Or. iBack.Eq.iContinuum                                    &
            &        .Or. iBack.Eq.iFB4MHz                                       &
            &        .Or. ( iBack.Eq.iFTS                                        &
            &          .And. Abs(vars(iTemp,iBack)%bandWidth-FTSbw%wide).Lt.0.1) &    !!  FTS   wide
            &        .Or. ( iBack.Eq.iFB1MHz                                     &
            &              .And. Abs(vars(iTemp,iBack)%bandWidth-1024.0).Lt.0.1) &
            &       ) ) Then                                                     !
          !
          If (Abs(vars(iTemp,iBack)%fShift-0.0) .Lt. 0.1) Then
             !
             If ( vars(iTemp,iBack)%subBand .Eq. esb%lo ) Then
                fShiftForced = fRangeSky(iBack,iLO,iCenter)                      &
                     &       - fRangeSky(iVESPA,iLO,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%li ) Then
                fShiftForced = fRangeSky(iBack,iLI,iCenter)                      &
                     &       - fRangeSky(iVESPA,iLI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%ui ) Then
                fShiftForced = fRangeSky(iBack,iUI,iCenter)                      &
                     &       - fRangeSky(iVESPA,iUI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%uo ) Then
                fShiftForced = fRangeSky(iBack,iUO,iCenter)                      &
                     &       - fRangeSky(iVESPA,iUO,iCenter)                     !
             End If
             !
             vars(iTemp,iBack)%fShift = fShiftForced
             !
             Write (messageText,'(a)')                                           &
                  &   " paKo OVERRULES USER parameter fShift to hardware value:" !
             Call pakoMessage(priorityI,severityI,command,messageText)
             Write (messageText,'(a,f7.1,a,a,a,a)')                              &
                  &   " set fShift to fixed value ", fShiftForced,               &
                  &   " for ",      backendChoices(iBack),                       &
                  &   " on EMIR ",  vars(iTemp,iBack)%subBand                    !
             Call pakoMessage(priorityW,severityW,command,messageText)
             !
          End If
          !
       End If   !!   EMIR  &  FTS wide
       !
       If     (     receiverIsEMIR                                               &
            & .And.(( iBack.Eq.iFTS                                              &
            &        .And. Abs(vars(iTemp,iBack)%resolution-FTSres%fine).Lt.0.01 &  !!  FTS   /fine
            &        .And. Abs(vars(iTemp,iBack)%bandWidth-FTSbw%fine).Lt.0.1)   &
            &      )) Then                                                       !
          !
          If (Abs(vars(iTemp,iBack)%fShift-0.0) .Lt. 0.1) Then
             !
             If ( vars(iTemp,iBack)%subBand .Eq. esb%lo ) Then
                fShiftForced = fRangeSkyFTSfine(iLO,iCenter)                     &
                     &       - fRangeSky(iVESPA,iLO,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%li ) Then
                fShiftForced = fRangeSkyFTSfine(iLI,iCenter)                     &
                     &       - fRangeSky(iVESPA,iLI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%ui ) Then
                fShiftForced = fRangeSkyFTSfine(iUI,iCenter)                     &
                     &       - fRangeSky(iVESPA,iUI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%uo ) Then
                fShiftForced = fRangeSkyFTSfine(iUO,iCenter)                     &
                     &       - fRangeSky(iVESPA,iUO,iCenter)                     !
             End If
             !
             vars(iTemp,iBack)%fShift = fShiftForced
             !
             Write (messageText,'(a)')                                           &
                  &   " paKo OVERRULES USER parameter fShift to hardware value:" !
             Call pakoMessage(priorityI,severityI,command,messageText)
             Write (messageText,'(a,f7.1,a,a,a,a)')                              &
                  &   " set fShift to fixed value ", fShiftForced,               &
                  &   " for ",      backendChoices(iBack),                       &
                  &   " on EMIR ",  vars(iTemp,iBack)%subBand                    !
             Call pakoMessage(priorityW,severityW,command,messageText)
             !
          End If
          !
       End If   !!   EMIR &  FTS /fine
       !
       If     (     receiverIsEMIR                                               &
            & .And. (     iBack.Eq.iBBC                                          &
            &       ) ) Then                                                     !
          !
          If (Abs(vars(iTemp,iBack)%fShift-0.0) .Lt. 0.1) Then
             !
             If ( vars(iTemp,iBack)%subBand .Eq. esb%lo ) Then
                fShiftForced = fRangeSky(iContinuum,iLO,iCenter)                 &
                     &       - fRangeSky(iVESPA,iLO,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%li ) Then
                fShiftForced = fRangeSky(iContinuum,iLI,iCenter)                 &
                     &       - fRangeSky(iVESPA,iLI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%ui ) Then
                fShiftForced = fRangeSky(iContinuum,iUI,iCenter)                 &
                     &       - fRangeSky(iVESPA,iUI,iCenter)                     !
             Else If ( vars(iTemp,iBack)%subBand .Eq. esb%uo ) Then
                fShiftForced = fRangeSky(iContinuum,iUO,iCenter)                 &
                     &       - fRangeSky(iVESPA,iUO,iCenter)                     !
             End If
             !
             vars(iTemp,iBack)%fShift = fShiftForced
             !
             Write (messageText,'(a)')                                           &
                  &   " paKo OVERRULES USER parameter fShift to hardware value:" !
             Call pakoMessage(priorityI,severityI,command,messageText)
             Write (messageText,'(a,f7.1,a,a,a,a)')                              &
                  &   " set fShift to fixed value ", fShiftForced,               &
                  &   " for ",      backendChoices(iBack),                       &
                  &   " on EMIR ",  vars(iTemp,iBack)%subBand                    !
             Call pakoMessage(priorityW,severityW,command,messageText)
             !
          End If
          !
       End If   !!   EMIR BBC
       !
       ! **  FTS: enforce formally correct resolution (channel separation)
       !
       If (         (    gv%privilege.EQ.setPrivilege%staff                      &  !! allow to avoid rules
            &       .Or. gv%privilege.EQ.setPrivilege%ncsTeam )                  &
            & .And.      GV%limitCheck.Eq.setLimitCheck%loose )                  &
            & Then                                                               !
          !
       Else
          !
          If      ( iBack.Eq.iFTS                                                &
               &   .And. .Not.(      setDefaults                                 &
               &                .Or. doConnect                                   &
               &                .Or. doDisconnect                                &
               &                .Or. shortSyntax )                               &
               &  ) Then                                                         !
             !
             If (      Abs(vars(iTemp,iBack)%resolution-FTSres%wide)             &
                  &       .Le. 0.1*FTSres%wide) Then                             !
                !
                If (Abs(vars(iTemp,iBack)%resolution-FTSres%wide).Ge.0.001) Then
                   !
                   Write (messageText,'(a)')                                     &
                        &   " paKo OVERRULES USER resolution to hardware value:" !
                   Call pakoMessage(priorityI,severityI,command,messageText)
                   Write (messageText,'(a,f7.3,a,a,a,a)')                        &
                        &   " set resolution to exact value ",                   &
                        &     FTSres%wide,                                       &
                        &   " for ",      backendChoices(iBack),                 &
                        &   " on EMIR ",  vars(iTemp,iBack)%subBand              !
                   Call pakoMessage(priorityW,severityW,command,messageText)
                   !
                End If
                !
                vars(iTemp,iBack)%resolution = FTSres%wide
                !
             Else If ( Abs(vars(iTemp,iBack)%resolution-FTSres%fine)             &
                  &       .Le. 0.1*FTSres%fine) Then                             !
                !
                If (Abs(vars(iTemp,iBack)%resolution-FTSres%fine).Ge.0.001) Then
                   !
                   Write (messageText,'(a)')                                     &
                        &   " paKo OVERRULES USER resolution to hardware value:" !
                   Call pakoMessage(priorityI,severityI,command,messageText)
                   Write (messageText,'(a,f7.3,a,a,a,a)')                        &
                        &   " set resolution to exact value ",                   &
                        &     FTSres%fine,                                       &
                        &   " for ",      backendChoices(iBack),                 &
                        &   " on EMIR ",  vars(iTemp,iBack)%subBand              !
                   Call pakoMessage(priorityW,severityW,command,messageText)
                   !
                End If
                !
                vars(iTemp,iBack)%resolution = FTSres%fine
                !
             Else
                !
                ERROR = .True.
                !
                Write (messageText,'(a,f7.3,a,a,a,f7.3,f7.3)')                   &
                     &   " resolution ", vars(iTemp,iBack)%resolution,           &
                     &   " for ",      backendChoices(iBack),                    &
                     &   " wrong. valid choices: ",                              &
                     &   FTSres%fine, FTSres%wide                                !
                Call pakoMessage(priorityE,severityE,command,messageText)
                !
             End If
             !
          End If                 !!    FTS
          !
       End If                                                                      !! allow to avoid rules
       !      
    End If                       !!    enforce special and fixed values
    !
    !D     Write (6,*)    "          ---> SR Backend after overrule "
    !D     Do jj = 1,8,1 
    !D        Write (6,*) "          listBE(iFTS,jj)E: ", listBE(iFTS,jj)
    !D     End Do
    !
    ! *** store from Temp into persistent variables   ***                        
    !
    !! If (.Not.ERROR .And. .Not.setDefaults) Then
    If ( .Not.ERROR ) Then
       vars(iValue,iBack) = vars(iTemp,iBack)
       iPart = vars(iValue,iBack)%nPart
       If (.Not. doCCD) Then
          listBe(iBack,iPart) = vars(iValue,iBack)
       End If
    End If
    !
    !D     Write (6,*)    "          ---> SR Backend after store from Temp into persistent"
    !D     !
    !D     Write (6,*)    "               vars(:,iBBC)%samplesIsSet:   ",               &
    !D          &                         vars(:,iBBC)%samplesIsSet                     !
    !D     Write (6,*)    "               vars(:,iVESPA)%samplesIsSet:  ",              &
    !D          &                         vars(:,iVESPA)%samplesIsSet                   !
    !D     Write (6,*)    "               vars(:,iWILMA)%samplesIsSet:  ",              &
    !D          &                         vars(:,iWILMA)%samplesIsSet                   !
    !
    GV%pulsarMode =      vars(iValue,iBBC)%samplesIsSet                          &
         &          .Or. vars(iValue,iVESPA)%samplesIsSet                        &
         &          .Or. vars(iValue,iWILMA)%samplesIsSet                        !
    !D     Write (6,*)    "               GV%pulsarMode:     ", GV%pulsarMode
    !
    !D     Do jj = 1,8,1 
    !D        Write (6,*) "          listBE(iFTS,jj)E: ", listBE(iFTS,jj)
    !D     End Do
    !
    !     NOTE: 
    !           Character(len=lenVar)   ::  errorCode = GPnone in standardWorkingVariables.inc
    !           Integer, Parameter      ::  lenVar = 128       in sizes.inc
    !           Character(len=lenLine)  ::  messageText        in standardWorkingVariables.inc
    !           Integer, Parameter      ::  lenLine = 256      in sizes.inc
    !
    Call  checkContinuum (listRX,                                                &
         &  listBe(iContinuum,:),                                                &
         &  errorBE, errorCode, percentUsed)                                     !
    Write (messageText,'(a,f5.1)')                                               &
         &             bac%continuum//" total hardware used [%]: ", percentUsed  !
    Call pakoMessage(priorityI,severityI,command,messageText)
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = bac%continuum//" error. "                                &
               &        //errorCode(1:Len_trim(errorCode))                       !
       Else
          messageText = bac%continuum//" error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
    Call  checkBBC (listRX,                                                      &
         &  listBe(iBBC,:),                                                      &
         &  errorBE, errorCode, percentUsed)                                     !
    Write (messageText,'(a,f5.1)')                                               &
         &             bac%BBC//" total hardware used [%]: ", percentUsed        !
    Call pakoMessage(priorityI,severityI,command,messageText)
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = bac%BBC//" error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = bac%BBC//" error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
!!$    Call  checkFB100kHz (listRX,                                                 &   !!   OBSOLETE
!!$         &  listBe(iFB100kHz,:),                                                 &
!!$         &  errorBE, errorCode, percentUsed)
!!$    Write (messageText,'(a,f5.1)')                                               &
!!$         &             " 100kHz total hardware used [%]: ", percentUsed
!!$    Call pakoMessage(priorityI,severityI,command,messageText)
!!$    If (errorBE) Then
!!$       If (Len_trim(errorCode) .Ge. 1) Then
!!$          messageText = " 100kHz error. "//errorCode(1:Len_trim(errorCode))
!!$       Else
!!$          messageText = " 100kHz error. "
!!$       End If
!!$       Call pakoMessage(priorityE,severityE,command,messageText)
!!$    End If
!!$    errorInconsistent = errorInconsistent .Or. errorBE
!!$    !
!!$    Call  checkFB1MHz (listRX,                                                   &   !!   OBSOLETE
!!$         &  listBe(iFB1MHz,:),                                                   &
!!$         &  errorBE, errorCode, percentUsed)
!!$    Write (messageText,'(a,f5.1)')                                               &
!!$         &             " 1MHz total hardware used [%]: ", percentUsed
!!$    Call pakoMessage(priorityI,severityI,command,messageText)
!!$    If (errorBE) Then
!!$       If (Len_trim(errorCode) .Ge. 1) Then
!!$          messageText = " 1MHz error. "//errorCode(1:Len_trim(errorCode))
!!$       Else
!!$          messageText = " 1MHz error. "
!!$       End If
!!$       Call pakoMessage(priorityE,severityE,command,messageText)
!!$    End If
!!$    errorInconsistent = errorInconsistent .Or. errorBE
    !
    Call  checkFB4MHz (listRX,                                                   &
         &  listBe(iFB4MHz,:),                                                   &
         &  errorBE, errorCode, percentUsed)                                     !
    Write (messageText,'(a,f5.1)')                                               &
         &             " 4MHz total hardware used [%]: ", percentUsed            !
    Call pakoMessage(priorityI,severityI,command,messageText)
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = " 4MHz error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = " 4MHz error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !                                                                            
    If (         (    gv%privilege.EQ.setPrivilege%staff                         &  !! allow to avoid rules
         &       .Or. gv%privilege.EQ.setPrivilege%ncsTeam )                     &
         & .And.      GV%limitCheck.Eq.setLimitCheck%loose )                     &
         & Then                                                                  !
       !
    Else
       !
       Call  checkFTS (listRX,                                                   &
            &  listBe(iFTS,:),                                                   &
            &  errorBE, errorCode, percentUsed)                                  !
       !
       Write (messageText,'(a,f5.1)')                                            &
            &             " FTS total hardware used [%]: ", percentUsed          !
       Call pakoMessage(priorityI,severityI,command,messageText)
       If (errorBE) Then
          length  = Len_trim(errorCode)
          If (Len_trim(errorCode) .Ge. 1) Then
             messageText = "FTS error. "//errorCode(1:length)
          Else
             messageText = "FTS error. "
          End If
          Call pakoMessage(priorityE,severityE,command,messageText)
       End If
       errorInconsistent = errorInconsistent .Or. errorBE
       !
    End If                                                                          !! allow to avoid rules
    !
    Call  checkUsb (listRX,                                                      &
         &  listBe(iUSBack,:),                                                   &
         &  errorBE, errorCode, percentUsed)                                     !
    Write (messageText,'(a,f5.1)')                                               &
         &             " USB total hardware used [%]: ", percentUsed             !
    Call pakoMessage(priorityI,severityI,command,messageText)
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = " USB error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = " USB error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
    Call  checkWILMA (listRX,                                                    &
         &  listBe(iWILMA,:),                                                    &
         &  errorBE, errorCode, percentUsed)                                     !
    Write (messageText,'(a,f5.1)') " WILMA total hardware requested [%]: ",      &
         &                         percentUsed                                   !
    If (percentUsed.Le.100.001) Then
       Call pakoMessage(priorityI,severityI,command,messageText)
    Else
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = " WILMA error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = " WILMA error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
    errorBE   = .False.
    errorCode = ""
    !
    Call  checkVespaEMIR (listRX,                                                &
         &  listBe(iVESPA,:),                                                    &
         &  errorBE, errorCode, VESPApercentUsed)                                !
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = " VESPA error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = " VESPA error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
    Call  checkVespa (listRX,                                                    &
         &  listBe(iVESPA,:),                                                    &
         &  errorBE, errorCode, VESPApercentUsed)                                !
    Write (messageText,'(a,f9.3)')                                               &
         &             " VESPA total hardware used [%]: ", VESPApercentUsed      !
    Call pakoMessage(priorityI,severityI,command,messageText)
    If (errorBE) Then
       If (Len_trim(errorCode) .Ge. 1) Then
          messageText = " VESPA error. "//errorCode(1:Len_trim(errorCode))
       Else
          messageText = " VESPA error. "
       End If
       Call pakoMessage(priorityE,severityE,command,messageText)
    End If
    errorInconsistent = errorInconsistent .Or. errorBE
    !
    If (errorInconsistent) Then
       ERROR = .True.
       listBe(iBack,iPart) = beReset
    End If
    !
    ! *** display values ***
    !     (Note: this is done independantly of the Error status)
    Call queryReceiver(rec%Bolo,Result)
    If (.Not.Result) Then
       Call displayBackend
       Call displayBackend2
    End If
    !
    If (.Not.ERROR) Then
       GV%notReadyRXafterBE = .False.
    End If
    !D    Write (6,*) "   GV%notReadyRXafterBE: ", GV%notReadyRXafterBE
    !
    ! *** list backends in command line window ***
    !
    If (.Not.error) Then
       !
       If (iBack.Ge.1 .And. iBack.Le.nDimBackends) Then
          backendCommand(iBack) = line(1:lenc(line))
       End If
       !
       countBE = 0
       Do jj = 1,nDimBackends,1
          Do ii = 1,nDimParts,1
             l1 = lenc(listBe(jj,ii)%name)
             l2 = lenc(listBe(jj,ii)%receiverName)
             !
             If (listBe(jj,ii)%isConnected.Or.listBe(jj,ii)%wasConnected) Then
                If (listBe(jj,ii)%isConnected) Then                                  !   2011-10-20
                   countBE = countBE+1 
                End If
                !
                Write   (messageText,                                            &
                     &       '(" ",                       A10,                   & 
                     &         " ",                       I3,                    &  
                     &         " ",                       F10.3,                 &
                     &         " ",                       F10.1,                 &
                     &         " ",                       F10.1,                 &
                     &         " ",                       A5,                    &   
                     &         " ")'                                             &
                     &  )                                                        &
                     &        listBe(jj,ii)%name,                                &   !   1  A10,   
                     &        listBe(jj,ii)%nPart,                               &   !   2  I3,    
                     &        listBe(jj,ii)%resolution,                          &   !   3  F10.3, 
                     &        listBe(jj,ii)%bandwidth,                           &   !   4  F10.1, 
                     &        listBe(jj,ii)%fShift,                              &   !   5  F10.3, 
                     &        listBe(jj,ii)%receiverName                         !   !   6  A5,    
                !
                length = Len_trim(messageText)
                !
                !D  Write (6,*) " messageText(1:length) -->", messageText(1:length), "<--"
                !
                !  ** EMIR
                If (        listBe(jj,ii)%receiverName .Eq. rec%E090             &
                     & .Or. listBe(jj,ii)%receiverName .Eq. rec%E150             &
                     & .Or. listBe(jj,ii)%receiverName .Eq. rec%E230             &
                     & .Or. listBe(jj,ii)%receiverName .Eq. rec%E300 ) Then      !
                   Write   (messageText(length+2:),                              &
                        &       '(" ",                       A3,                 &   
                        &         " ",                       A5,                 &   
                        &         " ")'                                          &
                        &  )                                                     &
                        &        listBe(jj,ii)%polarization,                     &   !   7  A3,    
                        &        listBe(jj,ii)%subband                           !   !   8  A5,    
                   !
                End If
                length = Len_trim(messageText)
                !
                If (jj.eq.iVESPA .And. listBe(jj,ii)%mode.Ne.GPnone) Then
                   !
                   Write   (messageText(length+2:),                              &
                        &       '(" /mod ",                      A4,             & 
                        &         " ")'                                          &
                        &  )                                                     &
                        &        listBe(jj,ii)%mode                              !
                   !
                End If
                length = Len_trim(messageText)
                !
!!OLD                      If (jj.eq.iVESPA) Then
                If (listBe(jj,ii)%percentage.Lt.100.0) Then
                   !
                   Write   (messageText(length+2:),                              &
                        &       '(" /perc ",                     F5.1,           &
                        &         " ")'                                          &
                        &  )                                                     &
                        &        listBe(jj,ii)%percentage                        !
                   !
                End If
                length = Len_trim(messageText)
                !
                If (listBe(jj,ii)%lineName.Ne.GPnone) Then
                   !
                   Write   (messageText(length+2:),                              &
                        &       '(" /lineName ",                 A12,            & 
                        &         " ")'                                          &
                        &  )                                                     &
                        &        listBe(jj,ii)%lineName                          !
                   !
                End If
                length = Len_trim(messageText)
                !
                If (    .Not.listBe(jj,ii)%isConnected                           &
                     &  .And.listBe(jj,ii)%wasConnected) Then                    !
                   !
                   Write   (messageText(length+2:),                              &
                        &       '(" DISCONNECTED ",                              & 
                        &         " ")'                                          &
                        &  )                                                     !
                   !
                End If
                length = Len_trim(messageText)
                !
                Call pakoMessage(priorityI+1,severityI,command,messageText)
                !
             End If
             !
          End Do   !!   ii = 1,nDimParts,1
          !
          If ( vars(iValue,jj)%samplesIsSet ) Then
             Write   (messageText,                                            &
                  &       '(" "                       A10,                    &
                  &         "   /nSamples ",           I6,                    &  
                  &         " ")'                                             &
                  &  )                                                        &
                  &    backendChoices(jj),                                    &
                  &    vars(iValue,jj)%nSamples                               !  I6
             !
             length = Len_trim(messageText)
             !
             !D              Write (6,*) " messageText(1:length) -->", messageText(1:length), "<--"
             Call pakoMessage(priorityI+1,severityI,command,messageText)          
             !
          End If
          !
       End Do   !!   jj = 1,nDimBackends,1
       !
       GV%backendSet       = countBE .Ge. 1
       GV%backendNumberOf  = countBE
       !
       !D             Write (6,*) "      GV%backendSet:      ",  GV%backendSet  
       !D             Write (6,*) "      GV%backendNumberOf: ",  GV%backendNumberOf
       !
    End If   !!   (.Not.error)
    !
    !D     Write (6,*) "                                 "
    !D     Write (6,*) " BBC    samples ", vars(  :   ,iBBC)%nSamples,   vars(  :   ,iBBC)%samplesIsSet 
    !D     Write (6,*) " VESPA  samples ", vars(  :   ,iVESPA)%nSamples, vars(  :   ,iVESPA)%samplesIsSet 
    !D     Write (6,*) " WILMA  samples ", vars(  :   ,iWILMA)%nSamples, vars(  :   ,iWILMA)%samplesIsSet 
    !D     Write (6,*) "                                 "
    !D     Write (6,*) " BBC    samples ", vars(iValue,iBBC)%nSamples,   vars(iValue,iBBC)%samplesIsSet 
    !D     Write (6,*) " VESPA  samples ", vars(iValue,iVESPA)%nSamples, vars(iValue,iVESPA)%samplesIsSet 
    !D     Write (6,*) " WILMA  samples ", vars(iValue,iWILMA)%nSamples, vars(iValue,iWILMA)%samplesIsSet 
    !D     Write (6,*) "                                 "
    !D     Write (6,*) " GV%pulsarMode: ", GV%pulsarMode
    !D     Write (6,*) "                                 "
    !D     !
    !D     Write (6,*) " module backend <--- SR: backend "
    !D     Write (6,*) "                                 "
    !
    Call pako_message(seve%t,programName,                                        &
         &          " module Backend                   <--- SR: Backend ")       ! this allows to trace execution
    !
    Return
  End Subroutine Backend                                                         
!!!
!!!
  Subroutine displayBackend
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Character (len=lenVar)  ::  displayLine   = GPnone
    !
    Include 'inc/display/commandDisplayBackend.inc'
    !
  End Subroutine displayBackend
!!!
!!!
  Subroutine displayBackend2
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Character (len=lenVar)  ::  displayLine   = GPnone
    !
    Include 'inc/display/commandDisplayBackend2.inc'
    !
  End Subroutine displayBackend2
!!!
!!!
  Subroutine saveBackend(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical :: isEMIR, isAC
    Logical :: receiverIsBolo
    Character (len=lenCh) :: bolometerName
    !
    Integer                :: ii, jj
    Character(len=lenLine) :: messageText
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: saveBackend , v 1.3.0 2017-08-31")  !
    !
    ERROR = .False.
    !
    B = "\"   ! "
    S = " "
    CMD =  programName//B//"BACKEND"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    contC = contNN
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    !
    Call queryReceiver(rec%bolo, receiverIsBolo,                                 &
         &       bolometerName = bolometerName)                                  !
    !
    !D     Write (6,*) "     bolometerName  ", bolometerName
    !
    If ( receiverIsBolo .And. bolometerName.Eq.bolo%NIKA ) Then
       !
       messageText = " nothing to save with receiver " // bolo%NIKA
       Call pakoMessage(priorityI,severityI,"BACKEND",messageText)
       !
    Else
       !
       Write (iUnit,*) CMD(1:lCMD), " /clear"
       !
    End If
    !
    If (.Not.receiverIsBolo) Then
       !
       backends: Do jj = 1,nDimBackends,1
          parts: Do ii = 1,nDimParts,1
             isConnected: If (listBe(jj,ii)%isConnected) Then
                !
                isAC   =      listBe(jj,ii)%name.Eq.bac%VESPA                    &
                     &   .Or. listBe(jj,ii)%name.Eq.bac%WILMA                    !
                !
                isEMIR =      listBe(jj,ii)%receiverName.Eq.rec%E090             &
                     &   .Or. listBe(jj,ii)%receiverName.Eq.rec%E150             &
                     &   .Or. listBe(jj,ii)%receiverName.Eq.rec%E230             &
                     &   .Or. listBe(jj,ii)%receiverName.Eq.rec%E300             !
                !
                autocorr: If (isAC) Then                                         !! autocorrelator: VESPA or WILMA
                   !
                   contC = contNC
                   Include 'inc/parameters/saveBackend.inc'
                   contC = contCC
                   Include 'inc/parameters/saveBackendPart.inc'
                   Include 'inc/parameters/saveBackendResolution.inc'
                   Include 'inc/parameters/saveBackendBandwidth.inc'
                   Include 'inc/parameters/saveBackendFshift.inc'
                   !
                   If (isEMIR) Then                                              !! EMIR
                      Include 'inc/parameters/saveBackendReceiverEMIR.inc'
                      If  ( listBe(jj,ii)%lineNameIsSet ) Then
                         Include 'inc/options/saveBElineName.inc'
                      End If
                   Else                                                          !! not EMIR
                      Include 'inc/parameters/saveBackendReceiver.inc'
                   End If                                                        !! isEMIR
                   !
                   Include 'inc/options/saveBEmode.inc'
                   contC = contCN
                   Include 'inc/options/savePercentage.inc'
                   !
                Else autocorr                                                    !! NOT autocorrelator
                   !
                   contC = contNC
                   Include 'inc/parameters/saveBackend.inc'
                   contC = contCC
                   Include 'inc/parameters/saveBackendPart.inc'
                   Include 'inc/parameters/saveBackendResolution.inc'
                   Include 'inc/parameters/saveBackendBandwidth.inc'
                   Include 'inc/parameters/saveBackendFshift.inc'
                   !
                   If (isEMIR) Then                                              !! is EMIR
                      If  ( listBe(jj,ii)%lineNameIsSet ) Then
                         Include 'inc/parameters/saveBackendReceiverEMIR.inc'
                         contC = contCN
                         Include 'inc/options/saveBElineName.inc'
                      Else                                   
                         contC = contCN
                         Include 'inc/parameters/saveBackendReceiverEMIR.inc'
                      End If
                   Else                                                          !! not EMIR
                      If  ( listBe(jj,ii)%lineNameIsSet ) Then
                         Include 'inc/parameters/saveBackendReceiver.inc'
                         contC = contCN
                         Include 'inc/options/saveBElineName.inc'
                      Else
                         contC = contCN
                         Include 'inc/parameters/saveBackendReceiver.inc'
                      End If
                   End If                                                        !! isEMIR
                   !
                End If autocorr                                                  !! isAC
                !
             End If isConnected
          End Do parts   !!   ii = 1,nDimParts,1 
          !
          If ( jj.Eq.iBBC .And. vars(iValue,iBBC)%isConnected                    &
               &          .And. vars(iValue,iBBC)%samplesIsSet  ) Then           !
             !D  Write (6,*) "     vars(iValue,iBBC) :   ", vars(iValue,iBBC)
             Include 'inc/options/saveBEnSamples.inc'
          End If
          If ( jj.Eq.iVESPA .And. vars(iValue,iVESPA)%isConnected                &
               &            .And. vars(iValue,iVESPA)%samplesIsSet  ) Then       !
             !D  Write (6,*) "     vars(iValue,iVESPA) : ", vars(iValue,iVESPA) 
             Include 'inc/options/saveBEnSamples.inc'
          End If
          If ( jj.Eq.iWILMA .And. vars(iValue,iWILMA)%isConnected                &
               &            .And. vars(iValue,iWILMA)%samplesIsSet  ) Then       !
             !D  Write (6,*) "     vars(iValue,iWILMA) : ", vars(iValue,iWILMA) 
             Include 'inc/options/saveBEnSamples.inc'
          End If
          !
       End Do backends   !!   Do jj = 1,nDimBackends,1
       !
    End If
    !
    Write (iUnit,*) "! "
    !
    Return
  End Subroutine saveBackend
!!!
!!!
  Subroutine writeXMLbackend(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical :: errorXML
    Logical :: receiverIsBolo
    Logical :: receiverIsEMIR
    Logical :: receiverIsHolo
    Character (len=lenCh) :: bolometerName
    !
    Integer :: ii, jj
    Character (len=24) :: valueC
    Character (len=48) :: valueComment
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,"    --> SR: writeXMLbackend ")  
    !
    !D    Write (6,*)  "      moduleBackend --> SR: writeXMLbackend "
    !
    Include 'inc/startXML/backend.inc'
    !
    Return
  End Subroutine writeXMLbackend
!!!
!!!
  Subroutine checkContinuum (listRX, listContinuum,                              &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX             !! list of all Receivers             !
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listContinuum      !! list of all Continuum parts       !
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, l
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR
    !
    Real    :: fShiftMin, fShiftMax
    Character(len=lenVar)  :: limitCode
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: checkContinuum (NBC)")              !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listContinuum,1)
    uBoundBE = Ubound(listContinuum,1)
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       If (listContinuum(ii)%isConnected) Then
          jj = jj+1
          countParts = countParts+1
          !
          !  **  EMIR
          !
          If (receiverIsEMIR) Then
             !
             If    (      listContinuum(ii)%receiverName .Eq. rec%E090) Then
                If (      listContinuum(ii)%subband      .Eq. esb%lsb            &
                   & .Or. listContinuum(ii)%subband      .Eq. esb%usb ) Then     !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//                               &
                           &      listContinuum(ii)%subband//" not possible"     !
                   End If
                End If
             End If
             !
             !! **   EMIR: fShift depending on EMIR sub band
             !! TBD: abtract the following checks on fShift into a Subroutine
             !!      compare checkContinuum, checkWILMA, checkFB4MHz, checkFB1MHz
             !
             If (listContinuum(ii)%subBand .Eq. esb%lo) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iLO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iLO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
             Else If (listContinuum(ii)%subBand .Eq. esb%li) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iLI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iLI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
             Else If (listContinuum(ii)%subBand .Eq. esb%ui) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iUI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iUI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
             Else If (listContinuum(ii)%subBand .Eq. esb%uo) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iUO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iUO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
             End If
             !
             If (        listContinuum(ii)%fShift.LT.fShiftMin                   &
                  & .Or. listContinuum(ii)%fShift.GT.fShiftMax  )   Then         !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" invalid fShift "
                End If
                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
                     &            listContinuum(ii)%fShift, fShiftMin, fShiftMax !
                l  = Len_trim(errorCode)
                l2 = Len_trim(limitCode)
                If ( Len(errorCode) .Ge. (l+l2) ) Then
                   errorCode = errorCode(1:l)//limitCode(1:l2)
                End If
             End If
             !
             If (.Not. error) Then
                percentUsed = percentUsed                                        &
                     &        +100.0*listContinuum(ii)%bandwidth/(1000.0*4)      !
             End If
             !
          Else  !!  not EMIR
             !
             If      (listContinuum(ii)%receiverName .Eq. rec%HERA1) Then
                percentUsed = percentUsed+50.0
             Else If (listContinuum(ii)%receiverName .Eq. rec%HERA2) Then
                percentUsed = percentUsed+50.0
             Else If (listContinuum(ii)%receiverName .Eq. rec%HERA) Then
                percentUsed = percentUsed+100.0
             Else
                percentUsed = percentUsed                                        &
                     &        +100.0*listContinuum(ii)%bandwidth/(1000.0*4)      !
             End If
             !
          End If
          !
          If (.Not. ( Abs(listContinuum(ii)%bandWidth-1000.0) .Lt. 0.001         &
               &    ) ) Then                                                     !
             error = .True.
             l = Len_trim(errorCode)
             If (Len(errorCode).Ge.l+30) Then
                errorCode = errorCode(1:l)//" wrong bandwidth"
             End If
          End If
          !
       End If
    End Do
    !
    If (countParts .Gt. 4) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkContinuum
!!!
!!!
  Subroutine checkBBC (listRX, listBBC,                                          &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX    !! list of all Receivers                      !
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listBBC   !! list of all BBC parts                      !
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, kk, l
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR
    !
    Real    :: fShiftMin, fShiftMax
    Real(Kind=kindSingle)  :: delta, impliedValue
    Character(len=lenVar)  :: limitCode
    !
    Call pako_message(seve%t,"paKo backend","    --> SR: checkBBC ")  
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listBBC,1)
    uBoundBE = Ubound(listBBC,1)
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       !
       If (listBBC(ii)%isConnected) Then
          !
          jj = jj+1
          countParts = countParts+1
          !
          !  **  EMIR
          !
          If (.Not. receiverIsEMIR) Then
             !
             error = .True.                                                                                                        
             l = Len_trim(errorCode)                                                                                               
             If (Len(errorCode).Ge.l+30) Then                                                                                      
                errorCode = errorCode(1:l)//                                     &                                                       
                     &      bac%BBC//" works only with EMIR "                    !
             End If
             !
          Else                               !!  receiver is EMIR
             !
             If (ii.LT.uBoundBE) Then
                Do kk = ii+1, uBoundBE, 1
                   If (         listBBC(ii)%receiverName.Eq.listBBC(kk)%receiverName  &
                        & .And. listBBC(ii)%polarization.Eq.listBBC(kk)%polarization  &
                        & .And. listBBC(ii)%subband.Eq.listBBC(kk)%subband ) Then     !
                      error = .True.
                      l = Len_trim(errorCode)
                      If (Len(errorCode).Ge.l+80) Then
                         errorCode = errorCode(1:l)//                            &
                              &    " parts must be on different EMIR sub bands " &
                              &  //" or side bands."                             !
                      End If
                   End If
                End Do
             End If
             !
             delta = 0.1
             !
             impliedValue = 8000.0
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$             If (listBBC(ii)%receiverName .Eq. rec%E150) Then
!!$                impliedValue = 4000.0
!!$             Else
!!$                impliedValue = 8000.0
!!$             End If
             !
             If (Abs(listBBC(ii)%resolution-impliedValue) .Ge. delta) Then
                error = .True.                                                                                                        
                l = Len_trim(errorCode)                                                                                               
                If (Len(errorCode).Ge.l+30) Then                                                                                      
                   Write(errorCode,'(a,a,a,F11.1,a)')                            &
                        &      errorCode(1:l),                                   &
                        &      listBBC(ii)%receiverName,                         &
                        &    " implies resolution ", impliedValue, ". "          !
                End If
             End If
             !
             If (Abs(listBBC(ii)%bandWidth-impliedValue) .Ge. delta) Then
                error = .True.                                                                                                        
                l = Len_trim(errorCode)                                                                                               
                If (Len(errorCode).Ge.l+30) Then                                                                                      
                   Write(errorCode,'(a,a,a,F11.1,a)')                            &
                        &      errorCode(1:l),                                   &
                        &      listBBC(ii)%receiverName,                         &
                        &    " implies bandWidth ", impliedValue, ". "           !
                End If
             End If
             !
             If     (        listBBC(ii)%subband.Eq.esb%lo                       &
                  &     .Or. listBBC(ii)%subband.Eq.esb%uo                       &
                  & ) Then                                                       !
                error = .True.                                                                                                        
                l = Len_trim(errorCode)                                                                                               
                If (Len(errorCode).Ge.l+30) Then                                                                                      
                   errorCode = errorCode(1:l)//                                  &
                        &      bac%BBC//" cannot connect to "                    &
                        &      //listBBC(ii)%subband//". "                       !
                End If
             End If
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$             If     (       listBBC(ii)%receiverName .Ne. rec%E150               &
!!$                  &   .And. listBBC(ii)%subband.Ne.esb%lsb                       &
!!$                  &   .And. listBBC(ii)%subband.Ne.esb%usb                       &
!!$                  & ) Then                                                       !
             !
             If     (       listBBC(ii)%subband.Ne.esb%lsb                       &
                  &   .And. listBBC(ii)%subband.Ne.esb%usb                       &
                  & ) Then                                                       !
                error = .True.                                                                                                        
                l = Len_trim(errorCode)                                                                                               
                If (Len(errorCode).Ge.l+80) Then                                                                                      
                   errorCode = errorCode(1:l)//                                  &
                        &      " On "//listBBC(ii)%receiverName//" "//           &
                        &      " use "                                           &
                        &      //esb%lsb//" or "//esb%usb//" (8 GHz wide). "     !
                End If
             End If
             !
             !! **   EMIR: fShift depending on EMIR sub band
             !! TBD: abstract the following checks on fShift into a Subroutine
             !!      compare checkContinuum, checkWILMA, checkFB4MHz, checkFB1MHz
             !
             If (listBBC(ii)%subBand .Eq. esb%lo) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iLO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iLO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
             Else If (listBBC(ii)%subBand .Eq. esb%li) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iLI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iLI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iLI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
             Else If (listBBC(ii)%subBand .Eq. esb%ui) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iUI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iUI,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUI,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
             Else If (listBBC(ii)%subBand .Eq. esb%uo) Then
                !
                fShiftMin = Min ( fRangeSky(iContinuum,iUO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iContinuum,iUO,iCenterFrom),         &
                     &            fRangeSky(iContinuum,iUO,iCenterTo)    )       &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
             Else
                !
                fShiftMin = 0.0
                fShiftMax = 0.0
                !
             End If
             !
             If (        listBBC(ii)%fShift.LT.fShiftMin                         &
                  & .Or. listBBC(ii)%fShift.GT.fShiftMax  )   Then               !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" invalid fShift "
                End If
                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
                     &            listBBC(ii)%fShift, fShiftMin, fShiftMax       !
                l  = Len_trim(errorCode)
                l2 = Len_trim(limitCode)
                If ( Len(errorCode) .Ge. (l+l2) ) Then
                   errorCode = errorCode(1:l)//limitCode(1:l2)
                End If
             End If
             !

             If (.Not. error) Then
                countParts  = countParts  + 1
                percentUsed = percentUsed + 100.0*1/16
             End If
             !
          End If                             !!  receiver is EMIR
          !
       End If                                !!  listBBC(ii)%isConnected
       !
    End Do                                   !!  loop over backend parts
    !
    If (countParts .Gt. 16) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkBBC
!!!
!!!
  Subroutine checkFB4Mhz (listRX, listFB4MHz,                                    &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX          !! list of all Receivers                !
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listFB4MHz      !! list of all FB4MHz parts             !
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, l
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR
    !
    Real    :: fShiftMin, fShiftMax
    Character(len=lenVar)  :: limitCode
    !
    Logical, Dimension(nDimReceiverChoices) :: usesRX
    Character(len=lenCh)                    :: RXname
    Integer                                 :: iMatch, nMatch
    Logical                                 :: errorC
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: checkFB4Mhz")                       !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    !
    usesRX(:) = .False.
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listFB4MHz,1)
    uBoundBE = Ubound(listFB4MHz,1)
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       If (listFB4MHz(ii)%isConnected) Then
          jj = jj+1
          !
          !  **  EMIR
          !
          If (receiverIsEMIR) Then
             !
             If (ii.Gt.2) Then
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" maximum 2 parts with EMIR"
                End If
             End If
             !
             If     (           listFB4Mhz(ii)%receiverName .Eq. rec%E090        &
                  & .And. (     listFB4Mhz(ii)%subband      .Eq. esb%lsb         &
                  &        .Or. listFB4Mhz(ii)%subband      .Eq. esb%usb )       &
                  & ) Then                                                       !
                !
                If (.Not. ( Abs(listFB4Mhz(ii)%bandWidth-                        &
                     &          stdBandWidth (iFB4MHz,iLSB)) .Lt. 50.0           &
                     &    ) ) Then                                               !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//" wrong bandwidth"
                   End If
                End If
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+100.0
                End If
                !
             Else
                !
                If (.Not. ( Abs(listFB4Mhz(ii)%bandWidth-                        &
                     &          stdBandWidth (iFB4MHz,iLO)) .Lt. 50.0            &
                     &    ) ) Then                                               !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//" wrong bandwidth"
                   End If
                End If
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+50.0
                End If
                !
             End If
             !
             !! **   EMIR: fShift depending on EMIR sub band
             !! TBD: abtract the following checks on fShift into a Subroutine
             !!      compare checkContinuum, checkWILMA, checkFB4MHz, checkFB1MHz
             !
             If (listFB4MHz(ii)%subBand .Eq. esb%lo) Then
                !
                fShiftMin = Min ( fRangeSky(iFB4MHz,iLO,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iLO,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iFB4MHz,iLO,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iLO,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
             Else If (listFB4MHz(ii)%subBand .Eq. esb%li) Then
                !
                fShiftMin = Min ( fRangeSky(iFB4MHz,iLI,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iLI,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iFB4MHz,iLI,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iLI,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
             Else If (listFB4MHz(ii)%subBand .Eq. esb%ui) Then
                !
                fShiftMin = Min ( fRangeSky(iFB4MHz,iUI,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iUI,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iFB4MHz,iUI,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iUI,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
             Else If (listFB4MHz(ii)%subBand .Eq. esb%uo) Then
                !
                fShiftMin = Min ( fRangeSky(iFB4MHz,iUO,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iUO,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iFB4MHz,iUO,iCenterFrom),            &
                     &            fRangeSky(iFB4MHz,iUO,iCenterTo)    )          &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
             End If
             !
             If (        listFB4MHz(ii)%fShift.LT.fShiftMin                      &
                  & .Or. listFB4MHz(ii)%fShift.GT.fShiftMax  )   Then            !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" invalid fShift "
                End If
                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
                     &            listFB4MHz(ii)%fShift, fShiftMin, fShiftMax    !
                l  = Len_trim(errorCode)
                l2 = Len_trim(limitCode)
                If ( Len(errorCode) .Ge. (l+l2) ) Then
                   errorCode = errorCode(1:l)//limitCode(1:l2)
                End If
             End If
             !
          Else  !!  not EMIR
             !
             countParts = countParts+1
             !
             RXname = listFB4MHz(ii)%receiverName 
             Call pakoUmatchKey (                                                &
               &              keys=receiverChoices,                              &
               &              key=RXname,                                        &
               &              command='BACKEND',                                 &
               &              howto='Start Upper',                               &
               &              iMatch=iMatch,                                     &
               &              nMatch=nMatch,                                     &
               &              error=errorC,                                      &
               &              errorCode=errorCode                                &
               &             )                                                   !
             !
             If (iMatch.Ge.1 .And. iMatch.Le.nDimReceiverChoices) Then
                If (.Not.usesRX(iMatch)) Then
                   usesRX(iMatch) = .True.
                Else
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+40) Then
                      errorCode = errorCode(1:l)//                               &
                           &   " can connect only once to receiver "//RXname     !
                   End If
                End If
             End If
             If      (listFB4MHz(ii)%receiverName .Eq. rec%HERA1) Then
                percentUsed = percentUsed+100.0
             Else If (listFB4MHz(ii)%receiverName .Eq. rec%HERA2) Then
                percentUsed = percentUsed+100.0
             Else If (listFB4MHz(ii)%receiverName .Eq. rec%HERA) Then
                percentUsed = percentUsed+200.0
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" cannot use 4MHz with "//rec%HERA
                End If
             Else
                percentUsed = percentUsed                                        &
                     &        +100.0*listFB4MHz(ii)%bandwidth/(1024.0*18)        !
             End If
             If (.Not. ( Abs(listFB4MHz(ii)%bandWidth-1024.0) .Lt. 0.001         &
                  &    ) ) Then                                                  !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" wrong bandwidth"
                End If
             End If
             !
          End If
          !
       End If
    End Do
    !
    If (countParts.Gt.4 .Or. (receiverIsEMIR.And.countParts.Gt.2)) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkFB4Mhz
!!!
!!!
  Subroutine checkFTS (listRX, listFTS,                                          &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX                                                  !   !! list of all Receivers
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listFTS                                                 !   !! list of all FTS parts    
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, kk, l
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR, receiverIsHERA
    !
    Real    :: fDelta
    Real    :: fShiftMin, fShiftMax
    Character(len=lenVar)  :: limitCode
    !
    Logical, Dimension(nDimReceiverChoices) :: usesRX
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: checkFTS")                          !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    Call queryReceiver("HERA", receiverIsHERA)
    !
    usesRX(:) = .False.
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listFTS,1)
    uBoundBE = Ubound(listFTS,1)
    !
    !     NOTE: 
    !           Character(len=lenVar)   ::  errorCode      in calling SR (Backend)
    !           Character(len=lenVar)   ::  limitCode      in this SR, see above
    !           Integer, Parameter      ::  lenVar = 128   in sizes.inc
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       If (listFTS(ii)%isConnected) Then
          jj = jj+1
          !
          !  **  EMIR
          !
          If (receiverIsEMIR) Then
             !
             !   *   maximum bandwidth for fine resolution
             !
             !! TBD: here and below:
             !!      general mechanism to safely add text to errorCode:
             !!              directly by addinf strings
             !!              by a Write(errorCode, ...
             !!              through an auxiliary variable, e.g., limitCode
             !
             IF (Abs(listFTS(ii)%resolution-FTSres%fine).Le.0.002) Then
                !
                IF ((listFTS(ii)%bandwidth-FTSbw%fine).Ge.0.001) Then
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+80) Then
                      Write (limitCode,'(A,F8.3,A,F8.1,a)')                      &
                           &            " with resolution ",  FTSres%fine,       &
                           &            " max bandWidth is",  FTSbw%fine,        &
                           &            "."                                      !
                      l  = Len_trim(errorCode)
                      l2 = Len_trim(limitCode)
                      If ( Len(errorCode) .Ge. (l+l2) ) Then
                         errorCode = errorCode(1:l)//limitCode(1:l2)
                      End If
                   End If
                End If
             End If
             !
             If (ii.LT.uBoundBE) Then
                !
                !   *   all parts must have same resolution
                !
                Do kk = ii+1,  uBoundBE, 1
                   !
                   If (listFTS(kk)%isConnected) Then
                      !
                      If (Abs(listFTS(ii)%resolution-listFTS(kk)%resolution)     &
                           & .Gt.0.02) Then                                      !
                         error = .True.
                         l = Len_trim(errorCode)
                         If (Len(errorCode).Ge.l+80) Then
                            errorCode = errorCode(1:l)//" all parts "            &
                                 &  //" must have same resolution. "             !
                         End If
                      End If
                   End If
                   !
                End Do
                !
                !   *   parts 1 to 4 must be on 4 different EMIR sub bands
                !
                Do kk = ii+1, 4, 1
                   !
                   If (listFTS(kk)%isConnected) Then
                      !
                      If (             listFTS(ii)%receiverName                  &
                           &       .Eq.listFTS(kk)%receiverName                  &
                           & .And.     listFTS(ii)%polarization                  &
                           &       .Eq.listFTS(kk)%polarization                  &
                           & .And.     listFTS(ii)%subband                       &
                           &       .Eq.listFTS(kk)%subband ) Then                !
                         error = .True.
                         l = Len_trim(errorCode)
                         If (Len(errorCode).Ge.l+80) Then
                            errorCode = errorCode(1:l)//" parts 1 to 4 must be " &
                                 &  //" on 4 different EMIR sub bands. "         !
                         End If
                      End If
                   End If
                   !
                End Do
                !
             End If
             !
             !   *   parts 5 to 8 must be on E090 sub bands Hor UO, Hor LO, Ver UO, Ver LO
             !
             If (ii.Gt.4 .And. ii.LT.uBoundBE) Then
                Do kk = ii+1, uBoundBE, 1
                   !
                   If (listFTS(kk)%isConnected) Then
                      !
                      If (         listFTS(ii)%receiverName.Eq.listFTS(kk)%receiverName  &
                           & .And. listFTS(ii)%polarization.Eq.listFTS(kk)%polarization  &
                           & .And. listFTS(ii)%subband.Eq.listFTS(kk)%subband ) Then     !
                         error = .True.
                         l = Len_trim(errorCode)
                         If (Len(errorCode).Ge.l+90) Then
                            errorCode = errorCode(1:l)                           &
                                 &  //" parts 5 to 8 must be"                    &
                                 &  //" on 4 different E090 sub bands:"          &
                                 &  //" Hor UO, Hor LO, Ver UO, Ver LO."         !
                            !          123456789012345678901234567890
                         End If
                      End If
                   End If
                   !
                End Do
             End If
             !
             If (ii.Gt.4 .And. ii.LT.uBoundBE) Then
                !
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$                If     (             listFTS(ii)%receiverName.Eq.rec%E150        &
!!$                     &   .Or. (      listFTS(ii)%subband.Ne.esb%lo               &
!!$                     &         .And. listFTS(ii)%subband.Ne.esb%uo )             &
!!$                     & ) Then                                                    !
!
                If     (             listFTS(ii)%subband.Ne.esb%lo               &
                     &         .And. listFTS(ii)%subband.Ne.esb%uo               &
                     & ) Then                                                    !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+90) Then
                      errorCode = errorCode(1:l)                                 &
                           &  //" parts 5 to 8 must be"                          &
                           &  //" connected the 4 Outer sub bands "              &
                           &  //" on Coax 5 to 8 ."                              !
                      !          123456789012345678901234567890
                   End If
                   !
                End If
                !
             End If
             !
             !   *   maximum 8 FTS parts with EMIR
             !
             If (ii.Gt.8) Then
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" maximum 8 parts with EMIR."
                End If
             End If
             !
             !       count parts and percentUsed
             !
             If     (           listFTS(ii)%receiverName .Eq. rec%E090           &
                  & .And. (     listFTS(ii)%subband      .Eq. esb%lsb            &
                  &        .Or. listFTS(ii)%subband      .Eq. esb%usb )          &
                  & ) Then                                                       !
                !
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+25.0
                End If
                !
             Else
                !
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+12.5
                End If
                !
             End If
             !
             !! **   EMIR: fShift depending on EMIR sub band
             !! TBD: abtract the following checks on fShift into a Subroutine
             !!      compare checkContinuum, checkWILMA, checkFTS, checkFB1MHz
             !
             If (GV%doDebugMessages) Then
                Write (6,*) "          ii:                           ", ii
                Write (6,*) "          listFTS(ii)%resolution:       ", listFTS(ii)%resolution
                Write (6,*) "          listFTS(ii)%bandWidth:        ", listFTS(ii)%bandWidth
                Write (6,*) "          listFTS(ii)%fShift:           ", listFTS(ii)%fShift
                Write (6,*) "          listFTS(ii)%receiverName:     ", listFTS(ii)%receiverName
                Write (6,*) "          listFTS(ii)%polarization:     ", listFTS(ii)%polarization
                Write (6,*) "          listFTS(ii)%subBand:          ", listFTS(ii)%subBand
             End If       
             !
             IF (Abs(listFTS(ii)%resolution-FTSres%wide).Le.0.002) Then
                !
                If (listFTS(ii)%subBand .Eq. esb%lo) Then
                   !
                   fShiftMin = Min ( fRangeSky(iFTS,iLO,iCenterFrom),            &
                        &            fRangeSky(iFTS,iLO,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iLO,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSky(iFTS,iLO,iCenterFrom),            &
                        &            fRangeSky(iFTS,iLO,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iLO,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%li) Then
                   !
                   fShiftMin = Min ( fRangeSky(iFTS,iLI,iCenterFrom),            &
                        &            fRangeSky(iFTS,iLI,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iLI,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSky(iFTS,iLI,iCenterFrom),            &
                        &            fRangeSky(iFTS,iLI,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iLI,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%ui) Then
                   !
                   fShiftMin = Min ( fRangeSky(iFTS,iUI,iCenterFrom),            &
                        &            fRangeSky(iFTS,iUI,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iUI,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSky(iFTS,iUI,iCenterFrom),            &
                        &            fRangeSky(iFTS,iUI,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iUI,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%uo) Then
                   !
                   fShiftMin = Min ( fRangeSky(iFTS,iUO,iCenterFrom),            &
                        &            fRangeSky(iFTS,iUO,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iUO,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSky(iFTS,iUO,iCenterFrom),            &
                        &            fRangeSky(iFTS,iUO,iCenterTo)    )          &
                        &       -    fRangeSky(iVESPA,iUO,iCenter)               !
                   !
                End If
                !
             End IF
             !
             IF (Abs(listFTS(ii)%resolution-FTSres%fine).Le.0.002) Then
                !
                If (listFTS(ii)%subBand .Eq. esb%lo) Then
                   !
                   fShiftMin = Min ( fRangeSkyFTSfine(iLO,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iLO,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iLO,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSkyFTSfine(iLO,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iLO,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iLO,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%li) Then
                   !
                   fShiftMin = Min ( fRangeSkyFTSfine(iLI,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iLI,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iLI,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSkyFTSfine(iLI,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iLI,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iLI,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%ui) Then
                   !
                   fShiftMin = Min ( fRangeSkyFTSfine(iUI,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iUI,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iUI,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSkyFTSfine(iUI,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iUI,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iUI,iCenter)               !
                   !
                Else If (listFTS(ii)%subBand .Eq. esb%uo) Then
                   !
                   fShiftMin = Min ( fRangeSkyFTSfine(iUO,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iUO,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iUO,iCenter)               !
                   !
                   fShiftMax = Max ( fRangeSkyFTSfine(iUO,iCenterFrom),          &
                        &            fRangeSkyFTSfine(iUO,iCenterTo)    )        &
                        &       -    fRangeSky(iVESPA,iUO,iCenter)               !
                   !
                End If
                !
             End IF
             !
             fShiftMin = fShiftMin+0.5*listFTS(ii)%bandWidth
             fShiftMax = fShiftMax-0.5*listFTS(ii)%bandWidth
             !
             If (GV%doDebugMessages) Then
                Write (6,*) "          ... +/- 1/2 bandWidth:"
                Write (6,*) "          fShiftMin:   ", fShiftMin
                Write (6,*) "          fShiftMax:   ", fShiftMax
             End If
             !
             If (        listFTS(ii)%fShift.LT.fShiftMin                         &
                  & .Or. listFTS(ii)%fShift.GT.fShiftMax  )   Then               !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" fShift "
                End If
                Write (limitCode,'(F8.1,3(A,F8.1),A)')                           &
                     &            listFTS(ii)%fShift,                            &
                     &            " out of range",  fShiftMin,                   &
                     &            " to",            fShiftMax,                   &
                     &            " for bandWidth", listFTS(ii)%bandWidth,       &
                     &            "."                                            !
                l  = Len_trim(errorCode)
                l2 = Len_trim(limitCode)
                If ( Len(errorCode) .Ge. (l+l2) ) Then
                   errorCode = errorCode(1:l)//limitCode(1:l2)
                End If
             End If
             !
             !  **  HERA
             !
          Else  If (receiverIsHERA) Then
             !
             fDelta = 0.1
             !
!!OLD           
!!OLD          the following restrictions disabled 2011-07-22 following 
!!OLD          request by GP in meeting 2011-07-20
!!OLD
!!OLD             If (abs(listFTS(ii)%resolution-FTSres%fine).Le.0.01) Then
!!OLD                !
!!OLD                !   *   for /fine resolution HERA must have /width narrow
!!OLD                !
!!OLD                If (          .Not.listRX(iHERA1)%width.Eq.width%narrow          &
!!OLD                     &   .Or. .Not.listRX(iHERA2)%width.Eq.width%narrow          &
!!OLD                     &   ) Then                                                  !
!!OLD                   !
!!OLD                   error = .True.
!!OLD                   l = Len_trim(errorCode)
!!OLD                   If (Len(errorCode).Ge.l+80) Then
!!OLD                      Write (errorCode,'(A, F8.3, A, A, A)')                     & 
!!OLD                           &  " for /fine resolution",                           &
!!OLD                           &   FTSres%fine,                                      &
!!OLD                           &  ": HERA must have /width ",                        &
!!OLD                           &   width%narrow,                                     &
!!OLD                           &  "."                                                !
!!OLD                   End If
!!OLD                   !
!!OLD                End If
!!OLD                !
!!OLD                !   *   for /fine resolution bandwidth must be (512)
!!OLD                !
!!OLD                bwHERA = FTSbw%fineHERA
!!OLD                !
!!OLD                If (abs(listFTS(ii)%bandWidth-bwHERA).Gt.fDelta) Then
!!OLD                   error = .True.
!!OLD                   l = Len_trim(errorCode)
!!OLD                   If (Len(errorCode).Ge.l+80) Then
!!OLD                      Write (errorCode,'(A, F8.3, A, F8.1, A)')                  & 
!!OLD                           &  " for resolution ",                                &
!!OLD                           &   FTSres%fine,                                      &
!!OLD                           &  " on HERA: bandwidth must be ",                    &
!!OLD                           &   bwHERA,                                           &
!!OLD                           &  "."                                                !
!!OLD                   End If
!!OLD                End If
!!OLD                !
!!OLD             Else
!!OLD                !
!!OLD                !   *   for wide bandwidth HERA must have /width wide
!!OLD                !
!!OLD                If (          .Not.listRX(iHERA1)%width.Eq.width%wide            &
!!OLD                     &   .Or. .Not.listRX(iHERA2)%width.Eq.width%wide            &
!!OLD                     &   ) Then                                                  !
!!OLD                   !
!!OLD                   error = .True.
!!OLD                   l = Len_trim(errorCode)
!!OLD                   If (Len(errorCode).Ge.l+80) Then
!!OLD                      Write (errorCode,'(A, F8.3, A, A, A)')                     & 
!!OLD                           &  " for resolution ",                                &
!!OLD                           &   FTSres%wide,                                      &
!!OLD                           &  ": HERA must have /width ",                        &
!!OLD                           &   width%wide,                                       &
!!OLD                           &  "."                                                !
!!OLD                   End If
!!OLD                   !
!!OLD                End If
!!OLD                !
!!OLD                !   *   for not /fine resolution bandwidth must be (1024)
!!OLD                !
!!OLD                bwHERA = stdBandWidthHERA (iFTS)
!!OLD                !
!!OLD                If (abs(listFTS(ii)%bandWidth-bwHERA).Gt.fDelta) Then
!!OLD                   error = .True.
!!OLD                   l = Len_trim(errorCode)
!!OLD                   If (Len(errorCode).Ge.l+80) Then
!!OLD                      Write (errorCode,'(A, F8.3, A, F8.1, A)')                  & 
!!OLD                           &  " for resolution ",                                &
!!OLD                           &   FTSres%wide,                                      &
!!OLD                           &  " on HERA: bandwidth must be ",                    &
!!OLD                           &   bwHERA,                                           &
!!OLD                           &  "."                                                !
!!OLD                   End If
!!OLD                End If
!!OLD                !
!!OLD             End If
             !
             !   *   maximum bandwidth of FTS on HERA (GP 2011-07-21)
             !
             fDelta = 0.01
             !
             !       maxBandWidthHeraFTSfine
             !
             If (abs(listFTS(ii)%resolution-FTSres%fine).Le.0.01) Then
                !
                If (        listFTS(ii)%bandWidth-maxBandWidthHeraFTSfine        &
                     & .Gt. fDelta) Then                                         !
                   !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+80) Then
                      Write (errorCode,'(A, F8.3, A, F8.3, A)')                  & 
                           &  " for /fine resolution",                           &
                           &   FTSres%fine,                                      &
                           &  " on HERA max. width is ",                         &
                           &   maxBandWidthHeraFTSfine,                          &
                           &  "."                                                !
                   End If
                   !
                End If
                !
             End If
             !
             !       maxBandWidthHeraFTSwide
             !
             If (abs(listFTS(ii)%resolution-FTSres%wide).Le.0.01) Then
                !
                If (        listFTS(ii)%bandWidth-maxBandWidthHeraFTSwide        &
                     & .Gt. fDelta) Then                                         !
                   !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+80) Then
                      Write (errorCode,'(A, F8.3, A, F8.3, A)')                  & 
                           &  " for resolution",                                 &
                           &   FTSres%wide,                                      &
                           &  " on HERA max. width is ",                         &
                           &   maxBandWidthHeraFTSwide,                          &
                           &  "."                                                !
                   End If
                   !
                End If
                !
             End If
             !
             !   *   all parts must have same resolution
             !
             Do kk = ii+1, 2, 1          
                !
                If (listFTS(kk)%isConnected) Then
                   !
                   If (Abs(listFTS(ii)%resolution-listFTS(kk)%resolution)        &
                        & .Gt.0.02) Then                                         !
                      error = .True.
                      l = Len_trim(errorCode)
                      If (Len(errorCode).Ge.l+80) Then
                         errorCode = errorCode(1:l)//" all parts"                &
                              &  //" must have same resolution."                 !
                      End If
                   End If
                End If
                !
             End Do
                !
             If (.Not. error) Then
                countParts = countParts+1
                percentUsed = percentUsed+50.0
             End If
             !
          Else     !!  not EMIR nor HERA
             !
             error = .True.
             l = Len_trim(errorCode)
             If (Len(errorCode).Ge.l+30) Then
                errorCode = errorCode(1:l)//" works only with EMIR or HERA"
             End If
             !
          End If   !!  If EMIR Else If HERA or Else
          !
       End If
    End Do
    !
    !  **   HERA
    !   *   FTS parts 1 and 2 can no be on the same HERA (1,2)
    !
    If (receiverIsHERA) Then    
       !
       If (listFTS(1)%isConnected.And.listFTS(2)%isConnected) Then
          !
          If (             listFTS(1)%receiverName                               &
               &       .Eq.listFTS(2)%receiverName) Then                         !
             error = .True.
             l = Len_trim(errorCode)
             If (Len(errorCode).Ge.l+80) Then
                errorCode = errorCode(1:l)                                       &
                     &  //" parts 1 and 2 can not both be on "                   &
                     &  //listFTS(1)%receiverName                                &
                     &  //"."                                                    !
             End If
          End If
          !
       End If
       !
    End If
    !
    If (           countParts.Ge.1  ) Then
       If (GV%switchingMode.Ne.GPnone .And. GV%tPhase.Le.0.1-0.001) Then
          error = .True.
          l = Len_trim(errorCode)
          If (Len(errorCode).Ge.l+80) Then
             Write (errorCode(l+1:Len(errorCode)), '(a,F8.2,a,a,a,a,a)')         &
                  &   " /tPhase ",            GV%tPhase,                         &
                  &   " in ",                 GV%switchingMode,                  &
                  &   " too small for ",      bac%FTS,                           &
                  &   "."                                                        !
          End If
       End If
    End If
    !
    If (           countParts.Gt.8                                               &
         &   .Or. (receiverIsEMIR.And.countParts.Gt.8)                           &
         &   .Or. (receiverIsHERA.And.countParts.Gt.2)                           &
         &   ) Then                                                              !
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts."
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels."
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    GV%backendFTS = .Not.ERROR .And. countParts.Ge.1
    !
    Return
    !
  End Subroutine checkFTS
!!!
!!!
  Subroutine checkUSB (listRX, listUSB,                                          &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX         !! list of all Receivers                 !
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listUSB        !! list of all USB parts                 !
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, l
    !
    Integer :: countParts
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: checkUSB")                          !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listUSB,1)
    uBoundBE = Ubound(listUSB,1)
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       If (listUSB(ii)%isConnected) Then
          jj = jj+1
          countParts = countParts+1
          percentUsed = percentUsed+100.0/nDimUSB
          If (listUSB(ii)%resolution .Gt. listUSB(ii)%bandWidth ) Then
             error = .True.
             l = Len_trim(errorCode)
             If (Len(errorCode).Ge.l+30) Then
                errorCode = errorCode(1:l)//" resolution > bandwidth"
             End If
          End If
       End If
    End Do
    !
    If (countParts .Gt. nDimUSB) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.001) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkUSB
!!!
!!!
  Subroutine checkWILMA (listRX, listWILMA,                                      &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX         !! list of all Receivers                 !
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listWILMA      !! list of all WILMA parts               !
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, kk, l, l2
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR
    !
    Real    :: fShiftMin, fShiftMax
    Character(len=lenVar)  :: limitCode
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &                      "    --> SR: checkWILMA")                        !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    !D    Write (6,*) "      lBoundRX ", lBoundRX
    !D    Write (6,*) "      uBoundRX ", uBoundRX
    !
    !
    lBoundBE = Lbound(listWILMA,1)
    uBoundBE = Ubound(listWILMA,1)
    !
    !D     Write (6,*) "      lBoundBE ", lBoundBE
    !D     Write (6,*) "      uBoundBE ", uBoundBE
    !
    jj = 0
    Do ii = lBoundBE,uBoundBE,1
       If (listWILMA(ii)%isConnected) Then
          !
          !  **  EMIR
          !
          If (receiverIsEMIR) Then
             !
             jj = jj+1
             !
             If (ii.LT.uBoundBE) Then
                Do kk = ii+1, uBoundBE, 1
                   If (         listWILMA(ii)%receiverName.Eq.listWILMA(kk)%receiverName  &
                        & .And. listWILMA(ii)%polarization.Eq.listWILMA(kk)%polarization  &
                        & .And. listWILMA(ii)%subband.Eq.listWILMA(kk)%subband ) Then     !
                      error = .True.
                      l = Len_trim(errorCode)
                      If (Len(errorCode).Ge.l+30) Then
                         errorCode = errorCode(1:l)//                            &
                              &    " parts must be on different EMIR sub bands." !
                      End If
                   End If
                End Do
             End If
             !
             If (jj.Gt.4) Then
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" maximum 4 parts with EMIR."
                End If
             End If
             !
             If     (           listWILMA(ii)%receiverName .Eq. rec%E090         &
                  & .And. (     listWILMA(ii)%subband      .Eq. esb%lsb          &
                  &        .Or. listWILMA(ii)%subband      .Eq. esb%usb )        &
                  & ) Then                                                       !
                !
                If (.Not. ( Abs(listWILMA(ii)%bandWidth-                         &
                     &          stdBandWidth (iWILMA,iLSB)) .Lt. 50.0            &
                     &    ) ) Then                                               !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//" wrong bandwidth."
                   End If
                End If
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+50.0
                End If
                !
             Else
                !
                If (.Not. ( Abs(listWILMA(ii)%bandWidth-                         &
                     &          stdBandWidth (iWILMA,iLO)) .Lt. 50.0             &
                     &    ) ) Then                                               !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//" wrong bandwidth."
                   End If
                End If
                If (.Not. error) Then
                   countParts = countParts+1
                   percentUsed = percentUsed+25.0
                End If
                !
             End If
             !
             !! **   EMIR: fShift depending on EMIR sub band
             !! TBD: abtract the following checks on fShift into a Subroutine
             !!      compare checkContinuum, checkWILMA, checkFB4MHz, checkFB1MHz
             !
             If (listWILMA(ii)%subBand .Eq. esb%lo) Then
                !
                fShiftMin = Min ( fRangeSky(iWILMA,iLO,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iLO,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iWILMA,iLO,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iLO,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
                !
             Else If (listWILMA(ii)%subBand .Eq. esb%li) Then
                !
                fShiftMin = Min ( fRangeSky(iWILMA,iLI,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iLI,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iWILMA,iLI,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iLI,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
                !
             Else If (listWILMA(ii)%subBand .Eq. esb%ui) Then
                !
                fShiftMin = Min ( fRangeSky(iWILMA,iUI,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iUI,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iWILMA,iUI,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iUI,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
                !
             Else If (listWILMA(ii)%subBand .Eq. esb%uo) Then
                !
                fShiftMin = Min ( fRangeSky(iWILMA,iUO,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iUO,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
                fShiftMax = Max ( fRangeSky(iWILMA,iUO,iCenterFrom),             &
                     &            fRangeSky(iWILMA,iUO,iCenterTo)    )           &
                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
                !
             End If
             !
             If (        listWILMA(ii)%fShift.LT.fShiftMin                       &
                  & .Or. listWILMA(ii)%fShift.GT.fShiftMax  )   Then             !
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" invalid fShift "
                End If
                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
                     &            listWILMA(ii)%fShift, fShiftMin, fShiftMax     !
                l  = Len_trim(errorCode)
                l2 = Len_trim(limitCode)
                If ( Len(errorCode) .Ge. (l+l2) ) Then
                   errorCode = errorCode(1:l)//limitCode(1:l2)
                End If
             End If
             !
          Else  !!  not EMIR
             !
             If (        listWILMA(ii)%receiverName.Eq.rec%Hera1                 &
                  & .Or. listWILMA(ii)%receiverName.Eq.rec%Hera2) Then
                jj = jj+1
                countParts = countParts+1
                percentUsed = percentUsed+50.0
             Else
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" wrong receiver"
                End If
             End If
             If (.Not. ( Abs(listWILMA(ii)%bandWidth-1024.0) .Lt. 0.001          &
                  &    ) ) Then
                error = .True.
                l = Len_trim(errorCode)
                If (Len(errorCode).Ge.l+30) Then
                   errorCode = errorCode(1:l)//" wrong bandwidth"
                End If
             End If
             !
          End If
          !
       End If
       !
    End Do
    !
    If (countParts.Gt.4 .Or. (.Not.receiverIsEMIR.And.countParts.Gt.2)) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkWILMA
!!!
!!!
  Subroutine checkVespaEMIR (listRX, listVESPA,                                  &
       &                 error,  errorCode, percentUsed)                         !
    !
    Type (varReceiver),  Dimension(:), Intent(IN)  ::  listRX                    ! !! list of all Receivers
    Type (varBackend),   Dimension(:), Intent(IN)  ::  listVESPA                 ! !! list of all VESPA parts    
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, l
    !
    Integer :: countParts
    !
    Logical :: receiverIsEMIR, isConnectedRX
    !
    Character(len=lenCh)   ::  otherPolarization     =  GPnone         
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &     "    --> SR: checkVespaEMIR v 1.2.3  2013-02-27")                 !
    !
    error        = .False.
    percentUsed  = 0.0
    !
    countParts   = 0
    !
    Call queryReceiver("EMIR", receiverIsEMIR)
    !
    If (Len(errorCode).Ge.2) Then
       errorCode = ""
    End If
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    lBoundBE = Lbound(listVESPA,1)
    uBoundBE = Ubound(listVESPA,1)
    !
    jj = 0
    !
    ! FIXME: we have to match from EMIRsubBands
    Do ii = lBoundBE,uBoundBE,1
       !
       If (listVESPA(ii)%isConnected) Then
          jj = jj+1
          countParts = countParts+1
          !          
          !  **  EMIR
          !
          If (receiverIsEMIR) Then
             !
             If    (      listVESPA(ii)%receiverName .Eq. rec%E090) Then
                If (      listVESPA(ii)%subband      .Eq. esb%lsb                &
                   & .Or. listVESPA(ii)%subband      .Eq. esb%usb ) Then         !
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)//                               &
                           &      listVESPA(ii)%subband//" not possible"         !
                   End If
                End If
             End If
             !
             If    (      listVESPA(ii)%mode .Eq. BEmode%pola                    &
                  &  .Or. listVESPA(ii)%mode .Eq. BEmode%para  ) Then            !
                !
                If      (listVESPA(ii)%polarization .Eq. pol%hor) Then
                   otherPolarization = pol%ver
                Else If (listVESPA(ii)%polarization .Eq. pol%ver) Then
                   otherPolarization = pol%hor
                End If
                !
                Call queryReceiver(               listVESPA(ii)%receiverName,    &
                     &             IsConnected  = IsConnectedRX,                 &
                     &             polarization = otherPolarization,             &
                     &             subband      = listVESPA(ii)%subband     )    !
                !
                If (GV%doDebugMessages) Then
                   !
                   Write (6,*)  " "
                   Write (6,*)  "      listVESPA(ii)%mode: ", listVESPA(ii)%mode
                   Write (6,*)  "      POLA"
                   Write (6,*)  "      listVESPA(ii)%fShift       : ", listVESPA(ii)%fShift      
                   Write (6,*)  "      listVESPA(ii)%receiverName : ", listVESPA(ii)%receiverName
                   Write (6,*)  "      listVESPA(ii)%polarization : ", listVESPA(ii)%polarization
                   Write (6,*)  "      listVESPA(ii)%subBand      : ", listVESPA(ii)%subBand
                   Write (6,*)  "      otherPolarization          : ", otherPolarization
                   Write (6,*)  "      IsConnectedRX              : ", IsConnectedRX
                   !
                End If
                !                
             End If
             !
             If    (      listVESPA(ii)%mode .Eq. BEmode%pola  ) Then
                !
                If (.Not. IsConnectedRX) Then
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)                                 &
                           &   // listVESPA(ii)%mode                             &
                           &   // " requires RECEIVER "                          &
                           &   // listVESPA(ii)%receiverName                     &
                           &   // "/"//otherPolarization                         &
                           &   // " "//listVESPA(ii)%subband                     !
                   End If
                End If
                !
             End If
             !
             If    (      listVESPA(ii)%mode .Eq. BEmode%para  ) Then
                !
                If (.Not. IsConnectedRX) Then
                   error = .True.
                   l = Len_trim(errorCode)
                   If (Len(errorCode).Ge.l+30) Then
                      errorCode = errorCode(1:l)                                 &
                           &   // "/"//listVESPA(ii)%mode                        &
                           &   // " requires RECEIVER "                          &
                           &   // listVESPA(ii)%receiverName                     &
                           &   // "/"//otherPolarization                         &
                           &   // " "//listVESPA(ii)%subband                     !
                   End If
                End If
                !
             End If
             !
          End If
          !
       End If
       !
    End Do
    !
    If (countParts .Gt. 12) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many parts"
       End If
    End If
    !
    If (percentUsed .Gt. 100.) Then
       error = .True.
       l = Len_trim(errorCode)
       If (Len(errorCode).Ge.l+30) Then
          errorCode = errorCode(1:l)//" too many channels"
       End If
    End If
    !
    If (.Not. ERROR) Then
       If (Len(errorCode).Ge.2) Then
          errorCode = "OK"
       End If
    End If
    !
    Return
    !
  End Subroutine checkVespaEMIR
!!!
!!!                                                                              ! !! findEsb is a utility for the
!!!                                                                              ! !! following checkVESPA (by GP)
  Subroutine findEsb(backend, sb1, sb2)
    Type (varBackend), Intent(IN) :: backend
    Integer, Intent(OUT)          :: sb1, sb2

    Integer:: i

    sb1 = 0
    sb2 = 0

    Do i = 1, nesbIF
       If (EMIRsubBands(i)%isRequested &
         & .and. (EMIRsubBands(i)%bandName .eq. backend%receiverName) &
         & .and. (EMIRsubBands(i)%subband .eq. backend%subband)) Then
          If (EMIRsubBands(i)%polarization .eq. backend%polarization) Then
             sb1 = i
          Else
             sb2 = i
          End If
       End If
    End Do
  End Subroutine findEsb
!!!
!!!
  Subroutine checkVespa (listRX, listVESPA,                                      &
       &                 error,  errorCode, percentUsed)
    !
    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
         &               listRX         !! list of all Receivers
    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
         &               listVESPA      !! list of all VESPA parts    
    !
    Logical,                Intent(OUT)            ::  error
    Character(len=*),       Intent(OUT), Optional  ::  errorCode
    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
    !
    ! Actually 19 is not used, it was foreseen for 20kHz polarimetry but
    ! implementing it would require pervasive changes through the system.
    Integer, Parameter :: NumModes = 24

    Interface 
       Function CheckSigList(SigList)
         Integer, Dimension(12), Intent(In):: SigList
         Integer:: CheckSigList
       End Function CheckSigList
    End Interface

    ! There can only be 12 different Vespa bands.
    Integer, Parameter :: MaxActive = 12

    ! Step and limits for the synthesizers (after LO4 shift)
    Real(Kind=kindSingle), Parameter:: &
         &    stepSyn = 0.625,              &
         &    minSyn  = 105.0,              &
         &    maxSyn  = 600.0

    ! Low and upper frequency limits in IF2
    Real(Kind=kindSingle), Parameter:: &
         &    minIF = 80.0
    Real(Kind=kindSingle), Parameter:: &
         &    maxIF = 647.0

    Integer, Dimension(NumModes), Parameter :: &
         &   sigBases=(/7,  7,  7, 11, 11, 15, 19, 19, &
         &             23, 23, 23, 29, 29,             &
         &             35, 35, 35, 41, 47, 53,         &
         &              1,  1,  1,  1,  3/)

    Integer, Parameter:: SIG_UNUSED = 59

    Integer, Dimension(NumModes), Parameter :: &
         &   cost=(/1, 1, 1, 1, 1, 1, 2, 2, &
         &          1, 1, 1, 1, 1,          &
         &          1, 1, 1, 1, 2, 4,       &
         &          3, 3, 3, 3, 6/)

    Logical, Dimension(NumModes), Parameter :: &
         &   uumode =(/.False., .False., .False., .False.,                   &
         &                               .False., .False., .False., .True.,  &
         &             .False., .False., .False., .False., .False.,          & 
         &             .True.,  .True.,  .True.,  .False., .False., .False., &
         &             .False., .False., .False., .False., .False./)

    Integer, Dimension(NumModes), Parameter :: &
         &   dropped = (/0, 0, 0, 0, 1, 2, 2, 3,    &
         &               0, 0, 0, 0, 1,             &
         &               1, 1, 1, 1, 1, 1,          &
         &               0, 0, 0, 1, 1/)

    ! Number of channels per band
    Integer, Dimension(NumModes), Parameter :: &
         &   bchans =(/128,  256,  512, 1024, 1024, 2048, 3072, 3072, &
         &             128,  256,  512, 1024, 1024,                   &
         &              32,   64,  128,  256,  512, 1024,             &
         &             128,  256,  512,  512, 1024/)

    ! Number of frequency domain channels in the raw data
    Integer, Dimension(NumModes), Parameter :: &
         &   fchans =(/128,  256,  512, 1024, 1024, 2048, 3072, 3072, &
         &             256,  512, 1024, 2048, 2048,                   &
         &             128,  256,  512, 1024, 2048, 4096,             &
         &            1152, 2304, 4608, 4608, 9216/)

    ! Number of time domain channels 
    Integer, Dimension(NumModes), Parameter :: &
         &   tchans =(/512,  512,  512, 1024, 1024, 2048, 3072, 3072, &
         &            1024, 1024, 1024, 2048, 2048,                   &
         &             512,  512,  512, 1024, 2048, 4096,             &
         &            4608, 4608, 4608, 4608, 9216/)

    ! These are the rounded values for the user interface
    !Real(kind=kindSingle), Dimension(NumModes), Parameter ::        &
    !&   resolutions = (/1250., 320.,  80., 40., 20., 10., 6.6, 3.3, &
    !&                   1250., 320.,  80., 40., 20.,                &
    !&                   2500., 640., 160., 80., 40., 20.,           &
    !&                   1250., 320.,  80., 40., 20./)

    ! These are the rounded values for the user interface
    Real(kind=kindSingle), Dimension(NumModes), Parameter ::             &
         &   minRes = (/1.2, 0.31, 0.07, 0.038, 0.019, 0.009, 0.0065, 0.0032, &
         &              1.2, 0.31, 0.07, 0.038, 0.019,                        &
         &              2.4, 0.62, 0.15, 0.070, 0.038, 0.019,                 &
         &              1.2, 0.31, 0.07, 0.038, 0.019/)

    ! These are the rounded values for the user interface
    Real(kind=kindSingle), Dimension(NumModes), Parameter ::             &
         &   maxRes = (/1.3, 0.33, 0.09, 0.042, 0.021, 0.011, 0.0067, 0.0034, &
         &              1.3, 0.33, 0.09, 0.042, 0.021,                        &
         &              2.6, 0.65, 0.17, 0.090, 0.042, 0.021,                 &
         &              2.4, 0.33, 0.09, 0.042, 0.021/)

    ! and these are exact values of channels per 5MHz for frequency checks.
    Integer, Dimension(NumModes), Parameter ::              &
         &   chansPer5M = (/4, 16, 64, 128, 256, 512, 768, 1536, &
         &                  4, 16, 64, 128, 256,                 &
         &                  2,  8, 32,  64, 128, 256,            &
         &                  4, 16, 64, 128, 256/)

    Integer, Dimension(NumModes), Parameter :: &
         &   bwMin =(/160, 80, 40, 40, 20, 20, 20, 10, &
         &            160, 80, 40, 40, 20,             &
         &             80, 40, 20, 20, 20, 20,         &
         &            160, 80, 40, 20, 20/)

    Integer, Dimension(NumModes), Parameter :: &
         &   bwMax =(/640, 640, 480, 480, 240, 120, 120, 60, &
         &            640, 640, 480, 360, 180,               &
         &            640, 480, 240, 240, 120,  60,          &
         &            640, 320, 160,  80, 40/)

    ! In this table 64 means invalid entry.
    Integer, Dimension(4,4), Parameter:: &
         &   r2code = reshape((/64, 0,  1, 2,   &
         &                       0, 64, 3, 4,   &
         &                       1, 3, 64, 5,   &
         &                       2, 4, 5, 64/), &
         &                    (/4, 4/))

    Integer, Dimension(4), Parameter:: &
         &    ModeStart =(/1,  9, 14, 20/), &
         &    ModeEnd   =(/8, 13, 18, 24/)

    Type:: VespaBand
       Integer               :: Origin
       Integer               :: Ident ! May turn out to be useless
       Integer               :: Mode
       Integer               :: RX1
       Integer               :: input1
       Integer               :: input2
       Real(kind=kindSingle) :: percentage
       Real(kind=kindSingle) :: fShift
       Integer               :: nBands
       Integer               :: kind
       Integer               :: Signature
       Logical               :: Flipping
    End Type VespaBand

    Type(VespaBand), Dimension(MaxActive) :: Bands
    !
    Integer :: lBoundRX, uBoundRX
    Integer :: lBoundBE, uBoundBE
    Integer :: ii, jj, nActive, nr1, nOrig, iMode, bw
    Integer :: totcost, nDP4, iSig, tmpSig
    Integer :: nUsed
    Logical :: valid, Flipping
    Logical :: useEMIR
    Integer, Dimension(MaxActive) :: sigList
    Real(kind=kindSingle) :: res, deltaSyn, xFreq
    Real(kind=kindSingle) :: lowSyn, highSyn, lowFreq, highFreq
    Real(kind=kindSingle) :: lowShift, highShift
    !
    Call pako_message(seve%t,"paKo backend",                                     &
         &     "    --> SR: checkVespa v 1.2.3  2013-03-01 "                     &
         &               //  " like <= v 1.1.14 2012-11-30 ")                    !
    !
    error        = .False.
    errorCode    = "OK"
    percentUsed  = 0.0
    Call queryReceiver("EMIR", useEMIR)
    !
    lBoundRX = Lbound(listRX,1)
    uBoundRX = Ubound(listRX,1)
    !
    !D    Write (6,*) "      lBoundRX ", lBoundRX
    !D    Write (6,*) "      uBoundRX ", uBoundRX
    !
    !
    lBoundBE = Lbound(listVESPA,1)
    uBoundBE = Ubound(listVESPA,1)

    ! FIXME! Problem with initial conditions: if no receiver 
    ! command has been entered, all receivers have name "none" 
    ! and also are disconnected. This plays havoc with the name 
    ! matching done a bit later, so we exit early. There is anyway
    ! no point in trying to configure Vespa under these conditions.
    ! But of course this code should not complain if a BACK/CLEAR
    ! command comes before any receiver definition, so first
    ! check that we are not called for an empty configuration,
    ! which happens to be always valid.

    valid = .False.
    Do ii = lBoundBE, uBoundBE
       valid = valid .Or. listVespa(ii)%isConnected
    End Do
    If (.Not. valid) Return

    valid = .False.
    Do ii = lBoundRX, uBoundRX
       valid = valid .Or. listRX(ii)%isConnected
       !print *, "Receiver ", ii, " has ", listRX(ii)%nPixels, " pixels."
    End Do

    If (.Not. valid) Then
       error = .True.
       errorCode = "No receiver configured"
       Return
    End If

    nActive = 0
    Do ii = lBoundBE, uBoundBE
       If (listVESPA(ii)%isConnected) Then
          If (nActive.Eq.12) Then
             error = .True.
             errorCode = "Vespa can have at most 12 active parts"
             Return
          End If
          If (useEMIR) Then
             !
             If    (      listVESPA(ii)%receiverName .Eq. rec%E090) Then
                If (      listVESPA(ii)%subband      .Eq. esb%lsb                &
                   & .Or. listVESPA(ii)%subband      .Eq. esb%usb ) Then         !
                   error = .True.
                   errorCode = "Subband " // listVESPA(ii)%subband // &
                        & " not possible for Vespa"         !
                   Return
                End If
             End If
          End If
          nActive = nActive+1
          Bands(nActive)%Origin = ii
          Bands(nActive)%Ident = listVespa(ii)%nPart
          Bands(nActive)%percentage = listVespa(ii)%percentage
          Bands(nActive)%fShift = listVespa(ii)%fShift
          ! listVespa(ii)%Mode is often "none", let us default to "simple"
          Bands(nActive)%Mode = iSimp
          Do jj = 1, nDimBEmodeChoices 
             If (listVespa(ii)%mode .Eq. BEmodeChoices(jj)) Then
                Bands(nActive)%Mode = jj 
             End If
          End Do
          Flipping = listVespa(ii)%subband .eq. esb%usb &
               & .or. listVespa(ii)%subband .eq. esb%ui &
               & .or. listVespa(ii)%subband .eq. esb%lo
          Bands(ii)%Flipping = Flipping

          Bands(nActive)%RX1 = lBoundRX-1
          ! FIXME: here we need an IF cable equivalent, even if it is
          ! arbitrary, EMIRsubBands should be the right band to test
          Do jj = lBoundRX, uBoundRX
             If (listVespa(ii)%receiverName .Eq. listRX(jj)%name) Then  
                Bands(nActive)%RX1 = jj
                If (useEMIR) Then
                   call findEsb(listVespa(ii), Bands(nActive)%input1, &
                        &       Bands(nActive)%input2)
                Else
                   Bands(nActive)%input1 = listRX(jj)%disBoxInput
                End If
             End If
          End Do
       End If
    End Do

    totcost = 0
    nDP4 = 0
    Do ii=1,nActive
       ! Receiver verifications
       nr1 = Bands(ii)%RX1
       If (nr1 .Eq. lBoundRx-1) Then
          error = .True.
          errorCode = "No receiver specified"
          Return
       End If
       If (.Not.(listRX(nr1)%isConnected)) Then
          error = .True.
          errorCode = "Attempt to use a disabled receiver"
          Return
       End If
       If (Bands(ii)%input1 .eq. 0) Then
          error = .True.
          errorCode = "Attempt to use an unconnected subband"
          Return
       End If
       If (Bands(ii)%Mode .Ne. iSimp) Then
          If (listRX(nr1)%nPixels .Ne. 1) Then
             error = .True.
             errorCode = "Parallel and polarization modes are only available" &
                  //" with single pixel receivers"
             Return
          End If
          If (Bands(ii)%input2 .eq.0) Then
             error = .True.
             errorCode = "Parallel and polarization modes need two identical" &
                  // "subbands from H and V mixers"
             Return
          End If
       End If

       ! Take receiver setup into account to transform frequency offset
       ! into IF frequency.
       ! Some compilers accept .XOR. for .NEQV., but sadly not all!

       iMode = Bands(ii)%Mode
       If (listRX(nr1)%nPixels .Eq. 9) Then
          iMode= 4 ! We know that it was 1
       Else If (listRX(nr1)%nPixels .Ne. 1) Then
          error = .True.
          errorCode = "Vespa only accepts receivers with 1 or 9 pixels"
          Return
       End If
       nOrig = Bands(ii)%Origin


       jj = ModeStart(iMode)
       res = listVespa(nOrig)%resolution
       Do While((res .Lt. minRes(jj)) .And. (jj.Ne.ModeEnd(iMode)))
          jj = jj+1
       End Do

       If ((res .Lt. minRes(jj)) .Or. (res.Gt. maxRes(jj))) Then
          error = .True.
          errorCode = "Invalid resolution (type it in MHz)"
          Return
       End If

       ! FIXME: it would be better to put the values back in place in
       ! listVespa since after this step the system should only see
       ! canonical values for resolution and bandwidth 
       ! (within round-off errors of course).
       Bands(ii)%kind=jj
       ! Now fill the signature field, by far the most important item 
       ! in the whole Vespa configuration rigamarole
       If (Bands(ii)%Mode .Eq. iSimp) Then
          Bands(ii)%Signature = sigBases(jj) + Bands(ii)%input1-1
       Else
          Bands(ii)%Signature = sigBases(jj) +  &
               r2code(Bands(ii)%input1, Bands(ii)%input2)
       End If

       bw = Nint(listVespa(nOrig)%bandWidth)/bwMin(jj)*bwMin(jj)
       If (Abs(bw-listVespa(nOrig)%bandWidth) .Gt. 0.1) Then
          error = .True.
          Write (errorCode, '(A, I3, A)') &
               "Bandwidth must be a multiple of ", bwmin(jj), "MHz"
       End If
       If (bw .Gt. bwMax(jj)) Then
          error = .True. 
          errorcode = "Requested bandwidth is too large"
          Return
       End If
       Bands(ii)%nBands = bw/bwMin(jj)
       totcost = totcost + Bands(ii)%nBands * cost(jj)
       If (sigBases(jj).Eq.29) Then
          nDP4 = nDP4 + Bands(ii)%nBands
       End If
       !D       print *, "jj res bw signature", jj, res, bw, Bands(ii)%Signature
    End Do

    ! Now evaluate the total cost and exit early if it turns out to be 
    ! impossible. DP4 (20 or 40 kHz resolution in parallel mode) bands 
    ! are special, the first 6 are cheap, although they may constrain
    ! distribution box setup, the last 3 are more expensive.
    If (nDP4 .Gt. 6) Then
       totcost = totcost + nDP4 - 6
    End If

    ! This estimate of percentUsed is correct when only Simple mode
    ! with resolutions between 20kHz and 1.25Mhz are used. As soon
    ! as parallel, polarimetry or ultra-high resolution (3.3, 6.6,
    ! and 10kHz) modes are used, this becomes an approximation. 
    percentUsed = totcost*(100./12.)

    If (totcost .Gt. 12) Then
       error = .True.
       errorCode = "Too many Vespa resources requested"
       Return
    End If

    iSig = 0
    Do ii = 1, nActive
       Do jj = 1, Bands(ii)%nBands
          iSig = iSig + 1
          sigList(iSig) = Bands(ii)%Signature
       End Do
    End Do

    Do ii = iSig+1, MaxActive
       sigList(ii) = SIG_UNUSED
    End Do

    ! Now sort sigList. The array is really small, so use a stupid method. 
    Do ii = 1, iSig-1
       Do jj = ii+1, iSig
          If (sigList(ii) .Gt. sigList(jj)) Then
             tmpSig = sigList(ii)
             sigList(ii) = sigList(jj)
             sigList(jj) = tmpSig
          End If
       End Do
    End Do
    !print *, "sigList", sigList

    ! At this point a configuration is possible if and only if
    ! siglist can be built from a list of 3 signature lists for
    ! a pair of units. 
    jj = CheckSigList(sigList)
    !print *, "CheckSigList result", jj
    If (jj.Ne.0) Then
       error = .True.
       errorCode = "The requested Vespa configuration is impossible"
       Return
    End If

    ! Now try to compute the frequencies of the synthesizers and see if they
    ! are in the allowed range. The arithmetic has to be done very carefully.
    Do ii = 1, nActive

       nr1 = Bands(ii)%RX1

       jj = Bands(ii)%kind
       ! Compute difference between succesive synthesizer frequencies
       ! rounded down do the maximum of synthesizer resolution and
       ! spectral resolution for this part.
       deltaSyn = bwMin(jj)*Bands(ii)%percentage/100
       res = Max(5.0/chansPer5M(jj), stepSyn)
       ! Difference between first and last synthesizer
       deltaSyn = res * Int(deltaSyn/res) * (Bands(ii)%nBands-1)
       ! Now the real resolution
       res = 5.0/chansPer5M(jj)

       ! Compute limits: lowSyn is the lowest possible frequency of
       ! the first synthesizer and highSyn the highest possible frequency
       ! of the last synthesizer. lowFreq and highFreq are the smallest
       ! and largest frequencies of the center of the part in the IF.

       ! There are 3 cases:
       !
       ! 1) dual sideband modes, in which both the L and U bands of
       ! an IRM are used. In these modes the number of dropped channels
       ! is zero. These are the lowest resolution simple and parallel 
       ! modes (80, 320, and 1250 kHz) as well as 40kHz resolution on
       ! single pixel receivers.
       !
       ! 2) upper side band mode:  in which only the U output of an IRM
       ! can be used. These are the 3.3 kHz resolution mode and the
       ! polarimetry modes at 160, 640, and 2500 kHz resolution.
       !
       ! 3) single side band modes, in which we either use the lower of
       ! the upper output of an IRM (but never mix them up). This is the 
       ! case for the 6.6, 10, and 20 kHz resolution modes on single pixel
       ! receiver (for 20kHz this alsmo includes parallel mode), the 20 and
       ! 40kHz resolution modes for multi-beam receivers, and the 40 and 80kHz
       ! resolution polarimetry modes.
       If (dropped(jj).Eq.0) Then
          nUsed = Int(bchans(jj)*Bands(ii)%percentage/100)/2 + 1
          xFreq = deltaSyn/2 + (nUsed-1)*res

          lowSyn = Ceiling(Max(minSyn, minIF+(nUsed-1)*res)/stepSyn)*stepSyn
          lowFreq = lowSyn + deltasyn/2
          highSyn = Floor(Min(maxSyn, maxIF-(nUsed-1)*res)/stepSyn)*stepSyn
          highFreq = highSyn - deltasyn/2
       Else
          nUsed = Int(bchans(jj)*Bands(ii)%percentage/100)

          xFreq =  (res*(nUsed+2*dropped(jj)-1) - deltasyn)/2

          highSyn = Floor(Min(maxSyn, &
               maxIF- (nUsed+dropped(jj)-1)*res)/stepSyn) &
               *stepSyn
          highFreq = highSyn + xFreq
          If (uumode(jj)) Then 
             lowSyn = minSyn
             lowFreq = minSyn + deltaSyn + xFreq
          Else
             lowSyn = Ceiling(Max(minSyn, &
                  minIF+(nUsed+dropped(jj)-1)*res)/stepSyn) &
                  *stepSyn
             lowFreq = lowSyn - xFreq
          End If
       End If
       !D       print *, "deltaSyn, xFreq", deltaSyn, xFreq
       !D       print *, "low and high syn and freq"
       !D       print *, lowSyn, highSyn, lowFreq, highFreq

       ! Convert highFreq and lowFreq to on sky frequency shifts

       xFreq = listRX(nr1)%IF2*1000
       If (Bands(ii)%Flipping) Then
          lowShift  = xFreq - highFreq
          highShift = xFreq - lowFreq
       Else
          lowShift  = lowFreq  - xFreq
          highShift = highFreq - xFreq
       End If

       If (lowShift .Gt. highShift) Then
          error = .True.
          errorCode = "Reduce the bandwidth or use the /PERCENT option"
          Return
       End If

       ! Round to the next 0.1 MHz step either way for the display
       ! in case the requested shift is out of bounds.
       lowShift = Floor(lowShift*10)/10.0
       highShift = Ceiling(highShift*10)/10.0
       !D       print *, "shift limits:", lowShift, highShift

       ! And even tolerate another 50kHz in case the observer believes
       ! in typing the tens of kHz, despite the fact that the synthesizers
       ! have a step of 625 kHz.
       If ((Bands(ii)%fShift .Lt. lowShift-0.05) .Or. &
            &   (Bands(ii)%fshift .Gt. highShift+0.05)) Then
          error = .True.
          Write (errorCode, '(A, 2F7.1)') "Frequency shift outside limits:", &
               lowShift, highShift
          Return
       End If
    End Do

    jj = 0
    Do ii = lBoundRX,uBoundRX,1
       If (listRX(ii)%isConnected) Then
          !D          Write (6,*) " ii ", ii
          l1 = Len_trim(listRX(ii)%name)
          !D          Write (6,*)                                                                 & 
          !D               &          listRX(ii)%name(1:l1)," ",                                  &
          !D               &          listRX(ii)%lineName(1:Len_trim(listRX(ii)%lineName))," ",   &
          !D               &          listRX(ii)%frequency%value,                                 &
          !D               &          listRX(ii)%offset%value,                                    &
          !D               &          listRX(ii)%sideBand(1:Len_trim(listRX(ii)%sideBand))," ",   &
          !D               &          listRX(ii)%frequency%doppler (1:Len_trim(listRX(ii)%frequency%doppler)), " ",   &
          !D               &          listRX(ii)%width(1:Len_trim(listRX(ii)%width))," ",         &
          !D               &          listRX(ii)%gainImage%value,                                 &
          !D               &          listRX(ii)%tempCold,                                        & 
          !D               &          listRX(ii)%tempAmbient,                                     &
          !D               &          listRX(ii)%effForward,                                      &
          !D               &          listRX(ii)%effBeam,                                         &
          !D               &          listRX(ii)%scale(1:Len_trim(listRX(ii)%scale))," ",         &
          !D               &          listRX(ii)%iPixel,                                          &
          !D               &          listRX(ii)%nPixels,                                         &
          !D               &          listRX(ii)%centerIf,                                        &
          !D               &          listRX(ii)%bandWidth,                                       &
          !D               &          listRX(ii)%disBoxInput,                                     &
          !D               &          listRX(ii)%IF2,                                             &
          !D               &          listRX(ii)%isFlippingIF2,                                   &
          !D               &          listRX(ii)%polarization(1:len_trim(listRX(ii)%polarization))," ",  &
          !D               &          listRX(ii)%useSpecialLO
       End If
    End Do
    !
    !D  jj = 0
    !D  Do ii = lBoundBE,uBoundBE,1
    !D     If (listVESPA(ii)%isConnected) Then
    !D          Write (6,*) " ii ", ii
    !D        jj = jj+1
    !D        l1 = Len_trim(listVESPA(ii)%name)
    !D        l2 = Len_trim(listVESPA(ii)%receiverName)
    !D          Write (6,*)                                                 &
    !D               &          listVESPA(ii)%name(1:l1)," ",               &
    !D               &          listVESPA(ii)%nPart,                        &
    !D               &          listVESPA(ii)%resolution,                   &
    !D               &          listVESPA(ii)%bandwidth,                    &
    !D               &          listVESPA(ii)%fShift,                       &
    !D               &     " ", listVESPA(ii)%receiverName(1:l2)," ",       &
    !D               &          listVESPA(ii)%receiverName2," ",            &
    !D               &          listVESPA(ii)%mode," ",                     &
    !D               &          listVESPA(ii)%percentage,                   &
    !D               &          " "
    !D     End If
    !D  End Do
    !
    Return
    !
  End Subroutine checkVespa
!!!
!!!
End Module modulePakoBackend
!!!
!!!  ************************
!!!  ************************
!!!  *****  END OF CODE *****
!!!  ************************
!!!  ************************
!!!                                                                              ! !! REVIEWED TO HERE 2013-02-27



!!OLDWrite (6,*) "   ii, listFB1MHz(ii)%receiverName, listFB1MHz(ii)%bandWidth, listFB1MHz(ii)%fShift ",  &
!!OLD     &          ii, listFB1MHz(ii)%receiverName, listFB1MHz(ii)%bandWidth, listFB1MHz(ii)%fShift     !
!!OLD!

!!OLD                            errorCode = "123456789A123456789B123456789C123456789D" &
!!OLD                                 &    //"123456789E123456789F123456789G123456789H" &
!!OLD                                 &    //"123456789I123456789J123456789K123456789L" &
!!OLD                                 &    //"123456789M123456789N123456789O123456789P" !

!!OLD
!!OLD       Write (6,*) "          Len(errorCode):    ", Len(errorCode)
!!OLD       Write (6,*) "       Len_trim(errorCode):  ", Len_trim(errorCode)
!!OLD       Write (6,*) "       length:               ", length
!!OLD       Write (6,*) "       Len(messageText):     ", Len(messageText)
!!OLD
!!OLD       length = Min (length,70)
!!OLD       Write (6,*) "      Len_trim(messageText):   ", Len_trim(messageText)
!!OLD

                      !D                       Write (6,*) " kk: ", kk, listFTS(kk)%receiverName,         &
                      !D                            &       listFTS(kk)%polarization, listFTS(kk)%subband !
                      !D                       Write (6,*) " kk: ", kk, listFTS(kk)%receiverName,         &
                      !D                            &       listFTS(kk)%polarization, listFTS(kk)%subband !
                      !
             !
!!OLD                   If (jj.eq.iVESPA) Then
!!OLD                      Write   (messageText,                                         &
!!OLD                           &    '(A10,I3,F10.3,F10.1,F10.3,                         &
!!OLD                           &    3(" ",A5)," /mode ",A12," /perc ",F10.1," /line ",A)'     &
!!OLD                           &  )                                                     & 
!!OLD                           &        listBe(jj,ii)%name,                             &
!!OLD                           &        listBe(jj,ii)%nPart,                            &
!!OLD                           &        listBe(jj,ii)%resolution,                       &
!!OLD                           &        listBe(jj,ii)%bandwidth,                        &
!!OLD                           &        listBe(jj,ii)%fShift,                           &
!!OLD                           &        listBe(jj,ii)%receiverName,                     &
!!OLD                           &        listBe(jj,ii)%polarization,                     &
!!OLD                           &        listBe(jj,ii)%subband,                          &
!!OLD                           &        listBe(jj,ii)%mode,                             &
!!OLD                           &        listBe(jj,ii)%percentage,                       &
!!OLD                           &        listBe(jj,ii)%lineName                          !
!!OLD                   Else 
!!OLD
!!OLD                      Write   (messageText,                                         &
!!OLD                           &    '(A10,I3,F10.3,F10.1,F10.3,                         &
!!OLD                           &    3(" ",A5)," /perc ",F10.1," /line ",A)'             &
!!OLD                           &  )                                                     & 
!!OLD                           &        listBe(jj,ii)%name,                             &
!!OLD                           &        listBe(jj,ii)%nPart,                            &
!!OLD                           &        listBe(jj,ii)%resolution,                       &
!!OLD                           &        listBe(jj,ii)%bandwidth,                        &
!!OLD                           &        listBe(jj,ii)%fShift,                           &
!!OLD                           &        listBe(jj,ii)%receiverName,                     &
!!OLD                           &        listBe(jj,ii)%polarization,                     &
!!OLD                           &        listBe(jj,ii)%subband,                          &
!!OLD                           &        listBe(jj,ii)%percentage,                       &
!!OLD                           &        listBe(jj,ii)%lineName                          !
!!OLD                !D                   Write (6,*) "      messageText: -->", messageText, "<--"
!!OLD
!!OLD             Else If (listBe(jj,ii)%wasConnected) Then
!!OLD                !!   countBE = countBE+1 
!!OLD                Write   (messageText,                                         &
!!OLD                     &    '(A10,I3,F10.3,F10.1,F10.3,                         &
!!OLD                     &  A,3(" ",A5)," ",A12,F10.1," ",A)'                     &
!!OLD                     &  )                                                     & 
!!OLD                     &        listBe(jj,ii)%name,                             &
!!OLD                     &        listBe(jj,ii)%nPart,                            &
!!OLD                     &        listBe(jj,ii)%resolution,                       &
!!OLD                     &        listBe(jj,ii)%bandwidth,                        &
!!OLD                     &        listBe(jj,ii)%fShift,                           &
!!OLD                     &        " DISCONNECTED FROM ",                          &
!!OLD                     &        listBe(jj,ii)%receiverName,                     &
!!OLD                     &        listBe(jj,ii)%polarization,                     &
!!OLD                     &        listBe(jj,ii)%subband,                          &
!!OLD                     &        listBe(jj,ii)%mode,                             &
!!OLD                     &        listBe(jj,ii)%percentage,                       &
!!OLD                     &        listBe(jj,ii)%lineName                          !
!!OLD                Call pakoMessage(priorityI+1,severityI,command,messageText)
!!OLD             End If
!!OLD             Else
!!OLD                If (listBe(jj,ii)%isConnected) Then
!!OLD                   countBE = countBE+1 
!!OLD                   Write (messageText, '(A10,I,3F10.3,3(" ",A10),F10.1)' )          & 
!!OLD                        &        listBe(jj,ii)%name,                                &
!!OLD                        &        listBe(jj,ii)%nPart,                               &
!!OLD                        &        listBe(jj,ii)%resolution,                          &
!!OLD                        &        listBe(jj,ii)%bandwidth,                           &
!!OLD                        &        listBe(jj,ii)%fShift,                              &
!!OLD                        &        listBe(jj,ii)%receiverName,                        &
!!OLD                        &        listBe(jj,ii)%receiverName2,                       &
!!OLD                        &        listBe(jj,ii)%mode,                                &
!!OLD                        &        listBe(jj,ii)%percentage
!!OLD                   Call pakoMessage(priorityI,severityI,command,messageText)
!!OLD                Else If (listBe(jj,ii)%wasConnected) Then
!!OLD                   !!   countBE = countBE+1 
!!OLD                   Write (messageText, '(A10,I,3F10.3,4A10,F10.1)' )                & 
!!OLD                        &        listBe(jj,ii)%name,                                &
!!OLD                        &        listBe(jj,ii)%nPart,                               &
!!OLD                        &        listBe(jj,ii)%resolution,                          &
!!OLD                        &        listBe(jj,ii)%bandwidth,                           &
!!OLD                        &        listBe(jj,ii)%fShift,                              &
!!OLD                        &        " DISCONNECTED FROM ",                             &
!!OLD                        &        listBe(jj,ii)%receiverName,                        &
!!OLD                        &        listBe(jj,ii)%receiverName2,                       &
!!OLD                        &        listBe(jj,ii)%mode,                                &
!!OLD                        &        listBe(jj,ii)%percentage
!!OLD                   Call pakoMessage(priorityI,severityI,command,messageText)
!!OLD                End If
!!OLD             End If
             !




!!$!!!
!!$  Subroutine checkFB100kHz (listRX, listFB100kHz,                                &   !!   OBSOLETE
!!$       &                 error,  errorCode, percentUsed)
!!$    !
!!$    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
!!$         &               listRX            !! list of all Receivers
!!$    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
!!$         &               listFB100kHz      !! list of all FB100kHz parts    
!!$    !
!!$    Logical,                Intent(OUT)            ::  error
!!$    Character(len=*),       Intent(OUT), Optional  ::  errorCode
!!$    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
!!$    !
!!$    Integer :: lBoundRX, uBoundRX
!!$    Integer :: lBoundBE, uBoundBE
!!$    Integer :: ii, jj, ll, l
!!$    !
!!$    Integer :: countParts
!!$    !
!!$    ! * limits on fShift:
!!$    Real(Kind=kindSingle),  Parameter  ::  fMinN128 = -250.0  ! receiver Narrow
!!$    Real(Kind=kindSingle),  Parameter  ::  fMaxN128 =  250.0  ! 12.6 band
!!$    Real(Kind=kindSingle),  Parameter  ::  fMinN253 = -245.0  ! 25.3 band
!!$    Real(Kind=kindSingle),  Parameter  ::  fMaxN253 =  245.0  ! 25.3 band
!!$    Real(Kind=kindSingle),  Parameter  ::  fMinW128 = -506.0  ! receiver Wide
!!$    Real(Kind=kindSingle),  Parameter  ::  fMaxW128 =   -6.0  ! 12.6 band
!!$    Real(Kind=kindSingle),  Parameter  ::  fMinW253 = -501.0  ! 25.3 band
!!$    Real(Kind=kindSingle),  Parameter  ::  fMaxW253 =  -11.0  ! 25.3 band
!!$    Real(Kind=kindSingle)              ::  fMin, fMax
!!$    !
!!$    Character(len=20)  ::  limitCode
!!$    !
!!$    !D    Write (6,*) "   --> check100kHz "
!!$    !
!!$    error        = .False.
!!$    percentUsed  = 0.0
!!$    !
!!$    countParts   = 0
!!$    !
!!$    If (Len(errorCode).Ge.2) Then
!!$       errorCode = ""
!!$    End If
!!$    !
!!$    lBoundRX = Lbound(listRX,1)
!!$    uBoundRX = Ubound(listRX,1)
!!$    !
!!$    lBoundBE = Lbound(listFB100kHz,1)
!!$    uBoundBE = Ubound(listFB100kHz,1)
!!$    !
!!$!!OLD    jj = 0
!!$!!OLD    Do ii = lBoundRX,uBoundRX,1
!!$!!OLD       If (listRX(ii)%isConnected) Then
!!$!!OLD          !
!!$!!OLD       End If
!!$!!OLD    End Do
!!$    !
!!$    jj = 0
!!$    Do ii = lBoundBE,uBoundBE,1
!!$       If (listFB100kHz(ii)%isConnected) Then
!!$          error = .True.
!!$          l = Len_trim(errorCode)
!!$          If (Len(errorCode).Ge.l+40) Then
!!$             errorCode = errorCode(1:l)//" 100 kHz Filterbank decommissioned 2007-09-12"
!!$          End If
!!$          !
!!$          jj = jj+1
!!$          countParts = countParts+1
!!$          If      (Abs(listFB100kHz(ii)%bandWidth-25.6) .Lt. 0.001) Then
!!$             percentUsed = percentUsed +100.0
!!$          Else If (Abs(listFB100kHz(ii)%bandWidth-25.3) .Lt. 0.001) Then
!!$             percentUsed = percentUsed +100.0
!!$          Else If (Abs(listFB100kHz(ii)%bandWidth-12.8) .Lt. 0.001) Then
!!$             percentUsed = percentUsed  +50.0
!!$          Else
!!$             error = .True.
!!$             l = Len_trim(errorCode)
!!$             If (Len(errorCode).Ge.l+30) Then
!!$                errorCode = errorCode(1:l)//" wrong bandwidth"
!!$             End If
!!$          End If
!!$          !
!!$          If (.Not. error) Then
!!$             Do ll = lBoundRX, uBoundRX, 1
!!$                If (listRX(ll)%isConnected .And.                                 &
!!$                     & listFB100kHz(ii)%receiverName.Eq.listRX(ll)%name) Then
!!$                   !
!!$                   If (listRX(ll)%width.Eq.width%narrow) Then
!!$                      If (      listFB100kHz(ii)%bandWidth.Lt.13.0 ) Then
!!$                         fMin = fMinN128
!!$                         fMax = fMaxN128
!!$                      Else
!!$                         fMin = fMinN253
!!$                         fMax = fMaxN253
!!$                      End If
!!$                   End If
!!$                   !
!!$                   If (listRX(ll)%width.Eq.width%wide) Then
!!$                      error = .True.
!!$                      l = Len_trim(errorCode)
!!$                      If (Len(errorCode).Ge.l+40) Then
!!$                         errorCode = errorCode(1:l)//" doesn't work with RECEIVER /WIDTH wide."
!!$                      End If
!!$                      If (      listFB100kHz(ii)%bandWidth.Lt.13.0 ) Then
!!$                         fMin = fMinW128
!!$                         fMax = fMaxW128
!!$                      Else
!!$                         fMin = fMinW253
!!$                         fMax = fMaxW253
!!$                      End If
!!$                   End If
!!$                   !
!!$                   If (   listFB100kHz(ii)%fShift.Lt.fMin .Or.                   &
!!$                        & listFB100kHz(ii)%fShift.Gt.fMax) Then
!!$                      error = .True.
!!$                      Write (limitCode,'(2F6.1)') fMin, fMax
!!$                      l = Len_trim(errorCode)
!!$                      If (Len(errorCode).Ge.l+55) Then
!!$                         errorCode = errorCode(1:l)//                            &
!!$                              &   " Frequency shift outside limits."//           &
!!$                              &   limitCode
!!$                      End If
!!$                   End If
!!$                   !
!!$                End If
!!$             End Do      !!  receivers
!!$          End If      !! not error
!!$       End If      !!  backend conected
!!$    End Do      !!  backends
!!$    !
!!$    If (countParts .Gt. 2) Then
!!$       error = .True.
!!$       l = Len_trim(errorCode)
!!$       If (Len(errorCode).Ge.l+30) Then
!!$          errorCode = errorCode(1:l)//" too many parts"
!!$       End If
!!$    End If
!!$    !
!!$    If (percentUsed .Gt. 100.) Then
!!$       error = .True.
!!$       l = Len_trim(errorCode)
!!$       If (Len(errorCode).Ge.l+30) Then
!!$          errorCode = errorCode(1:l)//" too many channels"
!!$       End If
!!$    End If
!!$    !
!!$    If (.Not. ERROR) Then
!!$       If (Len(errorCode).Ge.2) Then
!!$          errorCode = "OK"
!!$       End If
!!$    End If
!!$    !
!!$    Return
!!$    !
!!$  End Subroutine checkFB100kHz
!!$!!!
!!$!!!
!!$  Subroutine checkFB1Mhz (listRX, listFB1MHz,                                    &   !!   OBSOLETE
!!$       &                 error,  errorCode, percentUsed)
!!$    !
!!$    Type (varReceiver),        Dimension(:), Intent(IN)  ::                      &
!!$         &               listRX          !! list of all Receivers
!!$    Type (varBackend),         Dimension(:), Intent(IN)  ::                      &
!!$         &               listFB1MHz      !! list of all FB1MHz parts    
!!$    !
!!$    Logical,                Intent(OUT)            ::  error
!!$    Character(len=*),       Intent(OUT), Optional  ::  errorCode
!!$    Real(Kind=kindSingle),  Intent(OUT), Optional  ::  percentUsed
!!$    !
!!$    Integer :: lBoundRX, uBoundRX
!!$    Integer :: lBoundBE, uBoundBE
!!$    Integer :: ii, jj, iSB, l
!!$    !
!!$    Integer :: countParts
!!$    !
!!$    Logical :: receiverIsEMIR
!!$    !
!!$    Real    :: fShiftMin, fShiftMax
!!$    Character(len=lenVar)  :: limitCode
!!$    !
!!$    !D    Write (6,*) "   --> checkFB1Mhz "
!!$    !
!!$    error        = .False.
!!$    percentUsed  = 0.0
!!$    !
!!$    countParts   = 0
!!$    !
!!$    Call queryReceiver("EMIR", receiverIsEMIR)
!!$    !
!!$    If (Len(errorCode).Ge.2) Then
!!$       errorCode = ""
!!$    End If
!!$    !
!!$    lBoundRX = Lbound(listRX,1)
!!$    uBoundRX = Ubound(listRX,1)
!!$    !
!!$    lBoundBE = Lbound(listFB1MHz,1)
!!$    uBoundBE = Ubound(listFB1MHz,1)
!!$    !
!!$    jj = 0
!!$    Do ii = lBoundBE,uBoundBE,1
!!$       !
!!$       If (listFB1MHz(ii)%isConnected) Then
!!$          jj = jj+1
!!$          countParts = countParts+1
!!$          percentUsed = percentUsed+100.0*listFB1MHz(ii)%bandwidth/1024.0
!!$          If (.Not. ( Abs(listFB1MHz(ii)%bandWidth-256.0)  .Lt. 0.001            &
!!$               & .Or. Abs(listFB1MHz(ii)%bandWidth-512.0)  .Lt. 0.001            &
!!$               & .Or. Abs(listFB1MHz(ii)%bandWidth-768.0)  .Lt. 0.001            &
!!$               & .Or. Abs(listFB1MHz(ii)%bandWidth-1024.0) .Lt. 0.001            &
!!$               &    ) ) Then
!!$             error = .True.
!!$             l = Len_trim(errorCode)
!!$             If (Len(errorCode).Ge.l+30) Then
!!$                errorCode = errorCode(1:l)//" wrong bandwidth"
!!$             End If
!!$          End If
!!$          !
!!$          !  **  not possible on HERA !
!!$          !
!!$          If      (      listFB1MHz(ii)%receiverName .Eq. rec%HERA               &
!!$               & .Or.    listFB1MHz(ii)%receiverName .Eq. rec%HERA1              &
!!$               & .Or.    listFB1MHz(ii)%receiverName .Eq. rec%HERA2              &
!!$               &  )  Then
!!$             error = .True.
!!$             l = Len_trim(errorCode)
!!$             If (Len(errorCode).Ge.l+30) Then
!!$                errorCode = errorCode(1:l)                //                     &
!!$                     &      " not possible on "           //                     &
!!$                     &      listFB1MHz(ii)%receiverName                          !
!!$             End If
!!$          End If
!!$          !          
!!$          !  **  EMIR
!!$          !
!!$          If (receiverIsEMIR) Then
!!$             !
!!$             If    (      listFB1MHz(ii)%receiverName .Eq. rec%E090) Then
!!$                If (      listFB1MHz(ii)%subband      .Eq. esb%lsb               &
!!$                   & .Or. listFB1MHz(ii)%subband      .Eq. esb%usb ) Then        !
!!$                   error = .True.
!!$                   l = Len_trim(errorCode)
!!$                   If (Len(errorCode).Ge.l+30) Then
!!$                      errorCode = errorCode(1:l)//                               &
!!$                           &      listFB1MHz(ii)%subband//" not possible"        !
!!$                   End If
!!$                End If
!!$             End If
!!$             !
!!$             !! **   EMIR: fShift depending on EMIR sub band
!!$             !! check that shifted band fits in range
!!$             !! this is essential for 1MHz with bandwidth /channels 512 and 768
!!$             !
!!$             If      (listFB1MHz(ii)%subBand .Eq. esb%lo) Then
!!$                iSB = iLO
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%li) Then
!!$                iSB = iLI
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%ui) Then
!!$                iSB = iUI
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%uo) Then
!!$                iSB = iUO
!!$             End If
!!$             !
!!$             fShiftMin = Min ( fRangeSky(iFB1MHz,iSB,iBandFrom),                 &
!!$                  &            fRangeSky(iFB1MHz,iSB,iBandTo)    )               &
!!$                  &    + 0.5 * listFB1MHz(ii)%bandWidth                          &
!!$                  &       -    fRangeSky(iVESPA,iSB,iCenter)                     !
!!$             !
!!$             fShiftMax = Max ( fRangeSky(iFB1MHz,iSB,iBandFrom),                 &
!!$                  &            fRangeSky(iFB1MHz,iSB,iBandTo)    )               &
!!$                  &    - 0.5 * listFB1MHz(ii)%bandWidth                          &
!!$                  &       -    fRangeSky(iVESPA,iSB,iCenter)                     !
!!$             !
!!$             !D              Write (6,*) "   iSB, fRangeSky(iFB1MHz,iSB,iBandFrom)   ",         &
!!$             !D                   &          iSB, fRangeSky(iFB1MHz,iSB,iBandFrom)              !
!!$             !D              Write (6,*) "   iSB, fRangeSky(iFB1MHz,iSB,iBandTo)     ",         &
!!$             !D                   &          iSB, fRangeSky(iFB1MHz,iSB,iBandTo)                !
!!$             !D 
!!$             !D              Write (6,*) "   fShiftMin:   ", fShiftMin
!!$             !D              Write (6,*) "   fShiftMax:   ", fShiftMax
!!$             !
!!$             If (        listFB1MHz(ii)%fShift.LT.fShiftMin                      &
!!$                  & .Or. listFB1MHz(ii)%fShift.GT.fShiftMax  )   Then            !
!!$                error = .True.
!!$                l = Len_trim(errorCode)
!!$                If (Len(errorCode).Ge.l+30) Then
!!$                   errorCode = errorCode(1:l)//" invalid fShift "
!!$                End If
!!$                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
!!$                     &            listFB1MHz(ii)%fShift, fShiftMin, fShiftMax    !
!!$                l  = Len_trim(errorCode)
!!$                l2 = Len_trim(limitCode)
!!$                If ( Len(errorCode) .Ge. (l+l2) ) Then
!!$                   errorCode = errorCode(1:l)//limitCode(1:l2)
!!$                End If
!!$             End If
!!$             !
!!$             !! **   EMIR: fShift depending on EMIR sub band
!!$             !! TBD: abtract the following checks on fShift into a Subroutine
!!$             !!      compare checkContinuum, checkWILMA, checkFB4MHz, checkFB1MHz
!!$             !
!!$             !D             Write (6,*) "   listFB1MHz(ii)%fShift:  ", listFB1MHz(ii)%fShift
!!$             !D             Write (6,*) "   listFB1MHz(ii)%subBand: ", listFB1MHz(ii)%subBand
!!$             !
!!$             If (listFB1MHz(ii)%subBand .Eq. esb%lo) Then
!!$                !
!!$                fShiftMin = Min ( fRangeSky(iFB1MHz,iLO,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iLO,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
!!$                !
!!$                fShiftMax = Max ( fRangeSky(iFB1MHz,iLO,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iLO,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iLO,iCenter)                  !
!!$                !
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%li) Then
!!$                !
!!$                fShiftMin = Min ( fRangeSky(iFB1MHz,iLI,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iLI,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
!!$                !
!!$                fShiftMax = Max ( fRangeSky(iFB1MHz,iLI,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iLI,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iLI,iCenter)                  !
!!$                !
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%ui) Then
!!$                !
!!$                fShiftMin = Min ( fRangeSky(iFB1MHz,iUI,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iUI,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
!!$                !
!!$                fShiftMax = Max ( fRangeSky(iFB1MHz,iUI,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iUI,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iUI,iCenter)                  !
!!$                !
!!$             Else If (listFB1MHz(ii)%subBand .Eq. esb%uo) Then
!!$                !
!!$                fShiftMin = Min ( fRangeSky(iFB1MHz,iUO,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iUO,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
!!$                !
!!$                fShiftMax = Max ( fRangeSky(iFB1MHz,iUO,iCenterFrom),            &
!!$                     &            fRangeSky(iFB1MHz,iUO,iCenterTo)    )          &
!!$                     &       -    fRangeSky(iVESPA,iUO,iCenter)                  !
!!$                !
!!$             End If
!!$             !
!!$             !D             Write (6,*) "   fShiftMin:   ", fShiftMin
!!$             !D             Write (6,*) "   fShiftMax:   ", fShiftMax
!!$             !
!!$             If (        listFB1MHz(ii)%fShift.LT.fShiftMin                      &
!!$                  & .Or. listFB1MHz(ii)%fShift.GT.fShiftMax  )   Then            !
!!$                error = .True.
!!$                l = Len_trim(errorCode)
!!$                If (Len(errorCode).Ge.l+30) Then
!!$                   errorCode = errorCode(1:l)//" invalid fShift "
!!$                End If
!!$                Write (limitCode,'(F8.1," out of range ",F8.1," to ",F8.1)')     &
!!$                     &            listFB1MHz(ii)%fShift, fShiftMin, fShiftMax    !
!!$                l  = Len_trim(errorCode)
!!$                l2 = Len_trim(limitCode)
!!$                If ( Len(errorCode) .Ge. (l+l2) ) Then
!!$                   errorCode = errorCode(1:l)//limitCode(1:l2)
!!$                End If
!!$             End If
!!$             !
!!$             !!
!!$          End If
!!$          !
!!$       End If
!!$       !
!!$    End Do
!!$    !
!!$!!OLD             If (Abs(listFB1MHz(ii)%bandWidth-768.0) .Lt. 0.001) Then
!!$!!OLD                If (Abs(listFB1MHz(ii)%fShift+128.0) .Gt. 0.001) Then
!!$!!OLD                   error = .True.
!!$!!OLD                   l = Len_trim(errorCode)
!!$!!OLD                   If (Len(errorCode).Ge.l+30) Then
!!$!!OLD                      errorCode = errorCode(1:l)//" fShift must be -128.0 "
!!$!!OLD                   End If
!!$!!OLD                End If
!!$!!OLD             End If
!!$!!OLD             !
!!$!!OLD             If (Abs(listFB1MHz(ii)%bandWidth-1024.0) .Lt. 0.001) Then
!!$!!OLD                If (Abs(listFB1MHz(ii)%fShift +256.0) .Gt. 0.001) Then
!!$!!OLD                   error = .True.
!!$!!OLD                   l = Len_trim(errorCode)
!!$!!OLD                   If (Len(errorCode).Ge.l+30) Then
!!$!!OLD                      errorCode = errorCode(1:l)//" fShift must be -256.0 "
!!$!!OLD                   End If
!!$!!OLD                End If
!!$!!OLD             End If
!!$             !
!!$             !
!!$    If (countParts .Gt. 4) Then
!!$       error = .True.
!!$       l = Len_trim(errorCode)
!!$       If (Len(errorCode).Ge.l+30) Then
!!$          errorCode = errorCode(1:l)//" too many parts"
!!$       End If
!!$    End If
!!$    !
!!$    If (percentUsed .Gt. 100.) Then
!!$       error = .True.
!!$       l = Len_trim(errorCode)
!!$       If (Len(errorCode).Ge.l+30) Then
!!$          errorCode = errorCode(1:l)//" too many channels"
!!$       End If
!!$    End If
!!$    !
!!$    If (.Not. ERROR) Then
!!$       If (Len(errorCode).Ge.2) Then
!!$          errorCode = "OK"
!!$       End If
!!$    End If
!!$    !
!!$    Return
!!$    !
!!$  End Subroutine checkFB1Mhz
