!
!     Id: pako-package.f90,v 1.2.4 2013-12-01 Hans Ungerechts
!     Id: pako-package.f90,v 1.2.4 2013-04-19 Hans Ungerechts
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the PAKO package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine pako_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !
  type(gpack_info_t), intent(out) :: pack
  !
  External :: pako_pack_init
  External :: pako_pack_clean
  !
  pack%name='pako'
  pack%ext='.pako'
  pack%depend(1:2) = (/ locwrd(sic_pack_set), locwrd(greg_pack_set) /) ! List here dependencies
  pack%init=locwrd(pako_pack_init)
  pack%clean=locwrd(pako_pack_clean)
  pack%authors="Hans Ungerechts"
  !
End Subroutine pako_pack_set
!
Subroutine pako_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! 
  !----------------------------------------------------------------------
  Integer :: gpack_id
  Logical :: error
  ! Global
  Integer :: sic_setlog
  ! Local
  Integer :: ier
  !
  Call pako_message_set_id(gpack_id)
  !
  ! Specific variables
  ier = sic_setlog('gag_help_pako','gag_doc:hlp/pako-help-pako.hlp')
  If (ier.Eq.0) Then
    error=.True.
    Return
  Endif
  !
  ! Load local language
  Call pako_load
  !
  ! Specific initializations
  !
End Subroutine pako_pack_init
!
Subroutine pako_pack_clean(error)
  !----------------------------------------------------------------------
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  Logical :: error
  !
  Character*80 shellCommand
  Integer :: SYSTEM
  Integer :: ier2
  !
  !D   Write (6,*) " --> pako_pack_clean"
  !
  shellCommand = "rm -f .pakoLock"
  ier2 = system(shellCommand)                                                    !  !!  TBD: handle error
  !
End Subroutine pako_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
