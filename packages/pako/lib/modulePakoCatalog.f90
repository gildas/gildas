!
!     Id: modulePakoCatalog.f90,v 1.2.4 2015-05-14 Hans Ungerechts
!     Id: modulePakoCatalog.f90,v 1.2.3 2014-02-11 Hans Ungerechts
!         modulePakoCatalog.f90,v 1.2   2012-02-01 Hans Ungerechts
!     
!----------------------------------------------------------------------
!     PAKO module for CATALOG
!
Module modulePakoCatalog
  !
  Use modulePakoMessages
  Use modulePakoDisplayText
  Use modulePakoGlobalParameters
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  !
  Use gbl_message
  Use gkernel_interfaces
  !
  Implicit None
  Save
  Private
  !
  Public :: catalog, saveCatalog, displayCatalog
!!!
Contains
!!!
!!!
  Subroutine CATALOG (LINE,ERROR)
    !
    !!  TBD 1.2: subroutine parameters like other commands /modules
    !
    Character(len=*),  Intent(in)   :: line
    Logical,           Intent(out)  :: error
    !
    Character(len=lenCO)   ::  Type, ext, extEdb
    !
    Character(len=lenVar)  ::  name, file, fileEdb, pathEdb
    !
    Character(len=lenCh)   ::  command, errorCode, cPar, cPar1
    Character(len=lenCh)   ::  sourceName, systemName, raString, decString
    !
    Character(len=lenLine) ::  catalogLine, content, comment, messageText, shellCommand
    !
    Integer                ::  lfile, ier, nc, nt
    Integer                ::  indexComment, ioerr, ioStatus, iUnitR, iUnitW 
    Integer                ::  ll, llPath, lPar, nPar, nPar1, nParL
    !
    Integer                ::  iMatch, nMatch
    !
    Character(len=lenLine) ::  lonString,    latString
    Real(kind=kindSingle)  ::  epoch,        raOff, decOff
    Real(kind=kindDouble)  ::  lambda, beta, ra, dec
    !
    Logical                ::  errorM, errorG
    !
    !**   functions called:   ***
    Integer  ::  System
    !
    Call pako_message(seve%t,"pako",                                             &
         &         " module Catalog ,v 1.2.4 2015-05-14 ---> SR: Catalog ")      ! this allows to trace execution
    !
    !D    Write (6,*) "   ---> modulePakoCatalog: CATALOG "
    !D    Write (6,*) "        Line : "
    !D    Write (6,*) "->",line(1:len_trim(line)),"<-"
    !
    !D    Write (6,*) "     GVsourceCatalog(1): ", GVsourceCatalog(1)
    !D    Write (6,*) "     GVlineCatalog(1):   ", GVlineCatalog(1)
    !
    command = "CATALOG"
    iUnitR  =  ioUnit%Read
    iUnitW  =  ioUnit%Write
    extEdb  =  GPnone
    !
    !  ** catalog type and name
    Call SIC_CH (LINE,0,1,Type,NT,.True.,ERROR)
    If (ERROR) Then
       Call PakoMessage(priorityE,severityE,'CATALOG',                           &
            &   'no valid catalog type')
       Return
    End If
    Call SIC_CH (LINE,0,2,NAME,NC,.True.,ERROR)
    If (ERROR) Then
       Call PakoMessage(priorityE,severityE,'CATALOG',                           &
            &   'no valid catalog name')
       Return
    End If
    !
    Call sic_lower(Type)
    If (Type(1:1).Eq.'s') Then
       EXT = '.sou'
    Elseif (Type(1:1).Eq.'l') Then
       EXT = '.lin'
    Else
       Call PakoMessage(priorityE,severityE,'CATALOG',                           &
            &   'type must be SOURCE or LINE')
       ERROR = .True.
       Return
    Endif
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "   --> Call SIC_PARSEF(NAME,FILE,' ',EXT) "
    End If
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "         NAME:    ->", NAME(1:len_trim(NAME)) ,"<-"
       Write (6,*) "         FILE:    ->", FILE(1:len_trim(FILE)) ,"<-"
       Write (6,*) "         EXT:     ->", EXT(1:len_trim(EXT))  ,"<-"
       Write (6,*) "         fileEdb: ->", fileEdb(1:len_trim(fileEdb)) ,"<-"
       Write (6,*) "         pathEdb: ->", pathEdb(1:len_trim(pathEdb)) ,"<-"
    End If
    !
    Call SIC_PARSEF(NAME,FILE,' ',EXT)
    ll = Len_trim(file)-4
    fileEdb = NAME(1:len_trim(NAME))//".edb"
    pathEdb = file(1:ll)//".edb"
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "         NAME:    ->", NAME(1:len_trim(NAME)) ,"<-"
       Write (6,*) "         FILE:    ->", FILE(1:len_trim(FILE)) ,"<-"
       Write (6,*) "         EXT:     ->", EXT(1:len_trim(EXT))  ,"<-"
       Write (6,*) "         fileEdb: ->", fileEdb(1:len_trim(fileEdb)) ,"<-"
       Write (6,*) "         pathEdb: ->", pathEdb(1:len_trim(pathEdb)) ,"<-"
    End If
    !
    !  ** open catalog file
    lfile = Len_trim(file)
    Open(unit=iUnitR, action='read', file=file(1:lfile), status='old',           &
         &              iostat=ier)
    !D    Write (6,*) "      ier: ", ier
    If (ier.Ne.0) Then
       Call PakoMessage(priorityE,severityE,'CATALOG',                           &
            &   'can not open catalog file '//file(1:lfile))
       ERROR = .True.
       Return
    End If
    !
    If (Type(1:1).Eq.'s') Then
       !
       ! ***  check source catalog lines and generate XEphem .edb file
       !
       !  **  open XEphem .edb file
       Open (unit=iUnitW,file=pathEdb,iostat=ier,recl=512,status='unknown')
       lfile = Len_trim(pathEdb)
       If (ier.Ne.0) Then
          Call PakoMessage(priorityE,severityE,'CATALOG ',                       &
               &   'can not open XEphem file '//pathEdb(1:lfile))
          ERROR = .True.
          Return
       End If
       !
       ioStatus = 0
       Read (iUnitR,'(a)',iostat=ioStatus) catalogLine
       !
       Do While (ioStatus.Eq.0)
          !
          ll            = Len_trim(catalogLine)
          !
          If (GV%doDebugMessages) Then
             Write (6,*) "      ll = Len_trim(catalogLine)       : ", ll
             Write (6,*) "                    catalogLine(1:ll)  : "
             Write (6,*) ">>",catalogLine(1:ll),"<<"
          End If
          !
          If (ll.Ge.1) Then
             !
             !  ** split comment and content
             indexComment  = Index(catalogLine,'!')
             If (indexComment.Ge.1) Then
                content = catalogLine(1:indexComment-1)
                comment = catalogLine(indexComment:ll)
                ll      = indexComment-1
             Else
                content = catalogLine
             End If
             !
             If (ll.Ge.1) Then
                !
                Call SIC_BLANC(content,ll)
                !D             Write (6,*) "-->", content(1:ll),"<--"
                !
                nPar =  1
                lPar =  1
                Call SIC_NEXT(content(1:ll),cPar,lPar,nPar)
                !D             Write (6,*) "  cPar: ->",cPar(1:lPar),"<-" 
                !D             Write (6,*) "  lPar: ->",lPar,"<-" 
                !D             Write (6,*) "  nPar: ->",nPar,"<-" 
                sourceName = cPar
                !
                nParL = nPar       !   to keep nPar
                lPar  =  1
                Call SIC_NEXT(content(nPar:ll),cPar,lPar,nPar) 
                !D             Write (6,*) "  cPar: ->",cPar(1:lPar),"<-" 
                !D             Write (6,*) "  lPar: ->",lPar,"<-" 
                !D             Write (6,*) "  nPar: ->",nPar,"<-" 
                cPar1 = cPar
                nPar1 = nPar
                Call pakoUmatchKey (                                                &
                     &              keys=systemChoices,                             &
                     &              key=cpar1,                                      &
                     &              command=command,                                &
                     &              howto='start upper quiet',                      &
                     &              iMatch=iMatch,                                  & 
                     &              nMatch=nMatch,                                  &
                     &              error=errorM,                                   &
                     &              errorCode=errorCode                             &
                     &             )
                !
                If (.Not.errorM) Then
                   systemName = systemChoices(iMatch)
                Else
                   systemName        = sys%equatorial
                   epoch             = 1950.0
                   nPar              = nParL           !   back to previous nPar
                End If
                !
                If (.Not.errorM .And. systemName .Eq. sys%equatorial) Then
                   lPar = 1
                   Call SIC_NEXT(content(nPar:ll),cPar,lPar,nPar)
                   Call Sic_Upper(cPar)
                   If (cPar(1:1).EQ."J" .Or. cPar(1:1).EQ."B") Then
                      cPar = cPar(2:len_trim(cPar))
                   End If
                   Read(cPar(1:lPar),*,iostat=ioerr) epoch
                   If (ioerr.Ne.0) Then
                      Call PakoMessage(priorityE,severityE,'CATALOG SOURCE',        &
                           &   'error decoding epoch ')
                      Call PakoMessage(priorityE,severityE,'CATALOG SOURCE',        &
                           &   'line: '//catalogLine(1:Len_trim(catalogLine)))
                      ERROR = .True.
                      Return
                   End If
                End If
                !
                lPar =  1
                Call SIC_NEXT(content(nPar:ll),cPar,lPar,nPar) 
                raString = cPar(1:lPar)
                !
                lPar =  1
                Call SIC_NEXT(content(nPar:ll),cPar,lPar,nPar) 
                decString = cPar(1:lPar)
                !
                !D             Write (6,*) "  sourceName: ->",sourceName,"<-"
                !D             Write (6,*) "  systemName: ->",systemName,"<-"
                !D             Write (6,*) "  epoch:        ",epoch
                !D             Write (6,*) "  raString:   ->",raString,"<-"
                !D             Write (6,*) "  decString:  ->",decString,"<-"
                !
             End If
             !
             If (indexComment.Ne.1 .And. systemName .Eq. sys%equatorial) Then
                !   *  write standard line /entry to .edb
                Write (iUnitW,'(10A,F10.3)')                                        &
                     &             sourceName(1:Len_trim(sourceName))," ,",         &
                     &             "f|J"                           ," ,",           &
                     &             raString(1:Len_trim(raString))  ," ,",           &
                     &             decString(1:Len_trim(decString))," ,",           &
                     &             "-2.0"                          ," ,",           &
                     &             epoch
                !
             Else If (indexComment.Ne.1 .And. systemName .Eq. sys%galactic) Then
                !   *  write standard line /entry to .edb
                !
                errorG = .False.
                !
                lonString = raString
                latString  = decstring
                !
                Call CVDEGlocal(lonString,len_trim(lonString),'D',LAMBDA,errorG)
                Call CVDEGlocal(latString,len_trim(latString),'D',BETA,errorG)
                !
                !! lambda = lambda*deg
                !! beta   = beta*deg
                !
                If (GV%doDebugMessages) Then
                   Write (6,*) "      --> GALACTIC"
                   Write (6,*) "          lonString:  ->"      , lonString
                   Write (6,*) "          latString:  ->"      , latString
                   Write (6,*) "          lambda [rad]        ", lambda
                   Write (6,*) "          beta   [rad]        ", beta
                   Write (6,*) "          lambda [deg]        ", lambda/deg
                   Write (6,*) "          beta   [deg]        ", beta/deg
                End If
                !
                !!  Call gal_eq1950 (lambda, beta, ra, dec, 1)
                !
                Call gal_to_equ (lambda, beta, 0.0, 0.0,                         &
                     &           ra, dec, raOff, decOff, 2000.0,errorG)
                !
                If (GV%doDebugMessages) Then
                   Write (6,*) "          ra     [rad]        ", ra
                   Write (6,*) "          dec    [rad]        ", dec
                   Write (6,*) "          ra     [h]          ", ra/deg/15.0d0
                   Write (6,*) "          dec    [deg]        ", dec/deg
                End If
                !
                !!  Call sexag (  rastring , ra,   24)
                !!  Call sexag ( decstring , dec, 360)
                !
                Call sexrad (  rastring , ra,   24)
                Call sexrad ( decstring , dec, 360)
                !
                If (GV%doDebugMessages) Then
                   Write (6,*) "          ra     ", rastring
                   Write (6,*) "          dec    ", decstring
                End If
                !
                !!  Write (iUnitW,*)                                                 &
                Write (iUnitW,'(                                                 &
                     &             A,  A3,                                       &
                     &             A,  A2,                                       &
                     &             A,  A2,                                       &
                     &             A,  A2,                                       &
                     &             A,  A2,                                       &
                     &             F10.3                                         &
                     &          )' )                                             &
                     &             sourceName(1:Len_trim(sourceName))," ,",      &
                     &             "f|J"                           ," , ",       &
                     &             raString(1:Len_trim(raString))  ,", ",        &
                     &             decString(1:Len_trim(decString)),", ",        &
                     &             "-2.0"                          ,", ",        &
                     &             2000.000                                      !
                !
             Else If (indexComment.Ne.1) Then
                !   *  write unsupported catalog line as comment to .edb
                Write (iUnitW,'(2A)')                                            &
                     &             "! ",                                         &
                     &             catalogLine(1:len_trim(catalogLine))
             End If
             !
             If (indexComment.Ge.1) Then
                Write (iUnitW,'(A)') comment(1:Len_trim(comment))
             End If
             !
          Else
             !   *  blank line as comment to .edb
             Write (iUnitW,'(A)')                                               &
                  &             "! "
          End If
          !
          Read (iUnitR,'(a)',iostat=ioStatus) catalogLine
          !
       End Do
       !
       Close (unit=iUnitR)
       Close (unit=iUnitW)
       !
       ll = Len_trim(fileEdb)
       !
       !!   mv existing .edb file to .edb~
       !
       shellCommand = 'mv -f '//                                                 &
            &        '~/.xephem/'//fileEdb(1:ll)//                               &
            &        ' '//                                                       &
            &        '~/.xephem/'//fileEdb(1:ll)//'~ '                           !
       !
       If (GV%doDebugMessages) Then
          Write (6,*) " shellCommand: -->", shellCommand, "<--"
       End If
       !
       ier = system(shellCommand)
       !
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &        'moved XEphem file: "',                                  &
               &        '~/.xephem/'//fileEdb(1:ll),                             &
               &        '" to "',                                                &
               &        '~/.xephem/'//fileEdb(1:ll)//'~ "'                       ! 
          Call pakoMessage(priorityI,severityI,command,messageText)
       Else
          Write (messageText,*)                                                  &
               &        'could not move XEphem file: "',                         &
               &        '~/.xephem/'//fileEdb(1:ll)//                            &
               &        '" to "',                                                &
               &        '~/.xephem/'//fileEdb(1:ll)//'~ "'                       ! 
          Call pakoMessage(priorityW,severityW,command,messageText)
       End If
       !
       !!   cp new .edb file into ~/.xephem/
       !
       llPath = Len_trim(pathEdb)
       !
       shellCommand = 'cp -f '//                                                 & 
            &        pathEdb(1:llPath)//                                         &
            &        ' '//                                                       &
            &        '~/.xephem/'//fileEdb(1:ll)//' '                            !
       !
       If (GV%doDebugMessages) Then
          Write (6,*) " shellCommand: -->", shellCommand, "<--"
       End If
       !
       ier = system(shellCommand)
       !
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &           'copied XEphem file: "',                              &
               &           pathEdb(1:llPath),                                    &
               &           '" to "~/.xephem/"'                                   !
          Call pakoMessage(priorityI,severityI,command,messageText)
       Else
          Write (messageText,*)                                                  &
               &           'could not copy XEphem file: "',                      &
               &           pathEdb(1:ll),                                        &
               &           '" to "~/.xephem/"'                                   !
          Call pakoMessage(priorityE,severityE,command,messageText)
       End If
       !
    End If
    !
    If (Type(1:1).Eq.'s') Then
       GVsourceCatalog(1) = file(1:lfile)
       Call PakoMessage(priorityI,severityI,                                     &
            &   'CATALOG SOURCE ',file(1:lfile)//' selected')                    !
       Call PakoMessage(priorityI,severityI,                                     &
            &   'CATALOG SOURCE ',pathEdb(1:llPath)//' XEphem file generated')   !
    Else If (Type(1:1).Eq.'l') Then
       GVlineCatalog(1)  = file(1:lfile)
       Call PakoMessage(priorityI,severityI,                                     &
            &   'CATALOG LINE ',file(1:lfile)//' selected')                      !
        close(unit=iUnitR)
    Endif
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "     GVsourceCatalog(1): ", GVsourceCatalog(1)
       Write (6,*) "     GVlineCatalog(1):   ", GVlineCatalog(1)
    End If
    !
    Call displayCatalog
    !
  End Subroutine CATALOG
!!!
!!!
  Subroutine saveCatalog(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    !
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ERROR = .False.
    !
    contC = contNN
    !
    B = '\'   ! '
    S = ' '
    CMD =  programName//B//"CATALOG"
    lCMD = lenc(CMD)
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    Write (iUnit,*) "! "
    !
    Include 'inc/parameters/saveCatalogSource.inc'
    !
    Include 'inc/parameters/saveCatalogLine.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
    !
  End Subroutine saveCatalog
!!!
!!!
  Subroutine displayCatalog
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplayCatalog.inc'
    !
  End Subroutine displayCatalog
!!!
!!!
!!!  private copies of (gildas) library routines
!!!                             needed for debugging
!!!
  Subroutine DECDEGlocal(CHAIN,LCHAIN,Value,NDIV,ERROR)
    !----------------------------------------------------------------------
    ! ASTRO	Internal routine
    !	Decode a sexagesimal character string into angle value in
    !	DEGREES. Formats can be
    !	61.5000000	or	161
    !	61:30.0000	or	161:40
    !	61:45:50.9	or	161:40:50
    ! Arguments :
    !	CHAIN	C*(*)	Character string		Input
    !	LCHAIN	I	length of			Input
    !	VALUE	R*8	Value of angle(radians)		Output
    !	NDIV	I	360/24				Input
    !	ERROR	L	Error flag
    ! No subroutines
    ! No common
    !----------------------------------------------------------------------
    Character(len=*)      :: CHAIN
    Real(kind=kindDouble) :: Value,CONV,DEGREE,MINUTE,SECOND
    Integer NDIV,NDOT,NLEN,NMIN,NSEC, LENC, LCHAIN
    Logical MINUS, ERROR
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "      --> DECDEGlocal "
       Write (6,*) "         LCHAIN:   ", LCHAIN
       Write (6,*) "          CHAIN: ->", CHAIN(1:LCHAIN), "<-"
       Write (6,*) "           NDIV    ", NDIV
       Write (6,*) "          ERROR:   ", ERROR
    End If
    !
    ERROR = .False.                                                              !  !!  fixed this 2015-05-..
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "          reset    "
       Write (6,*) "          ERROR:   ", ERROR
    End If
    !
    NLEN = LENC(CHAIN(1:LCHAIN))
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "           NLEN    ", NLEN
    End If
    !
    ! Zeroth find minus sign if any
    NDOT = Index(CHAIN(1:NLEN),'-')
    MINUS = NDOT.Ne.0
    !
    ! First find if any dot in Chain
    NDOT = Index(CHAIN(1:NLEN),'.')
    If (NDOT.Eq.0) Then
       NDOT=NLEN+1
    Endif
    !
    ! Set CONV factor
    CONV = 360.D0/NDIV
    !
    ! Find if any minute Field
    NMIN = Index(CHAIN(1:NLEN),':')
    If (NMIN.Eq.0) Then
       ! No Minutes Field
       Read(CHAIN(1:NLEN),100,ERR=99) DEGREE
       Value = CONV*DEGREE
       If (GV%doDebugMessages) Then
          Write (6,*) "          DEGREE: ", DEGREE
          Write (6,*) "          CONV:   ", CONV
          Write (6,*) "          Value:  ", Value
          Write (6,*) "          ERROR:   ", ERROR
          Write (6,*) "      <-- DECDEGlocal "
       End If
       Return
    Endif
    !
    ! Find if any Second field
    NSEC = Index(CHAIN(NMIN+1:NLEN),':')
    If (NSEC.Eq.0) Then
       ! No Seconds Field
       If (NDOT.Ne.NMIN+3) Then
          ! Minutes Field not two characters
          Goto 99
       Endif
       Read(CHAIN(NMIN+1:NLEN),100,ERR=99) MINUTE
       If (MINUTE.Ge.60.D0) Then
          write(6,*) 'E-ANGLE, more than 60 minutes...'
          ERROR = .True.
          Write (6,*) "          ERROR:   ", ERROR
          Write (6,*) "      <-- DECDEGlocal "
          Return
       Endif
       Read(CHAIN(1:NMIN-1),100,ERR=99) DEGREE
       If (.Not.MINUS) Then
          Value = CONV*(DEGREE+MINUTE/60.D0)
       Else
          Value = CONV*(DEGREE-MINUTE/60.D0)
       Endif
    Elseif (NSEC.Ne.3) Then
       ! Minutes Field not two characters
       Goto 99
    Else
       ! Seconds Field is present
       If (NDOT.Ne.NMIN+6) Then
          ! Seconds Field not two characters
          Goto 99
       Endif
       Read(CHAIN(NMIN+4:NLEN),100,ERR=99) SECOND
       If (SECOND.Ge.60.D0) Then
          write(6,*) 'E-ANGLE,  more than 60 seconds...'
          ERROR = .True.
          Write (6,*) "          ERROR:   ", ERROR
          Write (6,*) "      <-- DECDEGlocal "
          Return
       Endif
       Read(CHAIN(NMIN+1:NMIN+2),100,ERR=99) MINUTE
       If (MINUTE.Ge.60.D0) Then
          write(6,*) 'E-ANGLE,  more than 60 minutes...'
          ERROR = .True.
          Write (6,*) "          ERROR:   ", ERROR
          Write (6,*) "      <-- DECDEGlocal "
          Return
       Endif
       Read(CHAIN(1:NMIN-1),100,ERR=99) DEGREE
       If (.Not.MINUS) Then
          Value = CONV*(DEGREE+MINUTE/60.D0+SECOND/3600.D0)
       Else
          Value = CONV*(DEGREE-MINUTE/60.D0-SECOND/3600.D0)
       Endif
    Endif
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "          ERROR:   ", ERROR
       Write (6,*) "      <-- DECDEGlocal "
    End If
    !
    Return
    !
99  write(6,*) 'E-ANGLE,  Invalid angle format'
    ERROR = .True.
    Write (6,*) "          ERROR:   ", ERROR
    Write (6,*) "      <-- DECDEGlocal "
    Return
100 Format(F20.0)
  End Subroutine DECDEGlocal
!!!
!!!
  Subroutine CVDEGlocal (ARG,NARG,ANG,DVAL,ERROR)
    !------------------------------------------------------------------------
    ! POM	internal routine
    !	converts a chain into RADIANS according to angle unit
    !	arg	c*1	chain to be decoded	Input
    !	narg	I	length of arg		Input
    !	ang	C*1	angle unit : H = hours, M = arc minutes,
    !			D = arc degrees, S = arc seconds	Input
    !	dval	R*8	result			Output
    !	error	L*1	error code		Output
    !
    ! Dummy -----------------------------------------------------------------
    Character(len=*)      ::  ARG, ANG
    Integer NARG, NDIV1
    Logical ERROR
    Real(kind=kindDouble) ::  DVAL, PI
    Parameter (PI=3.141592653589793D0)
    ! Code ------------------------------------------------------------------
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "      --> CVDEGlocal "
       Write (6,*) "         NARG   ", NARG
       Write (6,*) "          ARG ->", ARG(1:NARG), "<-"
       Write (6,*) "          ANG   ", ANG
    End If
    !
    If (ANG.Eq.'S') Then
       Read(ARG(1:NARG),'(f20.0)',ERR=900) DVAL
       DVAL = DVAL / 3600.
    Else
       If (ANG.Eq.'M') Then
          NDIV1 = 21600
       Elseif (ANG.Eq.'H') Then
          NDIV1 = 24
       Else
          NDIV1 = 360
       Endif
       Call DECDEGlocal(ARG,NARG,DVAL,NDIV1,ERROR)
!!$        Write (6,*) "      DECDEGlocal ERROR:  ", ERROR
       If(ERROR) Return
       DVAL = DVAL * PI /180D0
    Endif
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "          DVAL:      ", dval
       Write (6,*) "      <-- CVDEGlocal "
    End If
    !
    Return
900 Write(6,100) 'E-CVDEGlocal,  error decoding ',ARG(1:NARG)
    ERROR = .True.
    Return
100 Format(1X,A,A)
  End Subroutine CVDEGlocal
!!!
!!!
  subroutine sexdeg(ch,rr8,ndiv)
    !---------------------------------------------------------------------
    ! ASTRO	Utility routine. Duplicates LIBRARY SEXAG routine.
    !	Writes an angle in sexagesimal notatiom
    ! Arguments :
    !	CH	C*1	Character string receiving the format	Output
    !	RR8	R*8	Angle in DEGREES 			Input
    !	NDIV	I	Number of divisions per turn (24 / 360)	Input
    ! Subroutines :
    !	CFLAB
    ! No common
    !---------------------------------------------------------------------
    character(len=*) :: ch            !
    real*8 :: rr8                     !
    integer :: ndiv                   !
    ! Local
    real*8 :: pi
    parameter (pi=3.141592653589793d0)
    real*8 :: r8
    integer :: lch
    ! Code ------------------------------------------------------------------------
    !
    r8 = rr8
    go to 5
    !
    entry sexrad (ch,rr8,ndiv)
    r8 = rr8/pi*180.d0           ! Convert radians to degrees
5   continue
    !
    lch = len(ch)
    if (ndiv.eq.360) then
       r8 = r8*3.6d3              ! In seconds
       call gag_cflab1 (ch, r8, lch, 3)
    else
10     continue
       r8 = r8+360.d0
       if (r8.lt.0.d0) go to 10
       r8 = dmod(r8,360.d0)*240.d0    ! In seconds of time
       call gag_cflab1 (ch, r8, lch, 4)
    endif
    call sic_blanc(ch,lch)
  end subroutine sexdeg
!!!
!!!
End Module modulePakoCatalog

