!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoVlbi">
!   <VERSION>
!                Id: modulePakoVlbi.f90,v 1.2.3 2014-10-27 Hans Ungerechts
!                Id: modulePakoVlbi.f90,v 1.2.3 2014-02-10 Hans Ungerechts
!                Id: modulePakoVlbi.f90,v 1.2.3 2014-02-03 Hans Ungerechts
!                                       v 1.2.0 2012-08-17 Hans Ungerechts
!                based on
!                Id: modulePakoVlbi.f90,v 1.1.0 2009-03-25 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>
!                Calibrate
!                Focus
!                OnOff
!                OtfMap
!                Pointing
!                Tip
!                Track
!                VLBI
!   </SIBLINGS>        
!
!   Pako module for command: VLBI
!
! </DOCUMENTATION> <!-- name="modulePakoVlbi" -->
!
!----------------------------------------------------------------------
!
Module modulePakoVlbi
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  !! TBD 2.0:   Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  Public :: vlbi
  Public :: saveVlbi
  Public :: startVlbi
  Public :: displayVlbi
!!$  Public :: plotOMVlbi
  !     
  ! *** Variables for Vlbi ***
  Include 'inc/variables/variablesVlbi.inc'
!!!   
Contains
!!!
!!!
  Subroutine Vlbi(programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Include 'inc/ranges/rangesVlbi.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Vlbi, v 1.2.3 2014-02-03 ---> SR: Vlbi ")                   ! trace execution
    error = .False.
    !    
    !! TBD 2.0: !!$    If (GV%privilege.Eq.setPrivilege%VLBI) Then
       !
       ! *** initialize ***
       If (.Not.isInitialized) Then
          Include 'inc/variables/setDefaults.inc'
          isInitialized = .True.
       Endif
       !
       ! *** set In-values = previous Values ***
       If (.Not.ERROR) Then
          vars(iIn) = vars(iValue)
       End If
       !
       ! *** check N of Parameters: 0, n --> at  most n allowed  ***
       !     read Parameters, Options (to detect errors)            
       !     and check for validity and ranges                      
       Call checkNofParameters(command,                                             &
            &     0, 0,                                                             &
            &     nArguments,                                                       &
            &     errorNumber)                                                      !
       ERROR = ERROR .Or. errorNumber
       !
       If (.Not. ERROR) Then
          Call pakoMessageSwitch (setOn = .True.)
          !
          !! Include 'inc/parameters/parameters*.inc'
          Include 'inc/options/readOptionsVlbi.inc'
          !
       End If
       !
       ! *** set defaults ***
       Include 'inc/options/optionDefaults.inc'
       !
       ! *** read Parameters, Options (again, after defaults!) ***
       !     and check for validity and ranges 
       If (.Not. ERROR) Then
          Call pakoMessageSwitch (setOff = .True.)
          !
          !! Include 'inc/parameters/parameters*.inc'
          Include 'inc/options/readOptionsVlbi.inc'
          !
       End If
       Call pakoMessageSwitch (setOn = .True.)
       !
       ! *** check consistency and                           ***
       ! *** store into temporary (intermediate) variables   ***
       If (.Not.error) Then
          !
          errorInconsistent = .False.
          !
          If (.Not. errorInconsistent) Then
             vars(iTemp) = vars(iIn)
          End If
          !
       End If
       !
       ERROR = ERROR .Or. errorInconsistent
       !
       ! *** store from temporary into final variables ***
       If (.Not.ERROR) Then
          vars(iValue) = vars(iTemp)
          !! TBD 2.0: !!$          If (vars(iValue)%doLastSource) Then
          !! TBD 2.0: !!$             Call getSource(OM%vlbi,iOmVLBI)
          !! TBD 2.0: !!$             vars(iValue)%doLastSource = .False.     !  reset doLastSource
          !! TBD 2.0: !!$             !  after it's done
          !! TBD 2.0: !!$          End If
       End If
       !
       ! *** display values ***
       If (.Not.ERROR) Then
          Call displayVlbi
       End If
       !
       ! *** set "selected" observing mode & plot ***
       If (.Not.error) Then
          GV%sessionMode       = OM%vlbi
          GV%observingMode     = OM%vlbi
          GV%observingModePako = OMpako%vlbi
          GV%omSystemName      = GPnone
          GV%notReadySWafterOM = .False.
          GV%notReadySecondaryRafterOM = .False.
          !D       Write (6,*) '      GV%notReadySecondaryRafterOM: ',           &
          !D            &             GV%notReadySecondaryRafterOM               !
          vlbiCommand          = line(1:lenc(line))
          Call analyzeVlbi (errorA)
          Call listSegmentList (errorA)
       End If
       !
       !! TBD 2.0: !!$    Else
       !! TBD 2.0: !!$       ERROR = .True.
       !! TBD 2.0: !!$       Write (messageText,*)                                                     &
       !! TBD 2.0: !!$            &           'not privileged for VLBI mode.'                          !
       !! TBD 2.0: !!$       Call pakoMessage(priorityE,severityE,command,messageText)                 
       !! TBD 2.0: !!$    End If
    Return
  End Subroutine Vlbi
!!!
!!!
  Subroutine analyzeVlbi (errorA)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer                :: nScan 
    Character (len=lenCh)  :: aUnit, sUnit
    Integer                :: iSS           = 0     ! counts subcans/segment
    !
    errorA    = .False.
    iSS       = 0
    nScan     = 1111
    aUnit     = GV%angleUnitC                        ! 
    sUnit     = GV%speedUnitC                        ! 
    !
    !D    Write (6,*) " module VLBI ---> SR: analyze VLBI "
    !D    Write (6,*) "                                   "
    !
    segList(:) = segDefault
    !
    Call countSubscans(value=0)
    Call countSegments(value=0)
    !
    Do kk = 1, vars(iValue)%nSubscans, 1
       !
       Call countSubscans(ii)
       Call countSegments(jj)
       !
       segList(ii)%newSubscan =  .True.
       segList(ii)%scanNumber =  nScan
       segList(ii)%ssNumber   =  ii
       segList(ii)%segNumber  =  1
       segList(ii)%ssType     =  ss%VLBI
       segList(ii)%segType    =  seg%track
       segList(ii)%angleUnit  =  aUnit
       segList(ii)%speedUnit  =  sUnit
       segList(ii)%flagOn     =  .True.
       segList(ii)%flagRef    =  .False.
       segList(ii)%pStart     =  vars(iValue)%offset
       segList(ii)%pEnd       =  segList(ii)%pStart
       segList(ii)%speedStart =  0.0
       segList(ii)%speedEnd   =  0.0
       segList(ii)%systemName =  vars(iValue)%systemName
       segList(ii)%altOption  =  'tSegment'
       segList(ii)%tSegment   =  vars(iValue)%tSubscan
       segList(ii)%tRecord    =  0.1
       !
    End Do
    !
    Call getCountSegments(nSegments)
    !
    Return 
    !
  End Subroutine analyzeVlbi
!!!
!!!
  Subroutine displayVlbi
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplayVlbi.inc'
    !
  End Subroutine displayVlbi
!!!
!!!
!!$  Subroutine plotOMVlbi (errorP)
!!$    !
!!$    !**   Variables  for Plots   ***
!!$    Include 'inc/variables/headerForPlotMethods.inc'
!!$    !
!!$    !**   standard working variables   ***
!!$    Include 'inc/variables/standardWorkingVariables.inc'
!!$    !
!!$    errorP = .False.
!!$    !
!!$    Call configurePlots
!!$    !
!!OLD !!$    ! TBD: general plot of segList
!!OLD !!$    !
!!$    !
!!$    Return
!!$  End Subroutine plotOMVlbi
!!$!!!
!!!
  Subroutine saveVlbi(programName,LINE,commandToSave,  iUnit, ERROR)
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !      
    !D      Write (6,*) '      ---> SR: saveVlbi '
    !
    contC = contNN
    !
    Include 'inc/commands/saveCommand.inc'
    !
    contC = contCC
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveVlbi
!!!
!!!
  Subroutine startVlbi(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ! to format message text
    Character(len=lenLine)  ::  messageText
    !
    Character (len=lenCh)   :: valueC
    Logical                 :: errorL, errorXML
    !
    !D    Write (6,*) " module VLBI ---> SR: start VLBI "
    !D    Write (6,*) "                                 "
    !
    !! TBD 2.0: !!$    Call storeSource(OM%vlbi,iOmVLBI)
    !
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",                       &
         &                         comment="save from pako",                     &
         &                         error=errorXML)                               !
    Call pakoXMLwriteStartElement("DESCRIPTION",                                 &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call saveVlbi        (programName,LINE,commandToSave, iUnit, errorL)
    !                                                                       
    Call pakoXMLwriteEndElement("DESCRIPTION",                                   &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    Call pakoXMLwriteEndElement  ("RESOURCE","pakoScript",                       &
         &                         space="after",                                &
         &                         error=errorXML)                               !
    !
    Call writeXMLantenna(programName,LINE,commandToSave,                         &
         &         iUnit, ERROR)                                                 !
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,                        &
         &        iUnit, ERROR)                                                  !
    !
    Call writeXMLbackend(programName,LINE,commandToSave,                         &
         &        iUnit, ERROR)                                                  !
    !
    Call writeXMLswTotalPower(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLsource(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/generalScanHead.inc'
    !
    ! **  Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       messageText = "writing observing mode in XML format"
       Call PakoMessage(priorityI,severityI,                                     &
               &           commandToSave,messageText)                            !
       Call writeXMLvlbi(programName,LINE,commandToSave,                         &
         &         iUnit, ERROR)                                                 !
    End If
    !
    ! **  subscan list to XML
    Call writeXMLsubscanList(programName,LINE,commandToSave,iUnit, ERROR)
    !   
    Include 'inc/startXML/generalTail.inc'
    !
    Return
  End Subroutine startVlbi
!!!
!!!
  Subroutine writeXMLvlbi(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: len1, len2
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Logical                 :: errorXML
    !
    Call pako_message(seve%t,"PAKO",                                             &
         &  " module Vlbi, v 1.2.3  --> SR: writeXMLVlbi 2014-10-24")            ! trace execution
    !
    ERROR = .False.
    !
    !D Write (6,*) "   --> writeXMLvlbi "
    !D Write (6,*) "       iUnit:    ", iUnit
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/annotationsXML.inc'
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLvlbi
!!!
!!!
End Module modulePakoVlbi
