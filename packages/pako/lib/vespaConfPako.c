
#include "vespaConfPako.h"

/* This could have been written in Fortran, if the preceding 
 * include file would not have a table so big that it largely 
 * exceeds the limits on statement length of the Fortran compiler.
 */

#include "gsys/cfc.h"

#define checksiglist CFC_EXPORT_NAME( checksiglist)


static int 
issubseries(const unsigned char *sub,
	    const unsigned char *ser,
	    int len,
	    unsigned char *rest)
{
        int i, j, k;
	/* First item must match, and we don'd care about zero 
	 * length since the len parameter is either 8 or 12
	 */
	if (sub[0]!= ser[0]) return 0;
        for (i=j=1, k=0; j<len; j++) {
                if(i<4 && sub[i] == ser[j]) {
                        i+=1;
                } else {
			rest[k++] = ser[j];
		}
        }
        return i==4;
}

/* The name has been uglified to what the Fortran 
 * compiler generates: everything is lowercase.
 */
int 
checksiglist(int SigList[12])
{
	int i;
	unsigned char sigs[12];
	for (i=0; i<12; i++) {
		sigs[i] = SigList[i];
	}
	for (i=0; i<DBENTRIES; i++) {
	  	/* Normally only the first eight elements are useful, but
		 * the algorithm used in issubseries may copy up to the
		 * input length -1 when there is no match.
		 */
		unsigned char rem1[12];
		if (issubseries(db[i], sigs, 12, rem1)) {
			int j;
			for (j=i; j<DBENTRIES; j++) {
				unsigned char rem2[8];
				if (issubseries(db[j], rem1, 8, rem2)) {
					int k;
					for (k=j; k<DBENTRIES; k++) {
						if (!memcmp(db+k, rem2, 4))
							return 0;
					}
				}
			}
		}
	}
	return 1;
}
