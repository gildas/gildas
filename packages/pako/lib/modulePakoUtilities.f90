!     
! Id: modulePakoUtilities.f90,v 1.0.6.4 2007-03-29 Hans Ungerechts
!    
! <DOCUMENTATION name="modulePakoUtilities">
! </DOCUMENTATION>
!
! <DEV> 
! TBD: - more documentation
! <\DEV> 
!
Module modulePakoUtilities
  !
  Use modulePakoGlobalParameters
  Use modulePakoMessages
  !     
  Implicit None
  Save
  Private
  !
  ! *** Subroutines
  !
  Public :: pakoSpecialChar
  Public :: pakoUmatchKey
  Public :: pakoUmatchKeyNew
  Public :: pakoUmatchReal
!!!
!!!
Contains
!!!
!!!
  Subroutine pakoSpecialChar(string,chars,command,howto,                         &
       &                     iMatch,nMatch,                                      &
       &                     hasSpecial,returnCode)
    !
    Character(len=*),               Intent(IN)            :: string      ! string to check
    Character(len=*),               Intent(IN) , Optional :: chars       ! additional chars to flag
    !                                                                    ! 
    Character(len=*),               Intent(IN) , Optional :: command     ! command (for messages)
    Character(len=*),               Intent(IN) , Optional :: howto       ! how-to
    Integer,                        Intent(OUT), Optional :: iMatch      ! index of matched key
    Integer,                        Intent(OUT), Optional :: nMatch      ! total number of matches
    !                                                                    
    Logical,                        Intent(OUT)           :: hasSpecial  ! 
    Character(len=*),               Intent(OUT), Optional :: returnCode  ! 
    !
    ! <DOCUMENTATION name="pakoSpecialChar">
    ! 
    ! checks string for presence of "special" characters, i.e.,
    ! characters not on the list normalChars (see below) or this in the
    ! optional inpout list chars
    !
    ! </DOCUMENTATION>
    !
    ! <DEV>
    ! TBD: implement different how-tos
    !      implement built-in list of "special characters" : <>&  (for XML)
    ! <\DEV>
    !
    Integer                            ::  ii, jj
    Character(len=lenLine)             ::  messageText
    Character(len=lenLine), Parameter  ::  normalChars  =                        &
         &" 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^*()_-+{}[]:;'|\~`,.?/"         
    !
    !D    Write (6,*) "   ---> pakoSpecialChar:"
    !D    Write (6,*) "        string: ->", string, "<-"
    !D    If (Present(chars)) Then
    !D    Write (6,*) "        chars:  ->", chars,  "<-"
    !D    End If
    !D    Write (6,*) "        normalChars:  ->", normalChars(1:Len_trim(normalChars)), "<-"
    !
    hasSpecial = .False.
    If (Present(iMatch)) iMatch = GPnoneI
    If (Present(nMatch)) nMatch = 0
    If (Present(returnCode)) Then
       returnCode = GPnone
    End If
    !
!!$    If (Scan(string,chars).Ge.1) Then
!!$       If (Present(iMatch)) iMatch     = Scan(string,chars)
!!$       hasSpecial = .True.
!!$    End If
    !
    Do ii = 1, Len_trim(string)
       If (Index(normalChars,string(ii:ii)).Eq.0 ) Then
          messageText =                                                          &
               &   string(1:Len_trim(string))//                                  &
               &   " contains special character: "//                             &
               &   string(ii:ii)
          If (present(command)) Then
             Call pakoMessage(priorityW,severityW,command,messageText)
          Else
             Call pakoMessage(priorityW,severityW,"string",messageText)
          End If
          hasSpecial = .True.
          If (Present(iMatch).And.ii.Eq.GPnoneI) iMatch = ii
          If (Present(nMatch)) nMatch = nMatch+1
       Else If (Present(chars)) Then
          Do jj = 1, Len_trim(chars)
             If ( string(ii:ii).Eq.chars(jj:jj) ) Then
                messageText =                                                    &
                     &   string(1:Len_trim(string))//                            &
                     &   " contains special character: "//                       &
                     &   string(ii:ii)
                If (present(command)) Then
                   Call pakoMessage(priorityW,severityW,command,messageText)
                Else
                   Call pakoMessage(priorityW,severityW,"string",messageText)
                End If
                hasSpecial = .True.
                If (Present(iMatch).And.ii.Eq.GPnoneI) iMatch = ii
                If (Present(nMatch)) nMatch = nMatch+1
             End If
          End Do
       End If
    End Do
    !
  End Subroutine pakoSpecialChar
!!!
!!!
  Subroutine pakoUmatchKey(keys,key,command,howto,iMatch,nMatch,error,errorCode)
    !
    Character(len=*), Dimension(:), Intent(IN)            :: keys        ! allowed keys
    Character(len=*),               Intent(INOUT)         :: key         ! IN: key to match
    !                                                                    ! OUT: matched key
    Character(len=*),               Intent(IN) , Optional :: command     ! command (for messages)
    Character(len=*),               Intent(IN) , Optional :: howto       ! how-to match
    Integer,                        Intent(OUT), Optional :: iMatch      ! index of matched key
    Integer,                        Intent(OUT), Optional :: nMatch      ! total number of matches
    !                                                                    
    Logical,                        Intent(OUT)           :: error       ! errorFlag 
    Character(len=*),               Intent(OUT), Optional :: errorCode   ! errorCode
    !
    ! <DOCUMENTATION name="pakoUmatchKey">
    ! 
    ! Tries to match "key" in allowed "keys"
    ! If exactly one match is found, the matched key from "keys" is returned in "key"
    !    and the index of the matched key is returned in iMatch
    ! If zero or more than 1 matches are found .TRUE. is returned in "error"
    !    and an explanation is return in "errorCode"
    ! If "howto" contains: 
    !          "MATCH" : any substring can match
    !          "START" : substring at start must match
    !          "EXACT" : match must be exact
    !          "UPPER" : strings are converted to upper case before match is attempted
    !                    and any value returned in "key" is also upper case
    !                    (NB: the case of the value of "howto" itself is always ignored!)
    !          "QUIET" : no error messages are displayed
    ! Local varaible lC (see below) limits the total number of characters considered
    !
    ! </DOCUMENTATION>
    !
    ! <DEV>
    ! TBD: check / debug all the different howto s!
    ! <\DEV>
    !
    Integer            :: lBoundKeys, uBoundKeys, ii, lii, lKey, iM, nM
    Logical            :: quiet, match, matchStart, matchExact  
    Integer, Parameter :: lC = 128
    Character(len=lC)  :: K, Kii
    Character(len=lC)  :: HT
    Character(len=lC)  :: messageText
    !
    !D    Write (6,*) "   --> pakoUmatchKey "
    !D    If (present(command)) Write (6,*) "       command: ", command
    !D    If (present(howto))   Write (6,*) "       howto:   ", howto
    !
    error = .False.
    quiet = .False.
    If (Present(errorCode)) errorCode  = "OK"
    If (Present(iMatch))    iMatch     =  0
    If (Present(nMatch))    nMatch     =  0
    !
    lKey = Min(Len_trim(key),lC)
    K = key(1:lKey)
    !
    HT = ' '
    If (Present(howto)) Then
       HT = howto
       Call Sic_upper(HT)
       If (Index(HT,'UPPER').Ge.1) Then
          Call Sic_upper(K)
       End If
       If (Index(HT,'QUIET').Ge.1) Then
          quiet = .True.
       End If
    End If
    !
    !D    Write (6,*) ' key: K ->',K(1:lKey),'<-'
    !
    lBoundKeys = Lbound(keys,1)
    uBoundKeys = Ubound(keys,1)
    !
    iM = 0
    nM = 0
    !
    Do ii = lBoundKeys, uBoundKeys, 1
       lii = (Min(Len_trim(keys(ii)),lC))
       Kii = keys(ii)(1:lii)
       !D       Write (6,*) ii, 'keys(ii) ->',keys(ii)(1:lii),'<-'
       If (Index(HT,'UPPER').Ge.1) Then
          Call Sic_upper(Kii)
       End If
       !D       Write (6,*) ii, 'Kii      ->',Kii(1:lii),'<-'
       match      = .False.
       matchstart = .False.
       matchExact = Kii(1:lii).Eq.key(1:lKey) .And. lii.Eq.lKey
       If (Present(howto)) Then
          HT = howto
          Call Sic_upper(HT)
          match      = Index(HT,'MATCH').Ge.1 .And. Index(Kii,K(1:lKey)).Ge.1
          matchStart = Index(HT,'START').Ge.1 .And. Kii(1:lKey).Eq.K(1:lKey)
          matchExact = Index(HT,'EXACT').Ge.1 .And. Kii(1:lii).Eq.K(1:lKey) .And. lii.Eq.lKey
       End If
       If (match .Or. matchstart .Or. matchExact) Then
          !D           Write (6,*) '      MATCH '
           iM = ii
           nM = nM+1
       End If
    End Do
    !
    If (Present(nMatch)) nMatch = nM
    !
    If (nM .Eq. 1) Then
       If (Present(iMatch)) iMatch = iM
       key = keys(iM)
       If (Present(howto)) Then
          HT = howto
          Call Sic_upper(HT)
          If (Index(HT,'UPPER').Ge.1) Then
             Call sic_upper(key)
          End If
       End If
    Else If (nM .Eq. 0) Then
       error = .True.
       messageText = ' keyword "'//key(1:lKey)//'" not valid'
    Else If (nM .Ge. 1) Then
       error = .True.
       messageText = ' keyword "'//key(1:lKey)//'" not unique'
    End If
!
    If (error) Then
       If (Present(errorCode)) errorCode = messageText
       If (.Not. quiet) Then
          If (Present(command)) Then
             Call PakoMessage(priorityE,severityE,command,messageText)
          Else
             Call PakoMessage(priorityE,severityE,'in subroutine pakoUmatchKey',messageText)
          End If
       End If
    End If
    !
    !D    Write (6,*) ' last match:        iM: ', iM
    !D    Write (6,*) ' last matched key:      ', keys(iM)
    !D    Write (6,*) ' number of matches: nM: ', nM
    !D    Write (6,*) ' understood as key:     ', key
    !
  End Subroutine pakoUmatchKey
!!!
!!!
  Subroutine pakoUmatchKeyNew(keys,key,command,howto,                            &
       &                      iMatch,nMatch,error,errorCode)                     !
    !
    ! New /experimental flavor of pakoUmatchKey
    !
    Character(len=*), Dimension(:), Intent(IN)            :: keys        ! allowed keys
    Character(len=*),               Intent(INOUT)         :: key         ! IN: key to match
    !                                                                    ! OUT: matched key
    Character(len=*),               Intent(IN) , Optional :: command     ! command (for messages)
    Character(len=*),               Intent(IN) , Optional :: howto       ! how-to match
    Integer,                        Intent(OUT), Optional :: iMatch      ! index of matched key
    Integer,                        Intent(OUT), Optional :: nMatch      ! total number of matches
    !                                                                    
    Logical,                        Intent(OUT)           :: error       ! errorFlag 
    Character(len=*),               Intent(OUT), Optional :: errorCode   ! errorCode
    !
    ! <DOCUMENTATION name="pakoUmatchKey">
    ! 
    ! Tries to match "key" in allowed "keys"
    ! If exactly one match is found, the matched key from "keys" is returned in "key"
    !    and the index of the matched key is returned in iMatch
    ! If zero or more than 1 matches are found .TRUE. is returned in "error"
    !    and an explanation is return in "errorCode"
    ! If "howto" contains: 
    !          "MATCH" : any substring can match
    !          "START" : substring at start must match
    !          "EXACT" : match must be exact
    !          "UPPER" : strings are converted to upper case before match is attempted
    !                    and any value returned in "key" is also upper case
    !                    (NB: the case of the value of "howto" itself is always ignored!)
    !          "QUIET" : no error messages are displayed
    ! If several of MATCH START EXACT are specified, the least restrictive one rules!
    ! Local varaible lC (see below) limits the total number of characters considered
    !
    ! </DOCUMENTATION>
    !
    ! <DEV>
    ! TBD: check / debug all the different howto s!
    ! <\DEV>
    !
    Integer            :: lBoundKeys, uBoundKeys, ii, lii, lKey, iM, nM
    Logical            :: quiet, match, matchStart, matchExact  
    Integer, Parameter :: lC = 128
    Character(len=lC)  :: K, Kii
    Character(len=lC)  :: HT
    Character(len=lC)  :: messageText
    !
    !D     Write (6,*) "   --> pakoUmatchKeyNew "
    !D     If (present(command)) Write (6,*) "       command: ", command
    !D     If (present(howto))   Write (6,*) "       howto:   ", howto
    !
    error = .False.
    quiet = .False.
    If (Present(errorCode)) errorCode  = "OK"
    If (Present(iMatch))    iMatch     =  0
    If (Present(nMatch))    nMatch     =  0
    !
    lKey = Min(Len_trim(key),lC)
    K = key(1:lKey)
    !
    HT = ' '
    If (Present(howto)) Then
       HT = howto
       Call Sic_upper(HT)
       If (Index(HT,'UPPER').Ge.1) Then
          Call Sic_upper(K)
       End If
       If (Index(HT,'QUIET').Ge.1) Then
          quiet = .True.
       End If
    End If
    !
    !D     Write (6,*) ' key: K ->',K(1:lKey),'<-'
    !
    lBoundKeys = Lbound(keys,1)
    uBoundKeys = Ubound(keys,1)
    !
    iM = 0
    nM = 0
    !
    Do ii = lBoundKeys, uBoundKeys, 1
       lii = (Min(Len_trim(keys(ii)),lC))
       Kii = keys(ii)(1:lii)
       !D        Write (6,*) ii, 'keys(ii) ->',keys(ii)(1:lii),'<-'
       If (Index(HT,'UPPER').Ge.1) Then
          Call Sic_upper(Kii)
       End If
       !D        Write (6,*) ii, 'Kii      ->',Kii(1:lii),'<-'
       match      = .False.
       matchstart = .False.
       matchExact = Kii(1:lii).Eq.key(1:lKey) .And. lii.Eq.lKey
       If (Present(howto)) Then
          HT = howto
          Call Sic_upper(HT)
       End If
       match      = Index(HT,'MATCH').Ge.1 .And. Index(Kii,K(1:lKey)).Ge.1
       matchStart = Index(HT,'START').Ge.1 .And. Kii(1:lKey).Eq.K(1:lKey)
       matchExact = Index(HT,'EXACT').Ge.1 .And. Kii(1:lii).Eq.K(1:lKey) .And. lii.Eq.lKey
       !
       If (match .Or. matchstart .Or. matchExact) Then
          !D           Write (6,*) '      MATCH '
          iM = ii
          nM = nM+1
       End If
    End Do
    !
    If (Present(nMatch)) nMatch = nM
    !
    If (nM .Eq. 1) Then
       If (Present(iMatch)) iMatch = iM
       key = keys(iM)
       If (Present(howto)) Then
          HT = howto
          Call Sic_upper(HT)
          If (Index(HT,'UPPER').Ge.1) Then
             Call sic_upper(key)
          End If
       End If
    Else If (nM .Eq. 0) Then
       error = .True.
       messageText = ' keyword "'//key(1:lKey)//'" not valid'
    Else If (nM .Ge. 1) Then
       error = .True.
       messageText = ' keyword "'//key(1:lKey)//'" not unique'
    End If
!
    If (error) Then
       If (Present(errorCode)) errorCode = messageText
       If (.Not. quiet) Then
          If (Present(command)) Then
             Call PakoMessage(priorityE,severityE,command,messageText)
          Else
             Call PakoMessage(priorityE,severityE,'in subroutine pakoUmatchKey',messageText)
          End If
       End If
    End If
    !
    !D     Write (6,*) ' last match:        iM: ', iM
    !D     Write (6,*) ' last matched key:      ', keys(iM)
    !D     Write (6,*) ' number of matches: nM: ', nM
    !D     Write (6,*) ' understood as key:     ', key
    !
  End Subroutine pakoUmatchKeyNew
!!!
!!!
  Subroutine pakoUmatchReal(values,value,delta,command,howto,iMatch,nMatch,error,errorCode)
    !
    Real, Dimension(:), Intent(IN)                        :: values      ! allowed values
    Real,               Intent(INOUT)                     :: value       ! IN: value to match
    !                                                                    ! OUT: matched value
    Real,               Intent(IN)                        :: delta       ! IN: allowed delta 
!
    Character(len=*),               Intent(IN) , Optional :: command     ! command (for messages)
    Character(len=*),               Intent(IN) , Optional :: howto       ! how-to match
    Integer,                        Intent(OUT), Optional :: iMatch      ! index of matched key
    Integer,                        Intent(OUT), Optional :: nMatch      ! total number of matches
    !                                                                    
    Logical,                        Intent(OUT)           :: error       ! errorFlag 
    Character(len=*),               Intent(OUT), Optional :: errorCode   ! errorCode
    !
    ! <DOCUMENTATION name="pakoUmatchKey">
    ! 
    !          "EXACT" : match must be exact
    !          "DELTA" : match must be within delta 
    !
    ! </DOCUMENTATION>
    !
    ! <DEV>
    ! <\DEV>
    !
    Integer            :: lBoundValues, uBoundValues, ii, iM, nM
    Logical            :: matchExact, matchDelta  
    Integer, Parameter :: lC = 128
    Character(len=lC)  :: HT
    Character(len=lC)  :: messageText
    !
    error = .False.
    If (Present(errorCode)) errorCode  = "OK"
    If (Present(iMatch))    iMatch     =  0
    If (Present(nMatch))    nMatch     =  0
    !
    matchExact = .True.
    matchDelta = .False.
    If (Present(howto)) Then
       HT = howto
       Call Sic_upper(HT)
       matchExact = HT .Eq. "EXACT"
       matchDelta = HT .Eq. "DELTA"
    End If
    !
    lBoundValues = Lbound(values,1)
    uBoundValues = Ubound(values,1)
    !
    iM = 0
    nM = 0
    !
    Do ii = lBoundValues, uBoundValues, 1
       If (matchExact .And. Abs(value-values(ii)) .Eq. 0.0) Then
          iM = ii
          nM = nM+1
       End If
       If (matchDelta .And. Abs(value-values(ii)) .Le. delta) Then
          iM = ii
          nM = nM+1
       End If
    End Do
    !
    If (Present(nMatch)) nMatch = nM
    !
    If (nM .Eq. 1) Then
       If (Present(iMatch)) iMatch = iM
       value = values(iM)
    Else If (nM .Eq. 0) Then
       error = .True.
       Write (messageText, *) ' value ', value, ' not valid'
    Else If (nM .Ge. 1) Then
       error = .True.
       Write (messageText, *) ' value ', value, ' not unique'
    End If
!
    If (error) Then
       If (Present(errorCode)) errorCode = messageText
       If (Present(command)) Then
          Call PakoMessage(priorityE,severityE,command,messageText)
       Else
          Call PakoMessage(priorityE,severityE,'in subroutine pakoUmatchKey',messageText)
       End If
    End If
    !
!D    Write (6,*) ' last match:        iM: ', iM
!D    Write (6,*) ' last matched value:      ', values(iM)
!D    Write (6,*) ' number of matches: nM: ', nM
!D    Write (6,*) ' understood as value:     ', value
    !
  End Subroutine 
!!!
!!!
End Module modulePakoUtilities

