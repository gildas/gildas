!
!--------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoSubscanList.f90">
!   <VERSION>
!              Id: modulePakoSubscanList.f90,v 1.2.3  2014-10-20 Hans Ungerechts
!              Id: modulePakoSubscanList.f90,v 1.2.3  2014-10-15 Hans Ungerechts
!              Id: modulePakoSubscanList.f90,v 1.2.3  2014-09-23 Hans Ungerechts
!              Id: modulePakoSubscanList.f90,v 1.2.2  2013-01-24 Hans Ungerechts
!              based on:
!              Id: modulePakoSubscanList.f90,v 1.1.13 2012-10-29 Hans Ungerechts
!   </VERSION>
!   <HISTORY>

!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                
!   </FAMILY>
!   <SIBLINGS>

!   </SIBLINGS>        
!
!   Pako module for subscan lists
!   <-- used by other modules, in particular observing modes and DIY
!   list of subscans and segments generated directly with user commands
!   or by the "analyze" subroutines of the observing modes
!
! </DOCUMENTATION> <!-- name="modulePakoSubscanList.f90" -->
!
!--------------------------------------------------------------------------------
!
!
Module modulePakoSubscanList
  !
  Use modulePakoMessages
  Use modulePakoTypes
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  !
  !
  Use gbl_message
  Implicit None
  Save
  Private
  !
  ! ***   data types and variables
  !
  Public  :: nSegments                 ! total number of segments -- DEPRECATED
  !
  ! *     points
  Public  :: pII
  !
  Public  :: dimensionList             ! dimension of segment list
  !
  ! *     subscans                     ! DEPRECATED
  Public  :: subscanType,     ssList,    ssDefault
  !
  ! *     segments
  Public  :: segmentType,     segList,   segDefault,  segOtfII
  !
  ! ***   subroutines
  Public  :: saveSubscanList            ! save into .pako script 
  Public  :: startSubscanList           ! write complete XML for start of subscan list
  Public  :: writeXMLsubscanList        ! write XML only for subscan list
  Public  :: getNumberSegments          ! get     number of segments
  Public  :: getCountSubscans           ! get     subscan counter
  Public  :: getCountSegments           ! get     segment    "
  Public  :: countSubscans              ! control subscan counter
  Public  :: countSegments              !         segment    "
  Public  :: listSegmentList            ! list  nicely   list of segments
  Public  :: plotSegmentList            ! plot           list of segments
  Public  :: printSegmentList           ! print detailed list of segments
  !
  ! ***   type for list of segments
  !
  Type    :: segmentType
     Logical           :: newSubscan     = .False. ! flag for start of new subscan
     Logical           :: updateDoppler  = .False. ! update Doppler correction
     Integer           :: scanNumber     = 0       ! scan number (dummy)
     Integer           :: ssNumber       = 0       ! subscan number
     Integer           :: segNumber      = 0       ! segment number
     Character(len=24) :: ssType         = "none"  ! subscan type
     Character(len=24) :: segType        = "none"  ! segment type
     Character(len=24) :: angleUnit      = "none"  
     Character(len=24) :: speedUnit      = "none" 
     Logical           :: flagOn         = .False. ! flag as On-source
     Logical           :: flagRef        = .False. ! flag as off-source REF
     Logical           :: flagDropin     = .False. ! flag as "Dropin" (inserted/added): do not SAVE
     Type(xyPointType) :: pStart                   ! start
     Type(xyPointType) :: pEnd                     ! end
     Type(xyPointType) :: pDoppler                 ! point/offsets for Doppler correction
     Real              :: lengthOtf      = 0.0     ! linear length
     Real              :: turnAngle      = 0.0     ! turn angle for circle segment
     Type(xyPointType) :: CPstart                  ! start and
     Type(xyPointType) :: CPend                    ! end for curve segment
     Character(len=24) :: systemName     = "none"  ! system of mapping offsets
     Character(len=24) :: altOption      = "none"  ! alternative speed or tSegment 
     Real              :: speedStart     = 0.0     
     Real              :: speedEnd       = 0.0
     Real              :: tSegment       = 0.0     ! time per segment
     Real              :: tRecord        = 0.0     ! time per record
     Real              :: focusOffset    = 0.0     ! focus offset
     Character(len=24) :: directionFocus = "none"  ! focus direction
     Character(len=24) :: sideBand       = GPnone  ! RX sideband (for calib.)
     !
     ! added for Lissajous:
     !
     Type(xyPointType) ::  pCenter        = xyPointType(GPnoneR,GPnoneR)   ! center --> xCenter yCenter
     Real              ::  xAmplitude     = GPnoneR                        ! amplitude
     Real              ::  yAmplitude     = GPnoneR                        !
     Real              ::  omegaX         = GPnoneR                        ! frequency
     Real              ::  omegaY         = GPnoneR                        !
     Real              ::  phiX           = GPnoneR                        ! phase
     Real              ::  phiY           = GPnoneR                        !
     !
     ! added for NIKA tune
     !
     Logical           ::  flagTune       = .False.                        ! make Track subscan sing NIKA Tune
     !
  End Type segmentType
  !
  !       NB: subscanType is deprecated
  Type    :: subscanType
     Integer           :: scanNumber  = 1       ! scan number
     Integer           :: ssNumber    = 1       ! subscan number
     Integer           :: segNumber   = 1       ! segment number
     Character(len=24) :: ssType      = "none"  ! subscan type
     Character(len=24) :: segType     = "none"  ! segment type
     Character(len=24) :: angleUnit   = "none"  
     Character(len=24) :: speedUnit   = "none"  
     Logical           :: flagOn      = .False. ! flag as On-source
     Logical           :: flagRef     = .False. ! flag as off-source REF
     Type(xyPointType) :: pStart 
     Type(xyPointType) :: pEnd
     ! TBD:     start & end are obsolete -- use pStart and pEnd !
     Type(xyPointType) :: start 
     Type(xyPointType) :: End
     Real              :: lengthOtf   = 0.0
     Real              :: turnAngle   = 0.0
     Type(xyPointType) :: CPstart 
     Type(xyPointType) :: CPend
     Character(len=24) :: altOption   = "none"  
     Real              :: speedStart  = 0.0
     Real              :: speedEnd    = 0.0
     Real              :: tSegment    = 0.0
     Real              :: tRecord     = 0.0
  End Type subscanType
  !
  Integer, Parameter   :: dimensionList = 999
  !
  Type (segmentType)                             :: segDefault
  Type (segmentType),  Dimension (dimensionList) :: segList
  !
  Type (segmentType)                             :: segOtfII
  !
  Type (xyPointType)                             :: pII
  !
  !       NB: subscanType is deprecated
  Type (subscanType)                             :: ssDefault
  Type (subscanType),  Dimension (dimensionList) :: ssList
  !
  Integer              :: iiSubscan   = 0       ! counter of subscans
  Integer              :: iiSegment   = 0       ! counter of segments
  Integer              :: nSegments   = 0       ! total number of segments
  !
!!!
!!!
Contains
!!!
!!!
  Subroutine saveSubscanList(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    !      
    Character(len=lenLine)             ::  messageText
    !
    ERROR = .False.
    !
    !D     Write (6,*) '      ---> SR: saveSubscanList using inc/commands/saveDIYlist.inc'
    Call pako_message(seve%t,programName,                                        &
         &    " module subscanList, 1.2.3 2014-10-15 ---> SR: saveSubscanList ") ! trace execution
    !
    Include 'inc/commands/saveDIYlist.inc'
    !
    Return
  End Subroutine SaveSubscanList
!!!
!!!
  Subroutine startSubscanList(programName,LINE,commandToSave, iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Character (len=lenLine) :: messageText
    Logical                 :: errorL, errorXML
    !
    !D     Write (6,*) '   --> moduleSubscanList: SR startSubscanList '
    Call pako_message(seve%t,programName,                                        &
         &   " module subscanList, 1.2.3 2014-10-15 ---> SR: startSubscanList ") ! trace execution
    !
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",                       &
         &                         comment="save from pako",                     &
         &                         error=errorXML)                               !
    Call pakoXMLwriteStartElement("DESCRIPTION",                                 &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call saveSubscanList (programName,LINE,commandToSave, iUnit, errorL)
    !                                                                       
    Call pakoXMLwriteEndElement("DESCRIPTION",                                   &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    Call pakoXMLwriteEndElement  ("RESOURCE","pakoScript",                       &
         &                         space="after",                                &
         &                         error=errorXML)                               !
    !
    Call writeXMLantenna(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLbackend(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/switchingMode.inc'
    !
    Call writeXMLsource(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! TBD: Include 'inc/startXML/generalScanHead.inc' instead of the following:
    !
    Call pakoXMLwriteStartElement("RESOURCE","scans",                            &
         &                         comment="generated by paKo",                  &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    Call pakoXMLwriteStartElement("RESOURCE","scan",                             &
         &                         comment="generated by paKo",                  &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    ! **
    !       
    valueC = GV%observingMode 
    !
    Call pakoXMLcase(valueC,error=errorXML)
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    !
    ! **  TBD: Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       messageText = "writing observing mode in XML format"
       Call PakoMessage(priorityI,severityI,                                     &
            &           commandToSave,messageText)                               !
       !
       Call writeXMLdiy(programName,LINE,commandToSave,                          &
         &         iUnit, ERROR)                                                 !
       !
    End If
    !
    If (GV%condElevation%isSet) Then
       !
       Call pakoXMLwriteStartElement("RESOURCE","conditions",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               ! 
       !
       Call pakoXMLwriteStartElement("RESOURCE","condition",                     &
            &                         error=errorXML)                            !   
       !
       valueC = GV%condElevation%parameter
       Call pakoXMLwriteElement("PARAM","parameter",valueC,                      &
            &                         dataType="char",                           &
            &                         error=errorXML)                            !
       !
       Write (valueC,*)       GV%condElevation%minimum
       Write (valueComment,'(F10.2,A,A)')                                        &
            &                 GV%condElevation%minimum/degree, " ", "degrees"    !
       !
       Call pakoXMLwriteElement("PARAM","minimum",valueC,                        &
            &                         unit=GV%condElevation%unit,                &
            &                         dataType="float",                          &
            &                         comment=valueComment,                      &
            &                         error=errorXML)                            !
       !
       Write (valueC,*)       GV%condElevation%maximum
       Write (valueComment,'(F10.2,A,A)')                                        &
            &                 GV%condElevation%maximum/degree, " ", "degrees"    !
       !
       Call pakoXMLwriteElement("PARAM","maximum",valueC,                        &
            &                         unit=GV%condElevation%unit,                &
            &                         dataType="float",                          &
            &                         comment=valueComment,                      &
            &                         error=errorXML)                            !
       !
       Call pakoXMLwriteEndElement("RESOURCE","condition",                       &
            &                         error=errorXML)                            !
       !
       Call pakoXMLwriteEndElement("RESOURCE","conditions",                      &
            &                         error=errorXML)                            !
       !
    End If
    !
    If (iiSegment.Ge.1) Then
       !
       ! **  subscan list to XML
       Call writeXMLsubscanList(programName,LINE,commandToSave,iUnit, ERROR)
       !   
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'START '//commandToSave,messageText)
       Write (messageText,*) '      -- specify at least 1 subscan and 1 segment!'
       Call PakoMessage(priorityW,severityW,'START '//commandToSave,messageText)
       !
    End If
    !
    Include 'inc/startXML/generalTail.inc'
    !
    Return
  End Subroutine startSubscanList
!!!
!!!
  Subroutine writeXMLdiy(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)   :: valueC
    Logical                 :: errorXML
    !
    ERROR = .False.
    !
    !D     Write (6,*) "   --> writeXMLdiy "
    !D     Write (6,*) "   GV%purposeIsSet:   ", GV%purposeIsSet
    !D     Write (6,*) "   GV%purpose:      ->", GV%purpose, "<-"
    !
    !D Write (6,*) "       iUnit:    ", iUnit
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
!!OLD !!$    Include 'inc/startXML/optionSystemXML.inc'
    !
    If (GV%purposeIsSet) Then
       !
       Call pakoXMLwriteStartElement("RESOURCE","annotations",                   &
            &                         error=errorXML)                            !
       !
       valueC = GV%purpose
       Call pakoXMLwriteElement("PARAM","purpose",valueC,                        &
            &                         dataType="char",                           &
            &                         error=errorXML)                            !
       !
       Call pakoXMLwriteEndElement("RESOURCE","annotations",                     &
            &                         error=errorXML)                            !
       !
    End If
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLdiy
!!!
!!!
  Subroutine writeXMLsubscanList(programName,LINE,commandToSave,                 &
       &        iUnit, ERROR)                                                    !
    !
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer            :: ii
    Character (len=24) :: valueC
    Character (len=48) :: valueComment
    Logical            :: errorXML
    ! 
    Character(len=128) :: messageText
    !
    Logical            :: inOtfSubscan
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,"SubscanList",                                      &
         &            " module SubscanList, v 1.2.3  2014-09-23 "                &
         &          //" --> SR: writeXMLsubscanList ")  ! trace execution        !
    !
    !D     Write (6,*) '       iiSegment:  ',  iiSegment
    !D     Write (6,*) '        nSegments: ',   nSegments
    !
    If (iiSegment.Ge.1) Then
       !
!!OLD !!$       Include 'inc/startXML/subScanListSubscansXML.inc'
       !
       !D       Write (6,*) "iiSegment:            ", iiSegment
       !D       Write (6,*) "number of segments:   ", nSegments
       !
       Call pakoXMLwriteStartElement("RESOURCE","subscans",                      &
            &                         comment="generated by paKo",               &
            &                         space ="before",                           &
            &                         error=errorXML)                            !
       !
       inOtfSubscan = .False.
       !
       Do ii = 1,nSegments, 1
          !
          !D           Write (6,*) ' '
          !D           Write (6,*) ' Segment #:           ', ii
          !D           Write (6,*) ' newsubscan:          ', segList(ii)%newSubscan
          !D           Write (6,*) ' segList(ii)%ssType:  ', segList(ii)%ssType
          !D           Write (6,*) ' segList(ii)%segType: ', segList(ii)%segType
          !
          If      (segList(ii)%ssType .Eq. ss%cc) Then
             Include 'inc/startXML/segCalColdXML.inc'
             !
          Else If (segList(ii)%ssType .Eq. ss%ca) Then
             Include 'inc/startXML/segCalAmbientXML.inc'
             !
          Else If (segList(ii)%ssType .Eq. ss%cg) Then
             Include 'inc/startXML/segCalGridXML.inc'
             !
          Else If (segList(ii)%ssType .Eq. ss%cs) Then
             Include 'inc/startXML/segCalSkyXML.inc'
             !
          Else If (segList(ii)%ssType.Eq.ss%on .And.                             &
               &       segList(ii)%segType.Eq.seg%track) Then                    !
             Include 'inc/startXML/trackXML.inc'
          Else If (segList(ii)%ssType.Eq.ss%ref .And.                            &
               &       segList(ii)%segType.Eq.seg%track) Then                    !
             Include 'inc/startXML/trackXML.inc'
          Else If (segList(ii)%ssType.Eq.ss%track .And.                          &
               &       segList(ii)%segType.Eq.seg%track) Then                    !
             Include 'inc/startXML/trackXML.inc'
          Else If (segList(ii)%ssType.Eq.ss%VLBI .And.                           &
               &       segList(ii)%segType.Eq.seg%track) Then                    !
             Include 'inc/startXML/vlbiTrackXML.inc'
          Else If (segList(ii)%ssType.Eq.ss%tune .And.                           &
               &       segList(ii)%segType.Eq.seg%track) Then                    !
             !D              Write (6,*) '      -> write tune subscan '
             Include 'inc/startXML/trackXML.inc'
          End If
          !
          ! ***    OTF subscan
          If (segList(ii)%ssType.Eq.ss%otf) Then
             !
             ! ***       start of OTF subscan
             If (segList(ii)%newSubscan) Then
                inOtfSubscan = .True.
                Call pakoXMLwriteStartElement("RESOURCE","subscan",              &
                     &                         comment="OTF",                    &
                     &                         error=errorXML)                   !
                valueC = segList(ii)%ssType
                Call pakoXMLcase(valueC,error=errorXML)
                If (valueC.Eq."OTF") valueC="onTheFly"
                Call pakoXMLwriteElement("PARAM","type",valueC,                  &
                     &                         dataType="char",                  &
                     &                         error=errorXML)                   !
                valueC = segList(ii)%systemName
                Call pakoXMLcase(valueC,error=errorXML)
                If (valueC.Eq."horizontalT") valueC="horizontalTrue"
                Call pakoXMLwriteElement("PARAM","systemOffset",valueC,          &
                     &                         dataType="char",                  &
                     &                         error=errorXML)                   !
                !
                Include 'inc/startXML/pOffsetDoppler.inc'
                !
                Call pakoXMLwriteStartElement("RESOURCE","segments",             &
                     &                         error=errorXML)                   !
             End If
             !
             ! ***       OTF segments
             If (segList(ii)%ssType.Eq.ss%otf .And.                              &
                  &       segList(ii)%segType.Eq.seg%track) Then                 !
                Include 'inc/startXML/segTrackXML.inc'
             End If
             !
             If (segList(ii)%ssType.Eq.ss%otf .And.                              &
                  &       segList(ii)%segType.Eq.seg%linear) Then                !
                Include 'inc/startXML/segLinearXML.inc'
             End If
             !
             If (segList(ii)%ssType.Eq.ss%otf .And.                              &
                  &       segList(ii)%segType.Eq.seg%circle) Then                !
                Include 'inc/startXML/segCircleXML.inc'
             End If
             !
             If (segList(ii)%ssType.Eq.ss%otf .And.                              &
                  &       segList(ii)%segType.Eq.seg%curve) Then                 !
                Include 'inc/startXML/segCurveXML.inc'
             End If
             !
             If (segList(ii)%ssType.Eq.ss%otf .And.                              &
                  &       segList(ii)%segType.Eq.seg%lissajous) Then             !
                Include 'inc/startXML/segLissajousXML.inc'
             End If
             !
             ! ***       end of OTF subscan
             If (ii.Eq.iiSegment .Or.                                            &
                  &  ii.Lt.iiSegment .And. segList(ii+1)%newSubscan) Then        !
                !
                Call pakoXMLwriteEndElement("RESOURCE","segments",               &
                     &                         error=errorXML)                   !
                !
                Call pakoXMLwriteEndElement("RESOURCE","subscan",                &
                     &                         error=errorXML)                   !
                inOtfSubscan = .False.
             End If
             !
          End If
          !
       End Do
       !
       Call pakoXMLwriteEndElement("RESOURCE","subscans",                        &
            &                         error=errorXML)                            !
       !
       !   
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'START '//commandToSave,messageText)
       !
    End If
    !
    Return
  End Subroutine writeXMLsubscanList
!!!
!!!
  Subroutine printSegmentList  (error)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical            :: error
    ! 
    !
    !D     Write (6,*) '   --> moduleSubscanList: SR printSegmentList '
    !D     Write (6,*) '       iiSegment: ', iiSegment
    !
    If (iiSegment.Ge.1) Then
       !
       Do ii = 1, iiSegment, 1
          Write (6,*) '  ii                ', ii
          Write (6,*) '     newSubscan     ', segList(ii)%newSubscan  
          Write (6,*) '     scanNumber     ', segList(ii)%scanNumber  
          Write (6,*) '     ssNumber       ', segList(ii)%ssNumber    
          Write (6,*) '     segNumber      ', segList(ii)%segNumber   
          Write (6,*) '     ssType         ', segList(ii)%ssType      
          Write (6,*) '     segType        ', segList(ii)%segType     
          Write (6,*) '     angleUnit      ', segList(ii)%angleUnit   
          Write (6,*) '     speedUnit      ', segList(ii)%speedUnit   
          Write (6,*) '     flagOn         ', segList(ii)%flagOn      
          Write (6,*) '     flagRef        ', segList(ii)%flagRef     
          Write (6,*) '     pStart         ', segList(ii)%pStart      
          Write (6,*) '     pEnd           ', segList(ii)%pEnd        
          Write (6,*) '     lengthOtf      ', segList(ii)%lengthOtf   
          Write (6,*) '     turnAngle      ', segList(ii)%turnAngle   
          Write (6,*) '     CPstart        ', segList(ii)%CPstart     
          Write (6,*) '     CPend          ', segList(ii)%CPend       
          Write (6,*) '     systemName     ', segList(ii)%systemName  
          Write (6,*) '     altOption      ', segList(ii)%altOption   
          Write (6,*) '     speedStart     ', segList(ii)%speedStart  
          Write (6,*) '     speedEnd       ', segList(ii)%speedEnd    
          Write (6,*) '     tSegment       ', segList(ii)%tSegment    
          Write (6,*) '     tRecord        ', segList(ii)%tRecord     
          Write (6,*) '     focusOffset    ', segList(ii)%focusOffset
          Write (6,*) '     directionFocus ', segList(ii)%directionFocus
       End Do
       !   
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'printSegmentList ',messageText)
       !
    End If
    !
    Return
  End Subroutine printSegmentList
!!!
!!!
  Subroutine listSegmentList  (error)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical            :: error
    Integer            :: ierr
    ! 
    !!    Call getCountSegments(nSegments)
    !
    Call pako_message(seve%t,'paKo',                                             &
         &    " module SubscanList, 1.2.3 2014-07-10 ---> SR: Subscanlist ")     ! !! Trace execution
    !
    !D     Write (6,*) '   --> moduleSubscanList: SR listSegmentList '
    !D     Write (6,*) '        nSegments: ',  nSegments
    !D     Write (6,*) '       iiSegment:  ', iiSegment
    !
    Write (messageText,*)  "  subscans #: 1 to ", nSegments
    l = Len_trim(GV%observingMode)
    Call pakoMESSAGE(priorityI,severityI,GV%observingMode(1:l),messageText)
    !
    If (iiSegment.Ge.1) Then
       !
       Do ii = 1, iiSegment, 1
          !
          If (segList(ii)%ssType .Eq. ss%otf) Then
             !
             Write (messageText, iostat=ierr, fmt =                              &
                     &  '(I3," ",  A, 2F9.1," to ", 2F9.1, " ", A," ",           &
                     &   A, F8.1," s")   '                                       &
                     &  )                                                        &
                  &  ii,                                                         &
                  &  segList(ii)%ssType,                                         &
                  &  segList(ii)%pStart%x/GV%angleFactorR,                       &
                  &  segList(ii)%pStart%y/GV%angleFactorR,                       &
                  &  segList(ii)%pEnd%x/GV%angleFactorR,                         &
                  &  segList(ii)%pEnd%y/GV%angleFactorR,                         &
                  &  GV%angleUnitSetC,                                           &
                  &  segList(ii)%systemName,                                     &
                  &  segList(ii)%tsegment                                        !
             If (ierr.Ne.0) Then
                Write (6,*) "         ierr:          ", ierr
                Write (6,*) "         messageText: ->", messageText, "<-"
                Write (messageText, *)                                           &
                  &  ii,                                                         &
                  &  segList(ii)%ssType,                                         &
                  &  segList(ii)%pStart%x/GV%angleFactorR,                       &
                  &  segList(ii)%pStart%y/GV%angleFactorR,                       &
                  &  segList(ii)%pEnd%x/GV%angleFactorR,                         &
                  &  segList(ii)%pEnd%y/GV%angleFactorR,                         &
                  &  GV%angleUnitSetC,                                           &
                  &  segList(ii)%systemName,                                     &
                  &  segList(ii)%tsegment                                        !
                Write (6,*) "         fallback * format:"
                Write (6,*) "         messageText: ->", messageText, "<-"
             End If                                        !
            !
          Else If (segList(ii)%ssType .Eq. ss%cs) Then
             Write (messageText,*)  "  subscan #:   ", ii," ",                   &
                  &  segList(ii)%ssType," ",                                     &
                  &  "at ",   segList(ii)%pStart%x/GV%angleFactorR," ",          &
                  &           segList(ii)%pStart%y/GV%angleFactorR," ",          &
                  &  GV%angleUnitSetC                                            !
             !
          Else If (segList(ii)%ssType.Eq.ss%ca .or.                              &
               &   segList(ii)%ssType .Eq. ss%cc) Then                           !
             If (segList(ii)%sideBand.Ne.GPnone) Then
                Write (messageText,*)  "  subscan #:   ", ii," ",                &
                     &  segList(ii)%ssType," ",                                  &
                     &  segList(ii)%sideBand                                     !
             Else
                Write (messageText,*)  "  subscan #:   ", ii," ",                &
                     &  segList(ii)%ssType                                       !
             End If
             !
          Else If (segList(ii)%ssType .Eq. ss%track) Then
             If (segList(ii)%flagOn) Then
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3," ", A,A, "at ",f9.1, f9.1," ", A," ",              &
                     &   A, F8.1," s")   '                                       &
                     &  )                                                        &
                     &           ii,                                             &
                     &           segList(ii)%ssType,traceflag%on,                &
                     &           segList(ii)%pStart%x/GV%angleFactorR,           &
                     &           segList(ii)%pStart%y/GV%angleFactorR,           &
                     &           GV%angleUnitSetC,                               &
                     &           segList(ii)%systemName,                         &
                     &           segList(ii)%tsegment                            !
             Else If (segList(ii)%flagRef) Then
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3," ", A,A, "at ",f9.1, f9.1," ", A," ",              &
                     &   A, F8.1," s")   '                                       &
                     &  )                                                        &
                     &           ii,                                             &
                     &           segList(ii)%ssType,traceflag%ref,               &
                     &           segList(ii)%pStart%x/GV%angleFactorR,           &
                     &           segList(ii)%pStart%y/GV%angleFactorR,           &
                     &           GV%angleUnitSetC,                               &
                     &           segList(ii)%systemName,                         &
                     &           segList(ii)%tsegment                            !
             End If
             !
          Else
             Write (messageText,*)  "  subscan #:   ", ii," ",                   &
                  &  segList(ii)%ssType                                          !
             !
          End If
          !
          l = Len_trim(GV%observingMode)
          Call pakoMESSAGE(priorityI,severityI,'Subscans',                       &
               &           messageText)                                          !
       End Do
       !
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'Subsans',messageText)
       !
    End If
    !
    Return
  End Subroutine listSegmentList
!!!
!!!
  Subroutine plotSegmentList  (error)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical            :: error
    ! 
    jj = 0             !   counts OTF segments
    !
    Call pako_message(seve%t,"",                                                 &
         &  " module SubscanList, v 1.2.3 2014-10-20 --> SR: plotSegmentList "   &
         &  //" DEPRECATED ")                                                    ! trace execution
    !
    Do ii = 1, nSegments, 1
       !
       !D        Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType,                  &
       !D             &          segList(ii)%flagOn, segList(ii)%flagRef,                  &
       !D             &          segList(ii)%flagTune                                      !
       !
       If (         segList(ii)%ssType .Eq.ss%track                              &
            & .And. segList(ii)%segType.Eq.seg%track                             &
            & .And. segList(ii)%flagOn) Then                                     !   !!   Track On
          !
          Call plotTrackP( segList(ii)%pStart,                                   &
               &           errorP,                                               &
               &           number=segList(ii)%ssNumber,                          &
               &           Type=ss%on)                                           !
          !
       End If
       !
       If (         segList(ii)%ssType .Eq.ss%track                              &
            & .And. segList(ii)%segType.Eq.seg%track                             &
            & .And. segList(ii)%flagRef) Then                                    !   !!   Flag Ref
          !
          Call plotTrackP( segList(ii)%pStart,                                   &
               &           errorP,                                               &
               &           number=segList(ii)%ssNumber,                          &
               &           Type=ss%ref)                                          !
          !
       End If
       !
       If (         segList(ii)%ssType .Eq.ss%ref                                &
            & .And. segList(ii)%segType.Eq.seg%track) Then                       !   !!   Track Ref
          !
          Call plotTrackP( segList(ii)%pStart,                                   &
               &           errorP,                                               &
               &           number=segList(ii)%ssNumber,                          &
               &           Type=segList(ii)%ssType)                              !
          !
       End If
       !
       If (         segList(ii)%ssType .Eq.ss%otf                                &
            & .And. segList(ii)%segType.Eq.seg%linear) Then                      !   !!   OTF linear
          !
          jj = jj+1
          Call plotOtfLinearP(segList(ii)%pStart,                                &
               &             segList(ii)%pEnd,                                   &
               &             errorP,                                             &
               &             number=jj,                                          &
               &             Type='otfMap')                                      !
          !
       End If
       !
    End Do
    !
!! TBD   !
!! TBD   Call gr_exec(      'set character 0.6' )
!! TBD   Call gr_exec(      'pen 0'             )
!! TBD   !
    !
    error =  errorP 
    !
    Return
  End Subroutine plotSegmentList
!!!
!!!
  Subroutine getNumberSegments(ii)
    !
    Integer, Intent(out) :: ii
    !
    ii = iiSegment
    !
    Return
    !
  End Subroutine getNumberSegments
!!!
!!!
  Subroutine getCountSubscans(ii)
    !
    Integer, Intent(out) :: ii
    !
    ii = iiSubscan
    !
    Return
  End Subroutine getCountSubscans
!!!
!!!
  Subroutine getCountSegments(ii)
    !
    Integer, Intent(out) :: ii
    !
    ii = iiSegment
    !
    Return
  End Subroutine getCountSegments
!!!
!!!
  Subroutine countSubscans(ii,value)
    !
    Integer, Intent(in), Optional  :: value
    Integer, Intent(out), Optional :: ii
    !
    If (Present(value)) Then
       iiSubscan = value
    Else
       iiSubscan = iiSubscan+1
    End If
    !
    If (Present(ii)) Then
       ii = iiSubscan
    End If
    !
    Return
  End Subroutine countSubscans
!!!
!!!
  Subroutine countSegments(ii,value)
    !
    Integer, Intent(in), Optional  :: value
    Integer, Intent(out), Optional :: ii
    !
    If (Present(value)) Then
       iiSegment = value
    Else
       iiSegment = iiSegment+1
    End If
    !
    If (Present(ii)) Then
       ii = iiSegment
    End If
    !
    Return
  End Subroutine countSegments
!!!
!!!
End Module modulePakoSubscanList
!!!
!!!
