!
!--------------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoDIYlist.f90">
!   <VERSION>
!                Id: modulePakoDIYlist.f90,v 1.3.0  2017-10-05    Hans Ungerechts  !! obsolete setPrivilege%GISMO
!                Id: modulePakoDIYlist.f90,v 1.2.3  2014-12-15    Hans Ungerechts  
!                Id: modulePakoDIYlist.f90,v 1.2.3  2014-10-21    Hans Ungerechts  
!                Id: modulePakoDIYlist.f90,v 1.2.3  2014-09-29    Hans Ungerechts  
!                Id: modulePakoDIYlist.f90,v 1.2.3  2014-07-11    Hans Ungerechts  
!                Id: modulePakoDIYlist.f90,v 1.2.3  2014-05-19    Hans Ungerechts  
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-11-06    Hans Ungerechts  !!  allow Lissajous segment without ramp
!                                                                                  !!  depending variable %doNextSubscan
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-10-29    Hans Ungerechts  !!  make next (new)subscan optional
!                                                                                  !!  depending variable %doNextSubscan
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-09-05    Hans Ungerechts  !!  internal ramp
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-06-25,26 Hans Ungerechts  !!  internal ramp
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-06-24,25 Hans Ungerechts  !!  ramp down
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-06-20,24 Hans Ungerechts  !!  very loose
!                Id: modulePakoDIYlist.f90,v 1.2.3  2013-05-02 Hans Ungerechts
!                Id: modulePakoDIYlist.f90,v 1.2.2  2013-02-08 Hans Ungerechts
!                Id: modulePakoDIYlist.f90,v 1.2.2  2013-01-24 Hans Ungerechts
!                based on:
!                Id: modulePakoDIYlist.f90,v 1.1.14 2012-11-20 Hans Ungerechts
!   </VERSION>
!   <HISTORY>
!                2010-04-08 HU: force safe limit on maximum elevation
!                          for GISMO run, based on TT-NCS           
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>

!   </SIBLINGS>        
!
!   Pako module for command: DIY SUBSCAN
!
! </DOCUMENTATION> <!-- name="modulePakoDIYlist.f90" -->
!
!--------------------------------------------------------------------------------
!
Module modulePakoDIYlist
  !
  Use modulePakoMessages
  Use modulePakoTypes
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
!!$  Use modulePakoSwBeam
!!$  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  Public :: DIYlist
  Public :: Subscan
  Public :: saveSubscan
  Public :: saveDIYlist
  Public :: startDIYlist
  Public :: displayDIYlist
  Public :: listDIYlist
  Public :: plotDIYlist
  !     
  ! *** Variables for DIYlist ***
  Include 'inc/variables/variablesDIYlist.inc'
!!!   
!!!   
Contains
!!!
!!!
  Subroutine DIYlist(programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
!!OLD
!!OLD    Logical               :: isConnected   = .False.
!!OLD    Character (len=lenCh) :: bolometerName
!!OLD
!!OLD    Integer                :: iQuery
!!OLD    Character(len=lenCh)   :: nameQuery
!!OLD    Logical                :: isSet
!!OLD    Type(xyPointType)      :: pointResult
    !
    Include 'inc/ranges/rangesDIYlist.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module DIYlist, 1.2.3 2013-07-11 ---> SR: DIYlist ")             ! trace execution
    !
    !D    Write (6,*) " module DIYlist ---> SR: DIYlist "
    !D    Write (6,*) "                          "
    !
    ! *** initialize ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    !     read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    Call checkNofParameters(command,                                             &
         &     0, 0,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    Call queryReceiver(rec%Bolo, isConnected, bolometerName = bolometerName)
    !
    !D     Write (6,*) "      rec%Bolo, isConnected: ", rec%Bolo, isConnected
    !D     Write (6,*) "      bolometerName:         ", bolometerName
    !D     Write (6,*) "      bolo%NIKA:             ", bolo%NIKA
    !
    If (   .Not.                                                                 &
         & (                                                                     &
!!$         &       GV%privilege.Eq.setPrivilege%GISMO                              &
         &       GV%privilege.Eq.setPrivilege%ncsTeam                            &
         &  .Or. isConnected.And.bolometerName.Eq.bolo%GISMO                     &
         &  .Or. isConnected.And.bolometerName.Eq.bolo%NIKA                      &
         & )    ) Then                                                           !
       ERROR = .True.
       Write (messageText, *) " requires privilege"
       Call pakoMessage(priorityE,severityE,command,                             &
            &           messageText)                                             !
    End If
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/options/readOptionsDIYlist.inc'
    End If
    !
    ! *** set defaults ***
    Include 'inc/options/optionDefaults.inc'
    !
    ! *** read Parameters, Options (again, after defaults!) ***
    !     and check for validity and ranges 
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/options/readOptionsDIYlist.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    !D     Write (6,*) "      vars(iIn)%hasPurpose:   ", vars(iIn)%hasPurpose
    !D     Write (6,*) "      vars(iIn)%purpose:    ->", vars(iIn)%purpose,     "<-"
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.error) Then
       !
       errorInconsistent = .False.
       !
    End If
    !
    If (.Not. errorInconsistent) Then
       vars(iTemp) = vars(iIn)
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue)    = vars(iTemp)
    End If
    !
    !D     Write (6,*) "      vars(iValue)%hasPurpose:   ", vars(iValue)%hasPurpose
    !D     Write (6,*) "      vars(iValue)%purpose:    ->", vars(iValue)%purpose,     "<-"
    !
    If (.Not.error) Then
       GV%observingMode     = OM%DIY
       GV%observingModePako = OMpako%DIY
       GV%purpose           = vars(iValue)%purpose
       GV%purposeIsSet      = vars(iValue)%hasPurpose
       !D        Write (6,*) "   GV%observingMode:     ", GV%observingMode
       !D        Write (6,*) "   GV%observingModePako: ", GV%observingModePako
       !D        Write (6,*) "   GV%purposeIsSet:   ", GV%purposeIsSet
       !D        Write (6,*) "   GV%purpose:      ->", GV%purpose, "<-"
    End If
    !
!!$!!OLD    !
!!$!!OLD    ! *** set "selected" observing mode & plot ***
!!$!!OLD    If (.Not.error) Then
!!$!!OLD       GV%observingMode     = OM%DIYlist
!!$!!OLD       GV%observingModePako = OMpako%DIYlist
!!$!!OLD       GV%omSystemName      = vars(iValue)%systemName 
!!$!!OLD       GV%notReadySWafterOM = .False.
!!$!!OLD       GV%notReadySecondaryRafterOM = .False.
!!$!!OLD       !D       Write (6,*) '      GV%notReadySecondaryRafterOM: ',                       &
!!$!!OLD       !D            &             GV%notReadySecondaryRafterOM
!!$!!OLD       GV%notReadyOffsetsAfterDIYlist  = .False.
!!$!!OLD       GV%notReadyDIYlist     = .False.
!!$!!OLD       DIYlistCommand         = line(1:lenc(line))
!!$!!OLD       Call analyzeDIYlist (errorA)
!!$!!OLD       Call listSegmentList (errorA)
!!$!!OLD       Call plotOMDIYlist (errorP)
!!$!!OLD    End If
!!$    !
!!$!!OLD    Call printSegmentList (error)
    !
    Call getCountSegments(nSegments)
    !
    ! *** display values ***
    ! 
    If (.Not. ERROR) Then
       Call displayDIYlist
       Call listDIYlist (error)
       Call plotDIYlist (error)
    End If
    !
    Return
  End Subroutine DIYlist
!!!
!!!
  Subroutine Subscan(programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    ! *** special working variables:   ***
    Logical               :: optionSpeedIsPresent, optionTotfIsPresent
    Logical, Dimension(3) :: alternative
    Real                  :: vIn, vStdMin, vStdMax, vLimitMin, vLimitMax
    !
    Real                  :: vLissMaxX,   vLissMaxY,   aLissMaxX,   aLissMaxY,   vLissLimit, aLissLimit
    Real                  :: vLissFacX,   vLissFacY,   aLissFacX,   aLissFacY
    Real                  :: vLissMaxXel, vLissMaxYel, aLissMaxXel, aLissMaxYel, lissMaxEl
    !
    Real                  :: vMaxX,   vMaxY,   vLimit
    Real                  ::                                     vLimitAz, vLimitEl
    Real                  :: vMaxXel, vMaxYel, MaxEl
    !
    Character(len=lenCh)   ::  sysName  = GPnone
    !
!!OLD    Type(conditionMinMax)  ::  condElevation                                         &
!!OLD         &                     = conditionMinMax("elevation",                        &
!!OLD         &                                        0.0, Pi/2.0,                       &
!!OLD         &                                       "rad",                              &
!!OLD         &                                       .False.         )                   ! 
    !
    Integer               :: iErr
    !
    optionSpeedIsPresent = .False.
    optionTotfIsPresent  = .False.
    alternative          = .False.
    !
    Include 'inc/ranges/rangesDIYlist.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module DIYlist, 1.2.3 2013-06-25 ---> SR: Subscan ")             ! trace execution
    !
    !D     Write (6,*) " module DIYlist ---> SR: Subscan "
    !D     Write (6,*) "                          "
    !
    ! *** initialize ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    !     read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    Call checkNofParameters(command,                                             &
         &     0, 8,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    Call queryReceiver(rec%Bolo, isConnected, bolometerName = bolometerName)
    !
    If (   .Not.                                                                 &
         & (                                                                     &
!!$         &       GV%privilege.Eq.setPrivilege%GISMO                              &
         &       GV%privilege.Eq.setPrivilege%ncsTeam                            &
         &  .Or. isConnected.And.bolometerName.Eq.bolo%GISMO                     &
         &  .Or. isConnected.And.bolometerName.Eq.bolo%NIKA                      &
         & )    ) Then                                                           !
       ERROR = .True.
       Write (messageText, *) " requires privilege"
       Call pakoMessage(priorityE,severityE,command,                             &
            &           messageText)                                             !
    End If
    !
    ! *** check for alternative Options   ***
    If (.Not. ERROR) Then
       Include 'inc/options/alternativeSpeedTotf.inc'
    End If
    !
    Include 'inc/options/optionSubscanType.inc'
    !
    !D     Write (6,*) "   after optionSubscanType: "
    !D     Write (6,*) "         vars(iIn)%doTrack         ", vars(iIn)%doTrack        
    !D     Write (6,*) "         vars(iIn)%doLinearOtf     ", vars(iIn)%doLinearOtf    
    !D     Write (6,*) "         vars(iIn)%doLissajousOtf  ", vars(iIn)%doLissajousOtf 
    !D     Write (6,*) "         vars(iIn)%segType         ", vars(iIn)%segType
    !D     !
    !D     Write (6,*) "      nArguments: ", nArguments
    !D     Write (6,*) " "
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersSegment.inc'
       Include 'inc/options/readOptionsSubscan.inc'
    End If
    !
    ! *** set defaults ***
    Include 'inc/options/optionDefaults.inc'
    !
    ! *** read Parameters, Options (again, after defaults!) ***
    !     and check for validity and ranges 
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersSegment.inc'
       Include 'inc/options/readOptionsSubscan.inc'
       Include 'inc/options/alternativeSpeedTotfSet.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    !D     Write (6,*) "        -> vars(iIn)%doTune:   ", vars(iIn)%doTune
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.error) Then                                                           !! check consistency
       !
       !! condition for linear OTF, compare POINTING, OTFAMP. <-- 2012-10-30
       !
       errorInconsistent = .False.
       !
       !D        Write (6,*) "              vars(iIn)%doTrack:         ", vars(iIn)%doTrack
       !D        Write (6,*) "              vars(iIn)%doLinearOtf:     ", vars(iIn)%doLinearOtf
       !D        Write (6,*) "              vars(iIn)%doLissajousOtf:  ", vars(iIn)%doLissajousOtf
       !D        Write (6,*) "              vars(iIn)%doTune:          ", vars(iIn)%doTune
       !
       If      (      (vars(iIn)%doLinearOtf .Or. vars(iIn)%doLissajousOtf)      &
            &    .And. vars(iIn)%doTune                                          &
            &  ) Then                                                            !
          !
          errorInconsistent = .True.
          !
          Write (messageText, *) "/Tune only works with track subscans "
          Call pakoMESSAGE(priorityE,severityE,command,messageText)
          !
       End If
       !
       If (vars(iIn)%doLissajousOtf) Then
          !
          vLissMaxX  =   vars(iIn)%xAmplitude*GV%angleUnit * vars(iIn)%omegaX
          vLissMaxY  =   vars(iIn)%yAmplitude*GV%angleUnit * vars(iIn)%omegay
          !
          aLissMaxX  =   vars(iIn)%xAmplitude*GV%angleUnit * vars(iIn)%omegaX**2
          aLissMaxY  =   vars(iIn)%yAmplitude*GV%angleUnit * vars(iIn)%omegay**2
          !
          vLissLimit = 300.0*arcsec
          aLissLimit = 108.0*arcsec
          !
          !D           Write (6,*) "      vLissMaxX:  ", vLissMaxX
          !D           Write (6,*) "      vLissMaxY:  ", vLissMaxY
          !D           Write (6,*) "      aLissMaxX:  ", aLissMaxX
          !D           Write (6,*) "      aLissMaxY:  ", aLissMaxY
          !D           Write (6,*) "      vLissLimit: ", vLissLimit
          !D           Write (6,*) "      aLissLimit: ", aLissLimit
          !
          vLissFacX  = vLissMaxX/vLissLimit
          vLissFacY  = vLissMaxY/vLissLimit
          aLissFacX  = aLissMaxX/aLissLimit
          aLissFacY  = aLissMaxY/aLissLimit
          !
          !!          vLissLimit = Min(vLissLimit,300.0*arcsec)
          !!          aLissLimit = Min(aLissLimit,108.0*arcsec)
          !
          If (vLissMaxX.Gt.vLissLimit) Then
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("x max. speed = xAmplitude [",    F12.1,                  &
                  &    " ] * frequencyX [",            F12.4,                    &
                  &    " ] is too large by factor: ",  F12.3 )' )                &
                  & vars(iIn)%xAmplitude,                                        &
                  & vars(iIn)%frequencyX,                                        &
                  & vLissFacX                                                    !
             Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("to correct his, try: frequencyX  ", F12.4                &
                  &                                          )' )                &
                  & 0.999*vars(iIn)%frequencyX/(vLissFacX)                       !
             Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
             errorInconsistent = .True.
          End If
          !
          If (vLissMaxY.Gt.vLissLimit) Then
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("y max. speed = yAmplitude [",    F12.1,                  &
                  &    " ] * frequencyY [",            F12.4,                    &
                  &    " ] is too large by factor: ",  F12.3 )' )                &
                  & vars(iIn)%yAmplitude,                                        &
                  & vars(iIn)%frequencyY,                                        &
                  & vLissFacY                                                    !
             Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("to correct his, try: frequencyY  ", F12.4                &
                  &                                          )' )                &
                  & 0.999*vars(iIn)%frequencyY/(vLissFacY)                       !
             Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
             errorInconsistent = .True.
          End If
          !
          If (aLissMaxX.Gt.aLissLimit) Then
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("x max. acceleration = xAmplitude [",  F12.1,             &
                  &    " ] * frequencyX [",                 F12.4,               &
                  &    " ] ^2 is too large by factor: ",    F12.3 )' )           &
                  & vars(iIn)%xAmplitude,                                        &
                  & vars(iIn)%frequencyX,                                        &
                  & aLissFacX                                                    !
             Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("to correct his, try: frequencyX  ", F12.4                &
                  &                                          )' )                &
                  & 0.999*vars(iIn)%frequencyX/sqrt(aLissFacX)                   !
             Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
             errorInconsistent = .True.
          End If
          !
          If (aLissMaxY.Gt.aLissLimit) Then
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("y max. acceleration = yAmplitude [",  F12.1,             &
                  &    " ] * frequencyY [",                 F12.4,               &
                  &    " ] ^2 is too large by factor ",     F12.3 )' )           &
                  & vars(iIn)%yAmplitude,                                        &
                  & vars(iIn)%frequencyY,                                        &
                  & aLissFacY                                                    !
             Call pakoMESSAGE(priorityE,severityE,"DIY",messageText)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("to correct his, try: frequencyY  ", F12.4                &
                  &                                          )' )                &
                  & 0.999*vars(iIn)%frequencyY/sqrt(aLissFacY)                   !
             Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
             errorInconsistent = .True.
          End If
          !
          !D   Write (6,*) "   vars(iIn)%doRampUp   ", vars(iIn)%doRampUp
          !D   Write (6,*) "   vars(iIn)%doRampDown ", vars(iIn)%doRampDown
          !
          If (.Not.vars(iIn)%doRampUp) Then
             Write (messageText,*) "/ramp up [tRamp] is RECOMMENDED for Lissajous"
             Call pakoMESSAGE(priorityW,severityW,"SUBSCAN",messageText)
!!$             errorInconsistent = .True.
          End If
!!$          If (.Not.vars(iIn)%doRampUp) Then
!!$             Write (messageText,*) "/ramp up [tRamp] is REQUIRED for Lissajous"
!!$             Call pakoMESSAGE(priorityE,severityE,"SUBSCAN",messageText)
!!$             errorInconsistent = .True.
!!$          End If
          !
          !  ** calculate maximum allowed elevation:
          !
          If (.Not.errorInconsistent) Then
             !
             vLissMaxXel = Acos(vLissMaxX/(2*vLissLimit))
             vLissMaxYel = Acos(vLissMaxY/(2*vLissLimit))
             !
             aLissMaxXel = Acos(aLissMaxX/(2*aLissLimit))  
             aLissMaxYel = Acos(aLissMaxY/(2*aLissLimit))  
             !
             If (         vars(iIn)%systemName.Eq.offsPako%tru                   &
                  &  .Or. vars(iIn)%systemName.Eq.offsPako%hor) Then             !
                lissMaxEl   = min(vLissMaxXel,aLissMaxXel)                         !! use only "x" offsets (Azimuth) for el. condition
             Else
                lissMaxEl   = min(vLissMaxXel,vLissMaxYel,                       &
                     &            aLissMaxXel,aLissMaxYel)                       !
             End If
             !
             !D              Write (6,*) " max el vLissMaxXel: ", vLissMaxXel/deg
             !D              Write (6,*) " max el vLissMaxYel: ", vLissMaxYel/deg
             !D              Write (6,*) " max el aLissMaxXel: ", aLissMaxXel/deg
             !D              Write (6,*) " max el aLissMaxYel: ", aLissMaxYel/deg
             !                                                                            
             !D              Write (6,*) " max el  lissMaxEl:  ", lissMaxEl/deg
             !
             ! 2010-04-08 HU: force safe limit on maximum elevation
             !                for GISMO run, based on TT-NCS
!!OLD             If      (GV%limitCheck.eq.setLimitCheck%loose) Then                 !! loose:   beyond limits for good tracking
!!OLD                lissMaxEl = lissMaxEl+5.0*deg
!!OLD             Else 
!!OLD             If (GV%limitCheck.eq.setLimitCheck%relaxed) Then                    !! relaxed: limits for good tracking
!!OLD                lissMaxEl = lissMaxEl
!!OLD             Else                                                                !! strict:  safe
             lissMaxEl = lissMaxEl-2.0*deg
!!OLD             End If
             !
             lissMaxEl   = min(lissMaxEl,85.0*deg)
             !D              Write (6,*) "  "
             !D              Write (6,*) " max el  lissMaxEl:  ", lissMaxEl/deg
             !
             !!             If (lissMaxEl.Ge.60.0*deg) Then
             !
             If (condElevation%isSet) Then                                         !! maximum elevation: minimum over subsans
                condElevation%maximum   = min(condElevation%maximum,lissMaxEl)
             Else
                condElevation%isSet     = .True.
                condElevation%maximum   = lissMaxEl
             End If
             !
             !D  Write (6,*) " condElevation%isSet:   ", condElevation%isSet
             !D  Write (6,*) " condElevation%maximum: ", condElevation%maximum/deg
             !
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("WARNING--CONDITION: Elevation must be less than ",       &
                  &     F10.2,                                                   &
                  &    " [deg] ")' )                                             &
                  & condElevation%maximum/deg                                    !
             Call pakoMESSAGE(priorityE+4,severityW,"DIY-SUBSCAN",messageText)
             !
          End If
             !
       End If   !!   vars(iIn)%doLissajousOtf
       !
       !
    End If   !!   .Not.error
    !
    If (.Not. errorInconsistent) Then
       vars(iTemp) = vars(iIn)
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** conclude alternative options   ***
    If (.Not.error) Then
       Include 'inc/options/alternativeSpeedTotfEnd.inc'
    End If
    !
    !D     Write (6,*) "   after alternativeSpeedTotfEnd.inc: "
    !D     Write (6,*) "         vars(iTemp)%doTrack         ", vars(iTemp)%doTrack        
    !D     Write (6,*) "         vars(iTemp)%doLinearOtf     ", vars(iTemp)%doLinearOtf    
    !D     Write (6,*) "         vars(iTemp)%doLissajousOtf  ", vars(iTemp)%doLissajousOtf 
    !D     Write (6,*) "         vars(iTemp)%segType         ", vars(iTemp)%segType
    !
    If (.Not. error) Then
       !
       errorInconsistent = .False.
       !
       If (vars(iIn)%doLinearOtf) Then
          !
          !D Write (6,*) "         -> will determine conditions for Linear OTF "
          !
          !! --> 2012-03-25:
          !
          If (         vars(iTemp)%systemName.Eq.offsPako%tru                    &
               &  .Or. vars(iTemp)%systemName.Eq.offsPako%hor) Then              !
             !
             vMaxX  =   Abs(vars(iTemp)%pEnd%x-vars(iTemp)%pStart%x)             &
                  &        /vars(iTemp)%tOtf                                     !
             vMaxY  =   Abs(vars(iTemp)%pEnd%y-vars(iTemp)%pStart%y)             &
                  &        /vars(iTemp)%tOtf                                     !
             !
          Else
             vMaxX  =   max(vars(iTemp)%speedStart, vars(iTemp)%speedEnd)
             vMaxY  =   max(vars(iTemp)%speedStart, vars(iTemp)%speedEnd)
          End If
          !
          !! NOTE:  code from here --> copied on 2012-05-30 from OTFMAP (2012-03-28)
          !! TBD:   extract this code, also use in OTFMAP, SUBSCAN OTF LINEAR
          !!        --> define / use local variable sysName in OTFMAP, SUBSCAN OTF LINEAR
          !! TBD:   long-term: ramp-up!
          !                        !!  speed limit az. axis, el. axis
          vLimit   = 200.0         !!  based on tests 2012-03-15 to -20
          !                                 
          !
          If      (     GV%privilege.Eq.setPrivilege%ncsTeam                     &
               &  .Or.  GV%userLevel.Eq.setUserLevel%experienced                 &
               &  ) Then                                                         !
             If (GV%limitCheck.Eq.setLimitCheck%relaxed) vLimit = 220.0
          End If
          If      (     GV%privilege.Eq.setPrivilege%ncsTeam                     &
               &  ) Then                                                         !
             If (GV%limitCheck.Eq.setLimitCheck%loose)   vLimit = 1200.0           !!  for tests 2013
!!normal:    If (GV%limitCheck.Eq.setLimitCheck%loose)   vLimit =  300.0           !!  normal
             If (GV%limitCheck.Eq.setLimitCheck%relaxed) vLimit =  220.0
          End If
          !
          vLimitAz = vLimit        !!  speed limit az axis
          vLimitEl = vLimit        !!  speed limit el axis
          !
          !! aLimit = 108.0*arcsec
          !
          If (GV%doDebugMessages) Then
             Write (6,*) " vMaxX:     ", vMaxX
             Write (6,*) " vMaxY:     ", vMaxY
             Write (6,*) " vLimit:    ", vLimit
             Write (6,*) " vLimitAz:  ", vLimitAz
             Write (6,*) " vLimitEl:  ", vLimitEl
          End If
          !
          sysName = vars(iTemp)%systemName
          !
          !! NOTE: using local variable sysName instead of vars(iTemp)%systemName:
          If (         sysName.Eq.offsPako%tru                                   &  
               &  .Or. sysName.Eq.offsPako%hor) Then                             !
             !
             If (vMaxX.Le.vLimitAz) Then
                vMaxXel = Acos(vMaxX/(vLimitAz))
             Else
                errorInconsistent = .true.
                Write (messageText,*) "Azimuth speed ", vMaxX,                   &
                     &                " too large for safe tracking"             !
                Call PakoMessage(priorityE,severityE,command,messageText)
             End If
             !
             If (vMaxY.Le.vLimitEl) Then
                vMaxYel = 90.0*deg
             Else
                errorInconsistent = .true.
                Write (messageText,*) "Elevation speed ", vMaxY,                 &
                     &                " too large for safe tracking"             !
                Call PakoMessage(priorityE,severityE,command,messageText)
             End If
             !
          Else
             !
             If (vMaxX.Le.vLimit .And. vMaxY.Le.vLimit) Then
                vMaxXel = Acos(vMaxX/(vLimit))
                vMaxYel = Acos(vMaxY/(vLimit))
             Else
                errorInconsistent = .true.
                Write (messageText,*) "Speed ", max(vMaxX,vMaxY),                &
                     &                " too large for safe tracking"             !
                Call PakoMessage(priorityE,severityE,command,messageText)
             End If
             !
          End If
          !
          If (GV%doDebugMessages) Then
             Write (6,*) " max el vMaxXel: ", vMaxXel/deg
             Write (6,*) " max el vMaxYel: ", vMaxYel/deg
          End If
          !
          !! <<-- 2012-03-25
          !
          !! aMaxXel = Acos(aMaxX/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
          !! aMaxYel = Acos(aMaxY/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
          !
          !D           Write (6,*) " max el  MaxEl:  ", MaxEl/deg
          !
          ! 2010-04-08 HU: force safe limit on maximum elevation
          !                for GISMO run, based on TT-NCS
          !
          If      (     GV%privilege.Eq.setPrivilege%ncsTeam                     & !! very loose for tests 2013
               &   .And.                                                         &
               &        GV%limitCheck.Eq.setLimitCheck%loose) Then               !
             !
             MaxEl  =  min(vMaxXel,vMaxYel)
             MaxEl  =  MaxEl-0.0*deg
             MaxEl  =  max(0.0*deg,MaxEl) 
             MaxEl  =  min(MaxEl,89.0*deg)
             !
          Else                                                                     !! normal limits
             !
             MaxEl = min(vMaxXel,vMaxYel)
             MaxEl  =  MaxEl-2.0*deg
             MaxEl  =  max(0.0*deg,MaxEl) 
             MaxEl  =  min(MaxEl,85.0*deg)
             !
          End If
          !
          Write (6,*) " max el  MaxEl:  ", MaxEl/deg
          !
          !
          !!             If (MaxEl.Ge.60.0*deg) Then
          !
          If (condElevation%isSet) Then                                            !! maximum elevation: minimum over subsans
             condElevation%maximum   = min(condElevation%maximum,MaxEl)
          Else
             condElevation%isSet     = .True.
             condElevation%maximum   = MaxEl
          End If
          !
          If (GV%doDebugMessages) Then
             Write (6,*) " condElevation%isSet:   ", condElevation%isSet
             Write (6,*) " condElevation%maximum: ", condElevation%maximum/deg
          End If
          !
          !! NOTE =: <-- copied on 2012-05-30 from OTFMAP (2012-03-28)
          !
          Write (messageText, fmt =                                              &
               &  '("WARNING--CONDITION: Elevation must be less than ",          &
               &     F10.2,                                                      &
               &    " [deg] ")' )                                                &
               & condElevation%maximum/deg                                       !
          Call pakoMESSAGE(priorityE+4,severityW,"POINTING",messageText)
          !
       End If   !!   vars(iIn)%doLinearOtf
       !
    End If   !!   .Not. error
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue)    = vars(iTemp)
    End If
    !
    !D     Write (6,*) "        -> vars(iValue)%doTune:   ", vars(iValue)%doTune
    !
    ! *** display values ***
    ! 
    ! *** 
    If (.Not.error) Then
       !
       If (vars(iValue)%doLinearOtf) Then
          If (vars(iValue)%doRampUp) Then
             Write (messageText,*)  "Up ", vars(iValue)%tRampUp, " [sec]"
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
          If (vars(iValue)%doRampDown) Then
             Write (messageText,*)  "Down ", vars(iValue)%tRampDown, " [sec]"
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
          If (.Not.(vars(iValue)%doRampUp.Or.vars(iValue)%doRampDown)) Then
             Write (messageText,*)  ramp%none
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
       End If
       !
       If (vars(iValue)%doLissajousOtf) Then
          If (vars(iValue)%doRampUp) Then
             Write (messageText,*)  "Up ", vars(iValue)%tRampUp, " [sec]"
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
          If (vars(iValue)%doRampDown) Then
             Write (messageText,*)  "Down ", vars(iValue)%tRampDown, " [sec]"
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
          If (.Not.(vars(iValue)%doRampUp.Or.vars(iValue)%doRampDown)) Then
             Write (messageText,*)  ramp%none
             Call pakoMessage(priorityI,severityI,command//"/"//"RAMP",messageText)    
          End If
       End If
       !
       GV%condElevation     = condElevation
       !D        Write (6,*) "GV%condElevation: ", GV%condElevation
       !
       GV%omSystemName      = vars(iValue)%systemName 
       GV%notReadySWafterOM = .False.
       GV%notReadySecondaryRafterOM = .False.
       SubscanCommand         = line(1:lenc(line))
       Call analyzeSubscan (errorA)
       error = error.Or.errorA
    End If
    !
    If (.Not. error) Then
       Call displaySubscan
    End If
    !
    Return
  End Subroutine Subscan
!!!
!!!
  Subroutine analyzeSubscan (errorA)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer                :: nScan 
    Character (len=lenCh)  :: aUnit, sUnit
    Integer                :: iSS           = 0     ! counts subcans/segment
    !
    Integer                :: iQuery
    Character(len=lenCh)   :: nameQuery
    Logical                :: isSet
    Type(xyPointType)      :: pointResult
    !
    Real                   :: delta = 0.1499850     !!
    !
    Integer                :: nSub, nSeg
    !
    errorA    = .False.
    !
!!$    Call pako_message(seve%t,'paKo',                                             &
!!$         &    " module DIYlist, 1.2.2 2013-01-22 ---> SR: analyzeSubscan ")      ! trace execution
!!$    Call pako_message(seve%t,'paKo',                                             &
!!$         &    " module DIYlist, 1.2.3 2013-04-29 ---> SR: analyzeSubscan ")      ! trace execution
    Call pako_message(seve%t,'paKo',                                             &
         &    " module DIYlist, 1.2.3 2013-10-29 ---> SR: analyzeSubscan ")      ! trace execution
    !
    ii        = 0
    jj        = 0
    iSS       = 1
    nScan     = 1111
    aUnit     = GV%angleUnitC                        ! 
    sUnit     = GV%speedUnitC                        ! 
    !
    Call getCountSubscans(nSub)
    !D     Write (6,*) "     nSub:   ", nSub
    If (nSub.Ge.99) Then
       errorA    = .True.
       Write (messageText,*)  " too many subscans. maximum: ", 99
       Call pakoMESSAGE(priorityE,severityE,"Subscan",messageText)
    End If
    !
    !D     Write (6,*) "     nLissajous:   ", nLissajous
    !
    Call getCountSegments(nSeg)
    !D     Write (6,*) "     nSeg:   ", nSeg
    !
    If (vars(iValue)%segType.Eq.seg%lissajous) Then
       !
       If (   .Not.                                                              &
            &      (     .False.                                                 &
!! Test     &       .Or. GV%privilege.Eq.setPrivilege%GISMO                      &
            &       .Or. GV%privilege.Eq.setPrivilege%ncsTeam                    &
            &      )                                                             &
            & .And.                                                              &
            &      nLissajous.Ge.1 ) Then                                        !
          errorA = .True.
          Write (messageText, *)                                                 &
               &       "more than 1 Lissajous subscan requires privilege"        !
          Call pakoMessage(priorityE,severityE,"Subscan",messageText)
          Write (messageText, *)                                                 &
               &       "enter DIY /CLEAR first "                                 !
          Call pakoMessage(priorityI,severityI,"Subscan",messageText)
       End If
       !
       If (nSegments.Ge.99-1) Then   !!  99-1 because Lissajous makes 2 segments
          errorA    = .True.
          Write (messageText,*)  " too many segments. maximum: ", 99
          Call pakoMESSAGE(priorityE,severityE,"Subscan",messageText)
       End If
       !
    Else
       ! 
       If (nSegments.Ge.99) Then
          errorA    = .True.
          Write (messageText,*)  " too many segments. maximum: ", 99
          Call pakoMESSAGE(priorityE,severityE,"Subscan",messageText)
       End If
       !
    End If
    !
    If (vars(iValue)%segType.Eq.seg%linear) Then
       !
       If (   .Not.                                                              &
            &      (     .False.                                                 &
!!OLD       &       .Or. GV%privilege.Eq.setPrivilege%GISMO                      &
            &       .Or. GV%privilege.Eq.setPrivilege%ncsTeam                    &
            &       .Or. isConnected.And.bolometerName.Eq.bolo%NIKA              &
!!OLD       &       .Or. GV%privilege.Eq.setPrivilege%staff                      &
            &      )                                                             &
            &      ) Then                                                        !
          errorA = .True.
          Write (messageText, *)                                                 &
               &       ss%otf , " ", seg%linear, " subscan requires privilege"   !
          Call pakoMessage(priorityE,severityE,"Subscan",messageText)
       End If
       !
    End If
    !
    If (.Not. errorA) Then
       !
       !!  ** 
       Call countSubscans(ii)
!!$       Call countSegments(jj)
       !
       !D        Write (6,*) "       ii: ", ii
       !D        Write (6,*) "       jj: ", jj
       !D        Write (6,*) "  segType: ", vars(iValue)%segType
       !
       ! TBD: calibrate subscans 
       !
       ! TBD: segment parameters to defaults
       !
       If (vars(iValue)%segType.Eq.seg%track) Then
          !
          Call countSegments(jj)
          !
          segList(jj)%newSubscan =  .True.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  1
          segList(jj)%ssType     =  ss%track
          segList(jj)%segType    =  seg%track
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
          If      (vars(iValue)%croFlag.Eq."O") Then
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          Else If (vars(iValue)%croFlag.Eq."R") Then
             segList(jj)%flagOn     =  .False.
             segList(jj)%flagRef    =  .True.
          Else
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          End If
          !
          segList(jj)%flagDropin    =  .False.
          !
          segList(jj)%flagTune      =  vars(iIn)%doTune
          !
          segList(jj)%pStart     =  vars(iValue)%offset
          segList(jj)%pEnd       =  segList(jj)%pStart
          segList(jj)%speedStart =  0.0
          segList(jj)%speedEnd   =  0.0
          segList(jj)%systemName =  vars(iValue)%systemName
          segList(jj)%altOption  =  'tSegment'
          segList(jj)%tSegment   =  vars(iValue)%tSubscan
          segList(jj)%tRecord    =  0.1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !
          If (jj.Eq.1) Then
             segList(jj)%updateDoppler = .True.
             If (vars(iValue)%systemName.Eq.offs%pro) Then
                segList(jj)%pDoppler      = vars(iValue)%offset
                segList(jj)%pDoppler      = xyPointType(0.0,0.0)        !  TBD: this is only a quick fix
             Else
                segList(jj)%pDoppler      = xyPointType(0.0,0.0)
             End If
             If (isSet .And. vars(iValue)%systemName.Ne.offs%pro) Then  !  OFFSETS /SYS PROJ
                segList(jj)%pDoppler      = pointResult
             End If
          Else If (GV%doDopplerUpdates) Then
             segList(jj)%updateDoppler = .True.
             If (vars(iValue)%systemName.Eq.offs%pro) Then
                segList(jj)%pDoppler      = vars(iValue)%offset
             Else
                segList(jj)%pDoppler      = xyPointType(0.0,0.0)
             End If
             If (isSet .And. vars(iValue)%systemName.Ne.offs%pro) Then  !  OFFSETS /SYS PROJ
                segList(jj)%pDoppler      = pointResult
             End If
          End If
          !
          !D       Write (6,*) "jj, GV%doDopplerUpdates:  ", jj, GV%doDopplerUpdates
          !D       Write (6,*) "jj, segList(jj)%pDoppler: ", jj, segList(jj)%pDoppler
          !
       Else If (vars(iValue)%segType.Eq.seg%linear) Then
          !
          !D           Write (6,*) "      -> OTF linear "
          !
          If (vars(ivalue)%doRampUp) Then     !!     determine details for RampUp
             !
             !D              Write (6,*) " "
             !D              Write (6,*) "            doRampUp                  ", vars(ivalue)%doRampUp  
             !D              Write (6,*) "            tRampUp                   ", vars(ivalue)%tRampUp
             !D              Write (6,*) "            doInternal                ", vars(ivalue)%doInternal
             !D              !
             !D              Write (6,*) "            vars(iValue)%pStart       ", vars(iValue)%pStart 
             !D              Write (6,*) "            vars(iValue)%pEnd         ", vars(iValue)%pEnd      
             !D              Write (6,*) "            vars(iValue)%lengthOtf    ", vars(iValue)%lengthOtf
             !D              Write (6,*) "            vars(iValue)%speedStart   ", vars(iValue)%speedStart 
             !
             pStart = vars(iValue)%pStart
             vStart%x = vars(iValue)%speedStart                                  &
                  &        * (vars(iValue)%pEnd%x-vars(iValue)%pStart%x)         &
                  &        / vars(iValue)%lengthOtf                              !
             vStart%y = vars(iValue)%speedStart                                  &
                  &        * (vars(iValue)%pEnd%y-vars(iValue)%pStart%y)         &
                  &        / vars(iValue)%lengthOtf                              !
             vStart%speed = sqrt(vStart%x**2+vStart%y**2)
             !
             !D              Write (6,*) "            pStart    ", pStart
             !D              Write (6,*) "            vStart    ", vStart
             !
             !
             accelMax = 0.03*degree                                       !  maximum aceleration in [rad/s^2]
             tAccel   = Int(vStart%speed*GV%angleUnit/accelMax) + 1.0     !  TBD: use max accel. from globals
             !
             If (tAccel.Lt.vars(ivalue)%tRampUp) Then
                !D                 Write (6,*) "             tAccel:    ", tAccel,                  &
                !D                      &      " will be increased to ", vars(ivalue)%tRampUp       !
                tAccel = vars(ivalue)%tRampUp
             End If
             !
!!$             If (tAccel.Lt.5) Then
!!$                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to 5 [sec]"
!!$                tAccel = 5
!!$             End If
             !
             If (vars(ivalue)%doInternal) Then 
                pAccel%x = pStart%x +0.5*vstart%x*tAccel
                pAccel%y = pStart%y +0.5*vstart%y*tAccel
             Else
                pAccel%x = pStart%x -0.5*vstart%x*tAccel
                pAccel%y = pStart%y -0.5*vstart%y*tAccel
             End If
             !
             !D              Write (6,*) "            tAccel   ", tAccel
             !D              Write (6,*) "            pAccel   ", pAccel
             !
             lAccel = sqrt((pStart%x-pAccel%x)**2 + (pStart%y-pAccel%y)**2 )
             !
             !D              Write (6,*) "            lAccel   ", lAccel
             !
          End If     !!     (vars(ivalue)%doRampUp) 
          !
          If (vars(ivalue)%doRampDown) Then     !!     determine details for RampDown
             !
             !D              Write (6,*) " "
             !D              Write (6,*) "            doRampDown                ", vars(ivalue)%doRampDown  
             !D              Write (6,*) "            tRampDown                 ", vars(ivalue)%tRampDown
             !D              Write (6,*) "            doInternal                ", vars(ivalue)%doInternal
             !D              !
             !D              Write (6,*) "            vars(iValue)%pStart       ", vars(iValue)%pStart 
             !D              Write (6,*) "            vars(iValue)%pEnd         ", vars(iValue)%pEnd      
             !D              Write (6,*) "            vars(iValue)%lengthOtf    ", vars(iValue)%lengthOtf
             !D              Write (6,*) "            vars(iValue)%speedEnd     ", vars(iValue)%speedEnd 
             !
             pEnd = vars(iValue)%pEnd
             vEnd%x = vars(iValue)%speedEnd                                      &
                  &        * (vars(iValue)%pEnd%x-vars(iValue)%pStart%x)         &
                  &        / vars(iValue)%lengthOtf                              !
             vEnd%y = vars(iValue)%speedEnd                                      &
                  &        * (vars(iValue)%pEnd%y-vars(iValue)%pStart%y)         &
                  &        / vars(iValue)%lengthOtf                              !
             vEnd%speed = sqrt(vEnd%x**2+vEnd%y**2)
             !
             !D              Write (6,*) "            pEnd    ", pEnd
             !D              Write (6,*) "            vEnd    ", vEnd
             !
             !
             accelMax = 0.03*degree                                      !  maximum aceleration in [rad/s^2]
             tStop   = Int(vStart%speed*GV%angleUnit/accelMax) + 1.0     !  TBD: use max accel. from globals
             !
             If (tStop.Lt.vars(ivalue)%tRampDown) Then
                !D                 Write (6,*) "             tStop:    ", tStop,                    &
                !D                      &      " will be increased to ", vars(ivalue)%tRampDown     !
                tStop = vars(ivalue)%tRampDown
             End If
             !
!!$             If (tStop.Lt.5) Then
!!$                !D              Write (6,*) "             tStop:    ", tStop, " will be increased to 5 [sec]"
!!$                tStop = 5
!!$             End If
             !
             If (vars(ivalue)%doInternal) Then 
                pStop%x = pEnd%x -0.5*vEnd%x*tStop
                pStop%y = pEnd%y -0.5*vEnd%y*tStop
             Else
                pStop%x = pEnd%x +0.5*vEnd%x*tStop
                pStop%y = pEnd%y +0.5*vEnd%y*tStop
             End If
             !
             !D              Write (6,*) "            tStop   ", tStop
             !D              Write (6,*) "            pStop   ", pStop
             !
             lStop = sqrt((pEnd%x-pStop%x)**2 + (pEnd%y-pStop%y)**2 )
             !
             !D              Write (6,*) "            lStop   ", lStop
             !
          End If     !!     vars(ivalue)%doRampDown)
          !
          !
          If (vars(ivalue)%doRampUp) Then     !!     add RampUp to segment list
             !
             Call countSegments(jj)
             !
             If (jj.Eq.1) Then
                segList(jj)%newSubscan =  .True.                                 
             Else 
                If (vars(ivalue)%doNextSubscan) Then
                   segList(jj)%newSubscan =  .True.                              
                Else
                   segList(jj)%newSubscan =  .False.                             
                End If
             End If
             !
             If (GV%doDebugMessages) Then
                Write (6,*) "      "
                Write (6,*) "      jj :                         ", jj
                Write (6,*) "      vars(ivalue)%doRampUp :      ", vars(ivalue)%doRampUp
                Write (6,*) "      vars(ivalue)%doNextSubscan : ", vars(ivalue)%doNextSubscan
                Write (6,*) "      segList(jj)%newSubscan :     ", segList(jj)%newSubscan
                Write (6,*) "      "
             End If
             !
             segList(jj)%scanNumber =  nScan
             segList(jj)%ssNumber   =  jj
             segList(jj)%segNumber  =  1
             segList(jj)%ssType     =  ss%otf
             segList(jj)%segType    =  seg%linear
             segList(jj)%angleUnit  =  aUnit
             segList(jj)%speedUnit  =  sUnit
             !
             If      (vars(iValue)%croFlag.Eq."O") Then
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             Else If (vars(iValue)%croFlag.Eq."R") Then
                segList(jj)%flagOn     =  .False.
                segList(jj)%flagRef    =  .True.
             Else
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             End If
             !
             segList(jj)%flagDropin    =  .True.
             !
             segList(jj)%lengthOtf  =  lAccel
             !
             If (vars(ivalue)%doInternal) Then
                segList(jj)%pStart     =  vars(iValue)%pStart
                segList(jj)%pEnd       =  pAccel 
             Else
                segList(jj)%pStart     =  pAccel
                segList(jj)%pEnd       =  vars(iValue)%pStart
             End If
             !
             segList(jj)%speedStart =  0.0
             segList(jj)%speedEnd   =  1.0001*vStart%speed      !!  IMPORTANT: needed to get smoothly "into" linear OTF ??
             !                                                  !!  TBD:       check /optimize
             segList(jj)%systemName =  vars(iValue)%systemName
             segList(jj)%altOption  =  'SPEED'                  !!  TBD:       debug/generalize
             segList(jj)%tSegment   =  tAccel
             segList(jj)%tRecord    =  0.1
             !
          End If     !!    (vars(ivalue)%doRampUp)
          !
          !                              !!     add OTF segment to list
          Call countSegments(jj)
          !
          If (vars(ivalue)%doRampUp) Then                                        
             segList(jj)%newSubscan =  .False.                                   
          Else                                                                   
             If (vars(ivalue)%doNextSubscan) Then
                segList(jj)%newSubscan =  .True.                              
             Else
                segList(jj)%newSubscan =  .False.                             
             End If
          End If                                                                 
          !
          If (GV%doDebugMessages) Then
             Write (6,*) "      "
             Write (6,*) "      jj :                         ", jj
             Write (6,*) "      vars(ivalue)%doRampUp :      ", vars(ivalue)%doRampUp
             Write (6,*) "      vars(ivalue)%doNextSubscan : ", vars(ivalue)%doNextSubscan
             Write (6,*) "      segList(jj)%newSubscan :     ", segList(jj)%newSubscan
             Write (6,*) "      "
          End If
          !
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  1
          segList(jj)%ssType     =  ss%otf
          segList(jj)%segType    =  seg%linear
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
          If      (vars(iValue)%croFlag.Eq."O") Then
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          Else If (vars(iValue)%croFlag.Eq."R") Then
             segList(jj)%flagOn     =  .False.
             segList(jj)%flagRef    =  .True.
          Else
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          End If
          !
          segList(jj)%flagDropin    =  .False.
          !
          segList(jj)%pStart     =  vars(iValue)%pStart
          segList(jj)%pEnd       =  vars(iValue)%pEnd
          !
          segList(jj)%tSegment   =  vars(iValue)%tOtf
          !
          If ( vars(ivalue)%doRampUp .And. vars(ivalue)%doInternal ) Then
             segList(jj)%pStart     =  pAccel
!!$             segList(jj)%tSegment   =  segList(jj)%tSegment-tAccel
          End If
          !
          If ( vars(ivalue)%doRampDown .And. vars(ivalue)%doInternal ) Then
             segList(jj)%pEnd       =  pStop
!!$             segList(jj)%tSegment   =  segList(jj)%tSegment-tStop
          End If
          !
          segList(jj)%lengthOtf  =  sqrt(                                        &
               &                 (segList(jj)%pEnd%x-segList(jj)%pStart%x)**2    &
               &               + (segList(jj)%pEnd%y-segList(jj)%pStart%y)**2    &
               &                        )                                        !
          segList(jj)%speedStart =  vars(iValue)%speedStart
          segList(jj)%speedEnd   =  vars(iValue)%speedEnd
          segList(jj)%tSegment   =  segList(jj)%lengthOtf /                      &
               &         (0.5 * (segList(jj)%speedStart+segList(jj)%speedEnd) )  !
          !
          segList(jj)%systemName =  vars(iValue)%systemName
          segList(jj)%altOption  =  'TOTF'
          segList(jj)%tRecord    =  0.1
          !
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !
          !
          If (vars(ivalue)%doRampDown) Then     !!     add RampDown to segment list
             !
             Call countSegments(jj)
             !
             segList(jj)%newSubscan =  .False.
             !
             If (GV%doDebugMessages) Then
                Write (6,*) "      "
                Write (6,*) "      jj :                         ", jj
                Write (6,*) "      vars(ivalue)%doRampDown :    ", vars(ivalue)%doRampDown
                Write (6,*) "      vars(ivalue)%doNextSubscan : ", vars(ivalue)%doNextSubscan
                Write (6,*) "      segList(jj)%newSubscan :     ", segList(jj)%newSubscan
                Write (6,*) "      "
             End If
             !
             segList(jj)%scanNumber =  nScan
             segList(jj)%ssNumber   =  jj
             segList(jj)%segNumber  =  1
             segList(jj)%ssType     =  ss%otf
             segList(jj)%segType    =  seg%linear
             segList(jj)%angleUnit  =  aUnit
             segList(jj)%speedUnit  =  sUnit
             !
             If      (vars(iValue)%croFlag.Eq."O") Then
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             Else If (vars(iValue)%croFlag.Eq."R") Then
                segList(jj)%flagOn     =  .False.
                segList(jj)%flagRef    =  .True.
             Else
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             End If
             !
             segList(jj)%flagDropin    =  .True.
             !
             segList(jj)%lengthOtf  =  lStop
             !
             If (vars(ivalue)%doInternal) Then
                segList(jj)%pStart     =  pStop
                segList(jj)%pEnd       =  vars(iValue)%pEnd
             Else
                segList(jj)%pStart     =  vars(iValue)%pEnd
                segList(jj)%pEnd       =  pStop
             End If
             !
             segList(jj)%speedStart =  0.9999*vEnd%Speed        !!  IMPORTANT: needed to get smoothly "into" linear OTF ?? GUESS ??
             segList(jj)%speedEnd   =  0.0+delta                !!  do NOT slow down to 0.0 to avoid bug in antennaMD 
             !                                                  !!  cp TT NCS 2013-08-09 scans 167 168 169
             !                                                  !!  TBD:       check /optimize
             segList(jj)%systemName =  vars(iValue)%systemName  
             segList(jj)%altOption  =  'SPEED'                  !!  TBD:       debug/generalize
             segList(jj)%tSegment   =  tStop
             segList(jj)%tRecord    =  0.1
             !
          End If     !!    (vars(ivalue)%doRampDown)
          !
          !
       Else If (vars(iValue)%segType.Eq.seg%lissajous) Then
          !
          !D        Write (6,*) "      -> OTF Lissajous "
          !
          ! derived parameters positions in GV%angleUnit, speeds in GV%angleUnit/[s]
          ! derived parameters all other in [s], [rad], [rad/s], [rad/s^2]
          !
          !D        Write (6,*) "            doRampUp   ", vars(ivalue)%doRampUp
          !D        Write (6,*) "            doRampDown ", vars(ivalue)%doRampDown
          !
          If (vars(ivalue)%doRampUp) Then
             !
             !D           Write (6,*) "            tRampUp   ", vars(ivalue)%tRampUp
             !D           Write (6,*) "            tRampDown ", vars(ivalue)%tRampDown
             !
             pStart%x = vars(iValue)%pCenter%x + vars(iValue)%xAmplitude * sin(vars(iValue)%phiX)
             pStart%y = vars(iValue)%pCenter%y + vars(iValue)%yAmplitude * sin(vars(iValue)%phiY)
             vStart%x = vars(iValue)%omegaX    * vars(iValue)%xAmplitude * cos(vars(iValue)%phiX)
             vStart%y = vars(iValue)%omegaY    * vars(iValue)%yAmplitude * cos(vars(iValue)%phiY)
             vStart%speed = sqrt(vStart%x**2+vStart%y**2)
             !
             accelMax = 0.03*degree                                       !  maximum aceleration in [rad/s^2]
             tAccel   = Int(vStart%speed*GV%angleUnit/accelMax) + 1.0     !  TBD: use max accel. from globals
             !
             If (tAccel.Lt.vars(ivalue)%tRampUp) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to ", vars(ivalue)%tRampUp
                tAccel = vars(ivalue)%tRampUp
             End If
             !
             If (tAccel.Lt.5) Then
                !D              Write (6,*) "             tAccel:    ", tAccel, " will be increased to 5 [sec]"
                tAccel = 5
             End If
             !
             pAccel%x = pStart%x -0.5*vstart%x*tAccel
             pAccel%y = pStart%y -0.5*vstart%y*tAccel
             !
             Call countSegments(jj)
             !
             segList(jj)%newSubscan =  .True.
             segList(jj)%scanNumber =  nScan
             segList(jj)%ssNumber   =  jj
             segList(jj)%segNumber  =  1
             segList(jj)%ssType     =  ss%otf
             segList(jj)%segType    =  seg%linear
             segList(jj)%angleUnit  =  aUnit
             segList(jj)%speedUnit  =  sUnit
             !
             If      (vars(iValue)%croFlag.Eq."O") Then
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             Else If (vars(iValue)%croFlag.Eq."R") Then
                segList(jj)%flagOn     =  .False.
                segList(jj)%flagRef    =  .True.
             Else
                segList(jj)%flagOn     =  .True.
                segList(jj)%flagRef    =  .False.
             End If
             !
             segList(jj)%flagDropin    =  .True.
             !
             segList(jj)%lengthOtf  =  vars(iValue)%lengthOtf    !!! FIX ME !!!
             segList(jj)%pStart     =  pAccel
             segList(jj)%pEnd       =  pStart
             segList(jj)%speedStart =  0.0
             segList(jj)%speedEnd   =  1.0001*vStart%speed      !!  IMPORTANT: needed to get smoothly "into" Lissajous
             !                                                  !!  TBD:       check /optimize
             segList(jj)%systemName =  vars(iValue)%systemName
             segList(jj)%altOption  =  'SPEED'                  !!  TBD:       debug/generalize
             segList(jj)%tSegment   =  tAccel
             segList(jj)%tRecord    =  0.1
             !
          End If     !!    If (vars(ivalue)%doRampUp) Then
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !D       Write (6,*) "       jj: ", jj
          !
          !D       Write (6,*) "             Lissajous Segment"
          !
          Call countSegments(jj)
          !
          segList(jj)%newSubscan =  .False.                      !!  TBD: handle case no ramp --> newSubscan .True.
          segList(jj)%scanNumber =  nScan
          segList(jj)%ssNumber   =  jj
          segList(jj)%segNumber  =  2
          segList(jj)%ssType     =  ss%otf
          segList(jj)%segType    =  seg%lissajous
          segList(jj)%angleUnit  =  aUnit
          segList(jj)%speedUnit  =  sUnit
          !
          If      (vars(iValue)%croFlag.Eq."O") Then
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          Else If (vars(iValue)%croFlag.Eq."R") Then
             segList(jj)%flagOn     =  .False.
             segList(jj)%flagRef    =  .True.
          Else
             segList(jj)%flagOn     =  .True.
             segList(jj)%flagRef    =  .False.
          End If
          !
          segList(jj)%flagDropin    =  .False.
          !
          segList(jj)%pCenter    =  vars(iValue)%pCenter
          segList(jj)%xAmplitude =  vars(iValue)%xAmplitude
          segList(jj)%yAmplitude =  vars(iValue)%yAmplitude
          segList(jj)%omegaX     =  vars(iValue)%omegaX    
          segList(jj)%omegaY     =  vars(iValue)%omegaY    
          segList(jj)%phiX       =  vars(iValue)%phiX      
          segList(jj)%phiY       =  vars(iValue)%phiY      
          !
          segList(jj)%systemName =  vars(iValue)%systemName
          segList(jj)%altOption  =  'TOTF'
          segList(jj)%tSegment   =  vars(iValue)%tOtf
          segList(jj)%tRecord    =  0.1
          !
          nLissajous = nLissajous+1
          !
          iQuery    = iPro
          nameQuery = GPnone
          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
          !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
          !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
          !
          ! TBD: clever /proper handling of Doppler 
          !
       End If
       !
    End If
    !
    Call getCountSegments(nSegments)
    !
    !D     Write (6,*) "         nSegments:   ", nSegments
    !D     Write (6,*) "         segList(jj): "
    !D     Write (6,*)                          segList(jj)
    !
    Return 
    !
  End Subroutine analyzeSubscan
!!!
!!!
  Subroutine displayDIYlist
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplayDIYlist.inc'
    !
  End Subroutine displayDIYlist
!!!
!!!
  Subroutine displaySubscan
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplaySubscan.inc'
    !
  End Subroutine displaySubscan
!!!
!!!
  Subroutine listDIYlist(error)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical            :: error
    Integer            :: ierr
    !
    Real               :: fX, fY
    ! 
    Call getCountSegments(nSegments)
    !
    Call pako_message(seve%t,'paKo',                                             &
         &    " module DIYlist, 1.2.3.2 2014-12-04 ---> SR: listDIYlist ")       ! !! Trace execution
    !
    If (nSegments.Ge.1) Then
       Write (messageText,*)  "  segments #: 1 to ", nSegments
       l = Len_trim(GV%observingMode)    
       !! Call pakoMESSAGE(priorityI,severityI,GV%observingMode(1:l),messageText)  !!  TBD: recover!
       Call pakoMESSAGE(priorityI,severityI,"DIY",messageText)
    End If
    !
    If (nSegments.Ge.1) Then
       !
       Do ii = 1, nSegments, 1
          !
          If (segList(ii)%ssType .Eq. ss%otf                                     &
               &   .And. segList(ii)%segType .Eq. seg%lissajous) Then            ! !!  Lissajous
             !
             ierr = 1
             If (segList(ii)%flagOn) Then
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3, " ",A, " ",A, " Amplitude ",2F12.1, " ",2A)' )     &
                     &  ii,                                                      &
                     &  segList(ii)%segType,     traceflag%on,                   &
                     &  segList(ii)%xAmplitude,                                  &  
                     &  segList(ii)%yAmplitude,                                  &  
                     &  GV%angleUnitSetC,                                        &
                     &  segList(ii)%systemName                                   !
             Else If (segList(ii)%flagRef) Then
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3, " ",A, " ",A, " Amplitude ",2F12.1, " ",2A)' )     &
                     &  ii,                                                      &
                     &  segList(ii)%segType,     traceflag%ref,                  &
                     &  segList(ii)%xAmplitude,                                  &  
                     &  segList(ii)%yAmplitude,                                  &  
                     &  GV%angleUnitSetC,                                        &
                     &  segList(ii)%systemName                                   !
             End If
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             !
             fX = segList(ii)%omegaX/(2*Pi)
             fY = segList(ii)%omegaY/(2*Pi)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("___",  " Frequency ", 2F12.5, " [Hz] ",                  &
                  &            " Omega ",     2F12.5, " [rad/s] ",               &
                  &                            F12.5, " [s] " ) ' )              &
                  &  fX, fY,                                                     &
                  &  segList(ii)%omegaX    ,                                     &  
                  &  segList(ii)%omegaY    ,                                     &  
                  &  segList(ii)%tsegment                                        !
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("___",  " Center ", 2F12.1, " ", A, " Phi ",              &
                  &                        2F12.5, " [rad] " ) ' )               &
                  &  segList(ii)%pCenter%x/GV%angleFactorR,                      &
                  &  segList(ii)%pCenter%y/GV%angleFactorR,                      &
                  &  GV%angleUnitSetC   ,                                        &
                  &  segList(ii)%phiX   ,                                        &  
                  &  segList(ii)%phiY                                            !
             Call pakoMESSAGE(max(1,priorityI-1),severityI,'DIY',messageText)
             !
          Else If (segList(ii)%ssType .Eq. ss%otf) Then                          !  !!  OTF 
             !
!!$             Write (6,*) " ii :                     ", ii
!!$             Write (6,*) " segList(ii)%newSubscan : ", segList(ii)%newSubscan
             If (segList(ii)%newSubscan) Then
                !                                                                 
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '( A                                                     &
                     &  )'  )                                                    &
                     &  "new SUBSCAN : "                                         !
                Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
                !                
             End If
             !
             If (.Not.segList(ii)%flagDropin) Then
                !
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3," ",  A, 2F12.1," to ", 2F12.1, " ", A," ", A       &
                     &  )'  )                                                    &
                     &  ii,                                                      &
                     &  segList(ii)%ssType,                                      &
                     &  segList(ii)%pStart%x/GV%angleFactorR,                    &
                     &  segList(ii)%pStart%y/GV%angleFactorR,                    &
                     &  segList(ii)%pEnd%x/GV%angleFactorR,                      &
                     &  segList(ii)%pEnd%y/GV%angleFactorR,                      &
                     &  GV%angleUnitSetC,                                        &
                     &  segList(ii)%systemName                                   !
                !
             Else
                !
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3," ",  A, 2F12.1," to ", 2F12.1, " ", A," ", A       &
                     &       " drop-in "                                         &
                     &  )'  )                                                    &
                     &  ii,                                                      &
                     &  segList(ii)%ssType,                                      &
                     &  segList(ii)%pStart%x/GV%angleFactorR,                    &
                     &  segList(ii)%pStart%y/GV%angleFactorR,                    &
                     &  segList(ii)%pEnd%x/GV%angleFactorR,                      &
                     &  segList(ii)%pEnd%y/GV%angleFactorR,                      &
                     &  GV%angleUnitSetC,                                        &
                     &  segList(ii)%systemName                                   !
                !
             End If
             !
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             !
             Write (messageText, iostat=ierr, fmt =                              &
                  &  '("___",                                                    &
                  &    " tOtf ",   F12.5, " [s]",                                &
                  &    " speed ", 2F12.5, " [", A, "/s]"                         &
                  &  )'  )                                                       &
                  &  segList(ii)%tsegment,                                       &
                  &  segList(ii)%speedStart,                                     &
                  &  segList(ii)%speedEnd,                                       &
                  &  GV%angleUnitSetC                                            !
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             !
             If (ierr.Ne.0) Then
                Write (messageText, *)                                           &
                     &  ii,                                                      &
                     &  segList(ii)%ssType,                                      &
                     &  segList(ii)%pStart%x/GV%angleFactorR,                    &
                     &  segList(ii)%pStart%y/GV%angleFactorR,                    &
                     &  segList(ii)%pEnd%x/GV%angleFactorR,                      &
                     &  segList(ii)%pEnd%y/GV%angleFactorR,                      &
                     &  GV%angleUnitSetC,                                        &
                     &  segList(ii)%systemName,                                  &
                     &  segList(ii)%tsegment                                     !
                Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             End If                                        !
             !
          Else If (segList(ii)%ssType .Eq. ss%cs) Then                           !  !!  CS
             Write (messageText,*)  "  subscan #:   ", ii," ",                   &
                  &  segList(ii)%ssType," ",                                     &
                  &  "at ",   segList(ii)%pStart%x/GV%angleFactorR," ",          &
                  &           segList(ii)%pStart%y/GV%angleFactorR," ",          &
                  &  GV%angleUnitSetC                                            !
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             !
          Else If (segList(ii)%ssType.Eq.ss%ca .Or.                              & !!  CA, CC
               &   segList(ii)%ssType .Eq. ss%cc) Then                           !
             If (segList(ii)%sideBand.Ne.GPnone) Then
                Write (messageText,*)  "  subscan #:   ", ii," ",                &
                     &  segList(ii)%ssType," ",                                  &
                     &  segList(ii)%sideBand                                     !
             Else
                Write (messageText,*)  "  subscan #:   ", ii," ",                &
                     &  segList(ii)%ssType                                       !
             End If
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
             !
!!$          Else If (segList(ii)%ssType .Eq. ss%track) Then                     
          Else If (     segList(ii)%ssType .Eq. ss%track                         &   !!  TRACK
               &   .Or. segList(ii)%ssType .Eq. ss%ref                           &   !!  REF
               &   .Or. segList(ii)%ssType .Eq. ss%tune                          &   !!  TUNE
               &  ) Then                                                         !   
             !
             If (segList(ii)%flagTune) Then                                      !   !!     -> NIKA tune
                Write (messageText, iostat=ierr, fmt =                           &
                     &  '(I3," ", A,A, "at ", 2F12.1, " [", A,"] ",              &
                     &   A, F12.5," [s] ", A)   '                                &
                     &  )                                                        &
                     &           ii,                                             &
                     &           segList(ii)%ssType,traceflag%on,                &
                     &           segList(ii)%pStart%x/GV%angleFactorR,           &
                     &           segList(ii)%pStart%y/GV%angleFactorR,           &
                     &           GV%angleUnitSetC,                               &
                     &           segList(ii)%systemName,                         &
                     &           segList(ii)%tsegment,                           &
                     &           " /Tune "                                       !
                Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
                !
             Else If (.Not.segList(ii)%flagTune) Then                            !  !!     -> not NIKA tune
                !
                If (segList(ii)%flagOn) Then                                     !  !!     -> ON
                   Write (messageText, iostat=ierr, fmt =                        &
                        &  '(I3," ", A,A, "at ", 2F12.1," [", A,"] ",            &
                        &   A, F12.5," [s]")   '                                 &
                        &  )                                                     &
                        &           ii,                                          &
                        &           segList(ii)%ssType,traceflag%on,             &
                        &           segList(ii)%pStart%x/GV%angleFactorR,        &
                        &           segList(ii)%pStart%y/GV%angleFactorR,        &
                        &           GV%angleUnitSetC,                            &
                        &           segList(ii)%systemName,                      &
                        &           segList(ii)%tsegment                         !
                   Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
                Else If (segList(ii)%flagRef) Then                               !  !!       -> REF
                   Write (messageText, iostat=ierr, fmt =                        &
                        &  '(I3," ", A,A, "at ", 2F12.1," [", A,"] ",            &
                        &   A, F12.5," [s]")   '                                 &
                        &  )                                                     &
                        &           ii,                                          &
                        &           segList(ii)%ssType,traceflag%ref,            &
                        &           segList(ii)%pStart%x/GV%angleFactorR,        &
                        &           segList(ii)%pStart%y/GV%angleFactorR,        &
                        &           GV%angleUnitSetC,                            &
                        &           segList(ii)%systemName,                      &
                        &           segList(ii)%tsegment                         !
                   Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
                End If
                !
             End If                                                              ! !!     <- flagTune or else
             !
          Else                                                                   ! !!  <-- TRACK  
             !
             Write (messageText,*)  "  subscan #:   ", ii," ",                   &
                  &  segList(ii)%ssType                                          !
             !
             Call pakoMESSAGE(priorityI,severityI,'DIY',messageText)
          End If
          !
       End Do  !!  loop over segments
       !
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'DIY',messageText)
       !
    End If     !!  nSegments.Ge.1 or else
    !
    Return
    !
  End Subroutine listDIYlist
!!!
!!!
  Subroutine plotDIYlist (errorP)
    !
    !**   Variables  for Plots   ***
    Include 'inc/variables/headerForPlotMethods.inc'
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    errorP = .False.
    !
    Call pako_message(seve%t,"DIY",                                              &
         &    " module DIYlist, 1.2.3 2014-12-15 ---> SR: plotDIYlist ")         ! trace execution
    !
    Call configurePlots
    !
    Call getCountSegments(nSegments)
    !
    !D     Write (6,*) '        nSegments: ',  nSegments
    !D     Write (messageText,*)  "  subscans #: 1 to ", nSegments
    !
    If (nSegments.Ge.1) Then
       !
       Do ii = 1, nSegments, 1
          !
          !D           Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType,               &
          !D                &          segList(ii)%flagOn, segList(ii)%flagRef,               &
          !D                &          segList(ii)%flagTune                                   !
          !
          If      (segList(ii)%ssType.Eq.ss%otf                                  &
               &   .And. segList(ii)%segType.Eq.seg%linear) Then                 !  !!  OTF linear
             !
             If (segList(ii)%flagDropin) Then                                       !!  drop-in
                Call plotOtfLinearP(                                             &
                     &  segList(ii)%pStart,                                      &
                     &  segList(ii)%pEnd,                                        &
                     &  errorP,                                                  &
                     &  speedStart= segList(ii)%speedStart,                      &
                     &  speedEnd  = segList(ii)%speedEnd,                        &
                     &  tSegment  = segList(ii)%tSegment,                        &
                     &  tSample   = 1/GV%slowRateAMD,                            &
                     &  number    = ii,                                          &
                     &  Type      = "dropin"                                     &
                     &  )                                                        !
             Else
                Call plotOtfLinearP(                                             &
                     &  segList(ii)%pStart,                                      &
                     &  segList(ii)%pEnd,                                        &
                     &  errorP,                                                  &
                     &  speedStart= segList(ii)%speedStart,                      &
                     &  speedEnd  = segList(ii)%speedEnd,                        &
                     &  tSegment  = segList(ii)%tSegment,                        &
                     &  tSample   = 1/GV%slowRateAMD,                            &
                     &  number    = ii)                                          !
             End If
             !
          Else If      (segList(ii)%ssType.Eq.ss%otf                             &
               &   .And. segList(ii)%segType.Eq.seg%lissajous) Then              !  !!  OTF Lisajous
             !
             !D              Write (6,*) "   --> plotDIYlist: about to call plotOtfLissajous"
             !D              Write (6,*) "                GV%slowRateAMD: ", GV%slowRateAMD
             !
             Call plotOtfLissajousP(                                             &
                  &  segList(ii)%pCenter,                                        &
                  &  segList(ii)%xAmplitude,                                     &
                  &  segList(ii)%yAmplitude,                                     &
                  &  segList(ii)%omegaX,                                         &
                  &  segList(ii)%omegaY,                                         &
                  &  segList(ii)%phiX,                                           &
                  &  segList(ii)%phiY,                                           &
                  &  segList(ii)%tSegment,                                       &
                  &  errorP,                                                     &
                  &  tSample=1/GV%slowRateAMD,                                   &
                  &  number=ii)                                                  !
             !
          Else If (segList(ii)%ssType .Eq.ss%track                               &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagTune) Then                              !  !!  TRACK tune
             !!D  Write (6,*) "      ii: ", ii, " -> plot Track /Tune "
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%tune)                                      !
             !
          Else If (segList(ii)%ssType .Eq.ss%tune                                &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagTune) Then                              !  !!  TRACK tune
             !!D  Write (6,*) "      ii: ", ii, " -> plot Track /Tune "
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%tune)                                      !
             !
          Else If (segList(ii)%ssType .Eq.ss%track                               &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagOn) Then                                !  !!  TRACK On
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%on)                                        !
             !
          Else If (segList(ii)%ssType .Eq.ss%track                               &
               &   .And. segList(ii)%segType.Eq.seg%track                        &
               &   .And. segList(ii)%flagRef) Then                               !  !!  TRACK Reference
             Call plotTrackP( segList(ii)%pStart,                                &
                  &           errorP,                                            &
                  &           number=segList(ii)%ssNumber,                       &
                  &           Type=ss%ref)                                       !
             !
          End If
          !
       End Do
       !
    Else
       !
       Write (messageText,*) '   list of subscans and segments is empty.'
       Call PakoMessage(priorityW,severityW,'DIY',messageText)
       !
    End If
    !
    If (errorP)                                                                  &
         &        Call pakoMessage(priorityE,severityE,                          &
         &        'DIYlist',' could not plot ')                                  !
    !
    Return
  End Subroutine plotDIYlist
!!!
!!!
  Subroutine saveDIYlist(programName,LINE,commandToSave,iUnit, ERROR)
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !      
    Character(len=lenLine)             ::  messageText
    !
    ERROR = .False.
    !
    !D     Write (6,*) '      ---> modulePakoDIYlist SR: saveDIYlist '
    !
    Include 'inc/commands/saveDIYlist.inc'
    !
    Return
  End Subroutine saveDIYlist
!!!
!!!
  Subroutine saveSubscan(programName,LINE,commandToSave,iUnit, ERROR)
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !      
    Character(len=lenLine)             ::  messageText
    !
    ERROR = .False.
    !
    !D     Write (6,*) '      ---> modulePakoDIYlist SR: saveSubscan '
    !
    Include 'inc/commands/saveSubscan.inc'
    !
    Return
  End Subroutine saveSubscan
!!!
!!!
  Subroutine startDIYlist(programName,LINE,commandToSave, iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ERROR = .False.
    !
    !D     Write (6,*) " ---> modulePakoDIYlist: SR startDIYlist "
    !
    Call startSubscanList(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Return
  End Subroutine startDIYlist
!!!
!!!
  Subroutine writeXMLDIYlist(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ERROR = .False.
    !
    Return
  End Subroutine writeXMLDIYlist
!!!
!!!
End Module modulePakoDIYlist
!!!
!!!
