!
!  Id: modulePakoGlobalVariables.f90 ,v 1.3.0  2017-10-09 Hans Ungerechts
!  Id: modulePakoGlobalVariables.f90 ,v 1.2.6  2017-08-17 Hans Ungerechts
!                                       1.2.6  2017-03-15 Hans Ungerechts
!                                       1.2.4  2016-09-05 Hans Ungerechts
!                                       1.2.4  2016-02-15 Hans Ungerechts
!                                       1.2.4  2015-06-15 Hans Ungerechts
!                                       1.2.3  2014-10-10 Hans Ungerechts
!                                       1.2.3  2014-09-01 Hans Ungerechts
!                                       1.2.3  2014-07-17 Hans Ungerechts
!                                       1.2.3  2014-03-13 Hans Ungerechts
!                                       1.2.2  2013-01-23 Hans Ungerechts
!      <-- modifications from         v 1.1.13 2012-10-29 Hans Ungerechts
!
!  Id: modulePakoGlobalVariables.f90 ,v 1.2.1  2012-06-25 Hans Ungerechts
!  X-checked with:
!  Id: modulePakoGlobalVariables.f90 ,v 1.1.12 2012-03-01 Hans Ungerechts
!
!-------------------------------------------------------------------------
!
!  PAKO 
!
! <DOCUMENTATION name="moduleGlobalVariables">
!
! 
! 
!
! 
!  SET keyword   value [value* ...]
! 
!  Set values for some general parameters, e.g., project ID, PI (principal
!  investigator), observer, operator.
!
!  Character          ::  keyword
!  Character|Integer  ::  value*
!
!  Keywords   Type of Value(s)
!  ========   ================
!
!  Level      Integer [Integer] ! minumum value(s) for informational messages
!  Project    Character(len=24) ! project ID
!  PI         Character(len=24) ! PI
!  Observer   Character(len=24) ! observer(s)
!  Operator   Character(len=24) ! telescope operator(s)
!  Pointing   Real Real         ! pointing corrections [arc sec]
!  Focus      Real              ! focus correction [mm]
!  Topology   Character(len=24) ! "Topology" for the overlapping azimuth range
!  Automatic  Logical           ! send a message to Coordinator to turn 
!                               ! Automatic mode on or off (Special Command!)
!
! </DOCUMENTATION> <!-- name="moduleGlobalVariables" -->
!
Module modulePakoGlobalVariables
  !
  Use modulePakoMessages
  Use modulePakoTypes
  Use modulePakoLimits
  Use modulePakoUtilities
  Use modulePakoXML
  Use modulePakoDisplayText
  Use modulePakoGlobalParameters 
  !
  Use gbl_message
  !              
  Implicit None
  Save
  Private
  !
  Public :: GVsourceCatalog, GVlineCatalog
  !
  Public :: ioUnitType,             ioUnit
  Public :: GVtype,                 GV
  Public :: pointingCorrectionType, GVpCorr
  Public :: focusCorrectionType,    GVfCorr,    GVfCorrX,    GVfCorrY
  !
  Public :: pakoSet
  Public :: displaySet
  Public :: pakoShow
  Public :: pakoSaveSet
  Public :: writeXMLantenna
  Public :: writeXMLset
  Public :: writeXMLversion
  !
  Include 'inc/parameters/sizes2.inc'
!!   TBD:  Include 'inc/parameters/sizes.inc'
!! < v1.2  Include 'inc/parameters/sizes2.inc'
  !
  ! *** for FORTRAN I/O Units:
  !     TBD: assign on startup through SIC_GETLUN
  Type :: ioUnitType
     Integer :: Read      = 11
     Integer :: Write     = 12
     Integer :: Save      = 21
     Integer :: Start     = 22
     Integer :: Display   = 23
     Integer :: DisplayBE = 24
     Integer :: Lock      = 31
  End Type ioUnitType
  !
  Type (ioUnitType) :: ioUnit
  !
  Integer, Parameter ::             nDimCatalog   = 1
  Character(len=lenVar), Dimension (nDimCatalog) :: GVsourceCatalog = GPnone2
  Character(len=lenVar), Dimension (nDimCatalog) :: GVlineCatalog   = GPnone2
  !
  Type :: GVtype
     !
     Logical                     ::  automatic                  =  GPnoneL
     !
     !                                                                           !  noReady because:
     Logical                     ::  notReadyFocus              =  GPnoneL
     Logical                     ::  notReadyOnOff              =  GPnoneL
     Logical                     ::  notReadyLissajous          =  GPnoneL
     Logical                     ::  notReadyOtfmap             =  GPnoneL
     Logical                     ::  notReadyPointing           =  GPnoneL
     Logical                     ::  notReadyTrack              =  GPnoneL
     Logical                     ::  notReadyRXafterBE          =  GPnoneL       !  Receiver set after Backends
     Logical                     ::  notReadySWafterOM          =  GPnoneL       !  SW* set after Obs. Mode
     Logical                     ::  notReadyRXafterSecondary   =  GPnoneL       !  Receiver set after SET 2ndRotation
     Logical                     ::  notReadyRXafterTip         =  GPnoneL       !  Receiver set after OM TIP
     Logical                     ::  notReadySecondaryRafterOM  =  GPnoneL       !  2ndRotation set after Obs. Mode  
     !!                                                                          !! TBD: correct typo "Rafter" -- "After" !
     Logical                     ::  notReadyOffsetsAfterOnOff  =  GPnoneL       !  OFFSETS set after ONOFF
     Logical                     ::  notReadyReceiverOffsets    =  GPnoneL       !  inconsistent Nasmyth Offsets
     !
     Logical                     ::  doSubmit                   =  GPnoneL
     Logical                     ::  doDopplerUpdates           =  GPnoneL
     Logical                     ::  doXMLobservingMode         =  .True.
     !
     Logical                     ::  doDebugMessages            =  GPnoneL
     !
     Character(len=128)          ::  testMode                   =  GPnone
     !
     Logical                     ::  backendFTS                 =  GPnoneL       !  is backend FTS selected?
     !
     Logical                     ::  pulsarMode                 =  GPnoneL       !  special (sampling) mode for Pulsars  
     !
     Type(conditionMinMax)       ::  condElevation                               &
          &                          = conditionMinMax("elevation",              &
          &                                             0.0, Pi/2.0,             &
          &                                            "rad",                    &
          &                                            .False.       )           !
     !
     Integer                     ::  iPstdio                    =  GPnone2I      ! message priority
     Integer                     ::  iPfile                     =  GPnone2I       
     !
     Character(len=lenCh2)       ::  plotStyle     =  setPlot%simple
     !
     Character(len=lenCh2)       ::  privilege     =  setPrivilege%user
     Integer                     ::  iPrivilege    =  iUser
     !
     Character(len=lenCh2)       ::  userLevel     =  setUserLevel%normal
     Integer                     ::  iUserLevel    =  iNormal
     !
     Character(len=lenCh2)       ::  limitCheck    =  setLimitCheck%Strict
     Integer                     ::  iLimitCheck   =  iStrict
     !
     Character(len=lenCh2)       ::  EMIRcheck     =  setLimitCheck%test
     Integer                     ::  iEMIRcheck    =  iTest
     !
     Character(len=lenCh2)       ::  matchSource   =  setMatchSource%Exact
     Integer                     ::  iMatchSource  =  iExact
     !
     Character(len=lenCh2)       ::  project                    =  GPnone2
     Character(len=lenCh2)       ::  PI                         =  GPnone2
     Character(len=lenCh2)       ::  observer                   =  GPnone2
     Character(len=lenCh2)       ::  oper                       =  GPnone2
     !
     Character(len=lenCh2)       ::  observingMode              =  GPnone2
     Character(len=lenCh2)       ::  observingModePako          =  GPnone2
     Character(len=lenCh2)       ::  sessionMode                =  GPnone2       ! e.g., to flag VLBI session 
     Character(len=lenCh2)       ::  switchingMode              =  GPnone2
     Character(len=lenCh2)       ::  switchingModePako          =  GPnone2
     !
     Character(len=lenCh2)       ::  receiverName               =  GPnone2
     Character(len=lenCh2)       ::  bolometerName              =  GPnone2
     !
     Character(len=lenCh2)       ::  purpose                    =  GPnone2
     Logical                     ::  purposeIsSet               =  GPNoneL
     !
     Character(len=lenVar)       ::  userComment                =  GPnone
     Logical                     ::  userCommentIsSet           =  GPNoneL
     !
     Real(Kind=4)                ::  transitionAcceleration     =  GPnoneR       ! "acceleration" in antenna MD 
     !                                                                           ! transition trajectories 
     !
     Real(Kind=4)                ::  secondaryRotation          =  GPnoneR       ! 2ndary rotation
     !                                                                           ! (subreflector rotation)
     !
     Real(Kind=4)                ::  slowRateAMD                =  1             ! rate for antennaMD slow loop: [1|2|4|8] [Hz]
     !
     Character(len=lenCh2)       ::  sourceProjectID            =  GPnone2       ! project ID for source
     !
     Character(len=lenCh2)       ::  topology                   =  topo%low      ! "topology" for azimuth turns
     !
     Real(Kind=4)                ::  tRecord                    =  GPnone2R4
     Real(Kind=4)                ::  tPhase                     =  GPnoneR       ! global (copy of) switching tPhase
     !
     Character(len=lenCh2)       ::  omSystemName               =  GPnone2       ! /system of observing mode
     !
     Type (varOffsets), Dimension(nDimOffsetChoices)                             &
          &                      ::  sourceOffsets                               ! global (copy of) source offsets
     Logical                     ::  arrayNasmythOffsetsAreSet  =  GPNoneL       ! e.g, for NIKA
     !
     Real(Kind=4), Dimension(2)  ::  wOffset                    =  GPnoneR       ! wobbler offsets
     Real(Kind=4)                ::  wAngle                     =  GPnoneR       ! wobbler angle
     !
     !     angle unit specified by user with command: SET angleUnit 
     Integer                     ::  iAU                        =  iarcsec
     Character(len=lenCh2)       ::  angleUnitSetC              =  au%arcsec
     Real(Kind=8)                ::  angleUnitSet               =     arcsec
     Character(len=lenCh2)       ::  speedUnitSetC              =  su%arcsec
     Real(Kind=8)                ::  speedUnitSet               =     arcsec
     !
     !     angle unit in which angles are stored internally
     Character(len=lenCh2)       ::  angleUnitC                 =  au%arcsec
     Real(Kind=8)                ::  angleUnit                  =     arcsec
     Character(len=lenCh2)       ::  speedUnitC                 =  su%arcsec
     Real(Kind=8)                ::  speedUnit                  =     arcsec
     !
     !     factors to convert from SET units to stored values
     Real(Kind=8)                ::  angleFactorD               = 1.0
     Real(Kind=4)                ::  angleFactorR               = 1.0
     !
     Real(Kind=8)                ::  speedFactorD               = 1.0
     Real(Kind=4)                ::  speedFactorR               = 1.0
     !
     !     frequency unit in which resolution, bandwidth, etc.
     !     are stored internally
     Character(len=lenCh2)       ::  frequencyUnitC             =  "MHz"
     Real(Kind=8)                ::  frequencyUnit              =   MHz
     !
     Logical                     ::  sourceSet                  =  GPnoneL      
     !
     Logical                     ::  backendSet                 =  GPnoneL
     Integer                     ::  backendNumberOf            =  GPnoneI
     Logical                     ::  receiverSet                =  GPnoneL
     Integer                     ::  receiverNumberOf           =  GPnoneI
     !
  End Type GVtype
  Type (GVtype)  ::  GV
  !
  Type :: pointingCorrectionType
     Real(Kind=4)                ::  azimuth                    =  GPnone2R4
     Real(Kind=4)                ::  elevation                  =  GPnone2R4
     Real(Kind=4), Dimension(10) ::  pointingP                  =  GPnone2R4
     Real(Kind=4)                ::  azimuthCorrection          =  GPnone2R4
     Real(Kind=4)                ::  azimuthCorrectionError     =  GPnone2R4
     Real(Kind=4)                ::  elevationCorrection        =  GPnone2R4
     Real(Kind=4)                ::  elevationCorrectionError   =  GPnone2R4
  End Type pointingCorrectionType
  Type (pointingCorrectionType)  ::  pCorr
  Type (pointingCorrectionType)  ::  GVpCorr
  Type (pointingCorrectionType)  ::  pCorrDefault
  Namelist / pCorrNML / pCorr
  !
  Type :: focusCorrectionType
     Real(Kind=4)                ::  azimuth                    =  GPnone2R4
     Real(Kind=4)                ::  elevation                  =  GPnone2R4
     Real(Kind=4)                ::  focusCorrection            =  GPnone2R4
     Real(Kind=4)                ::  focusCorrectionError       =  GPnone2R4
  End Type focusCorrectionType
  Type (focusCorrectionType)     ::  fCorr
  Type (focusCorrectionType)     ::  GVfCorr
  Type (focusCorrectionType)     ::  GVfCorrX                   = focusCorrectionType(0.0,0.0,0.0,0.0)
  Type (focusCorrectionType)     ::  GVfCorrY                   = focusCorrectionType(0.0,0.0,0.0,0.0)
  Type (focusCorrectionType)     ::  fCorrDefault
  Namelist / fCorrNML / fCorr
  !
  ! NOTE: OBSERVINGMODE, SWITCHINGMODE ARE DEPRECATED, USE: GV%*MODE ABOVE
  Character(len=lenCh2)          :: observingMode = "NONE"
  Character(len=lenCh2)          :: switchingMode = "NONE"
  !
  Include 'inc/variables/variablesSet.inc'
  !
Contains
!!!
!!!
  Subroutine pakoSet(programName,line,command,error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical                       ::  isInitialized  = .False.
    Character (len=lenCh)         ::  keyword        = GPnone2
    Character (len=lenCh)         ::  valueC         = GPnone2
    !
    Integer                       ::  ier
    !
    Logical                       ::  logicalF1      = .False.
    Logical                       ::  logicalF2      = .False.
    Logical                       ::  errorAngleUnit = .False.
    Logical                       ::  errorPlotStyle = .False.
    Logical                       ::  errorLimitCheck= .False.
    Logical                       ::  errorPrivilege = .False.
    Logical                       ::  errorUserLevel = .False.
    Logical                       ::  errorTopology  = .False.
    !
    Character(len=lenLineDouble)  ::  shellCommand
    !
    !**   functions called:   ***
    Integer                       ::  System
    !
    Call pako_message(seve%t,programName,                                        &
         &  " modulePakoGlobalVariables ,v 1.3.0  2017-10-09 ---> SR: pakoSet ") ! trace execution
    !
    Include 'inc/ranges/rangesSet.inc'
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       GV%transitionAcceleration = vars(iDefault)%transitionAcceleration
       GV%secondaryRotation      = vars(iDefault)%secondaryRotation
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    !TBD   If (.Not.ERROR) Then
    !TBD        vars(iIn) = vars(iValue)
    !TBD   End If
    !TBD   !
    ! *** check N of Parameters: 2, 3 --> SET keyword value  ***
    Call checkNofParameters(command,                                             &
         &     2, 3,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    If (errorNumber) Then
       Write (messageText,*) "Usage: SET keyword value [value]"
       Call PakoMessage(priorityE,severityE,command,messageText)
    End If
    ERROR = ERROR .Or. errorNumber
    !
    ! *** read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    If (.Not. ERROR) Then
       Include 'inc/parameters/parametersSetKeywordValue.inc'
    End If
    !
    !D    Write (6,*) "      errorAngleUnit: ", errorAngleUnit
    !D    Write (6,*) "      error:          ", error
    !
    ! *** display values ***
    !     (Note: this is done indepenently of the Error status)
    Call displaySet
    !
  End Subroutine pakoSet
!!!
!!!
  Subroutine displaySet
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=lenCh)  :: command
    !
    Character (len=lenVar2) ::  displayLine   = GPnone2
    !
    Include 'inc/display/commandDisplaySet.inc'
    !
  End Subroutine displaySet
!!!
!!!
  Subroutine pakoShow(programName,line,command,error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical                  ::  isInitialized = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module GlobalVariables.f90 ,v 1.3.0  --> SR: pakoShow "            &
         &  //"2017-10-09 ")                                                     ! trace execution
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       !!
    End If
    !
    ! *** check N of Parameters: 0, 0 --> none allowed  ***
    Call checkNofParameters(command,                                             &
         &     0, 0,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber 
    !
    ! *** read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    If (.Not. ERROR) Then
       !!         Include 
    End If
    !
    Call pakoMessage_level(0)       ! message level 0 so that
    !                               ! the following show
    !
    Write (messageText,*)     "....................................... "         !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,*)     "paKo Revision ......................... ",        &
         &                     pakoVersionString, pakoVersionDate                !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,*)     "paKo subVersion ....................... ",        &
         &                     pakoSubVersion                                    !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
!!$    If (.Not. (      GV%userLevel.Eq.setUserLevel%normal                         &
!!$         &      .Or. GV%userLevel.Eq.setUserLevel%beginner                       &
!!$         &      .Or. GV%userLevel.Eq.setUserLevel%virgenExtra                    &
!!$         &    )    )   Then                                                      !
    If ( GV%iUserLevel.Ne.iNormal ) Then
       Write (messageText,*)  "User Level ............................ ",        &
            &                  GV%userLevel                                      !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
!!$    If ( GV%privilege.Ne.setPrivilege%user ) Then
    If ( GV%iPrivilege.Ge.2 ) Then
       Write (messageText,*)  "Privilege ............................. ",        &
            &                  GV%privilege                                      !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
    If ( GV%matchSource.Ne.setMatchSource%Exact ) Then
       Write (messageText,*)  "match source name ..................... ",        &
            &                  GV%matchSource                                    !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
    Write (messageText,*)     "Level ............. for standard output ",        &
         &                     GV%iPstdio,                                       &
         &                    " for file ",                                      &
         &                    GV%iPfile                                          !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,*)     "doSubmit Queue ........................ ",        &
         &                     GV%doSubmit                                       !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,*)     "PlotStyle ............................. ",        &
         &                     GV%plotStyle                                      !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    messageText =             "Project ............................... "  //     &
         &                     GV%project(1:Len_trim(GV%project))                !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    messageText =             "PI .................................... "  //     &
         &                     GV%PI(1:Len_trim(GV%PI))                          !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    messageText =             "Observer .............................. "  //     &
         &                     GV%observer(1:Len_trim(GV%observer))              !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    messageText =             "Operator .............................. "  //     &
         &                     GV%oper(1:Len_trim(GV%oper))                      !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    messageText =             "Topology .............................. "  //     &
         &                     GV%topology(1:Len_trim(GV%topology))              !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    If ( GV%purposeIsSet ) Then
       messageText =          "Purpose ............................... "  //     &
            &                  GV%purpose(1:Len_trim(GV%purpose))                !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
    If ( GV%userCommentIsSet ) Then
       messageText =          "Comment ............................... "  //     &    
            &                  GV%userComment(1:Len_trim(GV%userComment))        !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
!!$    messageText = ' angle Unit set: "'//GV%angleUnitSetC(1:Len_trim(GV%angleUnitSetC))//'"'
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
    !
!D    messageText = ' angle Unit stored: "'//GV%angleUnitC(1:Len_trim(GV%angleUnitC))//'"'
!D    Call PakoMessage(priorityI,severityI,command,messageText)
    !
!!$    messageText = ' speed Unit set: "'//GV%speedUnitSetC(1:Len_trim(GV%speedUnitSetC))//'"'
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
    !
!D    messageText = ' speed Unit stored: "'//GV%speedUnitC(1:Len_trim(GV%speedUnitC))//'"'
!D    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    If ( GV%slowRateAMD .Gt. 1 ) Then
       Write (messageText,'(A,F10.0,A)')                                         &
            &                 "slowRate for antenna mount drive ...... ",        &
            &                  GV%slowRateAMD , " [Hz]"                          !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
!!$    Write (messageText,*) 'Pointing. azimuth:             ', GVpCorr%azimuth
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    !
!!$    Write (messageText,*) 'Pointing. elevation:           ', GVpCorr%elevation
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    !
!!$    Write (messageText,*) 'Pointing. P(1):                ', GVpCorr%pointingP(1)
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    !
!!$    Write (messageText,*) 'Pointing. P(2):                ', GVpCorr%pointingP(2)
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    !
!!$    Write (messageText,*) 'Pointing. P(7):                ', GVpCorr%pointingP(7)
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,'(A,F10.2,A)')                                            &
         &                    "Pointing ............ azimuthCorrection ",        &
         &                     GVpCorr%azimuthCorrection , " [arc sec] "         !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,'(A,F10.2,A)')                                            &
         &                    "Pointing .......... elevationCorrection ",        &
         &                     GVpCorr%elevationCorrection , " [arc sec] "       !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,'(A,F10.2,A)')                                            &
         &                    "Focus ............... focusCorrection Z ",        &
         &                     GVfCorr%focusCorrection , " [mm] "                !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    If (isSetfocusCorrectionX) Then
       !
       Write (messageText,'(A,F10.2,A)')                                         &
            &                 "Focus ............... focusCorrection X ",        &
            &                  GVfCorrX%focusCorrection                          !
       Call PakoMessage(priorityI,severityI,command,messageText)
       !
    End If
    !
    If (isSetfocusCorrectionY) Then
       Write (messageText,'(A,F10.2,A)')                                         &
            &                 "Focus ............... focusCorrection Y ",        &
            &                  GVfCorrY%focusCorrection                          !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
!!$    Write (messageText,*) "Pointing. azimuthCorrectionError: ", GVpCorr%azimuthCorrectionError
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    Write (messageText,*) "Pointing. elevationCorrectionError:', GVpCorr%elevationCorrectionError
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    Write (messageText,*) 'Focus.    focusCorrectionError: ', GVfCorr%focusCorrectionError
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    Write (messageText,'(A,F7.3)') '2nd. rotationAngle [deg]:   ', GV%secondaryRotation
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
!!$    Write (messageText,'(A,F6.3)') 'transitionAcceleration [deg/s^2]:   ', GV%transitionAcceleration
!!$    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Write (messageText,*)     "....................................... "         !
    Call PakoMessage(priorityI,severityI,command,messageText)
    !
    Call pakoMessage_level(GV%iPstdio)  ! reset message level
    !
  End Subroutine pakoShow
!!!
!!!
  Subroutine pakoSaveSet(programName,LINE,commandToSave,iUnit,ERROR,             &
       &                 doCorrections)                                          !
    !
    ! *** Variables   ***
    Logical, Optional, Intent(in)  ::  doCorrections
    !
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " modulePakoGlobalVariables, v 1.2.3 ---> SR: pakoSaveSet ")         ! trace execution
    !
    contC = contNN
    !
    B = '\'   ! '
    S = ' '
    CMD =  programName//B//"SET"
    lCMD = lenc(CMD)
    !
    Include 'inc/parameters/saveSetLevel.inc'
    Include 'inc/parameters/saveSetProject.inc'
    Include 'inc/parameters/saveSetPI.inc'
    Include 'inc/parameters/saveSetObserver.inc'
    Include 'inc/parameters/saveSetOperator.inc'
    If (Present(doCorrections)) Then
       If (doCorrections) Then
          !
          !D          Write (6,*) "   --> pakoSaveSet: doCorrections ", doCorrections
          Include 'inc/parameters/saveSetPointing.inc'
          Include 'inc/parameters/saveSetFocus.inc'
          !
       End If
    End If
    Include 'inc/parameters/saveSetTopology.inc'
    Include 'inc/parameters/saveSetSecondaryRotation.inc'
    Include 'inc/parameters/saveSetTransitionAcceleration.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
    !
  End Subroutine pakoSaveSet
!!!
!!!
  Subroutine writeXMLset(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical                 :: errorXML
    !
    Character (len=lenCh2)  :: valueC
    !
    ERROR = .False.
    !
    !D    write (6,*)  "      moduleGlobalVariables --> SR: writeXMLset "
    !
    valueC = GV%project
    !
    !  ** if a specific project ID is associated with source, 
    !     use source projectID:
    !
    If (GV%sourceProjectID .Ne. GPnone) Then
       valueC = GV%sourceProjectID
    End If
    Call pakoXMLwriteElement("PARAM","project",valueC,                           &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%PI
    Call pakoXMLwriteElement("PARAM","PI",valueC,                                &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observer
    Call pakoXMLwriteElement("PARAM","observer",valueC,                          &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%oper
    Call pakoXMLwriteElement("PARAM","operator",valueC,                          &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    valueC = angleChoicesXML(GV%iAU)
    Call pakoXMLwriteElement("PARAM","angleUnit",valueC,                         &
         &                         dataType="char",                              &
         &                         space="after",                                &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLset
!!!
!!!
  Subroutine writeXMLversion
    !
    ! *** Variables   ***
    !! Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical                 :: error
    Logical                 :: errorXML
    !
    Character (len=lenCh2)  :: valueC
    !
    ERROR = .False.
    !
    !D    write (6,*)  "      moduleGlobalVariables --> SR: writeXMLset "
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoDescription",                  &
         &                         error=errorXML)                               !
    !
    valueC = pakoVersionString
    Call pakoXMLwriteElement("PARAM","pakoVersion",valueC,                       &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    valueC = pakoVersionDate
    Call pakoXMLwriteElement("PARAM","pakoVersionDate",valueC,                   &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","pakoDescription",                  &
         &                         space="after",                                &
         &                         error=errorXML)                               !
    !
    Return
    !
  End Subroutine writeXMLversion
!!!
!!!
  Subroutine writeXMLantenna(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical                 :: errorXML
    !
    Real                    :: value
    Character (len=lenCh2)  :: valueC
    Character (len=lenVar2) :: valueComment
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                     &
         &  " modulePakoGlobalVariables, v 1.2.1 ---> SR: writeXMLantenna ")     ! trace execution
    !D     Write (6,*)  "      moduleGlobalVariables --> SR: writeXMLantenna "
    !D     Write (6,*)  "                   GV%slowRateAMD: ", GV%slowRateAMD
    !
    !
    If (    Abs(GV%secondaryRotation).Gt.0.01                                    &
         & .Or.                                                                  &
         &      GV%slowRateAMD.Gt.1                                              &
         & .Or.                                                                  &
         &  (Abs(vars(iValue)%transitionAcceleration-                            &
         &       vars(iDefault)%transitionAcceleration)                          &
         &    .Gt.0.001) )                                                       &
         &  Then                                                                 !
       !
       Call pakoXMLwriteStartElement("RESOURCE","antenna",                       &
            &                         space="before",                            &
            &                         error=errorXML)                            !
       !
       If (                                                                      &
         &      GV%slowRateAMD.Gt.1                                              &
         & .Or.                                                                  &
            (Abs(vars(iValue)%transitionAcceleration-                            &
         &       vars(iDefault)%transitionAcceleration)                          &
         &    .Gt.0.001) )                                                       &
         &  Then                                                                 !
          !
          Call pakoXMLwriteStartElement("RESOURCE","mountDrive",                 &
               &                         error=errorXML)                         !
          !
          If (                                                                   &
               &      GV%slowRateAMD.Gt.1 )                                      &
               &  Then                                                           !
             value = GV%slowRateAMD
             Write (valueC,*) Real(value)
             Call pakoXMLwriteElement("PARAM","slowRate",valueC,                 &
                  &                         "Hz", "float",                       &
                  &                         error=errorXML)                      !
          End If
          !
          If (                                                                   &
               (Abs(vars(iValue)%transitionAcceleration-                         &
               &       vars(iDefault)%transitionAcceleration)                    &
               &    .Gt.0.001) )                                                 &
               &  Then                                                           !
             value = GV%transitionAcceleration
             Write (valueC,*) Real(value*deg)
             Write (valueComment,*) Real(value), " ", au%degree, "/s^2"
             !
             Call pakoXMLwriteStartElement("RESOURCE","azimuth",                 &
                  &                         error=errorXML)                      !
             Call pakoXMLwriteElement("PARAM","xAccel",valueC,                   &
                  &                         "rad.s-2", "float",                  &
                  &                         comment=valueComment,                &
                  &                         error=errorXML)                      !
             Call pakoXMLwriteEndElement  ("RESOURCE","azimuth",                 &
                  &                         error=errorXML)                      !
             !
             Call pakoXMLwriteStartElement("RESOURCE","elevation",               &
                  &                         error=errorXML)                      !
             Call pakoXMLwriteElement("PARAM","xAccel",valueC,                   &
                  &                         "rad.s-2", "float",                  &
                  &                         comment=valueComment,                &
                  &                         error=errorXML)                      !
             Call pakoXMLwriteEndElement  ("RESOURCE","elevation",               &
                  &                         error=errorXML)                      !
             !
          End If
          !
          Call pakoXMLwriteEndElement  ("RESOURCE","mountDrive",                 &
               &                         error=errorXML)                         !
          !
       End If
       !
       If  (Abs(GV%secondaryRotation).Gt.0.01) Then
          Call pakoXMLwriteStartElement("RESOURCE","secondary",                  &
               &                         error=errorXML)                         !
          !
          value = GV%secondaryRotation
          Write (valueC,*) Real(value*deg)
          Write (valueComment,*) Real(value), " ", au%degree
          Call pakoXMLwriteElement("PARAM","rotationAngle",valueC,               &
               &                         "rad", "float",                         &
               &                         comment=valueComment,                   &
               &                         error=errorXML)                         !
          !
          Call pakoXMLwriteEndElement  ("RESOURCE","secondary",                  &
               &                         error=errorXML)                         !
          !
       End If
       !
       Call pakoXMLwriteEndElement  ("RESOURCE","antenna",                       &
            &                         space="after",                             &
            &                         error=errorXML)                            !
       !
    End If
    !
    Return
    !
  End Subroutine writeXMLantenna
!!!
!!!
End Module modulePakoGlobalVariables






