!
!     Id: indexCommandOption.f,v 1.2.0 2012-01-26 Hans Ungerechts
!
!! TBD 1.2: --> module
!! TBD:     review for F90 style 
!
      SUBROUTINE indexCommmandOption                                             &
           (command,option,commandFull,optionFull,                               &
            commandIndex,optionIndex,iCommand,iOption,                           &
            errorNotFound,errorNotUnique)                                        !
!
!     <doc>  
!     SR searches the SIC vocabulary INCLUDED below for 
!        a unique combination of command and option
!     INPUT:   command       (substring of) command
!              option        (substring of) option
!     OUTPUT:  commandFull   full command as in vocab
!              optionFull    full option as in vocab
!              comandIndex   index of command in vocab
!              optionIndex   index of option in vocab
!              iCommand      sequential number of command
!              iOption       number of option
!                            as required by SIC_I4, SIC_R4 etc.
!              errorNotFound 
!              errorNotUnique
!     <\doc>
!     <dev>
!     TBD:    vocabulary should be passed as parameter
!     TBD:    entry for call with minimal set of parameters
!     TBD:    replace use of (1:noBlanks(...)) with more general feature
!     <\dev>
!
      IMPLICIT NONE
!
      CHARACTER*12 command,option
      CHARACTER*12 commandFull,optionFull
      INTEGER      commandIndex,optionIndex,iCommand,iOption
      LOGICAL      errorNotFound,errorNotUnique
!
      INCLUDE 'inc/commands/pakoVocab.inc'
!
!     local tables of numbers of commands and options
!        --- these are initialized on first call of this SR:
!
      INTEGER iCommandLoc(mcom),iOptionLoc(mcom)
      LOGICAL isInitializedLoc
!
      COMMON /indexCommandOptionCommon/                                          &
              iCommandLoc,iOptionLoc,                                            &
              isInitializedLoc                                                   !
!
      DATA    isInitializedLoc /.false./
!
      INTEGER ii
      INTEGER noBlanks
!
      LOGICAL      errorNotFoundCommand, errorNotUniqueCommand      
      LOGICAL      errorNotFoundOption,  errorNotUniqueOption
!
      errorNotFoundCommand  = .FALSE.
      errorNotUniqueCommand = .FALSE.    
      errorNotFoundOption   = .FALSE.
      errorNotUniqueOption  = .FALSE.
      errorNotFound         = .FALSE.
      errorNotUnique        = .FALSE.
!
!CDD      WRITE (6,*) "                                  "
!CDD      WRITE (6,*) " SR: indexCommandOption           "
!CDD      WRITE (6,*) "                      000000000111"
!CDD      WRITE (6,*) "                      123456789012"
!CDD      WRITE (6,*) "     command      = ->", command ,"<-"
!CDD      WRITE (6,*) "     option       = ->", option  ,"<-"
!
      IF (.NOT. isInitializedLoc) THEN 
!
!CDD         WRITE (6,*) "     --> Initializing   "
!CDD         WRITE (6,*) "                                  "
!
!        we assume the first item in vocab is a command:
         iCommandLoc(1) = 1
         iOptionLoc(1)  = 0
!
         DO ii = 2,mcom,1
            IF (vocab(ii)(1:1) .NE.  '/') THEN
!              not an option: incr n of command; set n of option to 0
               iCommandLoc(ii) = iCommandLoc(ii-1)+1
               iOptionLoc(ii)  = 0
            ELSE
!                  an option: incr n of option
               iCommandLoc(ii) = iCommandLoc(ii-1)
               iOptionLoc(ii)  = iOptionLoc(ii-1)+1
            END IF               
         END DO
!
!CDD         DO ii = 1,mcom,1
!CDD            WRITE (6,*) ii, "   ", vocab(ii), 
!CDD $           iCommandLoc(ii), iOptionLoc(ii)
!CDD         END DO
!
         isInitializedLoc = .true.
!
      END IF
!
!**** end of initialization part
!****
!
!CDD      WRITE (6,*) "     commandNoB   = ->", 
!CDD     $     command(1:noBlanks(command)),"<-", noBlanks(command)
!
!     search for command  in vocab
      commandIndex = 0
      DO ii = 1,mcom,1
         IF (index(vocab(ii)(1:1),"/") .EQ. 0) THEN
!              not an option: must be next command!
            IF (index(vocab(ii),command(1:noBlanks(command))) .GE. 1) THEN
               IF (commandIndex .EQ. 0) THEN
                  commandIndex = ii
               ELSE
                  errorNotUniqueCommand = .true.
               END IF
            END IF
         END IF
      END DO
      errorNotFoundCommand =  commandIndex .EQ. 0
!
!CDD      WRITE (6,*) "     commandIndex = ", commandIndex
   
!CDD        WRITE (6,*) "     optionNoB    = ->", 
!CDD     $     option(1:noBlanks(option)),"<-", noBlanks(option)
!
!     search for option in vocab, starting after command found:
      optionIndex  = 0
      IF (commandIndex .GE. 1) THEN
         DO ii = commandIndex+1,mcom,1
            IF (index(vocab(ii)(1:1),"/") .EQ. 0) THEN
!              not an option: must be next command, therefore exit loop!
               GO TO 100
            END IF
            IF (index(vocab(ii),option(1:noBlanks(option))) .GE. 1) THEN
               IF (optionIndex .EQ. 0) THEN
                  optionIndex = ii
               ELSE
                  errorNotUniqueOption = .true.
               END IF
            END IF
         END DO
 100     errorNotFoundOption =  optionIndex .EQ. 0
      END IF         
!
!CDD      WRITE (6,*) "     optionIndex  = ", optionIndex
!
      IF (errorNotFoundCommand) THEN
         commandFull = ""
         iCommand    = 0
      ELSE
         commandFull =       vocab(commandIndex)
         iCommand    = iCommandLoc(commandIndex)
      END IF
      IF (errorNotFoundOption) THEN
         optionFull  = ""
         iOption     = 0
      ELSE
         optionFull  =       vocab(optionIndex)
         iOption     =  iOptionLoc(optionIndex)
      END IF
      errorNotFound  = errorNotFoundCommand  .OR.  errorNotFoundOption
      errorNotUnique = errorNotUniqueCommand .OR.  errorNotUniqueOption
!
!CDD      IF (errorNotFoundCommand) THEN
!CDD         WRITE (6,*) " errorNotFoundCommand  = ", errorNotFoundCommand
!CDD      END IF
!CDD      IF (errorNotFoundOption) THEN
!CDD         WRITE (6,*) " errorNotFoundOption   = ", errorNotFoundOption
!CDD      END IF
!CDD      IF (errorNotFound) THEN
!CDD         WRITE (6,*) " errorNotFound         = ", errorNotFound
!CDD      END IF
!CDD      IF (errorNotUniqueCommand) THEN
!CDD         WRITE (6,*) " errorNotUniqueCommand = ", errorNotUniqueCommand
!CDD      END IF
!CDD      IF (errorNotUniqueOption) THEN
!CDD         WRITE (6,*) " errorNotUniqueOption  = ", errorNotUniqueOption
!CDD      END IF
!CDD      IF (errorNotUnique) THEN
!CDD         WRITE (6,*) " errorNotUnique        = ", errorNotUnique
!CDD      END IF
!CDD      WRITE (6,*) "     commandFull  = ->", commandFull ,"<-"
!CDD      WRITE (6,*) "     iCommand     =   ", iCommand    
!CDD      WRITE (6,*) "     optionFull   = ->", optionFull    ,"<-"
!CDD      WRITE (6,*) "     iOption      =   ", iOption    
!
      RETURN
      END SUBROUTINE indexCommmandOption
!
      INTEGER FUNCTION noBlanks(text)
!
!<DOC>  
!     F  counts the number of non-blank characters in text
!        starting from the first
!     INPUT:   text          string
!     OUTPUT:  noBlanks      
!<\DOC>
!<DEV>
!      TBD:    should also delete blanks at beginning of text
!      TBD:    should retrun 1 and flag error if there are 
!              only blanks in text
!<\DEV>
!
      IMPLICIT NONE
!
      CHARACTER*12 text
!
      INTEGER ii
!
      noBlanks = 0
!
      DO ii = 1,12,1
         IF (text(ii:ii).NE." ") THEN
            noBlanks = ii
         END IF
      END DO
!
      RETURN
      END FUNCTION noBlanks
