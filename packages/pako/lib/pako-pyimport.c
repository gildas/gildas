
/* Define entry point "initpypako" for command "import pypako" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(pako);
