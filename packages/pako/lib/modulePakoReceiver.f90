!
! Id: modulePakoReceiver.f90,v 1.2.5    2016-09-05        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.5    2016-04-04        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.3.3  2016-01-15        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.3.3  2014-09-02        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.3.2  2014-04-29        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.3.1  2014-02-10        Hans Ungerechts
! Id: modulePakoReceiver.f90,v 1.2.3.1  2014-01-23        Hans Ungerechts
!     modulePakoReceiver.f90,v 1.2.3    2013-08-26, 28    Hans Ungerechts
!                              upgrade of E150 to H&V 2SB LO LI UI UO
!
!     modulePakoReceiver.f90,v 1.2.2  2013-02-06, 2013-01-18 Hans Ungerechts
!     with updates from:
!     modulePakoReceiver.f90,v 1.1.14 2012-10-22 Hans Ungerechts
!
!     from:                    1.2.1  2012-07-18 Hans Ungerechts
!     based on:
!     modulePakoReceiver.f90,v 1.1.12 2012-06-01 Hans Ungerechts
!     
! <DOCUMENTATION name="modulePakoReceiver">
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Receiver
!   </FAMILY>
!   <SIBLINGS>
!
!   </SIBLINGS>        
!
!   Pako module for command: Receiver
!
! </DOCUMENTATION> <!-- name="modulePakoReceiver" -->
!
Module modulePakoReceiver
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoDisplayText
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  !
  Use gbl_message
  Use gkernel_interfaces
  !
  Implicit None
  Save
  Private
  Public :: receiver
  Public :: queryReceiver
  Public :: saveReceiver
  Public :: writeXMLreceiver
  !
  Public :: listRX, varReceiver
  Public :: nEsbIF, EMIRsbType, EMIRsubBands
  Public :: nEMIRsbCoax, esbCoax
  !
  Public :: displayReceiver
  !
!!$  Public :: nDimHeraPixel, heraPixelType, heraPixel
!!$  Public :: receiver, queryReceiver
!!$  Public :: displayReceiver, saveReceiver, writeXMLreceiver
!!$  Public :: checkBolometerArray
  !     
  ! *** variables for command Receiver ***
  Include 'inc/variables/variablesReceiver.inc'
!!!   
!!!   
Contains
!!!   
!!!
  Subroutine receiver(programName,line,command,error)
    !
    ! *** arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=lenLine)  ::  cInputLongFrequency
    !
    Real(Kind=kindSingle)  ::  tempCold
    Integer                ::  countRX
    Character(len=lenCh)   ::  EMIRlist
    Logical                ::  inconsistentReceiverOffsets,IsOpen
    !
    Integer                ::  nArgument
    !
    Integer                ::  l1, l2, iiSubBand, iBand, iBand1, iBand2
    Real                   ::  loss
    !
    Character(len=lenCh)   ::  polarization, sideband, subband, subbandsJJ
    !
    Real(kind=kindDouble)  ::  fLO         =  GPnoneD
    Real(kind=kindDouble)  ::  fLSB        =  GPnoneD
    Real(kind=kindDouble)  ::  fUSB        =  GPnoneD
    Real(kind=kindDouble)  ::  fSubBandLO  =  GPnoneD
    Real(kind=kindDouble)  ::  fSubBandLI  =  GPnoneD
    !
    Logical                  ::  foundPixel
    Integer                  ::  iUnitR, ier, ioStatus, indexTrim
    Character (len=lenLine)  ::  lineIn, lineInTrim
    Integer                  ::  len
    !
    Character(len=lenLine), Dimension(4), Parameter ::                           &
         &  NIKApixelFiles =                                                     &
         &               (/                                                      &
         &  "NIKAkidOffsetA.config                                          " ,  &
         &  "NIKAkidOffsetB.config                                          " ,  &
         &  "/ncsServer/mrt/ncs/configuration/receiver/NIKAkidOffsetA.config" ,  &
         &  "/ncsServer/mrt/ncs/configuration/receiver/NIKAkidOffsetB.config"    &
!!$         &  "~/ncsServer/mrt/ncs/configuration/receiver/NIKAkidOffsetA.config",  &
!!$         &  "~/ncsServer/mrt/ncs/configuration/receiver/NIKAkidOffsetB.config"   &
         &               /)                                                      !
    !
    Integer                  ::  pixel
    Real                     ::  xx, yy
    !
    ! *** ranges *** 
    Include 'inc/ranges/rangesReceiver.inc'
    !
    ERROR = .False.
    !
!!$    Call pako_message(seve%t,programName,                                        &
!!$         &    " module Receiver, 1.2.3.3  2014-08-28 ---> SR: Receiver ")        ! this allows to trace execution
    Call pako_message(seve%t,programName,                                        &
         &    " module Receiver, 1.2.5    2016-09-05 ---> SR: Receiver ")        ! this allows to trace execution
    !
    !D     Write (6,*) " module receiver ---> SR: receiver "
    !D     Write (6,*) "        gv%limitCheck : ", gv%limitCheck
    !D     Write (6,*) "        gv%EMIRcheck :  ", gv%EMIRcheck
    !
!!$    Open  ( 88, File='receiver.nml', Delim='APOSTROPHE')
!!$    Write ( Unit=88, NML=nmlReceiver )
!!$    Close ( 88 )
!!$    Open  ( 88, File='receiverConfig.nml', Delim='APOSTROPHE')
!!$    Read  ( Unit=88, NML=nmlReceiver )
!!$    Close ( 88 )
    !
    ! *** initialize ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/receiverSetDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn,:) = vars(iValue,:)
    End If
    !
    ! *** check number of Parameters: 0, 4 --> at  most 4 allowed   ***
    ! *** read Parameters, Options (to detect errors)               ***
    ! *** and check for validity and ranges                         ***
    Call checkNofParameters(command,                                  &
         &     0, 4,                                                  &
         &     nArguments,                                            &
         &     errorNumber)
    ERROR = ERROR .Or. errorNumber
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/options/detectOptionsReceiver.inc'
       Include 'inc/parameters/parametersReceiver.inc'      
       !D        Write (6,*) vars(iIn,iRec)
       ! TBD: options /gainBolo(meter) /channel for bolometer
       Include 'inc/options/readOptionsReceiver.inc'
       !
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                       &
            &        (command,option,commandFull,optionFull,          &
            &        commandIndex,optionIndex,iCommand,iOption,       &
            &        errorNotFound,errorNotUnique)
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/receiverSetDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersReceiver.inc'
       ! TBD: options /gainBolo(meter) /channel for bolometer
       Include 'inc/options/readOptionsReceiver.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    !
    !  **  EMIR: generate list of sub bands                                        !!  EMIR upgrade 2011-11-11
    !
    If (GV%doDebugMessages) Then
       !
       Write (6,*) " "
       Write (6,*) " --> Generation of EMIRsubBands EMIRsubBandsBis esbAll esbWish esbCoax v1.1.14 2012-10 "
       Write (6,*) " "
       !
    End If
    !
    !   *  EMIR: special case:
    !   *  EMIR: check if we use E150 and (E330 USB or E090 USB)                    !! E150 sw. box upgrade 2012-09 
    !
    requestUSB(:,:) = .False.
    requestLSB(:,:) = .False.
    !
    !D  Write (6,*) " reset requestUSB(:,:) --> ", requestUSB(:,:)
    !D  Write (6,*) " reset requestLSB(:,:) --> ", requestLSB(:,:)
    !
    If (vars(iIn,iE090)%isConnected) Then 
       !
       If (        Index(vars(iIn,iE090)%horizontalSubbands,esb%ui(1:3)) .Ge.1   &
            & .Or. Index(vars(iIn,iE090)%horizontalSubbands,esb%uo(1:3)) .Ge.1   &
            &  )   Then                                                          !
          requestUSB(iE090,ihorizontalPol) = .True.
       End If
       If (        Index(vars(iIn,iE090)%verticalSubbands,esb%ui(1:3)) .Ge.1     &
            & .Or. Index(vars(iIn,iE090)%verticalSubbands,esb%uo(1:3)) .Ge.1     &
            &  )   Then                                                          !
          requestUSB(iE090,iverticalPol) = .True.
       End If
       !
       If (        Index(vars(iIn,iE090)%horizontalSubbands,esb%li(1:3)) .Ge.1   &
            & .Or. Index(vars(iIn,iE090)%horizontalSubbands,esb%lo(1:3)) .Ge.1   &
            &  )   Then                                                          !
          requestLSB(iE090,ihorizontalPol) = .True.
       End If
       If (        Index(vars(iIn,iE090)%verticalSubbands,esb%li(1:3)) .Ge.1     &
            & .Or. Index(vars(iIn,iE090)%verticalSubbands,esb%lo(1:3)) .Ge.1     &
            &  )   Then                                                          !
          requestLSB(iE090,iverticalPol) = .True.
       End If
       !
    End If
    !
    If (vars(iIn,iE150)%isConnected) Then 
       !
       If (         Index(vars(iIn,iE150)%horizontalSubbands,esb%ui(1:3)) .Ge.1  &
            & .And. Index(vars(iIn,iE150)%verticalSubbands,esb%ui(1:3)) .Ge.1    &
            &  )    Then                                                         !
          requestUSB(iE150,ihorizontalPol) = .True.
          requestUSB(iE150,iverticalPol)   = .True.
       End If
       !
       If (         Index(vars(iIn,iE150)%horizontalSubbands,esb%li(1:3)) .Ge.1  &
            & .And. Index(vars(iIn,iE150)%verticalSubbands,esb%li(1:3)) .Ge.1    &
            &  )    Then                                                         !
          requestLSB(iE150,ihorizontalPol) = .True.
          requestLSB(iE150,iverticalPol)   = .True.
       End If
       !
    End If
    !
    If (vars(iIn,iE300)%isConnected) Then 
       !
       If (        Index(vars(iIn,iE300)%horizontalSubbands,esb%ui(1:3)) .Ge.1   &
            & .Or. Index(vars(iIn,iE300)%horizontalSubbands,esb%uo(1:3)) .Ge.1   &
            &  )   Then                                                          !
          requestUSB(iE300,ihorizontalPol) = .True.
       End If
       If (        Index(vars(iIn,iE300)%verticalSubbands,esb%ui(1:3)) .Ge.1     &
            & .Or. Index(vars(iIn,iE300)%verticalSubbands,esb%uo(1:3)) .Ge.1     &
            &  )   Then                                                          !
          requestUSB(iE300,iverticalPol) = .True.
       End If
       !
       If (        Index(vars(iIn,iE300)%horizontalSubbands,esb%li(1:3)) .Ge.1   &
            & .Or. Index(vars(iIn,iE300)%horizontalSubbands,esb%lo(1:3)) .Ge.1   &
            &  )   Then                                                          !
          requestLSB(iE300,ihorizontalPol) = .True.
       End If
       If (        Index(vars(iIn,iE300)%verticalSubbands,esb%li(1:3)) .Ge.1     &
            & .Or. Index(vars(iIn,iE300)%verticalSubbands,esb%lo(1:3)) .Ge.1     &
            &  )   Then                                                          !
          requestLSB(iE300,iverticalPol) = .True.
       End If
       !
    End If
    !
    If (GV%doDebugMessages) Then
       !
       If (requestUSB(iE090,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE090)%name,                           &
               &      " Horizontal we request USB: "                             !
          Write (6,*) "   requestUSB(iE090,ihorizontalPol): ",                   &
               &          requestUSB(iE090,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestUSB(iE090,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE090)%name,                           &
               &      " Vertical we request USB: "                               !
          Write (6,*) "   requestUSB(iE090,iverticalPol): ",                     &
               &          requestUSB(iE090,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE090,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE090)%name,                           &
               &      " Horizontal we request LSB: "                             !
          Write (6,*) "   requestLSB(iE090,ihorizontalPol): ",                   &
               &          requestLSB(iE090,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE090,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE090)%name,                           &
               &      " Vertical we request LSB: "                               !
          Write (6,*) "   requestLSB(iE090,iverticalPol): ",                     &
               &          requestLSB(iE090,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
       If (requestUSB(iE150,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE150)%name,                           &
               &      " Horizontal we request USB: "                             !
          Write (6,*) "   requestUSB(iE150,ihorizontalPol): ",                   &
               &          requestUSB(iE150,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestUSB(iE150,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE150)%name,                           &
               &      " Vertical we request USB: "                               !
          Write (6,*) "   requestUSB(iE150,iverticalPol): ",                     &
               &          requestUSB(iE150,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE150,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE150)%name,                           &
               &      " Horizontal we request LSB: "                             !
          Write (6,*) "   requestLSB(iE150,ihorizontalPol): ",                   &
               &          requestLSB(iE150,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE150,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE150)%name,                           &
               &      " Vertical we request LSB: "                               !
          Write (6,*) "   requestLSB(iE150,iverticalPol): ",                     &
               &          requestLSB(iE150,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
       If (requestUSB(iE300,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE300)%name,                           &
               &      " Horizontal we request USB: "                             !
          Write (6,*) "   requestUSB(iE300,ihorizontalPol): ",                   &
               &          requestUSB(iE300,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestUSB(iE300,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE300)%name,                           &
               &      " Vertical we request USB: "                               !
          Write (6,*) "   requestUSB(iE300,iverticalPol): ",                     &
               &          requestUSB(iE300,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE300,ihorizontalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE300)%name,                           &
               &      " Horizontal we request LSB: "                             !
          Write (6,*) "   requestLSB(iE300,ihorizontalPol): ",                   &
               &          requestLSB(iE300,ihorizontalPol)                       !
          Write (6,*) " "
       End If
       !
       If (requestLSB(iE300,iverticalPol)) Then
          Write (6,*) "   For ", vars(iIn,iE300)%name,                           &
               &      " Vertical we request LSB: "                               !
          Write (6,*) "   requestLSB(iE300,iverticalPol): ",                     &
               &          requestLSB(iE300,iverticalPol)                         !
          Write (6,*) " "
       End If
       !
    End If
    !
    !
    !   *  reset:
    !
    iiSubBand = 0
    EMIRsubBands(:)    = esbNone
    EMIRsubBandsBis(:) = esbNone
    !
    esbAll   =  esbAllParameter 
    esbWish  =  esbWishParameter
    esbCoax  =  esbCoaxParameter
    !
    !   *  go through all EMIR (sub)bands:
    !
    Do ii = iE090, iE300, 1                                                      !  4 EMIR bands
       !
       If (vars(iIn,ii)%isConnected) Then
          !
          Do jj = 1, 2, 1                                                        !  2 polarizations
             !
             Select Case (jj)
             Case (1) 
                polarization  =  pol%hor
                subbandsJJ      =  vars(iIn,ii)%horizontalSubbands
             Case (2) 
                polarization  =  pol%ver
                subbandsJJ      =  vars(iIn,ii)%verticalSubbands
             End Select
             !D Write (6,*)  "      jj, polarization, subbandsJJ: ", jj, polarization, subbandsJJ
             !
             Do kk = 1, 4, 1                                                     !  up to 4 sub/side bands
                !
                Select Case (kk)
                Case (1)
                   sideband      =  esb%lsb
                   subband       =  esb%lo
                Case (2) 
                   sideband      =  esb%lsb
                   subband       =  esb%li
                Case (3) 
                   sideband      =  esb%usb
                   subband       =  esb%ui
                Case (4) 
                   sideband      =  esb%usb
                   subband       =  esb%uo
                End Select
                !D Write (6,*) "      kk, sideband, subband: ", kk, sideband, subband
                !
                If     (    Index(subbandsJJ,sideband(1:3)) .Ge.1 .Or.              &
                     &      Index(subbandsJJ, subband(1:3)) .Ge.1                   &
                     & ) Then                                                       !
                   !
                   iiSubBand = iiSubBand+1
                   !D Write (6,*) " iiSubBand incremented to: ", iiSubBand
                   !
                   If (iiSubBand.Le.nEsbIF) Then                                 !  !!  for general purposes
                      EMIRsubBands(iiSubBand)%i            =  iiSubband
                      EMIRsubBands(iiSubBand)%bandName     =  vars(iIn,ii)%name
                      EMIRsubBands(iiSubBand)%polarization =  polarization
                      EMIRsubBands(iiSubBand)%sideband     =  sideband
                      EMIRsubBands(iiSubBand)%subband      =  subband
                      EMIRsubBands(iiSubBand)%isRequested  =  .True.
                   End If
                   !
                   !                                                             !  !! NOTE/TBD: probably could 
                   !                                                             !  !! directly use esbWish here?
                   If (iiSubBand.Le.nEMIRsbWish) Then                            !  !!  try up to 8, see esbWish
                      EMIRsubBandsBis(iiSubBand)%i            =  iiSubband       !  !!  (sub bands "wished")
                      EMIRsubBandsBis(iiSubBand)%bandName     =  vars(iIn,ii)%name
                      EMIRsubBandsBis(iiSubBand)%polarization =  polarization
                      EMIRsubBandsBis(iiSubBand)%sideband     =  sideband
                      EMIRsubBandsBis(iiSubBand)%subband      =  subband
                      EMIRsubBandsBis(iiSubBand)%isRequested  =  .True.
                   End If
                   !
                End If
                !
             End Do
             !
          End Do
          !
       End If
       !
    End Do
    !
    
    Do ii = 1, nEMIRsbWish, 1                                                       !!  changed for E150 sw. box upgrade 2012-09
       !
       If (         EMIRsubbandsBis(ii)%isRequested                              &
            &       ) Then                                                       !
          !
          esbWish(ii) = EMIRsubBandsBis(ii)                                      !  !!  sub bands "wished"
          !                                                                      !  !!  try up to 8
          !
          If (GV%doDebugMessages) Then
             Write (6,*)   "   ii: ", ii
             Write (6,*)   "   esbWish "
             Write (6,110) esbWish(ii)
             Write (6,*) "  esbWish(ii)%bandName -->",esbWish(ii)%bandName,"<--"
             Write (6,*) "  rec%E150             -->",rec%E150,"<--"
          End If
          !
          Do jj = 1, nEMIRsbAll, 1
             !
             If (     (      esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
                  &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
                  &    .And. esbWish(ii)%sideband    .Eq.esbAll(jj)%sideband     &
                  &    .And. esbWish(ii)%subband     .Eq.esbAll(jj)%subband      &  !!  this rules out E150, which has been      !!  TBD E150  ??
                  &   )                                                          &  !!  reset to esbAll(jj)%subband --> GPnone   !!  TBD E150  ??
                  &  ) Then                                                      !
                !
                esbAll(jj)%isRequested = .True.
                !D                 Write (6,*)   "   esbAll "
                !D                 Write (6,110) esbAll(jj)
                !
                kk = esbAll(jj)%i4                                                  !!  %i4 codes to which Coax cable (1 to 4)
                !                                                                   !!  this subband goes
                !                                                                   !!  kk is safe in range 1 to 4 if %i4 is OK
                esbCoax(kk)     =  esbAll(jj)
                esbCoax(kk)%i   =  kk                                               !!  --> %i   codes number (1 to  8) of Coax
                esbWish(ii)%i8  =  esbAll(jj)%i8                                    !!  --> %i8  codes number (1 to 16) of EMIR output
                esbWish(ii)%i4  =  esbAll(jj)%i4                                    !!  --> %i4  codes number (1 to  4) of Coax
                !
!!$                If (.Not. esbWish(ii)%bandName    .Eq.rec%E150) Then                !!  for E090, E230, E330:        !!  obsolete with E150 upgrade 2013-09
                esbCoax(kk+4)   = esbCoax(kk)                                    !!  outer subbands on Coax 5 to 8
                esbCoax(kk+4)%i = kk+4
                If      (esbCoax(kk)%subband.Eq.esb%li) Then
                   esbCoax(kk+4)%subband = esb%lo
                Else If (esbCoax(kk)%subband.Eq.esb%ui) Then
                   esbCoax(kk+4)%subband = esb%uo
                End If
!!$                End If                                                                                               !!  obsolete with E150 upgrade 2013-09
                !
             End If
             !
             !
!!$             If (     (      esbWish(ii)%bandName    .Eq.rec%E150                &                                   !!  obsolete with E150 upgrade 2013-09
!!$                  &    .And. esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
!!$                  &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
!!$                  &   )                                                          &
!!$                  &  ) Then                                                      !
!!$                !
!!$                If      (       (      (      requestUSB(iE150,ihorizontalPol)   &  !! new 2012-09, 10
!!$                     &                  .And. requestUSB(iE150,iverticalPol)     &
!!$                     &                 )                                         &
!!$                     &            .Or. (      requestLSB(iE150,ihorizontalPol)   &  !! new 2012-09, 10
!!$                     &                  .And. requestLSB(iE150,iverticalPol)     &
!!$                     &                 )                                         &
!!$                     &          )                                                &
!!$                     &    .And. (      requestUSB(iE300,ihorizontalPol)          &
!!$                     &           .And. requestUSB(iE300,iverticalPol)            &
!!$                     &          )                                                &
!!$                     &  ) Then                                                   !  !! TBD
!!$                   ! 
!!$                   If (jj.Eq.10 .or. jj.Eq.12) Then
!!$                      !
!!$                      esbAll(jj)%isRequested = .True.
!!$                      !D                 Write (6,*)   "   esbAll "
!!$                      !D                 Write (6,110) esbAll(jj)
!!$                      !
!!$                      kk = esbAll(jj)%i4                                            !!  %i4 codes to which Coax cable (1 to 4)
!!$                      !                                                             !!  this subband goes
!!$                      !                                                             !!  kk is safe in range 1 to 4 if %i4 is OK
!!$                      esbCoax(kk)     =  esbAll(jj)
!!$                      esbCoax(kk)%i   =  kk                                         !!  --> %i   codes number (1 to  8) of Coax
!!$                      esbWish(ii)%i8  =  esbAll(jj)%i8                              !!  --> %i8  codes number (1 to 16) of EMIR output
!!$                      esbWish(ii)%i4  =  esbAll(jj)%i4                              !!  --> %i4  codes number (1 to  4) of Coax
!!$                      !
!!$                      If (      esbWish(ii)%bandName    .Eq.rec%E150) Then          !!  for E150: 
!!$                         esbCoax(kk)%sideband = esbWish(ii)%sideband                !!  sideband and 
!!$                         esbCoax(kk)%subband  = esbWish(ii)%subband                 !!  subband from user wish
!!$                      End If
!!$                      !
!!$                   End If
!!$                   !
!!$                Else If (       (      (      requestUSB(iE150,ihorizontalPol)   &  !! new 2012-09, 10
!!$                     &                  .And. requestUSB(iE150,iverticalPol)     &
!!$                     &                 )                                         &
!!$                     &            .Or. (      requestLSB(iE150,ihorizontalPol)   &  !! new 2012-09, 10
!!$                     &                  .And. requestLSB(iE150,iverticalPol)     &
!!$                     &                 )                                         &
!!$                     &          )                                                &
!!$                     &    .And. (      requestUSB(iE090,ihorizontalPol)          &
!!$                     &           .And. requestUSB(iE090,iverticalPol)            &
!!$                     &          )                                                &
!!$                     &  ) Then                                                   !  !! TBD
!!$                   !
!!$!!OLD                Else If (       (      requestUSB(iE150,ihorizontalPol)          &  !! new 2012-09
!!$!!OLD                     &           .And. requestUSB(iE150,iverticalPol)            &
!!$!!OLD                     &          )                                                &
!!$!!OLD                     &    .And. (      requestUSB(iE090,ihorizontalPol)          &
!!$!!OLD                     &           .Or.  requestUSB(iE090,iverticalPol)            &
!!$!!OLD                     &          )                                                &
!!$!!OLD                     &  ) Then                                                   !  !!  TBD
!!$!!OLD                   !
!!$                   !
!!$                   If (jj.Eq.10 .or. jj.Eq.12) Then
!!$                      !
!!$                      esbAll(jj)%isRequested = .True.
!!$                      !D                 Write (6,*)   "   esbAll "
!!$                      !D                 Write (6,110) esbAll(jj)
!!$                      !
!!$                      kk = esbAll(jj)%i4                                            !!  %i4 codes to which Coax cable (1 to 4)
!!$                      !                                                             !!  this subband goes
!!$                      !                                                             !!  kk is safe in range 1 to 4 if %i4 is OK
!!$                      esbCoax(kk)     =  esbAll(jj)
!!$                      esbCoax(kk)%i   =  kk                                         !!  --> %i   codes number (1 to  8) of Coax
!!$                      esbWish(ii)%i8  =  esbAll(jj)%i8                              !!  --> %i8  codes number (1 to 16) of EMIR output
!!$                      esbWish(ii)%i4  =  esbAll(jj)%i4                              !!  --> %i4  codes number (1 to  4) of Coax
!!$                      !
!!$                      If (      esbWish(ii)%bandName    .Eq.rec%E150) Then          !!  for E150: 
!!$                         esbCoax(kk)%sideband = esbWish(ii)%sideband                !!  sideband and 
!!$                         esbCoax(kk)%subband  = esbWish(ii)%subband                 !!  subband from user wish
!!$                      End If
!!$                      !
!!$                   End If
!!$                   !
!!$                Else
!!$                   !
!!$                   If (jj.Eq.9 .or. jj.Eq.11) Then
!!$                      !
!!$                      esbAll(jj)%isRequested = .True.
!!$                      !D                 Write (6,*)   "   esbAll "
!!$                      !D                 Write (6,110) esbAll(jj)
!!$                      !
!!$                      kk = esbAll(jj)%i4                                            !!  %i4 codes to which Coax cable (1 to 4)
!!$                      !                                                             !!  this subband goes
!!$                      !                                                             !!  kk is safe in range 1 to 4 if %i4 is OK
!!$                      esbCoax(kk)     =  esbAll(jj)
!!$                      esbCoax(kk)%i   =  kk                                         !!  --> %i   codes number (1 to  8) of Coax
!!$                      esbWish(ii)%i8  =  esbAll(jj)%i8                              !!  --> %i8  codes number (1 to 16) of EMIR output
!!$                      esbWish(ii)%i4  =  esbAll(jj)%i4                              !!  --> %i4  codes number (1 to  4) of Coax
!!$                      !
!!$                      If (      esbWish(ii)%bandName    .Eq.rec%E150) Then          !!  for E150: 
!!$                         esbCoax(kk)%sideband = esbWish(ii)%sideband                !!  sideband and 
!!$                         esbCoax(kk)%subband  = esbWish(ii)%subband                 !!  subband from user wish
!!$                      End If
!!$                      !
!!$                   End If
!!$                   !
!!$                End If   !!!   requestUSB(iE150,ihorizontalPol)
!!$                !
!!$             End If   !!!   esbWish(ii)%bandName    .Eq.rec%E150  ...
             !
             !
!!$ OLD  before upgrade of switch box for E150 2012-09:
!!$ OLD               !
!!$ OLD               If (     (      esbWish(ii)%bandName    .Eq.rec%E150                &
!!$ OLD                    &    .And. esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
!!$ OLD                    &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
!!$ OLD                    &   )                                                          &
!!$ OLD                    &  .Or.                                                        &
!!$ OLD                    &   (      esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
!!$ OLD                    &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
!!$ OLD                    &    .And. esbWish(ii)%sideband    .Eq.esbAll(jj)%sideband     &
!!$ OLD                    &    .And. esbWish(ii)%subband     .Eq.esbAll(jj)%subband      &
!!$ OLD                    &   )                                                          &
!!$ OLD                    &  ) Then                                                      !
!!$ OLD                  !
!!$ OLD                  esbAll(jj)%isRequested = .True.
!!$ OLD                  !D                 Write (6,*)   "   esbAll "
!!$ OLD                  !D                 Write (6,110) esbAll(jj)
!!$ OLD                  !
!!$ OLD                  kk = esbAll(jj)%i4                                               !!  %i4 codes to which Coax cable (1 to 4)
!!$ OLD                  !                                                                !!  this subband goes
!!$ OLD                  !                                                                !!  kk is safe in range 1 to 4 if %i4 is OK
!!$ OLD                  esbCoax(kk)     =  esbAll(jj)
!!$ OLD                  esbCoax(kk)%i   =  kk                                            !!  --> %i   codes number (1 to  8) of Coax
!!$ OLD                  esbWish(ii)%i8  =  esbAll(jj)%i8                                 !!  --> %i8  codes number (1 to 16) of EMIR output
!!$ OLD                  esbWish(ii)%i4  =  esbAll(jj)%i4                                 !!  --> %i4  codes number (1 to  4) of Coax
!!$ OLD                  !
!!$ OLD                  If (      esbWish(ii)%bandName    .Eq.rec%E150) Then             !!  for E150: 
!!$ OLD                     esbCoax(kk)%sideband = esbWish(ii)%sideband                   !!  sideband and 
!!$ OLD                     esbCoax(kk)%subband  = esbWish(ii)%subband                    !!  subband from user wish
!!$ OLD                  End If
!!$ OLD                  If (.Not. esbWish(ii)%bandName    .Eq.rec%E150) Then             !!  for E090, E230, E330:
!!$ OLD                     esbCoax(kk+4)   = esbCoax(kk)                                 !!  outer subbands on Coax 5 to 8
!!$ OLD                     esbCoax(kk+4)%i = kk+4
!!$ OLD                     If      (esbCoax(kk)%subband.Eq.esb%li) Then
!!$ OLD                        esbCoax(kk+4)%subband = esb%lo
!!$ OLD                     Else If (esbCoax(kk)%subband.Eq.esb%ui) Then
!!$ OLD                        esbCoax(kk+4)%subband = esb%uo
!!$ OLD                     End If
!!$ OLD                  End If
!!$ OLD                  !
!!$ OLD               End If
             !
          End Do
          !
       End If
    End Do
    !
    If (GV%doDebugMessages) Then
       !
       Write (6,*) " "
       Write (6,*) " list of All (32) subbands"
       Write (6,*) " "
       Write (6,100)
       !
       Do ii = 1, nEMIRsbAll, 1
          !
          Write (6,110) esbAll(ii)
          !
       End Do
       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of up to 4 requested subbands EMIRsubbands "
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEsbIF, 1
!!OLD          !
!!OLD          Write (6,110) EMIRsubBands(ii)
!!OLD          !
!!OLD       End Do
       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of up to 8 requested subbands EMIRsubbandsBis "
!!OLD       Write (6,*) " -- before check -- only 4 are possible "
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEMIRsbWish, 1
!!OLD          !
!!OLD          Write (6,110) EMIRsubBandsBis(ii)
!!OLD          !
!!OLD       End Do
       !
       Write (6,*) " "
       Write (6,*) " list of up to 8 requested subbands esbWish "
       Write (6,*) " -- before check -- only 4 are possible "
       Write (6,*) " "
       Write (6,100)
       !
       Do ii = 1, nEMIRsbWish, 1
          !
          If (esbWish(ii)%isRequested) Then
             Write (6,110) esbWish(ii)
          Else
             Write (6,112) esbWish(ii)%i
          End If
          !
       End Do
       !
       Write (6,*) " "
       Write (6,*) " list of    subbands on Coax 1 to 8 "
       Write (6,*) " "
       Write (6,100)
       !
       Do ii = 1, nEMIRsbCoax, 1
          !
          If (esbCoax(ii)%isRequested) Then
             Write (6,110) esbCoax(ii)
          Else
             Write (6,112) esbCoax(ii)%i
          End If
          !
       End Do
       !
    End If
    !
    !
100 Format("    i  i8/16  i4  iBand  Band   Polar     Sideband  Subband  isRequested? ")
110 Format(1X, I4, I4,3X, I4, I4,5X, A4,3X, A12,2X,   A4,2X,    A6,5X,   L1 )
112 Format(1X, I4 )
    !!          i  i8/16  i4  iBand  Band   Polar     Sideband  Subband  isRequested? 
    !!          1   1      1  13     E090   vertical      LSB   inner   F
    !
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       errorInconsistent = .False.
       !
       If (      (      vars(iIn,iE090)%isConnected                              &
            &      .Or. vars(iIn,iE150)%isConnected                              &
            &      .Or. vars(iIn,iE230)%isConnected                              &
            &      .Or. vars(iIn,iE300)%isConnected ) )                          &
            &      Then                                                          !
          !
          Include 'inc/parameters/checkEMIR.inc'
          !
       Else If (      vars(iIn,iRxTest)%isConnected ) Then                       !   !!  TBD 2014-04: consitency checks for test receiver
          !
       Else
          !
          Include 'inc/parameters/checkConsistentReceiver.inc'
          !
       End If
       !
       Include 'inc/options/checkConsistentEfficiencyScale.inc'
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp,:) = vars(iIn,:)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from Temp into persistent variables   ***
    !
    If (.Not.ERROR) Then
       vars(iValue,:) = vars(iTemp,:)
       listRX         = vars(iValue,:) 
    End If
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "   "
       Write (6,*) "       receiverSelected: -->",                               &
            &              receiverSelected(1:lenc(receiverSelected)),"<--"      !
       Write (6,*) "       iMatch:                       ", iMatch
       Write (6,*) "       iRec:                         ", iRec
       Write (6,*) "       vars(iTemp,iRec)%isConnected:   ",                    &
            &              vars(iTemp,iRec)%isConnected                          !
       Write (6,*) "       vars(iTemp,iRec)%wasConnected:  ",                    &
            &              vars(iTemp,iRec)%wasConnected                         !
       Write (6,*) "       vars(iTemp,iRec)%name:   ",                           &
            &          vars(iTemp,iRec)%name(1:len_trim(vars(iTemp,iRec)%name))  !
       Write (6,*) "       *************************** "
       Write (6,*) "   "
       Write (6,*) "       receiverSelected: -->",                               &
            &              receiverSelected(1:lenc(receiverSelected)),"<--"      !
       Write (6,*) "       iMatch:                       ", iMatch
       Write (6,*) "       iRec:                         ", iRec
       Write (6,*) "       vars(iValue,iRec)%isConnected:   ",                   &
            &              vars(iValue,iRec)%isConnected                         !
       Write (6,*) "       vars(iValue,iRec)%wasConnected:  ",                   &
            &              vars(iValue,iRec)%wasConnected                        !
       Write (6,*) "       vars(iValue,iRec)%name:   ",                          &
            &          vars(iValue,iRec)%name(1:len_trim(vars(iValue,iRec)%name))!
       Write (6,*) "       *************************** "
    End If
    !
    isEmir = .False.
    isHera = .False.
    !
    If     (    vars(iValue,iE090)%isConnected                                   &
         & .Or. vars(iValue,iE150)%isConnected                                   &
         & .Or. vars(iValue,iE230)%isConnected                                   &
         & .Or. vars(iValue,iE300)%isConnected                                   &
         & ) Then                                                                !
       isEmir = .True.
       If (GV%doDebugMessages) Then
          Write (messageText,*)                                                  &
               &         ' isEmir', isEmir                                       !
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
    End If
    !
    If     (    vars(iValue,iHERA1)%isConnected                                  &
         & .Or. vars(iValue,iHERA2)%isConnected                                  &
         & ) Then                                                                !
       isHera = .True.
       If (GV%doDebugMessages) Then
          Write (messageText,*)                                                  &
               &         ' isHera', isHera                                       !
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
    End If
    !
    !! TBD 2.0 ?? -- obsolete?: 
    !!    If (isEMir) Call analyzeEMIR
    !
    !
    !  **  EMIR: check for losses in dichroics
    !
    iBand  = GPnoneI
    iBand1 = GPnoneI
    iBand2 = GPnoneI
    !
    If     (     vars(iValue,iE090)%isConnected                                  &
         & .And. vars(iValue,iE150)%isConnected                                  &
         & ) Then                                                                !
       iBand1 = iE090
       iBand2 = iE150
    End If
    !
    If     (     vars(iValue,iE090)%isConnected                                  &
         & .And. vars(iValue,iE230)%isConnected                                  &
         & ) Then                                                                !
       iBand1 = iE090
       iBand2 = iE230
    End If
    !
    If     (     vars(iValue,iE150)%isConnected                                  &
         & .And. vars(iValue,iE300)%isConnected                                  &
         & ) Then                                                                !
       iBand1 = iE150
       iBand2 = iE300
    End If
    !
    errorCall = .False.
    !
    If ( iBand1.Ne.GPnoneI .And. iBand2.Ne.GPnoneI)  Then
       !
       Do ii = 1, 2, 1
          If (ii.eq.1) iBand = iBand1
          If (ii.eq.2) iBand = iBand2
          Call  dichroicLoss(vars(iValue,iBand)%frequency%value,                    &
               &             iBand,                                                 &
               &             iBand1,   iBand2,                                      &
               &             loss,     errorCall )                                  !
          If (loss.Ge.4.999) Then
             Write (messageText,   '( A,A,A, F12.6,A, A, F5.2,A )' )                &
                  &                "for ", receiverChoices(iBand), " at ",          &
                  &                 vars(iValue,iBand)%frequency%value, " GHz ",    &
                  &                "the DICHROIC LOSS is  ",                        &
                  &                 loss, " % "                                     !
             Call PakoMessage(priorityW+8,severityW,command,messageText)
          End If
       End Do
       !
    End If
    !
    !D    Write (6,*) "   vars(iValue,iBolo)%isConnected: ",                     &
    !D         &          vars(iValue,iBolo)%isConnected 
    If (.Not.ERROR) Then
       If     (.Not.(                                                            &
            & (hasDerotator                                                      &
            & .And..Not.hasDefaults    .And..Not.hasConnect                      &
            & .And..Not.hasDisconnect  .And..Not.hasClear                        &
            & .And..Not.hasCenterIF                                              &
            & .And..Not.hasDoppler     .And..Not.hasEfficiency                   &
            & .And..Not.hasFoffset     .And..Not.hasGainImage                    &
            & .And..Not.hasScale       .And..Not.hasTempload                     &
            & .And..Not.hasWidth                                                 &
            & .And..Not.nArguments.Ge.2                                          &
            & )     )                                                            &
            & ) Then                                                             !
          GV%notReadyRXafterBE = .True.
       End If
       If (vars(iValue,iBolo)%isConnected) Then
          GV%notReadyRXafterBE = .False.
       End If
       GV%notReadyRXafterSecondary = .True.
       GV%notReadyRXafterTip       = .True.
    End If
    !D     Write (6,*) "   GV%notReadyRXafterBE: ", GV%notReadyRXafterBE
    !
    ! *** display values ***
    !     (Note: this is done independently of the Error status)
    Call displayReceiver
    !
    ! *** list receivers in command line screen ***
    If (GV%doDebugMessages) Then
       Write (6,*) "   "
       Write (6,*) "   *** list receivers in command line screen *** "
       Write (6,*) "       receiverSelected: -->",                               &
            &              receiverSelected(1:lenc(receiverSelected)),"<--"      !
       Write (6,*) "       iMatch:                       ", iMatch
       Write (6,*) "       iRec:                         ", iRec
       Write (6,*) "       vars(iValue,iRec)%isConnected:   ",                   &
            &              vars(iValue,iRec)%isConnected                         !
       Write (6,*) "       vars(iValue,iRec)%wasConnected:  ",                   &
            &              vars(iValue,iRec)%wasConnected                        !
       Write (6,*) "       vars(iValue,iRec)%name:   ",                          &
            &          vars(iValue,iRec)%name(1:len_trim(vars(iValue,iRec)%name))!
       Write (6,*) "       *************************** "
    End If


!!$    !
!!$    !   ***   for NIKA: check configuration files for pixel Nasmyth offsets
!!$    !
!!$    foundPixel                    =  .False.
!!$    NIKApixel%isSet               =  .False.
!!$    GV%sourceOffsets(iNas)%isSet  =  .False.
!!$    GV%arrayNasmythOffsetsAreSet  =  .False.
!!$    !
!!$    Call queryReceiver(rec%Bolo, isConnected, bolometerName = bolometerName)
!!$    !
!!$    If (isConnected .And. bolometerName.Eq.bolo%NIKA ) Then
!!$       !
!!$       pixel = vars(iValue,iBolo)%channel
!!$       !
!!$       Write (messageText,*)                                                     &
!!$            &         " looking for Nasmyth offsets for NIKA pixel ", pixel      !
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       !
!!$       iUnitR  =  ioUnit%Read
!!$       !
!!$       ii = 1
!!$       !
!!$       Do While ( .Not.foundPixel .And. ii.Le.4 )
!!$          !
!!$          Open(unit=iUnitR, action='read', file=NIKApixelFiles(ii),              &
!!$               &              status='old',                                      &
!!$               &              iostat=ier)                                        ! 
!!$          !
!!$          Write (messageText,*)                                                  &
!!$               &      " try file ", NIKApixelFiles(ii)(1:len_trim(NIKApixelFiles(ii)))   !
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          !
!!$          If (ier.Eq.0) then
!!$             !
!!$             Write (messageText,*)  " opened file ",                             &
!!$                  &             NIKApixelFiles(ii)(1:len_trim(NIKApixelFiles(ii)))       !
!!$             Call PakoMessage(priorityI,severityI,command,messageText)
!!$             !
!!$             ioStatus = 0
!!$             !
!!$             Do While ( .Not.foundPixel .And. ioStatus.Eq.0 )
!!$                !
!!$                Read (iUnitR,'(a)',iostat=ioStatus) lineIn
!!$                !
!!$                If (ioStatus.Eq.0) Then
!!$                   !
!!$                   If ( lineIn(1:1).Ne."!"  .And. lineIn(1:1).Ne."#" ) Then
!!$                      !                   
!!$                      lineInTrim = lineIn
!!$                      !
!!$                      indexTrim  = min( Index(lineIn,"!"),Index(lineIn,"#") ) -1
!!$                      If ( indexTrim.Ge.1 ) Then
!!$                         lineInTrim = lineIn(1:indexTrim)
!!$                      End If
!!$                      !
!!$                      Read (lineInTrim,*,iostat=ioStatus)  jj, xx, yy
!!$                      !
!!$                      !D                       Write (6,*) "   jj, xx, yy ", jj, xx, yy
!!$                      !
!!$                      If (ioStatus.Eq.0) Then
!!$                         !
!!$                         If ( jj.Eq.pixel) then
!!$                            foundPixel      =  .True.
!!$                            !
!!$                            NIKApixel%isSet        =  .True.
!!$                            NIKApixel%systemName   =  offs%nas
!!$                            NIKApixel%point%x      =  xx
!!$                            NIKApixel%point%y      =  yy
!!$                            NIKApixel%channel      =  pixel
!!$                            NIKApixel%elevation    =  0
!!$                         End If
!!$                         !
!!$                      Else
!!$                         !
!!$                         Write (messageText,*)  " line ",                           &
!!$                              &                   lineIn(1:len_trim(lineIn)),       &
!!$                              &                 " not valid"                        !
!!$                         Call PakoMessage(priorityW,severityW,command,messageText)
!!$                         !
!!$                      End If
!!$                      !
!!$                   End If
!!$                   !
!!$                End If
!!$                !
!!$             End Do
!!$             !
!!$             Close(unit=iUnitR)
!!$             !
!!$          Else
!!$             !
!!$             Write (messageText,*)  " could not open file ",                     &
!!$                  &             NIKApixelFiles(ii)(1:len_trim(NIKApixelFiles(ii)))       !
!!$             Call PakoMessage(priorityI,severityI,command,messageText)
!!$             !
!!$          End If
!!$          !
!!$          ii = ii+1
!!$          !
!!$       End Do
!!$       !
!!$       If ( foundPixel ) Then
!!$          !
!!$          Write (messageText,'(A,I5,A,2F12.3)')                                  &
!!$               &     " found offsets for NIKA pixel ", NIKApixel%channel,        &
!!$               &                                " : ", NIKApixel%point%x,        &
!!$               &                                       NIKApixel%point%y         !
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          !
!!$          GV%sourceOffsets(iNas)          =  NIKApixel
!!$          !
!!$          GV%arrayNasmythOffsetsAreSet  =  .True.
!!$          !
!!$       Else
!!$          !
!!$          ERROR  =  .True.
!!$          Write (messageText,'(A,I5)')                                           &
!!$               &     " could not find offsets for NIKA pixel ", pixel            !
!!$          Call PakoMessage(priorityE,severityE,command,messageText)
!!$          !
!!$          GV%sourceOffsets(iNas)%isSet  =  .False.
!!$          GV%arrayNasmythOffsetsAreSet  =  .False.
!!$          !
!!$       End If
!!$       !
!!$       ! *** display values ***
!!$       !     (Note: this is done independently of the Error status)
!!$       Call displayReceiver
!!$       !
!!$    End If   !!   (isConnected .And. bolometerName.Eq.bolo%NIKA )
    !
    If (.Not.ERROR) Then
       !
       !D       Write (6,*) "     iRec:  ", iRec
       If (iRec.Ge.1 .And. iRec.Le.nDimReceivers) Then
          receiverCommand(iRec) = line(1:lenc(line))
       End If
       !
       countRX = 0
       !
       Do ii = 1,nDimReceivers,1
          If (vars(iValue,ii)%isConnected) Then
             countRX = countRX+1
             If (ii.Eq.iBolo) Then
                l = lenc(vars(iValue,ii)%bolometerName)
                Write (messageText,*)                                            & 
                     &         vars(iValue,ii)%name,                             &
                     &         ' ',                                              &
                     &         vars(iValue,ii)%bolometerName(1:l),               &
                     &         ' connected'                                      !
             Else If (isEMIR) Then
                l  = lenc(vars(iValue,ii)%horizontalSubbands)
                l2 = lenc(vars(iValue,ii)%verticalSubbands)
                Write (messageText,'(2A,F12.6,6A)')                              & 
                     &         vars(iValue,ii)%name,                             &
                     &         vars(iValue,ii)%lineName,                         &
                     &         vars(iValue,ii)%frequency%value,                  &
                     &         " ",                                              &
                     &         vars(iValue,ii)%sideBand,                         &
                     &         " /Horizontal ",                                  &
                     &         vars(iValue,ii)%horizontalSubbands(1:l),          &
                     &         " /Vertical ",                                    &
                     &         vars(iValue,ii)%verticalSubbands(1:l2)            !
             Else If (isHERA) Then
                Write (messageText,'(2A,F12.6,6A)')                              & 
                     &         vars(iValue,ii)%name,                             &
                     &         vars(iValue,ii)%lineName,                         &
                     &         vars(iValue,ii)%frequency%value,                  &
                     &         " ",                                              &
                     &         vars(iValue,ii)%sideBand                          !
             Else If (ii.EQ.iRxTest) Then
                l = lenc(vars(iValue,ii)%lineName)
                Write (messageText,'(2A,F12.6,2A)')                              & 
                     &         vars(iValue,ii)%name,                             &
                     &         vars(iValue,ii)%lineName(1:l),                    &
                     &         vars(iValue,ii)%frequency%value,                  &
                     &         " ",                                              &
                     &         vars(iValue,ii)%sideBand                          !
             Else
                l = lenc(vars(iValue,ii)%lineName)
                Write (messageText,*)                                            & 
                     &         vars(iValue,ii)%name,                             &
                     &         ' ',                                              &
                     &         vars(iValue,ii)%lineName(1:l),                    &
                     &         ' connected'                                      !
             End If
             Call PakoMessage(priorityI,severityI,command,messageText)
          End If
       End Do
       !
       GV%receiverSet       = countRX .Ge. 1
       GV%receiverNumberOf  = countRX
       !
!!OLD       If      ( vars(iValue,iHera1)%isConnected                                 &
!!OLD            &  .Or.                                                              &
!!OLD            &    vars(iValue,iHera2)%isConnected                                 &
!!OLD            &  ) Then                                                            !
!!OLD...
!!OLD       Else If (    vars(iValue,iE090)%isConnected                               &
!!OLD            &  .Or. vars(iValue,iE150)%isConnected                               &
!!OLD            &  .Or. vars(iValue,iE230)%isConnected                               &
!!OLD            &  .Or. vars(iValue,iE300)%isConnected                               &
!!OLD            &  ) Then                                                            !
!!OLD...
       !
       GV%slowRateAMD = 1                                                        ! !! slowRate 1
       !
       If (isHERA) Then
          GV%receiverName = "HERA"
          GV%bolometerName = GPnone
       Else If ( vars(iValue,iBolo)%isConnected                                  &
            &  ) Then                                                            !
          GV%receiverName  = rec%Bolo
          GV%bolometerName = vars(iValue,iBolo)%bolometerName
          If (GV%bolometerName.Eq. bolo%GISMO) Then                              ! !! for GISMO:
             GV%slowRateAMD = 8                                                  ! !! slowRate 8
          End If
          If (GV%bolometerName.Eq. bolo%NIKA) Then                               ! !! for NIKA:
             GV%slowRateAMD = 8                                                  ! !! slowRate 8
          End If
       Else If ( isEMIR) Then
          GV%receiverName  = "EMIR"
          GV%bolometerName = GPnone
       Else If ( countRX.Ge.1 ) Then
          GV%receiverName  = "other"
          GV%bolometerName = GPnone
       Else
          GV%receiverName  = GPnone
          GV%bolometerName = GPnone
       End If
       !
       !D        Write (6,*) "       --> GV%slowRateAMD :  ", GV%slowRateAMD 
       !
       If (         vars(iValue,iHera1)%isConnected .And.                        &
            &       vars(iValue,iHera1)%gainImage%value.Ne.0.1                   &
            &  .Or.                                                              &
            &       vars(iValue,iHera2)%isConnected .And.                        &
            &       vars(iValue,iHera2)%gainImage%value.Ne.0.1                   &
            &  ) Then
          Write (messageText,*)                                                  &
               ' for HERA /GAIN 0.1 is recommended'
          Call PakoMessage(priorityW,severityW,command,messageText)
       End If
       !
       If (isHERA .And. GV%doDebugMessages) Then
          !! TBD 2.0:           Call showArray(command)
       End If
       !
!!OLD       If (GV%doDebugMessages) Then
!!OLD          If      (    vars(iValue,iHera1)%isConnected                           &
!!OLD               &  .Or.                                                           &
!!OLD               &       vars(iValue,iHera2)%isConnected                           &
!!OLD               &  ) Then                                                         !
!!OLD             Call showArray(command)
!!OLD          End If
!!OLD       End If
       !
    End If
    !
    If (GV%doDebugMessages) Then
       Write (messageText,*)                                                     &
            " global variable receiverName:  ", GV%receiverName                  !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            " global variable bolometerName: ", GV%bolometerName                 !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            " global variable slowRateAMD:   ", GV%slowRateAMD                   !
       Call PakoMessage(priorityI,severityI,command,messageText)
    End If
    !
!!OLD    If (GV%BackendSet .And. .Not.vars(iValue,iBolo)%isConnected) Then
    If (GV%notReadyRXafterBE .And. .Not.vars(iValue,iBolo)%isConnected) Then
       messageText = "Review BACKEND settings for consistency!"
       Call PakoMessage(priorityW,severityW,command,messageText)
    End If
    !
    If (GV%switchingMode .Eq. "SWFREQUENCY") Then
       messageText = "Review SWFREQUENCY settings for consistency!"
       Call PakoMessage(priorityW,severityW,command,messageText)
    End If
    !
    !D     Write (6,*) vars(iIn,iE090 )
    !D     Write (6,*) vars(iTemp,iE090 )
    !D     Write (6,*) vars(iValue,iE090 )
    !
    Return
  End Subroutine receiver
!!!
!!$!!!
!!$  Subroutine analyzeEMIR
!!$    !
!!$    Include 'inc/variables/standardWorkingVariables.inc'
!!$    !
!!$    If (GV%doDebugMessages) Then
!!$       Write (6,*) " "
!!$       Write (6,*) "   --> analyzeEMIR"
!!$    End If
!!$    !
!!$    !  **   initialize to undefined
!!$    !
!!$    Do ii = 1, nEsbBoxes, 1
!!$       !
!!$       Do jj = 1, nEsbBoxIn, 1
!!$          esbBoxIn(ii,jj) = esbNone
!!$       End Do
!!$       !
!!$       Do jj = 1, nEsbBoxSw, 1
!!$          esbBoxSwI (ii,jj) = nEsbSwNone
!!$       End do
!!$       esbBoxSwC (ii) = GPNone
!!$       !
!!$       Do jj = 1, nEsbBoxOut, 1
!!$          esbBoxOut(ii,jj) = esbNone
!!$       End Do
!!$       !
!!$    End Do
!!$    !
!!$    esbSwitchesI     =  GPnoneI
!!$    esbSwitchesC     =  GPnone
!!$    esbSwitchesChex  =  GPnone
!!$    !
!!$    Do jj = 1, nEsbIF, 1
!!$       IFcable(jj) = esbNone
!!$    End Do
!!$    !
!!$    !  **   boxes 1 to 4
!!$    !
!!$    Do ii = 1, 4, 1
!!$       !
!!$       Do jj = 1, nEsbBoxIn, 1
!!$          !
!!$          !   *   Inputs
!!$          kk = nEsbBoxIn*(ii-1)+jj
!!$          If (kk.Ge.1 .And. kk.Le.nEMIRsb .And. esbIn(kk)%isRequested) Then
!!$             esbBoxIn(ii,jj) = esbIn(kk)
!!$          End If
!!$       End Do
!!$       !
!!$       !   *   Switches
!!$       Call switchEsbBox(iBox=ii)
!!$       !
!!$    End Do
!!$    !
!!$    !  **   boxes 5 and 6
!!$    !
!!$    Do ii = 5, 6, 1
!!$       !
!!$       !   *   Inputs
!!$       If (ii.Eq.5) jj = 1
!!$       If (ii.Eq.6) jj = 3
!!$       esbBoxIn(ii,1) = esbBoxOut( jj   , 1 )
!!$       esbBoxIn(ii,2) = esbBoxOut( jj   , 2 )
!!$       esbBoxIn(ii,3) = esbBoxOut( jj+1 , 1 )
!!$       esbBoxIn(ii,4) = esbBoxOut( jj+1 , 2 )
!!$       !
!!$       !   *   Switches
!!$       Call switchEsbBox(iBox=ii)
!!$       !
!!$    End Do
!!$    !
!!$    !  **   list boxes 1 to 6
!!$    !
!!$    If (GV%userLevel.Eq.setUserLevel%guru .And. GV%doDebugMessages) Then
!!$       !
!!$       Write (6,*)
!!$       Do ii = 1, 4, 1
!!$          Do jj = 1, nEsbBoxIn, 1
!!$             If (esbBoxIn(ii,jj)%isRequested) Then
!!$                Write (6,*) " Box ",ii," Input ",jj," : ",                       &
!!$                     &        esbBoxIn(ii,jj)%bandName(1:5)," ",                 &
!!$                     &        esbBoxIn(ii,jj)%polarization(1:5)," ",             &
!!$                     &        esbBoxIn(ii,jj)%sideBand(1:5)," ",                 &
!!$                     &        esbBoxIn(ii,jj)%subBand(1:5)                       !
!!$             End If
!!$          End Do
!!$       End Do
!!$       !
!!$       !D     Do ii = 1, 6, 1
!!$       !D        Write (6,*) " Box ",ii," Switches: ", esbBoxSwI(ii,:)
!!$       !D     End Do
!!$       !
!!$       Write (6,*)
!!$       Write (6,*) " EMIR subBand Switches "
!!$       Write (6,*)
!!$       Write (6,*) " Box      1           2           3           4 "
!!$       Write (6,*)
!!$       Write (6,'(4(9X,I3))')          esbBoxSwI(1:4,1)
!!$       Write (6,'(4(9X,I3))')          esbBoxSwI(1:4,2)
!!$       Write (6,'(2X,4(4X,I3,2X,I3))') esbBoxSwI(1,3:4), esbBoxSwI(2,3:4),          &
!!$            &                          esbBoxSwI(3,3:4), esbBoxSwI(4,3:4)           !
!!$       !
!!$       Write (6,*)
!!$       Write (6,*) " Box            5                       6        "
!!$       Write (6,*)
!!$       Write (6,'(15X,(I3,21X,I3))')             esbBoxSwI(5:6,1)
!!$       Write (6,'(15X,(I3,21X,I3))')             esbBoxSwI(5:6,2)
!!$       Write (6,'(12X,(I3,2X,I3,16X,I3,2X,I3))') esbBoxSwI(5,3:4), esbBoxSwI(6,3:4)
!!$       Write (6,*)
!!$       !
!!$    End If
!!$    !
!!$    esbSwitchesC = esbBoxSwC0(1)//esbBoxSwC0(2)//esbBoxSwC0(3)                   &
!!$         &       //esbBoxSwC0(4)//esbBoxSwC0(5)//esbBoxSwC0(6)                   !
!!$    Read (esbSwitchesC,'(B24)') esbSwitchesI
!!$    !
!!$    Write (esbSwitchesChex,'(Z6.6)') esbSwitchesI
!!$    !
!!$    If (GV%userLevel.Eq.setUserLevel%guru .And. GV%doDebugMessages) Then
!!$       !
!!$       !D    Write (6,*)         " esbSwitchesI: ", esbSwitchesI
!!$       !D    Write (6,'(A,B24)') " esbSwitchesI: ", esbSwitchesI
!!$       !D    Write (6,'(A,Z6)')  " esbSwitchesI: ", esbSwitchesI
!!$       !
!!$       Write (6,*) " esbSwitches:   ",esbSwitchesC(1:4),  " ",esbSwitchesC(5:8), &
!!$            " ",esbSwitchesC(9:12), " ",esbSwitchesC(13:16),                     &
!!$            " ",esbSwitchesC(17:20)," ",esbSwitchesC(21:24)                      !
!!$       Write (6,*) " esbSwitchesC: -->", esbSwitchesC, "<--"
!!$       Write (6,*) " esbSwitchesChex: -->", esbSwitchesChex, "<--"
!!$       !
!!$       !D     Write (6,*)
!!$       !D     Do ii = 5, 6, 1
!!$       !D        Do jj = 1, nEsbBoxOut, 1
!!$       !D           Write (6,*) " Box ",ii," Output ",jj," : ",                            &
!!$       !D                &        esbBoxOut(ii,jj)%bandName(1:5)," ",                      &
!!$       !D                &        esbBoxOut(ii,jj)%polarization(1:5)," ",                  &
!!$       !D                &        esbBoxOut(ii,jj)%sideBand(1:5)," ",                      &
!!$       !D                &        esbBoxOut(ii,jj)%subBand(1:5)                            !
!!$       !D        End Do
!!$       !D     End Do
!!$       !
!!$    End If
!!$    !
!!$    !  ** IF cables
!!$    !
!!$    IFcable(1) = esbBoxOut(5,1)
!!$    IFcable(2) = esbBoxOut(5,2)
!!$    IFcable(3) = esbBoxOut(6,1)
!!$    IFcable(4) = esbBoxOut(6,2)
!!$    !
!!$    If (GV%userLevel.Eq.setUserLevel%guru .And. GV%doDebugMessages) Then
!!$       !
!!$       Write (6,*)
!!$       Do jj = 1, nEsbIF, 1
!!$          Write (6,*) " IF cable ",jj," : ",                                     &
!!$               &        IFcable(jj)%bandName(1:5)," ",                           &
!!$               &        IFcable(jj)%polarization(1:5)," ",                       &
!!$               &        IFcable(jj)%sideBand(1:5)," ",                           &
!!$               &        IFcable(jj)%subBand(1:5)                                 !
!!$       End Do
!!$       Write (6,*)
!!$       !
!!$    End If
!!$    !
!!$    If (GV%doDebugMessages) Then
!!$       Write (6,*) "   <-- analyzeEMIR"
!!$       Write (6,*) " "
!!$    End If
!!$    !
!!$  End Subroutine analyzeEMIR
!!!
!!!
!!$  Subroutine switchEsbBox(iBox, Error, ErrorCode)
!!$    !
!!$    Integer,          Intent(In)              ::  iBox
!!$    Logical,          Intent(Out), Optional   ::  Error
!!$    Character(len=*), Intent(OUT), Optional   ::  ErrorCode
!!$    !
!!$    Integer  ::  jj
!!$    !
!!$    !D    Write (6,*) "   --> SR switchEsbBox"
!!$    !D    Write (6,*) "       iBox: ",iBox
!!$    !
!!$    If (Present(Error)) Error = .False.
!!$    If (Present(ErrorCode) .And. Len_Trim(ErrorCode).Ge.2) ErrorCode = "OK"
!!$
!!$    !
!!$    !  ** switches for boxes 1 to 4
!!$    !
!!$    If (esbBoxIn (iBox,1)%isRequested) Then
!!$       esbBoxSwI (iBox,3) = 0
!!$       esbBoxOut (iBox,1) = esbBoxIn(iBox,1)
!!$       !D           Write (6,*) " esbBoxSwI(",iBox,",3): ", esbBoxSwI(iBox,3) 
!!$       !D           Write (6,*) " esbBoxOut(",iBox,",1): ", esbBoxOut(iBox,1) 
!!$    End If
!!$    !
!!$    If (esbBoxIn (iBox,2)%isRequested) Then
!!$       esbBoxSwI (iBox,1) = 0
!!$       !D       Write (6,*) " esbBoxSwI(",iBox,",1): ", esbBoxSwI(iBox,1) 
!!$       If       (esbBoxIn (iBox,1)%isRequested                                   &
!!$            &    .Or.                                                            &
!!$            &    (           (iBox.Eq.2.Or.iBox.eq.4)                            &
!!$            &     .And. .Not.esbBoxIn (iBox,3)%isRequested                       &
!!$            &     .And. .Not.esbBoxIn (iBox,4)%isRequested)                      &
!!$            &   ) Then                                                           !
!!$          esbBoxSwI (iBox,2) = 1
!!$          esbBoxSwI (iBox,4) = 0
!!$          esbBoxOut (iBox,2) = esbBoxIn(iBox,2)
!!$          !D          Write (6,*) " esbBoxSwI(",iBox,",2): ", esbBoxSwI(iBox,2) 
!!$          !D          Write (6,*) " esbBoxSwI(",iBox,",4): ", esbBoxSwI(iBox,4) 
!!$          !D          Write (6,*) " esbBoxOut(",iBox,",2): ", esbBoxOut(iBox,2)           
!!$       Else
!!$          esbBoxSwI (iBox,2) = 0
!!$          esbBoxSwI (iBox,3) = 1
!!$          esbBoxOut (iBox,1) = esbBoxIn(iBox,2)
!!$          !D              Write (6,*) " esbBoxSwI(",iBox,",2): ", esbBoxSwI(iBox,2) 
!!$          !D              Write (6,*) " esbBoxSwI(",iBox,",3): ", esbBoxSwI(iBox,3) 
!!$          !D              Write (6,*) " esbBoxOut(",iBox,",1): ", esbBoxOut(iBox,1)           
!!$       End If
!!$    End If
!!$    !
!!$    If (esbBoxIn (iBox,3)%isRequested) Then
!!$       esbBoxSwI (iBox,1) = 1
!!$       !D       Write (6,*) " esbBoxSwI(",iBox,",1): ", esbBoxSwI(iBox,1) 
!!$       If       (esbBoxIn (iBox,4)%isRequested                                   &
!!$            &    .Or.                                                            &
!!$            &    (           (iBox.Eq.1.Or.iBox.eq.3)                            &
!!$            &     .And. .Not.esbBoxIn (iBox,1)%isRequested                       &
!!$            &     .And. .Not.esbBoxIn (iBox,2)%isRequested )                     &
!!$            &   ) Then                                                           !
!!$          esbBoxSwI (iBox,2) = 0
!!$          esbBoxSwI (iBox,3) = 1
!!$          esbBoxOut (iBox,1) = esbBoxIn(iBox,3)
!!$          !D          Write (6,*) " esbBoxSwI(",iBox,",2): ", esbBoxSwI(iBox,2) 
!!$          !D          Write (6,*) " esbBoxSwI(",iBox,",3): ", esbBoxSwI(iBox,3) 
!!$          !D          Write (6,*) " esbBoxOut(",iBox,",1): ", esbBoxOut(iBox,1)           
!!$       Else
!!$          esbBoxSwI (iBox,2) = 1
!!$          esbBoxSwI (iBox,4) = 0
!!$          esbBoxOut (iBox,2) = esbBoxIn(iBox,3)
!!$          !D              Write (6,*) " esbBoxSwI(",iBox,",2): ", esbBoxSwI(iBox,2) 
!!$          !D              Write (6,*) " esbBoxSwI(",iBox,",4): ", esbBoxSwI(iBox,4) 
!!$          !D              Write (6,*) " esbBoxOut(",iBox,",2): ", esbBoxOut(iBox,2)           
!!$       End If
!!$    End If
!!$    !
!!$    If (esbBoxIn (iBox,4)%isRequested) Then
!!$       esbBoxSwI (iBox,4) = 1
!!$       esbBoxOut (iBox,2) = esbBoxIn(iBox,4)
!!$       !D           Write (6,*) " esbBoxSwI(",iBox,"4): ", esbBoxSwI(iBox,4) 
!!$       !D           Write (6,*) " esbBoxOut(",iBox,"2): ", esbBoxOut(iBox,2) 
!!$    End If
!!$    !
!!$    !D    Write (6,*) " For Box ", iBox, "Switches:"
!!$    !D    Write (*,'(4I1)') esbBoxSwI(iBox,1), esbBoxSwI(iBox,2), esbBoxSwI(iBox,3), esbBoxSwI(iBox,4)
!!$    !
!!$    Do jj = 1, nEsbBoxSw, 1
!!$       If      (esbBoxSwI(iBox,jj).Eq.0) Then
!!$          esbBoxSwC (iBox)(jj:jj) = "0"
!!$          esbBoxSwC0(iBox)(jj:jj) = "0"
!!$       Else If (esbBoxSwI(iBox,jj).Eq.1) Then
!!$          esbBoxSwC (iBox)(jj:jj) = "1"
!!$          esbBoxSwC0(iBox)(jj:jj) = "1"
!!$       Else
!!$          esbBoxSwC (iBox)(jj:jj) = " "
!!$          esbBoxSwC0(iBox)(jj:jj) = "0"
!!$       End If
!!$    End Do
!!$    !
!!$    !D    Write (6,*) " esbBoxSwC:  -->", esbBoxSwC (iBox), "<--"
!!$    !D    Write (6,*) " esbBoxSwC0: -->", esbBoxSwC0(iBox), "<--"
!!$    !
!!$  End Subroutine switchEsbBox
!!$!!!
!!!
  Subroutine dichroicLoss (frequency, iBand, iBand1, iBand2,                     &
       &                   loss, error )                                         !
    !
    !!  revised 2016-09
    !!  updated 2010-07-26 based on wiki EmirforAstronomers
    !!  only for losses .Ge. 5.0
    !
    Real(kind=kindDouble), Intent(IN)             ::  frequency
    Integer              , Intent(IN)             ::  iBand, iBand1,iBand2
    !
    Real                 , Intent(OUT)            ::  loss
    Logical              , Intent(OUT), Optional  ::  ERROR
    !
    ERROR = .False.
    loss  = 1.0
    !
    !D     Write (6,*) "      --> dichroicLoss"
    !D     Write (6,*) "          frequency: ", frequency
    !D     Write (6,*) "          iBand:     ", iBand
    !D     Write (6,*) "          iBand1:    ", iBand1
    !D     Write (6,*) "          iBand2:    ", iBand2
    !
    If (    iBand1.Eq.iE090 .And. iBand2.Eq.iE150 ) Then
       !
       If ( iBand.Eq.iE090) Then
          If (Frequency.Ge. 100.0 )  loss =  5.0
          If (Frequency.Ge. 110.0 )  loss = 10.0
       End If
       If ( iBand.Eq.iE150) Then
          If (Frequency.Le. 129.0 )  loss =  5.0
       End If
    End If
    !
    If (    iBand1.Eq.iE090 .And. iBand2.Eq.iE230 ) Then
       !
       If ( iBand.Eq.iE090) Then
       End If
       If ( iBand.Eq.iE230) Then                                                 ! !! revision 2016-09
          If (Frequency.Le. 213.0 )  loss =  4.0
          If (Frequency.Le. 202.0 )  loss =  4.0
          !
          If (Frequency.Ge. 243.0 )  loss =  4.0
          If (Frequency.Ge. 255.0 )  loss =  4.0
          If (Frequency.Ge. 258.0 )  loss =  4.0
          If (Frequency.Ge. 267.0 )  loss =  4.0
       End If
!!$       If ( iBand.Eq.iE230) Then                                              ! !! version before 2016-09
!!$          If (Frequency.Le. 213.0 )  loss =  5.0
!!$          If (Frequency.Le. 202.0 )  loss = 10.0
!!$          !
!!$          If (Frequency.Ge. 243.0 )  loss =  5.0
!!$          If (Frequency.Ge. 255.0 )  loss =  5.0
!!$          If (Frequency.Ge. 258.0 )  loss = 10.0
!!$          If (Frequency.Ge. 267.0 )  loss = 50.0
!!$       End If
    End If
    !
    If (    iBand1.Eq.iE150 .And. iBand2.Eq.iE300 ) Then
       !
       If ( iBand.Eq.iE150) Then
       End If
       If ( iBand.Eq.iE300) Then
          If (Frequency.Ge. 369.0 )  loss =  7.0
       End If
    End If
    !
    !D     Write (6,*) "          loss:      ", loss
    !D     Write (6,*) "          "
    !
    Return
    !
  End Subroutine dichroicLoss
!!!
!!!
  Subroutine displayReceiver
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplayReceiver.inc'
    !
  End Subroutine displayReceiver
!!!
!!!
  Subroutine queryReceiver(queryName, IsConnected, bolometerName, polarization, subband)
    !
    Character(len=*), Intent(IN)            ::  queryName
    Logical,          Intent(OUT)           ::  IsConnected
    Character(len=*), Intent(OUT), Optional ::  bolometerName
    Character(len=*), Intent(IN),  Optional ::  polarization
    Character(len=*), Intent(IN),  Optional ::  subband
    !
    Integer                                 ::  lenc
    Integer                                 ::  ii, l, l2
    !
    IsConnected = .False.
    !
    l = lenc(queryName)
    !
    !D     Write (6,*) "   --> queryReceiver "
    !D     Write (6,*) "       queryname:                    ", queryName          
    !D     Write (6,*) "       iRxTest:                      ", iRxTest
    !D     Write (6,*) "       vars(iValue,iRxTest)%name:    ", vars(iValue,iRxTest)%name
    !D     If (Present(polarization)) Write (6,*) "    polarization: ", polarization    
    !D     If (Present(subband))      Write (6,*) "    subband: ", subband              
    !
    If (            queryName.Eq.RxHolography                                    &
         &    .And. vars(iValue,iRxTest)%isConnected                             &
         &    .And. vars(iValue,iRxTest)%name.Eq.RxHolography) Then              !
       IsConnected = .True.
    End If
    !
    Do ii = 1, nDimReceivers, 1
       !
       If (vars(iValue,ii)%isConnected) Then
          !
          !  ** EMIR
          !
          If     (      queryName.Eq."EMIR"                                      &
               & .And. (vars(iValue,ii)%name.Eq.rec%E090                         &
               &   .Or. vars(iValue,ii)%name.Eq.rec%E150                         &
               &   .Or. vars(iValue,ii)%name.Eq.rec%E230                         &
               &   .Or. vars(iValue,ii)%name.Eq.rec%E300                         &
               &       ) ) Then                                                  !
             IsConnected = .True.
          End If
          !
          If (Present(polarization) .And. Present(subband)) Then
             !
             !  ** EMIR
             !
             If      (                                                           &
                  &      vars(iValue,ii)%name.Eq.rec%E090                        &
                  & .Or. vars(iValue,ii)%name.Eq.rec%E150                        &
                  & .Or. vars(iValue,ii)%name.Eq.rec%E230                        &
                  & .Or. vars(iValue,ii)%name.Eq.rec%E300                        &
                  &  ) Then                                                      !
                !
                If (vars(iValue,ii)%name(1:l) .Eq. queryName(1:l)) Then
                   l2 = Len_trim(subband)
                   If (polarization .Eq. pol%hor) Then
                      IsConnected = Index(vars(iValue,ii)%horizontalSubbands,    &
                           &         subband(1:l2)) .Ge. 1                       !
                   End If
                   If (polarization .Eq. pol%ver) Then
                      IsConnected = Index(vars(iValue,ii)%verticalSubbands,      &
                           &         subband(1:l2)) .Ge. 1                       !
                   End If
                End If
                !
             End If
             !
          Else     
             !
             If (vars(iValue,ii)%name(1:l) .Eq. queryName(1:l)) Then
                IsConnected = .True.
                If (Present(bolometerName) .And.                                 &
                     &   vars(iValue,ii)%name(1:l).Eq.rec%bolo) Then             !
                   bolometerName = vars(iValue,ii)%bolometerName
                End If
             End If
             !
          End If
          !
       End If
       !
    End Do
    !
    Return
  End Subroutine queryReceiver
!!!
!!!
  Subroutine saveReceiver(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                :: l1, l2
    !
    Integer                :: ii
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module Receiver,        --> SR: saveReceiver v 2014-10-23")      ! this allows to trace execution
    !
    B = "\"     ! "
    S = " "
    CMD =  programName//B//"RECEIVER"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    contC = contNN
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    Write (iUnit,*) CMD(1:lCMD)," /clear"
    !
    Do ii = 1,12,1
       If (vars(iValue,ii)%isConnected) Then
          !D           Write (messageText,*)                                              & 
          !D                &         " saving RX: ", vars(iValue,ii)%name
          !D           Call PakoMessage(priorityI,severityI,commandToSave,messageText)
          contC = contNC
          Include 'inc/parameters/saveReceiver.inc'
          If (ii .Eq. iBolo) Then
             If ( NIKApixel%isSet ) Then
                contC = contCC
                Include 'inc/parameters/saveNameBolometer.inc'
                contC = contCN
                Include 'inc/parameters/saveChannel.inc'
             Else
                contC = contCN
                Include 'inc/parameters/saveNameBolometer.inc'
             End If
!!$!!OLD             contC = contCC
!!$!!OLD             Include 'inc/parameters/saveNameBolometer.inc'
!!$!!OLD             Include 'inc/parameters/saveGainBolometer.inc'
!!$!!OLD             contC = contCN
!!$!!OLD             Include 'inc/parameters/saveChannel.inc'
          Else
             contC = contCC
             Include 'inc/parameters/saveLineName.inc'
             Include 'inc/parameters/saveFrequency.inc'
             Include 'inc/parameters/saveSideBand.inc'
!!$!!OLD             rxName = vars(iValue,ii)%name
!!$!!OLD             lName  = lenc(rxName)
             Include 'inc/options/saveDoppler.inc'
             Include 'inc/options/saveWidth.inc'
             Include 'inc/options/saveGain.inc'
             Include 'inc/options/saveTempLoad.inc'
             Include 'inc/options/saveEfficiency.inc'
             If (ii.Eq.iHera .Or. ii.Eq.iHERA1 .Or. ii.Eq.iHERA2) Then
                Include 'inc/options/saveScale.inc'
                contC = contCN
                Include 'inc/options/saveDerotator.inc'
             Else
                contC = contCN
                Include 'inc/options/saveScale.inc'
             End If
          End If
       Else If (vars(iValue,ii)%name .Ne. GPnone) Then
       !D   l = lenc(vars(iValue,ii)%name)
       !D   Write (iUnit,*) "!"
       !D   Write (iUnit,*) CMD(1:lCMD)," ",                                   &
       !D        &         vars(iValue,ii)%name(1:l)," /connect ", "no"
       End If
    End Do
    !
    Do ii = iE090, iE300, 1
       If (vars(iValue,ii)%isConnected) Then
          contC = contNC
          Include 'inc/parameters/saveReceiver.inc'
          contC = contCC
          Include 'inc/parameters/saveLineName.inc'
          Include 'inc/parameters/saveFrequency.inc'
          Include 'inc/parameters/saveSideBand.inc'
          Include 'inc/options/saveDoppler.inc'
          Include 'inc/options/saveGain.inc'
          Include 'inc/options/saveTempLoad.inc'
          Include 'inc/options/saveEfficiency.inc'
          Include 'inc/options/saveScale.inc'
          Include 'inc/options/saveFoffset.inc'
          Include 'inc/options/saveHorizontal.inc'
          contC = contCN
          Include 'inc/options/saveVertical.inc'
       End If
    End Do
    !
    Do ii = iRxTest, iRxTest, 1
       If (vars(iValue,ii)%isConnected) Then
          contC = contNC
          Include 'inc/parameters/saveReceiver.inc'
          contC = contCC
          Include 'inc/parameters/saveLineName.inc'
          Include 'inc/parameters/saveFrequency.inc'
          Include 'inc/parameters/saveSideBand.inc'
          contC = contCN
          Include 'inc/options/saveDoppler.inc'
       End If
    End Do
    !
    Write (iUnit,*) "!"
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module Receiver,        <-- SR: saveReceiver ")                  ! this allows to trace execution
    !
    Return
  End Subroutine saveReceiver
!!!
!!!
  Subroutine writeXMLreceiver(programName,LINE,commandToSave,         &
       &        iUnit, ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Logical :: errorXML
    !
    Integer :: ii, jj
    Character (len=lenCh) :: valueC
    Character (len=lenLine) :: valueComment
    !
    ERROR = .False.
    !
!!OLD      write (6,*)  "      moduleReceiver --> SR: writeXMLreceiver "
    !
    Include 'inc/startXML/receiver.inc'
    !
    Return
  End Subroutine writeXMLreceiver
!!!
!!!
!!$  Subroutine checkBolometerArray (error,  errorCode)
!!$    !
!!$    Logical,                Intent(OUT)            ::  error
!!$    Character(len=*),       Intent(OUT), Optional  ::  errorCode
!!$    !
!!$    Real(kind=kindSingle)    :: xSource, ySource   
!!$    Real(kind=kindSingle)    :: xArray,  yArray
!!$    !
!!$    Real(kind=kindSingle)    :: epsilon
!!$    !
!!$    Integer                  :: l
!!$    !
!!$    Character(len=lenLine)   :: messageText
!!$    !
!!$    If (        GV%privilege.Eq.setPrivilege%bolometerPool                       &
!!$         & .Or. GV%privilege.Eq.setPrivilege%ncsTeam                             &
!!$         & ) Then                                                                !
!!$       If      (GV%limitCheck.Eq.setLimitCheck%loose) Then
!!$          epsilon = 1000.0
!!$       Else If (GV%limitCheck.Eq.setLimitCheck%relaxed) Then
!!$          epsilon =   10.0
!!$       Else If (GV%limitCheck.Eq.setLimitCheck%strict) Then
!!$          epsilon =    0.1
!!$       Else If (GV%limitCheck.Eq.setLimitCheck%anal) Then
!!$          epsilon =    0.001
!!$       Else
!!$          epsilon =    0.1
!!$       End If
!!$       Write (messageText,*)                                                     & 
!!$            &  " epsilon for consistency check Nasmyth and RX Array offsets: ",  &
!!$            &    epsilon                                                         !
!!$       Call PakoMessage(priorityI,severityI,"RECEIVER bolometer",messageText)
!!$       Write (messageText,*)                                                     & 
!!$            &  " epsilon can be changed with SET LimitCheck "                    !
!!$       Call PakoMessage(priorityI,severityI,"RECEIVER bolometer",messageText)
!!$    Else
!!$       epsilon =    0.1
!!$    End If
!!$    !
!!$    error = .False.
!!$    !
!!$    If (Present(errorCode)) Then
!!$       l = len(errorCode)
!!$       If (l.GE.2) errorCode = "OK"
!!$    End If
!!$    !
!!$    !D     Write (6,*) " --> checkBolometerArray "
!!$    !D     Write (6,*) " GV%limitCheck: ", GV%limitCheck
!!$    !D     Write (6,*) " GV%privilege:  ", GV%privilege
!!$    !
!!$    If (vars(iIn,iBolo)%isConnected                                              &
!!$         &  ) Then                                                               !
!!$       !
!!$       xSource = GV%sourceOffsets(inas)%point%x
!!$       ySource = GV%sourceOffsets(inas)%point%y
!!$       !
!!$       If      (vars(iIn,iBolo)%bolometerName.Eq.bolo%mambo1) Then
!!$          xArray  = MAMBO1%pixel(vars(iIn,iBolo)%channel)%x
!!$          yArray  = MAMBO1%pixel(vars(iIn,iBolo)%channel)%y
!!$       Else If (vars(iIn,iBolo)%bolometerName.Eq.bolo%mambo2) Then
!!$          xArray  = MAMBO2%pixel(vars(iIn,iBolo)%channel)%x
!!$          yArray  = MAMBO2%pixel(vars(iIn,iBolo)%channel)%y
!!$       Else
!!$          xArray  = 0.0
!!$          yArray  = 0.0
!!$       End If
!!$       !
!!$       If (        abs(xSource-xArray).Gt.epsilon                                &
!!$            & .Or. abs(ySource-yArray).Gt.epsilon                                &
!!$            & ) Then                                                             !
!!$          error = .True.
!!$          If (Present(errorCode)) Then
!!$             l = len(errorCode)
!!$             If (l.GE.40) Then                                                       
!!$                Write (errorCode,*) "RX Array: Offsets ",                        &
!!$                     &              xArray, yArray,                              &
!!$                     &              " /System Nasmyth"                           !
!!$             End If
!!$          End If
!!$       End If
!!$       !
!!$    End If
!!$    !
!!$  End Subroutine checkBolometerArray
!!$!!!
!!!
!!$  Subroutine showArray(command)
!!$    !
!!$    Character(len=*),   Intent(in)   :: command
!!$    ! to format message text
!!$    Character(len=lenLine)  ::  messageText
!!$    !
!!$    ! low priority for messages
!!$    Integer, Parameter       ::  priorityIlow = 1
!!$    !
!!$    If (vars(iValue,iHera1)%isConnected) Then
!!$       !
!!$       Write (messageText,*) " HERA1 configuration: "
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " nPixels:     ", HeraCFig%nPixels
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " nPixels HERA1: ", HeraCFig%nPixelsHera1
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " Center Pixel: "
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,'(I,8F9.2," ",A)') HeraCFig%pixel(HeraCFig%iPixelCenter)
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       !
!!$       Write (messageText,*) " HERA1 Pixels: "
!!$       Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$       Do ii = 1, HeraCFig%nPixelsHera1
!!$          Write (messageText,'(I,8F9.2," ",A)') HeraCFig%pixel(ii)
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$       End Do
!!$       !
!!$    End If
!!$    !
!!$    If (vars(iValue,iHera2)%isConnected) Then
!!$       !
!!$       Write (messageText,*) " HERA2 configuration: "
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " nPixels:     ", HeraCFig%nPixels
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " nPixels HERA2: ", HeraCFig%nPixelsHera2
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " Center Pixel: "
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,'(I,8F9.2," ",A)') HeraCFig%pixel(HeraCFig%iPixelCenter)
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,*) " Best Pixel: "
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       Write (messageText,'(I,8F9.2," ",A)') HeraCFig%pixel(HeraCFig%iPixelBest)
!!$       Call PakoMessage(priorityI,severityI,command,messageText)
!!$       !
!!$       Write (messageText,*) " HERA2 Pixels: "
!!$       Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$       Do ii = HeraCFig%nPixelsHera1+1,                                          &
!!$            &  HeraCFig%nPixelsHera1+HeraCFig%nPixelsHera2, 1                    !
!!$          Write (messageText,'(I,8F9.2," ",A)') HeraCFig%pixel(ii)
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$       End Do
!!$       !
!!$    End If
!!$    !
!!$    If (vars(iValue,iBolo)%isConnected) Then
!!$       If (vars(iValue,iBolo)%bolometerName.Eq.bolo%mambo1) Then
!!$          !
!!$          Write (messageText,*) " MAMBO1: "
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels:     ", MAMBO1%nPixels
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels AC:  ", MAMBO1%nPixelsAC
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels DC:  ", MAMBO1%nPixelsDC
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " Center Pixel: "
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,'(I,4F9.3," ",A)') MAMBO1%pixel(MAMBO1%iPixelCenter)
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$!!OLD          Write (messageText,*) " Best Pixel: "
!!$!!OLD          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$!!OLD          Write (messageText,'(I,4F9.3," ",A)') MAMBO1%pixel(MAMBO1%iPixelBest)
!!$!!OLD          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          !
!!$          Write (messageText,*) " AC Pixels: "
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          Do ii = 1, MAMBO1%nPixelsAC, 1
!!$             Write (messageText,'(I,4F9.3," ",A)') MAMBO1%pixel(ii)
!!$             Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          End Do
!!$          !
!!$          Write (messageText,*) " DC Pixels: "
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          Do ii = MAMBO1%nPixelsAC+1, MAMBO1%nPixelsAC+MAMBO1%nPixelsDC, 1
!!$             Write (messageText,'(I,4F9.3," ",A)') MAMBO1%pixel(ii)
!!$             Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          End Do
!!$          !
!!$       End If
!!$       !
!!$       If (vars(iValue,iBolo)%bolometerName.Eq.bolo%mambo2) Then
!!$          !
!!$          Write (messageText,*) " MAMBO2: "
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels:     ", MAMBO2%nPixels
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels AC:  ", MAMBO2%nPixelsAC
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " nPixels DC:  ", MAMBO2%nPixelsDC
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " Center Pixel: "
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,'(I,4F9.3," ",A)') MAMBO2%pixel(MAMBO2%iPixelCenter)
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,*) " Best Pixel: "
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          Write (messageText,'(I,4F9.3," ",A)') MAMBO2%pixel(MAMBO2%iPixelBest)
!!$          Call PakoMessage(priorityI,severityI,command,messageText)
!!$          !
!!$          Write (messageText,*) " AC Pixels: "
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          Do ii = 1, MAMBO2%nPixelsAC, 1
!!$             Write (messageText,'(I,4F9.3," ",A)') MAMBO2%pixel(ii)
!!$             Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          End Do
!!$          !
!!$          Write (messageText,*) " DC Pixels: "
!!$          Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          Do ii = MAMBO2%nPixelsAC+1, MAMBO2%nPixelsAC+MAMBO2%nPixelsDC, 1
!!$             Write (messageText,'(I,4F9.3," ",A)') MAMBO2%pixel(ii)
!!$             Call PakoMessage(priorityIlow,severityI,command,messageText)
!!$          End Do
!!$          !
!!$       End If
!!$       !
!!$    End If
!!$    !
!!$  End Subroutine showArray
!!$!!!
!!!
  Subroutine checkLine(NAME,LNAME,PAR,LPAR,FOUND)
    Character(len=*) NAME,PAR
    Integer LNAME,LPAR
    Logical FOUND
    !
    Integer J,K,J1,J2,NAME_OUT,JK
    Character(len=80) NOM
    Data NAME_OUT/0/
    !
    ! Syntax  is   NAME1[|NAME2[|NAME3]]
    !
!!OLD      Write (6,*) "   ---> SR: checkLine "
!!OLD      write (6,*) "        name   ->", name(1:lname)
!!OLD      Write (6,*) "        par    ->", par(1:lpar)
    !
    If (.Not.FOUND) Then
       JK = 1
       Do While (.Not.FOUND .And. JK.Lt.LPAR)
          J = Index(PAR(JK:LPAR),NAME(1:LNAME))
          FOUND = J.Ne.0
          If (.Not.FOUND) Return
          K = JK-1+J-1
          If (K.Gt.0 .And. PAR(K:K).Ne.'|') FOUND = .False.
          K = JK-1+J+LNAME
          If (K.Le.LPAR .And. PAR(K:K).Ne.'|') FOUND = .False.
          JK = K
       Enddo
       If (.Not.FOUND) Return
       ! Use only one name if NAME_OUT is set
       If (NAME_OUT.Eq.0) Then
          PAR = NAME(1:LNAME)
          LPAR = LNAME
          ! Use all names
       Elseif (NAME_OUT.Eq.-1) Then
          Return
       Endif
    Elseif (NAME_OUT.Eq.-1) Then
       Return
    Else
       ! Extract name of order NAME_OUT
       J = 0
       J1 = 1
       Do While (.True.)
          J2 = Index(PAR(J1:LPAR),'|')
          If (J2.Eq.0) Then
             NOM = PAR(J1:LPAR)
             LPAR = LPAR-J1+1
             PAR = NOM
             Return
          Endif
          J2 = J2+J1-2
          If (J.Eq.NAME_OUT) Then
             NOM = PAR(J1:J2)
             LPAR = J2-J1+1
             PAR = NOM
             Return
          Endif
          J1 = J2+2
          J = J+1
       Enddo
    Endif
  End Subroutine checkLine
!!!
!!!
!!$  Subroutine tempColdLookup (NAMEREC,FREQ,TCI,error,errorCode)
!!$    !!----------------------------------------------------------------------
!!$    !! adapted from T_C_INT from old CS / OBS (AS)
!!$    !!  NCS Version 0.9 2005-06-29 by HU
!!$    !!
!!$    !! OBS 	Support routine to compute the effective cold load
!!$    !!       using linear interpolation 
!!$    !!
!!$    !! Arguments :
!!$    !!       NAMEREC		Nome of receiver		Input
!!$    !!	FREQ	R8	Frequency			Input
!!$    !!	NO_REC	I4	Receiver No.			
!!$    !!	TCI     R4      Interpolated T_cold		Output
!!$    !!	*		Error return label.		
!!$    !!
!!$    !!       Created 	15-Nov-1999		A.Sievers
!!$    !!       Version 1.0     13-Dec-2000             A.Sievers 
!!$    !!       Version 1.1     22-Apr-2003             A.Sievers
!!$    !!	Version 1.1 	New tables, calibration system rebuild 
!!$    !!----------------------------------------------------------------------
!!$    Implicit None
!!$    !!
!!$    Integer NO_REC, TABM, RECM, I, ii, Im
!!$    Parameter (TABM=28,RECM=8)
!!$    Real   TCI,TTAB(RECM,TABM),FTAB(RECM,TABM),FUDGE
!!$    Real*8 FREQ
!!$    !
!!$    Character(len=*) , Intent(in)            :: namerec
!!$    Logical          , Intent(out), Optional :: error
!!$    Character(len=*) , Intent(out), Optional :: errorCode
!!$    !
!!$    ! to format message text
!!$    Character(len=lenLine)  ::  messageText
!!$    !!----------------------------------------------------------------------
!!$    !!
!!$    !!       Setup tables
!!$    !!
!!$    !! A100 New Values from Dave John, error ? several degrees!
!!$    Data (FTAB(1,I),TTAB(1,I),I=1,TABM) / &
!!$         &	 82.0,79.28539427,  84.0,79.36419173, 86.0,78.87304134, & 
!!$         &   88.0,77.33930634,  90.0,74.74394836, 92.0,76.2, &
!!$         &   94.0,80.22309026,  96.0,80.77572836, &
!!$         &   98.0,78.52266313, 100.0,79.49901781, 102.0,79.79847397, &
!!$         &  104.0,80.33021456, 106.0,78.25091009, 108.0,77.21979716, &
!!$         &  110.0,77.06043059, 112.0,78.02250369, 114.0,76.08970391, &
!!$         &  115.5,79.72159487, 115.5,79.72159487, 115.5,79.72159487, &
!!$         &  115.5,79.72159487, 115.5,79.72159487, 115.5,79.72159487, &
!!$         &  115.5,79.72159487, 115.5,79.72159487, 115.5,79.72159487, &
!!$         &  115.5,79.72159487, 115.5,79.72159487/
!!$    !! A230
!!$    Data (FTAB(2,I),TTAB(2,I),I=1,TABM) / & 
!!$         &  204.0,75.64284350, 206.0,81.12907545, 208.0,78.18370586, &
!!$         &  210.0,82.01923300, 212.0,78.51682771, 214.0,72.9, &
!!$         &  216.0,70.91066326, 218.0,74.57344500, &
!!$         &  220.0,72.15172230, 222.0,75.42722651, 224.0,78.58834339, &
!!$         &  226.0,80.50220981, 228.0,79.16053347, 230.0,78.37329700, &
!!$         &  232.0,82.16072320, 234.0,80.66672628, 236.0,76.79622694, &
!!$         &  238.0,77.7, 	   240.0,77.43085170, &
!!$         &  242.0,76.62425994, 244.0,78.40878622, 246.0,78.29355113, &
!!$         &  248.0,80.47119801, 250.0,80.31817053, 252.0,82.18676248, &
!!$         &  254.0,77.25953740, 254.0,77.25953740, 254.0,77.25953740/
!!$
!!$    !! B100 
!!$    Data (FTAB(3,I),TTAB(3,I),I=1,TABM) / &
!!$         &   82.0,68.59386065,  84.0,67.59779737,  86.0,68.69892307,  &
!!$         &   88.0,68.32334428,  90.0,71.82370853,  92.0,71.7, &
!!$         &   94.0,76.54149959,  96.0,72.84129552,  98.0,69.48285138, &
!!$         &  100.0,71.72952787, 102.0,71.17372858, 104.0,71.64114644, &
!!$         &  106.0,71.12030477, 108.0,72.22260376, 110.0,72.43761196, &
!!$         &  112.0,71.6109246,  114.0,71.83854025, 115.5,72.19719516, &
!!$         &  115.5,72.19719516, 115.5,72.19719516, 115.5,72.19719516, &
!!$         &  115.5,72.19719516, 115.5,72.19719516, 115.5,72.19719516, &
!!$         &  115.5,72.19719516, 115.5,72.19719516, 115.5,72.19719516, &
!!$         &  115.5,72.19719516/
!!$    !! B230 
!!$    Data (FTAB(4,I),TTAB(4,I),I=1,TABM) / &
!!$         &  204.0,66.90966056, 206.0,66.07041291, 208.0,66.60681411, &
!!$         &  210.0,66.37912687, 212.0,67.0352464,  214.0,66.6, &
!!$         &  216.0,68.53212206, 218.0,67.55091886, 220.0,68.64164677, &
!!$         &  222.0,67.59334743, 224.0,67.74791837, 226.0,70.52761081, &
!!$         &  228.0,68.19831677, 230.0,70.91007502, 232.0,72.20539123, &
!!$         &  234.0,70.34649618, 236.0,69.66383092, 238.0,70.8, &
!!$         &  240.0,70.42851594, 242.0,67.82191156, 244.0,67.78787879, &
!!$         &  246.0,71.45622477, 248.0,72.37223837, 250.0,70.44579107, &
!!$         &  252.0,72.37912211, 254.0,68.31875688, 254.0,68.31875688, &
!!$         &  254.0,68.31875688/
!!$    !! C150 
!!$    Data (FTAB(5,I),TTAB(5,I),I=1,TABM) / &
!!$         &  130,77.47554342,   132,76.60257551,   134,79.18560725, &
!!$         &  136,78.678192,     138,87.7390095,    140,83.894388, &
!!$         &  142,81.54445385,   144,76.97783123,   146,82.11088438, &
!!$         &  148,77.50291049,   150,76.75835286,   152,77.02970026, &
!!$         &  154,80.09240042,   156,77.76880217,   158,82.11060296, &
!!$         &  160,82.41046061,   162,75.24133874,   164,77.08489482, &
!!$         &  166,81.49741556,   168,78.99862302,   170,75.95060729, &
!!$         &  172,78.53174021,   174,78.21234799,   176,76.67341811, &
!!$         &  178,77.91047312,   180,79.21411521,   182,79.74872126, &
!!$         &  183,80.27693657/
!!$
!!$    !! C270
!!$    Data (FTAB(6,I),TTAB(6,I),I=1,TABM) / &
!!$         &  240,80.37402572,   242,79.48600082,   244,79.40036566, &
!!$         &  246,80.84111917,   248,86.28865985,   250,83.91799072, &
!!$         &  252,82.26435805,   254,86.46439072,   256,79.72362894, &
!!$         &  258,78.56262117,   260,82.73993708,   262,75.23604406, &
!!$         &  264,72.22125557,   266,79.69740481,   268,81.80502499, &
!!$         &  270,83.36979793,   272,75.18306892,   274,84.00889837, &
!!$         &  276,83.77066928,   278,81.10882111,   280,78.48633341, &
!!$         &  280,78.48633341,   280,78.48633341,   280,78.48633341, &
!!$         &  280,78.48633341,   280,78.48633341,   280,78.48633341, &
!!$         &  280,78.48633341/
!!$    !! D150 
!!$    Data (FTAB(7,I),TTAB(7,I),I=1,TABM) / &
!!$         &  130,69.97449774,   132,67.07968514,   134,70.36562075, &
!!$         &  136,68.3368046,    138,70.83212109,   140,76.90886613, &
!!$         &  142,72.90028819,   144,74.37918861,   146,68.99538888, &
!!$         &  148,71.2387818,    150,70.3398638,    152,66.13024442, &
!!$         &  154,69.59240935,   156,68.53318476,   158,65.47794253, &
!!$         &  160,69.27703072,   162,71.34096847,   164,69.48283335, &
!!$         &  166,69.62980543,   168,72.70242694,   170,69.98561696, &
!!$         &  172,71.12030916,   174,70.90413314,   176,62.5191961, &
!!$         &  178,65.97821174,   180,67.00975835,   182,68.06622192, &
!!$         &  183,67.99825614/
!!$    !! D270
!!$    Data (FTAB(8,I),TTAB(8,I),I=1,TABM) / &
!!$         &  240,72.01136077,   242,70.30988412,   244,69.42904474, &
!!$         &  246,69.61226319,   248,73.89558941,   250,73.85412535, &
!!$         &  252,74.26176233,   254,73.99950189,   256,75.40678669, &
!!$         &  258,70.93697571,   260,72.99616393,   262,72.84875039, &
!!$         &  264,70.87394447,   266,71.48251662,   268,72.41725601, &
!!$         &  270,70.17013115,   272,73.47033715,   274,72.61650426, &
!!$         &  276,74.81469649,   278,76.25131173,   280,74.37153512, &
!!$         &  280,74.37153512,   280,74.37153512,   280,74.37153512, &
!!$         &  280,74.37153512,   280,74.37153512,   280,74.37153512, &
!!$         &  280,74.37153512/
!!$    !!
!!$    error = .False.
!!$    errorCode = GPnone
!!$    !!
!!$    NO_REC=0
!!$    !!
!!$    !!* Temporary disabled 2002-11-27 
!!$    !!        print*,'Table of Cold Load Temperatures disabled, need to
!!$    !!     &         SET CHOPPER '
!!$    !!        return
!!$
!!$    !!	For now the temperatures are systematical higher than previously
!!$    !!	Date 2002-12-05 A.S. Implement a fudge factor
!!$    !!	FUDGE=1.34
!!$    !!	Date 2003-02-25 AS disable for now
!!$    !!	Date 2004-01-13 AS back to one! I have heard!
!!$    !!	FUDGE=1.0
!!$    !!	I have heard that we have a new fudge factor 2003-11-30 AS
!!$    !!	FUDGE=0.9
!!$    !!       From Dave John. Implemented 2004-04-06
!!$    !!	FUDGE=0.94
!!$    !!	FUDGE=1.04 ! From Dave John. Implemented 2004-04-28
!!$    !!	FUDGE=1.00 ! From Dave John. Implemented 2004-07-07
!!$    !!	FUDGE=0.97 ! From Dave John. Implemented 2004-11-23
!!$    !!	FUDGE=0.91 ! From Dave John. Implemented 2005-02-01
!!$    !!	FUDGE=0.89 ! From Dave John. Implemented 2005-03-29
!!$    FUDGE=0.937 ! From DJ 2005-06-01 via email AS 2005-06-27
!!$    FUDGE=1.0   ! From DJ 2005-10    via email AS 2005-10-18
!!$    FUDGE=1.05  ! From DJ 2006-07    pako v1.0.4
!!$    FUDGE=1.18  ! From DJ 2006-09-26 pako v1.0.4.1
!!$    FUDGE=1.11  ! From DJ 2006-10-25 pako v1.0.4.2
!!$    FUDGE=1.00  ! From DJ 2007-01-09 pako v1.0.6.1
!!$    !!
!!$    !!
!!$    tci = -1.0  !! added by HU to support check below: If (TCI .Lt. 0.0) Then
!!$    !!
!!$    !!* Find the Receiver Number using its name
!!$    If     (NAMEREC(1:4).Eq. 'A100') Then
!!$       NO_REC=1
!!$    Else If(NAMEREC(1:4).Eq. 'A230') Then
!!$       NO_REC=2 
!!$    Else If(NAMEREC(1:4).Eq. 'B100') Then
!!$       NO_REC=3
!!$    Else If(NAMEREC(1:4).Eq. 'B230') Then
!!$       NO_REC=4
!!$    Else If(NAMEREC(1:4).Eq. 'C150') Then
!!$       NO_REC=5
!!$    Else If(NAMEREC(1:4).Eq. 'C270') Then
!!$       NO_REC=6
!!$    Else If(NAMEREC(1:4).Eq. 'D150') Then
!!$       NO_REC=7
!!$    Else If(NAMEREC(1:4).Eq. 'D270') Then
!!$       NO_REC=8 
!!$    Else
!!$       error = .True.
!!$       If (Len(errorCode).Ge.lenCH) Then
!!$          Write(errorCode,*) 'NOTABLE: no table of cold load temp for: ',NAMEREC(1:4),' --> set manually '
!!$       End If
!!$       Return
!!$    End If
!!$    !!* Should not happen
!!$    If (NO_REC.Lt.1.Or.NO_REC.Gt.RECM) Then
!!$       TCI=-1.0
!!$    End If
!!$    !!
!!$    Do I=1,TABM
!!$       If(FREQ .Lt. FTAB(NO_REC,I)) Then
!!$          If(I.Eq.1) Then
!!$             !!               Outside table (frequency low)
!!$             TCI=TTAB(NO_REC,I)
!!$             TCI = FUDGE*TCI
!!$             error = .True.
!!$             If (Len(errorCode).Ge.lenCH) Then
!!$                Write(errorCode,'(a,F11.6,a)') &
!!$                     &		'NOTFOUND: frequency ',FREQ,' out of range of table of cold load temp  --> set manually '
!!$             End If
!!$             Return
!!$          End If
!!$          TCI = (TTAB(NO_REC,I) - TTAB(NO_REC,I-1)) / &
!!$               &	    (FTAB(NO_REC,I) - FTAB(NO_REC,I-1)) *  &
!!$               &	    (FREQ - FTAB(NO_REC,I-1)) + TTAB(NO_REC,I-1)
!!$          !!             Finished
!!$          TCI = FUDGE*TCI
!!$          Write (messageText,*)                                                  &
!!$               ' cold load correction factor applied: ', fudge
!!$          Call PakoMessage(priorityI,severityI,'Receiver /tempLoad ',messageText)
!!$          Return
!!$       End If
!!$    End Do
!!$    !!
!!$    !!	do Im=1,TABM      
!!$    !!	   write(99,'(1x,16f8.2)')(FTAB(ii,Im),TTAB(ii,Im),ii=1,8)
!!$    !!	end do
!!$    !!
!!$    If (TCI .Lt. 0.0) Then
!!$       !! Nothing found in table
!!$       !! Outside table high frequency side (take the last value in table
!!$       TCI=TTAB(NO_REC,TABM)
!!$       TCI = FUDGE*TCI
!!$       error = .True.
!!$       If (Len(errorCode).Ge.lenCH) Then
!!$          Write(errorCode,'(a,F11.6,a)') &
!!$               &		'NOTFOUND: frequency ',FREQ,' out of range of table of cold load temp --> set manually '
!!$       End If
!!$    End If
!!$    Return
!!$  End Subroutine tempColdLookup
!!$!!!
!!!
End Module modulePakoReceiver
!!!
!!!
!!! -----------------------------------------------------------------------------







!!
!!  TBD (?) : default beam efficiency, if /scale beam and no explicit values
!!            NOTE: 
!!            this would need an additional (2nd) default beam efficiency
!!            in variables .inc and ranges .inc
!!
!!OLD    If (.Not.ERROR) Then
!!OLD       !
!!OLD       If (hasScale .And. .Not.hasEfficiency) Then
!!OLD          If (vars(iIn,iRec)%scale .Eq. scale%beam) Then
!!OLD             !
!!OLD             !D Write (6,*) " --> will set default efficiencies "
!!OLD             !
!!OLD             vars(iIn,iRec)%effForward = vars(iDefault,iRec)%effForward
!!OLD             vars(iIn,iRec)%effBeam    = vars(iDefault,iRec)%effBeam
!!OLD             !
!!OLD             Write (messageText,*)                                               &
!!OLD                  &      "implied: /efficiency ",                                &
!!OLD                  &                    vars(iDefault,iRec)%effForward,           &
!!OLD                  &                    vars(iDefault,iRec)%effBeam,              &
!!OLD                  &      "(default values) "                                     !
!!OLD             Call PakoMessage(priorityW,severityW,command,messageText)
!!OLD             Write (messageText,*)                                               &
!!OLD                  &      "to get other /efficiency values ",                     &
!!OLD                  &      "set them explicitly "                                  !
!!OLD             Call PakoMessage(priorityI,severityI,command,messageText)
!!OLD             !
!!OLD          End If
!!OLD       End If
!!OLD       !
!!OLD    End If

!!OLD    If (GV%doDebugMessages) Then
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " --> SECOND GENERATION OF esbAll esbWish esbCoax "
!!OLD       Write (6,*) " "
!!OLD    End If
!!OLD    !
!!OLD    !  **  EMIR: generate list of selected sub bands                             !!  EMIR upgrade 2011-11-11
!!OLD    !
!!OLD    !   *  reset:
!!OLD    iiSubBand = 0
!!OLD    EMIRsubBands(:) = esbNone
!!OLD    !
!!OLD    esbAll   =  esbAllParameter 
!!OLD    esbWish  =  esbWishParameter
!!OLD    esbCoax  =  esbCoaxParameter
!!OLD    !
!!OLD    !   *  go through all EMIR (sub)bands:
!!OLD    !
!!OLD    Do ii = iE090, iE300, 1                                                      !  4 EMIR bands
!!OLD       !
!!OLD       If (listRX(ii)%isConnected) Then
!!OLD          !
!!OLD          Do jj = 1, 2, 1                                                        !  2 polarizations
!!OLD             !
!!OLD             Select Case (jj)
!!OLD             Case (1) 
!!OLD                polarization  =  pol%hor
!!OLD                subbandsJJ      =  listRX(ii)%horizontalSubbands
!!OLD             Case (2) 
!!OLD                polarization  =  pol%ver
!!OLD                subbandsJJ      =  listRX(ii)%verticalSubbands
!!OLD             End Select
!!OLD             !D Write (6,*)  "      jj, polarization, subbandsJJ: ", jj, polarization, subbandsJJ
!!OLD             !
!!OLD             Do kk = 1, 4, 1                                                     !  up to 4 sub/side bands
!!OLD                !
!!OLD                Select Case (kk)
!!OLD                Case (1)
!!OLD                   sideband      =  esb%lsb
!!OLD                   subband       =  esb%lo
!!OLD                Case (2) 
!!OLD                   sideband      =  esb%lsb
!!OLD                   subband       =  esb%li
!!OLD                Case (3) 
!!OLD                   sideband      =  esb%usb
!!OLD                   subband       =  esb%ui
!!OLD                Case (4) 
!!OLD                   sideband      =  esb%usb
!!OLD                   subband       =  esb%uo
!!OLD                End Select
!!OLD                !D Write (6,*) "      kk, sideband, subband: ", kk, sideband, subband
!!OLD                !
!!OLD                If     (    Index(subbandsJJ,sideband(1:3)) .Ge.1 .Or.              &
!!OLD                     &      Index(subbandsJJ, subband(1:3)) .Ge.1                   &
!!OLD                     & ) Then                                                       !
!!OLD                   !
!!OLD                   iiSubBand = iiSubBand+1
!!OLD                   !D Write (6,*) " iiSubBand incremented to: ", iiSubBand
!!OLD                   !
!!OLD                   If (iiSubBand.Le.nEsbIF) Then
!!OLD                      EMIRsubBands(iiSubBand)%i            =  iiSubband
!!OLD                      EMIRsubBands(iiSubBand)%bandName     =  listRX(ii)%name
!!OLD                      EMIRsubBands(iiSubBand)%polarization =  polarization
!!OLD                      EMIRsubBands(iiSubBand)%sideband     =  sideband
!!OLD                      EMIRsubBands(iiSubBand)%subband      =  subband
!!OLD                      EMIRsubBands(iiSubBand)%isRequested  =  .True.
!!OLD                   End If
!!OLD                   !
!!OLD                End If
!!OLD                !
!!OLD             End Do
!!OLD             !
!!OLD          End Do
!!OLD          !
!!OLD       End If
!!OLD       !
!!OLD    End Do
!!OLD    !
!!OLD    Do ii = 1, nEMIRsbWish, 1
!!OLD       !
!!OLD       If (         EMIRsubbands(ii)%isRequested                                 &
!!OLD            &       ) Then                                                       !
!!OLD          esbWish(ii) = EMIRsubBands(ii)
!!OLD          !D           Write (6,*)   "   esbWish "
!!OLD          !D           Write (6,110) esbWish(ii)
!!OLD          !D           Write (6,*) "  esbWish(ii)%bandName -->",esbWish(ii)%bandName,"<--"
!!OLD          !D           Write (6,*) "  rec%E150             -->",rec%E150,"<--"
!!OLD          !
!!OLD          Do jj = 1, nEMIRsbAll, 1
!!OLD             !
!!OLD             If (     (      esbWish(ii)%bandName    .Eq.rec%E150                &
!!OLD                  &    .And. esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
!!OLD                  &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
!!OLD                  &   )                                                          &
!!OLD                  &  .Or.                                                        &
!!OLD                  &   (      esbWish(ii)%bandName    .Eq.esbAll(jj)%bandName     &
!!OLD                  &    .And. esbWish(ii)%polarization.Eq.esbAll(jj)%polarization &
!!OLD                  &    .And. esbWish(ii)%sideband    .Eq.esbAll(jj)%sideband     &
!!OLD                  &    .And. esbWish(ii)%subband     .Eq.esbAll(jj)%subband      &
!!OLD                  &   )                                                          &
!!OLD                  &  ) Then                                                      !
!!OLD                !
!!OLD                esbAll(jj)%isRequested = .True.
!!OLD                !D                 Write (6,*)   "   esbAll "
!!OLD                !D                 Write (6,110) esbAll(jj)
!!OLD                !
!!OLD                kk = esbAll(jj)%i4                                               !!  %i4 codes to which Coax cable (1 to 4)
!!OLD                !                                                                !!  this subband goes
!!OLD                !                                                                !!  kk is safe in range 1 to 4 if %i4 is OK
!!OLD                esbCoax(kk)   = esbAll(jj)
!!OLD                esbCoax(kk)%i = kk                                               !!  --> %i  codes number (1 to 8) of Coax
!!OLD                !
!!OLD                If (      esbWish(ii)%bandName    .Eq.rec%E150) Then             !!  for E150: 
!!OLD                   esbCoax(kk)%sideband = esbWish(ii)%sideband                   !!  sideband and 
!!OLD                   esbCoax(kk)%subband  = esbWish(ii)%subband                    !!  subband from user wish
!!OLD                End If
!!OLD                If (.Not. esbWish(ii)%bandName    .Eq.rec%E150) Then             !!  for E090, E230, E330:
!!OLD                   esbCoax(kk+4)   = esbCoax(kk)                                 !!  outer subbands on Coax 5 to 8
!!OLD                   esbCoax(kk+4)%i = kk+4
!!OLD                   If      (esbCoax(kk)%subband.Eq.esb%li) Then
!!OLD                      esbCoax(kk+4)%subband = esb%lo
!!OLD                   Else If (esbCoax(kk)%subband.Eq.esb%ui) Then
!!OLD                      esbCoax(kk+4)%subband = esb%uo
!!OLD                   End If
!!OLD                End If
!!OLD                !
!!OLD             End If
!!OLD             !
!!OLD          End Do
!!OLD          !
!!OLD       End If
!!OLD    End Do
!!OLD    !
!!OLD    If (GV%doDebugMessages) Then
!!OLD       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of All (32) subbands"
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEMIRsbAll, 1
!!OLD          !
!!OLD          Write (6,110) esbAll(ii)
!!OLD          !
!!OLD       End Do
!!OLD       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of up to 4 requested subbands EMIRsubbands "
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEsbIF, 1
!!OLD          !
!!OLD          Write (6,110) EMIRsubBands(ii)
!!OLD          !
!!OLD       End Do
!!OLD       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of up to 4 requested subbands esbWish "
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEMIRsbWish, 1
!!OLD          !
!!OLD          If (esbWish(ii)%isRequested) Then
!!OLD             Write (6,110) esbWish(ii)
!!OLD          Else
!!OLD             Write (6,112) esbWish(ii)%i
!!OLD          End If
!!OLD          !
!!OLD       End Do
!!OLD       !
!!OLD       Write (6,*) " "
!!OLD       Write (6,*) " list of    subbands on Coax 1 to 8 "
!!OLD       Write (6,*) " "
!!OLD       Write (6,100)
!!OLD       !
!!OLD       Do ii = 1, nEMIRsbCoax, 1
!!OLD          !
!!OLD          If (esbCoax(ii)%isRequested) Then
!!OLD             Write (6,110) esbCoax(ii)
!!OLD          Else
!!OLD             Write (6,112) esbCoax(ii)%i
!!OLD          End If
!!OLD          !
!!OLD       End Do
!!OLD       !
!!OLD    End If
!!OLD    Write (6,*)
