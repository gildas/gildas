!
! -----------------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoSwWobbler">
!   <VERSION>
!                Id: modulePakoSwWobbler.f90,v 1.2.2 2013-02-14 Hans Ungerechts
!                    checked wrt pako                2012-11-30
!                Id: modulePakoSwWobbler.f90,v 1.2.1 2012-08-13 Hans Ungerechts
!                based on
!                Id: modulePakoSwWobbler.f90,v 1.1.3 2010-1-18  Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Switchinging Modes
!   </FAMILY>
!   <SIBLINGS>
!                SwBeam
!                SwFrequency
!                SwTotalPower
!                SwWobbler
!   </SIBLINGS>        
!
!   Pako module for command: SWWOBBLER
!
! </DOCUMENTATION> <!-- name="modulePakoSwWobbler" -->
!
!----------------------------------------------------------------------
!
Module modulePakoSwWobbler
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: swWobbler, displaySwWobbler, saveSwWobbler, writeXMLswWobbler
  !     
  ! *** Variables for swWobbler ***
  Include 'inc/variables/variablesSwWobbler.inc'
  !     
!!!   
Contains
!!!
!!!
  Subroutine swWobbler(programName,line,command,error)
    !
    ! *** Arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Real(Kind=kindSingle)   ::  rr
    !
    Include 'inc/ranges/rangesSwWobbler.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module swWobbler, v1.2.1 2012-08-13 ---> SR: swWobbler ")          ! this allows to trace execution
    !
    !D    Write (6,*) " module swWobbler ---> SR: swWobbler "
    !D    Write (6,*) "                          "
    !D    Write (6,*) "        swModeSelected:   ", swModeSelected
    !
    ! *** initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/swWobblerSetDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    Call checkNofParameters(command,                                             &
         &     0, 2,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersWoffsets.inc'      
       Include 'inc/options/readOptionsSwWobbler.inc'
    End If
    !
    ! *** set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/swWobblerSetDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!)   ***
    ! *** and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersWoffsets.inc'
       Include 'inc/options/readOptionsSwWobbler.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** derived parameters                              ***
    !
    If (.Not.ERROR) Then
       !
       rr                     =     vars(iIn)%tPhase                             &
            &                     * vars(iIn)%nPhases                            &
            &                     * vars(iIn)%nCycles                            !
       !
       Write (messageText,*)                                                     &
            &                 " number of phases = nPhases =  ",                 &
            &                 vars(iIn)%nPhases                                  !
       Call PakoMessage(priorityI,severityI,command,messageText)
       Write (messageText,*)                                                     &
            &                 " time per record = tRecord = " //                 &
            &                 " tPhase*nPhases*nCycles = " ,                     &
            &                 rr                                                 !
       If      (rr.Gt.vars(iLimit2)%tRecord) Then
          Call PakoMessage(priorityE,severityE,command,messageText)
       Else If (rr.Gt.vars(iStd2)%tRecord) Then
          Call PakoMessage(priorityW,severityW,command,messageText)
       Else 
          Call PakoMessage(priorityI,severityI,command,messageText)
       End If
       !
       Call checkR4("SWW tRecord ",rr,                                           &
            &           vars%tRecord,                                            &
            &           errorR)                                                  !
       !
       If (errorR) Then
          Write (messageText,*)                                                  &
               &         " tRecord = tPhase*nPhases*nCycles "//                  &
               &         " is outside limits "                                   !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       ERROR = ERROR .or. errorR
       !
    End If
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.ERROR) Then
       !
       errorInconsistent = .False.
       !
       Include 'inc/options/checkConsistentWoffsetsTphase.inc'
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from Temp into persistent variables   ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
       GV%wOffset(1) = vars(iValue)%wOffset(1)
       GV%wOffset(2) = vars(iValue)%wOffset(2)
       GV%wAngle     = vars(iValue)%wAngle
       !D       Write (6,*) " GV%wOffset(1) ", GV%wOffset(1) 
       !D       Write (6,*) " GV%wOffset(2) ", GV%wOffset(2) 
       !D       Write (6,*) " GV%wAngle ", GV%wAngle
    End If
    !
    ! *** display values ***
    If (.Not.ERROR) Then
       Call displaySwWobbler
    End If
    !
    ! *** set "selected" switching mode ***
    If (.Not.error) Then
       GV%switchingMode     = swMode%Wobb
       GV%tPhase            = vars(iValue)%tPhase
       GV%tRecord           = vars(iValue)%tRecord
       GV%notReadySWafterOM = .True.
       GV%notReadyFocus     = .True.
       GV%notReadyOnOff     = .True.
       GV%notReadyOtfMap    = .True.
       GV%notReadyPointing  = .True.
       GV%notReadyTrack     = .True.
       !
       Include 'inc/messages/messageSwModeSelected.inc'
       !
       swWobblerCommand = line(1:lenc(line))
    End If
    !
    !D    Write (6,*) "      GV%tRecord:  ", GV%tRecord
    !
!!$    ! ** warnings about OMs that depend on SW Wobbler:
!!$    !
!!$!!OLD    If (GV%observingMode .Eq. OM%Focus) Then
!!$!!OLD       GV%notReadyFocus = .True.
!!$!!OLD       messageText = "Please re-execute "//OM%Focus//" for Wobbler."
!!$!!OLD       Call PakoMessage(priorityW,severityW,command,messageText)
!!$!!OLD       messageText = "Recommended Usage: FOCUS"
!!$!!OLD       Call PakoMessage(priorityI,severityI,command,messageText)
!!$!!OLD    End If
!!$!!OLD    !
!!$!!OLD    If (GV%observingMode .Eq. OM%OnOff) Then
!!$!!OLD       GV%notReadyOnOff = .True.
!!$!!OLD       messageText = "Please re-execute "//OM%OnOff//" for Wobbler."
!!$!!OLD       Call PakoMessage(priorityW,severityW,command,messageText)
!!$!!OLD       messageText = "Recommended Usage: ONOFF /SWW"
!!$!!OLD       Call PakoMessage(priorityI,severityI,command,messageText)
!!$!!OLD    End If
!!$!!OLD    !
!!$!!OLD    If (GV%observingMode .Eq. OM%OtfMap) Then
!!$!!OLD       GV%notReadyOtfMap = .True.
!!$!!OLD       messageText = "Please re-execute "//OM%OtfMap//" for Wobbler."
!!$!!OLD       Call PakoMessage(priorityW,severityW,command,messageText)
!!$!!OLD       messageText = "Recommended Usage: OTFMAP "
!!$!!OLD       Call PakoMessage(priorityI,severityI,command,messageText)
!!$!!OLD    End If
!!$!!OLD    !
!!$!!OLD    If (GV%observingMode .Eq. OM%Pointing) Then
!!$!!OLD       GV%notReadyPointing = .True.
!!$!!OLD       messageText = "Please re-execute "//OM%Pointing//" for Wobbler."
!!$!!OLD       Call PakoMessage(priorityW,severityW,command,messageText)
!!$!!OLD       messageText = "Recommended Usage: POINTING"
!!$!!OLD       Call PakoMessage(priorityI,severityI,command,messageText)
!!$!!OLD    End If
    !
    Return
  End Subroutine swWobbler
!!!
!!!
  Subroutine displaySwWobbler
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character (len=24)      ::  command
    Character (len=lenVar)  ::  displayLine   = GPnone
    ! 
    Include 'inc/display/commandDisplaySwWobbler.inc'
    !
  End Subroutine displaySwWobbler
!!!
!!!
  Subroutine saveSwWobbler(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    contC = contNN
    !
    B = '\'   !   '
    S = ' '
    CMD =  programName//B//"SWWOBBLER"
    lCMD = lenc(CMD)
    !
    ERROR = .False.
    !
    Write (iUnit,*) "! "
    Write (iUnit,*) "! ", CMD(1:lCMD)
    Write (iUnit,*) CMD(1:lCMD), " -"
    !
    contC = contCC
    !
    Include 'inc/parameters/saveWoffsets.inc'
    !
    !TBD Include 'inc/options/saveWangle.inc'
    !
    contC = contCN
    !
    Include 'inc/options/saveTphase.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveSwWobbler
!!!
!!!
  Subroutine writeXMLswWobbler(programName,LINE,commandToSave,                   &
       &        iUnit, ERROR)                                                    !
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)  :: valueC
    Character (len=lenVar) :: valueComment
    Real                   :: value
    Logical                :: errorXML
    !
    ERROR = .False.
    !
    !
    !D      write (6,*)  "      moduleSwWobbler --> SR: writeXMLWobbler "
    !
    Include 'inc/startXML/swWobbler.inc'
    !
    Return
  End Subroutine writeXMLswWobbler
!!!
!!!
End Module modulePakoSwWobbler
!!!
!!!  ************************
!!!  ************************
!!!  *****  END OF CODE *****
!!!  ************************
!!!  ************************
!!!
