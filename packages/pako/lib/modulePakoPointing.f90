!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoPointing">
!   <VERSION>
!                Id: modulePakoPointing.f90,v 1.2.3  2014-12-15 Hans Ungerechts
!                Id: modulePakoPointing.f90,v 1.2.3  2014-10-27 Hans Ungerechts
!                Id: modulePakoPointing.f90,v 1.2.3  2014-09-23 Hans Ungerechts
!                Id: modulePakoPointing.f90,v 1.2.3  2014-02-03 Hans Ungerechts
!                                           v 1.2.1  2012-09-21 Hans Ungerechts
!                Id: modulePakoPointing.f90,v 1.2.0  2012-08-16 Hans Ungerechts
!                based on
!                Id: modulePakoPointing.f90,v 1.1.12 2012-05-30 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>
!                Calibrate
!                Focus
!                OnOff
!                OtfMap
!                Pointing
!                Tip
!                Track
!                VLBI
!   </SIBLINGS>        
!
!   Pako module for command: POINTING
!
! </DOCUMENTATION> <!-- name="modulePakoPointing" -->
!
!----------------------------------------------------------------------
!
Module modulePakoPointing
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  Use modulePakoDIYlist
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: pointing
  Public :: savePointing
  Public :: startPointing
  Public :: displayPointing
  Public :: plotPointing
  !     
  ! *** Variables for pointing ***
  Include 'inc/variables/variablesPointing.inc'
  !
  Real    :: swWobblerAmplitude       = 0.0
  Real    :: swWobblerShiftAzimuth    = 0.0       !   Shift in Azimuth when Wobbler Switching
  Real    :: swWobblerShiftElevation  = 0.0       !   Shift in Elevation when Wobbler Switching
  !
!!OLD  Real (kind=kindSingle) :: angleFactorR
!!OLD  Real (kind=kindDouble) :: angleFactorD
!!!   
!!!   
Contains
!!!   
!!!
  Subroutine pointing(programName,line,command,                    &
       &        error)
    !
    ! *** Arguments: ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical :: isConnected   = .False.
    !
    Character (len=lenCh) ::  bolometerName
    !
    Real                  :: vIn, vStdMin, vStdMax, vLimitMin, vLimitMax
    !     
    Real                  :: vMaxX,   vMaxY, vLimit
    Real                  ::                 vLimitAz, vLimitEl
    Real                  :: vMaxXel, vMaxYel, MaxEl
    !
    Character(len=lenCh)   ::  sysName  = GPnone
    !
    Type(conditionMinMax)  ::  condElevation                                     &
         &                     = conditionMinMax("elevation",                    &
         &                                        0.0, Pi/2.0,                   &
         &                                       "rad",                          &
         &                                       .False.         )               ! 
    !
    Include 'inc/ranges/rangesPointing.inc'
    !
    !D    Write (6,*) " modulePako pointing ---> SR: pointing 21"
    !D    Write (6,*) "                          "
    !D    Write (6,*) " programName:  ->", programName(1:lenc(programName)), "<-"
    !D    Write (6,*) " line:         ->", line(1:lenc(line)), "<-"
    !D    Write (6,*) " command:      ->", command(1:lenc(command)), "<-"
    !D    Write (6,*) " error:        ->", error
    !
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Pointing, v 1.2.3  2014-02-03 ---> SR: Pointing ")          ! trace execution
    !
    !  **   initialize:   ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    !  **   set In-values = previous Values   ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    ! *** read Parameters, Options (to detect errors)         ***
    ! *** and check for validity and ranges                   ***
    Call checkNofParameters(command,                                             &
         &     0, 1,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersLength.inc'
       Include 'inc/options/readOptionsPointing.inc'
    End If
    !
    !**   set defaults   ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
    !
    ! ***   read Parameters, Options (again, after defaults!)   ***
    ! ***   and check for validity and ranges                   ***
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       !
       !   old style v < 2
       Include 'inc/parameters/parametersLength.inc'
       Include 'inc/options/readOptionsPointing.inc'
!!$       !  test style v >= 2 (2012-08-10):
!!$       !  ** to support /default /doubleBeam --> doubleBeam **
!!$       Include 'inc/options/optionDoubleBeam.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** check consistency and                           ***
    ! ***   store from In into persistent variables       ***
    If (.Not.error) Then
       errorInconsistent = .False.
       !
       !! following check (speed) compare OM OTFMAP / alternativeSpeedTotfEnd.inc
       !
       vars(iIn)%speedStart = vars(iIn)%length/vars(iIn)%tOtf
       vars(iIn)%speedEnd   = vars(iIn)%length/vars(iIn)%tOtf
       !
       vIn       = vars(iIn)%speedStart
       vStdMin   = Min(vars(iStd1)%speedStart,  vars(iStd2)%speedStart)
       vStdMax   = Max(vars(iStd1)%speedStart,  vars(iStd2)%speedStart)     
       vLimitMin = Min(vars(iLimit1)%speedStart,vars(iLimit2)%speedStart)
       vLimitMax = Max(vars(iLimit1)%speedStart,vars(iLimit2)%speedStart)
       !
       If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
          errorInconsistent = .True.
          Write (messageText,'(a,F8.1,a,F8.1,a,F8.1,a,F8.1)')                    &
               &           ' value ',                  vars(iIn)%tOtf,           &
               &           ' implies /speed ',         vIn,                      &
               &           ' outside limits ',         vLimitMin,                &
               &           ' to ',                     vLimitMax                 !
          Call pakoMessage(priorityE,severityE,                                  &
               &                command//" /tOtf", messageText)                  !
       Else If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
          Write (messageText,'(a,F8.1,a,F8.1,a,F8.1,a,F8.1)')                    &
               &           ' value ',                  vars(iIn)%tOtf,           &
               &           ' implies /speed ',         vIn,                      &
               &           ' outside standard range ', vStdMin,                  &
               &           ' to ',                     vStdMax                   !
          Call pakoMessage(priorityW,severityW,                                  &
               &                command//" /tOtf", messageText)                  !
       End If
       !
       If (vars(iIn)%tOtf .Lt. 10*GV%tRecord) Then
          errorInconsistent = .True.
          Write (messageText,*)    "must be at least 10 times "                  &
               &                // "tRecord = tPhase*nPhases*nCycles = "         &
               &                 ,  GV%tRecord                                   &
               &                 , "of switching mode "//GV%switchingMode        !
          Call pakoMessage(priorityE,severityE,                                  &
               &           command(1:l)//"/"//"tOtf",messageText)                !
       End If
       !
       !! TBD 2.0: !!$       Call queryReceiver(rec%bolo, isBolometer)
       !! TBD 2.0: !!$       !D Write (6,*) "BOLOMETER isBolometer", isBolometer
       !! TBD 2.0: !!$       If (vars(iIn)%doCalibrate .And. isBolometer) Then
       !! TBD 2.0: !!$          Write (messageText,*)  "/CALIBRATE will not be used with BOLOMETER"
       !! TBD 2.0: !!$          Call PakoMessage(priorityW,severityW,command//"",messageText)
       !! TBD 2.0: !!$       End If
       !
!!OLD       If (GV%privilege.Eq.setPrivilege%user                                     &
!!OLD            &   .And. vars(iIn)%doDoubleBeam .And. .Not.isBolometer) Then        !
!!OLD          errorInconsistent   = .True.
!!OLD          Write (messageText,*)  "/DOUBLEBEAM can only be used with BOLOMETER"
!!OLD          Call PakoMessage(priorityE,severityE,command//"",messageText)
!!OLD       End If
       !
       If (vars(iIn)%doDoubleBeam .And. .Not.GV%switchingMode.Eq.swMode%Wobb) Then
          errorInconsistent   = .True.
          Write (messageText,*)  "/DOUBLEBEAM can only be used with SWWOBBLER"
          Call PakoMessage(priorityE,severityE,command//"",messageText)
       End If
       !
       If (GV%switchingMode.Eq.swMode%Freq) Then
          errorInconsistent   = .True.
          GV%notReadyPointing = .True.
          Write (messageText,*)  "will not work with switching mode ", swModePako%Freq
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    If (.Not.error) Then
       !
       vMaxX  =   vars(iTemp)%length/vars(iTemp)%tOtf
       vMaxY  =   vars(iTemp)%length/vars(iTemp)%tOtf
       !
!!OLD       !! NOTE: this code replaced on 2012-05-30 by code from OTFMAP (2012-03-28)
!!OLD       !!
!!OLD       !! aMaxX  =   vars(iTemp)%xAmplitude*GV%angleUnit * vars(iTemp)%omegaX**2
!!OLD       !! aMaxY  =   vars(iTemp)%yAmplitude*GV%angleUnit * vars(iTemp)%omegay**2
!!OLD       !
!!OLD       !! vLimit = 300.0
!!OLD       !! aLimit = 108.0*arcsec
!!OLD       !
!!OLD       Write (6,*) " vMaxX:   ", vMaxX
!!OLD       Write (6,*) " vMaxY:   ", vMaxY
!!OLD       Write (6,*) " vLimit:  ", vLimit
!!OLD       !
!!OLD       vMaxXel = Acos(vMaxX/(2*vLimit))
!!OLD       vMaxYel = Acos(vMaxY/(2*vLimit))
!!OLD       !
!!OLD       Write (6,*) " max el vMaxXel: ", vMaxXel/deg
!!OLD       Write (6,*) " max el vMaxYel: ", vMaxYel/deg
!!OLD       !
!!OLD       !! aMaxXel = Acos(aMaxX/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
!!OLD       !! aMaxYel = Acos(aMaxY/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
!!OLD       !
!!OLD       MaxEl   = min(vMaxXel,vMaxYel)
!!OLD       !
!!OLD       Write (6,*) " max el  MaxEl:  ", MaxEl/deg
!!OLD       !
!!OLD       ! 2010-04-08 HU: force safe limit on maximum elevation
!!OLD       !                for GISMO run, based on TT-NCS
!!OLD       MaxEl = MaxEl-2.0*deg
!!OLD       !
!!OLD       Write (6,*) " max el  MaxEl:  ", MaxEl/deg
!!OLD       !
!!OLD       MaxEl   = min(MaxEl,85.0*deg)
!!OLD       !
!!OLD       Write (6,*) " max el  MaxEl:  ", MaxEl/deg
!!OLD       !
!!OLD       !!             If (MaxEl.Ge.60.0*deg) Then
!!OLD       !
!!OLD       condElevation%isSet     = .True.
!!OLD       condElevation%maximum   = MaxEl
!!OLD       !
!!OLD       Write (6,*) " condElevation%isSet:   ", condElevation%isSet
!!OLD       Write (6,*) " condElevation%maximum: ", condElevation%maximum/deg
!!OLD       !
       !
       sysName = offsPako%tru
       !
       !! NOTE:  code from here --> copied on 2012-05-30 from OTFMAP (2012-03-28)
       !! TBD:   extract this code, also use in OTFMAP, SUBSCAN OTF LINEAR
       !!        --> define / use local variable sysName in OTFMAP, SUBSCAN OTF LINEAR
       !! TBD:   long-term: ramp-up!
       !
       !                        !!  speed limit az. axis, el. axis
       vLimit   = 200.0         !!  based on tests 2012-03-15 to -20
       !                                 
       !
       If      (     GV%privilege.Eq.setPrivilege%ncsTeam                        &
            &  .Or.  GV%userLevel.Eq.setUserLevel%experienced                    &
            &  ) Then                                                            !
          If (GV%limitCheck.Eq.setLimitCheck%loose)   vLimit = 300.0 
          If (GV%limitCheck.Eq.setLimitCheck%relaxed) vLimit = 220.0
       End If
       !
       vLimitAz = vLimit        !!  speed limit az axis
       vLimitEl = vLimit        !!  speed limit el axis
       !
       !! aLimit = 108.0*arcsec
       !
       If (GV%doDebugMessages) Then
          Write (6,*) " vMaxX:     ", vMaxX
          Write (6,*) " vMaxY:     ", vMaxY
          Write (6,*) " vLimit:    ", vLimit
          Write (6,*) " vLimitAz:  ", vLimitAz
          Write (6,*) " vLimitEl:  ", vLimitEl
       End If
       !
       !! NOTE: use local variable sysName instead of vars(iTemp)%systemName:
       If (         sysName.Eq.offsPako%tru                                      &  
            &  .Or. sysName.Eq.offsPako%hor) Then                                !
          !
          If (vMaxX.Le.vLimitAz) Then
             vMaxXel = Acos(vMaxX/(vLimitAz))
          Else
             errorInconsistent = .true.
             Write (messageText,*) "Azimuth speed ", vMaxX,                      &
                  &                " too large for safe tracking"                !
             Call PakoMessage(priorityE,severityE,command,messageText)
          End If
          !
          If (vMaxY.Le.vLimitEl) Then
             vMaxYel = 90.0*deg
          Else
             errorInconsistent = .true.
             Write (messageText,*) "Elevation speed ", vMaxY,                    &
                  &                " too large for safe tracking"                !
             Call PakoMessage(priorityE,severityE,command,messageText)
          End If
          !
       Else
          !
          If (vMaxX.Le.vLimit .And. vMaxY.Le.vLimit) Then
             vMaxXel = Acos(vMaxX/(vLimit))
             vMaxYel = Acos(vMaxY/(vLimit))
          Else
             errorInconsistent = .true.
             Write (messageText,*) "Speed ", max(vMaxX,vMaxY),                   &
                  &                " too large for safe tracking"                !
             Call PakoMessage(priorityE,severityE,command,messageText)
          End If
          !
       End If
       !
       If (GV%doDebugMessages) Then
          Write (6,*) " max el vMaxXel: ", vMaxXel/deg
          Write (6,*) " max el vMaxYel: ", vMaxYel/deg
       End If
       !
       !! <<-- 2012-03-25
       !
       !! aMaxXel = Acos(aMaxX/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
       !! aMaxYel = Acos(aMaxY/(2*aLimit))  ! <-- corrected aLimit on 2010-03-18 
       !
       MaxEl   = min(vMaxXel,vMaxYel)
       !
       ! 2010-04-08 HU: force safe limit on maximum elevation
       !                for GISMO run, based on TT-NCS
       MaxEl = MaxEl-2.0*deg
       !
       !D        Write (6,*) " max el  MaxEl:  ", MaxEl/deg
       !
       MaxEl   = max(0.0*deg,MaxEl) 
       MaxEl   = min(MaxEl,85.0*deg)
       !
       !D        Write (6,*) " max el vMaxXel: ", vMaxXel/deg
       !D        Write (6,*) " max el vMaxYel: ", vMaxYel/deg
       !D        Write (6,*) " max el aMaxXel: ", aMaxXel/deg
       !D        Write (6,*) " max el aMaxYel: ", aMaxYel/deg
       !D        !
       !D        Write (6,*) " max el  MaxEl:  ", MaxEl/deg
       !
       !!             If (MaxEl.Ge.60.0*deg) Then
       !
       condElevation%isSet     = .True.
       condElevation%maximum   = MaxEl
       !
       If (GV%doDebugMessages) Then
          Write (6,*) " condElevation%isSet:   ", condElevation%isSet
          Write (6,*) " condElevation%maximum: ", condElevation%maximum/deg
       End If
       !
       !! NOTE =: <-- copied on 2012-05-30 from OTFMAP (2012-03-28)
       !
       Write (messageText, fmt =                                           &
            &  '("WARNING--CONDITION: Elevation must be less than ",       &
            &     F10.2,                                                   &
            &    " [deg] ")' )                                             &
            & condElevation%maximum/deg                                    !
       Call pakoMESSAGE(priorityW,severityW,"POINTING",messageText)
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue)  = vars(iTemp)
    End If
    !
    !**   set "selected" observing mode & plot  ***
    If (.Not.error) Then
       GV%observingMode     = OM%Pointing
       GV%observingModePako = OMpako%Pointing
       GV%omSystemName      = offsPako%tru
       GV%condElevation     = condElevation
       !D        Write (6,*) "GV%condElevation: ", GV%condElevation                      !
       GV%notReadySWafterOM = .False.
       GV%notReadySecondaryRafterOM = .False.
       !D       Write (6,*) '      GV%notReadySecondaryRafterOM: ',                       &
       !D            &             GV%notReadySecondaryRafterOM
       GV%notReadyPointing  = .False.
       pointingCommand      = line(1:lenc(line))
       Call analyzePointing (errorA)
!!$       Call listSegmentList (errorA)
       Call plotPointing (errorP)
    End If
    !
    !**   display values  ***
    If (.Not.ERROR) Then
       Call displayPointing
       Call listDIYlist (error)
    End If
    !
    Return
  End Subroutine pointing
!!!
!!!
  Subroutine analyzePointing (errorA)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer                :: nScan 
    Character (len=lenCh)  :: aUnit, sUnit
    Integer                :: iSS           = 0     ! counts subcans/segment
    !
    Logical :: isBolometer   = .False.
    !
    Integer :: mm = 0
    !
    Real    ::  tAzFactor
    !
    errorA    = .False.
    iSS       = 0
    nScan     = 1111
    aUnit     = GV%angleUnitC                        ! 
    sUnit     = GV%speedUnitC                        ! 
    !
    !D     Write (6,*) "   --> analyzePointing   "
    !D Write (6,*) "       aeCode:      ", vars(iValue)%aeCode
    !D Write (6,*) "       aeCodeBites: ", vars(iValue)%aeCodeBites
    !D Write (6,*) "       nAeCode:     ", vars(iValue)%nAeCode
    !
    Call pako_message(seve%t,"paKo",                                             &
         &  " module Pointing, v 1.2.3  2014-09-23  --> SR: analyzePointing ")   ! trace execution
    !
    !D     Write (6,*) "       vars(iValue)%doDoubleBeam: ", vars(iValue)%doDoubleBeam
    !
    isBolometer = .False.
    !! TBD 2.0:    Call queryReceiver(rec%bolo, isBolometer)
    !
    !D    Write (6,*) " rec%bolo? ", isBolometer
    !
    swWobblerAmplitude       =  0.0
    swWobblerShiftAzimuth    =  0.0
    swWobblerShiftElevation  =  0.0
    If (GV%switchingMode .Eq. swMode%wobb) Then   
       !D       Write (6,*) '   GV%secondaryRotation: ', GV%secondaryRotation
       swWobblerAmplitude = 0.5*Abs(GV%wOffset(2)-GV%wOffset(1))
       swWobblerShiftAzimuth   =                                                 &
            &        -swWobblerAmplitude*Cos(GV%secondaryRotation/180.0*Pi)      !
       swWobblerShiftElevation =                                                 &
            &         swWobblerAmplitude*Sin(GV%secondaryRotation/180.0*Pi)      !
       If (Abs(swWobblerShiftAzimuth)  .Lt.0.1e-11) swWobblerShiftAzimuth  =0.0
       If (Abs(swWobblerShiftElevation).Lt.0.1e-11) swWobblerShiftElevation=0.0
          Write (messageText,*)                                                  &
               &      "SWWOBBLER implies: pointing cross will be shifted by: "   !
          Call pakoMessage(priorityI,severityI,'POINTING',messageText)
          Write (messageText,*)                                                  &
               &      swWobblerShiftAzimuth, " [arc sec] in azimuth"             !
          Call pakoMessage(priorityI,severityI,'POINTING',messageText)
          Write (messageText,*)                                                  &
               &      swWobblerShiftElevation, " [arc sec] in elevation"         !
          Call pakoMessage(priorityI,severityI,'POINTING',messageText)
          !
          If (           vars(iValue)%doDoubleBeam                               &
               &  .And.  .Not.isBolometer) Then                                  !
             tAzFactor  =                                                        &
                  & (                                                            &
                  &  1.0 +                                                       &
                  &  Abs(GV%wOffset(2)-GV%wOffset(1))/vars(iValue)%length        &
                  & )                                                            !
             Write (messageText,*)                                               &
                  &    "for azimuth subscans tSegment will be multiplied by ",   &
                  &    tAzFactor                                                 !
             Call pakoMessage(priorityI,severityI,'POINTING',messageText)
          Else
             tAzFactor = 1.0
          End If
          !
    End If
    !
    segList(:) = segDefault
    !
    Call countSubscans(value=0)
    Call countSegments(value=0)
    !

    !
    If (vars(ivalue)%doTune) Then
       !
       If (GV%doDebugMessages) Then
          Write (6,*) "  --> Tune Subscan "
          Write (6,*) "      vars(iValue)%doTune : ", vars(iValue)%doTune
          Write (6,*) "      vars(iValue)%tTune :  ", vars(iValue)%tTune
       End If
       !
       Call countSubscans(ii)
       Call countSegments(jj)
       !            
       iSS = iSS+1
       !
       segList(iSS)            =  segDefault                    ! TUNE
       !
       segList(iSS)%newSubscan =  .True.
       segList(iSS)%scanNumber =  nScan
       segList(iSS)%ssNumber   =  iSS
       segList(iSS)%segNumber  =  1
       segList(iSS)%ssType     =  ss%tune
       segList(iSS)%segType    =  seg%track
       segList(iSS)%angleUnit  =  aUnit
       segList(iSS)%speedUnit  =  sUnit
       !
       segList(iSS)%flagOn     =  .True.
       segList(iSS)%flagRef    =  .False.
       !
       segList(iSS)%flagDropin =  .False.
       !
       segList(iSS)%flagTune   =  vars(iIn)%doTune
       !
!!$       If (vars(ivalue)%doReference) Then
!!$          segList(iSS)%pStart     =  vars(iValue)%offsetR
!!$       Else
!!$          segList(iSS)%pStart     =  vars(iValue)%pStart
!!$       End If
       !
       segList(iSS)%pStart     =  vars(iValue)%offsetTune
       !
       segList(iSS)%pEnd       =  segList(iSS)%pStart
       segList(iSS)%speedStart =  0.0
       segList(iSS)%speedEnd   =  0.0
       segList(iSS)%systemName =  offs%tru
       segList(iSS)%altOption  =  'tSegment'
       segList(iSS)%tSegment   =  vars(iValue)%tTune
       segList(iSS)%tRecord    =  0.1
       !
!!$          iQuery    = iPro
!!$          nameQuery = GPnone
!!$          Call  queryOffsets(iQuery,nameQuery,isSet,pointResult)
       !D          Write (6,*) "  SUBSCAN:  isSet = ", isSet
       !D          Write (6,*) "  SUBSCAN:  pointResult = ", pointResult
       !
       ! TBD: clever /proper handling of Doppler 
       segList(iSS)%upDateDoppler = .False.
       !
       If (GV%doDebugMessages) Then
          Write (messageText,*)  "  subscan #:   ", iSS, " ",                    &
               &             segList(iSS)%ssType, " ", segList(iSS)%pStart       !
!!$            &           " segList(iSS)%flagTune: ", segList(iSS)%flagTune        !
          Call pakoMessage(priorityI,severityI,"OTFMAP "//" Analyze",messageText)
          !
          Write (6,*) "  <-- Tune Subscan "
       End If
       !
    End If   !!   (vars(ivalue)%doTune)
    !





    If (vars(iValue)%doCalibrate .And. .Not.isBolometer) Then
       !
       !                                                        Cal Sky
       Call countSubscans(ii)
       Call countSegments(jj)
       !D       Write (6,*) " Cal Sky    ii, jj ", ii, jj
       segList(ii)%newSubscan = .True.
       segList(ii)%ssNumber   =  ii
       segList(ii)%segNumber  =  1
       segList(ii)%ssType     =  ss%cs
       segList(ii)%segType    =  seg%track
       segList(ii)%angleUnit  =  aUnit
       segList(ii)%speedUnit  =  sUnit
       segList(ii)%flagOn     =  .True.
       segList(ii)%flagRef    =  .False.
       segList(ii)%pStart%x   = -vars(iValue)%length/2.0
       segList(ii)%pStart%y   =  0.0
       segList(ii)%pEnd       =  segList(ii)%pStart
       segList(ii)%systemName =  offsetSystemChoices(iHORIZONTRUE)
       segList(ii)%tSegment   =  3.0
       segList(ii)%tRecord    =  1.0
       !
       If (GV%switchingMode .Eq. swMode%wobb) Then   
          segList(ii)%pStart%x  = segList(ii)%pStart%x +swWobblerShiftAzimuth  
          segList(ii)%pEnd%x    = segList(ii)%pEnd%x   +swWobblerShiftAzimuth  
          segList(ii)%pStart%y  = segList(ii)%pStart%y +swWobblerShiftElevation 
          segList(ii)%pEnd%y    = segList(ii)%pEnd%y   +swWobblerShiftElevation
       End If
       !
       !                                                        Cal Cold
       Call countSubscans(ii)
       Call countSegments(jj)
       !D       Write (6,*) " Cal Cold   ii, jj ", ii, jj
       segList(ii)            =  segList(1)
       segList(ii)%ssNumber   =  ii
       segList(ii)%segNumber  =  1
       segList(ii)%ssType     =  ss%cc
       segList(ii)%segType    =  seg%track
       !                                                        Cal Ambient
       Call countSubscans(ii)
       Call countSegments(jj)
       !D       Write (6,*) " Cal Amb    ii, jj ", ii, jj
       segList(ii)            =  segList(1)
       segList(ii)%ssNumber   =  ii
       segList(ii)%segNumber  =  1
       segList(ii)%ssType     =  ss%ca
       segList(ii)%segType    =  seg%track
       !
    End If
    !
    If (vars(iValue)%aeCode .Eq.GPnone) Then                        ! normal sequence
       !
       If (vars(iValue)%nOtf .Ge. 1) Then                           ! first OTF incr. in Az
          Call countSubscans(ii)
          Call countSegments(jj)
          !D       Write (6,*) " 1st OTF    ii, jj ", ii, jj
          segList(ii)%newSubscan  =  .True.
          segList(ii)%scanNumber  =  nScan
          segList(ii)%ssNumber    =  ii
          segList(ii)%segNumber   =  1
          segList(ii)%ssType      =  ss%otf
          segList(ii)%segType     =  seg%linear
          segList(ii)%angleUnit   =  aUnit
          segList(ii)%speedUnit   =  sUnit
          segList(ii)%flagOn      =  .True.
          segList(ii)%flagRef     =  .False.
          segList(ii)%pStart%x    = -vars(iValue)%length/2.0
          segList(ii)%pStart%y    =  0.0
          segList(ii)%pEnd%x      =  vars(iValue)%length/2.0
          segList(ii)%pEnd%y      =  0.0
          segList(ii)%lengthOtf   =  vars(iValue)%length
          segList(ii)%systemName  =  offsetSystemChoices(iHORIZONTRUE)
          segList(ii)%altOption   =  "TOTF"
          segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%tSegment    =  vars(iValue)%tOtf
          segList(ii)%tRecord     =  vars(iValue)%tRecord
          !
          If (GV%switchingMode .Eq. swMode%wobb) Then   
             segList(ii)%pStart%x  = segList(ii)%pStart%x+swWobblerShiftAzimuth  
             segList(ii)%pEnd%x    = segList(ii)%pEnd%x  +swWobblerShiftAzimuth  
             segList(ii)%pStart%y  = segList(ii)%pStart%y+swWobblerShiftElevation 
             segList(ii)%pEnd%y    = segList(ii)%pEnd%y  +swWobblerShiftElevation
             If (vars(iValue)%doDoubleBeam                                       &
                  & .And. Abs(GV%secondaryRotation).Lt.0.05) Then                !
                segList(ii)%pEnd%x = segList(ii)%pEnd%x                          &
                     &             + Abs(GV%wOffset(2)-GV%wOffset(1))            !
!!$             segList(ii)%tSegment = vars(iValue)%tOtf * tAzFactor
                segList(ii)%tSegment   = Nint(vars(iValue)%tOtf * tAzFactor)
                segList(ii)%speedStart = ( Abs(  segList(ii)%pEnd%x              &
                     &                          -segList(ii)%pStart%x)           &
                     &                        /  segList(ii)%tSegment  )         !
                segList(ii)%speedEnd   = segList(ii)%speedStart 
             End If
          End If
          !
       End If
       !
       If (vars(iValue)%nOtf .Eq. 2) Then                               ! 2nd OTF if only 2 incr. in El.
          Call countSubscans(ii)
          Call countSegments(jj)
          !D       Write (6,*) " 2nd OTF El ii, jj ", ii, jj
          segList(ii)             =  segList(ii-1)                     
          segList(ii)%ssNumber    =  ii
          segList(ii)%segNumber   =  1
          !
          segList(ii)%pStart%x    =  0.0
          segList(ii)%pStart%y    =  -vars(iValue)%length/2.0
          segList(ii)%pEnd%x      =  0.0
          segList(ii)%pEnd%y      =   vars(iValue)%length/2.0
          !
          !!
          segList(ii)%lengthOtf   =  vars(iValue)%length
          segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%tSegment    =  vars(iValue)%tOtf
          !!
          !
          If (GV%switchingMode .Eq. swMode%wobb) Then   
             segList(ii)%pStart%x  = segList(ii)%pStart%x +swWobblerShiftAzimuth  
             segList(ii)%pEnd%x    = segList(ii)%pEnd%x   +swWobblerShiftAzimuth  
             segList(ii)%pStart%y  = segList(ii)%pStart%y +swWobblerShiftElevation 
             segList(ii)%pEnd%y    = segList(ii)%pEnd%y   +swWobblerShiftElevation
          End If
          !
       Else If (vars(iValue)%nOtf .Ge. 2) Then
          !
          Do kk = 2, vars(iValue)%nOtf, 1
             !
             If (Mod(kk,4).Eq.1) Then                                   ! incr. in Az.
                Call countSubscans(ii)
                Call countSegments(jj)
                !D             Write (6,*) " nth OTF    ii, jj, kk ", ii, jj, kk
                segList(ii)             =  segList(ii-1)                     
                segList(ii)%ssNumber    =  ii
                segList(ii)%segNumber   =  1
                !
                segList(ii)%pStart%x    =  -vars(iValue)%length/2.0
                segList(ii)%pStart%y    =  0.0                     
                segList(ii)%pEnd%x      =   vars(iValue)%length/2.0
                segList(ii)%pEnd%y      =  0.0                     
                !
                !!
                segList(ii)%lengthOtf   =  vars(iValue)%length
                segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%tSegment    =  vars(iValue)%tOtf
                !!
                !
             End If
             !
             If (Mod(kk,4).Eq.2) Then                                   ! decr. in Az.
                Call countSubscans(ii)
                Call countSegments(jj)
                !D             Write (6,*) " nth OTF    ii, jj, kk ", ii, jj, kk
                segList(ii)             =  segList(ii-1)                 
                segList(ii)%ssNumber    =  ii
                segList(ii)%segNumber   =  1
                !
                segList(ii)%pStart%x    =   vars(iValue)%length/2.0
                segList(ii)%pStart%y    =  0.0                     
                segList(ii)%pEnd%x      =  -vars(iValue)%length/2.0
                segList(ii)%pEnd%y      =  0.0                     
                !
                !!
                segList(ii)%lengthOtf   =  vars(iValue)%length
                segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%tSegment    =  vars(iValue)%tOtf
                !!
                !
             End If
             !
             If (Mod(kk,4).Eq.3) Then                                   ! incr. in El.
                Call countSubscans(ii)
                Call countSegments(jj)
                segList(ii)             =  segList(ii-1)
                !D             Write (6,*) " nth OTF    ii, jj, kk ", ii, jj, kk
                segList(ii)%ssNumber    =  ii
                segList(ii)%segNumber   =  1
                !
                segList(ii)%pStart%x    =  0.0                     
                segList(ii)%pStart%y    =  -vars(iValue)%length/2.0
                segList(ii)%pEnd%x      =  0.0                     
                segList(ii)%pEnd%y      =   vars(iValue)%length/2.0
                !
                !!
                segList(ii)%lengthOtf   =  vars(iValue)%length
                segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%tSegment    =  vars(iValue)%tOtf
                !!
                !
             End If
             !
             If (Mod(kk,4).Eq.0) Then                                   ! decr. in El.
                Call countSubscans(ii)
                Call countSegments(jj)
                !D             Write (6,*) " nth OTF    ii, jj, kk ", ii, jj, kk
                segList(ii)             =  segList(ii-1)
                segList(ii)%ssNumber    =  ii
                segList(ii)%segNumber   =  1
                !
                segList(ii)%pStart%x    =  0.0                     
                segList(ii)%pStart%y    =   vars(iValue)%length/2.0
                segList(ii)%pEnd%x      =  0.0                     
                segList(ii)%pEnd%y      =  -vars(iValue)%length/2.0
                !
                !!
                segList(ii)%lengthOtf   =  vars(iValue)%length
                segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
                segList(ii)%tSegment    =  vars(iValue)%tOtf
                !!
                !
             End If
             !
             If (GV%switchingMode .Eq. swMode%wobb) Then   
                segList(ii)%pStart%x  = segList(ii)%pStart%x+swWobblerShiftAzimuth  
                segList(ii)%pEnd%x    = segList(ii)%pEnd%x  +swWobblerShiftAzimuth  
                segList(ii)%pStart%y  = segList(ii)%pStart%y+swWobblerShiftElevation 
                segList(ii)%pEnd%y    = segList(ii)%pEnd%y  +swWobblerShiftElevation
                If (vars(iValue)%doDoubleBeam                                       &
                     & .And. Abs(GV%secondaryRotation).Lt.0.05                      &
                     & .And. Mod(kk,4).Eq.1) Then                                   !
                   segList(ii)%pEnd%x = segList(ii)%pEnd%x                          &
                        &             + Abs(GV%wOffset(2)-GV%wOffset(1))            !
!!$                segList(ii)%tSegment = vars(iValue)%tOtf * tAzFactor
                   segList(ii)%tSegment = Nint(vars(iValue)%tOtf * tAzFactor)
                   segList(ii)%speedStart = ( Abs(  segList(ii)%pEnd%x           &
                        &                          -segList(ii)%pStart%x)        &
                        &                        /  segList(ii)%tSegment  )      !
                   segList(ii)%speedEnd   = segList(ii)%speedStart 
                End If
                If (vars(iValue)%doDoubleBeam                                       &
                     & .And. Abs(GV%secondaryRotation).Lt.0.05                      &
                     & .And. Mod(kk,4).Eq.2) Then                                   !
                   segList(ii)%pStart%x = segList(ii)%pStart%x                      &
                        &               + Abs(GV%wOffset(2)-GV%wOffset(1))          !
!!$                segList(ii)%tSegment = vars(iValue)%tOtf * tAzFactor
                   segList(ii)%tSegment = Nint(vars(iValue)%tOtf * tAzFactor)
                   segList(ii)%speedStart = ( Abs(  segList(ii)%pEnd%x           &
                        &                          -segList(ii)%pStart%x)        &
                        &                        /  segList(ii)%tSegment  )      !
                   segList(ii)%speedEnd   = segList(ii)%speedStart 
                End If
             End If
             !
          End Do
          !
       End If
       !
    Else                                                                ! use /aeLoop aeCode
       !
       mm    = 0
       !
       Do kk = 1, vars(iValue)%nOtf, 1
          mm = mm+1
          If (mm.Gt.vars(iValue)%nAeCode) mm = mm - vars(iValue)%nAeCode
          !D       Write (6,*)  "      subscan kk:        ", kk
          !D       Write (6,*)  "      aeLoop  mm:        ", mm
          !D       Write (6,*)  "      aeCodeBites(mm):   ", vars(iValue)%aeCodeBites(2*mm-1:2*mm)
          !
          Call countSubscans(ii)
          Call countSegments(jj)
          !D       Write (6,*) " nth OTF    ii, jj, kk ", ii, jj, kk
          !
          segList(ii)%newSubscan  =  .True.
          segList(ii)%scanNumber  =  nScan
          segList(ii)%ssNumber    =  ii
          segList(ii)%segNumber   =  1
          segList(ii)%ssType      =  ss%otf
          segList(ii)%segType     =  seg%linear
          segList(ii)%angleUnit   =  aUnit
          segList(ii)%speedUnit   =  sUnit
          segList(ii)%flagOn      =  .True.
          segList(ii)%flagRef     =  .False.
          segList(ii)%lengthOtf   =  vars(iValue)%length
          segList(ii)%systemName  =  offsetSystemChoices(iHORIZONTRUE)
          segList(ii)%altOption   =  "TOTF"
          segList(ii)%speedStart  =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%speedEnd    =  vars(iValue)%length/vars(iValue)%tOtf
          segList(ii)%tSegment    =  vars(iValue)%tOtf
          segList(ii)%tRecord     =  vars(iValue)%tRecord
          !
          Call pakoUmatchKey (                                              &
               &              keys=azelChoices,                             &
               &              key=vars(iValue)%aeCodeBites(2*mm-1:2*mm),    &
               &              command='POINTING',                           &
               &              howto='Start Upper',                          &
               &              iMatch=iMatch,                                &
               &              nMatch=nMatch,                                &
               &              error=errorM,                                 &
               &              errorCode=errorCode                           &
               &             )

          !
          !D       Write (6,*) ii
          !D       Write (6,*) azElChoices(iMatch)
          !D       Write (6,*) azel%pa
          !
          If (azElChoices(iMatch).Eq.azel%pa) Then                               ! incr. in Az.
             !
             segList(ii)%pStart%x    =  -vars(iValue)%length/2.0
             segList(ii)%pStart%y    =  0.0                     
             segList(ii)%pEnd%x      =   vars(iValue)%length/2.0
             segList(ii)%pEnd%y      =  0.0                     
             !
             !D          Write (6,*) segList(ii)%pStart%x, segList(ii)%pStart%y
             !D          Write (6,*) segList(ii)%pEnd%x ,  segList(ii)%pEnd%y
             !
          End If
          !
          If (azElChoices(iMatch).Eq.azel%ma) Then                               ! decr. in Az.
             !
             segList(ii)%pStart%x    =   vars(iValue)%length/2.0
             segList(ii)%pStart%y    =  0.0                     
             segList(ii)%pEnd%x      =  -vars(iValue)%length/2.0
             segList(ii)%pEnd%y      =  0.0                     
             !
          End If
          !
          If (azElChoices(iMatch).Eq.azel%pe) Then                               ! incr. in El.
             !
             segList(ii)%pStart%x    =  0.0                     
             segList(ii)%pStart%y    =  -vars(iValue)%length/2.0
             segList(ii)%pEnd%x      =  0.0                     
             segList(ii)%pEnd%y      =   vars(iValue)%length/2.0
             !
          End If
          !
          If (azElChoices(iMatch).Eq.azel%me) Then                               ! decr. in El.
             !
             segList(ii)%pStart%x    =  0.0                     
             segList(ii)%pStart%y    =   vars(iValue)%length/2.0
             segList(ii)%pEnd%x      =  0.0                     
             segList(ii)%pEnd%y      =  -vars(iValue)%length/2.0
             !
          End If
          !
          If (GV%switchingMode .Eq. swMode%wobb) Then   
             segList(ii)%pStart%x  = segList(ii)%pStart%x+swWobblerShiftAzimuth  
             segList(ii)%pEnd%x    = segList(ii)%pEnd%x  +swWobblerShiftAzimuth  
             segList(ii)%pStart%y  = segList(ii)%pStart%y+swWobblerShiftElevation 
             segList(ii)%pEnd%y    = segList(ii)%pEnd%y  +swWobblerShiftElevation
             If (vars(iValue)%doDoubleBeam                                       &
                  & .And. Abs(GV%secondaryRotation).Lt.0.05                      &
                  & .And. azElChoices(iMatch).Eq.azel%pa) Then                   !
                segList(ii)%pEnd%x = segList(ii)%pEnd%x                          &
                     &             + Abs(GV%wOffset(2)-GV%wOffset(1))            !
!!$             segList(ii)%tSegment = vars(iValue)%tOtf * tAzFactor
                segList(ii)%tSegment = Nint(vars(iValue)%tOtf * tAzFactor)
                segList(ii)%speedStart = ( Abs(  segList(ii)%pEnd%x              &
                     &                          -segList(ii)%pStart%x)           &
                     &                        /  segList(ii)%tSegment  )         !
                segList(ii)%speedEnd   = segList(ii)%speedStart 
             End If
             If (vars(iValue)%doDoubleBeam                                       &
                  & .And. Abs(GV%secondaryRotation).Lt.0.05                      &
                  & .And. azElChoices(iMatch).Eq.azel%ma) Then                   !
                segList(ii)%pStart%x = segList(ii)%pStart%x                      &
                     &               + Abs(GV%wOffset(2)-GV%wOffset(1))          !
!!$             segList(ii)%tSegment = vars(iValue)%tOtf * tAzFactor
                segList(ii)%tSegment = Nint(vars(iValue)%tOtf * tAzFactor)
                segList(ii)%speedStart = ( Abs(  segList(ii)%pEnd%x              &
                     &                          -segList(ii)%pStart%x)           &
                     &                        /  segList(ii)%tSegment  )         !
                segList(ii)%speedEnd   = segList(ii)%speedStart 
             End If
          End If
          !
       End Do
       !
    End If
    !
    Call getCountSegments(nSegments)
    !
    Return 
    !
  End Subroutine analyzePointing
!!!
!!!
  Subroutine displayPointing
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    !D     Write (6,*) "      --> SR displayPointing "
    !
    Include 'inc/display/commandDisplayPointing.inc'
    !
  End Subroutine displayPointing
!!!
!!!
  Subroutine plotPointing (errorP)
    !
    !**   Variables  for Plots   ***
    Include 'inc/variables/headerForPlotMethods.inc'
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer :: nSegments
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Pointing, v 1.2.3  2014-12-15 ---> SR: plotPointing ")      ! trace execution
    !
    errorP = .False.
    !
    Call configurePlots
    !
    Call plotPointingStart(errorP)
    !
    Call getCountSegments(nSegments)
    !D     Write (6,*) '      number of segments: ', nSegments
    !
    Do ii = 1, nSegments, 1
       !
       !D        Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType,                  &
       !D             &          segList(ii)%flagOn, segList(ii)%flagRef,                  &
       !D             &          segList(ii)%flagTune                                      !
       !
       If (segList(ii)%ssType .Eq.ss%cs .And.                                    &
            &       segList(ii)%segType.Eq.seg%track) Then                       !
          Call plotCalSky(segList(ii)%pStart%x,segList(ii)%pStart%y,             &
               &                    errorP,                                      &
               &                    number=segList(ii)%ssNumber,                 &
               &                    Type=segList(ii)%ssType)                     !
          !
       Else If (segList(ii)%ssType .Eq.ss%tune                                   &
            &   .And. segList(ii)%segType.Eq.seg%track                           &
            &   .And. segList(ii)%flagTune) Then                                 !  !!  TRACK tune
          !!D  Write (6,*) "      ii: ", ii, " -> plot Track /Tune "
          Call plotTrackP( segList(ii)%pStart,                                   &
               &           errorP,                                               &
               &           number=segList(ii)%ssNumber,                          &
               &           Type=ss%tune)                                         !
          !
       End If
       !
    End Do
    !
    If (GV%switchingMode .Ne. swMode%wobb) Then   
       jj = 0
       Do ii = 1, nSegments, 1
          !
          If (segList(ii)%ssType .Eq.ss%otf .And.                                &
               segList(ii)%segType.Eq.seg%linear) Then                           !
             jj = jj+1
             If (jj.Le.4) Then
                Call plotOtfLinear(segList(ii)%pStart%x,segList(ii)%pStart%y,    &
                     &                    segList(ii)%pEnd%x,segList(ii)%pEnd%y, &
                     &                    errorP,                                &
                     &                    number=jj,                             &
                     &                    Type='pointing')                       !
             Else
                Call plotOtfLinear(segList(ii)%pStart%x,segList(ii)%pStart%y,    &
                     &                    segList(ii)%pEnd%x,segList(ii)%pEnd%y, &
                     &                    errorP,                                &
                     &                    Type='pointing')                       !
             End If
          End If
       End Do
    End If
    !
    If (GV%switchingMode .Eq. swMode%wobb) Then   
       !
       jj = 0
       Do ii = 1, nSegments, 1
          If (segList(ii)%ssType .Eq.ss%otf .And.                                &
               segList(ii)%segType.Eq.seg%linear) Then                           !
             jj = jj+1
             If (jj.Le.4) Then
                Call plotOtfLinear(segList(ii)%pStart%x,segList(ii)%pStart%y,    &
                     &                    segList(ii)%pEnd%x,segList(ii)%pEnd%y, &
                     &                    errorP,                                &
                     &                    number=jj,                             &
                     &                    Type='extra')                          !
                Call plotOtfLinear(segList(ii)%pStart%x-swWobblerShiftAzimuth,   &
                     &             segList(ii)%pStart%y-swWobblerShiftElevation, &
                     &             segList(ii)%pEnd%x  -swWobblerShiftAzimuth,   &
                     &             segList(ii)%pEnd%y  -swWobblerShiftElevation, &
                     &                    errorP,                                &
                     &                    number=jj,                             &
                     &                    Type='pointing')                       !
             Else
                Call plotOtfLinear(segList(ii)%pStart%x,segList(ii)%pStart%y,    &
                     &                    segList(ii)%pEnd%x,segList(ii)%pEnd%y, &
                     &                    errorP,                                &
                     &                    Type='extra')                          !
                Call plotOtfLinear(segList(ii)%pStart%x-swWobblerShiftAzimuth,   &
                     &             segList(ii)%pStart%y-swWobblerShiftElevation, &
                     &             segList(ii)%pEnd%x  -swWobblerShiftAzimuth,   &
                     &             segList(ii)%pEnd%y  -swWobblerShiftElevation, &
                     &                    errorP,                                &
                     &                    Type='pointing')                       !
             End If
          End If
       End Do
    End If
    !
    Call gr_exec(      'set character 0.6' )
    Call gr_exec(      'pen 0'             )
    !
    errorP =  errorP .Or. GR_ERROR()
    !
    If (errorP)                                                                 &
         &        Call PakoMessage(priorityE,severityE,                         &
         &        'Pointing',' could not plot ')                                !
    !
    Return
  End Subroutine plotPointing
!!!
!!!
  Subroutine savePointing(programName,LINE,commandToSave, iUnit, ERROR)                           
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    contC = contNN
    !
    error = .False.
    !
    Include 'inc/commands/saveCommand.inc'
    !
    contC = contCC
    !
    Include 'inc/parameters/saveLength.inc'
    !
    Include 'inc/options/saveDoubleBeam.inc'
    !TBD:      Include 'inc/options/saveCalibrate.inc'
    !TBD:      Include 'inc/options/saveFeBe.inc'
    !TBD:      Include 'inc/options/saveMore.inc'
    Include 'inc/options/saveNotf.inc'
    !TBD:      Include 'inc/options/saveTrecord.inc'
    !TBD:      Include 'inc/options/saveUpdate.inc'
    !
    !D     Write (6,*) "      vars(iValue)%doTuneSet:   ", vars(iValue)%doTuneSet
    !
    If (.Not.vars(iValue)%doTuneSet .And. vars(iValue)%aeCode.Eq.GPnone) Then
       contC = contCN
    End If
    !
    Include 'inc/options/saveTotf.inc'
    !
    If (vars(iValue)%aeCode.Ne.GPnone) Then
       !
       If (.Not. vars(iValue)%doTuneSet ) Then
          contC = contCN
       End If
       !
       Include'inc/options/saveAeLoop.inc'
       !
    End If
    !
    If (vars(iValue)%doTuneSet) Then
       If ( vars(iValue)%doTune) Then
          Include 'inc/options/saveTuneOffsets.inc'
          doContinue = .False.
          contC = contCN
          Include 'inc/options/saveTuneTime.inc'
       Else 
          doContinue = .False.
          contC = contCN
          Include 'inc/options/saveTuneOffsets.inc'
       End If
    End If
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine savePointing
!!!
!!!
  Subroutine startPointing(programName,LINE,commandToSave,         &
       &        iUnit, ERROR)
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Character (len=lenLine) :: messageText
    Logical                 :: errorL, errorXML
    !
    !D      Write (6,*) "   --> modulePakoPointing: SR startPointing "
    !
    ERROR = .False.
    !
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
!!OLD    Include 'inc/startXML/pointingHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,                  &
         &           iUnit, ERROR)
    !
    !!! Call writeXMLversion !!! incorporated in generalHead above
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",             &
         &                         comment="save from pako",                 &
         &                         error=errorXML)
    Call pakoXMLwriteStartElement("DESCRIPTION",                      &
         &                         doCdata=.True.,                          &
         &                         error=errorXML)
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call savePointing     (programName,LINE,commandToSave, iUnit, errorL)
    !
    Call pakoXMLwriteEndElement("DESCRIPTION",                         &
         &                         doCdata=.True.,                           &
         &                         error=errorXML)
    Call pakoXMLwriteEndElement  ("RESOURCE","pakoScript",             &
         &                         space="after",                            &
         &                         error=errorXML)
    !
    Call writeXMLantenna(programName,LINE,commandToSave,              &
         &         iUnit, ERROR)
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,              &
         &        iUnit, ERROR)
    !
    Call writeXMLbackend(programName,LINE,commandToSave,               &
         &        iUnit, ERROR)
    !
    Include 'inc/startXML/switchingMode.inc'
    !
    Call writeXMLsource(programName,LINE,commandToSave,                &
         &        iUnit, ERROR)
    !
    Include 'inc/startXML/generalScanHead.inc'
    !    
    Include 'inc/startXML/conditions.inc'
    !
    ! **  Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       messageText = "writing observing mode in XML format"
       Call PakoMessage(priorityI,severityI,                                     &
               &           commandToSave,messageText)                            !
       Call writeXMLpointing(programName,LINE,commandToSave,                     &
         &         iUnit, ERROR)                                                 !
    End If
    !
    ! **  subscan list to XML
    Call writeXMLsubscanList(programName,LINE,commandToSave,         &
         &        iUnit, ERROR)
    !   
    Include 'inc/startXML/generalTail.inc'
    !
    Return
  End Subroutine startPointing
!!!
!!!
  Subroutine writeXMLpointing(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: iMatch, nMatch, len1, len2
    Character (len=lenCh)   :: valueC, errorCode
    Character (len=lenLine) :: valueComment
    Logical                 :: errorM, errorXML
    Character (len=lenCh)   :: systemName
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Pointing, v 1.2.3 ---> SR: writeXMLpointing 2014-10-27")    ! trace execution
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/parametersLengthXML.inc'
    !
    !! TBD 2.0:    option aeLoop
    !
    Include 'inc/startXML/optionDoubleBeamXML.inc'
    Include 'inc/startXML/optionNotfXML.inc'
    Include 'inc/startXML/optionTotfXML.inc'
    !
    systemName = offsPako%tru
    If (vars(iValue)%doTune) Then 
       Include 'inc/startXML/optionTuneXML.inc'
       Include 'inc/startXML/optionTtuneXML.inc'
    End If
    !D     Write (6,*) " systemName: --> ", systemName, "<-- "
    !D     Write (6,*) " offs%tru:   --> ", offs%tru,   "<-- "
    !
    Include 'inc/startXML/annotationsXML.inc'
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLpointing
!!!
!!!
End Module modulePakoPointing
!!!
