!
!     Id: pako-load.f90,v 1.2.3 2014-12-01 Hans Ungerechts
!     Id: pako-load.f90,v 1.2.3 2014-02-17 Hans Ungerechts
!     Id: pako-load.f90,v 1.2.3 2014-02-03 Hans Ungerechts
!
!         previous (inconsistent v number)
!         pako-load.f90,v 1.2.4 2013-04-19 Hans Ungerechts
!
Subroutine pako_load
  !
  Use gbl_message
  Use gkernel_interfaces
  Use modulePakoGlobalParameters    !  to get pakoVersion string
  Use modulePakoMessages            !
  !
  !---------------------------------------------------------------------
  ! Define and load languages of the package.
  !---------------------------------------------------------------------
  ! Global
  External :: pako_run
  Logical, External :: pako_error
  !
  ! Local
  ! Define commands of a new language
  !
!!$  from template:
!!$  integer, parameter :: mcom=4
!!$  character(len=12) :: vocab(mcom)
!!$  data vocab /                      &
!!$    ' DUMMY', '/OPT1', '/OPT2',     &  ! A command with 2 options
!!$    ' VOID' /                          ! A standalone command
  !
  Include 'inc/commands/pakoVocab.inc'
  !
  Character*80 messageText, shellCommand
  Integer system
  Integer ier2, ipid, iou
  Character*8   Date
  Character*10  Time
  Character*5   Zone
  Integer       Values(8)
  !
  Logical ERROR
  !
  Character*12 programName
  programName = "pako"                      !! TBD program/language name for messages and scripts
  !
  Call pako_message(seve%t,programName,                                          &
       &    " pako_load.f90 ---> SR: pako_load: ")                               ! trace execution
!!$  Call pako_message(seve%t,programName,                                          &
!!$       &    " SR pako_load, pakoVersion: "//pakoVersion )                        ! trace execution
  !
  !D   Write (6,*) " --> pako_load "
  !
  ! Load the new language:
  Call sic_begin(                                                                &
    'PAKO',                                                                      &  ! Language name
    'GAG_HELP_PAKO',                                                             &  !
    mcom,                                                                        &  ! Number of commands + options in language
    vocab,                                                                       &  ! Array of commands
    pakoVersion,                                                                 &  ! Some version string
    pako_run,                                                                    &  ! The routine which handles incoming commands
    pako_error)                                                                  !  ! The error status routine of the library
  !
!!$  from template:
!!$  call sic_begin(           &
!!$    'PAKO',             &  ! Language name
!!$    'GAG_HELP_PAKO',   &  !
!!$    mcom,                   &  ! Number of commands + options in language
!!$    vocab,                  &  ! Array of commands
!!$    '1.0    27-Oct-2008 ',  &  ! Some version string
!!$    pako_run,          &  ! The routine which handles incoming commands
!!$    pako_error)           ! The error status routine of the library
  !
  !  
  !! Check if pakoLock file exists; if yes: STOP
  if (gag_inquire(".pakoLock",9).eq.0) then
     messageText = "THIS WORKING DIRECTORY APPEARS"                              &
          &             //" TO BE LOCKED BY ANOTHER PAKO:"                       !
     Call pakoMessage(8,4,"pako",messageText)
     shellCommand = "cat .pakoLock"
     ier2 = system(shellCommand)
     messageText = "EXIT THE OTHER PAKO"                                         &
          &               //" OR USE ANOTHER WORKING DIRECTORY"                  !
     Call pakoMessage(8,4,"pako",messageText)
     messageText = 'if you are sure there is no other pako'                      &
          &              //' try: "rm .pakoLock" and again "pako"'               !
     Call pakoMessage(8,4,"pako",messageText)
     Error = .True.
     STOP
  End If
  !
  ! Create lock file
  ipid = gag_getpid()
  Call Date_And_Time(date,time,zone,values)
  iou = 31                                                                       !  !!  TBD: cleanup, use global value
!!$      iou = ioUnit%Lock
  Open (unit=iou,file=".pakoLock")
  Write (iou,'(A,A,I4.4,A,I2.2,A,I2.2,A,I2.2,A,I2.2,A,I2.2,A5,A,I0)')            &
       &            "         PAKO LOCK. ",                                      &
       &                      " DateTime:",                                      &
       &                Values(1), "-", Values(2), "-",Values(3),                &
       &                      "T",                                               &
       &                Values(5), ":", Values(6), ":",Values(7),Zone,           &
       &                      " PID: ",                                          &
       &                      ipid                                               !
  Close (unit=iou)
  shellCommand = "chmod 400 .pakoLock"
  ier2 = system(shellCommand)
  !
End Subroutine pako_load
!<FF>
Subroutine pako_run(line,comm,error)
  !---------------------------------------------------------------------
  ! Support routine for the language PAKO.
  ! This routine is able to call the routines associated to each
  ! command of the language PAKO
  !---------------------------------------------------------------------
  !
  Use modulePakoGlobalVariables
  Use modulePakoDisplayText
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  Use modulePakoCatalog
  Use modulePakoSource
  Use modulePakoCalibrate
  Use modulePakoDIYlist
  Use modulePakoFocus
  Use modulePakoOnOff
  Use modulePakoLissajous
  Use modulePakoOtfMap
  Use modulePakoPointing
  Use modulePakoTip
  Use modulePakoTrack
  Use modulePakoVlbi
  Use modulePakoDisplay
  Use modulePakoStart
  Use modulePakoStop
  Use modulePakoSave
  !
!!   from run*.f < 2011:
!!$ !!$OLD      Use modulePakoMessages
!!$ !!$OLD      Use modulePakoGlobalParameters
!!$ !!$OLD      Use modulePakoGlobalVariables
!!$ !!$OLD      Use modulePakoDisplayText
!!$ !!$OLD      Use modulePakoReceiver
!!$ !!$OLD      Use modulePakoBackend
!!$ !!$OLD      Use modulePakoSwBeam
!!$ !!$OLD      Use modulePakoSwFrequency
!!$ !!$OLD      Use modulePakoSwWobbler
!!$ !!$OLD      Use modulePakoSwTotalPower
!!$ !!$OLD      Use modulePakoCatalog
!!$ !!$OLD      Use modulePakoSource
!!$ !!$OLD      Use modulePakoCalibrate
!!$ !!$OLD      Use modulePakoDIYlist
!!$ !!$OLD      Use modulePakoFocus
!!$ !!$OLD      Use modulePakoOnOff
!!$ !!$OLD      Use modulePakoOtfMap
!!$ !!$OLD      Use modulePakoPointing
!!$ !!$OLD      Use modulePakoTip
!!$ !!$OLD      Use modulePakoTrack
!!$ !!$OLD      Use modulePakoVlbi
!!$ !!$OLD      Use modulePakoDisplay
!!$ !!$OLD      Use modulePakoStart
!!$ !!$OLD      Use modulePakoStop
!!$ !!$OLD      Use modulePakoSave
  !
  Use gbl_message
  !
  Character(len=*),  Intent(in)    :: line   ! Command line to send to support routines
  Character(len=12), Intent(in)    :: comm   ! Command resolved by the SIC interpreter
  Logical,           Intent(inout) :: error  ! Logical error flag
  !
  Integer iUnit, ioerr
  !
  Character*128 messageText
  !
  Character*12  programName
  programName = "pako"                      !! TBD program/language name for messages and scripts
  !
!!$  Call pako_message(seve%c,'PAKO',line)
  !
  Call pako_message(seve%t,programName,                                          &
       &    " pako-load.f90, ---> SR: pako_run: ")                               ! trace execution
  Call pako_message(seve%t,programName,                                          &
       &    " SR pako-run, line: "//line )                                       ! trace execution
  !
  Include 'inc/display/displayCommandLine.inc'
  !
  ! Call appropriate subroutine according to COMM
  !
  Select Case(comm)
     !
!!$  from template:
!!$  Case('DUMMY')
!!$    Call pako_dummy(line,error)
!!$  Case('VOID')
!!$    Call pako_void(line,error)
     !
  Case ('BACKEND')        
     Call backend             (programName,LINE,COMM,ERROR)
  Case ('CALIBRATE')  
     Call CALIBRATE           (programName,LINE,COMM, ERROR)
  Case('CATALOG')  
     Call CATALOG             (LINE,ERROR)   !! TBD 1.2 review parameters
  Case ('DISPLAY')  
     Call pakoDisplay         (programName,LINE,COMM, ERROR)
  Case ('DIYLIST')  
     Call DIYlist             (programName,LINE,COMM, ERROR)
  Case('FOCUS')  
     Call FOCUS               (programName,LINE,COMM, ERROR)
  Case('OFFSETS') 
     Call OFFSETS             (programName,LINE,COMM,ERROR)
  Case('ONOFF')  
     Call ONOFF               (programName,LINE,COMM,ERROR)
!!$!! TBD future     Case('OTFCIRCLE')  
!!$!! TBD future        Call  OTFCIRCLE(LINE,COMM, ERROR)
!!$!! TBD future     Case('OTFLINEAR')  
!!$!! TBD future         Call OTFLINEAR(LINE,COMM, ERROR)
  Case('LISSAJOUS')     
     Call LISSAJOUS           (programName,LINE,COMM, ERROR)
  Case('OTFMAP')     
     Call OTFMAP              (programName,LINE,COMM, ERROR)
  Case('POINTING') 
     Call POINTING            (programName,LINE,COMM, ERROR)
  Case('RECEIVER')  
     Call receiver            (programName,LINE,COMM,ERROR)
  Case('SAVE') 
     Call pakoSave            (programName,LINE,COMM, ERROR)
!! TBD future  Case('SEGMENT') 
!! TBD future     Call segment(programName,LINE,COMM, ERROR)
  Case('SET') 
     Call pakoSet             (programName,LINE,COMM,ERROR)
  Case('SHOW')
!!$     Call pako_message(seve%t,'PAKO_RUN',' CASE '//comm)
     Call pakoShow            (programName,LINE,COMM,ERROR)
  Case('SOURCE')  
     Call source              (programName,LINE,COMM,ERROR)
  Case('START') 
     Call pakoStart           (programName,LINE,COMM, ERROR)
  Case('STOP') 
     Call pakoStop            (programName,LINE,COMM, ERROR)
  Case('SUBSCAN') 
     Call Subscan             (programName,LINE,COMM, ERROR)
  Case('SWBEAM') 
     Call swBeam              (programName,LINE,COMM,ERROR)
  Case('SWFREQUENCY') 
     Call swFrequency         (programName,LINE,COMM,ERROR)
  Case('SWTOTAL') 
     Call swTotalPower        (programName,LINE,COMM,ERROR)
  Case('SWWOBBLER') 
     Call swWobbler           (programName,LINE,COMM,ERROR)
  Case('TIP') 
     Call TIP                 (programName,LINE,COMM, ERROR)
  Case('TRACK') 
     Call TRACK               (programName,LINE,COMM, ERROR)
  Case('VLBI') 
     Call VLBI                (programName,LINE,COMM, ERROR)
     !
  Case default
    Call pako_message(seve%e,'PAKO_RUN','Unimplemented command '//comm)
    error = .True.
  End Select
  !
End Subroutine pako_run
!<FF>
Function pako_error()
  !---------------------------------------------------------------------
  ! Support routine for the error status of the library. Does nothing
  ! here.
  !---------------------------------------------------------------------
  Logical :: pako_error
  !
  pako_error = .False.
  !
End Function pako_error
