!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoTip">
!   <VERSION>
!                Id: modulePakoTip.f90,v 1.3.0 2017-10-05 Hans Ungerechts  !!  obsolete setPrivilege%GISMO
!                Id: modulePakoTip.f90,v 1.2.5 2016-06-08 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.4 2016-02-15 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.4 2016-01-28 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.4 2015-05-28 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2015-02-12 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2014-10-27 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2014-06-25 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2014-05-29 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2014-02-10 Hans Ungerechts
!                Id: modulePakoTip.f90,v 1.2.3 2014-02-03 Hans Ungerechts
!                                      v 1.2.1 2013-04-08 Hans Ungerechts
!                incorporating
!                Id: modulePakoTip.f90,v 1.1.12.2  2013-03-26  Hans Ungerechts
!
!                Id: modulePakoTip.f90,v 1.2.0 2012-08-20 Hans Ungerechtsts
!                based on
!                Id: modulePakoTip.f90,v 1.1.5 2011-03-17 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>
!                Calibrate
!                Focus
!                OnOff
!                OtfMap
!                Pointing
!                Tip
!                Track
!                VLBI
!   </SIBLINGS>        
!
!   Pako module for command: TIP
!
! </DOCUMENTATION> <!-- name="modulePakoTip" -->
!
!----------------------------------------------------------------------
!
!
!     Id: modulePakoTip.f90,v 1.1.5 2011-03-17 Hans Ungerechts
!     
!     Family:   OBSERVING MODES
!     Siblings: moduleTip.f90
!
!----------------------------------------------------------------------
!     PAKO module for TIP
!
Module modulePakoTip
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  !! TBD 2.0:   Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  Public :: Tip
  Public :: displayTip
  Public :: saveTip
  Public :: startTip
!!$  Public :: tip, displayTip, saveTip, startTip
  !     
  ! *** Variables for tip ***
  Include 'inc/variables/variablesTip.inc'
!!!   
Contains
!!!
!!!
  Subroutine Tip( programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer   ::  ia1, ia2, ia3, iiMax
    Real      ::  aFirst, aLast, eBot, eTop, speed, speedMax, timeMin
    !
    Logical   ::  isConnected
    !
    Include 'inc/ranges/rangesTip.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Tip, v 1.2.5 2016-06-08 ---> SR: Tip ")                     ! trace execution
    !
    ! *** initialize ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    !     read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    Call checkNofParameters(command,                                             &
         &     0, 1,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       Include 'inc/parameters/parametersAzimuth.inc'
       Include 'inc/options/readOptionsTip.inc'
    End If
    !
    ! *** set defaults ***
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                                  &
            &        (command,option,commandFull,optionFull,                     &
            &        commandIndex,optionIndex,iCommand,iOption,                  &
            &        errorNotFound,errorNotUnique)                               !
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
    !
    ! *** read Parameters, Options (again, after defaults!) ***
    !     and check for validity and ranges 
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       Include 'inc/parameters/parametersAzimuth.inc'
       Include 'inc/options/readOptionsTip.inc'
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.error) Then
       !
       errorInconsistent = .False.
       !
       aFirst = vars(iIn)%airmass%from
       aLast  = vars(iIn)%airmass%to
       !
       If (.Not. vars(iIn)%doSlew) Then
          If (aFirst .Gt. aLast) Then
             errorInconsistent = .True.
             Write (messageText,*) " from larger airmass ", aFirst,              &
                  &                " to smaller airmass ",  aLast,               &
                  &                " is only supported with option /Slew "       ! 
             Call pakoMessage(priorityE,severityE,"TIP ",messageText)
             Write (messageText,*) " up in elevation ",                          &
                  &                " is only supported with option /Slew "       ! 
             Call pakoMessage(priorityW,severityW,"TIP ",messageText)
          End If
       End If
       !
       Call queryReceiver(rec%Bolo, isConnected)
       !
       !D       Write (6,*) "   GV%privilege:   ", GV%privilege
       !
!!$       If      ( .Not. (     GV%privilege.Eq.setPrivilege%GISMO                  &
!!$            &           .Or. GV%privilege.Eq.setPrivilege%ncsTeam                &
       If      ( .Not. (     GV%privilege.Eq.setPrivilege%ncsTeam                &
            &          )     ) Then                                              !
          !
          If (.Not.isConnected .And. vars(iIn)%doSlew) Then
             errorInconsistent = .True.
             Write (messageText,*) "only supported with MAMBO bolometer"
             Call pakoMessage(priorityE,severityE,"TIP /Slew",messageText)
          End If
          !
       End If
       !
       If (vars(iIn)%doSlew) Then
!!OLD !!$          aFirst = Min (vars(iIn)%airmass%from, vars(iIn)%airmass%to)
!!OLD !!$          aLast  = Max (vars(iIn)%airmass%from, vars(iIn)%airmass%to)
          aFirst = vars(iIn)%airmass%from
          aLast  = vars(iIn)%airmass%to
          eTop     = Asin(1.0/aFirst)/deg
          eBot     = Asin(1.0/aLast)/deg
          speed    = Abs(eTop-eBot)/vars(iIn)%tSubscan
!!OLD !!$       speedMax = 30.0/60.0  !! JP: maximum  0.5 deg/sec                    !! 2007-01-23
!!OLD !!$                             !!            =  30 deg/min 
!!OLD !!$                             !!            = 360 deg / 12 min 
          speedMax = 22.5/60.0  !!     maximum  22.5 deg/min                   !! 2010-11-16 
                                !!            =              0.375 deg/sec 
                                !!            =  50% (JP) of 45    deg/min     !! 50% of Verstellbetrieb El (Brandt & Gatzlaff, 1981)
                                !!            =              360 deg / 16 min 
          timeMin  = Abs(eTop-eBot)/speedMax
          If (speed.Gt.speedMax) Then
             errorInconsistent = .True.
             Write (messageText,*) " implied speed ",                            &
                  &                speed,    " [deg/s] ",                        &
                  &                " is larger than limit ",                     &
                  &                speedMax, " [deg/s] "                         !
             Call pakoMessage(priorityE,severityE,"TIP ",messageText)
             Write (messageText,*) "use longer time, at least /tSubscan ",       &
                  &                timeMin,  " [s] "                             !
             Call pakoMessage(priorityI,severityI,"TIP ",messageText)
          End If
          If (Abs(eTop-eBot).Lt.40.0) Then
             Write (messageText,*) "small range in elevation ", Abs(eTop-eBot)
             Call pakoMessage(priorityW,severityW,"TIP ",messageText)
          End If
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
    End If
    !
    ! *** display values ***
    If (.Not.ERROR) Then
       Call displayTip
    End If
    !
    ! *** set "selected" observing mode & plot ***
    If (.Not.error) Then
       GV%observingMode              =  OM%Tip
       GV%observingModePako          =  OMpako%Tip
       GV%omSystemName               =  GPnone
       GV%notReadyRXafterTip         =  .False.
       GV%notReadySWafterOM          =  .False.
       GV%notReadySecondaryRafterOM  =  .False.
       tipCommand                    =  line(1:lenc(line))
       Call analyzeTip (programName, errorA)
!!$       !TBD Call listSegmentList (errorA)
!!$       !TBD Call plotTip (errorP)
    End If
    !
    Return
  End Subroutine Tip
!!!
!!!
  Subroutine displayTip
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical            :: isConnected
    Character(len=24)  :: command
    Character(len=36)  :: valueText36
    !
    Call queryReceiver(rec%Bolo, isConnected)
    !
    Include 'inc/display/commandDisplayTip.inc'
    !
  End Subroutine displayTip
!!!
!!!
  Subroutine analyzeTip (programName, errorA)
    !
    Character(len=*),   Intent(in)   :: programName
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer              :: nScan 
    Character (len=12)   :: aUnit, sUnit
    Integer              :: iSS           = 0     ! counts subcans/segment
    Integer              :: nSS           = 1
    Real                 :: aa            = 1.0
    Real                 :: aFirst        = 1.0
    Real                 :: aLast         = 1.0
    Real                 :: aStep         = 1.0
    Real                 :: ee            = 10.0
    Real                 :: eFirst        = 10.0
    Real                 :: eLast         = 10.0
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Tip, v 1.2.5 2016-06-08 ---> SR: analyzeTip ")              ! trace execution
    !
    !D     Write (6,*) "      vars(iValue)%nAir:       ", vars(iValue)%nAir
    !D     Write (6,*) "      vars(iValue)%doTune:     ", vars(iValue)%doTune
    !D     Write (6,*) "      vars(iValue)%tTune:      ", vars(iValue)%tTune
    !
    errorA = .False.
    iSS    = 0
    nScan     = 1111
    aUnit     = au%degree                            ! TIP is in degree
    sUnit     = su%degree                            !     and   degree /second
    !
    ! TBD: info messages about number of subscans and elevations!
    !
!!OLD !!$    aFirst = Min (vars(iValue)%airmass%from, vars(iValue)%airmass%to)
!!OLD !!$    aLast  = Max (vars(iValue)%airmass%from, vars(iValue)%airmass%to)
    aFirst = vars(iValue)%airmass%from
    aLast  = vars(iValue)%airmass%to
    aStep  = Abs (vars(iValue)%airmass%by)
    eFirst = Asin(1.0/aFirst)/deg
    eLast  = Asin(1.0/aLast)/deg
    !
    If (.Not. vars(iValue)%doSlew) Then
       !
       nSS    = Int(Abs(aLast-aFirst)/aStep)+1
       aLast  = aFirst+(nSS-1)*aStep
       eLast  = Asin(1.0/aLast)/deg
       !
    End If
    !
    vars(iValue)%elevation = eLast
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "    aFirst:            ", aFirst
       Write (6,*) "    Asin(1.0/aFirst):  ", Asin(1.0/aFirst)
       Write (6,*) "    eFirst:            ", eFirst
       Write (6,*) "    aLast:             ", aLast
       Write (6,*) "    Asin(1.0/aLast):   ", Asin(1.0/aLast)
       Write (6,*) "    eLast:             ", eLast
       Write (6,*) "    vars(iValue)%elevation: ", vars(iValue)%elevation
    End If
    !
    !                                                                            ! !! alternative versions of TIP /airmass
!!$    If ( vars(iValue)%nAir.Ge.6 ) Then                                        ! !! list of airmasses
    If ( vars(iValue)%hasAirmassList ) Then                                      ! !! list of airmasses
       !
       aLast = vars(iValue)%airmassList(vars(iValue)%nAir)
       eLast  = Asin(1.0/aLast)/deg
       vars(iValue)%elevation = eLast
       !
       If (GV%doDebugMessages) Then
          Write (6,*) "   --> list of Airmasses"
          Write (6,*) "    aLast:                    ", aLast
          Write (6,*) "    Asin(1.0/aLast):          ", Asin(1.0/aLast)
          Write (6,*) "    eLast:                    ", eLast
          Write (6,*) "    vars(iValue)%elevation:   ", vars(iValue)%elevation
       End If
       !
       Do ii = 1, vars(iValue)%nAir, 1                                           ! !! loop over list of airmasses
          !
          aa = vars(iValue)%airmassList(ii)
          ee = Asin(1.0/aa)/deg
          !
          If ( vars(iValue)%doTune ) Then                                        ! !! insert tune subscan
             !
             iSS = iSS+1
             !
             segList(iSS)            =  segDefault
             segList(iSS)%newSubscan =  .True.
             segList(iSS)%scanNumber =  nScan
             segList(iSS)%ssNumber   =  iSS
             segList(iSS)%segNumber  =  1
             segList(iSS)%ssType     =  ss%tune
             segList(iSS)%segType    =  seg%track
             segList(iSS)%angleUnit  =  aUnit
             segList(iSS)%speedUnit  =  sUnit
             segList(iSS)%flagOn     =  .True.
             segList(iSS)%flagRef    =  .False.
             segList(iSS)%pStart%x   =  0.0
             segList(iSS)%pStart%y   =  ee-eLast                                
             segList(iSS)%pEnd       =  segList(iSS)%pStart
             segList(iSS)%speedStart =  0.0
             segList(iSS)%speedEnd   =  0.0
             segList(iSS)%systemName =  'horizontal'
             segList(iSS)%altOption  =  'tSegment'
             segList(iSS)%tSegment   =  vars(iValue)%tTune
             segList(iSS)%tRecord    =  0.1
             !
             Write (messageText,'( A10,I4,A,A, A,F6.2, A,F6.2,A, A,F6.2,A )')    &
                  &                "subscan #:", iSS, " ", segList(iSS)%ssType,  &
                  &                " at airmass ", aa,                           &
                  &                " Elevation ", ee, " [deg]",                  &
                  &                " time ", segList(iSS)%tSegment, " [sec]"     !
             Call pakoMessage(priorityI,severityI,"TIP "//" Analyze",messageText)
             !D              Write (6,*) "      segList(iSS)%segType:   ", segList(iSS)%segType
             !D              Write (6,*) "      segList(iSS)%flagOn:    ", segList(iSS)%flagOn
             !D              Write (6,*) "      segList(iSS)%flagRef:   ", segList(iSS)%flagRef
             !
          End If   !!   ( vars(iValue)%doTune )
          !
          iSS = iSS+1                                                            ! !! track subscan
          !
          segList(iSS)            =  segDefault
          segList(iSS)%newSubscan =  .True.
          segList(iSS)%scanNumber =  nScan
          segList(iSS)%ssNumber   =  iSS
          segList(iSS)%segNumber  =  1
!!$       segList(iSS)%ssType     =  ss%track                                    ! !! standard track subscan (test) 2014-06-25
          segList(iSS)%ssType     =  ss%am                                       ! !! airmass subscan (special handling in execution!)
          segList(iSS)%segType    =  seg%track
          segList(iSS)%angleUnit  =  aUnit
          segList(iSS)%speedUnit  =  sUnit
          segList(iSS)%flagOn     =  .True.
          segList(iSS)%flagRef    =  .False.
          segList(iSS)%pStart%x   =  0.0
          segList(iSS)%pStart%y   =  ee-eLast
          segList(iSS)%pEnd       =  segList(iSS)%pStart
          segList(iSS)%speedStart =  0.0
          segList(iSS)%speedEnd   =  0.0
          segList(iSS)%systemName =  'horizontal'
          segList(iSS)%altOption  =  'tSegment'
          segList(iSS)%tSegment   =  vars(iValue)%tSubscan
          segList(iSS)%tRecord    =  0.1
          !
          Write (messageText,'( A10,I4,A,A, A,F6.2, A,F6.2,A, A,F6.2,A )')       &
               &                "subscan #:", iSS, " ", segList(iSS)%ssType,     &
               &                " at airmass ", aa,                              &
               &                " Elevation ", ee, " [deg]",                     &
               &                " time ", segList(iSS)%tSegment, " [sec]"        !
          Call pakoMessage(priorityI,severityI,"TIP "//" Analyze",messageText)
          !D           Write (6,*) "      segList(iSS)%segType:   ", segList(iSS)%segType
          !D           Write (6,*) "      segList(iSS)%flagOn:    ", segList(iSS)%flagOn
          !D           Write (6,*) "      segList(iSS)%flagRef:   ", segList(iSS)%flagRef
          !
!!$          Write (messageText,*) "subscan #:", iSS, " ", segList(iSS)%ssType,  &
!!$               &                " at airmass ", aa,                           &
!!$               &                " Elevation ", ee, " [deg]",                  &
!!$               &                " time ", segList(iSS)%tSegment, " [sec]"     !
!!$          Call pakoMessage(priorityI,severityI,"TIP "//" Analyze",messageText)
          !
       End Do   !!   ii = 1, vars(iValue)%nAir, 1  !! loop over list of airmasses
       !
    Else If (vars(iValue)%doSlew) Then                                           ! !! slew tip
       !
       iSS = 1
       !
          segList(iSS)            =  segDefault
          segList(iSS)%newSubscan =  .True.
          segList(iSS)%scanNumber =  nScan
          segList(iSS)%ssNumber   =  iSS
          segList(iSS)%segNumber  =  1
          segList(iSS)%ssType     =  ss%se
          segList(iSS)%segType    =  GPnone
          segList(iSS)%angleUnit  =  aUnit
          segList(iSS)%speedUnit  =  sUnit
          segList(iSS)%flagOn     =  .False.
          segList(iSS)%flagRef    =  .False.
          segList(iSS)%pStart%x   =  vars(iValue)%azimuth
          segList(iSS)%pStart%y   =  eFirst
          segList(iSS)%pEnd%x     =  vars(iValue)%azimuth
          segList(iSS)%pEnd%y     =  eLast
          segList(iSS)%speedStart =  Abs(eFirst-eLast)/vars(iValue)%tSubscan
          segList(iSS)%speedEnd   =  segList(iSS)%speedStart 
          segList(iSS)%systemName =  'horizontal'
          segList(iSS)%altOption  =  'tSegment'
          segList(iSS)%tSegment   =  vars(iValue)%tSubscan
          segList(iSS)%tRecord    =  0.1
          Write (messageText,*) "subscan #:", iSS, " ", segList(iSS)%ssType,     &
               &                        segList(iSS)%pStart%y,                   &
               &                " to ", segList(iSS)%pEnd%y  ,                   &
               &                " [deg] with speed ", segList(iSS)%speedStart    !
          Call pakoMessage(priorityI,severityI,"TIP "//" Analyze",messageText)
          !
    Else                                                                         ! !! from-to-by loop of airmasses
       !
       Do ii = 1, nSS, 1                                                         ! !! loop over airmasses
          !
          aa = aFirst + (ii-1)*aStep
          ee = Asin(1.0/aa)/deg
          !
          iSS = iSS+1
          segList(iSS)            =  segDefault
          segList(iSS)%newSubscan =  .True.
          segList(iSS)%scanNumber =  nScan
          segList(iSS)%ssNumber   =  iSS
          segList(iSS)%segNumber  =  1
          segList(iSS)%ssType     =  ss%am
          segList(iSS)%segType    =  seg%track
          segList(iSS)%angleUnit  =  aUnit
          segList(iSS)%speedUnit  =  sUnit
          segList(iSS)%flagOn     =  .False.
          segList(iSS)%flagRef    =  .False.
          segList(iSS)%pStart%x   =  0.0
          segList(iSS)%pStart%y   =  ee-eLast
          segList(iSS)%pEnd       =  segList(iSS)%pStart
          segList(iSS)%speedStart =  0.0
          segList(iSS)%speedEnd   =  0.0
          segList(iSS)%systemName =  'horizontal'
          segList(iSS)%altOption  =  'tSegment'
          segList(iSS)%tSegment   =  vars(iValue)%tSubscan
          segList(iSS)%tRecord    =  0.1
          !
          Write (messageText,*) "subscan #:", iSS, " ", segList(iSS)%ssType,     &
               &                " at airmass ", aa,                              &
               &                " Elevation ", ee, " [deg]",                     &
               &                " time ", segList(iSS)%tSegment, " [sec]"        !
          Call pakoMessage(priorityI,severityI,"TIP "//" Analyze",messageText)
          !
       End Do   !!   ii = 1, nSS, 1  !! loop over airmasses 
       !
    End If   !!  ( vars(iValue)%nAir.Ge.6 )  alternative versions of TIP /airmass
    !                                                     
    nSegments = iSS
    !
    Return 
    !
  End Subroutine analyzeTip
!!!
!!!
  Subroutine saveTip(programName,LINE,commandToSave,                             &
       &         iUnit, ERROR)                                                   !
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    Integer    ::  jj
    !
    contC = contNN
    Include 'inc/commands/saveCommand.inc'
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Tip, v 1.2.4 2015-05-27 --> SR: saveTip ")                  ! trace execution
    !
    contC = contCC
    Include 'inc/options/saveAirmass.inc'
    Include 'inc/options/saveSlew.inc'
!!$    Include 'inc/options/saveTpaddle.inc'
    !
    If (.Not. vars(iValue)%doTune) Then
       doContinue = .False.
       contC = contCN
    End If
    Include 'inc/options/saveTsubscan.inc'
    !
    If (vars(iValue)%doTune) Then
       Include 'inc/options/saveTune.inc'
       doContinue = .False.
       contC = contCN
       Include 'inc/options/saveTuneTime.inc'
    End If
    !
    ! NB: parameter Azimuth gets saved last
    !     because TIP without argument has special meaning:
    !     "current azimuth"
    contC = contNN
    Include 'inc/parameters/saveAzimuth.inc'
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveTip
!!!
!!!
  Subroutine startTip(programName,LINE,commandToSave,                            &
       &         iUnit, ERROR)                                                   !
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: ii
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Logical                 :: errorL, errorXML
    !
    Character(len=lenLine)  ::  messageText
    !
    ! TBD: tip for heterodyne receivers
    !
    !D      Write (6,*) "  --> module moduleTip: SR startTip "
    ! 
    If (vars(iValue)%doCurrentAzimuth) Then
       Write (messageText,*)  " will be done at current azimuth "
       Call pakoMessage(priorityI,severityI,OM%tip(1:12),messageText)
    End If
    !
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,                             &
         &         iUnit, ERROR)                                                 !
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",                       &
         &                         comment="save from pako",                     &
         &                         error=errorXML)                               !
    Call pakoXMLwriteStartElement("DESCRIPTION",                                 &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call saveTip          (programName,LINE,commandToSave, iUnit, errorL)
    !                                                                       
    Call pakoXMLwriteEndElement("DESCRIPTION",                                   &
         &      error=errorXML)                                             
    Call pakoXMLwriteEndElement("RESOURCE", "pakoScript",                        &
         &      error=errorXML)                                                  !
    !
    Call writeXMLantenna(programName,LINE,commandToSave,                         &
         &         iUnit, ERROR)                                                 !
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,                        &
         &         iUnit, ERROR)                                                 !
    !
    Call writeXMLbackend(programName,LINE,commandToSave,                         &
         &        iUnit, ERROR)                                                  !
    !
    Include 'inc/startXML/swTipXML.inc'
    !
    Include 'inc/startXML/sourceTip.inc'
    !
    Include 'inc/startXML/generalScanHead.inc'
    !
    ! **  Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       messageText = "writing observing mode in XML format"
       Call PakoMessage(priorityI,severityI,                                     &
               &           commandToSave,messageText)                            !
       Call writeXMLtip(programName,LINE,commandToSave,                          &
         &         iUnit, ERROR)                                                 !
    End If
    !
    ! **  subscan list to XML
    Include 'inc/startXML/tipSubscansXML.inc'
    !   
    Include 'inc/startXML/generalTail.inc'
    !
    Return
  End Subroutine startTip
!!!
!!!
  Subroutine writeXMLtip(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: ii, len1, len2
    Character (len=lenCh)   :: valueC
    Character (len=lenLine) :: valueComment
    Logical                 :: errorXML
    !
    Call pako_message(seve%t,"PAKO",                                             &
         &  " module Tip, v 1.2.4  --> SR: writeXMLTip 2015-05-28")              ! trace execution
    !
    ERROR = .False.
    !
    !D Write (6,*) "   --> writeXMLtip "
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               ! 
    !
    Include 'inc/startXML/parametersAzimuthXML.inc'
    !
    Include 'inc/startXML/optionAirmassXML.inc'
    Include 'inc/startXML/optionSlewXML.inc'
    Include 'inc/startXML/optionTsubscanXML.inc'
!!$    Include 'inc/startXML/optionTpaddleXML.inc'
    Include 'inc/startXML/optionTune.inc'
    Include 'inc/startXML/optionTtune.inc'
    !
    Include 'inc/startXML/annotationsXML.inc'
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLtip
!!!
!!!
End Module modulePakoTip
