!                                                                                  !! NCSstd
! Id: modulePakoGlobalParameters.f90 ,v 1.3.0  2018-06-06 Hans Ungerechts          !! NCSstd
!                                    ,v 1.3.0  2017-11-11 Hans Ungerechts          !! NCSstd
!                                    ,v 1.2.6  2017-08-17 Hans Ungerechts          !! NCSstd
!                                    ,v 1.2.5  2016-06-09 Hans Ungerechts          !! 
!                                              2016-05-18 Hans Ungerechts          !! 
!                                              2016-04-04
!                                    ,v 1.2.3  2014-10-29 Hans Ungerechts          !! 
!                                    ,v 1.2.3  2014-07-17 Hans Ungerechts          !! 
!                                    ,v 1.2.3  2014-04-24 Hans Ungerechts          !! 
!                                     v 1.2.2  2013-01-21 Hans Ungerechts          !! 
!                                     v 1.2.1  2012-06-22 Hans Ungerechts          !! 
!     from:                                                                        !!
!     modulePakoGlobalParameters.f90 ,v 1.1.12 2012-03-26 Hans Ungerechts          !! 
!                                                                                  !! 
!----------------------------------------------------------------------------------!! 
! PAKO 
!
! <DOCUMENTATION name="moduleGlobalParameters">
!
! constants  (parameters in the FORTRAN sense),
!    including unit conversion factors
!
! integer codes for SIC error messages
! 
! character strings for "choice" keywords
!    lenCH : length of these strings 
!    they come in 2 flavors:
!    *%* :  
!       derived data types, 
!          e.g., rec%Bolo                =   "Bolometer"
!          short, recognizable by their peculiar construction
!    *Choices(i) :
!       elements of arrays of char strings, 
!          e.g., receiverChoices(iBolo)  =   "Bolometer"
!          more elaborate, suitable for general matching routine
!    nDim*Choices : number of available choices in each case
!    i*           : predefined indices 
!    The strings normally are in mixed case, e.g., "horizontalTrue",
!    but use standard spelling, e.g., "Doppler" or standard 
!    abbreviations, e.g., "HERA", where appropriate.
!
! print* subroutines to print the choice keywords
!
! </DOCUMENTATION> <!-- name="moduleGlobalParameters" -->
!
Module modulePakoGlobalParameters
  !
  Implicit None
  Save
  Public
  !
  ! *** Sizes, kinds for variables ***     
  Include 'inc/parameters/sizes.inc'
  !
  ! ********* Global Version Id *********
  !Character (len=lenCh),  Parameter :: pakoVersion       = "v 1.3.0 for 2017-11-11"
  !Character (len=lenCh),  Parameter :: pakoVersionString = "v 1.3.0"
  !Character (len=lenCh),  Parameter :: pakoVersionDate   = "2017-11-11"
  !Character (len=lenCh),  Parameter :: pakoSubversion    = "s 1.3.0.4 2018-06-06"
  !
  Character (len=lenCh),  Parameter :: pakoVersion       = "v 2.0.1 for 2018-07-11"
  Character (len=lenCh),  Parameter :: pakoVersionString = "v 2.0.1"
  Character (len=lenCh),  Parameter :: pakoVersionDate   = "2018-07-11"
  Character (len=lenCh),  Parameter :: pakoSubversion    = "s 2.0.1.1 2018-07-11"
  !
  Real(Kind=kindDouble),  Parameter :: Pi     = 3.141592653589793D0
  !
  ! *** conversion factors
  !   * for frequencies:
  Real(Kind=kindDouble),  Parameter :: MHz      = 1.0D6
  Real(Kind=kindDouble),  Parameter :: MHzToGhz = 1.0D-3
  !
  !   * to rad:
  Real(Kind=kindDouble),  Parameter :: rad    = 1.0D0
  Real(Kind=kindDouble),  Parameter :: radian = 1.0D0
  Real(Kind=kindDouble),  Parameter :: arcsec = Pi/(180.0D0*60.0D0*60.0D0)
  Real(Kind=kindDouble),  Parameter :: arcmin = Pi/(180.0D0*60.0D0)
  Real(Kind=kindDouble),  Parameter :: deg    = Pi/(180.0D0)
  Real(Kind=kindDouble),  Parameter :: degree = Pi/(180.0D0)
  Real(Kind=kindDouble),  Parameter :: degs   = Pi/(180.0D0)
  Real(Kind=kindDouble),  Parameter :: second = Pi/( 12.0D0*60.0D0*60.0D0)
  Real(Kind=kindDouble),  Parameter :: minute = Pi/( 12.0D0*60.0D0)
  Real(Kind=kindDouble),  Parameter :: hour   = Pi/( 12.0D0)
  !
  ! *** for special codes:
  Character (len=1),      Parameter :: asterisk = '*'
  Character (len=1),      Parameter :: fit      = 'f'
  Character (len=1),      Parameter :: lookup   = 'l'
  !
  ! *** for SIC messages:
  !
  Integer, Parameter :: severityI = 1
  Integer, Parameter :: severityW = 2
  Integer, Parameter :: severityE = 3
  Integer, Parameter :: severityF = 4
  !
  Integer, Parameter :: priorityI = severityI*2
  Integer, Parameter :: priorityW = severityW*2
  Integer, Parameter :: priorityE = severityE*2
  Integer, Parameter :: priorityF = severityF*2
  !
  ! *** for display styles:
  !
  Character (len=lenCh), Parameter  ::  DScommand    =  "#0F0"
  Character (len=lenCh), Parameter  ::  DSok         =  "#0F0"
  Character (len=lenCh), Parameter  ::  DSinactive   =  "#CCC"
  Character (len=lenCh), Parameter  ::  DShighlight  =  "#AAF"
  Character (len=lenCh), Parameter  ::  DSwarning    =  "#FE0"
  Character (len=lenCh), Parameter  ::  DSerror      =  "#E00"
  !
  !                                                                                  !! NCSstd
  ! *** CHARACTER STRINGS / KEYWORDS FOR CHOICES                                     !! NCSstd
  !                                                                                  !! NCSstd
  ! *** RECEIVERS                                                                    !! NCSstd
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88
  !
  Integer, Parameter      :: nDimReceiverChoices  =  16
  Integer, Parameter      :: iA100          =  1       !   "A100"            
  Integer, Parameter      :: iA230          =  2       !   "A230"              
  Integer, Parameter      :: iB100          =  3       !   "B100"              
  Integer, Parameter      :: iB230          =  4       !   "B230"              
  Integer, Parameter      :: iC150          =  5       !   "C150"              
  Integer, Parameter      :: iC270          =  6       !   "C270"              
  Integer, Parameter      :: iD150          =  7       !   "D150"              
  Integer, Parameter      :: iD270          =  8       !   "D270"              
  Integer, Parameter      :: iBolo          =  9       !   "Bolometer"         
  Integer, Parameter      :: iHERA          = 10       !   "HERAall"              
  Integer, Parameter      :: iHERA1         = 11       !   "HERA1vertical"    
  Integer, Parameter      :: iHERA2         = 12       !   "HERA2horizontal"  
  !  ** EMIR
  Integer, Parameter      :: iE090          = 13       !   "E090"            
  Integer, Parameter      :: iE150          = 14       !   "E150"            
  Integer, Parameter      :: iE230          = 15       !   "E230"            
  Integer, Parameter      :: iE300          = 16       !   "E330"            
  !  ** test / technical receiver
  Integer, Parameter      :: iRxTest        = 17       !   "    "
  !
  Type :: receiverChoicesType
     Character(len=lenCh) ::  A100                     =   "A100"            
     Character(len=lenCh) ::  A230                     =   "A230"            
     Character(len=lenCh) ::  B100                     =   "B100"            
     Character(len=lenCh) ::  B230                     =   "B230"            
     Character(len=lenCh) ::  C150                     =   "C150"            
     Character(len=lenCh) ::  C270                     =   "C270"            
     Character(len=lenCh) ::  D150                     =   "D150"            
     Character(len=lenCh) ::  D270                     =   "D270"            
     Character(len=lenCh) ::  Bolo                     =   "Bolometer"       
     Character(len=lenCh) ::  HERA                     =   "HERAall"            
     Character(len=lenCh) ::  HERA1                    =   "HERA1vertical"  
     Character(len=lenCh) ::  HERA2                    =   "HERA2horizontal"
     !  ** EMIR
     Character(len=lenCh) ::  E090                     =   "E090"            
     Character(len=lenCh) ::  E150                     =   "E150"            
     Character(len=lenCh) ::  E230                     =   "E230"            
     Character(len=lenCh) ::  E300                     =   "E330"            
  End Type receiverChoicesType
  !
  ! style: line up in column: 30                            60                    82 85 88
  Type (receiverChoicesType), Parameter ::                                        &  !! NCSstd
       &  rec = receiverChoicesType                                               &
       &                                                 (                        &
       &                                                   "A100           "   ,  &  !! NCSstd
       &                                                   "A230           "   ,  &  !! NCSstd
       &                                                   "B100           "   ,  &  !! NCSstd
       &                                                   "B230           "   ,  &  !! NCSstd
       &                                                   "C150           "   ,  &  !! NCSstd
       &                                                   "C270           "   ,  &  !! NCSstd
       &                                                   "D150           "   ,  &  !! NCSstd
       &                                                   "D270           "   ,  &  !! NCSstd
       &                                                   "Bolometer      "   ,  &  !! NCSstd
       &                                                   "HERAall        "   ,  &  !! NCSstd
       &                                                   "HERA1vertical  "   ,  &  !! NCSstd
       &                                                   "HERA2horizontal"   ,  &  !! NCSstd
       !  ** EMIR
       &                                                   "E090           "   ,  &  !! NCSstd
       &                                                   "E150           "   ,  &  !! NCSstd
       &                                                   "E230           "   ,  &  !! NCSstd
       &                                                   "E330           "      &  !! NCSstd
       &                                                 ) 
  !
  Character(len=lenCh), Dimension(nDimReceiverChoices), Parameter ::              &
       &  receiverChoices =                                                       &
       &               (/                                                         &
       &                  rec%A100   ,                                            &
       &                  rec%A230   ,                                            &
       &                  rec%B100   ,                                            &
       &                  rec%B230   ,                                            &
       &                  rec%C150   ,                                            &
       &                  rec%C270   ,                                            &
       &                  rec%D150   ,                                            &
       &                  rec%D270   ,                                            &
       &                  rec%Bolo   ,                                            &
       &                  rec%HERA   ,                                            &
       &                  rec%HERA1  ,                                            &
       &                  rec%HERA2  ,                                            &
       !  ** EMIR                                                                 
       &                  rec%E090   ,                                            &
       &                  rec%E150   ,                                            &
       &                  rec%E230   ,                                            &
       &                  rec%E300                                                &
       &               /)
  !
     Character(len=lenCh) ::  RxHolography             = "HOLOGRAPHY"            !  !!  receiver name for holography
  !
  !                                                                                  !! NCSstd
  ! *   sidebands                                                                    !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSidebandChoices  =  2
  Integer, Parameter      :: ilsb           =  1       !   "LSB"
  Integer, Parameter      :: iusb           =  2       !   "USB"
  !
  Type :: sidebandChoicesType
     Character(len=lenCh) ::  lsb                      =   "LSB"
     Character(len=lenCh) ::  usb                      =   "USB"
  End Type sidebandChoicesType
  !
  Type (sidebandChoicesType), Parameter ::                                        &  !! NCSstd
       &  sb =                                                                    &  !! NCSstd
       &                         sidebandChoicesType (     "LSB"     ,            &  !! NCSstd
       &                                                   "USB"  )                  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSidebandChoices), Parameter ::              &
       &  sidebandChoices =                                                       &
       &                (/ sb%lsb    ,                                            &
       &                   sb%usb   /)
  !
  !                                                                                  !! NCSstd
  !  ** EMIR                                                                         !! NCSstd
  !   * subbands                                                                     !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSubbandChoices  =  7  !   count continued from sidebands!
  Integer, Parameter      :: ilo            =  3       !   "LO" 
  Integer, Parameter      :: ili            =  4       !   "LI" 
  Integer, Parameter      :: iui            =  5       !   "UI" 
  Integer, Parameter      :: iuo            =  6       !   "UO" 
  Integer, Parameter      :: iESBnone       =  7       !   GPnone
  !
  Type :: subbandChoicesType
     Character(len=lenCh) ::  lsb                      =   "LSB"
     Character(len=lenCh) ::  usb                      =   "USB"
     Character(len=lenCh) ::  lo                       =   "LO" 
     Character(len=lenCh) ::  li                       =   "LI" 
     Character(len=lenCh) ::  ui                       =   "UI" 
     Character(len=lenCh) ::  uo                       =   "UO" 
     Character(len=lenCh) ::  none                     =   GPnone
  End Type subbandChoicesType
  !
  ! style: line up in column: 30                            60                    82 85 88
  Type (subbandChoicesType), Parameter ::                                         &  !! NCSstd
       &  esb = subbandChoicesType                                                &
       &                                                 (                        &
       &                                                   "LSB "    ,            &  !! NCSstd
       &                                                   "USB "    ,            &  !! NCSstd
       &                                                   "LO  "    ,            &  !! NCSstd
       &                                                   "LI  "    ,            &  !! NCSstd
       &                                                   "UI  "    ,            &  !! NCSstd
       &                                                   "UO  "    ,            &  !! NCSstd
       &                                                   GPnone                 &
       &                                                 )                        !
  !
  ! style: line up in column: 30                            60                    82 85 88
  Character(len=lenCh), Dimension(nDimSubbandChoices), Parameter ::               &
       &  subbandChoices =                                                        &
       &                (/                                                        &
       &                  esb%lsb    ,                                            &
       &                  esb%usb    ,                                            &
       &                  esb%lo     ,                                            &
       &                  esb%li     ,                                            &
       &                  esb%ui     ,                                            &
       &                  esb%uo     ,                                            &
       &                  esb%none                                                &
       &                /)                                                        !
  !
  !                                                                                  !! NCSstd
  ! *   EMIR inner/outer                                                             !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimEMIRioChoices  =  2
  Integer, Parameter      :: iInner             =  1   !   "inner"
  Integer, Parameter      :: iOuter             =  2   !   "outer"
  !
  Type :: EMIRioChoicesType
     Character(len=lenCh) ::  inner                    =   "inner"
     Character(len=lenCh) ::  outer                    =   "outer"
  End Type EMIRioChoicesType
  !
  ! style: line up in column: 30                            60                    82 85 88
  Type (EMIRioChoicesType), Parameter ::                                          &  !! NCSstd
       &  eio =                                                                   &  !! NCSstd
       &                         EMIRioChoicesType (       "inner"     ,          &  !! NCSstd
       &                                                   "outer"  )             !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimEMIRioChoices), Parameter ::                &
       &  EMIRioChoices =                                                         &
       &               (/ eio%inner  ,                                            &
       &                  eio%outer /)                                            !
  !
  !                                                                                  !! NCSstd
  ! *   choices for EMIR rules                                                       !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimEMIRruleChoices=  3
  Integer, Parameter      :: iUnder             =  1   !   "under"    
  Integer, Parameter      :: iInRange           =  2   !   "in range" 
  Integer, Parameter      :: iOver              =  3   !   "over"     
  !
  Type :: EMIRruleChoicesType
     Character(len=lenCh) ::  Under                    =   "under"    
     Character(len=lenCh) ::  InRange                  =   "in range" 
     Character(len=lenCh) ::  Over                     =   "over"     
  End Type EMIRruleChoicesType
  !
  ! style: line up in column: 30                            60                    82 85 88
  Type (EMIRruleChoicesType), Parameter ::                                        &  !! NCSstd
       &  EMIRtest =                                                              &  !! NCSstd
       &                   EMIRruleChoicesType     (       "under   "  ,          &  !! NCSstd
       &                                                   "in range"  ,          &  !! NCSstd
       &                                                   "over    " )           !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimEMIRruleChoices), Parameter ::              &
       &  EMIRruleChoices  =                                                      &
       &          (/ EMIRtest%Under    ,                                          &
       &             EMIRtest%InRange  ,                                          &
       &             EMIRtest%Over    /)                                          !
  !
  !                                                                                  !! NCSstd
  ! *   doppler (apply Doppler correction for RX or keep frequency fixed)            !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimDopplerChoices  =  2
  Integer, Parameter      :: idoppler       =  1       !   "Doppler"
  Integer, Parameter      :: ifixed         =  2       !   "fixed"  
  !
  Type :: dopplerChoicesType
     Character(len=lenCh) ::  doppler                  =   "Doppler"
     Character(len=lenCh) ::  fixed                    =   "fixed"  
  End Type dopplerChoicesType
  !
  Type (dopplerChoicesType), Parameter ::                                         &  !! NCSstd
       &  doppler =                                                               &  !! NCSstd
       &                          dopplerChoicesType (     "Doppler"  ,           &  !! NCSstd
       &                                                   "fixed  "  )              !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimDopplerChoices), Parameter ::               &
       &  dopplerChoices =                                                        &
       &           (/ doppler%doppler ,                                           &
       &              doppler%fixed   /)
  !
  !                                                                                  !! NCSstd
  ! *   width (effective RX bandwidth used with spectrometers)                       !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimWidthChoices  =  2
  Integer, Parameter      :: inarrow        =  1       !   "narrow"
  Integer, Parameter      :: iwide          =  2       !   "wide"  
  !
  Type :: widthChoicesType
     Character(len=lenCh) ::  narrow                   =   "narrow" 
     Character(len=lenCh) ::  wide                     =   "wide"  
  End Type widthChoicesType
  !
  Type (widthChoicesType), Parameter ::                                           &  !! NCSstd
       &  width =                                                                 &  !! NCSstd
       &                            widthChoicesType (     "narrow"  ,            &  !! NCSstd
       &                                                   "wide  "  )               !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimWidthChoices), Parameter ::                 &
       &  widthChoices =                                                          &
       &             (/ width%narrow ,                                            &
       &                width%wide   /)
  !
  ! *   bandWidth
  ! **  EMIR: TBS: review values!
  !
  Integer, Parameter      :: nDimBWChoices   =  4
  Integer, Parameter      :: iBWNarrow       =  1
  Integer, Parameter      :: iBWWide         =  2
  Integer, Parameter      :: iBwEMIRsubband  =  3
  Integer, Parameter      :: iBwEMIRsideband =  4

  !
  Type :: BWChoicesType
     Real(kind=kindDouble) ::   narrow       =  0.500D0  ! narrow mode high freq.      
     Real(kind=kindDouble) ::   wide         =  1.000D0  ! wide   mode high freq.      
     Real(kind=kindDouble) ::   EMIRsubband  =  4.000D0  ! EMIR sub band
     Real(kind=kindDouble) ::   EMIRsideband =  8.000D0  ! EMIR full sideband
  End Type BWChoicesType
  !
  Type (BWChoicesType), Parameter ::                                             &
       &  BW =                                                                   &
       &                       BWChoicesType (                                   &
       &                                        0.500D0,                         &
       &                                        1.000D0,                         &
       &                                        4.000D0,                         &
       &                                        8.000D0                          &
       &                                     )                                   !
  !
  Real(kind=kindDouble), Dimension(nDimBWChoices), Parameter ::                  &
       &  BWChoices =                                                            &
       &           (/        BW%narrow,                                          &
       &                     BW%wide,                                            &
       &                     BW%EMIRsubband,                                     &
       &                     BW%EMIRsideband                                     &
       &           /)                                                            !
  !
  !                                                                                  !! NCSstd
  ! *   calibration scale                                                            !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimScaleChoices  =  2
  Integer, Parameter      :: iScaleAntenna  =  1        !  "antenna"
  Integer, Parameter      :: iScaleBeam     =  2        !  "beam"  
  !
  Type :: scaleChoicesType
     Character(len=lenCh) ::       antenna              =  "antenna" 
     Character(len=lenCh) ::       beam                 =  "beam"  
  End Type scaleChoicesType
  !
  Type (scaleChoicesType), Parameter ::                                           &  !! NCSstd
       &  scale =                                                                 &  !! NCSstd
       &                            scaleChoicesType (     "antenna" ,            &  !! NCSstd
       &                                                   "beam   " )               !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimScaleChoices), Parameter ::                 &
       &  scaleChoices =                                                          &
       &             (/      scale%antenna ,                                      &
       &                     scale%beam   /)
  !
  !                                                                                  !! NCSstd
  ! *   polarization of RX in Nasmyth system (RX cabin)                              !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimPolarizationChoices  =  2
  Integer, Parameter      :: ihorizontalPol    =  1    !   "horizontal"
  Integer, Parameter      :: iverticalPol      =  2    !   "vertical"  
  !
  Type :: polarizationChoicesType
     Character(len=lenCh) ::  hor                      =   "horizontal"
     Character(len=lenCh) ::  ver                      =   "vertical"  
  End Type polarizationChoicesType
  !
  Type (polarizationChoicesType), Parameter ::                                    &  !! NCSstd
       &  pol =                                                                   &  !! NCSstd
       &                     polarizationChoicesType (     "horizontal",          &  !! NCSstd
       &                                                   "vertical  " )            !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimPolarizationChoices), Parameter ::          &
       &  polarizationChoices =                                                   &
       &           (/     pol%hor      ,                                          &
       &                  pol%ver      /)
  !
  ! *   IF
  ! **  EMIR: TBD: review values!
  !
  Integer, Parameter      :: nDimIFChoices  =  6
  Integer, Parameter      :: iIFNarrow      =  1
  Integer, Parameter      :: iIFWide        =  2
  Integer, Parameter      :: iIFlow         =  3
  Integer, Parameter      :: iIfEMIRinner   =  4
  Integer, Parameter      :: iIfEMIRio      =  5
  Integer, Parameter      :: iIfEMIRouter   =  6
  !
  Type :: IFChoicesType
     Real(kind=kindDouble) ::   narrow      =  4.258D0  ! narrow mode high freq.      
     Real(kind=kindDouble) ::   wide        =  4.002D0  ! wide   mode high freq.      
     Real(kind=kindDouble) ::   low         =  1.498D0  !             low  freq. (3mm)
     Real(kind=kindDouble) ::   EMIRinner   =  6.25D0   ! EMIR "inner" subbands
     Real(kind=kindDouble) ::   EMIRio      =  7.84D0   ! EMIR "inner+outer" subbands (full sidebands)
     Real(kind=kindDouble) ::   EMIRouter   =  9.43D0   ! EMIR "outer" subbands
  End Type IFChoicesType
  !
  Type (IFChoicesType), Parameter ::                                             &
       &  If =                                                                   &
       &                       IFChoicesType ( 4.258D0,                          &
       &                                       4.002D0,                          &
       &                                       1.498D0,                          &
       &                                       6.25D0,                           &
       &                                       7.84D0,                           &
       &                                       9.43D0                            &
       &                                     )                                   !
  !
  Real(kind=kindDouble), Dimension(nDimIFChoices), Parameter ::                  &
       &  IFChoices =                                                            &
       &           (/        IF%narrow,                                          &
       &                     IF%wide,                                            &
       &                     IF%low,                                             &
       &                     IF%EMIRinner,                                       &
       &                     IF%EMIRio,                                          &
       &                     IF%EMIRouter                                        &
       &           /)                                                            !
  !
  ! *   IF2
  ! **  EMIR: TBD: review values!
  !
  Integer, Parameter      :: nDimIF2Choices  =  6
  Integer, Parameter      :: iIF2Narrow      =  1
  Integer, Parameter      :: iIF2Wide        =  2
  Integer, Parameter      :: iIF2low         =  3
  Integer, Parameter      :: iIf2EMIRinner   =  4
  Integer, Parameter      :: iIf2EMIRio      =  5
  Integer, Parameter      :: iIf2EMIRouter   =  6
  !
  Type :: IF2ChoicesType
     Real(kind=kindDouble) ::    narrow      =  0.342D0  ! narrow mode high freq.      
     Real(kind=kindDouble) ::    wide        =  0.598D0  ! wide   mode high freq.      
     Real(kind=kindDouble) ::    low         =  0.342D0  !             low  freq. (3mm)
     Real(kind=kindDouble) ::    EMIRinner   =  0.35D0   ! EMIR "inner" subbands
     Real(kind=kindDouble) ::    EMIRio      =  0.35D0   ! EMIR "inner+outer" subbands (full sidebands)
     Real(kind=kindDouble) ::    EMIRouter   =  0.35D0   ! EMIR "outer" subbands
  End Type IF2ChoicesType
  !
  Type (IF2ChoicesType), Parameter ::                                            &
       &  IF2 =                                                                  &
       &                      IF2ChoicesType (  0.342D0,                         &
       &                                        0.598D0,                         &
       &                                        0.342D0,                         &
       &                                        0.35D0 ,                         &
       &                                        0.35D0 ,                         &
       &                                        0.35D0                           &
       &                                     )                                   !
  !
  Real(kind=kindDouble), Dimension(nDimIF2Choices), Parameter ::                 &
       &  IF2Choices =                                                           &
       &           (/        IF2%narrow,                                         &
       &                     IF2%wide,                                           &
       &                     IF2%low,                                            &
       &                     IF2%EMIRinner,                                      &
       &                     IF2%EMIRio,                                         &
       &                     IF2%EMIRouter                                       &
       &           /)                                                            !
  !
  !                                                                                  !! NCSstd
  ! *   derotator systems for HERA                                                   !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimDerotatorSystemChoices  =  4
  Integer, Parameter      :: iDerotFra      =  1       !   "frame"                   
  Integer, Parameter      :: iDerotSky      =  2       !   "sky"                     
  Integer, Parameter      :: iDerotHor      =  3       !   "horizontal"                 
  Integer, Parameter      :: iDerotEqu      =  4       !   "equatorial"              
  !
  Type :: derotatorSystemChoicesType
     Character(len=lenCh) ::       fra                 =   "frame"                 
     Character(len=lenCh) ::       sky                 =   "sky"                   
     Character(len=lenCh) ::       hor                 =   "horizontal"               
     Character(len=lenCh) ::       equ                 =   "equatorial"            
  End Type derotatorSystemChoicesType
  !
  Type (derotatorSystemChoicesType), Parameter ::                                 &  !! NCSstd
       &  derot =                                                                 &  !! NCSstd
       &                  derotatorSystemChoicesType (                            &  !! NCSstd
       &                                                   "frame     "    ,      &  !! NCSstd
       &                                                   "sky       "    ,      &  !! NCSstd
       &                                                   "horizontal"    ,      &  !! NCSstd
       &                                                   "equatorial"           &  !! NCSstd same as "sky"
       &                                             )                               !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimDerotatorSystemChoices), Parameter ::       &
       &  derotatorSystemChoices =                                                &
       &               (/                                                         &
       &                     derot%fra  ,                                         &
       &                     derot%sky  ,                                         &
       &                     derot%hor  ,                                         &
       &                     derot%equ                                            &
       &               /)
  !
  !                                                                                  !! NCSstd
  ! **  Bolometers                                                                   !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimBolometerChoices  =  4
  Integer, Parameter      :: imambo1        =  1       !   "MAMBO1"
  Integer, Parameter      :: imambo2        =  2       !   "MAMBO2"
  Integer, Parameter      :: iGISMO         =  3       !   "GISMO"
  Integer, Parameter      :: iNIKA          =  4       !   "NIKA" 
  !
  Type :: bolometerChoicesType
     Character(len=lenCh) ::  mambo1                   =   "MAMBO1" 
     Character(len=lenCh) ::  mambo2                   =   "MAMBO2"
     Character(len=lenCh) ::  GISMO                    =   "GISMO"
     Character(len=lenCh) ::  NIKA                     =   "NIKA" 
  End Type bolometerChoicesType
  !
  Type (bolometerChoicesType), Parameter ::                                      &  !! NCSstd
       &  bolo =                                                                 &  !! NCSstd
       &                        bolometerChoicesType (     "MAMBO1"     ,        &  !! NCSstd
       &                                                   "MAMBO2"     ,        &  !! NCSstd
       &                                                   "GISMO "     ,        &  !! NCSstd
       &                                                   "NIKA  "  )           !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimBolometerChoices), Parameter ::            &
       &  bolometerChoices =                                                     &
       &              (/ bolo%mambo1    ,                                        &
       &                 bolo%mambo2    ,                                        &
       &                 bolo%GISMO     ,                                        &
       &                 bolo%NIKA      /)                                       ! 
  !
  !                                                                                  !! NCSstd
  ! *** BACKENDS                                                                     !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimBackendChoices  =  10
  Integer, Parameter      :: iContinuum     =  1       !   "NBC"
  Integer, Parameter      :: iBBC           =  2       !   "BBC"
  Integer, Parameter      :: iFB4MHz        =  3       !   "4MHz"     
  Integer, Parameter      :: iFB1MHz        =  4       !   "1MHz"     
  Integer, Parameter      :: iFB100kHz      =  5       !   "100kHz"                  !! deprecated
  Integer, Parameter      :: iFTS           =  6       !   "FTS"
  Integer, Parameter      :: iWILMA         =  7       !   "WILMA"    
  Integer, Parameter      :: iVESPA         =  8       !   "VESPA"
  Integer, Parameter      :: iABBA          =  9       !   "ABBA"
  Integer, Parameter      :: iUSBack        = 10       !   "USB"
 
  !
  Type :: backendChoicesType
     Character(len=lenCh) ::  Continuum                =   "NBC"
     Character(len=lenCh) ::  BBC                      =   "BBC"      
     Character(len=lenCh) ::  FB4MHz                   =   "4MHz"     
     Character(len=lenCh) ::  FB1MHz                   =   "1MHz"     
     Character(len=lenCh) ::  FB100kHz                 =   "100kHz"   
     Character(len=lenCh) ::  FTS                      =   "FTS"
     Character(len=lenCh) ::  WILMA                    =   "WILMA"    
     Character(len=lenCh) ::  VESPA                    =   "VESPA"
     Character(len=lenCh) ::  ABBA                     =   "ABBA"
     Character(len=lenCh) ::  USB                      =   "USB"
  End Type backendChoicesType
  !
  Type (backendChoicesType), Parameter ::                                         &  !! NCSstd
       &  bac =                                                                   &  !! NCSstd
       &                          backendChoicesType (     "NBC   "      ,        &  !! NCSstd
       &                                                   "BBC   "      ,        &  !! NCSstd
       &                                                   "4MHz  "      ,        &  !! NCSstd
       &                                                   "1MHz  "      ,        &  !! NCSstd
       &                                                   "100kHz"      ,        &  !! deprecated
       &                                                   "FTS   "      ,        &  !! NCSstd
       &                                                   "WILMA "      ,        &  !! NCSstd
       &                                                   "VESPA "      ,        &  !! NCSstd
       &                                                   "ABBA  "      ,        &  !! NCSstd
       &                                                   "USB   "  )   ! for tests !! NCSstd 

  !
  Character(len=lenCh), Dimension(nDimBackendChoices), Parameter ::               &
       &  backendChoices =                                                        &
       &           (/     bac%Continuum ,                                         &
       &                  bac%BBC       ,                                         &
       &                  bac%FB4MHz    ,                                         &
       &                  bac%FB1MHz    ,                                         &
       &                  bac%FB100kHz  ,                                         &
       &                  bac%FTS       ,                                         &
       &                  bac%WILMA     ,                                         &
       &                  bac%VESPA     ,                                         &
       &                  bac%ABBA      ,                                         &
       &                  bac%USB       /)
  !
  !                                                                                  !! NCSstd
  ! *   backendModes (VESPA)                                                         !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimBEmodeChoices  =  3
  Integer, Parameter      :: iSimp          =  1       !   "simple"      
  Integer, Parameter      :: iPara          =  2       !   "parallel"    
  Integer, Parameter      :: iPola          =  3       !   "polarization"
  !
  Type :: BEmodeChoicesType
     Character(len=lenCh) ::  Simp                     =   "simple"      
     Character(len=lenCh) ::  Para                     =   "parallel"    
     Character(len=lenCh) ::  Pola                     =   "polarization"
  End Type BEmodeChoicesType
  !
  Type (BEmodeChoicesType), Parameter ::                                          &  !! NCSstd
       &  BEmode =                                                                &  !! NCSstd
       &                          BEmodeChoicesType (      "simple      ",        &  !! NCSstd
       &                                                   "parallel    ",        &  !! NCSstd
       &                                                   "polarization" )          !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimBEmodeChoices), Parameter ::                &
       &  BEmodeChoices =                                                         &
       &           (/  BEmode%Simp      ,                                         &
       &               BEmode%Para      ,                                         &
       &               BEmode%Pola      /)
  !
  ! *   FTS  backend                                                                 !! NCSstd                       
  ! *   FTS: resolutions (MHz)                                                       !! NCSstd
  !
  Integer, Parameter      :: nDimFTSChoices  =  4
  Integer, Parameter      :: iFTSfine        =  1
  Integer, Parameter      :: iFTSwide        =  2
  Integer, Parameter      :: iFTSfineHERA    =  3
  Integer, Parameter      :: iFTSwideHERA    =  4
  !
  ! style: line up in column: 30                            60                   81  85 88  
  !
  Type ::FTSresChoicesType
     Real(kind=kindDouble) :: fine         =  100.0D0/512.0/4.0                      !! fine / high resolution   EMIR
     Real(kind=kindDouble) :: wide         =  100.0D0/512.0                          !! wide / low  resolution   EMIR
     Real(kind=kindDouble) :: fineHERA     =  100.0D0/512.0/4.0                      !! fine / high resolution   HERA
     Real(kind=kindDouble) :: wideHERA     =  100.0D0/512.0                          !! wide / low  resolution   HERA
  End Type FTSresChoicesType
  !
  Type (FTSresChoicesType), Parameter ::                                         &   !! NCSstd
       &  FTSres =                                                               &   !! NCSstd
       &                   FTSresChoicesType (                                   &   !! NCSstd
       &                                        100.0D0/512.0/4.0,               &   !! NCSstd fine / high resolution   EMIR
       &                                        100.0D0/512.0,                   &   !! NCSstd wide / low  resolution   EMIR
       &                                        100.0D0/512.0/4.0,               &   !! NCSstd fine / high resolution   HERA
       &                                        100.0D0/512.0                    &   !! NCSstd wide / low  resolution   HERA
       &                                     )                                   !   !! NCSstd
  !
  Real(kind=kindDouble), Dimension(nDimFTSChoices), Parameter ::                 &
       &  FTSresChoices =                                                        &
       &           (/  FTSres%fine,                                              &
       &               FTSres%wide,                                              &
       &               FTSres%fineHERA,                                          &
       &               FTSres%wideHERA                                           &
       &           /)                                                            !
  !
  ! *   FTS: maximum bandwidths (MHz)                                                !! NCSstd
  !
  Type ::FTSbwChoicesType
     Real(kind=kindDouble) :: fine           =  1820.0D0                             !! fine / high resolution   EMIR
     Real(kind=kindDouble) :: wide           =  4050.0D0                             !! wide / low  resolution   EMIR
     Real(kind=kindDouble) :: fineHERA       =   512.0D0                             !! fine / high resolution   HERA
     Real(kind=kindDouble) :: wideHERA       =  1024.0D0                             !! wide / low  resolution   HERA
  End Type FTSbwChoicesType
  !
  Type (FTSbwChoicesType), Parameter ::                                          &   !! NCSstd
       &  FTSbw =                                                                &   !! NCSstd
       &                     FTSbwChoicesType (                                  &   !! NCSstd
       &                                        1820.0D0,                        &   !! NCSstd fine / high resolution   EMIR
       &                                        4050.0D0,                        &   !! NCSstd wide / low  resolution   EMIR
       &                                         512.0D0,                        &   !! NCSstd fine / high resolution   HERA
       &                                        1024.0D0                         &   !! NCSstd wide / low  resolution   HERA
       &                                     )                                   !   !! NCSstd
  !
  Real(kind=kindDouble), Dimension(nDimFTSChoices), Parameter ::                 &
       &  FTSbwChoices =                                                         &
       &           (/   FTSbw%fine,                                              &
       &                FTSbw%wide,                                              &
       &                FTSbw%fineHERA,                                          &
       &                FTSbw%wideHERA                                           &
       &           /)                                                            !
  !
  !                                                                                  !! NCSstd
  ! *** SWITCHING MODES                                                              !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSwModeChoices  =  4
  Integer, Parameter      :: iSwTotal       =  1       !   "totalPower"        
  Integer, Parameter      :: iSwBeam        =  2       !   "beamSwitching"     
  Integer, Parameter      :: iSwWobb        =  3       !   "wobblerSwitching"  
  Integer, Parameter      :: iSwFreq        =  4       !   "frequencySwitching"
  !
  Type :: swModeChoicesType
     Character(len=lenCh) ::    Total                  =   "totalPower"        
     Character(len=lenCh) ::    Beam                   =   "beamSwitching"     
     Character(len=lenCh) ::    Wobb                   =   "wobblerSwitching"  
     Character(len=lenCh) ::    Freq                   =   "frequencySwitching"
  End Type swModeChoicesType
  !
  Type (swModeChoicesType), Parameter ::                                          &  !! NCSstd
       &  swMode =                                                                &  !! NCSstd
       &                          swModeChoicesType (      "totalPower        " , &  !! NCSstd
       &                                                   "beamSwitching     " , &  !! NCSstd
       &                                                   "wobblerSwitching  " , &  !! NCSstd
       &                                                   "frequencySwitching" )    !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimswModeChoices), Parameter ::                &
       &  swModeChoices =                                                         &
       &           (/    swMode%Total     ,                                       &
       &                 swMode%Beam      ,                                       &
       &                 swMode%Wobb      ,                                       &
       &                 swMode%Freq      /)
  !
  Type (swModeChoicesType), Parameter ::                                          &  !! NCSstd
       &  swModePako =                                                            &  !! NCSstd
       &                          swModeChoicesType (      "swTotal    "        , &  !! NCSstd
       &                                                   "swBeam     "        , &  !! NCSstd
       &                                                   "swWobbler  "        , &  !! NCSstd
       &                                                   "swFrequency" )           !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimswModeChoices), Parameter ::                &
       &  swModeChoicesPako =                                                     &
       &           (/    swModePako%Total     ,                                   &
       &                 swModePako%Beam      ,                                   &
       &                 swModePako%Wobb      ,                                   &
       &                 swModePako%Freq      /)
  !
  !                                                                                  !! 
  ! *** paKo COMMANDS (for SAVE etc.)                                                !! 
  !                                                                                  !! 
  ! style: line up in column: 30                            60                       85 88
  !
  !TBD: add "STOP"
  !
  Integer, Parameter      :: nDimCOMChoices  = 32
  Integer, Parameter      :: iComCal         =  1       !  "calibrate"  
  Integer, Parameter      :: iComPointing    =  2       !  "pointing"   
  Integer, Parameter      :: iComFocus       =  3       !  "focus"      
  Integer, Parameter      :: iComTip         =  4       !  "tip"        
  Integer, Parameter      :: iComTrack       =  5       !  "track"      
  Integer, Parameter      :: iComOnOff       =  6       !  "onOff"      
  Integer, Parameter      :: iComOtfMap      =  7       !  "OTFmap"     
  Integer, Parameter      :: iComRaster      =  8       !  "raster"     
  Integer, Parameter      :: iComOtfTip      =  9       !  "OTFtip"     
  Integer, Parameter      :: iComCross       = 10       !  "crossRaster"
  Integer, Parameter      :: iComOtfCross    = 11       !  "OTFcross"   
  Integer, Parameter      :: iComOtfFocus    = 12       !  "OTFfocus"   
  Integer, Parameter      :: iComVLBI        = 13       !  "VLBI"       
  Integer, Parameter      :: iCombackend     = 14       !  "backend"    
  Integer, Parameter      :: iComcatalog     = 15       !  "catalog"    
  Integer, Parameter      :: iComoffsets     = 16       !  "offsets"    
  Integer, Parameter      :: iComsegment     = 17       !  "segment"    
  Integer, Parameter      :: iComsubscan     = 18       !  "subscan"    
  Integer, Parameter      :: iComreceiver    = 19       !  "receiver"   
  Integer, Parameter      :: iComsave        = 20       !  "save"       
  Integer, Parameter      :: iComset         = 21       !  "set"        
  Integer, Parameter      :: iComshow        = 22       !  "show"       
  Integer, Parameter      :: iComsource      = 23       !  "source"     
  Integer, Parameter      :: iComswBeam      = 24       !  "swBeam"     
  Integer, Parameter      :: iComswFrequency = 25       !  "swFrequency"
  Integer, Parameter      :: iComswTotal     = 26       !  "swTotal"    
  Integer, Parameter      :: iComswWobbler   = 27       !  "swWobbler"  
  Integer, Parameter      :: iComswitching   = 28       !  "switching"  
  Integer, Parameter      :: iComoptions     = 29       !  "options"    
  Integer, Parameter      :: iComall         = 30       !  "all"        
  Integer, Parameter      :: iComDIY         = 31       !  "DIY"        
  Integer, Parameter      :: iComLissajous   = 32       !  "Lissajous"        
  !
  Type :: COMChoicesType
     Character(len=lenCh) ::     Cal                    =  "calibrate"    
     Character(len=lenCh) ::     Pointing               =  "pointing"     
     Character(len=lenCh) ::     Focus                  =  "focus"        
     Character(len=lenCh) ::     Tip                    =  "tip"          
     Character(len=lenCh) ::     Track                  =  "track"        
     Character(len=lenCh) ::     OnOff                  =  "onOff"        
     Character(len=lenCh) ::     OtfMap                 =  "onTheFlyMap"  
     Character(len=lenCh) ::     Raster                 =  "raster"       
     Character(len=lenCh) ::     OtfTip                 =  "onTheFlyTip"  
     Character(len=lenCh) ::     Cross                  =  "crossRaster"  
     Character(len=lenCh) ::     OtfCross               =  "onTheFlyCross"
     Character(len=lenCh) ::     OtfFocus               =  "onTheFlyFocus"
     Character(len=lenCh) ::     VLBI                   =  "VLBI"
     Character(len=lenCh) ::     backend                =  "backend"    
     Character(len=lenCh) ::     catalog                =  "catalog"    
     Character(len=lenCh) ::     offsets                =  "offsets"    
     Character(len=lenCh) ::     segment                =  "segment"    
     Character(len=lenCh) ::     subscan                =  "subscan"    
     Character(len=lenCh) ::     receiver               =  "receiver"   
     Character(len=lenCh) ::     save                   =  "save"       
     Character(len=lenCh) ::     set                    =  "set"        
     Character(len=lenCh) ::     show                   =  "show"       
     Character(len=lenCh) ::     source                 =  "source"     
     Character(len=lenCh) ::     swBeam                 =  "swBeam"     
     Character(len=lenCh) ::     swFrequency            =  "swFrequency"
     Character(len=lenCh) ::     swTotal                =  "swTotal"    
     Character(len=lenCh) ::     swWobbler              =  "swWobbler"  
     Character(len=lenCh) ::     switching              =  "switching"  
     Character(len=lenCh) ::     options                =  "options"    
     Character(len=lenCh) ::     all                    =  "all"        
     Character(len=lenCh) ::     DIY                    =  "DIY"        
     Character(len=lenCh) ::     Lissajous              =  "Lissajous"        
  End Type COMChoicesType
  !
  ! **  
  !
  Type (COMChoicesType), Parameter ::                                             &  
       &  COMpako =                                                               &  
       &                        COMChoicesType (           "calibrate  "   ,      &  
       &                                                   "pointing   "   ,      &  
       &                                                   "focus      "   ,      &  
       &                                                   "tip        "   ,      &  
       &                                                   "track      "   ,      &  
       &                                                   "onOff      "   ,      &  
       &                                                   "OTFmap     "   ,      &  
       &                                                   "raster     "   ,      &  
       &                                                   "OTFtip     "   ,      &  
       &                                                   "crossRaster"   ,      &  
       &                                                   "OTFcross   "   ,      &  
       &                                                   "OTFfocus   "   ,      &  
       &                                                   "VLBI       "   ,      &
       &                                                   "backend    "   ,      &
       &                                                   "catalog    "   ,      &
       &                                                   "offsets    "   ,      &
       &                                                   "segment    "   ,      &
       &                                                   "subscan    "   ,      &
       &                                                   "receiver   "   ,      &
       &                                                   "save       "   ,      &
       &                                                   "set        "   ,      &
       &                                                   "show       "   ,      &
       &                                                   "source     "   ,      &
       &                                                   "swBeam     "   ,      &
       &                                                   "swFrequency"   ,      &
       &                                                   "swTotal    "   ,      &
       &                                                   "swWobbler  "   ,      &
       &                                                   "switching  "   ,      &
       &                                                   "options    "   ,      &
       &                                                   "all        "   ,      &
       &                                                   "DIY        "   ,      &
       &                                                   "Lissajous  "   )      !
  !
  Character(len=lenCh), Dimension(nDimCOMChoices), Parameter ::                   &
       &  COMChoicesPako =                                                        &
       &           (/         COMpako%Cal             ,                           &
       &                      COMpako%Pointing        ,                           &
       &                      COMpako%Focus           ,                           &
       &                      COMpako%Tip             ,                           &
       &                      COMpako%Track           ,                           &
       &                      COMpako%OnOff           ,                           &
       &                      COMpako%OtfMap          ,                           &
       &                      COMpako%Raster          ,                           &
       &                      COMpako%OtfTip          ,                           &
       &                      COMpako%Cross           ,                           &
       &                      COMpako%OtfCross        ,                           &
       &                      COMpako%OtfFocus        ,                           &
       &                      COMpako%VLBI            ,                           &
       &                      COMpako%backend         ,                           &
       &                      COMpako%catalog         ,                           &
       &                      COMpako%offsets         ,                           &
       &                      COMpako%segment         ,                           &
       &                      COMpako%subscan         ,                           &
       &                      COMpako%receiver        ,                           &
       &                      COMpako%save            ,                           &
       &                      COMpako%set             ,                           &
       &                      COMpako%show            ,                           &
       &                      COMpako%source          ,                           &
       &                      COMpako%swBeam          ,                           &
       &                      COMpako%swFrequency     ,                           &
       &                      COMpako%swTotal         ,                           &
       &                      COMpako%swWobbler       ,                           &
       &                      COMpako%switching       ,                           &
       &                      COMpako%options         ,                           &
       &                      COMpako%all             ,                           &
       &                      COMpako%DIY             ,                           &
       &                      COMpako%Lissajous                                   &
       &                                             /)                           !

  !
  !                                                                                  !! NCSstd
  ! *** OBSERVING MODES                                                              !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimOMChoices  =  16
  Integer, Parameter      :: iOmCal         =  1       !   "calibrate"    
  Integer, Parameter      :: iOmPointing    =  2       !   "pointing"     
  Integer, Parameter      :: iOmFocus       =  3       !   "focus"        
  Integer, Parameter      :: iOmTip         =  4       !   "tip"          
  Integer, Parameter      :: iOmTrack       =  5       !   "track"        
  Integer, Parameter      :: iOmOnOff       =  6       !   "onOff"        
  Integer, Parameter      :: iOmOtfMap      =  7       !   "onTheFlyMap"  
  Integer, Parameter      :: iOmRaster      =  8       !   "raster"       
  Integer, Parameter      :: iOmOtfTip      =  9       !   "onTheFlyTip"  
  Integer, Parameter      :: iOmCross       = 10       !   "crossRaster"  
  Integer, Parameter      :: iOmOtfCross    = 11       !   "onTheFlyCross"
  Integer, Parameter      :: iOmOtfFocus    = 12       !   "onTheFlyFocus"
  Integer, Parameter      :: iOmVLBI        = 13       !   "VLBI"
  Integer, Parameter      :: iOmSubscanList = 14       !   "subscanList"
  Integer, Parameter      :: iOmDIY         = 15       !   "DIY"
  Integer, Parameter      :: iOmLissajous   = 16       !   "Lissajous"
  !
  Type :: OMChoicesType
     Character(len=lenCh) ::    Cal                    =   "calibrate"    
     Character(len=lenCh) ::    Pointing               =   "pointing"     
     Character(len=lenCh) ::    Focus                  =   "focus"        
     Character(len=lenCh) ::    Tip                    =   "tip"          
     Character(len=lenCh) ::    Track                  =   "track"        
     Character(len=lenCh) ::    OnOff                  =   "onOff"        
     Character(len=lenCh) ::    OtfMap                 =   "onTheFlyMap"  
     Character(len=lenCh) ::    Raster                 =   "raster"       
     Character(len=lenCh) ::    OtfTip                 =   "onTheFlyTip"  
     Character(len=lenCh) ::    Cross                  =   "crossRaster"  
     Character(len=lenCh) ::    OtfCross               =   "onTheFlyCross"
     Character(len=lenCh) ::    OtfFocus               =   "onTheFlyFocus"
     Character(len=lenCh) ::    VLBI                   =   "VLBI"
     Character(len=lenCh) ::    SubscanList            =   "subscanList"
     Character(len=lenCh) ::    DIY                    =   "DIY"
     Character(len=lenCh) ::    Lissajous              =   "Lissajous"
  End Type OMChoicesType
  !
  Type (OMChoicesType), Parameter ::                                              &  !! NCSstd
       &  OM =                                                                    &  !! NCSstd
       &                        OMChoicesType (            "calibrate    " ,      &  !! NCSstd
       &                                                   "pointing     " ,      &  !! NCSstd
       &                                                   "focus        " ,      &  !! NCSstd
       &                                                   "tip          " ,      &  !! NCSstd
       &                                                   "track        " ,      &  !! NCSstd
       &                                                   "onOff        " ,      &  !! NCSstd
       &                                                   "onTheFlyMap  " ,      &  !! NCSstd
       &                                                   "raster       " ,      &  !! NCSstd ! TBD soon   
       &                                                   "onTheFlyTip  " ,      &  !! NCSstd ! TBD soon ?  
       &                                                   "crossRaster  " ,      &  !! NCSstd ! future
       &                                                   "onTheFlyCross" ,      &  !! NCSstd ! future   
       &                                                   "onTheFlyFocus" ,      &  !! NCSstd ! future?
       &                                                   "VLBI         " ,      &  !! NCSstd
       &                                                   "subscanList  " ,      &  !! NCSstd
       &                                                   "DIY          " ,      &  !! NCSstd
       &                                                   "Lissajous    " )      !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimOMChoices), Parameter ::                    &
       &  OMChoices =                                                             &
       &           (/        OM%Cal                 ,                             &
       &                     OM%Pointing            ,                             &
       &                     OM%Focus               ,                             &
       &                     OM%Tip                 ,                             &
       &                     OM%Track               ,                             &
       &                     OM%OnOff               ,                             &
       &                     OM%OtfMap              ,                             &
       &                     OM%Raster              ,                             &
       &                     OM%OtfTip              ,                             &
       &                     OM%Cross               ,                             &
       &                     OM%OtfCross            ,                             &
       &                     OM%OtfFocus            ,                             &
       &                     OM%VLBI                ,                             &
       &                     OM%subscanList         ,                             &
       &                     OM%DIY                 ,                             &
       &                     OM%Lissajous           /)                            !
  !
  ! **  the following is a variation corresponding to the paKo commands:
  !
  Type (OMChoicesType), Parameter ::                                              &  
       &  OMpako =                                                                &  
       &                        OMChoicesType (            "calibrate  "   ,      &  
       &                                                   "pointing   "   ,      &  
       &                                                   "focus      "   ,      &  
       &                                                   "tip        "   ,      &  
       &                                                   "track      "   ,      &  
       &                                                   "onOff      "   ,      &  
       &                                                   "OTFmap     "   ,      &  
       &                                                   "raster     "   ,      &  
       &                                                   "OTFtip     "   ,      &  
       &                                                   "crossRaster"   ,      &  
       &                                                   "OTFcross   "   ,      &  
       &                                                   "OTFfocus   "   ,      &  
       &                                                   "VLBI       "   ,      &  
       &                                                   "subscanList"   ,      &  
       &                                                   "DIY        "   ,      &  
       &                                                   "Lissajous  "   )      !  
  !
  Character(len=lenCh), Dimension(nDimOMChoices), Parameter ::                    &
       &  OMChoicesPako =                                                         &
       &           (/        OMpako%Cal             ,                             &
       &                     OMpako%Pointing        ,                             &
       &                     OMpako%Focus           ,                             &
       &                     OMpako%Tip             ,                             &
       &                     OMpako%Track           ,                             &
       &                     OMpako%OnOff           ,                             &
       &                     OMpako%OtfMap          ,                             &
       &                     OMpako%Raster          ,                             &
       &                     OMpako%OtfTip          ,                             &
       &                     OMpako%Cross           ,                             &
       &                     OMpako%OtfCross        ,                             &
       &                     OMpako%OtfFocus        ,                             &
       &                     OMpako%VLBI            ,                             &
       &                     OMpako%subscanList     ,                             &
       &                     OMpako%DIY             ,                             &
       &                     OMpako%Lissajous       /)                            !

  !                                                                                  !! NCSstd
  ! *** choices for Purpose                                                          !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimPurposeChoices  =  5
  Integer, Parameter      :: iPurposeCal         =  1       !   "calibrate"    
  Integer, Parameter      :: iPurposePointing    =  2       !   "pointing"     
  Integer, Parameter      :: iPurposeFocus       =  3       !   "focus"        
  Integer, Parameter      :: iPurposeTip         =  4       !   "tip"          
  Integer, Parameter      :: iPurposeMap         =  5       !   "map"          
  !
  Type :: PurposeChoicesType
     Character(len=lenCh) ::         Cal                    =   "calibrate"
     Character(len=lenCh) ::         Pointing               =   "pointing" 
     Character(len=lenCh) ::         Focus                  =   "focus"    
     Character(len=lenCh) ::         Tip                    =   "tip"      
     Character(len=lenCh) ::         Map                    =   "map"      
  End Type PurposeChoicesType
  !
  Type (PurposeChoicesType), Parameter ::                                        &  !! NCSstd
       &  Purpose =                                                              &  !! NCSstd
       &                        PurposeChoicesType (            "calibrate"  ,   &  !! NCSstd
       &                                                        "pointing "  ,   &  !! NCSstd
       &                                                        "focus    "  ,   &  !! NCSstd
       &                                                        "tip      "  ,   &  !! NCSstd
       &                                                        "map      "  )   !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimPurposeChoices), Parameter ::              &
       &  PurposeChoices =                                                       &
       &           (/        Purpose%Cal                 ,                       &
       &                     Purpose%Pointing            ,                       &
       &                     Purpose%Focus               ,                       &
       &                     Purpose%Tip                 ,                       &
       &                     Purpose%Map                 /)                      !
  !

  !
  !                                                                                  !! NCSstd
  ! **  Subscans                                                                     !! NCSstd
  !                                                                                  !! NCSstd
  !
    Integer, Parameter      :: nDimSubscanChoices  =  14
  Integer, Parameter      :: iSSca          =  1       !   "calAmbient"  
  Integer, Parameter      :: iSScc          =  2       !   "calCold"     
  Integer, Parameter      :: iSScs          =  3       !   "calSky"      
  Integer, Parameter      :: iSScp          =  4       !   "calPaddle"   
  Integer, Parameter      :: iSSam          =  5       !   "airmass"     
  Integer, Parameter      :: iSSonFocus     =  6       !   "onFocus"     
  Integer, Parameter      :: iSStrack       =  7       !   "track"       
  Integer, Parameter      :: iSSotf         =  8       !   "onTheFly"    
  Integer, Parameter      :: iSSon          =  9       !   "on"          
  Integer, Parameter      :: iSSref         = 10       !   "reference" 
  Integer, Parameter      :: iSSVLBI        = 11       !   "VLBI" 
  Integer, Parameter      :: iSScg          = 12       !   "calGrid"  
  Integer, Parameter      :: iSSse          = 13       !   "slewElevation"
  Integer, Parameter      :: iSStune        = 14       !   "tune"
  !
  Type :: subscanChoicesType
     Character(len=lenCh) ::    ca                     =   "calAmbient"
     Character(len=lenCh) ::    cc                     =   "calCold"     
     Character(len=lenCh) ::    cs                     =   "calSky"      
     Character(len=lenCh) ::    cp                     =   "calPaddle"   
     Character(len=lenCh) ::    am                     =   "airmass"   
     Character(len=lenCh) ::    onFocus                =   "onFocus"     
     Character(len=lenCh) ::    track                  =   "track"       
     Character(len=lenCh) ::    otf                    =   "onTheFly"    
     Character(len=lenCh) ::    on                     =   "on"            
     Character(len=lenCh) ::    ref                    =   "reference" 
     Character(len=lenCh) ::    VLBI                   =   "VLBI"
     Character(len=lenCh) ::    cg                     =   "calGrid"
     Character(len=lenCh) ::    se                     =   "slewElevation"
     Character(len=lenCh) ::    tune                   =   "tune"
  End Type subscanChoicesType
  !
  Type (subscanChoicesType), Parameter ::                                         &  !! NCSstd
       &  ss =                                                                    &  !! NCSstd
       &                          subscanChoicesType (     "calAmbient   ",       &  !! NCSstd
       &                                                   "calCold      ",       &  !! NCSstd
       &                                                   "calSky       ",       &  !! NCSstd
       &                                                   "calPaddle    ",       &  !! NCSstd
       &                                                   "airmass      ",       &  !! NCSstd
       &                                                   "onFocus      ",       &  !! NCSstd
       &                                                   "track        ",       &  !! NCSstd
       &                                                   "onTheFly     ",       &  !! NCSstd
       &                                                   "on           ",       &  !! NCSstd
       &                                                   "reference    ",       &  !! NCSstd
       &                                                   "VLBI         ",       &  !! NCSstd
       &                                                   "calGrid      ",       &  !! NCSstd
       &                                                   "slewElevation",       &  !! NCSstd
       &                                                   "tune         ")       !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSubscanChoices), Parameter ::               &
       &  subscanChoices =                                                        &
       &                (/   ss%ca      ,                                         &
       &                     ss%cc      ,                                         &
       &                     ss%cs      ,                                         &
       &                     ss%cp      ,                                         &
       &                     ss%am      ,                                         &
       &                     ss%onFocus ,                                         &
       &                     ss%track   ,                                         &
       &                     ss%otf     ,                                         &
       &                     ss%on      ,                                         &
       &                     ss%ref     ,                                         &
       &                     ss%VLBI    ,                                         &
       &                     ss%cg      ,                                         &
       &                     ss%se      ,                                         &
       &                     ss%tune   /)                                         !
  !
  !                                                                                  !! NCSstd
  ! **  Segments (sub-units of OTF subscans)                                         !! NCSstd     
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSegmentChoices  =  5
  Integer, Parameter      :: iSegtrack      =  1       !   "track" 
  Integer, Parameter      :: iSeglinear     =  2       !   "linear"
  Integer, Parameter      :: iSegcircle     =  3       !   "circle"
  Integer, Parameter      :: iSegcurve      =  4       !   "curve" 
  Integer, Parameter      :: iSegLissajous  =  5       !   "lissajous" 
  !
  Type :: segmentChoicesType
     Character(len=lenCh) ::     track                 =   "track" 
     Character(len=lenCh) ::     linear                =   "linear"
     Character(len=lenCh) ::     circle                =   "circle"
     Character(len=lenCh) ::     curve                 =   "curve" 
     Character(len=lenCh) ::     lissajous             =   "lissajous" 
  End Type segmentChoicesType
  !
  Type (segmentChoicesType), Parameter ::                                         &  !! NCSstd
       &  seg =                                                                   &  !! NCSstd
       &                          segmentChoicesType (     "track    "   ,        &  !! NCSstd
       &                                                   "linear   "   ,        &  !! NCSstd
       &                                                   "circle   "   ,        &  !! NCSstd  TBD later
       &                                                   "curve    "   ,        &  !! NCSstd  TBD later
       &                                                   "lissajous"  )         !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSegmentChoices), Parameter ::               &
       &  segmentChoices =                                                        &
       &                (/   seg%track   ,                                        &
       &                     seg%linear  ,                                        &
       &                     seg%circle  ,                                        &
       &                     seg%curve   ,                                        &
       &                     seg%lissajous   /)                                   !
  !
  !                                                                                  !! NCSstd
  ! **  ramp (at start and end of OTF subscans)                                      !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimRampChoices  =  4
  Integer, Parameter      :: irampUp     =  1         !   "Up"       
  Integer, Parameter      :: irampDown   =  2         !   "Down"
  Integer, Parameter      :: irampBoth   =  3         !   "Both"  
  Integer, Parameter      :: irampNone   =  4         !   "none"
  !
  Type :: rampChoicesType
     Character(len=lenCh) ::      Up                  =   "Up"  
     Character(len=lenCh) ::      Down                =   "Down"
     Character(len=lenCh) ::      Both                =   "Both"  
     Character(len=lenCh) ::      None                =   "none"
  End Type rampChoicesType
  !
  Type (rampChoicesType), Parameter ::                                           &  !! NCSstd
       &  ramp =                                                                 &  !! NCSstd
       &                          rampChoicesType (       "Up  "       ,         &  !! NCSstd
       &                                                  "Down"       ,         &  !! NCSstd
       &                                                  "Both"       ,         &  !! NCSstd
       &                                                  "none"       )         !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimrampChoices), Parameter ::                 &
       &  rampChoices =                                                          &
       &              (/     ramp%Up  ,                                          &
       &                     ramp%Down ,                                         &
       &                     ramp%Both ,                                         &
       &                     ramp%None  /)                                       !
  !
  !                                                                                  !! NCSstd
  ! **  traceFlag (to flag "on" and "reference" in segments of OTF subscans)         !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimtraceflagChoices  =  3
  Integer, Parameter      :: itraceOn     =  1         !   "on"       
  Integer, Parameter      :: itraceRef    =  2         !   "reference"
  Integer, Parameter      :: itraceNone   =  3         !   "none"
  !
  Type :: traceflagChoicesType
     Character(len=lenCh) ::       on                  =   "on"       
     Character(len=lenCh) ::       ref                 =   "reference"
     Character(len=lenCh) ::       none                =   "none"
  End Type traceflagChoicesType
  !
  Type (traceflagChoicesType), Parameter ::                                       &  !! NCSstd
       &  traceflag =                                                             &  !! NCSstd
       &                          traceflagChoicesType (   "on       "  ,         &  !! NCSstd
       &                                                   "reference"  ,         &  !! NCSstd
       &                                                   "none     "  )            !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimtraceflagChoices), Parameter ::             &
       &  traceflagChoices =                                                      &
       &              (/ traceflag%on   ,                                         &
       &                 traceflag%ref  ,                                         &
       &                 traceflag%none  /)
  !
  !                                                                                  !! NCSstd
  ! *** UNITS                                                                        !! NCSstd
  ! *   units for angles                                                             !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimAngleChoices  =  7
  Integer, Parameter      :: iradian        =  1       !      
  Integer, Parameter      :: iarcsec        =  2       !   
  Integer, Parameter      :: iarcmin        =  3       !   
  Integer, Parameter      :: idegree        =  4       !    
  Integer, Parameter      :: isecond        =  5       !   
  Integer, Parameter      :: iminute        =  6       !   
  Integer, Parameter      :: ihour          =  7       !      
  !
  Type :: angleChoicesType
     Character(len=lenCh) ::  radian                   =   "radian"   
     Character(len=lenCh) ::  arcsec                   =   "arcsec"
     Character(len=lenCh) ::  arcmin                   =   "arcmin"
     Character(len=lenCh) ::  degree                   =   "degree"
     Character(len=lenCh) ::  second                   =   "second"     
     Character(len=lenCh) ::  minute                   =   "minute"     
     Character(len=lenCh) ::  hour                     =   "hour"     
  End Type angleChoicesType
  !
  Type (angleChoicesType), Parameter ::                                           &  !! 
       &  au =                                                                    &  !! 
       &                            angleChoicesType (     "radian"  ,            &  !! 
       &                                                   "arcsec"  ,            &  !! 
       &                                                   "arcmin"  ,            &  !! 
       &                                                   "degree"  ,            &  !! 
       &                                                   "second"  ,            &  !! 
       &                                                   "minute"  ,            &  !! 
       &                                                   "hour  "  )               !! 
  !
  Character(len=lenCh), Dimension(nDimAngleChoices), Parameter ::                &
       &  angleChoices =                                                         &
       &                (/ au%radian ,                                           &
       &                   au%arcsec ,                                           &
       &                   au%arcmin ,                                           &
       &                   au%degree ,                                           &
       &                   au%second ,                                           &
       &                   au%minute ,                                           &
       &                   au%hour   /)
  !
  Real (Kind=kindDouble), Dimension(nDimAngleChoices), Parameter ::              &
       &  auConv =                                                               &
       &                (/    radian    ,                                        &
       &                      arcsec    ,                                        &
       &                      arcmin    ,                                        &
       &                      degree    ,                                        &
       &                      second    ,                                        &
       &                      minute    ,                                        &
       &                      hour      /)
  !
  !  -- following are our XML conventions (~VOTable)                                 !! NCSstd
  Type (angleChoicesType), Parameter ::                                           &  !! NCSstd
       &  auXML =                                                                 &  !! NCSstd
       &                            angleChoicesType (     "rad   "    ,          &  !! NCSstd
       &                                                   "arcsec"    ,          &  !! NCSstd
       &                                                   "arcmin"    ,          &  !! NCSstd
       &                                                   "deg   "    ,          &  !! NCSstd
       &                                                   "s     "    ,          &  !! NCSstd
       &                                                   "min   "    ,          &  !! NCSstd
       &                                                   "h     "    )             !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimAngleChoices), Parameter ::                &
       &  angleChoicesXML =                                                      &
       &             (/ auXML%radian ,                                           &
       &                auXML%arcsec ,                                           &
       &                auXML%arcmin ,                                           &
       &                auXML%degree ,                                           &
       &                auXML%second ,                                           &
       &                auXML%minute ,                                           &
       &                auXML%hour   /)
  !
  !                                                                                  !! NCSstd
  ! *   units for angular speeds                                                     !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSpeedChoices  =  nDimAngleChoices
  ! NB: indices i* are the same as for  angle units above
  !
  Type :: speedChoicesType
     Character(len=lenCh) ::  radian                   =   "radian/s"   
     Character(len=lenCh) ::  arcsec                   =   "arcsec/s"
     Character(len=lenCh) ::  arcmin                   =   "arcmin/s"
     Character(len=lenCh) ::  degree                   =   "degree/s"
     Character(len=lenCh) ::  second                   =   "second/s"     
     Character(len=lenCh) ::  minute                   =   "minute/s"     
     Character(len=lenCh) ::  hour                     =   "hour/s"     
  End Type speedChoicesType
  !
  Type (speedChoicesType), Parameter ::                                           &  !! 
       &  su =                                                                    &  !! 
       &                            speedChoicesType (     "radian/s"  ,          &  !! 
       &                                                   "arcsec/s"  ,          &  !! 
       &                                                   "arcmin/s"  ,          &  !! 
       &                                                   "degree/s"  ,          &  !! 
       &                                                   "second/s"  ,          &  !! 
       &                                                   "minute/s"  ,          &  !! 
       &                                                   "hour/s  "   )            !! 
  !
  !  -- following are our XML conventions (~VOTable)                                 !! NCSstd
  Type (speedChoicesType), Parameter ::                                           &  !! NCSstd
       &  suXML =                                                                 &  !! NCSstd
       &                            speedChoicesType (     "rad/s   "  ,          &  !! NCSstd
       &                                                   "arcsec/s"  ,          &  !! NCSstd
       &                                                   "arcmin/s"  ,          &  !! NCSstd
       &                                                   "deg/s   "  ,          &  !! NCSstd
       &                                                   "s/s     "  ,          &  !! NCSstd
       &                                                   "min/s   "  ,          &  !! NCSstd
       &                                                   "h/s     "  )             !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSpeedChoices), Parameter ::                &
       &  speedChoices =                                                         &
       &                (/ su%radian ,                                           &
       &                   su%arcsec ,                                           &
       &                   su%arcmin ,                                           &
       &                   su%degree ,                                           &
       &                   su%second ,                                           &
       &                   su%minute ,                                           &
       &                   su%hour   /)
  !
  Character(len=lenCh), Dimension(nDimSpeedChoices), Parameter ::                &
       &  speedChoicesXML =                                                      &
       &             (/ suXML%radian ,                                           &
       &                suXML%arcsec ,                                           &
       &                suXML%arcmin ,                                           &
       &                suXML%degree ,                                           &
       &                suXML%second ,                                           &
       &                suXML%minute ,                                           &
       &                suXML%hour   /)
  !
  !                                                                                  !! NCSstd
  ! *   units for Frequencies                                                        !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimFrequencyChoices  =  5
  Integer, Parameter      :: iTHz           =  1       !   "THz"
  Integer, Parameter      :: iGHz           =  2       !   "GHz"   
  Integer, Parameter      :: iMHz           =  3       !   "MHz"   
  Integer, Parameter      :: ikHz           =  4       !   "kHz"   
  Integer, Parameter      :: iHz            =  5       !   "Hz" 
  !                                                         
  Type :: frequencyChoicesType
     Character(len=lench) ::  THz                      =   "THz"
     Character(len=lench) ::  GHz                      =   "GHz"
     Character(len=lench) ::  MHz                      =   "MHz"
     Character(len=lench) ::  kHz                      =   "kHz"
     Character(len=lench) ::  Hz                       =   "Hz" 
  End Type frequencyChoicesType
  !
  Type (frequencyChoicesType), Parameter ::                                       &  !! NCSstd
       &  FU =                                                                    &  !! NCSstd
       &                        frequencyChoicesType (     "THz"      ,           &  !! NCSstd
       &                                                   "GHz"      ,           &  !! NCSstd
       &                                                   "MHz"      ,           &  !! NCSstd
       &                                                   "kHz"      ,           &  !! NCSstd
       &                                                   "Hz " )                   !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimFrequencyChoices), Parameter ::            &
       &  frequencyChoices =                                                     &
       &                (/ fu%THz ,                                              &
       &                   fu%GHz ,                                              &
       &                   fu%MHz ,                                              &
       &                   fu%kHz ,                                              &
       &                   fu%Hz  /)
  !
  !
  !                                                                                  !! NCSstd
  ! *** SOURCES:                                                                     !! NCSstd
  ! **  Planets                                                                      !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimPlanetChoices  = 10
  Integer, Parameter      :: imercury       =  1       !   "Mercury"
  Integer, Parameter      :: ivenus         =  2       !   "Venus"  
  Integer, Parameter      :: iearth         =  3       !   "Earth"  
  Integer, Parameter      :: imars          =  4       !   "Mars"   
  Integer, Parameter      :: ijupiter       =  5       !   "Jupiter"
  Integer, Parameter      :: iuranus        =  6       !   "Saturn" 
  Integer, Parameter      :: isaturn        =  7       !   "Uranus" 
  Integer, Parameter      :: ineptune       =  8       !   "Neptune"
  Integer, Parameter      :: ipluto         =  9       !   "Pluto"  
  Integer, Parameter      :: isun           = 10       !   "Sun"    
  !
  Type :: planetChoicesType
     Character(len=lenCh) ::  mercury                  =   "Mercury"
     Character(len=lenCh) ::  venus                    =   "Venus"  
     Character(len=lenCh) ::  earth                    =   "Earth"  
     Character(len=lenCh) ::  mars                     =   "Mars"   
     Character(len=lenCh) ::  jupiter                  =   "Jupiter"
     Character(len=lenCh) ::  uranus                   =   "Saturn" 
     Character(len=lenCh) ::  saturn                   =   "Uranus" 
     Character(len=lenCh) ::  neptune                  =   "Neptune"
     Character(len=lenCh) ::  pluto                    =   "Pluto"  
     Character(len=lenCh) ::  sun                      =   "Sun"    
  End Type planetChoicesType
  !
  Type (planetChoicesType), Parameter ::                                          &  !! NCSstd
       &  planets =                                                               &  !! NCSstd
       &                          planetChoicesType (      "Mercury"       ,      &  !! NCSstd
       &                                                   "Venus  "       ,      &  !! NCSstd
       &                                                   "Earth  "       ,      &  !! NCSstd
       &                                                   "Mars   "       ,      &  !! NCSstd
       &                                                   "Jupiter"       ,      &  !! NCSstd
       &                                                   "Saturn "       ,      &  !! NCSstd
       &                                                   "Uranus "       ,      &  !! NCSstd
       &                                                   "Neptune"       ,      &  !! NCSstd
       &                                                   "Pluto  "       ,      &  !! NCSstd
       &                                                   "Sun    "  )              !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimPlanetChoices), Parameter ::               &
       &  planetChoices =                                                        &
       &           (/ planets%mercury          ,                                 &
       &              planets%venus            ,                                 &
       &              planets%earth            ,                                 &
       &              planets%mars             ,                                 &
       &              planets%jupiter          ,                                 &
       &              planets%uranus           ,                                 &
       &              planets%saturn           ,                                 &
       &              planets%neptune          ,                                 &
       &              planets%pluto            ,                                 &
       &              planets%sun              /)
  !
  !                                                                                  !! NCSstd
  ! **  Satellites                                                                   !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSatChoices  = 22
  Integer, Parameter      :: iPhobos         =  1      !   "Phobos"   
  Integer, Parameter      :: iDeimos         =  2      !   "Deimos"   
  Integer, Parameter      :: iIo             =  3      !   "Io"       
  Integer, Parameter      :: iEuropa         =  4      !   "Europa"   
  Integer, Parameter      :: iGanymede       =  5      !   "Ganymede" 
  Integer, Parameter      :: iCallisto       =  6      !   "Callisto" 
  Integer, Parameter      :: iMimas          =  7      !   "Mimas"    
  Integer, Parameter      :: iEnceladus      =  8      !   "Enceladus"
  Integer, Parameter      :: iTethys         =  9      !   "Tethys"   
  Integer, Parameter      :: iDione          = 10      !   "Dione"    
  Integer, Parameter      :: iRhea           = 11      !   "Rhea"     
  Integer, Parameter      :: iTitan          = 12      !   "Titan"    
  Integer, Parameter      :: iHyperion       = 13      !   "Hyperion" 
  Integer, Parameter      :: iIapetus        = 14      !   "Iapetus"  
  Integer, Parameter      :: iMiranda        = 15      !   "Miranda"  
  Integer, Parameter      :: iAriel          = 16      !   "Ariel"    
  Integer, Parameter      :: iUmbriel        = 17      !   "Umbriel"  
  Integer, Parameter      :: iTitania        = 18      !   "Titania"  
  Integer, Parameter      :: iOberon         = 19      !   "Oberon"   
  Integer, Parameter      :: iGabriel        = 20      !   "Gabriel"  
  Integer, Parameter      :: iMoon           = 21      !   "Moon"      
  Integer, Parameter      :: iHolography     = 22      !   "2013-038A"     
  !
  Type :: satChoicesType
     Character(len=lenCh) ::  Phobos                   =   "Phobos"   
     Character(len=lenCh) ::  Deimos                   =   "Deimos"   
     Character(len=lenCh) ::  Io                       =   "Io"       
     Character(len=lenCh) ::  Europa                   =   "Europa"   
     Character(len=lenCh) ::  Ganymede                 =   "Ganymede" 
     Character(len=lenCh) ::  Callisto                 =   "Callisto" 
     Character(len=lenCh) ::  Mimas                    =   "Mimas"    
     Character(len=lenCh) ::  Enceladus                =   "Enceladus"
     Character(len=lenCh) ::  Tethys                   =   "Tethys"   
     Character(len=lenCh) ::  Dione                    =   "Dione"    
     Character(len=lenCh) ::  Rhea                     =   "Rhea"     
     Character(len=lenCh) ::  Titan                    =   "Titan"    
     Character(len=lenCh) ::  Hyperion                 =   "Hyperion" 
     Character(len=lenCh) ::  Iapetus                  =   "Iapetus"  
     Character(len=lenCh) ::  Miranda                  =   "Miranda"  
     Character(len=lenCh) ::  Ariel                    =   "Ariel"    
     Character(len=lenCh) ::  Umbriel                  =   "Umbriel"  
     Character(len=lenCh) ::  Titania                  =   "Titania"  
     Character(len=lenCh) ::  Oberon                   =   "Oberon"   
     Character(len=lenCh) ::  Gabriel                  =   "Gabriel"  
     Character(len=lenCh) ::  Moon                     =   "Moon"     
     Character(len=lenCh) ::  Holography               =   "2013-038A"
  End Type satChoicesType
  !
  Type (satChoicesType), Parameter ::                                            &  !! NCSstd
       &  sats =                                                                 &  !! NCSstd
       &                       satChoicesType (            "Phobos   "     ,     &  !! NCSstd ! Mars
       &                                                   "Deimos   "     ,     &  !! NCSstd
       &                                                   "Io       "     ,     &  !! NCSstd ! Jupiter
       &                                                   "Europa   "     ,     &  !! NCSstd
       &                                                   "Ganymede "     ,     &  !! NCSstd
       &                                                   "Callisto "     ,     &  !! NCSstd
       &                                                   "Mimas    "     ,     &  !! NCSstd ! Saturn
       &                                                   "Enceladus"     ,     &  !! NCSstd
       &                                                   "Tethys   "     ,     &  !! NCSstd
       &                                                   "Dione    "     ,     &  !! NCSstd
       &                                                   "Rhea     "     ,     &  !! NCSstd
       &                                                   "Titan    "     ,     &  !! NCSstd
       &                                                   "Hyperion "     ,     &  !! NCSstd
       &                                                   "Iapetus  "     ,     &  !! NCSstd
       &                                                   "Miranda  "     ,     &  !! NCSstd ! Uranus
       &                                                   "Ariel    "     ,     &  !! NCSstd
       &                                                   "Umbriel  "     ,     &  !! NCSstd
       &                                                   "Titania  "     ,     &  !! NCSstd
       &                                                   "Oberon   "     ,     &  !! NCSstd
       &                                                   "Gabriel  "     ,     &  !! NCSstd
       &                                                   "Moon     "     ,     &  !! NCSstd ! Earth
       &                                                   "2013-038A"     )     !  !! NCSstd 
  !
  Character(len=lenCh), Dimension(nDimSatChoices), Parameter ::                  &
       &  satChoices =                                                           &
       &              (/ sats%Phobos           ,                                 &
       &                 sats%Deimos           ,                                 &
       &                 sats%Io               ,                                 &
       &                 sats%Europa           ,                                 &
       &                 sats%Ganymede         ,                                 &
       &                 sats%Callisto         ,                                 &
       &                 sats%Mimas            ,                                 &
       &                 sats%Enceladus        ,                                 &
       &                 sats%Tethys           ,                                 &
       &                 sats%Dione            ,                                 &
       &                 sats%Rhea             ,                                 &
       &                 sats%Titan            ,                                 &
       &                 sats%Hyperion         ,                                 &
       &                 sats%Iapetus          ,                                 &
       &                 sats%Miranda          ,                                 &
       &                 sats%Ariel            ,                                 &
       &                 sats%Umbriel          ,                                 &
       &                 sats%Titania          ,                                 &
       &                 sats%Oberon           ,                                 &
       &                 sats%Gabriel          ,                                 &
       &                 sats%Moon             ,                                 &
       &                 sats%Holography       /)                                !
  !
  !                                                                                  !! NCSstd

  !                                                                                  !! NCSstd
  ! **  Special Source Names                                                         !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimSpecialSourceChoices  =  2
  Integer, Parameter      :: iTipCurAzi        =  1    !   "TipCurrentAzimuth"
  Integer, Parameter      :: ispecialTest      =  2    !   "SpecialTest"    
  !
  Type :: SpecialSourceChoicesType
     Character(len=lenCh) ::  TipCurAzi                =   "TipCurrentAzimuth"
     Character(len=lenCh) ::  specialTest              =   "SpecialTest"    
  End Type SpecialSourceChoicesType
  !
  Type (SpecialSourceChoicesType), Parameter ::                                  &  !! NCSstd
       &  SpecialSources =                                                       &  !! NCSstd
       &          SpecialSourceChoicesType (               "TipCurrentAzimuth",  &  !! NCSstd
       &                                                   "SpecialTest      ")  !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSpecialSourceChoices), Parameter ::        &
       &  SpecialSourceChoices =                                                 &
       &    (/ SpecialSources%TipCurAzi          ,                               &
       &       SpecialSources%specialTest              /)                        !
  !



  ! **  equinox systems for sources                                                  !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimEquinoxChoices  = 2
  Integer, Parameter      :: iJ             =  1       !   "J"
  Integer, Parameter      :: iB             =  2       !   "B"
  !
  Type :: equinoxChoicesType
     Character(len=lenCh) ::  J                        =   "J"
     Character(len=lenCh) ::  B                        =   "B"
  End Type equinoxChoicesType
  !
  Type (equinoxChoicesType), Parameter ::                                         &  !! NCSstd
       &  equ =                                                                   &  !! NCSstd
       &                          equinoxChoicesType (     "J"         ,          &  !! NCSstd
       &                                                   "B"         )             !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimEquinoxChoices), Parameter ::              &
       &  equinoxChoices =                                                       &
       &               (/ equ%J                           ,                      &
       &                  equ%B                           /)                                
  !
  !                                                                                  !! NCSstd
  ! **  coordinate systems for sources                                               !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimSystemChoices    =  7
  Integer, Parameter      :: igalactic            =  1  !  "galactic"          
  Integer, Parameter      :: iequatorial          =  2  !  "equatorial"        
  Integer, Parameter      :: iapparentEquatorial  =  3  !  "apparentEquatorial"
  Integer, Parameter      :: iecliptic            =  4  !  "ecliptic"          
  Integer, Parameter      :: iapparentEcliptic    =  5  !  "apparentEcliptic"  
  Integer, Parameter      :: ihaDec               =  6  !  "haDec "            
  Integer, Parameter      :: ihorizontal          =  7  !  "horizontal"        
  !
  Type :: systemChoicesType
     Character(len=lenCh) ::  galactic                  =  "galactic"          
     Character(len=lenCh) ::  equatorial                =  "equatorial"        
     Character(len=lenCh) ::  apparentEquatorial        =  "apparentEquatorial"
     Character(len=lenCh) ::  ecliptic                  =  "ecliptic"          
     Character(len=lenCh) ::  apparentEcliptic          =  "apparentEcliptic"  
     Character(len=lenCh) ::  haDec                     =  "haDec "            
     Character(len=lenCh) ::  horizontal                =  "horizontal"        
  End Type systemChoicesType
  !
  Type (systemChoicesType), Parameter ::                                          &  !! NCSstd
       &  sys =                                                                   &  !! NCSstd
       &                            systemChoicesType (    "galactic          " , &  !! NCSstd
       &                                                   "equatorial        " , &  !! NCSstd
       &                                                   "apparentEquatorial" , &  !! NCSstd
       &                                                   "ecliptic          " , &  !! NCSstd
       &                                                   "apparentEcliptic  " , &  !! NCSstd
       &                                                   "haDec             " , &  !! NCSstd
       &                                                   "horizontal        " )    !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimSystemChoices), Parameter ::                &
       &  systemChoices =                                                         &
       &               (/ sys%galactic                  ,                         &
       &                  sys%equatorial                ,                         &
       &                  sys%apparentEquatorial        ,                         &
       &                  sys%ecliptic                  ,                         &
       &                  sys%apparentEcliptic          ,                         &
       &                  sys%haDec                     ,                         &
       &                  sys%horizontal                /)                                
  !
  ! style: line up in column: 30                            60                       85 88
  !                                                                                  !! NCSstd
  ! **  descriptive systems for sources                                              !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimDescriptiveChoices  =  4
  Integer, Parameter      :: inone                =  1  !  "none"  
  Integer, Parameter      :: iorigin              =  2  !  "origin"
  Integer, Parameter      :: ipolar               =  3  !  "polar" 
  Integer, Parameter      :: iEuler               =  4  !  "Euler" 
  !
  Type :: descriptiveChoicesType
     Character(len=lenCh) ::  none                      =  "none"  
     Character(len=lenCh) ::  origin                    =  "origin"
     Character(len=lenCh) ::  polar                     =  "polar" 
     Character(len=lenCh) ::  Euler                     =  "Euler" 
  End Type descriptiveChoicesType
  !
  Type (descriptiveChoicesType), Parameter ::                                     &  !! NCSstd
       &  des =                                                                   &  !! NCSstd
       &                       descriptiveChoicesType (    "none  "             , &  !! NCSstd
       &                                                   "origin"             , &  !! NCSstd
       &                                                   "polar "             , &  !! NCSstd
       &                                                   "Euler "             )    !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimDescriptiveChoices), Parameter ::           &
       &  descriptiveChoices =                                                    &
       &               (/ des%none                      ,                         &
       &                  des%origin                    ,                         &
       &                  des%polar                     ,                         &
       &                  des%Euler                     /)                                
  !                                                                                  !! NCSstd
  ! **  projections for sources                                                      !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimProjectionChoices  =  12
  !! Integer, Parameter   :: inone                =  1  !  "none"                    ! already defined
  Integer, Parameter      :: iradio               =  2  !  "radio"
  Integer, Parameter      :: iTAN                 =  3  !  "TAN"  
  Integer, Parameter      :: iSIN                 =  4  !  "SIN"  
  Integer, Parameter      :: iSTG                 =  5  !  "STG"  
  Integer, Parameter      :: iARC                 =  6  !  "ARC"  
  Integer, Parameter      :: iZEA                 =  7  !  "ZEA"  
  Integer, Parameter      :: iCAR                 =  8  !  "CAR"  
  Integer, Parameter      :: iMER                 =  9  !  "MER"  
  Integer, Parameter      :: iCEA                 = 10  !  "CEA"  
  Integer, Parameter      :: iGLS                 = 11  !  "GLS"  
  Integer, Parameter      :: iAIT                 = 12  !  "AIT"  
  !
  Type :: projectionChoicesType
     Character(len=lenCh) ::  none                      =  "none" 
     Character(len=lenCh) ::  radio                     =  "radio"
     Character(len=lenCh) ::  TAN                       =  "TAN"  
     Character(len=lenCh) ::  SIN                       =  "SIN"  
     Character(len=lenCh) ::  STG                       =  "STG"  
     Character(len=lenCh) ::  ARC                       =  "ARC"  
     Character(len=lenCh) ::  ZEA                       =  "ZEA"  
     Character(len=lenCh) ::  CAR                       =  "CAR"  
     Character(len=lenCh) ::  MER                       =  "MER"  
     Character(len=lenCh) ::  CEA                       =  "CEA"  
     Character(len=lenCh) ::  GLS                       =  "GLS"  
     Character(len=lenCh) ::  AIT                       =  "AIT"  
  End Type projectionChoicesType
  !
  Type (projectionChoicesType), Parameter ::                                      &  !! NCSstd
       &  pro =                                                                   &  !! NCSstd
       &                       projectionChoicesType (     "none "              , &  !! NCSstd
       &                                                   "radio"              , &  !! NCSstd
       &                                                   "TAN  "              , &  !! NCSstd
       &                                                   "SIN  "              , &  !! NCSstd
       &                                                   "STG  "              , &  !! NCSstd
       &                                                   "ARC  "              , &  !! NCSstd
       &                                                   "ZEA  "              , &  !! NCSstd
       &                                                   "CAR  "              , &  !! NCSstd
       &                                                   "MER  "              , &  !! NCSstd
       &                                                   "CEA  "              , &  !! NCSstd
       &                                                   "GLS  "              , &  !! NCSstd
       &                                                   "AIT  "              )    !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimProjectionChoices), Parameter ::            &
       &  projectionChoices =                                                     &
       &               (/ pro%none                      ,                         &
       &                  pro%radio                     ,                         &
       &                  pro%TAN                       ,                         &
       &                  pro%SIN                       ,                         &
       &                  pro%STG                       ,                         &
       &                  pro%ARC                       ,                         &
       &                  pro%ZEA                       ,                         &
       &                  pro%CAR                       ,                         &
       &                  pro%MER                       ,                         &
       &                  pro%CEA                       ,                         &
       &                  pro%GLS                       ,                         &
       &                  pro%AIT                       /)                                
  !
  ! **  reference rames for radial velocities                                        !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimRefFrameChoices  =  3
  Integer, Parameter      :: iLSR              =  1    !   "LSR"           
  Integer, Parameter      :: iheliocentric     =  2    !   "heliocentric"  
  Integer, Parameter      :: ibarycentric      =  3    !   "barycentric"   
  !
  Type :: refFrameChoicesType
     Character(len=lenCh) ::  LSR                      =   "LSR"           
     Character(len=lenCh) ::  heliocentric             =   "heliocentric"  
     Character(len=lenCh) ::  barycentric              =   "barycentric"   
  End Type refFrameChoicesType
  !
  Type (refFrameChoicesType), Parameter ::                                        &  !! NCSstd
       &  refFrame =                                                              &  !! NCSstd
       &                           refFrameChoicesType (                          &  !! NCSstd
       &                                                   "LSR         "       , &  !! NCSstd
       &                                                   "heliocentric"       , &  !! NCSstd 
       &                                                   "barycentric "         &  !! NCSstd
       &                                               )                          !  !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimRefFrameChoices), Parameter ::              &
       &  refFrameChoices =                                                       &
       &          (/ refFrame%LSR                      ,                          &
       &             refFrame%heliocentric             ,                          &
       &             refFrame%barycentric             /)                          !
  !
!!$  ! **  reference rames for radial velocities                                        !! NCSstd
!!$  !                                                                                  !! NCSstd
!!$  !
!!$  Integer, Parameter      :: nDimRefFrameChoices  =  9
!!$  Integer, Parameter      :: inull             =  1    !   "null"          
!!$  Integer, Parameter      :: itopocentric      =  2    !   "topocentric"   
!!$  Integer, Parameter      :: igeocentric       =  3    !   "geocentric"    
!!$  Integer, Parameter      :: iheliocentric     =  4    !   "heliocentric"  
!!$  Integer, Parameter      :: ibarycentric      =  5    !   "barycentric"   
!!$  Integer, Parameter      :: ibody             =  6    !   "body"          
!!$  Integer, Parameter      :: iLSR              =  7    !   "LSR"           
!!$  Integer, Parameter      :: igalactocentric   =  8    !   "galactocentric"
!!$  Integer, Parameter      :: imwb3K            =  9    !   "3K"            
!!$  !
!!$  Type :: refFrameChoicesType
!!$     Character(len=lenCh) ::  null                     =   "null"          
!!$     Character(len=lenCh) ::  topocentric              =   "topocentric"   
!!$     Character(len=lenCh) ::  geocentric               =   "geocentric"    
!!$     Character(len=lenCh) ::  heliocentric             =   "heliocentric"  
!!$     Character(len=lenCh) ::  barycentric              =   "barycentric"   
!!$     Character(len=lenCh) ::  body                     =   "body"          
!!$     Character(len=lenCh) ::  LSR                      =   "LSR"           
!!$     Character(len=lenCh) ::  galactocentric           =   "galactocentric"
!!$     Character(len=lenCh) ::  mwb3K                    =   "3K"            
!!$  End Type refFrameChoicesType
!!$  !
!!$  Type (refFrameChoicesType), Parameter ::                                        &  !! NCSstd
!!$       &  refFrame =                                                              &  !! NCSstd
!!$       &                            refFrameChoicesType (                         &  !! NCSstd
!!$       &                                                   "null"               , &  !! NCSstd --> no Doppler corr.
!!$       &                                                   "topocentric"        , &  !! NCSstd TBD later
!!$       &                                                   "geocentric"         , &  !! NCSstd TBD later
!!$       &                                                   "heliocentric"       , &  !! NCSstd 
!!$       &                                                   "barycentric"        , &  !! NCSstd
!!$       &                                                   "body"               , &  !! NCSstd --> use ephemeris
!!$       &                                                   "LSR"                , &  !! NCSstd Default
!!$       &                                                   "galactocentric"     , &  !! NCSstd TBD later
!!$       &                                                   "3K"                   &  !! NCSstd TBD later           
!!$       )    !! NCSstd
!!$  !
!!$  Character(len=lenCh), Dimension(nDimRefFrameChoices), Parameter ::              &
!!$       &  refFrameChoices =                                                       &
!!$       &          (/ refFrame%null                     ,                          &
!!$       &             refFrame%topocentric              ,                          &
!!$       &             refFrame%geocentric               ,                          &
!!$       &             refFrame%heliocentric             ,                          &
!!$       &             refFrame%barycentric              ,                          &
!!$       &             refFrame%body                     ,                          &
!!$       &             refFrame%LSR                      ,                          &
!!$       &             refFrame%galactocentric           ,                          &
!!$       &             refFrame%mwb3K                    /)                                
!!$  !
  !                                                                                  !! NCSstd
  ! **  conventions for Doppler corrections                                          !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimDopplerConventionChoices  = 2
  Integer, Parameter      :: iDCradio       =  1       !   "radio"  
  Integer, Parameter      :: iDCoptical     =  2       !   "optical"
  !
  Type :: DopplerConventionChoicesType
     Character(len=lenCh) ::    radio                  =   "radio"  
     Character(len=lenCh) ::    optical                =   "optical"
  End Type DopplerConventionChoicesType
  !
  Type (DopplerConventionChoicesType), Parameter ::                               &  !! NCSstd
       &  dc  =                                                                   &  !! NCSstd
       &                DopplerConventionChoicesType (     "radio  "   ,          &  !! NCSstd
       &                                                   "optical"   )             !! NCSstd Default
  !
  Character(len=lenCh), Dimension(nDimDopplerConventionChoices), Parameter ::     &
       &  DopplerConventionChoices =                                              &
       &               (/    dc%radio                  ,                          &
       &                     dc%optical                /)                                
  !
  !                                                                                  !! NCSstd
  ! **  systems for offsets                                                          !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimOffsetChoices  = 8
  Integer, Parameter      :: ipro           =  1       !   "projection" 
  Integer, Parameter      :: ides           =  2       !   "descriptive"
  Integer, Parameter      :: ibas           =  3       !   "basis"      
  Integer, Parameter      :: iequ           =  4       !   "equatorial" 
  Integer, Parameter      :: ihad           =  5       !   "haDec "     
  Integer, Parameter      :: itru           =  6       !   "trueHorizon"
  Integer, Parameter      :: ihor           =  7       !   "horizontal" 
  Integer, Parameter      :: inas           =  8       !   "Nasmyth"    
  !
  Type :: offsetChoicesType
     Character(len=lenCh) ::  pro                      =   "projection" 
     Character(len=lenCh) ::  des                      =   "descriptive"
     Character(len=lenCh) ::  bas                      =   "basis"      
     Character(len=lenCh) ::  equ                      =   "equatorial" 
     Character(len=lenCh) ::  had                      =   "haDec "     
     Character(len=lenCh) ::  tru                      =   "trueHorizon"
     Character(len=lenCh) ::  hor                      =   "horizontal" 
     Character(len=lenCh) ::  nas                      =   "Nasmyth"    
  End Type offsetChoicesType
  !
  Type (offsetChoicesType), Parameter ::                                          &  !! NCSstd
       &  offs =                                                                  &  !! NCSstd
       &                          offsetChoicesType (      "projection    ",      &  !! NCSstd
       &                                                   "descriptive   ",      &  !! NCSstd
       &                                                   "basis         ",      &  !! NCSstd
       &                                                   "equatorial    ",      &  !! NCSstd
       &                                                   "haDec         ",      &  !! NCSstd
       &                                                   "horizontalTrue",      &  !! NCSstd
       &                                                   "horizontal    ",      &  !! NCSstd
       &                                                   "Nasmyth       ")         !! NCSstd
  !
  Character(len=lenCh), Dimension(nDimOffsetChoices), Parameter ::               &
       &  offsetChoices =                                                        &
       &              (/ offs%pro          ,                                     &
       &                 offs%des          ,                                     &
       &                 offs%bas          ,                                     &
       &                 offs%equ          ,                                     &
       &                 offs%had          ,                                     &
       &                 offs%tru          ,                                     &
       &                 offs%hor          ,                                     &
       &                 offs%nas          /)
  !
  ! The following is a variation to be used in evaluating pako commands:
  !     This is necessary to offer an easy match /distinction between 
  !     trueHorizon and horizontal
  !
  Type (offsetChoicesType), Parameter ::                                         &
       &  offsPako =                                                             &
       &                              offsetChoicesType (  "projection " ,       &
       &                                                   "descriptive" ,       &
       &                                                   "basis      " ,       &
       &                                                   "equatorial " ,       &
       &                                                   "haDec      " ,       &
       &                                                   "trueHorizon" ,       &
       &                                                   "horizontal " ,       &
       &                                                   "Nasmyth    " )
  !
  Character(len=lenCh), Dimension(nDimOffsetChoices), Parameter ::               &
       &  offsetChoicesPako =                                                    &
       &          (/ offsPako%pro          ,                                     &
       &             offsPako%des          ,                                     &
       &             offsPako%bas          ,                                     &
       &             offsPako%equ          ,                                     &
       &             offsPako%had          ,                                     &
       &             offsPako%tru          ,                                     &
       &             offsPako%hor          ,                                     &
       &             offsPako%nas          /)
  !
  !                                                                                  !! NCSstd
  ! *   topology (for azimuth wrap)                                                  !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimTopologyChoices  = 5
  Integer, Parameter      :: ilow           = 1        !   "low"    
  Integer, Parameter      :: ihigh          = 2        !   "high"   
  Integer, Parameter      :: inearest       = 3        !   "nearest"
  Integer, Parameter      :: isouth         = 4        !   "south"  
  Integer, Parameter      :: inorth         = 5        !   "north"  
  !
  Type :: topologyChoicesType
     Character(len=lenCh) ::  low                      =   "low"    
     Character(len=lenCh) ::  high                     =   "high"   
     Character(len=lenCh) ::  nearest                  =   "nearest"
     Character(len=lenCh) ::  south                    =   "south"  
     Character(len=lenCh) ::  north                    =   "north"  
  End Type topologyChoicesType
  !
  Type   (topologyChoicesType), Parameter ::                                      &  !! NCSstd
       &  topo =                                                                  &  !! NCSstd
       &                         topologyChoicesType (     "low    "   ,          &  !! NCSstd   available
       &                                                   "high   "   ,          &  !! NCSstd   available
       &                                                   "nearest"   ,          &  !! NCSstd   available
       &                                                   "south  "   ,          &  !! NCSstd   TBD later
       &                                                   "north  ")                !! NCSstd   TBD later
  !
  Character(len=lenCh), Dimension(nDimTopologyChoices), Parameter ::              &
       &  topologyChoices =                                                       &
       &              (/ topo%low     ,                                           &
       &                 topo%high    ,                                           &
       &                 topo%nearest ,                                           &
       &                 topo%south   ,                                           &
       &                 topo%north   /)
  !
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88
  ! *   az.-el. direction (for Pointing)                                             !! NCSstd
  !                                                                                  !! NCSstd
  Integer, Parameter      :: nDimAzelChoices  = 4
  Integer, Parameter      :: ipa            = 1        !   "+a"     
  Integer, Parameter      :: ima            = 2        !   "-a"     
  Integer, Parameter      :: ipe            = 3        !   "+e"     
  Integer, Parameter      :: ime            = 4        !   "-e"     
  !
  Type :: azelChoicesType
     Character(len=lenCh) ::  pa                       =   "+a"
     Character(len=lenCh) ::  ma                       =   "-a"
     Character(len=lenCh) ::  pe                       =   "+e"
     Character(len=lenCh) ::  me                       =   "-e"
  End Type azelChoicesType
  !
  Type   (azelChoicesType), Parameter ::                                          &  !! NCSstd
       &  azel =                                                                  &  !! NCSstd
       &                         azelChoicesType (         "+a"   ,               &  !! NCSstd   
       &                                                   "-a"   ,               &  !! NCSstd   
       &                                                   "+e"   ,               &  !! NCSstd   
       &                                                   "-e"   )                  !! NCSstd   
  !
  Character(len=lenCh), Dimension(nDimAzelChoices), Parameter ::                  &
       &  azelChoices =                                                           &
       &              (/ azel%pa   ,                                              &
       &                 azel%ma   ,                                              &
       &                 azel%pe   ,                                              &
       &                 azel%me   /)
  !
  ! style: line up in column: 30                            60                       85 88
  !                                                                                  !! NCSstd
  ! *** SET keywords (supported by "SET" command)                                    !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSetChoices = 26
  Integer, Parameter      :: iproject                  =  1  !   "project" 
  Integer, Parameter      :: iPI                       =  2  !   "PI"      
  Integer, Parameter      :: iobserver                 =  3  !   "observer"
  Integer, Parameter      :: ioperator                 =  4  !   "operator"
  Integer, Parameter      :: ilevel                    =  5  !   "level"   
  Integer, Parameter      :: ipointing                 =  6  !   "pointing"
  Integer, Parameter      :: ifocus                    =  7  !   "focus"   
  Integer, Parameter      :: itopology                 =  8  !   "topology"
  Integer, Parameter      :: iAngleUnit                =  9  !   "angleUnit"
  Integer, Parameter      :: iAutomatic                = 10  !   "automatic"
  Integer, Parameter      :: iDoSubmit                 = 11  !   "doSubmit"
  Integer, Parameter      :: iDoDopplerUpdates         = 12  !   "doDopplerUpdates"
  Integer, Parameter      :: isecondaryRotation        = 13  !   "2ndRotation"
  Integer, Parameter      :: iPlotStyle                = 14  !   "plotStyle"
  Integer, Parameter      :: iDoXMLobservingMode       = 15  !   "XMLobservingMode"
  Integer, Parameter      :: iPrivilege                = 16  !   "privilege"
  Integer, Parameter      :: iLimitCheck               = 17  !   "limitCheck"
  Integer, Parameter      :: iEMIRcheck                = 18  !   "EMIRcheck"
  Integer, Parameter      :: imatchSource              = 19  !   "matchSource"
  Integer, Parameter      :: iTransitionAcceleration   = 20  !   "transitionAcceleration"
  Integer, Parameter      :: iSlowRateAMD              = 21  !   "slowRateAMD"
  Integer, Parameter      :: iDoDebugMessages          = 22  !   "doDebugMessages"
  Integer, Parameter      :: iUserLevel                = 23  !   "userLevel"
  Integer, Parameter      :: iTestMode                 = 24  !   "testMode"
  Integer, Parameter      :: iComment                  = 25  !   "comment"
  Integer, Parameter      :: iPurpose                  = 26  !   "purpose"
  !
  Type :: setChoicesType
     Character(len=lench) ::  project                  =   "project" 
     Character(len=lench) ::  PI                       =   "PI"      
     Character(len=lench) ::  observer                 =   "observer"
     Character(len=lench) ::  Operator                 =   "operator"
     Character(len=lench) ::  level                    =   "level"   
     Character(len=lench) ::  pointing                 =   "pointing"
     Character(len=lench) ::  focus                    =   "focus"   
     Character(len=lench) ::  topology                 =   "topology"
     Character(len=lench) ::  angleUnit                =   "angleUnit"
     Character(len=lench) ::  automatic                =   "automatic"
     Character(len=lench) ::  doSubmit                 =   "doSubmit"
     Character(len=lench) ::  doDopplerUpdates         =   "doDopplerUpdates"
     Character(len=lench) ::  secondaryRotation        =   "2ndRotation"
     Character(len=lench) ::  plotStyle                =   "plotStyle"
     Character(len=lench) ::  doXMLobservingMode       =   "doXMLobservingMode"
     Character(len=lench) ::  privilege                =   "privilege"
     Character(len=lench) ::  limitCheck               =   "limitCheck"
     Character(len=lench) ::  EMIRcheck                =   "EMIRcheck"
     Character(len=lench) ::  matchSource              =   "matchSource"
     Character(len=lench) ::  transitionAcceleration   =   "transitionAcceleration"
     Character(len=lench) ::  slowRateAMD              =   "slowRateAMD"
     Character(len=lench) ::  doDebugMessages          =   "doDebugMessages"
     Character(len=lench) ::  userlevel                =   "userLevel"
     Character(len=lench) ::  testMode                 =   "testMode"
     Character(len=lench) ::  comment                  =   "comment"
     Character(len=lench) ::  purpose                  =   "purpose"
  End Type setChoicesType     
  !
  Type (setChoicesType), Parameter ::                                          &  !! NCSstd
       &  set =                                                                &  !! NCSstd
       &                           setChoicesType (  "project               ", &  !! NCSstd
       &                                             "PI                    ", &  !! NCSstd
       &                                             "observer              ", &  !! NCSstd
       &                                             "operator              ", &  !! NCSstd
       &                                             "level                 ", &  !! NCSstd
       &                                             "pointing              ", &  !! NCSstd
       &                                             "focus                 ", &  !! NCSstd
       &                                             "topology              ", &  !! NCSstd
       &                                             "angleUnit             ", &  !! NCSstd
       &                                             "automatic             ", &  !! NCSstd
       &                                             "doSubmit              ", &  !! NCSstd
       &                                             "doDopplerUpdates      ", &  !! NCSstd
       &                                             "2ndRotation           ", &  !! NCSstd
       &                                             "plotStyle             ", &  !! NCSstd
       &                                             "doXMLobservingMode    ", &  !! NCSstd
       &                                             "privilege             ", &  !! NCSstd
       &                                             "limitCheck            ", &  !! NCSstd
       &                                             "EMIRcheck             ", &  !! NCSstd
       &                                             "matchSource           ", &  !! NCSstd
       &                                             "transitionAcceleration", &  !! NCSstd
       &                                             "slowRateAMD           ", &  !! NCSstd
       &                                             "doDebugMessages       ", &  !! NCSstd
       &                                             "userLevel             ", &  !! NCSstd
       &                                             "testMode              ", &  !! NCSstd
       &                                             "comment               ", &  !! NCSstd
       &                                             "purpose               "  &  !! NCSstd
       &                                                               )          !  !! NCSstd to be extended!
  !                                                     
  !
  Character(len=lenCh), Dimension(nDimSetChoices), Parameter ::                   &
       &  setChoices =                                                            &
       &               (/ set%project  ,                                          &
       &                  set%PI       ,                                          &
       &                  set%observer ,                                          &
       &                  set%operator ,                                          &
       &                  set%level    ,                                          &
       &                  set%pointing ,                                          &
       &                  set%focus    ,                                          &
       &                  set%topology ,                                          &
       &                  set%angleUnit,                                          &
       &                  set%automatic,                                          &
       &                  set%doSubmit ,                                          &
       &                  set%doDopplerUpdates  ,                                 &
       &                  set%secondaryRotation ,                                 &
       &                  set%plotStyle         ,                                 &
       &                  set%doXMLobservingMode,                                 &
       &                  set%privilege         ,                                 &
       &                  set%limitCheck        ,                                 &
       &                  set%EMIRcheck         ,                                 &
       &                  set%matchSource       ,                                 &
       &                  set%transitionAcceleration,                             &
       &                  set%slowRateAMD       ,                                 &
       &                  set%doDebugMessages   ,                                 &
       &                  set%userlevel         ,                                 &
       &                  set%testMode          ,                                 &
       &                  set%comment           ,                                 &
       &                  set%purpose                                             &
       &               /)                                                         !
  !
  !                                                                                  !! NCSstd
  ! *** SET plotStyle keywords                                                       !! NCSstd
  !                                                                                  !! NCSstd
  !
  Integer, Parameter      :: nDimSetPlotChoices =  3
  Integer, Parameter      :: ifancy         =  1       !   "fancy"
  Integer, Parameter      :: isimple        =  2       !   "simple"  
  Integer, Parameter      :: ioff           =  3       !   "off"     
  !
  Type :: setPlotChoicesType
     Character(len=lench) ::  fancy                    =   "fancy"
     Character(len=lench) ::  simple                   =   "simple"  
     Character(len=lench) ::  off                      =   "off"     
  End Type setPlotChoicesType
  !
  Type (setPlotChoicesType), Parameter ::                                         &  !! NCSstd
       &  setPlot =                                                               &  !! NCSstd
       &                          setPlotChoicesType (     "fancy "    ,          &  !! NCSstd
       &                                                   "simple"    ,          &  !! NCSstd
       &                                                   "off   "               &  !! NCSstd
       &                                                               )             !! NCSstd 
  !                                                     
  Character(len=lenCh), Dimension(nDimSetPlotChoices), Parameter ::               &
       &  setPlotChoices =                                                        &
       &           (/ setPlot%fancy    ,                                          &
       &              setPlot%simple   ,                                          &
       &              setPlot%off                                                 &
       &               /)
  !
  !                                                                                  !! NCSstd
  ! *** SET privilege keywords                                                       !! NCSstd
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88  !
  Integer, Parameter      :: nDimSetPrivilegeChoices =  8
  Integer, Parameter      :: iuser          =  1       !   "user"
  Integer, Parameter      :: iVLBI          =  2       !   "VLBI"
  Integer, Parameter      :: ibolometerPool =  3       !   "bolometerPool"
  Integer, Parameter      :: ipNIKA         =  4       !   "NIKA"
  Integer, Parameter      :: iprivileged    =  5       !   "privileged"
  Integer, Parameter      :: istaff         =  6       !   "mrtStaff"  
  Integer, Parameter      :: incsTeam       =  7       !   "csTeam"
  Integer, Parameter      :: iHU            =  8       !   "HansUngerechts"
  !
!!$  Integer, Parameter      :: iGISMOpriv     = 7       !   "GISMO"
  !
  Type :: setPrivilegeChoicesType
     Character(len=lench) ::  user                     =   "user"          
     Character(len=lench) ::  VLBI                     =   "VLBI"          
     Character(len=lench) ::  bolometerPool            =   "bolometerPool" 
     Character(len=lench) ::  NIKA                     =   "NIKA"          
     Character(len=lench) ::  privileged               =   "privileged"    
     Character(len=lench) ::  staff                    =   "mrtStaff"      
     Character(len=lench) ::  ncsTeam                  =   "csTeam"        
     Character(len=lench) ::  HU                       =   "HansUngerechts"          
     !
!!$     Character(len=lench) ::  GISMO                   =   "GISMO"
     !
  End Type setPrivilegeChoicesType
  !
  Type (setPrivilegeChoicesType), Parameter ::                                   &   !! NCSstd
       &  setPrivilege =                                                         &   !! NCSstd
       &                       setPrivilegeChoicesType(                          &   !! NCSstd
       &                                                   "user          " ,    &   !! NCSstd
       &                                                   "VLBI          " ,    &   !! NCSstd
       &                                                   "bolometerPool " ,    &   !! NCSstd
       &                                                   "NIKA          " ,    &   !! NCSstd
       &                                                   "privileged    " ,    &   !! NCSstd
       &                                                   "mrtStaff      " ,    &   !! NCSstd
       &                                                   "csTeam        " ,    &   !! NCSstd
       &                                                   "HansUngerechts"      &   
       &                                              )                          !   !! NCSstd
  !
!!$       &                            "GISMO"               &  !! NCSstd
  !                                
  Character(len=lenCh), Dimension(nDimSetPrivilegeChoices), Parameter ::         &
       &  setPrivilegeChoices =                                                  &
       &      (/ setPrivilege%user           ,                                   &
       &         setPrivilege%VLBI           ,                                   &        
       &         setPrivilege%bolometerPool  ,                                   &
       &         setPrivilege%NIKA           ,                                   &
       &         setPrivilege%privileged     ,                                   &
       &         setPrivilege%staff          ,                                   &
       &         setPrivilege%ncsTeam        ,                                   &
       &         setPrivilege%HU                                                 &
       &      /)                                                                 !
  !
!!$       &              setPrivilege%GISMO                                          &
  !
  !
  !                                                                                  !! NCSstd
  ! *** SET userLevel keywords                                                       !! NCSstd
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88  !
  Integer, Parameter      :: nDimSetUserLevelChoices =  6
  Integer, Parameter      :: ivirgenExtra  =  1         !   "virgenExtra"    
  Integer, Parameter      :: ibeginner     =  2         !   "beginner"       
  Integer, Parameter      :: inormal       =  3         !   "normal"          
  Integer, Parameter      :: iexperienced  =  4         !   "experienced"    
  Integer, Parameter      :: iguru         =  5         !   "guru"             
  Integer, Parameter      :: iprimaDonna   =  6         !   "primaDonna"     
  !
  Type :: setUserLevelChoicesType
     Character(len=lench) ::  virgenExtra               =   "virgenExtra"
     Character(len=lench) ::  beginner                  =   "beginner"  
     Character(len=lench) ::  normal                    =   "normal"     
     Character(len=lench) ::  experienced               =   "experienced"
     Character(len=lench) ::  guru                      =   "guru"       
     Character(len=lench) ::  primaDonna                =   "primaDonna" 
  End Type setUserLevelChoicesType
  !
  Type (setUserLevelChoicesType), Parameter ::                                   &   !! NCSstd
       &  setUserLevel =                                                         &   !! NCSstd
       &                          setUserLevelChoicesType (                      &   !! NCSstd
       &                                                    "virgenExtra"  ,     &   !! 
       &                                                    "beginner   "  ,     &   !! NCSstd
       &                                                    "normal     "  ,     &   !! NCSstd
       &                                                    "experienced"  ,     &   !! NCSstd
       &                                                    "guru       "  ,     &   !! NCSstd
       &                                                    "primaDonna "        &           
       &                                                               )         !   !! NCSstd 
  !                                                     
  Character(len=lenCh), Dimension(nDimSetUserLevelChoices), Parameter ::         &
       &  setUserLevelChoices =                                                  &
       &           (/ setUserLevel%virgenExtra ,                                 &
       &              setUserLevel%beginner    ,                                 &        
       &              setUserLevel%normal      ,                                 &
       &              setUserLevel%experienced ,                                 &
       &              setUserLevel%guru        ,                                 &
       &              setUserLevel%primaDonna                                    &
       &               /)                                                        !
  !
  !                                                                                  !! NCSstd
  ! *** SET limitCheck keywords                                                      !! NCSstd
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88  !
  Integer, Parameter      :: nDimSetLimitCheckChoices =  5
  Integer, Parameter      :: ianal         =  1         !  "anal"   
  Integer, Parameter      :: istrict       =  2         !  "strict" 
  Integer, Parameter      :: irelaxed      =  3         !  "relaxed"
  Integer, Parameter      :: iloose        =  4         !  "loose"  
  Integer, Parameter      :: itest         =  5         !  "test "  
  !
  Type :: setLimitCheckChoicesType
     Character(len=lench) ::  anal                      =  "anal"    
     Character(len=lench) ::  strict                    =  "strict"  
     Character(len=lench) ::  relaxed                   =  "relaxed" 
     Character(len=lench) ::  loose                     =  "loose"  
     Character(len=lench) ::  test                      =  "test "  
  End Type setLimitCheckChoicesType
  !
  Type (setLimitCheckChoicesType), Parameter ::                                  &  !! NCSstd
       &  setLimitCheck =                                                        &  !! NCSstd
       &                         setLimitCheckChoicesType ("anal   "   ,         &  
       &                                                   "strict "   ,         &  !! NCSstd
       &                                                   "relaxed"   ,         &  !! NCSstd
       &                                                   "loose  "   ,         &  !! NCSstd
       &                                                   "test   "   )         !  !! NCSstd 
  !                                                     
  Character(len=lenCh), Dimension(nDimSetLimitCheckChoices), Parameter ::        &
       &  setLimitCheckChoices =                                                 &
       &           (/ setLimitCheck%anal     ,                                   &
       &              setLimitCheck%strict   ,                                   &        
       &              setLimitCheck%relaxed  ,                                   &
       &              setLimitCheck%loose    ,                                   &
       &              setLimitCheck%test                                         &
       &               /)                                                        !
  !
  !                                                                                  !! NCSstd
  ! *** SET matchSource keywords                                                     !! NCSstd
  !                                                                                  !! NCSstd
  ! style: line up in column: 30                            60                       85 88  !
  Integer, Parameter      :: nDimSetMatchSourceChoices =  3
  Integer, Parameter      :: iExact        =  1         !  "exact" 
  Integer, Parameter      :: iStart        =  2         !  "start"
  Integer, Parameter      :: iMatch        =  3         !  "match"  
  !
  Type :: setMatchSourceChoicesType
     Character(len=lench) ::  Exact                     =  "exact" 
     Character(len=lench) ::  Start                     =  "start"
     Character(len=lench) ::  Match                     =  "match"  
  End Type setMatchSourceChoicesType
  !
  Type (setMatchSourceChoicesType), Parameter ::                                 &  !! NCSstd
       &  setMatchSource =                                                       &  !! NCSstd
       &                       setMatchSourceChoicesType ( "exact"    ,          &  !! NCSstd
       &                                                   "start"   ,           &  !! NCSstd
       &                                                   "match"     )         !  !! NCSstd 
  !                                                     
  Character(len=lenCh), Dimension(nDimSetMatchSourceChoices), Parameter ::       &
       &  setMatchSourceChoices =                                                &
       &           (/ setMatchSource%Exact  ,                                    &
       &              setMatchSource%Start  ,                                    &
       &              setMatchSource%Match                                       &
       &               /)                                                        !
  !
  ! ***
  Integer :: ii
!!!
!!! >>> TBD: the rest down to <<< is deprecated or obsolete
  Integer, Parameter :: nDimSegmentTypes = 4
  Character(len=lenCh),  Dimension(nDimSegmentTypes)                      &
       &                                   :: segmentTypeChoices="none"
  !
  Integer, Parameter :: nDimSystemNameChoices = 7
  Character(len=lenCh),  Dimension(nDimSystemNameChoices)                 &
       &                                   :: systemNameChoices="none"
  !
  Character(len=lenCh),  Dimension(nDimOffsetChoices)                     &
       &     ::  offsetSystemChoices="none"
  Integer, Parameter :: iPROJECTION   = 1
  Integer, Parameter :: iDESCRIPTIVE  = 2
  Integer, Parameter :: iBASIS        = 3
  Integer, Parameter :: iEQUATORIALobsolete   = 4
  Integer, Parameter :: iHADECobsolete        = 5
  Integer, Parameter :: iHORIZONTRUE  = 6
  Integer, Parameter :: iHORIZON      = 7
  Integer, Parameter :: iNASMYTH      = 8
  !
!!! <<<
!!!
!!!
Contains
!!!
!!!
  Subroutine printAngleChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimAngleChoices, 1
       Write (6,*) "    angleChoices   ( ",ii,") = ", angleChoices(ii)
    End Do
    !
  End Subroutine printAngleChoices
!!!
!!!
  Subroutine printSpeedChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimSpeedChoices, 1
       Write (6,*) "    speedChoices   ( ",ii,") = ", speedChoices(ii)
    End Do
    !
  End Subroutine printSpeedChoices
!!!
!!!
  Subroutine printReceiverChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimReceiverChoices, 1
       Write (6,*) " receiverChoices   ( ",ii,") = ", receiverChoices(ii)
    End Do
    !
    Write (6,*) " "
    Do ii = 1, nDimBolometerChoices, 1
       Write (6,*) "bolometerChoices   ( ",ii,") = ", bolometerChoices(ii)
    End Do
    !
    Write (6,*) " "
    Do ii = 1, nDimSidebandChoices, 1
       Write (6,*) " sidebandChoices   ( ",ii,") = ", sidebandChoices(ii)
    End Do
    !
    Write (6,*) " "
    Do ii = 1, nDimWidthChoices, 1
       Write (6,*) "    widthChoices   ( ",ii,") = ", widthChoices(ii)
    End Do
    !
    Write (6,*) " "
    Do ii = 1, nDimScaleChoices, 1
       Write (6,*) "    scaleChoices   ( ",ii,") = ", scaleChoices(ii)
    End Do
    !
    Write (6,*) " "
    Do ii = 1, nDimDopplerChoices, 1
       Write (6,*) "  dopplerChoices   ( ",ii,") = ", dopplerChoices(ii)
    End Do
    !
  End Subroutine printReceiverChoices
!!!
!!!
  Subroutine printOffsetChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimOffsetChoices, 1
       Write (6,*) "   offsetChoices   ( ",ii,") = ", offsetChoices(ii)
    End Do
    !
  End Subroutine printOffsetChoices
!!!
!!!
  Subroutine printTopologyChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimTopologyChoices, 1
       Write (6,*) "   topologyChoices ( ",ii,") = ", topologyChoices(ii)
    End Do
    !
  End Subroutine printTopologyChoices
!!!
!!!
  Subroutine printSubscanChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimSubscanChoices, 1
       Write (6,*) "   subscanChoices  ( ",ii,") = ", subscanChoices(ii)
    End Do
    !
  End Subroutine printSubscanChoices
!!!
!!!
  Subroutine printSegmentChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimSegmentChoices, 1
       Write (6,*) "   segmentChoices  ( ",ii,") = ", segmentChoices(ii)
    End Do
    !
  End Subroutine printSegmentChoices
!!!
!!!
  Subroutine printSetChoices
    !
    Write (6,*) " "
    Do ii = 1, nDimSetChoices, 1
       Write (6,*) "       setChoices  ( ",ii,") = ", setChoices(ii)
    End Do
    !
  End Subroutine printSetChoices
!!!
!!!
  Subroutine initializeReceiverChoices
    ! TBD: SR IS OBSOLETE
    !
!!$    receiverNameChoices(iA100)       =  "A100"      
!!$    receiverNameChoices(iA230)       =  "A230"      
!!$    receiverNameChoices(iB100)       =  "B100"      
!!$    receiverNameChoices(iB230)       =  "B230"      
!!$    receiverNameChoices(iC150)       =  "C150"      
!!$    receiverNameChoices(iC270)       =  "C270"      
!!$    receiverNameChoices(iD150)       =  "D150"      
!!$    receiverNameChoices(iD270)       =  "D270"      
!!$    receiverNameChoices(iBOLO)       =  "BOLOMETER" 
!!$    receiverNameChoices(iHERA)       =  "HERA"      
!!$    receiverNameChoices(iVERTICAL)   =  "VERTICAL"  
!!$    receiverNameChoices(iHORIZONTAL) =  "HORIZONTAL"
!!$    !
!!$    bolometerNameChoices(i37)        =  "37"
!!$    bolometerNameChoices(i117)       =  "117"
!!$    !
!!$    dopplerChoices(1)     =  "DOPPLER"
!!$    dopplerChoices(2)     =  "FIXED"
!!$    !
!!$    scaleChoices(1)       =  "ANTENNA"
!!$    scaleChoices(2)       =  "BEAM"
!!$    !
!!$    widthChoices(1)       =  "NARROW"
!!$    widthChoices(2)       =  "WIDE"
    !
  End Subroutine initializeReceiverChoices
!!!
!!!
  Subroutine initializeSystemNameChoices
    ! TBD: SR IS DEPRECATED
    !
    !D          Write (6,*) "   moduleGlobalParameters: SR initializeSystemNameChoices"
    !
    ! TBD: SYSTEMNAMECHOICES IS OBSOLETE
    systemNameChoices(1)    = "PROJECTION"
    systemNameChoices(2)    = "DESCRIPTIVE"
    systemNameChoices(3)    = "BASIS"
    systemNameChoices(4)    = "EQUATORIAL"
    systemNameChoices(5)    = "HA/DECL"
    systemNameChoices(6)    = "HORIZONTAL"
    systemNameChoices(7)    = "NASMYTH"
    !
    ! TBD: OFFSETSYSTEMNAMECHOICES IS OBSOLETE
    offsetSystemChoices(iPROJECTION ) = "PROJECTION" 
    offsetSystemChoices(iDESCRIPTIVE) = "DESCRIPTIVE"
    offsetSystemChoices(iBASIS      ) = "BASIS"      
    offsetSystemChoices(iEQUATORIAL ) = "EQUATORIAL" 
    offsetSystemChoices(iHADEC      ) = "HADEC "     
    offsetSystemChoices(iHORIZONTRUE) = "TRUEHORIZON"
    offsetSystemChoices(iHORIZON    ) = "HORIZONTAL" 
    offsetSystemChoices(iNASMYTH    ) = "NASMYTH"    
    !
    !D          do ii = 1, nDimOffsets, 1
    !D             Write (6,*) ii, offsetSystemChoices(ii)
    !D          end do
    !
  End Subroutine initializeSystemNameChoices
!!!
!!!
End Module modulePakoGlobalParameters





