!     
!  Id: modulePakoStop.f90 ,v 1.2.3 2014-02-17 Hans Ungerechts
!  Id: modulePakoStop.f90 ,v 0.9   2005-09-13 Hans Ungerechts
!    
! <DOCUMENTATION name="moduleStop">
!
! handle Stop command.
!
! </DOCUMENTATION>
!
! <DEV> 
! TBD: - more documentation
! <\DEV> 
!
Module modulePakoStop
  !
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  Use modulePakoCalibrate
  Use modulePakoFocus
  Use modulePakoOnOff
  Use modulePakoOtfMap
  Use modulePakoPointing
!!$  Use modulePakoSubscanList
  Use modulePakoTip
  Use modulePakoTrack
  Use modulePakoVlbi
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  ! *** Subroutines
  !
  Public :: pakoStop
  !
!!!
!!!
Contains
!!!
!!!
  Subroutine pakoStop(programName,LINE,COMMAND,ERROR)
    !
    !**   arguments:   ***
    Character(len=lenPro),        Intent(in)    ::  programName
    Character(len=lenLineDouble), Intent(in)    ::  LINE
    Character(len=lenCo),         Intent(in)    ::  COMMAND
    Logical,                      Intent(out)   ::  ERROR
    !
    !**   specific local variables:    ***
    Logical   ::  doCancel  = GPnoneL
    !
    Character(len=lenLineDouble)  ::  shellCommand
    Integer                       ::  ier
    !
    !**   functions called:   ***
    Logical  ::  SIC_PRESENT
    Integer  ::  System
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Error = .False.
    !
    errorL = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Stop, v 1.2.3 2014-02-03 ---> SR: Stop ")                   ! trace execution
    !
    !**   read Parameters, Options   ***
    !
    !     Include 'inc/parameters/parametersCommandToStop.inc'
    Include 'inc/options/optionCancel.inc'
    !TBD: Include 'inc/options/optionHalt.inc'
    !
    If (GV%doDebugMessages) Then
       Write (6,*) "   --> moduleStop: SR pakoStop "
       Write (6,*) "                   doCancel:   ", doCancel
    End If
    !
    If (doCancel) Then
       !
       shellCommand = 'ncsMessageSend.py sync '                                  &
            &       //' control:cancelScan:on > /dev/null  2>&1 '                !  !! to work
       !D           //'    test:cancelScan:on > /dev/null  2>&1 '                !  !! for tests
       If (GV%doDebugMessages) Then
          Write (6,*)   shellCommand(1:Len_trim(shellCommand))
          Write (messageText,*) ' shellCommand: '                                &
               &                //shellCommand(1:Len_trim(shellCommand))         !
          Call pakoMessage(priorityI,severityI,command,messageText)
       End If
       ier = system(shellCommand)
       !D ier = 1
       !
       If (ier.Eq.0) Then
          Write (messageText,*)                                                  &
               &           'CANCEL was sent to Coordinator.'                     !
          Call pakoMessage(priorityW,severityW,command,messageText)
       Else
          Error = .True.
          Write (messageText,*)                                                  &
               &           'ERROR', ier,                                         &
               &           ' shellCommand: '                                     &
               &           //shellCommand(1:Len_trim(shellCommand))              ! 
          Call pakoMessage(priorityE,severityE,command,messageText)
       End If
       !
    Else
       !
       Error = .True.
       Write (messageText,*)                                                     &
            &           "paKo doesn't know yet how to simply stop"               !
       Call pakoMessage(priorityE,severityE,command,messageText)
!!$       Write (messageText,*)                                                  &
!!$            &           "maybe you want to cancel -- Usage: stop /cancel"     !
!!$       Call pakoMessage(priorityI,severityI,command,messageText)
       !
    End If
    !
    Return
    !
  End Subroutine pakoStop
!!!
!!!
End Module modulePakoStop



