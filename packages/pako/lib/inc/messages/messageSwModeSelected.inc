!
!  messageSwModeSelected.inc, v1.0.5 2006-10-16 Hans Ungerechts
!
Write (messageText,*)                                                     & 
     &         'Switching Mode selected: ',                               &
     &         GV%switchingMode,                                          &
     &         ' '
Call pakoMessage(priorityI,severityI,command,messageText)
valueText = GV%observingModePako
Call SIC_UPPER(valueText)
Write (messageText,*)                                                     & 
     &         'After Switching Mode, please specify (again) ',           &
     &         ' Observing Mode, e.g., ',                                 &
     &         valueText,                                                 &
     &         ' '
Call pakoMessage(priorityW,severityW,command,messageText)
!

