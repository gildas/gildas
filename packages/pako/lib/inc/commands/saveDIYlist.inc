!
!  Id: saveDIYlist.inc,v 1.1.13 2012-10-18 Hans Ungerechts
!
contC = contNN
!
B = '\'   ! '
S = ' '
CMD =  programName//B//"DIY"
lCMD = lenc(CMD)
!
ERROR = .False.
!
Write (iUnit,*) "! "
Write (iUnit,*) "! ", CMD(1:lCMD)
Write (iUnit,*) CMD(1:lCMD), " -"
!
contC = contCN
!
lineOut = " /clear"
Include 'inc/options/caseContinuationLine.inc'
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
Call getCountSegments(nSegments)
!
If (nSegments.Ge.1) Then
   !
   Do ii = 1, nSegments, 1
      !
      !D           Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType
      !
      CMD =  programName//B//"SUBSCAN"
      lCMD = lenc(CMD)
      !
      If      (.Not. segList(ii)%flagDropin) Then
         !
         Write (iUnit,*) "! "
         Write (iUnit,*) CMD(1:lCMD), " -"
         !
      Else
         !
         Write (messageText,*) 'skip "drop-in" segment ',ii,' (not saved)' 
         If (GV%doDebugMessages) Then
            Call PakoMessage(priorityI,severityI,'Save DIY',messageText)
         End If
         !
      End If
      !
      If      (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%otf                           &
           &   .And. segList(ii)%segType.Eq.seg%linear) Then                 !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveStartEndDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTotfDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      Else If (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%otf                           &
           &   .And. segList(ii)%segType.Eq.seg%lissajous) Then              !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveLissajousDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         !
         ! TBD: more elegant solution for ramps --> include info in subscanlist
         If      (ii.Ge.2) Then
            If(segList(ii-1)%flagDropin) Then
               Write (lineOut,*) "/ramp up ", segList(ii-1)%tSegment
               Include 'inc/options/caseContinuationLine.inc'
               Write (iUnit,*) lineOut(1:lenc(lineOut))
            Else
               Write (lineOut,*) "/ramp no "
               Include 'inc/options/caseContinuationLine.inc'
               Write (iUnit,*) lineOut(1:lenc(lineOut))
            End If
         Else
               Write (lineOut,*) "/ramp no "
               Include 'inc/options/caseContinuationLine.inc'
               Write (iUnit,*) lineOut(1:lenc(lineOut))
         End If
         !
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTotfDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      Else If (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%track                         &
           &   .And. segList(ii)%segType.Eq.seg%track) Then                  !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveOffsetsDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTsubscanDIY.inc'
         Include 'inc/options/saveTuneDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      End If
      !
   End Do
   !
Else
   !
   Write (messageText,*) '   list of subscans and segments is empty.'
   Call PakoMessage(priorityW,severityW,'Save DIY',messageText)
   !
End If
!
CMD =  programName//B//"DIY"
lCMD = lenc(CMD)
!
Write (iUnit,*) "! "
Write (iUnit,*) CMD(1:lCMD)
!
Write (iUnit,*) "!"
!
