!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="pakoVocab">
!   <VERSION>
!                Id: pakoVocab.inc,   v 1.3.0  2017-08-24 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.3  2014-07-17 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.3  2014-07-03 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.3  2014-05-29 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.3  2014-05-27 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.3  2014-02-27 Hans Ungerechts            
!                Id: pakoVocab.inc,   v 1.2.2  2013-01-22 Hans Ungerechts            
!                based on:            v 1.1.13 2012-10-29 Hans Ungerechts            
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                
!   </FAMILY>
!
!   vocabulary for pako
!
! </DOCUMENTATION> <!-- name="poakoVocab" -->
!
!----------------------------------------------------------------------
! 
      INTEGER MCOM
      PARAMETER (MCOM=198)
      CHARACTER*12 VOCAB(MCOM)
      DATA VOCAB /                                                               &
!**   *' xxxxxxxxxxx',                                                             !! 
!**   *              111            111            111            111              !! 
!**   *     123456789012   123456789012   123456789012   123456789012              !! 
!**   *    '/1          ','/2          ','/3          ','/4          ',            !! 
     & ' BACKEND'    ,                                                           &
     &     '/DEFAULTS',                                                          &
     &     '/CLEAR',                                                             &
     &     '/CONNECT',    '/DISCONNECT',                                         &
     &     '/FINEFTS',                                                           &
     &     '/LINENAME',                                                          &
     &     '/MODE',       '/PERCENTAGE',                                         &
     &     '/RECEIVER',                                                          &
     &     '/NSAMPLES',                                                          &
!**   2 +  9 = 11                                                                  !!  
     & ' CALIBRATE'  ,                                                           &
     &     '/DEFAULTS',                                                          &
     &     '/AMBIENT',    '/COLD',       '/GRID',       '/FAST',                 &
     &     '/NULL',       '/GAINIMAGE',                                          &
     &     '/SKY',        '/SYSTEM',     '/TEST',       '/TCALIBRATE',           &
     &     '/VARIABLE',                                                          &
!**   2 + 11 = 13                                                                  !! 
     & ' CATALOG'     ,                                                          &
     &     '/DEFAULTS',                                                          &
!**   2 +  0 =  2                                                                  !! 
     & ' DISPLAY'     ,                                                          &
     &     '/DEFAULTS',                                                          &
!**   2 +  0 =  2                                                                  !! 
     & ' DIYLIST',                                                               &
     &     '/DEFAULTS',                                                          &
     &     '/CLEAR',      '/PURPOSE',    '/TRECORD',                             &
!**   2 +  3 =  5                                                                  !! 
     & ' FOCUS',                                                                 &
     &     '/DEFAULTS',                                                          &
     &     '/CALIBRATE',  '/DIRECTION',  '/FEBE',       '/NSUBSCANS',            &
     &     '/OTFFOCUS',   '/TSUBSCAN',                                           &
     &     '/TUNE', '/TTUNE',                                                    &
     &     '/UPDATE',                                                            &
!**   2 +  9 =  11                                                                  !! 
     & ' LISSAJOUS',                                                             &
     &     '/DEFAULTS',                                                          &
     &     '/CENTER',     '/CROLOOP',    '/FREQUENCY',                           &
     &     '/FOCUS',      '/POINTING',                                           &
     &     '/PHASES',     '/RAMP',       '/REFERENCE',  '/SYSTEM',               &
     &     '/TIMEPEROTF', '/TOTF',       '/TREFERENCE',                          &
     &     '/TRECORD',                                                           &
     &     '/TUNE', '/TTUNE',                                                    &
!**   2 + 15 = 17                                                                  !! 
     & ' OFFSETS',                                                               &
     &     '/DEFAULTS',                                                          &
     &     '/CHANNEL',    '/CLEAR',      '/ELEVATION',                           &
     &     '/SYSTEM',                                                            &
!**   2 +  3 =  6                                                                  !! 
     & ' ONOFF',                                                                 &
     &     '/DEFAULTS',                                                          &
     &     '/BALANCE',    '/CALIBRATE',  '/NSUBSCANS',   '/REFERENCE',           &
     &     '/SYMMETRIC',                                                         &
     &     '/SYSTEM',     '/TRECORD',    '/TSUBSCAN',    '/SWWOBBLER',           &
!**   2 +  9 = 11                                                                  !! 
     & ' OTFMAP',                                                                &
     &     '/DEFAULTS',                                                          &
     &     '/BALANCE',    '/CROLOOP',    '/NUMBEROFOTF', '/NOTF',                &
     &     '/REFERENCE',  '/RESUME',     '/STEP',        '/SYSTEM',              &
     &     '/SPEED',      '/TIMEPEROTF', '/TOTF',        '/TREFERENCE',          &
     &     '/TRECORD',    '/ZIGZAG',                                             &
     &     '/TUNE', '/TTUNE',                                                    &
!**   2 + 16 = 18                                                                  !! 
     & ' POINTING',                                                              &
     &     '/DEFAULTS',                                                          &
     &     '/AELOOP',                                                            &
     &     '/CALIBRATE',                                                         &
     &     '/DOUBLEBEAM',                                                        &
     &     '/FEBE',       '/MORE',       '/NUMBEROFOTF',                         &
     &     '/NOTF',       '/TIMEPEROTF', '/TOTF',        '/TRECORD',             &
     &     '/TUNE', '/TTUNE',                                                    &
     &     '/UPDATE',                                                            &
!**   2 + 13 = 15                                                                  !! 
     & ' RECEIVER',                                                              &
     &     '/DEFAULTS',                                                          &
     &     '/CLEAR',                                                             &
     &     '/CENTERIF',   '/CONNECT',    '/DEROTATOR',                           &
     &     '/DISCONNECT', '/DOPPLER',    '/EFFICIENCY',                          &
     &     '/FOFFSET',                                                           &
     &     '/SBAND',      '/HORIZONTAL', '/VERTICAL',                            &
     &     '/GAINIMAGE',  '/SCALE',      '/TEMPLOAD',   '/WIDTH',                &
!**   2 + 15 = 17                                                                  !! 
     & ' SAVE',                                                                  &
     &     '/DEFAULTS',                                                          &
     &     '/FILE',       '/APPEND',                                             &
!**   2 +  2 =  4                                                                  !! 
     & ' SET',                                                                   &
     &    '/DEFAULTS',                                                           &
     &    '/DIRECTION',                                                          &
!**   2 +  1 =  3                                                                  !! 
     & ' SHOW',                                                                  &
     &     '/DEFAULTS',                                                          &
!**   2 +    =  2                                                                  !! 
     & ' SOURCE',                                                                &
     &     '/DEFAULTS',                                                          &
     &     '/CATALOG',                                                           &
     &     '/TYPE',       '/VELOCITY',  '/FLUX',        '/GREP',                 &
!**   2 +  5 =  7                                                                  !! 
     & ' START',                                                                 &
!**   1 +  0 =  1                                                                  !! 
     & ' STOP',                                                                  &
     &     '/CANCEL',     '/HALT',                                               &
!**   1 +  2 =  3                                                                  !! 
     & ' SUBSCAN',                                                               &
     &     '/DEFAULTS',                                                          &
     &     '/CROFLAG',    '/RAMP',       '/SYSTEM',     '/TSUBSCAN',             &
     &     '/SPEED',      '/TIMEPEROTF', '/TOTF',                                &
     &     '/TUNE',                                                              &
     &     '/NEXT',                                                              &
     &     '/TYPE',                                                              &
!**   2 + 10 = 12                                                                  !! 
     & ' SWBEAM',                                                                &
     &     '/DEFAULTS',                                                          &
     &     '/NCYCLES',                                                           &
!**   2 +  1 =  3                                                                  !! 
     & ' SWFREQUENCY',                                                           &
     &     '/DEFAULTS',                                                          &
     &     '/NCYCLES',                                                           &
     &     '/NPHASES',    '/RECEIVER',  '/FUNCTION',    '/TPHASE',               &
     &     '/TBLANKING',                                                         &
!**   2 +  6 =  8                                                                  !! 
     & ' SWTOTAL',                                                               &
     &     '/DEFAULTS',                                                          &
     &     '/NCYCLES',                                                           &
     &     '/TPHASE',                                                            &
!**   2 +  2 =  4                                                                  !! 
     & ' SWWOBBLER',                                                             &
     &     '/DEFAULTS',                                                          &
     &     '/ANGLE',                                                             &
!           /angle is obsolete, see: SET 2ndRotation                               !! 
     &     '/NCYCLES',                                                           &
     &     '/TPHASE',                                                            &
!**   2 +  3 =  5                                                                  !! 
     & ' TIP',                                                                   &
     &     '/DEFAULTS',                                                          &
     &     '/AIRMASS',    '/SLEW',       '/TPADDLE',    '/TSUBSCAN',             &
     &     '/TUNE', '/TTUNE',                                                    &
!**   2 +  6 =  8                                                                  !! 
     & ' TRACK',                                                                 &
     &     '/DEFAULTS',                                                          &
     &     '/NSUBSCANS',                                                         &
     &     '/SYSTEM',     '/TSUBSCAN',   '/TRECORD',                             &
!**   2 +  4 =  6                                                                  !! 
     & ' VLBI',                                                                  &
     &     '/DEFAULTS',                                                          &
     &     '/LASTSOURCE',                                                        &
!**   2 +  1 =  3                                                                  !! 
     & ' OPTIONS'                                                                &
     &     /                                                                     !
!**        1 =  1                                                                  !!
!                                                                                  !!
!**   total  = 198                                                                 !!
!                                                                                  !!
