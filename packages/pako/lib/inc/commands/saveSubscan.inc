!
!  Id: saveSubscan.inc,v 1.1.13 2012-10-18 Hans Ungerechts
!
contC = contNN
!
B = '\'   ! '
S = ' '
CMD =  programName//B//"SUBSCAN"
lCMD = lenc(CMD)
!
ERROR = .False.
!
contC = contCN
!
Call getCountSegments(nSegments)
!
If (nSegments.Ge.1) Then
   !
   Do ii = nSegments, nSegments, 1
      !
      !D      Write (6,*) ii, segList(ii)%ssType, segList(ii)%segType
      !
      CMD =  programName//B//"SUBSCAN"
      Write (iUnit,*) "! ", CMD(1:lCMD)
      lCMD = lenc(CMD)
      !
      If      (.Not. segList(ii)%flagDropin) Then
         !
         Write (iUnit,*) "! "
         Write (iUnit,*) CMD(1:lCMD), " -"
         !
      Else
         !
         Write (messageText,*) 'skip "drop-in" segment ',ii,' (not saved)' 
         !!   Call PakoMessage(priorityI,severityI,'Save DIY',messageText)
         !
      End If
      !
      If      (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%otf                           &
           &   .And. segList(ii)%segType.Eq.seg%linear) Then                 !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveStartEndDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTotfDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      Else If (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%otf                           &
           &   .And. segList(ii)%segType.Eq.seg%lissajous) Then              !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveLissajousDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         Include 'inc/options/saveRampDIY.inc'
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTotfDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      Else If (.Not. segList(ii)%flagDropin                                  &
           &   .And. segList(ii)%ssType .Eq.ss%track                         &
           &   .And. segList(ii)%segType.Eq.seg%track) Then                  !
         !
         contC = contCC
         !
         Include 'inc/parameters/saveOffsetsDIY.inc'
         Include 'inc/options/saveCroFlagDIY.inc'
         Include 'inc/options/saveSystemDIY.inc'
         Include 'inc/options/saveTsubscanDIY.inc'
         Include 'inc/options/saveTuneDIY.inc'
         !
         contC = contCN
         !
         Include 'inc/options/saveTypeDIY.inc'
         !
      End If
      !
   End Do
   !
Else
   !
   Write (messageText,*) '   list of subscans and segments is empty.'
   Call PakoMessage(priorityW,severityW,'Save DIY',messageText)
   !
End If
!
