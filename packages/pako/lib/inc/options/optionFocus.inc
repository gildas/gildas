  !
  !     Id: optionFocus.inc,v 1.2.3   2014-09-25 Hans Ungerechts
  !     Id: optionFocus.inc,v 1.2.3   2014-07-08 Hans Ungerechts
  !
  !     Family:    option list of values
  !     Cousin:    optionAirmass
  !
  !D   Write (6,*) '      ->  optionFocus.f90 v 1.2.3   2014-09-25'
  !
  !D   Write (6,*) "     limits(iStd1)   %focus:     ", limits(iStd1)   %focus
  !D   Write (6,*) "     limits(iStd2)   %focus:     ", limits(iStd2)   %focus
  !D   Write (6,*) "     limits(iLimit1) %focus:     ", limits(iLimit1)   %focus
  !D   Write (6,*) "     limits(iLimit2) %focus:     ", limits(iLimit2)   %focus
  !
  Option = 'FOCUS'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  If ( SIC_PRESENT(iOption,0) ) Then
     !
     !D      Write (6,*) " ( SIC_PRESENT(iOption,0) )  "
     !
     errorR            = .False.
     errorInconsistent = .False.
     !
     vars(iIn)%doFocus  =  .False.
     vars(iIn)%nFocus   =  0
     !
     nPar  =  sic_narg(iOption)
     !
     !D   Write (6,*) '      NARG:   ', sic_narg(iOption)
     !
     If (sic_narg(iOption) .Ge. 1) Then
        !
        !D         Write (6,*) " (sic_narg(iOption) .Ge. 1)  "
        !
        vars(iIn)%nFocus = 0
        !
        vars(iIn)%doFocus = .True.
        !
        If ( sic_narg(iOption).Eq.1 ) Then
           !
           !D            Write (6,*) " ( sic_narg(iOption).Eq.1 )  "
           !
           iArgument = 1
           Call SIC_L4(LINE,iOption,iArgument,                                   &
              &           vars(iIn)%doFocus,.False.,errorL)                      !
           !
        End If
        !
        !D         Write (6,*) "      vars(iIn)%doFocus:   ", vars(iIn)%doFocus
        !
        If ( vars(iIn)%doFocus ) Then
           !
           !D            Write (6,*) " ( vars(iIn)%doFocus)  "
           !
           If ( sic_narg(iOption).Ge.2 ) Then
              !
              !D               Write (6,*) " ( sic_narg(iOption).Ge.2 )  "
              !D               Write (6,*) "      try last argument for direction  "
              !
              iArgument = sic_narg(iOption)
              Call SIC_CH(LINE,iOption,iArgument,                                   & 
                   &           cInput,lengthInput,.True.,errorC)                    !
              !
              Call SIC_UPPER(cInput)
              !
              If (         cInput(1:1).EQ."X"                                       &
                   &  .Or. cInput(1:1).EQ."Y"                                       &
                   &  .Or. cInput(1:1).EQ."Z" ) Then                                ! !! TBD NIKA 2014: use standard choice mechanism
                 vars(iIn)%directionFocus = cInput(1:1)
                 nPar = nPar-1
              Else
                 vars(iIn)%directionFocus  =  "Z"
              End If
              !
              !D         Write (6,*) "      nPar:    ", nPar
              !D         Write (6,*) "      vars(iIn)%directionFocus(1:1):   -->",                &
              !D              &             vars(iIn)%directionFocus(1:1), "<--"                  !
              !
           End If
           !
           If ( sic_narg(iOption).Ge.2 .And. nPar.Le.nFocusMax ) Then
              !
              !D               Write (6,*) " ( sic_narg(iOption).Ge.2 .And. nPar.Le.nFocusMax )  "
              !
              Do ii = 1, nPar, 1
                 !
                 !D                  Write (6,*) "      try argument # ", ii
                 !
                 iArgument = ii
                 !
                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)        ! 
                 !
                 If (.Not. errorR) Then                                          !   !!   TBD 2014: abstract out range checks
                    !
                    If       (   (       limits(iLimit1)%focus.Le.rInput         &
                         &         .And. limits(iLimit2)%focus.Ge.rInput         &
                         &       )                                               &
                         &  .Or. (       GV%limitCheck.Eq.setLimitCheck%relaxed  &
                         &         .And. limits(iLimit1)%focus-10.0.Le.rInput    &
                         &         .And. limits(iLimit2)%focus+10.0.Ge.rInput    &
                         &       )                                               &
                         &  .Or. (       GV%limitCheck.Eq.setLimitCheck%loose)   &
                         &   ) Then                                              !
                       !
                       vars(iIn)%nFocus         =  vars(iIn)%nFocus+1
                       vars(iIn)%focusList(ii)  =  rInput
                       !
                       If (    .Not.                                             &
                            &        (       limits(iStd1)%focus.Le.rInput       &
                            &          .And. limits(iStd2)%focus.Ge.rInput       &
                            &        )                                           &
                            &   ) Then                                           !  
                          !
                          Write (messageText,*)  rInput, " outside ",            &
                               &         " standard range ", limits(istd1)%focus,&
                               &         " to ",             limits(iStd2)%focus !
                          Call PakoMessage(priorityW,severityW,                  &
                               &                 command//" /"//option,          &
                               &                 messageText)                    !
                          !
                       End If
                       !
                    Else
                       !
                       errorInconsistent = .True.
                       Write (messageText,*)  rInput, " outside ",               &
                            &                 " limits ", limits(iLimit1)%focus, &
                            &                 " to ",     limits(iLimit2)%focus  !
                       Call PakoMessage(priorityE,severityE,                     &
                            &                 command//" /"//option,             &
                            &                 messageText)                       !

                    End If
                    !
                 End If   !!   (.Not. errorR)
                 !
              End Do   !!   ii = 1, nPar, 1
              !
           Else If ( sic_narg(iOption).Lt.2 ) Then
              !
              error  =  .True.
              Write (messageText,*)  " must have at least 2 ",                   &
                   &                 " focus values [mm]"                        !
              Call PakoMessage(priorityE,severityE,command//" /"//option,        &
                   &                 messageText)                                !
              !
           Else If ( nPar.Gt.nFocusMax  ) Then
              !
              error  =  .True.
              Write (messageText,*)  " can have at most ", nFocusMax,            &
                   &                 " focus values [mm]"                        !
              Call PakoMessage(priorityE,severityE,command//" /"//option,        &
                   &                 messageText)                                !
              !
           End If   !!    ( sic_narg(iOption).Ge.2 .And. nPar.Le.nFocusMax )
           !
        End If   !!   (vars(iIn)%doFocus)
        !
     End If   !!   (sic_narg(iOption) .Ge. 1)
     !
     !D      Write (6,*) "      vars(iIn)%doFocus:   ", vars(iIn)%doFocus
     !
     ERROR  =  ERROR.Or.errorR.Or.errorInconsistent
     !
     !D      Write (6,*) "      errorR:              ", errorR
     !D      Write (6,*) "      errorInconsistent:   ", errorInconsistent
     !D      Write (6,*) "      error:               ", error
     !D      Write (6,*) '      <-  optionFocus.f90 '
     !
  End If   !!!   (SIC_PRESENT(iOption,0)) 
  !

!!$           If ( nPar.Gt.nFocusMax ) Then
!!$              !
!!$              !
!!$           Else     !!!   i.e., not: ( nPar.Gt.nFocusMax )
!!$              !
!!$              Do ii = 1, nPar, 1
!!$                 !
!!$                 !D         Write (6,*) "      try argument # ", ii
!!$                 !
!!$                 iArgument = ii
!!$                 !
!!$                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)           ! 
!!$                 !
!!$                 If (.Not. errorR) Then                                             !   !!   TBD 2014: abstract out range checks
!!$                    !
!!$                    If       (   (       limits(iLimit1)%focus.Le.rInput            &
!!$                         &         .And. limits(iLimit2)%focus.Ge.rInput            &
!!$                         &       )                                                  &
!!$                         &  .Or. (       GV%limitCheck.Eq.setLimitCheck%relaxed     &
!!$                      &         .And. limits(iLimit1)%focus-10.0.Le.rInput       &
!!$                      &         .And. limits(iLimit2)%focus+10.0.Ge.rInput       &
!!$                      &       )                                                  &
!!$                      &  .Or. (       GV%limitCheck.Eq.setLimitCheck%loose)      &
!!$                      &   ) Then                                                 !
!!$                    !
!!$                    vars(iIn)%nFocus         =  vars(iIn)%nFocus+1
!!$                    vars(iIn)%focusList(ii)  =  rInput
!!$                    !
!!$                    If (    .Not.                                                &
!!$                         &        (       limits(iStd1)%focus.Le.rInput          &
!!$                         &          .And. limits(iStd2)%focus.Ge.rInput          &
!!$                         &        )                                              &
!!$                         &   ) Then                                              !  
!!$                       !
!!$                       Write (messageText,*)  rInput, " outside ",               &
!!$                            &         " standard range ", limits(istd1)%focus,   &
!!$                            &         " to ",             limits(iStd2)%focus    !
!!$                       Call PakoMessage(priorityW,severityW,                     &
!!$                            &                 command//" /"//option,             &
!!$                            &                 messageText)                       !
!!$                       !
!!$                    End If
!!$                    !
!!$                 Else
!!$                    !
!!$                    errorInconsistent = .True.
!!$                    Write (messageText,*)  rInput, " outside ",                  &
!!$                         &                 " limits ", limits(iLimit1)%focus,    &
!!$                         &                 " to ",     limits(iLimit2)%focus     !
!!$                    Call PakoMessage(priorityE,severityE,command//" /"//option,  &
!!$                         &                 messageText)                          !
!!$                    
!!$                 End If
!!$                 !
!!$              End If   !!   (.Not. errorR)
!!$              !
!!$           End Do   !!   ii = 1, nPar, 1
!!$           !
!!$           vars(iIn)%doFocus  = .True.
!!$           !
!!$        End If   !!!   ( nPar.Gt.nFocusMax )
!!$        !
!!$        !D      Write (6,*) "      vars(iIn)%nFocus:   ",  vars(iIn)%nFocus
!!$        !
!!$        Do ii = 1, vars(iIn)%nFocus, 1
!!$           !
!!$           !D         Write (6,*) "      vars(iIn)%focusList(",ii,")    :    ",              &
!!$           !D              &             vars(iIn)%focusList(ii)                             !
!!$           !
!!$        End Do
!!$        !
!!$     Else     !!!   (sic_narg(iOption) .Ge. 1)
!!$        !
!!$        errorInconsistent = .True.
!!$        Write (messageText,*)  "needs at least one focus value [mm]"
!!$        Call PakoMessage(priorityE,severityE,command//" /"//option,              &
!!$             &                 messageText)                                      !
!!$        !
!!$        End If   !!!   (sic_narg(iOption) .Ge. 1)

