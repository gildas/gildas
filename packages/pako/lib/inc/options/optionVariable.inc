!
!     $Id$
!
!     Note:     NOT YET IMPLEMENTED
!     Family:   logical with one real prarameter
!     Siblings: optionVariable.f
!     Cousins:  optionReference.f
!               optionSky.f
!
      OPTION = 'VARIABLE'
!
      Call indexCommmandOption                                           &
     &     (command,option,commandFull,optionFull,                       &
     &      commandIndex,optionIndex,iCommand,iOption,                   &
     &      errorNotFound,errorNotUnique)
!
      errorL = .False.
      errorR = .False.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
!
         messageText = 'not yet implemented'
         Call pakoMessage(priorityE,severityE,                               &
     &                command//'/'//option,messageText)
         errorL = .True.
!
      Endif
!
      ERROR = ERROR .Or. errorR .Or. errorL
!

