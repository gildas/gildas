!
!     Id: optionSwfReceiver.inc,v 1.0.3 2006-03-29 Hans Ungerechts
!     Family:   single char variabe, choice of values
!     Siblings: optionFunction.f90
!     Siblings: optionReceiver.f90
!
errorL    = .False.
errorR    = .False.
errorC    = .False.
errorNarg = .False.
ERROR     = .False.
!
iRec = 0
!
OPTION = 'RECEIVER'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
If (SIC_PRESENT(iOption,0)) Then
   !
   iArgument = 1
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        & 
           &           cInputLong,lengthInput,.True.,errorC)
      !D      Write (6,*) "   cInputLong: ->", cInputLong, "<-"
   Else
      errorL = .True.
      messageText =                                                              &
           &        "no receiver name specified "
      Call pakoMessage(priorityE,severityE,command,messageText)
   End If
   !
   ERROR = ERROR .Or. errorC .Or. errorL
   !
   If (.Not. ERROR) Then
      !
      Call pakoUmatchKey (                                                       &
           &              keys=receiverChoices,                                  &
           &              key=cInputLong,                                        &
           &              command='SWFREQUENCY',                                 &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         &
           &              nMatch=nMatch,                                         &
           &              error=errorC,                                          &
           &              errorCode=errorCode                                    &
           &             )
      !
      If (.Not.errorC) Then
         If (receiverChoices(iMatch).Eq.rec%Bolo) Then
            messageText = "can not be used with receiver "//rec%Bolo
            Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
            errorC = .True.
         End If
      End If
      !
      ERROR = ERROR .Or. errorC
      !
      If (.Not. ERROR) Then
         !
         vars(iIn)%receiverName = receiverChoices(iMatch)
         vars(iIn)%receiverI    = iMatch
         iRec                   = iMatch
         !
      Else
         !
         errorL = .True.
         messageText =                                                           &
              &        "generic error"
         Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
         !
      End If
      !
   End If
   !
End If
