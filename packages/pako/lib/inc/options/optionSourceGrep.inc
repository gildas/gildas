!
!     Id: optionSourceGrep.inc ,v 1.4 2015-05-20 Hans Ungerechts
!     Id: optionSourceGrep.inc ,v 0.3 2005-07-07 Hans Ungerechts
!
Call pako_message(seve%t,programName,                                            &
     &    " module source,   --> optionSourceGrep.inc ,v 1.4 2015-05-20 ")       !
!
!!   IMPLEMENTATION NOTE: 
!!   cFile (name of catalog file) has been set in parametersSource.inc
!
fileName = cFile
!
filePath     = ''
fileBaseName = cFile
fileExt      = '.sou'
!
!!$Write (6,*) "    --> will Call SIC_PARSEF(fileBaseName,filePath,' ',fileExt)"
!!$Write (6,*) "   "
!
!!   IMPLEMENTATION NOTE: here we are "abusing" SIC_PARSEF 
!!   e.g., before Call SIC_PARSEF(fileBaseName,filePath,' ',fileExt)
!!                     SIC_PARSEF is in kernel/lib/gsys/parse.f90
!!
!!         fileName:        ->
!! /local/users/ungerech/Projects/999-99/test-galactic.sou<-
!!         fileBaseName:    ->
!! /local/users/ungerech/Projects/999-99/test-galactic.sou<-
!!         fileExt:         ->.sou<-
!!         filePath:        -><-
!!
!!     --> after Call SIC_PARSEF(fileBaseName,filePath,' ',fileExt)
!!          fileName:        ->
!!  /local/users/ungerech/Projects/999-99/test-galactic.sou<-
!!          fileBaseName:    ->test-galactic<-
!!          fileExt:         ->.sou<-
!!          filePath:        ->
!! 
!
Call SIC_PARSEF(fileBaseName,filePath,' ',fileExt)
!
!!$If (GV%doDebugMessages) Then
!!$   Write (6,*) "   "
!!$   Write (6,*) "    --> after Call SIC_PARSEF(fileBaseName,filePath,' ',fileExt)"
!!$   Write (6,*) "        fileName:        ->",                                    &
!!$        &               fileName(1:len_trim(fileName)) ,"<-"                     !
!!$   Write (6,*) "        fileBaseName:    ->",                                    &
!!$        &               fileBaseName(1:len_trim(fileBaseName)) ,"<-"             !
!!$   Write (6,*) "        fileExt:         ->",                                    &
!!$        &               fileExt(1:len_trim(fileExt))  ,"<-"                      !
!!$   Write (6,*) "        filePath:        ->",                                    &
!!$        &               filePath(1:len_trim(filePath)) ,"<-"                     !
!!$   Write (6,*) "   "
!!$End If
!
OPTION = 'GREP'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL = .False.
errorR = .False.
errorC = .False.
!
If (SIC_PRESENT(iOption,0)) Then
   !
   ! TBD: ignore comment lines in source catalog!
   ! TBD: options for grep
   !
!!$   cFileGrep = '.GREP'//cFile(1:len_trim(cFile))
!!$   cFileCount = '.COUNT'//cFile(1:len_trim(cFile))
   cFileGrep = '.GREP'//fileBaseName(1:len_trim(fileBaseName))
   cFileCount = '.COUNT'//fileBaseName(1:len_trim(fileBaseName))
   !
   shellCommand = 'grep -i "'                                                    &
        &  //vars(iIn)%sourceName(1:len_trim(vars(iIn)%sourceName))              &
        &  //'" '//cFile(1:lenc(cFile))//' >'                                    &
        &  //cFileGrep(1:lenc(cFileGrep))                                        !
   !D    write (6,*) " shellCommand: -->",shellCommand(1:len_trim(shellCommand)),"<--" 
   iEr = System(shellCommand(1:len_trim(shellCommand)))
   !
   shellCommand = 'wc -l '                                                       &
        &  //cFileGrep(1:lenc(cFileGrep))                                        &
        &  //' >'                                                                &
        &  //cFileCount(1:len_trim(cFileCount))                                  !
   iEr = System(shellCommand(1:len_trim(shellCommand)))
   !
!!$   shellCommand = 'cat '//cFileCount(1:len_trim(cFileCount))
!!$   iEr = System(shellCommand(1:len_trim(shellCommand)))
   !
   shellCommand = 'cat '//cFileGrep(1:lenc(cFileGrep))
   iEr = System(shellCommand(1:len_trim(shellCommand)))
   !
End If
!
