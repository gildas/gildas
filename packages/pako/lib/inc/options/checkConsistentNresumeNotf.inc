!
!     $Id$
!
      if (vars(iIn)%doResume .and.                                       &
     &    vars(iIn)%nResume.gt.vars(iIn)%nOtf) then
         errorInconsistent = .TRUE.
      end if
!
      IF (errorInconsistent) THEN
         write (messageText,*)                                           &
     &        'nResume: ', vars(iIn)%nResume,                            &
     &        ' inconsistent with nOtf: ', vars(iIn)%nOtf
         CALL pakoMessage(priorityE,severityE,'otfmap',messageText)
      END IF
!
