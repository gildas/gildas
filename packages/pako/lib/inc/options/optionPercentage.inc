!
!     Id: optionPercentage.inc,v 1.1.6 2011-06-28 Hans Ungerechts
!     Family:   option 1 real parameter / with conditions
!     Siblings: 
!
!     errorL = .False.
!
!DWrite (6,*) ' -->  optionPercentage: '
!DWrite (6,*) '      iBack: ', iBack
!DWrite (6,*) '      iPart: ', iPart
!
Option = 'PERCENTAGE'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (.Not. (iBack.Eq.iVESPA .Or. iBack.Eq.iWILMA) ) Then
      !
      errorL = .True.
      messageText = "works only with "//bac%VESPA//" or "//bac%WILMA             !
      Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
      !
   Else If (iBack .Ne. GPnoneI .And. iPart .Ne. GPnoneI) Then
      !
      Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
      If (.Not.errorR) Then
         Call checkR4co(command,option,rInput,                                   &
              &           vars(:,iBack)%percentage, errorR)                      !
      End If
      !
      !D      Write (6,*) '      vars(:,iBack)%percentage:'
      !D      Write (6,*)        vars(:,iBack)%percentage
      !
   Else
      !
      errorL = .True.
      messageText = "needs valid backend name and part"
      Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
      !
   End If
End If
!
ERROR = ERROR .Or. errorR .Or. errorL
!
