!
!     Id: optionSymmetric.inc,v 0.94 2005-11-09 Hans Ungerechts
!
OPTION = 'SYMMETRIC'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   vars(iIn)%doSymmetric = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &  
           &                  vars(iIn)%doSymmetric,.True.,errorL)
   End If
Endif
!
ERROR = ERROR .Or. errorL
!
