!
!     $Id$
!     Family:   option -- simple logical
!     Siblings: optionAmbient.f90
!               optionBalance.f90
!               optionUpdate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
      OPTION = 'AMBIENT'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      errorL    = .FALSE.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
         vars(iIn)%doAmbient = .True.
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_L4(LINE,iOption,iArgument,                        &
     &                  vars(iIn)%doAmbient,.True.,errorL)
         Endif
      Endif
      ERROR = ERROR .Or. errorL
!
