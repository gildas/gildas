!
!     $Id$
!
      OPTION = 'RESUME'
!
      CALL indexCommmandOption                                           &
     &     (command,option,commandFull,optionFull,                       &
     &      commandIndex,optionIndex,iCommand,iOption,                   &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      iArgument = 1
!
      errorL = .FALSE.
      errorI = .FALSE.
!
      IF (SIC_PRESENT(iOption,0)) THEN
!!
         CALL pakoMessage(priorityE,severityE,command//'/'//option,          &
     &                " not yet implemented ")
         error  = .True.
!!
         vars(iIn)%doResume = .TRUE.
         IF (SIC_PRESENT(iOption,iArgument)) THEN
            CALL SIC_L4(LINE,iOption,iArgument,                          &
     &           vars(iIn)%doResume,.FALSE.,errorL)
!     TBD:  should catch:
!           E-MTH,  Result type mismatch
!           E-LOGICAL,  Error evaluating 234
!
            IF (errorL) THEN
               CALL SIC_I4(LINE,iOption,iArgument,iInput,.TRUE.,errorI)
               if (.NOT. errorI) CALL checkI4(option,iInput,             &
     &              vars%nResume,                                        &
     &              errorI)
            END IF
         END IF
         ERROR = ERROR .OR. errorI
!
! TBD:   could alow text codes "C" and "R" 
!        for %doResumeCalibrate %doResumeReference
         errorL = .FALSE.
         iArgument = 2
         IF (.NOT.error .AND. SIC_PRESENT(iOption,iArgument)) THEN
            CALL SIC_L4(LINE,iOption,iArgument,                          &
     &           vars(iIn)%doResumeCalibrate,.FALSE.,errorL)
         END IF
         ERROR = ERROR .OR. errorL
! 
         errorL = .FALSE.
         iArgument = 3
         IF (.NOT.error .AND. SIC_PRESENT(iOption,iArgument)) THEN
            CALL SIC_L4(LINE,iOption,iArgument,                          &
     &           vars(iIn)%doResumeReference,.FALSE.,errorL)
         END IF
         ERROR = ERROR .OR. errorL
! 
      END IF
!
      errorL = .FALSE.
      errorI = .FALSE.
!
