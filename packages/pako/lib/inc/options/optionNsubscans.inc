!
!     $Id$
!     Family:   option -- simple integer
!     Siblings: optionFeBe
!     Siblings: optionNsubscans
!     Siblings: optionNphases
!
errorI = .False.
!
OPTION = 'NSUBSCANS'
!
Call indexCommmandOption                                               &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
   !
   If (.Not. errorI)                                                   &
        &      Call checkI4co(command,option,iInput,                   &
        &           vars%nSubscans,                                    &
        &           errorI)
   !
Endif
!
ERROR = ERROR .Or. errorI
