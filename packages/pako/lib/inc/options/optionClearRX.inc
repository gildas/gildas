!
!     Id: optionClearRX.inc, v 1.2.3 2014-09-03 Hans Ungerechts
!     Id: optionClearRX.inc, v 1.2.3 2014-04-16 Hans Ungerechts
!         (only cleaned up and debug writes added)
!
!     Id: optionClearRX.inc, v  0.95 2005-11-23 Hans Ungerechts
!     Family:   option -- simple logical with action
!     Siblings: optionConnect.f90
!               optionBalance.f90
!               optionCalibrate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
  !D   Write (6,*) " "
  !D   Write (6,*) "   --> optionClearRX.inc, v 1.2.3 2014-09-03"
  !
  OPTION = 'CLEAR'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  errorL = .False.
  !
  If (SIC_PRESENT(iOption,0)) Then
     iArgument = 1
     doClearRX      = .True.
     doConnectRX    = .False.
     doDisconnectRX = .False.
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_L4(LINE,iOption,iArgument,                                      &
             &           doClearRX,.True.,errorL)                                !
     End If
     !
     If (doClearRX .And. .Not. errorL) Then
        !
        vars(iIn,:)             =  vars(iDefault,:)
        vars(iIn,:)%isConnected = .False.
        vars(iTemp,:)           =  vars(iIn,:)
        vars(iValue,:)          =  vars(iIn,:)
        !
        NIKApixel%isSet         =  .False.
        NIKApixel%systemName    =  offs%nas
        NIKApixel%point%x       =  0.0
        NIKApixel%point%y       =  0.0
        NIKApixel%channel       =  1
        NIKApixel%elevation     =  0
        !
        GV%sourceOffsets(iNas)        =  NIKApixel
        GV%sourceOffsets(iNas)%isSet  =  .False.
        GV%arrayNasmythOffsetsAreSet  =  .False.
        !
        !! ** 2005-11-23 HU: wrong: vars(iIn,:)     =  rxReset
        !! ** 2005-11-23 HU: wrong: vars(iTemp,:)   =  rxReset
        !! ** 2005-11-23 HU: wrong: vars(iValue,:)  =  rxReset
        !
     End If
     !
  End If
  !
  !D   Write (6,*) "   GV%arrayNasmythOffsetsAreSet: ", GV%arrayNasmythOffsetsAreSet
  !
  ERROR = ERROR .Or. errorL
  !
  !! ** Note 2005-11-23: change of logic, because the previous
  !!    implementation caused problems with the "internal" RX 
  !!    parameters undefined in the XML files
  !
  !D Write (6,*) "   <-- optionClearRX.inc"
  !D Write (6,*) " "
