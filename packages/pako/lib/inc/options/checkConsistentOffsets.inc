!
!   Id: checkConsistentOfsets.inc, v0.95 2005-11-11 Hans Ungerechts
!
!D      Write (6,*) "   moduleSource: --> checkConsistentOffsets.f90 "
!
errorInconsistent = .False.
!
nPDB = 0
nEH  = 0
nTH  = 0
!
If (varsOffsets(iIn,iProjection)%isSet)  nPDB = nPDB+1
If (varsOffsets(iIn,iDescriptive)%isSet) nPDB = nPDB+1
If (varsOffsets(iIn,iBasis      )%isSet) nPDB = nPDB+1
!
If (varsOffsets(iIn,iEquatorial )%isSet) nEH  = nEH+1
If (varsOffsets(iIn,iHadec      )%isSet) nEH  = nEH+1
!
If (varsOffsets(iIn,iHorizonTrue)%isSet) nTH  = nTH+1
If (varsOffsets(iIn,iHorizon    )%isSet) nTH  = nTH+1
!
!D      Write (6,*) "                               nPDB:  ", nPDB
!D      Write (6,*) "                               nEH:   ", nEH
!D      Write (6,*) "                               nTH:   ", nTH
!
errorInconsistent = nPDB .Gt. 1                                                  &
     !TBD       &         .Or. nEH  .GT. 1                                       &
     &         .Or. nTH  .Gt. 1
!
If (errorInconsistent) Then
   messageText = "offset combination not possible"
   Call pakoMessage(priorityE,severityE,command,messageText)         
End If
!
!!$If (varsOffsets(iIn,iProjection)%isSet) Then
!!$   messageText = "offset /system PROJECTION is not yet supported"
!!$   Call pakoMessage(priorityE,severityE,command,messageText)         
!!$   errorInconsistent = .True.
!!$End If
!
If (varsOffsets(iIn,iDescriptive)%isSet) Then
   messageText = "offset /system DESCRIPTIVE is not yet supported"
   Call pakoMessage(priorityE,severityE,command,messageText)         
   errorInconsistent = .True.
End If
!


