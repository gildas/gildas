!
!     $Id$
!
      OPTION = 'FLUX'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &     commandIndex,optionIndex,iCommand,iOption,                  &
     &     errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      If (SIC_PRESENT(iOption,0)) Then
!
         If (SIC_NARG(iOption).Eq.1.Or.SIC_NARG(iOption).Eq.2) Then
            iArgument = 1
            Call SIC_R4(LINE,iOption,iArgument,rInput1,.True.,errorR)
            If (.Not. errorR) Call checkR4co(command,                  &
     &                                       option,                   &
     &                                       rInput1,                  &
     &                                       vars%flux,                &
     &                                       errorR)
!
            If (.Not.errorR) Then 
               If (SIC_NARG(iOption).Eq.2) Then
                  iArgument = 2
                  Call SIC_R4(LINE,iOption,iArgument,rInput2,.True.,errorR)
                  If (.Not. errorR) Call checkR4co(command,            &
     &                                             option,             &
     &                                             rInput2,            &
     &                                             vars%index,         &
     &                                             errorR)
               Else
                  vars(iIn)%index = 0.0
               End If
            End If
         Else 
            errorR = .True.
         End If
!
      End If
!
      If (errorR) Then
         messageText =                                                 &
     &        "flux [index] -> 1 or 2 real values are required"
         Call pakoMessage(6,3,command//"/"//option,messageText)
      End If
!
      ERROR = ERROR .Or. errorR


