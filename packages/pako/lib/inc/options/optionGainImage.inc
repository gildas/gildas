!
!     $Id$
!     Family:   option 1 real parameter
!     Siblings: optionGainImage.f 
!
      errorR = .False.
!
      Option = 'GAINIMAGE'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
! TBD check for number of arguments
! TBD allow input with units decibel
!
      iArgument = 1
!
      If (SIC_PRESENT(iOption,0)) Then
!
         Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
!
         If (.Not.SIC_PRESENT(iOption,2)) Then
            If (.Not.errorR) Then
               Call checkR4co(command,option,rInput,                   &
     &           vars(:,iRec)%gainImage%value,                         &
     &           errorR)
            End If
            If (.Not. errorR) Then
                vars(iIn,iRec)%gainImage%db =                          &
     &          10.0*Log10(vars(iIn,iRec)%gainImage%value)
             End If
          End If
!
         If (SIC_PRESENT(iOption,2)) Then
            Call SIC_CH(LINE,iOption,2,                                & 
     &                  cInput,lengthInput,.True.,errorC)
            cInputUpper = cInput
            Call SIC_UPPER(cInputUpper)
            If (cInputUpper(1:2).Eq."DB") Then
               If (.Not.errorR) Then
                  Call checkR4co(command,option,rInput,                &
     &                           vars(:,iRec)%gainImage%db,            &
     &                           errorR)
               End If
               If (.Not.errorR) Then
                  vars(iIn,iRec)%gainImage%value =                     &
     &            10.0**(0.1*rInput)
               End If
            Else
               l = lengthInput
               messageText = "invalid unit: "//cInputUpper(1:l)
               Call PakoMessage(priorityE,severityE,                       &
     &                      command,messageText)
               errorC = .True.
            End If
         End If
!
      End If
!
      ERROR = ERROR .Or. errorR .Or. errorC
!



