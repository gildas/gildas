!
      if (vars(iValue)%doResume) then
         write (lineOut,*) CMD(1:lCMD),                                  &
     &        " /resume ",S,                                             &
     &        vars(iValue)%nResume
         if (vars(iValue)%doResumeCalibrate) then
            lineOut = lineOut(1:lenc(lineOut))//" yes"
         else
            lineOut = lineOut(1:lenc(lineOut))//"  no"
         end if
         if (vars(iValue)%doResumeReference) then
            lineOut = lineOut(1:lenc(lineOut))//" yes"
         else
            lineOut = lineOut(1:lenc(lineOut))//"  no"
         end if
         write (iUnit,*) lineOut(1:lenc(lineOut))
      else
         write (iUnit,*) CMD(1:lCMD),                                    &
     &        " /resume ", "no"
      end if
!
