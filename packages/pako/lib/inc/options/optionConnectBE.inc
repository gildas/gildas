!
! Id: optionConnectBE.inc,v 1.1.11 2011-11-04 Hans Ungerechts
!     Family:   option -- logical with conditions
!     Siblings: optionConnectBE.inc
!               optionDisConnectBE.inc
!               optionConnectRX.inc
!               optionDisconnectRX.inc
!
OPTION = 'CONNECT'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
doConnectExplicit = .False.
!
If (SIC_PRESENT(iOption,0)) Then
   iArgument = 1
   !
   If (gv%iUserLevel.Ge.iExperienced) Then
      !
      messageText = " /connect is DEPRECATED. USE AT YOUR OWN RISK! "
      Call pakoMessage(priorityW+10,severityW,command,messageText)
      !
      doConnect = .True.
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_L4(LINE,iOption,iArgument,                                     &
              &           doConnect,.True.,errorL)                               !
      Endif
      !
      doConnectExplicit = doConnect
      !
      If (doConnect) Then
         Include 'inc/options/doConnectBE.inc'
      Else
         doDisconnect = .True.
         Include 'inc/options/doDisconnectBE.inc'
      End If
      !
   Else
      !
      errorL = .True.
      messageText = " /connect is DEPRECATED. USE AT YOUR OWN RISK! "
      Call pakoMessage(priorityW+10,severityW,command,messageText)
      messageText = " /connect requires higher User Level"
      Call pakoMessage(priorityE+10,severityE,command,messageText)
      !
   End If
   !
End If
!
ERROR = ERROR .Or. errorL
!
