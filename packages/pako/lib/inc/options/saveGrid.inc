!
!  Id: saveGrid.inc,v 0.96 2005-12-05 Hans Ungerechts
!
If (vars(iValue)%doGrid) Then
   lineOut = " /grid yes"
Else
   lineOut = " /grid no"
End If
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
