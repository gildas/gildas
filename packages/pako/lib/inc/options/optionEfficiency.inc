!
!     $Id$
!     Family:   option with 1 [or 2] real parameters
!                           1st      allows * = no change
!                 optional: warning  if 1st .lt. 2nd 
!
!     Siblings: optionEfficiency.f90
!
      errorR = .False.
!
      Option = 'EFFICIENCY'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      If (SIC_PRESENT(iOption,0)) Then
!
         If (SIC_NARG(iOption).Lt.1. .Or. SIC_NARG(iOption).Gt.2) Then
            messageText =                                              &
     &      "1 or 2 real parameters effForward [effBeam] are required"
            Call PakoMessage(priorityE,severityE,command,messageText)
            errorR = .True.
         End If
         ERROR = ERROR .Or. errorR
!
         iArgument = 1
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                        &
     &                  cInput,lengthInput,.False.,errorC)
            If (cInput(1:1).Ne.'*') Then
               Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
               If (.Not.errorR) Then
                  Call checkR4co(command,option,rInput,                &
     &                           vars(:,iRec)%effForward,              &
     &                           errorR)
               End If
            End If
         End If
         ERROR = ERROR .Or. errorR
!
         iArgument = 2
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
            If (.Not.errorR) Then
               Call checkR4co(command,option,rInput,                   &
     &                        vars(:,iRec)%effBeam,                    &
     &                        errorR)
            End If
         End If
         ERROR = ERROR .Or. errorR
!
         If (vars(iIn,iRec)%effForward .Lt.                            &
     &       vars(iIn,iRec)%effBeam)          Then
            Write(messageText,*)                                       &
     &      "strange values: effForward ",vars(iIn,iRec)%effForward,  &
     &      " less than effBeam ",vars(iIn,iRec)%effBeam
            Call PakoMessage(priorityW,severityW,command,messageText)
         End If
!
      End If
