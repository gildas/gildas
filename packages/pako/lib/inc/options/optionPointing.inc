  !
  !     Id: optionPointing.inc, v 1.2.3   2014-07-07 Hans Ungerechts
  !     Id: optionPointing.inc, v 1.0.6.1 2007-01-13 Hans Ungerechts
  !     Family:   option -- simple logical
  !     Siblings: optionBalance.f90
  !               optionCalibrate.f90
  !               optionFlagRef.f90
  !               optionMore.f90
  !               optionUpdate.f90
  !               optionWriteToSeg.f90
  !               optionZigzag.f90
  !
  !D   Write (6,*) '      ->  optionPointing.inc '
  !
  OPTION = 'POINTING'
  !
  vars(iIn)%doPointing  =  .False.
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  errorL    = .False.
  !
  iArgument = 1
  If (SIC_PRESENT(iOption,0)) Then
     !
     !!         Include 'inc/options/nyi.inc'
     !
     vars(iIn)%doPointing = .True.
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_L4(LINE,iOption,iArgument,                                      &
             &                  vars(iIn)%doPointing,.True.,errorL)              !
     Endif
  Endif
  ERROR = ERROR .Or. errorL
  !
  !D   Write (6,*) "         vars(iIn)%doPointing :   ", vars(iIn)%doPointing 
  !D   !
  !D   Write (6,*) "      <-  optionPointing.inc "
