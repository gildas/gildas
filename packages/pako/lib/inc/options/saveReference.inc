!
!     $Id$
!
      if (vars(iValue)%doReference) then
         if (vars(iValue)%altRef.eq."OFFSETS") then
            l = lenc(vars(iValue)%systemNameRef)
            write(lineOut,*)                                             &
     &           "/reference ",                                          &
     &           vars(iValue)%offsetR%x,                                 &
     &           vars(iValue)%offsetR%y,                                 &
     &           ' "',vars(iValue)%systemNameRef(1:l),'"'
         else if (vars(iValue)%altRef.eq."LIST") then
            l = lenc(vars(iValue)%listOfNamesRef)            
            lineOut =                                                    &
     &         " /reference "//Q//                                       &
     &         vars(iValue)%listOfNamesRef(1:l)//Q
         end if
      else
         lineOut = " /reference no"
      end if
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
