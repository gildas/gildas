!
!  Id: optionCatalog.inc,v 1.0.9 2007-10-02 Hans Ungerechts
!
OPTION = 'CATALOG'
!
vars(iIn)%sourceCatalog = GPnone
!
Call indexCommmandOption                                                         & 
     &     (command,option,commandFull,optionFull,                               & 
     &      commandIndex,optionIndex,iCommand,iOption,                           & 
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        & 
           &           cInput100,lengthInput,.True.,errorC)                      !
      If (errorC) Then
         Write (messageText,*) 'value: "',cInput100,'" not valid'
         Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,              &
              &           messageText)                                           !
      Else If (cInput100(1:1).Eq.'*') Then
         vars(iIn)%sourceCatalog = 'iram-J2000.sou'
      Else If (cInput100(1:1).Eq.'$') Then
         vars(iIn)%sourceCatalog = 'lines-J2000.sou'
      Else
         Call SIC_PARSEF(cInput100,vars(iIn)%sourceCatalog,' ','.sou')
      End If
   Else
      errorC = .True.
      messageText = "parameter sourceCatalog is required"
      Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,                 &
           &           messageText)                                              !
   End If
End If
!     
ERROR = ERROR .Or. errorC 
!
