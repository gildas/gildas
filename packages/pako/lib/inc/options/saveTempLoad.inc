!
! Id: saveTempLoad.inc,v 1.0.6.4 2007-03-05 Hans Ungerechts
!
If (vars(iValue,ii)%tempColdCode.Eq.GPnone) Then
   Write (c1,*)   vars(iValue,ii)%tempCold
Else
   c1 = "L"
End If
l1 = lenc(c1)
!
If (vars(iValue,ii)%tempAmbientCode.Eq.GPnone) Then
   Write (c2,*)   vars(iValue,ii)%tempAmbient
Else
   c2 = "L"
End If
l2 = lenc(c2)
!
!
lineOut = " /tempLoad "//c1(1:l1)//" "//c2(1:l2)
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
