!
!  Id: saveTotfDIY.inc,v 1.1.2 2010-02-05 Hans Ungerechts
!
Write (lineOut,*)                                                                &
     &     "/tOtf ", segList(ii)%tSegment                                    !
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
