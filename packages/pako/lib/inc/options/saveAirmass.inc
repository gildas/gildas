  !
  !   Id: saveAirmass, v 1.2.4 2015-05-28 Hans Ungerechts
  !
  !D   Write (6,*) "      --> saveAirmass.inc "
  !D   Write (6,*) "          vars(iValue)%nAir:   ", vars(iValue)%nAir
  !
  If (vars(iValue)%nAir.Ge.6) Then
     !
     Write (lineOut,*)                                                           &
          &     " /airmass -"                                                    !
     !
     Write (iUnit,*) lineOut(1:lenc(lineOut))
     !
     Do ii = 1, vars(iValue)%nAir, 10
        !
        jj = min(ii+9, vars(iValue)%nAir)
        !
        If ( jj.Lt.vars(iValue)%nAir ) Then
           Write (lineOut,'(10F12.2)')                                           &
                &     vars(iValue)%airmassList(ii:jj)                            !
           Write (iUnit,*) lineOut(1:lenc(lineOut)), " - "
        Else
           Write (lineOut,'(10F12.2)')                                           &
                &     vars(iValue)%airmassList(ii:jj)                            !
           Write (iUnit,*) lineOut(1:lenc(lineOut)), " LIST - "
        End If
        !
     End Do
     !
  Else
     !
     Write (lineOut,*)                                                           &
          &     " /airmass ", vars(iValue)%airmass%from,                         &
          &     " to ", vars(iValue)%airmass%to,                                 &
          &     " by ", vars(iValue)%airmass%by                                  !
     Include 'inc/options/caseContinuationLine.inc'
     Write (iUnit,*) lineOut(1:lenc(lineOut))
     !
  End If
  !
