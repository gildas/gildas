!
! Id: readOptionsReceiver.inc,v 1.1.1 2009-04-16 Hans Ungerechts
!
! TBD: options /gainBolo(meter) /channel for bolometer
!
! TBD         Include 'inc/options/optionCenterIf.inc'
!
doClearRX      = .False.
doConnectRX    = .False.
doDisconnectRX = .False.
!
Include 'inc/options/optionClearRX.inc'
Include 'inc/options/optionConnectRX.inc'
Include 'inc/options/optionDisconnectRX.inc'
!
! *** Read options for heterodyne receivers (not BOLOmeter)
!
If (iRec .Ne. iBOLO) Then
   !
   Include 'inc/options/optionDerorator.inc'
   Include 'inc/options/optionDoppler.inc'
   Include 'inc/options/optionEfficiency.inc'
   Include 'inc/options/optionFoffset.inc'
   Include 'inc/options/optionGainImage.inc'
   Include 'inc/options/optionHorizontal.inc'
   Include 'inc/options/optionSband.inc'
   Include 'inc/options/optionScale.inc'
   Include 'inc/options/optionTempLoad.inc'
   Include 'inc/options/optionVertical.inc'
   Include 'inc/options/optionWidth.inc'
   !
End If
