!
!  Id: optionLinename.inc,v 1.1.6 2011-06-29 Hans Ungerechts
!
OPTION = 'LINENAME'
!
Call indexCommmandOption                                                         & 
     &     (command,option,commandFull,optionFull,                               & 
     &      commandIndex,optionIndex,iCommand,iOption,                           & 
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
vars(iIn,iBack)%lineNameIsSet = .False.
vars(iIn,iBack)%Linename      = GPnone
!
If (SIC_PRESENT(iOption,0)) Then
   !
   vars(iIn,iBack)%lineNameIsSet = .False.
   vars(iIn,iBack)%Linename      = GPnone
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        & 
           &           cInputLong,lengthInput,.True.,errorC)                     !
      l = Len_trim(cInputLong)
      If (l.Gt.12) Then
         errorC = .True.
         Write (messageText,*) 'value: "', cInputLong(1:l),                      &
              &                " longer than 12 characters"                      !
         Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,              &
              &           messageText)                                           !
      End If
      If (errorC) Then
         Write (messageText,*) 'value: "', cInputLong(1:l), '" not valid'
         Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,              &
              &           messageText)                                           !
      End If
      If (.Not. ErrorC) Then
         vars(iIn,iBack)%lineNameIsSet = .True.
         vars(iIn,iBack)%Linename      = cInputLong(1:l)
      End If
   Else
      vars(iIn,iBack)%lineNameIsSet = .False.
      vars(iIn,iBack)%Linename      = GPnone
      messageText = "no Linename set"
      Call PAKOMESSAGE(priorityW,severityW,command//"/"//option,                 &
           &           messageText)                                              !
   End If
   !
End If
!     
ERROR = ERROR .Or. errorC 
!
!D Write (6,*) "         vars(iIn,iBack)%Linename: ",    vars(iIn,iBack)%Linename 
!

