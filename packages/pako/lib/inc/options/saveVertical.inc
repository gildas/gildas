!
!  Id: saveVertical.inc, v1.1.0 2009-03-09 Hans Ungerechts
!
Write (c1,*) vars(iValue,ii)%verticalSubbands       
l1 = lenc(c1)
!
lineOut = " /vertical "//c1(1:l1)
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
