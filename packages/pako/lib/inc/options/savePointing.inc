  !
  !  Id: savePointing.inc,v 1.2.3 2014-09-26 Hans Ungerechts
  !
  !  Family:   simple logic option
  !  Sibings:  saveSymmetric.inc
  !
  If (vars(iValue)%doPointing) Then
     lineOut = " /pointing yes"
  Else
     lineOut = " /pointing no"
  End If
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
