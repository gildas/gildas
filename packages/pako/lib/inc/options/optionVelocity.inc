!
! Id: optionVelocity.inc,v 1.0.9 2007-10-02 Hans Ungerechts
!
OPTION = 'VELOCITY'
!
Call indexCommmandOption                                               &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
errorL = .False.
errorR = .False.
errorC = .False.
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                              &
           &           cInput,lengthInput,.True.,errorC)
      cInputUpper = cInput
      Call SIC_UPPER(cInputUpper)
      !
!!$      If (cInputUpper(1:2).Eq."EA") Then
!!$         cInputUpper = refFrame%geocentric  ! for compa PdB
!!$         messageText = 'reference frame '//cInput(1:Len_trim(cInput))  &
!!$              &   //' interpreted as: '//refFrame%geocentric
!!$         Call pakoMessage(priorityW,severityW,command,messageText)
!!$      End If
      !
      Call pakoUmatchKey (                                             &
           &              keys=refFrameChoices,                        &
           &              key=cInputUpper,                             &
           &              command=command,                             &
           &              howto='start upper',                         &
           &              iMatch=iMatch,                               & 
           &              nMatch=nMatch,                               &
           &              error=errorM,                                &
           &              errorCode=errorCode                          &
           &             )
      !
      ERROR = ERROR .Or. errorM
      !
      If (.Not. ERROR) Then
         vars(iIn)%velocity%name = "radialVelo"
         vars(iIn)%velocity%type = refFrameChoices(iMatch)
         vars(iIn)%velocity%unit = "km/s"
      End If
      !
   Else
      errorC = .True.
      messageText = "parameter velocityType is required"
      Call pakoMessage(priorityE,severityE,                            &
           &   command//" /"//option,messageText)
   End If
End If
!
ERROR = ERROR .Or. errorC
!
If (.Not. ERROR) Then
   !
   iArgument = 2
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_R8(LINE,iOption,iArgument,dInput,.True.,errorR)
      Call checkR8co(command,                                          &
           &         option,                                           &
           &         dInput,                                           &
           &         vars%velocity%value,                              &
           &         errorR)
!!$      If (vars(iIn)%velocity%type.Eq.refFrame%null) Then
!!$         vars(iIn)%velocity%value = 0.0
!!$      End If
   End If
   !
   ERROR = ERROR .Or. errorR
   !
End If ! ERROR



