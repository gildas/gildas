  !
  !  Id: saveFocus.inc,v 1.2.3 2014-10-10 Hans Ungerechts
  !  Id: saveFocus.inc,v 1.2.3 2014-09-26 Hans Ungerechts
  !
  !  Family:   logic option with parameter values
  !  Sibings:  
  !
  !D   Write (6,*) "   --> saveFocus.inc  "
  !D   Write (6,*) " vars(iValue)%nFocus:   ", vars(iValue)%nFocus  
  !
  If (vars(iValue)%doFocus) Then
     !
     If ( vars(iValue)%nFocus.Ge.1 .And. vars(iValue)%nFocus.Le.6 ) Then
        Write (lineOut,*) " /focus ",                                            &
             &          (vars(iValue)%focusList(ii), ii=1,vars(iValue)%nFocus),  &
             &          " ", vars(iValue)%directionFocus                         !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
     End If
     !
     If ( vars(iValue)%nFocus.Ge.7 .And. vars(iValue)%nFocus.Le.12 ) Then
        Write (lineOut,*) " /focus ",                                            &
             &          (vars(iValue)%focusList(ii), ii=1,6)                     !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
        Write (lineOut,*)                                                        &
             &          (vars(iValue)%focusList(ii), ii=7,vars(iValue)%nFocus),  &
             &          " ", vars(iValue)%directionFocus                         !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
     End If
     !
     If ( vars(iValue)%nFocus.Ge.13 ) Then
        Write (lineOut,*) " /focus ",                                            &
             &          (vars(iValue)%focusList(ii), ii=1,6)                     !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
        Write (lineOut,*)                                                        &
             &          (vars(iValue)%focusList(ii), ii=7,12)                    !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
        Write (lineOut,*)                                                        &
             &          (vars(iValue)%focusList(ii), ii=13,vars(iValue)%nFocus), &
             &          " ", vars(iValue)%directionFocus                         !
        Include 'inc/options/caseContinuationLine.inc'
        Write (iUnit,*) lineOut(1:lenc(lineOut))
     End If
     !
  Else
     lineOut = " /focus no"
     Include 'inc/options/caseContinuationLine.inc'
     Write (iUnit,*) lineOut(1:lenc(lineOut))
  End If
  !
  !
