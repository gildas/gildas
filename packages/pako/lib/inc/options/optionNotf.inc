!
!     $Id$
!     Family:   optionNotf
!     Siblings: 
!               
!
      OPTION = 'NUMBEROFOTF'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      OPTIONShort = 'NOTF'
!
      Call indexCommmandOption                                         &
     &     (command,optionShort,commandFull,optionFull,                &
     &      commandIndex,optionIndex,iCommand,iOptionShort,            &
     &      errorNotFound,errorNotUnique)
!
      iArgument = 1
!
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
         If (.Not. errorI) Call checkI4(option,iInput,                 &
     &        vars%nOtf,                                               &
     &        errorI)
      Elseif (SIC_PRESENT(iOptionShort,iArgument)) Then
         Call SIC_I4(LINE,iOptionShort,iArgument,iInput,.True.,errorI)
         If (.Not. errorI) Call checkI4(optionShort,iInput,            &
     &        vars%nOtf,                                               &
     &        errorI)
      Endif
!
      If (.Not.errorI .And. command.Eq."POINTING") Then
!
         If (.Not.(vars(iIn)%nOtf.Eq.2 .Or.                            &
     &         Mod(vars(iIn)%nOtf,4).Eq.0)) Then
            Write (messageText,*) 'value ', vars(iIn)%nOtf,            &
     &           ' not recommended. better use: 2 or multiple of 4'
            Call PakoMessage(priorityI,severityI,                          &
     &                   command//"/"//optionShort,messageText)
         Endif
!
      End If
!
      ERROR = ERROR .Or. errorI
!




