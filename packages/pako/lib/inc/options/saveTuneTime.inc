  !
  !  Id: saveTuneTime.inc ,v 1.2.3 2014-08-28 Hans Ungerechts
  !  Id: saveTuneTime.inc ,v 1.2.3 2014-05-29 Hans Ungerechts
  !
  !D   Write (6,*) "      saveTuneTime.inc ,v 1.2.3 2014-08-28 "
  !
  If (vars(iValue)%doTune) Then
     !
     Write(lineOut,*)                                                            &
          &           "/tTune ",                                                 &
          &           vars(iValue)%tTune                                         !
     !
!!$  Else
!!$     !
!!$     lineOut = "/tune no"
     !
  End If
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
