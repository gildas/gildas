!
!     $Id$
!
      l = lenc(vars(iValue)%functionName)
      lineOut = " /function "//'"'//vars(iValue)%functionName(1:l)//'"'
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
