!
!   Id: doConnectBE.inc,v 1.1.6 2011-07-07 Hans Ungerechts
!
If      (iBack.Eq.0 .And. iPart.eq.0 .And.doConnectExplicit) Then
   !
   Where (listBE(:,:)%wasConnected) listBE(:,:)%isConnected = .True.
   !
Else If (iBack.Ge.1 .And. iPart.Eq.0) Then
   !
   Where (listBE(iBack,:)%wasConnected) listBE(iBack,:)%isConnected = .True.
   !
Else If (iBack.Ge.1 .And. iPart.Ge.1) Then
   !
   If (listBE(iBack,iPart)%wasConnected) listBE(iBack,iPart)%isConnected = .True.
   !
End If
!
