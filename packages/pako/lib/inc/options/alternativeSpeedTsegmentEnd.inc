!
!     $Id$
!
      If (vars(iTemp)%altOption.Eq."SPEED") Then
!        constant acceleration --> average speed is (speed1+speed2)/2
         vars(iTemp)%tSegment  =  vars(iTemp)%lengthOtf/                     &
     &        ((vars(iTemp)%speedStart+vars(iTemp)%speedEnd)/2.0)
      Endif
      If (vars(iTemp)%altOption.Eq."TSEGMENT") Then
         vars(iTemp)%speedStart = vars(iTemp)%lengthOtf/vars(iTemp)%tSegment
         vars(iTemp)%speedEnd   = vars(iTemp)%lengthOtf/vars(iTemp)%tSegment
      Endif
!
