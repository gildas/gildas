!
! Id: optionModeBE.inc,v 0.9 205-07-12 Hans Ungerechts
!
!D Write (6,*) "      --> optionModeBE.inc"
!
! always reset to "none"
! i.e., option must explicitly be given!
!
!     TBD: check that backend is VESPA!
!
vars(iIn,iBack)%mode        = GPnone
!
OPTION = 'MODE'
!
!D Write (6,*) "          Command: ", command
!D Write (6,*) "          OPTION:  ", OPTION
!
Call indexCommmandOption                                              &
     &     (command,option,commandFull,optionFull,                    &
     &      commandIndex,optionIndex,iCommand,iOption,                &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
!D Write (6,*) "          iOption:   ", iOption
!D Write (6,*) "          iArgument: ", iArgument
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (vars(iIn,iBack)%name .Eq. bac%VESPA                              &
        &  .And. iBack .Ne. GPnoneI .And. iPart .Ne. GPnoneI) Then
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         !
         Call SIC_CH(LINE,iOption,iArgument,                             & 
              &           cInput,lengthInput,.True.,errorC)
         !
         !D       Write (6,*) "          cInput ->",cInput(1:lengthInput),"<-"
         !
         !**   Check for valid backend modes **
         !
         If (.Not. errorC) Then 
            Call pakoUmatchKey (                                         &
                 &              keys=BEmodeChoices,                      &
                 &              key=cInput,                              &
                 &              command='BACKEND',                       &
                 &              howto='Start Upper',                     &
                 &              iMatch=iMatch,                           &
                 &              nMatch=nMatch,                           &
                 &              error=errorC,                            &
                 &              errorCode=errorCode                      &
                 &             )
         End If
         !
         If (.Not. errorC) Then
            vars(iIn,iBack)%mode        = BEmodeChoices(iMatch)
         End If
         !
      Else
         !
         errorL = .True.
         messageText =                                                      &
              &        "must specify a valid backend mode"
         Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
         !
      End If
      !
   Else
      !
      errorL = .True.
      messageText =                                                      &
           &        "only valid for backend "//bac%VESPA
      Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
      !
   End If
   !
End If
!
!D Write (6,*) "      vars(iIn,iBack)%mode ", vars(iIn,iBack)%mode
!
!!$Else
!!$   !
!!$   errorL = .True.
!!$   messageText =                                                      &
!!$        &        " only allowed for backend "//bac%VESPA
!!$   Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
!!$   !  
!!$End IF
!
ERROR = ERROR .Or. errorC .Or. errorL
!
