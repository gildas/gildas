!
! doDisconnectBE.inc,v 0.9 205-07-12 Hans Ungerechts
!
vars(iIn,:)     =  beReset
vars(iTemp,:)   =  beReset
vars(iValue,:)  =  beReset
!
If      (iBack.Eq.0 .And. iPart.eq.0) Then
   !
   listBE(:,:)%isConnected = .False.
   !
Else If (iBack.Ge.1 .And. iPart.Eq.0) Then
   !
   listBE(iBack,:)%isConnected = .False.
   !
Else If (iBack.Ge.1 .And. iPart.Ge.1) Then
   !
   listBE(iBack,iPart)%isConnected = .False.
   !
End If
!
