  !
  !  Id: optionTuneOffsets.inc,v 1.2.3 2014-12-04 Hans Ungerechts
  !  Id: optionTuneOffsets.inc,v 1.2.3 2014-08-28 Hans Ungerechts
  !  Id: optionTuneOffsets.inc,v 1.2.3 2014-08-27 Hans Ungerechts
  !
  !  Family:     Option --> 2 positions or logical
  !  Cousins:    optionReference
  !
  !D   Write (6,*) "   --> optionTuneOffsets.inc,v 1.2.3 2014-12-04 "
  !
  Option = 'TUNE'
  !
  errorM = .False.
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  vars(iIn)%doTuneSet = .False.
  vars(iIn)%doTune    = .False.
  !
  Call queryReceiver(rec%Bolo, isConnected, bolometerName = bolometerName)
  !
  !D   Write (6,*) "    bolometer isConnected:    ", isConnected
  !D   Write (6,*) "              bolometerName:  ", bolometerName
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (    .Not. (      ( isConnected .And. bolometerName.Eq.bolo%NIKA )       &
          &          .Or. ( isConnected .And. bolometerName.Eq.bolo%GISMO )      &
          &          .Or.   GV%privilege.Eq.setPrivilege%staff                   &
          &          .Or.   GV%privilege.Eq.setPrivilege%ncsTeam                 &
          &        )      )  Then                                                !
        !
        errorL = .True.
        vars(iIn)%doTuneSet = .False.
        vars(iIn)%doTune    = .False.
        !
        Write (messageText,*)  " requires RECEIVER bolometer NIKA | GISMO "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        !
     Else
        !
        vars(iIn)%doTuneSet = .True.
        !
     End If
     !
     ERROR = ERROR.Or.errorL
     !
     If (SIC_NARG(iOption).Eq.0) Then
        !
        vars(iIn)%doTune = .True.
        !
     Else If (SIC_NARG(iOption).Eq.1) Then
        !
        iArgument = 1
        !
        If (SIC_PRESENT(iOption,iArgument)) Then
           Call SIC_L4(LINE,iOption,iArgument,                                   &
                &           vars(iIn)%doTune,.False.,errorL)                     !
        End If
        !
        ERROR = ERROR .Or. errorL
        !
     Else If (SIC_NARG(iOption).Ge.2 .And. SIC_NARG(iOption).Le.2) Then
        !
        If (.Not. errorR .And. SIC_NARG(iOption).Ge.1) Then
           iArgument = 1
           Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                &
                &              .False.,errorC)                                   !
           If (cInput(1:1).Ne.'*') Then
              Call SIC_R4(LINE,iOption,iArgument,                                &
                   &                 rInput,.False.,errorR)                      !
              If (.Not. errorR) Call checkR4angle(option,rInput,                 &
                   &                 vars%offsetTune%x,                          &
                   &           GV%angleFactorR,                                  &
                   &                 errorR)                                     !
              If (.Not. errorR) Then
                 vars(iIn)%doTune = .True.
              End If
           End If
        End If
        !
        If (.Not. errorR .And. SIC_NARG(iOption).Ge.2) Then
           iArgument = 2
           Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                &
                &              .False.,errorC)                                   !
           If (cInput(1:1).Ne.'*') Then
              Call SIC_R4(LINE,iOption,iArgument,                                &
                   &                 rInput,.False.,errorR)                      !
              If (.Not. errorR) Call checkR4angle(option,rInput,                 &
                   &                 vars%offsetTune%y,                          &
                   &           GV%angleFactorR,                                  &
                   &                 errorR)                                     !
           End If
        End If
        !
     Else If (SIC_NARG(iOption).Ge.3) Then
        !
        Write (messageText,*)  "too many arguments "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        errorL = .True.
        !
     End If
     !
     ERROR = ERROR .Or. errorM .Or. errorL .Or. errorR .Or. errorC
     !
  End If
  !
