  !
  !  Id: optionPurpose.inc ,v 1.2.3 2014-10-15 Hans Ungerechts
  !
  !D   Write (6,*) '      optionPurpose.f90 ,v 1.2.3 2014-10-15'
  !
  OPTION = 'PURPOSE'
  !
  Call indexCommmandOption                                                       & 
       &     (command,option,commandFull,optionFull,                             & 
       &      commandIndex,optionIndex,iCommand,iOption,                         & 
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
!!$ vars(iIn)%hasPurpose   = .False.
!!$ vars(iIn)%Purpose      = GPnone
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     vars(iIn)%hasPurpose   = .False.
     vars(iIn)%Purpose      = GPnone
     !
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_CH(LINE,iOption,iArgument,                                      & 
             &           cInputLong,lengthInput,.True.,errorC)                   !
        l = Len_trim(cInputLong)
        If (l.Gt.24) Then
           errorC = .True.
           Write (messageText,*) 'value: "', cInputLong(1:l),                    &
                &                " longer than 24 characters"                    !
           Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,            &
                &           messageText)                                         !
        End If
        !
        Call pakoUmatchKey (                                                     &
             &              keys=purposeChoices,                                 &
             &              key=cInputLong,                                      &
             &              command=command//"/"//option,                        &
             &              howto='Start Upper',                                 &
             &              iMatch=iMatch,                                       & 
             &              nMatch=nMatch,                                       &
             &              error=errorM,                                        &
             &              errorCode=errorCode                                  &
             &             )                                                     !
        !
        !D         Write (6,*) "      errorM:   ", errorM
        If ( .Not. errorM) Then
           Write (messageText,*) ' set to "', purposeChoices(iMatch),            &
                &                        '"'                                     !
           Call PAKOMESSAGE(priorityI,severityI,command//"/"//option,            &
                &           messageText)                                         !
        Else
           Write (messageText,*) 'value "', cInputLong(1:l),                     &
                &                        '" is not a standard Purpose'           !
           Call PAKOMESSAGE(       77,severityW,command//"/"//option,            &
                &           messageText)                                         !
           Write (messageText,*) 'value "', cInputLong(1:l),                     &
                &                        '" is deprecated'                       !
           Call PAKOMESSAGE(       77,severityW,command//"/"//option,            &
                &           messageText)                                         !
        End If
        !
        If (errorC) Then
           Write (messageText,*) 'value: "', cInputLong(1:l), '" not valid'
           Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,            &
                &           messageText)                                         !
        End If
        If (.Not. ErrorC) Then
           vars(iIn)%hasPurpose   = .True.
           vars(iIn)%Purpose      = cInputLong(1:l)
        End If
     Else
        vars(iIn)%hasPurpose   = .False.
        vars(iIn)%Purpose      = GPnone
        messageText = "no Purpose set"
        Call PAKOMESSAGE(priorityW,severityW,command//"/"//option,               &
             &           messageText)                                            !
     End If
     !
  End If
  !     
  ERROR = ERROR .Or. errorC 
  !


