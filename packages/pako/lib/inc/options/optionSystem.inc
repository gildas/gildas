!
! Id: optionSystem.inc,v 1.0.4 2006-07-13
!
! TBD: default system based on SOURCE command
!
OPTION = 'SYSTEM'
!
errorM = .False.
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &           cInput,lengthInput,.True.,errorC)
      cInputUpper = cInput
      Call SIC_UPPER(cInputUpper)
      !
   Else
      errorC = .True.
      messageText = "parameter systemName is required"
      Call pakoMessage(priorityE,severityE,                                      &
           &                   command//"/"//option,messageText)
   End If
   !
   !**   search for cInputUpper in offsetSystemChoices   ***
   !
   ERROR = ERROR .Or. errorC
   !
   If (.Not. Error) Then
      !
      Call pakoUmatchKey (                                                       &
           &              keys=offsetChoicesPako,                                &
           &              key=cInputUpper,                                       &
           &              command=command//"/"//option,                          &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         & 
           &              nMatch=nMatch,                                         &
           &              error=errorM,                                          &
           &              errorCode=errorCode                                    &
           &             )
      !
   End If
   !
   If (.Not. errorM) Then
      If (        iMatch.Eq.ipro                                                 &
!!           & .Or. iMatch.Eq.ibas                                               &
           & .Or. iMatch.Eq.itru                                                 &
           & .Or. iMatch.Eq.ihor           ) Then
         vars(iIn)%systemName = offsetChoicesPako(iMatch)
         vars(iIn)%iSystem    = iMatch
         !D         Write (6,*) "      vars(iIn)%iSystem: ", vars(iIn)%iSystem  
      Else
         Write (messageText,*)  offsetChoicesPako(iMatch),                       &
              &      "is not yet implemented "
         Call pakoMessage(priorityW,severityW,                                   &
              &       command//"/"//OPTION,messageText)
         errorM = .True.
      End If
   End If
   !
End If
!
ERROR = ERROR .Or. errorM
!
