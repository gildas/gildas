!
!     Id: optionTphase.inc,v 1.2.0 2012-01-26 Hans Ungerechts
!     Family:   option 1 real parameter
!     Siblings: optionTotf.f 
!               optionTrecord.f
!               optionTsegment.f
!               optionTphase.f90
!               optionTblanking.f90
!
      errorR = .False.
!
!!obsolete    iOption = 3 
      Option  = 'TPHASE'
!
      Call indexCommmandOption                                                   &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
      iArgument = 1
!
      If (SIC_PRESENT(iOption,0)) Then
         Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
         !
         ! D Write (6,*) "     Option, rInput ", Option, rInput
         !
         If (.Not.errorR) Then
            Call checkR4co(command,option,rInput,                                &
     &           vars%tPhase,                                                    &
     &           errorR)                                                         !
         End If
      End If
!
      ERROR = ERROR .Or. errorR
!



