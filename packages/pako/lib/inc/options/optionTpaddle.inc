!
! Id: optionTpaddle.inc,v 1.0.10.7 2008-10-29 Hans Ungerechts
! Family:   option 1 real parameter
! Siblings: optionTpaddle.f90
!           optionTotf.f 
!           optionTrecord.f
!           optionTsegment.f
!           optionTphase.f90
!           optionTblanking.f90
!
!D      write (6,*) '      optionTpaddle.f90 '
!
errorL = .False.
errorR = .False.
!
Call queryReceiver(rec%Bolo, isConnected)
!
Option = 'TPADDLE'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (isConnected) Then
      !
      Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
      If (.Not.errorR) Then
         Call checkR4co(command,option,rInput,                                   &
              &           vars%tPaddle,                                          &
              &           errorR)                                                !
      End If
      !
   Else
      !
      errorL = .True.
      Write (messageText,*) "only valid with MAMBO bolometer"
      Call pakoMessage(priorityE,severityE,"TIP /tPaddle",messageText)
      !
   End If
   !
End If
!
ERROR = ERROR .Or. errorL .Or. errorR
!



