!
!     Id: saveDoubleBeam.inc,v 1.0.6.1 2007-01-13 Hans Ungerechts
!
If (vars(iValue)%doDoubleBeam) Then
   lineOut = " /doubleBeam yes"
Else
   lineOut = " /doubleBeam no"
End If
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!

