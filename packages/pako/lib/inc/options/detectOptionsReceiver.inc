!
! Id: detectOptionsReceiver.inc,v 1.1.1 2009-04-16 Hans Ungerechts
!
!D Write (6,*) "      --> detectOptionsReceiver.inc   v1.1.1"
!
! *** check for options
!
OPTION = 'DEFAULT'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasDefaults = SIC_PRESENT(iOption,0)
!
OPTION = 'CLEAR'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
hasClear = SIC_PRESENT(iOption ,0)
!
OPTION = 'CONNECT'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
hasConnect = SIC_PRESENT(iOption,0)
!
OPTION = 'DISCONNECT'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasDisconnect = SIC_PRESENT(iOption,0)
!
OPTION = 'CENTERIF'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasCenterIf = SIC_PRESENT(iOption,0)
!
OPTION = 'DEROTATOR'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasDerotator = SIC_PRESENT(iOption,0)
!
OPTION = 'DOPPLER'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasDoppler = SIC_PRESENT(iOption,0)
!
OPTION = 'EFFICIENCY'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasEfficiency = SIC_PRESENT(iOption,0)
!
OPTION = 'FOFFSET'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasFoffset = SIC_PRESENT(iOption,0)
!
OPTION = 'GAINIMAGE'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasGainImage = SIC_PRESENT(iOption,0)
!
OPTION = 'SBAND'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasSband = SIC_PRESENT(iOption,0)
!
OPTION = 'SCALE'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasScale = SIC_PRESENT(iOption,0)
!
OPTION = 'TEMPLOAD'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasTempLoad = SIC_PRESENT(iOption,0)
!
OPTION = 'WIDTH'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        ! 
hasWidth = SIC_PRESENT(iOption,0)
!
!D Write (6,*) "          hasDefaults: ", hasDefaults
!D Write (6,*) "          hasClear: ", hasClear
!D Write (6,*) "          hasConnect: ", hasConnect
!D Write (6,*) "          hasCenterIf: ", hasCenterIf
!D Write (6,*) "          hasDisconnect: ", hasDisconnect
!D Write (6,*) "          hasDerotator: ", hasDerotator
!D Write (6,*) "          hasDoppler: ", hasDoppler
!D Write (6,*) "          hasEfficiency: ", hasEfficiency
!D Write (6,*) "          hasFoffset: ", hasFoffset
!D Write (6,*) "          hasGainImage: ", hasGainImage
!D Write (6,*) "          hasSband: ", hasSband
!D Write (6,*) "          hasScale: ", hasScale
!D Write (6,*) "          hasTempLoad: ", hasTempLoad
!D Write (6,*) "          hasWidth: ", hasWidth


