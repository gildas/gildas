!
! doDisconnectRX.inc,v 0.9 205-07-12 Hans Ungerechts
!
If      (iRec.Eq.0) Then
   !
   vars(iIn,:)%isConnected = .False.
   !
Else If (iRec.Ge.1) Then
   !
   vars(iIn,iRec)%isConnected = .False.
   !
End If
!
