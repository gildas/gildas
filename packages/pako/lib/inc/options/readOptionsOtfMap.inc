  !
  !   Id: readOptionsOtfMap.inc, v 1.2.3 2014-08-28 Hans Ungerechts
  !   Id: readOptionsOtfMap.inc, v 1.2.3 2014-05-28 Hans Ungerechts
  !
  !  Family:   readOptions for observing mode
  !  Siblings: readOptionsOtfMap.inc
  !  Siblings: readOptionsTip.inc
  !
  Include 'inc/options/optionBalance.inc'
  Include 'inc/options/optionCroLoop.inc'
  Include 'inc/options/optionNotf.inc'
  Include 'inc/options/optionReference.inc'
  Include 'inc/options/optionResume.inc'
  Include 'inc/options/optionStep.inc'
  Include 'inc/options/optionSystem.inc'
  Include 'inc/options/optionSpeed.inc'
  Include 'inc/options/optionTotf.inc'
  Include 'inc/options/optionTrecord.inc'
  Include 'inc/options/optionTreference.inc'
  Include 'inc/options/optionZigzag.inc'
  !
  Include 'inc/options/optionTuneOffsets.inc'
  Include 'inc/options/optionTuneTime.inc'
  !
