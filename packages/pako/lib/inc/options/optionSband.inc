!
!  Id: optionSband.inc,v      1.1.11 2011-09-29 Hans Ungerechts
!
!                             1.1.11 EMIR upgrade
!
!  FAMILY:    
!  Siblings:  optionSband
!
!D Write (6,*) "      --> optionSband.inc,v "
!
OPTION = 'SBAND'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
l  =  Len_trim(vars(iIn,iRec)%name(1:l))
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (.Not.   (                                                                 &
        &      vars(iIn,iRec)%name .Eq. rec%E090                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E150                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E230                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E300 )                               &
        & ) Then                                                                 !
      errorL = .True.
      messageText = "only valid for EMIR bands "
      Call PakoMessage(priorityE,severityE,                                      &
           &   command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText) !
   End If
   !
   nArgument = SIC_NARG(iOption)
   !
   If (.Not. errorL) Then
      !
      If (nArgument.Lt.1) Then
         errorL = .True.
         messageText = "no subband specified "
         Call PakoMessage(priorityW,severityW,                                   &
              & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText)!
      End If
      !
   End If
   !
   If (.Not. errorL .And. nArgument.Ge.1) Then
      !
      iArgument = 1
      !
      Call SIC_CH(LINE,iOption,iArgument,                                        & 
           &           cInput,lengthInput,.True.,errorC)                         !
      !
      ERROR = ERROR .Or. errorC .Or. errorL
      !
      ! *** Check against valid codes
      !
      If (.Not. ERROR) Then
         Call pakoUmatchKey (                                                    &
              &              keys=subbandChoices,                                &
              &              key=cInput,                                         &
              &              command=command//"/"//option,                       &
              &              howto='Start Upper',                                &
              &              iMatch=iMatch,                                      &
              &              nMatch=nMatch,                                      &
              &              error=errorC,                                       &
              &              errorCode=errorCode                                 &
              &             )                                                    !
         !
         l2 = Len_trim(subbandChoices(iMatch))
         ERROR = ERROR .Or. errorC
         !
         If    (         GV%iUserLevel .Lt. iExperienced                         &
              &  .And. (iMatch.Eq.iLSB .Or. iMatch.Eq.iUSB) ) Then               !
            errorL = .True.
            messageText = subbandChoices(iMatch)//                               &
                 &        "requires higher user level"                           !
            Call PakoMessage(priorityE,severityE,                                &
                 & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,         &
                 & messageText)                                                  !
         End If
         ERROR = ERROR .Or. errorL
         !
!!$      If ( (iRec.Eq.iE150.Or.iRec.Eq.iE230.Or.iRec.Eq.iE300)                  & !!  EMIR upgrade:
         If ( (iRec.Eq.iE150)                                                    & !!  v 1.1.11
              &    .And. (iMatch.Eq.iLo .Or. iMatch.Eq.iUo)                      &
              &  ) Then                                                          !
            !                     
            errorL = .True.
            messageText = "subband: "                                            &
                 &       //subbandChoices(iMatch)(1:l2)//                        &
                 &        " does not exist for this band "                       !
            Call PakoMessage(priorityE,severityE,                                &
                 & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,         &
                 & messageText)                                                  !
            !
         End If
         ERROR = ERROR .Or. errorL
         !
      End If
      !
      If (.Not. ERROR) Then
         !
         vars(iIn,iRec)%sideBand = subbandChoices(iMatch)
         !
      End If
      !
   End If
   !
End If
