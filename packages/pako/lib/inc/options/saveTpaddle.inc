!
! Id: saveTpaddle.inc,v 1.0.10.7 2009-10-29 Hans Ungerechts
!
Call queryReceiver(rec%Bolo, isConnected)
!
If (isConnected) Then
   !
   Write (lineOut,*) "/tPaddle ", vars(iValue)%tPaddle
   Include 'inc/options/caseContinuationLine.inc'
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
