!
!     Id: optionClearOffsets.inc,v 1.0.4 2009-02-19 Hans Ungerechts
!
!D Write (6,*) " optionClearOffsets -->"
!D Write (6,*) "      iiSystemOffset:     ", iiSystemOffset
!D Write (6,*) "      ccSystemOffset:     ", ccSystemOffset
!
OPTION = 'CLEAR'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   doClear = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,doClear,.True.,errorL)
   End If
Else
   doClear = .False.
End If
ERROR = ERROR .Or. errorL
!
If (doClear .And. .Not. Error) Then
   !
   If (iiSystemOffset.Eq.iNas) Then
      varsOffsets(iIn,iNas)%isSet = .False.
      varsOffsets(iIn,iNas)%point = xyPointType(0.0,0.0)
   Else
      Do ii = 1, nDimOffsetChoices, 1
         varsOffsets(iIn,ii)%isSet = .False.
         varsOffsets(iIn,ii)%point = xyPointType(0.0,0.0)
      End Do
   End If
   !
   Write (messageText,*)                                                         &
        & " /SYSTEM ", offs%nas,                                                 &
        & " are cleared: will use automatic values "                             !
   Call  pakoMessage(666,severityW,command,messageText)
   !
   iiSystemOffset = GPnoneI
   ccSystemOffset = GPnone
   !
End If
