!
!     $Id$
!
      if (vars(iValue)%doVariable) then
         write (lineOut,*) CMD(1:lCMD),                                  &
     &        " /variable ",                                             &
     &        vars(iValue)%tempVariable
         write (iUnit,*) lineOut(1:lenc(lineOut))
      else
         write (iUnit,*) CMD(1:lCMD),                                    &
     &        " /variable ", "no"
      end if
!
