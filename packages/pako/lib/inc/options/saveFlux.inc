!
!     $Id$
!
      write (c1,*) vars(iValue)%flux
      l1 = lenc(c1) 
      write (c2,*) vars(iValue)%index
      l2 = lenc(c2) 
      lineOut = ' /flux '//c1(1:l1)//' '//c1(1:l2)
!
      Include 'caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
