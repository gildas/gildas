!
!     Id: optionTempLoad.inc,v 1.2.0 2012-07-17 Hans Ungerechts
!     Id: optionTempLoad.inc,v 1.0.8 2007-09-15 Hans Ungerechts
!     Family:   
!               
!               
!
!     Siblings: optionEfficiency.f90
!               optionTempLoad.f90
!
errorC = .False.
errorR = .False.
!
Option = 'TEMPLOAD'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
If (SIC_PRESENT(iOption,0)) Then
   !
   !D   Write (6,*) '   --> optionTempLoad.inc: ',                                    &
   !D        &      ' vars(iIn,iRec)%name: ',                                         &
   !D        &        vars(iIn,iRec)%name                                             !
   !D   Write (6,*) '   --> optionTempLoad.inc: ',                                    &
   !D        &    ' vars(iIn,iRec)%frequency%value: ',                                &
   !D        &      vars(iIn,iRec)%frequency%value                                    !
   !
   ! *** New evaluation of L(ookup) (only for non-HERA RXs)
!!$   If (GV%privilege.Eq.setPrivilege%ncsTeam                                      &
!!$        &   .And. iRec.Ne.iHERA .And. iRec.Ne.iHERA1 .And. iRec.Ne.iHERA2) Then  !
   If (          iRec.Ne.iHERA .And. iRec.Ne.iHERA1 .And. iRec.Ne.iHERA2) Then  !
      !
      !D       Write (6,*) '   --> optionTempLoad.inc *** New evaluation of L(ookup) '
      !
      If (SIC_NARG(iOption).Lt.1. .Or. SIC_NARG(iOption).Gt.2) Then
         messageText =                                                           &
              &   '1 or 2 parameters tempCold [tempAmbient] are required'        !
         Call PakoMessage(priorityE,severityE,command//" /"//option,messageText)
         messageText =                                                           &
              &   '1 or 2 parameters can be Real or "L" (lookup)'                !
         Call PakoMessage(priorityI,severityI,command//" /"//option,messageText)
         errorR = .True.
      End If
      ERROR = ERROR .Or. errorR
      !
      If (.Not. ERROR) Then
         iArgument = 1
         !D          Write (6,*) " optionTempLoad: cold load " 
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &                  cInput,lengthInput,.False.,errorC)           !
            Call SIC_UPPER(cInput)
            If (cInput(1:1).Eq.'L') Then
               !D                Write (6,*) " optionTempLoad: L lookup " 
               vars(iIn,iRec)%tempColdCode = 'L'
!!$               If (   coldLimits(1,iRec)%frequency%value.Le.                     &
!!$                    &     vars(iIn,iRec)%frequency%value                         &
!!$                    & .And.                                                      &
!!$                    & coldLimits(2,iRec)%frequency%value.Ge.                     &
!!$                    &     vars(iIn,iRec)%frequency%value       ) Then            !
!!$                  vars(iIn,iRec)%tempColdCode = 'L'
!!$                  messageText =                                                  &
!!$                       &   'tempColdLoad will be taken from online look-up'      !
!!$                  Call PakoMessage(priorityI,severityI,                          &
!!$                       &           command//" /"//option,messageText)            !
!!$               Else
!!$                  vars(iIn,iRec)%tempColdCode = GPnone
!!$                  errorC = .True.
!!$                  Write (messageText,'(a,f12.6,a)')                              &
!!$                       &   'frequency ', vars(iIn,iRec)%frequency%value,         &
!!$                       &   ' out of range for online look-up of tempColdLoad'    !    
!!$                  Call PakoMessage(priorityE,severityE,                          &
!!$                       &           command//" /"//option,messageText)            !
!!$                  Write (messageText,'(a,f12.6,a,f12.6)')                        &
!!$                       &   'range for online look-up of tempColdLoad: ',         &    
!!$                       &      coldLimits(1,iRec)%frequency%value,                &
!!$                       &   ' to: ',                                              &    
!!$                       &      coldLimits(2,iRec)%frequency%value                 !
!!$                  Call PakoMessage(priorityI,severityI,                          &
!!$                       &           command//" /"//option,messageText)            !
!!$               End If
            End If
            If (cInput(1:1).Ne.'L' .And. cInput(1:1).Ne.'*') Then
               !D                Write (6,*) " optionTempLoad: not L nor * " 
               vars(iIn,iRec)%tempColdCode = GPnone
               Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
               If (.Not.errorR) Then
                  Call checkR4co(command//" /"//option,option,rInput,            &
                       &                           vars(:,iRec)%tempCold,        &
                       &                           errorR)                       !
               End If
            End If
         End If
      End If
      ERROR = ERROR .Or. errorR .Or. errorC
      !
      If (.Not. ERROR) Then
         iArgument = 2
         !D          Write (6,*) " optionTempLoad: ambient load " 
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &                  cInput,lengthInput,.False.,errorC)           !
            Call SIC_UPPER(cInput)
            If (cInput(1:1).Eq.'L') Then
               !D                Write (6,*) " optionTempLoad: L lookup " 
               vars(iIn,iRec)%tempAmbientCode = 'L'
               messageText =                                                     &
                    &   'tempAmbientLoad will be taken from online look-up'      !
               Call PakoMessage(priorityI,severityI,                             &
                    &           command//" /"//option,messageText)               !
            End If
            If (cInput(1:1).Ne.'L' .And. cInput(1:1).Ne.'*') Then
               !D                Write (6,*) " optionTempLoad: not L nor * " 
               vars(iIn,iRec)%tempAmbientCode = GPnone
               Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
               If (.Not.errorR) Then
                  Call checkR4co(command//" /"//option,option,rInput,            &
                       &                           vars(:,iRec)%tempAmbient,     &
                       &                           errorR)                       !
               End If
            End If
         End If
      End If
      ERROR = ERROR .Or. errorR
      !
   Else
      ! *** OLD evaluation of L(ookup): deprecated!
      vars(iIn,iRec)%tempColdCode = GPnone
      vars(iIn,iRec)%tempAmbientCode = GPnone
      !
      If (SIC_NARG(iOption).Lt.1. .Or. SIC_NARG(iOption).Gt.2) Then
         messageText =                                                           &
              &     "1 or 2 real parameters tempCold [tempAmbient] are required" !
         Call PakoMessage(priorityE,severityE,command,messageText)
         errorR = .True.
      End If
      ERROR = ERROR .Or. errorR
      !
      iArgument = 1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,                                     &
              &                  cInput,lengthInput,.False.,errorC)              !
         Call SIC_UPPER(cInput)
         If (cInput(1:1).Eq.'L') Then
            !D Write (6,*) " optionTempLoad: L lookup " 
            !! TBD 2.0: needed?
!!$            Call tempColdLookup (vars(iIn,iRec)%name,                            &
!!$                 &      vars(iIn,iRec)%frequency%value,tempCold,errorR,errorCode)!
            If (errorR) Then
               Call PakoMessage(priorityE,severityE,command,errorCode)
            Else
               vars(iIn,iRec)%tempCold=tempCold
            End If
         End If
         If (cInput(1:1).Ne.'L' .And. cInput(1:1).Ne.'*') Then
            !D Write (6,*) " optionTempLoad: not L nor * " 
            Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
            If (.Not.errorR) Then
               Call checkR4co(command,option,rInput,                             &
                    &                           vars(:,iRec)%tempCold,           &
                    &                           errorR)                          !
            End If
         End If
      End If
      ERROR = ERROR .Or. errorR
      !
      iArgument = 2
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
         If (.Not.errorR) Then
            Call checkR4co(command,option,rInput,                                &
                 &                        vars(:,iRec)%tempAmbient,              &
                 &                        errorR)                                !
         End If
      End If
      ERROR = ERROR .Or. errorR
      !
   End If
!
End If
