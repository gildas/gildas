!
!
!     
      write (c1,*)                                                     &
     &         vars(iValue,ii)%effForward
      l1 = lenc(c1)
!
      write (c2,*)                                                     &
     &         vars(iValue,ii)%effBeam
      l2 = lenc(c2)
!
      lineOut = " /efficiency "//c1(1:l1)//" "//c2(1:l2)
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
