!
!     $Id$
!     Family:   option -- simple logical
!     Siblings: optionConnect.f90
!               optionBalance.f
!               optionCalibrate.f
!               optionFlagRef.f
!               optionMore.f
!               optionUpdate.f
!               optionWriteToSeg.f
!               optionZigzag.f
!
!
      OPTION = 'DISCONNECT'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!!$      errorL    = .False.
      iArgument = 1
!
      If (SIC_PRESENT(iOption,0)) Then
         vars(iIn,iRec)%isConnected = .False.
!
         If (.Not. errorL .And. SIC_NARG(0).Eq.0) Then
            If (.Not. vars(iIn,iRec)%isConnected) Then
               vars(iIn,:)%isConnected = .False.
            End If
         End If
!
      End If
!     
!!$      ERROR = ERROR .Or. errorL
!

