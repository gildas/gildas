!
!  Id: parametersCommandToSave.inc, v 0.9 2005-08-02 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: optionBalance.f
!               optionCalibrate.f
!               optionFlagRef.f
!               optionMore.f
!               optionUpdate.f
!               optionWriteToSeg.f
!               optionZigzag.f
!
!               optionAppend.f
!
!
OPTION = 'APPEND'
!
doAppend = .False.
!
Call indexCommmandOption                                                     & 
     &     (command,option,commandFull,optionFull,                           & 
     &      commandIndex,optionIndex,iCommand,iOption,                       & 
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!*    errorL    = .FALSE.
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   doAppend = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                    & 
           &           doAppend,.True.,errorL)
   Endif
Endif
ERROR = ERROR .Or. errorL
!
