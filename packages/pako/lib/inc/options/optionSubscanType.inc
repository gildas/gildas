!
!  optionSubscanType.inc,v 1.0.10 2008-10-15 Hans Ungerechts
!
OPTION = 'TYPE'
!
errorM = .False.
!
vars(iIn)%doTrack         =  .False.
vars(iIn)%doLinearOtf     =  .False.
vars(iIn)%doLissajousOtf  =  .False.
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &           cInput,lengthInput,.True.,errorC)
      cInputUpper = cInput
      Call SIC_UPPER(cInputUpper)
      !
   Else
      errorC = .True.
      messageText = "parameter subscanType is required"
      Call pakoMessage(priorityE,severityE,                                      &
           &                   command//"/"//option,messageText)
   End If
   !
   ! **   search for cInputUpper in segmentChoices   ***
   !
   ERROR = ERROR .Or. errorC
   !
   If (.Not. Error) Then
      !
      Call pakoUmatchKey (                                                       &
           &              keys=segmentChoices,                                   &
           &              key=cInputUpper,                                       &
           &              command=command//"/"//option,                          &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         & 
           &              nMatch=nMatch,                                         &
           &              error=errorM,                                          &
           &              errorCode=errorCode                                    &
           &             )
      !
   End If
   !
   If (.Not. errorM) Then
         vars(iIn)%segType = segmentChoices(iMatch)
         Select Case (segmentChoices(iMatch))
            Case (seg%Track)
               vars(iIn)%doTrack         =  .True.
            Case (seg%Linear)
               vars(iIn)%doLinearOtf     =  .True.
            Case (seg%Lissajous)
               vars(iIn)%doLissajousOtf  =  .True.
            End Select
   End If
   !
End If
!
ERROR = ERROR .Or. errorM
!
