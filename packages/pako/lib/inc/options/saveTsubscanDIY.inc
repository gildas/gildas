!
!  Id: saveTsubscanDIY.inc,v 1.0.10.5 2008-10-09 Hans Ungerechts
!
Write (lineOut,*)                                                                &
     &     "/tSubscan ", segList(ii)%tSegment                                    !
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
