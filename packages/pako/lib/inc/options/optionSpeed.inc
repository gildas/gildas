!
!     Id: optionSpeed.inc,v 0.9 2005-10-18 Hans Ungerechts
!
OPTION = 'SPEED'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &     commandIndex,optionIndex,iCommand,iOption,                            &
     &     errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_NARG(iOption).Eq.1.Or.SIC_NARG(iOption).Eq.2) Then
      iArgument = 1
      Call SIC_R4(LINE,iOption,iArgument,rInput1,.True.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput1,                       &
           &              vars%speedStart,                                       &
           &              GV%angleFactorR,                                       &
           &              errorR)
      vars(iIn)%speedEnd = vars(iIn)%speedStart
      If (SIC_NARG(iOption).Eq.2) Then
         iArgument = 2
         Call SIC_R4(LINE,iOption,iArgument,rInput2,.True.,errorR)
         If (.Not. errorR) Call checkR4angle(command,rInput2,                    &
              &              vars%speedEnd,                                      &
              &              GV%angleFactorR,                                    &
              &              errorR)
      End If
   Else 
      errorR = .True.
   End If
   !
   !     TBD: maybe this would be clearer using CASE:
   !
   If (command.Ne."OTFMAP") Then
      If (errorR) Then
         messageText =                                                           &
              &              "speed1 [speed2] -> 1 or 2 speeds are required"
         Call pakoMessage(priorityE,severityE,                                   &
              &                      command//"/"//option,messageText)
      End If
   End If
   !
   If (command.Eq."OTFMAP") Then
      errorR = errorR .Or.                                                       &
           &           SIC_NARG(iOption).Eq.2.And.                               &
           &           (vars(iIn)%speedEnd.Ne.vars(iIn)%speedStart)
      If (errorR) Then
         messageText =                                                           &
              &              "speed1 -> 1 uniform speed is required"
         Call pakoMessage(priorityE,severityE,                                   &
              &                      command//"/"//option,messageText)
      End If
   End If
   !
End If
!
ERROR = ERROR .Or. errorR
!
