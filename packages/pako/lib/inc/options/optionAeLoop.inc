!
!     Id: optionAeloop.inc,v 1.0.4 2006-07-03 Hans Ungerechts
!
OPTION = 'AELOOP'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
!D Write (6,*) command," : ",iCommand,"   ",option," : ",iOption
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &           cInput100,lengthInput,.True.,errorC)
      cInput100Upper = cInput100
      Call SIC_UPPER(cInput100Upper)
      !
      !     TBD: allow longer (variable) string
      !
      !D Write (6,*) " cInput100 = ->",cInput100(1:lengthInput),"<-"
      !
      !     search for cInput100 in aeCodeChoices
      !
      errorNotFound         = .False.
      !
      kk = 0
      !D Write (6,*) "      len_trim: ", Len_trim(cInput100Upper)
      !D Write (6,*) "  mod len_trim: ", mod(Len_trim(cInput100Upper),2)
      !
      If (cInput100Upper(1:1).Eq.'N') Then
         vars(iIn)%aeCode       = GPnone
         vars(iIn)%aeCodeBites  = GPnone
         vars(iIn)%nAeCode      = 0 
         Write (messageText,*) 'aeLoop set to none'
         Call pakoMessage(priorityI,severityI,command//"/"//option,messageText)
         errorC = .False.
      Else If (Mod(Len_trim(cInput100Upper),2) .Ne. 0) Then
         errorC = .True.
      Else
         !
         Do jj = 1,Len_trim(cInput100Upper)-1, 2
            !
            Call pakoUmatchKey (                                                    &
                 &              keys=azelChoices,                                   &
                 &              key=cInput100(jj:jj+1),                             &
                 &              command='POINTING',                                 &
                 &              howto='Start Upper',                                &
                 &              iMatch=iMatch,                                      &
                 &              nMatch=nMatch,                                      &
                 &              error=errorM,                                       &
                 &              errorCode=errorCode                                 &
                 &             )
            !
            If (.Not. errorM) Then
               kk = kk+1
            Else
               errorC = .True.
               Write (messageText,*) 'value: "',cInput100(jj:jj+1),'" not valid'
               Call pakoMessage(priorityE,severityE,command//"/"//option,messageText)
            End If
         End Do
         !
         !D Do jj = 1, 2*kk-1, 2
         !D   Write (6,*) "      ", jj, cInput100(jj:jj+1)
         !D End Do
         !
         If (.Not. errorC) Then
            vars(iIn)%aeCode       = cInput100Upper
            vars(iIn)%aeCodeBites  = cInput100Upper
            vars(iIn)%nAeCode      = kk
         End If
         !
      End If
      !
      If (errorC) Then
         Write (messageText,*) 'value: "',cInput100,'" not valid'
         Call pakoMessage(priorityE,severityE,command//"/"//option,messageText)
      End If
      !
   Else
      errorC = .True.
      messageText = "parameter aeCode is required"
      Call pakoMessage(priorityE,severityE,command//"/"//option,messageText)
   End If
End If
!
ERROR = ERROR .Or. errorC .Or. errorI
!
