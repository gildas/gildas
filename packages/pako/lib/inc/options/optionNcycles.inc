!
!  Id: optionNcycles.inc,v 1.1.3 2010-11-18 Hans Ungerechts
!  Family:   option -- simple integer
!  Siblings: optionFeBe
!  Siblings: optionNsubscans
!  Siblings: optionNcycles
!
errorI = .False.
errorL = .False.
!
OPTION = 'NCYCLES'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
!!$   If (         GV%privilege.Eq.setPrivilege%ncsTeam) Then                       
   If (         GV%privilege.Eq.setPrivilege%ncsTeam                             &
        &  .Or. GV%privilege.Eq.setPrivilege%staff) Then                         !
      !
      Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
      !
      If (.Not. errorI)                                                          &
           &      Call checkI4co(command,option,iInput,                          &
           &           vars%nCycles,                                             &
           &           errorI)                                                   !
      !
   Else
      !
      Include 'inc/options/nyi.inc'
      ERROR = ERROR .Or. errorL
      !
   End If
   !
Endif
!
ERROR = ERROR .Or. errorI
