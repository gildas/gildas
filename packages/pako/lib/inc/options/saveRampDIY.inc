!
!  Id: saveRampDIY.inc,v 1.1.2 2010-03-25 Hans Ungerechts
!
If (vars(iValue)%doRampUp) Then
   Write (lineOut,*) "/ramp up ", vars(iValue)%tRampUp 
Else
   Write (lineOut,*) "/ramp no "
End If
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
