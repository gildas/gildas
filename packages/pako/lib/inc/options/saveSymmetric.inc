!
!  Id: saveSymmetric.inc,v 1.0.2 2006-01-05 
!
If (vars(iValue)%doSymmetric) Then
   lineOut = " /symmetric yes"
Else
   lineOut = " /symmetric no"
End If
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
