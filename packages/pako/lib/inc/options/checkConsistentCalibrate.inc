!
!   Id: checkConsistentCalibrate.inc,v 1.1.14 2012-11-20 Hans Ungerechts
!
  !D   Write (6,*) "      --> checkConsistentCalibrate.inc,v 1.1.14 2012-11-20 "
  !D       write (6,*) '      vars(iIn)%doAmbient: ', vars(iIn)%doAmbient
  !D       write (6,*) '      vars(iIn)%doCold:    ', vars(iIn)%doCold
  !D       write (6,*) '      vars(iIn)%doSky:     ', vars(iIn)%doSky
  !
  nSubscans = 0
  !
  If  (vars(iIn)%doAmbient) nSubscans = nSubscans+1
  If  (vars(iIn)%doCold)    nSubscans = nSubscans+1
  If  (vars(iIn)%doGrid)    nSubscans = nSubscans+1
  If  (vars(iIn)%doSky)     nSubscans = nSubscans+1
  !
  If (nSubscans .Eq. 0) Then
     !
     messageText = 'no subcans specified'
     Call pakoMessage(priorityE,severityE,                                         &
          &                command//'/'//option,messageText)
     errorInconsistent = .True.
     !
  End If
  !
  If  (.Not. vars(iIn)%doSky)  Then
     !
     messageText = 'no Sky subscan specified'
     Call pakoMessage(priorityW,severityW,                                         &
          &                "calibrate",messageText)
     !
  End If
  !
  If (vars(iIn)%doGrid .And. vars(iIn)%doGainImage) Then
     !
     messageText = "incompatible options"
     Call pakoMessage(priorityE,severityE,                                         &
          &                command//"/GRID /GAINIMAGE",messageText)
     errorInconsistent = .True.
     !
  End If
  !
  Call queryReceiver(rec%Bolo, isConnected, bolometerName = bolometerName)
  !
  !D Write (6,*) "      rec%Bolo, isConnected: ", rec%Bolo, isConnected
  !D Write (6,*) "      bolometerName:         ", bolometerName
  !
  If      (       isConnected                                                    &
       &   .And. .Not. (      GV%privilege.Eq.setPrivilege%privileged            &
       &                 .Or. GV%privilege.Eq.setPrivilege%staff                 &
       &                 .Or. GV%privilege.Eq.setPrivilege%ncsTeam               &
       &                 .Or. GV%iUserLevel.Ge.iExperienced                      &
       &               )                                                         &
       &  ) Then                                                                 !
     !              
     messageText = "does not work with bolometers"
     Call pakoMessage(priorityE,severityE,command,messageText)
     errorInconsistent = .True.
     !                                                                                                                             
  End If
  !
