!
!     Id: savePercentage.inc,v 0.9 2005-10-10 Hans Ungerechts
!
!D Write (6,*) listBe(jj,ii)%percentageIsSet
!D Write (6,*) listBe(jj,ii)%percentage
!
!! If  (listBe(jj,ii)%percentageIsSet) Then
!
   Write (lineOut,*) "/percentage ", listBe(jj,ii)%percentage
!
   Include 'inc/options/caseContinuationLine.inc'
!
   Write (iUnit,*) lineOut(1:lenc(lineOut))
!
!! End If
!
