!
!   Id: saveBElineName.inc,v 1.0.10 2009-03-10 Hans Ungerechts
!
If  ( listBe(jj,ii)%lineNameIsSet ) Then
   !
   l = len_trim(listBe(jj,ii)%lineName)
   lineOut = ' /lineName "'//listBe(jj,ii)%lineName(1:l)//'"'
!
   Include 'inc/options/caseContinuationLine.inc'
!
   Write (iUnit,*) lineOut(1:lenc(lineOut))
!
End If
