!
!  Id: optionReference.inc,v 1.0.4 2006-07-10 Hans Ungerechts
!
Option = 'REFERENCE'
!
errorM = .False.
errorL = .False.
errorR = .False.
errorC = .False.
!
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_NARG(iOption).Eq.0) Then
      !
      vars(iIn)%doReference = .True.
      !
   Else If (SIC_NARG(iOption).Eq.1) Then
      !
      iArgument = 1
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_L4(LINE,iOption,iArgument,                                     &
              &           vars(iIn)%doReference,.False.,errorL)
      End If
      !
      ERROR = ERROR .Or. errorL
      !
   Else If (SIC_NARG(iOption).Ge.2 .And. SIC_NARG(iOption).Le.3) Then
      !
      If (.Not. errorR .And. SIC_NARG(iOption).Ge.1) Then
         iArgument = 1
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                  &
              &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,iOption,iArgument,                                  &
                 &                 rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4angle(option,rInput,                   &
                 &                 vars%offsetR%x,                               &
                 &           GV%angleFactorR,                                    &
                 &                 errorR)
            If (.Not. errorR) Then
               vars(iIn)%altRef = "OFFSETS"
            End If
         End If
      End If
      !
      If (.Not. errorR .And. SIC_NARG(iOption).Ge.2) Then
         iArgument = 2
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                  &
              &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,iOption,iArgument,                                  &
                 &                 rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4angle(option,rInput,                   &
                 &                 vars%offsetR%y,                               &
                 &           GV%angleFactorR,                                    &
                 &                 errorR)
         End If
      End If
      !
      If (.Not.errorR .And. SIC_NARG(iOption).Eq.3) Then
         iArgument = 3
         Call SIC_CH(LINE,iOption,iArgument,                                     &
              &                 cInput,lengthInput,.True.,errorC)
         cInputUpper = cInput
         Call SIC_UPPER(cInputUpper)
         !
         !**   search for cInput in choices    ***
         !
         ERROR = ERROR .Or. errorC
         !
         If (.Not. Error) Then
            !
            Call pakoUmatchKey (                                                 &
                 &              keys=offsetChoicesPako,                          &
                 &              key=cInputUpper,                                 &
                 &              command=command//"/"//option,                    &
                 &              howto='Start Upper',                             &
                 &              iMatch=iMatch,                                   & 
                 &              nMatch=nMatch,                                   &
                 &              error=errorM,                                    &
                 &              errorCode=errorCode                              &
                 &             )
            !
         End If
         !
         If (.Not. errorM) Then
            If (        iMatch.Eq.ipro                                           &
                 & .Or. iMatch.Eq.itru                                           &
                 & .Or. iMatch.Eq.ihor           ) Then
               vars(iIn)%systemNameRef = offsetChoicesPako(iMatch)
            Else
               Write (messageText,*)  offsetChoicesPako(iMatch),                 &
                    &      "is not yet implemented "
               Call pakoMessage(priorityW,severityW,                             &
                    &      command//"/"//OPTION,messageText)
               errorM = .True.
            End If
         End If
         !
      End If
      !
      vars(iIn)%doReference = .Not.errorM .And. .Not.errorR .And. .Not.errorC
      !
   Else If (SIC_NARG(iOption).Ge.4) Then
      !
      Write (messageText,*)  "too many arguments "
      Call pakoMessage(priorityE,severityE,                                      &
           &      command//"/"//OPTION,messageText)
      errorL = .True.
      !
   End If
   !
   ERROR = ERROR .Or. errorM .Or. errorL .Or. errorR .Or. errorC
   !
End If
!
