  !
  !     Id: savePhasesXY.inc,v 1.1.12 2012-03-05 Hans Ungerechts
  !
  Write(lineOut,*)                                                               &
       &           "/phases ",                                                   &
       &           vars(iValue)%phase                                            !
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !



