!
!     $Id$
!     Family:   option 1 real parameter
!     Siblings: 
!               optionTreference.f
!               optionTsegment.f
!
      Option = 'TSEGMENT'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      iArgument = 1
!
      If (SIC_PRESENT(iOption,0)) Then
         Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
         If (.Not.errorR) Then
            Call checkR4(option,rInput,                                &
     &           vars%tSegment, errorR)
         End If
      End If
!
      ERROR = ERROR .Or. errorR
!
