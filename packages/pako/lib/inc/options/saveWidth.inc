!
!     
!
If      (vars(iValue,ii)%name.Ne.rec%A100 .And.                              &
     &   vars(iValue,ii)%name.Ne.rec%B100) Then
!
   l = lenc(vars(iValue,ii)%width)
   lineOut =                                                                 &
        &   ' /width "'//vars(iValue,ii)%width(1:l)//'"'
   !
   Include 'inc/options/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If

