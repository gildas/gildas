!
!  Id: saveHorizontal.inc, v1.1.0 2009-03-09 Hans Ungerechts
!
Write (c1,*) vars(iValue,ii)%horizontalSubbands       
l1 = lenc(c1)
!
lineOut = " /horizontal "//c1(1:l1)
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
