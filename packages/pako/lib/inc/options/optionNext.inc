  !
  !     Id: optionNext.inc,v 1.2.3 2013-01-29 Hans Ungerechts
  !     Family:   option -- logical: if not present --> .True.
  !     Siblings: optionNext.f90
  !
  !               optionCalibrate.f90
  !               optionFlagRef.f90
  !               optionMore.f90
  !               optionUpdate.f90
  !               optionWriteToSeg.f90
  !               optionZigzag.f90
  !
  vars(iIn)%doNextSubscan = .True.
  !
  OPTION = 'NEXT'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  errorL    = .FALSE.
  !
  iArgument = 1
  If (SIC_PRESENT(iOption,0)) Then
     vars(iIn)%doNextSubscan = .True.
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_L4(LINE,iOption,iArgument,                                      &
             &                  vars(iIn)%doNextSubscan,.True.,errorL)           !
     Endif
  Endif
  ERROR = ERROR .Or. errorL
  !
