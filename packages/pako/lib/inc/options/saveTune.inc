!
!     Id: saveTune.inc,v 1.2.4 2015-05-27 Hans Ungerechts
!
  If (vars(iValue)%doTune) Then
     lineOut = " /tune yes"
  Else
     lineOut = " /tune no"
  End If
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
