  !
  !--------------------------------------------------------------------------------
  !
  ! <DOCUMENTATION name="optionFrequency">
  !   <VERSION>
  !                Id: optionFrequency.inc,v 1.2.2  2013-02-06
  !                Id: optionFrequency.inc,v 1.1.12 2012-03-01
  !   </VERSION>
  !   <HISTORY>
  !                2013-02-06: corrected error handling 
  !   <PROGRAM>
  !                Pako
  !   </PROGRAM>
  !   <FAMILY>
  !                Option
  !                   Lissajous
  !   </FAMILY>
  !   <SIBLINGS>
  !
  !   </SIBLINGS>        
  !
  !   Pako module for option Lissajous /Frequency
  !
  ! </DOCUMENTATION> <!-- name="optionFrequency" -->
  !
  !--------------------------------------------------------------------------------
  !
  !
  !D   Write (6,*) ' ---> optionFrequency '
  !D   Write (6,*) "               ERROR: ", ERROR
  !
  Option = 'FREQUENCY'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.0) Then
        !
        Write (messageText,*)  " expects 2 arguments: x y "
        Call pakoMessage(priorityI,severityI,                                    &
             &      command//"/"//OPTION,messageText)                            !
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.1) Then
        !
        iArgument = 1
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4(option,rInput,                         &
                &                 vars%frequency%x,                              &
                &                 errorR)                                        !
           If (.Not. errorR) vars(iIn)%omega%x = 2.0*Pi*vars(iIn)%frequency%x
        End If
        !
        If ( SIC_NARG(iOption).Eq.1                                              &
             &  .And.                                                            &
             &  Sqrt(2.0)*vars(iIn)%frequency%x .Lt.                             &
             &       Max(vars(iLimit1)%frequency%x,vars(iLimit2)%frequency%x)    &
             &  )    Then                                                        !
           If (.Not. errorR) vars(iIn)%frequency%y =                             &
                &                   Sqrt(2.0)*vars(iIn)%frequency%x              !
           If (.Not. errorR) vars(iIn)%omega%y = 2.0*Pi*vars(iIn)%frequency%y
        End If
        !
        ERROR = ERROR .Or. errorR .Or. errorL
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.2) Then
        !
        iArgument = 2
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4(option,rInput,                         &
                &                 vars%frequency%y,                              &
                &                 errorR)                                        !
           If (.Not. errorR) vars(iIn)%omega%y = 2.0*Pi*vars(iIn)%frequency%y
        End If
        !
        ERROR = ERROR .Or. errorR .Or. errorL
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.3) Then
        !
        Write (messageText,*)  "too many arguments "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        errorL = .True.
        !
     End If
     !
     ERROR = ERROR .Or. errorR .Or. errorL
     !
  End If
  !
  !D   Write (6,*) "               ERROR: ", ERROR
  !D   Write (6,*) ' <--- optionFrequency '
  !
