!
!     $Id$
!     Note:     NOT YET IMPLEMENTED
!     Family:   option -- simple logical
!     Siblings: optionAmbient.f90
!               optionBalance.f90
!               optionFast.f90
!               optionNull.f90
!               optionUpdate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
      OPTION = 'NULL'
!
      Call indexCommmandOption                                           &
     &     (command,option,commandFull,optionFull,                       &
     &      commandIndex,optionIndex,iCommand,iOption,                   &
     &      errorNotFound,errorNotUnique)
!
      errorL    = .False.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
!
         messageText = 'not yet implemented'
         Call pakoMessage(priorityE,severityE,                               &
     &                command//'/'//option,messageText)
         errorL = .True.
!
         vars(iIn)%doNull = .True.
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_L4(LINE,iOption,iArgument,                          &
     &                  vars(iIn)%doNull,.True.,errorL)
         Endif
      Endif
      ERROR = ERROR .Or. errorL
!
