!
!  Id: optionWidth.inc,v 1.1.0 2009-03-31 Hans Ungerechts
!
!  FAMILY:    choice of character strings
!  Siblings:  
!             optionDoppler
!             optionScale
!             optionWidth
!
!D Write (6,*) "   --> optionWidth "
!
OPTION = 'WIDTH'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (        vars(iIn,iRec)%name .Eq. rec%E090                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E150                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E230                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E300 ) Then                          !
      errorL = .True.
      messageText = "not valid for EMIR bands "
      Call PakoMessage(priorityE,severityE,                                      &
           &           command//"/"//option,messageText)                         !
   End If
   !
   If (.Not. errorL) Then
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,                                     & 
              &           cInput,lengthInput,.True.,errorC)                      !
      Else
         errorL = .True.
         messageText = "parameter width [string] is required"
         Call PakoMessage(priorityE,severityE,                                   &
              &           command//"/"//option,messageText)                      !
      End If
   End If
End If
!
ERROR = ERROR .Or. errorC .Or. errorL
!
! *** Check against valid codes
!
If (.Not. ERROR) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call pakoUmatchKey (                                                       &
           &              keys=widthChoices,                                     &
           &              key=cInput,                                            &
           &              command=command//"/"//option,                          &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         &
           &              nMatch=nMatch,                                         &
           &              error=errorC,                                          &
           &              errorCode=errorCode                                    &
           &             )                                                       !
      !
      ERROR = ERROR .Or. errorC
      !
      If (.Not. ERROR) Then
         If (iRec .Ne. iA100 .And. iRec .Ne. iB100) Then
            !
            ! NOTE:
            ! for RXs other than the 3mm RXs (A100 and B100)
            ! %centerIF and %IF2 are set to different values
            ! depending on whether the RXs are used in
            ! "narrow" (special for VESPA) or "wide" (standard) mode
            !
            vars(iIn,iRec)%width  = widthChoices(iMatch)
            If      (widthChoices(iMatch) .Eq. width%narrow) Then
               !D               Write (6,*) "      NARROW sets parameters "
               vars(iIn,iRec)%centerIF   =  IF%narrow
               vars(iIn,iRec)%IF2        =  IF2%narrow
            Else If (widthChoices(iMatch) .Eq. width%wide) Then
               !D               Write (6,*) "      WIDE   sets parameters "
               vars(iIn,iRec)%centerIF   =  IF%wide
               vars(iIn,iRec)%IF2        =  IF2%wide
            End If
         Else    !!  i.e., receiver A100 or B100:
            If      (widthChoices(iMatch) .Eq. width%narrow) Then
               vars(iIn,iRec)%width  = widthChoices(iMatch)
               !D               Write (6,*) "      NARROW sets parameters "
               vars(iIn,iRec)%centerIF   =  IF%low
               vars(iIn,iRec)%IF2        =  IF2%low
            Else 
               errorL = .True.
               messageText = "is fixed for to "//width%narrow//                  &
                    &        " for "//receiverChoices(iRec)                      !
               Call PakoMessage(priorityE,severityE,                             &
                    &           command//"/"//option,messageText)                !
            End If
         End If
      End If
      !
   End If
End If
!
ERROR = ERROR .Or. errorL
!

