  !
  !   Id: saveBEnSamples.inc ,v 1.3.0 2017-08-31 Hans Ungerechts
  !
  !
  contC = contNC
  l = lenc(vars(iValue,jj)%name)
  lineOut = ' "'//vars(iValue,jj)%name(1:l)//'"'
  !
  Include 'inc/parameters/caseContinuationLine.inc'
  !
  Write (iUnit,*) "!"
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
  contC = contCN
  Write (lineOut,*)                                                              & 
       &       " /nSamples ", vars(iValue,jj)%nSamples                           !
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
