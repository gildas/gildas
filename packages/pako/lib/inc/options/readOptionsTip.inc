  !
  !  Id: readOptionsTip,v 1.2.4    2015-04-24  Hans Ungerechts
  !  Id: readOptionsTip,v 1.2.3    2014-08-28  Hans Ungerechts
  !  Id: readOptionsTip,v 1.2.3    2014-05-29  Hans Ungerechts
  !  Id: readOptionsTip,v 1.0.10.7 2008-10-28  Hans Ungerechts
  !  
  !  Family:   readOptions for observing mode
  !  Siblings: readOptionsOtfMap.inc
  !  Siblings: readOptionsTip.inc
  !
  !----------------------------------------------------------------------
  !
  Include 'inc/options/optionSlew.inc'        !! to be done first 
  Include 'inc/options/optionAirmass.inc'     !! depends on /slew
  Include 'inc/options/optionTpaddle.inc'
  Include 'inc/options/optionTsubscan.inc'
  !
!!$  Include 'inc/options/optionTuneTimeLogical.inc'
  Include 'inc/options/optionTune.inc'
  Include 'inc/options/optionTuneTime.inc'
  !
