!
!     Id: optionGrid.inc,v 0.96 2005-12-05 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: optionAmbient.f90
!               optionBalance.f90
!               optionGrid.f90
!               optionUpdate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
OPTION = 'GRID'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   vars(iIn)%doGrid = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &                  vars(iIn)%doGrid,.True.,errorL)
   Endif
Endif
ERROR = ERROR .Or. errorL
!
