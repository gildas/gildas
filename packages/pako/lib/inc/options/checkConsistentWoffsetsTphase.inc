!
!     Id: checkConsistentWffsetsTphase.inc 2005-11-21 Hans Ungerechts
!
!D Write (6,*) vars(iIn)%wOffset
!D Write (6,*) vars(iIn)%wThrow
!D Write (6,*) vars(iIn)%tPhase
!
If (vars(iIn)%tPhase .Lt. 0.25+0.75*(vars(iIn)%wThrow-90.0)/(240.0-90.0)) Then
   Write (messageText,*)                                                         &
        &        "tPhase ", vars(iIn)%tPhase,                                    &
        &        "is too small for throw ", vars(iIn)%wThrow
   Call PakoMessage(priorityE,severityE,command,messageText)
   errorInconsistent = .True.
End If
!
! ** according to documentation / plot by Juan P.
!
