!
!     $Id$
!
      If (vars(iValue)%doCalibrate) Then
         lineOut = " /calibrate yes"
      Else
         lineOut = " /calibrate no"
      End If
!
      Include 'caseContinuationLine.f90'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
