  !
  !     Id: optionTcalibrate.inc,v 1.2.4 2015-07-06 Hans Ungerechts
  !     Id: optionTcalibrate.inc,v 1.1.2 2010-02-24 Hans Ungerechts
  !
  !     Family:   option 1 real parameter (time)
  !               but with privileged override of standard limits
  !
  !     Related:  optionTcalibrate.inc
  !               optionTotf.inc
  !               optionTref.inc
  !               optionTtune.inc
  !
  !               optionTrecord.inc
  !               optionTsegment.inc
  !               optionTphase.inc
  !               optionTblanking.inc
  !
  !D   Write (6,*) "                            "
  !D   Write (6,*) "     --> optionTcalibrate.f90 "
  !D   Write (6,*) "          GV%privilege:     ",  GV%privilege
  !D   Write (6,*) "          GV%userLevel:     ",  GV%userLevel
  !D   Write (6,*) "          iUserLevel:       ",  iUserLevel
  !
  errorR = .False.
  !
  Option = 'TCALIBRATE'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.1) Then
        iArgument = 1
        Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
     Else
        errorR = .True.
     End If
     !
  End If
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If       ( .Not. (       GV%privilege.Eq.setPrivilege%staff                 &
          &             .Or.  GV%privilege.Eq.setPrivilege%ncsTeam               &
          &             .Or.  GV%iUserLevel.Ge.iGuru                             &
          &           )                                                          &
          &   ) Then                                                             !
        !
        If (.Not.errorR) Then
           Call checkR4co(command,option,rInput,                                 &
                &           vars%tCalibrate,                                     &
                &           errorR)                                              !
        End If
        !
     Else
        !
        If      (      rInput .Lt. timeExtended%min                              &
             &     .Or.  rInput .Gt. timeExtended%max                            &
             &  ) Then                                                           !
           !
           errorR = .True.
           Write (messageText, *)                                                &
                &  " value ", rInput,                                            &
                &  " outside extended limits ", timeExtended%min,                &
                &  " to ",  timeExtended%max                                     !
           Call PakoMessage(priorityE,severityE,                                 &
                &           command//" /tCalibrate",messageText)                 !
           !
        Else
           vars(iIn)%tCalibrate = rInput
        End If
        !
        !D   Write (6,*) "   vars(iIn)%tCalibrate:   ", vars(iIn)%tCalibrate
        !
     End If
     !
  End If   !!   (SIC_PRESENT(iOption,0))
  !
  If (errorR) Then
     messageText =                                                               &
          &        "tCalibrate -> 1 real parameter is required"                  !
     Call PAKOMESSAGE(priorityI,severityI,&
          &command//"/"//option,messageText)
  End If
  !
  ERROR = ERROR .Or. errorR
  !




