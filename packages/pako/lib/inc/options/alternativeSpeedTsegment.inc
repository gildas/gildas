!
!     $Id$
!
!     TBD: use optionFull found by indexCommmandOption ...
!     TBD: use named indices to distinguish alternatives
!
      option = "SPEED"
      Call indexCommmandOption                                           &
     &     (command,option,commandFull,optionFull,                       &
     &      commandIndex,optionIndex,iCommand,iOption,                   &
     &      errorNotFound,errorNotUnique)
      alternative(1) = SIC_PRESENT(iOption,0)
!     TBD: handle errors, short call
!
      optionShort = "TSEGMENT"
      Call indexCommmandOption                                           &
     &     (command,optionShort,commandFull,optionFull,                  &
     &      commandIndex,optionIndex,iCommand,iOptionShort,              &
     &      errorNotFound,errorNotUnique)
      alternative(3) = SIC_PRESENT(iOptionShort,0)
!
      If (alternative(1) .And. alternative(3)) Then
         Write (messageText,*)                                           &
     &        "incompatible options: ",                                  &
     &        "/SPEED and /TSEGMENT"
         Call pakoMessage(priorityE,severityE,command,messageText)
         ERROR = .True.
      End If
!
