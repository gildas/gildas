  !
  !  Id: optionCenter.inc,v 1.1.12 2012-03-01
  !
  !  Family:   offset 
  !            CENTER       optionCenter.inc
  !
  !D   Write (6,*) ' ---> optionCenter '
  !
  Option = 'CENTER'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.0) Then
        !
        l = len_trim(GV%angleUnitSetC(1:l))
        Write (messageText,*)  " expects 2 arguments: x y "
        Call pakoMessage(priorityI,severityI,                                    &
             &      command//"/"//OPTION,messageText)                            !
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.1) Then
        !
        iArgument = 1
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4angle(option,rInput,                    &
                &                 vars%pCenter%x,                                &
                &                 GV%angleFactorR,                               &
                &                 errorR)                                        !
        End If
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.2) Then
        !
        iArgument = 2
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4angle(option,rInput,                    &
                &                 vars%pCenter%y,                                &
                &                 GV%angleFactorR,                               &
                &                 errorR)                                        !
        End If
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.3) Then
        !
        Write (messageText,*)  "too many arguments "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        errorL = .True.
        !
     End If
     !
     ERROR = ERROR .Or. errorR .Or. errorL
     !
  End If
  !
