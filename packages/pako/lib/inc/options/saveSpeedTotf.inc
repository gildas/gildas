!
!     $Id$
!
      if (vars(iValue)%altOption.eq."SPEED") then
         write (lineOut,*)                                               &
     &        "/speed ",                                                 &
     &        vars(iValue)%speedStart,                                   &
     &        vars(iValue)%speedEnd
      else 
         write (lineOut,*)                                               &
     &        "/tOtf ", vars(iValue)%tOtf
      end if
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
