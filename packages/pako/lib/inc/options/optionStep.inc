!
!     Id: optionStep.inc,v 0.9 2005-10-18 Hans Ungerechts
!
OPTION = 'STEP'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &     commandIndex,optionIndex,iCommand,iOption,                            &
     &     errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_NARG(iOption).Eq.1.Or.SIC_NARG(iOption).Eq.2) Then
      iArgument = 1
      Call SIC_R4(LINE,iOption,iArgument,rInput1,.True.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput1,                       &
           &              vars%delta%x,                                          &
           &              GV%angleFactorR,                                       &
           &              errorR)
      !
      If (.Not.errorR .And. SIC_NARG(iOption).Eq.2) Then
         iArgument = 2
         Call SIC_R4(LINE,iOption,iArgument,rInput2,.True.,errorR)
         If (.Not. errorR) Call checkR4angle(command,rInput2,                    &
              &              vars%delta%y,                                       &
              &              GV%angleFactorR,                                    &
              &              errorR)
      End If
   Else 
      errorR = .True.
   End If
   !
End If
!
If (errorR) Then
   messageText =                                                                 &
        &        "dx [dy] -> 1 or 2 angles are required"
   Call pakoMessage(priorityE,severityE,                                         &
        &                command//"/"//option,messageText)
End If
!
ERROR = ERROR .Or. errorR
