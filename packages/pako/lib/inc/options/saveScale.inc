!
!     
!
      l = lenc(vars(iValue,ii)%scale)
      lineOut =                                                          &
     &   ' /scale "'//vars(iValue,ii)%scale(1:l)//'"'
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
