!
!     $Id$
!     Family:   option -- simple integer
!     Siblings: optionFeBe
!     Siblings: optionNsubscans
!     Siblings: optionNphases
!
      errorI = .false.
!
      OPTION = 'NPHASES'
!D
!D    Write (6,*) "        start reading option: ", option
!D    Write (6,*) "        ERROR:        ", error
!D
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      iArgument = 1
!D
!D    Write (6,*) "   SIC_PRESENT(iOption,0): ", SIC_PRESENT(iOption,0)
!D
      If (SIC_PRESENT(iOption,0)) Then
         Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
!D
!D       Write (6,*) "        after SIC_I4: "
!D       Write (6,*) "        errorI:        ", errorI
!D
         If (.Not. errorI)                                             &
     &      Call checkI4co(command,option,iInput,                      &
     &           vars%nPhases,                                         &
     &           errorI)
!D
!D       Write (6,*) "        after checkI4: "
!D       Write (6,*) "        errorI:        ", errorI
!D
      Endif
!
      ERROR = ERROR .Or. errorI
!D
!D    Write (6,*) "        finished reading option: ", option
!D    Write (6,*) "        ERROR:        ", error
!D




