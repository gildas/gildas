!
!     $Id$
!
      OPTION = 'FUNCTION'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
      iArgument = 1
!
      If (SIC_PRESENT(iOption,0)) Then
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                        & 
     &           cInput,lengthInput,.True.,errorC)
            cInputUpper = cInput
            Call SIC_UPPER(cInputUpper)
!
!**   search for cInput in functionNameChoices   ***
!
            errorNotFound         = .False.
            errorNotUnique        = .False.
            choiceIndex = 0
!
            Do ii = 1,2,1
!           TBD: number of choices should be a variable
               If (Index(functionNameChoices(ii),                      &
     &              cInputUpper(1:lengthInput)).Eq.1) Then
                  If (choiceIndex .Eq. 0) Then
                     choiceIndex = ii
                  Else
                     errorNotUnique = .True.
                  End If
               End If
            End Do
            errorNotFound =  choiceIndex .Eq. 0
!
            errorC = errorC .Or. errorNotUnique .Or. errorNotFound
!
            If (.not.errorC) Then
               vars(iIn)%functionName =                                &
     &                   functionNameChoices(choiceIndex)
            Else If(errorNotFound) then
               Write (messageText,*) 'value: "',cInput,'" not valid'
               Call pakoMessage(6,3,command//"/"//option,messageText)
            Else If(errorNotUnique) then
               Write (messageText,*) 'value: "',cInput,'" not unique'
               Call pakoMessage(6,3,command//"/"//option,messageText)
            End If
!
         Else
            errorC = .True.
            messageText = "parameter functionName is required"
            Call pakoMessage(6,3,command//"/"//option,messageText)
         End If
      End If
!
      ERROR = ERROR .Or. errorC
!




