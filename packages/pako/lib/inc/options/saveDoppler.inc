!
!     
!
      l = lenc(vars(iValue,ii)%frequency%doppler)
      lineOut =                                                          &
     &   ' /doppler "'//vars(iValue,ii)%frequency%doppler(1:l)//'"'
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
