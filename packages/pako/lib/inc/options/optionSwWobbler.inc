!
!     Id: optionSwWobbler.inc,v 1.0.4 2006-07-27 Hans Ungerechts
!
OPTION = 'SWWOBBLER'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   vars(iIn)%doSwWobbler = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &  
           &                  vars(iIn)%doSwWobbler,.True.,errorL)
   End If
Else
   l = len_trim(command)
   If (GV%switchingMode .Eq. swMode%wobb) Then
      vars(iIn)%doSwWobbler = .True.
      Write (messageText,*)                                                      &
           &      "SWWOBBLER implies: /SWWOBBLER "
      Call pakoMessage(priorityI,severityI,command(1:l),messageText)
   Else
      vars(iIn)%doSwWobbler = .False.
      Write (messageText,*)                                                      &
           &      "non-Wobbler switching implies: /SWWOBBLER no "
      Call pakoMessage(priorityI,severityI,command(1:l),messageText)
   End If
Endif
!
ERROR = ERROR .Or. errorL
!
If (.Not. ERROR .And. vars(iIn)%doSwWobbler) Then
   !
   If (GV%switchingMode .Eq. swMode%wobb) Then
!!$            If (SIC_NARG(0).Eq.0) Then
!!$            vars(iIn)%offset%x =  -abs( (GV%wOffset(1)-GV%wOffset(2)) /2.0)      
      vars(iIn)%offset%x =  - GV%wOffset(1)   
      vars(iIn)%offset%y =    0.0
      vars(iIn)%systemName = offsPako%tru
      vars(iIn)%iSystem    =         itru
      Write (messageText,*)                                                      &
           &      "SWWOBBLER implies: xOffset, yOffset = ",                      &
           &      vars(iIn)%offset%x, vars(iIn)%offset%y,                        &
           &      " ", vars(iIn)%systemName
      Call pakoMessage(priorityI,severityI,command,messageText)
!!$            End If
   End If
   !
!!$         If (.NOT. SIC_PRESENT(iOption,0) .Or.                                &
!!$              & SIC_NARG(iOption).Eq.0) Then
   !
   If (GV%switchingMode .Eq. swMode%wobb) Then
!!$            vars(iIn)%offset%x =  -abs( (GV%wOffset(1)-GV%wOffset(2)) /2.0)      
      vars(iIn)%doReference = .True.
      vars(iIn)%offsetR%x =  - GV%wOffset(2)   
      vars(iIn)%offsetR%y =    0.0
      vars(iIn)%systemNameRef = offsPako%tru
      Write (messageText,*)                                                      &
           &      "SWWOBBLER implies: xOffsetR, yOffsetR = ",                    &
           &      vars(iIn)%offsetR%x, vars(iIn)%offsetR%y,                      &
           &      " ", vars(iIn)%systemNameRef
      Call pakoMessage(priorityI,severityI,command,messageText)
   End If
!!$         End If
   !
End If
!
