!
!     Id: optionTune.inc,v 1.1.13 2012-10-17 Hans Ungerechts
!     Family:   option -- logical with default to .False. if not present
!     Siblings: 
!
!D     Write (6,*) "          --> optionTune.inc,v 1.1.13 2012-10-17  "
!D     Write (6,*) "                          "
!
OPTION = 'TUNE'
!
vars(iIn)%doTune = .false.
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   !
   !! Include 'inc/options/nyi.inc'
   !
   vars(iIn)%doTune = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &                  vars(iIn)%doTune,.True.,errorL)                    !
   Endif
   !
Endif
ERROR = ERROR .Or. errorL
!
