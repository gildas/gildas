!
!     Id: saveAeLoop.inc,v 1.0.4 2006-07-05 Hans Ungerechts
!
If (vars(iValue)%aeCode .Ne. GPnone) Then
   !
   l = lenc(vars(iValue)%aeCode)
   lineOut = " /aeLoop "//'"'//vars(iValue)%aeCode(1:l)//'"'
   !
   Include 'inc/options/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
