!
!  Id: optionCroFlag.inc,v 1.0.10 2007-10-31 Hans Ungerechts 
!
!D Write (6,*) "       -> optionCroFlag"
!
OPTION = 'CROFLAG'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &           cInput,lengthInput,.True.,errorC)                         !
      cInputUpper = cInput
      Call SIC_UPPER(cInputUpper)
      !
      ! ***   TBD: search for cInput in croflagChoices   ***
      !
      If (errorC) Then
         Write (messageText,*) 'value: "',cInput,'" not valid'
         Call pakoMessage(priorityE,severityE,command//"/"//option,messageText)
      Else 
         vars(iIn)%croflag = cInputUpper
      End If
      !
   Else
      errorC = .True.
      messageText = "parameter croflag is required"
      Call pakoMessage(priorityE,severityE,command//"/"//option,messageText)
   End If
End If
!
ERROR = ERROR .Or. errorC
!
