!
!     $Id$
!
      if (vars(iIn)%lengthOtf.lt.1.0                                     &
     &     .and. vars(iIn)%altOption.eq."SPEED"                          &
     &     ) then
         errorInconsistent = .TRUE.
         write (messageText,*)                                           &
     &        'length of Otf Subscan: ', vars(iIn)%lengthOtf,            &
     &        ' inconsistent with /SPEED ',vars(iIn)%speedStart,         &
     &        ' use option /TOTF'
         CALL pakoMessage(priorityE,severityE,command,messageText)
      end if
!
      if (vars(iIn)%lengthOtf.gt.0.0                                     &
     &     .and. vars(iIn)%altOption.eq."SPEED"                          &
     &     .and. vars(iIn)%speedStart.eq.0                               &
     &     ) then
         errorInconsistent = .TRUE.
         write (messageText,*)                                           &
     &        'length of Otf Subscan: ', vars(iIn)%lengthOtf,            &
     &        ' inconsistent with /SPEED ', vars(iIn)%speedStart,        &
     &        ' use option /TOTF'
         CALL pakoMessage(priorityE,severityE,command,messageText)
      end if
!
