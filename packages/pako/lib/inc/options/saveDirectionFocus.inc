!
!  Id: saveDirectionFocus.inc,v 1.1.0 2009-04-02 Hans Ungerechts
!
If (                                                                             &
     &      GV%privilege.Eq.setPrivilege%staff                                   &
     &  .Or.GV%privilege.Eq.setPrivilege%ncsTeam                                 &
     &  ) Then                                                                   !
   !
!!$   If (          vars(iValue)%directionFocus.Eq.directionFocusChoices(1)         &
!!$        &  .Or.  vars(iValue)%directionFocus.Eq.directionFocusChoices(2)         &
!!$        &   )  Then                                                              !
      !
      l = lenc(vars(iValue)%directionFocus)
      lineOut = ' /direction "'//vars(iValue)%directionFocus(1:l)//'"'
      !
      Include 'inc/options/caseContinuationLine.inc'
      !
      Write (iUnit,*) lineOut(1:lenc(lineOut))
      !
!!$   End If
!
End If
