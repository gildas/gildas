!
! Id: readOptionsBackend.inc,v 1.3.0 2017-08-24 Hans Ungerechts
!
! NB:order matters!
!
Include 'inc/options/optionClearBE.inc'
Include 'inc/options/optionConnectBE.inc'
Include 'inc/options/optionDisconnectBE.inc'
Include 'inc/options/optionReceiver.inc'
Include 'inc/options/optionLineName.inc'
Include 'inc/options/optionModeBE.inc'
Include 'inc/options/optionPercentage.inc'
Include 'inc/options/optionNsamples.inc'
!
