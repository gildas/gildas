!
!     $Id$
!
      If (vars(iValue)%doAmbient) Then
         lineOut = " /ambient yes"
      Else
         lineOut = " /ambient no"
      End If
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
