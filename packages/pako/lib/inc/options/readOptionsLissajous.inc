  !
  !   Id: readOptionsLissajous.inc, v 1.2.3 2014-08-27 Hans Ungerechts
  !
  !  Family:   readOptions for observing mode
  !  Siblings: readOptionsOtfMap.inc
  !  Siblings: readOptionsTip.inc
  !
  Include 'inc/options/optionCenter.inc'
  Include 'inc/options/optionCroLoop.inc'
  Include 'inc/options/optionFrequency.inc'
  Include 'inc/options/optionPhases.inc'
  Include 'inc/options/optionReference.inc'
  Include 'inc/options/optionSystem.inc'
  Include 'inc/options/optionTotf.inc'
  Include 'inc/options/optionTrecord.inc'
  Include 'inc/options/optionTreference.inc'
  !
  Include 'inc/options/optionFocus.inc'
  Include 'inc/options/optionPointing.inc'
  Include 'inc/options/optionTuneOffsets.inc'
  Include 'inc/options/optionTuneTime.inc'
  !
