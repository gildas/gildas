!
!     $Id$
!     Family:   option -- simple logical
!     Siblings: optionAmbient.f90
!               optionBalance.f90
!               optionCold.f90
!               optionUpdate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
      OPTION = 'COLD'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      errorL    = .FALSE.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
         vars(iIn)%doCold = .True.
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_L4(LINE,iOption,iArgument,                        &
     &                  vars(iIn)%doCold,.True.,errorL)
         Endif
      Endif
      ERROR = ERROR .Or. errorL
!
