!
!     Id: optionSlew.inc,v 1.0.9.4 2009-01-20 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: optionBalance.f90
!               optionCalibrate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionSlew.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
errorL    = .False.
!
Call queryReceiver(rec%Bolo, isConnected)
!
OPTION = 'SLEW'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
!!$   If (isConnected) Then
   !
   vars(iIn)%doSlew = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &                  vars(iIn)%doSlew,.True.,errorL)                    !
   Endif
   !
!!$   Else
!!$      !
!!$      errorL = .True.
!!$      Write (messageText,*) "only supported with MAMBO bolometer"
!!$      Call pakoMessage(priorityE,severityE,"TIP /Slew",messageText)
!!$      !
!!$   End If
   !
End If
!
ERROR = ERROR .Or. errorL
!
!!$
!!$   If (         GV%privilege.Eq.setPrivilege%ncsTeam                             &
!!$        &  .Or. GV%privilege.Eq.setPrivilege%staff) Then                         !
!!$      !
!!$      vars(iIn)%doSlew = .True.
!!$      If (SIC_PRESENT(iOption,iArgument)) Then
!!$         Call SIC_L4(LINE,iOption,iArgument,                                     &
!!$              &                  vars(iIn)%doSlew,.True.,errorL)                 !
!!$      Endif
!!$      !
!!$   Else
!!$      !
!!$      Include 'inc/options/nyi.inc'
!!$      !
!!$   End If
!


