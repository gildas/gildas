  !
  !     Id: optionTtune.inc,v 1.2.4 2015-07-06 Hans Ungerechts
  !
  !     Family:   option 1 real parameter (time)
  !               but with privileged override of standard limits
  !
  !     Related:  optionTsubscan.inc
  !               optionTotf.inc
  !               optionTref.inc
  !               optionTtune.inc
  !
  !               optionTrecord.inc
  !               optionTsegment.inc
  !               optionTphase.inc
  !               optionTblanking.inc
  !
  !D   Write (6,*) "                            "
  !D   Write (6,*) "     --> optionTtune.f90 "
  !D   Write (6,*) "          GV%privilege:     ",  GV%privilege
  !D   Write (6,*) "          GV%userLevel:     ",  GV%userLevel
  !D   Write (6,*) "          iUserLevel:       ",  iUserLevel
  !
  errorR = .False.
  !
  Option = 'TTUNE'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.1) Then
        iArgument = 1
        Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
     Else
        errorR = .True.
     End If
     !
  End If
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If       ( .Not. (       GV%privilege.Eq.setPrivilege%staff                 &
          &             .Or.  GV%privilege.Eq.setPrivilege%ncsTeam               &
          &             .Or.  GV%iUserLevel.Ge.iGuru                             &
          &           )                                                          &
          &   ) Then                                                             !
        !
        If (.Not.errorR) Then
           Call checkR4co(command,option,rInput,                                 &
                &           vars%tTune,                                          &
                &           errorR)                                              !
        End If
        !
     Else
        !
        If      (      rInput .Lt. timeExtended%min                              &
             &     .Or.  rInput .Gt. timeExtended%max                            &
             &  ) Then                                                           !
           !
           errorR = .True.
           Write (messageText, *)                                                &
                &  " value ", rInput,                                            &
                &  " outside extended limits ", timeExtended%min,                &
                &  " to ",  timeExtended%max                                     !
           Call PakoMessage(priorityE,severityE,                                 &
                &           command//" /tTune",messageText)                      !
           !
        Else
           vars(iIn)%tTune = rInput
        End If
        !
        !D   Write (6,*) "   vars(iIn)%tTune:   ", vars(iIn)%tTune
        !
     End If
     !
  End If   !!   (SIC_PRESENT(iOption,0))
  !
  If (errorR) Then
     messageText =                                                               &
          &        "tTune -> 1 real parameter is required"                       !
     Call PAKOMESSAGE(priorityI,severityI,&
          &command//"/"//option,messageText)
  End If
  !
  ERROR = ERROR .Or. errorR
  !




!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     !  Id: optionTuneTime.inc ,v 1.2.3 2014-08-27 Hans Ungerechts
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) " *** "
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) "     --> optionTuneTime.inc ,v 1.2.3 2014-08-27"
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     Option = 'TTUNE'
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     errorM = .False.
!!OLD v 1.2.3 2014-08-27   !!     errorL = .False.
!!OLD v 1.2.3 2014-08-27   !!     errorR = .False.
!!OLD v 1.2.3 2014-08-27   !!     errorC = .False.
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     Call indexCommmandOption                                                       &
!!OLD v 1.2.3 2014-08-27   !!          &     (command,option,commandFull,optionFull,                             &
!!OLD v 1.2.3 2014-08-27   !!          &      commandIndex,optionIndex,iCommand,iOption,                         &
!!OLD v 1.2.3 2014-08-27   !!          &      errorNotFound,errorNotUnique)                                      !
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     If (SIC_PRESENT(iOption,0)) Then
!!OLD v 1.2.3 2014-08-27   !!        !
!!OLD v 1.2.3 2014-08-27   !!        If (SIC_NARG(iOption).Eq.0) Then
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!   !!$        vars(iIn)%doTune = .True.
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!        Else If (SIC_NARG(iOption).Eq.1) Then                                       !  
!!OLD v 1.2.3 2014-08-27   !!           !                                                                        !  
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!           iArgument = 1
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!           Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                   &
!!OLD v 1.2.3 2014-08-27   !!                &              .False.,errorC)                                      !
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!           !D         Write (6,*) " cInput(1:lengthInput) : -->", cInput(1:lengthInput), "<--"
!!OLD v 1.2.3 2014-08-27   !!           !D   Write (6,*) " *** "3        Write (6,*) " cInput(1:1)           : -->", cInput(1:1), "<--"
!!OLD v 1.2.3 2014-08-27   !!           !D         Write (6,*) " *** "
!!OLD v 1.2.3 2014-08-27   !!           !D         Write (6,*) "     "
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!           Select Case ( cInput(1:1) )
!!OLD v 1.2.3 2014-08-27   !!              !
!!OLD v 1.2.3 2014-08-27   !!           Case ("*")
!!OLD v 1.2.3 2014-08-27   !!              !D            Write (6,*) ' Case "*" --> .True. same value '
!!OLD v 1.2.3 2014-08-27   !!   !!$           vars(iIn)%doTune = .True.
!!OLD v 1.2.3 2014-08-27   !!   !!$           !
!!OLD v 1.2.3 2014-08-27   !!   !!$        Case (".", "y", "Y", "n", "N")
!!OLD v 1.2.3 2014-08-27   !!   !!$           !D            Write (6,*) ' Case "." "Y" "N" --> logical '
!!OLD v 1.2.3 2014-08-27   !!   !!$           !
!!OLD v 1.2.3 2014-08-27   !!   !!$           If (SIC_PRESENT(iOption,iArgument)) Then
!!OLD v 1.2.3 2014-08-27   !!   !!$              Call SIC_L4(LINE,iOption,iArgument,                                &
!!OLD v 1.2.3 2014-08-27   !!   !!$                   &           vars(iIn)%doTune,.True.,errorL)                   !
!!OLD v 1.2.3 2014-08-27   !!   !!$           End If
!!OLD v 1.2.3 2014-08-27   !!   !!$           !D            Write (6,*) " errorL:           ", errorL
!!OLD v 1.2.3 2014-08-27   !!   !!$           !D            Write (6,*) " vars(iIn)%doTune: ", vars(iIn)%doTune
!!OLD v 1.2.3 2014-08-27   !!   !!$           !
!!OLD v 1.2.3 2014-08-27   !!   !!$           If ( errorL ) Then
!!OLD v 1.2.3 2014-08-27   !!   !!$              Write (messageText,*)                                              &
!!OLD v 1.2.3 2014-08-27   !!   !!$                   &     cInput(1:lengthInput)//" not valid logical "            !
!!OLD v 1.2.3 2014-08-27   !!   !!$              Call pakoMessage(priorityE,severityE,                              &
!!OLD v 1.2.3 2014-08-27   !!   !!$                   &      command//"/"//OPTION,messageText)                      !
!!OLD v 1.2.3 2014-08-27   !!   !!$           End If
!!OLD v 1.2.3 2014-08-27   !!   !!$           !
!!OLD v 1.2.3 2014-08-27   !!           Case DEFAULT
!!OLD v 1.2.3 2014-08-27   !!              !D            Write (6,*) ' Case DEFAULT --> real '
!!OLD v 1.2.3 2014-08-27   !!              !
!!OLD v 1.2.3 2014-08-27   !!              Call SIC_R4(LINE,iOption,iArgument,                                   &
!!OLD v 1.2.3 2014-08-27   !!                   &                 rInput,.False.,errorR)                         !
!!OLD v 1.2.3 2014-08-27   !!              !D            Write (6,*) " rInput = ", rInput
!!OLD v 1.2.3 2014-08-27   !!              If (.Not. errorR) Call checkR4co(command,option,rInput,               &
!!OLD v 1.2.3 2014-08-27   !!                   &                 vars%tTune,                                    &
!!OLD v 1.2.3 2014-08-27   !!                   &                 errorR)                                        !
!!OLD v 1.2.3 2014-08-27   !!              !D            Write (6,*) " errorR:           ", errorR
!!OLD v 1.2.3 2014-08-27   !!              !D            Write (6,*) " vars(iIn)%tTune : ", vars(iIn)%tTune
!!OLD v 1.2.3 2014-08-27   !!              !
!!OLD v 1.2.3 2014-08-27   !!   !!$           If ( .Not.erroR ) Then
!!OLD v 1.2.3 2014-08-27   !!   !!$              vars(iIn)%doTune = .True.
!!OLD v 1.2.3 2014-08-27   !!   !!$           End If
!!OLD v 1.2.3 2014-08-27   !!              !
!!OLD v 1.2.3 2014-08-27   !!           End Select
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!        Else If (SIC_NARG(iOption).Ge.2) Then
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!           Write (messageText,*)  "too many arguments "
!!OLD v 1.2.3 2014-08-27   !!           Call pakoMessage(priorityE,severityE,                                    &
!!OLD v 1.2.3 2014-08-27   !!                &      command//"/"//OPTION,messageText)                            !
!!OLD v 1.2.3 2014-08-27   !!           errorL = .True.
!!OLD v 1.2.3 2014-08-27   !!           !
!!OLD v 1.2.3 2014-08-27   !!        End If
!!OLD v 1.2.3 2014-08-27   !!        !
!!OLD v 1.2.3 2014-08-27   !!     End If
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     ERROR = ERROR .Or. errorC .Or. errorL .Or. errorR 
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) " ERROR:            ", ERROR
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) " vars(iIn)%tTune = ", vars(iIn)%tTune
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) "     <-- optionTuneTime.inc ,v 1.2.3 2014-05-29"
!!OLD v 1.2.3 2014-08-27   !!     !D   Write (6,*) " *** "
!!OLD v 1.2.3 2014-08-27   !!     !
!!OLD v 1.2.3 2014-08-27   !!   
