!
!     $Id$
!
      if (vars(iValue)%doSky) then
         if (vars(iValue)%altSky .eq. "OFFSETS") then
            write(lineOut,*)                                             &
     &           "/sky ",                                                &
     &           vars(iValue)%xOffsetRC, " ",                            &
     &           vars(iValue)%yOffsetRC                                  
         else if (vars(iValue)%altSky.eq.'LIST') then
            l = lenc(vars(iValue)%sourceNameSky)            
            lineOut =                                                    &
     &      " /sky "//Q//vars(iValue)%sourceNameSky(1:l)//Q
         end if
      else
         lineOut =  " /sky no"
      end if
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!



