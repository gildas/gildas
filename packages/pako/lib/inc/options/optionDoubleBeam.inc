!
!     Id: optionDoubleBeam.inc,v 1.0.6.1 2007-01-13 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: optionBalance.f90
!               optionCalibrate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
OPTION = 'DOUBLEBEAM'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   !
   !!         Include 'inc/options/nyi.inc'
   !
   vars(iIn)%doDoubleBeam = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &                  vars(iIn)%doDoubleBeam,.True.,errorL)              !
   Endif
Endif
ERROR = ERROR .Or. errorL
!
