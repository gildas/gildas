  !
  !     Id: optionDirectionFocus.inc,v 1.2.3 2014-03-13 Hans Ungerechts
  !     based on:
  !     Id: optionDirectionFocus.inc,v 1.1.0 2009-03-22 Hans Ungerechts
  !
  Call pako_message(seve%t,programName,                                          &
       &    " ->  optionDirectionFocus.inc,v 1.2.3 2014-03-13")                  ! trace execution
  !
  OPTION = 'DIRECTION'
  !
  !D       Write (6,*) "         command:      ", command
  !D       Write (6,*) "         OPTION:       ", OPTION
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  !D       Write (6,*) "         iOption:      ", iOption
  If (command.Eq."SET") then                                                     !  !! to override a possible 
                                                                                 !  !! issue with indexCommmandOption( )
     iOption = 2
  End If
  ! 
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
!!$         If (                                                                    & !! require privilege
!!$              &      GV%privilege.Eq.setPrivilege%staff                          &
!!$              &  .Or.GV%privilege.Eq.setPrivilege%ncsTeam                        &
!!$              &  ) Then                                                          !
!!$            errorL = .False.
!!$         Else
!!$            errorL = .True.
!!$            Write (messageText, *) " requires privilege"
!!$            Call pakoMessage(priorityE,severityE,command//" /"//option,          &
!!$                 &           messageText)                                        !
!!$         End If
     !
     ERROR = ERROR.or.errorL
     !
     If (.Not. ERROR) Then
        If (SIC_PRESENT(iOption,iArgument)) Then
           Call SIC_CH(LINE,iOption,iArgument,                                   &
                &           cInput,lengthInput,.True.,errorC)                    !
           cInputUpper = cInput
           Call SIC_UPPER(cInputUpper)
           !
           ! ***   search for cInput in directionFocusChoices   ***
           !
           errorNotFound         = .False.
           errorNotUnique        = .False.
           choiceIndex = 0
           !
           Do ii = 1,3,1
              !           TBD: number of choices should be a variable
              If (Index(directionFocusChoices(ii),                               &
                   &              cInputUpper(1:lengthInput)).Eq.1) Then         !
                 If (choiceIndex .Eq. 0) Then
                    choiceIndex = ii
                 Else
                    errorNotUnique = .True.
                 End If
              End If
           End Do
           errorNotFound =  choiceIndex .Eq. 0
           !
           errorC = errorC .Or. errorNotUnique .Or. errorNotFound
           !
           If (errorC) Then
              Write (messageText,*) 'value: "',cInput,'" not valid'
              Call pakoMessage(6,3,command//"/"//option,messageText)
           Else 
              vars(iIn)%directionFocus = directionFocusChoices(choiceIndex)
           End If
           !
        Else
           errorC = .True.
           messageText = "parameter directionFocus is required"
           Call pakoMessage(6,3,command//"/"//option,messageText)
        End If
     End If
  End If
  !
  ERROR = ERROR .Or. errorC
  !
