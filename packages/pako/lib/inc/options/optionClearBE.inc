!
! Id: optionClearBE.inc ,v 1.3.0 2017-08-28 Hans Ungerechts
!
!D      Write (6,*) "      --> optionClearBE.inc "
!
OPTION = 'CLEAR'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   !
   !D   Write (6,*) "   option CLEAR "
   !
   
   !
   clearBackends = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &      clearBackends,.True.,errorL)                                   !
   Endif
   !
   If (clearBackends .And. .Not. errorL) Then
      vars(iIn,:)     =  beReset
      vars(iTemp,:)   =  beReset
      vars(iValue,:)  =  beReset
      listBE(:,:)     =  beReset
      GV%pulsarMode   = .False.
   End If
   !
End If
!     
ERROR = ERROR .Or. errorL
!


