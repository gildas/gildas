!
!     Id: optionLastSource.inc,v 1.0.5 2006-10-20
!     Family:   option -- logical which triggers special action
!
!D Write (6,*) "      --> optionLastSource.inc,v 1.0.5 2006-10-20"
!
OPTION = 'LASTSOURCE'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   vars(iIn)%doLastSource = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        &
           &                  vars(iIn)%doLastSource,.True.,errorL)              !
   Endif
Endif
ERROR = ERROR .Or. errorL
!
!D Write (6,*) "      vars(iIn)%doLastSource: ", vars(iIn)%doLastSource
