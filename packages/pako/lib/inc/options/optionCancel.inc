!
!  Id: parametersCommandToSave.inc, v 0.9 2005-09-20 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: 
!               optionAppend.f
!               optionCancel.f
!
!
OPTION = 'CANCEL'
!
doCancel = .False.
!
Call indexCommmandOption                                                         & 
     &     (command,option,commandFull,optionFull,                               & 
     &      commandIndex,optionIndex,iCommand,iOption,                           & 
     &      errorNotFound,errorNotUnique)
!
errorL    = .FALSE.
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   doCancel = .True.
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_L4(LINE,iOption,iArgument,                                        & 
           &           doCancel,.True.,errorL)
   Endif
Endif
ERROR = ERROR .Or. errorL
!
