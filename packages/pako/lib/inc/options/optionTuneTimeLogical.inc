  !
  !  Id: optionTuneTimeLogical.inc ,v 1.2.3 2014-08-28 Hans Ungerechts
  !
  !  Family:     Option --> 1 time or logical
  !  Cousins:    
  !
  !D   Write (6,*) " *** "
  !D   Write (6,*) "     --> optionTuneTimeLogical.inc ,v 1.2.3 2014-08-28"
  !
  Option = 'TTUNE'
  !
  errorM = .False.
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.0) Then
        !
        vars(iIn)%doTune = .True.
        !
     Else If (SIC_NARG(iOption).Eq.1) Then                                       !  
        !                                                                        !  
        !
        iArgument = 1
        !
        Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                   &
             &              .False.,errorC)                                      !
        !
        !D         Write (6,*) " cInput(1:lengthInput) : -->", cInput(1:lengthInput), "<--"
        !D   Write (6,*) " *** "3        Write (6,*) " cInput(1:1)           : -->", cInput(1:1), "<--"
        !D         Write (6,*) " *** "
        !D         Write (6,*) "     "
        !
        Select Case ( cInput(1:1) )
           !
        Case ("*")
           !D            Write (6,*) ' Case "*" --> .True. same value '
           vars(iIn)%doTune = .True.
           !
        Case (".", "y", "Y", "n", "N")
           !D            Write (6,*) ' Case "." "Y" "N" --> logical '
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_L4(LINE,iOption,iArgument,                                &
                   &           vars(iIn)%doTune,.True.,errorL)                   !
           End If
           !D            Write (6,*) " errorL:           ", errorL
           !D            Write (6,*) " vars(iIn)%doTune: ", vars(iIn)%doTune
           !
           If ( errorL ) Then
              Write (messageText,*)                                              &
                   &     cInput(1:lengthInput)//" not valid logical "            !
              Call pakoMessage(priorityE,severityE,                              &
                   &      command//"/"//OPTION,messageText)                      !
           End If
           !
        Case DEFAULT
           !D            Write (6,*) ' Case DEFAULT --> real '
           !
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           !D            Write (6,*) " rInput = ", rInput
           If (.Not. errorR) Call checkR4co(command,option,rInput,               &
                &                 vars%tTune,                                    &
                &                 errorR)                                        !
           !D            Write (6,*) " errorR:           ", errorR
           !D            Write (6,*) " vars(iIn)%tTune : ", vars(iIn)%tTune
           !
           If ( .Not.erroR ) Then
              vars(iIn)%doTune = .True.
           End If
           !
        End Select
        !
     Else If (SIC_NARG(iOption).Ge.2) Then
        !
        Write (messageText,*)  "too many arguments "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        errorL = .True.
        !
     End If
     !
  End If
  !
  ERROR = ERROR .Or. errorC .Or. errorL .Or. errorR 
  !
  !D   Write (6,*) " ERROR:            ", ERROR
  !D   Write (6,*) " vars(iIn)%tTune = ", vars(iIn)%tTune
  !D   Write (6,*) "     <-- optionTuneTime.inc ,v 1.2.3 2014-05-29"
  !D   Write (6,*) " *** "
  !
