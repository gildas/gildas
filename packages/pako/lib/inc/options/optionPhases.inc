  !
  !  Id: optionPhases.inc,v 1.1.12 2012-03-01
  !
  !  Family:   offset 
  !            PHASES       optionPhases.inc
  !
  !D   Write (6,*) ' ---> optionPhases '
  !
  Option = 'PHASES'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.0) Then
        !
        Write (messageText,*)  " expects 2 arguments: x y "
        Call pakoMessage(priorityI,severityI,                                    &
             &      command//"/"//OPTION,messageText)                            !
        !
        Call Random_Number(rr)
        !D         Write (6,*) "      rr: ", rr
        !
        rr(1) = vars(iLimit1)%phase%x+rr(1) *                                    &
             &  Abs(vars(iLimit2)%phase%x-vars(iLimit1)%phase%x)                 !
        rr(2) = vars(iLimit1)%phase%y+rr(2) *                                    &
             &  Abs(vars(iLimit2)%phase%y-vars(iLimit1)%phase%y)                 !
        !D         Write (6,*) "      rr: ", rr
        !
        vars(iIn)%phase%x=rr(1)
        vars(iIn)%phase%y=rr(2)
        !
        Write (messageText,*)  " set to random values ",                         &
             &      vars(iIn)%phase%x, vars(iIn)%phase%y                         !
        Call pakoMessage(priorityI+6,severityI,                                  &
             &      command//"/"//OPTION,messageText)                            !
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.1) Then
        !
        iArgument = 1
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4(option,rInput,                         &
                &                 vars%phase%x,                                  &
                &                 errorR)                                        !
        End If
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.2) Then
        !
        iArgument = 2
        Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                     &
             &              .False.,errorC)                                      !
        If (cPar(1:1).Ne.'*') Then
           Call SIC_R4(LINE,iOption,iArgument,                                   &
                &                 rInput,.False.,errorR)                         !
           If (.Not. errorR) Call checkR4(option,rInput,                         &
                &                 vars%phase%y,                                  &
                &                 errorR)                                        !
        End If
        !
     End If
     !
     If (SIC_NARG(iOption).Ge.3) Then
        !
        Write (messageText,*)  "too many arguments "
        Call pakoMessage(priorityE,severityE,                                    &
             &      command//"/"//OPTION,messageText)                            !
        errorL = .True.
        !
     End If
     !
     ERROR = ERROR .Or. errorR .Or. errorL
     !
  End If
  !
