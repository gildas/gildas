! 
! Id: optionFoffset.inc,v 1.1.0 2008-12-15 hans Ungerechts
!
! Family:
!
errorR = .False.
!
Option = 'FOFFSET'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (.Not.   (                                                                 &
        &      vars(iIn,iRec)%name .Eq. rec%E090                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E150                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E230                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E300 )                               &
        & ) Then                                                                 !
      errorL = .True.
      messageText = "only valid for EMIR bands "
      Call PakoMessage(priorityE,severityE,                                      &
           &           command//"/"//option,messageText)                         !
   End If
   !
   If (SIC_NARG(iOption).Lt.1. .Or. SIC_NARG(iOption).Gt.1) Then
      messageText = "1 real parameter fOffset [GHz] required"
      Call PakoMessage(priorityE,severityE,command,messageText)
      errorR = .True.
   End If
   ERROR = ERROR .Or. errorR
   !
   iArgument = 1
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &                  cInput,lengthInput,.False.,errorC)                 !
      If (cInput(1:1).Ne.'*') Then
         Call SIC_R8(LINE,iOption,iArgument,dInput,.True.,errorR)
         If (.Not.errorR) Then
            Call checkR8co(command,option,dInput,                                &
                 &                           vars(:,iRec)%fOffset%value,         &
                 &                           errorR)                             !
         End If
      End If
   End If
   ERROR = ERROR .Or. errorR
   !
End If
