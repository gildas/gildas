!
!     Id: optionCalibrate.inc,v 1.0.4 2006-07-10 Hans Ungerechts
!     Family:   option -- simple logical
!     Siblings: optionBalance.f90
!               optionCalibrate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
OPTION = 'CALIBRATE'
!
Call indexCommmandOption                                               &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
errorL    = .False.
!
iArgument = 1
If (SIC_PRESENT(iOption,0)) Then
   !
   Include 'inc/options/nyi.inc'
   !
   !TBD   vars(iIn)%doCalibrate = .True.
   !TBD   If (SIC_PRESENT(iOption,iArgument)) Then
   !TBD      Call SIC_L4(LINE,iOption,iArgument,                              &
   !TBD           &                  vars(iIn)%doCalibrate,.True.,errorL)
   !TBD   Endif
   !
Endif
ERROR = ERROR .Or. errorL
!
