!
!
!
Call queryReceiver(receiverChoices(iRec), Result)
!
If (.Not. Result) Then
   !
   errorInconsistent  =  .True.
   !
   Write (messageText,*)                                                     &
        &         'receiver ',                                               &
        &           receiverChoices(iRec),                                   &
        &         ' is not connected'
   Call PakoMessage(priorityE,severityE,command,messageText)
   !
End If
!
Call queryReceiver(rec%Bolo, Result)
!
If (Result) Then
   !
   errorInconsistent  =  .True.
   !
   messageText = "can not be used with receiver "//rec%Bolo
   Call PakoMessage(priorityE,severityE,command,messageText)
   !
End If
!
