!
!     $Id$
!     Family:   option -- simple logical
!     Siblings: optionBalance.f90
!               optionUpdate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
      OPTION = 'UPDATE'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      errorL    = .FALSE.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
!
         Include 'inc/options/nyi.inc'
!
!TBD:         vars(iIn)%doUpdate = .True.
!TBD:         If (SIC_PRESENT(iOption,iArgument)) Then
!TBD:            Call SIC_L4(LINE,iOption,iArgument,                        &
!TBD:     &                  vars(iIn)%doUpdate,.True.,errorL)
!TBD:         Endif
      Endif
      ERROR = ERROR .Or. errorL
!
