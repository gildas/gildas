  !
  !     Id: optionTotf.inc,v 1.2.4 2015-07-06
  !
  !     Family:   option 1 real parameter (time)
  !               but with privileged override of standard limits
  !               ++ short and long version of option TOTF and TIMEPEROTF
  !
  !     Related:  optionTsubscan.inc
  !               optionTotf.inc
  !               optionTref.inc
  !               optionTtune.inc
  !
  !               optionTrecord.inc
  !               optionTsegment.inc
  !               optionTphase.inc
  !               optionTblanking.inc
  !
  !D   Write (6,*) "                            "
  !D   Write (6,*) "     --> optionTotf.f90     "
  !D   Write (6,*) "          GV%privilege:     ",  GV%privilege
  !D   Write (6,*) "          GV%userLevel:     ",  GV%userLevel
  !D   Write (6,*) "          iUserLevel:       ",  iUserLevel
  !
  errorR = .False.
  !
  option = 'TIMEPEROTF'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  optionShort = 'TOTF'
  !
  Call indexCommmandOption                                                       &
       &     (command,optionShort,commandFull,optionFull,                        &
       &      commandIndex,optionIndex,iCommand,iOptionShort,                    &
       &      errorNotFound,errorNotUnique)                                      !
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (SIC_NARG(iOption).Eq.1) Then
        iArgument = 1
        Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
     Else
        errorR = .True.
     End If
     !
  Elseif (SIC_PRESENT(iOptionShort,0)) Then
     !
     If (SIC_NARG(iOptionShort).Eq.1) Then
        iArgument = 1
        Call SIC_R4(LINE,iOptionShort,iArgument,rInput,.True.,errorR)
     Else
        errorR = .True.
     End If
     !
  End If   !!   SIC_PRESENT(iOption,0) Elseif (SIC_PRESENT(iOptionShort,0))
  !
  If (SIC_PRESENT(iOption,0)                                                     &
       &    .Or. SIC_PRESENT(iOptionShort,0)) Then                               !
     !
     If       ( .Not. (       GV%privilege.Eq.setPrivilege%staff                 &
          &             .Or.  GV%privilege.Eq.setPrivilege%ncsTeam               &
          &             .Or.  GV%iUserLevel.Ge.iGuru                             &
          &           )                                                          &
          &   ) Then                                                             
        ! 
        If (.Not.errorR) Then
           Call checkR4co(command,optionShort,rInput,                            &
             &           vars%tOtf,                                              &
             &           errorR)                                                 !
        End If
        !
     Else
        !
        If      (        rInput .Lt. timeExtended%min                            &
             &     .Or.  rInput .Gt. timeExtended%max                            &
             &  ) Then                                                           !
           !
           errorR = .True.
           Write (messageText, *)                                                &
                &  " value ", rInput,                                            &
                &  " outside extended limits ", timeExtended%min,                &
                &  " to ",  timeExtended%max                                     !
           Call PakoMessage(priorityE,severityE,                                 &
                &           command//" /tOtf",messageText)                       !
           !
        Else
           vars(iIn)%tOtf = rInput
        End If
        !
        !D   Write (6,*) "   vars(iIn)%tOtf:   ", vars(iIn)%tOtf
        !
     End If   !!   .Not. (       GV%privilege.Eq.setPrivilege%staff 
     !
  End If   !!   (SIC_PRESENT(iOption,0))
  !
  If (errorR) Then
     messageText =                                                               &
          &        "tOtf -> 1 real parameter is required"                        !
     Call PAKOMESSAGE(priorityI,severityI,&
          &command//"/"//optionShort,messageText)
  End If
  !
  ERROR = ERROR .Or. errorR
  !





