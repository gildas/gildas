!
!     Id: optionSky.inc,v 1.0.4 2006-07-25 Hans Ungerechts
!
!     Family:   offset or source name(s)
!               SKY       optionSky.f90
!
!D Write (6,*) ' ---> optionSky '
!
Option = 'SKY'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
errorL = .False.
errorR = .False.
errorC = .False.
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_NARG(iOption).Eq.0) Then
      !
      vars(iIn)%doSky = .True.
      !
   Else If (SIC_NARG(iOption).Eq.1) Then
      !
      iArgument = 1
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_L4(LINE,iOption,iArgument,                                     &
              &           vars(iIn)%doSky,.False.,errorL)
      End If
      !
      ERROR = ERROR .Or. errorL
      !
   Else If (SIC_NARG(iOption).Ge.2 .And. SIC_NARG(iOption).Le.2) Then
      !
      If (SIC_NARG(iOption).Ge.1 .And.                                           &
           &              SIC_NARG(iOption).Le.2) Then
         iArgument = 1
         Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                    &
              &              .False.,errorC)
         If (cPar(1:1).Ne.'*') Then
            Call SIC_R4(LINE,iOption,iArgument,                                  &
                 &                 rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4angle(option,rInput,                   &
                 &                 vars%xOffsetRC,                               &
                 &                 GV%angleFactorR,                              &
                 &                 errorR)
         End If
      End If
      !
      If (.Not. errorR .And.                                                     &
           &              SIC_NARG(iOption).Ge.2 .And.                           &
           &              SIC_NARG(iOption).Le.2) Then
         iArgument = 2
         Call SIC_CH(LINE,iOption,iArgument,cPar,lengthInput,                    &
              &              .False.,errorC)
         If (cPar(1:1).Ne.'*') Then
            Call SIC_R4(LINE,iOption,iArgument,                                  &
                 &                 rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4angle(option,rInput,                   &
                 &                 vars%yOffsetRC,                               &
                 &                 GV%angleFactorR,                              &
                 &                 errorR)
         End If
      End If
      !
      If (.Not. errorR) Then
         vars(iIn)%altSky = "OFFSETS"
         vars(iIn)%doSky  = .True.
      End If
      !
   Else If (SIC_NARG(iOption).Ge.3) Then
      !
      Write (messageText,*)  "too many arguments "
      Call pakoMessage(priorityE,severityE,                                      &
           &      command//"/"//OPTION,messageText)
      errorL = .True.
      !
      !
   End If
   !
   ERROR = ERROR .Or. errorR .Or. errorL
   !
End If
!
!D Write (6,*) '      errorL: ', errorL
!D Write (6,*) '      errorR: ', errorR
!D Write (6,*) '      errorC: ', errorC
!D Write (6,*) '      ERROR: ', error
!
