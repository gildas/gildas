!
!   Id: saveBEmode.inc,v 1.0.10 2009-03-10 Hans Ungerechts
!
If (    listBe(jj,ii)%mode .Eq. BEmode%Para .Or.                                 &
     &  listBe(jj,ii)%mode .Eq. BEmode%Pola) Then
   !
   l = len_trim(listBe(jj,ii)%mode)
   lineOut =                                                                     &
        &   ' /mode "'//listBe(jj,ii)%mode(1:l)//'"'
!
   Include 'inc/options/caseContinuationLine.inc'
!
   Write (iUnit,*) lineOut(1:lenc(lineOut))
!
End If
