  !
  !   Id: saveTuneOffsets.inc , v 1.2.3 2014-08-28 Hans Ungerechts  
  !
  !D   Write (6,*) "      saveTuneOffsets.inc , v 1.2.3 2014-08-28 "
  !
  If (vars(iValue)%doTune) Then
     Write(lineOut,*)                                                            &
          &           "/tune ",                                                  &
          &           vars(iValue)%offsetTune%x,                                 &
          &           vars(iValue)%offsetTune%y                                  !
  Else
     lineOut = " /tune no"
  End If
  !
  Include 'inc/options/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
