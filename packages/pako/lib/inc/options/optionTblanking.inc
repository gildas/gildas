  !
  !     Id: optionTblanking.inc,v 1.2.2 2013-02-15 Hans Ungerechts
  !         based on:
  !     Id: optionTblanking.inc,v 1.1   2005-03-23 Hans Ungerechts
  !     Family:   option 1 real parameter
  !     Siblings: optionTotf.f 
  !               optionTrecord.f
  !               optionTsegment.f
  !               optionTphase.f90
  !               optionTblanking.f90
  !
  errorR = .False.
  !
  Option = 'TBLANKING'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  !     TBD: short call; test for errors
  !
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
     If (.Not.errorR) Then
        Call checkR4co(command,option,rInput,                                    &
             &           vars%tBlanking,                                         &
             &           errorR)                                                 !
     End If
  End If
  !
  ERROR = ERROR .Or. errorR
  !



