  !
  !  Id: readOptionsFocus.inc,v 1.2.3   2014-09-23 Hans Ungerechts
  !  Id: readOptionsFocus.inc,v ??      ??
  !
  Include 'inc/options/optionCalibrate.inc'
  Include 'inc/options/optionDirectionFocus.inc'
  Include 'inc/options/optionFeBe.inc'
  Include 'inc/options/optionNsubscans.inc'
  Include 'inc/options/optionOtfFocus.inc'
  Include 'inc/options/optionTsubscan.inc'
  Include 'inc/options/optionUpdate.inc'
  !
  Include 'inc/options/optionTuneOffsets.inc'
  Include 'inc/options/optionTuneTime.inc'
  !
