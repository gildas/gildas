!
!     $Id$
!
!     FAMILY:    choice of character strings
!     Siblings:  
!                optionDoppler
!                optionScale
!                optionWidth
!
      OPTION = 'SCALE'
!
Call indexCommmandOption                                              &
     &     (command,option,commandFull,optionFull,                    &
     &      commandIndex,optionIndex,iCommand,iOption,                &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                             & 
           &           cInput,lengthInput,.True.,errorC)
   Else
      errorL = .True.
      messageText = "parameter scale [string] is required"
      Call PakoMessage(priorityE,severityE,                           &
           &           command//"/"//option,messageText)
   End If
End If
!
ERROR = ERROR .Or. errorC .Or. errorL
!
! *** Check against valid codes
!
If (.Not. ERROR) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call pakoUmatchKey (                                            &
           &              keys=scaleChoices,                          &
           &              key=cInput,                                 &
           &              command=command//"/"//option,               &
           &              howto='Start Upper',                        &
           &              iMatch=iMatch,                              &
           &              nMatch=nMatch,                              &
           &              error=errorC,                               &
           &              errorCode=errorCode                         &
           &             )
      !
      ERROR = ERROR .Or. errorC
      !
      If (.Not. ERROR) Then
         vars(iIn,iRec)%scale  = scaleChoices(iMatch)
      End If
      !
   End If
End If
!






