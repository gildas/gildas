!
!  Id: readOptionsSwFrequency.inc v 1.0.5 2006-10-17 Hans Ungerechts
!
Include 'inc/options/optionFunction.inc'
Include 'inc/options/optionNphases.inc'
Include 'inc/options/optionNcycles.inc'
Include 'inc/options/optionTphase.inc'
Include 'inc/options/optionTblanking.inc'
!
