!
!  Id: optionNcyclesNotSupported.inc v 1.1.3 2010-11-18 Hans Ungerechts
!
errorL = .False.
!
OPTION = 'NCYCLES'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   Write (messageText,*)                                                         &
        &      "is not (yet) supported"                                          !
   !D   Write (6,*) command//"/"//OPTION,messageText
   Call PakoMessage(priorityE,severityE,command//"/"//OPTION,messageText)
   errorL = .True.
   !
Endif
!
ERROR = ERROR .Or. errorL
