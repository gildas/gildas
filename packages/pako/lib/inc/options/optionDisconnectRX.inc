!
! Id: optionDisconnectRX.inc,v 1.1.11 2011-11-04 Hans Ungerechts
!     Family:   option -- logical with conditions
!     Siblings: optionConnectBE.inc
!               optionDisConnectBE.inc
!               optionConnectRX.inc
!               optionDisconnectRX.inc
!
OPTION = 'DISCONNECT'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorL    = .False.
!
If (SIC_PRESENT(iOption,0)) Then
   iArgument = 1
   !
   If (gv%iUserLevel.Ge.iExperienced) Then
      !
      messageText = " /disconnect is DEPRECATED. USE AT YOUR OWN RISK! "
      Call pakoMessage(priorityW+10,severityW,command,messageText)
      !
      doDisconnectRX = .True.
      doConnectRX = .Not. doDisconnectRX
      !
      If (doDisconnectRX) Then
         Include 'inc/options/doDisconnectRX.inc'
      End If
      !
   Else
      !
      errorL = .True.
      messageText = " /disconnect is DEPRECATED. USE AT YOUR OWN RISK! "
      Call pakoMessage(priorityW+10,severityW,command,messageText)
      messageText = " /disconnect requires higher User Level"
      Call pakoMessage(priorityE+10,severityE,command,messageText)
      !
   End If
   !
End If
!
ERROR = ERROR .Or. errorL
!
