!
!  Id: readOptionsCalibrate.inc,v 0.96 2005-12-05 Han Ungerechts
!     
Call initializeSystemNameChoices
!
Include 'inc/options/optionAmbient.inc'
Include 'inc/options/optionCold.inc'
Include 'inc/options/optionFast.inc'
Include 'inc/options/optionCalibrateGainImage.inc'
Include 'inc/options/optionGrid.inc'
Include 'inc/options/optionNull.inc'
Include 'inc/options/optionSky.inc'
Include 'inc/options/optionSystem.inc'
Include 'inc/options/optionTcalibrate.inc'
Include 'inc/options/optionTest.inc'
Include 'inc/options/optionVariable.inc'
!
