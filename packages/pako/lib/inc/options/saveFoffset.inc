!
!  Id: saveFoffset.inc,v 1.1.0 2009-03-20 Hans Ungerechts
!
If ( Abs(vars(iValue,ii)%fOffset%value-GPnoneD) .GE. 0.1E-9) Then
   !
   !D Write (6,*) vars(iValue,ii)%fOffset%value
   !
   Write (c1,*) vars(iValue,ii)%fOffset%value
   l1 = lenc(c1)
   !
   lineOut = " /fOffset "//c1(1:l1)
   !
   Include 'inc/options/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
