!
!     Id: saveSlew.inc,v 1.0.6.1 2007-01-17 Hans Ungerechts
!
If (vars(iValue)%doSlew) Then
   Write (lineOut,*) " /slew ", "yes"       
   !
   Include 'inc/options/caseContinuationLine.inc'
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
Else
   !TBD         write (lineOut,*) " /slew ", "no"
   !
   !TBD   Include 'inc/options/caseContinuationLine.inc'
   !TBD   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
!
!
