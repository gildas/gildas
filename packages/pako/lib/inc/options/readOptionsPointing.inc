  !
  !  Id: readOptionsPointing.inc,v 1.2.3   2014-09-23 Hans Ungerechts
  !  Id: readOptionsPointing.inc,v 1.0.6.1 2007-01-13 Hans Ungerechts
  !
  Include 'inc/options/optionCalibrate.inc'
  Include 'inc/options/optionDoubleBeam.inc'
  Include 'inc/options/optionFeBe.inc'
  Include 'inc/options/optionMore.inc'
  Include 'inc/options/optionNotf.inc'
  Include 'inc/options/optionTotf.inc'
  Include 'inc/options/optionTrecord.inc'
  Include 'inc/options/optionUpdate.inc'
  !
  Include 'inc/options/optionTuneOffsets.inc'
  Include 'inc/options/optionTuneTime.inc'
  !
  Include 'inc/options/optionAeLoop.inc'
  !
