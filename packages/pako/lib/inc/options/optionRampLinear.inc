  !
  ! Id: optionRampLinear.inc,v 1.2.3 2013-08-** Hans Ungerechts
  !
  OPTION = 'RAMP'
  !
  vars(iIn)%doInternal = .False.
  !
  Call pako_message(seve%t,programName,                                          &
       &    " module DIYlist  --> optionRampLinear.inc,v 1.2.3 2013-08-** ")     ! trace execution
  !
  errorM = .False.
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_CH(LINE,iOption,iArgument,                                      &
             &           cInput,lengthInput,.True.,errorC)                       !
        cInputUpper = cInput
        Call SIC_UPPER(cInputUpper)
        !
     Else
!!$      errorC = .True.
        messageText = "no choice keyword"
        Call pakoMessage(priorityI,severityI,                                    &
             &                   command//"/"//option,messageText)               !
     End If
     !
     ERROR = ERROR .Or. errorC
     !
     If (.Not. Error) Then
        !
        Call pakoUmatchKey (                                                     &
             &              keys=rampChoices,                                    &
             &              key=cInputUpper,                                     &
             &              command=command//"/"//option,                        &
             &              howto='Start Upper',                                 &
             &              iMatch=iMatch,                                       & 
             &              nMatch=nMatch,                                       &
             &              error=errorM,                                        &
             &              errorCode=errorCode                                  &
             &             )                                                     !
        !
     End If
     !
     ERROR = ERROR .Or. errorM
     !
     If (.Not. errorM) Then
        !D       Write (6,*) "         iMatch:   ", imatch
        !D       Write (6,*) "         choice:   ->", rampChoices(iMatch) ,"<-"
        !D       Write (6,*) "         ramp%up   ->", ramp%up             ,"<-"
        !D       Write (6,*) "         ramp%down ->", ramp%down           ,"<-"
        !D       Write (6,*) "         ramp%none ->", ramp%none           ,"<-"
        !
        If      (rampChoices(iMatch).Eq.ramp%up) Then
           vars(iIn)%doRampUp   = .True.
           !
           iArgument = 2
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,             &
                   &              .False.,errorC)                                !
              If (cInput(1:1).Ne.'*') Then
                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
                 If (.Not.errorR) Then
                    Call checkR4co(command,option,rInput,                        &
                         &           vars%tRampUp,                               &
                         &           errorR)                                     !
                 End If
              End If
           End If
           !
           ERROR = ERROR .Or. errorR
           !
        Else If (rampChoices(iMatch).Eq.ramp%down) Then
           vars(iIn)%doRampDown = .True.
           !
           iArgument = 2
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,             &
                   &              .False.,errorC)                                !
              If (cInput(1:1).Ne.'*') Then
                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
                 If (.Not.errorR) Then
                    Call checkR4co(command,option,rInput,                        &
                         &           vars%tRampDown,                             &
                         &           errorR)                                     !
                 End If
              End If
           End If
           !
           ERROR = ERROR .Or. errorR
           !
        Else If (rampChoices(iMatch).Eq.ramp%both) Then
           vars(iIn)%doRampUp   = .True.
           vars(iIn)%doRampDown = .True.
           !
           iArgument = 2
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,             &
                   &              .False.,errorC)                                !
              If (cInput(1:1).Ne.'*') Then
                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
                 If (.Not.errorR) Then
                    Call checkR4co(command,option,rInput,                        &
                         &           vars%tRampUp,                               &
                         &           errorR)                                     !
                 End If
              End If
           End If
           !
           iArgument = 3
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,             &
                   &              .False.,errorC)                                !
              If (cInput(1:1).Ne.'*') Then
                 Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
                 If (.Not.errorR) Then
                    Call checkR4co(command,option,rInput,                        &
                         &           vars%tRampDown,                             &
                         &           errorR)                                     !
                 End If
              End If
           End If
           !
           iArgument = 4
           !
           If (SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,             &
                   &              .False.,errorC)                                !  
              If (GV%doDebugMessages) Then
                 Write (6,*) "   iArgument:  ", iArgument,                       &
                      &      " cInput:  -->", cInput(1:lengthInput), "<--"       !
!!$                 vars(iIn)%doInternal = .True.
              End If
              vars(iIn)%doInternal = .True.
           End If
           !
           ERROR = ERROR .Or. errorR
           !
        Else If (rampChoices(iMatch).Eq.ramp%none) Then
           vars(iIn)%doRampUp   = .False.
           vars(iIn)%doRampDown = .False.
           !
        End If
        !
     End If
     !
     If (GV%doDebugMessages) Then
        Write (6,*) "   vars(iIn)%doRampUp:        ", vars(iIn)%doRampUp
        Write (6,*) "   vars(iIn)%tRampUp:         ", vars(iIn)%tRampUp
        Write (6,*) "   vars(iIn)%doRampDown:      ", vars(iIn)%doRampDown
        Write (6,*) "   vars(iIn)%tRampDown:       ", vars(iIn)%tRampDown
        Write (6,*) "   vars(iIn)%doInternal:      ", vars(iIn)%doInternal
     End If
     !
  End If
  !
