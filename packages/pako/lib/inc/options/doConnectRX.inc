!
! Id: doConnectRX.inc,v 1.1 2005/03/23 14:25:55 pety Exp $
!
If      (iRec.Eq.0) Then
   !
   Where (vars(iIn,:)%wasConnected) vars(iIn,:)%isConnected = .True.
   !
Else If (iRec.Ge.1) Then
   !
   If (vars(iIn,iRec)%wasConnected) vars(iIn,iRec)%isConnected = .True.
   !
End If
!
