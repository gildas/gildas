!
!     $Id: optionTrecord.inc,v 1.0.5 2006-10-19 Hans Ungerechts
!     Family:   option 1 real parameter
!     Siblings: optionTsubscan.f90
!               optionTotf.f 
!               optionTrecord.f
!               optionTsegment.f
!               optionTphase.f90
!               optionTblanking.f90
!
!D      write (6,*) '      optionTrecord.f90 '
!
errorR = .False.
!
Option = 'TRECORD'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
!!$   Include 'inc/options/nyi.inc'
   !
   Write (messageText,*)                                                         &
        &      "is not yet implemented "                                         !
   Call PakoMessage(priorityE,severityE,command//"/"//OPTION,messageText)
!!$   Write (messageText,*)                                                         &
!!$        &      "with SWFREQUENCY use instead: SWFREQUENCY /nCycles "                              !
!!$   Call PakoMessage(priorityI,severityI,command//"/"//OPTION,messageText)
   errorL = .True.
   !
   Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
   !TBD   If (.Not.errorR) Then
   !TBD      Call checkR4co(command,option,rInput,                            &
   !TBD           &           vars%tRecord,                                   &
   !TBD           &           errorR)                                         !
   !TBD   End If
End If
ERROR = ERROR .Or. errorR .Or. errorL
!
