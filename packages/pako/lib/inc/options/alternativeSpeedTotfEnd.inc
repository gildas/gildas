  !
  !     Id: alternativeSpeedTotfEnd.inc,v 1.1.12 2012-02-24 Hans Ungerechts
  !
  If (vars(iTemp)%altOption.Eq."SPEED") Then
     !
     !        constant acceleration --> average speed is (speed1+speed2)/2
     vars(iTemp)%tOtf  =  vars(iTemp)%lengthOtf/                                 &
          &        ((vars(iTemp)%speedStart+vars(iTemp)%speedEnd)/2.0)           !
     !
     vIn       = vars(iTemp)%tOtf
     vStdMin   = Min(vars(iStd1)%tOtf,  vars(iStd2)%tOtf)
     vStdMax   = Max(vars(iStd1)%tOtf,  vars(iStd2)%tOtf)     
     vLimitMin = Min(vars(iLimit1)%tOtf,vars(iLimit2)%tOtf)
     vLimitMax = Max(vars(iLimit1)%tOtf,vars(iLimit2)%tOtf)
     !
     If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
        errorInconsistent = .True.
        Write (messageText,'(a,2F8.1,a,F8.1,a,F8.1,a,F8.1)')                     &
             &           ' value ',                  vars(iTemp)%speedStart,     &
             &                                       vars(iTemp)%speedEnd,       &
             &           ' implies /tOtf ',          vIn,                        &
             &           ' outside limits ',         vLimitMin,                  &
             &           ' to ',                     vLimitMax                   !
        Call pakoMessage(priorityE,severityE,                                    &
             &                command//" /speed",messageText)                    !
     Else If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
        Write (messageText,'(a,2F8.1,a,F8.1,a,F8.1,a,F8.1)')                     &
             &           ' value ',                  vars(iTemp)%speedStart,     &
             &                                       vars(iTemp)%speedEnd,       &
             &           ' implies /tOtf ',          vIn,                        &
             &           ' outside standard range ', vStdMin,                    &
             &           ' to ',                     vStdMax                     !
        Call pakoMessage(priorityW,severityW,                                    &
             &                command//" /speed",messageText)                    !
     End If
     !
  End If
  !
  If (vars(iTemp)%altOption.Eq."TOTF") Then
     vars(iTemp)%speedStart = vars(iTemp)%lengthOtf/vars(iTemp)%tOtf
     vars(iTemp)%speedEnd   = vars(iTemp)%lengthOtf/vars(iTemp)%tOtf
     !
     vIn       = vars(iTemp)%speedStart
     vStdMin   = Min(vars(iStd1)%speedStart,  vars(iStd2)%speedStart)
     vStdMax   = Max(vars(iStd1)%speedStart,  vars(iStd2)%speedStart)     
     vLimitMin = Min(vars(iLimit1)%speedStart,vars(iLimit2)%speedStart)
     vLimitMax = Max(vars(iLimit1)%speedStart,vars(iLimit2)%speedStart)
     !
     If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
        errorInconsistent = .True.
        Write (messageText,'(a,F8.1,a,F8.1,a,F8.1,a,F8.1)')                      &
             &           ' value ',                  vars(iTemp)%tOtf,           &
             &           ' implies /speed ',         vIn,                        &
             &           ' outside limits ',         vLimitMin,                  &
             &           ' to ',                     vLimitMax                   !
        Call pakoMessage(priorityE,severityE,                                    &
             &                command//" /tOtf", messageText)                    !
     Else If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
        Write (messageText,'(a,F8.1,a,F8.1,a,F8.1,a,F8.1)')                      &
             &           ' value ',                  vars(iTemp)%tOtf,           &
             &           ' implies /speed ',         vIn,                        &
             &           ' outside standard range ', vStdMin,                    &
             &           ' to ',                     vStdMax                     !
        Call pakoMessage(priorityW,severityW,                                    &
             &                command//" /tOtf", messageText)                    !
     End If
     !
  End If
  !
