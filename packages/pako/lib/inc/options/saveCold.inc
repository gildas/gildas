!
!     $Id$
!
      If (vars(iValue)%doCold) Then
         lineOut = " /cold yes"
      Else
         lineOut = " /cold no"
      End If
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
