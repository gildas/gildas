  !
  !     Id: optionAirmass.inc,v 1.2.4   2015-05-28 Hans Ungerechts
  !     Id: optionAirmass.inc,v 1.2.3   2014-06-12 Hans Ungerechts
  !     Id: optionAirmass.inc,v 1.0.6.1 2007-01-18 Hans Ungerechts
  !     Family:   range of real parameter
  !               or list of values followed by LIST
  !     Siblings: optionAirmass.f90
  !
  If (GV%doDebugMessages) Then
     Write (6,*) '      --> optionAirmass.f90 1.2.4    2015-05-28 '
  End If
  !
  errorR = .False.
  errorL = .False.
  !
  Option = 'AIRMASS'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     !D      Write (6,*) '      NARG:   ', sic_narg(iOption)
     !
     Call SIC_CH(LINE,iOption,sic_narg(iOption),                                 &
          &           cInput,lengthInput,.True.,errorC)                          !
     cInputUpper = cInput
     Call SIC_UPPER(cInputUpper)
     !D   Write (6,*) '      cInputUpper: ->', cInputUpper(1:lengthInput), '<-'
     !
     If (cInputUpper(1:lengthInput).Eq."LIST"                                    &
          &     .And. sic_narg(iOption) .Lt. 4) Then                             !
        !
        errorL = .True.
        messageText =   "requires list with at least 3 values: "                 &
             &        //" real real real [...] LIST"                             !
        Call pakoMessage(priorityE,severityE,command,messageText)
        !
     End If
     !
     If      ( .Not.errorL .And.                                                 &
          &                 (    cInputUpper(1:lengthInput).Eq."LIST"            &
          &                  .Or. sic_narg(iOption) .Ge. 6            )          &
          &  )   Then                                                            !
        !
        If (GV%doDebugMessages) Then
           Write (6,*) '      trying list of airmasses '
        End If
        !
        vars(iIn)%nAir = 0
        !
        iiMax = sic_narg(iOption)
        If (cInputUpper(1:lengthInput).Eq."LIST") iiMax = iiMax - 1
        !
        Do ii = 1, iiMax, 1
           !
           If (GV%doDebugMessages) Then
              Write (6,*) "      trying argument # ", ii
           End If
           !
           iArgument = ii
           !
           Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
           !
           If (.Not.errorR) Then
              Call checkR4co(command,option,rInput,                              &
                   &           airmassII,                                        &
                   &           errorR)                                           !
           End If
           !
           If (.Not. errorR ) Then
              vars(iIn)%hasAirmassList   = .True.
              vars(iIn)%nAir             =  vars(iIn)%nAir+1
              vars(iIn)%airmassList(ii)  =  rInput
           End If
           !
        End Do
        !
        If     (.Not.   Abs( vars(iIn)%airmassList(2)-vars(iIn)%airmassList(1))  &
             &         .Gt. 0                                                    &
             & )  Then                                                           !
           !
           Write (messageText,'(A,2F8.2)')                                       &
                &    " values in list are not strictly monotonic at ",           &
                &        vars(iIn)%airmassList(1), vars(iIn)%airmassList(2)      !
           Call pakoMessage(priorityW,severityW,command,messageText)
           !
        Else
           !
           Do ii = 3, vars(iIn)%nAir, 1
              !
              If (.Not. (vars(iIn)%airmassList(2)-vars(iIn)%airmassList(1))      &
                   &  * (vars(iIn)%airmassList(ii)-vars(iIn)%airmassList(ii-1))  &
                   &         .Gt. 0                                              &
                   & )  Then                                                     !
                 !
                 Write (messageText,'(A,2F8.2)')                                 &
                      & " values in list are not strictly monotonic at ",        &
                      &   vars(iIn)%airmassList(ii-1), vars(iIn)%airmassList(ii) !
                 Call pakoMessage(priorityW,severityW,command,messageText)
                 !
              End If
              !
           End Do
           !
        End If
        !
        If (GV%doDebugMessages) Then
           Do ii = 1, vars(iIn)%nAir, 1
              Write (6,*) "      vars(iIn)%airmassList(",ii,")    :    ",        &
                   &             vars(iIn)%airmassList(ii)                       !
           End Do
        End If
        !
        ERROR = ERROR .Or. errorR
        !
     Else If (.Not.errorL) Then
        !
        If (GV%doDebugMessages) Then
           Write (6,*) '      trying range of airmasses '
        End If
        !
        vars(iIn)%nAir = 0
        !
        ia1 = 0
        ia2 = 0
        ia3 = 0
        !
        If (SIC_PRESENT(iOption,0)) Then
           If (sic_narg(iOption) .Eq. 5) Then
              ia1 = 1
              ia2 = 3
              ia3 = 5
           Else If (sic_narg(iOption) .Eq. 3) Then
              Call SIC_CH(LINE,iOption,2,                                        &
                   &           cInput,lengthInput,.True.,errorC)                 !
              cInputUpper = cInput
              Call SIC_UPPER(cInputUpper)
              If (cInputUpper.Eq."TO") Then
                 ia1 = 1
                 ia2 = 3
              Else
                 ia1 = 1
                 ia2 = 2 
                 ia3 = 3
              End If
           Else If (sic_narg(iOption) .Eq. 2) Then
              ia1 = 1
              ia2 = 2
           Else If (sic_narg(iOption) .Eq. 1) Then
              ia1 = 1
           Else
              errorR = .True.
           End If
        End If
        !
        iArgument = ia1
        If (iArgument.Ge.1 .And. SIC_PRESENT(iOption,iArgument)) Then
           Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
           If (.Not.errorR) Then
              Call checkR4co(command,option,rInput,                              &
                   &           vars%Airmass%from,                                &
                   &           errorR)                                           !
           End If
        End If
        !
        If (.Not.errorR) Then
           iArgument = ia2
           If (iArgument.Ge.1 .And. SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
              If (.Not.errorR) Then
                 Call checkR4co(command,option,rInput,                           &
                      &              vars%Airmass%to,                            &
                      &              errorR)                                     !
              End If
           End If
        End If
        !
        If (.Not.errorR) Then
           iArgument = ia3
           !D   Write (6,*) "      iArgument:        ", iArgument
           If (iArgument.Ge.1 .And. SIC_PRESENT(iOption,iArgument)) Then
              Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
              If (.Not.errorR) Then
                 Call checkR4co(command,option,rInput,                           &
                      &              vars%Airmass%by,                            &
                      &              errorR)                                     !
              End If
           End If
        End If
        !
        If (.Not. errorR) Then
           vars(iIn)%hasAirmassList = .False.
        End If
        !
        If (errorR) Then
           messageText = "requires range: real [TO] real [[BY] real]"
           Call pakoMessage(priorityE,severityE,command,messageText)
        End If
        !
        ERROR = ERROR .Or. errorR
        !
     End If   !!!   (sic_narg(iOption) .Ge. 6)
     !
     ERROR = ERROR .Or. errorL
     !
     !D   Write (6,*) '          vars(iIn)%hasAirmassList:  ',  vars(iIn)%hasAirmassList
     If (GV%doDebugMessages) Then
        Write (6,*) '          errorR:   ', errorR
        Write (6,*) '          errorL:   ', errorL
        Write (6,*) '          ERROR:    ', ERROR
        Write (6,*) '      <-- optionAirmass.f90 1.2.4    2015-05-28 '
     End If
     !
  End If
  !
