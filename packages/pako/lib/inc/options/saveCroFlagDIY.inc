!
!  Id: saveCroFlagDIY.inc,v 1.0.10.5 2008-10-09 Hans Ungerechts
!
If      (segList(ii)%flagOn) Then
   lineOut = " /croFlag "//"O"
Else If (segList(ii)%flagRef) Then
   lineOut = " /croFlag "//"R"
Else
   lineOut = " /croFlag "//"O"
End If
!
Include 'inc/options/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
