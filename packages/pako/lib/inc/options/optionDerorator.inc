!
! Id: optionDerotator.inc,v 1.0.6.4 2007-03-08 Hans Ungerechts
!
!D Write (6,*) '   --> optionDerotator.inc '
!D Write (6,*) '       vars(iIn,iRec)%name   ', vars(iIn,ii)%name 
!
OPTION = 'DEROTATOR'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
If (SIC_PRESENT(iOption,0)) Then
   !
   !D Write (6,*) '         OPTION ', OPTION, 'is present'
   !
   If (        vars(iIn,iRec)%name .Eq. rec%HERA                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%HERA1                                &
        & .Or. vars(iIn,iRec)%name .Eq. rec%HERA2 ) Then                         !
      !
      If (SIC_NARG(iOption).Ne.2) Then
         messageText =                                                           &
              &     "2 parameters: angle [Real] and system [Char] are required"  !
         Call PakoMessage(priorityE,severityE,command,messageText)
         errorL = .True.
      End If
      ERROR = ERROR .Or. errorL
      !
      iArgument = 1
      If (.Not. ERROR) Then
         Call SIC_R4(LINE,iOption,iArgument,rInput,.True.,errorR)
         If (.Not.errorR) Then
            Call checkR4co(command,option,rInput,                                &
                 &           vars(:,iRec)%derotatorAngle,                        &
                 &           errorR)                                             !
         End If
      End If
      ERROR = ERROR .Or. errorR
      !
      iArgument = 2
      If (.Not. ERROR) Then
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &           cInput,lengthInput,.True.,errorC)                   !
            cInputUpper = cInput
            Call SIC_UPPER(cInputUpper)
         End If
      End If
      ERROR = ERROR .Or. errorC
      !
      If (.Not. ERROR) Then
         Call pakoUmatchKey (                                                    &
              &              keys=derotatorSystemChoices,                        &
              &              key=cInputUpper,                                    &
              &              command=command//"/"//option,                       &
              &              howto='Start Upper',                                &
              &              iMatch=iMatch,                                      & 
              &              nMatch=nMatch,                                      &
              &              error=errorM,                                       &
              &              errorCode=errorCode                                 &
              &             )                                                    !
         !
         If (.Not. errorM) Then
            vars(iIn,iRec)%derotatorSystem = derotatorSystemChoices(iMatch)
         End If
      End If
      ERROR = ERROR .Or. errorM
      !
      If (.Not. ERROR .And.                                                      &
           &          (vars(iIn,iRec)%derotatorSystem.Eq.derot%fra)              &
           &    ) Then                                                           !
         If (          vars(iIn,iRec)%derotatorAngle .Gt.                        &
              &        Max(derotatorLimitFrame(1),derotatorLimitFrame(2))        &
              &  .Or.  vars(iIn,iRec)%derotatorAngle .Lt.                        &
              &        Min(derotatorLimitFrame(1),derotatorLimitFrame(2))        &
              &   ) Then                                                         !
            Write (messageText,*)                                                &
                 &     vars(iIn,iRec)%derotatorAngle,                            &
                 &     " outside limits ",                                       &
                 &     derotatorLimitFrame,                                      &
                 &     " for system ", vars(iIn,iRec)%derotatorSystem            !
            Call PakoMessage(priorityE,severityE,command,messageText)
            errorR = .True.
         End If
      End If
      ERROR = ERROR .Or. errorR
      !
      If (.Not. ERROR) Then
         ! ensure same derotator parameters for all of HERA:
         vars(iIn,iHERA:iHERA2)%derotatorSystem = vars(iIn,iRec)%derotatorSystem
         vars(iIn,iHERA:iHERA2)%derotatorAngle  = vars(iIn,iRec)%derotatorAngle
      End If
      !
   Else
      !
      ERROR = .True.
      Write (messageText,*) " works only for RECEIVER HERA1 (HERA2)"        
      Call PakoMessage(priorityE,severityE,command//"/"//option,messageText)
      !
   End If
   !
End If










