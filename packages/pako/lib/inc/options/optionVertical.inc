!
!  Id: optionVertical.inc,v   1.1.15 2013-09-20 Hans Ungerechts
!  Id: optionVertical.inc,v   1.1.15 2013-08-26 Hans Ungerechts
!
!                             1.1.15 2013-08-26 E150 upgrade
!                             1.1.11 2011-09-29 EMIR upgrade
!
!  FAMILY:    
!  strict !!
!  Siblings:  optionHorizontal
!             optionVertical
!
If (GV%doDebugMessages) Then
   Write (6,*) "      --> optionVertical.inc,v 1.1.15"
End If
!
OPTION = 'VERTICAL'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
l  =  len_trim(vars(iIn,iRec)%name(1:l))
!
If (SIC_PRESENT(iOption,0)) Then
   !
   If (.Not.   (                                                                 &   !!!   TBD for HEMT
        &      vars(iIn,iRec)%name .Eq. rec%E090                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E150                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E230                                 &
        & .Or. vars(iIn,iRec)%name .Eq. rec%E300 )                               &
        & ) Then                                                                 !
      errorL = .True.
      messageText = "only valid for EMIR bands "
      Call PakoMessage(priorityE,severityE,                                      &
           &   command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText) !
   End If
   !
   nArgument = SIC_NARG(iOption)
   !
   If (.Not. errorL) Then
      !
      If (nArgument.Lt.1) Then
         vars(iIn,iRec)%verticalSubbands =  vars(iIn,iRec)%sideBand
         messageText = "no subband specified; use: "                             &
              &       //vars(iIn,iRec)%verticalSubbands                          !
         Call PakoMessage(priorityI,severityI,                                   &
              & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText)!
      End If
      !
!!$                                                                                   !!  obsolete with upgrade E150 2013-09
!!$      If (iRec.Eq.iE150 .Or. iRec.Eq.iE230) Then                                   !!  EMIR upgrade:
!!$      If (iRec.Eq.iE150) Then                                                      !!  v 1.1.11 
!!$         If (nArgument.Gt.1) Then
!!$            errorL = .True.
!!$            messageText = "at most 1 subband [string] possible"
!!$            Call PakoMessage(priorityE,severityE,                                &
!!$              & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText)!
!!$         End If
!!$      End If
      !
!!$   If (iRec.Eq.iE090 .Or. iRec.Eq.iE230 .Or. iRec.Eq.iE300) Then                
      If (       iRec.Eq.iE090 .Or. iRec.Eq.iE150                                & !!  EMIR upgrade E150 2013-09
           &                   .Or. iRec.Eq.iE230 .Or. iRec.Eq.iE300) Then       ! 
         If (nArgument.Gt.2) Then
            errorL = .True.
            messageText = "at most 2 subbands [string] possible"
            Call PakoMessage(priorityE,severityE,                                &
              & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText)!
         End If
      End If
      !
      If (nArgument.Gt.6) Then
         errorL = .True.
         messageText = "at most 6 subband(s) [string] allowed"
         Call PakoMessage(priorityE,severityE,                                   &
              & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,messageText)!
      End If
      !
   End If
   !
   If (.Not. errorL .And. nArgument.Ge.1) Then
      !
      vars(iIn,iRec)%verticalSubbands = " "
      !
      Do iArgument = 1, nArgument, 1
         !
         Call SIC_CH(LINE,iOption,iArgument,                                     & 
              &           cInput,lengthInput,.True.,errorC)                      !
         !
         ERROR = ERROR .Or. errorC .Or. errorL
         !
         ! *** Check against valid codes
         !
         If (.Not. ERROR) Then
            Call pakoUmatchKey (                                                 &
                 &              keys=subbandChoices,                             &
                 &              key=cInput,                                      &
                 &              command=command//"/"//option,                    &
                 &              howto='Start Upper',                             &
                 &              iMatch=iMatch,                                   &
                 &              nMatch=nMatch,                                   &
                 &              error=errorC,                                    &
                 &              errorCode=errorCode                              &
                 &             )                                                 !
            !
            ERROR = ERROR .Or. errorC
            !
            If    (         GV%iUserLevel .Lt. iExperienced                      &
                 &  .And. (iMatch.Eq.iLSB .Or. iMatch.Eq.iUSB) ) Then            !
               errorL = .True.
               messageText = subbandChoices(iMatch)//                            &
                    &        "requires higher user level"                        !
               Call PakoMessage(priorityE,severityE,                             &
                    & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,      &
                    & messageText)                                               !
            End If
            ERROR = ERROR .Or. errorL
            !
            If (.Not. ERROR) Then
               !
               l1 = Len_trim (vars(iIn,iRec)%verticalSubbands)
               l2 = Len_trim(subbandChoices(iMatch))
               !
               If      (iMatch.Eq.iESBnone) Then
                  !
                  vars(iIn,iRec)%verticalSubbands = GPnone
                  !
!!$                                                                                   !!  obsolete with upgrade E150 2013-09
!!$               Else If ( (iRec.Eq.iE150.Or.iRec.Eq.iE230.Or.iRec.Eq.iE300)       & !!  EMIR upgrade:
!!$               Else If ( (iRec.Eq.iE150)                                         & !!  v 1.1.11 
!!$                    &    .And. (iMatch.Eq.iLo .Or. iMatch.Eq.iUo)                &
!!$                    &  ) Then                                                    !
!!$                  !                     
!!$                  ERROR = .True.
!!$                  messageText = "subband: "                                      &
!!$                       &       //subbandChoices(iMatch)(1:l2)//                  &
!!$                       &        " does not exist for this band "                 !
!!$                  Call PakoMessage(priorityE,severityE,                          &
!!$                       & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,   &
!!$                       & messageText)                                            !
                  !
               Else If (Index(vars(iIn,iRec)%verticalSubbands,                   &
                    &    subbandChoices(iMatch)(1:l2)).Lt.1) Then                !
                  !
                  vars(iIn,iRec)%verticalSubbands =                              &
                       &   vars(iIn,iRec)%verticalSubbands(1:l1+1)//             &
                       &                     subbandChoices(iMatch)(1:l2)        !
                  !
               Else
                  !
                  ERROR = .True.
                  messageText = "multiple key for subband: "//                   &
                       &                 subbandChoices(iMatch)(1:l2)            !
                  Call PakoMessage(priorityE,severityE,                          &
                       & command//" "//vars(iIn,iRec)%name(1:l)//" /"//option,   &
                       & messageText)                                            !
                  !
               End If
               !
            End If
            !
         End If
         !
      End Do
      !
   End If
   !
End If
