!
!   checkConsistentEfficiencyScale.inc, v 1.1.3 2010-07-23 Hans Ungerechts
!                        introduced in  v 1.1.3 2010-07-23 Hans Ungerechts
!
!D Write (6,*) "   --> checkConsistentEfficiencyScale.inc, v 1.1.3"
!
!D Write (6,*) "       vars(iIn,iRec)%effForward:  ", vars(iIn,iRec)%effForward 
!D Write (6,*) "       vars(iIn,iRec)%effBeam:     ", vars(iIn,iRec)%effBeam
!D Write (6,*) "       vars(iIn,iRec)%scale:       ", vars(iIn,iRec)%scale
!
If (vars(iIn,iRec)%effForward .Lt. vars(iIn,iRec)%effBeam) Then
   !
   Write (messageText,*) "/efficiency forwardEff", vars(iIn,iRec)%effForward,    &
        &                " less than beamEff",  vars(iIn,iRec)%effBeam,          &
        &                " is non-standard "                                     !
   Call PakoMessage(priorityW,severityW,command,messageText)
   !
   ! NOTE: effBeam > effForward is supported by CLASS MODIFY BEAM_EFF !!
   !       checked 2010-07-23 on mrt-lx3 in
   !       /sw/gildas/gildas-exe-apr10/x86_64-debian4.0-ifort-32bits/bin/class
   !
End If
!
If (vars(iIn,iRec)%scale .Eq. scale%antenna) Then
   !
   If (vars(iIn,iRec)%effForward .Ne. vars(iIn,iRec)%effBeam) Then
      !
      errorInconsistent = .True.
      !
      Write (messageText,*) "/scale ",vars(iIn,iRec)%scale,                      &
           &             "requires beamEfficiency equal to forwardEfficiency "   !
      Call PakoMessage(priorityE,severityE,command,messageText)
      !
   End If
   !
End If
!
If (vars(iIn,iRec)%scale .Eq. scale%beam) Then
   !
   If (vars(iIn,iRec)%effForward .Eq. vars(iIn,iRec)%effBeam) Then
      !
      Write (messageText,*) "/scale ",vars(iIn,iRec)%scale,                      &
           &             "with forwardEff equal to beamEff ",                    &
           &             "is the same as /scale ", scale%antenna                 !
      Call PakoMessage(priorityW,severityW,command,messageText)
      !
   End If
   !
End If
!
