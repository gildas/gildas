!
! ID: optionSystemOffsets.inc,v 1.0.5.2 2006-11-27 Hans Ugerechts
!
! TBD: disallow systems that are not yet supported
! TBD: default system base on SOURCE command
!
OPTION = 'SYSTEM'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                        &
           &           cInput,lengthInput,.True.,errorC)
      cInputUpper = cInput
      Call SIC_UPPER(cInputUpper)
      !
   Else
      errorC = .True.
      messageText = "one parameter is required"
      Call pakoMessage(priorityE,severityE,                                      &
           &                   command//"/"//option,messageText)
   End If
   !
   !**   search for cInputUpper ***
   !
   ERROR = ERROR .Or. errorC
   !
   If (.Not. Error) Then
      !
      Call pakoUmatchKey (                                                       &
           &              keys=offsetChoicesPako,                                &
           &              key=cInputUpper,                                       &
           &              command=command//"/"//option,                          &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         &
           &              nMatch=nMatch,                                         &
           &              error=errorM,                                          &
           &              errorCode=errorCode                                    &
           &             )
      !
   End If
   !
   !D Write (6,*) " optionSystemOffsets -> iMatch         ", iMatch
   !D Write (6,*) "                        cInputUpper    ", cInputUpper
   !D Write (6,*) "                        offsetChoicesPako  ", offsetChoicesPako
   !
   If (.Not. errorM) Then
      If (iMatch.Eq.iPro .Or. iMatch.Eq.iTru .Or. iMatch.Eq.iNas) Then
         iiSystemOffset     = iMatch
         ccSystemOffset     = offsetChoicesPako(iMatch)
      Else
         errorM = .True.
         Write (messageText,*)                                                   &
              &    offsetChoicesPako(iMatch)//                                   &
              &    " is not yet supported!"                                      !
         Call PakoMessage(priorityE,severityE,                                   &
              &    command(1:l)//" /SYSTEM",                                     &
              &    messageText)                                                  !
      End If
   End If
   !
End If
!
ERROR = ERROR .Or. errorM
!
