  !
  !  Id: readOptionsSubscan.inc, v1.2.3 2013-1029 Hans Ungerechts
  !
  If (GV%doDebugMessages) Then
     Call pako_message(seve%t,programName,                                       &
       &    " module DIYlist ---> readOptionsSubscan.inc, v1.2.3 2013-10-29 ")   ! trace execution
  End If
  !
  Include 'inc/options/optionCroFlag.inc'
  Include 'inc/options/optionSystem.inc'
  !
  !D Write (6,*) "           vars(iIn)%doTrack:     ", vars(iIn)%doTrack
  !
  Include 'inc/options/optionTune.inc'
  !
  If      (vars(iIn)%doTrack       ) Then
     Include 'inc/options/optionTsubscan.inc'
  Else If (vars(iIn)%doLinearOtf   ) Then
     Include 'inc/options/optionRampLinear.inc'
     Include 'inc/options/optionNext.inc'
     Include 'inc/options/optionSpeed.inc'    
     Include 'inc/options/optionTotf.inc'     
  Else If (vars(iIn)%doLissajousOtf) Then
     Include 'inc/options/optionRamp.inc'
     Include 'inc/options/optionTotf.inc'     
  Else If (nArguments.Le.2) Then
     Include 'inc/options/optionTsubscan.inc'
  Else If (nArguments.Le.4) Then              
     Include 'inc/options/optionRamp.inc'
     Include 'inc/options/optionSpeed.inc'    
     Include 'inc/options/optionTotf.inc'     
  Else If (nArguments.Le.8) Then              
     Include 'inc/options/optionRamp.inc'
     Include 'inc/options/optionTotf.inc'     
  End If
  !
