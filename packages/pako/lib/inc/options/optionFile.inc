!
!  Id: optionFile.inc, v 0.9 2005-08-02 Hans Ungerechts
!
OPTION = 'FILE'
!
Call indexCommmandOption                                                     & 
     &     (command,option,commandFull,optionFull,                           & 
     &      commandIndex,optionIndex,iCommand,iOption,                       & 
     &      errorNotFound,errorNotUnique)
!
!     TBD: short call; test for errors
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,                                    & 
           &           cInput100,lengthInput,.True.,errorC)
      If (errorC) Then
         Write (messageText,*) 'value: "',cInput100,'" not valid'
         Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,          &
              &           messageText)
      Else
         saveFile = cInput100
      End If
   Else
      errorC = .True.
      messageText = "parameter fileName is required"
      Call PAKOMESSAGE(priorityE,severityE,command//"/"//option,             &
           &           messageText)
   End If
End If
!     
ERROR = ERROR .Or. errorC 
!
