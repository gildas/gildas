!
!  Id:       optionClearDIYlist.inc,v 1.1.13 2012-10-29 Hans Ungerechts
!  Siblings: optionClearOffsets.inc
!
  OPTION = 'CLEAR'
  !
  Call indexCommmandOption                                                       &   
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  errorL    = .False.
  !
  iArgument = 1
  If (SIC_PRESENT(iOption,0)) Then
     doClear = .True.
     If (SIC_PRESENT(iOption,iArgument)) Then
        Call SIC_L4(LINE,iOption,iArgument,                                      &
             &                  doClear,.True.,errorL)                           !
     End If
  Else
     doClear = .False.
  End If
  ERROR = ERROR .Or. errorL
  !
  If (doClear .And. .Not. Error) Then
     !
     Call countSubscans(value=0)
     Call countSegments(value=0)
     condElevation%isSet     = .False.
     GV%condElevation%isSet  = .False.
     nLissajous = 0
     !
     vars(:)%purpose         = GPnone
     vars(:)%hasPurpose      = GPnoneL
     !
  End If
  !
  !D Write (6,*) "      -> /Clear: ", doClear
  !
