!
!     Id: optionReceiver.inc,v 1.1.5 2011-03-14 Hans Ungerechts
!     Family:   single char variabe, choice of values
!     Siblings: optionFunction.f90
!     Siblings: optionReceiver.f90
!
OPTION = 'RECEIVER'
!
Call indexCommmandOption                                               &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
If (SIC_PRESENT(iOption,0)) Then
   !
   nArguments = SIC_NARG(iOption)
   !
   If (iBack .Ne. GPnoneI .And. iPart .Ne. GPnoneI) Then
      !
      iArgument = 1
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,                        & 
              &           cInput,lengthInput,.True.,errorC)
      Else
         errorL = .True.
         messageText =                                                   &
              &        "no receiver name specified "
         Call pakoMessage(priorityE,severityE,command,messageText)
      End If
      !
      ERROR = ERROR .Or. errorC .Or. errorL
      !
      If (.Not. ERROR) Then
         !
         Call pakoUmatchKey (                                            &
              &              keys=receiverChoices,                       &
              &              key=cInput,                                 &
              &              command='BACKEND',                          &
              &              howto='Start Upper',                        &
              &              iMatch=iMatch,                              &
              &              nMatch=nMatch,                              &
              &              error=errorC,                               &
              &              errorCode=errorCode                         &
              &             )
         !
         ERROR = ERROR .Or. errorC
         !
         If (.Not. ERROR) Then
            vars(iIn,iBack)%receiverName = receiverChoices(iMatch)
         End If
         !
      End If
      !
      !  ** EMIR
      If (        vars(iIn,iBack)%receiverName .Eq. rec%E090                     &
           & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E150                     &
           & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E230                     &
           & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E300 ) Then              !
         !
         If (nArguments.Lt.3) Then
            errorL = .True.
            messageText = " 3 arguments required for EMIR: "                     &
                 &      //" band polarization subband/sideband "                 !
            Call pakoMessage(priorityE,severityE,command,messageText)
         End If
         !
         ERROR = ERROR .Or. errorL
         !
         ! *** polarization
         !
!!$         If (.Not.ERROR .And. .Not.shortSyntax) Then
!!$            !
!!$         iOption   = 0
!!$         iArgument = 7
            iArgument = 2                                                        !! /Receiver
            !
            If (SIC_PRESENT(iOption,iArgument)) Then
               Call SIC_CH(LINE,iOption,iArgument,                               &
                    &           cInput,lengthInput,.True.,errorC)                !
            Else
               vars(iIn,iBack)%polarization = GPnone
               messageText = "no EMIR polarization specified "
               Call pakoMessage(priorityI,severityI,command,messageText)
            End If
            !
            If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
               !
               Call pakoUmatchKey (                                              &
                    &              keys=polarizationChoices,                     &
                    &              key=cInput,                                   &
                    &              command='BACKEND',                            &
                    &              howto='Start Upper',                          &
                    &              iMatch=iMatch,                                &
                    &              nMatch=nMatch,                                &
                    &              error=errorC,                                 &
                    &              errorCode=errorCode                           &
                    &             )                                              !
               !
               ERROR = ERROR .Or. errorC
               !
               If (.Not. ERROR) Then
                  vars(iIn,iBack)%polarization = polarizationChoices(iMatch)
               End If
               !
            End If
            !
!!$         End If
         !
         ! *** subband
!!$         !
!!$         If (.Not.ERROR .And. .Not.shortSyntax) Then
!!$            !
!!$         iOption   = 0
!!$         iArgument = 8
            iArgument = 3                                                        !! /Receiver
            !
            If (SIC_PRESENT(iOption,iArgument)) Then
               Call SIC_CH(LINE,iOption,iArgument,                               &
                    &           cInput,lengthInput,.True.,errorC)                !
            Else
               vars(iIn,iBack)%subband = GPnone
               messageText = "no EMIR subband specified "
               Call pakoMessage(priorityI,severityI,command,messageText)
            End If
            !
            !D          Write (6,*) "      cInput   ", cInput
            !
            If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
               !
               Call pakoUmatchKey (                                              &
                    &              keys=subbandChoices,                          &
                    &              key=cInput,                                   &
                    &              command='BACKEND',                            &
                    &              howto='Start Upper',                          &
                    &              iMatch=iMatch,                                &
                    &              nMatch=nMatch,                                &
                    &              error=errorC,                                 &
                    &              errorCode=errorCode                           &
                    &             )                                              !
               !
               ERROR = ERROR .Or. errorC
               !
               If (iBack.Eq.iBBC) Then               !!  
                  !
                  If (.Not. ERROR) Then
                     vars(iIn,iBack)%subband = subbandChoices(iMatch)
                  End If
                  !
               Else                                  !!  not BBC
                  !
                  If    (         GV%iUserLevel .Lt. iExperienced                &
                       &  .And. (iMatch.Eq.iLSB .Or. iMatch.Eq.iUSB) ) Then      !
                     errorL = .True.
                     messageText = subbandChoices(iMatch)//                      &
                          &        "requires higher user level"                  !
                     Call PakoMessage(priorityE,severityE,                       &
                          & command,                                             &
                          & messageText)                                         !
                  End If
                  ERROR = ERROR .Or. errorL
                  !
                  If (.Not. ERROR) Then
                     vars(iIn,iBack)%subband = subbandChoices(iMatch)
                  End If
                  !
               End If
               !
            End If
            !
!!$         End If
         !
      Else  !!  --> not EMIR
         !
         ! *** RECEIVER 2
         !
         iArgument = 2
         !
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                             &  
                 &           cInput,lengthInput,.True.,errorC)
         Else
            vars(iIn,iBack)%receiverName2 = GPnone
            messageText =                                                   &
                 &        "no 2nd receiver name specified "
            Call pakoMessage(priorityI,severityI,command,messageText)
         End If
         !
         ERROR = ERROR .Or. errorC 
         !
         If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
            !
            Call pakoUmatchKey (                                            &
                 &              keys=receiverChoices,                       &
                 &              key=cInput,                                 &
                 &              command='BACKEND',                          &
                 &              howto='Start Upper',                        &
                 &              iMatch=iMatch,                              &
                 &              nMatch=nMatch,                              &
                 &              error=errorC,                               &
                 &              errorCode=errorCode                         &
                 &             )
            !
            ERROR = ERROR .Or. errorC
            !
            If (.Not. ERROR) Then
               vars(iIn,iBack)%receiverName2 = receiverChoices(iMatch)
            End If
            !
         End If
         !

      End If

   Else
      !
      errorL = .True.
      messageText =                                                      &
           &        "needs valid backend name and part"
      Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
      !
   End If
   !
End If
!
!D Write (6,*) "   <-- optionReceiver "
!D Write (6,*) "      iBack:                         ", iBack
!D Write (6,*) "      ipart:                         ", iPart
!D Write (6,*) "      vars(iIn,iBack)%receiverName   ", vars(iIn,iBack)%receiverName 
!D Write (6,*) "      vars(iIn,iBack)%polarization   ", vars(iIn,iBack)%polarization
!D Write (6,*) "      vars(iIn,iBack)%subBand        ", vars(iIn,iBack)%subBand     
