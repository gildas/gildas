!
!     $Id$
!     Family:   option -- simple logical
!     Siblings: optionBalance.f90
!               optionCalibrate.f90
!               optionFlagRef.f90
!               optionMore.f90
!               optionUpdate.f90
!               optionWriteToSeg.f90
!               optionZigzag.f90
!
!D      write (6,*) "   --> optionFlagRef.f90 "
!
      OPTION = 'FLAGREF'
!
      Call indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      errorL    = .False.
!
      iArgument = 1
      If (SIC_PRESENT(iOption,0)) Then
         vars(iIn)%flagRef = .True.
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_L4(LINE,iOption,iArgument,                        &
     &                  vars(iIn)%flagRef,.True.,errorL)
         Endif
      Endif
      ERROR = ERROR .Or. errorL
!
      If (.Not.error .And. vars(iIn)%flagRef) Then
         vars(iIn)%flagOn = .False.
      End If
!
