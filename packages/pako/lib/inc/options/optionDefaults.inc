!
! $Id$
!
    If (.Not. ERROR) Then
       option = 'DEFAULTS'
       Call indexCommmandOption                                         &
            &        (command,option,commandFull,optionFull,            &
            &        commandIndex,optionIndex,iCommand,iOption,         &
            &        errorNotFound,errorNotUnique)
       setDefaults = SIC_PRESENT(iOption,0)
       If (setDefaults) Then
          Include 'inc/variables/setDefaults.inc'
       Endif
    Endif
!
