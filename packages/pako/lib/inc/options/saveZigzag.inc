!
!     $Id$
!
      If (vars(iValue)%doZigzag) Then
         lineOut = " /zigzag yes"
      Else
         lineOut = " /zigzag no"
      End If
!
      Include 'inc/options/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
