  !
  !     Id: optionNSamples.inc,v 1.3.0 2017-09-25 Hans Ungerechts
  !     Family:   option 1 integer parameter / with conditions
  !     Siblings: 
  !
  !     errorL = .False.
  !
  !D   Write (6,*) '  ->  optionNSamples v 1.3.0 2017-09-13 : '
  !D   Write (6,*) '      iBack: ', iBack
  !D   Write (6,*) "      GV%privilege:  ", GV%privilege
  !D   Write (6,*) "      GV%userLevel:  ", GV%userLevel
  !D   Write (6,*) "      GV%iUserLevel: ", GV%iUserLevel
  !D   Write (6,*) "      "
  !
  Option = 'NSAMPLES'
  !
  Call indexCommmandOption                                                       &
       &     (command,option,commandFull,optionFull,                             &
       &      commandIndex,optionIndex,iCommand,iOption,                         &
       &      errorNotFound,errorNotUnique)                                      !
  !
  iArgument = 1
  !
  If (SIC_PRESENT(iOption,0)) Then
     !
     If (    .Not. (       GV%iUserLevel.Ge.iExperienced                         &
          &          .Or.  GV%privilege.Eq.setPrivilege%privileged               &
          &          .Or.  GV%privilege.Eq.setPrivilege%staff                    &
          &          .Or.  GV%privilege.Eq.setPrivilege%ncsTeam                  &
          &        )     )     Then                                              !
        !
        errorL = .True.
        messageText = " requires higher user level or privilege "
        Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
        !
     Else If (.Not. (iBack.Eq.iBBC.Or.iBack.Eq.iVESPA.Or.iBack.Eq.iWILMA) ) Then  
        !
        errorL = .True.
        messageText = "works only with "//bac%BBC//" "//bac%VESPA//              &
             &                    " or "//bac%WILMA                              !
        Call pakoMessage(priorityE,severityE,command//'/'//option,messageText)
        !
     Else If ( iBack.Eq.iBBC ) Then 
        !
        Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
        If (.Not.errorI) Then
           Call checkI4co(command,option,iInput,                                 &
                &           vars(:,iBack)%nSamples, errorI)                      !
           If ( .Not.errorI .And. iInput.Eq.0 ) Then
              vars(iIn,iBack)%samplesIsSet = .False.
           Else If ( .Not. ( iInput.Ge.4                                         &
                &     )   )   Then                                               !
              errorI = .True.
              messageText =    " must be at least 4 "                            &
                   &        // " for "//  backendChoices(iBack)                  !
              Call PakoMessage(priorityE,severityE,                              &
                   &           command//"/"//option,messageText)                 !
           Else If ( .Not.errorI ) Then
              vars(iIn,iBack)%samplesIsSet = .True.
           End If
        End If
        !
     Else
        !
        Call SIC_I4(LINE,iOption,iArgument,iInput,.True.,errorI)
        If (.Not.errorI) Then
           Call checkI4co(command,option,iInput,                                 &
                &           vars(:,iBack)%nSamples, errorI)                      !
           If ( .Not.errorI .And. iInput.Eq.0 ) Then
              vars(iIn,iBack)%samplesIsSet = .False.
           Else If ( .Not. ( iInput.Eq.1                                         &
                &       .Or. iInput.Eq.2  .Or. iInput.Eq.4  .Or. iInput.Eq.8     &
                &       .Or. iInput.Eq.16 .Or. iInput.Eq.32 .Or. iInput.Eq.64    &
                &       .Or. iInput.Eq.128                                       &
                &     )   )   Then                                               !
              errorI = .True.
              messageText =    " must be a power of 2 "                          &
                   &        // " for "//  backendChoices(iBack)                  !
              Call PakoMessage(priorityE,severityE,                              &
                   &           command//"/"//option,messageText)                 !
           Else If ( .Not.errorI ) Then
              vars(iIn,iBack)%samplesIsSet = .True.
           End If
        End If
        !
     End If
  End If
  !
  !
  ERROR = ERROR .Or. errorI .Or. errorL
  !
  !D   Write (6,*) '  <-  optionNSamples v 1.3.0 2017-09-13 : '
  !
  ! -------------------------------------------------------------------------------
  !D
  !D       Write (6,*) ' vars(iIn,iBack)%nSamples:    ', vars(iIn,iBack)%nsamples
  !D       Write (6,*) ' vars(iIn,iBack)%nSamples:    ', vars(iIn,iBack)%nsamples
  !D       Write (6,*) ' vars(iIn,iBack)%samplesIsSet: ', vars(iIn,iBack)%samplesIsSet
  !D       !
  !D       Write (6,*) ' vars(iTemp,iBack)%nSamples:  ', vars(iTemp,iBack)%nsamples
  !D       Write (6,*) ' vars(iTemp,iBack)%nSamples:  ', vars(iTemp,iBack)%nsamples
  !D       Write (6,*) ' vars(iTemp,iBack)%samplesIsSet: ', vars(iTemp,iBack)%samplesIsSet
  !D       !
  !D       Write (6,*) ' vars(iValue,iBack)%nSamples: ', vars(iValue,iBack)%nsamples
  !D       Write (6,*) ' vars(iValue,iBack)%nSamples: ', vars(iValue,iBack)%nsamples
  !D       Write (6,*) ' vars(iValue,iBack)%samplesIsSet: ', vars(iValue,iBack)%samplesIsSet
  !D       !
