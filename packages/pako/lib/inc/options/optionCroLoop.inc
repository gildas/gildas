!
!     Id: optionCroLoop.inc,v 1.1.12 2012-02-23 Hans Ungerechts
!
      OPTION = 'CROLOOP'
!
      CALL indexCommmandOption                                           &
     &     (command,option,commandFull,optionFull,                       &
     &      commandIndex,optionIndex,iCommand,iOption,                   &
     &      errorNotFound,errorNotUnique)
!
!     D:      WRITE (6,*) command," : ",iCommand,"   ",option," : ",iOption
!
!     TBD: short call; test for errors
!
      iArgument = 1
!
      IF (SIC_PRESENT(iOption,0)) THEN
         IF (SIC_PRESENT(iOption,iArgument)) THEN
            CALL SIC_CH(LINE,iOption,iArgument,                          &
     &           cInput100,lengthInput,.TRUE.,errorC)
            cInput100Upper = cInput100
            CALL SIC_UPPER(cInput100Upper)
!
!     TBD: allow longer (variable) string
!
!     D:            WRITE (6,*) " cInput100 = ->",cInput100,"<-",lengthInput
!     D:            WRITE (6,*) " cInput100 = ->",cInput100(1:lengthInput),"<-"
!
!     search for cInput100 in croCodeChoices
!
            errorNotFound         = .FALSE.
!
            iInput = 0
!     TBD: max number of characters should be a variable
            DO jj = 1,100,1
               if (.not. cInput100Upper(jj:jj).eq." ") then
                  choiceIndex = 0
                  do ii = 2,3,1    !!   2012-02-23 to disallow "C" in croLoop
!!    TBD: needed to allow "C": do ii = 1,3,1
!!    TBD: number of choices should be a variable
                     if (index(croCodeChoices(ii),                       &
     &                    cInput100Upper(jj:jj)).eq.1) then
                        choiceIndex = ii
                        iInput = iInput + 1
                        cInput100Two(iInput:iInput) =                    &
     &                       cInput100Upper(jj:jj)
                     end if
                  end do
                  errorNotFound = errorNotFound .or. choiceIndex .eq. 0 
               end if
            END DO
!
!     TBD: check for reasonable codes at start end end 
!          depending on switching mode
!
            errorC = errorC .OR. errorNotFound
!
            IF (errorC) THEN
               write (messageText,*) 'value: "',cInput100,'" not valid'
               CALL pakoMessage(priorityF,severityE,' /CROLOOP',messageText)
            else
               vars(iIn)%croCode      = cInput100Upper
               vars(iIn)%croCodeBites = cInput100Two
               vars(iIn)%croCodeCount = lenc(cInput100Two)
               CALL checkI4(option,iInput,                               &
     &              vars%croCodeCount,                                   &
     &              errorI)
               if (errorI) then
                  write (messageText,*)                                  &
     &              "invalid number ",iInput," of valid letters in code"
                  CALL pakoMessage(priorityF,severityE,' /CROLOOP',messageText)
               end if
            END IF
!
!     D:            WRITE (6,*) " choiceIndex =   ",choiceIndex
!     D:            WRITE (6,*) " croCode  = ->",croCode(iIn),"<-"
!
         ELSE
            errorC = .TRUE.
            messageText = "parameter croCode is required"
            CALL pakoMessage(priorityF,severityE,' /CROLOOP',messageText)
         END IF
      END IF
!
      ERROR = ERROR .OR. errorC .or. errorI
!
