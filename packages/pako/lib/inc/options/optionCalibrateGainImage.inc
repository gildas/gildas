!
!     Id: optionGainImage.inc,v 1.1.0 2009-03-19 Hans Ungerechts
!     Family:   option -- logical with parameter
!     Siblings: 
!
OPTION = 'GAINIMAGE'
!
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
!
errorC    = .False.
errorL    = .False.
!
iArgument = 1
!
If (SIC_PRESENT(iOption,0)) Then
   !
   errorL = .True.
   !
   messageText = " is not supported with current receivers "                  
   Call PakoMessage(priorityE,severityE,                                         &
        &           "calibrate"//" /gainImage ",messageText)                     !
   !
End If
!
ERROR = ERROR .Or. errorC .Or. errorL
!
!!$   If (SIC_PRESENT(iOption,iArgument)) Then
!!$      Call SIC_CH(LINE,iOption,iArgument,                        & 
!!$           &           cInput,lengthInput,.True.,errorC)
!!$      !
!!$      ERROR = ERROR .Or. errorC
!!$      !
!!$      If (.Not. ERROR) Then
!!$         !
!!$         Call SIC_upper(cInput)
!!$         !
!!$         If (cInput.Eq.".FALSE." .Or. cInput.Eq."NO") Then
!!$            !
!!$            vars(iIn)%doGainImage  = .False.
!!$            vars(iIn)%receiverName = GPnone
!!$            !
!!$         Else
!!$            !
!!$            Call pakoUmatchKey (                                            &
!!$                 &              keys=receiverChoices,                       &
!!$                 &              key=cInput,                                 &
!!$                 &              command='CALIBRATE',                        &
!!$                 &              howto='Start Upper',                        &
!!$                 &              iMatch=iMatch,                              &
!!$                 &              nMatch=nMatch,                              &
!!$                 &              error=errorC,                               &
!!$                 &              errorCode=errorCode                         &
!!$                 &             )
!!$            !
!!$            If (.Not.errorC) Then
!!$               If (iMatch.Le.iD270) Then
!!$                  Call queryReceiver(receiverChoices(iMatch),isConnected)
!!$                  If (isConnected) Then
!!$                     vars(iIn)%doGainImage  = .True.
!!$                     vars(iIn)%receiverName = receiverChoices(iMatch)
!!$                  Else
!!$                     errorC = .True.
!!$                     messageText =   "receiver "                                 &
!!$                          &        //receiverChoices(iMatch)                     &
!!$                          &        //" is not connected "
!!$                     Call PakoMessage(priorityE,severityE,                       &
!!$                          &           "calibrate"//" /gainImage ",messageText)
!!$                  End If
!!$               Else
!!$                  errorC = .True.
!!$                  messageText = "not possible for receiver "                        &
!!$                       &        //receiverChoices(iMatch)
!!$                  Call PakoMessage(priorityE,severityE,                             &
!!$                       &           "calibrate"//" /gainImage ",messageText)
!!$               End If
!!$               !
!!$            End If
!!$            !
!!$         End If
!!$         !
!!$      End If
!!$      !
!!$   Else
!!$      !
!!$      errorC = .True.
!!$      messageText = "one argument (RX or NO) is required "
!!$      Call PakoMessage(priorityE,severityE,                             &
!!$           &           "calibrate"//" /gainImage ",messageText)
!!$      !
!!$   End If
!!$!
!!$End If
