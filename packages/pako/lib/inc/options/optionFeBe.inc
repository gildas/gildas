!
!     $Id$
!     Family:   option -- simple integer
!     Siblings: optionFeBe
!
      OPTION = 'FEBE'
!
      CALL indexCommmandOption                                         &
     &     (command,option,commandFull,optionFull,                     &
     &      commandIndex,optionIndex,iCommand,iOption,                 &
     &      errorNotFound,errorNotUnique)
!
      iArgument = 1
!
      IF (SIC_PRESENT(iOption,0)) THEN
         CALL SIC_I4(LINE,iOption,iArgument,iInput,.TRUE.,errorI)
         if (.NOT. errorI) CALL checkI4(option,iInput,                 &
     &        vars%feBe,                                               &
     &        errorI)
      ENDIF
!
      ERROR = ERROR .OR. errorI
!




