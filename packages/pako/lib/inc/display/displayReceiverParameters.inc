  !
  ! Id: displayReceiverParameters.inc,v 1.2.3 2014-04-21 Hans Ungerechts
  !     based on:
  !     displayReceiverParameters.inc,v 1.1.0 2008-12-02 Hans Ungerechts
  !
  l = lenc(vars(iValue,ii)%name)
  valueText = vars(iValue,ii)%name(1:8)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+5+4
  !
  l = lenc(vars(iValue,ii)%lineName)
  valueText = vars(iValue,ii)%lineName(1:12)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+1+18
  !
  Write (valueText,'(f10.6)') vars(iValue,ii)%frequency%value
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+1+12
  !
  l = lenc(vars(iValue,ii)%sideBand)
  valueText = vars(iValue,ii)%sideBand(1:3)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+2+3
  !
  l = lenc(vars(iValue,ii)%frequency%doppler)
  valueText = vars(iValue,ii)%frequency%doppler(1:7)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+2+7
  !
  If (ii.NE.iRxTest) Then                                                        !  !! exception: test receiver
     !
     If (        vars(iValue,ii)%name .Eq. rec%E090                              &
          & .Or. vars(iValue,ii)%name .Eq. rec%E150                              &
          & .Or. vars(iValue,ii)%name .Eq. rec%E230                              &
          & .Or. vars(iValue,ii)%name .Eq. rec%E300 ) Then                       !
        valueText    = "/width"
        larg = lenc(valueText)
        Call xmlOut(10,56,valueText(1:larg),style=DSinactive)
     Else
        l = lenc(vars(iValue,ii)%width)
        valueText = vars(iValue,ii)%width(1:6)
        larg = lenc(valueText)
        Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     End If
     iCoffset = iCoffset+1+6
     !
     Write (valueText,'(f7.3)') vars(iValue,ii)%gainImage%value
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+1+7
     !
     Write (valueText,'(f4.0)') vars(iValue,ii)%gainImage%db
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+4
     !
     If (vars(iValue,ii)%tempColdCode.Eq.GPnone) Then
        Write (valueText,'(f5.1)') vars(iValue,ii)%tempCold
        larg = lenc(valueText)
     Else
        valueText = vars(iValue,ii)%tempColdCode
        larg = 5
     End If
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+5
     !
     If (vars(iValue,ii)%tempAmbientCode.Eq.GPnone) Then
        Write (valueText,'(f5.1)') vars(iValue,ii)%tempAmbient
        larg = lenc(valueText)
     Else
        valueText = vars(iValue,ii)%tempAmbientCode
        larg = 5
     End If
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+5
     !
     Write (valueText,'(f5.2)') vars(iValue,ii)%effForward
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+5
     !
     Write (valueText,'(f5.2)') vars(iValue,ii)%effBeam
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+5
     !
     l = lenc(vars(iValue,ii)%scale)
     valueText = vars(iValue,ii)%scale(1:7)
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+7
     !
  Else
     !
     !
  End If      !!!      If (ii.NE.iRxTest)

