!
!     $Id$
!
      variableName = "latitude          "
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset,variableName(1:larg))
!
      valueText=vars(iValue)%latitude%text
      larg = lenc(valueText)
      Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
!
      Write (valueText,'(f12.6)') vars(iValue)%latitude%deg
      larg = lenc(valueText)
      Call xmlOut(iRoffset+3,iCoffset,valueText(1:larg))
!
      Write (valueText,'(f12.8)') vars(iValue)%latitude%rad
      larg = lenc(valueText)
      Call xmlOut(iRoffset+4,iCoffset,valueText(1:larg))
!
