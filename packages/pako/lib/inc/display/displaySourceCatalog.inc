  !
  ! Id: displaySourceCatalog.inc, v 1.2.4 2015-07-16 Hans Ungerechts
  ! Id: displaySourceCatalog.inc, v 1.0.2 2006-01-03 Hans Ungerechts
  !
  l = len_trim(vars(iValue)%sourceCatalog) 
  !D   Write (6,*) " vars(iValue)%sourceCatalog(1:l) -->",                            &
  !D        &        vars(iValue)%sourceCatalog(1:l), "<--"                           !
  !
  If (vars(iValue)%sourceCatalog.Ne.GPnone) Then
     variableName = "/catalog"
     larg = lenc(variableName)
     Call xmlOut(iRoffset+1,iCoffset,variableName(1:larg))
     !
     l = len_trim(vars(iValue)%sourceCatalog)
     If (l.Le.22) Then
        Call xmlOut(iRoffset+2, iCoffset,                                        &
             &              vars(iValue)%sourceCatalog(l-22:l-4))                !
     Else
        Call xmlOut(iRoffset+2, iCoffset,                                        &
             &       "..."//vars(iValue)%sourceCatalog(l-22:l-4))                !
     End If
     !
  End If
  !

