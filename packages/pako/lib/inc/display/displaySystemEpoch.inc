!
!     $Id$
!
!D      Write (6,*) "  vars(iValue)%systemName:     ",                  &
!D     &               vars(iValue)%systemName        
!D      Write (6,*) "  vars(iValue)%epoch:          ",                  &
!D     &               vars(iValue)%epoch                               
!D!
      variableName = "system"
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset,variableName(1:larg))
!
      if (vars(iValue)%systemName.eq.sys%equatorial.or.                         &
     &    vars(iValue)%systemName.eq.sys%ecliptic) then
         Write (valueText,'(f10.1)') vars(iValue)%epoch
         larg = lenc(valueText)
         Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
         valueText=vars(iValue)%equinoxSystemName(1:1)
         Call xmlOut(iRoffset+1,iCoffset+3,valueText(1:1))
         larg = lenc(vars(iValue)%systemName)
         valueText(1:larg)=vars(iValue)%systemName(1:larg)
         Call xmlOut(iRoffset+1,iCoffset,valueText(1:2))
      Else
         larg = lenc(vars(iValue)%systemName)
         valueText(1:larg)=vars(iValue)%systemName(1:larg)
         Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
      end if

!


