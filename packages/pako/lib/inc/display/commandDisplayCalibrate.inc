!
!  Id: commandDisplayCalibrate.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!  Family:   commandDisplayOM
!  Siblings: commandDisplayCalibrate.inc
!            commandDisplayFocus.inc
!            commandDisplayOnOff.inc
!            commandDisplayOtfMap.inc
!            commandDisplayPointing.inc
!            commandDisplayTrack.inc
!            commandDisplayTip.inc
!            commandDisplayVlbi.inc
!
Include 'inc/display/headerDisplayOM.inc'
!
variableName = "CALIBRATE"
Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
!
valueTextLong    = "(Calibration)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
l = Len_trim(GV%angleUnitSetC)
valueText    = "["//GV%angleUnitSetC(1:l)//"]"
Call xmlOut (iRoffset, iCoffset+44, valueText)
!
iRoffset = iRoffset+1
!
Include 'inc/display/displayAmbient.inc'
Include 'inc/display/displayCold.inc'
Include 'inc/display/displaySky.inc'
Include 'inc/display/displaySystem.inc'
Include 'inc/display/displayTcalibrate.inc'
!!TBD Include 'inc/display/displayFast.inc'
!!TBD Include 'inc/display/displayNull.inc'
!!TBD Include 'inc/display/displayTest.inc'
!!TBD Include 'inc/display/displayVariable.inc'
!
iRoffset = iRoffset+1
Include 'inc/display/displayGainImage.inc'
Include 'inc/display/displayGrid.inc'
!
Close (iUnit, IOSTAT=ioerr )
!
