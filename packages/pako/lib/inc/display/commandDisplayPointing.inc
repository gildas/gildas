  !
  !  Id: commandDisplayPointing.inc,v 1.2.3  2014-09-23 Hans Ungerechts
  !  Id: commandDisplayPointing.inc,v 1.1.12 2012-02-29 Hans Ungerechts
  !
  !  Family:   commandDisplayOM
  !  Siblings: commandDisplayCalibrate.inc
  !            commandDisplayFocus.inc
  !            commandDisplayOnOff.inc
  !            commandDisplayOtfMap.inc
  !            commandDisplayPointing.inc
  !            commandDisplayTrack.inc
  !            commandDisplayTip.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "POINTING"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  l = Len_trim(GV%angleUnitSetC)
  valueText    = "["//GV%angleUnitSetC(1:l)//"]"
  Call xmlOut (iRoffset, iCoffset+44, valueText)
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayLength.inc'
  !
  iRoffset = iRoffset+1
  !
  !!TBD:      Include 'inc/display/displayCalibrate.inc'
  !!TBD:      Include 'inc/display/displayFeBe.inc'
  !!TBD:      Include 'inc/display/displayMore.inc'
  Include 'inc/display/displayNotf.inc'
  Include 'inc/display/displayTotf.inc'
  !!TBD:      Include 'inc/display/displayTrecord.inc'
  !!TBD:      Include 'inc/display/displayUpdate.inc'
  !
  iRoffset = iRoffset+1
  Include 'inc/display/displayTuneOffsets.inc'
  Include 'inc/display/displayTuneTime.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayDoubleBeam.inc'
  Include 'inc/display/displayAeLoop.inc'
  !
  If (GV%condElevation%isSet.And.GV%condElevation%maximum/deg.Lt.85.00-0.01) Then
     !
     iRoffset = iRoffset+1
     Include 'inc/display/displayConditionElevation.inc'
     !
  End If
  !
  Close (iUnit, IOSTAT=ioerr )
  !
