!
! Id: commandDisplayOffsets.inc,v 1.0.9.4 2009-02-17 Hans Ungerechts
!
! Family:  
!
Include 'inc/display/openDisplay.inc'
!
! *** displayClearAreaOffsets
!
iCoffset    =  0
!
Do iRoffset =  7,7,1
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        &       "                                                           ")   !
   Call xmlOutBlank(iRoffset,iCoffset+57,                                        &
        &       "                                                           ")   !
End Do
!
!D      Write (6,*)   "   --> displayOffsets.f90 "
!D      Write (6,*)   "       iCoffset :  ", iCoffset
!D      Write (6,*)   "       iRoffset :  ", iRoffset
!
iCoffset     =  1
iCindent     =  0
iRoffset     =  7
!
valueText = "OFFSETS"
larg = Len_trim(valueText)
Call xmlOut(iRoffset, iCoffset, valueText(1:larg), style=DScommand)
!
nCount = 0
!
If (varsOffsets(iValue,ipro)%isSet) Then
   !
   nCount = nCount+1
   !
   iCoffset = iCoffset+10
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,ipro)%point%x/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,ipro)%point%y/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   valueTextLong = "/SYS "//offsPako%pro(1:5)
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
End If
!
If (varsOffsets(iValue,itru)%isSet) Then
   !
   nCount = nCount+1
   !
   iCoffset = iCoffset+larg+1
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,itru)%point%x/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,itru)%point%y/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   valueTextLong = "/SYS "//offsPako%tru(1:5)
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
!
End If
!
If (varsOffsets(iValue,inas)%isSet) Then
   !
   nCount = nCount+1
   !
   iCoffset = iCoffset+larg+1
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,inas)%point%x/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   Write (valueTextLong,'(f8.1)') varsOffsets(iValue,inas)%point%y/GV%anglefactorR
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   !
   iCoffset = iCoffset+larg+1
   valueTextLong = "/SYS "//offsPako%nas(1:7)
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg),style=DSwarning)
!
End If
!
If (nCount.Ge.1) Then
!
   iCoffset = iCoffset+larg+1
   larg = Len_trim(GV%angleUnitSetC)
   valueTextLong = '['//GV%angleUnitSetC(1:larg)//']'
   larg = Len_trim(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
!
End If
!
!!$Do ii = nDimOffsetChoices, 1, -1
!!$   If (varsOffsets(iValue,ii)%isSet) Then
!!$      !
!!$      nCount = nCount+1
!!$      !
!!$      iCoffset = 10
!!$      Include 'inc/display/displayOffsets.inc'
!!$      iCoffset = iCoffset+30
!!$      Include 'inc/display/displayOffsetsSystem.inc'
!!$      !
!!$   End If
!!$End Do
!!$!
!!$If (nCount .Gt. 1) Then
!!$   !
!!$   iCoffset = iCoffset+20
!!$   If (nCount-1 .Eq. 1) Then
!!$      Write (valueTextLong,'("(+",I2," more offset)")') nCount-1
!!$   Else
!!$      Write (valueTextLong,'("(+",I2," more offsets)")') nCount-1
!!$   End If
!!$   larg = lenc(valueTextLong)
!!$   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
!!$   !
!!$End If
!
If (doWarnOffsets) Then
   !
   iCoffset     =  1
   iCindent     =  0
   iRoffset     =  7
   !
   valueText    = "offsets"
   Call SIC_UPPER(valueText)
   l= Len_trim(valueText)
   !
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
   !
End If
!
If (doWarnOffsets .Or. doWarnOM) Then
   !
   iCoffset     =  1
   iCindent     =  0
   iRoffset     = 16
   !
   valueText    = GV%observingMode
   Call SIC_UPPER(valueText)
   l= Len_trim(valueText)
   !
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
End If
!
If (GV%notReadyReceiverOffsets) Then
   iRoffset = 10
   iCoffset =  1
   valueText    = "RECEIVER"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSwarning)
Else
   iRoffset = 10
   iCoffset =  1
   valueText    = "RECEIVER"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
End If
!
Close (iUnit, IOSTAT=ioerr )
!
