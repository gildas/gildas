  !
  ! Id: commandDisplayOtfMap.inc,v 1.2.3  2014-08-28 Hans Ungerechts
  ! Id: commandDisplayOtfMap.inc,v 1.2.3  2014-05-27 Hans Ungerechts
  ! Id: commandDisplayOtfMap.inc,v 1.1.12 2012-02-27 Hans Ungerechts
  !
  !  Family:   commandDisplayOM
  !  Siblings: commandDisplayCalibrate.inc
  !            commandDisplayFocus.inc
  !            commandDisplayLissajous.inc
  !            commandDisplayOnOff.inc
  !            commandDisplayOtfMap.inc
  !            commandDisplayPointing.inc
  !            commandDisplayTrack.inc
  !            commandDisplayTip.inc
  !            commandDisplayVlbi.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "OTFMAP"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  valueTextLong    = "(On-The-Fly OTF Map)"
  Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
  !
  l = Len_trim(GV%angleUnitSetC)
  valueText    = "["//GV%angleUnitSetC(1:l)//"]"
  Call xmlOut (iRoffset, iCoffset+44, valueText)
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayStartEnd.inc'
  Include 'inc/display/displayLengthOtf.inc'
  !!TBD: Include 'inc/display/displayBalance.inc'
  Include 'inc/display/displaySystem.inc'
  Include 'inc/display/displayReference.inc'
  Include 'inc/display/displayCroLoop.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayNotf.inc'
  !!TBD: Include 'inc/display/displayResume.inc'
  Include 'inc/display/displayStep.inc'
  Include 'inc/display/displaySpeed.inc'
  Include 'inc/display/displayTotf.inc'
  !!deprecated: Include 'inc/display/displayAltOption.inc'
  !!TBD: Include 'inc/display/displayTrecord.inc'
  Include 'inc/display/displayTreference.inc'
  Include 'inc/display/displayZigzag.inc'
  !
  iRoffset = iRoffset+1
  Include 'inc/display/displayTuneOffsets.inc'
  Include 'inc/display/displayTuneTime.inc'
  !
  If (GV%condElevation%isSet.And.GV%condElevation%maximum/deg.Lt.85.00-0.01) Then
     !
     iRoffset = iRoffset+1
     Include 'inc/display/displayConditionElevation.inc'
     !
  End If
  !
  Close (iUnit, IOSTAT=ioerr )
  !
