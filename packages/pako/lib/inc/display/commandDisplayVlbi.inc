!
! $Id: commandDisplayVlbi.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!  Family:   commandDisplayOM
!  Siblings: commandDisplayCalibrate.inc
!            commandDisplayFocus.inc
!            commandDisplayOnOff.inc
!            commandDisplayOtfMap.inc
!            commandDisplayPointing.inc
!            commandDisplayTrack.inc
!            commandDisplayTip.inc
!            commandDisplayVlbi.inc
!
Include 'inc/display/headerDisplayOM.inc'
!
variableName = "VLBI"
Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
!
valueTextLong    = "(Track source position)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
!!$l = Len_trim(GV%angleUnitSetC)
!!$valueText    = "["//GV%angleUnitSetC(1:l)//"]"
!!$Call xmlOut (iRoffset, iCoffset+44, valueText)
!
iRoffset = iRoffset+1
!
!! Include 'inc/display/display*.inc'
!
iRoffset = iRoffset+1
!
!! Include 'inc/display/display*inc'
!
Close (iUnit, IOSTAT=ioerr )
!
