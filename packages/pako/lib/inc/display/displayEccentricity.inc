!
!     $Id$
!
      variableNameLong = " Eccentricity "
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f10.6)') vars(iValue)%eccentricity
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset+3,valueTextLong(1:larg))
!


