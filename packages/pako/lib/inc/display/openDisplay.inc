!
!   Id: openDisplay.inc,v 1.0.7 2008-08-14 Hans Ungerechts
!
!
!D Write (6,*) "   --> openDisplay.inc "
!D Write (6,*) "       ioUnit%Display  ", ioUnit%Display
!D Write (6,*) "       iUnit           ", iUnit
!
iUnit = ioUnit%Display
ioerr= 0
!
Open (UNIT=iUnit, FILE='pakoDisplay.txt', STATUS='UNKNOWN', ACCESS='APPEND',     &
     &            IOSTAT=ioerr )                                                 !
!
If (ioerr .Ne. 0) Then
   Write (messageText,*) "ERROR ", ioerr, " in accessing pakoDisplay ", iunit
   Call PakoMessage(priorityW,severityW,                                         &
        &           command,messageText)                                         !
   Write (messageText,*) "if problem persists, ",                                &
        &                "try to restart (pako and) pakoDisplay "                !
   Call PakoMessage(priorityW,severityW,                                         &
        &           command,messageText)                                         !
End If
!
Call displayTextSetOutputUnit(iUnit, errorCall)
!
