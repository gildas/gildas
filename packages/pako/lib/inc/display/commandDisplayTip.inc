  !
  !  Id: commandDisplayTip.inc,v 1.2.4    2015-04-24  Hans Ungerechts
  !  Id: commandDisplayTip.inc,v 1.2.3    2013-05-29  Hans Ungerechts
  !  Id: commandDisplayTip.inc,v 1.2.2    2013-04-09  Hans Ungerechts
  !         --> do not display Tpaddle (obsolete)
  !  based on
  !  Id: commandDisplayTip.inc,v 1.0.10.7 2008-10-28 Hans Ungerechts
  !
  !  Family:   commandDisplayOM
  !  Siblings: commandDisplayCalibrate.inc
  !            commandDisplayFocus.inc
  !            commandDisplayOnOff.inc
  !            commandDisplayOtfMap.inc
  !            commandDisplayPointing.inc
  !            commandDisplayTrack.inc
  !            commandDisplayTip.inc
  !            commandDisplayVlbi.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "TIP"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  valueTextLong    = "(Antenna Tipping/Skydip)"
  Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayAzimuth.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayAirmass.inc'
  !!$  Include 'inc/display/displayTpaddle.inc'
  Include 'inc/display/displayTsubscan.inc'
  !
  iRoffset = iRoffset+1
  Include 'inc/display/displaySlew.inc'
  !
  iRoffset = iRoffset+1
  Include 'inc/display/displayTune.inc'
  Include 'inc/display/displayTuneTime.inc'
  !
  Close (iUnit, IOSTAT=ioerr )
  !
