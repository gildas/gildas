  !
  !  Id: commandDisplayDIYlist.inc,v 1.2.3 2014-10-16 Hans Ungerechts
  !  Id: commandDisplayDIYlist.inc,v 1.1.2 2010-03-03 Hans Ungerechts
  !
  !  Family: commandOM.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "DIY"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  valueTextLong    = "(subscan sequence)"
  Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
  !
  l = Len_trim(GV%angleUnitSetC)
  valueText    = "["//GV%angleUnitSetC(1:l)//"]"
  Call xmlOut (iRoffset, iCoffset+44, valueText)
  !
  variableName = "# segments"
  iRoffset = iRoffset+1
  Call displayTextI (command,iRoffset,iCoffset+iCindent,variableName,nSegments)
  !
  iRoffset = iRoffset+1
  !
  !D   Write (6,*) " commandDisplayDIYlist -> GV%purposeIsSet : ", GV%purposeIsSet
  !D   Write (6,*) " commandDisplayDIYlist -> GV%purpose      : -->", GV%purpose, "<--"
  !
  If ( GV%purposeIsSet ) Then
     variableName = "/purpose"
     iRoffset = iRoffset+1
     Call displayTextC    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableName,GV%purpose)                                         !
  End If
  !
  iRoffset = iRoffset+1
  !
  If (.Not.doClear) Then
     Include 'inc/display/displayConditionElevation.inc'
  End If
  !
  Include 'inc/display/displayClear.inc'
