!
!     displayProject.inc,v 1.0.4.1 2006-09-26 Hans Ungerechts
!
l = lenc(pakoVersionString)
!D Write (6,*) "   pakoVersionString:  ->",pakoVersionString(1:l),"<-"
!
displayLine = 'paKo '// pakoVersionString(1:l)
l = lenc(displayLine)
!D Write (6,*) "   displayLine:  ->",displayLine(1:l),"<-"
!
iRoffset =  47
iCoffset = 101
Call xmlOut(iRoffset, iCoffset, displayLine(1:l),style=DSok)
!




