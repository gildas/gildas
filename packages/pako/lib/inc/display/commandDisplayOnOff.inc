!
! Id: commandDisplayOnOff.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!  Family:   commandDisplayOM
!  Siblings: commandDisplayCalibrate.inc
!            commandDisplayFocus.inc
!            commandDisplayOnOff.inc
!            commandDisplayOtfMap.inc
!            commandDisplayPointing.inc
!            commandDisplayTrack.inc
!            commandDisplayTip.inc
!            commandDisplayVlbi.inc
!
Include 'inc/display/headerDisplayOM.inc'
!
variableName = "ONOFF"
Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
!
valueTextLong    = "(Position Switching)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
l = Len_trim(GV%angleUnitSetC)
valueText    = "["//GV%angleUnitSetC(1:l)//"]"
Call xmlOut (iRoffset, iCoffset+44, valueText)
!
iRoffset = iRoffset+1
!
Include 'inc/display/displayXoffsetYoffset.inc'
Include 'inc/display/displaySystem.inc'
Include 'inc/display/displayReference.inc'
!!$! TBD:      Include 'inc/display/displaySourceName.inc'
!
iRoffset = iRoffset+1
!
!!TBD: Include 'inc/display/displayBalance.inc'
!!TBD: Include 'inc/display/displayCalibrate.inc'
Include 'inc/display/displayNsubscans.inc'
Include 'inc/display/displayTsubscan.inc'
!
iRoffset = iRoffset+1
Include 'inc/display/displaySymmetric.inc'
Include 'inc/display/displaySwWobbler.inc'
!
If (doWarnOffsets) Then
   !
   iCoffset     =  1
   iCindent     =  0
   iRoffset     =  7
   !
   valueText    = "offsets"
   Call SIC_UPPER(valueText)
   l= Len_trim(valueText)
   !
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
End If
!
Close (iUnit, IOSTAT=ioerr )
!
