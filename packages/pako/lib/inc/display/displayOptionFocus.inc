  !
  !  Id: displayOptionFocus.inc,v 1.2.3    2014-07-07 Hans Ungerechts
  !
  If (vars(iValue)%doFocus) Then
     !
     variableName = "/focus [mm]           "
     iRoffset = iRoffset+1
     Call displayTextC    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableName," ")                                                !
     !
     Do ii = 1, min(6,vars(iValue)%nFocus), 1
        !
        Write (valueText,'(f5.1)') vars(iValue)%focusList(ii)
        larg = lenc(valueText)
        Call xmlOut(iRoffset,iCoffset+iCindent+8+6*ii,valueText(1:larg))
        !
     End Do
     !
     If  (  vars(iValue)%nFocus.GT.6) Then
        iRoffset  =  iRoffset+1
        Do ii = 7, min(12,vars(iValue)%nFocus), 1
           !
           Write (valueText,'(f5.1)') vars(iValue)%focusList(ii)
           larg = lenc(valueText)
           Call xmlOut(iRoffset,iCoffset+iCindent+8+6*(ii-6),valueText(1:larg))
           !
        End Do
     End If
     !
     If  (  vars(iValue)%nFocus.GT.6) Then
        iRoffset  =  iRoffset+1
        Do ii = 13, min(18,vars(iValue)%nFocus), 1
           !
           Write (valueText,'(f5.1)') vars(iValue)%focusList(ii)
           larg = lenc(valueText)
           Call xmlOut(iRoffset,iCoffset+iCindent+8+6*(ii-12),valueText(1:larg))
           !
        End Do
     End If
     !
     iRoffset  =  iRoffset+1
     !
     variableName = "direction"
     Call displayTextC    (command,iRoffset,iCoffset+iCindent+2,                 &
          &     variableName," ")                                                !                                                                   
      Call xmlOut(iRoffset,iCoffset+iCindent+14,                                 &
           &      vars(iValue)%directionFocus(1:1))
     !
  Else   
     !
     variableName = "/focus [mm]           "
     iRoffset = iRoffset+1
     Call xmlOut(iRoffset,iCoffset+iCindent,variableName,style=DSinactive)
     !
     Do ii = 1, min(6,vars(iValue)%nFocus), 1
        !
        Write (valueText,'(f5.1)') vars(iValue)%focusList(ii)
        larg = lenc(valueText)
        Call xmlOut(iRoffset,iCoffset+iCindent+8+6*ii,valueText(1:larg),         &
             &                                         style=DSinactive)         !
        !
     End Do
     !
     iRoffset  =  iRoffset+1
     !
     variableName = "direction"
     Call xmlOut(iRoffset,iCoffset+iCindent+2,                                   &
          &      variableName,style=DSinactive)                                  !
     Call xmlOut(iRoffset,iCoffset+iCindent+14+5,                                &
          &      vars(iValue)%directionFocus(1:1),style=DSinactive)              !
     !
  End If
  !

