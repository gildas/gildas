  !
  !     Id: displayNext.inc,v 1.2.3 2013-10-29 Hans Ungerechts
  !
  If (.Not. vars(iValue)%doNextSubscan) Then
     !
     variableName = "/next subscan"
     iRoffset = iRoffset+1
     variableNameLong = "/next [subscan] "
     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSwarning)
     Write (valueText,*)  vars(iValue)%doNextSubscan
     Call xmlOut(iRoffset,iCoffset+iCindent+21,valueText,style=DSwarning)
     !
  End If
