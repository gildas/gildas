!
!     $Id$
!
      variableName = "/initialize         "
      iRoffset = iRoffset+1
      call displayTextL    (command,iRoffset,iCoffset+iCindent,          &
     &     variableName,vars(iValue)%doInitialize)
!
      If (vars(iValue)%doneInitialize) Then
         variableName = " --> doneInitialize "
         iRoffset = iRoffset+1
         call displayTextL    (command,iRoffset,iCoffset+iCindent,       &
     &     variableName,vars(iValue)%doneInitialize)
      End If
!
