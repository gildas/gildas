!
!     $Id$
!
      variableNameLong = "Arg.Perih.[deg] "
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f11.6)') vars(iValue)%argumentOfPerihelion
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset+4,valueTextLong(1:larg))
!


