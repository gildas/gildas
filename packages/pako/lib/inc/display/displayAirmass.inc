  !
  !     Id: displayAirmass.inc, v 1.2.4 2015-05-28 Hans Ungerechts
  !
  !D   Write (6,*) "      --> displayAirmass.inc "
  !D   Write (6,*) "          vars(iValue)%nAir:   ", vars(iValue)%nAir
  !
  If (vars(iValue)%hasAirmassList) Then
     !
     variableNameLong = "/airmass "
     iRoffset = iRoffset+1
     Call displayTextC   (command,iRoffset,iCoffset+iCindent,                    &
          &     variableNameLong," " )                                           !
     !
     Do ii = 1, vars(iValue)%nAir, 5
        !
        jj = min(ii+4, vars(iValue)%nAir)
        !D         Write (6,*) vars(iValue)%airmassList(ii:jj) 
        !D         Write (6,'(5F5.1)') vars(iValue)%airmassList(ii:jj) 
        Write (valueText36,'(5F5.1)')                                            &
             &     vars(iValue)%airmassList(ii:jj)                               !
        iRoffset = iRoffset+1
        Call xmlOut (iRoffset, iCoffset+22, valueText36)
        !
     End Do
     !
     iRoffset = iRoffset+1
     !
  Else
     !
     variableNameLong = "/airmass   (from)    "
     iRoffset = iRoffset+1
     Call displayTextR    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableNameLong,vars(iValue)%airmass%from)                      !
     !
     variableNameLong = "           (to)      "
     iRoffset = iRoffset+1
     Call displayTextR    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableNameLong,vars(iValue)%airmass%to)                        !
     !
     variableNameLong = "           (by)      "
     iRoffset = iRoffset+1
     Call displayTextR    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableNameLong,vars(iValue)%airmass%by)                        !
     !
  End If
  !
