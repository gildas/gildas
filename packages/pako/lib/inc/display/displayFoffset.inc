!
! Id: displayFoffset.inc,v 1.1.0 2008-12-15 Hans Ungerechts
!
If (        vars(iValue,ii)%name .Eq. rec%E090                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E150                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E230                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E300 ) Then                            !
   !
   iRoffset = iRoffset+1
   iCoffset = 20
   !
   If (vars(iValue,ii)%fOffset%value.Ne.GPnoneD) Then
      !
      variableName = "/Foffset"
      larg = Len_trim(variableName)
      Call xmlOut(iRoffset,iCoffset,variableName(1:larg))
      iCoffset = iCoffset+larg+1
      !
      Write (valueText,'(f10.6)') vars(iValue,ii)%fOffset%value
      Call sic_upper(valueText)
      larg = Len_trim(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      !
   End If
   !
End If
