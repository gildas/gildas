!
!  Id: displayTotf.inc,v 1.0.2 2006-01-05 Hans Ungerechts
!
If (vars(iValue)%altOption.Eq."TOTF" .Or. vars(iValue)%altOption.Eq.GPnone) Then
   iRoffset = iRoffset+1
   variableName = "/tOtf"
   Call displayTextR    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%tOtf)
Else If (vars(iValue)%altOption.Eq."SPEED") Then
   iRoffset = iRoffset+1
   variableNameLong = "/tOtf"
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   Write (valueText,'(f12.3)') vars(iValue)%tOtf
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
End If
!

