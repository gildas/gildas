  !
  !    Id: displayCenterXY.inc,v 1.1.12 2012-02-29 Hans Ungerechts
  !
  variableNameLong = "/center          x  y"
  iRoffset = iRoffset+1
  Call displayText2R   (command,iRoffset,iCoffset+iCindent,                      &
       &    variableNameLong,vars(iValue)%pCenter%x/GV%anglefactorR,             &
       &                     vars(iValue)%pCenter%y/GV%anglefactorR)             !
  !
