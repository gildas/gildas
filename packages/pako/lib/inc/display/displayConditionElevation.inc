!
!  Id: displayConditionElevation.inc,v 1.1.2 2010-03-03 Hans Ungerechts
!
If (GV%condElevation%isSet) Then
   iRoffset = iRoffset+1
   variableNameLong = "CONDITIONS"
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSwarning)
   iRoffset = iRoffset+1
   variableNameLong = "elevation less than"
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSwarning)
   Write (valueText,'(f12.3)') GV%condElevation%maximum/deg
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSwarning)
End If
!
