!
!  Id: commandDisplayReceiver.inc,v 1.2.3  2014-09-01 Hans Ungerechts
!  Id: commandDisplayReceiver.inc,v 1.1.12 2012-02-20 Hans Ungerechts
!
!  Family:  displayReceiver.f90
!
Include 'inc/display/openDisplay.inc'
!
! *** 'displayClearAreaReceiver'
!
iCoffset     =  1
iCindent     =  3
!
Do iRoffset = 10,15,1
   !
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        &   "                                                  ")                !
   !        0    5   10   15   20   25   30   35   40   45   50
   Call xmlOutBlank(iRoffset,iCoffset+50,                                        &
        &   "                                                  ")                !
   Call xmlOutBlank(iRoffset,iCoffset+100,                                       &
        &   "                             ")                                     !
   !
End Do
!
iRoffset     = 10
!
!D Write (6,*)   "   --> displayReceiver.f90 "
!D Write (6,*)   "              iRec: ", iRec
!D      write (6,*)   "       iCoffset :  ", iCoffset
!D      write (6,*)   "       iRoffset :  ", iRoffset
!
iCoffset = 1
!
If (vars(iValue,iBolo)%isConnected) Then
   !
   valueText    = "RECEIVER"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
   iCoffset = iCoffset+14
   !
   valueTextLong    = "bolometerName"
   larg = lenc(valueTextLong)
   Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   iCoffset = iCoffset+2+12
   !
!!$   valueText    = " gainBolo"
!!$   larg = lenc(valueText)
!!$   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
!!$   iCoffset = iCoffset+4+7-2
!!$   !
!!$   valueText    = " channel"
!!$   larg = lenc(valueText)
!!$   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
!!$   iCoffset = iCoffset+2+5
   !
   If (NIKApixel%isSet) Then
      !
      valueText    = " pixel"
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+2+5
      !
      valueTextLong    = "Nasmyth-Offsets"
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
      iCoffset = iCoffset+0+15
      !
      valueTextLong    = " (lookup)"
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg),style=DSwarning)
      iCoffset = iCoffset+2+5
      !
   End If
   !
   Do ii = 1,nDimReceivers,1
      If (vars(iValue,ii)%isConnected) Then
         iCoffset = 1
         iRoffset = iRoffset+1
         Include 'inc/display/displayBolometerParameters.inc'
      End If
   End Do
   !
   Include 'inc/display/headerDisplayBackend.inc'
   !
Else
   !
   valueText    = "RECEIVER"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
   iCoffset = iCoffset+9
   !
   valueText    = "lineName"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+14
   !
   valueText    = "frequency"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+10
   !
   valueText    = "[GHz]"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+9
   !
   valueText    = "SB"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+3
   !
   valueText    = "/doppler"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+10
   !
   valueText    = "/width"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+9
   !
   valueText    = "/gain"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+6
   !
   valueText    = "[dB]"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+7
   !
   valueText    = "/tempLoad"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+14
   !
   valueText    = "/efficiency"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+13
   !
   valueText    = "/scale"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+6
   !
   Do ii = 1,nDimReceivers,1
      If (vars(iValue,ii)%isConnected) Then
         iCoffset = 1
         iRoffset = iRoffset+1
         Include 'inc/display/displayReceiverParameters.inc'
         Include 'inc/display/displayDerotator.inc'
         Include 'inc/display/displayFoffset.inc'
         Include 'inc/display/displayHorizontalVertical.inc'
      End If
   End Do
   !
End If
!
If (GV%switchingMode .Eq. "SWFREQUENCY") Then
   iRoffset = 16
   iCoffset = 55
   valueText    = "SWFREQUENCY"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSwarning)
   !
End If
!
If (GV%notReadyRXafterBE .And. iRec.Ne.iBolo) Then
   iRoffset = 27
   iCoffset = 55
   valueText    = "BACKEND"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSwarning)
   !
End If
!
If (GV%notReadyReceiverOffsets) Then
   iRoffset =  7
   iCoffset =  1
   valueText    = "OFFSETS"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSwarning)
Else
   iRoffset =  7
   iCoffset =  1
   valueText    = "OFFSETS"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
End If
!
If (iRec.Eq.iBolo) Then
   iRoffset = 27
   iCoffset = 55
   valueText    = "BACKEND"
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSinactive)
   iRoffset = iRoffset+1
   l = lenc(vars(iValue,iBolo)%bolometerName)
   valueText = vars(iValue,iBolo)%bolometerName(1:12)
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DSinactive)
   l = lenc(vars(iValue,iBolo)%bolometerName)
   valueText = vars(iValue,iBolo)%bolometerName(1:12)
   larg = lenc(valueText)
   iCoffset = iCoffset+40
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   !
End If
!
Close (iUnit, IOSTAT=ioerr )
!
