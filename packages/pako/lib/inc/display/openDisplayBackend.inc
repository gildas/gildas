!
!  Id: openDisplayBackend.inc,v  1.0.7 2008-08-14 Hans Ungerechts
!
!
iUnit = ioUnit%DisplayBE
ioerr= 0
!
Open (UNIT=iUnit, FILE='pakoDisplayBackend.txt',STATUS='UNKNOWN',ACCESS='APPEND',&
     &            IOSTAT=ioerr )                                                 !
!
If (ioerr .Ne. 0) Then
   Write (messageText,*) "ERROR ", ioerr,                                        &
        &                " in accessing pakoDisplayBackend ", iunit              !
   Call PakoMessage(priorityW,severityW,                                         &
        &           command,messageText)                                         !
   Write (messageText,*) "if problem persists, ",                                &
        &                "try to restart (pako and) pakoDisplayBackend"          !
   Call PakoMessage(priorityW,severityW,                                         &
        &           command,messageText)                                         !
End If
!
Call displayTextSetOutputUnit(iUnit, errorCall)
!
