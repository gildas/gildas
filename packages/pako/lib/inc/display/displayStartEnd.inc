!
!     Id: displayStartEnd.inc,v 0.9 2005-10-18 Hans Ungerechts
!
variableNameLong = "xStart yStart       "
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%pStart%x/GV%anglefactorR,               &
     &                      vars(iValue)%pStart%y/GV%anglefactorR)
!
variableNameLong = "xEnd   yEnd         "
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%pEnd%x/GV%anglefactorR,                 &
     &                       vars(iValue)%pEnd%y/GV%anglefactorR)
!

