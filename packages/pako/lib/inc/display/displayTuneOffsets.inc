  !
  !  Id: displayTuneOffsets.inc,v 1.2.3 2014-08-27 Hans Ungerechts
  !
  !D   Write (6,*) "   --> displayTuneOffsets.inc,v 1.2.3 2014-08-27"
  !
  variableName = "/tune          "
  iRoffset = iRoffset+1
  Call displayTextL    (command,iRoffset,iCoffset+iCindent,                      &
       &     variableName,vars(iValue)%doTune)                                   !
  !
  iRoffset = iRoffset+1
  !
  If ( vars(iValue)%doTune ) Then                                                ! !! TBD 2014: maybe display only for NIKA
     !
     variableNameLong = "  xOffsetT  yOffsetT"
     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong)
     Write (valueText,'(f12.3)') vars(iValue)%offsetTune%x/GV%anglefactorR
     Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText)
     Write (valueText,'(f12.3)')  vars(iValue)%offsetTune%y/GV%anglefactorR
     Call xmlOut(iRoffset,iCoffset+iCindent+36,valueText)
     !
  Else
     !
     variableNameLong = "  xOffsetT  yOffsetT"
     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
     Write (valueText,'(f12.3)') vars(iValue)%offsetTune%x/GV%anglefactorR
     Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
     Write (valueText,'(f12.3)')  vars(iValue)%offsetTune%y/GV%anglefactorR
     Call xmlOut(iRoffset,iCoffset+iCindent+36,valueText,style=DSinactive)
     !
  End If
  !
