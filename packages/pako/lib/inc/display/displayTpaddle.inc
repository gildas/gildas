!
!  Id: displayTpaddle.inc,v 1.0.9.4 2009-02-17 Hans Ungerechts
!
variableName = "/tPaddle            "
iRoffset = iRoffset+1
!
If (isConnected) Then
   Call displayTextR    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%tPaddle)                                 !
Else
   Call xmlOut(iRoffset,iCoffset+iCindent,variableName,style=DSinactive)
   Write (valueText,'(f12.2)') vars(iValue)%tPaddle
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
End If
!

