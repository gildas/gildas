!
!     $Id$
!
!     Family:  
!
      iUnit = 22
!
      OPEN (UNIT=iUnit,                                                &
     &     FILE='pakoDisplay.txt', STATUS='UNKNOWN', ACCESS='APPEND') 
!
! *** displayClearAreaSource
!
      iCoffset     =  0
      iCindent     =  3
!
      do iRoffset =  2,7,1
         call xmlOut(iRoffset,iCoffset,                                &
     &       "                                                       ") 
      end do
!
      do iRoffset =  2,4,1
         call xmlOut(iRoffset,iCoffset+55,                             &
     &       "                                                       ") 
      end do
!
      do iRoffset =  5,7,1
         call xmlOut(iRoffset,iCoffset+55,                             &
     &       "          ") 
      end do
!
!!    END IF
!
      iCoffset     =  0
      iCindent     =  3
      iRoffset     =  2
!
!
      write (6,*)   "   --> displayOffsets.f90 "
      write (6,*)   "       iCoffset :  ", iCoffset
      write (6,*)   "       iRoffset :  ", iRoffset
!
      iCoffset     =  0
      iCindent     =  3
      iRoffset     =  3
!
      variableName = "SOURCE              "
      valueText    = "                    "
!
      call displayTextC    (command,iRoffset,iCoffset,                 &
     &     variableName,valueText)
!
      iCoffset     =  0
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displaySourceName.f90'
!
      iCoffset     = 22
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displaySystemEpoch.f90'
!
      iCoffset     = 34
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displayLongitude.f90'
!
      iCoffset     = 50
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displayLatitude.f90'
!
      iCoffset     = 66
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displayVelocity.f90'
!
      iCoffset     = 82
      iCindent     =  3
      iRoffset     =  2
!
      INCLUDE 'displayFluxIndex.f90'
!
      CLOSE (iUnit)
!

