  !
  !     Id: displayFocus.inc ,v 1.2.6 2017-03-15 Hans Ungerechts
  !     Id: displayFocus.inc ,v 1.2.3 2014-10-22 Hans Ungerechts
  !     Id: displayFocus.inc ,v 1.2.3 2014-03-13 Hans Ungerechts
  !     based on 
  !     Id: displayFocus.inc ,v 1.1 2005/03/23
  !
  !D   Write (6,*) "  -> displayFocus.inc ,v 1.2.6 2017-03-15 "
  !D   Write (6,*) "  vars%focusCorrectionZ:      ", vars%focusCorrection
  !
  displayLine = "SET Focus"
  l = len_trim(displayLine)
  Call xmlOut (iRoffset, iCoffset, displayLine(1:l), style=DScommand)
  !
  Write (displayLine,'(X,F7.2,A)') GVfCorr%focusCorrection,                      &
       &                ' [mm]'                                                  !
  l = lenc(displayLine)
  Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l))
  !
  !! 2017-03-14:
  !! If (Z not in standard range) Then
  If ( .Not. isStandardfocusCorrectionZ ) Then
     Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l),                  &
          &              style=DSwarning)                                        ! 
  End If
  !
!!$  If (isSetfocusCorrectionX .And. GVfCorrX%focusCorrection.Ne.0.0) Then
  If (isSetfocusCorrectionX) Then
     Write (displayLine,'(X,F7.2,A)') GVfCorrX%focusCorrection,                  &
          &                ' /dir X'                                             !
     l = lenc(displayLine)
     Call xmlOut(iRoffset, iCoffset+iCindent+15, displayLine(1:l),               &
          &                style=DSwarning)                                      !
  End If
  !
!!$  If (isSetfocusCorrectionY .And. GVfCorrY%focusCorrection.Ne.0.0) Then
  If (isSetfocusCorrectionY) Then
     Write (displayLine,'(X,F7.2,A)') GVfCorrY%focusCorrection,                  &
          &                ' /dir Y'                                             !
     l = lenc(displayLine)
     Call xmlOut(iRoffset, iCoffset+iCindent+30, displayLine(1:l),               &
          &                style=DSwarning)                                      !
  End If
  !
  iRoffset = iRoffset+1
  !
