!
!     $Id$
!
!!$      Write (6,*) "  vars(iValue)%velocity%name:  ",                  &
!!$     &               vars(iValue)%velocity%name
!!$      Write (6,*) "  vars(iValue)%velocity%type:  ",                  &
!!$     &               vars(iValue)%velocity%type                       
!!$      Write (6,*) "  vars(iValue)%velocity%unit:  ",                  &
!!$     &               vars(iValue)%velocity%unit                       
!!$      Write (6,*) "  vars(iValue)%velocity%value: ",                  &
!!$     &               vars(iValue)%velocity%value
!!$!
      variableName = "velocity         "
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset,variableName(1:larg))
!
      Write (valueText,'(f12.3)') vars(iValue)%velocity%value
      larg = lenc(valueText)
      Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
!
      larg = lenc(vars(iValue)%velocity%type)
      valueText(1:larg)=vars(iValue)%velocity%type(1:larg)
      Call xmlOut(iRoffset+1,iCoffset,valueText(1:4))
!


