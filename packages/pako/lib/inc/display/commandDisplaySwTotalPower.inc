!
! Id: commandDisplaySwTotalPower.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!     Family:   commandDisplaySw*.inc
!     Siblings: commandDisplaySwBeam.inc
!     Siblings: commandDisplaySwFrequency.inc
!     Siblings: commandDisplaySwTotalPower.inc
!     Siblings: commandDisplaySwWobbler.inc
!
Include 'inc/display/startDisplaySw.inc'
!
!D       write (6,*)   "   --> displaySwTotalPower.inc "
!D       write (6,*)   "       iCoffset :  ", iCoffset
!D       write (6,*)   "       iRoffset :  ", iRoffset
!
variableName = "SWTOTAL"
Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
valueTextLong  = "(Total Power)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
iRoffset = iRoffset+1
!
!
iRoffset = iRoffset+1
!
Include 'inc/display/displayTphase.inc'
!
iRoffset = 16
iCoffset =  1
valueText    = GV%observingModePako
Call SIC_UPPER(valueText)
l= Len_trim(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
Close (iUnit, IOSTAT=ioerr )
!
