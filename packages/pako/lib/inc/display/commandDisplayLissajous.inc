  !
  ! Id: commandDisplayLissajous.inc,v 1.2.3  2014-08-27 Hans Ungerechts
  ! Id: commandDisplayLissajous.inc,v 1.1.12 2012-03-07 Hans Ungerechts
  !
  !  Family:   commandDisplayOM
  !  Siblings: commandDisplayCalibrate.inc
  !            commandDisplayFocus.inc
  !            commandDisplayLissajous.inc
  !            commandDisplayOnOff.inc
  !            commandDisplayOtfMap.inc
  !            commandDisplayPointing.inc
  !            commandDisplayTrack.inc
  !            commandDisplayTip.inc
  !            commandDisplayVlbi.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "LISSAJOUS"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  valueTextLong    = "(Lissajous OTF Map)"
  Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
  !
  l = Len_trim(GV%angleUnitSetC)
  valueText    = "["//GV%angleUnitSetC(1:l)//"]"
  Call xmlOut (iRoffset, iCoffset+44, valueText)
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayAmplitudeXY.inc'
  Include 'inc/display/displayCenterXY.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayFrequencyXY.inc'
  Include 'inc/display/displayPhasesXY.inc'
  !
  Include 'inc/display/displayTotfSimple.inc'
  Include 'inc/display/displaySystem.inc'
  !
  iRoffset = iRoffset+1
  !
  If (vars(iValue)%doReference) Then
     Include 'inc/display/displayReference.inc'
     Include 'inc/display/displayTreference.inc'
!!$     Include 'inc/display/displayCroLoop.inc'
  End If
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayTuneOffsets.inc'
  Include 'inc/display/displayTuneTime.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayOptionFocus.inc'
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayOptionPointing.inc'
  !
  If (GV%condElevation%isSet.And.GV%condElevation%maximum/deg.Lt.85.00-0.01) Then
     !
     iRoffset = iRoffset+1
     Include 'inc/display/displayConditionElevation.inc'
     !
  End If
  !
  Close (iUnit, IOSTAT=ioerr )
  !
