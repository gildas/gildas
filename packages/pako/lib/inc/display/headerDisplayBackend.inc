!
!  Id: headerDisplayBackend.inc 2006-11-27 v1.0.2 Hans Ungerechts
!
iCoffset     = 55
iCindent     =  3
!
Do iRoffset = 27,41,1
   !
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        &      "                                                          ") 
   !
End Do
!
iRoffset     = 27
!
!DWrite (6,*)   "   --> displayBackend.inc "
!DWrite (6,*)   "       iCoffset :  ", iCoffset
!DWrite (6,*)   "       iRoffset :  ", iRoffset
!
iCoffset = 55
!
valueText    = "BACKEND"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
!
iCoffset = iCoffset+5+3
!
valueText    = "nPart"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+5+1
!
valueText    = "resolu."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+7+2
!
valueText    = "bandw."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+2
!
valueText    = "fShift"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+1
!
valueText    = "/receiver"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+9+1
!
valueText    = "/mod"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+4+1
!
valueText    = "/perc"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
