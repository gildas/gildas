!
!  Id: displayBackendParameters.inc,v 1.2.0 2012-06-13 Hans Ungerechts
!  Id: displayBackendParameters.inc,v 1.1.6 2011-06-28 Hans Ungerechts
!
l = lenc(vars1%name)
valueText = vars1%name(1:9)
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+1+9
!
Write (valueText,'(i2)') vars1%nPart
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+0+2
!
Write (valueText,'(f9.3)') vars1%resolution
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+1+9
!
Write (valueText,'(f7.1)') vars1%bandWidth
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+1+7
!
Write (valueText,'(f7.1)') vars1%fShift
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+1+7
!
l = lenc(vars1%receiverName)
valueText = vars1%receiverName(1:4)
If ( vars1%receiverName.Eq.rec%HERA1                                             &
     & .Or. vars1%receiverName.Eq.rec%HERA2 ) Then                               !
   valueText = vars1%receiverName(1:8)
End If
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+1+larg
!
!  ** EMIR
If (        vars1%receiverName .Eq. rec%E090                                     &
     & .Or. vars1%receiverName .Eq. rec%E150                                     &
     & .Or. vars1%receiverName .Eq. rec%E230                                     &
     & .Or. vars1%receiverName .Eq. rec%E300 ) Then                              !
   !
   l = lenc(vars1%polarization)
   valueText = vars1%polarization(1:3)
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+1+3
   !
   l = lenc(vars1%subband)
   valueText = vars1%subband(1:3)
   larg = lenc(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+1+3
   !
   l = lenc(vars1%mode)
   valueText = vars1%mode(1:4)
   larg = lenc(valueText)
   If (vars1%mode .Eq. GPnone .Or. vars1%name.Eq.bac%FTS) Then
      valueText = "    "
      larg = 4
   End If
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+1+4
   !
   If (vars1%name.Eq.bac%VESPA .Or. vars1%name.Eq.bac%WILMA) Then
      Write (valueText,'(f5.1)') vars1%percentage
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   End If
   iCoffset = iCoffset+1+5
   !
   If (vars1%lineNameIsSet) Then
      l = lenc(vars1%linename)
      valueText = vars1%linename(1:12)
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   End If
   iCoffset = iCoffset+1+12
   !
Else  !!  --> not EMIR
   !
   If ( vars1%receiverName.Eq.rec%HERA1                                          &
        & .Or. vars1%receiverName.Eq.rec%HERA2 ) Then                            !
      iCoffset = iCoffset+1
   Else
      l = lenc(vars1%receiverName2)
      valueText = vars1%receiverName2(1:4)
      larg = lenc(valueText)
      If (vars1%receiverName2 .Eq. GPnone) Then
         valueText = "    "
         larg = 4
      End If
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+4
   End If
   !
   iCoffset = iCoffset+3
   l = lenc(vars1%mode)
   valueText = vars1%mode(1:4)
   larg = lenc(valueText)
   If (vars1%mode .Eq. GPnone) Then
      valueText = "    "
      larg = 4
   End If
   Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   iCoffset = iCoffset+1+4
   !
   If (vars1%name.Eq.bac%VESPA .Or. vars1%name.Eq.bac%WILMA) Then
      Write (valueText,'(f5.1)') vars1%percentage
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
   End If
   iCoffset = iCoffset+1+5
   !
End If
!



