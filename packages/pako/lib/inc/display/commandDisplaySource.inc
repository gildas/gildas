!
!  Id: commandDisplaySource.inc, 1.0.7 2007-08-16 Hans Ungerechts
!
!  Family:  
!
Include 'inc/display/openDisplay.inc'
!
! *** displayClearAreaSource
!
iCoffset     =  0
iCindent     =  3
!
Do iRoffset =  2,6,1
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        &       "                                                       ")       !
End Do
!
Do iRoffset =  2,3,1
   Call xmlOutBlank(iRoffset,iCoffset+55,                                        &
        &   "                                                                 ") !
End Do
!
Do iRoffset =  5,6,1
   Call xmlOutBlank(iRoffset,iCoffset+55,                                        &
        &       "       ")                                                       !
End Do
!!
!!
If (vars(iValue)%isBody) Then
   !
   iCoffset     =  1
   iCindent     =  3
   iRoffset     =  3
   !
   variableNameLong = "SOURCE BODY"
   Call xmlOut (iRoffset, iCoffset, variableNameLong, style=DScommand)
   !
   iCoffset     = 15 
   iCindent     =  0
   iRoffset     =  2
   Include 'inc/display/displaySourceName.inc'
   !
   iCoffset     = 25
   Include 'inc/display/displayPerihelionEpoch.inc'
   !
   iCoffset     = 41
   Include 'inc/display/displayAscendingNode.inc'
   !
   iCoffset     = 56
   Include 'inc/display/displayArgumentOfPerihelion.inc'
   !
   iCoffset     = 73
   Include 'inc/display/displayInclination.inc'
   !
   iCoffset     = 85
   Include 'inc/display/displayPerihelionDistance.inc'
   !
   iCoffset     = 101
   Include 'inc/display/displayEccentricity.inc'
   !
End If
!!
!!
If (      (     vars(iValue)%isPlanet             &
     &     .Or. vars(iValue)%isSatellite) ) Then
   !
   iCoffset     =  1
   iCindent     =  3
   iRoffset     =  3
   !
   variableName = "SOURCE              "
   Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
   !
   iCoffset     = 10
   iCindent     =  0
   iRoffset     =  2
   !
   Include 'inc/display/displaySourceName.inc'
   !
   If (vars(iValue)%isPlanet)       variableNameLong = "[Planet]"
   If (vars(iValue)%isSatellite)    variableNameLong = "[Satellite]"
   larg = lenc(variableNameLong)
   iCoffset     = 26
   iRoffset     =  3
   Call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg), style=DShighlight)
   !
   iCoffset     = 66
   iRoffset     =  2
   Include 'inc/display/displayVelocity.inc'
   !
   iCoffset     = 26
   iRoffset     =  3
   If (vars(iValue)%sourceName .Eq. sats%Gabriel) Then   
      larg = Len_trim(sats%Gabriel)
      cInputLong = "[paKo recognizes "//sats%Gabriel(1:larg)//                   &
           &  ", but doesn't know how to track him.]"
      larg = Len_trim(cInputLong)
      Call xmlOut(iRoffset,iCoffset,cInputLong(1:larg), style=DSerror)
   End If
   !
End If
!!
!!
If (.Not. (     vars(iValue)%isBody                                              & 
     &     .Or. vars(iValue)%isPlanet                                            &
     &     .Or. vars(iValue)%isSatellite) ) Then                                 !
   !
   iCoffset     =  1
   iCindent     =  3
   iRoffset     =  3
   !
   variableName = "SOURCE"
   Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
   !
   Include 'inc/display/displaySourceCatalog.inc'
   !
   iCoffset     = 10
   iCindent     =  0
   iRoffset     =  2
   !
   Include 'inc/display/displaySourceName.inc'
   !
   iCoffset     = 23
   Include 'inc/display/displaySystemEpoch.inc'
   !
   iCoffset     = 34
   Include 'inc/display/displayLongitude.inc'
   !
   iCoffset     = 50
   Include 'inc/display/displayLatitude.inc'
   !
   iCoffset     = 66
   Include 'inc/display/displayVelocity.inc'
   !
   iCoffset     = 82
   !TBD:   Include 'inc/display/displayFluxIndex.inc'
   !
   iCoffset     = 98
   Include 'inc/display/displaySourceProjectID.inc'
   !
End If
!
If (.True.) Then
   iCoffset     =  1
   iCindent     =  0
   iRoffset     =  7
   valueText = "OFFSETS"
   l= Len_trim(valueText)
   Do ii = nDimOffsetChoices, 1, -1
      If (varsOffsets(iValue,ii)%isSet) Then
         !
         Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
         !
      End If
   End Do
   !
End If
!
Close (iUnit, IOSTAT=ioerr )
!

