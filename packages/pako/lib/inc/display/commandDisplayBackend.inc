!
!  Id: commandDisplayBackend.inc,v 1.3.0 2017-08-28 Hans Ungerechts
!
!   Family:   commandDisplayBackend.inc
!   Siblings: commandDdisplayBackend2.inc
!
Include 'inc/display/openDisplay.inc'
!
iCoffset     = 55
iCindent     =  3
!
Do iRoffset = 27,41,1
   !
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        & "                                                                   "  &
        & // "        " )                                                        !
   !
End Do
!
iRoffset     = 27
!
valueText    = "BACKEND"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg),style=DScommand)
iCoffset = iCoffset+5+3
!
valueText    = "nPart"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+5+1
!
valueText    = "resolu."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+7+2
!
valueText    = "bandw."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+2
!
valueText    = "fShift"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+1
!
valueText    = "/receiver"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+9+4
!
valueText    = "/mod"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+4+1
!
valueText    = "/perc"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+5+1
!
valueText    = "/lineName"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+9+1
!
Do jj = 1,nDimBackends,1
   Do ii = 1,nDimParts,1
      If (listBe(jj,ii)%isConnected) Then
         iCoffset = 55
         iRoffset = iRoffset+1
         vars1    = listBE(jj,ii)
         If (iRoffset .Le. 40) Then
            Include 'inc/display/displayBackendParameters.inc'
         Else
            valueText = " + more ... "
            larg = lenc(valueText)
            Call xmlOut(41,iCoffset,valueText(1:larg))
         End If
      End If
   End Do   !!   ii = 1,nDimParts,1
   !
   If ( vars(iValue,jj)%samplesIsSet ) Then
      !
      variableName = "/nSamples"
      iRoffset = iRoffset+1
      iCoffset = 55
      !
      displayLine = "/nSamples"
      l = len_trim(displayLine)
      Call xmlOut (iRoffset, iCoffset+iCindent, displayLine(1:l),                &
           &       style=DSwarning)                                              !
      !
      Write (displayLine,'(X,I8,X)') vars(iValue,jj)%nSamples
      l = lenc(displayLine)
      Call xmlOut(iRoffset, iCoffset+iCindent+9, displayLine(1:l),               &
           &        style=DSwarning)                                             !
      !
   End If
   !
End Do   !!   jj = 1,nDimBackends,1
!
Close (iUnit, IOSTAT=ioerr )
!
