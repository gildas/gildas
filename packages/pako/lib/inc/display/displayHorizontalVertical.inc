!
! Id: displayHorizontalVertical.inc,v 1.1.0 2008-12-10 Hans Ungerechts
!
If (        vars(iValue,ii)%name .Eq. rec%E090                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E150                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E230                                   &
     & .Or. vars(iValue,ii)%name .Eq. rec%E300 ) Then                            !
   !
   iCoffset = 56
   !
   variableName = "/Horizontal"
   larg = Len_trim(variableName)
   Call xmlOut(iRoffset,iCoffset,variableName(1:larg))
   iCoffset = iCoffset+larg+1
   !
   valueTextLong = vars(iValue,ii)%horizontalSubbands
   Call sic_upper(valuetextLong)
   larg = Len_trim(valueTextLong)
   !
   If (vars(iValue,ii)%horizontalSubbands.Eq.GPnone) Then
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg),style=DSinactive)
   Else
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   End If
   !
   iCoffset = 92
   !
   variableName = "/Vertical"
   larg = Len_trim(variableName)
   Call xmlOut(iRoffset,iCoffset,variableName(1:larg))
   iCoffset = iCoffset+larg+1
   !
   valueTextLong = vars(iValue,ii)%verticalSubbands
   Call sic_upper(valuetextLong)
   larg = Len_trim(valueTextLong)
   !
   If (vars(iValue,ii)%verticalSubbands.Eq.GPnone) Then
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg),style=DSinactive)
   Else
      Call xmlOut(iRoffset,iCoffset,valueTextLong(1:larg))
   End If
   !
End If
