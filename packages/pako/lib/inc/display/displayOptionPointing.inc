  !
  !  Id: displayOptionPointing.inc,v 1.2.3    2014-07-03 Hans Ungerechts
  !
  If (vars(iValue)%doPointing) Then
     !
     variableName = "/pointing                "
     iRoffset = iRoffset+1
     Call displayTextL    (command,iRoffset,iCoffset+iCindent,                   &
          &     variableName,vars(iValue)%doPointing)                            !
     !
  Else
     !
     variableName = "/pointing                "
     iRoffset = iRoffset+1
     Call xmlOut(iRoffset,iCoffset+iCindent,variableName,style=DSinactive)
     !
  End If
