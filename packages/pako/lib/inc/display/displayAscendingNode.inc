!
!     $Id$
!
      variableNameLong = "Asc.Node[deg]"
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f11.6)') vars(iValue)%ascendingNode
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset+2,valueTextLong(1:larg))
!


