!
! Id: commandDisplaySwBeam.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!     Family:   commandDisplaySw*.inc
!     Siblings: commandDisplaySwBeam.inc
!     Siblings: commandDisplaySwFrequency.inc
!     Siblings: commandDisplaySwTotalPower.inc
!     Siblings: commandDisplaySwWobbler.inc
!
Include 'inc/display/startDisplaySw.inc'
!
!D      write (6,*)   "   --> displaySwBeam.inc "
!D      write (6,*)   "       iCoffset :  ", iCoffset
!D      write (6,*)   "       iRoffset :  ", iRoffset
!
variableName   = "SWBEAM               "
Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
valueTextLong  = "(Beam Switching)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
iRoffset = iRoffset+1
!
iRoffset = 16
iCoffset =  1
valueText    = GV%observingModePako
Call SIC_UPPER(valueText)
l= Len_trim(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
Close (iUnit, IOSTAT=ioerr )
!
