!
! Id: displayCroLoop.inc,v 1.0.4 2006-07-19 Hans Ungerechts
!
If (GV%switchingMode.Eq.swMode%Freq .Or. GV%switchingMode.Eq.swMode%Wobb) Then
   !
   iRoffset = iRoffset+1
   variableNameLong = "/croLoop "
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   !
Else
   !
   variableName = "/croLoop             "
   valueText    = "  see below"
   !
   iRoffset = iRoffset+1
   !
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,valueText)
   !
   iRoffsetLong = iRoffsetLong+1
   !
   Call xmlOutBlank(iRoffsetLong,0,                                              &
        &        "                                                    ")
   Call xmlOutBlank(iRoffsetLong,52,                                             &
        &        "                                                    ")
   !
   Call displayTextClong(command,iRoffsetLong,iCoffset+iCindent,                 &
        &     variableName,vars(iValue)%croCode)
   !
End If
