  !
  !  Id: displayTuneTime, v1.2.3 2014-08-27 Hans Ungerechts
  !
  !D   Write (6,*) "   --> displayTuneTime, v1.2.3 2014-08-27"
  !
  If ( vars(iValue)%doTune ) Then                                                ! !! TBD 2014: maybe display only for NIKA
     !
     iRoffset = iRoffset+1
     variableNameLong = "/tTune "
     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong)
     Write (valueText,'(f12.3)') vars(iValue)%tTune
     Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText)
     !
  Else
     !
     iRoffset = iRoffset+1
     variableNameLong = "/tTune "
     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
     Write (valueText,'(f12.3)') vars(iValue)%tTune
     Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
     !
  End If
  !
!!$     Write (valueText,'(l12)') vars(iValue)%doTune
!!$     Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
!!$     !
!!$     iRoffset = iRoffset+1
!!$     variableNameLong = "  tTune"
!!$     Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
