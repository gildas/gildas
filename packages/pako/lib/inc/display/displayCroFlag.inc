!
! Id: displayCroFlag.inc,v 1.0.10.4 2008-10-08 Hans Ungerechts
!
variableName = "/croFlag"
iRoffset = iRoffset+1
Call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
     &     variableName,vars(iValue)%croFlag)                                    !
!
