!
!  Id: displaySlew.inc,v 1.0.9.4 2009-02-17 Hans Ungerechts
!
!!$If (vars(iValue)%doSlew) Then
!
variableName = "/slew                "
iRoffset = iRoffset+1
!
If (isConnected) Then
   Call displayTextL    (command,iRoffset,iCoffset+iCindent,                  &
        &     variableName,vars(iValue)%doSlew)                               !
Else
   Call xmlOut(iRoffset,iCoffset+iCindent,variableName,style=DSinactive)
   Write (valueText,'(l12)') vars(iValue)%doSlew
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
End If
!
!!$End If
!
