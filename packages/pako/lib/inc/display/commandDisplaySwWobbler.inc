!
! Id: commandDisplaySwWobbler.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!     Family:   commandDisplaySw*.inc
!     Siblings: commandDisplaySwBeam.inc
!     Siblings: commandDisplaySwFrequency.inc
!     Siblings: commandDisplaySwTotalPower.inc
!     Siblings: commandDisplaySwWobbler.inc
!
Include 'inc/display/startDisplaySw.inc'
!
!D      write (6,*)   "   --> displaySwWobbler.f90 "
!D      write (6,*)   "       iCoffset :  ", iCoffset
!D      write (6,*)   "       iRoffset :  ", iRoffset
!
variableName   = "SWWOBBLER"
Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
valueTextLong  = "(Wobbler Switching)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
iRoffset = iRoffset+1
!
Include 'inc/display/displayWoffsets.inc'
!
iRoffset = iRoffset+1
!
!! Include 'inc/display/displayWangle.inc'
Include 'inc/display/displayTphase.inc'
Include 'inc/display/displaySecondaryRotation.inc'
!
! ** following are warning markers for OMs depending on SW Wobbler:
!
iRoffset = 16
iCoffset =  1
valueText    = GV%observingModePako
Call SIC_UPPER(valueText)
l= Len_trim(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
If (GV%observingMode .Eq. OM%Focus) Then
   iRoffset = 16
   iCoffset =  1
   valueText    = "FOCUS"
   l= Len_trim(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
   !
End If
!
If (GV%observingMode .Eq. OM%OnOff) Then
   iRoffset = 16
   iCoffset =  1
   valueText    = "ONOFF"
   l= Len_trim(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
   !
End If
!
If (GV%observingMode .Eq. OM%Pointing) Then
   iRoffset = 16
   iCoffset =  1
   valueText    = "POINTING"
   l= Len_trim(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
   !
End If
!
Close (iUnit, IOSTAT=ioerr )
!
