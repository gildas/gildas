!
!  Id: displayGainImage.inc,v 0.96 2005-12-05 Hans Ungerechts
!
variableName = "/gainImage"
iRoffset = iRoffset+1
!
!
Call displayTextL    (command,iRoffset,iCoffset+iCindent,                        &
     &     variableName,vars(iValue)%doGainImage)
!
If (vars(iValue)%doGainImage) Then
   !
   l = len_trim(vars(iValue)%receiverName)
   valueText = vars(iValue)%receiverName(1:l)
   !
   Call xmlOut (iRoffset, iCoffset+44, valueText)
   !
End If
!
