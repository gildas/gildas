!
!  Id: displaySymmetric.inc,v 1.0.2 2006-01-05 Hans Ungerechts
!
variableName = "/symmetric"
iRoffset = iRoffset+1
call displayTextL    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,vars(iValue)%doSymmetric)
!
