!
!     Id: displaySecondaryRotation.inc,v 1.0.1 2005-12-28 Hans Ungerechts
!
iCoffset     = 55
iCindent     =  3
iRoffset     = 23
!
!D Write (6,*) "   displaySecondaryRotation "
!D Write (6,*) "   GV%switchingMode         ", GV%switchingMode
!D Write (6,*) "   swMode%Wobb              ", swMode%Wobb
!D Write (6,*) "   GV%secondaryRotation     ", GV%secondaryRotation
!
!! If (GV%switchingMode.Eq.swMode%Wobb .And. Abs(GV%secondaryRotation) .Ge.0.01) Then
   !
   displayLine = "SET 2ndRotation"
   l = len_trim(displayLine)
   Call xmlOut (iRoffset, iCoffset, displayLine(1:l), style=DScommand)
   !
   Write (displayLine,'(X,F7.2,X,A)')  GV%secondaryRotation, "[degree]"
   l = lenc(displayLine)
   Call xmlOut(iRoffset, iCoffset+28, displayLine(1:l))
   !
!! End If

