  !
  !    Id: displayPhaseXY.inc,v 1.1.12 2012-02-29 Hans Ungerechts
  !
  variableNameLong = "/phases   [rad]  x  y"
  iRoffset = iRoffset+1
  Call displayText2R   (command,iRoffset,iCoffset+iCindent,                      &
       &    variableNameLong,vars(iValue)%phase%x,                               &
       &                     vars(iValue)%phase%y  )                             !
  !


