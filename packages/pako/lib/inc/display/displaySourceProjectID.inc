!
!     Id: displaySourceProjectID, v1.0.1 2005-12-29 Hans Ungerechts
!
If (vars(iValue)%projectID .Ne. GPnone) Then
   !
   variableName = "project"
   larg = lenc(variableName)
   Call xmlOut(iRoffset,iCoffset,variableName(1:larg))
   !
   valueText=vars(iValue)%projectID
   larg = lenc(valueText)
   Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
   !
End If
