!
! Id: displayLissajous.inc,v 1.0.10.4 2008-10-08 Hans Ungerechts
!
variableNameLong = "xAmplitude yAmplitude"
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%xAmplitude/GV%anglefactorR,             &
     &                      vars(iValue)%yAmplitude/GV%anglefactorR)             !
!
variableNameLong = "frequencyX frequencyY"
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%frequencyX,                             &
     &                      vars(iValue)%frequencyY)                             !
!
variableNameLong = "xCenter    yCenter   "
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%pCenter%x/GV%anglefactorR,              &
     &                      vars(iValue)%pCenter%y/GV%anglefactorR)              !
!
variableNameLong = "phiX       phiY      "
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%phiX,                                   &
     &                      vars(iValue)%phiY)                                   !
!

