!
!     $Id$
!
      variableName = "/receiver            "
!
      iRoffset = iRoffset+1
      l = lenc(vars(iValue)%receiverName)
      call displayTextC    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,vars(iValue)%receiverName(1:l),                &
     &     "idReceiver", "displayReceiver", "displayNormal", "char")
!
!D      write (6,*) " --> displayReceiver "
!D      write (6,*) "     l:     ",l
!D      write (6,*) "   vars(iValue)%receiverName   ->",                 &
!D     &       vars(iValue)%receiverName(1:l),"<-"
!D!
