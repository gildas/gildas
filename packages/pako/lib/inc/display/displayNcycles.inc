!
! Id: displayNcycles.inc,v 1.0.6.3 2007-02-20 Hans Ungerechts
!
If (vars(iValue)%nCycles.Gt.1) Then
   !
   variableName = "/nCycles             "
   !
   iRoffset = iRoffset+1
   Call displayTextI    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%nCycles,                                 &
        &     "idNcycles", "displayNcycles", "displayNormal", "int" )            !
   !
End If
