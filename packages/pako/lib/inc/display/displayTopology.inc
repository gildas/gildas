!
!  Id: displayTopology.inc,v 0.96 2005-12-13 Hans Ungerechts
!
displayLine = 'SET Topology '
l = lenc(displayLine)
Call xmlOut (iRoffset, iCoffset, displayLine(1:l), style=DScommand)
!
!D      write (6,*) "   displayLine:  ->",displayLine(1:l),"<-"
displayLine = GV%topology
Call SIC_LOWER(displayLine)
l = lenc(displayLine)
Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l))
iRoffset = iRoffset+1
!
