  !
  !  Id: commandDisplaySubscan.inc,v 1.2.3 2013-10-29 Hans Ungerechts
  !
  !  Family:  commandDisplaySubscan.f90
  !
  Include 'inc/display/commandDisplayDIYlist.inc'
  !
  If (GV%doDebugMessages) Then
     Call pako_message(seve%t,"pako",                                            &
          &    " module DIYlist  --> commandDisplaySubscan.inc,v 1.2.3 ")        ! !! trace execution
  End If
  !
  iRoffset = iRoffset+2
  !
  variableName = "SUBSCAN"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  valueTextLong = "(subscan definition)"
  Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
  !
  If (vars(iValue)%segType.Eq.seg%lissajous) Then
     !
     valueTextLong = "(OTF Lissajous)"
     Call xmlOut (iRoffset, iCoffset+36, valueTextLong)
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayLissajous.inc'
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayCroFlag.inc'
     Include 'inc/display/displayRamp.inc'
     Include 'inc/display/displayTotf.inc'
     Include 'inc/display/displaySystem.inc'
     !
     !!   iRoffset = iRoffset+1
     !!   Include 'inc/display/displayConditionElevation.inc'
     !
  End If
  !
  If (vars(iValue)%segType.Eq.seg%linear) Then
     !
     valueTextLong = "(OTF linear)"
     Call xmlOut (iRoffset, iCoffset+36, valueTextLong)
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayStartEnd.inc'
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayCroFlag.inc'
     Include 'inc/display/displayRamp.inc'
     Include 'inc/display/displayTotf.inc'
     Include 'inc/display/displaySystem.inc'
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayNext.inc'
     !
  End If
  !
  If (vars(iValue)%segType.Eq.seg%Track) Then
     !
     valueTextLong  = "(Track)"
     Call xmlOut (iRoffset, iCoffset+36, valueTextLong)
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayXoffsetYoffset.inc'
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayCroFlag.inc'
     Include 'inc/display/displayTsubscan.inc'
     Include 'inc/display/displaySystem.inc'
     !
     iRoffset = iRoffset+1
     !
     Include 'inc/display/displayTune.inc'
     !
  End If
  !
  Close (iUnit, IOSTAT=ioerr )
  !
