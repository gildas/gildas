!
!     $Id$
!
!     Family:  displaySegment.f90
!
      Include 'headerDisplayOMrowP9.f90'
!
      variableName = "SEGMENT              "
      valueText    = "segment definition   "
!
      Call displayTextC    (command,iRoffset,iCoffset,                   &
     &     variableName,valueText)
!
      iRoffset = iRoffset+1
!
      Include 'displaySegType.f90'
      If (     vars(iValue)%segType.Eq.seg%track                         &
     &    .Or. vars(iValue)%segType.Eq.seg%linear                        &
     &    .Or. vars(iValue)%segType.Eq.seg%circle                        &
     &    .Or. vars(iValue)%segType.Eq.seg%curve) Then
         Include 'displayStartEnd.f90'
      End If
      If (vars(iValue)%segType.Eq.seg%circle) Then
         Include 'displayTurnAngle.f90'
      End If
      If (vars(iValue)%segType.Eq.seg%curve) Then
         Include 'displayCPStartEnd.f90'
      End If
      If (     vars(iValue)%segType.Eq.seg%linear                        &
     &    .Or. vars(iValue)%segType.Eq.seg%circle                        &
     &    .Or. vars(iValue)%segType.Eq.seg%curve) Then
         Include 'displayLengthOtf.f90'
      End If
!
      iRoffset = iRoffset+1
!
      Include 'displayFlagRef.f90'
      Include 'displaySpeed.f90'
      Include 'displayTsegment.f90'
      Include 'displayAltOption.f90'
      Include 'displayWriteToSeg.f90'
!
      Include 'displayCountSegments.f90'
!
      Close (iUnit)
!!
      Close (iUnit)
!

