!
!     $Id$
!
!     Family:  displayBackend.inc
!
Include 'inc/display/startDisplayBackend.inc'
!
!!$Write (6,*)   "   --> displayBackend.inc "
!!$Write (6,*)   "       iCoffset :  ", iCoffset
!!$Write (6,*)   "       iRoffset :  ", iRoffset
!
iCoffset = 55
!
valueText    = "BACKEND"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
!
iCoffset = iCoffset+5+3
!
valueText    = "nPart"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+5+1
!
valueText    = "resolu."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+7+2
!
valueText    = "bandw."
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+2
!
valueText    = "fShift"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+6+1
!
valueText    = "/receiver"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+9+1
!
valueText    = "/mod"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
iCoffset = iCoffset+4+1
!
valueText    = "/perc"
larg = lenc(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
!
Do jj = 1,nDimBackends,1
   Do ii = 1,nDimParts,1
      If (listBe(jj,ii)%isConnected) Then
         iCoffset = 55
         iRoffset = iRoffset+1
         vars1    = listBE(jj,ii)
         If (iRoffset .LE. 40) Then
            Include 'inc/display/displayBackendParameters.inc'
         Else
            valueText = " + more ... "
            larg = lenc(valueText)
            Call xmlOut(41,iCoffset,valueText(1:larg))
         End If
      End If
   End Do
End Do
!
Close (iUnit)
!








