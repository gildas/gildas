!
!     ID: displaySky.inc,v 1.0.2 2006-01-03 Hans Ungerechts
!
variableName = "/sky"
iRoffset = iRoffset+1
Call displayTextL    (command,iRoffset,iCoffset+iCindent,                        &
     &     variableName,vars(iValue)%doSky)
!
variableNameLong = "   xOffsetC yOffsetC"
iRoffset = iRoffset+1
Call displayText2R   (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,                                                     &
     &     vars(iValue)%xOffsetRC/GV%anglefactorR,                               &
     &     vars(iValue)%yOffsetRC/GV%anglefactorR)
!
!!$variableNameLong = "   --> altSky"
!!$iRoffset = iRoffset+1
!!$Call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
!!$     &     variableNameLong, vars(iValue)%altSky)
!




