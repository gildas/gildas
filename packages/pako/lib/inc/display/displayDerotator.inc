!
! Id: displayDerotator.inc,v 0.2 2005-07-18 Hans Ungerechts
!
If (        vars(iValue,ii)%name .Eq. rec%HERA               &
     & .Or. vars(iValue,ii)%name .Eq. rec%HERA1              &
     & .Or. vars(iValue,ii)%name .Eq. rec%HERA2 ) Then
   !
   variableName = "/derotator"
   larg = lenc(variableName)
   Call xmlOut(14,10,variableName(1:larg))
   !
   Write (valueText,'(f8.2)') vars(iValue,ii)%derotatorAngle
   larg = lenc(valueText)
   Call xmlOut(14,25,valueText(1:larg))
   !
   valueText = vars(iValue,ii)%derotatorSystem
   Call sic_upper(valuetext)
   larg = lenc(valueText)
   Call xmlOut(14,35,valueText(1:larg))
   !
   valueText = '[deg]'
   larg = lenc(valueText)
   Call xmlOut(14,50,valueText(1:larg))
   !
End If
