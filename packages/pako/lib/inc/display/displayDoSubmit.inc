!
!     Id: displayDoSubmit.inc,v 0.96 2005-12-13 Hans Ungerechts
!
iRoffset = iRoffset-1
!
displayLine = "SET doSubmit"
l = Len_trim(displayLine)
Call xmlOut (iRoffset, iCoffset+2*iCindent, displayLine(1:l), style=DScommand)
!
If (GV%doSubmit) Then
   !
   Call xmlOut(iRoffset,iCoffset+3*iCindent,"YES (T)")
   !
Else
   !
   Call xmlOut(iRoffset,iCoffset+3*iCindent,"NO  (F)",style=DSinactive)
   !
End If
!
iRoffset = iRoffset+1
!
