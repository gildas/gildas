!
!     Id: displayLengthOtf.inc,v 0.9 2005-10-18 Hans Ungerechts
!
variableNameLong = "    --> lengthOtf   "
iRoffset = iRoffset+1
call displayTextR    (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%lengthOtf/GV%anglefactorR)
!
