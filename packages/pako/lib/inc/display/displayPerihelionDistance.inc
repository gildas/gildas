!
!     $Id$
!
      variableNameLong = "Perih.Dist.[AU]"
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f12.6)') vars(iValue)%PerihelionDistance
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset+3,valueTextLong(1:larg))
!


