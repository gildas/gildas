!
!  Id: displayReference.inc,v 1.0.4 2006-07-20 Hans Ungerechts
!
!D Write (6,*) "   --> displayReference.inc,v 1.0.4 2006-07-19"
!
If (        (GV%observingMode.Eq.OM%OnOff .And.GV%switchingMode.Eq.swMode%Freq)  &
     & .Or. (GV%observingMode.Eq.OM%OtfMap.And.GV%switchingMode.Eq.swMode%Freq)  &
     & .Or. (GV%observingMode.Eq.OM%OtfMap.And.GV%switchingMode.Eq.swMode%Wobb)  &
     &  ) Then
   !
   iRoffset = iRoffset+1
   variableNameLong = "/reference "
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   Write (valueText,'(l12)') vars(iValue)%doReference
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
   !
   iRoffset = iRoffset+1
   variableNameLong = "  xOffsetR  yOffsetR"
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   Write (valueText,'(f12.3)') vars(iValue)%offsetR%x/GV%anglefactorR
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
   Write (valueText,'(f12.3)') vars(iValue)%offsetR%y/GV%anglefactorR
   Call xmlOut(iRoffset,iCoffset+iCindent+36,valueText,style=DSinactive)
   !
   iRoffset = iRoffset+1
   variableNameLong = "  systemNameRef     "
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   valueTextLong = vars(iValue)%systemNameRef
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
   !
Else
   !
   variableName = "/reference          "
   iRoffset = iRoffset+1
   Call displayTextL    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%doReference)
   !
   variableNameLong = "  xOffsetR  yOffsetR"
   iRoffset = iRoffset+1
   Call displayText2R   (command,iRoffset,iCoffset+iCindent,                     &
        &     variableNameLong,vars(iValue)%offsetR%x/GV%anglefactorR,           &
        &                      vars(iValue)%offsetR%y/GV%anglefactorR)
   !
   variableName = "  systemNameRef     "
   iRoffset = iRoffset+1
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%systemNameRef)
   !
End If
!
!!$variableNameLong = "  listOfNamesRef    "
!!$valueText        = "  see below       "
!!$iRoffset = iRoffset+1
!!$Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
!!$     &     variableNameLong,valueText)
!!$iRoffsetLong = iRoffsetLong+1
!!$Call displayTextClong(command,iRoffsetLong,iCoffset+iCindent,                 &
!!$     &     variableName,vars(iValue)%listOfNamesRef)
!!$!
!!$variableName = "  --> altRef        "
!!$iRoffset = iRoffset+1
!!$Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
!!$     &     variableName,vars(iValue)%altRef)
!!$!

