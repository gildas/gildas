!
!     $Id$
!
      variableName = "/write              "
      iRoffset = iRoffset+1
      call displayTextL    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,varsEtc(iValue)%doWriteToSeg)
!
      If (varsEtc(iValue)%doneWriteToSeg) Then
         variableName = " --> doneWrite      "
         iRoffset = iRoffset+1
         call displayTextL    (command,iRoffset,iCoffset+iCindent,     &
     &     variableName,varsEtc(iValue)%doneWriteToSeg)
      End If
!

