!
!     $Id$
!
      variableName = "/variable"
      iRoffset = iRoffset+1
      call displayTextL    (command,iRoffset,iCoffset+iCindent,          &
     &     variableName,vars(iValue)%doVariable)
!
      variableName = "   tempVariable"
      iRoffset = iRoffset+1
      call displayTextR    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,vars(iValue)%tempVariable)
!
