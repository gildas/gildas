!
! Id: commandDisplaySwFrequency.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!     Family:   commandDisplaySw*.inc
!     Siblings: commandDisplaySwBeam.inc
!     Siblings: commandDisplaySwFrequency.inc
!     Siblings: commandDisplaySwTotalPower.inc
!     Siblings: commandDisplaySwWobbler.inc
!
Include 'inc/display/startDisplaySw.inc'
!
!D      write (6,*)   "   --> displaySwFrequency.inc "
!D      write (6,*)   "       iCoffset :  ", iCoffset
!D      write (6,*)   "       iRoffset :  ", iRoffset
!
variableName   = "SWFREQUENCY"
Call xmlOut (iRoffset, iCoffset, variableName, style=DScommand)
valueTextLong  = "(Frequency Switching)"
Call xmlOut (iRoffset, iCoffset+12, valueTextLong)
!
iRoffset = iRoffset+1
iCoffset = 55
Include 'inc/display/displayFoffsets.inc'
!
iRoffset = iRoffset+1
iCoffset = 55
!!$  Include 'inc/display/displayFunction.inc'
!!$  Include 'inc/display/displayNphases.inc'
!!$  Include 'inc/display/displayTblanking.inc'
Include 'inc/display/displayTphase.inc'
Include 'inc/display/displayNcycles.inc'
!
iRoffset = 16
iCoffset =  1
valueText    = GV%observingModePako
Call SIC_UPPER(valueText)
l= Len_trim(valueText)
Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
!
Close (iUnit, IOSTAT=ioerr )
!
