  !
  !     Id: displayBolometerParameters.inc,v 1.2.3 2014-08-29 Hans Ungerechts
  !
  l = lenc(vars(iValue,ii)%name)
  valueText = vars(iValue,ii)%name(1:l)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+14
  !
  l = lenc(vars(iValue,ii)%bolometerName)
  valueText = vars(iValue,ii)%bolometerName(1:12)
  larg = lenc(valueText)
  Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
  iCoffset = iCoffset+1+12
  !
!!$      Write (valueText,'(f7.3)') vars(iValue,ii)%gainBolometer
!!$      larg = lenc(valueText)
!!$      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
!!$      iCoffset = iCoffset+1+7
  !
  If ( vars(iValue,ii)%bolometerName.Eq.bolo%nika .And. NIKApixel%isSet) Then
     !
     Write (valueText,'(i5)') vars(iValue,ii)%channel
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+6
     !
     Write (valueText,'(F10.3)') NIKApixel%point%x
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+10
     !
     Write (valueText,'(F10.3)') NIKApixel%point%y
     larg = lenc(valueText)
     Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
     iCoffset = iCoffset+2+10
     !
  End If
  !


