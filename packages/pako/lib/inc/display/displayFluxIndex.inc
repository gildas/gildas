!
!     $Id$
!
      variableName = "/flux     index   "
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset+8,variableName(1:larg))
!
      variableName = "FLUX              "
      larg = lenc(variableName)
      call xmlOut(iRoffset+1,iCoffset,variableName(1:larg))
!
      Write (valueText,'(f8.3)') vars(iValue)%flux
      larg = lenc(valueText)
      Call xmlOut(iRoffset+1,iCoffset+5,valueText(1:larg))
!
      Write (valueText,'(f8.3)') vars(iValue)%index
      larg = lenc(valueText)
      Call xmlOut(iRoffset+1,iCoffset+15,valueText(1:larg))
!



