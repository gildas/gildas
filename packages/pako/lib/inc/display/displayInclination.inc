!
!     $Id$
!
      variableNameLong = "Incl.[deg] "
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f11.6)') vars(iValue)%inclination
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset-1,valueTextLong(1:larg))
!


