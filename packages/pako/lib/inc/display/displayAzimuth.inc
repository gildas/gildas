!
!     Id: displayAzimuth.inc,v 
!
      variableNameLong = "azimuth [deg]       "
      iRoffset = iRoffset+1
!
      call xmlOutBlank(iRoffset,0,                                               &
     &        "                                                    ") 
!
      if (vars(iValue)%doCurrentAzimuth) then
         call displayTextC    (command,iRoffset,iCoffset+iCindent,               &
     &     variableNameLong,'current azimuth')
      else
         call displayTextR    (command,iRoffset,iCoffset+iCindent,               &
     &     variableNameLong,vars(iValue)%azimuth)
      end if
!
