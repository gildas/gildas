!
!     $Id$
!
      variableName = "longitude latitude"
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset+iCindent,variableName(1:larg))
!
!!$      call xmlOut(iRoffset,iCoffset+2*iCindent+larg+4,                 &
!!$     &                      vars(iValue)%longitude%text      )
!!$!
      Write (nameText, '(a)'    ) "[hours]" 
      Write (valueText,'(f14.6)') vars(iValue)%longitude%hour
!
      Call xmlOut(iRoffset+0,iCoffset+2*iCindent,nameText(1:larg))
      Call xmlOut(iRoffset+0,iCoffset+2*iCindent,valueText)
!
      Write (nameText, '(a)'    ) "[degrees]" 
      Write (valueText,'(f14.6)') vars(iValue)%longitude%deg
!
      Call xmlOut(iRoffset+1,iCoffset+2*iCindent,nameText(1:larg))
      Call xmlOut(iRoffset+1,iCoffset+2*iCindent,valueText)
!
      Write (nameText, '(a)'    ) "[radian]" 
      Write (valueText,'(f14.8)') vars(iValue)%longitude%rad
!
      Call xmlOut(iRoffset+2,iCoffset+2*iCindent,nameText(1:larg))
      Call xmlOut(iRoffset+2,iCoffset+2*iCindent,valueText)
!
!!$      iCoffset = iCoffset+16
!!$!
!!$      variableName = "latitude"
!!$!
!!$      call xmlOut(iRoffset,iCoffset+iCindent,variableName(1:larg))
!!$      call xmlOut(iRoffset,iCoffset+2*iCindent+larg+4,                 &
!!$     &                      vars(iValue)%latitude%text      )
!!$!
!!$      Write (nameText, '(a)'    ) "[degrees]" 
!!$      Call xmlOut(iRoffset+2,iCoffset+2*iCindent,nameText(1:larg))
!!$!
!!$      Write (valueText,'(f14.6)') vars(iValue)%latitude%deg
!!$      Call xmlOut(iRoffset+2,iCoffset+2*iCindent+larg,valueText)
!!$!
!!$      Write (nameText, '(a)'    ) "[radian]" 
!!$      Call xmlOut(iRoffset+3,iCoffset+2*iCindent,nameText(1:larg))
!!$!
!!$      Write (valueText,'(f14.8)') vars(iValue)%latitude%rad
!!$      Call xmlOut(iRoffset+3,iCoffset+2*iCindent+larg,valueText)
!!$!


