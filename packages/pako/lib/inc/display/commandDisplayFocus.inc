  !
  !  Id: commandDisplayFocus.inc,v 1.2.3 2014-09-24 Hans Ungerechts
  !  Id: commandDisplayFocus.inc,v 1.1.0 2009-03-22 Hans Ungerechts
  !
  !  Family:   commandDisplayOM
  !  Siblings: commandDisplayCalibrate.inc
  !            commandDisplayFocus.inc
  !            commandDisplayOnOff.inc
  !            commandDisplayOtfMap.inc
  !            commandDisplayPointing.inc
  !            commandDisplayTrack.inc
  !            commandDisplayTip.inc
  !            commandDisplayVlbi.inc
  !
  Include 'inc/display/headerDisplayOM.inc'
  !
  variableName = "FOCUS"
  Call xmlOut (iRoffset, iCoffset+1, variableName, style=DScommand)
  !
  iRoffset = iRoffset+1
  !
  Include 'inc/display/displayLengthFocus.inc'
  !
  iRoffset = iRoffset+1
  !
  !!TBD: Include 'inc/display/displayCalibrate.inc'
  !!TBD: displayDirectionFocus only for privileged users
  !!TBD: Include 'inc/display/displayFeBe.inc'
  Include 'inc/display/displayNsubscans.inc'
  Include 'inc/display/displayTsubscan.inc'
  Include 'inc/display/displayDirectionFocus.inc'
  !!TBD: Include 'inc/display/displayOtfFocus.inc'
  !!TBD: Include 'inc/display/displayUpdate.inc'
  !
  iRoffset = iRoffset+1
  Include 'inc/display/displayTuneOffsets.inc'
  Include 'inc/display/displayTuneTime.inc'
  !
  Close (iUnit, IOSTAT=ioerr )
  !
