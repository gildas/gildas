!
!    Id: displayAmplitudeXY.inc,v 1.1.12 2012-02-29 Hans Ungerechts
!
  variableNameLong = "xAmplitude yAmplitude"
  iRoffset = iRoffset+1
  Call displayText2R   (command,iRoffset,iCoffset+iCindent,                      &
       &    variableNameLong,vars(iValue)%amplitude%x/GV%anglefactorR,           &
       &                     vars(iValue)%amplitude%y/GV%anglefactorR)           !
!
