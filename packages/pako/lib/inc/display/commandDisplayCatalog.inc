  !
  !  Id: commandDisplayCatalog.inc,v 1.2.4 2015-06-18 Hans Ungerechts
  !  Id: commandDisplayCatalog.inc,v 1.0.7 2007-08-14 Hans Ungerechts
  !
  !  Family:  displayCatalog.inc
  !
  !D   Write (6,*) "      --> commandDisplayCatalog.inc,v 1.2.4 2015-06-18 "
  !D   Write (6,*) "          GVsourceCatalog ->",                                    &
  !D        &                 GVsourceCatalog(1)(1:Len_trim(GVsourceCatalog(1))),     &
  !D        &                 "<-"                                                    !
  !D   Write (6,*) "          GVlineCatalog ->",                                      &
  !D        &                 GVlineCatalog(1)(1:Len_trim(GVlineCatalog(1))),         &
  !D        &                 "<-"                                                    !
  !
  Include 'inc/display/openDisplay.inc'
  !
  ! *** 'displayClearAreaCatalogs'
  !
  Call xmlOutBlank(8,0,                                                          &
       & "                                                                    ") !
  Call xmlOutBlank(8,66,                                                         &
       & "                                                                    ") !
  !
  iRoffset = 8
  !
  iCoffset = 0
  valueTextLong = "CATALOG SOURCE"
  Call xmlOut (iRoffset, iCoffset+1, valueTextLong, style=DScommand)
  l = Len_trim(GVsourceCatalog(1))
  !D   Write (6,*) "CATALOG SOURCE"//" length l : ", l
  If (l.Le.40) Then
     Call xmlOut (iRoffset, iCoffset+16,  GVsourceCatalog(1)(1:l))
  Else
     Call xmlOut (iRoffset, iCoffset+16,  "... "//GVsourceCatalog(1)(l-40:l))
  End If
  !
  iCoffset = 66
  valueTextLong = "CATALOG LINE"
  Call xmlOut (iRoffset, iCoffset, valueTextLong, style=DScommand)
  l = Len_trim(GVlineCatalog(1))
  !D   Write (6,*) "CATALOG LINE"//" length l : ", l
  If (l.Le.40) Then
     Call xmlOut (iRoffset, iCoffset+16,  GVlineCatalog(1)(1:l))
  Else
     Call xmlOut (iRoffset, iCoffset+16,  "... "//GVlineCatalog(1)(l-40:l))
  End If
  !
  Close (iUnit, IOSTAT=ioerr )
  !
