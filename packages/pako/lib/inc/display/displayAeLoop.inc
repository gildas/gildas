!
!  Id: displayAeLoop.inc,v 1.0.6.1 2007-01-13  Hans Ungerechts
!
variableName      = "/aeLoop              "
valueTextLong    = "  --> see below      "
!
!D Write (6,*) "      vars(iValue)%aeCode: ", vars(iValue)%aeCode
!D Write (6,*) "      GPnone:              ", GPnone
!
If (vars(iValue)%aeCode .Ne. GPnone) Then
   !
   iRoffset = iRoffset+1
   !
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,valueTextLong)                                        !
   !
   iRoffsetLong = iRoffsetLong+1
   !
   !D      write (6,*) ' iRoffsetLong ', iRoffsetLong
   !
   Call xmlOutBlank(iRoffsetLong,0,                                              &
        &        "                                                    ")         !
   Call xmlOutBlank(iRoffsetLong,52,                                             &
        &        "                                                    ")         !
   !
   Call displayTextClong(command,iRoffsetLong,iCoffset+iCindent,                 &
        &     variableName,vars(iValue)%aeCode)                                  !
   !
End If
!

