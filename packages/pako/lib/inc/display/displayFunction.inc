!
!     $Id$
!
      variableName = "/function            "
!
      iRoffset = iRoffset+1
      l = lenc(vars(iValue)%functionName)
      call displayTextC    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,vars(iValue)%functionName(1:l),                &
     &     "idFunction", "displayFunction", "displayNormal", "char" )
!
