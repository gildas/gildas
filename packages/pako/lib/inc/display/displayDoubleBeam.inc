!
!  Id: displayDoubleBeam.inc,v 1.0.6.1 2007-01-13 Hans Ungerechts
!
If (vars(iValue)%doDoubleBeam) Then
!
   variableName = "/doubleBeam                "
   iRoffset = iRoffset+1
   Call displayTextL    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%doDoubleBeam)                            !
   !
End If
