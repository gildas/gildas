  !
  !     Id: displayCommandLine.inc,v 1.2.3 2014-05-19 Hans Ungerechts
  !
  !!    TBD 2014: investigate and fix problem wiht OPEN:
  !!    PAKO> subs 0 0 /tsub 1
  !!    T-PAKO,   pako-load.f90, ---> SR: pako_run:
  !!    T-PAKO,   SR pako-run, line: PAKO\SUBSCAN 0 0 /TSUBSCAN 1
  !!    --> displayCommandLine.inc 
  !!    ioUnit%Display            23
  !!    ioerr                      0
  !!    iUnit                     23
  !!    W-PAKO,  ERROR 104 in accessing pakoDisplay 23
  !!    W-PAKO,  if problem persists, try to restart (pako and) pakoDisplay
  !!    ioerr                    104
  !!    <-- displayCommandLine.inc 
  !!    T-PAKO,   module DIYlist, 1.2.3 2013-06-25 ---> SR: Subscan
  !
  !     Id: displayCommandLine.inc,v 1.2.3 2014-02-03 Hans Ungerechts
  !
  !     based on
  ! *   Id: displayCommandLine.inc,v 1.0.7 2007-08-14
  !
  !D   Write (6,*) "   --> displayCommandLine.inc "
  !D   Write (6,*) "       ioUnit%Display  ", ioUnit%Display
  !D   Write (6,*) "       ioerr           ", ioerr
  !
!!$  iUnit = ioUnit%Display                                                      !! possible conflict with module organization ? 
  iUnit = 23
  !D   Write (6,*) "       iUnit           ", iUnit
  !
  Open (UNIT=iUnit,FILE='pakoDisplay.txt',                                       &
       &      STATUS='UNKNOWN', ACCESS='APPEND', IOSTAT=ioerr )                  !
  !
  !D   If (ioerr .Ne. 0) Then
  !D      Write (messageText,*) "ERROR ", ioerr,                                      &
  !D           &                " in accessing pakoDisplay ", iunit                   !
  !D      Call PakoMessage(priorityW,severityW,                                       &
  !D           &                programName,messageText)                              !
  !D      Write (messageText,*) "if problem persists, ",                              &
  !D           &                "try to restart (pako and) pakoDisplay "              !
  !D      Call PakoMessage(priorityW,severityW,                                       &
  !D           &                 programName,messageText)                             !          
  !D   End If
  !
  !D   Write (6,*) "       ioerr           ", ioerr
  !
  Call displayTextSetOutputUnit(iUnit, error)
  !
  Call xmlOutBlank (47,1,line(1:100),style=DShighlight)
  !
  CLOSE (UNIT=iUnit, IOSTAT=ioerr)
  !
  !D   Write (6,*) "   <-- displayCommandLine.inc "

