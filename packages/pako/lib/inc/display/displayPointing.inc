  !
  !     Id: displayPointing.inc,v  1.2.6  2017-03-21  Hans Ungerechts
  !     Id: displayPointing.inc,v  1.2.3  2014-05-20  Hans Ungerechts
  !     Id: displayPointing.inc,v  1.1    2005-03-23  Hans Ungerechts
  !
  displayLine = "SET Pointing"
  l = len_trim(displayLine)
  Call xmlOut (iRoffset, iCoffset, displayLine(1:l), style=DScommand)
  !
  Write (displayLine,'(X,F8.1,X)') GVpCorr%azimuthCorrection
  l = lenc(displayLine)
  Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l))
  If (        .Not.isStandardAzimuthCorrection   ) Then                          
     Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l),                  &
          &        style=DSwarning)                                              !
  End If
  !
  Write (displayLine,'(X,F8.1,X)') GVpCorr%elevationCorrection
  l = lenc(displayLine)
  Call xmlOut(iRoffset, iCoffset+iCindent+10, displayLine(1:l))
  If (        .Not.isStandardElevationCorrection   ) Then                          
     Call xmlOut(iRoffset, iCoffset+iCindent+10, displayLine(1:l),               &
          &        style=DSwarning)                                              !
  End If
  !
  iRoffset = iRoffset+1
  !
!!$  Write (displayLine,'(X,F8.1,X,F8.1,X,A)') GVpCorr%azimuthCorrection,           &
!!$       &                                    GVpCorr%elevationCorrection          !
!!$  l = lenc(displayLine)
!!$  Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l))
!!$  If (        .Not.isStandardAzimuthCorrection                                   &
!!$       & .Or. .Not.isStandardElevationCorrection ) Then                          !
!!$     Call xmlOut(iRoffset, iCoffset+iCindent, displayLine(1:l),                  &
!!$          &        style=DSwarning)                                              !
!!$  End If
!!$  iRoffset = iRoffset+1
!!$  !

