!
!     $Id$
!
      variableName = "/writeToSub           "
      iRoffset = iRoffset+1
      call displayTextL    (command,iRoffset,iCoffset+iCindent,        &
     &     variableName,vars(iValue)%doWriteToSub)
!
      If (vars(iValue)%doneWriteToSub) Then
         variableName = " --> doneWrite      "
         iRoffset = iRoffset+1
         call displayTextL    (command,iRoffset,iCoffset+iCindent,     &
     &     variableName,vars(iValue)%doneWriteToSub)
      End If
!

