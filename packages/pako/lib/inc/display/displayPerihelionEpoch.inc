!
!     $Id$
!
      variableNameLong = "Perih.Epoch[d]"
      larg = lenc(variableNameLong)
      call xmlOut(iRoffset,iCoffset,variableNameLong(1:larg))
!
      Write (valueTextLong,'(f15.6)') vars(iValue)%perihelionEpoch
      larg = lenc(valueTextLong)
      Call xmlOut(iRoffset+1,iCoffset-1,valueTextLong(1:larg))
!


