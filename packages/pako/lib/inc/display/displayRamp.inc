!
! Id: displayRamp.inc,v 1.2.3 2013-06-25 Hans Ungerechts
!
variableName = "/ramp"
!
If      (vars(iValue)%doRampUp.And.vars(iValue)%doRampDown) Then
   !
   valueText = ramp%both
   !
   iRoffset = iRoffset+1
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,valueText)                                            !
   !
   variableNameLong = "   tRampUp tRampDown"
   iRoffset = iRoffset+1
   Call displayText2R   (command,iRoffset,iCoffset+iCindent,                     &
        &     variableNameLong,                                                  &
        &     vars(iValue)%tRampUp,                                              &
        &     vars(iValue)%tRampDown)                                            !
   !
   IF (vars(iValue)%doInternal) Then
      variableNameLong = "   doInternal"
      iRoffset = iRoffset+1
      Call displayTextL    (command,iRoffset,iCoffset+iCindent,                  &
           &     variableNameLong,vars(iValue)%doInternal)                       !
   End IF
   !
Else If (vars(iValue)%doRampUp) Then
   !
   valueText = ramp%Up
   !
   iRoffset = iRoffset+1
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
        &     variableName,valueText)                                               !
   !
   variableNameLong = "   tRampUp"
   iRoffset = iRoffset+1
   Call displayTextR    (command,iRoffset,iCoffset+iCindent,                        &
        &     variableNameLong,                                                     &
        &     vars(iValue)%tRampUp)                                                 !
   !
Else If (vars(iValue)%doRampDown) Then
   !
   valueText = ramp%Down
   !
   iRoffset = iRoffset+1
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
        &     variableName,valueText)                                               !
   !
   variableNameLong = "   tRampDown"
   iRoffset = iRoffset+1
   Call displayTextR    (command,iRoffset,iCoffset+iCindent,                        &
        &     variableNameLong,                                                     &
        &     vars(iValue)%tRampDown)                                                 !
   !
Else
   !
   valueText = ramp%none
   !
   iRoffset = iRoffset+1
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
        &     variableName,valueText)                                               !
   !
End If
!



