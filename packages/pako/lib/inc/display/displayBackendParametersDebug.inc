!
!     $Id$
!
      l = lenc(vars(iValue,ii)%name)
      valueText = vars(iValue,ii)%name(1:10)
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+10
!
      Write (valueText,'(i3)') vars(iValue,ii)%nPart
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+3
!
      Write (valueText,'(f10.3)') vars(iValue,ii)%resolution
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+10
!
      Write (valueText,'(f10.3)') vars(iValue,ii)%bandWidth
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+8
!
      Write (valueText,'(f10.1)') vars(iValue,ii)%fShift
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+5+8
!
      l = lenc(vars(iValue,ii)%receiverName)
      valueText = vars(iValue,ii)%receiverName(1:6)
      larg = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset,valueText(1:larg))
      iCoffset = iCoffset+1+6
!



