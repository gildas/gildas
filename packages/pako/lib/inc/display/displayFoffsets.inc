!
!     displayFoffsets.inc,v 1.0.3 2006-03-29 Hans Ungerechts
!
iRoffset = iRoffset+1
!
valueTextLong  = " fOffset1 "
l = lenc(valueTextLong)
Call xmlOut (iRoffset, iCoffset+iCindent, valueTextLong(1:l))
!
valueTextLong  = " fOffset2  [MHz]"
l = lenc(valueTextLong)
Call xmlOut (iRoffset, iCoffset+iCindent+10, valueTextLong(1:l))
!
Do ii = 1, nDimReceiverChoices, 1
   Call queryReceiver(receiverChoices(ii), Result)
   If (Result) Then
      !
      iRoffset = iRoffset+1
      !
      Write (valueText,'(f9.3)') vars(iValue)%fOffsets(ii,1)
      l = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset+iCindent,valueText(1:l))
      !
      Write (valueText,'(f9.3)') vars(iValue)%fOffsets(ii,2)
      l = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset+iCindent+10,valueText(1:l))
      !
      valueText = "/receiver"
      l = lenc(valueText)
      Call xmlOut(iRoffset,iCoffset+iCindent+20,valueText(1:l))
      !
      !! valueText = receiverChoices(ii)
      l = len_trim(receiverChoices(ii))
      Call xmlOut(iRoffset,iCoffset+iCindent+30,receiverChoices(ii)(1:l))
      !
   End If
End Do


