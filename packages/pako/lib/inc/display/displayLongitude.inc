!
!     $Id$
!
      variableName = "longitude         "
      larg = lenc(variableName)
      call xmlOut(iRoffset,iCoffset,variableName(1:larg))
!
      valueText=vars(iValue)%longitude%text
      larg = lenc(valueText)
      Call xmlOut(iRoffset+1,iCoffset,valueText(1:larg))
!
      variableName = "  [h]             "
      larg = lenc(variableName)
      call xmlOut(iRoffset+2,iCoffset-7,variableName(1:larg))
!
      variableName = "[deg]             "
      larg = lenc(variableName)
      call xmlOut(iRoffset+3,iCoffset-7,variableName(1:larg))
!
      variableName = "[rad]             "
      larg = lenc(variableName)
      call xmlOut(iRoffset+4,iCoffset-7,variableName(1:larg))
!
      Write (valueText,'(f12.6)') vars(iValue)%longitude%hour
      larg = lenc(valueText)
      Call xmlOut(iRoffset+2,iCoffset,valueText(1:larg))
!
      Write (valueText,'(f12.6)') vars(iValue)%longitude%deg
      larg = lenc(valueText)
      Call xmlOut(iRoffset+3,iCoffset,valueText(1:larg))
!
      Write (valueText,'(f12.8)') vars(iValue)%longitude%rad
      larg = lenc(valueText)
      Call xmlOut(iRoffset+4,iCoffset,valueText(1:larg))
!
