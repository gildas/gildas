!
!  Id: displayDirectionFocus.inc,v 1.1.0 2009-03-22 Hans Ungerechts
!
!D Write (6,*) "   --> displayDirectionFocus "
!D Write (6,*) "       vars(iValue)%directionFocus(1:1) ->",                        &
!D      &              vars(iValue)%directionFocus(1:1), "<-"                       !
!
If (vars(iValue)%directionFocus(1:1).Ne."Z") Then
   variableName = "/direction           "
   iRoffset = iRoffset+2
   Call displayTextC    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,vars(iValue)%directionFocus)                          !
End If
!
