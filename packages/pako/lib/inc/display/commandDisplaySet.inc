!
!  Id: commandDisplaySet.inc,v 1.2.3 2014-03-13 Hans Ungerechts
!  Id: commandDisplaySet.inc,v 1.0.7 2007-08-14 Hans Ungerechts
!
!  Family:  
!
Include 'inc/display/openDisplay.inc'
!
! *** displayClearAreaSet
!
iCoffset     = 64
iCindent     =  3
!
Do iRoffset =  4,6,1
   Call xmlOutBlank(iRoffset,iCoffset,                                           &
        &  "                                                                  ") !
End Do
!
Include 'inc/display/displayVersion.inc'
!
iCoffset     = 66
iCindent     = 14
iRoffset     =  4
!
!!      Include 'inc/display/displayProject.inc'
!!      Include 'inc/display/displayObserver.inc'
!!      Include 'inc/display/displayTopology.inc'
!!      Include 'inc/display/displayAngleUnit.inc'
Include 'inc/display/displayTopology.inc'
Include 'inc/display/displayDoSubmit.inc'
Include 'inc/display/displayPointing.inc'
Include 'inc/display/displayFocus.inc'
!
Include 'inc/display/displaySecondaryRotation.inc'
!
If (GV%notReadySecondaryRafterOM ) Then
   iRoffset = 16
   iCoffset =  1
   valueText    = GV%observingModePako
   Call SIC_UPPER(valueText)
   l= Len_trim(valueText)
   Call xmlOut(iRoffset,iCoffset,valueText(1:l),style=DSwarning)
End If
!
Close (iUnit, IOSTAT=ioerr )
!


