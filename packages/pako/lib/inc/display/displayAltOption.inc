!
!     Id: displayAltOption.inc,v 0.9 2005-10-18 Hans Ungerechts
!
variableNameLong = "   --> altOption     "
iRoffset = iRoffset+1
call displayTextC    (command,iRoffset,iCoffset+iCindent,                        &
     &     variableNameLong,vars(iValue)%altOption)
!
