!
!  Id: displayClear.inc,v 1.0.10.4 2008-10-08
!
If (doClear) Then
   variableName = "/clear"
   iRoffset = iRoffset+1
   Call displayTextL    (command,iRoffset,iCoffset+iCindent,                     &
        &     variableName,doClear)                                              !
   !
End If
