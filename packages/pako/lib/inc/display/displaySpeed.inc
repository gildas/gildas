!
!  Id: displaySpeed.inc,v 1.0.2 2006-01-05 Hans Ungerechts
!
If (vars(iValue)%altOption.Eq."SPEED") Then
   iRoffset = iRoffset+1
   variableNameLong = "/speed sStart sEnd   "
   Call displayText2R   (command,iRoffset,iCoffset+iCindent,                     &
        &     variableNameLong,vars(iValue)%speedStart/GV%anglefactorR,          &
        &                      vars(iValue)%speedEnd/GV%anglefactorR)
Else If (vars(iValue)%altOption.Eq."TOTF") Then
   iRoffset = iRoffset+1
   variableNameLong = "/speed sStart sEnd   "
   Call xmlOut(iRoffset,iCoffset+iCindent,variableNameLong,style=DSinactive)
   Write (valueText,'(f12.3)') vars(iValue)%speedStart/GV%anglefactorR
   Call xmlOut(iRoffset,iCoffset+iCindent+22,valueText,style=DSinactive)
   Write (valueText,'(f12.3)') vars(iValue)%speedEnd/GV%anglefactorR
   Call xmlOut(iRoffset,iCoffset+iCindent+36,valueText,style=DSinactive)
End If
!
