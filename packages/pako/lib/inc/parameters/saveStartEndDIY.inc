!
!  Id: saveStartEndDIY.inc,v 1.0.10.5 2008-10-09 Hans Ungerechts
!
Write (lineOut,*)                                                                &
     &     segList(ii)%pStart,                                                   &
     &     segList(ii)%pEnd                                                      !
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
