!
!     $Id$
!
      l = lenc(vars(iValue,ii)%lineName)
      lineOut = ' "'//vars(iValue,ii)%lineName(1:l)//'"'
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
