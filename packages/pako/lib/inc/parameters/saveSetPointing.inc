!
! Id: saveSetPointing.inc,v 1.0.9 2007-10-01 Hans Ungerechts
!
Write (lineOut,'(A,2F12.2)') " Pointing ", GVpCorr%azimuthCorrection, GVpCorr%elevationCorrection
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
