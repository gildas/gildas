!
!  Id: checkConsistentBackend.inc,v 1.2.3  2013-09-20,  Hans Ungerechts
!  Id: checkConsistentBackend.inc,v 1.2.3  2013-08-27,  Hans Ungerechts
!                                          upgrade of E150 to H&V 2SB LO LI UI UO
!
!  Id: checkConsistentBackend.inc,v 1.1.11 2011-10-17 Hans Ungerechts
!
If (GV%doDebugMessages) Then
   Write (6,*) '   --> checkConsistentBackend.inc,v 1.2.3  2013-08-27'
End If
!
If (vars(iIn,iBack)%isConnected                                                  &
     &   .And. vars(iIn,iBack)%receiverName.Eq.rec%Bolo) Then                    !
   Write (messageText,*)                                                         & 
        &            ' does not work with Receiver: ',                           &
        &             vars(iIn,iBack)%receiverName                               !
   Call pakoMessage(priorityE,severityE,command,messageText)            
   errorInconsistent = .True.
End If
!
If (vars(iIn,iBack)%isConnected                                                  &
     &   .And. vars(iIn,iBack)%receiverName.Ne.GPnone) Then                      !
   !
   !  ** EMIR
   !
   If (     vars(iIn,iBack)%receiverName .Eq. rec%E090                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E150                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E230                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E300 ) Then                    !
      !
      !D       Write (6,*) "  vars(iIn,iBack)%receiverName: ", vars(iIn,iBack)%receiverName
      !D       Write (6,*) "  vars(iIn,iBack)%polarization: ", vars(iIn,iBack)%polarization
      !D       Write (6,*) "  vars(iIn,iBack)%subband:      ", vars(iIn,iBack)%subband
      !
      If (iBack.eq.iBBC) Then                  !!     BBC
         !
         Call queryReceiver(vars(iIn,iBack)%receiverName,                        &
              &                 IsConnected  = IsConnectedRX)                    !
         If (.Not. IsConnectedRX) Then
            messageText  =                                                       & 
                 &            ' Receiver: '//vars(iIn,iBack)%receiverName//      &
                 &                     ' is not connected '                      !
            Call pakoMessage(priorityE,severityE,command,messageText)            
         End If
         !
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$         If (        vars(iIn,iBack)%receiverName .Eq. rec%E150 ) Then           !
!!$            !
!!$            Call queryReceiver(vars(iIn,iBack)%receiverName,                     &
!!$                 &                 IsConnected  = IsConnectedRX,                 &
!!$                 &                 polarization = pol%hor,                       &
!!$                 &                 subband      = vars(iIn,iBack)%subband     )  !
!!$            !
!!$            Call queryReceiver(vars(iIn,iBack)%receiverName,                     &
!!$                 &                 IsConnected  = IsConnectedRX2,                &
!!$                 &                 polarization = pol%ver,                       &
!!$                 &                 subband      = vars(iIn,iBack)%subband     )  !
!!$            !
!!$            If (.Not. IsConnectedRX .And. .Not. IsConnectedRX2) Then
!!$               messageText  =                                                    & 
!!$                    &            ' Receiver: '//vars(iIn,iBack)%receiverName//   &
!!$                    &                     '  '//vars(iIn,iBack)%subband//        &
!!$                    &                     ' is not connected '                   !
!!$               Call pakoMessage(priorityE,severityE,command,messageText)            
!!$            End If
!!$            !
!!$         End If
         !
      Else If (iBack.eq.iFTS .And. vars(iIn,iBack)%nPart.Le.4) Then              !  !!  FTS on Coax 1 to 4
         !
         !D          Write (6,*) "  Coax 1 to 4 "
         !D          Write (6,*) "  vars(iIn,iBack): "
         !D          Write (6,*)    vars(iIn,iBack)
         !
         Call queryReceiver(vars(iIn,iBack)%receiverName,                        &
              &                 IsConnected  = IsConnectedRX,                    &
              &                 polarization = vars(iIn,iBack)%polarization,     &
              &                 subband      = vars(iIn,iBack)%subband        )  !
         !
         If (.Not. IsConnectedRX) Then
            messageText  =                                                       & 
                 &            ' Receiver: '//vars(iIn,iBack)%receiverName//      &
                 &                     ' /'//vars(iIn,iBack)%polarization//      &
                 &                     '  '//vars(iIn,iBack)%subband//           &
                 &                     ' is not connected '                      !
            Call pakoMessage(priorityE,severityE,command,messageText)            
         End If
         !
!!$         Call queryReceiver(vars(iIn,iBack)%receiverName,                        &
!!$              &                 IsConnected  = IsConnectedRX)                    !
!!$         If (.Not. IsConnectedRX) Then
!!$            messageText  =                                                       & 
!!$                 &            ' Receiver: '//vars(iIn,iBack)%receiverName//      &
!!$                 &                     ' is not connected '                      !
!!$            Call pakoMessage(priorityE,severityE,command,messageText)            
!!$         End If
         !
      Else If (iBack.eq.iFTS .And. vars(iIn,iBack)%nPart.Ge.5                    &
           &                 .And. vars(iIn,iBack)%nPart.Le.8) Then              !  !!  FTS on Coax 5 to 5
         !                                                                       !  !!  Q/TBD: make independant of sequence?
         !
         !D          Write (6,*) "  Coax 5 to 8 "
         !D          Write (6,*) "  vars(iIn,iBack): "
         !D          Write (6,*)    vars(iIn,iBack)
         !
         ii = vars(iIn,iBack)%nPart
         !
         IsConnectedRX =                       esbCoax(ii)%isRequested           &
              &   .And.  vars(iIn,iBack)%receiverName                            &
              &                           .Eq. esbCoax(ii)%bandName              &
              &   .And.  vars(iIn,iBack)%polarization                            &
              &                           .Eq. esbCoax(ii)%polarization          &
              &   .And.  vars(iIn,iBack)%subband                                 &
              &                           .Eq. esbCoax(ii)%subband               !
         !
         If (.Not. IsConnectedRX) Then
            Write (messageText,*)                                                & 
                 &            ' Receiver: ',                                     &
                 &             vars(iIn,iBack)%receiverName,                     &
                 &        "/"//vars(iIn,iBack)%polarization,                     &
                 &             vars(iIn,iBack)%subband,                          &
                 &            ' is not connected on Coax ',                      &
                 &              ii                                               !
            Call pakoMessage(priorityE,severityE,command,messageText)      
         End If
         !
      Else                                     !! not BBC nor FTS
         !
         Call queryReceiver(vars(iIn,iBack)%receiverName,                        &
              &                 IsConnected  = IsConnectedRX,                    &
              &                 polarization = vars(iIn,iBack)%polarization,     &
              &                 subband      = vars(iIn,iBack)%subband        )  !
         !
         If (.Not. IsConnectedRX) Then
            messageText  =                                                       & 
                 &            ' Receiver: '//vars(iIn,iBack)%receiverName//      &
                 &                     ' /'//vars(iIn,iBack)%polarization//      &
                 &                     '  '//vars(iIn,iBack)%subband//           &
                 &                     ' is not connected '                      !
            Call pakoMessage(priorityE,severityE,command,messageText)            
         End If
         !
      End If                                   !! end if BBC or not
      !
   Else                                        !!  Not EMIR
      !
      Call queryReceiver(vars(iIn,iBack)%receiverName,                           &
           &                               IsConnectedRX )                       !
      If (.Not. IsConnectedRX) Then
         Write (messageText,*)                                                   & 
              &            ' Receiver: ',                                        &
              &             vars(iIn,iBack)%receiverName,                        &
              &            ' is not connected '                                  !
         Call pakoMessage(priorityE,severityE,command,messageText)            
      End If
      !
   End If
   !
   errorInconsistent = errorInconsistent .Or. (.Not. IsConnectedRX)
   !
End If
!
If (vars(iIn,iBack)%isConnected                                                  &
     &   .And. vars(iIn,iBack)%receiverName2.Ne.GPnone) Then                     !
   Call queryReceiver(vars(iIn,iBack)%receiverName2,                             &
        &                               IsConnectedRX)                           !
   If (.Not. IsConnectedRX) Then
      Write (messageText,*)                                                      & 
           &            ' Receiver: ',                                           &
           &             vars(iIn,iBack)%receiverName2,                          &
           &            ' is not connected '                                     !
      Call pakoMessage(priorityE,severityE,command,messageText)            
   End If
   errorInconsistent = errorInconsistent .Or. (.Not. IsConnectedRX)
End If
!
!D Write (6,*) "      receiverIsEMIR: ", receiverIsEMIR
!
Do jj = 1,nDimBackends,1                                                         !  !!  disconnect orphaned Backend Parts
   Do ii = 1,nDimParts,1                                                         !  !!  (not supported by sel. in RECEIVER)
      !             
      !D       Write (6,*) "  jj, ii : ", jj, ii
      !
      If (receiverIsEMIR) Then
      !
         If (listBe(jj,ii)%isConnected .And.                                     &
              & listBe(jj,ii)%receiverName.Ne.GPnone) Then                       !
            !
            If (jj.eq.iBBC) Then                     !!     BBC
               !
               Call queryReceiver(listBe(jj,ii)%receiverName,                    &
                    &                 IsConnected  = IsConnectedRX)              !
               If (.Not. IsConnectedRX) Then
                  messageText  =                                                 & 
                       &            ' Receiver: '//listBe(jj,ii)%receiverName//  &
                       &                     ' is not connected '                !
                  Call pakoMessage(priorityW,severityW,command,messageText)            
                  Write (messageText,*)                                          & 
                       &            ' Backend: ',                                &
                       &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,    &
                       &         ' previous configuration will be disconnected ' !
                  Call pakoMessage(priorityW,severityW,command,messageText)            
                  listBe(jj,ii)%isConnected = .False.
               End If
               !
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$               If (        listBe(jj,ii)%receiverName .Eq. rec%E150 ) Then       !
!!$                  !
!!$                  Call queryReceiver(listBe(jj,ii)%receiverName,                 &
!!$                       &                 IsConnected  = IsConnectedRX,           &
!!$                       &                 polarization = pol%hor,                 &
!!$                       &                 subband      = listBe(jj,ii)%subband )  !
!!$                  !
!!$                  Call queryReceiver(listBe(jj,ii)%receiverName,                 &
!!$                       &                 IsConnected  = IsConnectedRX2,          &
!!$                       &                 polarization = pol%ver,                 &
!!$                       &                 subband      = listBe(jj,ii)%subband )  !
!!$                  !
!!$                  If (.Not. IsConnectedRX .And. .Not. IsConnectedRX2) Then
!!$                     messageText  =                                              & 
!!$                          &   ' Receiver: '//listBe(jj,ii)%receiverName//        &
!!$                          &   '  '//listBe(jj,ii)%subband//                      &
!!$                          &   ' is not connected '                               !
!!$                     Call pakoMessage(priorityW,severityW,command,messageText)            
!!$                     Write (messageText,*)                                       & 
!!$                          &   ' Backend: ',                                      &
!!$                          &     listBe(jj,ii)%name, listBe(jj,ii)%nPart,         &
!!$                          &   ' previous configuration will be disconnected '    !
!!$                     Call pakoMessage(priorityW,severityW,command,messageText)            
!!$                     listBe(jj,ii)%isConnected = .False.
!!$                  End If
!!$                  !
!!$               End If   !!  listBe(jj,ii)%receiverName .Eq. rec%E150
               !
            Else If (jj.eq.iFTS .And. ii.Le.4       )  Then                      !  !!  FTS on Coax 1 to 4
               !
               Call queryReceiver(listBe(jj,ii)%receiverName,                    &
                    &                 IsConnected  = IsConnectedRX,              &
                    &                 polarization = listBe(jj,ii)%polarization, &
                    &                 subband      = listBe(jj,ii)%subband     ) !
               !
               If (.Not. IsConnectedRX) Then
                  Write (messageText,*)                                          & 
                       &            ' Receiver: ',                               &
                       &             listBe(jj,ii)%receiverName,                 &
                       &        "/"//listBe(jj,ii)%polarization,                 &
                       &             listBe(jj,ii)%subband,                      &
                       &            ' is not connected '                         !
                  Call pakoMessage(priorityW,severityW,command,messageText)      
                  Write (messageText,*)                                          & 
                       &            ' Backend: ',                                &
                       &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,    &
                       &         ' previous configuration will be disconnected ' !
                  Call pakoMessage(priorityW,severityW,command,messageText)            
                  listBe(jj,ii)%isConnected = .False.
               End If
               !
            Else If (jj.eq.iFTS .And. ii.Ge.5 .And. ii.Le.8 )   Then             !  !!  FTS on Coax 5 to 8
               !                                                                 !  !!  Q/TBD: make independant of sequence?
               !
               IsConnectedRX =                       esbCoax(ii)%isRequested     &
                    &   .And.  listBe(jj,ii)%receiverName                        &
                    &                           .Eq. esbCoax(ii)%bandName        &
                    &   .And.  listBe(jj,ii)%polarization                        &
                    &                           .Eq. esbCoax(ii)%polarization    &
                    &   .And.  listBe(jj,ii)%subband                             &
                    &                           .Eq. esbCoax(ii)%subband         !
               !
               If (.Not. IsConnectedRX) Then
                  Write (messageText,*)                                          & 
                       &            ' Receiver: ',                               &
                       &             listBe(jj,ii)%receiverName,                 &
                       &        "/"//listBe(jj,ii)%polarization,                 &
                       &             listBe(jj,ii)%subband,                      &
                       &            ' is not connected on Coax ',                &
                       &              ii                                         !
                  Call pakoMessage(priorityW,severityW,command,messageText)      
                  Write (messageText,*)                                          & 
                       &            ' Backend: ',                                &
                       &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,    &
                       &         ' previous configuration will be disconnected ' !
                  Call pakoMessage(priorityW,severityW,command,messageText)            
                  listBe(jj,ii)%isConnected = .False.
               End If
               !
            Else                                     !! not BBC nor FTS
               !
               Call queryReceiver(listBe(jj,ii)%receiverName,                    &
                    &                 IsConnected  = IsConnectedRX,              &
                    &                 polarization = listBe(jj,ii)%polarization, &
                    &                 subband      = listBe(jj,ii)%subband     ) !
               !
               If (.Not. IsConnectedRX) Then
                  Write (messageText,*)                                          & 
                       &            ' Receiver: ',                               &
                       &             listBe(jj,ii)%receiverName,                 &
                       &        "/"//listBe(jj,ii)%polarization,                 &
                       &             listBe(jj,ii)%subband,                      &
                       &            ' is not connected '                         !
                  Call pakoMessage(priorityW,severityW,command,messageText)      
                  Write (messageText,*)                                          & 
                       &            ' Backend: ',                                &
                       &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,    &
                       &         ' previous configuration will be disconnected ' !
                  Call pakoMessage(priorityW,severityW,command,messageText)            
                  listBe(jj,ii)%isConnected = .False.
               End If
               !
            End If                                   !! end if BBC or not
            !
         End If
         !
      Else                                           !!  not EMIR
         !
         If (listBe(jj,ii)%isConnected .And.                                     &
              & listBe(jj,ii)%receiverName.Ne.GPnone) Then                       !
            Call queryReceiver(listBe(jj,ii)%receiverName,                       &
                 &                               IsConnectedRX)                  !
            If (.Not. IsConnectedRX) Then
               Write (messageText,*)                                             & 
                    &            ' Receiver: ',                                  &
                    &             listBe(jj,ii)%receiverName,                    &
                    &            ' is not connected '                            !
               Call pakoMessage(priorityW,severityW,command,messageText)         
               Write (messageText,*)                                             & 
                    &            ' Backend: ',                                   &
                    &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,       &
                    &            ' previous configuration will be disconnected ' !
               Call pakoMessage(priorityW,severityW,command,messageText)            
               listBe(jj,ii)%isConnected = .False.
            End If
         End If
         !
      End If
      !
      If (listBe(jj,ii)%isConnected .And.                                        &
           & listBe(jj,ii)%receiverName2.Ne.GPnone) Then                         !
         Call queryReceiver(listBe(jj,ii)%receiverName2,                         &
              &                               IsConnectedRX)                     !
         If (.Not. IsConnectedRX) Then
            Write (messageText,*)                                                & 
                 &            ' Receiver: ',                                     &
                 &             listBe(jj,ii)%receiverName2,                      &
                 &            ' is not connected '                               !
            Call pakoMessage(priorityW,severityW,command,messageText)            
            Write (messageText,*)                                                & 
                 &            ' Backend: ',                                      &
                 &             listBe(jj,ii)%name, listBe(jj,ii)%nPart,          &
                 &            ' previous configuration will be disconnected '    !
            Call pakoMessage(priorityW,severityW,command,messageText)            
            listBe(jj,ii)%isConnected = .False.
         End If
      End If
      !
   End Do
End Do
!!
!!$               Write (6,*) "   ii: ", ii
!!$               Write (6,*) "   esbCoax(ii): "
!!$               Write (6,*) esbCoax(ii)
!!$               !
!!$               Write (6,*) "   esbCoax(ii)%bandName     : ",  esbCoax(ii)%bandName       
!!$               Write (6,*) "   esbCoax(ii)%polarization : ",  esbCoax(ii)%polarization   
!!$               Write (6,*) "   esbCoax(ii)%sideband     : ",  esbCoax(ii)%sideband       
!!$               Write (6,*) "   esbCoax(ii)%subband      : ",  esbCoax(ii)%subband        
!!$               Write (6,*) "   esbCoax(ii)%isRequested  : ",  esbCoax(ii)%isRequested 
!!$               !
!!$               Write (6,*) "   listBe(jj,ii)%receiverName : ", listBe(jj,ii)%receiverName
!!$               Write (6,*) "   listBe(jj,ii)%polarization : ", listBe(jj,ii)%polarization
!!$               Write (6,*) "   listBe(jj,ii)%subband      : ", listBe(jj,ii)%subband
!!$               !
