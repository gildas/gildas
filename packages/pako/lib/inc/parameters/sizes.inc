!
! Id: inc/parameters/sizes.inc, v1.0  Hans Ungerechts
!
! Internal use for definitions: (similar to usage in moduleGlobalParameters)  
!
!! TBD: compare sizes in standard gildas programs
!
Integer, Parameter :: lenPro        =   4  !  length of Program name
Integer, Parameter :: lenCO         =  12  !  length of commands and options
Integer, Parameter :: lenCh         =  24  !  length of choices/ character keys 
Integer, Parameter :: lenVar        = 128  !  length of standard character variables
Integer, Parameter :: lenLine       = 256  !  length of standard lines
Integer, Parameter :: lenLineDouble = 512  !  length of long lines
!
Integer, Parameter :: kindSingle = 4  !  kind / size of single precision float /real
Integer, Parameter :: kindDouble = 8  !  kind / size of double precision float /real
!
Character(len=lenCh),  Parameter  ::  GPnone   = "none"
Logical,               Parameter  ::  GPnoneL  = .False.
Integer,               Parameter  ::  GPnoneI  = 0
Real(Kind=kindSingle), Parameter  ::  GPnoneR  = 0.0E0
Real(Kind=kindSingle), Parameter  ::  GPnoneR4 = 0.0E0
Real(Kind=kindDouble), Parameter  ::  GPnoneD  = 0.0D0
Real(Kind=kindDouble), Parameter  ::  GPnoneR8 = 0.0D0
!
