!
      iBack           = 0
      backendSelected = "none"
!
! *** BACKEND NAME
!
      iOption   = 0
      iArgument = 1
!
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,                            &
     &        cInput,lengthInput,.True.,errorC)
         cInputUpper = cInput
         Call SIC_UPPER(cInputUpper)
      Else
         errorL = .True.
         messageText =                                                  &
     &        "no backend name specified"
         Call pakoMessage(priorityI,severityI,command,messageText)
      End If
!
      If (errorC) Then
         messageText =                                                  &
     &     "backend name invalid"
         Call pakoMessage(priorityE,severityE,command,messageText)
      End If
!
      error = error .Or. errorC
!
! *** Check against valid backend names
!
      If (SIC_PRESENT(iOption,iArgument) .And. .Not. error) Then
!
         errorNotFound         = .False.
         errorNotUnique        = .False.
         choiceIndex = 0
!
         Do ii = 1,nDimBackends,1
            c12 = backendNameChoices(ii)
            call SIC_UPPER(c12)
            If (Index(c12,                                              &
      &               cInputUpper(1:lengthInput)).Ge.1 ) Then
               If (choiceIndex .Eq. 0) Then
                  choiceIndex = ii
               Else
                  errorNotUnique = .True.
               End If
            End If
         End Do
         errorNotFound =  choiceIndex .Eq. 0
!
         If (errorNotFound) Then
            messageText =                                               &
     &     "backend name not found: "//cInputUpper(1:lengthInput)
            Call pakoMessage(priorityE,severityE,command,messageText)
         End If
!
         If (errorNotUnique) Then
            messageText =                                               &
     &     "backend name not unique: "//cInputUpper(1:lengthInput)
            Call pakoMessage(priorityE,severityE,command,messageText)
         End If
!
         If (.Not. (errorNotUnique .Or. errorNotFound)) Then
            iBack           = choiceIndex
            backendSelected = backendNameChoices(choiceIndex)
            vars(iIn,iBack)%wasConnected = .True.
            vars(iIn,iBack)%isConnected  = .True.
            if (SIC_NARG(0).eq.1 .and.                                  &
     &         .not. (SIC_PRESENT(1,0).or.SIC_PRESENT(2,0).or.          &
     &                SIC_PRESENT(3,0).or.SIC_PRESENT(4,0).or.          &
     &                SIC_PRESENT(5,0)                                  &
     &               ) ) then
               connectAllParts           =  .True.
            end if
         Else
            iBack           = 0
            backendSelected = "none"
         End If
!
         l = lenc(backendSelected)
!D         Write (6,*) "   backendSelected ->",                           &
!D      &                  backendSelected(1:l),"<-"
!D         Write (6,*) "   iBack:       ",                                &
!D      &                  iBack
!
      End If
!
      error = error .Or. errorNotUnique .Or. errorNotFound
!
