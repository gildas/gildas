!
!  Id: parametersStartEnd.inc,v 1.1.0 2009-04-02 Hans Ungerechts
!
!  parameters pStart pEnd %x %y
!
!D Write (6,*) "      --> parametersStartEnd.inc : "
!
errorR = .False.
!
iOption   = 0
iArgument = 1
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pStart%x,                                            &
           &           GV%angleFactorR,                                          &
           &           errorR)
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 2
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pStart%y,                                            &
           &           GV%angleFactorR,                                          &
           &           errorR)
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 3
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pEnd%x,                                              &
           &           GV%angleFactorR,                                          &
           &           errorR)
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 4
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pEnd%y,                                              &
           &           GV%angleFactorR,                                          &
           &           errorR)
   Endif
Endif
ERROR = ERROR .Or. errorR
!
