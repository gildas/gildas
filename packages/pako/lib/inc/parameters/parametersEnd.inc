!
!     $Id$
!
!     parameters pEnd%x pEnd%y
!
!     TBD: support angle units
!
      errorR = .False.
!
      iOption   = 0
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%pEnd%x,                                            &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%pEnd%y,                                            &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
