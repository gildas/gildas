!
!  Id: saveSetStransitionAcceleration.inc,v 1.0.8 2007-09-15 Hans Ungerechts
!
If   ( (      GV%privilege.Eq.setPrivilege%bolometerPool                         &
     &   .Or. GV%privilege.Eq.setPrivilege%staff                                 &
     &   .Or. GV%privilege.Eq.setPrivilege%ncsTeam )                             &
     & .And.                                                                     &
     &   (Abs(vars(iValue)%transitionAcceleration-                               &
     &        vars(iDefault)%transitionAcceleration)                             &
     &     .Gt.0.001)                                                            &
     & ) Then                                                                    !
!
   Write (lineOut,*) 'transitionAcceleration ', GV%transitionAcceleration
   !
   Include 'inc/parameters/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
