!
!
Do ii = 1, nDimReceiverChoices, 1
   Call queryReceiver(receiverChoices(ii), isConnected)
   If (isConnected) Then
      !
      Write (lineOut,*)                                                      &
           &     CMD(1:lCMD),                                                &
           &     vars(iValue)%fOffsets(ii,1),                                &
           &     vars(iValue)%fOffsets(ii,2),                                &
           &     " /receiver ", receiverChoices(ii)
      !
      Write (iUnit,*) lineOut(1:lenc(lineOut))
      !
   End If
   !
End Do
