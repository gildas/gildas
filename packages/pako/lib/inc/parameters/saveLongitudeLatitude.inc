!
!     $Id$
!
      l  = lenc(vars(iValue)%longitude%text)
      l2 = lenc(vars(iValue)%latitude%text)
      lineOut = ' '//vars(iValue)%longitude%text(1:l)//                  &
     &          ' '//vars(iValue)%latitude%text(1:l2)
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
