!
!  Id: saveSystem.inc,v 1.0.4 2006-07-18 Hans Ungerechts
!
l = lenc(vars(iValue)%systemName)
Write (c2,*) vars(iValue)%epoch
l2 = lenc(c2) 
!
If      (vars(iValue)%systemName(1:l) .Eq. sys%equatorial) Then
   lineOut = ' "'//vars(iValue)%systemName(1:l)//'" '//c2(1:l2)
Else If (vars(iValue)%systemName(1:l) .Eq. sys%ecliptic) Then
   lineOut = ' "'//vars(iValue)%systemName(1:l)//'" '//c2(1:l2)
Else
   lineOut = ' "'//vars(iValue)%systemName(1:l)//'" '
End If 
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
