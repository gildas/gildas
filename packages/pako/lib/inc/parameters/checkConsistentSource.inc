  !
  !  Id: checkConsistentSource.inc,v 1.3.0  2017-08-31 Hans Ungerechts
  !  Id: checkConsistentSource.inc,v 1.2.3  2014-10-16 Hans Ungerechts
  !  Id: checkConsistentSource.inc,v 1.2.2  2013-02-08 Hans Ungerechts
  !  Id: checkConsistentSource.inc,v 1.1.13 2012-11-19 Hans Ungerechts
  !
  !D   Write (6,*) "       --> checkConsistentSource v 1.3.0  2017-08-31 "
  !
  errorInconsistent = .False.
  !
  If (    .Not. (    vars(iIn)%isBody                                            &
       &        .Or. vars(iIn)%isPlanet                                          &
       &        .Or. vars(iIn)%isSatellite ) ) Then                              !
     !
     Call queryReceiver(rec%Bolo, isConnectedBolo, bolometerName = bolometerName)
     !D      Write (6,*) "bolometer ", bolometerName, " isConnectedBolo:", isConnectedBolo
     !
     Call queryReceiver(rec%HERA1, isConnectedHERA1)
     !D      Write (6,*) "HERA1", " isConnected:", isConnectedHERA1
     !
     Call queryReceiver(rec%HERA2, isConnectedHERA2)
     !D      Write (6,*) "HERA2", " isConnected:", isConnectedHERA2
     !
     isConnectedHERA = isConnectedHERA1.Or.isConnectedHERA2
     !D      Write (6,*) "HERA ", " isConnected:", isConnectedHERA
     !
     If (             isConnectedBolo                                            &
          &     .And. (       bolometerName.Eq.bolo%GISMO                        &
          &             .Or.  bolometerName.Eq.bolo%NIKA                         &
          &           ) ) Then                                                   !
        !        
        If     (.Not.                                                            &
             &        (    (vars(iIn)%systemName.Eq.sys%equatorial               &
             &             .And. vars(iIn)%equinoxSystemName.Eq.equ%J            &
             &             .And. vars(iIn)%epoch.Eq.2000.0 )                     &
             &        .Or.                                                       &
             &             vars(iIn)%systemName.Eq.sys%galactic                  &
             &        .Or.                                                       &
             &             vars(iIn)%systemName.Eq.sys%horizontal                &
             &        )                                                          &
             & ) Then                                                            !
           !
           errorInconsistent = .True.
           messageText =                                                         &
                   & "Systems other than Equatorial J2000.0 or Galactic are "    &
                   & //" NOT released for use"                                   !
           l = len_trim (messageText)
           Call  pakoMessage(priorityE,severityE,command,messageText(1:l))
           !
        End If
        !
     End If   !!   Bolo
     !
     If ( isConnectedHERA ) Then
        !
        If     (.Not.                                                            &
             &        (    (vars(iIn)%systemName.Eq.sys%equatorial               &
             &             .And. vars(iIn)%equinoxSystemName.Eq.equ%J            &
             &             .And. vars(iIn)%epoch.Eq.2000.0 )                     &
             &        )                                                          &
             & ) Then                                                            !
           !
           If (GV%privilege.Ne.setPrivilege%ncsTeam) Then
              !
              errorInconsistent = .True.
              messageText =                                                      &
                   & "Systems other than Equatorial J2000.0 "                    &
                   & //" NOT released for use with HERA"                         !
              l = len_trim (messageText)
              Call  pakoMessage(priorityE,severityE,command,messageText(1:l))
              !
           End If
           !
        End If
        !
     End If   !!   HERA
     !
     If ( .Not.isConnectedBolo .And. .Not.isConnectedHERA ) Then
        !
        If     (.Not.                                                            &
             &        (    (vars(iIn)%systemName.Eq.sys%equatorial               &
             &             .And. vars(iIn)%equinoxSystemName.Eq.equ%J            &
             &             .And. vars(iIn)%epoch.Eq.2000.0 )                     &
             &        .Or.                                                       &
             &             vars(iIn)%systemName.Eq.sys%galactic                  &
             &        )                                                          &
             & ) Then                                                            !
           !
           If (GV%privilege.Ne.setPrivilege%ncsTeam) Then
              !
              errorInconsistent = .True.
              messageText =                                                      &
                   & "Systems other than Equatorial J2000.0 or Galactic are "    &
                   & //" NOT released for use"                                   !
              l = len_trim (messageText)
              Call  pakoMessage(priorityE,severityE,command,messageText(1:l))
              !
           End If
           !
        End If
        !
     End If   !!  not Bolo   not HERA
     !
     If ( vars(iIn)%systemName.Eq.sys%horizontal ) Then
        !
        !D         Write (6,*) "   checks for system       ", sys%horizontal
        !D         Write (6,*) "   vars(iIn)%latitude%deg: ", vars(iIn)%latitude%deg
        !
        If (         vars(iIn)%latitude%deg.Lt.0.0                               &
             &  .Or. vars(iIn)%latitude%deg.Ge.90.0 ) Then                       !
           !
           errorInconsistent = .True.
           messageText =                                                         &
                &    "for system "//sys%horizontal                               &
                &    //" latitude must be between 0 and 90 [deg]"                !
           l = len_trim (messageText)
           Call  pakoMessage(priorityE,severityE,command,messageText(1:l))
           !
        End If
        !
     End If   !!   vars(iIn)%systemName.Eq.sys%horizontal
     !
  End If   !!   .Not. (    vars(iIn)%isBody ... isPlanet is Satellite  )
  !
  !
  !D   Write (6,*) "           errorInconsistent:        ", errorInconsistent
  !D   Write (6,*) "           checkConsistentSource --> "

  !! ******************************************************************************

  !D      Write (6,*) "      rec%Bolo, isConnected: ", rec%Bolo, isConnected
  !D      Write (6,*) "      bolometerName:         ", bolometerName
  !D      Write (6,*) "      bolo%GISMO:             ", bolo%GISMO
  !D      Write (6,*) "      bolo%NIKA:             ", bolo%NIKA
  !D      !
  !D      Write (6,*) "     vars(iIn)%systemName:   ", vars(iIn)%systemName
  !D      Write (6,*) "     sys%horizontal:         ", sys%horizontal
  !
