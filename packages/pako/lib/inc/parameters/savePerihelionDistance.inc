!
!     $Id$
!
!
      Write (c1,*) vars(iValue)%perihelionDistance
      l1 = lenc(c1) 
      lineOut = c1(1:l1)
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      lineOut(44:Len(lineOut)) = " ! Perihelion Distance [AU] "
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
