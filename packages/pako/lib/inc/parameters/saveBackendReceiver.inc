!
!     Id: saveBackendReceiver.inc, v0.9 2005-10-10 Hans Ungerechts
!
!D Write (6,*) "   --> saveBackendReceiver.inc "
!D Write (6,*) listBe(jj,ii)%receiverName
!D Write (6,*) listBe(jj,ii)%receiverName2
!D Write (6,*) listBe(jj,ii)%mode
!D Write (6,*) listBe(jj,ii)%percentage
!
l       = lenc(listBe(jj,ii)%receiverName)
lineOut = ' "'//listBe(jj,ii)%receiverName(1:l)//'"'
!
If (    listBe(jj,ii)%mode .Eq. BEmode%Para .Or.                                 &
     &  listBe(jj,ii)%mode .Eq. BEmode%Pola) Then
   !
   l = lenc(listBe(jj,ii)%receiverName2)
   lineOut = lineOut(1:len_trim(lineOut))//                                      &
        &    ' "'//listBe(jj,ii)%receiverName2(1:l)//'"'
   !
End If
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
