  !
  !     Id: parametersAmplitude.inc,v 1.1.12 2012-03-01 Hans Ungerechts
  !
  !     parameters amplitude pEnd %x %y
  !
  !     TBD: support angle units
  !
  !D   Write (6,*) "      --> parametersAmplitude.inc,v "
  !
  errorR = .False.
  !
  iOption   = 0
  iArgument = 1
  If (SIC_PRESENT(iOption,iArgument)) Then
     Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                      &
          &              .False.,errorC)                                         !
     If (cInput(1:1).Ne.'*') Then
        Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
        If (.Not. errorR) Call checkR4(command,rInput,                           &
             &           vars%amplitude%x,                                       &
             &           errorR)                                                 !
     Endif
  Endif
  ERROR = ERROR .Or. errorR
  !
  iArgument = iArgument+1
  If (SIC_PRESENT(iOption,iArgument)) Then
     Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                      &
          &              .False.,errorC)                                         !
     If (cInput(1:1).Ne.'*') Then
        Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
        If (.Not. errorR) Call checkR4(command,rInput,                           &
             &           vars%amplitude%y,                                       &
             &           errorR)                                                 !
     Endif
  Endif
  ERROR = ERROR .Or. errorR
  !
  !D   Write (6,*) "vars%amplitude%x:" , vars%amplitude%x
  !D   Write (6,*) "vars%amplitude%y:" , vars%amplitude%y
