  !
  !     Id: parametersFoffsets.inc,v 1.2.4 2015-07-15  Hans Ungerechts
  !     Id: parametersFoffsets.inc,v 0.9   2005-08-09  Hans Ungerechts
  !
  Call pako_message(seve%t,programName,                                          &
       &  " module SwFrequency, v 1.2.4 2015-07-15  --> parametersFoffsets.inc") ! trace execution
  !      
  !D Write (6,*) "      parametersFoffsets.inc "
  !D Write (6,*) "           vars(iIn)%receiverName ", vars(iIn)%receiverName
  !D Write (6,*) "           vars(iIn)%receiverI    ", vars(iIn)%receiverI
  !D Write (6,*) "           iMatch                 ", iMatch
  !D Write (6,*) "           iRec                   ", iRec
  !
!!$      If (SIC_NARG(0).Ge.1.And.SIC_NARG(0).Le.4) Then
  !
  If (SIC_NARG(0).Ge.1.And.SIC_NARG(0).Le.2) Then
     Do iArgument = 1, SIC_NARG(0), 1
        If (SIC_PRESENT(0,iArgument)) Then
           Call SIC_CH(LINE,0,iArgument,                                         &
                &           cInput,lengthInput,.False.,errorC)                   !
           If (cInput(1:1).Ne.'*') Then
              Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
              If (.Not. errorR) Then
                 If (iRec .Eq. 0 ) Then
                    !
                    Do ii = 1, nDimReceiverChoices, 1
                       Call queryReceiver(receiverChoices(ii), Result)
                       If (Result) Then
                          !
                          Call checkR4(command,rInput,                           &
                               &               vars%fOffsets(ii,iArgument),      &
                               &               errorR)                           !
                          If (errorR) Then
                             Write (messageText,*)                               &
                                  &   "value ", rInput,                          &
                                  &   " outside limits ",                        &
                                  &        vars(iLImit1)%fOffsets(ii,iArgument), &
                                  &   " to ",                                    &
                                  &        vars(iLImit2)%fOffsets(ii,iArgument), &
                                  &   " for ",  receiverChoices(ii)              !
                             Call PakoMessage(priorityE,severityE,               &
                                  &           command,messageText)               !
                          End If
                          errorL = errorL .Or. errorR
                          !D  Else
                          !D     Write (6,*) " ii, receiverChoices(ii): ",       &
                          !D          &        ii, receiverChoices(ii),          &
                          !D          &      " is  NOT connected "               !
                       End If
                    End Do
                    !
!!$!
!!$                  Call checkR4(command,rInput,                                &
!!$                       &               vars%fOffset(iArgument),               &
!!$                       &               errorR)                                !
!!$                  If (.Not. errorR) Then
!!$                     vars(iIn)%fOffsets(:,iArgument) =                        &
!!$                          &  vars(iIn)%fOffset(iArgument)                     !
!!$                  End If
!!$!
                 Else
                    Call checkR4(command,rInput,                                 &
                         &               vars%fOffsets(iRec,iArgument),          &
                         &               errorR)                                 !
                 Endif
              Endif
           Endif
        End If
        errorL = errorL .Or. errorR
     Enddo
  Endif
  !
!!$      If (SIC_NARG(0).Gt.4) Then
!!$         messageText =                                                 &
!!$     &        "fOffset1 [fOffset2] [fOffset3] [fOffset4] "
!!$         Call pakoMessage(6,3,command//" up to 4 parameter(s) allowed:",  &
!!$     &   messageText)
!!$         errorNarg = .True.
!!$      End If
  !
  If (SIC_NARG(0).Gt.2) Then
     messageText =                                                             &
          &        "fOffset1 [fOffset2] "
     Call PakoMessage(priorityE,severityE,                                     &
          &   command//" 1 or 2 parameter(s) allowed:",                        &
          &   messageText)
     errorNarg = .True.
  End If
  !
  ERROR = ERROR .Or. errorR .Or. errorL .Or. errorNarg
  !
  If (.Not. ERROR) Then
     If (SIC_NARG(0) .Eq. 1 .And. cInput(1:1).Ne.'*') Then
        vars(iIn)%fOffsets(:,2) = -vars(iIn)%fOffsets(:,1)
     End If
  End If



