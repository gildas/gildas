!
! Id: parametersBackend.inc,v 1.2.2 2012-08-21 Hans Ungerechts
!     parametersBackend.inc,v 1.2.1 2012-07-18 Hans Ungerechts
!     based on:
!     parametersBackend.inc,v 1.1.6 2011-07-01 Hans Ungerechts
!
Call pako_message(seve%t,programName,                                            &
     &          "    -->  parametersBackend.inc,v 1.2.2  2012-08-21")            ! this allows to trace execution 
!D Write (6,*) "                    receiverIsEMIR: ", receiverIsEMIR
!D Write (6,*) "                    receiverIsHERA: ", receiverIsHERA
!
ERROR     = .False.
errorL    = .False.
errorC    = .False.
errorR    = .False.
!
hasOptions = .False.
!
! *** check for options 
!
OPTION = 'CLEAR'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doClear = SIC_PRESENT(iOption ,0)
hasOptions = hasOptions .Or. doClear
!
OPTION = 'CONNECT'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption2,                          &
     &      errorNotFound,errorNotUnique)                                        !
doConnect = SIC_PRESENT(iOption2,0)
hasOptions = hasOptions .Or. doConnect
!
OPTION = 'DEFAULTS'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doDefaults = SIC_PRESENT(iOption ,0) 
hasOptions = hasOptions .Or. doDefaults
!
OPTION = 'DISCONNECT'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption3,                          &
     &      errorNotFound,errorNotUnique)                                        !
doDisconnect = SIC_PRESENT(iOption3,0)
hasOptions = hasOptions .Or. doDisconnect
!
option = 'FINEFTS'
Call indexCommmandOption                                                         &
     &        (command,option,commandFull,optionFull,                            &
     &        commandIndex,optionIndex,iCommand,iOption,                         &
     &        errorNotFound,errorNotUnique)                                      !
fineFTS = SIC_PRESENT(iOption,0)
hasOptions = hasOptions .Or. fineFTS
!
OPTION = 'LINENAME'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doLineName = SIC_PRESENT(iOption ,0) 
hasOptions = hasOptions .Or. doLineName
!
OPTION = 'MODE'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doMode = SIC_PRESENT(iOption ,0) 
hasOptions = hasOptions .Or. doMode
!
OPTION = 'PERCENTAGE'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doPercentage = SIC_PRESENT(iOption ,0) 
hasOptions = hasOptions .Or. doPercentage
!
OPTION = 'RECEIVER'
Call indexCommmandOption                                                         &
     &     (command,option,commandFull,optionFull,                               &
     &      commandIndex,optionIndex,iCommand,iOption,                           &
     &      errorNotFound,errorNotUnique)                                        !
doReceiver = SIC_PRESENT(iOption ,0) 
hasOptions = hasOptions .Or. doReceiver
!
!D Write (6,*) "          hasOptions:   ", hasOptions
!
If (SIC_NARG(0).Eq.2 .And. .Not.hasOptions) Then
   doConnect = .True.
End If
!
!
iBack           = GPnoneI
backendSelected = GPnone
iPart           = GPnoneI
!
! *** BACKEND NAME
!
iOption   = 0
iArgument = 1
!
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,                                           &
        &        cInput,lengthInput,.True.,errorC)                               !
Else
   errorL = .True.
   messageText =                                                                 &
        &        "no backend name specified"                                     !
   Call pakoMessage(priorityW,severityW,command,messageText)
End If
!
If (errorC) Then
   messageText =                                                                 &
        &     "backend name invalid"                                             !
   Call pakoMessage(priorityE,severityE,command,messageText)
End If
!
ERROR = ERROR .Or. errorC
!
! *** Check against valid backend names
!
If (.Not. ERROR) Then
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call pakoUmatchKey (                                                       &
           &              keys=backendChoices,                                   &
           &              key=cInput,                                            &
           &              command='BACKEND',                                     &
           &              howto='Start Upper',                                   &
           &              iMatch=iMatch,                                         &
           &              nMatch=nMatch,                                         &
           &              error=errorC,                                          &
           &              errorCode=errorCode                                    &
           &             )                                                       !
      !
      If (backendChoices(iMatch) .Eq. bac%USB) Then
         !
         If (        GV%privilege .Eq. setPrivilege%privileged                   &
              & .Or. GV%privilege .Eq. setPrivilege%staff                        &
              & .Or. GV%privilege .Eq. setPrivilege%ncsTeam     ) Then           !
            Write (messageText, *) backendChoices(iMatch),"is non-standard"
            Call pakoMessage(priorityW,severityW,command,messageText)
            Write (messageText, *) backendChoices(iMatch),"accepted"
            Call pakoMessage(priorityI,severityI,command,messageText)
         Else
            errorC = .True.
            Write (messageText, *) backendChoices(iMatch),"requires privilege"
            Call pakoMessage(priorityE,severityE,command,messageText)
         End If
         !
      End If
      !
      ERROR = ERROR .Or. errorC
      !
      If (.Not. ERROR) Then
         iBack                        = iMatch
         backendSelected              = backendChoices(iMatch)
         vars(iIn,iBack)%isConnected  = .True.
         vars(iIn,iBack)%wasConnected = .True.
         vars(iIn,iBack)%name         = backendChoices(iMatch)
         vars(iIn,iBack)%percentage   = vars(iDefault,iBack)%percentage
      End If
      !
   End If
End If
!
!D Write (6,*) "      iBack:                ", iBack
!D Write (6,*) "      vars(iIn,iBack)%name  " , vars(iIn,iBack)%name 
!
If   ( iBack.Eq.iFTS ) Then
   !
   If (GV%switchingMode.Ne.GPnone .And. GV%tPhase.Le.0.1-0.001) Then
      ERROR = .True.
      !D       Write (6,*) "          ERROR: ", error
      Write (messageText, '(a,F8.2,a,a,a,a)')                                    &
           &   " /tPhase ",            GV%tPhase,                                &
           &   " in ",                 GV%switchingMode,                         &
           &   " too small for ",      bac%FTS                                   !
      Call PakoMessage(priorityE,severityE,command,messageText)
   End If
   !
End If
!
If   ( iBack.Eq.iVESPA                                                           &
     & .And. (SIC_NARG(0).Eq.2 .Or. SIC_NARG(0).Eq.3 .Or. SIC_NARG(0).Eq.4)      &
     & .And. .Not.doClear .And. .Not.doConnect .And. .Not.doDisconnect           &
     &  ) Then                                                                   !
   !
   ERROR = .True.
   !
   messageText =                                                                 &
        &  backendChoices(iVESPA)//                                              &
        &  'needs full syntax: BACKEND VESPA '//                                 &
        &  ' nPart resolution bandwidth fShift '                                 !
   Call pakoMessage(priorityE,severityE,command,messageText)
   !
End If
!
! *** PART NUMBER
!
If (.Not. ERROR) Then
   errorI    = .False.
   errorL    = .False.
   iOption   = 0
   iArgument = 2
   !
   iInput = GPnoneI
   !
   !D    Write (6,*) "       LINE:        -->", LINE,"<--"
   !D    Write (6,*) "       LINE (trim): -->", LINE(1:len_trim(LINE)), "<--"
   !
   !D    Write (6,*) "       errorI:     ", errorI
   !D    Write (6,*) "       errorL:     ", errorL 
   !D    Write (6,*) "       iOption:    ", iOption
   !D    Write (6,*) "       iArgument:  ", iArgument
   !D    Write (6,*) "       iInput:     ", iInput
   !
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_I4(LINE,iOption,iArgument,                                        &
           &        iInput,.True.,errorI)                                        !
!!$           &        iInput,lengthInput,.True.,errorI)
      !
      !D       Write (6,*) "       iInput:      ", iInput
      !D       Write (6,*) "       lengthInput: ", lengthInput
      !D       Write (6,*) "       errorI:      ", errorI

      If (.Not. errorI) Call checkI4(command,iInput,                             &
           &           vars(:,iBack)%nPart,                                      &
           &           errorI)                                                   !
   Else
      doConnect = .True.
      messageText =                                                              &
           &        "no backend part specified"                                  !
      Call pakoMessage(priorityW,severityW,command,messageText)
   End If
   !
   ERROR     = ERROR .Or. errorI
   !
   If (.Not.ERROR) Then
      iPart = iInput
      !D       Write (6,*) "       iPart:     ", iPart
   End If
   !
End If  !! .not.error
!
! *** get previous parameters from backend list listBE:
!
If (iBack.Ne.GPnoneI .And. iPart.Ne.GPnoneI) Then 
   If (          listBe(iBack,iPart)%isConnected                                 &
        &   .Or. listBe(iBack,iPart)%wasConnected)   Then                        !
      vars(iIn,iBack)= listBe(iBack,iPart)
   End If
End If
!
! *** re-connect without explicit option
!
If (.Not.ERROR) Then
   !
   doImplicitConnect = .True.
   Include 'inc/options/doConnectBE.inc'
   !
End If
!
If (.Not.doConnect) Then
   !
   ! *** implied values
   !
   If (iBack .Eq. iContinuum .And. SIC_NARG(0) .Eq. 2) Then   !!  variables in code are called "continuum"
      !                                                       !!  but name is "NBC"!
      !
      shortsyntax = .True.
      !
      vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
      vars(iIn,iBack)%bandWidth   =  vars(iDefault,iBack)%bandWidth 
      vars(iIn,iBack)%fShift      =  vars(iDefault,iBack)%fShift 
      messageText =                                                              &
           &  backendChoices(iContinuum)//                                       &
           &  " implies resolution bandwidth fShift"                             !
      Call pakoMessage(priorityI,severityI,command,messageText)
      !
   End If
   !
   If (iBack .Eq. iBBC .And. SIC_NARG(0) .Eq. 2) Then
      !
      shortsyntax = .True.
      !
      OPTION = 'RECEIVER'
      !
      Call indexCommmandOption                                                   &
           &     (command,option,commandFull,optionFull,                         &
           &      commandIndex,optionIndex,iCommand,iOption,                     &
           &      errorNotFound,errorNotUnique)                                  !
      !
      If (SIC_PRESENT(iOption,0)) Then
         !
         nArguments = SIC_NARG(iOption)
         !
         If (iBack .Ne. GPnoneI .And. iPart .Ne. GPnoneI) Then
            !
            iArgument = 1
            !
            If (SIC_PRESENT(iOption,iArgument)) Then
               Call SIC_CH(LINE,iOption,iArgument,                               & 
                    &           cInput,lengthInput,.True.,errorC)                !
            Else
               errorL = .True.
               messageText = "no receiver name specified "
               Call pakoMessage(priorityE,severityE,command,messageText)
            End If
            !
            ERROR = ERROR .Or. errorC .Or. errorL
            !
            If (.Not. ERROR) Then
               !
               Call pakoUmatchKey (                                              &
                    &              keys=receiverChoices,                         &
                    &              key=cInput,                                   &
                    &              command='BACKEND',                            &
                    &              howto='Start Upper',                          &
                    &              iMatch=iMatch,                                &
                    &              nMatch=nMatch,                                &
                    &              error=errorC,                                 &
                    &              errorCode=errorCode                           &
                    &             )                                              !
               !
               ERROR = ERROR .Or. errorC
               !
               If (.Not. ERROR) Then
                  !
                  If (receiverChoices(iMatch) .Eq. rec%E090) Then
                     vars(iIn,iBack)%bandWidth  = 8000.0
                  Else
                     vars(iIn,iBack)%bandWidth  = 4000.0
                  End If
                  !
                  vars(iIn,iBack)%resolution  =  vars(iIn,iBack)%bandWidth
                  vars(iIn,iBack)%fShift      =  0.0
                  messageText =                                                  &
                       &  backendChoices(iBBC)//                                 &
                       &  " implies resolution bandwidth fShift"                 !
                  Call pakoMessage(priorityI,severityI,command,messageText)
                  !
               End If
               !
            End If
            !
         End If
         !
      End If
      !
   End If
   !
   If (iBack .Eq. iFB4MHz .And. SIC_NARG(0) .Eq. 2) Then
      !
      shortsyntax = .True.
      !
      If (receiverIsEMIR) Then
         vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
         vars(iIn,iBack)%bandWidth   =  stdBandWidth (iFB4MHz,iLO)
         vars(iIn,iBack)%fShift      =     0.0
      Else
         vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
         vars(iIn,iBack)%bandWidth   =  vars(iDefault,iBack)%bandWidth 
      End If
      messageText =                                                              &
           &  backendChoices(iFB4Mhz)//                                          &
           &  " implies resolution bandwidth "                                   !
      Call pakoMessage(priorityI,severityI,command,messageText)
      !
   End If
   !
!!$   If (iBack .Eq. iFB1MHz .And. SIC_NARG(0) .Le. 3) Then                          !! OBSOLETE
!!$      !
!!$      shortsyntax = .True.
!!$      !
!!$      vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
!!$      vars(iIn,iBack)%fShift      =  vars(iDefault,iBack)%fShift
!!$      messageText =                                                      &
!!$           &  backendChoices(iFB1Mhz)//                                  &
!!$           &  " implies resolution "
!!$      Call pakoMessage(priorityI,severityI,command,messageText)
!!$      !
!!$   End If
!!$   !
!!$   If (iBack .Eq. iFB100kHz .And. SIC_NARG(0) .Le. 3) Then                        !! OBSOLETE
!!$      !
!!$      shortsyntax = .True.
!!$      !
!!$      vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
!!$      messageText =                                                      &
!!$           &  backendChoices(iFB100kHz)//                                &
!!$           &  " implies resolution "
!!$      Call pakoMessage(priorityI,severityI,command,messageText)
!!$      !
!!$   End If
   !
   If (iBack .Eq. iWILMA .And. SIC_NARG(0) .Eq. 2) Then
      !
      shortsyntax = .True.
      !
      If (receiverIsEMIR) Then
         vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
         vars(iIn,iBack)%bandWidth   =  stdBandWidth (iWILMA,iLO)
         vars(iIn,iBack)%fShift      =     0.0
      Else
         vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
         vars(iIn,iBack)%bandWidth   =  vars(iDefault,iBack)%bandWidth 
      End If
      messageText =                                                              &
           &  backendChoices(iWILMA)//                                           &
           &  " implies resolution bandwidth "                                   !
      Call pakoMessage(priorityI,severityI,command,messageText)
      !
   End If
   !
   If (iBack.Eq.iFTS .And. SIC_NARG(0).Eq.2 .And. .Not.doConnect) Then
      !
      shortsyntax = .True.
      !
      If (vars(iIn,iBack)%isConnected .And. doLineName) Then
         !
         ! --> nothing to do here, we only want to use /lineName
         !D          Write (6,*) "         vars(iIn,iBack): ", vars(iIn,iBack)
         !
      Else
         !
         If (.Not.doReceiver .And. .Not.doDisconnect                             &
              &              .And. .Not.doConnect .And. .Not.doImplicitConnect   &
              & ) Then                                                           !
            !
            errorL = .True.
            messageText =  " short syntax only works with option /Receiver "
            Call pakoMessage(priorityE,severityE,command,messageText)
            !
         Else     !!   has option /receiver
            !
            Include 'inc/options/optionReceiver.inc'
            !
            If (receiverIsEMIR) Then
               !
               If (.Not.fineFTS) Then
               !
               vars(iIn,iBack)%resolution  =  vars(iDefault,iBack)%resolution 
               vars(iIn,iBack)%bandWidth   =  stdBandWidth (iFTS,iLO)
               vars(iIn,iBack)%fShift      =  0.0               !!  this will be overruled to +/- 250
               !
               Else If (fineFTS) Then
               !
               vars(iIn,iBack)%resolution  =  FTSres%fine
               vars(iIn,iBack)%bandWidth   =  FTSbw%fine
               vars(iIn,iBack)%fShift      =  0.0               !!  this will be overruled to +/-  50
               !
               End If
               !
            Else If (receiverIsHERA) Then
               !
               If (.Not.fineFTS) Then
               !
               vars(iIn,iBack)%resolution  =  FTSres%wideHERA
               vars(iIn,iBack)%bandWidth   =  FTSbw%wideHERA 
               vars(iIn,iBack)%fShift      =  0.0
               !
               Else If (fineFTS) Then
               !
               vars(iIn,iBack)%resolution  =  FTSres%fineHERA
               vars(iIn,iBack)%bandWidth   =  FTSbw%fineHERA 
               vars(iIn,iBack)%fShift      =  0.0
               !
               End If
               !
            End If  !!  isEMIR
            !
         End If  !!  /receiver
         !
      End If   !!   /lineName
      !
      messageText =                                                              &
           &  backendChoices(iFTS)//                                             &
           &  " implies resolution and bandwidth "                               !
      Call pakoMessage(priorityI,severityI,command,messageText)
      !
   End If   !!  FTSshortSyntax
   !
   ERROR = ERROR.Or.errorL
   !
   ! *** RESOLUTION
   !
   If (.Not.ERROR .And. .Not.shortSyntax) Then
      !
      errorR    = .False.
      errorL    = .False.
      iOption   = 0
      iArgument = 3
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_R4(LINE,iOption,iArgument,                                     &
              &        rInput,.True.,errorR)                                     !
         If (.Not. errorR) Call checkR4(command,rInput,                          &
              &           vars(:,iBack)%resolution,                              &
              &           errorR)                                                !
      Else
         errorL = .True.
         messageText =                                                           &
              &        "no resolution specified"                                 !
         Call pakoMessage(priorityW,severityW,command,messageText)
      End If
      !
      ERROR = ERROR .Or. errorR
      !
   End If  !! .not.error
   !
   ! *** BANDWIDTH
   !
   If (.Not.ERROR) Then
      !
      If (.Not.shortSyntax                                                       &
           &  .Or. (iBack.Eq.iFB1MHz   .And. SIC_NARG(0).Eq.3)                   &
           &  .Or. (iBack.Eq.iFB100kHz .And. SIC_NARG(0).Le.3) ) Then            !
         !
         errorR    = .False.
         errorL    = .False.
         iOption   = 0
         iArgument = 4
         !
         If (      (iBack.Eq.iFB1MHz   .And. SIC_NARG(0).Eq.3)                   &
              &  .Or. (iBack.Eq.iFB100kHz .And. SIC_NARG(0).Le.3) ) Then         !
            iArgument = 3
         End If
         !
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_R4(LINE,iOption,iArgument,                                  &
                 &        rInput,.True.,errorR)                                  !
            If (.Not. errorR) Call checkR4(command,rInput,                       &
                 &           vars(:,iBack)%bandwidth,                            &
                 &           errorR)                                             !
         Else
            errorL = .True.
            messageText =                                                        &
                 &        "no bandwidth specified"                               !
            Call pakoMessage(priorityW,severityW,command,messageText)
         End If
         !
         ERROR = ERROR .Or. errorR
         !
      End If   !!   .not.shortSyntax .Or.
      !
   End If  !! .not.error
   !
   ! *** FREQUENCY SHIFT
   !
   If (.Not.ERROR .And. .Not.shortSyntax) Then
      !
      errorR    = .False.
      errorL    = .False.
      iOption   = 0
      iArgument = 5
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_R4(LINE,iOption,iArgument,                                     &
              &        rInput,.True.,errorR)                                     !
         If (.Not. errorR) Call checkR4(command,rInput,                          &
              &           vars(:,iBack)%fShift,                                  &
              &           errorR)                                                !
      Else
         errorL = .True.
         messageText =                                                           &
              &        "no frequency shift specified"                            !
         Call pakoMessage(priorityW,severityW,command,messageText)
      End If
      !
      ERROR = ERROR .Or. errorR
      !
   End If  !! .not.error
   !
   ! *** RECEIVER
   !
   If (.Not.ERROR .And. .Not.shortSyntax) Then
      !
      iOption   = 0
      iArgument = 6
      !
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,                                     &
              &        cInput,lengthInput,.True.,errorC)                         !
      Else
         errorL = .True.
         messageText =                                                           &
              &        "no receiver name specified as parameter"                 !
         Call pakoMessage(priorityI,severityI,command,messageText)
      End If
      !
      !D       Write (6,*) "      cInput   ", cInput
      !
      ! *** Check against valid receiver names
      !
      If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
         !
         Call pakoUmatchKey (                                                    &
              &              keys=receiverChoices,                               &
              &              key=cInput,                                         &
              &              command='BACKEND',                                  &
              &              howto='Start Upper',                                &
              &              iMatch=iMatch,                                      &
              &              nMatch=nMatch,                                      &
              &              error=errorC,                                       &
              &              errorCode=errorCode                                 &
              &             )                                                    !
         !
         ERROR = ERROR .Or. errorC
         !
         If (.Not. ERROR) Then
            vars(iIn,iBack)%receiverName = receiverChoices(iMatch)
         End If
         !
      End If
      !
   End If  !! .not.error
   !
   !D    Write (6,*) "   vars(iIn,iBack)%resolution:    ", vars(iIn,iBack)%resolution
   !D    Write (6,*) "   vars(iIn,iBack)%bandwidth:     ", vars(iIn,iBack)%bandwidth
   !D    Write (6,*) "   vars(iIn,iBack)%fShift:        ", vars(iIn,iBack)%fShift
   !D    Write (6,*) "   vars(iIn,iBack)%receiverName: ", vars(iIn,iBack)%receiverName
   !
   !  ** EMIR
   If (     vars(iIn,iBack)%receiverName .Eq. rec%E090                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E150                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E230                           &
     & .Or. vars(iIn,iBack)%receiverName .Eq. rec%E300 ) Then                    !
      !
      ! *** polarization
      !
      If (.Not.ERROR .And. .Not.shortSyntax) Then
         !
         iOption   = 0
         iArgument = 7
         !
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &           cInput,lengthInput,.True.,errorC)                   !
         Else
            vars(iIn,iBack)%polarization = GPnone
            messageText = "no EMIR polarization specified as parameter"
            Call pakoMessage(priorityI,severityI,command,messageText)
         End If
         !
         !D          Write (6,*) "      cInput   ", cInput
         !
         If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
            !
            Call pakoUmatchKey (                                                 &
                 &              keys=polarizationChoices,                        &
                 &              key=cInput,                                      &
                 &              command='BACKEND',                               &
                 &              howto='Start Upper',                             &
                 &              iMatch=iMatch,                                   &
                 &              nMatch=nMatch,                                   &
                 &              error=errorC,                                    &
                 &              errorCode=errorCode                              &
                 &             )                                                 !
            !
            ERROR = ERROR .Or. errorC
            !
            If (.Not. ERROR) Then
               vars(iIn,iBack)%polarization = polarizationChoices(iMatch)
            End If
            !
         End If
         !
      End If
      !
      ! *** subband
      !
      If (.Not.ERROR .And. .Not.shortSyntax) Then
         !
         iOption   = 0
         iArgument = 8
         !
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &           cInput,lengthInput,.True.,errorC)                   !
         Else
            vars(iIn,iBack)%subband = GPnone
            messageText = "no EMIR subband specified as parameter"
            Call pakoMessage(priorityI,severityI,command,messageText)
         End If
         !
         !D          Write (6,*) "      cInput   ", cInput
         !
         If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
            !
            Call pakoUmatchKey (                                                 &
                 &              keys=subbandChoices,                             &
                 &              key=cInput,                                      &
                 &              command='BACKEND',                               &
                 &              howto='Start Upper',                             &
                 &              iMatch=iMatch,                                   &
                 &              nMatch=nMatch,                                   &
                 &              error=errorC,                                    &
                 &              errorCode=errorCode                              &
                 &             )                                                 !
            !
            ERROR = ERROR .Or. errorC
            !
            If (iBack.Eq.iBBC) Then               !!  
               !
               If (.Not. ERROR) Then
                  vars(iIn,iBack)%subband = subbandChoices(iMatch)
               End If
               !
            Else                                  !!  not BBC
               !
               If    (         GV%iUserLevel .Lt. iExperienced                   &
                    &  .And. (iMatch.Eq.iLSB .Or. iMatch.Eq.iUSB) ) Then         !
                  errorL = .True.
                  messageText = subbandChoices(iMatch)//                         &
                       &        "requires higher user level"                     !
                  Call PakoMessage(priorityE,severityE,                          &
                       & command,                                                &
                       & messageText)                                            !
               End If
               ERROR = ERROR .Or. errorL
               !
               If (.Not. ERROR) Then
                  vars(iIn,iBack)%subband = subbandChoices(iMatch)
               End If
               !
            End If                                !!  end if BBC or not
            !
         End If
         !
         !D          Write (6,*) "      vars(iIn,iBack)%subband:   ",                        &
         !D               &             vars(iIn,iBack)%subband                              !
         !
      End If
      !
      !D       Write (6,*) "     vars(iIn,iBack)%polarization:   ", vars(iIn,iBack)%polarization
      !D       Write (6,*) "     vars(iIn,iBack)%subband:        ", vars(iIn,iBack)%subband
      !
   Else  !!  --> not EMIR
      !
      !
      ! *** RECEIVER 2
      !
      If (.Not.ERROR .And. .Not.shortSyntax) Then
         !
         iOption   = 0
         iArgument = 7
         !
         If (SIC_PRESENT(iOption,iArgument)) Then
            Call SIC_CH(LINE,iOption,iArgument,                                  &
                 &        cInput,lengthInput,.True.,errorC)                      !
         Else
            vars(iIn,iBack)%receiverName2 = GPnone
            messageText =                                                        &
                 &        "no 2nd receiver name specified as parameter"          !
            Call pakoMessage(priorityI,severityI,command,messageText)
         End If
         !
         !D       Write (6,*) "      cInput   ", cInput
         !
         ! *** Check against valid receiver names
         !
         If (SIC_PRESENT(iOption,iArgument) .And. .Not. ERROR) Then
            !
            Call pakoUmatchKey (                                                 &
                 &              keys=receiverChoices,                            &
                 &              key=cInput,                                      &
                 &              command='BACKEND',                               &
                 &              howto='Start Upper',                             &
                 &              iMatch=iMatch,                                   &
                 &              nMatch=nMatch,                                   &
                 &              error=errorC,                                    &
                 &              errorCode=errorCode                              &
                 &             )                                                 !
            !
            ERROR = ERROR .Or. errorC
            !
            If (.Not. ERROR) Then
               vars(iIn,iBack)%receiverName2 = receiverChoices(iMatch)
            End If
            !
         End If
         !
      End If  !! .not.error
      !
   End If   !!   If "EMIR" .. Else
   !
End If  !! .not. doConnect
!!
!!   ***************************************************************************
!!   ***************************************************************************
!!   ***************************************************************************
!!   ***   END of code
!!   ***************************************************************************
!!   ***************************************************************************
!!   ***************************************************************************

!!$         If       (vars(iIn,iBack)%subband.Eq.esb%lo) Then
!!$                   vars(iIn,iBack)%fShift      =    -250.0
!!$         Else If  (vars(iIn,iBack)%subband.Eq.esb%li) Then
!!$                   vars(iIn,iBack)%fShift      =     250.0
!!$         Else If  (vars(iIn,iBack)%subband.Eq.esb%ui) Then
!!$                   vars(iIn,iBack)%fShift      =    -250.0
!!$         Else If  (vars(iIn,iBack)%subband.Eq.esb%uo) Then
!!$                   vars(iIn,iBack)%fShift      =     250.0
!!$         End If

!!$         vars(iIn,iBack)%fShift      =  sign(50.0,vars(iIn,iBack)%fShift)


!! doCCD =    SIC_PRESENT(iOption ,0)                                 &
!!     & .Or.SIC_PRESENT(iOption2,0)                                 &
!!     & .Or.SIC_PRESENT(iOption3,0)
!!
!D Write (6,*) "      doCCD:        ", doCCD
!D Write (6,*) "      doPercentage: ", doPercentage
!D Write (6,*) "      doReceiver:   ", doReceiver
!!
!!$disconnectAllBackends      =  .False.
!!$connectAllBackends         =  .False.
!!$disconnectAllParts         =  .False.
!!$connectAllParts            =  .False.
!!$!
!!$If (SIC_NARG(0).Eq.0 .And.                                            &
!!$     &   .Not. (SIC_PRESENT(1,0).Or.SIC_PRESENT(2,0).Or.              &
!!$     &          SIC_PRESENT(3,0).Or.SIC_PRESENT(4,0).Or.              &        
!!$     &          SIC_PRESENT(5,0)                                      &
!!$     &         ) ) Then
!!$   connectAllBackends         =  .True.
!!$End If
!! If (.Not.doPercentage .And. .Not.doReceiver .And. SIC_NARG(0).Le.2 .And. iBack.Ge.1) Then  
!! If (.Not.doPercentage .And. .Not.doReceiver .And. iBack.Ge.1) Then   !! 2005-11-11 HU
!!                                                                      !! 2005-11-13 HU
!!
!!   doCCD = .True. 2005-11-11 !! HU
!!
!!$   ! *** check minimum number of parameters for full syntax
!!$   !
!!$   If (   .Not.doCCD        .And. .Not.doDefault  .And.                          &
!!$        & .Not.doPercentage .And. .Not.doReceiver .And.                          &
!!$        & .Not.shortSyntax  .And. SIC_NARG(0) .Lt. 4) Then
!!$      !
!!$      messageText =                                                              &
!!$           &  " required parameters: name nPart resol. bandw. [fShift] "
!!$      Call pakoMessage(priorityE,severityE,command,messageText)   
!!$      !
!!$      ERROR = .True.
!!$      !
!!$   End If
!!$   !

