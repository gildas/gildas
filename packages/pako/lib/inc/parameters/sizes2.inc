!
! Internal use for definitions: (similar to usage in moduleGlobalParameters)  
!
Integer, Parameter :: lenCh2   =  24  !  length of choices/ character keys 
Integer, Parameter :: lenVar2  = 128  !  length of standard character variables
Integer, Parameter :: lenLine2 = 256  !  length of standard lines
!
Integer, Parameter :: kindSingle2 = 4  !  kind / size of single precision float /real
Integer, Parameter :: kindDouble2 = 8  !  kind / size of double precision float /real
!
Character(len=lenCh2), Parameter  ::  GPnone2   = "none"
Integer,               Parameter  ::  GPnone2I  = 0
Real(Kind=kindSingle2), Parameter  ::  GPnone2R4 = 0.0E0
Real(Kind=kindDouble2), Parameter  ::  GPnone2R8 = 0.0D0
!
