!
!  Id: parametersWoffsets.inc,v 0.96 2005-12-07 Hans Ungerechts
!
errorL    = .False.
errorR    = .False.
errorC    = .False.
errorNarg = .False.
!
If (SIC_NARG(0).Ge.1.And.SIC_NARG(0).Le.2) Then
   Do iArgument = 1, SIC_NARG(0), 1
      !D          write (6,*) "     iArgument: ", iArgument 
      If (SIC_PRESENT(0,iArgument)) Then
         Call SIC_CH(LINE,0,iArgument,                                           &
              &           cInput,lengthInput,.False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Then
               Call checkR4(command,rInput,                                      &
                    &               vars%wOffset(iArgument),                     &
                    &               errorR)
            Endif
         Endif
      Endif
      errorL = errorL .Or. errorR
   Enddo
   If (.Not. errorR .And.SIC_NARG(0).Eq.1) Then
      vars(iIn)%wOffset(2) = -vars(iIn)%wOffset(1)
   Endif
Endif
!
If ( Abs(vars(iIn)%wOffset(2)+vars(iIn)%wOffset(1)) .Ge. 0.1) Then
   errorL = .True.
   messageText =                                                                 &
        &        "wOffset1 must be -1*wOffset2 "
   Call pakoMessage(priorityE,severityE,command,                                 &
        &   messageText)
End If
!
If (SIC_NARG(0).Gt.2) Then
   messageText =                                                                 &
        &        " up to 2  Parameter(s) allowed: wOffset1 [wOffset2] " 
   Call pakoMessage(priorityE,severityE,command,                                 &
        &   messageText)
   errorNarg = .True.
End If
!
ERROR = ERROR .Or. errorR .Or. errorL .Or. errorNarg
!
If (.Not. ERROR) Then
   vars%wThrow = Abs(vars%wOffset(2)-vars%wOffset(1))
End If
!

