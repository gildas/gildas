!
!     $Id$
!
!
      Write (c1,*) vars(iValue)%argumentOfPerihelion
      l1 = lenc(c1) 
      lineOut = c1(1:l1)
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      lineOut(44:Len(lineOut)) = " ! Argument of Perihelion [deg] "
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
