!
!  Id: parametersDisplay.inc, v 0.9 2005-09-22 Hans Ungerechts
!
iArgument = 1
!
errorL = .False.
errorR = .False.
errorC = .False.
!
If (SIC_NARG(0).Eq.1) Then
   !
   iOption   = 0
   iArgument = 1
   Call SIC_CH(LINE,iOption,iArgument,                                           & 
        &        cInput,lengthInput,.True.,errorC)
   !
   If (.Not. errorC) Then
       Call SIC_LOWER(cInput)
       If (cInput(1:1).Eq."r") Then
          whatToDo = "redo"
       Else
          errorC = .True.
       End If
   End If
   !
End If
!
If (SIC_NARG(0).Ne.1. .Or. errorC) Then
   !
   messageText =                                                                 & 
        &        "1 parameters required: whatToDo "
   Call pakoMessage(priorityE,severityE,command,messageText)
   messageText =                                                                 & 
        &        "current usage: DISPLAY REDO "
   Call pakoMessage(priorityI,severityI,command,messageText)
   errorL = .True.
   !
End If
!
ERROR = ERROR .Or. errorC .Or. errorL
!
