!
!     $Id$
!
!     parameters: length
!     Family:     1 real variable --> Angle 
!     Cousins:    parametersTurnAngle.f
!
!     TBD: support angle units
!
      errorR = .False.
!
      iOption   = 0
      iArgument = 1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,        &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.True.,errorR)
            If (.Not. errorR) Call checkR4angle(command,rInput,             &
     &           vars%length,                                          &
     &           GV%angleFactorR,           &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
