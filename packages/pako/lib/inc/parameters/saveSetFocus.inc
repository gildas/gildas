  !
  !     Id: saveSetFocus.inc,v 1.2.3  2014-03-13 Hans Ungerechts
  !     Id: saveSetFocus.inc,v 1.0.9  2007-10-01 Hans Ungerechts
  !
  Write (lineOut,'(A,F12.2)') " Focus ", GVfCorr%focusCorrection
  !D   Write (6,*) "   lineOut: -->", lineOut(1:len_trim(lineOut)), "<--"
  !
  Include 'inc/parameters/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
!!$  If (isSetfocusCorrectionX .And. GVfCorrX%focusCorrection.Ne.0.0) Then
  If (isSetfocusCorrectionX) Then
     !
     Write (lineOut,'(A,F12.2,A)') " Focus ", GVfCorrX%focusCorrection,          &
          &                        " /direction X "                              !
     !
     Include 'inc/parameters/caseContinuationLine.inc'
     !
     Write (iUnit,*) lineOut(1:lenc(lineOut))
     !
  End If
  !
!!$  If (isSetfocusCorrectionY .And. GVfCorrY%focusCorrection.Ne.0.0) Then
  If (isSetfocusCorrectionY) Then
     !
     Write (lineOut,'(A,F12.2,A)') " Focus ", GVfCorrY%focusCorrection,          &
          &                        " /direction Y "                              !
     !
     Include 'inc/parameters/caseContinuationLine.inc'
     !
     Write (iUnit,*) lineOut(1:lenc(lineOut))
     !
  End If
