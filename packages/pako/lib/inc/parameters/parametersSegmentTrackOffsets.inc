!
!  Id: parametersSegmentTrackOffsets.inc,v 1.0.10 2008-10-16 Hans Ungerechts
!
!D Write (6,*) "      --> parametersSegmentTrackOffsets.inc : "
!
iArgument = 1
!
errorL = .False.
errorR = .False.
errorC = .False.
!
If (SIC_NARG(0).Ge.1) Then
   !
   iOption   = 0
   iArgument = 1
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,0,iArgument,                                   &
           &           cInput,lengthInput,.False.,errorC)
      If (cInput(1:1).Ne.'*') Then
         Call SIC_R4(LINE,iOption,iArgument,rInput,.False.,errorR)
         If (.Not. errorR) Call checkR4angle(command,rInput,               &
              &              vars%offset%x,                                          &
              &           GV%angleFactorR,           &
              &              errorR)
      Endif
   Endif
   errorL = errorL .Or. errorR
   !
   If (SIC_NARG(0).Ge.2) Then
      !
      iArgument = 2
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,0,iArgument,                                &
              &              cInput,lengthInput,.False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,iOption,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4angle(command,rInput,            &
                 &                 vars%offset%y,                                       &
                 &           GV%angleFactorR,           &
                 &                 errorR)
         Endif
      Endif
      errorL = errorL .Or. errorR
      !
   End If
   !
   If (.Not.errorL) Then
      vars(iIn)%altPosition = "offsets"
   End If
   !
   If (errorL .And. SIC_NARG(0).Eq.1) Then
      !
      messageText =                                                 &
           &      "sourceName not yet implemented "
      Call pakoMessage(priorityE,severityE,command,messageText)
      Error = .True.
      ! TBD:
!!$            iOption   = 0
!!$            iArgument = 1
!!$            Call SIC_CH(LINE,iOption,iArgument,                            &
!!$     &           cInput100,lengthInput,.True.,errorC)
!!$            cInput100Upper = cInput100
!!$            Call SIC_UPPER(cInput100Upper)
!!$!
!!$!D          Write (6,*) "          cInputUpper100", cInput100Upper
!!$!
!!$!     TBD: split into sourcenames, check in catalog
!!$!
!!$            vars(iIn)%sourceName  = cInput100Upper               
!!$            vars(iIn)%altPosition = "sourceName"
!!$!
!!$!D          Write (6,*) "   vars(iIn)%sourceName  ", vars(iIn)%sourceName
!!$!D          Write (6,*) "   vars(iIn)%altPosition ", vars(iIn)%altPosition
!!$!
!!$            errorR= .False.
!!$            errorL= .False.
      !
   End If
   !
End If
!
ERROR = ERROR .Or. errorR .Or. errorL
!
