!
!     Id: saveSourcePorjectID.inc,v 1.0.1 2005-12-28 Hans Ungerechts
!
l = lenc(vars(iValue)%projectID)
lineOut = " PROJECT "//vars(iValue)%projectID(1:l)
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
