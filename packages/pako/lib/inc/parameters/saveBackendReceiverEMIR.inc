!
!  Id: saveBackendReceiverEMIR.inc, v1.1.0 2009-03-10 Hans Ungerechts
!
l       = lenc(listBe(jj,ii)%receiverName)
lineOut = ' "'//listBe(jj,ii)%receiverName(1:l)//'"'
!
l       = lenc(listBe(jj,ii)%polarization)
lineOut = lineOut(1:lenc(lineOut))//' "'//listBe(jj,ii)%polarization(1:l)//'"'
!
l       = lenc(listBe(jj,ii)%subband)
lineOut = lineOut(1:lenc(lineOut))//' "'//listBe(jj,ii)%subband(1:l)//'"'
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
