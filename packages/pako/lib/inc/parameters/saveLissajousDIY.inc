!
!  Id: saveLissajousDIY.inc,v 1.0.10.5 2008-10-09 Hans Ungerechts
!
Write (lineOut,*)                                                                &
     &     segList(ii)%xAmplitude,                                               &
     &     segList(ii)%yAmplitude,                                               &
     &     segList(ii)%omegaX/(2.0*Pi),                                          &
     &     segList(ii)%omegaY/(2.0*Pi)                                           !
!
lineOut = lineOut(1:lenc(lineOut))//" -"
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
Write (lineOut,*)                                                                &
     &     segList(ii)%pCenter,                                                  &
     &     segList(ii)%phiX,                                                     &
     &     segList(ii)%phiY                                                      !
!
Include 'inc/parameters/caseContinuationLine.inc'
!
Write (iUnit,*) lineOut(1:lenc(lineOut))
!
