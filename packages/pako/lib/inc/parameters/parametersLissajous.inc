!
! Id: parametersLissajous.inc,v 1.0.10.4 2008-10-07 Hans Ungerechts
!
! parameters xAmplitude yAmplitude frequencyX frequencyY pCenter%x,%y phix phiY 
!
!D Write (6,*) "      --> parametersLissajous.inc,v 1.0.10.4 "
!
errorR = .False.
!
iOption   = 0
!
iArgument = 1
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%xAmplitude,                                          &
           &           GV%angleFactorR,                                          &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 2
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%yAmplitude,                                          &
           &           GV%angleFactorR,                                          &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 3
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4(command,rInput,                             &
           &           vars%frequencyX,                                          &
           &           errorR)                                                   !
      If (.Not. errorR) vars%omegaX = 2.0*Pi*vars%frequencyX
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 4
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4(command,rInput,                             &
           &           vars%frequencyY,                                          &
           &           errorR)                                                   !
      If (.Not. errorR) vars%omegaY = 2.0*Pi*vars%frequencyY
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 5
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pCenter%x,                                           &
           &           GV%angleFactorR,                                          &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 6
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4angle(command,rInput,                        &
           &           vars%pCenter%y,                                           &
           &           GV%angleFactorR,                                          &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 7
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4(command,rInput,                             &
           &           vars%phiX,                                                &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
iArgument = 8
If (SIC_PRESENT(iOption,iArgument)) Then
   Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                        &
        &              .False.,errorC)                                           !
   If (cInput(1:1).Ne.'*') Then
      Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
      If (.Not. errorR) Call checkR4(command,rInput,                             &
           &           vars%phiY,                                                &
           &           errorR)                                                   !
   Endif
Endif
ERROR = ERROR .Or. errorR
!
