!
!  Id: checkConsistentReceiver.inc,v 1.1.1 2009-02-16 Hans Ungerechts
!
Where (vars(iIn,:)%isConnected)
   iIsConnected = 1
Elsewhere
   iIsConnected = 0
End Where
!
errorInconsistent = .True.
!
Do ii = 1, nCombos, 1
   iComboCheck  = iIsConnected*iCombosN(:,ii) 
   If (Maxval(iComboCheck).Le.0) errorInconsistent = .False.
End Do
!
If (errorInconsistent) Then
   messageText = "receiver combination not possible"
   Call PakoMessage(priorityE,severityE,command,messageText)
End If
!
If (         vars(iIn,iA100)%isConnected                                         &
     & .And. vars(iIn,iA230)%isConnected                                         &
     & .And. vars(iIn,iA100)%useSpecialLO ) Then                                 !
   errorInconsistent = .True.
   messageText = rec%A230//" can not be used when "//rec%A100//                  &
        & "uses special LO at low frequency "                                    !
   Call PakoMessage(priorityE,severityE,command,messageText)
End If
!
If (         vars(iIn,iB100)%isConnected                                         &
     & .And. vars(iIn,iB230)%isConnected                                         &
     & .And. vars(iIn,iB100)%useSpecialLO ) Then                                 !
   errorInconsistent = .True.
   messageText = rec%B230//" can not be used when "//rec%B100//                  &
        & "uses special LO at low frequency "                                    !
   Call PakoMessage(priorityE,severityE,command,messageText)
End If
!
If (vars(iIn,iRec)%tempColdCode.Eq.'L') Then
   !
   If (   coldLimits(1,iRec)%frequency%value.Le.                                 &
        &     vars(iIn,iRec)%frequency%value                                     &
        & .And.                                                                  &
        & coldLimits(2,iRec)%frequency%value.Ge.                                 &
        &     vars(iIn,iRec)%frequency%value       ) Then                        !
      messageText =                                                              &
           &   'tempColdLoad will be taken from online look-up'                  !
      Call PakoMessage(priorityI,severityI,                                      &
           &           command//" /tempLoad",messageText)                        !
   Else
      errorInconsistent = .True.
      Write (messageText,'(a,f12.6,a)')                                          &
           &   'frequency ', vars(iIn,iRec)%frequency%value,                     &
           &   ' out of range for online look-up of tempColdLoad'                !
      Call PakoMessage(priorityE,severityE,                                      &
           &           command//" /tempLoad",messageText)                        !
      Write (messageText,'(a,f12.6,a,f12.6)')                                    &
           &   'range for online look-up of tempColdLoad: ',                     &
           &      coldLimits(1,iRec)%frequency%value,                            &
           &   ' to: ',                                                          &
           &      coldLimits(2,iRec)%frequency%value                             !
      Call PakoMessage(priorityI,severityI,                                      &
           &           command//" /tempLoad",messageText)                        !
   End If
   !
End If
!
If (        vars(iIn,iRec)%channel.LT.1                                          &
     & .Or. vars(iIn,iRec)%channel.GT.vars(iIn,iRec)%nChannels) Then             !
   errorInconsistent = .True.
   Write (messageText, *) ' channel ', vars(iIn,iRec)%channel, ' not valid. ',   &
        &  ' must be between 1 and ', vars(iIn,iRec)%nChannels                   !
   Call PakoMessage(priorityE,severityE,command//" channel",messageText)
End If
!
Do ii = iHERA1, iHERA2, 1
   If (vars(iIn,ii)%isConnected) Then
      !
      If      (     Adjustl(vars(iIn,ii)%sideband).Eq.sb%USB                     &
           &  ) Then                                                             !
         !
         errorInconsistent = .True.
         Write (messageText, *)                                                  &
              &                 receiverChoices(ii),                             &
              &                 " : can only use ",                              &
              &                 sb%LSB,                                          &
              &                 " for frequency and tuning "                     !
         Call PakoMessage(priorityE,severityE,                                   &
              &           command,                                               &
              &           messageText)                                           !
         !
      End If
      !
   End If
End Do
!
If (vars(iIn,iBolo)%isConnected) Then
   !! ! OBSOLETE:
   !! ! *** check consistency with receiver offsets
   !
   !! Call CheckBolometerArray (inconsistentReceiverOffsets,  errorCode)
   !D    Write (6,*) "  inconsistentReceiverOffsets: ", inconsistentReceiverOffsets
   !D    Write (6,*) "  errorCode: ", errorCode(1:len_trim(errorCode))
   If (inconsistentReceiverOffsets) Then
      GV%notReadyReceiverOffsets = .True.
      messageText = "OFFSETS /SYSTEM Nasmyth and "//                             &
               &    "RX Array offsets are inconsistent"                          !
      Call PakoMessage(priorityW,severityW,command,messageText)
      Call PakoMessage(priorityI,severityI,command,errorCode)
   Else
      GV%notReadyReceiverOffsets = .False.
   End If
Else
   GV%notReadyReceiverOffsets = .False.
End If
!
