!
!  Id: saveCatalogSource.inc,v 1.1.1 2009-04-16 Hans Ungerechts
!
If (GVsourceCatalog(1) .Ne. GPnone) Then
   !
   l = lenc(GVsourceCatalog(1))
   lineOut = ' Source "'//GVsourceCatalog(1)(1:l)//'"'
   !
   Include 'inc/parameters/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
