!
! ID: parametersOffsets.inc,v 1.0.5.2 2006-11-27 Hans Ugerechts
!
!     parameters p %x %y
!
!D Write (6,*) " parametersOffset -->"
!D Write (6,*) "          doClear: ", doClear
!D Write (6,*) "      iiSystemOffset:     ", iiSystemOffset
!D Write (6,*) "      ccSystemOffset:     ", ccSystemOffset
!
If (iiSystemOffset.Ge.1 .And. iiSystemOffset.Le.nDimOffsetChoices) Then
   !
   varsOffsets(iIn,iiSystemOffset)%isSet = .True.
   varsOffsets(iIn,iiSystemOffset)%systemName = ccSystemOffset
   !
   errorR = .False.
   !
   iOption   = 0
   iArgument = 1
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                     &
           &              .False.,errorC)
      If (cInput(1:1).Ne.'*') Then
         Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
         If (.Not. errorR) Call checkR4angle(command,rInput,                     &
              &           varsOffsets(:,iiSystemOffset)%point%x,                 &
              &           GV%angleFactorR,                                       &
              &           errorR)
      Endif
   Endif
   ERROR = ERROR .Or. errorR
   !
   iArgument = 2
   If (SIC_PRESENT(iOption,iArgument)) Then
      Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,                     &
           &              .False.,errorC)
      If (cInput(1:1).Ne.'*') Then
         Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
         If (.Not. errorR) Call checkR4angle(command,rInput,                     &
              &           varsOffsets(:,iiSystemOffset)%point%y,                 &
              &           GV%angleFactorR,                                       &
              &           errorR)
      Endif
   Endif
   ERROR = ERROR .Or. errorR
   !
Else If (.Not. doClear .And. SIC_NARG(0).Gt.0) Then
   ERROR = .True.
   messageText = "I don't know which /SYSTEM"
   Call  pakoMessage(priorityE,severityE,command,messageText)
End If
