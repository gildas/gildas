!
!  Id: parametersSegment.inc, v 1.1.2 2010-03-30 Hans Ungerechts
!
!D Write (6,*) "      --> parametersSegment.inc "
!
If      (vars(iIn)%doTrack       ) Then
   Include 'inc/parameters/parametersSegmentTrackOffsets.inc'
Else If (vars(iIn)%doLinearOtf   ) Then
   Include 'inc/parameters/parametersStartEnd.inc'
   Include 'inc/parameters/parametersLengthOtf.inc'
Else If (vars(iIn)%doLissajousOtf) Then
   Include 'inc/parameters/parametersLissajous.inc'
Else If (nArguments.Ge.1 .And. nArguments.Le.2) Then
   Include 'inc/parameters/parametersSegmentTrackOffsets.inc'
   vars(iIn)%doTrack         = .True.
   vars(iIn)%segType         =  seg%track 
Else If (nArguments.Ge.1 .And. nArguments.Le.4) Then
   Include 'inc/parameters/parametersStartEnd.inc'
   Include 'inc/parameters/parametersLengthOtf.inc'
   vars(iIn)%doLinearOtf     = .True.
   vars(iIn)%segType         =  seg%linear
Else If (nArguments.Ge.1 .And. nArguments.Le.8) Then
   Include 'inc/parameters/parametersLissajous.inc'
   vars(iIn)%doLissajousOtf  = .True.
   vars(iIn)%segType         =  seg%lissajous
End If
!
!D Write (6,*) "  vars(iIn)%segType: ", vars(iIn)%segType
