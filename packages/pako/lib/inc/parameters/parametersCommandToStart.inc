  !
  !     Id: parametersCommandToStart.inc,v 1.2.4 2015-06-08 Hans Ungerechts
  !     Id: parametersCommandToStart.inc,v 1.2.4 2015-06-04 Hans Ungerechts
  !     Id: parametersCommandToStart.inc,v 1.1   2005/03/23 
  !
  iArgument = 1
  !
  errorL = .False.
  errorR = .False.
  errorC = .False.
  !
  !D   Write (6,*) "  --> parametersCommandToStart.inc,v 1.2.4 2015-06-08"
  !
  If (SIC_NARG(0).Eq.1.) Then
     !
     iOption   = 0                                 
     iArgument = 1
     CALL SIC_CH(LINE,iOption,iArgument,                                         &
          &        cInput100,lengthInput,.TRUE.,errorC)                          !
     !
!!$     cInput100Upper = cInput100
!!$     CALL SIC_UPPER(cInput100Upper)
!!$     commandToStart  = cInput100Upper               
     !
     commandToStart  = cInput100
     !
     !D      Write (6,*) "      commandToStart:   ", commandToStart
     !
  Else  If (SIC_NARG(0).Gt.1.) Then
     !
     messageText =                                                               &
          &        "(only) 1 parameter allowed: commandToStart"                  !
     Call pakoMessage(priorityE,severityE,command,messageText)
     errorL = .True.
     !
  End If
  !
  ERROR = ERROR .Or. errorC .Or. errorL
  !


