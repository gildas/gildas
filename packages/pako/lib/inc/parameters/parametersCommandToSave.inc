!
!  Id: parametersCommandToSave.inc, v 0.91 2005-09-13 Hans Ungerechts
!
iArgument = 1
!
errorL = .False.
errorR = .False.
errorC = .False.
!
doCorrections  =  .False.
!
If (SIC_NARG(0).Ge.1) Then
   !
   iOption   = 0
   iArgument = 1
   Call SIC_CH(LINE,iOption,iArgument,                                           & 
        &        cInput100,lengthInput,.True.,errorC)
   !
   Call pakoUmatchKey (                                                          &
        &              keys=COMChoicesPako,                                      &
        &              key=cInput100,                                            &
        &              command='START',                                          &
        &              howto='Start Upper',                                      &
        &              iMatch=iMatch,                                            &
        &              nMatch=nMatch,                                            &
        &              error=errorC,                                             &
        &              errorCode=errorCode                                       &
        &             )
   !
   If (.Not. errorC) Then
      commandToSave = COMChoicesPako(iMatch)
   End If
   !
End If
!
If (SIC_NARG(0).Eq.2.) Then
   !
   If (commandToSave.Eq.COMpako%set .Or. commandToSave.Eq. COMpako%all) Then
      iOption   = 0
      iArgument = 2
      Call SIC_CH(LINE,iOption,iArgument,                                        & 
           &        cInput,lengthInput,.True.,errorC)
      Call SIC_UPPER(cInput)
      If (.Not. errorC .And. cInput(1:1).Eq."C") Then
         doCorrections  =  .True.
         messageText = "will save Corrections"
         Call pakoMessage(priorityI,severityI,command,messageText)
      End If
   End If
   !
End If
!
If (SIC_NARG(0).Gt.2.) Then
   !
   messageText =                                                                 & 
        &        "(only) 1 or 2 parameters allowed: commandToSave [corrections]"
   Call pakoMessage(priorityE,severityE,command,messageText)
   errorL = .True.
   !
End If
!
ERROR = ERROR .Or. errorC .Or. errorL
!
