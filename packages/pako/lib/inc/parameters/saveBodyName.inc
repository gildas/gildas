!
!     $Id$
!
      l = lenc(vars(iValue)%sourceName)
      lineOut = ' Body "'//vars(iValue)%sourceName(1:l)//'"'
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      lineOut(44:Len(lineOut)) = " ! Orbital Elements: "
!
      Write (iUnit,*) "!"
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
