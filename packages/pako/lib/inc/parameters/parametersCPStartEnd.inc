!
!     $Id$
!
!     parameters Contol Points start end %x %y
!
!     TBD: support angle units
!
      errorR = .False.
!
!D      write (6,*) "      --> parametersCPStartEnd.f90 "
!
      iOption   = 0
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%CPstart%x,                                         &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%CPstart%y,                                         &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
      iOption   = 0
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%CPend%x,                                           &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
      iArgument = iArgument+1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,          &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)          
            If (.Not. errorR) Call checkR4(command,rInput,               &
     &           vars%CPend%y,                                           &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorR
!
