!
!     $Id$
!
! TBD: check math
!
         xDelta = vars(iIn)%pEnd%x-vars(iIn)%pStart%x
         yDelta = vars(iIn)%pEnd%y-vars(iIn)%pStart%y
!     
!!  we need lambda = the argument of (xEnd-xStart)+i*(yEnd-yStart)
!!  in Mathematica this is ARcTan[xEnd-xStart,yEnd-yStart]
!!  but with the simple-minded Atan(y/x) we have to be careful:
!     
         If (xDelta.Gt.0.0.And.yDelta.Ge.0) Then
            lambda = Atan(yDelta/xDelta)
         End If
!     
         If (xDelta.Gt.0.0.And.yDelta.Lt.0) Then
            lambda = 2*Pi+Atan(yDelta/xDelta)
         End If
!     
         If (xDelta.Eq.0.0.And.yDelta.Gt.0) Then
            lambda = Pi/2
         End If
!     
         If (xDelta.Eq.0.0.And.yDelta.Lt.0) Then
            lambda = 3*Pi/2
         End If
!     
         If (xDelta.Lt.0.0) Then
            lambda = Pi+Atan(yDelta/xDelta)
         End If
!     
         xDistance = Sqrt((xDelta)**2+(yDelta)**2)
         xRadius   = (xDistance/2.0)/Sin(vars(iIn)%turnAngle/2.0*degs)
!
         vars(iIn)%lengthOtf =                                           &
     &        vars(iIn)%turnAngle/180.0*Pi*xRadius
!
