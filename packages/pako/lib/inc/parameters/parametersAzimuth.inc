!
!     $Id$
!
!     parameters: azimuth
!     Family:     1 real variable --> Angle, degrees 
!
      errorR = .False.
!
      iOption   = 0
      iArgument = 1
      If (SIC_PRESENT(iOption,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,        &
     &              .False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.True.,errorR)
            If (.Not. errorR) Call checkR4(command,rInput,             &
     &           vars%azimuth,                                         &
     &           errorR)
         vars(iIn)%doCurrentAzimuth = .False.
         Endif
      else
         vars(iIn)%doCurrentAzimuth = .True.
      Endif
      ERROR = ERROR .Or. errorR
!
