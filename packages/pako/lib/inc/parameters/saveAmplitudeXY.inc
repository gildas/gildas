  !
  !  Id: saveAmplitudeXY, v 1.1.12 2012-03-05 Hans Ungerechts
  !
  Write (lineOut,*) vars(iValue)%amplitude
  !
  Include 'inc/parameters/caseContinuationLine.inc'
  !
  Write (iUnit,*) lineOut(1:lenc(lineOut))
  !
