!
!     $Id$
!
!
      Write (c1,*) vars(iValue)%inclination
      l1 = lenc(c1) 
      lineOut = c1(1:l1)
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      lineOut(44:Len(lineOut)) = " ! Inclination [deg] "
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
