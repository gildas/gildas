!
!     Id: saveSetSecondayRotation.inc,v 1.0.1 2005-12-28 Hans Ungerechts
!
If (Abs(GV%secondaryRotation).Gt.0.01) Then
!
   Write (lineOut,*) '2ndRotation ', GV%secondaryRotation
   !
   Include 'inc/parameters/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
