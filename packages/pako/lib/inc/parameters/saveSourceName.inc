!
!     $Id$
!
      l = lenc(vars(iValue)%sourceName)
      lineOut = ' "'//vars(iValue)%sourceName(1:l)//'"'
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      Write (iUnit,*) "!"
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
