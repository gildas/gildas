!
!  Id: saveCatalogLine.inc,v 1.1.1 2009-04-16 Hans Ungerechts
!
If (GVlineCatalog(1) .Ne. GPnone) Then
   !
   l = lenc(GVlineCatalog(1))
   lineOut = ' Line "'//GVlineCatalog(1)(1:l)//'"'
   !
   Include 'inc/parameters/caseContinuationLine.inc'
   !
   Write (iUnit,*) lineOut(1:lenc(lineOut))
   !
End If
