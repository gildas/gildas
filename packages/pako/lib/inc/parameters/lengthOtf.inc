!
!     $Id$
!
! TBD: computation in case of circle and linear segments
!
         vars(iIn)%lengthOtf =                                           &
     &        ((vars(iIn)%pEnd%x-vars(iIn)%pStart%x)**2.0                &
     &        +(vars(iIn)%pEnd%y-vars(iIn)%pStart%y)**2.0)**0.5
!
