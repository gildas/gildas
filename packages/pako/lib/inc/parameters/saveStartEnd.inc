!
!     $Id$
!
      Write (lineOut,*)                                                  &
     &     vars(iValue)%pStart,                                          &
     &     vars(iValue)%pEnd,                                            &
     &     " -"
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
      lineOut = " /default"
!
      Include 'inc/parameters/caseContinuationLine.inc'
!
      Write (iUnit,*) lineOut(1:lenc(lineOut))
!
