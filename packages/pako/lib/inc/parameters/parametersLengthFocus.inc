!
!     $Id$
!
!     parameters: lengthFocus
!     Family:     1 real variable --> linear length
!     Cousins:    parametersTurnAngle.f
!     Cousins:    parametersLength.f
!
!     TBD: support linear units (for now: assumed to be mm)
!
      errorR = .False.
!
      If (.Not. errorNumber) Then
!
      iOption   = 0
      iArgument = 1
      If (SIC_PRESENT(0,iArgument)) Then
         Call SIC_CH(LINE,iOption,iArgument,cInput,lengthInput,.False.,errorC)
         If (cInput(1:1).Ne.'*') Then
            Call SIC_R4(LINE,0,iArgument,rInput,.False.,errorR)
            If (.Not. errorR) Call checkR4(command,rInput,           &
     &           vars%lengthFocus,                                   &
     &           errorR)
         Endif
      Endif
      ERROR = ERROR .Or. errorNumber .Or. errorR
!
      Endif
!

