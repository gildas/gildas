!
! Id: rangesSwFrequency.inc,v 1.1.0 2009-03-24 Hans Ungerechts
! Family:   rangesSw
! Siblings: rangesSwBeam.f90
!           rangesSwFrequency.f90
!           rangesSwTotalPower.f90
!           rangesSwWobbler.f90
!
vars(iDefault)%fOffsets(iA100,1)          =      -7.8/2.0       !    recommended
vars(iDefault)%fOffsets(iA100,2)          =       7.8/2.0       !    recommended
vars(iStd1)%fOffsets(iA100,:)             =      -9.0           
vars(iStd2)%fOffsets(iA100,:)             =       9.0           
vars(iLimit1)%fOffsets(iA100,:)           =      -9.0           !    limits     
vars(iLimit2)%fOffsets(iA100,:)           =       9.0           !    from old CS
!
vars(iDefault:iLimit2)%fOffsets(iB100,1)  =  vars(iDefault:iLimit2)%fOffsets(iA100,1)
vars(iDefault:iLimit2)%fOffsets(iB100,2)  =  vars(iDefault:iLimit2)%fOffsets(iA100,2)
!
vars(iDefault:iLimit2)%fOffsets(iA230,1)  =  vars(iDefault:iLimit2)%fOffsets(iA100,1)*3.0
vars(iDefault:iLimit2)%fOffsets(iA230,2)  =  vars(iDefault:iLimit2)%fOffsets(iA100,2)*3.0
!
vars(iDefault:iLimit2)%fOffsets(iB230,1)  =  vars(iDefault:iLimit2)%fOffsets(iA230,1)
vars(iDefault:iLimit2)%fOffsets(iB230,2)  =  vars(iDefault:iLimit2)%fOffsets(iA230,2)
!
vars(iDefault:iLimit2)%fOffsets(iC150,1)  =  vars(iDefault:iLimit2)%fOffsets(iA100,1)*2.0
vars(iDefault:iLimit2)%fOffsets(iC150,2)  =  vars(iDefault:iLimit2)%fOffsets(iA100,2)*2.0
!
vars(iDefault:iLimit2)%fOffsets(iD150,1)  =  vars(iDefault:iLimit2)%fOffsets(iC150,1)
vars(iDefault:iLimit2)%fOffsets(iD150,2)  =  vars(iDefault:iLimit2)%fOffsets(iC150,2)
!
vars(iDefault:iLimit2)%fOffsets(iC270,1)  =  vars(iDefault:iLimit2)%fOffsets(iA100,1)*3.0
vars(iDefault:iLimit2)%fOffsets(iC270,2)  =  vars(iDefault:iLimit2)%fOffsets(iA100,2)*3.0
!
vars(iDefault:iLimit2)%fOffsets(iD270,1)  =  vars(iDefault:iLimit2)%fOffsets(iC270,1)
vars(iDefault:iLimit2)%fOffsets(iD270,2)  =  vars(iDefault:iLimit2)%fOffsets(iC270,2)
!
vars(iDefault:iLimit2)%fOffsets(iHERA1,1) =  vars(iDefault:iLimit2)%fOffsets(iA100,1)*3.0
vars(iDefault:iLimit2)%fOffsets(iHERA1,2) =  vars(iDefault:iLimit2)%fOffsets(iA100,2)*3.0
!
vars(iDefault:iLimit2)%fOffsets(iHERA2,1) =  vars(iDefault:iLimit2)%fOffsets(iHERA1,1)
vars(iDefault:iLimit2)%fOffsets(iHERA2,2) =  vars(iDefault:iLimit2)%fOffsets(iHERA1,2)
!
vars(iDefault:iLimit2)%fOffsets(iHERA,1)  =  vars(iDefault:iLimit2)%fOffsets(iHERA1,1)
vars(iDefault:iLimit2)%fOffsets(iHERA,2)  =  vars(iDefault:iLimit2)%fOffsets(iHERA1,2)
!
vars(iDefault:iLimit2)%fOffsets(iBolo,1)  =  0.0
vars(iDefault:iLimit2)%fOffsets(iBolo,2)  =  0.0
!
!  *** EMIR
!
vars(iDefault)%fOffsets(iE090,1)          =      -7.8/2.0       !    recommended
vars(iDefault)%fOffsets(iE090,2)          =       7.8/2.0       !    recommended
vars(iStd1)%fOffsets(iE090,:)             =      -9.0           !!   TBD: check
vars(iStd2)%fOffsets(iE090,:)             =       9.0           !!   TBD: check
vars(iLimit1)%fOffsets(iE090,:)           =      -9.0           !    limits     
vars(iLimit2)%fOffsets(iE090,:)           =       9.0           !    from old CS
!
vars(iDefault:iLimit2)%fOffsets(iE150,1)  =  vars(iDefault:iLimit2)%fOffsets(iE090,1) * 2.0   !!  TBD: check
vars(iDefault:iLimit2)%fOffsets(iE150,2)  =  vars(iDefault:iLimit2)%fOffsets(iE090,2) * 2.0   !!  TBD: check
vars(iDefault:iLimit2)%fOffsets(iE230,1)  =  vars(iDefault:iLimit2)%fOffsets(iE090,1) * 3.0   !!  TBD: check
vars(iDefault:iLimit2)%fOffsets(iE230,2)  =  vars(iDefault:iLimit2)%fOffsets(iE090,2) * 3.0   !!  TBD: check
vars(iDefault:iLimit2)%fOffsets(iE300,1)  =  vars(iDefault:iLimit2)%fOffsets(iE090,1) * 3.0   !!  TBD: check
vars(iDefault:iLimit2)%fOffsets(iE300,2)  =  vars(iDefault:iLimit2)%fOffsets(iE090,2) * 3.0   !!  TBD: check
!
!     the following are used when option /RECEIVER is missing:
!
vars(iDefault:iLimit2)%fOffset(1)  =  vars(iDefault:iLimit2)%fOffsets(iE090,1)
vars(iDefault:iLimit2)%fOffset(2)  =  vars(iDefault:iLimit2)%fOffsets(iE090,2)
!                               
vars(iDefault)%functionName    = "STEP"
functionNameChoices(1)         = "STEP"
functionNameChoices(2)         = "RAMP"
!
vars(iDefault)%nPhases          =       2
vars(iStd1)%nPhases             =       2
vars(iStd2)%nPhases             =       2
vars(ILimit1)%nPhases           =       2
vars(iLimit2)%nPhases           =       4
!
vars(iDefault)%receiverName     = "A100"
!
!!$      receiverNameChoices(1)     = "A100"
!!$      receiverNameChoices(2)     = "A230"
!!$      receivernamechoices(3)     = "B100"
!!$      receiverNameChoices(4)     = "B230"
!!$      receiverNameChoices(5)     = "C150"
!!$      receiverNameChoices(6)     = "C270"
!!$      receiverNameChoices(7)     = "D150"
!!$      receiverNameChoices(8)     = "D270"
!!$      receiverNameChoices(9)     = "HERA"
!
vars(iDefault)%tBlanking        =     0.01
vars(iStd1)%tBlanking           =     0.01
vars(iStd2)%tBlanking           =     0.01
vars(ILimit1)%tBlanking         =     0.01
vars(iLimit2)%tBlanking         =     0.01
!
vars(iDefault)%tPhase           =     0.2
vars(iStd1)%tPhase              =     0.05
vars(iStd2)%tPhase              =     6.0
vars(ILimit1)%tPhase            =     0.05
vars(iLimit2)%tPhase            =     6.0
!
! ** above limits imposed by switching system (JP) / according to WB
!
vars(iDefault)%nCycles          =      1
vars(iStd1)%nCycles             =      1
vars(iStd2)%nCycles             =     10
vars(ILimit1)%nCycles           =      1
vars(iLimit2)%nCycles           =     20
!
vars(iDefault)%tRecord   =     vars(iDefault)%tPhase  *                          &
     &                         vars(iDefault)%nPhases * vars(iDefault)%nCycles  
vars(iStd1)%tRecord      =     vars(iStd1)%tPhase     *                          &
     &                         vars(iStd1)%nPhases    * vars(iStd1)%nCycles  
vars(iStd2)%tRecord      =     2
vars(ILimit1)%tRecord    =     vars(ILimit1)%tPhase   *                          &
     &                         vars(ILimit1)%nPhases  * vars(ILimit1)%nCycles
vars(iLimit2)%tRecord    =     2
!

