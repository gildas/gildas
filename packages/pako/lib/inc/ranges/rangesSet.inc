!
!  Id: rangesSet.inc ,v 1.2.6    2017-03-15 Hans Ungerechts
!  Id: rangesSet.inc ,v 1.2.3    2014-05-19 Hans Ungerechts // for holography (request by CK)
!  Id: rangesSet.inc ,v 1.2.3    2014-03-13 Hans Ungerechts
!  Id: rangesSet.inc ,v 1.0.10.4 2008-10-07 Hans Ungerechts
!
!! limits secondaryRotation according to meeting 2007-03-28
!
  Call pako_message(seve%t,programName,                                          &
       &  " modulePakoGlobalVariables ,v 1.2.6  2017-03-15   -> rangesSet.inc ") ! trace execution
  !
vars(iDefault)%transitionAcceleration    =   0.05    ! deg/s^2
vars(iStd1)%transitionAcceleration       =   0.01
vars(iStd2)%transitionAcceleration       =   vars(iDefault)%transitionAcceleration
vars(iLimit1)%transitionAcceleration     =   0.001
vars(iLimit2)%transitionAcceleration     =   vars(iDefault)%transitionAcceleration
!
vars(iDefault)%secondaryRotation    =     0.0
vars(iStd1)%secondaryRotation       =   -49.0
vars(iStd2)%secondaryRotation       =    49.0
vars(iLimit1)%secondaryRotation     =   -60.0
vars(iLimit2)%secondaryRotation     =    60.0
!
vars(iDefault)%azimuthCorrection    =     0.0
vars(iStd1)%azimuthCorrection       =   -30.0
vars(iStd2)%azimuthCorrection       =    30.0
vars(iLimit1)%azimuthCorrection     =  -3600.0                                   ! !! 2014-05-19 for holography (CK)
vars(iLimit2)%azimuthCorrection     =   3600.0                                   ! !! 2014-05-19 for holography (CK)
!! isStandardAzimuthCorrection          =   .True.
!
vars(iDefault)%elevationCorrection  =     0.0
vars(iStd1)%elevationCorrection     =   -30.0
vars(iStd2)%elevationCorrection     =    30.0
vars(iLimit1)%elevationCorrection   =  -3600.0                                   ! !! 2014-05-19 for holography (CK)
vars(iLimit2)%elevationCorrection   =   3600.0                                   ! !! 2014-05-19 for holography (CK)
!
vars(iDefault)%focusCorrection      =    -2.5
vars(iStd1)%focusCorrection         =    -3.0
vars(iStd2)%focusCorrection         =    -0.5
vars(iLimit1)%focusCorrection       =   -9.99
vars(iLimit2)%focusCorrection       =   +9.99
isSetFocusCorrectionZ               =   .True.
!! isStandardElevationCorrection         =   .True.
!
vars(iDefault)%focusCorrectionX     =     0.0
vars(iStd1)%focusCorrectionX        =    -4.0
vars(iStd2)%focusCorrectionX        =    +4.0
vars(iLimit1)%focusCorrectionX      =   -9.99
vars(iLimit2)%focusCorrectionX      =   +9.99
!! isSetFocusCorrectionX               =   .False.
!! isStandardFocusCorrectionX          =   .True.
!
vars(iDefault)%focusCorrectionY     =   vars(iDefault)%focusCorrectionX   
vars(iStd1)%focusCorrectionY        =   vars(iStd1)%focusCorrectionX      
vars(iStd2)%focusCorrectionY        =   vars(iStd2)%focusCorrectionX      
vars(iLimit1)%focusCorrectionY      =   vars(iLimit1)%focusCorrectionX    
vars(iLimit2)%focusCorrectionY      =   vars(iLimit2)%focusCorrectionX    
!! isSetFocusCorrectionY               =   .False.
!! isStandardFocusCorrectionY          =   .True.
!
vars(iDefault)%focusCorrectionEMIR  =    -2.5                                    ! !! 2017-03-13 standard depending on RX
vars(iStd1)%focusCorrectionEMIR     =    -3.0
vars(iStd2)%focusCorrectionEMIR     =    -0.5
vars(iLimit1)%focusCorrectionEMIR   =   -9.99
vars(iLimit2)%focusCorrectionEMIR   =   +9.99
!
vars(iDefault)%focusCorrectionNIKA  =     0.0                                    ! !! 2017-03-13 standard depending on RX
vars(iStd1)%focusCorrectionNIKA     =    -2.0
vars(iStd2)%focusCorrectionNIKA     =    +2.0
vars(iLimit1)%focusCorrectionNIKA   =   -9.99
vars(iLimit2)%focusCorrectionNIKA   =   +9.99
!
!
vars(iDefault)%slowRateAMD          =     1
vars(iStd1)%slowRateAMD             =     1
vars(iStd2)%slowRateAMD             =     8
vars(iLimit1)%slowRateAMD           =     1
vars(iLimit2)%slowRateAMD           =     8
!
directionFocusChoices(1)         = "X"
directionFocusChoices(2)         = "Y"
directionFocusChoices(3)         = "Z"
!
!D Write (6,*) "          GV%receiverName:   ", GV%receiverName
!D Write (6,*) "          GV%bolometerName:  ", GV%bolometerName
!
If (         GV%receiverName.Eq.rec%E090                                         &  !! 2017-03-13 standard depending on RX
     &  .Or. GV%receiverName.Eq.rec%E150                                         &
     &  .Or. GV%receiverName.Eq.rec%E230                                         &
     &  .Or. GV%receiverName.Eq.rec%E300                                         &
     &  .Or. GV%receiverName.Eq.rec%HERA                                         &
     &  .Or. GV%receiverName.Eq.rec%HERA1                                        &
     &  .Or. GV%receiverName.Eq.rec%HERA2   ) Then                               !
   !
   vars(iDefault)%focusCorrection = vars(iDefault)%focusCorrectionEMIR 
   vars(iStd1)%focusCorrection    = vars(iStd1)%focusCorrectionEMIR    
   vars(iStd2)%focusCorrection    = vars(iStd2)%focusCorrectionEMIR    
   vars(iLimit1)%focusCorrection  = vars(iLimit1)%focusCorrectionEMIR  
   vars(iLimit2)%focusCorrection  = vars(iLimit2)%focusCorrectionEMIR  
   !
End If
!
If (          GV%receiverName .Eq.rec%bolo                                       &  !! 2017-03-13 standard depending on RX
     &  .And. GV%bolometerName.Eq.bolo%NIKA  ) Then                              !
   !
   vars(iDefault)%focusCorrection = vars(iDefault)%focusCorrectionNIKA 
   vars(iStd1)%focusCorrection    = vars(iStd1)%focusCorrectionNIKA    
   vars(iStd2)%focusCorrection    = vars(iStd2)%focusCorrectionNIKA    
   vars(iLimit1)%focusCorrection  = vars(iLimit1)%focusCorrectionNIKA  
   vars(iLimit2)%focusCorrection  = vars(iLimit2)%focusCorrectionNIKA  
   !
End If
!
!D Write (6,*) " vars(iDefault)%focusCorrection ", vars(iDefault)%focusCorrection 
!D Write (6,*) " vars(iStd1)%focusCorrection    ", vars(iStd1)%focusCorrection    
!D Write (6,*) " vars(iStd2)%focusCorrection    ", vars(iStd2)%focusCorrection    
!D Write (6,*) " vars(iLimit1)%focusCorrection  ", vars(iLimit1)%focusCorrection  
!D Write (6,*) " vars(iLimit2)%focusCorrection  ", vars(iLimit2)%focusCorrection  
!D !
!D Write (6,*) "    <--   rangesSet "
!D Write (6,*) "  "
