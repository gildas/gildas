!
!     Id: rangesSwBeam.inc,v 1.0.5.2 2006-11-27 Hans Ungerechts
!     Family:   rangesSw
!     Siblings: rangesSwTotalPower.f90
!               rangesSwWobbler.f90
!               rangesSwBeam.f90
!
vars(iDefault)%nPhases          =       2
vars(iStd1)%nPhases             =       2
vars(iStd2)%nPhases             =       2
vars(ILimit1)%nPhases           =       2
vars(iLimit2)%nPhases           =       2
!
vars(iDefault)%tBlanking        =     0.1
vars(iStd1)%tBlanking           =     0.1
vars(iStd2)%tBlanking           =     0.1
vars(ILimit1)%tBlanking         =     0.1
vars(iLimit2)%tBlanking         =     0.1
!
vars(iDefault)%tPhase           =     swBeamTphase   !  see variablesSwBeam.inc 
vars(iStd1)%tPhase              =     swBeamTphase
vars(iStd2)%tPhase              =     swBeamTphase
vars(ILimit1)%tPhase            =     swBeamTphase
vars(iLimit2)%tPhase            =     swBeamTphase
!
vars(iDefault)%nCycles          =      1
vars(iStd1)%nCycles             =      1
vars(iStd2)%nCycles             =     10
vars(ILimit1)%nCycles           =      1
vars(iLimit2)%nCycles           =     10
!
vars(iDefault)%tRecord   =     vars(iDefault)%tPhase  *                          &
     &                         vars(iDefault)%nPhases * vars(iDefault)%nCycles  
vars(iStd1)%tRecord      =     vars(iStd1)%tPhase    *                           &
     &                         vars(iStd1)%nPhases   * vars(iStd1)%nCycles  
vars(iStd2)%tRecord      =     2
vars(ILimit1)%tRecord    =     vars(ILimit1)%tPhase  *                           &
     &                         vars(ILimit1)%nPhases * vars(ILimit1)%nCycles
vars(iLimit2)%tRecord    =     2
!
!D   Write (6,*) "       swBeamTphase: ", swBeamTphase
!D   Write (6,*) "       vars(iDefault)%tRecord: ", vars(iDefault)%tRecord
