  !
  !  Id: rangesFocus.inc,v 1.2.3 2014-09-24 Hans Ungerechts
  !  Id: rangesFocus.inc,v 1.0.5 2006-10-18 Hans Ungerechts
  !  Family:   ranges observing mode
  !            rangesOtfMap.f90
  !  Cousins:  rangesTip.f90
  !  Cousins:  rangesPointing.f90
  !
  vars(iDefault)%lengthFocus       =   2.0
  vars(iStd1)   %lengthFocus       =   0.6
  vars(iStd2)   %lengthFocus       =   3.0
  vars(iLimit1) %lengthFocus       =   0.1
  vars(iLimit2) %lengthFocus       =  10.0
  !
  vars(iDefault)%doCalibrate       = .False.
  !
  vars(iDefault)%directionFocus    = "Z"
  directionFocusChoices(1)         = "X"
  directionFocusChoices(2)         = "Y"
  directionFocusChoices(3)         = "Z"
  !
  vars(iDefault)%febe              =    1
  vars(iStd1)   %febe              =    1
  vars(iStd2)   %febe              =   10
  vars(iLimit1) %febe              =    1
  vars(iLimit2) %febe              =  100
  !
  vars(iDefault)%nSubscans         =    6
  vars(iStd1)   %nSubscans         =    3
  vars(iStd2)   %nSubscans         =    6
  vars(iLimit1) %nSubscans         =    3
  vars(iLimit2) %nSubscans         =   24
  !
  vars(iDefault)%doOtfFocus        = .False.
  !
  vars(iDefault)%tSubscan          =   10.0
  vars(iStd1)   %tSubscan          =    5.0
  vars(iStd2)   %tSubscan          =   30.0
  vars(iLimit1) %tSubscan          =    5.0
  vars(iLimit2) %tSubscan          =  360.0
  !
  vars(iDefault)%doUpdate          = .False.
  !
  vars(iDefault)%doTune            = .False.
  vars(iDefault)%doTuneSet         = .False.
  !
  vars(iDefault)%offsetTune%x      =       0.0
  vars(iStd1)   %offsetTune%x      =   -3600.0
  vars(iStd2)   %offsetTune%x      =   +3600.0
  vars(iLimit1) %offsetTune%x      =  -36000.0
  vars(iLimit2) %offsetTune%x      =  +36000.0
  !
  vars(iDefault)%offsetTune%y      =  vars(iDefault)%offsetTune%x
  vars(iStd1)   %offsetTune%y      =  vars(iStd1)   %offsetTune%x
  vars(iStd2)   %offsetTune%y      =  vars(iStd2)   %offsetTune%x
  vars(iLimit1) %offsetTune%y      =  vars(iLimit1) %offsetTune%x
  vars(iLimit2) %offsetTune%y      =  vars(iLimit2) %offsetTune%x
  !
  vars(iDefault)%tTune             =   10.0
  vars(iStd1)   %tTune             =    1.0
  vars(iStd2)   %tTune             =   15.0
  vars(iLimit1) %tTune             =    1.0
  vars(iLimit2) %tTune             = 3600.0
  !
