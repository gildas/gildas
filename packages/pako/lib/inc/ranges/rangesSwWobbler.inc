!
!     Id: rangesSwWobbler.inc,v 1.0.5 2006-10-19 Hans Ungerechts
!     Family:   rangesSw
!     Siblings: rangesSwBeam.f90
!               rangesSwFrequency.f90
!               rangesSwTotalPower.f90
!               rangesSwWobbler.f90
!
vars(iDefault)%nPhases          =       2
vars(iStd1)%nPhases             =       2
vars(iStd2)%nPhases             =       2
vars(ILimit1)%nPhases           =       2
vars(iLimit2)%nPhases           =       2
!
vars(iDefault)%wOffset(1)      =   -22.0
vars(iStd1)%wOffset(1)         =  -120.0
vars(iStd2)%wOffset(1)         =   -15.0
vars(ILimit1)%wOffset(1)       =  -120.0
vars(iLimit2)%wOffset(1)       =   -15.0
!                               
vars(iDefault)%wOffset(2)      =   +22.0
vars(iStd1)%wOffset(2)         =   +15.0
vars(iStd2)%wOffset(2)         =  +120.0
vars(ILimit1)%wOffset(2)       =   +15.0
vars(iLimit2)%wOffset(2)       =  +120.0
!                               
vars(iDefault)%wThrow          =    44.0
vars(iStd1)%wThrow             =    30.0
vars(iStd2)%wThrow             =   240.0
vars(ILimit1)%wThrow           =    30.0
vars(iLimit2)%wThrow           =   240.0
!
!!      vars(iDefault)%wAngle          =     0.0
!!      vars(iStd1)%wAngle             =     0.0
!!      vars(iStd2)%wAngle             =     0.0
!!      vars(ILimit1)%wAngle           =     0.0
!!      vars(iLimit2)%wAngle           =     0.0
!
vars(iDefault)%wAngle          =     0.0
vars(iStd1)%wAngle             =     0.0
vars(iStd2)%wAngle             =     0.0
vars(ILimit1)%wAngle           =   -90.0
vars(iLimit2)%wAngle           =    90.0
!
vars(iDefault)%tPhase          =     0.25
vars(iStd1)%tPhase             =     0.25
vars(iStd2)%tPhase             =     2.0
vars(ILimit1)%tPhase           =     0.25
vars(iLimit2)%tPhase           =     6.0
!
! ** Limits on throw & offsets accroding to AS 2005-11
! ** Limit 6.0 imposed by switching system (JP) / according to WB
! **
!
vars(iDefault)%nCycles          =      1
vars(iStd1)%nCycles             =      1
vars(iStd2)%nCycles             =     10
vars(ILimit1)%nCycles           =      1
vars(iLimit2)%nCycles           =     20
!
vars(iDefault)%tRecord   =     vars(iDefault)%tPhase  *                          &
     &                         vars(iDefault)%nPhases * vars(iDefault)%nCycles  
vars(iStd1)%tRecord      =     vars(iStd1)%tPhase    *                           &
     &                         vars(iStd1)%nPhases   * vars(iStd1)%nCycles  
vars(iStd2)%tRecord      =     2
vars(ILimit1)%tRecord    =     vars(ILimit1)%tPhase  *                           &
     &                         vars(ILimit1)%nPhases * vars(ILimit1)%nCycles
vars(iLimit2)%tRecord    =     vars(ILimit2)%tPhase  *                           &
     &                         vars(ILimit2)%nPhases * vars(ILimit2)%nCycles
!
