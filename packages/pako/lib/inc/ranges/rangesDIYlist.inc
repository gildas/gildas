!
! Id:       rangesDIYlist.inc,v 1.2.3  2013-11-06 Hans Ungerechts
! Id:       rangesDIYlist.inc,v 1.2.3  2013-04-29 Hans Ungerechts
! Id:       rangesDIYlist.inc,v 1.1.13 2012-10-18 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesDIYlist.inc
!           rangesOtfMap.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%offset%x           =       0.0
vars(iStd1)   %offset%x           =   -3600.0
vars(iStd2)   %offset%x           =   +3600.0
vars(iLimit1) %offset%x           =  -360000.0
vars(iLimit2) %offset%x           =  +360000.0
!
vars(iDefault)%offset%y           =       0.0
vars(iStd1)   %offset%y           =   -3600.0
vars(iStd2)   %offset%y           =   +3600.0
vars(iLimit1) %offset%y           =  -360000.0
vars(iLimit2) %offset%y           =  +360000.0
!
vars(iDefault)%tSubscan           =   30.0
vars(iStd1)   %tSubscan           =   10.0
vars(iStd2)   %tSubscan           =  600.0
vars(iLimit1) %tSubscan           =    1.0
vars(iLimit2) %tSubscan           = 3600.0
!
vars(iDefault)%pStart%x           =    -200.0
vars(iStd1)   %pStart%x           =   -3600.0
vars(iStd2)   %pStart%x           =   +3600.0
vars(iLimit1) %pStart%x           =  -36000.0
vars(iLimit2) %pStart%x           =  +36000.0
!
vars(iDefault)%pStart%y           =       0.0
vars(iStd1)   %pStart%y           =   -3600.0
vars(iStd2)   %pStart%y           =   +3600.0
vars(iLimit1) %pStart%y           =  -36000.0
vars(iLimit2) %pStart%y           =  +36000.0
!
vars(iDefault)%pEnd%x             =     200.0
vars(iStd1)   %pEnd%x             =   -3600.0
vars(iStd2)   %pEnd%x             =   +3600.0
vars(iLimit1) %pEnd%x             =  -36000.0
vars(iLimit2) %pEnd%x             =  +36000.0
!
vars(iDefault)%pEnd%y             =       0.0
vars(iStd1)   %pEnd%y             =   -3600.0
vars(iStd2)   %pEnd%y             =   +3600.0
vars(iLimit1) %pEnd%y             =  -36000.0
vars(iLimit2) %pEnd%y             =  +36000.0
!
vars(iDefault)%lengthOtf          =     400.0
!
vars(iDefault)%sourceName         =  GPnone
vars(iDefault)%altPosition        = "OFFSETS"
!
vars(iDefault)%systemName         = offs%pro
vars(iDefault)%iSystem            = ipro
!
vars(iDefault)%altOption          = "TOTF"
!
vars(iDefault)%tOtf               =   30.0
!
vars(iDefault)%speedStart         =       vars(iDefault)%lengthOtf/vars(iDefault)%tOtf
vars(iStd1)   %speedStart         =       0.0
vars(iStd2)   %speedStart         =     +60.0
vars(iLimit1) %speedStart         =       0.0
vars(iLimit2) %speedStart         =    +300.0
!
vars(iDefault)%speedEnd           =       vars(iDefault)%speedStart   
vars(iStd1)   %speedEnd           =       0.0
vars(iStd2)   %speedEnd           =     +60.0
vars(iLimit1) %speedEnd           =       0.0
vars(iLimit2) %speedEnd           =    +300.0
!
!*    vars(iDefault)%tOtf               =   30.0
vars(iStd1)   %tOtf               =   10.0
vars(iStd2)   %tOtf               =  600.0
vars(iLimit1) %tOtf               =    1.0
vars(iLimit2) %tOtf               = 3600.0
!
vars(iDefault)%pCenter%x          =       0.0
vars(iStd1)   %pCenter%x          =   -3600.0
vars(iStd2)   %pCenter%x          =   +3600.0
vars(iLimit1) %pCenter%x          =  -36000.0
vars(iLimit2) %pCenter%x          =  +36000.0
!
vars(iDefault)%pCenter%y          =  vars(iDefault)%pCenter%x
vars(iStd1)   %pCenter%y          =  vars(iStd1)   %pCenter%x
vars(iStd2)   %pCenter%y          =  vars(iStd2)   %pCenter%x
vars(iLimit1) %pCenter%y          =  vars(iLimit1) %pCenter%x
vars(iLimit2) %pCenter%y          =  vars(iLimit2) %pCenter%x
!
vars(iDefault)%xAmplitude         =     400.0
vars(iStd1)   %xAmplitude         =      20.0
vars(iStd2)   %xAmplitude         =   +3600.0
vars(iLimit1) %xAmplitude         =       0.0
vars(iLimit2) %xAmplitude         =  +36000.0
!
vars(iDefault)%yAmplitude         =  vars(iDefault)%xAmplitude
vars(iStd1)   %yAmplitude         =  vars(iStd1)   %xAmplitude
vars(iStd2)   %yAmplitude         =  vars(iStd2)   %xAmplitude
vars(iLimit1) %yAmplitude         =  vars(iLimit1) %xAmplitude
vars(iLimit2) %yAmplitude         =  vars(iLimit2) %xAmplitude
!
vars(iDefault)%frequencyX         =   0.08
vars(iStd1)   %frequencyX         =   0.01
vars(iStd2)   %frequencyX         =   0.18
vars(iLimit1) %frequencyX         =  -1.00
vars(iLimit2) %frequencyX         =   1.00
!
vars(iDefault)%frequencyY         =   vars(iDefault)%frequencyX
vars(iStd1)   %frequencyY         =   vars(iStd1)   %frequencyX
vars(iStd2)   %frequencyY         =   vars(iStd2)   %frequencyX
vars(iLimit1) %frequencyY         =   vars(iLimit1) %frequencyX
vars(iLimit2) %frequencyY         =   vars(iLimit2) %frequencyX
!
vars(iDefault)%omegaX             =   vars(iDefault)%frequencyX*2.0*Pi
vars(iStd1)   %omegaX             =   vars(iStd1)   %frequencyX*2.0*Pi
vars(iStd2)   %omegaX             =   vars(iStd2)   %frequencyX*2.0*Pi
vars(iLimit1) %omegaX             =   vars(iLimit1) %frequencyX*2.0*Pi
vars(iLimit2) %omegaX             =   vars(iLimit2) %frequencyX*2.0*Pi
!
vars(iDefault)%omegaY             =   vars(iDefault)%frequencyY*2.0*Pi
vars(iStd1)   %omegaY             =   vars(iStd1)   %frequencyY*2.0*Pi
vars(iStd2)   %omegaY             =   vars(iStd2)   %frequencyY*2.0*Pi
vars(iLimit1) %omegaY             =   vars(iLimit1) %frequencyY*2.0*Pi
vars(iLimit2) %omegaY             =   vars(iLimit2) %frequencyY*2.0*Pi
!
vars(iDefault)%phiX               =       0.0 
vars(iStd1)   %phiX               =    -2.0*Pi
vars(iStd2)   %phiX               =     2.0*Pi
vars(iLimit1) %phiX               =    -2.0*Pi
vars(iLimit2) %phiX               =     2.0*Pi
!
vars(iDefault)%phiY               =       0.0 
vars(iStd1)   %phiY               =    -2.0*Pi
vars(iStd2)   %phiY               =     2.0*Pi
vars(iLimit1) %phiY               =    -2.0*Pi
vars(iLimit2) %phiY               =     2.0*Pi
!
vars(iDefault)%croFlag            =    "O"
!
vars(iDefault)%doRampUp           = .True.
vars(iDefault)%doRampDown         = .False.
!
vars(iDefault)%tRampUp            =    5.0
vars(iStd1)   %tRampUp            =    5.0
vars(iStd2)   %tRampUp            =   10.0
vars(iLimit1) %tRampUp            =    1.0
vars(iLimit2) %tRampUp            =   60.0
!
vars(iDefault)%tRampDown          =    5.0
vars(iStd1)   %tRampDown          =    5.0
vars(iStd2)   %tRampDown          =   10.0
vars(iLimit1) %tRampDown          =    1.0
vars(iLimit2) %tRampDown          =   60.0
!
doClear                           = .False.
!
