!
!     Id: rangesVlbi.inc,v 1.0.5 2006-10-18 Hans Ungerechts
!     Family:   rangesOnOff.inc
!     Siblings: rangesOnOff.inc
!               rangesTrack.inc
!               rangesVlbi.inc
!
vars(iDefault)%offset%x           =       0.0
vars(iStd1)   %offset%x           =   -3600.0
vars(iStd2)   %offset%x           =   +3600.0
vars(iLimit1) %offset%x           =  -36000.0
vars(iLimit2) %offset%x           =  +36000.0
!
vars(iDefault)%offset%y           =       0.0
vars(iStd1)   %offset%y           =   -3600.0
vars(iStd2)   %offset%y           =   +3600.0
vars(iLimit1) %offset%y           =  -36000.0
vars(iLimit2) %offset%y           =  +36000.0
!
vars(iDefault)%nSubscans          =    1
vars(iStd1)   %nSubscans          =    1
vars(iStd2)   %nSubscans          =  100
vars(iLimit1) %nSubscans          =    1
vars(iLimit2) %nSubscans          =  200
!
vars(iDefault)%systemName         = offs%tru
!
vars(iDefault)%tSubscan           = 3601.1
vars(iStd1)   %tSubscan           =   10.0
vars(iStd2)   %tSubscan           =  600.0
vars(iLimit1) %tSubscan           =   10.0
vars(iLimit2) %tSubscan           = 3601.1
!
