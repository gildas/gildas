!
! Id:       rangesOtfMap.inc,v 1.2.4  2015-04-28 Hans Ungerechts
!           mods from     v 1.1.15.5 2015-02-05 Hans Ungerechts
! Id:       rangesOtfMap.inc,v 1.2.3  2014-08-28 Hans Ungerechts
! Id:       rangesOtfMap.inc,v 1.2.3  2014-07-10 Hans Ungerechts
! Id:       rangesOtfMap.inc,v 1.1.12 2012-02-24 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesOnOff.inc
!           rangesOtfMap.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%pStart%x           =    -200.0
vars(iStd1)   %pStart%x           =   -3600.0
vars(iStd2)   %pStart%x           =   +3600.0
vars(iLimit1) %pStart%x           =  -36000.0
vars(iLimit2) %pStart%x           =  +36000.0
!
vars(iDefault)%pStart%y           =     -10.0
vars(iStd1)   %pStart%y           =   -3600.0
vars(iStd2)   %pStart%y           =   +3600.0
vars(iLimit1) %pStart%y           =  -36000.0
vars(iLimit2) %pStart%y           =  +36000.0
!
vars(iDefault)%pEnd%x             =    +200.0
vars(iStd1)   %pEnd%x             =   -3600.0
vars(iStd2)   %pEnd%x             =   +3600.0
vars(iLimit1) %pEnd%x             =  -36000.0
vars(iLimit2) %pEnd%x             =  +36000.0
!
vars(iDefault)%pEnd%y             =     -10.0
vars(iStd1)   %pEnd%y             =   -3600.0
vars(iStd2)   %pEnd%y             =   +3600.0
vars(iLimit1) %pEnd%y             =  -36000.0
vars(iLimit2) %pEnd%y             =  +36000.0
!
vars(iDefault)%lengthOtf          =     400.0
!
vars(iDefault)%doBalance          = .False.
!
vars(iDefault)%croCode            = "ROR"
vars(iDefault)%croCodeBites       = "ROR"
vars(iDefault)%croCodeCount       =    3
vars(iStd1)   %croCodeCount       =    1
vars(iStd2)   %croCodeCount       =  100
vars(iLimit1) %croCodeCount       =    1
vars(iLimit2) %croCodeCount       =  100
!
vars(iDefault)%nOtf               =    3
vars(iStd1)   %nOtf               =    1
vars(iStd2)   %nOtf               =  100
vars(iLimit1) %nOtf               =    1
vars(iLimit2) %nOtf               =  300
!
vars(iDefault)%doReference        = .True.
vars(iDefault)%offsetR%x          =    -600.0
vars(iStd1)   %offsetR%x          =   -3600.0
vars(iStd2)   %offsetR%x          =   +3600.0
vars(iLimit1) %offsetR%x          =  -36000.0
vars(iLimit2) %offsetR%x          =  +36000.0
vars(iDefault)%offsetR%y          =       0.0
vars(iStd1)   %offsetR%y          =   -3600.0
vars(iStd2)   %offsetR%y          =   +3600.0
vars(iLimit1) %offsetR%y          =  -36000.0
vars(iLimit2) %offsetR%y          =  +36000.0
vars(iDefault)%systemNameRef      = offs%pro
vars(iDefault)%listOfNamesRef     = ""
vars(iDefault)%altRef             = "OFFSETS"
!
vars(iDefault)%doResume           = .False.
vars(iDefault)%nResume            =       1
vars(iStd1)   %nResume            =       1
vars(iStd2)   %nResume            =     100
vars(iLimit1) %nResume            =       1
vars(iLimit2) %nResume            =     200
vars(iDefault)%doResumeCalibrate  = .False.
vars(iDefault)%doResumeReference  = .False.
!
vars(iDefault)%delta%x            =       0.0
vars(iStd1)   %delta%x            =   -3600.0
vars(iStd2)   %delta%x            =   +3600.0
vars(iLimit1) %delta%x            =  -36000.0
vars(iLimit2) %delta%x            =  +36000.0
!
vars(iDefault)%delta%y            =      10.0
vars(iStd1)   %delta%y            =   -3600.0
vars(iStd2)   %delta%y            =   +3600.0
vars(iLimit1) %delta%y            =  -36000.0
vars(iLimit2) %delta%y            =  +36000.0
!
vars(iDefault)%systemName         = offs%pro
vars(iDefault)%iSystem            = ipro
!
vars(iDefault)%altOption          = "TOTF"
!
vars(iDefault)%tOtf               =   30.0
!
vars(iDefault)%speedStart         =       vars(iDefault)%lengthOtf/vars(iDefault)%tOtf
vars(iStd1)   %speedStart         =       0.0
vars(iStd2)   %speedStart         =     +60.0
vars(iLimit1) %speedStart         =       0.0
vars(iLimit2) %speedStart         =    +300.0
!
vars(iDefault)%speedEnd           =       vars(iDefault)%speedStart   
vars(iStd1)   %speedEnd           =       0.0
vars(iStd2)   %speedEnd           =     +60.0
vars(iLimit1) %speedEnd           =       0.0
vars(iLimit2) %speedEnd           =    +300.0
!
!*    vars(iDefault)%tOtf               =   30.0
vars(iStd1)   %tOtf               =   10.0
vars(iStd2)   %tOtf               =  600.0
vars(iLimit1) %tOtf               =    3.0
vars(iLimit2) %tOtf               = 3600.0
!
vars(iDefault)%tRecord            =    0.1
vars(iStd1)   %tRecord            =    0.1
vars(iStd2)   %tRecord            =    1.0
vars(iLimit1) %tRecord            =    0.05
vars(iLimit2) %tRecord            =   60.0
!
vars(iDefault)%tReference         =    10.0
vars(iStd1)   %tReference         =    10.0
vars(iStd2)   %tReference         =    60.0
vars(iLimit1) %tReference         =     3.0
vars(iLimit2) %tReference         =   120.0
!
vars(iDefault)%doZigzag           = .True.
!
vars(iDefault)%doTune             = .False.
vars(iDefault)%doTuneSet          = .False.
!
vars(iDefault)%offsetTune%x       =       0.0
vars(iStd1)   %offsetTune%x       =   -3600.0
vars(iStd2)   %offsetTune%x       =   +3600.0
vars(iLimit1) %offsetTune%x       =  -36000.0
vars(iLimit2) %offsetTune%x       =  +36000.0
!
vars(iDefault)%offsetTune%y       =  vars(iDefault)%offsetTune%x
vars(iStd1)   %offsetTune%y       =  vars(iStd1)   %offsetTune%x
vars(iStd2)   %offsetTune%y       =  vars(iStd2)   %offsetTune%x
vars(iLimit1) %offsetTune%y       =  vars(iLimit1) %offsetTune%x
vars(iLimit2) %offsetTune%y       =  vars(iLimit2) %offsetTune%x
!
vars(iDefault)%tTune              =   10.0
vars(iStd1)   %tTune              =    1.0
vars(iStd2)   %tTune              =   15.0
vars(iLimit1) %tTune              =    1.0
vars(iLimit2) %tTune              = 3600.0
!
! ***
!
croCodeChoices(1)       = "C"
croCodeChoices(2)       = "R"
croCodeChoices(3)       = "O"
!
! deprecated:
!
systemNameChoices(1)    = "PROJECTION"
systemNameChoices(2)    = "DESCRIPTIVE"
systemNameChoices(3)    = "BASIS"
systemNameChoices(4)    = "EQUATORIAL"
systemNameChoices(5)    = "HA/DECL"
systemNameChoices(6)    = "HORIZONTAL"
systemNameChoices(7)    = "NASMYTH"
!
systemNameRefChoices(1) = "PROJECTION"
systemNameRefChoices(2) = "DESCRIPTIVE"
systemNameRefChoices(3) = "BASIS"
systemNameRefChoices(4) = "EQUATORIAL"
systemNameRefChoices(5) = "HA/DECL"
systemNameRefChoices(6) = "HORIZONTAL"
systemNameRefChoices(7) = "NASMYTH"

