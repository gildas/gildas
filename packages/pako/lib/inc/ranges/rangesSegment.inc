!     $Id$
!     Family:   ranges observing mode
!               rangesSegment.f90
!     Cousins:  rangesTip.f90
!     Cousins:  rangesPointing.f90
!
      vars(iDefault)%scanNumber         =  1
      vars(iDefault)%ssNumber           =  1
      vars(iDefault)%segNumber          =  1
!
      vars(iDefault)%ssType             = ss%otf
      vars(iDefault)%segType            = seg%linear
!
      vars(iDefault)%angleUnit          = au%arcsec
      vars(iDefault)%speedUnit          = su%arcsec
!
      vars(iDefault)%flagOn             = .True.
      vars(iDefault)%flagRef            = .False.
!
      vars(iDefault)%pStart%x           =    -300.0
      vars(iStd1)   %pStart%x           =   -3600.0
      vars(iStd2)   %pStart%x           =   +3600.0
      vars(iLimit1) %pStart%x           =  -36000.0
      vars(iLimit2) %pStart%x           =  +36000.0
!
      vars(iDefault)%pStart%y           =       0.0
      vars(iStd1)   %pStart%y           =   -3600.0
      vars(iStd2)   %pStart%y           =   +3600.0
      vars(iLimit1) %pStart%y           =  -36000.0
      vars(iLimit2) %pStart%y           =  +36000.0
!
      vars(iDefault)%pEnd%x             =    +300.0
      vars(iStd1)   %pEnd%x             =   -3600.0
      vars(iStd2)   %pEnd%x             =   +3600.0
      vars(iLimit1) %pEnd%x             =  -36000.0
      vars(iLimit2) %pEnd%x             =  +36000.0
!
      vars(iDefault)%pEnd%y             =       0.0
      vars(iStd1)   %pEnd%y             =   -3600.0
      vars(iStd2)   %pEnd%y             =   +3600.0
      vars(iLimit1) %pEnd%y             =  -36000.0
      vars(iLimit2) %pEnd%y             =  +36000.0
!
      vars(iDefault)%lengthOtf          =     600.0
!
      vars(iDefault)%turnAngle          =       0.0
      vars(iStd1)   %turnAngle          =    -360.0
      vars(iStd2)   %turnAngle          =    +360.0
      vars(iLimit1) %turnAngle          =    -360.0
      vars(iLimit2) %turnAngle          =    +360.0
!
      vars(iDefault)%CPstart%x           =       0.0
      vars(iStd1)   %CPstart%x           =   -3600.0
      vars(iStd2)   %CPstart%x           =   +3600.0
      vars(iLimit1) %CPstart%x           =  -36000.0
      vars(iLimit2) %CPstart%x           =  +36000.0
!
      vars(iDefault)%CPstart%y           =       0.0
      vars(iStd1)   %CPstart%y           =   -3600.0
      vars(iStd2)   %CPstart%y           =   +3600.0
      vars(iLimit1) %CPstart%y           =  -36000.0
      vars(iLimit2) %CPsTart%y           =  +36000.0
!
      vars(iDefault)%CPend%x             =       0.0
      vars(iStd1)   %CPend%x             =   -3600.0
      vars(iStd2)   %CPend%x             =   +3600.0
      vars(iLimit1) %CPend%x             =  -36000.0
      vars(iLimit2) %CPend%x             =  +36000.0
!
      vars(iDefault)%CPend%y             =       0.0
      vars(iStd1)   %CPend%y             =   -3600.0
      vars(iStd2)   %CPend%y             =   +3600.0
      vars(iLimit1) %CPend%y             =  -36000.0
      vars(iLimit2) %CPend%y             =  +36000.0
!
      vars(iDefault)%altOption          = "TSEGMENT"
!
      vars(iDefault)%tSegment               =   30.0
!
      vars(iDefault)%speedStart         =       vars(iDefault)%lengthOtf/vars(iDefault)%tSegment
      vars(iStd1)   %speedStart         =       0.0
      vars(iStd2)   %speedStart         =     +60.0
      vars(iLimit1) %speedStart         =       0.0
      vars(iLimit2) %speedStart         =    +600.0
!
      vars(iDefault)%speedEnd           =       vars(iDefault)%speedStart   
      vars(iStd1)   %speedEnd           =       0.0
      vars(iStd2)   %speedEnd           =     +60.0
      vars(iLimit1) %speedEnd           =       0.0
      vars(iLimit2) %speedEnd           =    +600.0
!
!*    vars(iDefault)%tSegment               =   30.0
      vars(iStd1)   %tSegment               =   10.0
      vars(iStd2)   %tSegment               =  600.0
      vars(iLimit1) %tSegment               =    1.0
      vars(iLimit2) %tSegment               = 3600.0
!
      varsEtc(iDefault)%doWriteToSeg    =  .False.
      varsEtc(iDefault)%doneWriteToSeg  =  .False.
!
! ***
!
