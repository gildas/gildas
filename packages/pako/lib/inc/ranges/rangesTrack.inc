!
! Id:       rangesTrack.inc,v 1.1.12 2012-02-23 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesOnOff.inc
!           rangesOtfMap.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%offset%x           =       0.0
vars(iStd1)   %offset%x           =   -3600.0
vars(iStd2)   %offset%x           =   +3600.0
vars(iLimit1) %offset%x           =  -36000.0
vars(iLimit2) %offset%x           =  +36000.0
!
vars(iDefault)%offset%y           =       0.0
vars(iStd1)   %offset%y           =   -3600.0
vars(iStd2)   %offset%y           =   +3600.0
vars(iLimit1) %offset%y           =  -36000.0
vars(iLimit2) %offset%y           =  +36000.0
!
vars(iDefault)%sourceName         =  GPnone
vars(iDefault)%altPosition        = "OFFSETS"
!
vars(iDefault)%nSubscans          =    1
vars(iStd1)   %nSubscans          =    1
vars(iStd2)   %nSubscans          =   10
vars(iLimit1) %nSubscans          =    1
vars(iLimit2) %nSubscans          =   99
!
vars(iDefault)%systemName         = offs%pro
vars(iDefault)%iSystem            = ipro
!
vars(iDefault)%tSubscan           =   60.0
vars(iStd1)   %tSubscan           =   10.0
vars(iStd2)   %tSubscan           =  600.0
vars(iLimit1) %tSubscan           =   10.0
vars(iLimit2) %tSubscan           = 3600.0
!
vars(iDefault)%tRecord            =    0.1
vars(iStd1)   %tRecord            =    0.1
vars(iStd2)   %tRecord            =    1.0
vars(iLimit1) %tRecord            =    0.05
vars(iLimit2) %tRecord            =   60.0
!
