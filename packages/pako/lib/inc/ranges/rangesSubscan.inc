!     $Id$
!     Family:   ranges observing mode
!               rangesSubscan.f90
!     Cousins:  rangesTip.f90
!     Cousins:  rangesPointing.f90
!
      vars(iDefault)%doInitialize       = .False.
      vars(iDefault)%doneInitialize     = .False.
!
      vars(iDefault)%systemName         = "PROJECTION"
!
      vars(iDefault)%tRecord            =    0.1
      vars(iStd1)   %tRecord            =    0.1
      vars(iStd2)   %tRecord            =    1.0
      vars(iLimit1) %tRecord            =    0.05
      vars(iLimit2) %tRecord            =   60.0
!
      systemNameChoices(1)    = "PROJECTION"
      systemNameChoices(2)    = "DESCRIPTIVE"
      systemNameChoices(3)    = "BASIS"
      systemNameChoices(4)    = "EQUATORIAL"
      systemNameChoices(5)    = "HA/DECL"
      systemNameChoices(6)    = "HORIZONTAL"
      systemNameChoices(7)    = "NASMYTH"
!
      vars(iDefault)%doWriteToSub       = .False.
      vars(iDefault)%doneWriteToSub     = .False.
!




