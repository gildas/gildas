!
! Id: inc/ranges/checkLimitsAngle.inc, v 1.0 Hans Ungerechts
!
ERROR = .False.
!
vStdMin   = Min(variable(iStd1),  variable(iStd2))
vStdMax   = Max(variable(iStd1),  variable(iStd2))     
vLimitMin = Min(variable(iLimit1),variable(iLimit2))
vLimitMax = Max(variable(iLimit1),variable(iLimit2))
!
If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
   error = .True.
   Write (messageText,*) 'value ',                         vIn/angleFactor,  &
        &                ' outside limits ', variable(iLimit1)/angleFactor,  &
        &                ' to ',             variable(iLimit2)/angleFactor
   Call PakoMessage(priorityE,severityE,command//"/"//option,messageText)
Else
   variable(iIn) = vIn
   If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
      Write (messageText,*) 'value ',                   vIn            /angleFactor,  &
           &                ' outside standard range ', variable(iStd1)/angleFactor,  &
           &                ' to ',                     variable(iStd2)/angleFactor
      Call PakoMessage(priorityW,severityW,command//"/"//option,messageText)
   Endif
Endif
!
