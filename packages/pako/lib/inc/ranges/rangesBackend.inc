!
! Id: rangesBackend.inc,v 1.3.0 2017-08-29 Hans Ungerechts
! Id: rangesBackend.inc,v 1.1.6 2011-07-23 Hans Ungerechts
!
!     2009-03-31 HU: update fRangeSky based on discussion with Salvador SS
!
! *** ranges for backends ***
!
vars(iDefault,iContinuum)%name            =   bac%Continuum
!
vars(iDefault,iContinuum)%wasConnected    =   .True.
vars(iDefault,iContinuum)%isConnected     =   .True.
!
vars(iDefault,iContinuum)%nPart           =     1
vars(iStd1   ,iContinuum)%nPart           =     1
vars(iStd2   ,iContinuum)%nPart           =     nDimContinuum
vars(ILimit1 ,iContinuum)%nPart           =     1
vars(iLimit2 ,iContinuum)%nPart           =     nDimContinuum
!                               
vars(iDefault,iContinuum)%resolution      =   1000.0
vars(iStd1   ,iContinuum)%resolution      =   1000.0
vars(iStd2   ,iContinuum)%resolution      =   1000.0
vars(ILimit1 ,iContinuum)%resolution      =   1000.0
vars(iLimit2 ,iContinuum)%resolution      =   1000.0
!                               
vars(iDefault,iContinuum)%bandwidth       =   1000.0
vars(iStd1   ,iContinuum)%bandwidth       =   1000.0
vars(iStd2   ,iContinuum)%bandwidth       =   1000.0
vars(ILimit1 ,iContinuum)%bandwidth       =   1000.0
vars(iLimit2 ,iContinuum)%bandwidth       =   1000.0
!                               
vars(iDefault,iContinuum)%fShift          =     0.0
vars(iStd1   ,iContinuum)%fShift          =  -250.0
vars(iStd2   ,iContinuum)%fShift          =   250.0
vars(ILimit1 ,iContinuum)%fShift          =  -250.0
vars(iLimit2 ,iContinuum)%fShift          =   250.0
!                               
vars(iDefault,iContinuum)%percentage      =   100.0
vars(iStd1   ,iContinuum)%percentage      =   100.0
vars(iStd2   ,iContinuum)%percentage      =   100.0
vars(ILimit1 ,iContinuum)%percentage      =   100.0
vars(iLimit2 ,iContinuum)%percentage      =   100.0
!                               
vars(iDefault,iContinuum)%percentageIsSet =  GPnoneL
!
vars(iDefault,iContinuum)%mode            =  GPnone
!
vars(iDefault,iContinuum)%receiverName    =  "A100"
vars(iDefault,iContinuum)%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iBBC)%name            =  "BBC"
!
vars(iDefault,iBBC)%wasConnected    =   .True.
vars(iDefault,iBBC)%isConnected     =   .True.
!
vars(iDefault,iBBC)%nPart           =     1
vars(iStd1   ,iBBC)%nPart           =     1
vars(iStd2   ,iBBC)%nPart           =     nDimBBC
vars(ILimit1 ,iBBC)%nPart           =     1
vars(iLimit2 ,iBBC)%nPart           =     nDimBBC
!                               
vars(iDefault,iBBC)%resolution      =   8000.0
vars(iStd1   ,iBBC)%resolution      =   4000.0
vars(iStd2   ,iBBC)%resolution      =   8000.0
vars(ILimit1 ,iBBC)%resolution      =   4000.0
vars(iLimit2 ,iBBC)%resolution      =   8000.0
!                               
vars(iDefault,iBBC)%bandwidth       =   8000.0
vars(iStd1   ,iBBC)%bandwidth       =   4000.0
vars(iStd2   ,iBBC)%bandwidth       =   8000.0
vars(ILimit1 ,iBBC)%bandwidth       =   4000.0
vars(iLimit2 ,iBBC)%bandwidth       =   8000.0
!                               
vars(iDefault,iBBC)%fShift          =     0.0
vars(iStd1   ,iBBC)%fShift          =  -250.0
vars(iStd2   ,iBBC)%fShift          =   250.0
vars(ILimit1 ,iBBC)%fShift          =  -250.0
vars(iLimit2 ,iBBC)%fShift          =   250.0
!                               
vars(iDefault,iBBC)%percentage      =   100.0
vars(iStd1   ,iBBC)%percentage      =   100.0
vars(iStd2   ,iBBC)%percentage      =   100.0
vars(ILimit1 ,iBBC)%percentage      =   100.0
vars(iLimit2 ,iBBC)%percentage      =   100.0
!                               
vars(iDefault,iBBC)%percentageIsSet =  GPnoneL
!
vars(iDefault,iBBC)%nSamples        =   0
vars(iStd1   ,iBBC)%nSamples        =   4
vars(iStd2   ,iBBC)%nSamples        =   10000
vars(ILimit1 ,iBBC)%nSamples        =   0
vars(iLimit2 ,iBBC)%nSamples        =   25000
!                               
vars(iDefault,iBBC)%samplesIsSet    =  GPnoneL
!
vars(iDefault,iBBC)%mode            =  GPnone
!
vars(iDefault,iBBC)%receiverName    =  "E090"
vars(iDefault,iBBC)%receiverName2   =  GPnone
!
!
!     ******************************************************
!
vars(iDefault,iFB4MHz    )%name            =     "4MHz"
!                        
vars(iDefault,iFB4MHz    )%wasConnected    =     .True.
vars(iDefault,iFB4MHz    )%isConnected     =     .True.
!                        
vars(iDefault,iFB4MHz    )%nPart           =     1
vars(iStd1   ,iFB4MHz    )%nPart           =     1
vars(iStd2   ,iFB4MHz    )%nPart           =     nDim4MHz
vars(ILimit1 ,iFB4MHz    )%nPart           =     1
vars(iLimit2 ,iFB4MHz    )%nPart           =     nDim4MHz
!                          
vars(iDefault,iFB4MHz    )%resolution      =     4.0
vars(iStd1   ,iFB4MHz    )%resolution      =     4.0
vars(iStd2   ,iFB4MHz    )%resolution      =     4.0
vars(ILimit1 ,iFB4MHz    )%resolution      =     4.0
vars(iLimit2 ,iFB4MHz    )%resolution      =     4.0
!                          
vars(iDefault,iFB4MHz    )%bandwidth       =  1024.0
vars(iStd1   ,iFB4MHz    )%bandwidth       =  1024.0
vars(iStd2   ,iFB4MHz    )%bandwidth       =  8192.0
vars(ILimit1 ,iFB4MHz    )%bandwidth       =  1024.0
vars(iLimit2 ,iFB4MHz    )%bandwidth       =  8192.0
!                          
vars(iDefault,iFB4MHz    )%fShift          =     0.0
vars(iStd1   ,iFB4MHz    )%fShift          =  -250.0
vars(iStd2   ,iFB4MHz    )%fShift          =   250.0
vars(ILimit1 ,iFB4MHz    )%fShift          =  -250.0
vars(iLimit2 ,iFB4MHz    )%fShift          =   250.0
!                          
vars(iDefault,iFB4MHz    )%percentage      =   100.0
vars(iStd1   ,iFB4MHz    )%percentage      =   100.0
vars(iStd2   ,iFB4MHz    )%percentage      =   100.0
vars(ILimit1 ,iFB4MHz    )%percentage      =   100.0
vars(iLimit2 ,iFB4MHz    )%percentage      =   100.0
!                                
vars(iDefault,iFB4MHz    )%percentageIsSet =  GPnoneL
!
vars(iDefault,iFB4MHz    )%mode            =  GPnone
!
vars(iDefault,iFB4MHz    )%receiverName    =  "E090"
vars(iDefault,iFB4MHz    )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iFB1MHz    )%name            =     "1MHz"
!                       
vars(iDefault,iFB1MHz    )%wasConnected    =     .True.
vars(iDefault,iFB1MHz    )%isConnected     =     .True.
!                       
vars(iDefault,iFB1MHz    )%nPart           =     1
vars(iStd1   ,iFB1MHz    )%nPart           =     1
vars(iStd2   ,iFB1MHz    )%nPart           =     nDim1MHz
vars(ILimit1 ,iFB1MHz    )%nPart           =     1
vars(iLimit2 ,iFB1MHz    )%nPart           =     nDim1MHz
!                            
vars(iDefault,iFB1MHz    )%resolution      =     1.0
vars(iStd1   ,iFB1MHz    )%resolution      =     1.0
vars(iStd2   ,iFB1MHz    )%resolution      =     1.0
vars(ILimit1 ,iFB1MHz    )%resolution      =     1.0
vars(iLimit2 ,iFB1MHz    )%resolution      =     1.0
!                            
vars(iDefault,iFB1MHz    )%bandwidth       =   256.0
vars(iStd1   ,iFB1MHz    )%bandwidth       =   256.0
vars(iStd2   ,iFB1MHz    )%bandwidth       =  1024.0
vars(ILimit1 ,iFB1MHz    )%bandwidth       =   256.0
vars(iLimit2 ,iFB1MHz    )%bandwidth       =  1024.0
!                            
vars(iDefault,iFB1MHz    )%fShift          =     0.0
vars(iStd1   ,iFB1MHz    )%fShift          =  -512.0
vars(iStd2   ,iFB1MHz    )%fShift          =   512.0
vars(ILimit1 ,iFB1MHz    )%fShift          =  -512.0
vars(iLimit2 ,iFB1MHz    )%fShift          =   512.0
!                            
vars(iDefault,iFB1MHz    )%percentage      =   100.0
vars(iStd1   ,iFB1MHz    )%percentage      =   100.0
vars(iStd2   ,iFB1MHz    )%percentage      =   100.0
vars(ILimit1 ,iFB1MHz    )%percentage      =   100.0
vars(iLimit2 ,iFB1MHz    )%percentage      =   100.0
!                               
vars(iDefault,iFB1MHz    )%percentageIsSet =  GPnoneL
!
vars(iDefault,iFB1MHz    )%mode            =  GPnone
!
vars(iDefault,iFB1MHz    )%receiverName    =  "A100"
vars(iDefault,iFB1MHz    )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iFB100kHz  )%name            =     "100kHz"
!                         
vars(iDefault,iFB100kHz  )%wasConnected    =     .False.
vars(iDefault,iFB100kHz  )%isConnected     =     .False.
!                         
vars(iDefault,iFB100kHz  )%nPart           =     1
vars(iStd1   ,iFB100kHz  )%nPart           =     1
vars(iStd2   ,iFB100kHz  )%nPart           =     nDim100kHz
vars(ILimit1 ,iFB100kHz  )%nPart           =     1
vars(iLimit2 ,iFB100kHz  )%nPart           =     nDim100kHz
!                             
vars(iDefault,iFB100kHz  )%resolution      =     0.1
vars(iStd1   ,iFB100kHz  )%resolution      =     0.1
vars(iStd2   ,iFB100kHz  )%resolution      =     0.1
vars(ILimit1 ,iFB100kHz  )%resolution      =     0.1
vars(iLimit2 ,iFB100kHz  )%resolution      =     0.1
!                             
vars(iDefault,iFB100kHz  )%bandwidth       =    12.8
vars(iStd1   ,iFB100kHz  )%bandwidth       =    12.8
vars(iStd2   ,iFB100kHz  )%bandwidth       =    25.6
vars(ILimit1 ,iFB100kHz  )%bandwidth       =    12.8
vars(iLimit2 ,iFB100kHz  )%bandwidth       =    25.6
!                             
vars(iDefault,iFB100kHz  )%fShift          =     0.0
vars(iStd1   ,iFB100kHz  )%fShift          =  -250.0
vars(iStd2   ,iFB100kHz  )%fShift          =   250.0
vars(ILimit1 ,iFB100kHz  )%fShift          =  -510.0
vars(iLimit2 ,iFB100kHz  )%fShift          =   250.0
!                             
vars(iDefault,iFB100kHz  )%percentage      =   100.0
vars(iStd1   ,iFB100kHz  )%percentage      =   100.0
vars(iStd2   ,iFB100kHz  )%percentage      =   100.0
vars(ILimit1 ,iFB100kHz  )%percentage      =   100.0
vars(iLimit2 ,iFB100kHz  )%percentage      =   100.0
!                            
vars(iDefault,iFB100kHz  )%percentageIsSet =  GPnoneL
!
vars(iDefault,iFB100kHz  )%mode            =  GPnone
!
vars(iDefault,iFB100kHz  )%receiverName    =  "A100"
vars(iDefault,iFB100kHz  )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iFTS      )%name            =     "FTS "
!                         
vars(iDefault,iFTS      )%wasConnected    =     .True.
vars(iDefault,iFTS      )%isConnected     =     .True.
!                         
vars(iDefault,iFTS      )%nPart           =     1
vars(iStd1   ,iFTS      )%nPart           =     1
vars(iStd2   ,iFTS      )%nPart           =    nDimFTS
vars(ILimit1 ,iFTS      )%nPart           =     1
vars(iLimit2 ,iFTS      )%nPart           =    nDimFTS 
!                           
vars(iDefault,iFTS      )%resolution      =   100.0/512.0
vars(iStd1   ,iFTS      )%resolution      =   100.0/512.0/4.0 *(1.00-0.01)  !! *(1.00-0.01) fudge to relax
vars(iStd2   ,iFTS      )%resolution      =   100.0/512.0     *(1.00+0.05)
vars(iStd2   ,iFTS      )%resolution      =   0.212                         !! CK memo v1.4 2010-06-04
vars(ILimit1 ,iFTS      )%resolution      =   100.0/512.0/4.0 *(1.00-0.01)
vars(iLimit2 ,iFTS      )%resolution      =   100.0/512.0     *(1.00+0.05)
vars(iLimit2 ,iFTS      )%resolution      =   0.212                         !! CK memo v1.4 2010-06-04
!                           
vars(iDefault,iFTS      )%bandwidth       =  FTSbw%wide
vars(iStd1   ,iFTS      )%bandwidth       =   512.0
vars(iStd2   ,iFTS      )%bandwidth       =  FTSbw%wide
vars(ILimit1 ,iFTS      )%bandwidth       =   100.0
vars(iLimit2 ,iFTS      )%bandwidth       =  FTSbw%wide
!                           
vars(iDefault,iFTS      )%fShift          =     0.0
vars(iStd1   ,iFTS      )%fShift          = -2300.0
vars(iStd2   ,iFTS      )%fShift          =  2300.0
vars(ILimit1 ,iFTS      )%fShift          = -2300.0
vars(iLimit2 ,iFTS      )%fShift          =  2300.0
!                           
vars(iDefault,iFTS      )%percentage      =   100.0
vars(iStd1   ,iFTS      )%percentage      =    10.0
vars(iStd2   ,iFTS      )%percentage      =   100.0
vars(ILimit1 ,iFTS      )%percentage      =     1.0
vars(iLimit2 ,iFTS      )%percentage      =   100.0
!                               
vars(iDefault,iFTS      )%percentageIsSet =  .False.
!
vars(iDefault,iFTS      )%mode            =  BEmode%Simp
!
vars(iDefault,iFTS      )%receiverName    =  "E090"
vars(iDefault,iFTS      )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iWILMA    )%name            =     "WILMA"
!                         
vars(iDefault,iWILMA    )%wasConnected    =     .True.
vars(iDefault,iWILMA    )%isConnected     =     .True.
!                         
vars(iDefault,iWILMA    )%nPart           =     1
vars(iStd1   ,iWILMA    )%nPart           =     1
!! vars(iStd2   ,iWILMA    )%nPart           =    nDimWILMA
vars(iStd2   ,iWILMA    )%nPart           =     4
vars(ILimit1 ,iWILMA    )%nPart           =     1
!! vars(iLimit2 ,iWILMA    )%nPart           =    nDimWILMA
vars(iLimit2 ,iWILMA    )%nPart           =     4
!                           
vars(iDefault,iWILMA    )%resolution      =     2.0
vars(iStd1   ,iWILMA    )%resolution      =     2.0
vars(iStd2   ,iWILMA    )%resolution      =     2.0
vars(ILimit1 ,iWILMA    )%resolution      =     2.0
vars(iLimit2 ,iWILMA    )%resolution      =     2.0
!                           
vars(iDefault,iWILMA    )%bandwidth       =  1024.0
vars(iStd1   ,iWILMA    )%bandwidth       =  1000.0
vars(iStd2   ,iWILMA    )%bandwidth       =  8192.0
vars(ILimit1 ,iWILMA    )%bandwidth       =  1024.0
vars(iLimit2 ,iWILMA    )%bandwidth       =  8192.0
!                           
vars(iDefault,iWILMA    )%fShift          =  -265.0
vars(iStd1   ,iWILMA    )%fShift          =  -265.0
vars(iStd2   ,iWILMA    )%fShift          =   265.0
vars(ILimit1 ,iWILMA    )%fShift          =  -265.0
vars(iLimit2 ,iWILMA    )%fShift          =   265.0
!                           
vars(iDefault,iWILMA    )%percentage      =   100.0
vars(iStd1   ,iWILMA    )%percentage      =    50.0
vars(iStd2   ,iWILMA    )%percentage      =   100.0
vars(ILimit1 ,iWILMA    )%percentage      =     0.0
vars(iLimit2 ,iWILMA    )%percentage      =   100.0
!                               
vars(iDefault,iWILMA    )%percentageIsSet =  .True.
!
vars(iDefault,iWILMA    )%nSamples        =   0
vars(iStd1   ,iWILMA    )%nSamples        =   1
vars(iStd2   ,iWILMA    )%nSamples        =   128
vars(ILimit1 ,iWILMA    )%nSamples        =   0
vars(iLimit2 ,iWILMA    )%nSamples        =   128
!                               
vars(iDefault,iWILMA    )%samplesIsSet    =  GPnoneL
!
vars(iDefault,iWILMA    )%mode            =  GPnone
!
vars(iDefault,iWILMA    )%receiverName    =  "HERA1"
vars(iDefault,iWILMA    )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iVESPA    )%name            =     "VESPA"
!                         
vars(iDefault,iVESPA    )%wasConnected    =     .True.
vars(iDefault,iVESPA    )%isConnected     =     .True.
!                         
vars(iDefault,iVESPA    )%nPart           =     1
vars(iStd1   ,iVESPA    )%nPart           =     1
vars(iStd2   ,iVESPA    )%nPart           =    nDimVESPA
vars(ILimit1 ,iVESPA    )%nPart           =     1
vars(iLimit2 ,iVESPA    )%nPart           =    nDimVESPA
!                           
vars(iDefault,iVESPA    )%resolution      =     0.020
vars(iStd1   ,iVESPA    )%resolution      =     0.003
vars(iStd2   ,iVESPA    )%resolution      =     2.5
vars(ILimit1 ,iVESPA    )%resolution      =     0.003
vars(iLimit2 ,iVESPA    )%resolution      =     2.5
!                           
vars(iDefault,iVESPA    )%bandwidth       =    40.0
vars(iStd1   ,iVESPA    )%bandwidth       =    10.0
vars(iStd2   ,iVESPA    )%bandwidth       =   640.0
vars(ILimit1 ,iVESPA    )%bandwidth       =    10.0
vars(iLimit2 ,iVESPA    )%bandwidth       =   640.0
!                           
vars(iDefault,iVESPA    )%fShift          =     0.0
vars(iStd1   ,iVESPA    )%fShift          =  -256.0
vars(iStd2   ,iVESPA    )%fShift          =   256.0
vars(ILimit1 ,iVESPA    )%fShift          =  -256.0
vars(iLimit2 ,iVESPA    )%fShift          =   256.0
!                           
vars(iDefault,iVESPA    )%percentage      =    90.0
vars(iStd1   ,iVESPA    )%percentage      =    50.0
vars(iStd2   ,iVESPA    )%percentage      =    90.0
vars(ILimit1 ,iVESPA    )%percentage      =    10.0
vars(iLimit2 ,iVESPA    )%percentage      =    95.0
!                               
vars(iDefault,iVESPA    )%percentageIsSet =  .True.
!
vars(iDefault,iVESPA    )%nSamples        =   0
vars(iStd1   ,iVESPA    )%nSamples        =   1
vars(iStd2   ,iVESPA    )%nSamples        =   128
vars(ILimit1 ,iVESPA    )%nSamples        =   0
vars(iLimit2 ,iVESPA    )%nSamples        =   128
!                               
vars(iDefault,iVESPA    )%samplesIsSet    =  GPnoneL
!
vars(iDefault,iVESPA    )%mode            =  BEmode%Simp
!
vars(iDefault,iVESPA    )%receiverName    =  "A100"
vars(iDefault,iVESPA    )%receiverName2   =  GPnone
!
!     ******************************************************
!
vars(iDefault,iUSBack  )%name            =     "USB "
!                         
vars(iDefault,iUSBack  )%wasConnected    =     .False.
vars(iDefault,iUSBack  )%isConnected     =     .False.
!                         
vars(iDefault,iUSBack  )%nPart           =     1
vars(iStd1   ,iUSBack  )%nPart           =     1
vars(iStd2   ,iUSBack  )%nPart           =    nDimUSB 
vars(ILimit1 ,iUSBack  )%nPart           =     1
vars(iLimit2 ,iUSBack  )%nPart           =    nDimUSB 
!                           
vars(iDefault,iUSBack  )%resolution      =     0.001
vars(iStd1   ,iUSBack  )%resolution      =     0.001
vars(iStd2   ,iUSBack  )%resolution      =  1024.0
vars(ILimit1 ,iUSBack  )%resolution      =     0.001
vars(iLimit2 ,iUSBack  )%resolution      =  1024.0
!                           
vars(iDefault,iUSBack  )%bandwidth       =  4096.0   
vars(iStd1   ,iUSBack  )%bandwidth       =     0.001
vars(iStd2   ,iUSBack  )%bandwidth       =  4096.0
vars(ILimit1 ,iUSBack  )%bandwidth       =     0.001
vars(iLimit2 ,iUSBack  )%bandwidth       =  4096.0
!                           
vars(iDefault,iUSBack  )%fShift          =     0.0
vars(iStd1   ,iUSBack  )%fShift          =  -512.0
vars(iStd2   ,iUSBack  )%fShift          =   512.0
vars(ILimit1 ,iUSBack  )%fShift          =  -512.0
vars(iLimit2 ,iUSBack  )%fShift          =   512.0
!                           
vars(iDefault,iUSBack  )%percentage      =   100.0
vars(iStd1   ,iUSBack  )%percentage      =    10.0
vars(iStd2   ,iUSBack  )%percentage      =    90.0
vars(ILimit1 ,iUSBack  )%percentage      =     1.0
vars(iLimit2 ,iUSBack  )%percentage      =   100.0
!                               
vars(iDefault,iUSBack  )%percentageIsSet =  .False.
!
vars(iDefault,iUSBack  )%mode            =  BEmode%Simp
!
vars(iDefault,iUSBack  )%receiverName    =  "E090"
vars(iDefault,iUSBack  )%receiverName2   =  GPnone
!
! *** EMIR: to check backend ranges and f-shift
!
!DWrite (6,*) "      rangesBackend: nDimBackends        ", nDimBackends
!DWrite (6,*) "      rangesBackend: nDimSubbandChoices  ", nDimSubbandChoices
!DWrite (6,*) "      rangesBackend: nDimFrange          ", nDimFrange
!
!                                 bandFrom centerFrom center centerTo bandTo
!
fRangeSky(iContinuum,iUI,:)  =  (/  6.500,   6.000,   6.000,   6.000,  5.500  /)
fRangeSky(iFB1MHz,   iUI,:)  =  (/  6.5135,  6.377,   6.002,   5.625,  5.4905 /)
fRangeSky(iVESPA,    iUI,:)  =  (/  6.500,   6.500,   6.250,   6.000,  6.000  /)
fRangeSky(iWILMA,    iUI,:)  =  (/  7.845,   5.985,   5.985,   5.985,  4.125  /)
fRangeSky(iFB4MHz,   iUI,:)  =  (/  8.012,   6.002,   6.002,   6.002,  3.992  /)
fRangeSky(iFTS   ,   iUI,:)  =  (/  8.000,   8.000,   5.975,   3.950,  3.950  /)
!
fRangeSkyFTSfine    (iUI,:)  =  (/  7.110,   7.110,   6.200,   5.290,  5.290  /)
!
fRangeSky(iContinuum,iUI,:)  =  1000.0 * fRangeSky(iContinuum,iUI,:)
fRangeSky(iFB1MHz,   iUI,:)  =  1000.0 * fRangeSky(iFB1MHz,   iUI,:)
fRangeSky(iVESPA,    iUI,:)  =  1000.0 * fRangeSky(iVESPA,    iUI,:)
fRangeSky(iWILMA,    iUI,:)  =  1000.0 * fRangeSky(iWILMA,    iUI,:)
fRangeSky(iFB4MHz,   iUI,:)  =  1000.0 * fRangeSky(iFB4MHz,   iUI,:)
fRangeSky(iFTS   ,   iUI,:)  =  1000.0 * fRangeSky(iFTS   ,   iUI,:)
!
fRangeSkyFTSfine    (iUI,:)  =  1000.0 * fRangeSkyFTSfine    (iUI,:)
!
fRangeSky(iContinuum,iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iContinuum,iUI,:)  
fRangeSky(iFB1MHz,   iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iFB1MHz,   iUI,:)  
fRangeSky(iVESPA,    iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iVESPA,    iUI,:)  
fRangeSky(iWILMA,    iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iWILMA,    iUI,:)  
fRangeSky(iFB4MHz,   iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iFB4MHz,   iUI,:)  
fRangeSky(iFTS   ,   iUO,:)  =  1000.0 * fIFswBoxO - fRangeSky(iFTS   ,   iUI,:)  
!
fRangeSkyFTSfine    (iUO,:)  =  1000.0 * fIFswBoxO - fRangeSkyFTSfine    (iUI,:)  
!
fRangeSky(iContinuum,iLI,:)  =                     - fRangeSky(iContinuum,iUI,:)  
fRangeSky(iFB1MHz,   iLI,:)  =                     - fRangeSky(iFB1MHz,   iUI,:)  
fRangeSky(iVESPA,    iLI,:)  =                     - fRangeSky(iVESPA,    iUI,:)  
fRangeSky(iWILMA,    iLI,:)  =                     - fRangeSky(iWILMA,    iUI,:)  
fRangeSky(iFB4MHz,   iLI,:)  =                     - fRangeSky(iFB4MHz,   iUI,:)  
fRangeSky(iFTS   ,   iLI,:)  =                     - fRangeSky(iFTS   ,   iUI,:)  
!                                                                                                              
fRangeSkyFTSfine    (iLI,:)  =                     - fRangeSkyFTSfine    (iUI,:)  
!                                                                                                              
fRangeSky(iContinuum,iLO,:)  =                     - fRangeSky(iContinuum,iUO,:)  
fRangeSky(iFB1MHz,   iLO,:)  =                     - fRangeSky(iFB1MHz,   iUO,:)  
fRangeSky(iVESPA,    iLO,:)  =                     - fRangeSky(iVESPA,    iUO,:)  
fRangeSky(iWILMA,    iLO,:)  =                     - fRangeSky(iWILMA,    iUO,:)  
fRangeSky(iFB4MHz,   iLO,:)  =                     - fRangeSky(iFB4MHz,   iUO,:)  
fRangeSky(iFTS   ,   iLO,:)  =                     - fRangeSky(iFTS   ,   iUO,:)  
!
fRangeSkyFTSfine    (iLO,:)  =                     - fRangeSkyFTSfine    (iUO,:)  
!
! *** EMIR: standard fixed bandwidths
!
stdBandWidth (iContinuum,:)         =  vars(iDefault,iContinuum)%bandwidth 
stdBandWidth (iFB1MHz,   :)         =  vars(iDefault,iFB1MHz)%bandwidth 
stdBandWidth (iVESPA,    :)         =  vars(iDefault,iVESPA)%bandwidth 
!
stdBandWidth (iWILMA,    :)         =  3720.0
stdBandWidth (iWILMA,   iLSB:iUSB)  =  7430.0
!
stdBandWidth (iFB4MHz,   :)         =  4024.0
stdBandWidth (iFB4MHz,  iLSB:iUSB)  =  7896.0
!                            !                            
stdBandWidth (iFTS   ,   :)         =  FTSbw%wide
stdBandWidth (iFTS   ,  iLSB:iUSB)  =  8000.0
!                            !                            
!
! *** HERA: standard fixed bandwidths
!
stdBandWidthHERA ( : )              =  0.0
!
stdBandWidthHERA (iContinuum)       =  vars(iDefault,iContinuum)%bandwidth 
stdBandWidthHERA (iFB1MHz   )       =  vars(iDefault,iFB1MHz)%bandwidth 
stdBandWidthHERA (iVESPA    )       =  vars(iDefault,iVESPA)%bandwidth 
!
stdBandWidthHERA (iWILMA    )       =  1024.0
stdBandWidthHERA (iFB4MHz   )       =  1024.0
stdBandWidthHERA (iFTS      )       =  1024.0
!
! *** HERA: maximum FTS bandwidths
!
maxBandWidthHeraFTSwide             =  1050.195  !!  email GP 2011-07-21
maxBandWidthHeraFTSfine             =   625.684
!
!
!
Do ii = 1, nDimBackends, 1
   Select Case(ii)
   Case(iContinuum,iFB1MHz,iVESPA,iWILMA,iFB4MHz)
      Do jj = 1, nDimSubbandChoices, 1
         Select Case(jj)
         Case(iLO,iLI,iUI,iUO)
            fRangeTypeSky(ii,jj)  =  fRangeType (                                &
                 &                               fRangeSky(ii,jj,1),             &
                 &                               fRangeSky(ii,jj,2),             &
                 &                               fRangeSky(ii,jj,3),             &
                 &                               fRangeSky(ii,jj,4),             &
                 &                               fRangeSky(ii,jj,5)              &
                 &                              )                                !             
         Case Default
         End Select
      End Do
   Case Default
   End Select
End Do
!                            
!D Write (6,*)
!D Write (6,'(A,6F10.3)') " fRangeSky(iContinuum,iLO,:) ", fRangeSky(iContinuum,iLO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB1MHz,   iLO,:) ", fRangeSky(iFB1MHz,   iLO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iVESPA,    iLO,:) ", fRangeSky(iVESPA,    iLO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iWILMA,    iLO,:) ", fRangeSky(iWILMA,    iLO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB4MHz,   iLO,:) ", fRangeSky(iFB4MHz,   iLO,:)
!                            !                            
!D Write (6,*)
!D Write (6,'(A,6F10.3)') " fRangeSky(iContinuum,iLI,:) ", fRangeSky(iContinuum,iLI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB1MHz,   iLI,:) ", fRangeSky(iFB1MHz,   iLI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iVESPA,    iLI,:) ", fRangeSky(iVESPA,    iLI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iWILMA,    iLI,:) ", fRangeSky(iWILMA,    iLI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB4MHz,   iLI,:) ", fRangeSky(iFB4MHz,   iLI,:)
!                            !                            
!D Write (6,*)
!D Write (6,'(A,6F10.3)') " fRangeSky(iContinuum,iUI,:) ", fRangeSky(iContinuum,iUI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB1MHz,   iUI,:) ", fRangeSky(iFB1MHz,   iUI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iVESPA,    iUI,:) ", fRangeSky(iVESPA,    iUI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iWILMA,    iUI,:) ", fRangeSky(iWILMA,    iUI,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB4MHz,   iUI,:) ", fRangeSky(iFB4MHz,   iUI,:)
!                            !                            
!D Write (6,*)
!D Write (6,'(A,6F10.3)') " fRangeSky(iContinuum,iUO,:) ", fRangeSky(iContinuum,iUO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB1MHz,   iUO,:) ", fRangeSky(iFB1MHz,   iUO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iVESPA,    iUO,:) ", fRangeSky(iVESPA,    iUO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iWILMA,    iUO,:) ", fRangeSky(iWILMA,    iUO,:)
!D Write (6,'(A,6F10.3)') " fRangeSky(iFB4MHz,   iUO,:) ", fRangeSky(iFB4MHz,   iUO,:)
!
!!$!                                             bandFrom    centerFrom        center      centerTo        bandTo
!!$!
!!$fRangeTypeSky(iContinuum,iLO)  =  fRangeType( -15.68+ 6.500, -15.68+ 6.000, -15.68+ 6.000, -15.68+ 6.000, -15.68+ 5.500 )
!!$fRangeTypeSky(iFB1MHz,   iLO)  =  fRangeType( -15.68+ 6.500, -15.68+ 6.375, -15.68+ 6.000, -15.68+ 5.625, -15.68+ 5.500 )
!!$fRangeTypeSky(iVESPA,    iLO)  =  fRangeType( -15.68+ 6.500, -15.68+ 6.500, -15.68+ 6.250, -15.68+ 6.000, -15.68+ 6.000 )
!!$fRangeTypeSky(iWILMA,    iLO)  =  fRangeType( -15.68+ 7.845, -15.68+ 5.985, -15.68+ 5.985, -15.68+ 5.985, -15.68+ 4.125 )
!!$fRangeTypeSky(iFB4MHz,   iLO)  =  fRangeType( -15.68+ 8.000, -15.68+ 6.000, -15.68+ 6.000, -15.68+ 6.000, -15.68+ 4.000 )
!!$!                            
!!$fRangeTypeSky(iContinuum,iLI)  =  fRangeType(       - 6.500,       - 6.000,       - 6.000,       - 6.000,       - 5.500 )
!!$fRangeTypeSky(iFB1MHz,   iLI)  =  fRangeType(       - 6.500,       - 6.375,       - 6.000,       - 5.625,       - 5.500 )
!!$fRangeTypeSky(iVESPA,    iLI)  =  fRangeType(       - 6.500,       - 6.500,       - 6.250,       - 6.000,       - 6.000 )
!!$fRangeTypeSky(iWILMA,    iLI)  =  fRangeType(       - 7.845,       - 5.985,       - 5.985,       - 5.985,       - 4.125 )
!!$fRangeTypeSky(iFB4MHz,   iLI)  =  fRangeType(       - 8.000,       - 6.000,       - 6.000,       - 6.000,       - 4.000 )
!!$!                            
!!$fRangeTypeSky(iContinuum,iUI)  =  fRangeType(         6.500,         6.000,         6.000,         6.000,         5.500 )
!!$fRangeTypeSky(iFB1MHz,   iUI)  =  fRangeType(         6.500,         6.375,         6.000,         5.625,         5.500 )
!!$fRangeTypeSky(iVESPA,    iUI)  =  fRangeType(         6.500,         6.500,         6.250,         6.000,         6.000 )
!!$fRangeTypeSky(iWILMA,    iUI)  =  fRangeType(         7.845,         5.985,         5.985,         5.985,         4.125 )
!!$fRangeTypeSky(iFB4MHz,   iUI)  =  fRangeType(         8.000,         6.000,         6.000,         6.000,         4.000 )
!!$!                            
!!$fRangeTypeSky(iContinuum,iUO)  =  fRangeType(  15.68- 6.500,  15.68- 6.000,  15.68- 6.000,  15.68- 6.000,  15.68- 5.500 )
!!$fRangeTypeSky(iFB1MHz,   iUO)  =  fRangeType(  15.68- 6.500,  15.68- 6.375,  15.68- 6.000,  15.68- 5.625,  15.68- 5.500 )
!!$fRangeTypeSky(iVESPA,    iUO)  =  fRangeType(  15.68- 6.500,  15.68- 6.500,  15.68- 6.250,  15.68- 6.000,  15.68- 6.000 )
!!$fRangeTypeSky(iWILMA,    iUO)  =  fRangeType(  15.68- 7.845,  15.68- 5.985,  15.68- 5.985,  15.68- 5.985,  15.68- 4.125 )
!!$fRangeTypeSky(iFB4MHz,   iUO)  =  fRangeType(  15.68- 8.000,  15.68- 6.000,  15.68- 6.000,  15.68- 6.000,  15.68- 4.000 )
!!$!
!!$
!!$
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iLO) ", fRangeTypeSky(iContinuum,iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iLO) ", fRangeTypeSky(iFB1MHz,   iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iLO) ", fRangeTypeSky(iVESPA,    iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iLO) ", fRangeTypeSky(iWILMA,    iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iLO) ", fRangeTypeSky(iFB4MHz,   iLO)
!!$!                            !                            
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iLI) ", fRangeTypeSky(iContinuum,iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iLI) ", fRangeTypeSky(iFB1MHz,   iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iLI) ", fRangeTypeSky(iVESPA,    iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iLI) ", fRangeTypeSky(iWILMA,    iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iLI) ", fRangeTypeSky(iFB4MHz,   iLI)
!!$!                            !                            
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iUI) ", fRangeTypeSky(iContinuum,iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iUI) ", fRangeTypeSky(iFB1MHz,   iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iUI) ", fRangeTypeSky(iVESPA,    iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iUI) ", fRangeTypeSky(iWILMA,    iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iUI) ", fRangeTypeSky(iFB4MHz,   iUI)
!!$!                            !                            
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iUO) ", fRangeTypeSky(iContinuum,iUO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iUO) ", fRangeTypeSky(iFB1MHz,   iUO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iUO) ", fRangeTypeSky(iVESPA,    iUO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iUO) ", fRangeTypeSky(iWILMA,    iUO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iUO) ", fRangeTypeSky(iFB4MHz,   iUO)
!!$!
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iLO) ", fRangeTypeSky(iContinuum,iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iLI) ", fRangeTypeSky(iContinuum,iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iUI) ", fRangeTypeSky(iContinuum,iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iContinuum,iUO) ", fRangeTypeSky(iContinuum,iUO)
!!$!
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iLO) ", fRangeTypeSky(iFB1MHz,   iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iLI) ", fRangeTypeSky(iFB1MHz,   iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iUI) ", fRangeTypeSky(iFB1MHz,   iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB1MHz,   iUO) ", fRangeTypeSky(iFB1MHz,   iUO)
!!$!                            !                            
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iLO) ", fRangeTypeSky(iVESPA,    iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iLI) ", fRangeTypeSky(iVESPA,    iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iUI) ", fRangeTypeSky(iVESPA,    iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iVESPA,    iUO) ", fRangeTypeSky(iVESPA,    iUO)
!!$!                            !                            
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iLO) ", fRangeTypeSky(iWILMA,    iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iLI) ", fRangeTypeSky(iWILMA,    iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iUI) ", fRangeTypeSky(iWILMA,    iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iWILMA,    iUO) ", fRangeTypeSky(iWILMA,    iUO)
!!$!
!!$Write (6,*)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iLO) ", fRangeTypeSky(iFB4MHz,   iLO)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iLI) ", fRangeTypeSky(iFB4MHz,   iLI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iUI) ", fRangeTypeSky(iFB4MHz,   iUI)
!!$Write (6,'(A,6F10.3)') " fRangeTypeSky(iFB4MHz,   iUO) ", fRangeTypeSky(iFB4MHz,   iUO)
!!$!
