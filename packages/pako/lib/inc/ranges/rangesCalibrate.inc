!
! Id:       rangesCalibrate.inc,v 1.0.5 2006-10-19 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesOnOff.inc
!           rangesOtfMap.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%doAmbient        = .True.
!
vars(iDefault)%doCold           = .True.
!
vars(iDefault)%doFast           = .False.
!
vars(iDefault)%doGainImage      = .False.
!
vars(iDefault)%doGrid           = .False.
!
vars(iDefault)%doNull           = .False.
!
vars(iDefault)%doSky            = .True.
!
vars(iDefault)%xOffsetRC        =    -600.0
vars(iStd1)%xOffsetRC           =   -3600.0
vars(iStd2)%xOffsetRC           =   +3600.0
vars(iLimit1)%xOffsetRC         =  -36000.0
vars(iLimit2)%xOffsetRC         =  +36000.0
!                                     
vars(iDefault)%yOffsetRC        =       0.0
vars(iStd1)%yOffsetRC           =   -3600.0
vars(iStd2)%yOffsetRC           =   +3600.0
vars(iLimit1)%yOffsetRC         =  -36000.0
vars(iLimit2)%yOffsetRC         =  +36000.0
!
vars(iDefault)%sourceNameSky    = ""
vars(iDefault)%altSky           = "OFFSETS"
!
vars(iDefault)%systemName       = offs%pro
vars(iDefault)%iSystem          = ipro
!
vars(iDefault)%tCalibrate       =    5.0
vars(iStd1)%tCalibrate          =    3.0
vars(iStd2)%tCalibrate          =   10.0
vars(iLimit1)%tCalibrate        =    5.0
vars(iLimit2)%tCalibrate        =   60.0
!
vars(iDefault)%doTest           = .False.
!
vars(iDefault)%doVariable       = .False.
!
vars(iDefault)%tempVariable     =  100.0
vars(iStd1)%tempVariable        =  100.0
vars(iStd2)%tempVariable        =  200.0
vars(iLimit1)%tempVariable      =    3.0
vars(iLimit2)%tempVariable      =  400.0
!







