  !
  !     Id: rangesTip.inc,v 1.2.4   2015-05-28 Hans Ungerechts 
  !     Id: rangesTip.inc,v 1.2.3   2014-05-29 Hans Ungerechts
  !     Id: rangesTip.inc,v 1.1.3   2010-09-01 Hans Ungerechts
  !
  !     Family:   ranges observing mode
  !     Cousins:  rangesTip.f90
  !     Cousins:  rangesPointing.f90
  !
  vars(iDefault)%azimuth          =     180.0
  vars(iStd1)   %azimuth          =       0.0
  vars(iStd2)   %azimuth          =    +360.0
  vars(iLimit1) %azimuth          =       0.0
  vars(iLimit2) %azimuth          =    +420.0
  !
  vars(iDefault)%doCurrentAzimuth = .False.
  !
  vars(iDefault)%airmass%from     =    1.1
  vars(iStd1)   %airmass%from     =    1.1
  vars(iStd2)   %airmass%from     =    1.2
  vars(iLimit1) %airmass%from     =    1.01
  vars(iLimit2) %airmass%from     =    9.0
  !
  vars(iDefault)%airmass%to       =    3.501
  vars(iStd1)   %airmass%to       =    3.0
  vars(iStd2)   %airmass%to       =    4.2
  vars(iLimit1) %airmass%to       =    1.01
  vars(iLimit2) %airmass%to       =    9.0
  !
  vars(iDefault)%airmass%by       =    0.4
  vars(iStd1)   %airmass%by       =    0.1
  vars(iStd2)   %airmass%by       =    0.7
  vars(iLimit1) %airmass%by       =    0.1
  vars(iLimit2) %airmass%by       =    1.0
  !
  vars(iDefault)%tPaddle          =    10.0
  vars(iStd1)   %tPaddle          =     5.0
  vars(iStd2)   %tPaddle          =   180.0
  vars(iLimit1) %tPaddle          =     5.0
  vars(iLimit2) %tPaddle          =   360.0
  !
  vars(iDefault)%tSubscan         =    10.0
  vars(iStd1)   %tSubscan         =     5.0
  vars(iStd2)   %tSubscan         =   180.0
  vars(iLimit1) %tSubscan         =     5.0
  vars(iLimit2) %tSubscan         =   360.0
  !
  vars(iDefault)%doSlew           = .False.
  !
  vars(iDefault)%doTune           = .False.
  !
  vars(iDefault)%tTune            =    1.0
  vars(iStd1)   %tTune            =    1.0
  vars(iStd2)   %tTune            =   15.0
  vars(iLimit1) %tTune            =    1.0
  vars(iLimit2) %tTune            = 3600.0
  !
  vars(iDefault)%hasAirmassList   = .False.
  !
  airmassII(iDefault)             =    1.1
  airmassII(iStd1)                =    1.1
  airmassII(iStd2)                =    4.2
  airmassII(iLimit1)              =    1.01
  airmassII(iLimit2)              =    9.0
