!
! Id:       rangesLissajous.inc,v 1.2.3  2014-08-28 Hans Ungerechts
! Id:       rangesLissajous.inc,v 1.2.3  2014-07-08 Hans Ungerechts
! Id:       rangesLissajous.inc,v 1.2.3  2014-05-27 Hans Ungerechts
! Id:       rangesLissajous.inc,v 1.1.12 2012-03-17 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesOnOff.inc
!           rangesLissajous.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%amplitude%x        =    120.0
vars(iStd1)   %amplitude%x        =     45.0
vars(iStd2)   %amplitude%x        =    120.0
vars(iLimit1) %amplitude%x        =     10.0
vars(iLimit2) %amplitude%x        =   3600.0
!
vars(iDefault)%amplitude%y        =   vars(iDefault)%amplitude%x
vars(iStd1)   %amplitude%y        =   vars(iStd1)   %amplitude%x
vars(iStd2)   %amplitude%y        =   vars(iStd2)   %amplitude%x
vars(iLimit1) %amplitude%y        =   vars(iLimit1) %amplitude%x
vars(iLimit2) %amplitude%y        =   vars(iLimit2) %amplitude%x
!
vars(iDefault)%pCenter%x          =      0.0
vars(iStd1)   %pCenter%x          =  -3600.0
vars(iStd2)   %pCenter%x          =   3600.0
vars(iLimit1) %pCenter%x          =  -3600.0
vars(iLimit2) %pCenter%x          =   3600.0
!
vars(iDefault)%pCenter%y          =      0.0
vars(iStd1)   %pCenter%y          =   vars(iStd1)   %pCenter%x
vars(iStd2)   %pCenter%y          =   vars(iStd2)   %pCenter%x
vars(iLimit1) %pCenter%y          =   vars(iLimit1) %pCenter%x
vars(iLimit2) %pCenter%y          =   vars(iLimit2) %pCenter%x
!
vars(iDefault)%frequency%x        =      0.075
vars(iStd1)   %frequency%x        =      0.01 
vars(iStd2)   %frequency%x        =      0.18 
vars(iLimit1) %frequency%x        =      0.001
vars(iLimit2) %frequency%x        =      0.30 
!
vars(iDefault)%frequency%y        =   vars(iDefault)%frequency%x*Sqrt(2.0)
vars(iStd1)   %frequency%y        =   vars(iStd1)   %frequency%x
vars(iStd2)   %frequency%y        =   vars(iStd2)   %frequency%x
vars(iLimit1) %frequency%y        =   vars(iLimit1) %frequency%x
vars(iLimit2) %frequency%y        =   vars(iLimit2) %frequency%x
!
vars(iDefault)%omega%x            =   vars(iDefault)%frequency%x*2.0*Pi
vars(iStd1)   %omega%x            =   vars(iStd1)   %frequency%x*2.0*Pi
vars(iStd2)   %omega%x            =   vars(iStd2)   %frequency%x*2.0*Pi
vars(iLimit1) %omega%x            =   vars(iLimit1) %frequency%x*2.0*Pi
vars(iLimit2) %omega%x            =   vars(iLimit2) %frequency%x*2.0*Pi
!
vars(iDefault)%omega%y            =   vars(iDefault)%frequency%y*2.0*Pi
vars(iStd1)   %omega%y            =   vars(iStd1)   %frequency%y*2.0*Pi
vars(iStd2)   %omega%y            =   vars(iStd2)   %frequency%y*2.0*Pi
vars(iLimit1) %omega%y            =   vars(iLimit1) %frequency%y*2.0*Pi
vars(iLimit2) %omega%y            =   vars(iLimit2) %frequency%y*2.0*Pi
!
vars(iDefault)%phase%x            =    0.0
vars(iStd1)   %phase%x            =    0.0
vars(iStd2)   %phase%x            =    2.0*Pi
vars(iLimit1) %phase%x            =    0.0
vars(iLimit2) %phase%x            =    2.0*Pi
!
vars(iDefault)%phase%y            =   vars(iDefault)%phase%x
vars(iStd1)   %phase%y            =   vars(iStd1)   %phase%x
vars(iStd2)   %phase%y            =   vars(iStd2)   %phase%x
vars(iLimit1) %phase%y            =   vars(iLimit1) %phase%x
vars(iLimit2) %phase%y            =   vars(iLimit2) %phase%x
!

vars(iDefault)%doRampUp           = .True.
vars(iDefault)%doRampDown         = .False.
!
vars(iDefault)%tRampUp            =    5.0
vars(iStd1)   %tRampUp            =    5.0
vars(iStd2)   %tRampUp            =   10.0
vars(iLimit1) %tRampUp            =    5.0
vars(iLimit2) %tRampUp            =   60.0
!
vars(iDefault)%tRampDown          =    5.0
vars(iStd1)   %tRampDown          =    5.0
vars(iStd2)   %tRampDown          =   10.0
vars(iLimit1) %tRampDown          =    5.0
vars(iLimit2) %tRampDown          =   60.0


!
vars(iDefault)%croCode            =   "O"
vars(iDefault)%croCodeBites       =   "O"
vars(iDefault)%croCodeCount       =    1
vars(iStd1)   %croCodeCount       =    1
vars(iStd2)   %croCodeCount       =  100
vars(iLimit1) %croCodeCount       =    1
vars(iLimit2) %croCodeCount       =  100
!
vars(iDefault)%doReference        = .False.
vars(iDefault)%offsetR%x          =    -150.0
vars(iStd1)   %offsetR%x          =   -3600.0
vars(iStd2)   %offsetR%x          =   +3600.0
vars(iLimit1) %offsetR%x          =  -36000.0
vars(iLimit2) %offsetR%x          =  +36000.0
vars(iDefault)%offsetR%y          =       0.0
vars(iStd1)   %offsetR%y          =   -3600.0
vars(iStd2)   %offsetR%y          =   +3600.0
vars(iLimit1) %offsetR%y          =  -36000.0
vars(iLimit2) %offsetR%y          =  +36000.0
vars(iDefault)%systemNameRef      = offsPako%tru
vars(iDefault)%listOfNamesRef     = ""
vars(iDefault)%altRef             = "OFFSETS"
!
vars(iDefault)%systemName         = offsPako%tru
vars(iDefault)%iSystem            = iTru
!
vars(iDefault)%tOtf               =  600.0
vars(iStd1)   %tOtf               =   10.0
vars(iStd2)   %tOtf               =  600.0
vars(iLimit1) %tOtf               =   10.0
vars(iLimit2) %tOtf               = 3600.0
!
vars(iDefault)%tRecord            =    0.1
vars(iStd1)   %tRecord            =    0.1
vars(iStd2)   %tRecord            =    1.0
vars(iLimit1) %tRecord            =    0.05
vars(iLimit2) %tRecord            =   60.0
!
vars(iDefault)%tReference         =    12.0
vars(iStd1)   %tReference         =    10.0
vars(iStd2)   %tReference         =    60.0
vars(iLimit1) %tReference         =    10.0
vars(iLimit2) %tReference         =   120.0
!
vars(iDefault)%doTune             = .False.
vars(iDefault)%doTuneSet          = .False.
!
vars(iDefault)%offsetTune%x       =       0.0
vars(iStd1)   %offsetTune%x       =   -3600.0
vars(iStd2)   %offsetTune%x       =   +3600.0
vars(iLimit1) %offsetTune%x       =  -36000.0
vars(iLimit2) %offsetTune%x       =  +36000.0
!
vars(iDefault)%offsetTune%y       =  vars(iDefault)%offsetTune%x
vars(iStd1)   %offsetTune%y       =  vars(iStd1)   %offsetTune%x
vars(iStd2)   %offsetTune%y       =  vars(iStd2)   %offsetTune%x
vars(iLimit1) %offsetTune%y       =  vars(iLimit1) %offsetTune%x
vars(iLimit2) %offsetTune%y       =  vars(iLimit2) %offsetTune%x
!
vars(iDefault)%tTune              =    1.0
vars(iStd1)   %tTune              =    1.0
vars(iStd2)   %tTune              =   15.0
vars(iLimit1) %tTune              =    1.0
vars(iLimit2) %tTune              = 3600.0
!
! ***
!
limits(iStd1)   %focus            =   -3.0
limits(iStd2)   %focus            =   +3.0
limits(iLimit1) %focus            =   -9.99
limits(iLimit2) %focus            =   +9.99
!
! ***
!
croCodeChoices(1)       = "C"
croCodeChoices(2)       = "R"
croCodeChoices(3)       = "O"
!


