  !
  !  Id: inc/ranges/checkLimits.inc, v 1.2.3 2014-05-20 Hans Ungerechts
  !      based on:
  !  Id: inc/ranges/checkLimits.inc, v 1.0              Hans Ungerechts
  !
  !D   Write (6,*) " "
  !D   Write (6,*) "      --> inc/ranges/checkLimits.inc v 1.2.3 2014-05-20"
  !D   Write (6,*) "          pakoLimitsGV%privilege:     ", pakoLimitsGV%privilege
  !D   Write (6,*) "          pakoLimitsGV%userLevel:     ", pakoLimitsGV%userLevel
  !D   Write (6,*) "          pakoLimitsGV%iUserLevel:    ", pakoLimitsGV%iUserLevel
  !D   Write (6,*) "          pakoLimitsGV%limitCheck:    ", pakoLimitsGV%limitCheck
  !D   Write (6,*) "          pakoLimitsGV%iLimitCheck:   ", pakoLimitsGV%iLimitCheck
  !D   Write (6,*) " "
  !
  ERROR = .False.
  !
  vStdMin   = Min(variable(iStd1),  variable(iStd2))
  vStdMax   = Max(variable(iStd1),  variable(iStd2))     
  vLimitMin = Min(variable(iLimit1),variable(iLimit2))
  vLimitMax = Max(variable(iLimit1),variable(iLimit2))
  !
  !     D:   write (6,*) " --> checkLimitsInclude.f "
  !     D:   do ii = 1,8,1
  !     D:      write (6,*) "     variable(",ii,") = ", variable(ii)
  !     D:   end do
  !     D:
  !D   Write (6,*) " vIn         = ",vIn
  !D   Write (6,*) " vStdMin     = ",vStdMin
  !D   Write (6,*) " vStdMax     = ",vStdMax
  !D   Write (6,*) " vLimitMin   = ",vLimitMin
  !D   Write (6,*) " vLimitMax   = ",vLimitMax
  !
  !                                                                                  !!   2014
  If      (   (     pakoLimitsGV%privilege.Eq."30mStaff"                         &   !!   exceptions
       &      .Or.  pakoLimitsGV%privilege.Eq."ncsDevelopment"                   &   
       &      .Or.  pakoLimitsGV%userLevel.Eq."guru"                             &   
       &      .Or.  pakoLimitsGV%userLevel.Eq."primaDonna"                       &   
       &      )                                                                  &
       &  .And.                                                                  &
       &     (     pakoLimitsGV%limitCheck.Eq."relaxed"                          &
       &      .Or. pakoLimitsGV%limitCheck.Eq."loose"                            &
       &      .Or. pakoLimitsGV%limitCheck.Eq."free"                             &
       &     )                                                                   &
       &  ) Then                                                                 !
     !
     vXlimitFactor = 1.0
     If ( pakoLimitsGV%limitCheck.Eq."relaxed" )  vXlimitFactor =  1.1
     If ( pakoLimitsGV%limitCheck.Eq."loose"   )  vXlimitFactor = 10.0
     !
     Write (6,*) " vXlimitFactor:  ", vXlimitFactor
     !
     If  (  vLimitMin.Lt.0  )  vXlimitMin =   vXlimitFactor*vLimitMin
     If  (  vLimitMin.Eq.0  )  vXlimitMin =                 vLimitMin
     If  (  vLimitMin.Gt.0  )  vXlimitMin = 1/vXlimitFactor*vLimitMin
     !
     If  (  vLimitMax.Lt.0  )  vXlimitMax = 1/vXlimitFactor*vLimitMax
     If  (  vLimitMax.Lt.0  )  vXlimitMax =                 vLimitMax
     If  (  vLimitMax.Gt.0  )  vXlimitMax =   vXlimitFactor*vLimitMax
     !
     Write (6,*) " vXlimitMin:  ", vXlimitMin
     Write (6,*) " vXlimitMax:  ", vXlimitMax
     !
!!$     If (         (        pakoLimitsGV%limitCheck.Eq."relaxed"                  &
!!$          &         .And.  vIn.Ge.1.1*vLimitMin .And. vIn.Le.1.1*vLimitMax )     &   
!!$          & .Or.                                                                 &
!!$          &       (        pakoLimitsGV%limitCheck.Eq."loose"                    &
!!$          &         .And.  vIn.Ge.10.0*vLimitMin .And. vIn.Le.10.0*vLimitMax )   &   
!!$          & .Or.  (        pakoLimitsGV%limitCheck.Eq."free"                     &
!!$          &       )                                                              &
!!$          & ) Then                                                               !
     !
     If (         (        pakoLimitsGV%limitCheck.Eq."relaxed"                  &
          &         .And.  vIn.Ge.vXlimitMin .And. vIn.Le.vXlimitMax )           &   
          & .Or.                                                                 &
          &       (        pakoLimitsGV%limitCheck.Eq."loose"                    &
          &         .And.  vIn.Ge.vXlimitMin .And. vIn.Le.vXlimitMax )           &   
          & .Or.  (        pakoLimitsGV%limitCheck.Eq."free"                     &
          &       )                                                              &
          & ) Then                                                               !
        !
        !D Write (6,*) " case  pakoLimitsGV%limitCheck ", pakoLimitsGV%limitCheck
        !
        variable(iIn) = vIn
        !
        If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
           Write (messageText,*) 'value ', vIn,                                  &
                &        ' OUTSIDE LIMITS ', variable(iLimit1),                  &
                &        ' to ', variable(iLimit2)                               !
           Call PakoMessage(4,3,command//"/"//option,messageText)
           Write (messageText,*) 'value ', vIn,                                  &
                &        ' allowed by privilege or user level'                   !
           Call PakoMessage(4,2,command//"/"//option,messageText)
        Else
           If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
              Write (messageText,*) 'value ', vIn,                               &
                   &           ' outside standard range ', variable(iStd1),      &
                   &           ' to ', variable(iStd2)                           !
              Call PakoMessage(4,2,command//"/"//option,messageText)
           Endif
        End If
        !
     Else
        error = .True.
        Write (messageText,*) 'value ', vIn,                                     &
             &        ' outside ',  pakoLimitsGV%limitCheck, ' limits '          !
        Call PakoMessage(4,3,command//"/"//option,messageText)
     End If
     !
  Else                                                                           
     !                                                                             !!   standard case / checks
     If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
        error = .True.
        Write (messageText,*) 'value ', vIn,                                     &
             &        ' outside limits ', variable(iLimit1),                     &
             &        ' to ', variable(iLimit2)                                  !
        Call PakoMessage(4,3,command//"/"//option,messageText)
     Else
        variable(iIn) = vIn
        If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
           Write (messageText,*) 'value ', vIn,                                  &
                &           ' outside standard range ', variable(iStd1),         &
                &           ' to ', variable(iStd2)                              !
           Call PakoMessage(4,2,command//"/"//option,messageText)
        Endif
     Endif
     !
  End If   !!   exceptions  //  standard case
  !
