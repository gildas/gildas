!
! Id:       rangesPointing.inc,v 1.2.3  2014-09-23 Hans Ungerechts
! Id:       rangesPointing.inc,v 1.1.12 2012-02-27 Hans Ungerechts
! Family:   ranges observing mode
! Cousins:  rangesPointing.f90
!
vars(iDefault)%length          =     120.0
vars(iStd1)   %length          =      30.0
vars(iStd2)   %length          =     120.0
vars(iLimit1) %length          =       0.0
vars(iLimit2) %length          =   +7200.0
!
vars(iDefault)%doCalibrate     = .FALSE.
!
vars(iDefault)%doDoubleBeam    = .FALSE.
!
vars(iDefault)%febe            =       1
vars(iStd1)   %febe            =       1
vars(iStd2)   %febe            =      10
vars(iLimit1) %febe            =       1
vars(iLimit2) %febe            =     100
!
vars(iDefault)%doMore          = .FALSE.
!
vars(iDefault)%nOtf            =       4
vars(iStd1)   %nOtf            =       1
vars(iStd2)   %nOtf            =      12
vars(iLimit1) %nOtf            =       1
vars(iLimit2) %nOtf            =      99-3   !!  -3 because must be multiple of 4
!
vars(iDefault)%tOtf            =   30.0
vars(iStd1)   %tOtf            =   10.0
vars(iStd2)   %tOtf            =  600.0
vars(iLimit1) %tOtf            =   10.0
vars(iLimit2) %tOtf            = 3600.0
!
vars(iDefault)%speedStart      =       vars(iDefault)%length/vars(iDefault)%tOtf
vars(iStd1)   %speedStart      =       0.0
vars(iStd2)   %speedStart      =     +60.0
vars(iLimit1) %speedStart      =       0.0
vars(iLimit2) %speedStart      =    +300.0
!
vars(iDefault)%speedEnd        =       vars(iDefault)%speedStart   
vars(iStd1)   %speedEnd        =       0.0
vars(iStd2)   %speedEnd        =     +60.0
vars(iLimit1) %speedEnd        =       0.0
vars(iLimit2) %speedEnd        =    +300.0
!
vars(iDefault)%doTune             = .False.
vars(iDefault)%doTuneSet          = .False.
!
vars(iDefault)%offsetTune%x       =       0.0
vars(iStd1)   %offsetTune%x       =   -3600.0
vars(iStd2)   %offsetTune%x       =   +3600.0
vars(iLimit1) %offsetTune%x       =  -36000.0
vars(iLimit2) %offsetTune%x       =  +36000.0
!
vars(iDefault)%offsetTune%y       =  vars(iDefault)%offsetTune%x
vars(iStd1)   %offsetTune%y       =  vars(iStd1)   %offsetTune%x
vars(iStd2)   %offsetTune%y       =  vars(iStd2)   %offsetTune%x
vars(iLimit1) %offsetTune%y       =  vars(iLimit1) %offsetTune%x
vars(iLimit2) %offsetTune%y       =  vars(iLimit2) %offsetTune%x
!
vars(iDefault)%tTune              =   10.0
vars(iStd1)   %tTune              =    1.0
vars(iStd2)   %tTune              =   15.0
vars(iLimit1) %tTune              =    1.0
vars(iLimit2) %tTune              = 3600.0
!
vars(iDefault)%tRecord         =    0.1
vars(iStd1)   %tRecord         =    0.1
vars(iStd2)   %tRecord         =    1.0
vars(iLimit1) %tRecord         =    0.05
vars(iLimit2) %tRecord         =   60.0
!
vars(iDefault)%doUpdate        = .FALSE.
!
vars(iDefault)%aeCode          = GPnone
vars(iDefault)%aeCodeBites     = GPnone
!
vars(iDefault)%nAeCode         =       0
vars(iStd1)   %nAeCode         =       0
vars(iStd2)   %nAeCode         =       4
vars(iLimit1) %nAeCode         =       0
vars(iLimit2) %nAeCode         = lenVar/2
