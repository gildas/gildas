!
! Id:       rangesOnOff.inc,v 1.1.12 2012-02-23 Hans Ungerechts
! Family:   ranges(ObservingMode).inc
! Siblings: rangesCalibrate.inc
!           rangesOnOff.inc
!           rangesOtfMap.inc
!           rangesTrack.inc
! Cousins:  rangesTip.f90
! Cousins:  rangesPointing.f90
!
vars(iDefault)%offset%x           =       0.0
vars(iStd1)   %offset%x           =   -3600.0
vars(iStd2)   %offset%x           =   +3600.0
vars(iLimit1) %offset%x           =  -36000.0
vars(iLimit2) %offset%x           =  +36000.0
!
vars(iDefault)%offset%y           =       0.0
vars(iStd1)   %offset%y           =   -3600.0
vars(iStd2)   %offset%y           =   +3600.0
vars(iLimit1) %offset%y           =  -36000.0
vars(iLimit2) %offset%y           =  +36000.0
!
vars(iDefault)%sourceName         =  GPnone
vars(iDefault)%altPosition        = "OFFSETS"
!
vars(iDefault)%doBalance          = GPnoneL
!
vars(iDefault)%doCalibrate        = GPnoneL
!
vars(iDefault)%nSubscans          =    4
vars(iStd1)   %nSubscans          =    1
vars(iStd2)   %nSubscans          =   20
vars(iLimit1) %nSubscans          =    1
vars(iLimit2) %nSubscans          =   99-1     !  -1 because needs to be even
!
vars(iDefault)%doReference        = .True.
vars(iDefault)%offsetR%x          =    -600.0
vars(iStd1)   %offsetR%x          =   -3600.0
vars(iStd2)   %offsetR%x          =   +3600.0
vars(iLimit1) %offsetR%x          =  -36000.0
vars(iLimit2) %offsetR%x          =  +36000.0
vars(iDefault)%offsetR%y          =       0.0
vars(iStd1)   %offsetR%y          =   -3600.0
vars(iStd2)   %offsetR%y          =   +3600.0
vars(iLimit1) %offsetR%y          =  -36000.0
vars(iLimit2) %offsetR%y          =  +36000.0
vars(iDefault)%systemNameRef      =  offs%pro
vars(iDefault)%listOfNamesRef     =  GPnone
vars(iDefault)%altRef             = "OFFSETS"
!
vars(iDefault)%doSymmetric        =  GPnoneL
!
vars(iDefault)%doSwWobbler        =  GPnoneL
!
! deprecated:
systemNameRefChoices(1)           = "PROJECTION"
systemNameRefChoices(2)           = "DESCRIPTIVE"
systemNameRefChoices(3)           = "BASIS"
systemNameRefChoices(4)           = "EQUATORIAL"
systemNameRefChoices(5)           = "HA/DECL"
systemNameRefChoices(6)           = "HORIZONTAL"
systemNameRefChoices(7)           = "NASMYTH"
!
vars(iDefault)%systemName         = offs%pro
vars(iDefault)%iSystem            = ipro
!
vars(iDefault)%tSubscan           =   30.0
vars(iStd1)   %tSubscan           =   10.0
vars(iStd2)   %tSubscan           =  600.0
vars(iLimit1) %tSubscan           =   10.0
vars(iLimit2) %tSubscan           = 3600.0
!
