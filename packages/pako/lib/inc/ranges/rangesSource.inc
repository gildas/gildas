!
!     Id: rangesSource.inc,v 1.0.4 2006-08-04 Hans Ungerechts
!     Family:   
!
!TBD: delete/obsolete choices --> use global parameters!
!TBD: planetNameChoices(0)           = "Sun"
      planetNameChoices(1)           = "Mercury"
      planetNameChoices(2)           = "Venus"
      planetNameChoices(3)           = "Earth"
      planetNameChoices(4)           = "Mars"
      planetNameChoices(5)           = "Jupiter"
      planetNameChoices(6)           = "Saturn"
      planetNameChoices(7)           = "Uranus"
      planetNameChoices(8)           = "Neptune"
      planetNameChoices(9)           = "Pluto"
!                                    
      satelliteNameChoices(1)        = "Moon"
      satelliteNameChoices(2)        = "Io"
!
      vars(iDefault)%isBody          = .False.
      vars(iDefault)%isPlanet        = .False.
      vars(iDefault)%isSatellite     = .False.
!
      vars(iDefault)%sourceName      = "ORIIRC2"
!                                    
      vars(iDefault)%systemName      = sys%equatorial
!
      vars(iDefault)%equinoxSystemName  = equ%J
!
      vars(iDefault)%epoch           = 2000.0
      vars(iStd1)%epoch              = 2000.0
      vars(iStd2)%epoch              = 2000.0
      vars(iLimit1)%epoch            = 1800.0
      vars(iLimit2)%epoch            = 2200.0
!
      vars(iDefault)%longitude%text  = "05:35:14.4742"
      vars(iDefault)%longitude%hour  =  5.5873539444444
      vars(iDefault)%longitude%deg   = 83.810309166667
      vars(iDefault)%longitude%rad   = 1.4627658420727
!
      vars(iStd1)%longitude%rad      = 0.0
      vars(iStd2)%longitude%rad      = 2.0d0*Pi
      vars(iLimit1)%longitude%rad    = 0.0
      vars(iLimit2)%longitude%rad    = 2.0d0*Pi
!
      vars(iDefault)%latitude%text   = "-05:22:29.557"
      vars(iDefault)%latitude%hour   = -0.35832512962963 
      vars(iDefault)%latitude%deg    = -5.3748769444444 
      vars(iDefault)%latitude%rad    = -0.093809299570088 
!
      vars(iStd1)%latitude%rad       = -Pi/2.0d0
      vars(iStd2)%latitude%rad       =  Pi/2.0d0
      vars(iLimit1)%latitude%rad     = -Pi/2.0d0
      vars(iLimit2)%latitude%rad     =  Pi/2.0d0
!
      vars(iDefault)%velocity%type     =  refFrame%LSR
!
      vars(iDefault)%velocity%value    =      8.0
      vars(iStd1)%velocity%value       =  -3000.0
      vars(iStd2)%velocity%value       =   3000.0
      vars(iLimit1)%velocity%value     = -10000.0
      vars(iLimit2)%velocity%value     =  10000.0
!
! TBD: delete obsolete choices / use global parameters!
!
      velocityTypeChoices(1)           = "LSR"
      velocityTypeChoices(2)           = "HEL"
      velocityTypeChoices(3)           = "EARTH"
      velocityTypeChoices(4)           = "PLANET"
      velocityTypeChoices(5)           = "NUL"
      velocityTypeChoices(5)           = "NONE"
!
      vars(iDefault)%flux              =    0.0
      vars(iStd1)%flux                 =    0.0
      vars(iStd2)%flux                 =  100.0
      vars(iLimit1)%flux               = -Huge(1.0)
      vars(iLimit2)%flux               =  Huge(1.0)
!
      vars(iDefault)%index                  =   0.0
         vars(iStd1)%index                  =  -3.0
         vars(iStd2)%index                  =   3.0
       vars(iLimit1)%index                  = -10.0
       vars(iLimit2)%index                  =  10.0
!
      vars(iDefault)%perihelionEpoch        =  2450000.0000D0
         vars(iStd1)%perihelionEpoch        =  2450000.0000D0
         vars(iStd2)%perihelionEpoch        =  2460000.0000D0
       vars(iLimit1)%perihelionEpoch        =  2369916.0000D0
       vars(iLimit2)%perihelionEpoch        =  2369916.0000D0+365242.00D0
!
      vars(iDefault)%ascendingNode          =     0.0D0
         vars(iStd1)%ascendingNode          =  -360.0D0
         vars(iStd2)%ascendingNode          =   360.0D0
       vars(iLimit1)%ascendingNode          =  -360.0D0
       vars(iLimit2)%ascendingNode          =   360.0D0
!
      vars(iDefault)%argumentOfPerihelion   =     0.0D0
         vars(iStd1)%argumentOfPerihelion   =  -360.0D0
         vars(iStd2)%argumentOfPerihelion   =   360.0D0
       vars(iLimit1)%argumentOfPerihelion   =  -360.0D0
       vars(iLimit2)%argumentOfPerihelion   =   360.0D0
!
      vars(iDefault)%inclination            =     0.0D0
         vars(iStd1)%inclination            =  -360.0D0
         vars(iStd2)%inclination            =   360.0D0
       vars(iLimit1)%inclination            =  -360.0D0
       vars(iLimit2)%inclination            =   360.0D0
!
      vars(iDefault)%perihelionDistance     =    0.0D0
         vars(iStd1)%perihelionDistance     =    0.1D0
         vars(iStd2)%perihelionDistance     =   99.9D0
       vars(iLimit1)%perihelionDistance     =    0.001D0
       vars(iLimit2)%perihelionDistance     =  999.9D0
!
      vars(iDefault)%eccentricity           =    1.0D0
         vars(iStd1)%eccentricity           =    0.0D0
         vars(iStd2)%eccentricity           =    1.0D0
       vars(iLimit1)%eccentricity           =    0.0D0
       vars(iLimit2)%eccentricity           =   10.0D0
!
! *** Offsets:
!
      varsOffsets(iDefault,iPro)%systemName = offsetChoicesPako(iPro)
!
      varsOffsets(iDefault,iPro)%isSet      = .False.
!
      varsOffsets(iDefault,iPro)%point%x    =      0.0
      varsOffsets(iStd1,   iPro)%point%x    =  -3600.0
      varsOffsets(iStd2,   iPro)%point%x    =   3600.0
      varsOffsets(iLimit1, iPro)%point%x    = -36000.0
      varsOffsets(iLimit2, iPro)%point%x    =  36000.0
!
      varsOffsets(iDefault,iPro)%point%y    =      0.0
      varsOffsets(iStd1,   iPro)%point%y    =  -3600.0
      varsOffsets(iStd2,   iPro)%point%y    =   3600.0
      varsOffsets(iLimit1, iPro)%point%y    = -36000.0
      varsOffsets(iLimit2, iPro)%point%y    =  36000.0
!
      varsOffsets(iDefault,iPro)%channel    =      1
      varsOffsets(iStd1,   iPro)%channel    =      1  
      varsOffsets(iStd2,   iPro)%channel    =      1  
      varsOffsets(iLimit1, iPro)%channel    =      1  
      varsOffsets(iLimit2, iPro)%channel    =      1  
!
      varsOffsets(iDefault,iPro)%elevation  =      0.0  
      varsOffsets(iStd1,   iPro)%elevation  =      0.0 
      varsOffsets(iStd2,   iPro)%elevation  =      0.0
      varsOffsets(iLimit1, iPro)%elevation  =      0.0
      varsOffsets(iLimit2, iPro)%elevation  =      0.0
!
      do ii = 2, nDimOffsetChoices, 1
         varsOffsets(iDefault,ii) = varsOffsets(iDefault,iPro)
         varsOffsets(iStd1   ,ii) = varsOffsets(iStd1   ,iPro)
         varsOffsets(iStd2   ,ii) = varsOffsets(iStd2   ,iPro)
         varsOffsets(iLimit1 ,ii) = varsOffsets(iLimit1 ,iPro)
         varsOffsets(iLimit2 ,ii) = varsOffsets(iLimit2 ,iPro)
      end do
!
      varsOffsets(iDefault,iTru)%isSet      = .True.
!
      varsOffsets(iDefault,iDes)%systemName = offsetChoicesPako(iDes)
      varsOffsets(iDefault,iBas)%systemName = offsetChoicesPako(iBas)
      varsOffsets(iDefault,iEqu)%systemName = offsetChoicesPako(iEqu)
      varsOffsets(iDefault,iHad)%systemName = offsetChoicesPako(iHad)
      varsOffsets(iDefault,iTru)%systemName = offsetChoicesPako(iTru)
      varsOffsets(iDefault,iHor)%systemName = offsetChoicesPako(iHor)
!
      varsOffsets(iDefault,iNas)%systemName = offsetChoicesPako(iNas)
!
      varsOffsets(iDefault,iNas)%channel    =      1  
      varsOffsets(iStd1,   iNas)%channel    =      1  
      varsOffsets(iStd2,   iNas)%channel    =    117  
      varsOffsets(iLimit1, iNas)%channel    =      1  
      varsOffsets(iLimit2, iNas)%channel    =    117  
!
      varsOffsets(iDefault,iNas)%elevation  =     45.0  
      varsOffsets(iStd1,   iNas)%elevation  =     15.0
      varsOffsets(iStd2,   iNas)%elevation  =     85.0
      varsOffsets(iLimit1, iNas)%elevation  =      0.0
      varsOffsets(iLimit2, iNas)%elevation  =     90.0
!
