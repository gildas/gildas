!
!     Id: rangesSwTotalPower.inc,v 1.1.6 2011-07-21 Hans Ungerechts
!     Family:   rangesSw
!     Siblings: rangesSwBeam.f90
!               rangesSwFrequency.f90
!               rangesSwTotalPower.f90
!               rangesSwWobbler.f90
!
vars(iDefault)%nPhases          =       1
vars(iStd1)%nPhases             =       1
vars(iStd2)%nPhases             =       1
vars(ILimit1)%nPhases           =       1
vars(iLimit2)%nPhases           =       1
!
vars(iDefault)%tBlanking        =     0.01
vars(iStd1)%tBlanking           =     0.01
vars(iStd2)%tBlanking           =     0.01
vars(ILimit1)%tBlanking         =     0.01
vars(iLimit2)%tBlanking         =     0.01
!
vars(iDefault)%tPhase           =     1.0   !! changed according to request CK, AS 2011-07-20
!                                           !! previous value: 0.2
vars(iStd1)%tPhase              =     0.05
vars(iStd2)%tPhase              =     6.0
vars(ILimit1)%tPhase            =     0.05
vars(iLimit2)%tPhase            =     6.0
!
! ** Limits imposed by switching system (JP) / according to WB
!
vars(iDefault)%nCycles          =      1
vars(iStd1)%nCycles             =      1
vars(iStd2)%nCycles             =     10
vars(ILimit1)%nCycles           =      1
vars(iLimit2)%nCycles           =     20
!
vars(iDefault)%tRecord   =     vars(iDefault)%tPhase  *                          &
     &                         vars(iDefault)%nPhases * vars(iDefault)%nCycles  
vars(iStd1)%tRecord      =     vars(iStd1)%tPhase    *                           &
     &                         vars(iStd1)%nPhases   * vars(iStd1)%nCycles  
vars(iStd2)%tRecord      =     2
vars(ILimit1)%tRecord    =     vars(ILimit1)%tPhase  *                           &
     &                         vars(ILimit1)%nPhases * vars(ILimit1)%nCycles
vars(iLimit2)%tRecord    =     2
!
