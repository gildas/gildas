!
!  Id: inc/range/checkLimitsNoOption.inc, v 1.1.2 2010-02-24 Hans Ungerechts
!
!
ERROR = .False.
!
vStdMin   = Min(variable(iStd1),  variable(iStd2))
vStdMax   = Max(variable(iStd1),  variable(iStd2))     
vLimitMin = Min(variable(iLimit1),variable(iLimit2))
vLimitMax = Max(variable(iLimit1),variable(iLimit2))
!
If (vIn.Lt.vLimitMin .Or. vIn.Gt.vLimitMax) Then
   error = .True.
   Write (messageText,*) 'value ', vIn,  &
        &        ' outside limits ', variable(iLimit1),  &
        &        ' to ', variable(iLimit2)
   Call PakoMessage(priorityE,severityE,command//" ",messageText)
Else
   variable(iIn) = vIn
   If (vIn.Lt.vStdMin .Or. vIn.Gt.vStdMax) Then
      Write (messageText,*) 'value ', vIn,  &
           &           ' outside standard range ', variable(iStd1),  &
           &           ' to ', variable(iStd2)
      Call PakoMessage(priorityW,severityW,command//" ",messageText)
   Endif
Endif
!
