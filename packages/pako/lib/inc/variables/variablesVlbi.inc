!
!     $Id$
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varVlbi
   !
   Type(xyPointType)      ::  offset                = xyPointType(GPnoneR,GPnoneR)
   !
   Integer                ::  nSubscans             = GPnoneI
   !
   Character(len=lenCh)   ::  systemName            = GPnone          
   !
   Real                   ::  tsubscan              = GPnoneR
   !
   Logical                ::  doLastSource          = GPnoneL
   !
End Type varVlbi
!
! *** 
!
Type (varVlbi), Dimension(nDimValues) :: vars
!
Character(len=lenLineDouble) :: vlbiCommand 
!
Logical :: isInitialized = GPnoneL 
!
