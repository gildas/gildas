!
!  Id: variablesDIYlist.inc,v 1.2.4  2015-07-06 Hans Ungerechts
!  Id: variablesDIYlist.inc,v 1.2.3  2013-05-02 Hans Ungerechts
!  Id: variablesDIYlist.inc,v 1.1.14 2012-10-31 Hans Ungerechts
!
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: varDIYlist
     !
     Type(xyPointType)      ::  offset         = xyPointType(GPnoneR,GPnoneR)
     !
     Real                   ::  tsubscan       = GPnoneR
     !
     Type(xyPointType)      ::  pStart         = xyPointType(GPnoneR,GPnoneR)      ! start
     Type(xyPointType)      ::  pEnd           = xyPointType(GPnoneR,GPnoneR)      ! end
     !
     Real                   ::  lengthOtf      = GPnoneR                           ! linear length
     !
     Real                   ::  turnAngle      = 0.0                               ! turn angle for circle segment
     !
     Type(xyPointType)      ::  CPstart        = xyPointType(GPnoneR,GPnoneR)      ! start and
     Type(xyPointType)      ::  CPend          = xyPointType(GPnoneR,GPnoneR)      ! end for curve segment
     !
     Character(len=lenVar)  ::  sourceName     = GPnone
     Character(len=lenCh)   ::  altPosition    = GPnone
     !
     Character(len=lenCh)   ::  systemName     = GPnone          
     Integer                ::  iSystem        = GPnoneI
     !
     Character(len=12)      ::  altOption      = GPnone
     !
     Real                   ::  speedStart     = GPnoneR
     Real                   ::  speedEnd       = GPnoneR
     !
     Real                   ::  tOtf           = GPnoneR
     !
     ! new for Lissajous:
     !
     Type(xyPointType)      ::  pCenter        = xyPointType(GPnoneR,GPnoneR)      ! center --> xCenter yCenter
     Real                   ::  xAmplitude     = GPnoneR                           ! amplitude
     Real                   ::  yAmplitude     = GPnoneR                           !
     Real                   ::  frequencyX     = GPnoneR                           ! frequency
     Real                   ::  frequencyY     = GPnoneR                           !
     Real                   ::  omegaX         = GPnoneR                           ! frequency
     Real                   ::  omegaY         = GPnoneR                           !
     Real                   ::  phiX           = GPnoneR                           ! phase
     Real                   ::  phiY           = GPnoneR                           !
     !
     Character(len=lenCh)   ::  croFlag        = GPnone          
     !
     Character(len=lenCh)   ::  segType        = GPnone                            ! segment type
     !
     Logical                ::  doTrack        = .False.
     Logical                ::  doLinearOtf    = .False.
     Logical                ::  doCircleOtf    = .False.
     Logical                ::  doCurveOtf     = .False.
     Logical                ::  doLissajousOtf = .False.
     !
     Logical                ::  doRampUp       = .False.                           !!  ramps up/down with OTF 
     Logical                ::  doRampDown     = .False.
     Logical                ::  doInternal     = .False.                           !!  ramp inside spec. OTF
     !
     Logical                ::  doTune         = .False.                           !!  Tune NIKA etc. 
     !                                                                             !!  (option for Track subscans)
     !
     Real                   ::  tRampUp        = GPnoneR
     Real                   ::  tRampDown      = GPnoneR
     !
     Logical                ::  doNextSubscan  = .True.
     !
     Character(len=lenCh)   ::  purpose         =   GPnone
     Logical                ::  hasPurpose      =   GPNoneL  
     !
  End Type varDIYlist
  !
  ! *** 
  !
  Type (varDIYlist), Dimension(nDimValues) :: vars
  !
  Type(conditionMinMax)  ::  condElevation                                       &
       &                     = conditionMinMax("elevation",                      &
       &                                        0.0, Pi/2.0,                     &
       &                                       "rad",                            &
       &                                       .False.         )                 !
  !
  Character(len=lenLineDouble) :: DIYlistCommand 
  Character(len=lenLineDouble) :: SubscanCommand 
  !
  Logical :: isInitialized = GPnoneL 
  !
  Logical               :: isConnected   = .False.
  Character (len=lenCh) :: bolometerName
  !
  Logical :: doClear
  Logical :: doWarnOffsets
  !
  ! *** derived parameters for Lissajous and Ramps
  !
  Integer                ::  nLissajous = GPnoneI
  !
  Type(xyPointType)      ::  pAccel     = xyPointType(GPnoneR,GPnoneR)             ! accelerate from
  Type(xyPointType)      ::  pStart     = xyPointType(GPnoneR,GPnoneR)             ! start Lisasajous
  Type(xyPointType)      ::  pEnd       = xyPointType(GPnoneR,GPnoneR)             ! end   Lissajous
  Type(xyPointType)      ::  pStop      = xyPointType(GPnoneR,GPnoneR)             ! slow down to
  !
  Real                   ::  lAccel     = GPnoneD                                  ! linear length of accel
  Real                   ::  lStop      = GPnoneD                                  ! linear length of slow down
  !
  Type(xyVelocityType)   ::  vStart     = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  Type(xyVelocityType)   ::  vEnd       = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  !
  Real                   ::  tAccel     = GPnoneR
  Real                   ::  aAccel     = GPnoneR
  !
  Real                   ::  tStop      = GPnoneR
  Real                   ::  astop      = GPnoneR
  !
  Real                   ::  accelMax
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)             !  !!  values could be reset in 
  !                                                                              !  !!  ranges .inc
