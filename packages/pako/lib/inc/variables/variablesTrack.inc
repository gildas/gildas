  !
  ! Id: variablesTrack.inc,v 1.2.4 2015-07-06 Hans Ungerechts
  ! Id: variablesTrack.inc,v 1.1   2006-07-13 Hans Ungerechts
  !
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: varTrack
     !
     Type(xyPointType)      ::  offset       = xyPointType(GPnoneR,GPnoneR)
     !
     Character(len=lenVar)  ::  sourceName   = GPnone
     Character(len=lenCh)   ::  altPosition  = GPnone
     !
     Integer                ::  nSubscans    = GPnoneI
     !
     Character(len=lenCh)   ::  systemName   = GPnone
     Integer                ::  iSystem      = GPnoneI
     !
     Real                   ::  tsubscan     = GPnoneR
     !
     Real                   ::  trecord      = GPnoneR
     !
  End Type varTrack
  !
  ! *** 
  !
  Type (varTrack), Dimension(nDimValues) :: vars
  !
  Character(len=lenLineDouble) :: trackCommand 
  !
  Logical :: isInitialized = GPnoneL
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)             !  !!  values could be reset in 
  !                                                                              !  !!  ranges .inc
