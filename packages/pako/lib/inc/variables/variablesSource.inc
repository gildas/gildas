!
!  Id: variablesSource.inc,v 1.2.4  2015-05-20 Hans Ungerechts
!  Id: variablesSource.inc,v 1.2.3  2014-10-16 Hans Ungerechts
!  Id: variablesSource.inc,v 1.1.14 2012-10-31 Hans Ungerechts
!
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: coordinate
     !
     Character(len=lenCh)   :: name="none"
     Character(len=lenCh)   :: unit="none"
     Character(len=lenCh)   :: text="none"
     Real(kind=kindDouble)  :: hour=0.0
     Real(kind=kindDouble)  :: deg=0.0
     Real(kind=kindDouble)  :: rad=0.0
     !
  End Type coordinate
  !
  Type :: radialVelocity
     !
     Character(len=lenCh)   :: name="none"
     Character(len=lenCh)   :: Type="none"
     Character(len=lenCh)   :: unit="none"
     Real(kind=kindDouble)  :: value=0.0D0
     !
  End Type radialVelocity
  !
  Type :: varSource
     !
     Character(len=lenVar)  :: sourceCatalog   =  GPnone                           ! catalog file name
     !
     Character(len=lenCh)   :: sourceName="none"
     Logical                :: isBody=.False.
     Logical                :: isPlanet=.False.
     Logical                :: isSatellite=.False.
     Logical                :: isTipCurAzi=.False.
     Character(len=lenCh)   :: systemName="none"
     Character(len=lenCh)   :: equinoxSystemName = "none"
     Real                   :: epoch=0.0
     Type (coordinate)      :: longitude
     Type (coordinate)      :: latitude
     Type (radialVelocity)  :: velocity
     Real(kind=kindSingle)  :: flux
     Real(kind=kindSingle)  :: index
     !
     Real(kind=kindDouble)  :: perihelionEpoch        = 0.0D0
     Real(kind=kindDouble)  :: ascendingNode          = 0.0D0
     Real(kind=kindDouble)  :: argumentOfPerihelion   = 0.0D0
     Real(kind=kindDouble)  :: inclination            = 0.0D0
     Real(kind=kindDouble)  :: perihelionDistance     = 0.0D0
     Real(kind=kindDouble)  :: eccentricity           = 0.0D0
     !
     Character(len=lenCh)   :: projectID = GPnone
     !
  End Type varSource
  !
  Type (varSource), Dimension(nDimValues)      :: vars                             !   for command handling
  ! 
  Type (varSource), Dimension(nDimOMChoices)   :: OMsource                         !   to store sources "for" observingmodes
  !
  ! *** Offsets:
  !     NOTE: nDimOffsetChoices is in GlobalParameters.f90
  Type (varOffsets), Dimension(nDimValues, nDimOffsetChoices) :: varsOffsets
  !
  Type (varOffsets)         :: noOffsets
  Integer                   :: iiSystemOffset  = 0
  Character(len=lenCh)      :: ccSystemOffset  = "none"
  Logical                   :: doClear         = .False.
  !
  Logical                   :: isInitialized=.False.
  Logical                   :: catalogOpen=.False.
  !
  !! The following 3 are deprecated
  Character(len=lenCh), Dimension(6)  :: velocityTypeChoices="none"
  Character(len=lenCh), Dimension(9)  :: planetNameChoices="none"
  Character(len=lenCh), Dimension(8)  :: satelliteNameChoices="none"
  !
  Character(len=lenLineDouble)        :: sourceCommand 
  !
  Logical :: doWarnOffsets, doWarnOM
  !
  ! *** "helper" variables:
  !
  Character(len=lenVar)     :: cFile                                             !   !!  catalog file name
  Character(len=lenVar)     :: fileName, fileBaseName, fileExt, filePath         !
  !
  Character(len=lenCh)      :: name
  Integer                   :: lname
  !
  Real(kind=kindSingle)     :: epoch
  Real(kind=kindDouble)     :: lambda, beta
  Real(kind=kindDouble)     :: velo
  Real(kind=kindSingle)     :: flux, fluxIndex
  !
  Real(kind=kindSingle)     :: lambda1, lambda2
  Real(kind=kindSingle)     :: beta1, beta2
  Real(kind=kindSingle)     :: vr1, vr2
  !
  Integer                   :: larg
  !
  Logical                   :: found
  Integer                   :: npar, npar1
  Integer                   :: ier
  !
  Logical               :: isConnected   = .False.
  Character (len=lenCh) :: bolometerName
  !
