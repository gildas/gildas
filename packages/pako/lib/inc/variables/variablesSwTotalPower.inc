!
!  Id: variablesSwTotalPower.inc,v 1.0.5 2006-10-19 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varSwTotalPower
   !
   Integer                                 ::  nPhases      = GPnoneI
   Real                                    ::  tBlanking    = GPnoneR
   Real                                    ::  tPhase       = GPnoneR
   Real                                    ::  tRecord      = GPnoneR
   Integer                                 ::  nCycles      = GPnoneI
   !
End Type varSwTotalPower
!
Type (varSwTotalPower), Dimension(nDimValuesS) :: vars
!
Logical                          :: isInitialized=.False.
!
Character(len=lenLineDouble)     :: swTotalPowerCommand 
