!
!     $Id$
!
! *** arguments for command handler subroutines like "Backend" ***
Character(len=*),   Intent(in)   :: programName
Character(len=*),   Intent(in)   :: line
Character(len=*),   Intent(in)   :: command
Logical,            Intent(out)  :: ERROR
!
! *** functions called ***
Logical  :: SIC_PRESENT
Integer  :: SIC_NARG
!
