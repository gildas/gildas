  !
  !  Id: variablesPointing.inc,v 1.2.4  2015-07-06 Hans Ungerechts
  !  Id: variablesPointing.inc,v 1.2.3  2014-09-23 Hans Ungerechts
  !  Id: variablesPointing.inc,v 1.1.12 2012-02-27 Hans Ungerechts
  !
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: varPointing
     !
     Real                  :: length       =  GPnoneR
     !
     Logical               :: doCalibrate  =  GPnoneL          
     Integer               :: feBe         =  GPnoneI
     Logical               :: doDoubleBeam =  GPnoneL          
     Logical               :: doMore       =  GPnoneL          
     Integer               :: nOtf         =  GPnoneI          
     !
     Real                 ::  speedStart   =  GPnoneR   !!   speed: only derived from /TOTF tOtf   
     Real                 ::  speedEnd     =  GPnoneR   
     !                                 
     Real                  :: tOtf         =  GPnoneR   
     Real                  :: tRecord      =  GPnoneR          
     Logical               :: doUpdate     =  GPnoneL
     !
     Character(len=lenCO)  :: altOption    =  GPnone
     Character(len=lenVar) :: aeCode       =  GPnone
     Character(len=lenVar) :: aeCodeBites  =  GPnone
     Integer               :: nAeCode      =  GPnoneI
     !
     Logical              ::  doTune                = .False.
     Logical              ::  doTuneset             = .False.
     Type(xyPointType)    ::  offsetTune            !!  
     Real                 ::  tTune                 = 0.0
     !
  End Type varPointing
  !
  ! *** 
  !
  Type (varPointing), Dimension(nDimValues) :: vars
  !
  Character(len=lenLineDouble) :: pointingCommand 
  !
  Logical :: isInitialized = .False. 
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)             !  !!  values could be reset in 
  !                                                                              !  !!  ranges .inc
