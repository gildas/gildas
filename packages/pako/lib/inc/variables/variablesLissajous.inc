  !
  ! Id: variablesLissajous.inc,v 1.2.4  2015-07-06 Hans Ungerechts
  ! Id: variablesLissajous.inc,v 1.2.3  2014-08-28 Hans Ungerechts
  ! Id: variablesLissajous.inc,v 1.2.3  2014-07-08 Hans Ungerechts
  ! Id: variablesLissajous.inc,v 1.2.3  2014-05-27 Hans Ungerechts
  ! Id: variablesLissajous.inc,v 1.1.12 2012-03-02 Hans Ungerechts
  !
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: varLissajous
     !
     Type(xyPointType)    ::  amplitude
     Type(xyPointType)    ::  frequency
     Type(xyPointType)    ::  phase
     Type(xyPointType)    ::  pCenter
     !
     Character(len=100)   ::  croCode               = 'none'
     Character(len=100)   ::  croCodeBites          = 'none'
     Integer              ::  croCodeCount          = 0
     !
     Logical              ::  doRampUp              = .False.
     Real                 ::  tRampUp               = 0.0
     Logical              ::  doRampDown            = .False.
     Real                 ::  tRampDown             = 0.0
     !
     Logical              ::  doReference           = .False.         
     Type(xyPointType)    ::  offsetR
     Character(len=12)    ::  systemNameRef         = 'none'          
     Character(len=100)   ::  listOfNamesRef        = 'none' 
     Character(len=12)    ::  altRef                = 'none' 
     !
     Character(len=12)    ::  systemName            = 'none'          
     Integer              ::  iSystem               = GPnoneI
     !                                                  
     Real                 ::  tOtf                  = 0.0
     Real                 ::  tRecord               = 0.0
     Real                 ::  tReference            = 0.0
     !                                 
     Type(xyPointType)    ::  omega                 !!  derived from frequency
     !
     Logical              ::  doTune                = .False.
     Logical              ::  doTuneset             = .False.
     Type(xyPointType)    ::  offsetTune            !!  
     Real                 ::  tTune                 = 0.0
     !
     Integer              ::  nFocus                =  0
     Real, Dimension(18)  ::  focusList
     Logical              ::  doFocus               = .False.
     Character(len=12)    ::  directionFocus        = 'Z'
     !
     Logical              ::  doPointing            = .False.
     !
  End Type varLissajous
  !
  ! *** 
  Type (varLissajous), Dimension(nDimValues) :: vars
  !
  Integer, Parameter                         :: nFocusMax  =  18                         ! maximum number of focus values
  !
  Type :: limitsLissajous
     !
     Real                :: focus                                    ! limits for focus values
     !
  End Type limitsLissajous
  !
  Type (limitsLissajous), Dimension(nDimValues)  :: limits
  !
  Character(len=512)                 :: LissajousCommand 
  !
  Logical                            :: isInitialized = .False. 
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)             !  !!  values could be reset in 
  !                                                                              !  !!  ranges .inc
  ! *** 
  Character(len=1),  Dimension(3)    :: croCodeChoices
  Character(len=12), Dimension(7)    :: systemNameRefChoices
  !
  Type(xyPointType)      ::  pAccel         = xyPointType(GPnoneR,GPnoneR)   ! accelerate from
  Type(xyPointType)      ::  pStart         = xyPointType(GPnoneR,GPnoneR)   ! start Lisasajous
  Type(xyPointType)      ::  pEnd           = xyPointType(GPnoneR,GPnoneR)   ! end   Lissajous
  Type(xyPointType)      ::  pStop          = xyPointType(GPnoneR,GPnoneR)   ! slow down to
  !
  Type(xyVelocityType)   ::  vStart         = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  Type(xyVelocityType)   ::  vEnd           = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  !
  Real                   ::  tAccel         = GPnoneR
  Real                   ::  aAccel         = GPnoneR
  !
  Real                   ::  tStop          = GPnoneR
  Real                   ::  astop          = GPnoneR
  !
  Real                   ::  accelMax
  !



