!
!  Id: configurePlotMethods.inc,v 1.1.13 2012-10-18 Hans Ungerechts
!
! **  Cal Sky
!
pCalNpen        =  2
pCalCharSize    = 0.2
pCalMarker      = '4 1 0.3 45.0'
pCalChar        = 'C'
!
! **  Pointing
!
pPointNpen      =  2
pPointCharSize  = 0.2
pPointMarker    = '4 1 0.3 45.0'
pPointChar      = 'P'
!
! **  ON
!
pOnNpen         =  3
pOnCharSize     = 0.1
pOnMarker       = '4 1 0.15 45.0'
pOnChar         = 'O'
!
! **  Track /tune
!
pTuneNpen       =  2
pTuneCharSize   = 0.3
pTuneMarker     = '4 0 0.3 0.0'
pTuneChar       = 'T'
!
! **  OFF-SOURCE REFERENCE
!
pRefNpen        =  4
pRefCharSize    = 0.3
pRefMarker      = '4 1 0.3 45.0'
pRefChar        = 'R'
!
! **  Control Points
!
pCpNpen         =   6
pCpCharSize     = 0.2
pCpMarker       = '4 0 0.2 45.0'
pCpChar         = 'CP'
!
! **  OTF
!
pOtfNpen        =  3
pOtfCharSize    = 0.2
pOtfMarker      = '4 1 0.3 45.0'
pOtfChar        = 'O'
!
! **  OTF Curves
!
pCurveNpen        =  3
pCurveCharSize    = 0.2
pCurveMarker      = '4 0 0.1 0.0'
pCurveChar        = 'O'
!
! **  Dropin
!
pDropNpen         =  2
pDropCharSize     = 0.2
pDropMarker       = '4 0 0.1 0.0'
pDropChar         = 'O'
!
! **  Receiver Pixels
!
pPixNpen         =   1
pPixCharSize     = 0.2
pPixMarker       = '4 1 0.15 45.0'
pPixChar         = 'Pix'
!
