!
!  Id: variablesSwWobbler.inc,v 1.0.5 2006-10-19 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varSwWobbler
   !
   Real, Dimension(2)                      ::  wOffset      = GPnoneR
   Real                                    ::  wThrow       = GPnoneR
   Real                                    ::  wAngle       = GPnoneR
   Integer                                 ::  nPhases      = GPnoneI
   Real                                    ::  tBlanking    = GPnoneR
   Real                                    ::  tPhase       = GPnoneR
   Real                                    ::  tRecord      = GPnoneR
   Integer                                 ::  nCycles      = GPnoneI
   !
End Type varSwWobbler
!
Type (varSwWobbler), Dimension(nDimValuesS) :: vars
!
Logical                                     :: isInitialized=.False.
!
Character(len=lenLineDouble)                :: swWobblerCommand
!

