!
! Id: variablesOtfMap.inc,v 1.2.4 2015-07-06 Hans Ungerechts
! Id: variablesOtfMap.inc,v 1.2.3 2014-09-29 Hans Ungerechts
! Id: variablesOtfMap.inc,v 1.2.3 2014-08-28 Hans Ungerechts
! Id: variablesOtfMap.inc,v 1.2.3 2014-07-18 Hans Ungerechts
!                           1.2.3 2014-05-27 Hans Ungerechts
!                           1.0.4 2006-07-13 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varOtfMap
   !
   Type(xyPointType)    ::  pStart 
   Type(xyPointType)    ::  pEnd
   Real                 ::  lengthOtf             = 0.0
   !
   Logical              ::  doBalance             = .False.
   !
   Character(len=100)   ::  croCode               = 'none'
   Character(len=100)   ::  croCodeBites          = 'none'
   Integer              ::  croCodeCount          = 0
   !
   Integer              ::  nOtf                  = 0
   !
   Logical              ::  doReference           = .False.         
   Type(xyPointType)    ::  offsetR
   Character(len=12)    ::  systemNameRef         = 'none'          
   Character(len=100)   ::  listOfNamesRef        = 'none' 
   Character(len=12)    ::  altRef                = 'none' 
   !
   Logical              ::  doResume              = .False.
   Integer              ::  nResume               = 0
   Logical              ::  doResumeCalibrate     = .False.
   Logical              ::  doResumeReference     = .False.
   !
   Type(xyPointType)    ::  delta
   !
   Character(len=12)    ::  systemName            = 'none'          
   Integer              ::  iSystem               = GPnoneI
   !                                                  
   Character(len=12)    ::  altOption             = 'none'       
   !
   Real                 ::  speedStart            = 0.0
   Real                 ::  speedEnd              = 0.0
   !                                 
   Real                 ::  tOtf                  = 0.0
   !                                 
   Real                 ::  tRecord               = 0.0
   !                                 
   Real                 ::  tReference            = 0.0
   !                                 
   Logical              ::  doZigzag              = .False.
   !
   Logical              ::  doTune                = .False.
   Logical              ::  doTuneset             = .False.
   Type(xyPointType)    ::  offsetTune            !!  
   Real                 ::  tTune                 = 0.0
   !
End Type varOtfMap
!
! *** 
Type (varOtfMap), Dimension(nDimValues) :: vars
!
Character(len=512)                 :: otfMapCommand 
!
Logical                            :: isInitialized = .False. 
!
! *** 
Character(len=1),  Dimension(3)    :: croCodeChoices
Character(len=12), Dimension(7)    :: systemNameRefChoices
!!      Character(len=12), Dimension(7)    :: systemNameChoices
!
! *** extended time limits
!
Type :: realLimits
   Real                 ::  min
   Real                 ::  max
End type realLimits
Type (realLimits)       ::  timeExtended = realLimits(1.0, 86400.0)              !  !!  values could be reset in 
!                                                                                !  !!  rangesOtfMap.inc
  !
  ! *** derived parameters for Lead-ins
  !
  Type(xyPointType)      ::  pLead      = xyPointType(GPnoneR,GPnoneR)             ! lead-in from
  !
  Real                   ::  lLead      = GPnoneD                                  ! linear length of lead 
  !
  Real                   ::  tLead      = GPnoneR
  !
  ! *** derived parameters for Ramps
  !
  Type(xyPointType)      ::  pAccel     = xyPointType(GPnoneR,GPnoneR)             ! accelerate from
  Type(xyPointType)      ::  pStart     = xyPointType(GPnoneR,GPnoneR)             ! start 
  Type(xyPointType)      ::  pEnd       = xyPointType(GPnoneR,GPnoneR)             ! end   
  Type(xyPointType)      ::  pStop      = xyPointType(GPnoneR,GPnoneR)             ! slow down to
  !
  Real                   ::  lAccel     = GPnoneD                                  ! linear length of accel
  Real                   ::  lStop      = GPnoneD                                  ! linear length of slow down
  !
  Type(xyVelocityType)   ::  vStart     = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  Type(xyVelocityType)   ::  vEnd       = xyVelocityType(GPnoneR,GPnoneR,GPnoneR)
  !
  Real                   ::  tAccel     = GPnoneR
  Real                   ::  aAccel     = GPnoneR
  !
  Real                   ::  tStop      = GPnoneR
  Real                   ::  astop      = GPnoneR
  !
  Real                   ::  accelMax


