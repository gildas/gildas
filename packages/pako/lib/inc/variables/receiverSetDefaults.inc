!
! Id: receiverSetDefaults.inc,v 1.0.2 2006-02-11 Hans Ungerechts
!
!***  
!
  !D        Write (6,*) "      -> receiverSetDefaults.inc "
!
If (SIC_PRESENT(0,1) .And. isInitialized) Then
   !
!!             Write (6,*) "         iRec ", iRec
   vars(iIn,iRec)    = vars(iDefault,iRec)
   vars(iTemp,iRec)  = vars(iDefault,iRec)
   vars(iValue,iRec) = vars(iDefault,iRec)
   !
Else
   !
!!             Write (6,*) "         all "
   Do ii = 1,nDimReceivers,1
      !
!!            Write (6,*) ii, vars(iDefault,ii)%isConnected
      vars(iIn,ii)    = vars(iDefault,ii)
      vars(iTemp,ii)  = vars(iDefault,ii)
      vars(iValue,ii) = vars(iDefault,ii)
   End Do
   !
End If
!
heraPixel(1:nDimHeraPixel)%offsets    =  xyPointType(0.0,0.0)
heraPixel(1:nDimHeraPixel)%gainImage  =  gainRatio(GPnone,GPnone,GPnone,0.1,-10.0)
!
heraPixel(1)%offsets    =  xyPointType(-24.0,-24.0)
heraPixel(2)%offsets    =  xyPointType(-24.0,  0.0)
heraPixel(3)%offsets    =  xyPointType(-24.0, 24.0)
!
heraPixel(4)%offsets    =  xyPointType(  0.0,-24.0)
heraPixel(5)%offsets    =  xyPointType(  0.0,  0.0)
heraPixel(6)%offsets    =  xyPointType(  0.0, 24.0)
!
heraPixel(7)%offsets    =  xyPointType( 24.0,-24.0)
heraPixel(8)%offsets    =  xyPointType( 24.0,  0.0)
heraPixel(9)%offsets    =  xyPointType( 24.0, 24.0)
!
Do ii = 1,9,1
   heraPixel(ii+9)%offsets    =  heraPixel(ii)%offsets
End Do
