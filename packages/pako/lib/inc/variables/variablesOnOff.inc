!
!  Id: variablesOnOff.inc,v 1.2.4 2015-07-06 Hans Ungerechts
!  Id: variablesOnOff.inc,v 1.0.2 2006-01-05 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varOnOff
   !
   Type(xyPointType)      ::  offset                = xyPointType(GPnoneR,GPnoneR)
   !
   Character(len=lenVar)  ::  sourceName            = GPnone
   Character(len=lenCh)   ::  altPosition           = GPnone
   !
   Logical                ::  doBalance             = GPnoneL           
   !
   Logical                ::  doCalibrate           = GPnoneL
   !
   Integer                ::  nSubscans             = GPnoneI
   !
   Logical                ::  doReference           = GPnoneL       
   Type(xyPointType)      ::  offsetR
   Character(len=lenCh)   ::  systemNameRef         = GPnone          
   Character(len=lenVar)  ::  listOfNamesRef        = GPnone 
   Character(len=lenCh)   ::  altRef                = GPnone 
   !
   Character(len=lenCh)   ::  systemName            = GPnone          
   Integer                ::  iSystem               = GPnoneI
   !
   Logical                ::  doSymmetric           = GPnoneL
   !
   Logical                ::  doSwWobbler           = GPnoneL
   !
   Real                   ::  tsubscan              = GPnoneR
   !
End Type varOnOff
!
! *** 
!
Type (varOnOff), Dimension(nDimValues) :: vars
!
Character(len=lenLineDouble) :: onOffCommand 
!
Logical :: isInitialized = GPnoneL 
!
! *** extended time limits
!
Type :: realLimits
   Real                 ::  min
   Real                 ::  max
End type realLimits
Type (realLimits)       ::  timeExtended = realLimits(1.0, 86400.0)              !  !!  values could be reset in 
!                                                                                !  !!  ranges .inc
Logical :: doWarnOffsets
! *** 
! TBD: replace the folowing by global parameters
Character(len=lenCh), Dimension(7) :: systemNameRefChoices
