!
!     $Id$
!
      Include 'headerForVariables.f90'
!
! *** for most variables we can use the subscanType:
! *** 
      Type (subscanType), Dimension(nDimValues) :: vars
!
! *** additional variables go into varsEtc of type varSegment:
! ***
      Type :: varSegment      
!
         Logical               ::  doWriteToSeg     = .False.
         Logical               ::  doneWriteToSeg   = .False.
!
      End Type varSegment
!
      Type(varSegment),   Dimension(nDimValues) :: varsEtc
!
      Character(len=512)                 :: segmentCommand 
!
      Logical                            :: isInitialized = .False. 
!
! *** 


