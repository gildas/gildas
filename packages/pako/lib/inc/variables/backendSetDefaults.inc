!
!
!  Id: backendSetDefaults.inc ,v 1.3.0  2017-09-06     Hans Ungerechts
!                             ,v 1.2.3  2013-09-20     Hans Ungerechts
!                             ,v 1.2.3  2013-08-27, 28 Hans Ungerechts
!                                       upgrade of E150 to H&V 2SB LO LI UI UO
!
!  Id: backendSetDefaults.inc ,v 1.2.2  2012-08-22 Hans Ungerechts
!      backendSetDefaults.inc ,v 1.2.1  2012-07-18 Hans Ungerechts
!      based on:
!      backendSetDefaults.inc ,v 1.1.11 2011-11-14 Hans Ungerechts
!
! *** 
!
Call pako_message(seve%t,programName,                                            &
     &          "    -->  backendSetDefaults.inc ,v 1.3.0 2017-09-06")           ! this allows to trace execution
!
If (GV%doDebugMessages) Then
   Write (6,*) " "
   Write (6,*) "         receiverIsEMIR:  ", receiverIsEMIR
   Write (6,*) "         iBack:           ",          iBack
   Write (6,*) "         backendSelected: ", backendSelected
   Write (6,*) "         fineFTS:         ", fineFTS
End If
!
If (iBack.Lt.1) Then
   listBE(:,:)          = beReset
   vars(:,iBBC)%samplesIsSet   = GPnoneL
   vars(:,iBBC)%nSamples       = GPnoneI
   vars(:,iVESPA)%samplesIsSet = GPnoneL
   vars(:,iVESPA)%nSamples     = GPnoneI
   vars(:,iWILMA)%samplesIsSet = GPnoneL
   vars(:,iWILMA)%nSamples     = GPnoneI
End If
!
!D Write (6,*) "  vars(:,iBBC)%samplesIsSet:   ", vars(:,iBBC)%samplesIsSet
!D Write (6,*) "  vars(:,iBBC)%nSamples:       ", vars(:,iBBC)%nSamples
!D Write (6,*) "  vars(:,iVESPA)%samplesIsSet: ", vars(:,iVESPA)%samplesIsSet
!D Write (6,*) "  vars(:,iVESPA)%nSamples:     ", vars(:,iVESPA)%nSamples
!D Write (6,*) "  vars(:,iWILMA)%samplesIsSet: ", vars(:,iWILMA)%samplesIsSet
!D Write (6,*) "  vars(:,iWILMA)%nSamples:     ", vars(:,iWILMA)%nSamples
!
If      ( receiverIsEMIR .And.                                                   &
     &                   (      backendSelected.Eq.bac%Continuum                 &
     &                                                           )               &
     &  ) Then                                                                   !
   !
   Write (messageText,*) "paKo sets ", bac%Continuum,                            &
        &                " for EMIR subbands requested with RECEIVER"            !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   listBE(iContinuum,:)          = beReset
   !
   Do ii = 1, nEsbIF, 1
      If (EMIRsubBands(ii)%isRequested) Then
         !
         listBE(iContinuum,ii)               =   vars(iDefault,iContinuum)
         listBE(iContinuum,ii)%isConnected   =  .True.
         listBE(iContinuum,ii)%nPart         =   ii
         listBE(iContinuum,ii)%receiverName  =   EMIRsubBands(ii)%bandName
         listBE(iContinuum,ii)%polarization  =   EMIRsubBands(ii)%polarization
         listBE(iContinuum,ii)%subband       =   EMIRsubBands(ii)%subband
         !
         iBack = iContinuum
         !
         !
         If ( listBE(iContinuum,ii)%subBand .Eq. esb%lo ) Then
            fShiftForced = fRangeSky(iBack,iLO,iCenter)                          &
                 &       - fRangeSky(iVESPA,iLO,iCenter)                         !
         Else If ( listBE(iContinuum,ii)%subBand .Eq. esb%li ) Then                          
            fShiftForced = fRangeSky(iBack,iLI,iCenter)                          &
                 &       - fRangeSky(iVESPA,iLI,iCenter)                         !
         Else If ( listBE(iContinuum,ii)%subBand .Eq. esb%ui ) Then                          
            fShiftForced = fRangeSky(iBack,iUI,iCenter)                          &
                 &       - fRangeSky(iVESPA,iUI,iCenter)                         !
         Else If ( listBE(iContinuum,ii)%subBand .Eq. esb%uo ) Then                          
            fShiftForced = fRangeSky(iBack,iUO,iCenter)                          &
                 &       - fRangeSky(iVESPA,iUO,iCenter)                         !
         End If
         !
         listBE(iContinuum,ii)%fShift = fShiftForced
         !
      End If
   End Do
   !
Else If ( receiverIsEMIR .And.                                                   &
     &                   (      backendSelected.Eq.bac%BBC                       &
     &                     .Or. iBack.Lt.1                       )               &
     &  ) Then                                                                   !
   !
   Write (messageText,*) "paKo sets ", bac%BBC,                                  &
        &                " for EMIR "                                            !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   !D    Write (6,*) " beReset "
   !D    Write (6,*)   beReset 
   !
   listBE(iBBC,:)          = beReset
   vars  (iValue,iBBC)     = beReset
   !
   !D    Write (6,*) "   vars(:,iBBC)%samplesIsSet:   ", vars(:,iBBC)%samplesIsSet
   !D    Write (6,*) "   vars(:,iBBC)%nSamples:       ", vars(:,iBBC)%nSamples
   !
   jj = 0
   !
!!$   Do ii = iE230, iE300   !! before E150 upgrade 2013-09
   Do ii = iE090, iE300   !! for   E150 upgrade 2013-09
      !                   !! DONE: for EMIR upgrade: 
      !                   !!       -- do ii = iE230, iE300
      !                   !!       -- bandwidth 8000
      !                   !!       -- subbands: LSB USB
      !                   !!       -- fShift 0
      !
      Call queryReceiver(receiverChoices(ii), Result)
      !
      If (Result) Then
         !
         jj = jj+1
         !
         listBE(iBBC,jj)               =   vars(iDefault,iBBC)
         listBE(iBBC,jj)%isConnected   =  .True.
         listBE(iBBC,jj)%nPart         =   jj
         listBE(iBBC,jj)%bandWidth     =   8000.0
         listBE(iBBC,jj)%resolution    =   listBE(iBBC,jj)%bandWidth
         listBE(iBBC,jj)%receiverName  =   receiverChoices(ii)
         listBE(iBBC,jj)%polarization  =   pol%hor
         listBE(iBBC,jj)%subband       =   esb%LSB
         !
         jj = jj+1
         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
         listBE(iBBC,jj)%nPart         =   jj
         listBE(iBBC,jj)%polarization  =   pol%hor
         listBE(iBBC,jj)%subband       =   esb%USB
         !
         jj = jj+1
         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
         listBE(iBBC,jj)%nPart         =   jj
         listBE(iBBC,jj)%polarization  =   pol%ver
         listBE(iBBC,jj)%subband       =   esb%LSB
         !
         jj = jj+1
         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
         listBE(iBBC,jj)%nPart         =   jj
         listBE(iBBC,jj)%polarization  =   pol%ver
         listBE(iBBC,jj)%subband       =   esb%USB
         !
      End If
      !
   End Do   !!   ii = iE090, iE300 
   !
!!$   Do ii = iE090, iE090 
!!$      !
!!$      Call queryReceiver(receiverChoices(ii), Result)
!!$      !
!!$      If (ii.Eq.iE090 .And. Result) Then
!!$         !
!!$         jj = jj+1
!!$         !
!!$         listBE(iBBC,jj)               =   vars(iDefault,iBBC)
!!$         listBE(iBBC,jj)%isConnected   =  .True.
!!$         listBE(iBBC,jj)%nPart         =   jj
!!$         listBE(iBBC,jj)%bandWidth     =   8000.0
!!$         listBE(iBBC,jj)%resolution    =   listBE(iBBC,jj)%bandWidth
!!$         listBE(iBBC,jj)%receiverName  =   receiverChoices(ii)
!!$         listBE(iBBC,jj)%polarization  =   pol%hor
!!$         listBE(iBBC,jj)%subband       =   esb%LSB
!!$         !
!!$         jj = jj+1
!!$         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
!!$         listBE(iBBC,jj)%nPart         =   jj
!!$         listBE(iBBC,jj)%polarization  =   pol%hor
!!$         listBE(iBBC,jj)%subband       =   esb%USB
!!$         !
!!$         jj = jj+1
!!$         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
!!$         listBE(iBBC,jj)%nPart         =   jj
!!$         listBE(iBBC,jj)%polarization  =   pol%ver
!!$         listBE(iBBC,jj)%subband       =   esb%LSB
!!$         !
!!$         jj = jj+1
!!$         listBE(iBBC,jj)               =   listBE(iBBC,jj-1)
!!$         listBE(iBBC,jj)%nPart         =   jj
!!$         listBE(iBBC,jj)%polarization  =   pol%ver
!!$         listBE(iBBC,jj)%subband       =   esb%USB
!!$         !
!!$      End If
!!$      !
!!$   End Do
   !
!!!
!!!             obsolete with EMIR E150 upgrade 2013-09:
!!$   Do ii = 1, nEsbIF, 1
!!$      !
!!$      If     ( EMIRsubBands(ii)%isRequested                                      &
!!$           &   .And. (      EMIRsubBands(ii)%bandName .Eq. rec%E150              &
!!$           &         )                                                           &
!!$           & ) Then                                                              !
!!$         !
!!$         jj = jj+1
!!$         !
!!$         listBE(iBBC,jj)               =   vars(iDefault,iBBC)
!!$         listBE(iBBC,jj)%isConnected   =  .True.
!!$         listBE(iBBC,jj)%nPart         =   jj
!!$         listBE(iBBC,jj)%bandWidth     =   4000.0
!!$         listBE(iBBC,jj)%resolution    =   listBE(iBBC,jj)%bandWidth
!!$         listBE(iBBC,jj)%receiverName  =   EMIRsubBands(ii)%bandName
!!$         listBE(iBBC,jj)%polarization  =   EMIRsubBands(ii)%polarization
!!$         listBE(iBBC,jj)%subband       =   EMIRsubBands(ii)%subband
!!$         !
!!$         iBack = iContinuum
!!$         !
!!$         If (      listBE(iBBC,jj)%subBand .Eq. esb%lo ) Then
!!$            fShiftForced = fRangeSky(iBack,iLO,iCenter)                          &
!!$                 &       - fRangeSky(iVESPA,iLO,iCenter)                         !
!!$         Else If ( listBE(iBBC,jj)%subBand .Eq. esb%li ) Then
!!$            fShiftForced = fRangeSky(iBack,iLI,iCenter)                          &
!!$                 &       - fRangeSky(iVESPA,iLI,iCenter)                         !
!!$         Else If ( listBE(iBBC,jj)%subBand .Eq. esb%ui ) Then
!!$            fShiftForced = fRangeSky(iBack,iUI,iCenter)                          &
!!$                 &       - fRangeSky(iVESPA,iUI,iCenter)                         !
!!$         Else If ( listBE(iBBC,jj)%subBand .Eq. esb%uo ) Then
!!$            fShiftForced = fRangeSky(iBack,iUO,iCenter)                          &
!!$                 &       - fRangeSky(iVESPA,iUO,iCenter)                         !
!!$         End If
!!$         !
!!$         listBE(iBBC,jj)%fShift = fShiftForced
!!$         !
!!$         If      (listBE(iBBC,jj)%polarization .Eq. pol%hor) Then
!!$            otherPolarization = pol%ver
!!$         Else If (listBE(iBBC,jj)%polarization .Eq. pol%ver) Then
!!$            otherPolarization = pol%hor
!!$         End If
!!$         !
!!$         Call queryReceiver(               listBE(iBBC,jj)%receiverName,         &
!!$              &             IsConnected  = IsConnectedRX,                        &
!!$              &             polarization = otherPolarization,                    &
!!$              &             subband      = listBE(iBBC,jj)%subband     )         !
!!$         !
!!$         If (GV%doDebugMessages) Then
!!$            !
!!$            Write (6,*)  " "
!!$            Write (6,*)  "      listBE(iBBC,jj)%mode         : ", listBE(iBBC,jj)%mode
!!$            Write (6,*)  "      POLA"
!!$            Write (6,*)  "      listBE(iBBC,jj)%fShift       : ", listBE(iBBC,jj)%fShift      
!!$            Write (6,*)  "      listBE(iBBC,jj)%receiverName : ", listBE(iBBC,jj)%receiverName
!!$            Write (6,*)  "      listBE(iBBC,jj)%polarization : ", listBE(iBBC,jj)%polarization
!!$            Write (6,*)  "      listBE(iBBC,jj)%subBand      : ", listBE(iBBC,jj)%subBand
!!$            Write (6,*)  "      otherPolarization            : ", otherPolarization
!!$            Write (6,*)  "      IsConnectedRX                : ", IsConnectedRX
!!$            !
!!$         End If
!!$         !                
!!$         If (.Not. IsConnectedRX ) Then
!!$            !
!!$            jj = jj+1
!!$            !
!!$            listBE(iBBC,jj)               =   vars(iDefault,iBBC)
!!$            listBE(iBBC,jj)%isConnected   =  .True.
!!$            listBE(iBBC,jj)%nPart         =   jj
!!$            listBE(iBBC,jj)%bandWidth     =   4000.0
!!$            listBE(iBBC,jj)%resolution    =   listBE(iBBC,jj)%bandWidth
!!$            listBE(iBBC,jj)%receiverName  =   EMIRsubBands(ii)%bandName
!!$            listBE(iBBC,jj)%polarization  =   otherPolarization
!!$            listBE(iBBC,jj)%subband       =   EMIRsubBands(ii)%subband
!!$            !
!!$            iBack = iContinuum
!!$            !
!!$            If (      listBE(iBBC,jj)%subBand .Eq. esb%lo ) Then
!!$               fShiftForced = fRangeSky(iBack,iLO,iCenter)                       &
!!$                    &       - fRangeSky(iVESPA,iLO,iCenter)                      !
!!$            Else If ( listBE(iBBC,jj)%subBand .Eq. esb%li ) Then
!!$               fShiftForced = fRangeSky(iBack,iLI,iCenter)                       &
!!$                    &       - fRangeSky(iVESPA,iLI,iCenter)                      !
!!$            Else If ( listBE(iBBC,jj)%subBand .Eq. esb%ui ) Then
!!$               fShiftForced = fRangeSky(iBack,iUI,iCenter)                       &
!!$                    &       - fRangeSky(iVESPA,iUI,iCenter)                      !
!!$            Else If ( listBE(iBBC,jj)%subBand .Eq. esb%uo ) Then
!!$               fShiftForced = fRangeSky(iBack,iUO,iCenter)                       &
!!$                    &       - fRangeSky(iVESPA,iUO,iCenter)                      !
!!$            End If
!!$            !
!!$            listBE(iBBC,jj)%fShift = fShiftForced
!!$            !
!!$         End If
!!$         !
!!$      End If
!!$   End Do   !!   ii = 1, nEsbIF, 1
   !
   !
Else If (receiverIsEMIR .And. backendSelected.Eq.bac%WILMA) Then
   !
   Write (messageText,*) "paKo sets ", bac%WILMA,                                &
        &                " for EMIR subbands requested with RECEIVER"            !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   !D    Write (6,*) " beSimple "
   !D    Write (6,*)   beSimple 
   !
   listBE(iWILMA,:)          = beSimple
   vars  (iValue,iWILMA)     = beReset
   !
   !D    Write (6,*) "   vars(:,iWILMA)%samplesIsSet:   ", vars(:,iWILMA)%samplesIsSet
   !D    Write (6,*) "   vars(:,iWILMA)%nSamples:       ", vars(:,iBBC)%nSamples
   !
   Do ii = 1, nEsbIF, 1
      If (EMIRsubBands(ii)%isRequested) Then
         !
         listBE(iWILMA,ii)               =   vars(iDefault,iWILMA)
         listBE(iWILMA,ii)%isConnected   =  .True.
         listBE(iWILMA,ii)%nPart         =   ii
         listBE(iWILMA,ii)%bandWidth     =   stdBandWidth (iWILMA,iLO)
         listBE(iWILMA,ii)%receiverName  =   EMIRsubBands(ii)%bandName
         listBE(iWILMA,ii)%polarization  =   EMIRsubBands(ii)%polarization
         listBE(iWILMA,ii)%subband       =   EMIRsubBands(ii)%subband
         listBE(iWILMA,ii)%percentage    =   100.0
         !
         iBack = iWILMA
         !
         If ( listBE(iWILMA,ii)%subBand .Eq. esb%lo ) Then
            fShiftForced = fRangeSky(iBack,iLO,iCenter)                          &
                 &       - fRangeSky(iVESPA,iLO,iCenter)                         !
         Else If ( listBE(iWILMA,ii)%subBand .Eq. esb%li ) Then
            fShiftForced = fRangeSky(iBack,iLI,iCenter)                          &
                 &       - fRangeSky(iVESPA,iLI,iCenter)                         !
         Else If ( listBE(iWILMA,ii)%subBand .Eq. esb%ui ) Then
            fShiftForced = fRangeSky(iBack,iUI,iCenter)                          &
                 &       - fRangeSky(iVESPA,iUI,iCenter)                         !
         Else If ( listBE(iWILMA,ii)%subBand .Eq. esb%uo ) Then
            fShiftForced = fRangeSky(iBack,iUO,iCenter)                          &
                 &       - fRangeSky(iVESPA,iUO,iCenter)                         !
         End If
         !
         listBE(iWILMA,ii)%fShift = fShiftForced
         !
      End If
   End Do
   !
Else If (receiverIsEMIR .And. backendSelected.Eq.bac%FB4Mhz) Then
   !
   Write (messageText,*) "paKo sets ", bac%FB4MHz,                               &
        &                " for EMIR subbands requested with RECEIVER"            !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   listBE(iFB4Mhz,:)          = beReset
   !
   jj = 0
   Do ii = 1, nEsbIF, 1
      If (EMIRsubBands(ii)%isRequested) Then
         !
         jj = jj+1
         !
         If (jj.Le.2) Then
            listBE(iFB4Mhz,jj)               =   vars(iDefault,iFB4Mhz)
            listBE(iFB4Mhz,jj)%isConnected   =  .True.
            listBE(iFB4Mhz,jj)%nPart         =   jj
            listBE(iFB4Mhz,jj)%bandWidth     =   stdBandWidth (iFB4MHz,iLO)
            listBE(iFB4Mhz,jj)%receiverName  =   EMIRsubBands(ii)%bandName
            listBE(iFB4Mhz,jj)%polarization  =   EMIRsubBands(ii)%polarization
            listBE(iFB4Mhz,jj)%subband       =   EMIRsubBands(ii)%subband
            !
            iBack = iFB4Mhz
            !
            If ( listBE(iFB4Mhz,jj)%subBand .Eq. esb%lo ) Then
               fShiftForced = fRangeSky(iBack,iLO,iCenter)                       &
                    &       - fRangeSky(iVESPA,iLO,iCenter)                      !
            Else If ( listBE(iFB4Mhz,jj)%subBand .Eq. esb%li ) Then
               fShiftForced = fRangeSky(iBack,iLI,iCenter)                       &
                    &       - fRangeSky(iVESPA,iLI,iCenter)                      !
            Else If ( listBE(iFB4Mhz,jj)%subBand .Eq. esb%ui ) Then
               fShiftForced = fRangeSky(iBack,iUI,iCenter)                       &
                    &       - fRangeSky(iVESPA,iUI,iCenter)                      !
            Else If ( listBE(iFB4Mhz,jj)%subBand .Eq. esb%uo ) Then
               fShiftForced = fRangeSky(iBack,iUO,iCenter)                       &
                    &       - fRangeSky(iVESPA,iUO,iCenter)                      !
            End If
            !
            listBE(iFB4Mhz,jj)%fShift = fShiftForced
            !
         Else
            !
            Write (messageText,*) "more EMIR subbands than ", bac%FB4MHz,        &
                 &                " parts"                                       !
            Call pakoMessage(priorityW,severityW,command,messageText)
            Write (messageText,*) "only 2 parts of ", bac%FB4MHz,                &
                 &                " can be connected to EMIR subbands"           !
            Call pakoMessage(priorityI,severityI,command,messageText)
            !
         End If
         !
      End If
   End Do
   !
Else If (receiverIsEMIR .And. backendSelected.Eq.bac%FTS) Then
   !
   If (.Not. fineFTS) Then
      Write (messageText,*) "paKo sets ", bac%FTS,                               &
           &                " for EMIR subbands requested with RECEIVER"         !
   Else
      Write (messageText,*) "paKo sets ", bac%FTS, " /FINE ",                    &
           &                " for EMIR subbands requested with RECEIVER"         !
   End If
   !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   listBE(iFTS,:)          = beSimple
   !
   jj = 0
   Do ii = 1, nEsbIF, 1
      !
      If (GV%doDebugMessages) Then
         Write (6,*) "   EMIRsubBands(ii): ", EMIRsubBands(ii)
         Write (6,*) "   Coax : ", esbCoax(ii)
      End If
      !
      If (EMIRsubBands(ii)%isRequested) Then
         !
         jj = jj+1
         !
         If (jj.Le.4) Then
            listBE(iFTS,jj)               =   vars(iDefault,iFTS)
            listBE(iFTS,jj)%isConnected   =  .True.
            listBE(iFTS,jj)%nPart         =   jj
            listBE(iFTS,jj)%receiverName  =   EMIRsubBands(ii)%bandName
            listBE(iFTS,jj)%polarization  =   EMIRsubBands(ii)%polarization
            listBE(iFTS,jj)%subband       =   EMIRsubBands(ii)%subband
            !
            iBack = iFTS
            !
            If (.Not.fineFTS) Then
               !
               listBE(iFTS,jj)%resolution    =   FTSres%wide
               listBE(iFTS,jj)%bandWidth     =   stdBandWidth (iFTS,iLO)
               !
               If ( listBE(iFTS,jj)%subBand .Eq. esb%lo ) Then
                  fShiftForced = fRangeSky(iBack,iLO,iCenter)                    &
                       &       - fRangeSky(iVESPA,iLO,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%li ) Then
                  fShiftForced = fRangeSky(iBack,iLI,iCenter)                    &
                       &       - fRangeSky(iVESPA,iLI,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%ui ) Then
                  fShiftForced = fRangeSky(iBack,iUI,iCenter)                    &
                       &       - fRangeSky(iVESPA,iUI,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%uo ) Then
                  fShiftForced = fRangeSky(iBack,iUO,iCenter)                    &
                       &       - fRangeSky(iVESPA,iUO,iCenter)                   !
               End If
               !
               listBE(iFTS,jj)%fShift = fShiftForced
               !
            Else If (fineFTS) Then
               !
               listBE(iFTS,jj)%resolution    =   FTSres%fine
               listBE(iFTS,jj)%bandWidth     =   FTSbw%fine
               !
               If ( listBE(iFTS,jj)%subBand .Eq. esb%lo ) Then
                  fShiftForced = fRangeSkyFTSfine(iLO,iCenter)                   &
                       &       - fRangeSky(iVESPA,iLO,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%li ) Then
                  fShiftForced = fRangeSkyFTSfine(iLI,iCenter)                   &
                       &       - fRangeSky(iVESPA,iLI,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%ui ) Then
                  fShiftForced = fRangeSkyFTSfine(iUI,iCenter)                   &
                       &       - fRangeSky(iVESPA,iUI,iCenter)                   !
               Else If ( listBE(iFTS,jj)%subBand .Eq. esb%uo ) Then
                  fShiftForced = fRangeSkyFTSfine(iUO,iCenter)                   &
                       &       - fRangeSky(iVESPA,iUO,iCenter)                   !
               End If
               !
               listBE(iFTS,jj)%fShift = fShiftForced
               !
            End If
            !
         Else
            !
            Write (messageText,*) "more EMIR subbands selected than ",           &
                 &                "4 ", bac%FTS, " parts"                        !
            Call pakoMessage(priorityW,severityW,command,messageText)
            Write (messageText,*) "only 4 parts of ", bac%FTS,                   &
                 &                " can be connected to selected EMIR subbands"  !
            Call pakoMessage(priorityI,severityI,command,messageText)
            !
         End If
         !
      End If
   End Do
   !
   If (.Not. fineFTS) Then
      Write (messageText,*) "paKo sets ", bac%FTS,                               &
           &                " for EMIR Outer subbands on Coax 5 to 8 "           !
   Else
      Write (messageText,*) "paKo sets ", bac%FTS, " /FINE ",                    &
           &                " for EMIR Outer subbands on Coax 5 to 8 "           !
   End If
   !
   Call pakoMessage(priorityI,severityI,command,messageText)
   !
   Do ii = 5, nEMIRsbCoax ,1
      !
      If (GV%doDebugMessages) Then
         Write (6,*) "   Coax : ", esbCoax(ii)
      End If
      !
      iBack = iFTS
      !
      If (esbCoax(ii)%isRequested) Then
         !
         jj = ii
         !
         listBE(iFTS,jj)               =   vars(iDefault,iFTS)
         listBE(iFTS,jj)%isConnected   =  .True.
         listBE(iFTS,jj)%nPart         =   jj
         listBE(iFTS,jj)%receiverName  =   esbCoax(ii)%bandName
         listBE(iFTS,jj)%polarization  =   esbCoax(ii)%polarization
         listBE(iFTS,jj)%subband       =   esbCoax(ii)%subband
         !
         If (.Not.fineFTS) Then
            !
            listBE(ifts,jj)%resolution    =   FTSres%wide
            listBE(ifts,jj)%bandwidth     =   FTSbw%wide
            !
            If ( listBE(iFTS,jj)%subBand .Eq. esb%lo ) Then
               fShiftForced = fRangeSky(iBack,iLO,iCenter)                       &
                    &       - fRangeSky(iVESPA,iLO,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%li ) Then
               fShiftForced = fRangeSky(iBack,iLI,iCenter)                       &
                    &       - fRangeSky(iVESPA,iLI,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%ui ) Then
               fShiftForced = fRangeSky(iBack,iUI,iCenter)                       &
                    &       - fRangeSky(iVESPA,iUI,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%uo ) Then
               fShiftForced = fRangeSky(iBack,iUO,iCenter)                       &
                    &       - fRangeSky(iVESPA,iUO,iCenter)                      !
            End If
            !
            listBE(iFTS,jj)%fShift = fShiftForced
            !
         Else If (fineFTS) Then
            !
            listBE(iFTS,jj)%resolution    =   FTSres%fine
            listBE(iFTS,jj)%bandWidth     =   FTSbw%fine
            !
            If ( listBE(iFTS,jj)%subBand .Eq. esb%lo ) Then
               fShiftForced = fRangeSkyFTSfine(iLO,iCenter)                      &
                    &       - fRangeSky(iVESPA,iLO,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%li ) Then
               fShiftForced = fRangeSkyFTSfine(iLI,iCenter)                      &
                    &       - fRangeSky(iVESPA,iLI,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%ui ) Then
               fShiftForced = fRangeSkyFTSfine(iUI,iCenter)                      &
                    &       - fRangeSky(iVESPA,iUI,iCenter)                      !
            Else If ( listBE(iFTS,jj)%subBand .Eq. esb%uo ) Then
               fShiftForced = fRangeSkyFTSfine(iUO,iCenter)                      &
                    &       - fRangeSky(iVESPA,iUO,iCenter)                      !
            End If
            !
            listBE(iFTS,jj)%fShift = fShiftForced
            !
         End If
         !
      End If
      !
   End Do
   !
Else
   !
   vars(iIn,:)          = vars(iDefault,:)
   vars(iTemp,:)        = vars(iDefault,:)
   vars(iValue,:)       = vars(iDefault,:)
   !                              
   listBE(:,:)          = beReset
   !
   Do ii = 1, nDimBackends, 1
      listBE(ii,1)  =   vars(iIn,ii)
   End Do
   !
End If
