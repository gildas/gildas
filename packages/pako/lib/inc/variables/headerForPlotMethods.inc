!
!  Id: headerForPlotMethods.inc,v 1.1.13 2012-10-18 Hans Ungerechts
!
! *** arguments: ***
Character(len=4)    ::  programName
Character(len=512)  ::  LINE
Character(len=12)   ::  command
Logical             ::  ERROR
!
! *** functions called: ***
Logical             ::  GR_ERROR
!
! *** plot style     ***
!
! *** calibrate /sky ***
Integer             ::  pCalNpen
Character(len=32)   ::  pCalMarker
Real                ::  pCalCharSize
Character(len=8)    ::  pCalChar
!
! *** pointing ***
Integer             ::  pPointNpen
Character(len=32)   ::  pPointMarker
Real                ::  pPointCharSize
Character(len=8)    ::  pPointChar
!
! *** ON ***
Integer             ::  pOnNpen
Character(len=32)   ::  pOnMarker
Real                ::  pOnCharSize
Character(len=8)    ::  pOnChar
!
! *** Track /Tune ***
Integer             ::  pTuneNpen
Character(len=32)   ::  pTuneMarker
Real                ::  pTuneCharSize
Character(len=8)    ::  pTuneChar
!
! *** REF ***
Integer             ::  pRefNpen
Character(len=32)   ::  pRefMarker
Real                ::  pRefCharSize
Character(len=8)    ::  pRefChar
!
! *** OTF ***
Integer             ::  pOtfNpen
Character(len=32)   ::  pOtfMarker
Real                ::  pOtfCharSize
Character(len=8)    ::  pOtfChar
!
!   * OTF Curve *
Integer             ::  pCurveNpen
Character(len=32)   ::  pCurveMarker
Real                ::  pCurveCharSize
Character(len=8)    ::  pCurveChar
!
!   * Dropin *
Integer             ::  pDropNpen
Character(len=32)   ::  pDropMarker
Real                ::  pDropCharSize
Character(len=8)    ::  pDropChar
!
! *** Control Points ***
Integer             ::  pCpNpen
Character(len=32)   ::  pCpMarker
Real                ::  pCpCharSize
Character(len=8)    ::  pCpChar
!
! *** receiver pixels ***
Integer             ::  pPixNpen
Character(len=32)   ::  pPixMarker
Real                ::  pPixCharSize
Character(len=8)    ::  pPixChar
!
! *** local variables: ***
!
Character(len=512)  ::  plotCommand
!
Character(len=100)  ::  pCommand, pPandO
!

