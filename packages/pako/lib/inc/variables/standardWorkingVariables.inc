!
!     $Id$
!
! ***  internal variables for command handler subroutines like "Backend" ***
!
Character(len=lenCO)    ::                                commandFull
Character(len=lenCO)    ::  option,       optionShort,    optionFull
!
Integer                 ::  commandIndex, optionIndex
Integer                 ::  iCommand
Integer                 ::  iOption,      iOptionShort
!
Integer                 ::  iArgument
!
! index for choice of possible values
Integer                 ::  choiceIndex
!
! input variables read directly from LINE:
Character(len=lenCO)    ::  cInput, cInputUpper
Character(len=lenCh)    ::  cPar, cPar2, cPar3, cPar4
Character(len=lenVar)   ::  cInputVar, cInputVarUpper
Integer                 ::  nPar, lPar
Character(len=lenVar)   ::  cInput100, cInput100Upper, cInput100Two
Character(len=lenLine)  ::  cInputLong
Character(len=lenLine)  ::  cInput256
Integer                 ::  lengthInput
Integer                 ::  iInput, nArguments
Real(Kind=kindSingle)   ::  rInput, rInput1, rInput2
Real(Kind=kindDouble)   ::  dInput
Logical                 ::  lInput
!
Logical                 ::  setDefaults       = GPnoneL
!
! error conditions 
Logical                 ::  errorNotFound     = GPnoneL
Logical                 ::  errorNotUnique    = GPnoneL
Logical                 ::  errorNarg         = GPnoneL
Logical                 ::  errorL            = GPnoneL
Logical                 ::  errorR            = GPnoneL
Logical                 ::  errorC            = GPnoneL
Logical                 ::  errorI            = GPnoneL
Logical                 ::  errorA            
Logical                 ::  errorP            
Logical                 ::  errorCall         = GPnoneL
Logical                 ::  errorNumber       = GPnoneL
Logical                 ::  errorRange        = GPnoneL
Logical                 ::  errorInconsistent = GPnoneL
!
Logical                 ::  Result            = GPnoneL
!
! SIC_NEXT has a 5th argument
Logical                 ::  useTab
! loop variables:
Integer                 ::  ii, jj, kk
!
! status
Integer                 ::  ioerr
!
! to format message text
Character(len=lenLine)  ::  messageText
!
! for text display
Character(len=lenCO)    ::  variableName, valueText
Character(len=lenCh)    ::  variableNameLong, valueTextLong
Integer                 ::  iUnit, iCoffset, iCindent
Integer                 ::  iRoffset, iRoffsetLong
!
Integer                 ::  l
Integer                 ::  lenc
!
! for matching / call to pakoUmach
Integer                 ::  iMatch    = GPnoneI, nMatch = GPnoneI
Character(len=lenVar)   ::  errorCode = GPnone
Logical                 ::  errorM    = GPnoneL
!

