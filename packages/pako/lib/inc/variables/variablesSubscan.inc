!
!     $Id$
!
      Include 'headerForVariables.f90'
!
      Type :: varSubscan
!
         Logical              ::  doInitialize          = .False.
         Logical              ::  doneInitialize        = .False.
!
         Character(len=12)    ::  systemName            = 'none'          
!
         Real                 ::  tRecord               = 0.0
!
         Logical              ::  doWriteToSub          = .False.
         Logical              ::  doneWriteToSub        = .False.
!
      End Type varSubscan
!
! *** 
      Type (varSubscan), Dimension(nDimValues) :: vars
      Type (varSubscan)                        :: varsSub
      Type (varSubscan)                        :: subDefault
!
      Character(len=512)                 :: subscanCommand 
!
      Logical                            :: isInitialized = .False. 
!
! *** 
!!      Character(len=12), Dimension(7)    :: systemNameChoices
