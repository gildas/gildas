!
!     Id: headerForVariables.inc,v 1.1 Hans Ungerechts
!
! *** header for variables for commands
!
Integer, Parameter  :: iValue   = 1
Integer, Parameter  :: iTemp    = 2
Integer, Parameter  :: iIn      = 3
Integer, Parameter  :: iDefault = 4
Integer, Parameter  :: iStd1    = 5
Integer, Parameter  :: iStd2    = 6
Integer, Parameter  :: iLimit1  = 7
Integer, Parameter  :: iLimit2  = 8
!
Integer, Parameter  :: nDimValuesI = 8
Integer, Parameter  :: nDimValuesR = 8
Integer, Parameter  :: nDimValuesL = 4
Integer, Parameter  :: nDimValuesC = 4
Integer, Parameter  :: nDimValuesS = 8
!
Integer, Parameter  :: nDimValues  = 8
