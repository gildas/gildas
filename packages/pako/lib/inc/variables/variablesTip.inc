  !
  ! Id: variablesTip.inc,v 1.2.4    2015-07-06 Hans Ungerechts
  ! Id: variablesTip.inc,v 1.2.4    2015-05-28 Hans Ungerechts
  ! Id: variablesTip.inc,v 1.2.3    2014-06-11 Hans Ungerechts
  ! Id: variablesTip.inc,v 1.2.3    2014-05-29 Hans Ungerechts
  ! Id: variablesTip.inc,v 1.0.10.7 2008-10-28 Hans Ungerechts
  !
  Include 'inc/variables/headerForVariables.inc'
  !
  Integer, Parameter      :: nDimAirmass = 9
  !
  Type :: rangeR
     !
     Real                 :: from = 0.0
     Real                 :: to   = 0.0
     Real                 :: by   = 0.0
     !
  End Type rangeR

  !
  Type :: varTip
     !
     Real                 ::  azimuth          =  0.0
     Logical              ::  doCurrentAzimuth = .False.
     Real                 ::  elevation        =  0.0
     !
     Type(rangeR)         ::  airmass
     Real                 ::  tSubscan         =  1.0               
     Real                 ::  tPaddle          = GPnoneR
     Logical              ::  doSlew           = .False.
     !
     Logical              ::  doTune           = .True.
     Real                 ::  tTune            =  0.0
     !
     Integer              ::  nAir             =  0
     Real, Dimension(90)  ::  airmassList
     Logical              ::  hasAirmassList   = .False.
     !
  End Type varTip
  !
  ! *** 
  Type (varTip), Dimension(nDimValues) :: vars
  !
  Real*8                               :: angleUnitTip = Pi/180.0D0
  Real*8                               :: speedUnitTip = Pi/180.0D0
  !
  Real         , Dimension(nDimValues) :: airmassII
  !
  Character(len=512)                   :: tipCommand 
  !
  Logical                              :: isInitialized = .False. 
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)             !  !!  values could be reset in 
  !                                                                              !  !!  ranges .inc

