!
!  Id: inc/variables/headerForSaveMethods.inc, v 1.0 Hans Ungerechts
!
!     IMPLICIT NONE
!
! *** arguments:   ***
      Character(len=4),    Intent(in)   ::  programName
      Character(len=512),  Intent(in)   ::  line
      Character(len=12),   Intent(in)   ::  commandToSave
!!$      Character(len=12),   Intent(in)   ::  OMselected
      Integer,             Intent(in)   ::  iUnit
      Logical,             Intent(out)  ::  ERROR
!
! *** called Functions:   ***
      Integer              ::  lenc
!
! *** local variables:   ***
      Character(len=1)     ::  B, Q, S
      Character(len=100)   ::  c1, c2
      Character(len=100)   ::  CMD
      Character(len=512)   ::  lineOut
      Integer              ::  l, lCMD
!
!  *  to decide about SIC continuation lines:
      Character(len=2)     ::  contC  = "NN"
      Character(len=2), parameter     ::  contCN = "CN"   ! is         continuation
      Character(len=2), parameter     ::  contCC = "CC"   ! is and has continuation
      Character(len=2), parameter     ::  contNC = "NC"   !        has continuation
      Character(len=2), parameter     ::  contNN = "NN"   !     simple line    
!
      Logical              ::  doContinue = .False.
!




