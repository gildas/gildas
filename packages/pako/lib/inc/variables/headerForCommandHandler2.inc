!
! $Id$
!
! Family:   header
! Siblings: headerForCommandHandler2.inc 
!
Character(len=4),    Intent(in)   :: programName
Character(len=*),    Intent(in)   :: line
Character(len=12),   Intent(in)   :: command
!!$      Character(len=12),   Intent(out)  :: modeSelected
Logical,             Intent(out)  :: error
!
! *** functions called:   ***
Logical  :: SIC_PRESENT
Integer  :: SIC_NARG
!
