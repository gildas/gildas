!
!     Id: variablesSwFrequency.inc,v 1.0.5 2006-10-17 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Integer  ::  iRec
!
Type :: varSwFrequency
   !
   Character(len=lenCh)                    ::  functionName = GPnone
   Integer                                 ::  nPhases      = GPnoneI
   Integer                                 ::  ReceiverI    = GPnoneI
   Character(len=lenCh)                    ::  receiverName = GPnone
   Real, Dimension                    (4)  ::  fOffset      = GPnoneR
   Real, Dimension(nDimReceiverChoices,4)  ::  fOffsets     = GPnoneR
   Real                                    ::  tBlanking    = GPnoneR
   Real                                    ::  tPhase       = GPnoneR
   Real                                    ::  tRecord      = GPnoneR
   Integer                                 ::  nCycles      = GPnoneI
   !
End Type varSwFrequency
!
Type (varSwFrequency), Dimension(nDimValuesS) :: vars
!
Logical                                    :: isInitialized=.False.
!
Character(len=lenCh), Dimension(2)         :: functionNameChoices = GPnone
!
Character(len=lenLineDouble)               :: swFrequencyCommand 

