  !
  !  Id: variablesFocus.inc,v 1.2.4 2015-07-06 Hans Ungerechts
  !  Id: variablesFocus.inc,v 1.2.3 2014-09-24 Hans Ungerechts
  !  Id: variablesFocus.inc,v 1.2   2005/06/22 14:46:24 ungerech
  !
  Include 'inc/variables/headerForVariables.inc'
  !
  Type :: varFocus

     Real                :: lengthFocus       = 0.0
     !
     Logical             :: doCalibrate       = .False.   
     Character(len=12)   :: directionFocus    = 'none'
     Integer             :: feBe              = 0              
     Integer             :: nSubscans         = 0            
     Logical             :: doOtfFocus        = .False.   
     Real                :: tSubscan          = 0.0
     Logical             :: doUpdate          = .False.            
     !
     Logical              ::  doTune          = .False.
     Logical              ::  doTuneset       = .False.
     Type(xyPointType)    ::  offsetTune      !!  
     Real                 ::  tTune           = 0.0
     !
  End Type varFocus
  !
  ! *** 
  Type (varFocus), Dimension(nDimValues) :: vars
  !
  Character(len=512)     :: focusCommand 
  !
  Logical                :: isInitialized = .False. 
  !
  ! *** 
  Character(len=12)      :: directionFocusChoices (3)
  !
  ! *** extended time limits
  !
  Type :: realLimits
     Real                 ::  min
     Real                 ::  max
  End type realLimits
  Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)               !  !!  values could be reset in 
  !                                                                                !  !!  ranges .inc
