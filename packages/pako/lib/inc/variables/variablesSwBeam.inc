!
! Id: variablesSwBeam.inc,v 1.0.5.2 2006-11-27 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Real(kind=4), Parameter          :: swBeamTphase    = 0.057 !! according to JP 
Real(kind=4), Parameter          :: swBeamTblanking = 0.033 !! " email 2006-11-15
!
Type :: varSwBeam
   !
   Integer                                 ::  nPhases      = GPnoneI
   Real                                    ::  tBlanking    = GPnoneR
   Real                                    ::  tPhase       = GPnoneR
   Real                                    ::  tRecord      = GPnoneR
   Integer                                 ::  nCycles      = GPnoneI
   !
End Type varSwBeam
!
Type (varSwBeam), Dimension(nDimValuesS) :: vars
!
Logical                          :: isInitialized=.False.
!
Character(len=lenLineDouble)     :: swBeamCommand 
