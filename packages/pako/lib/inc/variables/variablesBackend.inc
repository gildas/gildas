!
! Id: variablesBackend.inc,v 1.3.0 2017-08-28 Hans Ungerechts
!
! *** variables for command Backend
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varBackend
   !
   Character(len=lenCh)   ::  name             =  GPnone
   !
   Logical                ::  wasConnected     =  GPnoneL
   Logical                ::  isConnected      =  GPnoneL
   !
   Integer                ::  nPart            =  GPnoneI
   Real(kind=kindSingle)  ::  resolution       =  GPnoneR
   Real(kind=kindSingle)  ::  bandWidth        =  GPnoneR
   !
   Real(kind=kindSingle)  ::  fShift           =  GPnoneR
   Real(kind=kindSingle)  ::  percentage       =  GPnoneR
   Logical                ::  percentageIsSet  =  GPnoneL
   !
   Character(len=lenCh)   ::  mode             =  GPnone         
   !
   Character(len=lenCh)   ::  receiverName     =  GPnone         
   Character(len=lenCh)   ::  receiverName2    =  GPnone         
   !
   !  ** EMIR
   Character(len=lenCh)   ::  polarization     =  GPnone         
   Character(len=lenCh)   ::  subband          =  GPnone         
   !
   Character(len=lenCh)   ::  lineName         =  GPnone         
   Logical                ::  lineNameIsSet    =  GPnoneL
   !
   !  ** samples for pulsars
   Integer                ::  nSamples         =  GPnoneI
   Logical                ::  samplesIsSet     =  GPnoneL
   !
End Type varBackend
!
Integer, Parameter     :: nDimBackends  = 10
!
Integer, Parameter     :: nDimContinuum =  4
Integer, Parameter     :: nDim4MHz      =  4
Integer, Parameter     :: nDim1MHz      =  4
Integer, Parameter     :: nDim100kHz    =  2
Integer, Parameter     :: nDimWilma     =  8
Integer, Parameter     :: nDimVespa     = 18
Integer, Parameter     :: nDimABBA      =  1
Integer, Parameter     :: nDimUSB       =  2
Integer, Parameter     :: nDimFTS       =  8
Integer, Parameter     :: nDimBBC       =  8
!
Integer, Parameter     :: nDimParts     =  Max(                                  &
     & nDimContinuum,                                                            &
     & nDim4MHz,                                                                 &
     & nDim1MHz,                                                                 &
     & nDim100kHz,                                                               &
     & nDimWilma,                                                                &
     & nDimVespa,                                                                &
     & nDimABBA,                                                                 &
     & nDimUSB,                                                                  &
     & nDimFTS,                                                                  &
     & nDimBBC               )
!
!!$Integer, Parameter     :: iContinuum    = 1
!!$Integer, Parameter     :: i4MHz         = 2
!!$Integer, Parameter     :: i1MHz         = 3
!!$Integer, Parameter     :: i100kHz       = 4
!!$Integer, Parameter     :: iWILMA        = 5
!!$Integer, Parameter     :: iVESPA        = 6
!
Type (varBackend), Dimension(nDimValues,nDimBackends)  ::  vars      !! std. vars array
Type (varBackend), Dimension(nDimBackends,nDimParts)   ::  listBe    !! list of all BE parts
Type (varBackend)                                      ::  vars1     !! simple BE var
Type (varBackend), Parameter                           ::  beReset & !! for reset to "none"
     &  = varBackend( GPnone,  GPnoneL, GPnoneL,                                 &
     &                GPnoneI, GPnoneR, GPnoneR,                                 &
     &                GPnoneR, GPnoneR, GPnoneL,                                 &
     &                GPnone,  GPnone,  GPnone,                                  &
     &                GPnone,  GPnone,  GPnone,  GPnoneL,                        &
     &                GPnoneI, GPnoneL                                           &   !!  nSamples, samplesIsSet
     &              )                                                            !     
Type (varBackend), Parameter                           ::  beSimple & !! for Preset of simple values
     &  = varBackend( GPnone,       GPnoneL, GPnoneL,                            &
     &                GPnoneI,      GPnoneR, GPnoneR,                            &
     &                GPnoneR,      100.0,   .True.,                             &   !!  percentage, percentageSet
     &                BEmode%Simp,  GPnone,  GPnone,                             &   !!  mode
     &                GPnone,       GPnone,  GPnone,  GPnoneL,                   &
     &                GPnoneI, GPnoneL                                           &   !!  nSamples, samplesIsSet
     &              )                                                            !     
!
Character(len=lenLineDouble), Dimension(nDimBackends)  ::  backendCommand = GPnone
!
Logical                :: isInitialized  = GPnoneL
!
! *** EMIR: to check backend ranges and f-shift
!
!!Real(kind=kindSingle), Parameter  ::  fIFswBoxO  =  15.68
Real(kind=kindSingle), Parameter  ::  fIFswBoxO  =  16.00

Integer, Parameter        ::  nDimFrange   =  5
!
Integer, Parameter        ::  iBandFrom    =  1
Integer, Parameter        ::  iCenterFrom  =  2
Integer, Parameter        ::  iCenter      =  3
Integer, Parameter        ::  iCenterTo    =  4
Integer, Parameter        ::  iBandTo      =  5
!
Type  ::  fRangeType
   !
   Real(kind=kindSingle)  ::  bandFrom     =  GPnoneR
   Real(kind=kindSingle)  ::  centerFrom   =  GPnoneR
   Real(kind=kindSingle)  ::  center       =  GPnoneR
   Real(kind=kindSingle)  ::  centerTo     =  GPnoneR
   Real(kind=kindSingle)  ::  bandTo       =  GPnoneR
   !
End Type fRangeType
!
Real(kind=kindSingle),                                                                  &
     &  Dimension(nDimBackends,nDimSubbandChoices,nDimFrange)  ::  fRangeSky            !
Real(kind=kindSingle),                                                                  &
     &  Dimension             (nDimSubbandChoices,nDimFrange)  ::  fRangeSkyFTSfine     !
Real(kind=kindSingle),                                                                  &
     &  Dimension(nDimBackends,nDimSubbandChoices,nDimFrange)  ::  fRangeIF1            !
!
Type (fRangeType), Dimension(nDimBackends,nDimSubbandChoices)  ::  fRangeTypeSky
Type (fRangeType), Dimension(nDimBackends,nDimSubbandChoices)  ::  fRangeTypeIF1
!
! *** EMIR: standard fixed bandwidths
!
Real(kind=kindSingle),                                                           &
     & Dimension(nDimBackends,nDimSubbandChoices)  ::  stdBandWidth
!
! *** HERA: standard fixed bandwidths
!
Real(kind=kindSingle),                                                           &
     & Dimension(nDimBackends)                     ::  stdBandWidthHERA          !
!
Real(kind=kindSingle)   ::  maxBandWidthHeraFTSwide  =  GPnoneR
Real(kind=kindSingle)   ::  maxBandWidthHeraFTSfine  =  GPnoneR
!
! *** "helper" variables ***
!
! *** New:
!
Logical                ::  doConnect = GPnoneL
!
! *** Old:
!
Character(len=lenCh)   ::  receiverSelected = GPnone
Character(len=lenCh)   ::  backendSelected = GPnone
Character(len=lenCh)   ::  name  = GPnone
Character(len=lenCh)   ::  c12  = GPnone
!
Logical                ::  isConnected
Logical                ::  clearBackends
Logical                ::  connectAllBackends
Logical                ::  connectAllParts
Logical                ::  disconnectAllBackends
Logical                ::  disconnectAllParts
!
Integer                ::  iRec  = GPnoneI
Integer                ::  iBack = GPnoneI
Integer                ::  iPart = GPnoneI
!
Real(kind=kindSingle)  ::  resolution = GPnoneR
Real(kind=kindSingle)  ::  bandWidth  = GPnoneR
Real(kind=kindSingle)  ::  fShift     = GPnoneR
Real(kind=kindSingle)  ::  percentage = GPnoneR
!
Integer                ::  lname = GPnoneI
Integer                ::  l1 = GPnoneI
Integer                ::  l2 = GPnoneI
! 
Integer                ::  larg  = GPnoneI
!
Logical                ::  found = GPnoneL
!










