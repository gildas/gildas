!
! Id: variablesCalibrate.inc,v 1.2.4 2015-07-06 Hans Ungerechts
! Id: variablesCalibrate.inc,v 1.0.4 , 2006-07-13 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varCalibrate
   !
   Logical              ::  doAmbient         = .False.            
   Logical              ::  doCold            = .False.               
   Logical              ::  doFast            = .False.               
   Logical              ::  doGrid            = .False.               
   Logical              ::  doNull            = .False. 
   !             
   Logical              ::  doSky             = .False.          
   Real                 ::  xOffsetRC         = 0.0              
   Real                 ::  yOffsetRC         = 0.0             
   Character(len=100)   ::  sourceNameSky     = 'none'           
   Character(len=12)    ::  altSky            = 'none'               
   Character(len=12)    ::  systemName        = 'none'               
   Integer              ::  iSystem           = GPnoneI
   !
   Real                 ::  tCalibrate        = 0.0           
   !
   Logical              ::  doTest            = .False.             
   !
   Logical              ::  doGainImage       = .False.               
   Character(len=lenCh) ::  receiverName      = GPnone
   !
   Logical              ::  doVariable        = .False.  
   Real                 ::  tempVariable      = 0.0          
   !
End Type varCalibrate
!
! *** 
Type(varCalibrate), Dimension(nDimValues) :: vars
!
Character(len=512)                        :: calibrateCommand 
!
Logical                                   :: isInitialized = .False. 
!
! *** extended time limits
!
Type :: realLimits
   Real                 ::  min
   Real                 ::  max
End type realLimits
Type (realLimits)       :: timeExtended = realLimits(1.0, 86400.0)               !  !!  values could be reset in 
!                                                                                !  !!  ranges .inc



