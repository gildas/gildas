!
!     Id: variablesSet.inc ,v 1.2.6    2017-03-21 Hans Ungerechts
!     Id: variablesSet.inc ,v 1.2.3    2014-03-13 Hans Ungerechts
!     Id: variablesSet.inc ,v 1.0.10.4 2008-10-07 Hans Ungerechts
!
Include 'inc/variables/headerForVariables.inc'
!
Type :: varSet
   !
   Real                ::  transitionAcceleration   =  GPnoneR
   Real                ::  secondaryRotation        =  GPnoneR
   Real                ::  azimuthCorrection        =  GPnoneR
   Real                ::  elevationCorrection      =  GPnoneR
   Real                ::  focusCorrection          =  GPnoneR
   Real                ::  focusCorrectionX         =  GPnoneR
   Real                ::  focusCorrectionY         =  GPnoneR
   Character(len=12)   ::  directionFocus           = 'none'
   Real                ::  slowRateAMD              =  GPnoneR
   !
   Real                ::  focusCorrectionEMIR      =  GPnoneR
   Real                ::  focusCorrectionNIKA      =  GPnoneR
   !
End Type varSet
!
Type (varSet), Dimension(nDimValuesS) :: vars
!
Logical                          :: isInitialized=.False.
!
Character(len=lenLineDouble)     :: setCommand 
!
! *** 
!
Logical                :: isStandardAzimuthCorrection   =  GPnoneL
Logical                :: isStandardElevationCorrection =  GPnoneL
!
Character(len=12)      :: directionFocusChoices (3)
!
Logical                :: isSetfocusCorrectionX         =  GPnoneL
Logical                :: isSetfocusCorrectionY         =  GPnoneL
Logical                :: isSetfocusCorrectionZ         =  GPnoneL
!
Logical                :: isStandardfocusCorrectionX    =  GPnoneL
Logical                :: isStandardfocusCorrectionY    =  GPnoneL
Logical                :: isStandardfocusCorrectionZ    =  GPnoneL
!
