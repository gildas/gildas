!
!  Id: optionSpeedXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","speed",                                &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%speedStart*GV%speedUnit)
Write (valueComment,*) Real(vars(iValue)%speedStart/GV%speedFactorR)," ",        &
     &                                             GV%speedUnitSetC              !
Call pakoXMLwriteElement("PARAM","speedStart",valueC,                            &
     &                         dataType="float",                                 &
     &                         unit="rad/s",                                     &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%speedEnd*GV%speedUnit)
Write (valueComment,*) Real(vars(iValue)%speedEnd/GV%speedFactorR)," ",          &
     &                                             GV%speedUnitSetC              !
Call pakoXMLwriteElement("PARAM","speedEnd",valueC,                              &
     &                         dataType="float",                                 &
     &                         unit="rad/s",                                     &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","speed",                                  &
     &                         error=errorXML)                                   !
!
