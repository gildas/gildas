!
!   Id: segCalAmbientXML.inc, v 1.01 2005-12-28 Hans Ungerechts
!
!D      write (6,*) "      ---> StartXML/segCalAmbientXML.f90 "
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",                              &
     &                         error=errorXML)
!
Call pakoXMLwriteElement("PARAM","type","calAmbient",                            &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
Write (valueC,*) Real(segList(ii)%tSegment)
Call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,                    &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)
!
If (segList(ii)%sideBand.Ne.GPnone) Then
   Call pakoXMLwriteElement("PARAM","sideBand",segList(ii)%sideBand,             &
     &                         dataType="char",                                  &
     &                         error=errorXML)
End If
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",                                &
     &                         error=errorXML)
!
