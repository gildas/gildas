!
!---------------------------------------------------------------------------------
!
! <DOCUMENTATION name="startXML/receiver.inc">
!   <VERSION>
!              Id: startXML/receiver.inc, v 1.2.3 2014-09-02 Hans Ungerechts
!              Id: startXML/receiver.inc, v 1.2.3 2014-04-24 Hans Ungerechts
!              Id: startXML/receiver.inc, v 1.2.2 2013-01-24 Hans Ungerechts
!              Id: startXML/receiver.inc, v 1.2.0 2012-06-14 Hans Ungerechts
!              based on:
!              Id: startXML/receiver.inc,v 1.1.12 2012-02-20 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                XML
!   </FAMILY>
!   <SIBLINGS>
!
!   </SIBLINGS>        
!
!   Pako include file to write XML for RECEIVER
!
! </DOCUMENTATION> <!-- name="startXML/receiver.inc" -->
!
!---------------------------------------------------------------------------------
!
!
Call pakoXMLwriteStartElement("RESOURCE","receivers",                            &
     &                         comment="generated by paKo",                      &
     &                         space ="before",                                  &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteStartElement("TABLE","receivers",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteElement("FIELD","receiverName",                                 &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
!  **  EMIR
!
If (isEMIR) Then
   Call pakoXMLwriteElement("FIELD","bandName",                                  &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
End If
!
If (vars(iValue,iBolo)%isConnected) Then
   !
   Call pakoXMLwriteElement("FIELD","bolometerName",                             &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","nChannels",                                 &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","gainBolometer",                             &
        &                         dataType="float",                              &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","channel",                                   &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
Else
   !
   Call pakoXMLwriteElement("FIELD","lineName",                                  &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","frequency",                                 &
        &                         dataType="double",                             &
        &                         unit="GHz",                                    &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","sideBand",                                  &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","doppler",                                   &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   !  **  EMIR
   !
   If (isEMIR) Then
      Call pakoXMLwriteElement("FIELD","frequencyOffset",                        &
           &                         dataType="double",                          &
           &                         unit="GHz",                                 &
           &                         error=errorXML)                             !
   End If
   !
   Call pakoXMLwriteElement("FIELD","width",                                     &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","gainImage",                                 &
        &                         dataType="float",                              &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","tempCold",                                  &
        &                         dataType="float",                              &
        &                         unit="K",                                      &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","tempAmbient",                               &
        &                         dataType="float",                              &
        &                         unit="K",                                      &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","effForward",                                &
        &                         dataType="float",                              &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","effBeam",                                   &
        &                         dataType="float",                              &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","scale",                                     &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   valueComment = "selected pixel"
   Call pakoXMLwriteElement("FIELD","iPixel",                                    &
        &                         comment=valueComment,                          &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   valueComment = "number of mulibeam pixels"
   Call pakoXMLwriteElement("FIELD","nPixels",                                   &
        &                         comment=valueComment,                          &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","centerIF",                                  &
        &                         dataType="double",                             &
        &                         unit="GHz",                                    &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","bandWidth",                                 &
        &                         dataType="double",                             &
        &                         unit="GHz",                                    &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","distributionBoxInput",                      &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","IF2",                                       &
        &                         dataType="double",                             &
        &                         unit="GHz",                                    &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","isFlippingIF2",                             &
        &                         dataType="boolean",                            &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","polarization",                              &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","useSpecialLO",                              &
        &                         dataType="boolean",                            &
        &                         error=errorXML)                                !
   !
   !  **  EMIR
   !
   If (isEMIR) Then
      Call pakoXMLwriteElement("FIELD","horizontalSubbands",                     &
           &                         dataType="char",                            &
           &                         error=errorXML)                             !
      !
      Call pakoXMLwriteElement("FIELD","verticalSubbands",                       &
           &                         dataType="char",                            &
           &                         error=errorXML)                             !
   End If
   !
End If
!
Call pakoXMLwriteStartElement("DATA",                                            &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteStartElement("TABLEDATA",                                       &
     &                         comment="generated by paKo",                      &
     &                         error=errorXML)                                   !
!
! **
!
If (vars(iValue,iBolo)%isConnected) Then
   !
   ii = iBolo
   !
   Call pakoXMLwriteStartElement("TR",                                           &
        &                         error=errorXML)                                !
   !
   valueC = vars(iValue,ii)%name
   Call pakoXMLwriteElement("TD",                                                &
        &                         content=valueC,                                &
        &                         error=errorXML)                                !
   !
   valueC = vars(iValue,ii)%bolometerName
   Call pakoXMLwriteElement("TD",                                                &
        &                         content=valueC,                                &
        &                         error=errorXML)                                !
   !
   Write (valueC,'(I10)')                                                        &
        &                   vars(iValue,ii)%nChannels                            !
   valueC = ''
   valueComment = ''
   Call pakoXMLwriteElement("TD",                                                &
        &                         content=valueC,                                &
!!$     &                         comment=valueComment,                          &
        &                         error=errorXML)                                !
   !
   Write (valueC,'(F20.6)')                                                      &
        &                   vars(iValue,ii)%gainBolometer                        !
   valueC = ''
   valueComment = ''
   Call pakoXMLwriteElement("TD",                                                &
        &                         content=valueC,                                &
!!$     &                         comment=valueComment,                          &
        &                         error=errorXML)                                !
   !
   If (           vars(iValue,ii)%bolometerName.Eq.bolo%NIKA                     &
        &  .And.  NIKApixel%isSet )   Then                                       !
      Write (valueC,'(I10)')                                                     &
           &                   vars(iValue,ii)%channel                           !
   Else
      valueC = ''
      valueComment = ''
   End If
   Call pakoXMLwriteElement("TD",                                                &
        &                         content=valueC,                                &
!!$     &                         comment=valueComment,                          &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteEndElement("TR",                                             &
        &                         error=errorXML)                                !
   !
Else   !!   not bolometer
   !
   Do ii = 1,nDimReceivers,1
      If (vars(iValue,ii)%isConnected) Then
         Call pakoXMLwriteStartElement("TR",                                     &
              &                         error=errorXML)                          !
         !
         !  **  EMIR
         If (isEMIR) Then
            valueC = "EMIR"             !! receiver Name for EMIR
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
         End If
         !
         valueC = vars(iValue,ii)%name  !! band Name for EMIR, RX name oterwise
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         valueC = vars(iValue,ii)%lineName
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%frequency%value                !
         valueComment = 'GHz'
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         comment=valueComment,                    &
              &                         error=errorXML)                          !
         !
         valueC = vars(iValue,ii)%sideBand
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         valueC = vars(iValue,ii)%frequency%doppler
         Call pakoXMLcase(valueC,error=errorXML)
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         !  **  EMIR
         !
         If (isEMIR) Then
            Write (valueC,'(F20.6)')                                             &
                 &                   vars(iValue,ii)%fOffset%value               !
            valueComment = 'GHz'
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         comment=valueComment,                 &
                 &                         error=errorXML)                       !
         End If
         !
         If (vars(iValue,ii)%width .Ne. GPnone) Then
            valueC = vars(iValue,ii)%width
         Else
            valueC = ""
         End If
         Call pakoXMLcase(valueC,error=errorXML)
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%gainImage%value                !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         If (vars(iValue,ii)%tempColdCode.Eq."L") Then
            Call pakoXMLwriteElement("TD",                                       &
                 &                         error=errorXML)                       !
         Else If (ii.Eq.iRxTest .And. vars(iValue,ii)%name.Eq.RxHolography) Then 
            Call pakoXMLwriteElement("TD",                                       &
                 &                         error=errorXML)                       !
         Else 
            Write (valueC,'(F20.6)') vars(iValue,ii)%tempCold       
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
         End If
         !
         If (vars(iValue,ii)%tempAmbientCode.Eq."L") Then
            Call pakoXMLwriteElement("TD",                                       &
                 &                         error=errorXML)                       !
         Else If (ii.Eq.iRxTest .And. vars(iValue,ii)%name.Eq.RxHolography) Then 
            Call pakoXMLwriteElement("TD",                                       &
                 &                         error=errorXML)                       !
         Else
            Write (valueC,'(F20.6)') vars(iValue,ii)%tempAmbient       
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
         End If
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%effForward                     !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%effBeam                        !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         If (ii.Eq.iRxTest .And. vars(iValue,ii)%name.Eq.RxHolography) Then      
            Call pakoXMLwriteElement("TD",                                       &
                 &                         error=errorXML)                       !
         Else
            valueC = vars(iValue,ii)%scale
            Call pakoXMLcase(valueC,error=errorXML)
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
         End If
         !
         Write (valueC,'(I5)')                                                   &
              &                   vars(iValue,ii)%iPixel                         !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(I5)')                                                   &
              &                   vars(iValue,ii)%nPixels                        !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%centerIF                       !
         valueComment = 'GHz'
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         comment=valueComment,                    &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%bandWidth                      !
         valueComment = 'GHz'
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         comment=valueComment,                    &
              &                         error=errorXML)                          !
         !
         If (vars(iValue,ii)%disBoxInput .Ne. GPnoneI) Then
            Write (valueC,'(I5)')                                                &
                 &                   vars(iValue,ii)%disBoxInput                 !
         Else
            valueC = ""
         End If
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,'(F20.6)')                                                &
              &                   vars(iValue,ii)%IF2                            !
         valueComment = 'GHz'
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         comment=valueComment,                    &
              &                         error=errorXML)                          !
         !
         If (isEMIR) Then
            valueC = ""
         Else
            Write (valueC,*)                                                     &
                 &                   vars(iValue,ii)%isFlippingIF2               !
         End If
         !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         If (vars(iValue,ii)%polarization .Ne. GPnone) Then
            valueC = vars(iValue,ii)%polarization
         Else
            valueC = ""
         End If
         Call pakoXMLcase(valueC,error=errorXML)
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Write (valueC,*)                                                        &
              &                   vars(iValue,ii)%useSpecialLO                   !
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         !  ** EMIR
         !
         If (isEMIR) Then
            !
            If (vars(iValue,ii)%horizontalSubbands.Ne.GPnone) Then
               valueC = vars(iValue,ii)%horizontalSubbands
            Else
               valueC =""
            End If
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
            !
            !  ** EMIR
            If (vars(iValue,ii)%verticalSubbands.Ne.GPnone) Then
               valueC = vars(iValue,ii)%verticalSubbands
            Else
               valueC =""
            End If
            Call pakoXMLwriteElement("TD",                                       &
                 &                         content=valueC,                       &
                 &                         error=errorXML)                       !
         End If
         !
         !
         Call pakoXMLwriteEndElement("TR",                                       &
              &                         error=errorXML)                          !
         !
      End If
      !
   End Do
   !
End If
!
! **
!
Call pakoXMLwriteEndElement  ("TABLEDATA",                                       &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement  ("DATA",                                            &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement  ("TABLE","receivers",                               &
     &                         error=errorXML)                                   !
!
!                                                                                                                               

If (isEMIR) Then
   !
!!$   !  ** for EMIR: SN's switch box  //  obsolete with EMIR upgrade 2011-11-11
!!$   !
!!$   Call pakoXMLwriteStartElement("RESOURCE","IFswitchBox",                       &
!!$        &                         comment="generated by paKo",                   &
!!$        &                         space ="before",                               &
!!$        &                         error=errorXML)                                !
!!$   !
!!$   Write (valueC,*)       esbSwitchesChex
!!$   Write (valueComment,*) esbSwitchesC
!!$   Call pakoXMLwriteElement("PARAM","programWord",valueC,                        &
!!$        &                         comment=valueComment,                          &
!!$        &                         dataType="char",                               &
!!$        &                         error=errorXML)                                !
!!$   !
!!$   Call pakoXMLwriteEndElement("RESOURCE","IFswitchBox",                         &
!!$        &                         error=errorXML)                                !
!!$   !
   !
   !  ** for EMIR: sub bands in up to 8 IF cables:
   !
   Call pakoXMLwriteStartElement("RESOURCE","IFcables",                          &
        &                         comment="generated by paKo",                   &
        &                         space ="before",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteStartElement("TABLE","IFcables",                             &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","IFcable",                                   &
        &                         dataType="int",                                &
        &                         unit="--",                                     &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","receiverName",                              &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","bandName",                                  &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","polarization",                              &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","sideBand",                                  &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteElement("FIELD","subBand",                                   &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteStartElement("DATA",                                         &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteStartElement("TABLEDATA",                                    &
        &                         comment="generated by paKo",                   &
        &                         error=errorXML)                                !
   !
   Do jj = 1,nEMIRsbCoax,1
      If (esbCoax(jj)%isRequested) Then
         !
         Call pakoXMLwriteStartElement("TR",                                     &
              &                         error=errorXML)                          !
         !
         Write (valueC,*) jj                                 !! IF cable number
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         valueC = "EMIR"                                     !! receiver Name
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         valueC = esbCoax(jj)%bandName                       !! band Name 
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         valueC = esbCoax(jj)%polarization                   !! polarization
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         If (esbCoax(jj)%sideBand .Ne. GPnone) Then
              valueC = esbCoax(jj)%sideBand                  !! side band
         Else
              valueC = ""
         End If
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         If (esbCoax(jj)%subBand .Ne. GPnone) Then
              valueC = esbCoax(jj)%subBand                  !! sub band
         Else
              valueC = ""
         End If
         Call pakoXMLwriteElement("TD",                                          &
              &                         content=valueC,                          &
              &                         error=errorXML)                          !
         !
         Call pakoXMLwriteEndElement("TR",                                       &
              &                         error=errorXML)                          !
         !
      End If
      !
   End Do
   !
   Call pakoXMLwriteEndElement  ("TABLEDATA",                                    &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteEndElement  ("DATA",                                         &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteEndElement  ("TABLE","IFcables",                             &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteEndElement("RESOURCE","IFcables",                            &
        &                         error=errorXML)                                !
   !
End If
!
If (        vars(iValue,iHera)%isConnected                                       &
     & .Or. vars(iValue,iHera1)%isConnected                                      &
     & .Or. vars(iValue,iHera2)%isConnected ) Then                               !
   !
   Call pakoXMLwriteStartElement("RESOURCE","derotator",                         &
        &                         comment="for HERA",                            &
        &                         error=errorXML)                                !
   !
   Write (valueC,*)       Real(vars(iValue,iHera)%derotatorAngle*degs)
   Write (valueComment,*) Real(vars(iValue,iHera)%derotatorAngle), " [deg]"
   Call pakoXMLwriteElement("PARAM","derotatorAngle",valueC,                     &
        &                         "rad", "double",                               &
        &                         comment=valueComment,                          &
        &                         error=errorXML)                                !
   !
   valueC = vars(iValue,iHera)%derotatorSystem
   Call pakoXMLwriteElement("PARAM","derotatorSystem",valueC,                    &
        &                         dataType="char",                               &
        &                         error=errorXML)                                !
   !
   Call pakoXMLwriteEndElement("RESOURCE","derotator",                           &
        &                         error=errorXML)                                !
   !
End If
!
Call pakoXMLwriteEndElement  ("RESOURCE","receivers",                            &
     &                         space="after",                                    &
     &                         error=errorXML)                                   !
!
