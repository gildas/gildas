!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="StartXML/airmassXML.inc">
!   <VERSION>
!                Id: StartXML/airmassXML.inc 1.2.0 2012-08-16 Hans Ungerechts
!                based on
!                Id: StartXML/airmassXML.inc 1.1.0 2012-06-04 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                StartXML/
!   </FAMILY>
!   <SIBLINGS>
!                airmassXML.inc
!   </SIBLINGS>        
!
!   write XML for subscan, type airmass
!
! </DOCUMENTATION> <!-- name="StartXML/airmassXML.inc" -->
!
!----------------------------------------------------------------------
!
!D      write (6,*) "      ---> 
!
      Call pakoXMLwriteStartElement("RESOURCE","subscan",                        &
     &                         error=errorXML,                                   &
     &                         comment="airmass")                                !
!
      Call pakoXMLwriteElement("PARAM","type","airmass",                         &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
      Write (valueC,*) Real(segList(ii)%tSegment)
      Call pakoXMLwriteElement("PARAM","timePerSubscan",valueC,                  &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
!!$   write (valueC,*)  segList(ii)%systemName                         !!  causes segmentation error with ifort v 1.11  !!
      valueC = segList(ii)%systemName
      Call pakoXMLcase(valueC,error=errorXML)
      Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                    &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
      Write (valueC,*)       Real(segList(ii)%pStart%x*angleUnitTip)
      Write (valueComment,*) Real(segList(ii)%pStart%x), segList(ii)%angleUnit
      Call pakoXMLwriteElement("PARAM","xOffset",valueC,                         &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
      Write (valueC,*)       Real(segList(ii)%pStart%y*angleUnitTip)
      Write (valueComment,*) Real(segList(ii)%pStart%y), segList(ii)%angleUnit
      Call pakoXMLwriteElement("PARAM","yOffset",valueC,                         &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
      Call pakoXMLwriteEndElement("RESOURCE","subscan",                          &
     &                         error=errorXML)                                   !
!
