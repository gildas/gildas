!
! $Id$
!
Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xStart",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yStart",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
