!
!  Id: pOffset.inc,v 0.94 2005-11-06 Hans Ungerechts
!
!D Write (6,*) "      ---> StartXML/pOffsetDopple.inc "
!
If (segList(ii)%upDateDoppler) Then
   !
   Write (valueC,*)       Real(segList(ii)%pDoppler%x*GV%angleUnit)
   !! Write (valueC,*)       0.0
   Write (valueComment,*) Real(segList(ii)%pDoppler%x/GV%angleFactorR),          &
        &                         " ",GV%angleUnitSetC
   Call pakoXMLwriteElement("PARAM","xOffsetDoppler",valueC,                     &
        &                         "rad", "float",                                &
        &                         comment=valueComment,                          &
        &                         error=errorXML)
   !
   Write (valueC,*)       Real(segList(ii)%pDoppler%y*GV%angleUnit)
   !! Write (valueC,*)       0.0
   Write (valueComment,*) Real(segList(ii)%pDoppler%y/GV%angleFactorR),          &
        &                         " ",GV%angleUnitSetC
   Call pakoXMLwriteElement("PARAM","yOffsetDoppler",valueC,                     &
        &                         "rad", "float",                                &
        &                         comment=valueComment,                          &
        &                         error=errorXML)
   !
End If
!
