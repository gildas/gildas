!
!  Id: optionSkyXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","sky",                                  &
     &                         error=errorXML)
!
If (vars(iValue)%doSky) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doSky",valueC,                                 &
     &                         dataType="boolean",                               &
     &                         error=errorXML)
!
Write (valueC,*)       Real(vars(iValue)%xOffsetRC*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%xOffsetRC/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)
!
Write (valueC,*)       Real(vars(iValue)%yOffsetRC*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%yOffsetRC/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","sky",                                    &
     &                         error=errorXML)
!
