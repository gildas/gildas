  !
  !  Id: optionTtuneXML.inc, v 1.2.3 2014-10-14 Hans Ungerechts
  !
  Call pakoXMLwriteStartElement("RESOURCE","tTune",                              &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(vars(iValue)%tTune)
  Call pakoXMLwriteElement("PARAM","timePerTune",valueC,                         &
       &                         dataType="float",                               &
       &                         unit="s",                                       &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteEndElement("RESOURCE","tTune",                                &
       &                         error=errorXML)                                 !
  !
