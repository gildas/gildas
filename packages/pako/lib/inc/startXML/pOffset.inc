!
! $Id$
!
Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xOffset",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yOffset",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
