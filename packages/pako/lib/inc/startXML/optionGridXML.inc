!
!  Id: optionGridXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","grid",                                 &
     &                         error=errorXML)
!
If (vars(iValue)%doGrid) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doGrid",valueC,                                &
     &                         dataType="boolean",                               &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","grid",                                   &
     &                         error=errorXML)
!
