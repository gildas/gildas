!
!  Id: parametersAmplitudeXML.inc, v 1.1.12 2012-03-08 Hans Ungerchts
!
Write (valueC,*)       Real(vars(iValue)%amplitude%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%amplitude%x/GV%angleFactorR)," ",       &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xAmplitude",valueC,                            &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%amplitude%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%amplitude%y/GV%angleFactorR)," ",       &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yAmplitude",valueC,                            &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
