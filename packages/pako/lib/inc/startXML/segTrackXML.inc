!
! $Id$
!
!D Write (6,*) "      ---> inc/startXML/segTrackXML.inc"
!
Call pakoXMLwriteStartElement("RESOURCE","segment",         &
     &                         error=errorXML)
!
Write (valueC,*) segList(ii)%segType
Call pakoXMLwriteElement("PARAM","type",valueC,             &
     &                         dataType="char",             &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xOffset",valueC,          &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yOffset",valueC,          &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%tSegment)
Write (valueComment,*) Real(segList(ii)%tSegment)
Call pakoXMLwriteElement("PARAM","tSegment",valueC,         &
     &                         "s", "float",                &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","segment",           &
     &                         error=errorXML)
!





