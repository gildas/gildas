!
! $Id$
!
      valueC = GV%project
      call pakoXMLwriteElement("PARAM","project",valueC,                 &
     &                         dataType="char",                          &
     &                         error=errorXML)
!
      valueC = GV%PI
      call pakoXMLwriteElement("PARAM","PI",valueC,                      &
     &                         dataType="char",                          &
     &                         error=errorXML)
!
      valueC = GV%observer
      call pakoXMLwriteElement("PARAM","observer",valueC,                &
     &                         dataType="char",                          &
     &                         error=errorXML)
!
      valueC = GV%oper
      call pakoXMLwriteElement("PARAM","operator",valueC,                &
     &                         dataType="char",                          &
     &                         space="after",                            &
     &                         error=errorXML)

!
