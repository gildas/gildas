!
!   Id: savePakoScriptFirst.inc, v0.9 2005-08-04 Hans Ungerechts
!
Call pakoSaveSet      (programName,LINE,commandToSave, iUnit, errorL)
Call saveReceiver     (programName,LINE,commandToSave, iUnit, errorL)
Call saveBackend      (programName,LINE,commandToSave, iUnit, errorL)
Call saveSwitching    (programName,LINE,commandToSave, iUnit, errorL)
Call saveSource       (programName,LINE,commandToSave, iUnit, errorL)
Call saveOffsets      (programName,LINE,commandToSave, iUnit, errorL)
!
