  !
  !  Id: lissajousAnnotationsXML.inc, v 1.3.4 2015-06-16 Hans Ungerechts
  !  Id: lissajousAnnotationsXML.inc, v 1.3.2 2014-10-15 Hans Ungerechts
  !  Id: lissajousAnnotationsXML.inc, v 1.3.2 2014-10-10 Hans Ungerechts
  !  Id: lissajousAnnotationsXML.inc, v 1.3.2 2014-09-25 Hans Ungerechts
  !
  !D   Write (6,*) Purpose
  !D   Write (6,*) "      Purpose%Pointing:  ->", Purpose%Pointing, "<-"
  !D   Write (6,*) "      Purpose%Focus:     ->", Purpose%Focus,    "<-"
  !D   Write (6,*) "      Purpose%Map:       ->", Purpose%Map,      "<-"
  !
  Call pakoXMLwriteStartElement("RESOURCE","annotations",                        &
       &                         error=errorXML)                                 !
  !
  If (vars(iValue)%doFocus) Then 
     !
     valueC  =  Purpose%Focus
     Call pakoXMLwriteElement("PARAM","purpose",valueC,                          &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
  Else If (vars(iValue)%doPointing) Then 
     !
     valueC  =  Purpose%Pointing
     Call pakoXMLwriteElement("PARAM","purpose",valueC,                          &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
  Else If (gv%purposeIsSet) Then
     !
     valueC  =  gv%purpose
     Call pakoXMLwriteElement("PARAM","purpose",valueC,                          &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
  Else 
     !
     valueC  =  Purpose%Map
     Call pakoXMLwriteElement("PARAM","purpose",valueC,                          &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
  End If   !!   (vars(iValue)%doFocus)
  !
  If (gv%userCommentIsSet) then
     !
     valueComment = gv%userComment
     !
  Else 
     !
     Write (valueComment,*)   "no comment"
     !
  End If
  !
  Call pakoXMLwriteElement("PARAM","comments",valueComment,                      &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteEndElement("RESOURCE","annotations",                          &
       &                         error=errorXML)                                 !
  !
