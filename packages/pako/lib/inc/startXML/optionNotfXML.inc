!
!  Id: optionNotfXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","nOtf",                                 &
     &                         error=errorXML)                                   !
!
Write (valueC,*) vars(iValue)%Notf
Call pakoXMLwriteElement("PARAM","nOtf",valueC,                                  &
     &                         dataType="int",                                   &
     &                         unit="--",                                        &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","nOtf",                                   &
     &                         error=errorXML)                                   !
!
