!
!  Id: optionCroLoopXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","croLoop",                              &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteElement("PARAM","croLoop",vars(iValue)%croCode,                 &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","croLoop",                                &
     &                         error=errorXML)                                   !
!
