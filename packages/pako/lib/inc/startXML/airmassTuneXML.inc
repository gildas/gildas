  !
  !----------------------------------------------------------------------
  !
  ! <DOCUMENTATION name="StartXML/airmassTuneXML.inc">
  !   <VERSION>
  !                Id: StartXML/airmassTuneXML.inc 1.2.3 2015-02-12 Hans Ungerechts
  !                Id: StartXML/airmassTuneXML.inc 1.2.3 2014-06-12 Hans Ungerechts
  !                based on 
  !                Id: StartXML/airmassXML.inc     1.2.0 2012-08-16 Hans Ungerechts
  !   </VERSION>
  !   <PROGRAM>
  !                Pako
  !   </PROGRAM>
  !   <FAMILY>
  !                StartXML/
  !   </FAMILY>
  !   <SIBLINGS>
  !                airmassTune.inc
  !                airmassTuneXML.inc
  !   </SIBLINGS>        
  !
  !   write XML for subscan, type airmassTune
  !
  ! </DOCUMENTATION> <!-- name="StartXML/airmassTuneXML.inc" -->
  !
  !----------------------------------------------------------------------
  !
  !D   Call pako_message(seve%t,programName,                                          &
  !D        &  " module Tip, v 1.2.3 2015-02-11  --> airmassTuneXML.inc ")            ! trace execution
  !
  Call pakoXMLwriteStartElement("RESOURCE","subscan",                            &
       &                         error=errorXML,                                 &
       &                         comment="airmassTune")                          !
  !
  Call pakoXMLwriteElement("PARAM","type","tune",                                &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*) Real(segList(ii)%tSegment)
  Call pakoXMLwriteElement("PARAM","timePerSubscan",valueC,                      &
       &                         dataType="float",                               &
       &                         unit="s",                                       &
       &                         error=errorXML)                                 !
  !
  valueC = segList(ii)%systemName
  Call pakoXMLcase(valueC,error=errorXML)
  Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                        &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pStart%x*angleUnitTip)
  Write (valueComment,*) Real(segList(ii)%pStart%x), segList(ii)%angleUnit
  Call pakoXMLwriteElement("PARAM","xOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pStart%y*angleUnitTip)
  Write (valueComment,*) Real(segList(ii)%pStart%y), segList(ii)%angleUnit
  Call pakoXMLwriteElement("PARAM","yOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Include 'inc/startXML/traceFlagXML.inc'
  !
  Call pakoXMLwriteEndElement("RESOURCE","subscan",                              &
       &                         error=errorXML)                                 !
  !
