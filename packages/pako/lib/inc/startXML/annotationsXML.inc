  !
  !  Id: annotationsXML.inc, v 1.2.6 2017-06-21 Hans Ungerechts
  !                            patch for display of Purpose & Comment in SDH etc.
  !      based on
  !      lissajousAnnotationsXML.inc, v 1.3.2 2014-10-10 Hans Ungerechts
  !
  !D   Write (6,*) "      --> annotationsXML.inc, v 1.2.6 2017-06-21 "
  !
  !D   Write (6,*) "   GV%purposeIsSet:      ", GV%purposeIsSet
  !D   Write (6,*) "   GV%purpose:           ", GV%purpose
  !D   Write (6,*) "   GV%userCommentIsSet:  ", GV%userCommentIsSet
  !D   Write (6,*) "   GV%userComment:       ", GV%userComment
  !D   Write (6,*) "   "
  !
  len1 = Len_Trim(GV%purpose)
  len2 = Len_Trim(GV%userComment)
  !
  !D   Write (6,*) "   len1:                 ", len1
  !D   Write (6,*) "   len2:                 ", len2
  !D   Write (6,*) "   "
  !
  If      (       ( GV%purposeIsSet     .And. len1.Ge.1 )                        &
       &     .Or. ( gv%userCommentIsSet .And. len2.Ge.1 )                        &
       &   ) Then                                                                !
     !
     Call pakoXMLwriteStartElement("RESOURCE","annotations",                     &
          &                         error=errorXML)                              !
     !
     If ( GV%purposeIsSet .And. len1.Ge.1 ) Then
        !
        valueC  =  GV%purpose(1:len1)
     Else
        valueC  =  "  "
        !
     End If
     !
     If ( gv%userCommentIsSet .And. len2.Ge.1 ) Then
        !
        valueComment = gv%userComment(1:len2)
     Else
        valueComment  =  "  "
        !
     End If
     !
     Call pakoXMLwriteElement("PARAM","purpose",valueC,                          &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteElement("PARAM","comments",valueComment,                   &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement("RESOURCE","annotations",                       &
          &                         error=errorXML)                              !
     !
  End If   !!   GV%purposeIsSet .Or. GV%userCommentIsSet


  !! *************************************************************************   ! !!
  !! previous version / before 2017-07-21 
  
!!$  !
!!$  !  Id: annotationsXML.inc, v 1.3.2 2014-10-24 Hans Ungerechts
!!$  !      based on
!!$  !      lissajousAnnotationsXML.inc, v 1.3.2 2014-10-10 Hans Ungerechts
!!$  !
!!$  !
!!$
!!$  If      (       ( GV%purposeIsSet     .And. len1.Ge.1 )                        &
!!$       &     .Or. ( gv%userCommentIsSet .And. len2.Ge.1 )                        &
!!$       &   ) Then                                                                !
!!$     !
!!$     Call pakoXMLwriteStartElement("RESOURCE","annotations",                     &
!!$          &                         error=errorXML)                              !
!!$     !
!!$     If ( GV%purposeIsSet .And. len1.Ge.1 ) Then
!!$        !
!!$        valueC  =  GV%purpose(1:len1)
!!$        Call pakoXMLwriteElement("PARAM","purpose",valueC,                       &
!!$             &                         dataType="char",                          &
!!$             &                         error=errorXML)                           !
!!$        !
!!$     End If
!!$     !
!!$     If ( gv%userCommentIsSet .And. len2.Ge.1 ) then
!!$        !
!!$        valueComment = gv%userComment(1:len2)
!!$        !
!!$        Call pakoXMLwriteElement("PARAM","comments",valueComment,                &
!!$             &                         dataType="char",                          &
!!$             &                         error=errorXML)                           !
!!$        !
!!$     End If
!!$     !
!!$     Call pakoXMLwriteEndElement("RESOURCE","annotations",                       &
!!$          &                         error=errorXML)                              !
!!$     !
!!$  End If   !!   GV%purposeIsSet .Or. GV%userCommentIsSet
