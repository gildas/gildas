!
!  Id: optionTreferenceXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tReference",                           &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%tReference)
Call pakoXMLwriteElement("PARAM","timePerReference",valueC,                      &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","tReference",                             &
     &                         error=errorXML)                                   !
!
