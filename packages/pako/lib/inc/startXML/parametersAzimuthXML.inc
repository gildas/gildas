!
!  Id: parametersAzimuthXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
If (vars(iValue)%doCurrentAzimuth) Then
   valueC = "--"
   valueComment = "currentAzimuth"
Else
   Write (valueC,'(ES20.12)') vars(iValue)%azimuth*angleUnitTip
   Write (valueComment,*) Real(vars(iValue)%azimuth), segList(1)%angleUnit
End If
!
Call pakoXMLwriteElement("PARAM","Azimuth",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!


