!
!  Id: optionGainImageXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","gainImage",                            &
     &                         error=errorXML)
!
If (vars(iValue)%doGainImage) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doGainImage",valueC,                           &
     &                         dataType="boolean",                               &
     &                         error=errorXML)
!
valueC = vars(iValue)%receiverName
Call pakoXMLwriteElement("PARAM","receiverName",valueC,                          &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
!
Call pakoXMLwriteEndElement("RESOURCE","gainImage",                              &
     &                         error=errorXML)
!
