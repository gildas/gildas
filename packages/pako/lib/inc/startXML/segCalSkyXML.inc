!
! $Id$
!
!D      write (6,*) "      ---> inc/startXML/segCalSkyXML.inc"
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",         &
     &                         error=errorXML)
!
Call pakoXMLwriteElement("PARAM","type","calSky",           &
     &                         dataType="char",             &
     &                         error=errorXML)
!
!D      write (6,*) "   valueC", valueC
!D      write (6,*) "   segList(ii)%systemName", segList(ii)%systemName
!
valueC = segList(ii)%systemName
Call pakoXMLcase(valueC,error=errorXML)
If (valueC.Eq."horizontalT") valueC="horizontalTrue"
Call pakoXMLwriteElement("PARAM","systemOffset",valueC,     &
     &                         dataType="char",             &
     &                         error=errorXML)
!
Write (valueC,*) Real(segList(ii)%tSegment)
Call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,  &
     &                         dataType="float",            &
     &                         unit="s",                    &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xOffset",valueC,          &
     &                         dataType="float",            &
     &                         unit="rad",                  &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yOffset",valueC,          &
     &                         dataType="float",            &
     &                         unit="rad",                  &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Include 'inc/startXML/pOffsetDoppler.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",           &
     &                         error=errorXML)
!
