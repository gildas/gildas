!
! $Id$
!
!
!D      write (6,*) "      ---> inc/startXML/segCurveXML.inc"
!
Call pakoXMLwriteStartElement("RESOURCE","segment",         &
     &                         error=errorXML)
!
Write (valueC,*) segList(ii)%segType
Call pakoXMLwriteElement("PARAM","type",valueC,             &
     &                         dataType="char",             &
     &                         error=errorXML)
!
Include 'inc/startXML/pStartEnd.inc'
!
Write (valueC,*)       Real(segList(ii)%CPstart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%CPstart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xCpStart",valueC,         &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%CPstart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%CPstart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yCpStart",valueC,         &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%CPend%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%CPend%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xCpEnd",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%CPend%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%CPend%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yCpEnd",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Include 'inc/startXML/speedStartEnd.inc'
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","segment",           &
     &                         error=errorXML)
!










