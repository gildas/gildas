!
!  Id: optionTuneXML.inc, v  1.2.4 2015-05-28 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tune",                                 &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doTune) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doTune",valueC,                                &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","tune",                                   &
     &                         error=errorXML)                                   !
!
