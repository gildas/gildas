!
!  Id: optionPhaseXML.inc, v 1.1.12 2012-03-08 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","phase",                                &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%phase%x)
Write (valueComment,*) Real(vars(iValue)%phase%x)," "
Call pakoXMLwriteElement("PARAM","phaseX",valueC,                                &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%phase%y)
Write (valueComment,*) Real(vars(iValue)%phase%y)," "
Call pakoXMLwriteElement("PARAM","phaseY",valueC,                                &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","phase",                                  &
     &                         error=errorXML)                                   !
!




