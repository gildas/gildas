!
!  Id: optionDoubleBeamXML.inc, v 1.0.6.1 2007-01-13 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","DoubleBeam",                           &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doDoubleBeam) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doDoubleBeam",valueC,                          &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","DoubleBeam",                             &
     &                         error=errorXML)                                   !
!
