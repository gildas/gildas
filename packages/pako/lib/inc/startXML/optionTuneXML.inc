  !
  !  Id: optionTuneXML.inc, v 1.2.3 2014-10-14 Hans Ungerechts
  !
  Call pakoXMLwriteStartElement("RESOURCE","tune",                               &
       &                         error=errorXML)                                 !
  !
  If (vars(iValue)%doTune) Then 
     valueC = "T"
  Else
     valueC = "F"
  End If
  Call pakoXMLwriteElement("PARAM","doTune",valueC,                              &
       &                         dataType="boolean",                             &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(vars(iValue)%OffsetTune%x*GV%angleUnit)
  Write (valueComment,*) Real(vars(iValue)%OffsetTune%x/GV%angleFactorR)," ",    &
       &                                             GV%angleUnitSetC            !
  Call pakoXMLwriteElement("PARAM","xOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(vars(iValue)%OffsetTune%y*GV%angleUnit)
  Write (valueComment,*) Real(vars(iValue)%OffsetTune%y/GV%angleFactorR)," ",    &
       &                                             GV%angleUnitSetC            !
  Call pakoXMLwriteElement("PARAM","yOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  valueC = systemName
  !
  !D   Write (6,*) " systemName: --> ", systemName, "<-- "
  !D   Write (6,*) " valueC:     --> ", valueC,     "<-- "
  !
  Call pakoUmatchKey (                                                           &
       &              keys=offsetChoicesPako,                                    &
       &              key=valueC,                                                &
       &              command=" ",                                               &
       &              howto='Start Upper',                                       &
       &              iMatch=iMatch,                                             & 
       &              nMatch=nMatch,                                             &
       &              error=errorM,                                              &
       &              errorCode=errorCode                                        &
       &             )                                                           !
  !
  valueC = offsetChoices(iMatch)
  !
  Call pakoXMLwriteElement("PARAM","systemName",valueC,                          &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteEndElement("RESOURCE","tune",                                 &
       &                         error=errorXML)                                 !
  !
