!
!  Id: optionSystemXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","system",                               &
     &                         error=errorXML)                                   !
!
valueC = vars(iValue)%systemName                
!
Call pakoUmatchKey (                                                             &
     &              keys=offsetChoicesPako,                                      &
     &              key=valueC,                                                  &
     &              command=" ",                                                 &
     &              howto='Start Upper',                                         &
     &              iMatch=iMatch,                                               & 
     &              nMatch=nMatch,                                               &
     &              error=errorM,                                                &
     &              errorCode=errorCode                                          &
     &             )                                                             !
!
valueC = offsetChoices(iMatch)
!
Call pakoXMLwriteElement("PARAM","systemName",valueC,                            &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","system",                                 &
     &                         error=errorXML)                                   !
!
