  !
  !  Id: otfLinearXML.inc,v 1.2.0 2012-06-12 Hans Ungerechts
  !
  !D Write (6,*) "      ---> StartXML/otfLinearXML.f90 "
  !
  Call pakoXMLwriteStartElement("RESOURCE","subscan",                            &
       &                         error=errorXML,                                 &
       &                         comment="OTF linear")                           !
  !
  valueC = 'onTheFly'
  Call pakoXMLcase(valueC,error=errorXML)
  Call pakoXMLwriteElement("PARAM","type",valueC,                                &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
!!    Note 2012-02-15:
!!$   Write (valueC,*) segList(ii)%systemName  !!  causes segmentation error with ifort v 1.11  !!
  !
  valueC = segList(ii)%systemName
  !
  Call pakoXMLcase(valueC,error=errorXML)
  If (valueC.Eq."horizontalT") valueC="horizontalTrue"
  Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                        &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  !! TBD   replace above by:
  !! TBD   Include 'inc/startXML/systemOffset.inc'
  !
  Include 'inc/startXML/pOffsetDoppler.inc'
  !
  Call pakoXMLwriteStartElement("RESOURCE","segments",  error=errorXML)    
  !
  Call pakoXMLwriteStartElement("RESOURCE","segment",   error=errorXML)
  !
  valueC = segList(ii)%segType
!!Write (valueC,*) segList(ii)%segType
  Call pakoXMLcase(valueC,error=errorXML)
  Call pakoXMLwriteElement("PARAM","type",valueC,                                &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
!
  Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),             &
     &                                       " ",GV%angleUnitSetC                !
  Call pakoXMLwriteElement("PARAM","xStart",valueC,                              &
       &                         "rad", "float",                                 &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
!
  Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),             &
       &                                       " ",GV%angleUnitSetC              !
  Call pakoXMLwriteElement("PARAM","yStart",valueC,                              &
       &                         "rad", "float",                                 &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pEnd%x*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pEnd%x/GV%angleFactorR),               &
       &                                     " ",GV%angleUnitSetC                !
  Call pakoXMLwriteElement("PARAM","xEnd",valueC,                                &
       &                         "rad", "float",                                 &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pEnd%y*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pEnd%y/GV%angleFactorR),               &
       &                                     " ",GV%angleUnitSetC                !
  Call pakoXMLwriteElement("PARAM","yEnd",valueC,                                &
       &                         "rad", "float",                                 &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%speedStart*GV%speedUnit)
  Write (valueComment,*) Real(segList(ii)%speedStart/GV%speedFactorR),           &
       &                                         " ",GV%speedUnitSetC            !
  Call pakoXMLwriteElement("PARAM","speedStart",valueC,                          &
       &                         "rad/s", "float",                               &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%speedEnd*GV%speedUnit)
  Write (valueComment,*) Real(segList(ii)%speedEnd/GV%speedFactorR),             &
       &                                       " ",GV%speedUnitSetC              !
  Call pakoXMLwriteElement("PARAM","speedEnd",valueC,                            &
       &                         "rad/s", "float",                               &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteElement("PARAM","traceFlag","on",                             &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteEndElement("RESOURCE","segment",    error=errorXML)
  !
  Call pakoXMLwriteEndElement("RESOURCE","segments",   error=errorXML)
  !
  Call pakoXMLwriteEndElement("RESOURCE","subscan",    error=errorXML)
  !






