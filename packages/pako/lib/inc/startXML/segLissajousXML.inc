!
!  Id: segLissajousXML.inc,v 1.0.10.2 2008-10-06 Hans Ungerechts
!
!D  Write (6,*) "      ---> inc/startXML/segLissajousXML.inc"
!
Call pakoXMLwriteStartElement("RESOURCE","segment",                              &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteElement("PARAM","type",seg%lissajous,                           &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Include 'inc/startXML/pCenterXML.inc'
Include 'inc/startXML/xyAmplitudeXML.inc'
Include 'inc/startXML/xyOmegaXML.inc'
Include 'inc/startXML/xyPhaseXML.inc'
!
Include 'inc/startXML/tSubscanXML.inc'
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","segment",                      &
     &                         error=errorXML)
!





