!
!   Id: segCalGrid.inc v0.96 2005-12-05 Hans Ungerechts
!
!D      write (6,*) "      ---> StartXML/segCalGridXML.f90 "
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",                              &
     &                         error=errorXML)
!
Call pakoXMLwriteElement("PARAM","type","calGrid",                               &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
Write (valueC,*) Real(segList(ii)%tSegment)
Call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,                    &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",                                &
     &                         error=errorXML)
!
