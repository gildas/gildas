!
!  Id: parametersOffsetsXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Write (valueC,*)       Real(vars(iValue)%Offset%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%Offset%x/GV%angleFactorR)," ",          &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%Offset%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%Offset%y/GV%angleFactorR)," ",          &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
