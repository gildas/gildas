!
      write (6,*)  "      StarXML/switchingModeXML.f90 "
      write (6,*)  "              switchingMode  -->",switchingMode(1:lenc(switchingMode)),"<--"
!
      select case (switchingMode(1:lenc(switchingMode)))
!
         case('SWBEAM')
            call writeXMLswBeam(programName,LINE,commandToSave,        &
     &        OMselected, iUnit, ERROR)
!
         case('SWTOTAL')
            call writeXMLswTotalPower(programName,LINE,commandToSave,  &
     &        OMselected, iUnit, ERROR)
!
         case('SWWOBBLER')
            call writeXMLswWobbler(programName,LINE,commandToSave,  &
     &        OMselected, iUnit, ERROR)
!
         case default
!
      end select
