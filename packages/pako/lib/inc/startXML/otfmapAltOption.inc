!
!  Id: optionAltOptionXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
!  TBD: this should be more properly handled thorugh standardized choices!
!
Call pakoXMLwriteStartElement("RESOURCE","alternativeSpeedTotf",                 &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%altOption.Eq."SPEED") Then
   valueC = "speed"
Else
   valueC = "timePerOtf"
End If
!
Call pakoXMLwriteElement("PARAM","alternativeSpeedTotf",valueC,                  &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","alternativeSpeedTotf",                   &
     &                         error=errorXML)                                   !
!
