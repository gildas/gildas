!
!  Id: optionStepXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","step",                                 &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%Delta%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%Delta%x/GV%angleFactorR)," ",           &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xStep",valueC,                                 &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%Delta%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%Delta%y/GV%angleFactorR)," ",           &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yStep",valueC,                                 &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","step",                                   &
     &                         error=errorXML)                                   !
!
