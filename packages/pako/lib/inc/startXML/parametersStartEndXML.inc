!
!  Id: parametersStartEnd.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Write (valueC,*)       Real(vars(iValue)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pStart%x/GV%angleFactorR)," ",          &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xStart",valueC,                                &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pStart%y/GV%angleFactorR)," ",          &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yStart",valueC,                                &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
!
Write (valueC,*)       Real(vars(iValue)%pEnd%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pEnd%x/GV%angleFactorR)," ",            &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xEnd",valueC,                                  &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%pEnd%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pEnd%y/GV%angleFactorR)," ",            &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yEnd",valueC,                                  &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
