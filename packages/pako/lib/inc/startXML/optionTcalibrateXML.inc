!
!  Id: optionTcalibrateXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tCalibrate",                           &
     &                         error=errorXML)
!
Write (valueC,*)       Real(vars(iValue)%tCalibrate)
Call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,                    &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","tCalibrate",                             &
     &                         error=errorXML)
!
