  !
  !  Id: optionLengthFocusXML.inc, v 1.2.0 2012-06-21 Hans Ungerechts
  !
  Write (valueC,*)       Real(vars(iValue)%lengthFocus)
  Call pakoXMLwriteElement("PARAM","lengthFocus",valueC,                         &
       &                         dataType="float",                               &
       &                         unit="mm",                                      &
       &                         error=errorXML)                                 !
  !

