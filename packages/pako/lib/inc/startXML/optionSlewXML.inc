!
!  Id: optionSlewXML.inc, v  1.0.6.1 2007-01-17 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","slew",                                 &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doSlew) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doSlew",valueC,                                &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","slew",                                   &
     &                         error=errorXML)                                   !
!
