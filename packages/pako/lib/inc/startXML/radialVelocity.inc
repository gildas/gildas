!
! Id: radialVelocity.inc v0.94 2005-11-06 Hans Ungerechts
!
!D Write (6,*)  "     radialVelocity.inc  "
!
!
Call pakoXMLwriteStartElement("RESOURCE","radialVelocity",                       &
     &                         error=errorXML)
!
! TBD: reference frame according to user input!
!
valueC = vars(iValue)%velocity%type
Call pakoXMLwriteElement("PARAM","referenceFrame",valueC,                        &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
Write (valueC,*) "optical"
Call pakoXMLwriteElement("PARAM","convention",valueC,                            &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
Write (valueC,'(E20.10)') vars(iValue)%velocity%value*1.0D3
Write (valueComment,'(F20.3,A)') vars(iValue)%velocity%value, " km/s"
Call pakoXMLwriteElement("PARAM","velocity",valueC,                              &
     &                         unit="m/s",                                       &
     &                         dataType="double",                                &
     &                         comment=valueComment,                             &
     &                         error=errorXML)
!
Write (valueC,*)       0.0
Write (valueComment,*) 0.0,   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xOffsetDoppler",valueC,                        &
     &                         "rad", "float",                                   &
     &                         comment=valueComment,                             &
     &                         error=errorXML)
!
Write (valueC,*)       0.0
Write (valueComment,*) 0.0,   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yOffsetDoppler",valueC,                        &
     &                         "rad", "float",                                   &
     &                         comment=valueComment,                             &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement  ("RESOURCE","radialVelocity",                       &
     &                         error=errorXML)
!
