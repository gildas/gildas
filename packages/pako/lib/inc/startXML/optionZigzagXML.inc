!
!  Id: optionZigzagXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","zigzag",                               &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doZigzag) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doZigzag",valueC,                              &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","zigzag",                                 &
     &                         error=errorXML)                                   !
!
