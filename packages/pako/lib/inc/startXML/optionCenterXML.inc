!
!  Id: optionCenterXML.inc, v 1.1.12 2012-03-08 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","center",                               &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%pCenter%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pCenter%x/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xCenter",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%pCenter%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%pCenter%y/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yCenter",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","center",                                 &
     &                         error=errorXML)                                   !
!




