  !
  ! Id: trackXML.inc,v 1.1.13 2012-10-18 Hans Ungerechts
  !
  !D   Write (6,*) "      ---> StartXML/trackXML.inc"
  !D   Write (6,*) "           segList(ii)%flagTune: ", segList(ii)%flagTune
  !
  !
  ! *** subscan Track:
  !
  !
  If (segList(ii)%flagTune) Then
     Call pakoXMLwriteStartElement("RESOURCE","subscan",                         &
          &                         error=errorXML,                              &
          &                         comment="tune the tone")                     !
     Call pakoXMLwriteElement("PARAM","type",ss%tune,                            &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
  Else If (.Not.segList(ii)%flagTune) Then
     Call pakoXMLwriteStartElement("RESOURCE","subscan",                         &
          &                         error=errorXML,                              &
          &                         comment=ss%track)                            !
     Call pakoXMLwriteElement("PARAM","type",ss%track,                           &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
  End If
  !
  Include 'inc/startXML/systemOffset.inc'
  Include 'inc/startXML/pOffset.inc'
  Include 'inc/startXML/tSubscanXML.inc'
  Include 'inc/startXML/traceFlagXML.inc'
!
      if (segList(ii)%flagOn) Then
         valueC = traceflag%on
      else if (segList(ii)%flagRef) Then
         valueC = traceflag%ref
      else
         valueC = traceflag%on
      end if
      call pakoXMLcase(valueC,error=errorXML)
      call pakoXMLwriteElement("PARAM","traceFlag",valueC,             &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
  !
  If (.Not.segList(ii)%flagTune) Then
     Include 'inc/startXML/pOffsetDoppler.inc'
  End If
  !
  Call pakoXMLwriteEndElement("RESOURCE","subscan",                              &
       &                         error=errorXML)                                 !
  !


