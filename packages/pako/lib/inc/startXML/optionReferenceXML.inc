!
!  Id: optionReferenceXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","reference",                            &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doReference) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doReference",valueC,                           &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%OffsetR%x*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%OffsetR%x/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","xOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%OffsetR%y*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%OffsetR%y/GV%angleFactorR)," ",         &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","yOffset",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
!
valueC = vars(iValue)%systemNameRef
!
Call pakoUmatchKey (                                                             &
     &              keys=offsetChoicesPako,                                      &
     &              key=valueC,                                                  &
     &              command=" ",                                                 &
     &              howto='Start Upper',                                         &
     &              iMatch=iMatch,                                               & 
     &              nMatch=nMatch,                                               &
     &              error=errorM,                                                &
     &              errorCode=errorCode                                          &
     &             )                                                             !
!
valueC = offsetChoices(iMatch)
!
Call pakoXMLwriteElement("PARAM","systemName",valueC,                            &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","reference",                              &
     &                         error=errorXML)                                   !
!




