!
!  Id: optionFrequencyXML.inc, v 1.1.12 2012-03-08 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","frequency",                            &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%frequency%x)
Write (valueComment,*) Real(vars(iValue)%frequency%x)," "
Call pakoXMLwriteElement("PARAM","frequencyX",valueC,                            &
     &                         dataType="float",                                 &
     &                         unit="Hz",                                        &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%frequency%y)
Write (valueComment,*) Real(vars(iValue)%frequency%y)," "
Call pakoXMLwriteElement("PARAM","frequencY",valueC,                             &
     &                         dataType="float",                                 &
     &                         unit="Hz",                                        &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","frequency",                              &
     &                         error=errorXML)                                   !
!




