  !
  !  Id: onFocusXML.inc, v1.2.0 2012-06-21 Hans Ungerechts
  !
  !D      write (6,*) "      ---> StartXML/onFocusXML.f90 "
  !
  Call pakoXMLwriteStartElement("RESOURCE","subscan",                            &
       &                         error=errorXML,                                 &
       &                         comment="onFocus")                              !
  !
  Call pakoXMLwriteElement("PARAM","type","onFocus",                             &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*) Real(segList(ii)%tSegment)
  Call pakoXMLwriteElement("PARAM","timePerSubscan",valueC,                      &
       &                         dataType="float",                               &
       &                         unit="s",                                       &
       &                         error=errorXML)                                 !
  !
!!$      Write (valueC,*)  segList(ii)%systemName
  valueC = segList(ii)%systemName
  Call pakoXMLcase(valueC,error=errorXML)
  If (valueC.Eq."horizontalT") valueC="horizontalTrue"
  Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                        &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pStart%x), GV%angleUnitC
  Call pakoXMLwriteElement("PARAM","xOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
  Write (valueComment,*) Real(segList(ii)%pStart%y), GV%angleUnitC
  Call pakoXMLwriteElement("PARAM","yOffset",valueC,                             &
       &                         dataType="float",                               &
       &                         unit="rad",                                     &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
  ! TBD: use of linear unit. for now we use default: mm
  Write (valueC,*)       Real(segList(ii)%focusOffset)
  Write (valueComment,*) Real(segList(ii)%focusOffset), "mm"
  Call pakoXMLwriteElement("PARAM","focusOffset",valueC,                         &
       &                         dataType="float",                               &
       &                         unit="mm",                                      &
       &                         comment=valueComment,                           &
       &                         error=errorXML)                                 !
  !
!!$   Write (valueC,*)  segList(ii)%directionFocus
  valueC = segList(ii)%directionFocus 
  Call pakoXMLcase(valueC,error=errorXML)
  Call pakoXMLwriteElement("PARAM","focusTranslation",valueC,                    &
       &                         error=errorXML)                                 !
  !
  Call pakoXMLwriteEndElement("RESOURCE","subscan",                              &
       &                         error=errorXML)                                 !
  !
