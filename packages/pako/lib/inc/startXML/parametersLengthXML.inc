!
!  Id: parametersLengthXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
!
Write (valueC,*)       Real(vars(iValue)%length*GV%angleUnit)
Write (valueComment,*) Real(vars(iValue)%length/GV%angleFactorR)," ",            &
     &                                             GV%angleUnitSetC              !
Call pakoXMLwriteElement("PARAM","length",valueC,                                &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
