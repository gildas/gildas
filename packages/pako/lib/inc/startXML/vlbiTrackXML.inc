!
!   Id: vlbiTrackXML.inc,v 1.0.4 2006-07-11 Hans Ungerechts
!
!D  Write (6,*) "      ---> StartXML/vlbiTrackXML.inc "
!
! *** subscan Track:
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",                              &
     &                         error=errorXML,                                   &
     &                         comment=ss%VLBI)
!
Call pakoXMLwriteElement("PARAM","type",ss%VLBI,                                 &
     &                         dataType="char",                                  &
     &                         error=errorXML)
!
Include 'inc/startXML/systemOffset.inc'
!
Include 'inc/startXML/pOffset.inc'
!
Include 'inc/startXML/tSubscanXML.inc'
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",                                &
     &                         error=errorXML)
!
