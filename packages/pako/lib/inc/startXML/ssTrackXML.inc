!
! $Id$
!
!D      write (6,*) "      ---> StartXML/ssTrackXML.f90 "
!
!D write (6,*) "      ---> inc/startXML/ssTrackXML.inc HU 2005-04-22"
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",                    &
     &                         error=errorXML,                         &
     &                         comment=ss%track)
!
Call pakoXMLwriteElement("PARAM","type",ss%track,                      &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
Include 'inc/startXML/systemOffsetXML.inc'
!
Include 'inc/startXML/pStartXML.inc'
!
Include 'inc/startXML/tSubscanXML.inc'
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",                      &
     &                         error=errorXML)
!


