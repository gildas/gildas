!
! Id: xyAmplitudeXML.inc,v 1.0.10.3 2008-09-15 Hans Ungerechts
!
Write (valueC,*)       Real(segList(ii)%xAmplitude*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%xAmplitude/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xAmplitude",valueC,                                                &
     &                         "rad", "float",                                                       &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
Write (valueC,*)       Real(segList(ii)%yAmplitude*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%yAmplitude/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yAmplitude",valueC,                                                &
     &                         "rad", "float",                                                       &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
