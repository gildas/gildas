!
!  Id: optionTotfXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tOtf",                                 &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%tOtf)
Call pakoXMLwriteElement("PARAM","timePerOtf",valueC,                            &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","tOtf",                                   &
     &                         error=errorXML)                                   !
!
