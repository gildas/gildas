!
!D      write (6,*) "      ---> StartXML/calSkyXML.f90 "
!
      call pakoXMLwriteStartElement("RESOURCE","subscan",              &
     &                         error=errorXML,                         &
     &                         comment="calSky")
!
      call pakoXMLwriteElement("PARAM","type","calSky",                &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
      write (valueC,*) real(segList(ii)%tSegment)
      call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,    &
     &                         dataType="float",                       &
     &                         unit="s",                               &
     &                         error=errorXML)
!
      write (valueC,*)  segList(ii)%systemName
      call pakoXMLcase(valueC,error=errorXML)
      call pakoXMLwriteElement("PARAM","systemOffset",valueC,          &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
      write (valueC,*)       real(segList(ii)%pStart%x*GV%angleUnit)
      write (valueComment,*) real(segList(ii)%pStart%x), GV%angleUnitC
      call pakoXMLwriteElement("PARAM","xOffset",valueC,               &
     &                         dataType="float",                       &
     &                         unit="rad",                             &
     &                         comment=valueComment,                   &
     &                         error=errorXML)
!
      write (valueC,*)       real(segList(ii)%pStart%y*GV%angleUnit)
      write (valueComment,*) real(segList(ii)%pStart%y), GV%angleUnitC
      call pakoXMLwriteElement("PARAM","yOffset",valueC,               &
     &                         dataType="float",                       &
     &                         unit="rad",                             &
     &                         comment=valueComment,                   &
     &                         error=errorXML)
!
      call pakoXMLwriteEndElement("RESOURCE","subscan",                &
     &                         error=errorXML)
!
