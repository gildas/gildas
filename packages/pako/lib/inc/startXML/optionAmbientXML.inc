!
!  Id: optionAmbientXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","ambient",                              &
     &                         error=errorXML)
!
If (vars(iValue)%doAmbient) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doAmbient",valueC,                             &
     &                         dataType="boolean",                               &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","ambient",                                &
     &                         error=errorXML)
!
