  !
  !  Id: optionAirmassXML.inc, v 1.2.4   2015-06-11 Hans Ungerechts
  !  Id: optionAirmassXML.inc, v 1.0.5.2 2006-11-13 Hans Ungerechts
  !
  Call pakoXMLwriteStartElement("RESOURCE","airmass",                            &
       &                         error=errorXML)                                 !
  !
  If (vars(iValue)%hasAirmassList) Then                                          !  !!  airmass list --> table
     !
     !D      Write (6,*) " optionAirmassXML.inc, v 1.2.4   2015-05-28 : airmassList "
     !
     Call pakoXMLwriteStartElement("TABLE","airmass",                            &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteElement("FIELD","airmass",                                 &
          &                         dataType="float",                            &
          &                         unit="--",                                   &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteStartElement  ("DATA",                                     &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteStartElement  ("TABLEDATA",                                &
          &                         error=errorXML)                              !
     !
     Do ii = 1, vars(iIn)%nAir, 1
        !
        Call pakoXMLwriteStartElement("TR",                                      &
             &                         error=errorXML)                           !
        !
        Write (valueC,'(F12.3)')                                                 &
             &                   vars(iIn)%airmassList(ii)                       !
        Call pakoXMLwriteElement("TD",                                           &
             &                         content=valueC,                           &
             &                         error=errorXML)                           !
        !
        Call pakoXMLwriteEndElement("TR",                                        &
             &                         error=errorXML)                           !
        !
     End Do
     !
     Call pakoXMLwriteEndElement  ("TABLEDATA",                                  &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement  ("DATA",                                       &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement  ("TABLE","airmass",                            &
          &                         error=errorXML)                              !
     !
  Else   !!!   not (vars(iValue)%hasAirmassList)                                 !  !!  airmass range
     !
     Write (valueC,*)       Real(vars(iValue)%airmass%from)
     Call pakoXMLwriteElement("PARAM","from",valueC,                             &
          &                         dataType="float",                            &
          &                         unit="--",                                   &
          &                         error=errorXML)                              !
     !
     Write (valueC,*)       Real(vars(iValue)%airmass%to)
     Call pakoXMLwriteElement("PARAM","to",valueC,                               &
          &                         dataType="float",                            &
          &                         unit="--",                                   &
          &                         error=errorXML)                              !
     !
     Write (valueC,*)       Real(vars(iValue)%airmass%by)
     Call pakoXMLwriteElement("PARAM","by",valueC,                               &
          &                         dataType="float",                            &
          &                         unit="--",                                   &
          &                         error=errorXML)                              !
     !
  End If   !!!   If (vars(iValue)%hasAirmassList) Then
  !
  Call pakoXMLwriteEndElement("RESOURCE","airmass",                              &
       &                         error=errorXML)                                 !
  !
