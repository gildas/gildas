  !
  !  Id: switchingMode.inc, v 1.2.3 2014-04-29 Hans Ungerechts
  !                                 cleanup
  !  Id: switchingMode.inc, v 1.2.1 2012-08-16 Hans Ungerechts
  !
  !D Write (6,*)  "      startXML/switchingMode.f90 "
  !D Write (6,*)  "              switchingMode  -->",                            &
  !D      &       GV%switchingMode(1:lenc(GV%switchingMode)),"<--"               !
  !
  Select Case (GV%switchingMode(1:lenc(GV%switchingMode)))
     !
  Case(swMode%Beam)
     Call writeXMLswBeam(programName,LINE,commandToSave,                         &
          &        iUnit, ERROR)                                                 !
     !
  Case(swMode%Freq)
     Call writeXMLswFrequency(programName,LINE,commandToSave,                    &
          &        iUnit, ERROR)                                                 !
     !
  Case(swMode%Total)
     Call writeXMLswTotalPower(programName,LINE,commandToSave,                   &
          &        iUnit, ERROR)                                                 !
     !
  Case(swMode%Wobb)
     Call writeXMLswWobbler(programName,LINE,commandToSave,                      &
          &        iUnit, ERROR)                                                 !
     !
  Case default
     Call writeXMLswTotalPower(programName,LINE,commandToSave,                   &
          &        iUnit, ERROR)                                                 !
     !
  End Select
  !
