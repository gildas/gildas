!
!  Id: optionTpaddleXML.inc, v 1.0.9.4 2009-02-17 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tPaddle",                              &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%tPaddle)
Call pakoXMLwriteElement("PARAM","timePerPaddle",valueC,                         &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","tPaddle",                                &
     &                         error=errorXML)                                   !
!
