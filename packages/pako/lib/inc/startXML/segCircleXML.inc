!
! $Id$
!
!D      write (6,*) "      ---> inc/startXML/segCircleXML.inc"
!
Call pakoXMLwriteStartElement("RESOURCE","segment",         &
     &                         error=errorXML)
!
Write (valueC,*) segList(ii)%segType
Call pakoXMLwriteElement("PARAM","type",valueC,             &
     &                         dataType="char",             &
     &                         error=errorXML)
!
Include 'inc/startXML/pStartEnd.inc'
!
Write (valueC,*)       Real(segList(ii)%turnAngle*degs)
Write (valueComment,*) Real(segList(ii)%turnAngle/GV%angleFactorR),  " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","turnAngle",valueC,        &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Include 'inc/startXML/speedStartEnd.inc'
!
Include 'inc/startXML/traceFlagXML.inc'
!
Call pakoXMLwriteEndElement("RESOURCE","segment",           &
     &                         error=errorXML)
!





