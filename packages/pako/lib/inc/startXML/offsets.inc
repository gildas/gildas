  !
  ! Id: startXMLoffsets.inc ,v 1.2.3 2014-09-01 Hans Ungerechts
  ! Id: offsets.inc,v 1.2.3 2014-09-01 Hans Ungerechts
  ! Id: offsets.inc,v 1.0.4 2006-07-13 Hans Ungerechts
  !
  !D   Write (6,*)  " "
  !D   Write (6,*)  " write XML --> startXMLoffsets.f90 ,v1.2.3 2014-09-01"
  !D   !
  !D   Do ii  =  1, nDimOffsetChoices, 1
  !D      !
  !D      Write (6,*) "   ii : ", ii
  !D      Write (6,*) "  GV%sourceOffsets(ii): "
  !D      Write (6,*)    GV%sourceOffsets(ii)
  !D      !
  !D   End Do
  !D   Write (6,*)  " "
  !D   Write (6,*)  " GV%arrayNasmythOffsetsAreSet : ", GV%arrayNasmythOffsetsAreSet
  !D   Write (6,*)  " "
  !
  nCount = 0
  !
  !D Write (6,*)  "     GV%omSystemName:   ",   GV%omSystemName     
  !
  Do ii = nDimOffsetChoices, 1, -1
     !D   Write (6,*) " ii, systemName ", ii, varsOffsets(iValue,ii)%systemName
     If (      varsOffsets(iValue,ii)%isSet       .And.                          &
          &    varsOffsets(iValue,ii)%systemName .Ne. GV%omSystemName) Then      !
        nCount = nCount+1
     End If
  End Do
  !
  !D   Write (6,*)  "     nCount:            ", nCount
  !D   Write (6,*)  " "
  !
  If (GV%arrayNasmythOffsetsAreSet .Or. nCount.Ge.1) Then
     !
     Call pakoXMLwriteStartElement("RESOURCE","offsets",                         &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteStartElement("TABLE","offsets",                            &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteElement("FIELD","xOffset",                                 &
          &                         dataType="float",                            &
          &                         unit="rad",                                  &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteElement("FIELD","yOffset",                                 &
          &                         dataType="float",                            &
          &                         unit="rad",                                  &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteElement("FIELD","systemOffset",                            &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteStartElement("DATA",                                       &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteStartElement("TABLEDATA",                                  &
          &                         error=errorXML)                              !
     !
  End If   !!   
  !
  If (nCount.Ge.1) Then
     !
     ! **
     Do ii = 1, nDimOffsetChoices, 1
        !
        !D         Write (6,*) "   ii:   ", ii
        !
        If (      varsOffsets(iValue,ii)%isSet       .And.                       &
             &    varsOffsets(iValue,ii)%systemName .Ne. GV%omSystemName) Then   !
           !
!!$           If  (        ii.Ne.iNas                                               &
!!$                &  .Or.   .Not. GV%arrayNasmythOffsetsAreSet ) Then              !  !!  "normal case"
              !
           !D               Write (6,*) "   ii:   ", ii
           !D               Write (6,*) "   --> simple case: offsets from OFFSET command "
              !
              Call pakoXMLwriteStartElement("TR",                                &
                   &                         error=errorXML)                     !
              !
              Write (valueC,*)                                                   &
                   &  Real(varsOffsets(iValue,ii)%point%x*GV%angleUnit)          !
              Write (valueComment,*)                                             &
                   &  Real(varsOffsets(iValue,ii)%point%x/GV%angleFactorR),      &
                   &  GV%angleUnitSetC                                           !
              Call pakoXMLwriteElement("TD",                                     &
                   &                         content=valueC,                     &
                   &                         comment=valueComment,               &
                   &                         error=errorXML)                     !
              !
              Write (valueC,*)                                                   &
                   &  Real(varsOffsets(iValue,ii)%point%y*GV%angleUnit)          !
              Write (valueComment,*)                                             &
                   &  Real(varsOffsets(iValue,ii)%point%y/GV%angleFactorR),      &
                   &  GV%angleUnitSetC                                           !
              Call pakoXMLwriteElement("TD",                                     &
                   &                         content=valueC,                     &
                   &                         comment=valueComment,               &
                   &                         error=errorXML)                     !
              !
              valueC = offsetChoices(ii)
              Call pakoXMLwriteElement("TD",                                     &
                   &                         content=valueC,                     &
                   &                         error=errorXML)                     !
              !
              Call pakoXMLwriteEndElement("TR",                                  &
                   &                         error=errorXML)                     !
              !
!!$           End If   !!   ii.Ne.iNas
!!$           !        !!   .Or.   .Not. GV%arrayNasmythOffsetsAreSet
           !
        End If   !!   varsOffsets(iValue,ii)%isSet .And.
        !        !!     varsOffsets(iValue,ii)%systemName .Ne. GV%omSystemName
        !
     End Do   !!   ii = 1, nDimOffsetChoices, 1
     !
  End If
  !
  If       (            GV%arrayNasmythOffsetsAreSet                             &
       &     .And. .Not.varsOffsets(iValue,iNas)%isSet                           &
       &   )  Then                                                               !  !!  Nasmyth from Receiver channel / pixel
     !
     !D      Write (6,*) "   --> special case: Nasmyth from Receiver "
     !
     Call pakoXMLwriteStartElement("TR",                                         &
          &                         error=errorXML)                              !
     !                                                                           
     Write (valueC,*)                                                            &
          &  Real(GV%sourceOffsets(iNas)%point%x*GV%angleUnit)                   !
     Write (valueComment,*)                                                      &
          &  Real(GV%sourceOffsets(iNas)%point%x/GV%angleFactorR),               &
          &  GV%angleUnitSetC                                                    !
     Call pakoXMLwriteElement("TD",                                              &
          &                         content=valueC,                              &
          &                         comment=valueComment,                        &
          &                         error=errorXML)                              !
     !                                                                           
     Write (valueC,*)                                                            &
          &  Real(GV%sourceOffsets(iNas)%point%y*GV%angleUnit)                   !
     Write (valueComment,*)                                                      &
          &  Real(GV%sourceOffsets(iNas)%point%y/GV%angleFactorR),               &
          &  GV%angleUnitSetC                                                    !
     Call pakoXMLwriteElement("TD",                                              &
          &                         content=valueC,                              &
          &                         comment=valueComment,                        &
          &                         error=errorXML)                              !
     !                                                                           
     valueC = offs%nas                                                           
     Call pakoXMLwriteElement("TD",                                              &
          &                         content=valueC,                              &
          &                         error=errorXML)                              !
     !                                                                           
     Call pakoXMLwriteEndElement("TR",                                           &
          &                         error=errorXML)                              !
     !
  End If   !!   GV%arrayNasmythOffsetsAreSet                        
  !        !!      .And. .Not.varsOffsets(iValue,iNas)%isSet   
  !
  ! **
  !
  If (GV%arrayNasmythOffsetsAreSet .Or. nCount.Ge.1) Then
     !
     Call pakoXMLwriteEndElement  ("TABLEDATA",                                  &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement  ("DATA",                                       &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement  ("TABLE","offsets",                            &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement  ("RESOURCE","offsets",                         &
          &                         error=errorXML)                              !
     !
  End If   !!    GV%arrayNasmythOffsetsAreSet .Or. nCount.Ge.1
  !
