!
! $Id$
!
Write (valueC,*)       Real(segList(ii)%pEnd%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pEnd%x/GV%angleFactorR),     " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xEnd",valueC,             &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pEnd%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pEnd%y/GV%angleFactorR),     " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yEnd",valueC,             &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
