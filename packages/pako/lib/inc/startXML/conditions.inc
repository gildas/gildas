  !
  !   Id: inc/startXML/conditions.inc, v1.1.12 2012-02-27 Hans Ungerechts
  !
  If (GV%condElevation%isSet                                                     &
       &   .And.GV%condElevation%maximum/degree.Lt.85.00-0.01) Then              !
     !
     Call pakoXMLwriteStartElement("RESOURCE","conditions",                      &
          &                         space ="before",                             &
          &                         error=errorXML)                              ! 
     !
     Call pakoXMLwriteStartElement("RESOURCE","condition",                       &
          &                         error=errorXML)                              !   
     !
     valueC = GV%condElevation%parameter
     Call pakoXMLwriteElement("PARAM","parameter",valueC,                        &
          &                         dataType="char",                             &
          &                         error=errorXML)                              !
     !
     Write (valueC,*)       GV%condElevation%minimum
     Write (valueComment,'(F10.2,A,A)')                                          &
          &                 GV%condElevation%minimum/degree, " ", "degrees"      !
     Call pakoXMLwriteElement("PARAM","minimum",valueC,                          &
          &                         unit=GV%condElevation%unit,                  &
          &                         dataType="float",                            &
          &                         comment=valueComment,                        &
          &                         error=errorXML)                              !
     !
     Write (valueC,*)       GV%condElevation%maximum
     Write (valueComment,'(F10.2,A,A)')                                          &
          &                 GV%condElevation%maximum/degree, " ", "degrees"      !
     Call pakoXMLwriteElement("PARAM","maximum",valueC,                          &
          &                         unit=GV%condElevation%unit,                  &
          &                         dataType="float",                            &
          &                         comment=valueComment,                        &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement("RESOURCE","condition",                         &
          &                         error=errorXML)                              !
     !
     Call pakoXMLwriteEndElement("RESOURCE","conditions",                        &
          &                         error=errorXML)                              !
     !
  End If
