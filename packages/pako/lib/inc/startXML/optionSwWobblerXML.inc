!
!  Id: optionSwWobblerXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","swWobbler",                            &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doSwWobbler) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doSwWobbler",valueC,                           &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","swWobbler",                              &
     &                         error=errorXML)                                   !
!
