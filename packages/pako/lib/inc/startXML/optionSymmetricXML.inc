!
!  Id: optionAmbientXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","symmetric",                            &
     &                         error=errorXML)                                   !
!
If (vars(iValue)%doSymmetric) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doSymmetric",valueC,                           &
     &                         dataType="boolean",                               &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","symmetric",                              &
     &                         error=errorXML)                                   !
!
