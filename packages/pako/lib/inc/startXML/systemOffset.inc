  !
  !   Id: inc/startXML/systemOffset.inc, v1.2.0 2012-02-15 Hans Ungerechts
  !
!!$   Write (valueC,*) segList(ii)%systemName  !!  causes segmentation error with ifort v 1.11  !!
  !
  valueC = segList(ii)%systemName
  !
  Call pakoXMLcase(valueC,error=errorXML)
  If (valueC .Eq. "horizontalT") valueC = "horizontalTrue" 
  Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                        &
       &                         dataType="char",                                &
       &                         error=errorXML)                                 !
  !

