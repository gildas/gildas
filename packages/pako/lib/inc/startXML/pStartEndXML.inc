!
!D write (6,*) "      ---> inc/startXML/pStartEndXML.inc HU 2005-04-22"
!
Write (valueC,*)       Real(segList(ii)%pStart%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xStart",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pStart%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pStart%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yStart",valueC,           &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pEnd%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pEnd%x/GV%angleFactorR),     " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xEnd",valueC,             &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pEnd%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pEnd%y/GV%angleFactorR),     " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yEnd",valueC,             &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
