!
!     Id: slewElevationXML.inc,v 1.0.6.1 2007-01-17 Hans Ungerechts
!     
!     Family:   
!     Siblings: 
!
!----------------------------------------------------------------------
!
!
Write (6,*) "      ---> StartXML/slewElevationXML.inc "
!
Call pakoXMLwriteStartElement("RESOURCE","subscan",                              &
     &                         error=errorXML,                                   &
     &                         comment="slewElevation")                          !
!
Call pakoXMLwriteElement("PARAM","type","slewElevation",                         &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(segList(ii)%pStart%y*angleUnitTip)
Write (valueComment,*) Real(segList(ii)%pStart%y), segList(ii)%angleUnit
Call pakoXMLwriteElement("PARAM","elevationStart",valueC,                        &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(segList(ii)%pEnd%y*angleUnitTip)
Write (valueComment,*) Real(segList(ii)%pEnd%y), segList(ii)%angleUnit
Call pakoXMLwriteElement("PARAM","elevationEnd",valueC,                          &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(segList(ii)%pStart%x*angleUnitTip)
Write (valueComment,*) Real(segList(ii)%pStart%x), segList(ii)%angleUnit
Call pakoXMLwriteElement("PARAM","azimuth",valueC,                               &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(segList(ii)%speedStart*speedUnitTip)
Write (valueComment,*) Real(segList(ii)%speedStart), segList(ii)%speedUnit
Call pakoXMLwriteElement("PARAM","speed",valueC,                                 &
     &                         "rad/s", "float",                                 &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","subscan",                                &
     &                         error=errorXML)                                   !
!
