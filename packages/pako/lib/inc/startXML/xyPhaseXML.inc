!
! Id: xyPhaseXML.inc,v 1.0.10.3 2008-08-29 Hans Ungerechts
!
Write (valueC,*)       Real(segList(ii)%phiX)
Write (valueComment,*) Real(segList(ii)%phiX)/Pi,   " Pi"
Call pakoXMLwriteElement("PARAM","phiX",valueC,                                                      &
     &                         "rad", "float",                                                       &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
Write (valueC,*)       Real(segList(ii)%phiY)
Write (valueComment,*) Real(segList(ii)%phiY)/Pi,  " Pi"   
Call pakoXMLwriteElement("PARAM","phiY",valueC,                                                      &
     &                         "rad", "float",                                                       &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
