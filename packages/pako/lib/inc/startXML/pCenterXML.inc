!
Write (valueC,*)       Real(segList(ii)%pCenter%x*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pCenter%x/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","xCenter",valueC,          &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%pCenter%y*GV%angleUnit)
Write (valueComment,*) Real(segList(ii)%pCenter%y/GV%angleFactorR),   " ",GV%angleUnitSetC
Call pakoXMLwriteElement("PARAM","yCenter",valueC,          &
     &                         "rad", "float",              &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
