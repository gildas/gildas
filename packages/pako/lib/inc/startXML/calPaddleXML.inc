!
!D      write (6,*) "      ---> StartXML/calPaddleXML.f90 "
!
      call pakoXMLwriteStartElement("RESOURCE","subscan",              &
     &                         error=errorXML,                         &
     &                         comment="calPaddle")
!
      call pakoXMLwriteElement("PARAM","type","calPaddle",             &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
      write (valueC,*) real(segList(ii)%tSegment)
      call pakoXMLwriteElement("PARAM","timePerCalibration",valueC,    &
     &                         dataType="float",                       &
     &                         unit="s",                               &
     &                         error=errorXML)
!
      write (valueC,*) segList(ii)%systemName
      call pakoXMLcase(valueC,error=errorXML)
      call pakoXMLwriteElement("PARAM","systemOffset",valueC,          &
     &                         dataType="char",                        &
     &                         error=errorXML)
!
      write (valueC,*)       real(segList(ii)%pStart%x*angleUnitTip)
      write (valueComment,*) real(segList(ii)%pStart%x), segList(ii)%angleUnit
      call pakoXMLwriteElement("PARAM","xOffset",valueC,               &
     &                         dataType="float",                       &
     &                         unit="rad",                             &
     &                         comment=valueComment,                   &
     &                         error=errorXML)
!
      write (valueC,*)       real(segList(ii)%pStart%y*angleUnitTip)
      write (valueComment,*) real(segList(ii)%pStart%y), segList(ii)%angleUnit
      call pakoXMLwriteElement("PARAM","yOffset",valueC,               &
     &                         dataType="float",                       &
     &                         unit="rad",                             &
     &                         comment=valueComment,                   &
     &                         error=errorXML)
!
      call pakoXMLwriteEndElement("RESOURCE","subscan",               &
     &                         error=errorXML)
!
