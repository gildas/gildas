!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="StartXML/subscanTrackXML.inc">
!   <VERSION>
!                Id: StartXML/subscanTrackXML.inc 1.2.3 2014-06-25 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                StartXML/
!   </FAMILY>
!   <SIBLINGS>
!                subscanTrackXML.inc
!   </SIBLINGS>        
!
!   write XML for subscan, type subscanTrack
!
! </DOCUMENTATION> <!-- name="StartXML/subscanTrackXML.inc" -->
!
!----------------------------------------------------------------------
!
!D      write (6,*) "      ---> 
!
      Call pakoXMLwriteStartElement("RESOURCE","subscan",                        &
     &                         error=errorXML,                                   &
     &                         comment="subscanTrack")                           !
!
      Call pakoXMLwriteElement("PARAM","type","track",                           &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
      Write (valueC,*) Real(segList(ii)%tSegment)
      Call pakoXMLwriteElement("PARAM","timePerSubscan",valueC,                  &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
!!$   write (valueC,*)  segList(ii)%systemName                         !!  causes segmentation error with ifort v 1.11  !!
      valueC = segList(ii)%systemName
      Call pakoXMLcase(valueC,error=errorXML)
      Call pakoXMLwriteElement("PARAM","systemOffset",valueC,                    &
     &                         dataType="char",                                  &
     &                         error=errorXML)                                   !
!
      Write (valueC,*)       Real(segList(ii)%pStart%x*angleUnitTip)
      Write (valueComment,*) Real(segList(ii)%pStart%x), segList(ii)%angleUnit
      Call pakoXMLwriteElement("PARAM","xOffset",valueC,                         &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
      Write (valueC,*)       Real(segList(ii)%pStart%y*angleUnitTip)
      Write (valueComment,*) Real(segList(ii)%pStart%y), segList(ii)%angleUnit
      Call pakoXMLwriteElement("PARAM","yOffset",valueC,                         &
     &                         dataType="float",                                 &
     &                         unit="rad",                                       &
     &                         comment=valueComment,                             &
     &                         error=errorXML)                                   !
!
      Include 'inc/startXML/traceFlagXML.inc'
!
      Call pakoXMLwriteEndElement("RESOURCE","subscan",                          &
     &                         error=errorXML)                                   !
!
