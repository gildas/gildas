!
!  Id: optionColdXML.inc, v 1.0.2 2006-01-11 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","cold",                                 &
     &                         error=errorXML)
!
If (vars(iValue)%doCold) Then 
   valueC = "T"
Else
   valueC = "F"
End If
Call pakoXMLwriteElement("PARAM","doCold",valueC,                                &
     &                         dataType="boolean",                               &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","cold",                                   &
     &                         error=errorXML)
!
