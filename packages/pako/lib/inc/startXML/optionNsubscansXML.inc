!
!  Id: optionNsubscansXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","nSubscans",                            &
     &                         error=errorXML)
!
Write (valueC,*) vars(iValue)%Nsubscans
Call pakoXMLwriteElement("PARAM","nSubscans",valueC,                             &
     &                         dataType="int",                                   &
     &                         unit="--",                                        &
     &                         error=errorXML)
!
Call pakoXMLwriteEndElement("RESOURCE","nSubscans",                              &
     &                         error=errorXML)
!
