!
!  Id: optionTsubscanXML.inc, v 1.0.5.2 2006-11-10 Hans Ungerechts
!
Call pakoXMLwriteStartElement("RESOURCE","tSubscan",                             &
     &                         error=errorXML)                                   !
!
Write (valueC,*)       Real(vars(iValue)%tSubscan)
Call pakoXMLwriteElement("PARAM","timePerSubscan",valueC,                        &
     &                         dataType="float",                                 &
     &                         unit="s",                                         &
     &                         error=errorXML)                                   !
!
Call pakoXMLwriteEndElement("RESOURCE","tSubscan",                               &
     &                         error=errorXML)                                   !
!
