!
! Id: xyOmegaXML.inc,v 1.0.10.3 2008-09-18 Hans Ungerechts
!
Write (valueC,*)       Real(segList(ii)%omegaX)
Write (valueComment,*) Real(segList(ii)%omegaX)/(2*Pi),  " Hz"
Call pakoXMLwriteElement("PARAM","omegaX",valueC,                                                    &
     &                         "rad/s", "float",                                                     &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
Write (valueC,*)       Real(segList(ii)%omegaY)
Write (valueComment,*) Real(segList(ii)%omegaY)/(2*Pi),  " Hz"   
Call pakoXMLwriteElement("PARAM","omegaY",valueC,                                                    &
     &                         "rad/s", "float",                                                     &
     &                         comment=valueComment,                                                 &
     &                         error=errorXML)                                                       !
!
