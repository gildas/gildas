!
! $Id$
!
Write (valueC,*)       Real(segList(ii)%speedStart*GV%speedUnit)
Write (valueComment,*) Real(segList(ii)%speedStart/GV%speedFactorR), " ",GV%speedUnitSetC
Call pakoXMLwriteElement("PARAM","speedStart",valueC,       &
     &                         "rad/s", "float",            &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
Write (valueC,*)       Real(segList(ii)%speedEnd*GV%speedUnit)
Write (valueComment,*) Real(segList(ii)%speedEnd/GV%speedFactorR),   " ",GV%speedUnitSetC
Call pakoXMLwriteElement("PARAM","speedEnd",valueC,         &
     &                         "rad/s", "float",            &
     &                         comment=valueComment,        &
     &                         error=errorXML)
!
