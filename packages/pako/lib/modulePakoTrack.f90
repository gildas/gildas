!
!----------------------------------------------------------------------
!
! <DOCUMENTATION name="modulePakoTrack">
!   <VERSION>
!                Id: modulePakoTrack.f90 ,v 1.3.0 2017-08-28 Hans Ungerechts
!                                        ,v 1.2.3 2014-10-27 Hans Ungerechts
!                                        ,v 1.2.3 2014-02-10 Hans Ungerechts
!                                        ,v 1.2.3 2014-02-03 Hans Ungerechts
!                                         v 1.2.0 2012-08-17 Hans Ungerechts 
!                based on 
!                Id: modulePakoTrack.f90 ,v 1.1.0 2009-03-25 Hans Ungerechts
!   </VERSION>
!   <PROGRAM>
!                Pako
!   </PROGRAM>
!   <FAMILY>
!                Observing Modes
!   </FAMILY>
!   <SIBLINGS>
!                Calibrate
!                Focus
!                OnOff
!                OtfMap
!                Pointing
!                Tip
!                Track
!                VLBI
!   </SIBLINGS>        
!
!   Pako module for command: TRACK
!
! </DOCUMENTATION> <!-- name="modulePakoTrack" -->
!
!----------------------------------------------------------------------
!
Module modulePakoTrack
  !
  Use modulePakoTypes
  Use modulePakoMessages
  Use modulePakoGlobalParameters
  Use modulePakoLimits
  Use modulePakoXML
  Use modulePakoUtilities
  Use modulePakoPlots
  Use modulePakoDisplayText
  Use modulePakoGlobalVariables
  Use modulePakoSubscanList
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwWobbler
  Use modulePakoSwTotalPower
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  Public :: track
  Public :: saveTrack
  Public :: startTrack
  Public :: displayTrack
  Public :: plotOMTrack
  !     
  ! *** Variables for Track ***
  Include 'inc/variables/variablesTrack.inc'
!!!   
Contains
!!!
!!!
  Subroutine Track(programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    ! *** standard working variables ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Logical               :: isConnected   = .False.
    !
    !     TBD: the following shall be replaced by a SR reading from XML:
    Include 'inc/ranges/rangesTrack.inc'
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Track, v 1.3.0 2017-08-28 ---> SR: Track ")                 ! trace execution
    !
    ! *** initialize ***
    If (.Not.isInitialized) Then
       Include 'inc/variables/setDefaults.inc'
       isInitialized = .True.
    Endif
    !
    ! *** set In-values = previous Values ***
    If (.Not.ERROR) Then
       vars(iIn) = vars(iValue)
    End If
    !
    ! *** check N of Parameters: 0, n --> at  most n allowed  ***
    !     read Parameters, Options (to detect errors)            
    !     and check for validity and ranges                      
    Call checkNofParameters(command,                                             &
         &     0, 2,                                                             &
         &     nArguments,                                                       &
         &     errorNumber)                                                      !
    ERROR = ERROR .Or. errorNumber
    !
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOn = .True.)
       !
       Include 'inc/parameters/parametersOffsetsSourceName.inc'
       Include 'inc/options/readOptionsTrack.inc'
       !
    End If
    !
    ! *** set defaults ***
    Include 'inc/options/optionDefaults.inc'
    !
    ! *** read Parameters, Options (again, after defaults!) ***
    !     and check for validity and ranges 
    If (.Not. ERROR) Then
       Call pakoMessageSwitch (setOff = .True.)
       !
       Include 'inc/parameters/parametersOffsetsSourceName.inc'
       Include 'inc/options/readOptionsTrack.inc'
       !
    End If
    Call pakoMessageSwitch (setOn = .True.)
    !
    ! *** check consistency and                           ***
    ! *** store into temporary (intermediate) variables   ***
    If (.Not.error) Then
       !
       errorInconsistent = .False.
       !
       If (vars(iIn)%tSubscan .Lt. 5*GV%tRecord) Then
          If (vars(iIn)%tSubscan .Lt. 3*GV%tRecord) Then
             errorInconsistent = .True.
             Write (messageText,*)    "must be at least 3 times "                &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityE,severityE,                               &
                  &           command(1:l)//"/"//"tSubscan",messageText)         !
          Else
             Write (messageText,*)    "should be at least 5 times "              &
                  &                // "tRecord = tPhase*nPhases*nCycles = "      &
                  &                 ,  GV%tRecord                                &
                  &                 , "of switching mode "//GV%switchingMode     !
             Call pakoMessage(priorityW,severityW,                               &
                  &           command(1:l)//"/"//"tSubscan",messageText)         !
          End If
       End If
       !
       If (GV%switchingMode.Eq.swMode%Beam) Then
          errorInconsistent = .True.
          GV%notReadyTrack  = .True.
          Write (messageText,*)  "will not work with switching mode ", swModePako%Beam
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       !D        Write (6,*) "  GV%privilege::   ", GV%privilege
       !D        Write (6,*) "  GV%userLevel:    ", GV%userLevel
       !D        Write (6,*) "  GV%iUserLevel:   ", GV%iUserLevel
       !D        Write (6,*) "  GV%pulsarMode:   ", GV%pulsarMode
       If     (      GV%iUserLevel.Ge.iExperienced                               &
            &  .Or.  GV%privilege .Eq.setPrivilege%privileged                    &
            &  .Or.  GV%privilege .Eq.setPrivilege%staff                         &
            &  .Or.  GV%privilege .Eq.setPrivilege%ncsTeam                       &
            & )  Then                                                            !
          If (GV%switchingMode.Eq.swMode%Total) Then
             errorInconsistent = .False.
             Write (messageText,*)  "is not very useful with switching mode ",   &
                  &                  swModePako%Total                            !
             Call PakoMessage(priorityW,severityW,command,messageText)
          End If
          !
       Else If ( GV%pulsarMode .And. GV%switchingMode.Eq.swMode%Total) Then
          errorInconsistent = .False.
          Write (messageText,*)  "accepted with switching mode ",                &
               &                  swModePako%Total,                              &
               &      "because Backend /nSamples is set (pulsars, tests) "       !
          Call PakoMessage(priorityI,severityI,command,messageText)
          !
       ElseIf ( .Not.GV%pulsarMode .And. GV%switchingMode.Eq.swMode%Total) Then
          errorInconsistent = .True.
          GV%notReadyTrack  = .True.
          Write (messageText,*) " with switching mode ", swModePako%Total,       &
               &                " requires special setup or privileges "         !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       If (GV%switchingMode.Eq.swMode%Wobb) Then
          errorInconsistent = .True.
          GV%notReadyTrack  = .True.
          Write (messageText,*)  "will not work with switching mode ",           &
               &                  swModePako%Wobb                                !
          Call PakoMessage(priorityE,severityE,command,messageText)
       End If
       !
       Call queryReceiver(rec%bolo, isConnected)
       If (isConnected .And. GV%switchingMode.Eq.swMode%Wobb                     &
            &          .And. Abs(GV%secondaryRotation).GT.0.005) Then            !
          errorInconsistent = .True.
          GV%notReadyTrack  = .True.
          Write (messageText,*)                                                  &
               &   "will not work with SWWOBBLER and SET 2ndRotation ",          &
               &                 GV%secondaryRotation                            !
          Call PakoMessage(priorityE,severityE,command(1:l),messageText)
       End If
       !
       If (.Not. errorInconsistent) Then
          vars(iTemp) = vars(iIn)
       End If
       !
    End If
    !
    ERROR = ERROR .Or. errorInconsistent
    !
    ! *** store from temporary into final variables ***
    If (.Not.ERROR) Then
       vars(iValue) = vars(iTemp)
    End If
    !
    ! *** display values ***
    If (.Not.ERROR) Then
       Call displayTrack
    End If
    !
    ! *** set "selected" observing mode & plot ***
    If (.Not.error) Then
       GV%observingMode     = OM%track
       GV%observingModePako = OMpako%track
       GV%omSystemName      = vars(iValue)%systemName 
       GV%notReadySWafterOM = .False.
       GV%notReadySecondaryRafterOM = .False.
       !D       Write (6,*) '      GV%notReadySecondaryRafterOM: ',                       &
       !D            &             GV%notReadySecondaryRafterOM
       GV%notReadyTrack     = .False.
       trackCommand = line(1:lenc(line))
       Call analyzeTrack (errorA)
       Call listSegmentList (errorA)
       Call plotOMTrack (errorP)
    End If
    !
    Return
  End Subroutine Track
!!!
!!!
  Subroutine analyzeTrack (errorA)
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Integer                :: nScan 
    Character (len=lenCh)  :: aUnit, sUnit
    Integer                :: iSS           = 0     ! counts subcans/segment
    !
    errorA    = .False.
    iSS       = 0
    nScan     = 1111
    aUnit     = GV%angleUnitC                        ! 
    sUnit     = GV%speedUnitC                        ! 
    !
    ! TBD: calibrate subscans 
    !
    segList(:) = segDefault
    !
    Call countSubscans(value=0)
    Call countSegments(value=0)
    !
    Do kk = 1, vars(iValue)%nSubscans, 1
       !
       Call countSubscans(ii)
       Call countSegments(jj)
       !
       segList(ii)%newSubscan =  .True.
       segList(ii)%scanNumber =  nScan
       segList(ii)%ssNumber   =  ii
       segList(ii)%segNumber  =  1
       segList(ii)%ssType     =  ss%track
       segList(ii)%segType    =  seg%track
       segList(ii)%angleUnit  =  aUnit
       segList(ii)%speedUnit  =  sUnit
       segList(ii)%flagOn     =  .True.
       segList(ii)%flagRef    =  .False.
       segList(ii)%pStart     =  vars(iValue)%offset
       segList(ii)%pEnd       =  segList(ii)%pStart
       segList(ii)%speedStart =  0.0
       segList(ii)%speedEnd   =  0.0
       segList(ii)%systemName =  vars(iValue)%systemName
       segList(ii)%altOption  =  'tSegment'
       segList(ii)%tSegment   =  vars(iValue)%tSubscan
       segList(ii)%tRecord    =  0.1
          If (ii.Eq.1) Then
             segList(ii)%updateDoppler = .True.
             If (vars(iValue)%systemName.Eq.offs%pro) Then
                segList(ii)%pDoppler      = vars(iValue)%offset
             Else
                segList(ii)%pDoppler      = xyPointType(0.0,0.0)
             End If
          Else If (GV%doDopplerUpdates .And.                                     &
               &   vars(iValue)%systemName.Eq.offs%pro) Then                     !
             segList(ii)%updateDoppler = .True.
             segList(ii)%pDoppler      = vars(iValue)%offset
          End If
       !
    End Do
    !
    Call getCountSegments(nSegments)
    !
    Return 
    !
  End Subroutine analyzeTrack
!!!
!!!
  Subroutine displayTrack
    !
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    Character(len=24)  :: command
    !
    Include 'inc/display/commandDisplayTrack.inc'
    !
  End Subroutine displayTrack
!!!
!!!
  Subroutine plotOMTrack (errorP)
    !
    !**   Variables  for Plots   ***
    Include 'inc/variables/headerForPlotMethods.inc'
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    errorP = .False.
    !
    Call configurePlots
    !
    Call plotTrackStart(errorP)
    !
    Call plotSegmentList(errorP)
    !
    If (errorP)                                                                  &
         &        Call pakoMessage(priorityE,severityE,                          &
         &        'Track',' could not plot ')                                    !
    !
    Return
  End Subroutine plotOMTrack
!!!
!!!
  Subroutine saveTrack(programName,LINE,commandToSave,iUnit, ERROR)
    !
    !**   Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !      
    Call pako_message(seve%t,programName,                                        &
         &  " module Track, v 1.2.0  --> SR: saveTrack ")                        ! trace execution
    !
    contC = contNN
    !
    Include 'inc/commands/saveCommand.inc'
    !
    contC = contCC
    !
    Include 'inc/parameters/saveOffsets.inc'
    !
    !TBD:      Include 'inc/options/saveBalance.inc'
    !TBD:      Include 'inc/options/saveCalibrate.inc'
    Include 'inc/options/saveNsubscans.inc'
    Include 'inc/options/saveSystem.inc'
    !
    contC = contCN
    !
    Include 'inc/options/saveTsubscan.inc'
    !
    !
    Write (iUnit,*) "!"
    !
    Return
  End Subroutine saveTrack
!!!
!!!
  Subroutine startTrack(programName,LINE,commandToSave,iUnit, ERROR)
    !
    ! *** Variables ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    ! to format message text
    Character(len=lenLine)  ::  messageText
    !
    Character (len=lenCh)   :: valueC
    Logical                 :: errorL, errorXML
    !
    ERROR = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &  " module Track, v 1.2.0  --> SR: startTrack ")                       ! trace execution
    !
    Call pakoXMLsetOutputUnit(iunit=iunit)
    Call pakoXMLsetIndent(iIndent=2)
    !
    Include 'inc/startXML/generalHead.inc'
    !
    Call writeXMLset(programName,LINE,commandToSave,iUnit, ERROR)
    !
    !!! Call writeXMLversion !!! incorporated in generalHead above
    !
    Call pakoXMLwriteStartElement("RESOURCE","pakoScript",                       &
         &                         comment="save from pako",                     &
         &                         error=errorXML)                               !
    Call pakoXMLwriteStartElement("DESCRIPTION",                                 &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/savePakoScriptFirst.inc'
    !
    Call saveTrack        (programName,LINE,commandToSave, iUnit, errorL)
     !                                                                       
    Call pakoXMLwriteEndElement("DESCRIPTION",                                   &
         &                         doCdata=.True.,                               &
         &                         error=errorXML)                               !
    Call pakoXMLwriteEndElement  ("RESOURCE","pakoScript",                       &
         &                         space="after",                                &
         &                         error=errorXML)                               !
    !
    Call writeXMLantenna(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLreceiver(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Call writeXMLbackend(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/switchingMode.inc'
    !
    Call writeXMLsource(programName,LINE,commandToSave,iUnit, ERROR)
    !
    Include 'inc/startXML/generalScanHead.inc'
    !
    ! **  Observing Mode to XML
    If (gv%doXMLobservingMode) Then
       messageText = "writing observing mode in XML format"
       Call PakoMessage(priorityI,severityI,                                     &
               &           commandToSave,messageText)                            !
       Call writeXMLtrack(programName,LINE,commandToSave,                        &
         &         iUnit, ERROR)                                                 !
    End If
    !
    ! **  subscan list to XML
    Call writeXMLsubscanList(programName,LINE,commandToSave,iUnit, ERROR)
    !   
    Include 'inc/startXML/generalTail.inc'
    !
    Return
  End Subroutine startTrack
!!!
!!!
  Subroutine writeXMLtrack(programName,LINE,commandToSave,iUnit,ERROR)
    !
    ! *** Variables   ***
    Include 'inc/variables/headerForSaveMethods.inc'
    !
    Integer                 :: iMatch, nMatch, len1, len2
    Character (len=lenCh)   :: valueC, errorCode
    Character (len=lenLine) :: valueComment
    Logical                 :: errorM, errorXML
    !
    Call pako_message(seve%t,"PAKO",                                             &
         &  " module Track, v 1.2.3  --> SR: writeXMLTrack 2014-10-24")          ! trace execution
    !
    ERROR = .False.
    !
    !D Write (6,*) "   --> writeXMLtrack "
    !D Write (6,*) "       iUnit:    ", iUnit
    !
    Call pakoXMLwriteStartElement("RESOURCE","observingMode",                    &
         &                         space ="before",                              &
         &                         error=errorXML)                               !
    !
    valueC = GV%observingMode
    Call pakoXMLwriteElement("PARAM","observingMode",valueC,                     &
         &                         dataType="char",                              &
         &                         error=errorXML)                               !
    !
    Include 'inc/startXML/parametersOffsetsXML.inc'
    !
    Include 'inc/startXML/optionSystemXML.inc'
    Include 'inc/startXML/optionNsubscansXML.inc'
    Include 'inc/startXML/optionTsubscanXML.inc'
    !
    Include 'inc/startXML/annotationsXML.inc'
    !
    Call pakoXMLwriteEndElement("RESOURCE","observingMode",                      &
         &                         error=errorXML)                               !
    !
    Return
  End Subroutine writeXMLtrack
!!!
!!!
End Module modulePakoTrack
