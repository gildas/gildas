!
!  Id: modulePakoSave.f90, v 1.2.3 2014-10-23 Hans Ungerechts
!  Id: modulePakoSave.f90, v 1.2.2 2013-02-06 Hans Ungerechts
!  Id: modulePakoSave.f90, v 1.2.1 2012-08-20 Hans Ungerechts
!
Module modulePakoSave
  !
  Use modulePakoMessages
  Use modulePakoTypes
  Use modulePakoGlobalParameters
  Use modulePakoUtilities
  Use modulePakoGlobalVariables
  Use modulePakoCatalog
  Use modulePakoReceiver
  Use modulePakoBackend
  Use modulePakoSource
  Use modulePakoSwBeam
  Use modulePakoSwFrequency
  Use modulePakoSwTotalPower
  Use modulePakoSwWobbler
  Use modulePakoCalibrate
  Use modulePakoFocus
  Use modulePakoOnOff
  Use modulePakoLissajous
  Use modulePakoOtfMap
  Use modulePakoPointing
  Use modulePakoDIYlist
!!$  Use modulePakoSubscanList
  Use modulePakoTip
  Use modulePakoTrack
  Use modulePakoVlbi
  !
  Use gbl_message
  !
  Implicit None
  Save
  Private
  !
  Public :: pakoSave
  !     
!!!   
Contains
!!!
!!!
  Subroutine pakoSave (programName, line, command, error)
    !
    ! *** Arguments ***
    Include 'inc/variables/headerForCommandHandler.inc'
    !
    !**   specific local variables:    ***
    Character(len=lenCO)   ::  commandToSave, omToSave
    Character(len=lenVar)  ::  saveFile, saveFileOLD
    !
    Integer  ::  ier           
    Logical  ::  doAppend       =  GPnoneL
    Logical  ::  doCorrections  =  GPnoneL
    !
    !**   functions called:   ***
    Integer Rename
    !
    Logical errorS
    !
    !**   standard working variables   ***
    Include 'inc/variables/standardWorkingVariables.inc'
    !
    ERROR  = .False.
    errorL = .False.
    errorS = .False.
    !
    Call pako_message(seve%t,programName,                                        &
         &    " module Save, 1.2.0 2012-08-13 ---> SR: Save ")                   ! trace execution
    !
    !**   Read Parameters, Options   ***
    !
    doCorrections  =  .False.
    !
    commandToSave = GV%observingModePako
    omToSave      = GV%observingModePako
    !
    !D     Write (6,*) "      --> modulePakoSave : "
    !D     Write (6,*) "           commandToSave : ", commandToSave
    !D     Write (6,*) "                omToSave : ", omToSave
    !
    Include 'inc/parameters/parametersCommandToSave.inc'
    !
    !D     Write (6,*) "           commandToSave : ", commandToSave
    !D     Write (6,*) "                omToSave : ", omToSave
    !
!!OLD    If (                                                                     & 
!!OLD         &    commandToSave .Eq. 'SUBSCANLIST' .Or.                          & 
!!OLD         &    commandToSave .Eq. 'LIST') Then
!!OLD       commandToSave =  'SUBSCANLIST'
!!OLD    End If
    !
    saveFile = commandToSave(1:lenc(commandToSave))//".pako"
    !D     Write (6,*) "                saveFile : ", saveFile
    !
    Include 'inc/options/optionAppend.inc'
    Include 'inc/options/optionFile.inc'
    !
    !D     Write (6,*) "                saveFile : ", saveFile
    !
    !     TBD: full standard SIC file parse   *
    If (Index(saveFile,".").Lt.1) Then
       saveFile = saveFile(1:lenc(saveFile))//".pako"
    End If
    Call sic_lower(saveFile)
    saveFileOLD = saveFile(1:lenc(saveFile))//"~"
    !
    If (.Not.Error) Then
       !
       If (.Not. doAppend) Then
          ier = Rename(saveFile, saveFileOLD)
          If (ier.Eq.0) Then
             Write (messageText,*)                                                  & 
                  &           'renamed previous save file: "',                      & 
                  &           saveFile(1:lenc(saveFile)),                           & 
                  &           '"'
             Call PakoMessage(priorityI,severityI,command,messageText)
             Write (messageText,*)                                                  & 
                  &           '   to: "',                                           & 
                  &           saveFileOLD(1:lenc(saveFileOLD)),                     & 
                  &           '"'
             Call PakoMessage(priorityI,severityI,command,messageText)
          End If
       End If
       !
       iunit = ioUnit%Save
       !
       !D     Write (6,*) "         unit: iunit = ", iunit
       !
       If (doAppend) Then
          Open (unit=iUnit, file=saveFile,  recl=132,                               & 
               &           status='unknown', access='append')
       Else
          Open (unit=iUnit, file=saveFile,  recl=132,                               & 
               &           status='unknown')
       End If
       !
       !**   Call the requested Save method:   ***
       !
       If      (commandToSave.Eq.COMpako%all) Then
          !
          Call pako_message(seve%t,programName,                                  &
               &    " module Save, 1.2.2 2013-02-06  --> Save "//commandToSave)  ! trace execution
          !
          Call pakoSaveSet      (programName,LINE,commandToSave, iUnit, errorL,  &
               &                 doCorrections)                                  !
          Call saveCatalog      (programName,LINE,commandToSave, iUnit, errorL)
          Call saveReceiver     (programName,LINE,commandToSave, iUnit, errorL)
          Call saveBackend      (programName,LINE,commandToSave, iUnit, errorL)
          Call saveSwitching    (programName,LINE,commandToSave, iUnit, errorL)
          Call saveSource       (programName,LINE,commandToSave, iUnit, errorL)
          Call saveOffsets      (programName,LINE,commandToSave, iUnit, errorL)
          !
          If      (omToSave.Eq.OMpako%Cal) Then
             Call saveCalibrate    (programName,LINE,omToSave, iUnit, errorL)
!!$          Else If (omToSave.Eq.OMpako%DIY) Then
!!$             Call saveDIYlist      (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%Focus) Then
             Call saveFocus        (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%LISSAJOUS) Then
             Call saveLISSAJOUS    (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%OtfMap) Then
             Call saveOtfMap       (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%OnOff) Then
             Call saveOnOff        (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%Pointing) Then
             Call savePointing     (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%Tip) Then
             Call saveTip          (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%Track) Then
             Call saveTrack        (programName,LINE,omToSave, iUnit, errorL)
          Else If (omToSave.Eq.OMpako%VLBI) Then
             Call saveVlbi         (programName,LINE,omToSave, iUnit, errorL)
          End If
          !
       Else If (commandToSave.Eq.COMpako%Set)         Then
          Call pakoSaveSet      (programName,LINE,commandToSave, iUnit, errorL,  &
               &                 doCorrections)                                  !
       Else If (commandToSave.Eq.COMpako%catalog)    Then
          Call saveCatalog      (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%receiver)    Then
          Call saveReceiver     (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%backend)     Then
          Call saveBackend      (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%switching)   Then
          Call saveSwitching    (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%swBeam)      Then
          Call saveSwBeam       (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%swFrequency) Then
          Call saveSwFrequency  (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%swTotal)     Then
          Call saveSwTotalPower (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%swWobbler)   Then
          Call saveSwWobbler    (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%source)      Then
          Call saveSource       (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.COMpako%offsets)     Then
          Call saveOffsets      (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%Cal) Then
          Call saveCalibrate    (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%DIY) Then
          Call saveDIYlist      (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%Focus) Then
          Call saveFocus        (programName,LINE,commandToSave, iUnit, errorL)
       Else If (omToSave.Eq.OMpako%LISSAJOUS) Then
          Call saveLISSAJOUS    (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%OtfMap) Then
          Call saveOtfMap       (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%OnOff) Then
          Call saveOnOff        (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%Pointing) Then
          Call savePointing     (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%Tip) Then
          Call saveTip          (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%Track) Then
          Call saveTrack        (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.Eq.OMpako%VLBI) Then
          Call saveVlbi         (programName,LINE,commandToSave, iUnit, errorL)
       Else If (commandToSave.eq.COMPako%subscan) Then
          Call saveSubscan      (programName,LINE,commandToSave, iUnit, errorL)       
!!$!!OLD         Else If (commandToSave.eq.'OTFCIRCLE') Then
!!$!!OLD            Call saveOtfCircle(programName,LINE,commandToSave,            & 
!!$!!OLD     &            iUnit, errorL)
!!$!!OLD         Else If (commandToSave.eq.'OTFLINEAR') Then
!!$!!OLD            Call saveOtfLinear(programName,LINE,commandToSave,            &
!!$!!OLD     &            iUnit, errorL)
!!$!!OLD         Else If (commandToSave.eq.'SEGMENT') Then
!!$!!OLD            Call saveSegment(programName,LINE,commandToSave,              & 
!!$!!OLD     &            iUnit, errorL)
!!$!!OLD         Else If (commandToSave.eq.'SUBSCANLIST') Then
!!$!!OLD            Call saveSubscanList(programName,LINE,commandToSave,          & 
!!$!!OLD     &            iUnit, errorL)
!!$!!OLD         Else If (commandToSave.eq.'SUBSCANOTF') Then
!!$!!OLD            Call saveSubscanOtf(programName,LINE,commandToSave,           & 
!!$!!OLD     &            iUnit, errorL)
       Else
          errorL = .True.
          errorS = .True. 
       End If
       !
       Close(unit=iunit)
       !
    End If
    !    ERROR = ERROR .Or. errorL
    !
    If (.Not. ERROR) Then
       !     
       If (doAppend) Then
          Write (messageText,*)                                                  & 
               &           ' OK. saved /append ',                                & 
               &           commandToSave(1:lenc(commandToSave)),                 & 
               &           ' to file: "',                                        & 
               &           saveFile(1:lenc(saveFile)),                           & 
               &           '"'
       Else
          Write (messageText,*)                                                  & 
               &           ' OK. saved ',                                        & 
               &           commandToSave(1:lenc(commandToSave)),                 & 
               &           ' to file: "',                                        & 
               &           saveFile(1:lenc(saveFile)),                           & 
               &           '"'
       End If
       Call PakoMessage(priorityI,severityI,command,messageText)
       !
    Else If (errorS) Then
       !     
       messageText =                                                             & 
            &          programName(1:lenc(programName))//                        & 
            &        " doesn't know (yet) how to  save "//                       & 
            &          commandToSave(1:lenc(commandToSave))
       Call PakoMessage(priorityW,severityW,command,messageText)
       !
    Else If (ERROR) Then
       !     
       messageText =                                                             & 
            &        " could not save "//                                        & 
            &          commandToSave(1:lenc(commandToSave))//                    & 
            &        " to file "//saveFile(1:lenc(saveFile))
       Call PakoMessage(priorityE,severityE,command,messageText)
       !
    End If
    !
    Return
    !
  End Subroutine pakoSave
!!!
!!!
End Module modulePakoSave
