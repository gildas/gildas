
\newpage
\section{PaKo's Cook Book}

This section provides a basic step-by-step cook book for doing
standard observations in an interactive session with the telescope.
It only gives simple examples, details on all the options that are available
can be found in later sections.

\newcommand{\overviewRecipes}
{
It starts with some general commands and the selection of a source catalog.
Then two more subsections explain how to do 
``Spectral Line Observations with Heterodyne Receivers''
and
``Continuum Observations with Bolometers''.
}
\overviewRecipes

\newcommand{\remarkOnSic}
{
Remember that in command language scripts based on SIC:

\command{!} starts a comment;
\command{-} indicates that a command is continued on the next line;
\command{PAUSE} pauses the execution of the script, e.g., in order
                to allow the user to review the parameters set;
and the case (upper or lower) generally doesn't matter.
}
\remarkOnSic

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Set General Information}

At the \pakoPrompt\ enter, e.g.:

\begin{verbatim}
SET PROJECT   111-22                        ! project ID (project number)
SET PI       "Dr. Jane D. Doe"              ! principal investigator
SET OBSERVER "John Doe"                     !
SET OPERATOR  Pako                          !
SET TOPOLOGY  low                           ! topology for azimuth 
SHOW
DEVICE image  w                             ! open window for plots
\end{verbatim}

\newcommand{\cookSetGeneralInfo}
{
With the \command{set} commands we specify some basic information:
the project number, 
the principal investigator (PI), 
the names of the observer 
and telescope operator.

\command{SHOW} lists everything previously set with \command{SET}.

\command{SET TOPOLOGY} deserves special attention:
      The 30m antenna has azimuth limits of 60 and 460 degrees.
      Azimuth 360 degrees is due North. Therefore there is an overlap range
      approximately toward ~ Northeast, which the antenna can reach at a low
      azimuth 60 to 100 (from the South) or at a high azimuth 400 to 460 
      (from the North).

\command{SET TOPOLOGY LOW}  selects to use the azimuth range  60 to 420 degrees

\command{SET TOPOLOGY HIGH} selects to use the azimuth range 100 to 460 degrees.

\command{DEVICE} is a standard command to open a graphics window for plots.
It is used by \pako\ to provide a preview plot for some observing modes.   
}
\cookSetGeneralInfo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Specify your Source Catalog}

At the \pakoPrompt\ enter, e.g.:

\begin{verbatim}
CATALOG SOURCE demo.sou
\end{verbatim}


\newcommand{\cookCatalogSource}
{
With this command we select the ``source catalog'',
a special file, in which information about the sources is 
stored. A typical example of a source catalog looks like:
}
\cookCatalogSource


\begin{verbatim}
!
NameEq1 EQ 2000.0 02:23:16.50 61:38:57.0 LSR -45.0 FLUX 3.73 1.00
NameEq2 EQ 1950.0 02:23:16.50 61:38:57.0 LSR -45.0 FLUX 3.73 1.00
!
NameGa1 GA        03:23:16.50 63:38:57.0 LSR -33.0 FLUX 3.33 -0.33
NameGa2 GA        123.45      67.89      NUL -33.0 FLUX 3.33
!
\end{verbatim}

\newcommand{\cookCatalogSourceTwo}
{
An example of the source catalog can be found in file \filename{demo.sou}.

Note that this format is like the format that can be used directly 
with the \command{SOURCE} (see \command{HELP SOURCE}) and also like \
the source catalog format used at \PdB.
}
\cookCatalogSourceTwo
































%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Spectral Line Observations with Heterodyne Receivers}

\subsubsection{Specify your Line Catalog}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Setup of the Receivers (Frontends)}

At the \pakoPrompt\ enter, e.g.:

\begin{verbatim}
RECEIVER A100                               ! defaults for all parameters        
RECEIVER B100 12CO(1-0)                     ! using line catalog
RECEIVER A230 12CO(2-1) 230.537990 LSB      ! basic parameters explicit
\end{verbatim}

\newcommand{\cookReceiver}
{
Normally we use the \command{RECEIVER} command with 2 parameters:
a receiver name and a line name. The line name must be the name
of a line in the line catalog selected earlier. 

Alternatively the frequency and sideband can be specified directly 
as the 3rd an 4th parameter.

One \command{RECEIVER} command is need for each receiver.

After all receivers are set up, and a source has been selected,
the telescope operator or receiver engineer will tune the
receivers.  After tuning, he can tell you for each receiver:
the ambient and cold load temperatures needed for the calibration, 
as well as the image band gain ratio. 

These values describe physical parameters of the system that are not directly 
controlled, but which are important for the calibration of the data during
data processing. 

You can set these values with options of \command{RECEIVER}.
}
\cookReceiver

{\small
\begin{verbatim}
RECEIVER B100 12CO(1-0) 115.271204 LSB -    ! all parameters explicit
              /doppler fixed           -    ! fixed: no Doppler correction
              /gain 0.002              -    ! image sideband gain
              /tempLoad 88 277         -    ! cold and ambient load temperatures
              /efficiency 0.96 0.77    -    ! forward and (main) beam efficiencies
              /scale beam                   ! scale (main) beam brightness temperature
RECEIVER B230 /gain -22 db                  ! image sideband gain in [dB]
\end{verbatim}
}

\newcommand{\cookReceiverTwo}
{
Note that with the syntax \option{/GAIN -22 db} you can specify the image gain
directly in [dB].

Option \option{/EFFICIENCY} allows to specify values for the forward and 
(main) beam efficiencies.

Normally the system will provide reasonable defaults for all these calibation parameters.

Option \option{/SCALE} allows to choose the calibration scale: 
\keyword{ANTENNA} temperature or (main) \keyword{BEAM} brightness temperature.
}
\cookReceiverTwo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Setup of the Backends (Spectrometers and Continuum)}

At the \pakoPrompt\ enter, e.g.:

\small
\begin{verbatim}
!
BACKEND CONTINUUM 1                 /r A100 ! continuum backend 
BACKEND CONTINUUM 2                 /r B100
BACKEND CONTINUUM 3                 /r A230 
BACKEND CONTINUUM 4                 /r B230 
!
BACKEND 4MHz   1   4     1024    0  /r A230 ! 4MHz filters full syntax 
BACKEND 4MHz   2                    /r B230 ! 4MHz filters short syntax
!
BACKEND 1MHz   1   1      512    0  /r A230 ! 1MHz filters full syntax 
BACKEND 1MHz   2          256       /r B230 ! 1MHz filters short syntax
!
BACKEND 100kHz 1   0.1     12.8  0  /r A100 ! 100kHz filters full syntax 
BACKEND 100kHz 2           12.8     /r B100 ! 100kHz filters short syntax
!
BACKEND WILMA  1   2     1024    0  /r A100 ! WILMA setup full syntax
BACKEND WILMA  2                    /r B100 ! WILMA setup short syntax
!
BACKEND VESPA  1   0.020   40.0  2  /r A100 ! standard VESPA setup
BACKEND VESPA  2   0.020   40.0  2  /r B100 !
BACKEND VESPA  3   0.040   80.0 -2  /r A230 !
BACKEND VESPA  4   0.040   80.0 -2  /r B230 !
!
\end{verbatim}

\newcommand{\cookBackend}
{
Normally the command \command{BACKEND} has 5 parameters: backend name,
part number, resolution [MHz], bandwidth [MHz], and frequency shift [MHz].
Option \option{/RECEIVER} connects this backend part to a receiver.
This receiver must have been previously specified with \command{RECEIVER}.

For some backends the resulution, bandwidth, and/or frequency shift are 
fixed and a shorter syntax is possible, see, e.g., the example above
for the continuum backends.  See the \help\ for complete information. 

\option{/DISCONNECT} disconnects a backend (part) while retaining its parameters.

\option{/CONNECT} (re-)connects the sepcfied backend (part).

Normally the continuum backends are used for pointing and focus.
}
\cookBackend

\subsubsection{Calibration}

\subsubsection{Pointing}

For pointing, you normally select first a pointing source and 
the continuum backends as described above. 

At the \pakoPrompt\ enter, e.g.:

\small
\begin{verbatim}
POINTING      60                            ! pointing with subscan length 80
START                                       ! start
\end{verbatim}

\newcommand{\cookPointing}
{
After a pointing the data processing software displays the results
and you can enter a correction for the observed pointing offsets 
with the command

\command{SET POINTING azimuthCorrection elevationCorrection}

}
\cookPointing

\subsubsection{Focus}

\subsubsection{Observing Modes for Line Spectroscopy}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Continuum Observations with Bolometers}

\subsubsection{Setup of the Bolometer}

\subsubsection{Pointing}

\subsubsection{Focus}

\subsubsection{Tip (``Skydip'')}

\subsubsection{Calibration}

\subsubsection{Observing Modes for the Bolometer}


