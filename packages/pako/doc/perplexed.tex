\newpage
\section{paKo ---  A Guide for the Perplexed}

paKo uses the usual SIC command line interpretor. It includes the
GREG and GUI languages for plotting and, of course, adding GUIs
(coming soon to a computer near you)!

\subsection{Help}

There is help available, e.g.,
\atPakoPrompt
\begin{verbatim}
help calibrate
\end{verbatim}

The display for most commands corresponds directly to the syntax of
the command. E.g., for each observing mode, the display shows the
syntax of all options.

The most recent command, as pako interpreted it, is usually shown 
at the bottom of the display.

The keywords for commands and options try to be meaningful and, if
possible, self-explanatory.  As usual with SIC, minimum match is
supported, so you can also write compact (and cryptic) commands, e.g.,
\atPakoPrompt
\begin{verbatim}
otfm /b /c croor /no 12 /ref /st -20 20 /sy basis /to 30 /tref 22 /z
\end{verbatim}

\subsection{Switching Modes}

In the \NCS\ we distinguish the following ``Switching Modes'':

\total

\swb

\sww

\swf

Note that \swb, \sww, and \swf\ are realized by a system
with hardware synchronization signals that allow a precise and
fast switching {\em within} subscans.  

In each switching mode the system goes through several
(1, 2, or 4) phases.

These 4 switching mode are mutually exclusive, i.e., at any time
the system uses only one of them.

\total\ refers simply to 
data acquistion without any of these 3 fast Switching Modes.


\subsection{Observing Modes}

The \NCS\ supports the following ``Observing Modes'':

All Observing Modes are realized by executing a sequence of 1 or more subscans. 
In most cases, the antenna moves between or during (most of) the subscans.

The observing modes are mutually exclusive, i.e., at any time
the system executes only one of them.

Several Observing Modes can be combined with different Switching Modes, 
e.g., \command{OTFMAP} with \total, \sww\ (for bolometer), or \swf.



\subsection{Preview Plots}

Commands for some observing modes, e.g., otfmap, automatically
generate ``preview'' plots.  The range of mapping offsets for these
plots can be set with the usual GREG command limits, e.g.,
\atPakoPrompt
\begin{verbatim}
device image w
limits 500 -500 -500 500
set box match
box
\end{verbatim}

\subsection{Defaults}

Most commands have an option \command{/default} which will set all options and
parameters to meaningful default values.  You can combine option
\command{/default} with explicit values for some parameters and options, e.g.,
\atPakoPrompt
\begin{verbatim}
otfmap /def /nOtf 12
\end{verbatim}
means: default values for otfmap, but 12 OTF subscans.

\subsection{Ranges and checks}

Most parameters are checked to be within 2 ranges:
\newline
1. limits of allowed values.
   If your try to enter a value outside that range,
   you get an error message and the value is not accepted, e.g.,
\atPakoPrompt
\begin{verbatim}
 pointing /totf 4000
E-TOTF        /,  value 4000.000 outside limits 1.000000 to 3600.000
\end{verbatim}

2. standard range.
   If you enter a value outside the standard range, you get 
   a warning message, e.g.,
\atPakoPrompt
\begin{verbatim}
pointing /totf 3000 
W-TOTF        /,  value 3000.000 outside standard range 10.00000 to 600.0000
\end{verbatim}

\subsection{Independence}

The parameters of each observing mode are independent from the
parameters of other observing modes. E.g., if you first
\atPakoPrompt
\begin{verbatim}
otfmap /notf 12
\end{verbatim}
and later:
\begin{verbatim}
pointing /notf 4
\end{verbatim}
the number of OTF subscans for OTFMAP is still at 12,
as you can see with:
\begin{verbatim}
otfmap
\end{verbatim}

Some options exist for several observing modes. E.g., options
\command{/nOtf}, \command{/tOtf}, \command{/tRecord} exist for POINTING and OTFMAP.  The syntax,
parameters and meaning of these options is then (almost) the same 
for all observing modes.

\subsection{Option keywords}

Options that start with:
\newline 
\command{/t...} refer to times (durations), e.g., \command{/tOtf} = time per OTF subscan;
\newline 
\command{/n...} refer to number of something, e.g., \command{/nOtf} = number of OTF subscans;
\newline 
\command{/f...} refer to frequency of something;
\newline 
\command{/temp...} refer to temperature of something.

\subsection{Logical (YES/NO or ON/OFF) Options}

Several options are ``logicals'' which can have only one of 2 values:
TRUE = YES, shown in the display as: T, or:
FALSE = NO, shown in the display as: F.
The command syntax and logic for ALL these options is the same, e.g.,
\atPakoPrompt
\begin{verbatim}
otfmap /zigzag          ! turn option  on/true/yes : T
otfmap /zigzag yes      ! turn option  on/true/yes : T
otfmap /zigzag .true.   ! turn option  on/true/yes : T
otfmap /zigzag no       ! turn option off/false/no : F
otfmap /zigzag .false.  ! turn option off/false/no : F
\end{verbatim}
More examples of this type of logical options are:
\begin{verbatim}
/balance 
/calibrate 
/zigzag
\end{verbatim}
[Note: the default value for some logical options, e.g., \command{/zigzag}, is
T; for other logical options, e.g., \command{/calibrate}, the default is F.]

\subsection{Saving and Restoring}

For most commands you can save the parameters into a file, e.g.,
\atPakoPrompt
\begin{verbatim}
save pointing
\end{verbatim}
saves the parameters of observing mode pointing.
The format of the saved files is that of a valid script in the command language.
Therefore you can restore it later:
\atPakoPrompt
\begin{verbatim}
@ pointing
\end{verbatim}
Tip: check out the options of command save:
\atPakoPrompt
\begin{verbatim}
help save /file 
help save /append
\end{verbatim}
After using @ ... to restore a saved observing mode, e.g.,
otfmap, the graphic display may look confused. To clean it up,
\atPakoPrompt
\begin{verbatim}
clear plot
box
otfmap       ! i.e., the observing mode.
\end{verbatim}


\subsection{Receiver Setup and Calibration Parameters}

All parameters and options related to the setup of receivers
and their calibration are specified with the command \command{receiver}.
This includes ambient and cold load temperatures, image sideband ratio,
forward and main beam efficiencies, and calibration scale: antenna or 
(main) beam.

\subsection{Source and Line Catalogs}

The format of the source catalog, e.g., iram.sou, is similar to source
catalogs at PdB.  Therefore it can be generated from standard ``old''
30m catalogs using ASTRO. The example, iram.sou, was generated
from IRAM.CAT using ASTRO.

The format of the line catalog, model.lin, is as in the old control
system and at PdB.

\subsection{Starting}

To start any observation in the NCS
\atPakoPrompt
\begin{verbatim}
start
\end{verbatim}
which actually generates an XML file with a full and detailed
specification of all subscans that will be excuted by the NCS
``coordinator'' software.  This is done by the ``scanAnalyzer'', which is
an integral part of the pako software.  
If you are looking
for an adventure, you are encouraged to explore these XML files,
e.g., using a recent version of Mozilla, the XML editor oxygen,
or emacs.

