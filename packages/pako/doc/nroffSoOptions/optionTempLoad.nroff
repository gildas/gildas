.nf
/TEMPLOAD tempColdLoad tempAmbientLoad
/TEMPLOAD L[OOKUP]     L[OOKUP]
/TEMPLOAD *            ...

Set effective temperatures for the calibration loads at cold and ambient
temperature.

Real      :: tempColdLoad      !
Real      :: tempAmbientLoad   !

NEW LOGIC FOR SINGLE-PIXEL SIS RECEIVERS (FROM SUMMER 2007, paKo v1.0.7)

If a numerical value is entered for tempColdLoad or tempAmbientLoad,
that value is used for calibration calculations.

Instead of specifying a value one may enter the string L(OOKUP) for
tempColdLoad and/or tempAmbientLoad.  This is shown in the pakoDisplay
by the letter "L" instead of a number.

In this case, during the execution of the observations, the NCS will
use the "best-known" values for the corresponding load temperature(s).
For tempColdLoad this is based on a lookup table, for tempAmbientLoad
it is derived from a measurement of the physical temperature.

The lookup table for tempColdLoad normally is valid for the standard
calibration system with a closed-cycle cooling system.

A * can be substituted for tempColdLoad, which means to leave the
value for tempColdLoad unchanged from the previous RECEIVER command.

IF THE OBSERVERS HAVE ANY DOUBT ABOUT THIS, THEY SHOULD ASK A RECEIVER
ENGINEER FOR THE CORRECT VALUE AND ENTER IT EXPLICITLY.


NOTE: FOR HERA The new logic is not yet available. However, for HERA
"best-known" values are always used during the execution of the
observations. This is the same logic as in pevious versions of paKo/NCS.

.fi

