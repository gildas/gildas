\newpage
\section{paKo ---  Guide of the Perplexed}

\label{sec-perplexed}

%%%%%%%%%%%
\subsection{\pako\ and SIC}

\pako\ uses the usual SIC command line interpretor,
and can be run in any X-windows terminal,
see Figure \ref{fig-pakoterminal}.

\begin{figure}
  \includegraphics[angle=0,width=16cm]{pakoTerminal.pdf}
\caption[pakoTerminal]
{
Screen shot of \pako\ runing in a terminal window.
}
\label{fig-pakoterminal}
\end{figure}

It includes the
GREG and GUI languages for plotting and, of course, adding GUI widgets.

%%%%%%%%%%%
\subsection{\pako\ and Linux}

\pako\ and the NCS run on Linux. Any files that are prepared off-line,
e.g., command scripts and source or line catalogs, should follow Linux 
standards.  (Files prepared on other operating systems may contain 
non-compliant control characters.)

%%%%%%%%%%%
\subsection{Running \pako\ offline and several instances of \pako}

\pako\ can run independently of the NCS.  This is useful, e.g., to prepare
command scripts and source or line catalogs.

Several instances of pako should not be run in the same working directory
(of the same project account.)  Also, at most one instance of pako should try
to send observations to the observation queue; other instances should
\command{SET DOSUBMIT NO}, see below.

%%%%%%%%%%%
\subsection{Help}

There is help available, e.g.,
\atPakoPrompt
\begin{verbatim}
HELP CALIBRATE
\end{verbatim}

The display for most commands corresponds directly to the syntax of
the command. E.g., for each observing mode, the display shows the
syntax of all options.

The most recent command, as pako interpreted it, is usually shown 
at the bottom of the display window.

The keywords for commands and options try to be meaningful and, if
possible, self-explanatory.  As usual with SIC, minimum match is
supported, so you can also write compact (and cryptic) commands, e.g.,
\atPakoPrompt
\begin{verbatim}
OTF /B /C CROOR /NO 12 /REF /ST -20 20 /SY PR /TO 30 /TR 22 /Z
\end{verbatim}


%%%%%%%%%%%
\subsection{SET LEVEL for Errors, Warnings, and Infos}

\pako\ can be very ``chatty'' and display many 
``messages'' in the command line window.  They are marked with
``I-'' for ``info'', ``W-'' for ``warning'', or ``E-'' for
``error''. ``E-'' ``error'' is reserved for true errors, 
something not accepted by \pako. 

You can control the number of messages you see with
with \command{SET LEVEL}, e.g.,

\begin{verbatim}
SET LEVEL 1 1 
\end{verbatim}
will enforce that you get all messages.

\begin{verbatim}
SET LEVEL 3 3 
\end{verbatim}
will supress most ``I-'' infos.

\begin{verbatim}
SET LEVEL 5 5 
\end{verbatim}
will supress most ``I-'' infos and ``W-'' warnings.

At this time ({\theRevisionDate}) \pako\ still displays some debug messages,
which are not flagged ``I-'', ``W-'', or ``E-''.  They will be eliminated
as soon as possible.


%%%%%%%%%%%
\subsection{Saving and Restoring}

For most commands you can save the parameters into a file, e.g.,
\atPakoPrompt
\begin{verbatim}
SAVE POINTING
\end{verbatim}
This saves the parameters of observing mode \pointing.
The format of the saved files is that of a valid script in the command language.
Therefore you can restore it later:
\atPakoPrompt
\begin{verbatim}
@ POINTING
\end{verbatim}

In a similar way, you can save the parameters of the source, 
receiver and backend setup, and of the switching mode.

\begin{verbatim}
SAVE ALL
\end{verbatim}
saves (nearly) all current parameters, as
well as the current switching and observing modes.
It saves the pointing and focus corrections
only if used in the form:
\begin{verbatim}
SAVE ALL C[ORRECTIONS]
\end{verbatim}
Normally \command{SAVE ALL} is meant to generate a paKo script
that can be used to re-produce the setup at a later time, when one
probably wants to use different pointing and focus corrections. 
On the other hand the idiomatic usage:
\begin{verbatim}
SAVE ALL C /FILE LAST 
\end{verbatim}
allows to save ``really everything'' in order to recover it with @ LAST.

The parameters of ``unused'' (unselected) hardware, switching
modes, and observing modes are never saved.

Tip: check out the options of command save:
\atPakoPrompt
\begin{verbatim}
HELP SAVE /FILE 
HELP SAVE /APPEND
\end{verbatim}
After using @ ... to restore a saved observing mode, e.g.,
otfmap, the graphic display may look confused. To clean it up,
\atPakoPrompt
\begin{verbatim}
CLEAR PLOT
BOX
OTFMAP       ! i.e., the observing mode.
\end{verbatim}


%%%%%%%%%%%
\subsection{Observation Queues and Starting}

In the NCS, all observations are handled through an observation queue.
So far this is rather simple, first-in-first-out.

The operator has to set the ``current observation queue''
to be that of the project account.  (Submission from other projects
will not be accepted by the NCS).

\atPakoPrompt
\begin{verbatim}
SET doSubmit yes
\end{verbatim}

to activate submission of observing commands to the
NCS observing queue.  \command{SET doSubmit no} is useful for
debugging, so that scripts including \command{START} can excute
without actually trying to submit observations.

To start any observation in the NCS
\atPakoPrompt
\begin{verbatim}
START
\end{verbatim}
which actually generates an XML file with a full and detailed
specification of all subscans that will be excuted by the NCS
``coordinator'' software.  
This is done by the ``scanAnalyzer'', which is
an integral part of the pako software.  
If you are looking
for an adventure, you are encouraged to explore these XML files,
e.g., using a recent version of Mozilla, the XML editor oxygen,
or emacs.

Several instances of pako should not be run in the same working directory
(of the same project account.)  Also, at most one instance of pako should try
to send observations to the observation queue.

%%%%%%%%%%%
\subsection{Source and Line Catalogs}

The format of the source catalog, e. g., iram.sou, is similar to
source catalogs at PdB.  A source catalog for the NCS can be generated
from standard ``old'' 30m catalogs (*.cat) using ASTRO. The example
source catalog, demo.sou, was generated from a historic version of
IRAM.CAT using ASTRO.

The format of the line catalog, e. g., model.lin, is as in the old control
system and at PdB.

\input{inc/pakoIncNames.tex}


%%%%%%%%%%%
\subsection{Switching Modes}

\input{inc/pakoIncSwitching.tex}



%%%%%%%%%%%
\subsection{Observing Modes}

\input{inc/pakoIncOM.tex}



%%%%%%%%%%%
\subsection{Combinations of Switching and Observing Modes}

The Switching modes and Observing modes are not fundamentally different from
what they were in the old CS.  However, in the NCS we try to be more explicit
about this distinction in order to: (i) avoid having several commands that
set up, e.g., parameters of the Wobbler, (ii) to support more
combinations of Observing Modes and Switching Modes in the future.



%%%%%%%%%%%
\subsection{Coordinate Systems, Map Projections, and Position Offsets}

For a more detailed explanation, see Section~\ref{sec-explainSysProOff}.

\input{inc/pakoIncSysProOff.tex}

\input{inc/pakoIncRemarkOnOffsets.tex}

\notes.
If you are unsure about any of this, read the additional information
in Section~\ref{sec-explainSysProOff}, or ask an astronomer whi is
familiar with the NCS.  For the time being, it is recommended not to
try ``fancy'' combinations of offsets, which probably have not yet
been fully tested and debugged.



%%%%%%%%%%%
\subsection{Receiver Setup and Calibration Parameters}

All parameters and options related to the setup of receivers
and their calibration are specified with the command \command{RECEIVER}.
This includes ambient and cold load temperatures, image sideband ratio,
forward and main beam efficiencies, calibration scale antenna or 
(main) beam, and the HERA derotator.


%%%%%%%%%%%
\subsection{Backends}

Backend setup for all backends is done with the command \command{BACKEND}.


%%%%%%%%%%%
\subsection{Continuous Data Acquisition and Data Streams}

In the \NCS, normally the data acquisition is continuous: 
fast independent data streams are generated by the backends as well as other 
subsystems, e.g., by the atenna mount drive to describe the antenna's
movements.  The data processing software synchronizes the data from
different streams based on time stamps in the data. 
Most data streams keep continuously running even between subscans.


%%%%%%%%%%%
\subsection{Display of Parameters}

Most parameters set by the observer are displayed by a separate program,
\command{pakoDisplay}, in another window, see Figure \ref{fig-pakodisplay}.

Several instances of this program can run at the same time, including
on different screens, ``desktops'', and Linux machines.

\begin{figure}
  \includegraphics[angle=0,width=16cm]{pakoDisplay.pdf}
\caption[pakodisplay]
{
Screen shot of the \pako\ Display.
}
\label{fig-pakodisplay}
\end{figure}


%%%%%%%%%%%
\subsection{Preview Plots}

Commands for some observing modes, e.g., otfmap, automatically
generate ``preview'' plots, see Figure \ref{fig-pakoplot}.

The range of mapping offsets for these
plots can be set with the usual GREG command limits, e.g.,
\atPakoPrompt
\begin{verbatim}
DEVICE IMAGE W
LIMITS 500 -500 -500 500
SET BOX MATCH
BOX
\end{verbatim}

Similarly, you can use other commands from GREG to change
the color of the plotting ``pens'' or background of the window.

\begin{figure}
  \includegraphics[angle=0,width=16cm]{pakoPlot.pdf}
\caption[pakoPlot]
{
Screen shot of the \pako\ preview plot.
}
\label{fig-pakoplot}
\end{figure}

%% \begin{figure}
%%   \includegraphics[angle=0,width=16cm]{otfmap.pdf}
%% \caption[otfmap]
%% {
%% otfmap
%% }
%% \label{fig-otfap}
%% \end{figure}


%%%%%%%%%%%
\subsection{Defaults}

Most commands have an option \command{/DEFAULT} which will set all options and
parameters to meaningful default values.  You can combine this option
with explicit values for some parameters and options, e.g.,
\atPakoPrompt
\begin{verbatim}
OTFMAP /DEF /NOTF 12
\end{verbatim}
means: default values for \otfmap, but 12 OTF subscans.


%%%%%%%%%%%
\subsection{Ranges and checks}

Most parameters are checked to be within 2 ranges:

\noindent
1. limits of allowed values.
   If you try to enter a value outside that range,
   you get an error message and the value is not accepted, e.g.,
\atPakoPrompt
\begin{verbatim}
POINTING /TOTF 4000
E-TOTF        /,  value 4000.000 outside limits 1.000000 to 3600.000
\end{verbatim}

\noindent
2. standard range.
   If you enter a value outside the standard range, you get 
   a warning message, but the value is accepted, e.g.,
\atPakoPrompt
\begin{verbatim}
POINTING /TOTF 3000 
W-TOTF        /,  value 3000.000 outside standard range 10.00000 to 600.0000
\end{verbatim}


%%%%%%%%%%%
\subsection{Independence of Command Parameters}

The parameters of each observing mode are independent from the
parameters of other observing modes.  The same is true fop the 
different switching modes.
For example, if you first
\atPakoPrompt
\begin{verbatim}
OTFMAP /NOTF 12
\end{verbatim}
and later:
\begin{verbatim}
POINTING /NOTF 4
\end{verbatim}
the number of OTF subscans for OTFMAP is still at 12,
as you can see with:
\begin{verbatim}
OTFMAP
\end{verbatim}

Some options exist for several observing modes. E.g., options
\command{/NOTF}, and \command{/TOTF} exist for 
POINTING and OTFMAP.  The syntax, parameters and meaning of these
options is then (almost) the same for all observing modes.


%%%%%%%%%%%
\subsection{Option keywords}

Options that start with:
\newline 
\command{/t...} refer to times (durations), e.g., \command{/tOtf} = time per OTF subscan;
\newline 
\command{/n...} refer to number of something, e.g., \command{/nOtf} = number of OTF subscans;
\newline 
\command{/f...} refer to frequency of something;
\newline 
\command{/temp...} refer to temperature of something.


%%%%%%%%%%%
\subsection{Logical (YES/NO or ON/OFF) Options}

Several options of commands are ``logicals'' which can have only one of 2 values:
TRUE = YES, shown in the display as: T, or:
FALSE = NO, shown in the display as: F.
The command syntax and logic for ALL these options is the same, e.g.,
\atPakoPrompt
\begin{verbatim}
OTFMAP /ZIGZAG          ! TURN OPTION  ON/TRUE/YES : T
OTFMAP /ZIGZAG YES      ! TURN OPTION  ON/TRUE/YES : T
OTFMAP /ZIGZAG .TRUE.   ! TURN OPTION  ON/TRUE/YES : T
OTFMAP /ZIGZAG NO       ! TURN OPTION OFF/FALSE/NO : F
OTFMAP /ZIGZAG .FALSE.  ! TURN OPTION OFF/FALSE/NO : F
\end{verbatim}

[Note: the default value for some logical options, e.g., \command{/ZIGZAG}, is
T; for other logical options the default is F.]


%%%%%%%%%%%
\subsection{Example Scripts and Catalogs}

Examples of \pako\ scripts, source and line catalogs are
available on the WWW and in each project account.
