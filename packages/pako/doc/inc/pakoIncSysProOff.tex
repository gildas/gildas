
The \NCS\ will support a variety of astronomical coordinate systems
and projections, as well as ``descriptive'' coordinate systems defined
by the user.
Up to now, \theRevisionDate, only 
equatorial coordinates, J2000.0, are well tested and available for use.

\paragraph{Map Projections and Offsets.}

In general, a ``map projection'' describes the relation between 
2 spherical coordinates, longitude $l$ and latitude $b$, 
\footnote{
In particular for equatorial coordinates, 
$l$ corresponds to Right Ascension and $b$ to Declination. 
}
on the
celestial sphere, and 2 Cartesian coordinates $x$ and $y$, which in
radio astronomy and the NCS we often call ``position offsets''.

Up to now, \theRevisionDate, only the {\bf ``radio''} projection 
is supported, for which:
$$x = (l-l_{source})*\cos(b)$$
$$y = b-b_{source}$$
where $l_{source}$ and $b_{source}$ are the source coordinates
specified with \command{SOURCE}.
\footnote{
For the equations all angles are assumed to be in radian. 
}
%%% \footnote{
%%% Due to a typographical error, versions before v 1.0.7 of this document
%%% had $/\cos(b)$ instead of $*\cos(b)$ in the formula for $x$.
%%% }
Note that this is the same system of offsets as in ``OBS'' 
of the old control system.


If we want to observe several positions on the sky at or near the 
source position as specified with \command{SOURCE}, 
we often do this by requesting position offsets
in the map projection.  Also, the resulting data, \eg, images,
are usually stored and displayed as a function of $x$ and $y$.

For most observations, parameters and options of the observing mode
are sufficient to specify the position offsets:
\begin{itemize}
\item for \track\ and \vlbi\ $x$ and $y$ are fixed during the complete scan;
\item for \onoff\ $x$ and $y$ change from subscan to subscan;
\item for \otf\ $x$ and $y$ change continuously or ``on-the-fly'' (OTF) 
      during the OTF subscans. 
\end{itemize}

The \pako\ commands for most Observing Modes expect
fixed offsets (or start- and end-offsets for \otf) as parameters.
These can be either
in the radio projection, specified with the option:                                \newline
\option{ /SYSTEM projection}                                                       \newline
or in the true angle horizon system (see below), specified with the option:        \newline
\option{ /SYSTEM trueHorizon} 

\notes. For POINTING, the OTF offsets are always in system trueHorizon,
and are specified implicitly though the angular length of the subscans.


\paragraph{Global Offsets.}
On the other hand, the command \command{OFFSETS} 
can be used to specify additional position offsets in other systems. 
These globally defined offsets stay fixed during a complete scan. 
{\em They are only needed in special cases, \eg, the Nasmyth offsets
or for ONOFF with wobbler switching}, see below.

At this time (\theRevisionDate),
the command \command{OFFSETS} supports offsets in the following 3 systems:

\begin{description}
\item[projection]     Offsets in 
                      the ``radio'' projection (see above).

\item[trueHorizon]    ``true angle horizon'' offsets in Azimuth and Elevation:
$$\Delta a = (a-a_{source})*\cos(e)$$
$$\Delta e = e-e_{source}$$
where $a$ and $e$ are the Azimuth and Elevation of the telescope; 
$a_{source}$ and $e_{source}$ are the Azimuth and Elevation of the source,
calculated from $l$ and $b$ (and the time and other parameters).

\item[Nasmyth]        offsets in the Nasmyth (receiver cabin) system.
                      The purpose of Nasmyth offsets is exclusively to
                      re-position the telescope so that an off-center 
                      element of a multibeam receiver looks at the
                      position where otherwise the center pixel would look.
                      E.~g.,
                      \command{OFFSETS -33 44 /SYSTEM Nasmyth}
                      adds offsets -33 and 44 in the Nasmyth
                      system (for all observing modes!)
\end{description}



\paragraph{Example 1}

Observe a single position with offsets 10 and 20
  in system radio projection; typically used with \swf:

\begin{verbatim}
TRACK 10 20 /SYSTEM projection
\end{verbatim}

\paragraph{Example 2}

Observe \onoff\ (``position switching'' with \total) 
with ON position at 30 40 and off-source reference
  at $-600$ $-700$, both in system radio projection:

\begin{verbatim}
ONOFF 30 40 /REFERENCE -600 -700 projection /SYSTEM projection
\end{verbatim}

\paragraph{Example 3}

Pointing with subscans of length 120:

\begin{verbatim}
POINTING 120
\end{verbatim}

\notes. For POINTING, the OTF offsets are always in system trueHorizon,
and are specified implicitly though the angular length of the subscans.

\paragraph{Example 4}

ONOFF observations with \sww\ are a special case, 
because the offsets for
the subscans must
be in system trueHorizon and their values must be selected 
according to the offsets of the \sww! E.~g.,

\begin{verbatim}
SWWOBBLER -33 +33
ONOFF 33 0 /REFERENCE -33 0 trueHorizon /SYSTEM trueHorizon
\end{verbatim}

This can also be achieved simply by saying

\begin{verbatim}
SWWOBBLER -33
ONOFF
\end{verbatim}

\pako\ ``knows'' the special requirements for onoff wobbler switching,
and will set the offset parameters for \onoff\ accordingly, 
if \command{SWWOBBLER} has been previously selected.
\footnote{
For special purposes, it is possible to overrule this with 
\option{/swWobbler no}, \eg,
\command{ONOFF 44 /swWobbler no}.
}

In this special case, in order to map the source, the observer may
add offsets on the source $l$ and $b$ using the command OFFSETS
with the system ``projection'', \eg:
\begin{verbatim}
SWWOBBLER -33
OFFSETS 110 120 /SYSTEM projection
ONOFF
\end{verbatim}



