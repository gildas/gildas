###########################################################################
#
# Makefile system for GILDAS softwares (2003-2012).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################

include $(gagadmdir)/Makefile.def

###########################################################################

subdirs = utilities legacy kernel packages contrib cookbooks admin

INTEG_CLEAN_LOCAL_COMMAND = $(RM) $(gagintdir)

###########################################################################

include $(gagadmdir)/Makefile.struct

###########################################################################
