!include LogicLib.nsh
!include EnvVarUpdate.nsh

/**********************
* FileAssociation.nsh *
**********************/

!include Util.nsh

!macro RegisterExtensionCall _EXECUTABLE _EXTENSION _DESCRIPTION
  Push `${_DESCRIPTION}`
  Push `${_EXTENSION}`
  Push `${_EXECUTABLE}`
  ${CallArtificialFunction} RegisterExtension_
!macroend

!macro UnRegisterExtensionCall _EXTENSION _DESCRIPTION
  Push `${_EXTENSION}`
  Push `${_DESCRIPTION}`
  ${CallArtificialFunction} UnRegisterExtension_
!macroend

!define RegisterExtension `!insertmacro RegisterExtensionCall`
!define un.RegisterExtension `!insertmacro RegisterExtensionCall`

!macro RegisterExtension_

  Exch $R2 ;exe
  Exch
  Exch $R1 ;ext
  Exch
  Exch 2
  Exch $R0 ;desc
  Exch 2
  Push $0
  Push $1

  ReadRegStr $1 HKCR $R1 ""  ; read current file association
  StrCmp "$1" "" NoBackup  ; is it empty
  StrCmp "$1" "$R0" NoBackup  ; is it our own
    WriteRegStr HKCR $R1 "backup_val" "$1"  ; backup current value
NoBackup:
  WriteRegStr HKCR $R1 "" "$R0"  ; set our file association

  ReadRegStr $0 HKCR $R0 ""
  StrCmp $0 "" 0 Skip
    WriteRegStr HKCR "$R0" "" "$R0"
    WriteRegStr HKCR "$R0\shell" "" "open"
    WriteRegStr HKCR "$R0\DefaultIcon" "" "$R2,0"
Skip:
  WriteRegStr HKCR "$R0\shell\open\command" "" '"$R2" say "%1"'
  WriteRegStr HKCR "$R0\shell\run" "" "Run $R0"
  WriteRegStr HKCR "$R0\shell\run\command" "" '"$R2" @ "%1"'

  Pop $1
  Pop $0
  Pop $R2
  Pop $R1
  Pop $R0

!macroend

!define UnRegisterExtension `!insertmacro UnRegisterExtensionCall`
!define un.UnRegisterExtension `!insertmacro UnRegisterExtensionCall`

!macro UnRegisterExtension_

  Exch $R1 ;desc
  Exch
  Exch $R0 ;ext
  Exch
  Push $0
  Push $1

  ReadRegStr $1 HKCR $R0 ""
  StrCmp $1 $R1 0 NoOwn ; only do this if we own it
  ReadRegStr $1 HKCR $R0 "backup_val"
  StrCmp $1 "" 0 Restore ; if backup="" then delete the whole key
  DeleteRegKey HKCR $R0
  DeleteRegKey HKCR $R1 ;Delete key with association name settings
  Goto NoOwn

Restore:
  WriteRegStr HKCR $R0 "" $1
  DeleteRegValue HKCR $R0 "backup_val"

NoOwn:

  Pop $1
  Pop $0
  Pop $R1
  Pop $R0

!macroend

/*****************************
* End of FileAssociation.nsh *
*****************************/

!define env_hklm 'HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"'
!define env_hkcu 'HKCU "Environment"'

;!define GTK_INSTALLER_EXE "gtk2-runtime-2.16.6-2010-05-12-ash.exe"
!define GILDAS_GROUP "$SMPROGRAMS\Gildas"
!define GAG_EXEC_SYSTEM "x86_64-mingw-gfortran"
!define MINGW_BIN "C:/cygwin64/usr/x86_64-w64-mingw32/sys-root/mingw/bin/"

name Gildas
OutFile "gildas-setup.exe"
VIProductVersion 1.0.0.0
VIAddVersionKey ProductName "Gildas"
;VIAddVersionKey Comments "A build of the PortableApps.com Launcher for ${NamePortable}, allowing it to be run from a removable drive.  For additional details, visit PortableApps.com"
VIAddVersionKey CompanyName IRAM
VIAddVersionKey LegalCopyright IRAM
VIAddVersionKey FileDescription "Gildas"
VIAddVersionKey FileVersion 1.0.0.0
;VIAddVersionKey ProductVersion apr16
;VIAddVersionKey InternalName "PortableApps.com Launcher"
;VIAddVersionKey LegalTrademarks "PortableApps.com is a Trademark of Rare Ideas, LLC."
VIAddVersionKey OriginalFilename "gildas-setup.exe"
InstallDir "c:\gildas"
Page directory
Page instfiles

SectionGroup "Installation"

Function setEnvVar
   ; include for some of the windows messages defines
   !include "winmessages.nsh"
   ; HKLM (all users) vs HKCU (current user) defines
   ; set variable
   WriteRegExpandStr ${env_hkcu} $0 $1
   ; make sure windows knows about the change
   SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} 0 "STR:Environment" /TIMEOUT=5000
FunctionEnd

Function un.setEnvVar
   DeleteRegValue ${env_hkcu} $0
FunctionEnd

# binaries
Section "test"
    #System::Call "kernel32::SetCurrentDirectory(t ${GAG_EXEC_SYSTEM})"
    #System::Call "kernel32::GetCurrentDirectory(i ${NSIS_MAX_STRLEN}, t .r0)"
    #MessageBox MB_OK "$0"
SectionEnd

Section "Environment"
  StrCpy $0 "GAG_PATH"
  StrCpy $1 "$INSTDIR"
  Call setEnvVar
  StrCpy $0 "GAG_GAG"
  StrCpy $1 "$USERPROFILE\.gag"
  Call setEnvVar
  ${EnvVarUpdate} $0 "PATH" "P" "HKCU" "$INSTDIR\bin"
SectionEnd

Section "binary files"
  SetOutPath "$INSTDIR"
  ;File "setup\${GTK_INSTALLER_EXE}"
  ;ExecWait '"${GTK_INSTALLER_EXE}" /sideeffects=no /S /D=$INSTDIR'
  ;Delete "$INSTDIR\${GTK_INSTALLER_EXE}"

  File /r /x include /x lib /x python "${GAG_EXEC_SYSTEM}\*.*"
  SetOutPath "$INSTDIR\bin"
  # put dll in bin
  File "${GAG_EXEC_SYSTEM}\lib\*.dll"
  # put extra dll in bin
  File "${MINGW_BIN}\*.dll"

  SetOutPath "$INSTDIR\demo"
  File "demo\*.*"
  SetOutPath "$INSTDIR"
  #File "etc\*.*"
  File etc\gag.dico.gbl etc\gag.dico.lcl
  SetOutPath "$INSTDIR\pro"
  File "pro\*.*"
  SetOutPath "$INSTDIR"
  File /r "doc"
SectionEnd

Section "Short cuts"
  CreateDirectory "${GILDAS_GROUP}"
  CreateShortCut "${GILDAS_GROUP}\greg.lnk" "$INSTDIR\bin\greg.exe" \
  "" "$INSTDIR\bin\win-greg.dll" 0
  CreateShortCut "${GILDAS_GROUP}\astro.lnk" "$INSTDIR\bin\astro.exe" \
  "" "$INSTDIR\bin\win-astro.dll" 0
  CreateShortCut "${GILDAS_GROUP}\class.lnk" "$INSTDIR\bin\class.exe" \
  "" "$INSTDIR\bin\win-class.dll" 0
  CreateShortCut "${GILDAS_GROUP}\clic.lnk" "$INSTDIR\bin\clic.exe" \
  "" "$INSTDIR\bin\win-clic.dll" 0
  CreateShortCut "${GILDAS_GROUP}\mapping.lnk" "$INSTDIR\bin\mapping.exe" \
  "" "$INSTDIR\bin\win-mapping.dll" 0
SectionEnd

Section "File association"
  ${registerExtension} "$INSTDIR\bin\greg.exe" ".greg" "Greg File"
  ${registerExtension} "$INSTDIR\bin\astro.exe" ".astro" "Astro File"
  ${registerExtension} "$INSTDIR\bin\class.exe" ".class" "Class File"
  ${registerExtension} "$INSTDIR\bin\clic.exe" ".clic" "Clic File"
  ${registerExtension} "$INSTDIR\bin\mapping.exe" ".map" "Mapping File"
SectionEnd

# Uninstall
Section "bin"
  WriteUninstaller "$INSTDIR\gildas_uninst.exe"
SectionEnd

SectionGroupEnd

Section "Uninstall"
  #SetAutoClose true
  Delete "$INSTDIR\gildas_uninst.exe"
  RMDir /r $INSTDIR\bin
  RMDir /r $INSTDIR\data
  RMDir /r $INSTDIR\demo
  RMDir /r $INSTDIR\doc
  RMDir /r $INSTDIR\tasks
  RMDir /r $INSTDIR\pro
  RMDir /r "${GILDAS_GROUP}"

  ExecWait "$INSTDIR\gtk2_runtime_uninst.exe /remove_config=yes /sideeffects=no /S"

  ;Delete "$INSTDIR\${GTK_INSTALLER_EXE}"

  #Messagebox MB_OK|MB_ICONINFORMATION \
    #"Uninstallation"
SectionEnd

Section "un.Environment"
  StrCpy $0 "GAG_PATH"
  Call un.setEnvVar
  StrCpy $0 "GAG_GAG"
  Call un.setEnvVar
  ${un.EnvVarUpdate} $0 "PATH" "R" "HKCU" "$INSTDIR\bin"
SectionEnd

Section "un.File association"
  ${unregisterExtension} ".greg" "Greg File"
  ${unregisterExtension} ".astro" "Astro File"
  ${unregisterExtension} ".class" "Class File"
  ${unregisterExtension} ".clic" "Clic File"
  ${unregisterExtension} ".map" "Mapping File"
SectionEnd

