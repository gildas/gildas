###########################################################################
#
# New makefile system for GILDAS softwares (J.Pety 2003-2024).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################
#
# Rules to make procedures
#
###########################################################################

ifneq ($(PRO_SUBDIR),)
  prodir := $(prodir)/$(PRO_SUBDIR)
endif

ifneq ($(PRO_FILES),)
  TARGETS = $(patsubst %,$(prodir)/%, $(PRO_FILES))
  EXPANDPRO = $(PRO_FILES)
else
  TARGETS = $(patsubst %,$(prodir)/%, $(wildcard *.*))
  EXPANDPRO = $(wildcard *.$(EXTENSION))
endif

DIRTY = $(TARGETS)

###########################################################################
# Common targets and rules

include $(gagadmdir)/Makefile.build

###########################################################################
# Specific targets and rules

expand: 
	for x in $(EXPANDPRO); do (echo "sic expand $$x $$x" | $(PROGRAM)  ) || exit 1; done

###########################################################################
