###########################################################################
#
# This file defines useful bash functions, environment variables and
# shortcuts for GILDAS/DEVELOPERS interaction.
#
###########################################################################
#
# This function loads your local GILDAS environment.
# It is compatible with the gagiram script.
# Type "gag -h" for more details.
# Make "dev" the default version (i.e. when gag is called without arguments)
#
function gag() {
     export GAG_COMM_TRACE=$HOME/.gag/logs/gag.log
     export gaghome=$HOME/gildas
     source $gaghome/gildas-admin/source-gag.sh -d dev "$@"
}
#
# This function enables easy browsing of the GILDAS source or executable 
# tree. $gagsrcdir and $gagexedir must be defined. You can use 
# "gag -ms" or "gag -me" to define them without changing the PATHs. 
# Type "go -h" for more details.
#
function go() {
     if [ -z "$gagadmdir" ]; then
         echo 'Please select first a GILDAS version using gag' 1>&2
     else
         source $gagadmdir/go.sh "$@"
     fi
}
#
export -f gag go
#
# Shortcuts
alias gags="gag -s"
alias gage="gag -e"
alias gos="go -s"
alias goi="go -i"
alias goe="go -e"
#
# The next command automatically loads the environment variables
# needed for the default GILDAS version (in this case your local version).
# Uncomment next line if this is really what you want. In any case, this
# is not recommended.
#
#gag > /dev/null 2>&1
#
###########################################################################
