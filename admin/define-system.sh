###########################################################################
#
# Shell verification. Try to be robust => very simple.
#
GAG_SHELL_TEST="shell test" || echo "define-system.sh error: You must be under a sh-compatible shell!"
GAG_SHELL_TEST="shell test" || exit 1
unset GAG_SHELL_TEST
#
###########################################################################
#
# Main function definition
#
gagdefsys() {
    #
    # Variable definitions
    #
    DEFSYS_AUTHOR='J. Pety <pety@iram.fr>'
    DEFSYS_PROJECT='GILDAS  <http://www.iram.fr/IRAMFR/GILDAS>'
    DEFSYS_PROGNAME='gagdefsys'
    #
    # Function definitions
    #
    # Portable functions for version strings comparison, based on sort -V.
    verle() {
        # Return 0 if first argument is "lower or equal" the second argument
        # NB: the line return in echo below is more portable than echo -e "\n"
        [  "$1" = "`echo "$1
$2" | sort -V | head -n1`" ]
    }
    verlt() {
        # Return 0 if first argument is "lower than" second argument
        [ "$1" = "$2" ] && return 1 || verle $1 $2
    }
    #
    defsys_usage() {
        cat <<EOF 1>&2

Define environment variables:
   GAG_COMP_SYSTEM=MACHINE-OS_KIND-COMPILER,
   GAG_EXEC_SYSTEM=MACHINE-OS_VERS-COMPILER(-CONFIG).
A default compiler is provided for each combination of MACHINE and OS. This
default compiler may be overriden using the -c option.

usage: source $DEFSYS_PROGNAME [options]

options:
  -h           Show this help page
  -v           Show version information
  -b ccompiler Replace default C compiler by this one
  -c fcompiler Replace default Fortran compiler by this one
  -d cxxcompil Replace default C++ compiler by this one
  -o config    Add config to the config list
  -n config    Remove config from the config list
  -s           Define compiler options, in order to compile Gildas later on
  -t target    Define the system target compiled under current
               build environment

EOF
        defsys_clean
    }
    #
    defsys_showversion() {
        echo "$DEFSYS_PROGNAME, by $DEFSYS_AUTHOR"
        echo "Project: $DEFSYS_PROJECT"
        defsys_clean
    }
    #
    defsys_message() {
        echo "$DEFSYS_PROGNAME: $1"
    }
    #
    defsys_error() {
        echo
        echo "$DEFSYS_PROGNAME error: $1"
        echo
        defsys_clean
    }
    #
    defsys_clean() {
        unset defsys_usage defsys_showversion defsys_message
        unset defsys_error defsys_clean
        unset IFORT_VERSION
        unset CPP_VERSION
        unset GFORTRAN_VERSION GFORTRAN_MINI
        unset DEFSYS_AUTHOR DEFSYS_PROGNAME DEFSYS_PROJECT
        unset DEFSYS_CCOMPILER DEFSYS_FCOMPILER DEFSYS_CXXCOMPILER
        unset DEFAULT_CCOMPILER DEFAULT_FCOMPILER DEFAULT_CXXCOMPILER
        unset DEFSYS_CONFIG DEFAULT_CONFIG
    }
    #
    defsys_generic() {
        # Echo a string suited for $GAG_ENV_NAME by parsing /etc/os-release
        . /etc/os-release
        # DO NOT strip off minor numbers after the coma, if any
        # VERSION_ID=`echo $VERSION_ID | sed "s/\..*//"`
        echo ${ID}${VERSION_ID}  # e.g. fedora19, redhat6.4, ubuntu12.04
    }
    #
    defsys_generic_full() {
        # Echo a string suited for $GAG_ENV_VERS by parsing /etc/os-release
        . /etc/os-release
        # Some OS provide more detailed numbers in the VERSION variable
        echo $VERSION | cut -d" " -f1  # e.g. (fedora)19, (redhat)6.4, (ubuntu)18.04.3
    }
    #
    defsys_mandrake() {
        # Echo a string suited for $GAG_ENV_NAME by parsing /etc/mandrake-release
        MANDRAKE_KIND=`cut -d" " -f1 /etc/mandrake-release`
        if [ "$MANDRAKE_KIND" = "Mandrakelinux" ]; then
            echo "mandrake"`cut -d" " -f3 /etc/mandrake-release|tr -d '()'`
        else
            echo "mandrake"`cut -d" " -f4 /etc/mandrake-release|tr -d '()'`
        fi
        unset MANDRAKE_KIND
    }
    #
    defsys_redhat() {
        # Echo a string suited for $GAG_ENV_NAME by parsing /etc/redhat-release
        # This subroutine is mostly obsolete except for RedHat 6* family (in
        # particular Scientific Linux) which does not provide a /etc/os-release
        REDHAT_KIND=`cut -d" " -f1 /etc/redhat-release | tr '[:upper:]' '[:lower:]'`
        REDHAT_VERS=`\sed "s%.*release\s*%%i" /etc/redhat-release | cut -d" " -f1`
        if [ "$REDHAT_KIND" = "fedora" ]; then
            echo "fedora"$REDHAT_VERS
        elif [ "$REDHAT_KIND" = "centos" -o "$REDHAT_KIND" = "almalinux" ]; then
            # Limit to the major version number (RedHat/CentOS/AlmaLinux ensure
            # compatibility between minor versions): grab the number "WX" just
            # after the "release" word
            REDHAT_VERS=`\sed -r "s%.*release\s*([0-9]*).*%\1%i" /etc/redhat-release`
            echo $REDHAT_KIND$REDHAT_VERS
        else
            # Generic for e.g.
            #   Scientific Linux SL release 5.5 (Boron)
            #   Scientific Linux release 6.0 (Carbon)
            echo "redhat"$REDHAT_VERS
        fi
        unset REDHAT_KIND REDHAT_VERS
    }
    #
    defsys_redhat_full() {
        # Echo a string suited for $GAG_ENV_VERS by parsing /etc/redhat-release
        \sed "s%.*release\s*%%i" /etc/redhat-release | cut -d" " -f1
    }
    #
    defsys_debian() {
        # Echo a string suited for $GAG_ENV_NAME by parsing /etc/debian_version
        echo "debian"`\sed "s#[/\.].*##" /etc/debian_version`
    }
    #
    defsys_debian_full() {
        # Echo a string suited for $GAG_ENV_VERS by parsing /etc/debian_version
        cat /etc/debian_version
    }
    #
    defsys_suse() {
        # Echo a string suited for $GAG_ENV_NAME by parsing /etc/SuSE-release
        SUSE_KIND=`grep -i suse /etc/SuSE-release | cut -d" " -f1 `
        if [ "$SUSE_KIND" = "openSUSE" ]; then
            echo "suse"`grep -i suse /etc/SuSE-release | cut -d" " -f2`
        else
            echo "suse"`grep -i suse /etc/SuSE-release | cut -d" " -f3`
        fi
        unset SUSE_KIND
    }
    #
    #######################################################################
    #
    # Option parsing
    #
    DEFSYS_CCOMPILER="default"   # Default C compiler will be setup by gagdefsys
    DEFSYS_CXXCOMPILER="default" # Default C++ compiler will be setup by gagdefsys
    DEFSYS_FCOMPILER="default"   # Default Fortran compiler will be setup by gagdefsys
    DEFSYS_CONFIG="default"      # Default config will be setup by gagdefsys
    DEFSYS_TARGET="default"      # Default target is same as build environment
    executable=1                 # Default is to set up the executable tree.
    temp=`getopt "hvb:c:d:o:n:st:" "$@"`
    if [ $? -ne 0 ]; then defsys_usage; \return 2; fi
    eval set -- "$temp"
    unset temp
    while [ $1 != -- ]; do
        case $1 in
        -b) DEFSYS_CCOMPILER=$2; shift ;;
        -c) DEFSYS_FCOMPILER=$2; shift ;;
        -d) DEFSYS_CXXCOMPILER=$2; shift ;;
        -o) if [ $DEFSYS_CONFIG = "default" ]; then DEFSYS_CONFIG=$2;    else DEFSYS_CONFIG=$DEFSYS_CONFIG-$2;    fi; shift;;
        -n) if [ $DEFSYS_CONFIG = "default" ]; then DEFSYS_CONFIG=no_$2; else DEFSYS_CONFIG=$DEFSYS_CONFIG-no_$2; fi; shift;;
        -s) executable=0 ;;
        -t) DEFSYS_TARGET=$2; shift ;;
        -v) defsys_showversion; \return 0 ;;
        -h) defsys_usage; \return 0 ;;
        esac
        shift # Next flag
    done
    shift # Skip double dash
    case $# in
        !0) defsys_usage; \return 2 ;;
    esac
    set abc; shift # This line to avoid remanence effect in a portable way
    #
    #######################################################################
    #
    # More detailed tests about the MACHINE and OS types will be implemented
    # when needed.
    #
    DEFAULT_CCOMPILER=none
    DEFAULT_CXXCOMPILER=none
    DEFAULT_FCOMPILER=none
    DEFAULT_CONFIG=          # Default config is empty
    case `uname` in
        Linux)
            if [ `uname -m | grep -c "x86_64"` -ne 0 ]; then
        	GAG_MACHINE=x86_64
            elif [ `uname -m | grep -c "aarch64"` -ne 0 ]; then
                GAG_MACHINE=arm64
            else
        	GAG_MACHINE=pc
            fi
            GAG_ENV_KIND=linux
            #
            # Define $GAG_ENV_NAME
            # There should not be any "-" character between the linux flavor
            # and its version to enable easy parsing of the GAG_EXEC_SYSTEM
            # environment variable back into MACHINE, GAG_ENV_NAME and COMPILER.
            if [ -f /etc/os-release ]; then
                GAG_ENV_NAME=`defsys_generic`
            elif [ -f /etc/redhat-release ]; then
                GAG_ENV_NAME=`defsys_redhat`
            elif [ -f /etc/mandrake-release ]; then
                GAG_ENV_NAME=`defsys_mandrake`
            elif [ -f /etc/debian_version ]; then
                GAG_ENV_NAME=`defsys_debian`
            elif [ -f /etc/SuSE-release ]; then
                GAG_ENV_NAME=`defsys_suse`
            else
                GAG_ENV_NAME="linux"
            fi
            #
            # Define $GAG_ENV_VERS. Custom OS files may provide more
            # detailed information than /etc/os-release
            if [ -f /etc/redhat-release ]; then
                GAG_ENV_VERS=`defsys_redhat_full`
            elif [ -f /etc/debian_version ]; then
                GAG_ENV_VERS=`defsys_debian_full`
            elif [ -f /etc/os-release ]; then
                GAG_ENV_VERS=`defsys_generic_full`
            else
                GAG_ENV_VERS=$GAG_ENV_NAME
            fi
            #
            # Define default compilers
            if which gcc > /dev/null 2>&1; then
                DEFAULT_CCOMPILER=gcc
            fi
            if which g++ > /dev/null 2>&1; then
                DEFAULT_CXXCOMPILER=g++
            fi
            if which ifort > /dev/null 2>&1; then
                DEFAULT_FCOMPILER=ifort
            elif which gfortran > /dev/null 2>&1; then
        	DEFAULT_FCOMPILER=gfortran
            fi ;;
        Darwin)
            GAG_ENV_KIND=darwin
            GAG_ENV_NAME=`sw_vers -productName | tr -d ' ' | tr '[:upper:]' '[:lower:]'`
            GAG_ENV_VERS=`sw_vers -productVersion`
            if [ `uname -p` = "i386" ]; then
        	if ! \which sysctl > /dev/null 2>&1; then
        	    defsys_message "WARNING: command 'sysctl' not found, assuming x86_64 architecture"
        	    GAG_MACHINE=x86_64
        	elif [ `sysctl -n hw.optional.x86_64 2> /dev/null` = 1 ]; then
        	    GAG_MACHINE=x86_64
        	else
        	    GAG_MACHINE=pc
        	fi
                if which gcc > /dev/null 2>&1; then
                    DEFAULT_CCOMPILER=gcc
                fi
                if which g++ > /dev/null 2>&1; then
                    DEFAULT_CXXCOMPILER=g++
                elif which clang++ > /dev/null 2>&1; then
                    DEFAULT_CXXCOMPILER=clang++
                fi
        	if which ifort > /dev/null 2>&1; then
        	    DEFAULT_FCOMPILER=ifort
        	elif which gfortran > /dev/null 2>&1; then
        	    DEFAULT_FCOMPILER=gfortran
        	fi
            elif [ `uname -p` = "arm" ]; then
                GAG_MACHINE=arm64
                if which gcc > /dev/null 2>&1; then
                    DEFAULT_CCOMPILER=gcc
                fi
                if which clang++ > /dev/null 2>&1; then
                    DEFAULT_CXXCOMPILER=clang++
                elif which g++ > /dev/null 2>&1; then
                    DEFAULT_CXXCOMPILER=g++
                fi
                if which ifort > /dev/null 2>&1; then
                    DEFAULT_FCOMPILER=ifort
                elif which gfortran > /dev/null 2>&1; then
                    DEFAULT_FCOMPILER=gfortran
                fi
            fi ;;
        CYGWIN*)
            if [ `uname -m | grep -c "x86_64"` -ne 0 ]; then
                GAG_MACHINE=x86_64
            else
                GAG_MACHINE=pc
            fi
            GAG_ENV_KIND=cygwin
            GAG_ENV_NAME=cygwin
            GAG_ENV_VERS=`uname -r`
            if which gcc > /dev/null 2>&1; then
                DEFAULT_CCOMPILER=gcc
            fi
            if which g++ > /dev/null 2>&1; then
                DEFAULT_CXXCOMPILER=g++
            fi
            if which gfortran > /dev/null 2>&1; then
        	DEFAULT_FCOMPILER=gfortran
            fi ;;
        MINGW32*)
            GAG_MACHINE=pc
            GAG_ENV_KIND=mingw
            GAG_ENV_NAME=mingw
            GAG_ENV_VERS=mingw
            if which gcc > /dev/null 2>&1; then
                DEFAULT_CCOMPILER=gcc
            fi
            if which g++ > /dev/null 2>&1; then
                DEFAULT_CXXCOMPILER=g++
            fi
            if which gfortran > /dev/null 2>&1; then
        	DEFAULT_FCOMPILER=gfortran
            fi ;;
        *)
            defsys_error "Unknown operating system. Sorry"
            \return 1
    esac
    #
    #######################################################################
    #
    case "$DEFSYS_TARGET" in
        default)
            GAG_TARGET_KIND=$GAG_ENV_KIND
            GAG_TARGET_NAME=$GAG_ENV_NAME ;;
        mingw)
            if [ "$GAG_ENV_KIND" = "cygwin" ]; then
                GAG_TARGET_KIND=mingw
                GAG_TARGET_NAME=mingw
            else
                defsys_error "Target $DEFSYS_TARGET not supported to build under $GAG_ENV_KIND"
                \return 1
            fi ;;
        *)
            defsys_error "Target $DEFSYS_TARGET not supported to build under $GAG_ENV_KIND"
            \return 1
    esac
    #
    #######################################################################
    #
    if [ "$DEFSYS_CCOMPILER" = "default" ]; then
        if [ "$DEFAULT_CCOMPILER" = "none" ]; then
            defsys_error "No default C compiler found. Sorry"
        else
            GAG_COMPILER_CEXE=$DEFAULT_CCOMPILER
        fi
    else
        # User provided the compiler (executable name)
        GAG_COMPILER_CEXE=$DEFSYS_CCOMPILER
    fi
    #
    if [ "$DEFSYS_CXXCOMPILER" = "default" ]; then
        if [ "$DEFAULT_CXXCOMPILER" = "none" ]; then
            # Not an error. C++ entry points will be disabled
            GAG_COMPILER_CXXEXE=
        else
            GAG_COMPILER_CXXEXE=$DEFAULT_CXXCOMPILER
        fi
    else
        # User provided the compiler (executable name)
        GAG_COMPILER_CXXEXE=$DEFSYS_CXXCOMPILER
    fi
    #
    if [ "$DEFSYS_FCOMPILER" = "default" ]; then
        if [ "$DEFAULT_FCOMPILER" = "none" ]; then
            defsys_error "No default FORTRAN 90/95 compiler found. Sorry"
        else
            GAG_COMPILER_FEXE=$DEFAULT_FCOMPILER
        fi
    else
        # User provided the compiler (executable name)
        GAG_COMPILER_FEXE=$DEFSYS_FCOMPILER
    fi
    #
    if [ "$DEFSYS_CONFIG" = "default" ]; then
        GAG_CONFIG=$DEFAULT_CONFIG
    else
        GAG_CONFIG=$DEFSYS_CONFIG
    fi
    # Sort GAG_CONFIG components
    if [ "$GAG_CONFIG" != "" ]; then
        GAG_CONFIG=`echo $GAG_CONFIG|\sed 's/-/=-/g;s/^/-/'|tr '=' '\n'|sort|uniq|tr -d '\n'`
    fi
    #
    if [ $executable -eq 0 ]; then
        # COMPILATION MODE
        which $GAG_COMPILER_FEXE > /dev/null 2>&1 ||{
            defsys_message "Compiler $GAG_COMPILER_FEXE not found in \$PATH"
            defsys_message "This may mean the compiler name is an alias resolved later on"
        }
        #
        # Detect the compiler "kind". Do not rely on its name.
        if [ `$GAG_COMPILER_CEXE --version | head -1 | grep -i -c "clang"` -ne 0 ]; then
            GAG_COMPILER_CKIND="clang"
        elif [ `$GAG_COMPILER_CEXE --version | head -1 | grep -i -c "gcc"` -ne 0 ]; then
            GAG_COMPILER_CKIND="gcc"
        else
            # Compiler kind not recognized, use executable name
            GAG_COMPILER_CKIND=$GAG_COMPILER_CEXE
        fi
        if [ `$GAG_COMPILER_CXXEXE --version | head -1 | grep -i -c "clang"` -ne 0 ]; then
            GAG_COMPILER_CXXKIND="clang++"
        elif [ `$GAG_COMPILER_CXXEXE --version | head -1 | grep -i -c "gcc"` -ne 0 ]; then
            GAG_COMPILER_CXXKIND="g++"
        else
            # Compiler kind not recognized, use executable name
            GAG_COMPILER_CXXKIND=$GAG_COMPILER_CXXEXE
        fi
        if [ `$GAG_COMPILER_FEXE --version | head -1 | grep -i -c "ifort"` -ne 0 ]; then
            GAG_COMPILER_FKIND="ifort"
        elif [ `$GAG_COMPILER_FEXE --version | head -1 | grep -i -c "ifx"` -ne 0 ]; then
            if [ -n "$GAG_IFX" ]; then
                GAG_COMPILER_FKIND="ifort"
            else
                defsys_error "IFX Fortran compiler is not supported."
                \return 1
            fi
        elif [ `$GAG_COMPILER_FEXE --version | head -1 | grep -i -c "gnu"` -ne 0 ]; then
            GAG_COMPILER_FKIND="gfortran"
        else
            # Compiler kind not recognized, use executable name
            GAG_COMPILER_FKIND=$GAG_COMPILER_FEXE
        fi
        #
        # Detect the compiler version.
        GAG_COMPILER_FVERSION=
        GAG_COMPILER_FVERSION_GE_10=no
        if [ "$GAG_COMPILER_FKIND" = "ifort" ]; then
            # Compress Major+Minor, no Micro number (no dot = easier preprocessing tests)
            # Example of possible strings from ifort --version:
            #   ifort (IFORT) 9.0  20051201
            #   ifort (IFORT) 11.1 20090511
            #   ifort (IFORT) 14.0.2 20140120
            #   ifort (IFORT) 2021.9.0 20230302
            #   ifx (IFX) 2023.1.0 20230320
            IFORT_VERSION=`$GAG_COMPILER_FEXE --version 2>/dev/null | head -1 | cut -d" " -f3`  # e.g. 14.0.2
            # Compress Major+Minor+Micro (no dot = easier preprocessing tests)
            GAG_COMPILER_FVERSION=`echo $IFORT_VERSION | \sed "s%\.%%;s%\..*%%"`  # e.g. 14.0.2 => 140
            #
            if [ "$GAG_CONFIG" != -no_openmp ] && verle $IFORT_VERSION "11.1"; then
                # ifort version 11.1 fails to execute OpenMP standard code regarding
                # allocatable arrays allocated out of the DO PARALLEL but PRIVATE
                # to the loop (which means they should be duplicated with implicit
                # allocation for each thread).
                defsys_error "ifort version $IFORT_VERSION unsupported with OpenMP configuration."
                \return 1
            fi
        elif [ "$GAG_COMPILER_FKIND" = "gfortran" ]; then
            GFORTRAN_VERSION=`$GAG_COMPILER_FEXE --version | head -1 | \sed "s%.*[^0-9]\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*%\1%"`
            # Compress Major+Minor+Micro (no dot = easier preprocessing tests)
            GAG_COMPILER_FVERSION=`echo $GFORTRAN_VERSION | \sed "s%\.%%g"`
            # Minimum Gfortran version:
            GFORTRAN_MINI="9.0.0"
            if verlt "$GFORTRAN_VERSION" "$GFORTRAN_MINI"; then
                defsys_error "gfortran version $GFORTRAN_VERSION unsupported. You can consider upgrading to version $GFORTRAN_MINI at least."
                \return 1
            fi
            if verlt "10" "$GFORTRAN_VERSION"; then
                # Gfortran 10 comes with a number of changes which need
                # to add options in the Malefiles
                GAG_COMPILER_FVERSION_GE_10=yes
            fi
            if [ "$GAG_CONFIG" = -sanitize ] && verlt "13" "$GFORTRAN_VERSION"; then
                # See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=110157
                # Solution is to disable one sanitizer feature. See
                # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=87875
                # There should be an upper version limit to this option at some point
                defsys_message "Disabling sanitizer's detect_stack_use_after_return for gfortran version $GFORTRAN_VERSION"
                export ASAN_OPTIONS='detect_stack_use_after_return=false'
            fi
        fi
        # Reject g95: no known version is able to compile new gildas.
        if [ "$GAG_COMPILER_FKIND" = "g95" ]; then
            # It can happen that the chosen compiler is not in the $PATH, e.g.
            # in executable branch (gagiram -c g95). If it is, check its version.
            defsys_error "g95 Fortran compiler is not supported."
            \return 1
        fi
        #
        # Define GAG_COMPILER_FLIBS and GAG_COMPILER_*FLAGS only if we are
        # about to compile.
        GAG_COMPILER_FLIBS=
        GAG_COMPILER_CPPFLAGS=
        GAG_COMPILER_FFLAGS=
        GAG_COMPILER_SLDFLAGS=
        GAG_COMPILER_FLDFLAGS=
        if [ "$GAG_COMPILER_FKIND" = "ifort" ]; then
            GAG_COMPILER_FLIBS="-lifcore -lgcc_s"
            if verlt "$IFORT_VERSION" "10"; then
                GAG_COMPILER_FFLAGS="-mp"
            elif verlt "$IFORT_VERSION" "2020"; then
                GAG_COMPILER_FFLAGS="-mieee-fp"
            else
                GAG_COMPILER_FFLAGS="-fltconsistency"
            fi
            if [ `$GAG_COMPILER_FEXE --version 2>&1 | grep -i -c "#10448"` -ne 0 ]; then
                # Hide warning about obsolescent ifort 21
                GAG_COMPILER_FFLAGS="$GAG_COMPILER_FFLAGS -diag-disable=10448"
                GAG_COMPILER_SLDFLAGS="$GAG_COMPILER_SLDFLAGS -diag-disable=10448"
                GAG_COMPILER_FLDFLAGS="$GAG_COMPILER_FLDFLAGS -diag-disable=10448"
            fi
        elif [ "$GAG_COMPILER_FKIND" = "gfortran" ]; then
            GAG_COMPILER_FLIBS="-lgfortran"
        fi
        #
        CPP_VERSION=`cpp --version | head -1 | \sed "s%.*[^0-9]\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*%\1%"`
        if verle "4.8" "$CPP_VERSION"; then
            # See http://gcc.gnu.org/gcc-4.8/porting_to.html
            GAG_COMPILER_CPPFLAGS="$GAG_COMPILER_CPPFLAGS -ffreestanding"
        fi
        #
        export GAG_COMPILER_FLIBS GAG_COMPILER_CPPFLAGS
        export GAG_COMPILER_FFLAGS GAG_COMPILER_SLDFLAGS GAG_COMPILER_FLDFLAGS
        #
        if \which python > /dev/null 2>&1; then
            GAG_PYTHON_EXE=python
        elif \which python3 > /dev/null 2>&1; then
            GAG_PYTHON_EXE=python3
        else
            GAG_PYTHON_EXE=
        fi
        if [ -n "$GAG_PYTHON_EXE" ]; then
            GAG_PYTHON_VERSION=`$GAG_PYTHON_EXE -V 2>&1 | cut -d' ' -f2`
        else
            GAG_PYTHON_VERSION=
        fi
        #
    else
        # EXECUTION MODE
        # Set GAG_COMPILER_FKIND (used below). Warning: we can not make
        # the assumption that the executable is callable from the machine
        # we are running (binaries may have been compiled elsewhere e.g.
        # because of licenses).
        GAG_COMPILER_CKIND=$GAG_COMPILER_CEXE
        GAG_COMPILER_FKIND=$GAG_COMPILER_FEXE
        GAG_COMPILER_FVERSION=
    fi
    #
    #######################################################################
    #
    GAG_COMP_SYSTEM=$GAG_MACHINE-$GAG_ENV_KIND-$GAG_COMPILER_FKIND
    # Build $GAG_EXEC_SYSTEM from $GAG_COMPILER_FEXE so that
    #  source admin/gildas-env.sh -c ifort  (use default ifort e.g. ifort 11) and
    #  source admin/gildas-env.sh -c ifort14  (i.e. use namely non default ifort 14)
    # does not build their binaries in the same integration branch.
    GAG_EXEC_SYSTEM=$GAG_MACHINE-$GAG_TARGET_NAME-$GAG_COMPILER_FEXE$GAG_CONFIG
    export GAG_COMP_SYSTEM GAG_EXEC_SYSTEM
    export GAG_MACHINE GAG_CONFIG
    export GAG_ENV_KIND GAG_ENV_NAME GAG_ENV_VERS
    export GAG_TARGET_KIND GAG_TARGET_NAME
    export GAG_COMPILER_CKIND GAG_COMPILER_CEXE
    export GAG_COMPILER_CXXKIND GAG_COMPILER_CXXEXE
    export GAG_COMPILER_FKIND GAG_COMPILER_FEXE GAG_COMPILER_FVERSION GAG_COMPILER_FVERSION_GE_10
    export GAG_PYTHON_EXE GAG_PYTHON_VERSION
    #
    defsys_clean
    #
}
#
###########################################################################
