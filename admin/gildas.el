;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;		         GILDAS Customization                        ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Load correct mode depending on the file suffix.
(setq auto-mode-alist
      (append
       '(("\\Makefile.*" . makefile-mode)
	 ("\\.nroff$" . nroff-mode)
	 ("\\.inc$" . fortran-mode)
	 ("\\.incpp$" . fortran-mode)
	 ("\\.fpp$" . fortran-mode)
	 ("\\.f90pp$" . f90-mode)
	 ("\\.pro$" . f90-mode)
	 ("\\.sic$" . f90-mode)
	 ("\\.gtv$" . f90-mode)
	 ("\\.greg$" . f90-mode)
	 ("\\.graphic$" . f90-mode)
	 ("\\.pako$" . f90-mode)
	 ("\\.astro$" . f90-mode)
	 ("\\.obs$" . f90-mode)
	 ("\\.cal$" . f90-mode)
	 ("\\.telcal$" . f90-mode)
	 ("\\.mira$" . f90-mode)
	 ("\\.class$" . f90-mode)
	 ("\\.clic$" . f90-mode)
	 ("\\.nic$" . f90-mode)
	 ("\\.map$" . f90-mode)
	 ("\\.mis$" . f90-mode)
	 ("\\.wifi$" . f90-mode)
	 ("\\.cube$" . f90-mode)
	 ("\\gag.dico*" . f90-mode))
       auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
