###########################################################################
#
# Shell verification. Try to be robust => very simple.
#
GAG_SHELL_TEST="shell test" || echo "go.sh error: You must be under a sh-compatible shell!"
GAG_SHELL_TEST="shell test" || exit 1
unset GAG_SHELL_TEST
#
###########################################################################
#
# Main function definition
#
gaggo() {
    #
    # Variable and function definitions
    #
    GO_AUTHOR='J. Pety <pety@iram.fr>'
    GO_PROJECT='GILDAS <http://www.iram.fr/IRAMFR/GILDAS>'
    GO_PROGNAME='go.sh'
    #
    go_usage() {
	cat <<EOF 1>&2

Enable easy browsing through GILDAS sources, integration branch, or
executables by providing shortcuts. When browsing the sources, verify
whether others are already editing the target directory. The default
browsed GILDAS trees are defined by the \$gagsrcdir, \$gagintdir, and
\$gagexedir environment variables. The default behavior (ie neither
-e, -i or -s is set) is to browse the source tree as this facility is
intended first for developers.

  usage: source $GO_PROGNAME [options] [subdirectory-name]

options:
  -e          Browse executable  tree
  -i          Browse integration tree
  -s          Browse source      tree
  -h          Show this help page
  -v          Show version information

Examples:

  source $GO_PROGNAME home        -> Go to \$gaghome if this variable is set
  source $GO_PROGNAME gadm        -> Go to \$gaghome/gildas-admin if this directory exists
  source $GO_PROGNAME logs        -> Go to \$gaghome/logs if this directory exists
  source $GO_PROGNAME back or $GO_PROGNAME -  -> Go to the before last directory visited with the go command

  source $GO_PROGNAME -s          -> Go to \$gagsrcdir
  source $GO_PROGNAME -s packages -> Go to \$gagsrcdir/packages
  source $GO_PROGNAME -s greg     -> Go to \$gagsrcdir/kernel/lib/greg (ie the greg library)

  source $GO_PROGNAME -i          -> Go to \$gagintdir
  source $GO_PROGNAME -i pro      -> Go to \$gagintdir/pro
  source $GO_PROGNAME -i lib      -> Go to \$gagintdir/MACHINE-OS-COMPILER/lib
  source $GO_PROGNAME -i system   -> Go to \$gagintdir/MACHINE-OS-COMPILER

  source $GO_PROGNAME -e          -> Go to \$gagexedir
  source $GO_PROGNAME -e pro      -> Go to \$gagexedir/pro
  source $GO_PROGNAME -e lib      -> Go to \$gagexedir/MACHINE-OS-COMPILER/lib
  source $GO_PROGNAME -e system   -> Go to \$gagexedir/MACHINE-OS-COMPILER

EOF
	go_clean
    }
    #
    go_showversion() {
	echo "$GO_PROGNAME by $GO_AUTHOR"
	echo "Project: $GO_PROJECT"
	go_clean
    }
    #
    go_message() {
	echo "$1"
    }
    #
    go_error() {
	go_message "$GO_PROGNAME error: $1" 1>&2
    }
    #
    go_clean() {
	unset GO_PACKAGE GO_SUBDIR GO_DIR
	unset GO_AUTHOR GO_PROJECT GO_PROGNAME
	unset go_usage go_showversion go_message go_error go_clean
	unset tree
    }
    #
    #######################################################################
    #
    # Option parsing
    #
    tree=1  # source tree = 1 (default), integration tree = 2, executable tree = 3
    temp=`getopt "hveis" "$@"`
    if [ $? -ne 0 ]; then go_usage; return 2; fi
    eval set -- "$temp"
    unset temp
    while [ $1 != -- ]; do
	case $1 in
	-e) tree=3 ;;
	-i) tree=2 ;;
	-s) tree=1 ;;
	-h) go_usage; set --; return 0 ;;
	-v) go_showversion; set --; return 0 ;;
	esac
	shift # Next flag
    done
    shift # Skip double dash
    case $# in
	0) GO_SUBDIR="" ;; # Default subdirectory (see below)!
	1) GO_SUBDIR=$1 ;;
	*) go_usage; set --; return 2 ;;
    esac
    set abc; shift # This line to avoid remanence effect in a portable way
    #
    #######################################################################
    #
    # Verifies whether the $gagexedir, $gagintdir, or $gagsrcdir directory exists.
    #
    if [ $tree -eq 2 ]; then
	if [ "$gagintdir" = "" ]; then
	    go_error "\$gagintdir is not set"
	    go_error "Please define it by hand"
	    go_clean
	    return 1
	fi
	if [ ! -d $gagintdir ]; then
	    go_error "$gagintdir tree does not exist"
	    go_error "Please (re)define \$gagintdir by hand"
	    go_clean
	    return 1
	fi
    elif [ $tree -eq 3 ]; then
	if [ "$gagexedir" = "" ]; then
	    go_error "\$gagexedir is not set"
	    go_error "Please define it by hand or through the source-gag.sh script with -me options"
	    go_clean
	    return 1
	fi
	if [ ! -d $gagexedir ]; then
	    go_error "$gagexedir tree does not exist"
	    go_error "Please (re)define \$gagexedir by hand or through the source-gag.sh script with -me options"
	    go_clean
	    return 1
	fi
    else
	if [ "$gagsrcdir" = "" ]; then
	    go_error "\$gagsrcdir is not set"
	    go_error "Please define it by hand or through the source-gag.sh script with -ms options"
	    go_clean
	    return 1
	fi
	if [ ! -d $gagsrcdir ]; then
	    go_error "$gagsrcdir tree does not exist"
	    go_error "Please (re)define \$gagsrcdir by hand or through the source-gag.sh script with -ms options"
	    go_clean
	    return 1
	fi
    fi
    #
    #######################################################################
    #
    # Define full directory path from alias
    #
    if [ "$GO_SUBDIR" = "home" ]; then
	if [ -z "$gaghome" ]; then
	    go_error "\$gaghome is not set"
	    go_clean
	    return 1
	elif [ ! -d $gaghome ]; then
	    go_error "\$gaghome=$gaghome directory does not exist"
	    go_clean
	    return 1
	else
	    GO_DIR=$gaghome
	fi
    elif [ "$GO_SUBDIR" = "gadm" ]; then
	if [ -z "$gaghome" ]; then
	    go_error "\$gaghome is not set"
	    go_clean
	    return 1
	elif [ ! -d $gaghome/gildas-admin ]; then
	    go_error "$gaghome/gildas-admin directory does not exist"
	    go_clean
	    return 1
	else
	    GO_DIR=$gaghome/gildas-admin
	fi
    elif [ "$GO_SUBDIR" = "logs" ]; then
	if [ -z "$gaghome" ]; then
	    go_error "\$gaghome is not set"
	    go_clean
	    return 1
	elif [ ! -d $gaghome/logs ]; then
	    go_error "$gaghome/logs directory does not exist"
	    go_clean
	    return 1
	else
	    GO_DIR=$gaghome/logs
	fi
    elif [ $tree -eq 2 ] || [ $tree -eq 3 ]; then
	if [ $tree -eq 2 ]; then GO_TREE=$gagintdir; fi
	if [ $tree -eq 3 ]; then GO_TREE=$gagexedir; fi
	case $GO_SUBDIR in
	    demo|doc|etc|pro)
		GO_DIR=$GO_TREE/$GO_SUBDIR ;;
	    hlp|html|pdf)
	        GO_DIR=$GO_TREE/doc/$GO_SUBDIR ;;
	    bin|data|include|lib|python|tasks)
		GO_DIR=$GO_TREE/$GAG_EXEC_SYSTEM/$GO_SUBDIR ;;
	    system)
		GO_DIR=$GO_TREE/$GAG_EXEC_SYSTEM ;;
	    back|-)
		GO_DIR=$GO_OLDDIR ;;
	    *)
		GO_DIR=$GO_TREE/$GO_SUBDIR
	esac
    else
	GO_PACKAGE=`pwd | \sed "s%$gagsrcdir/packages/%%;s%/.*%%"`
	if [ "$GO_PACKAGE" = "" ]; then
	   GO_PACKAGE="kernel"
	else
	   GO_PACKAGE="packages/$GO_PACKAGE"
	fi
	case $GO_SUBDIR in
	    demo|doc|etc|lib|main|pro|tasks*)
		GO_DIR=$gagsrcdir/$GO_PACKAGE/$GO_SUBDIR ;;
	    blas|lapack|slatec|cfitsio)
		GO_DIR=$gagsrcdir/legacy/$GO_SUBDIR ;;
	    30m|gildas|pdbi)
		GO_DIR=$gagsrcdir/cookbooks/$GO_SUBDIR ;;
	    astro*|atm*|classic*|class*|mira*|clic*|cube*|flux*|gtemplate*|map*|mis*|mrtcal*|mrtholo*|nic*|obs*|pako*|point*|telcal*|wifisyn*|mywifisyn*|mymis*)
		GO_DIR=$gagsrcdir/packages/$GO_SUBDIR ;;
	    include|python)
		GO_DIR=$gagsrcdir/kernel/$GO_SUBDIR ;;
	    gchar|gcont|gcore|ggtk|ggui|gfits|gio|gmath|greg|gsys|gtv|gwcs|gwidget|gwindows|gx11|sic|gkernel)
		GO_DIR=$gagsrcdir/kernel/lib/$GO_SUBDIR ;;
	    binding*|linedb*)
		GO_DIR=$gagsrcdir/kernel/python/$GO_SUBDIR ;;
	    lut-desc|term-desc)
		GO_DIR=$gagsrcdir/kernel/setup/$GO_SUBDIR ;;
	    weeds*|imager*)
		GO_DIR=$gagsrcdir/contrib/$GO_SUBDIR ;;
	    imbfits)
	        GO_DIR=$gagsrcdir/packages/mrtcal/lib-imbfits ;;
	    back|-)
		GO_DIR=$GO_OLDDIR ;;
	    *)
		GO_DIR=$gagsrcdir/$GO_SUBDIR
	esac
    fi
    #
    #######################################################################
    #
    # Try to go there (and to see whether someone else is doing something
    # in this subdirectory when browsing the source tree)
    #
    if [ -d $GO_DIR ]; then
	echo
	go_message "Going to $GO_DIR"
	GO_OLDDIR=`pwd`
	export GO_OLDDIR
	cd $GO_DIR || return $?
	echo
	if [ $tree -eq 1 ]; then
	    if git rev-parse --git-dir > /dev/null 2>&1; then
		# These are GIT sources
		git status --short --branch .
	    fi
	fi
    else
	if [ $tree -eq 2 ]; then
	    go_error "$GO_SUBDIR directory does not exist in $gagintdir tree"
	elif [ $tree -eq 3 ]; then
	    go_error "$GO_SUBDIR directory does not exist in $gagexedir tree"
	else
	    go_error "$GO_SUBDIR directory does not exist in $gagsrcdir tree"
	fi
	go_clean
	return 1
    fi
    #
    #######################################################################
    #
    # Clean up
    #
    go_clean
}
#
###########################################################################
#
gaggo "$@"
set abc; shift # This line to avoid remanence effect in a portable way
#
###########################################################################
