#!/bin/sh
#
# Variable and function definitions
#
AUTHOR='J. Pety <pety@iram.fr>'
PROJECT='GILDAS <http://www.iram.fr/IRAMFR/GILDAS>'
PROGNAME=`basename $0`
#
usage() {
    cat <<EOF 1>&2

(Re)build (ie get, compile and install) a GILDAS version using standard
directory name conventions. A log of all command output is automatically
kept. When used with the "source-gag.sh" script, it enable easy coexistence
of many different GILDAS version. However, it may also be used to regularly
rebuild just one version.

usage: $PROGNAME [options] [version]

version:     description:                 repository tag:
  dev          Latest repository state      HEAD
  day          Current day (ie today)       day
  last         Last fully compilable        last
  month        Current month                (Not available)
  jan04        jan04 monthly release        jan04

Comment: contrary to the "last" version, the "day" one may not 
         be compilable!

options:
  -c compiler Replace default compiler by this one
  -R          Start from scratch (ie remove current version first if needed)
  -e          Build executables only
  -a          Build all (executables and documentation)
  -o config   Add config to the config list
  -r          Mark the version as reference
  -h          Show this help page
  -v          Show version information

EOF
}
#
showversion() {
    echo "$PROGNAME, by $AUTHOR"
    echo "Project: $PROJECT"
}
#
message() {
    echo "$PROGNAME: $1"
}
#
error_exit() {
    echo 1>&2
    echo "$PROGNAME error: $1" 1>&2
    echo 1>&2
    exit $2
}
#
###########################################################################
#
# Option parsing
#
update=1                # Default is to update
BUILDOPT1="-e"          # Default is to build executables only
COMPILEOPT="-c default" # Default compiler will be setup by gagdefsys
temp=`getopt "c:Raeo:rhv" "$@"`
if [ $? -ne 0 ]; then usage; exit 1; fi
eval set -- "$temp"
unset temp
while [ $1 != -- ]; do
    case $1 in
    -c) COMPILEOPT="-c $2"; shift ;;
    -a) BUILDOPT1="-a" ;; # Mutually exclusive options
    -e) BUILDOPT1="-e" ;; # Mutually exclusive options
    -r) BUILDOPT2="-r $BUILDOPT2" ;;
    -o) BUILDOPT2="-o $2 $BUILDOPT2" ;;
    -R) BUILDOPT2="-R $BUILDOPT2" ; update=0 ;;
    -v) showversion; exit 0 ;;
    -h) usage; exit 0 ;;
    esac
    shift # Next flag
done
shift # Skip double dash
case $# in
    0) IN_VERSION="month" ;; # Default version!
    1) IN_VERSION=$1 ;;
    *) usage; exit 1 ;;
esac
set abc; shift # This line to avoid remanence effect in a portable way
#
###########################################################################
#
# Define the gaghome directory, create it if needed, then go there.
#
if [ -z "$gaghome" ]; then
    gaghome=$HOME/gildas
    export gaghome
fi
message "\$gaghome=$gaghome"
#
if [ ! -d $gaghome ]; then
    mkdir $gaghome || error_exit "Failed to create $gaghome" 11
fi
#
cd $gaghome || error_exit "Failed to go to $gaghome" 11
#
###########################################################################
#
# Define TAG to be used to recover the admin directory
#
# dev is equivalent to main trunk.
#
# month is an alias for the current month
# (ie the first 3 letters of the current month + last 2 digits of current year).
#
case $IN_VERSION in
    dev)   GAG_TAG=HEAD;;
    month) GAG_TAG=`date '+%b%y' | tr '[:upper:]' '[:lower:]'`;;
    *)     GAG_TAG=$IN_VERSION
esac
export GAG_TAG
#
###########################################################################
#
# Initiate processus by getting administrative files
#
message "Get $GAG_TAG administrative files"
#
if [ $update -eq 0 ]; then
    rm -rf gildas-admin
fi
#
if [ -z "$CVSROOT" ]; then
    CVSROOT=:pserver:anonymous@cvs.iram.fr:/CVS/GILDAS
    export CVSROOT
fi
#
# Frozen snapshot (i.e. impossible to commit) unless DEV version!
#
if [ "$GAG_TAG" != "HEAD" ]; then
    REVOPT="-r ${GAG_TAG}"
else
    REVOPT="-A"
fi
#
if [ -d gildas-admin ]; then
    cd gildas-admin   || error_exit "Failed to get $GAG_TAG administrative files" 11
    cvs up -d $REVOPT || error_exit "Failed to get $GAG_TAG administrative files" 11
    cd ..             || error_exit "Failed to get $GAG_TAG administrative files" 11
else
    cvs co $REVOPT -d gildas-admin gildas/admin || error_exit "Failed to get $GAG_TAG administrative files" 11
fi
#
###########################################################################
#
# (Re)build!
#
gildas-admin/rebuild $BUILDOPT1 $BUILDOPT2 $COMPILEOPT $IN_VERSION || error_exit "Failed to (re)build $IN_VERSION version" "$?"
#
###########################################################################
#
# Final message
#
message "It worked! Bye."
#
###########################################################################
