#!/usr/bin/perl -w

# Read STDIN and output to STDOUT.

our($progname) = "git2cl.pl";

$commit = 0;
$author = "";
$merge = 0;
$date = "";
$dolog = 0;
$logmessage = "";
$donelog = 0;
@filelist = ();

# If no argument, $dirname is left undefined
$dirname = $ARGV[0];  # ZZZ support more than one dir...

MAIN: while (defined($oneline = <STDIN>)) {

  # syswrite(STDOUT,">>> $oneline");  # Debug

  if ($donelog) {
    # Either blank line (ignore), new commit, or new file
    if ($oneline =~ /^$/) {
      # Ignore
      # syswrite(STDOUT,">>>>>> ignore blank line\n");  # Debug
    } elsif ($oneline =~ /^commit\s\w+$/) {
      # A new commit begins
      # syswrite(STDOUT,">>>>>> new commit, dump previous one\n");  # Debug
      &dump_commit($date,$author,$dirname,\@filelist,$logmessage);
      $commit = 0;
      $author = "";
      $merge = 0;
      $date = "";
      $dolog = 0;
      $logmessage = "";
      $donelog = 0;
      @filelist = ();
      # Let loop continue to the end
    } else {
      # A new file
      # syswrite(STDOUT,">>>>>> new file\n");  # Debug
      push(@filelist,$oneline);
      next MAIN;
    }
  }
  if ($dolog) {
    if ($oneline =~ /^$/) {
      $donelog = 1;
      # syswrite(STDOUT,">>>>>> stop logging\n");  # Debug
    } else {
      $logmessage .= $oneline;
      # syswrite(STDOUT,">>>>>> logging\n");  # Debug
    }
    next MAIN;
  }
  if ($date ne "") {
    $dolog = ($oneline =~ /^$/);
    # syswrite(STDOUT,">>>>>> start logging\n");  # Debug
    next MAIN;
  }
  if ($author ne "") {
    $oneline =~ /^Date:\s+(.*)/;
    $date = $1;
    # syswrite(STDOUT,">>>>>> new date $date\n");  # Debug
    next MAIN;
  }
  if ($commit) {
    # Optional "Merge:" line between "commit" and "Author:"
    $merge = ($oneline =~ /^Merge:\s/);
    if ($merge) {
      # syswrite(STDOUT,">>>>>> new merge\n");  # Debug
      next MAIN;
    }
  }
  if ($commit) {
    $oneline =~ /^Author:\s(.*)\s</;
    $author = $1;
    # syswrite(STDOUT,">>>>>> new author\n");  # Debug
    next MAIN;
  }
  $commit = ($oneline =~ /^commit\s\w+$/);  # A new commit block begins
  if ($commit) {
    # syswrite(STDOUT,">>>>>> found new commit\n");  # Debug
    next MAIN;
  }

} # EndWhile

&dump_commit($date,$author,$dirname,\@filelist,$logmessage);

#========================================================================
sub dump_commit {

  my($date,$author,$dirname,$list_ptr,$message) = @_;
  my($n,$commit);

  # Dump
  ($n,$commit) = dump_files($dirname,$list_ptr);
  if ($n) {
    # Dump only commits affecting the desired directory. Merges
    # are thus ignored.
    syswrite(STDOUT,$date."  ".$author."\n");
    syswrite(STDOUT,$commit);
    syswrite(STDOUT,"\n".$message."\n");
  }

}
#========================================================================
sub dump_files {

  my($dirname,$list_ptr) = @_;
  my($nmatch) = 0;
  my($commit) = "";

  $newline = 1;
  $line = "\n    * ";
  foreach $file (@{$list_ptr}) {

    if (defined($dirname)) {
      # A subdirectory name was passed to program: select
      # only files from this subdirectory. Strip off the
      # subdirectory from selected file names.
      $selected = ($file =~ /^$dirname\/(.*)/);
      $selected_file = $1;
    } else {
      # Select everything (test below always true)
      $selected = ($file =~ /(.*)/);  # Strip off carriage return
      $selected_file = $1;
    }

    if ($selected) {
      $nmatch += 1;
      $file = $selected_file;
      if (not $newline)  { $line .= ", "; }
      $oline = $line;
      $line .= $file;
      $newline = (length($line) > 86);
      if ($newline) {
        $commit .= $oline."\n";
        $line = "    ".$file;
        $newline = 0;
      }

    }

  }
  if ($nmatch)  { $commit .= $line.":\n"; }

  return $nmatch,$commit;

}
#========================================================================
