#!/bin/bash

### Pre-processing #####################################################

# 'raw' measurements file:
inputfile=wind-plot.dat

# Parsed file:
tmpdir=tmp
mkdir -p $tmpdir
tmpfile=${tmpdir}/wind-plot.dat

# Generate file suited for Astro procedure
# NB: should protect against missing input file, which seems to appear
#     when it is downloaded from Bure
grep weather-logger $inputfile | tail -n 1000 | sed -e 's/.*weather-logger://' | grep -v -e repeated > $tmpfile

# PNG directory
pngdir=png
mkdir -p $pngdir

### Processing #########################################################

# Define input file and output directory for astro procedure
export GAG_WINDPLOT_INPUT=$tmpfile
export GAG_WINDPLOT_OUTPUT=$pngdir
export GAG_WINDPLOT_EXPERIMENTAL=yes  # Overplot the string "EXPERIMENTAL"

# astro -nl: No Log files will be added in $HOME/.gag/logs, even in case
#            of error or normal exit
# NB: if you want to avoid all logs, you can also consider redirecting STDOUT
#     to /dev/null...
nohup astro -nw -nl < wind-plot.astro > wind-plot.log 2>&1


### Post-processing ####################################################

rm -rf $tmpdir
