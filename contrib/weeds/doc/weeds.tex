\documentclass[a4paper,12pt]{article}
\usepackage{url,graphicx,fancyvrb,html}
\usepackage[hmargin=2.2cm,vmargin=2.5cm]{geometry}

\newcommand{\aapaper}{Maret, Hily-Blant, Pety et al., \emph{Astronomy
    \& Astrophysics} 526, A47 (2011)}

\makeindex{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\VerbatimFootnotes

\thispagestyle{empty}

\begin{latexonly}
  \vspace*{5cm}
\end{latexonly}

\noindent
\huge{\textbf{Weeds}}

\begin{latexonly}
  \vskip4pt \hrule height 4pt width \hsize
\end{latexonly}

\begin{flushright}
  \noindent
  \small{
    Documentation Manual\\
    June 9th, 2016
  }
\end{flushright}

\noindent
\large{\textbf{S\'ebastien Maret}\\
\small{Institut de Plan\'etologie et d'Astrophysique de Grenoble\\}

\noindent
\large{\textbf{Pierre Hily-Blant}}\\
\small{Institut de Plan\'etologie et d'Astrophysique de Grenoble\\}

\noindent
\large{\textbf{J{\'e}r{\^o}me Pety}}\\
\small{Institut de Radio-Astronomie Millim{\'e}trique}\\

\noindent
\large{\textbf{S{\'e}bastien Bardeau}}\\
\small{Institut de Radio-Astronomie Millim{\'e}trique}\\

\noindent
\large{\textbf{Emmanuel Reynier}}\\
\small{Institut de Radio-Astronomie Millim{\'e}trique}\\

\noindent
\large{\textbf{Mathieu Lonjaret}}\\
\small{Institut de Radio-Astronomie Millim{\'e}trique}\\

\noindent
\large{\textbf{Arnaud Belloche}}\\
\small{Max-Planck-Institut f\"ur Radioastronomie}\\

\begin{latexonly}
  \vskip4pt \hrule height 2pt width \hsize \vskip4pt
\end{latexonly}

\begin{rawhtml}
  Note: this is the on-line version of the WEEDS Manual.
  A <A HREF="../../pdf/weeds.pdf"> PDF version</A>
  is also available.
\end{rawhtml}

\begin{latexonly}
  \newpage
  \vspace*{22cm}
\end{latexonly}

\small{
  \noindent This manual documents Weeds, an extension of the CLASS radio 
  astronomy software to reduce and analyze spectral surveys or spectral
  line observation with large bandwidths. 
  Please report any errors in this manual to
  \url{gildas@iram.fr}. 
  Related information is available in
  \begin{latexonly}
    the CLASS manual.
  \end{latexonly}
  \begin{rawhtml}
    <UL>
    <LI> <A HREF="../class-html/class.html"> the CLASS manual</A>
    </UL>
  \end{rawhtml}
}

\newpage

\section{What is Weeds?}

Weeds is an extension of the CLASS radio astronomy software which is
intended to ease the analysis of spectral surveys or spectral lines
observations with large bandwidths. Weeds provides several commands to
identify lines on a spectrum using spectral catalogs, which are
accessed on-line using the VO-compliant SLAP protocol. In addition,
Weeds can perform a simple modeling of the observed spectra, under the
assumption of local thermodynamic equilibrium. Together with the
catalog query commands, the modeling tool allows to efficiently
identify lines in crowded spectra, where the line overlap can be
important.

Weeds development was inspired by other packages, such as XCLASS and
CASSIS. Both Weeds and XCLASS are extensions of the widely used CLASS
spectra reduction software, which is part of GILDAS; this allows to
reduce and analyze spectral surveys within the same
environment. However, unlike XCLASS, Weeds is distributed with GILDAS
as a contributed extension, and it uses the most recent CLASS version
(CLASS90). Another difference between Weeds and both XCLASS and CASSIS
is that Weeds accesses to spectral catalogs on-line. Consequently any
changes in these catalogs (e.g. line additions) are readily available
in Weeds.  Nevertheless, Weeds can make a local copy a spectral line
catalog, for offline searches. This is useful when you want to use
Weeds with no (or a slow) internet connection. All this makes Weeds a
lightweight, easy-to-install and efficient alternative to other
spectral line analysis packages.

Weeds is named after some species that are detected in the
interstellar medium, such as methanol, methyl formate or dimethyl
ether. Because of internal motions, these species have an extremely
rich line spectrum at millimeter and sub-millimeter wavelengths. Often,
in order to analyze a spectral survey, one needs to identify the weeds
first before picking-up the flowers (that is the lines of other
species of interest). This is precisely the purpose of this extension.

This document describes how to use Weeds to analyze a spectral
survey. It is written as a tutorial; each of step of the data analysis
is explained through examples.

\section{Getting started}

Weeds has been distributed with GILDAS since March 2010. Provided that
you have installed GILDAS with the Python extension (named
PyGILDAS)\footnote{Weeds requires Python version $\ge$ 2.6 or newer.}
you should have Weeds already installed on your computer. If this is
not the case, please refer to the GILDAS documentation for
instructions on how to install PyGILDAS.

Let us first start CLASS from a terminal:

\begin{Verbatim}[fontsize=\scriptsize]
% class
GILDAS Version: dev (06jun16 15:32) (x86_64-darwin-gfortran-openmp) executable tree

 * Welcome to CLASS   

 * Loaded modules
    atm
    sic (J.Pety, S.Bardeau, S.Guilloteau, E.Reynier)
    greg (J.Pety, S.Bardeau, S.Guilloteau, E.Reynier)
    ephem (F.Gueth, J.Pety)
    class (S.Bardeau, J.Pety, P.Hily-Blant, S.Guilloteau)

 * Loaded extensions
    weeds (S.Maret, P.Hily-Blant, J.Pety, S.Bardeau, E.Reynier)

(...)
LAS> 
\end{Verbatim}

\noindent
Note that the welcome message mentions that the Weeds extension as
been loaded. The extension defines a language, named
\verb#weeds\#. This language has a number of commands, the list of
which can be obtained by typing:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> help weeds\
 
    Weeds is a CLASS extension to analyze spectral surveys or spectral lines
    observations with large bandwidths.  It  provides  several  commands  to
    identify lines on a spectrum and to model it.
 
    Available commands:
        LID       Identify lines on the current spectra
        LFIND     Find lines from a species within a frequency range
        LLIST     List lines from the line index
        LGET      Get a line from the line index
        LPLOT     Plot a line from the current line index
        MODSOURCE Model the emission of a source at the LTE
        MODSHOW   Show the results of MODSOURCE
 
    For more information of each command, type 'help <command>'
\end{Verbatim}

\noindent
Each of the commands provided by Weeds has an extensive
documentation. Type \verb#help# followed by the command name to read
it.

\section{Selecting a spectral database}
\label{sec:select-spectr-datab}

The first step to analyze our spectra is to select a spectral line
database. Weeds can access both the JPL database for molecular
spectroscopy and the Cologne Database for Molecular Spectroscopy
(CDMS). To select a database, type \verb#use in#, followed by the name
of the database\footnote{The \verb#use# command, as well as the
  \verb#select# and \verb#insert# commands that we use later on, are
  part of the \verb#linedb\# language. Please type \verb#help linedb\#
  for the full documentation of this language.}. For example:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> use in cdms 
I-USE,  cdms (online) selected
\end{Verbatim}

\noindent
or:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> use in jpl 
I-USE,  jpl (online) selected
\end{Verbatim}

\noindent
As mentioned already, Weeds makes online queries to the molecular
databases. However, it is sometimes helpful to make offline searches in
a database, for example if you are on travel with no (or a slow)
internet connection. Weeds allows us to download a part of -- or even
the entire -- spectral catalog from a database. This catalog is stored
in a local database file, and can be used later for offline
searches. Making a local copy of the CDMS database between 80 and
130~GHz is done as follows:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> use in cdms   
I-USE,  cdms (online) selected
LAS90> use out mycdms.db
LAS90> select /freq 80e3 130e3
I-SELECT,  128957 lines found in the frequency range 80000.0 to 130000.0 MHz
LAS90> insert
\end{Verbatim}

Depending on the speed of your internet connection and the frequency
range you have selected, making a local copy of a database may take
from a few seconds to a few tens of minutes, so please be patient! In
particular, copying the JPL database may take a while, because of
technical limitations with this database; copying the CDMS database is
much faster.

Once you have made a local copy of a database, you may select it with
the \verb#use in# command, followed by the name of the local database
file (\verb#.db#):

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> use in mycdms.db
I-USE,  mycdms.db (offline) selected
\end{Verbatim}

The \verb#use in# and \verb#use out# commands can be used to build a
local database containing lines from different sources (e.g. the CDMS
and the JPL), and even lines from a private catalog. See the online
documentation for more information.

\section{Identifying lines in a spectrum}

\begin{figure}
  \centering
  \includegraphics[width=14cm]{weeds-f1}
  \caption{Example of a spectrum obtained with the IRAM-30m telescope
    towards the IRAS~16293 protostar. One of the methanol transition
    has been labeled with lid. \label{fig:methanol-lid}}
\end{figure}

Fig. \ref{fig:methanol-lid} shows a part of a spectral
survey\footnote{The spectrum used in this tutorial is included in the
  Gildas distribution (courtesy of Emmanuel Caux), and can be copied
  in the current directory with the following CLASS command:
  \verb#sic copy gag_demo:demo-weeds-1.30m demo-weeds-1.30m#.}. The
spectrum on the figure shows several well detected lines that we want
to identify. This can be done with \verb#lid# which allows us to
select a line on the spectrum with the mouse. Clicking on the left
button of the mouse over some part of the spectrum selects a line.
Then the command searches in the database (which has been selected
previously with the \verb#use in# command, as explained above) for
lines within 10 channels of the selected region(s) of the
spectrum\footnote{This frequency interval can be changed with the
  \verb#/delta# option.}. For each line found, the command marks the
rest frequency of the line on the spectrum, and a table displaying the
line frequency, upper level energy, Einstein coefficient and quantum
numbers and is printed:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lid
I-SELECT,  1 lines found in the frequency range 96737.8185473 to 96740.9437312 MHz
  # Species                 Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin
  1 CH3OH, vt=0,1           96739.362   0.005    12.5    5  2.56e-06     2 -1  2  0 -- 1 -1  1  0     cdms
\end{Verbatim}

Most of the time, several line candidates are found for a given
frequency. For example, using \verb#lid# to search for lines in the
catalog around the brightest line in Fig. \ref{fig:methanol-lid},
we obtain:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lid
I-SELECT,  4 lines found in the frequency range 96739.7756444 to 96742.9008283 MHz
  # Species                 Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin
  1 n-C3H7CN                96741.208   0.009   158.0   50  7.60e-06       12 310 1 -- 11 2 9 1       cdms
  2 gGG'g-1,3-Propanediol   96741.245   0.030    40.1   31  1.58e-05       15 213 1 -- 14 212 0       cdms
  3 C4H, v7=1               96741.314   0.092   214.2   44  3.74e-06         10 111 -- 9-110          cdms
  4 CH3OH, vt=0,1           96741.375   0.005     7.0    5  3.41e-06     2  0  2 +0 -- 1  0  1 +0     cdms
\end{Verbatim}

\noindent
Searches in the database may be refined to lines with an upper level
energy lower than a given value using the \verb#/energy# option, or to
lines with an Einstein coefficient greater than a given value with the
\verb#/aeinstein# option. The \verb#/species# option allows to restrict
searches to a given species.

It should be noted that \verb#lid# prints the line upper and lower
level quantum numbers exactly as they appear in the selected
database. We refer the user to the documentation of these databases
for the meaning of these quantum numbers.

\section{Using spectral line indexes}

Once we have identified a line from some species in our spectral
survey, we may want to search for other lines of that species at
different frequencies. For this, Weeds allows to create a line index,
which works in a similar way as CLASS scan indexes. Scan indexes are
created by the CLASS \verb#find# command and they are listed by the
\verb#list# command. A given scan from the current index can then be
loaded and plotted using the \verb#get# and \verb#plot#
commands. Likewise, line indexes are created with the \verb#lfind#
command, listed with the \verb#llist# command, and a given line may be
loaded and plotted using the \verb#lget# and \verb#lplot# commands.

For example, let's assume that we have identified a methanol line on
the spectrum shown on Fig.~\ref{fig:methanol-lid}, and that we want to
see if other methanol lines are present in the spectrum (which covers
frequencies between 90 and 100~GHz). The methanol line index is built with:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lfind "CH3OH, vt=0,1"
I-SELECT,  36 lines found in the frequency range 89999.6813201 to 100000.269737 MHz
\end{Verbatim}

\noindent
and then listed with:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> llist
  # Species         Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin
  1 CH3OH, vt=0,1   90295.864   1.754  1234.0   47  1.12e-08    23 +8 15  1 -- 23 +6 17  1    cdms
  2 CH3OH, vt=0,1   90812.387   0.232   808.3   41  2.81e-06    20 -3 17  1 -- 19 -2 17  1    cdms
  3 CH3OH, vt=0,1   91254.686   0.010   514.3   41  5.31e-08    20 -2 19  0 -- 20 +2 18  0    cdms
  4 CH3OH, vt=0,1   92409.579   0.019    44.3    9  5.10e-10     4 +1  3  0 -- 3 -2  2  0     cdms
(...)
 36 CH3OH, vt=0,1   99776.775   0.649   902.3   41  4.98e-06    20 +3 18  1 -- 21 +4 18  1    cdms
\end{Verbatim}
 
\noindent
The command prints all methanol lines between 90 and 100 GHz, ordered
by increasing rest frequencies. It also builds an internal index
containing all these lines. Note that the species name must be typed
exactly as it appears in the database and between double quotes, i.e.
\verb#"CH3OH, vt=0,1"# in this case.

Like for scans, each line is associated with an entry number, that we
can use to load and to plot the line. Since, by default, lines are
ordered by frequency, subsequent entry numbers will correspond to
lines with increasing frequencies. However, if we search for lines of
a given species, it is usually a good idea to look for lines with the
lowest upper energy levels, because we expect them to be brighter than
higher lying lines. For this, we can re-order the lines by increasing
upper level energies using the \verb#/sortby e# option:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lfind "CH3OH, vt=0,1" /sortby e
I-SELECT,  36 lines found in the frequency range 89999.6813201 to 100000.269737 MHz
LAS90> llist
  # Species         Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin
  1 CH3OH, vt=0,1   96741.375   0.005     7.0    5  3.41e-06     2  0  2 +0 -- 1  0  1 +0     cdms
  2 CH3OH, vt=0,1   96739.362   0.005    12.5    5  2.56e-06     2 -1  2  0 -- 1 -1  1  0     cdms
  3 CH3OH, vt=0,1   96744.550   0.005    20.1    5  3.41e-06     2 +0  2  0 -- 1 +0  1  0     cdms
  4 CH3OH, vt=0,1   95914.309   0.005    21.4    5  2.49e-06     2  1  2 +0 -- 1  1  1 +0     cdms
(...)
 36 CH3OH, vt=0,1   95523.388   1.982  1289.5   49  1.74e-08    24 +8 16  1 -- 24 +6 18  1    cdms
\end{Verbatim}

Since we have already identified the first three lines in the index,
let's have a look at another one:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lget 4
I-LGET,  Found line frequency in the current scan
  # Species         Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin
  1 CH3OH, vt=0,1   95914.309   0.005    21.4    5  2.49e-06     2  1  2 +0 -- 1  1  1 +0     cdms
\end{Verbatim}

\noindent
The \verb#lget# command modifies the scan frequency so that the
velocity axis is centered at the line rest frequency. We can then use:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lplot
\end{Verbatim}

\noindent
which gives the spectrum shown on Fig.~\ref{fig:methanol-lplot}.

\begin{figure}
  \centering
  \includegraphics[width=14cm]{weeds-f2}
  \caption{Line displayed with lplot command. The blue vertical
    line shows the rest frequency of the line. The upper x-axis shows
    the velocity offset from the line rest
    frequency.\label{fig:methanol-lplot}}
\end{figure}

The \verb#lget# and \verb#lplot# commands allow to quickly
``navigate'' in a spectral survey to look for the different lines of a
given species. \verb#lget f# will load the first line of the index,
then \verb#lget n# will get the next one. \verb#lget p# will get the
previous one, etc. If you get lost at some point, you can always type
\verb#llist# to display the line index again.

\section{Modeling a spectrum}

Experience shows that in order to identify a species in a spectral
survey securely, it is often necessary to make a basic model of the
candidate line. For example, you may want to check if the relative
intensities of each candidate line imply a reasonable kinetic
temperature. You may also want to check that other lines with similar
predicted intensity are also detected.

Weeds allows to compute the emission of a source under the assumption
of local thermodynamical equilibrium. The source is assumed to have,
for each species, a given column density, excitation temperature, line
FWHM, systemic velocity and size. Several components (with e.g.
different temperatures and/or sizes) can be added. Weeds will compute
the emission of these various components -- taking into account the
line opacity and the beam dilution factor -- and it will display them
on the observed spectrum. The different source parameters can be
adjusted until a good match between the model and the observation is
obtained. More details on the formulas used can be found in
\S~\ref{modsource-details}.

Let us go back to the methanol lines that we have identified in our
spectrum (see Fig.~\ref{fig:methanol-lplot}). The emission can be
modeled using the \verb#modsource# command, which takes two arguments:
the name of the file containing the source parameters, and the size of
the antenna that we have used for the observations, in meters. To
model the emission, we need to use the JPL database, because the CDMS
does not provide the partition function for this species:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> use in jpl 
I-USE,  jpl (online) selected
\end{Verbatim}

\noindent
Our model file, that we have named \verb#iras16293.mdl#, looks like
this:

\begin{Verbatim}[fontsize=\scriptsize]
! species	Ntot	Tex  source_size v_off	width
! 		(cm-2)	(K)	('')	 (km/s)	(km/s)	
CH3OH		2.0e15	10	10	 0	 3.0
\end{Verbatim}

\noindent
Lines that start with a \verb|!| are comments: they are ignored. The
last line gives the name of the species, its column density, excitation
temperature, the size of the emission in arc seconds, the offset
velocity (with respect to the source velocity in the class file
header), and the line width. The spectrum, as it would be observed
with the IRAM-30m antenna, can be computed with:

\begin{Verbatim}[fontsize=\scriptsize]
LAS> modsource iras16293.mdl 30
I-SELECT,  4 lines found in the frequency range 96710.0 to 96770.0 MHz
I-MODSOURCE,   4 CH3OH lines found in the frequency range
I-MODSOURCE,  log10 of the partition function at 10.0 K from jpl is 1.3419
  # Species        Freq[MHz] Err[MHz] Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin Tau
  1 CH3OH          96739.358   0.002    12.5    5  2.56e-06        2-1   0 -- 1-1   0           jpl 7.25e-01
  2 CH3OH          96741.371   0.002     7.0    5  3.41e-06        2 0 + 0 -- 1 0 + 0           jpl 1.69e+00
  3 CH3OH          96744.545   0.002    20.1    5  3.41e-06        2 0   0 -- 1 0   0           jpl 4.56e-01
  4 CH3OH          96755.501   0.002    28.0    5  2.62e-06        2 1   0 -- 1 1   0           jpl 1.59e-01
I-MODEL,   Blanking value:   -1000.00000
I-RESAMPLE,  Frequency resolution: .31252 MHz (observatory), .31252 MHz (rest frame)
I-MODSOURCE,  Model has been stored in memory
\end{Verbatim}

\noindent
For efficiency reasons, the command computes the spectrum over the
frequency range covered by the current window only. In our case, four
methanol lines are found in the frequency range.  Note the
\verb#/verbose# option, that prints the frequency, upper level energy
and statistical weight, Einstein coefficient and computed opacity at
the line center. The \verb#modsource# command itself does not plot
anything; it just stores the observed and modeled spectrum into
buffers. These buffers can be listed with:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> memorize
I-MEMORIZE,  Current memories:
OBS           TB_MODEL
\end{Verbatim}

\noindent
The \verb#TB_MODEL# buffer contains the modeled brightness
temperature. The buffer can be retrieved with the \verb#retrieve#
command. The \verb#OBS# buffer contains the observed spectrum, which is
saved automatically by \verb#modsource#.

The predicted spectrum can be drawn over the observed spectrum using
the \verb#modshow# command\footnote{Alternatively, one may retrieve
  the predicted spectrum with \verb#retrieve TB_MODEL#, and plot it
  over the observed spectrum with \verb#spectrum#. However, it is
  important to fix the y-axis range; if the axis is set to \verb#auto#,
  then the predicted and modeled spectrum will not be on the same
  scale, and the comparison will be meaningless. The \verb#modshow#
  command ensures that both the observed and predicted spectra are on
  the same scale.}. This command gives the spectra shown on
Fig.~\ref{fig:methanol-modshow}. As it can be seen on this figure, our
model is in quite good agreement with the observed spectrum.

\begin{figure}
  \centering
  \includegraphics[width=14cm]{weeds-f3}
  \caption{Observed (black histogram) and predicted spectra (in red)
    displayed with the modshow
    command. \label{fig:methanol-modshow}}
\end{figure}

Line indexes can be used together with \verb#modshow# to examine other
lines between 90 and 100~GHz, and to check whether our model can also
reproduce them. For example:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> lfind "CH3OH" 90e3 100e3 /sortby e
I-SELECT,  142 lines found in the frequency range 90000.0 to 100000.0 MHz
LAS90> lget 4
LAS90> lplot
LAS90> modsource iras16293.mdl 30 
I-SELECT,  1 lines found in the frequency range 95898.6840807 to 95929.9359193 MHz
I-MODSOURCE: 1 CH3OH lines found in the frequency range
I-MODEL,   Blanking value:   -1000.00000
I-RESAMPLE,  Frequency resolution: .31252 MHz (observatory), .31252 MHz (rest frame)
I-MODSOURCE, Model has been stored in memory
LAS90> modshow
\end{Verbatim}

\noindent
Note that we must use the \verb#modsource# command again, because the
command computes a spectrum only over the frequency range covered by
the current scan. We must also rebuild the line index after each
\verb#modsource#, because the commands builds its own line index,
containing only lines in the frequency range covered by the current
window. Scripts can be easily created to loop over line index and
examine each of the observed and predicted lines.

\section{More information on modsource}

\verb#modsource# computes the spectrum emitted by a uniform source. The 
spectrum consists of continuum emission and spectral line emission. The line 
emission is computed under the assumption of local thermodynamic equilibrium 
(LTE). \verb#modsource# takes into account the optical depth of the lines, the finite 
angular resolution of the telescope, and the presence of the cosmic microwave 
background (CMB) to compute the radiative transfer. Depending on the continuum 
and line excitation temperatures, the lines can appear in emission or in 
absorption in the synthetic spectrum. Several molecules and/or components can 
be modeled at the same time. The spectra of these components are computed 
separately, and then added linearly. It is a correct treatment for components 
that do not overlap spectrally or spatially within the beam, or for components 
with optically thin emission. However, such a treatment is not correct in the 
case of spatially-overlapping components with optically thick emission. In 
such cases, the intensities of the lines will be overestimated.

The components belong by default to the ``main group'' of components. The
components marked with the option \verb#/absorption# belong to the ``foreground'' 
group of components. For the ``main group'' components, the background used to 
compute the radiative transfer is the sum of the CMB and the source continuum 
emission. For the ``foreground'' components, the spectrum is computed 
recursively and the background initially consists of the spectrum of the 
``main group'' (that already contains the CMB and the source continuum).

The source continuum emission is assumed to be optically thin (i.e. 
transparent to the CMB) and spatially uniform, to fill the beam of the 
single-dish telescope or the synthesized beam of the interferometer, and to 
come from a location behind the medium that contains the molecules modeled 
with modsource (i.e. lines and continuum are not mixed). The assumption of a 
uniform continuum emission filling the beam will produce inacurrate results in 
the case of a centrally peaked continuum emission and a molecule component 
with a beam filling factor smaller than 1. In such a case, the molecule should 
``see'' a stronger continuum than what modsource assumes. Given that different 
components may have different beam filling factors, properly taking into 
account the small-scale structure of the continuum emission would not be 
trivial and is beyond the scope of \verb#modsource#.

Let's call $T_{{\rm cont},\,\nu}$ the effective radiation temperature of the 
source continuum emission. It corresponds to the level of the ``baseline'' in 
the spectrum if this ``baseline'' represents the true continuum level of the 
source (case of, e.g., interferometric or some single-dish wobbler spectra), 
or the user has to assume its value if the baseline cannot be trusted and has 
been removed (case of, e.g., single-dish position-switching spectra). 
$T_{{\rm cont},\,\nu}$ can be a frequency-dependent function. 

For a component with a beam filling factor of 1, the brightness 
temperature of the spectrum is computed as:

\begin{equation}
  \label{eq:tb}
  T_{\rm B}(\nu) = J_\nu(T_{\rm ex}) (1-e^{-\tau_\nu}) + T_{{\rm cont},\,\nu} e^{-\tau_\nu} + J_\nu(T_{\rm cmb}) e^{-\tau_\nu} - J_\nu(T_{\rm cmb})\,\,,
\end{equation}

\noindent
where $\tau_\nu$ is the opacity of the transition, $T_{\rm ex}$ the excitation 
temperature of the transition, $T_{\rm cmb}$ the temperature of the CMB, and 
$J_\nu(T) = \frac{h\nu/k}{e^{h\nu/kT}-1}$,
with $h$ the Planck constant, and $k$ the Boltzmann constant.
The last term in Eq.~\ref{eq:tb} represents the subtraction of the spectrum of
the off-source position in the case of single-dish observations or the spatial 
filtering of the uniform CMB radiation in the case of interferometric 
observations. This equation can be rewritten as:

\begin{equation}
  \label{eq:tb2}
  T_{\rm B}(\nu) = T_{{\rm cont},\,\nu} + \left(J_\nu(T_{\rm ex})-J_\nu(T_{\rm cmb})-T_{{\rm cont},\,\nu}\right) (1-e^{-\tau_\nu})
\end{equation}

\noindent
For a component with an arbitrary beam filling factor, $\eta$, modsource 
computes the spectrum as:

\begin{equation}
  \label{eq:tb3}
  T_{\rm B}(\nu) = T_{{\rm cont},\,\nu} + \eta\left(J_\nu(T_{\rm ex})-J_\nu(T_{\rm cmb})-T_{{\rm cont},\,\nu}\right) (1-e^{-\tau_\nu})
\end{equation}

\noindent
For $N_1$ components in the ``main group'' (mg), the output spectrum is:

\begin{equation}
  \label{eq:tb4}
  T_{\rm B}^{\rm mg}(\nu) = T_{{\rm cont},\,\nu} + \sum_{i=1}^{N_1}\eta_i\left(J_\nu(T_{{\rm ex},i})-J_\nu(T_{\rm cmb})-T_{{\rm cont},\,\nu}\right) (1-e^{-\tau_{\nu,i}})
\end{equation}

\noindent
For $N_2$ additional components in the ``foreground group'', the output 
spectrum is computed with the following recursive equation:

\begin{equation}
  \label{eq:tb5}
  T_{\rm B}^j(\nu) = T_{\rm B}^{j-1}(\nu)e^{-\tau_{\nu,j}} + \eta_j\left(J_\nu(T_{{\rm ex},j})-J_\nu(T_{\rm cmb})\right) (1-e^{-\tau_{\nu,j}})
\end{equation}

\noindent
with $j$ varying from 1 to $N_2$ and $T_{\rm B}^0(\nu) = T_{\rm B}^{\rm mg}(\nu)$.
Note that this equation is correct only if all foreground components have a 
beam filling factor, $\eta_j$, of 1.

By default, the final output spectrum is 

\begin{equation}
  \label{eq:tb6}
  T_{\rm B}(\nu) = T_{\rm B}^{N_2}(\nu)-T_{{\rm cont},\,\nu}
\end{equation}

\noindent
or, if there is no ``foreground group'' component:

\begin{equation}
  \label{eq:tb7}
  T_{\rm B}(\nu) = T_{\rm B}^{\rm mg}(\nu)-T_{{\rm cont},\,\nu}
\end{equation}

\noindent
If the user selects the option \verb#/keepcont#, then the output spectrum is 
$T_{\rm B}(\nu) = T_{\rm B}^{N_2}(\nu)$ in the first case and 
$T_{\rm B}(\nu) = T_{\rm B}^{\rm mg}(\nu)$ in the second case. More details about
modsource can be found in \aapaper{}.

\section{For more information}

This documentation manual covers the basic usage of the Weeds
extension. More detailed information on each command is available
using the Gildas online help, e.g.:

\begin{Verbatim}[fontsize=\scriptsize]
LAS90> help modsource
WEEDS\MODSOURCE = "python weeds/modsource.py"
        [WEEDS\]MODSOURCE  MODFILE  ANTSIZE  [/TCONT  TCONT]  [/OPACITY   I]
    [/KEEPCONT] [/VERBOSE]
 
    Model  the emission of a source assuming local thermodynamic equilibrium
    (LTE). The model paramaters (column density, kinetic temperature, source
    size,  line  width  and  velocity offset for each species) are read from
    MODFILE. The modeled brightness temperature is stored  in  the  TB_MODEL
    memory. For convenience the current spectrum is also stored in a memory,
    OBS, so one can easily overplot the result of the model on the  observed
    spectrum.  See the documentation manual for more details on the computa-
    tion.
 
    Arguments:
      MODFILE     the name of the file containing the parameters.  The  file
                  must  have  one line per species with at least six columns
                  containing respectively:
 
                  1) The species name
                  2) The column density for this species in cm-2
                  3) The excitation temperature in K
                  4) The source size in arcsec
                  5) The offset velocity from the source VLSR, in km/s
                  6) The line FWHM in km/s
 
                  One additional column may be used to  select  a  different
                  origin for each species:
 
                  7)  The origin of this species (e.g. JPL or CDMS). If emp-
                  ty, species with any origin are selected.
 
                  The following options may be used at the end of the line:
 
                  /ABSORPTION  If this option is set, the component  is  as-
                  sumed to be a foreground layer.
 
                  /PARTFUNC  Q   Set  the log10 of the partition function at
                  the excitation temperature to Q. If not set, the partition
                  function  values (log-log interpolated/extrapolated at the
                  excitation temperature) from  the  selected  database  are
                  used.      
 
                  Lines that start with a "!" are treated as comments.
 
      ANTSIZE     the antenna size in meters
 
    Options:
      /TCONT      set the continuum level to TCONT, in K. Default is 0.
 
      /OPACITY  I  store  the  opacity of the Ith component in the TAU_MODEL
                  memory. Components are numbered in the same  order  as  in
                  MODFILE, starting from 1.
 
      /KEEPCONT   do not remove the continuum from the final spectra.
 
      /VERBOSE    print  partition  function,  line frequencies, upper level
                  energies and degeneracies, Einstein coefficients and opac-
                  ities at the line centers.
\end{Verbatim} 

For a more detailed description of the Weeds extension, we refer the
interested reader to \aapaper . This paper describes the
implementation of the extension, and gives more details about the
spectroscopic databases that it uses, and explains how spectra are
modeled.

Weeds developers can be contacted by email at
\url{gildas@iram.fr}. Bug reports should also be sent to this address.

\newpage
\appendix

\section{List of contributing authors}

The Weeds project started on mid-2009 at Laboratoire d'Astrophysique
de Grenoble (now known as Institut de Plan\'etologie et
d'Astrophysique de Grenoble). Soon after Institut de Radio Astronomie
Millim\'etrique started to support its development. 

The following authors have contributed to the project:

\begin{itemize}
\item S{\'e}bastien Maret (Institut de Plan\'etologie et
  d'Astrophysique de Grenoble)
\item Pierre Hily-Blant (Institut de Plan\'etologie et
  d'Astrophysique de Grenoble)
\item J{\'e}r{\^o}me Pety (Institut de Radio Astronomie Millim\'etrique)    
\item S{\'e}bastien Bardeau (Institut de Radio Astronomie Millim\'etrique)
\item Emmanuel Reynier (Institut de Radio Astronomie
  Millim\'etrique)
\item Mathieu Lonjaret (Institut de Radio Astronomie Millim\'etrique)
\item Arnaud Belloche (Max-Planck-Institut f\"ur Radioastronomie)
\end{itemize}

Weeds relies on PyGILDAS, a Python binding for GILDAS developed by
S{\'e}bastien Bardeau, J{\'e}r{\^o}me Pety, Emmanuel Reynier (Institut
de Radio Astronomie Millim\'etrique) and St{\'e}phane Guilloteau
(Observatoire de Bordeaux).

\section{Acknowledging Weeds developers and spectral line databases in
  your publications}

If you use Weeds in your research work, please cite \aapaper \, in
your publications. Please also acknowledge the use of the CDMS and JPL
databases by citing the following papers:

\begin{itemize}

\item H. M. Pickett, R. L. Poynter, E. A. Cohen, M. L. Delitsky,
  J. C. Pearson, and H. S. P. M{\"u}ller, ''Submillimeter, Millimeter, and
  Microwave Spectral Line Catalog'', J. Quant. Spectrosc. \& Rad. Transfer
  60, 883-890 (1998).

\item H. S. P. M{\" u}ller, F. Schl{\" o}der, J. Stutzki, and
  G. Winnewisser, ``The Cologne Database for Molecular Spectroscopy,
  CDMS: a useful tool for astronomers and spectroscopists''
  J. Mol. Struct. 742, 215–227 (2005)

\end{itemize}  

\section{List of major changes}

\subsection{April 1st, 2016 release}

The previous versions of modsource did not have a proper
implementation of the background emission in the cases where both the
cosmic microwave background (CMB) and another source of continuum
emission are present. The new version deals with such a situation in
an improved way. In addition, the parameter defining the source
continuum emission now corresponds to the level of continuum emission
that can be measured in the spectrum of the source. In the previous
versions, the continuum was assumed to be a blackbody and the
parameter was defined as the temperature of this blackbody, i.e. it
was not directly related to the level of continuum emission seen in
the spectrum. The new behaviour is particularly convenient for
interferometric observations, space observations, or in some cases
single-dish wobbler observations that usually deliver spectra with
reliable levels of continuum emission. Finally, the user now has the
possibility to define a frequency-dependent source continuum emission
by providing an array of continuum temperatures with the same size as
the spectrum. By default, modsource still produces a baseline-removed
synthetic spectrum. A new option has now been added to produce a
synthetic spectrum that includes the source continuum emission.

\subsection{March 1st, 2014 release}

Weeds used to rely on a Python library, \verb#linedb#, for querying
spectral line databases. This library has been developed as part of
Weeds, but it has been realized that it could be useful in other
GILDAS programs as well. Therefore it has been moved to the GILDAS
kernel, after some important modifications to make it more generic and
versatile. It is now a separate language (see \verb#help linedb\# for
more information). As a consequence, a couple of Weeds commands have
been modified:

\begin{itemize}
  \item The \verb#dbselect# has been removed. The database selection
    is now made with the \verb#use in# command, which is part of the
    \verb#linedb\# language.
  \item The  \verb#dbcache# command has also been removed. Users can
    now build a local copy of a database with a combination of 
    \verb#use in#,  \verb#use out#, \verb#select# and
    \verb#insert#. See section~\ref{sec:select-spectr-datab} for an
    example of this.
\end{itemize}

In addition, the following changes have been implemented in this
release:

\begin{itemize}
  \item \verb#modsource# now allows to model sources with one or
    several foreground absorbing layers. This is set with the
    \verb#/absorption# option in the model file. See the
    \verb#modsource# command help for more information.
  \item A bug in the interpolation of partition functions as a
    function of the temperature in \verb#modsource# has been
    corrected. The interpolation is now done in log-log.
  \item \verb#lid# now prints the uncertainty on the line frequencies
\end{itemize}

\newpage{}

\section{WEEDS Language Internal Help}
\input{weeds-help}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
