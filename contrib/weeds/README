Weeds
=====

What is Weeds?
--------------

Weeds is a package for the Gildas radioastronomy software which is
intented to ease the data reduction of spectral surveys or spectral
lines observations with large bandwidths. Weeds provides a command to
identify lines on a spectrum using VO-compliant spectrocopic
databases, which are accessed on-line using the SLAP protocol. In
addition, Weeds provides a command to perform a simple modelling of
the observed spectra, under the assumption of local thermodynamic
equilibrium. Together with the catalog query command, the modelling
tool allows to efficiently identify line on crowded spectrum, where
the line overlap can be important. Weeds development was inspired by
other packages, such as XCLASS and CASSIS. Both Weeds and XCLASS are
extensions of the widely used CLASS spectra reduction software, which
is part of GILDAS; this allows to reduce and analyze spectral surveys
within the same environment. However, unlike XCLASS, Weeds is
distributed with GILDAS as a contributed package, and it uses the most
recent CLASS version (CLASS90). Another difference between Weeds and
both XCLASS and CASSIS is that Weeds access to spectral catalogs
on-line. Consequently any changes in these catalogs (e.g. line
additions) are readily available in Weeds. Nevertheless, Weeds can
make a local copy of a spectral line catalog, for offline
searches. This is useful when you want to use Weeds with no (or a
slow) internet connection. All this make Weeds a lightweight,
easy-to-install and efficient alternative to other spectral line
analysis packages.

Installation
------------

Weeds is included in the standard Gildas distribution. It is installed
automatically if you have Python 2.6 and Numpy installed on your
computer.

Documentation
-------------

Weeds comes with a documentation manual. The LaTeX source of the
manual can be found in the doc/ directory.

Reporting bugs
--------------

A list of known bugs can be found in the BUGS file. If you find a bug
which is not listed in these files please report it to
<gildas@iram.fr>

