      SUBROUTINE DIAGSOLVE(ALPHA,ND,MD,BETA,LISTA,ELNAME,QUIET,
     +     WARNING,ERROR)
C------------------------------------------------------------------------
C Solve a symetric linear system through computation of its eigenvalues
C and eigenvectors. Uses LAPACK library routines.
C----------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
*
      INTEGER ND   ! Size of the linear system
      INTEGER MD   ! Leading matrix dimension
      LOGICAL QUIET,WARNING,ERROR
      REAL ALPHA(MD,MD),BETA(ND)
      INTEGER LISTA(ND)  ! True numbers of adjustable parameters (for messages)
      CHARACTER*(*) ELNAME(MA)
*
*
      REAL WORK(MA,MA)   ! Work array for eigenvalue determination
      REAL EIGENVAL(MA)  ! Eigenvalues of the covariance matrix
      REAL B(MA)
!      real*8 dalpha(MA,MA)
!      REAL*8 DWORK(MA,MA)   ! Work array for eigenvalue determination
!      REAL*8 DEIGENVAL(MA)  ! Eigenvalues of the covariance matrix
      REAL SCALE(MA)  ! Square root of normal matrix diagonal
      REAL X(MA)
      REAL SCL,NORM
      INTEGER IEL(MA),NEL
      INTEGER I,J,K,INFO
      CHARACTER CHAIN*512
      INTEGER LC
*
      WARNING = .FALSE.
      ERROR = .FALSE.


!      write(6,*) 'DIAGSOLVE: Matrix is'
!      do i=1,nd
!         write(6,'(1X,20(1X,G10.2))') (alpha(i,j),j=1,nd)
!      enddo
*
* Precondition the normal matrix by sqrt(aii)*sqrt(ajj) (and second member
* by sqrt(aii)). This is important for numerical stability when some
* parameters are much better constrained than others. A typical case happen
* in very close binaries observed over many cycles. The period can be 
* determined to 1e-5 (or better) relative accuracy, while other parameters
* still have the usual (lower) accuracy.
!      write(6,*) 'Diagonal elements:'
!      write(6,*) (ALPHA(I,I),I=1,ND)
      DO I=1,ND
         IF (ALPHA(I,I).GT.0) THEN
            SCALE(I) = 1./SQRT(ALPHA(I,I))
            IF (SCALE(I).EQ.0) THEN
               IF (.NOT.QUIET) WRITE(6,*) 
     +              'W-FIT, Infinite diagonal element in',
     +              ' covariance matrix'
            ENDIF
         ELSE
            IF (ALPHA(I,I).EQ.0) THEN
               IF (.NOT.QUIET) WRITE(6,*) 
     +              'W-FIT, Zero diagonal element in',
     +              ' covariance matrix'
            ELSE
               IF (.NOT.QUIET) WRITE(6,*) 
     +              'W-FIT, Negative diagonal element in',
     +              ' covariance matrix'
            ENDIF
            SCALE(I) = 0.
* This is clearly not a desirable attribute for a solution. Proceed, but
* warn the calling program.
            ERROR = .TRUE.   
         ENDIF
      ENDDO
!      write(6,*) 'Preconditioning factors are:'
!      write(6,*) (SCALE(I),I=1,ND)
      DO J=1,ND
         DO I=1,ND
            ALPHA(I,J)=ALPHA(I,J)*SCALE(I)*SCALE(J)
         ENDDO
         BETA(J)=BETA(J)*SCALE(J)
      ENDDO
* Determine all eigenvalues and vectors of the preconditioned normal
* matrix (S=Single, SY=SYmetric, EV=EigenValues/Vectors)
!      write(6,*) 'Preconditioned Normal Matrix'
!      do i=1,nd
!         write(6,'(1X,20(1X,G10.2))') (alpha(i,j),j=1,nd)
!         write(6,*) (alpha(i,j),j=1,nd)
!      enddo
!         SSYEV( JOBZ, UPLO, N, A, LDA, W, WORK, LWORK, INFO )
      CALL SSYEV  ('V', 'U',ND,ALPHA,MD,EIGENVAL,
     +     WORK,MA*MA,INFO)

      IF (INFO.NE.0) THEN
         IF (INFO.LT.0) THEN
            IF (.NOT.QUIET) WRITE(6,*) 
     +           'E-SSYEV, Argument ',INFO,
     +           ' has an incorrect value'
         ELSEIF (INFO.GT.0) THEN
            IF (.NOT.QUIET) WRITE(6,*) 
     +           'E-SSYEV, Only ',INFO,
     +           ' eigenvalues could be determined'
         ENDIF
         ERROR = .TRUE.
         RETURN
      ENDIF
      IF (WORK(1,1).GT.MA*MA) THEN
         IF (.NOT.QUIET) write(6,*) 
     +        'Work array size of ',MA*MA,
     +        ' was smaller than the optimal',work(1,1)
      ENDIF
* Pseudo-invert the diagonalised matrix
!      write(6,*) 'Eigenvalues are ',(eigenval(i),i=1,nd)
      DO I=1,ND
         IF (EIGENVAL(I).GE.1E-3) THEN  ! No big problem, invert
            EIGENVAL(I)=1./EIGENVAL(I)
         ELSE   ! Marginal at best: build up an explicit description of 
                ! the eigenvector for display. Components with marginal 
                ! contributions are pruned out for clarity
            NORM = 0
            DO J=1,ND
               SCL = 1.
               IF (SCALE(J).NE.0) SCL=SCALE(J)
               X(J) = ALPHA(J,I)*SCL
               NORM = NORM+X(J)**2
            ENDDO
            NORM = SQRT(NORM)
            NEL =  0
            DO J=1,ND
               X(J)=X(J)/NORM
               IF (ABS(X(J)).GE.(0.1/ND)) THEN
                  NEL = NEL +1
                  IEL(NEL) = J
               ENDIF
            ENDDO
            IF (.NOT.QUIET) THEN
               LC = LEN(CHAIN)
               WRITE(CHAIN,100,ERR=90) 
     +              (X(IEL(J)),ELNAME(LISTA(IEL(J))),J=1,
     +              MIN(NEL,LC/(1+6+1+LEN(ELNAME(1)))))
 100           FORMAT(100(SP,F6.3,'*',A))
               CALL BLANC(CHAIN,LC)
 90            CONTINUE
            ENDIF
* Low but still some significance left. (Not sure this is quite the right
* test!)
            IF (EIGENVAL(I).GE.3E-5) THEN 
               IF (.NOT.QUIET) THEN
                  WRITE(6,101) 'W-FIT  , ',
     +                 CHAIN(1:MAX(1,LC)),
     +                 ' is weakly constrained: ',EIGENVAL(I)
               ENDIF
               EIGENVAL(I)=1./EIGENVAL(I)
               WARNING = .TRUE.
* Zero within numerical accuracy
            ELSE             
               IF (.NOT.QUIET) THEN
                  WRITE(6,101) 'W-FIT  ,',CHAIN(1:MAX(1,LC)),
     +                 ' is unconstrained: ', EIGENVAL(I)
 101              FORMAT(A,A,A,G9.2)
               ENDIF
               EIGENVAL(I)=0
               ERROR = .TRUE.
            ENDIF
         ENDIF
      ENDDO
*
* Project BETA onto the orthogonal eigenvector basis 
      DO I=1,ND
         B(I) = 0
         DO J=1,ND
            B(I)=B(I)+ALPHA(J,I)*BETA(J)
         ENDDO
      ENDDO
* Apply pseudo-inverse in the diagonal basis
      DO I=1,ND
         B(I)=B(I)*EIGENVAL(I)
      ENDDO
* Reproject onto the initial basis
      DO I=1,ND
         BETA(I) = 0
         DO J=1,ND
            BETA(I)=BETA(I)+ALPHA(I,J)*B(J)
         ENDDO
         BETA(I) = BETA(I)*SCALE(I)
      ENDDO
* Compute inverse matrix (could use symetry for a factor of 2 speedup,
* or look for a BLAS library routine combination that does that)
      DO I=1,ND
         DO J=1,ND
            WORK(I,J)=ALPHA(I,J)
         ENDDO
      ENDDO
      DO I=1,ND
         DO J=1,ND
            ALPHA(I,J)=0
            DO K=1,ND
               ALPHA(I,J)=ALPHA(I,J)+
     +              WORK(I,K)*SCALE(I)*EIGENVAL(K)*WORK(J,K)*SCALE(J)
            ENDDO
         ENDDO
      ENDDO
      END

