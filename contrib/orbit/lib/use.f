      SUBROUTINE USE_VR(NFIT,LIST,EL,ALPHA,BETA,CHI2,
     +     SD,ND,CVR,CODE,TIME,VR,S_VR,OC_VR,PHASE,
     +     QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the linearised normal equation system, using constraints
C from one radial velocity.
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      REAL*8 TIME,VR,S_VR,OC_VR,PHASE
      REAL*8 EL(MA)
      CHARACTER*(*) CVR
      INTEGER NFIT,LIST(*)
      INTEGER CODE 
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL SD(NDTYPE),CHI2(NDTYPE)
      INTEGER ND(NDTYPE)
      LOGICAL QUIET,ERROR
*
      REAL S,DY,SIG2I
      REAL V,PH(MORBIT)
      INTEGER J
      REAL B(MA)            ! Gradient of data relative to elements
*
      IF (CVR.EQ.'2') THEN
         CALL SET_TIME(NORB0,LISTORB0,TIME,PH)
         J=TYPE_V2
         CALL GRAD_V2(CODE,V,B)
      ELSEIF (CVR.EQ.'1') THEN
         CALL SET_TIME(NORB0,LISTORB0,TIME,PH)
         J=TYPE_V1
         CALL GRAD_V1(CODE,V,B)
      ELSEIF (CVR.EQ.'11') THEN
         CALL SET_TIME(NORB1,LISTORB1,TIME,PH)
         J=TYPE_V11
         CALL GRAD_V11(CODE,V,B)
      ELSEIF (CVR.EQ.'12') THEN
         CALL SET_TIME(NORB1,LISTORB1,TIME,PH)
         J=TYPE_V12
         CALL GRAD_V12(CODE,V,B)
      ELSEIF (CVR.EQ.'21') THEN
         CALL SET_TIME(NORB2,LISTORB2,TIME,PH)
         J=TYPE_V21
         CALL GRAD_V21(CODE,V,B)
      ELSEIF (CVR.EQ.'22') THEN
         CALL SET_TIME(NORB2,LISTORB2,TIME,PH)
         J=TYPE_V22
         CALL GRAD_V22(CODE,V,B)
      ELSE
         WRITE(6,*) 'E-FIT,  Unsupported velocity type: V',CVR
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      PHASE = PH(1)
      S=VR-V
      SD(J)=SD(J)+S*S
      ND(J)=ND(J)+1
      OC_VR=S
      IF (OC_VR.NE.OC_VR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'Radial velocity O-C for ',
     +           'velocity is incorrect ',OC_VR
            write(6,*) 'Estimated Velocity ',V
            write(6,*) 'Elements:',EL
         ENDIF
         ERROR = .TRUE.
         RETURN
      ENDIF
      IF (S_VR.EQ.0.OR. S_VR.NE.S_VR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'Incorrect standard error for ',
     +           'velocity ',S_VR
         ENDIF
         ERROR = .TRUE.
         RETURN
      ENDIF
      SIG2I=1/(S_VR*S_VR)
      DY=OC_VR
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LIST,DY,CHI2(J),SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 
     +           'E-NORMAL, Error using radial velocity information'
         ENDIF
         RETURN
      ENDIF
      END

*
      SUBROUTINE USE_RHOTHETA(NFIT,LISTA,EL,ALPHA,BETA,
     +     CHI2,SD,ND,
     +     TIME,THETA,S_THETA,OC_THETA,RHO,S_RHO,OC_RHO,
     +     QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the linearised normal equation system, using constraints
C from one rho/theta pair
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2(2),SD(2)
      INTEGER ND(2)
      REAL*8 TIME,THETA,S_THETA,OC_THETA,RHO,S_RHO,OC_RHO
      LOGICAL QUIET,ERROR
*
      REAL THETA_M,RHO_M,WT,DY
      REAL SIG2I,B(MA),PHASE(MORBIT)
      REAL*4 MODULO
      EXTERNAL MODULO
*
      CALL SET_TIME(1,IORB0,TIME,PHASE)
*     Process teta...
      CALL GRAD_THETA(THETA_M,B)
      WT=SNGL(THETA)-THETA_M
* Adjust range of O-C to ]-180,180]
      WT=MODULO(WT,360.0)
      IF (WT.GT.180.) WT=WT-360.0
      OC_THETA=WT
      SIG2I=1/(S_THETA/GR)**2
      DY=OC_THETA/GR
      SD(1) = SD(1)+OC_THETA**2
      ND(1) = ND(1)+1
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2(1),SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'E-NORMAL, Error using Theta information'
         ENDIF
         RETURN
      ENDIF
*     Process Rho
      CALL GRAD_RHO(RHO_M,B)
      OC_RHO=RHO-RHO_M
      SIG2I=1/(S_RHO**2)
      DY=OC_RHO
      SD(2) = SD(2)+OC_RHO**2
      ND(2)=ND(2)+1
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2(2),SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'E-NORMAL, Error using Rho information'
         ENDIF
         RETURN
      ENDIF
      RETURN
      END

      SUBROUTINE USE_THETA(NFIT,LISTA,EL,ALPHA,BETA,
     +     CHI2,SD,ND,
     +     TIME,THETA,S_THETA,OC_THETA,
     +     QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the linearised normal equation system, using constraints
C from one position angle observation
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2(1),SD(1)
      INTEGER ND(1)
      REAL*8 TIME,THETA,S_THETA,OC_THETA
      LOGICAL QUIET,ERROR
*
      REAL THETA_M,WT,DY
      REAL SIG2I,B(MA),PHASE(MORBIT)
      REAL*4 MODULO
      EXTERNAL MODULO
*
      CALL SET_TIME(1,IORB0,TIME,PHASE)
*     Process teta...
      CALL GRAD_THETA(THETA_M,B)
      WT=SNGL(THETA)-THETA_M
* Adjust range of O-C to ]-180,180]
      WT=MODULO(WT,360.0)
      IF (WT.GT.180.) WT=WT-360.0
      OC_THETA=WT
      SIG2I=1/(S_THETA/GR)**2
      DY=OC_THETA/GR
      SD(1) = SD(1)+OC_THETA**2
      ND(1) = ND(1)+1
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2(1),SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'E-NORMAL, Error using Theta information'
         ENDIF
         RETURN
      ENDIF
      RETURN
      END

      SUBROUTINE USE_PROJ(NFIT,LISTA,EL,ALPHA,BETA,CHI2,SD,ND,
     +     TIME,PROJ,S_PROJ,OC_PROJ,POSANG,QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the normal equation system for the linearised problem, 
C using constraints from one projected relative separation
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2,SD
      INTEGER ND
      REAL*8 TIME,PROJ,S_PROJ,OC_PROJ,POSANG
      LOGICAL QUIET,ERROR
*
      REAL SIG2I,B(MA),PHASE(MORBIT),DY
      REAL XX,YY,POS,CPOS,SPOS
      REAL GX(MA),GY(MA)
      INTEGER J
*
      CALL SET_TIME(1,IORB0,TIME,PHASE)
!* Convert from astronomical convention (E of N) to trigonometric convention
!* (X axis is zero degrees)
!      POS = (90-POSANG)/GR 
      POS = POSANG/GR 
      CPOS=COS(POS)
      SPOS=SIN(POS)
      IF (CPOS.NE.0) CALL GRAD_X(XX,GX)
      IF (SPOS.NE.0) CALL GRAD_Y(YY,GY)
*
      OC_PROJ=PROJ-(XX*CPOS+YY*SPOS)
      SIG2I=1/(S_PROJ**2)
      DO J=1,MA
         B(J)=(CPOS*GX(J)+SPOS*GY(J))
      ENDDO
      DY=OC_PROJ
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2,SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 
     +           'E-NORMAL, Error using angular separation information'
         ENDIF
         RETURN
      ENDIF
      ND = ND+1
      SD = SD+OC_PROJ**2
      END
*
      SUBROUTINE USE_PPROJ(NFIT,LISTA,EL,ALPHA,BETA,CHI2,SD,ND,
     +     TIME,PROJ,S_PROJ,OC_PROJ,POSANG,QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the normal equation system for the linearised problem, 
C using constraints from one projected photocenter position, pre-corrected
C for parallax and proper motion
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2,SD
      INTEGER ND
      REAL*8 TIME,PROJ,S_PROJ,OC_PROJ,POSANG
      LOGICAL QUIET,ERROR
*
      REAL SIG2I,B(MA),PHASE(MORBIT),DY
      REAL XX,YY,POS,CPOS,SPOS
      REAL GX(MA),GY(MA)
      INTEGER J
*
      CALL SET_TIME(1,IORB0,TIME,PHASE)
      POS = POSANG/GR 
      CPOS=COS(POS)
      SPOS=SIN(POS)
      IF (CPOS.NE.0) CALL GRAD_PX(XX,GX)
      IF (SPOS.NE.0) CALL GRAD_PY(YY,GY)
*
      OC_PROJ=PROJ-(XX*CPOS+YY*SPOS)
      SIG2I=1/(S_PROJ**2)
      DO J=1,MA
         B(J)=(CPOS*GX(J)+SPOS*GY(J))
      ENDDO
      DY=OC_PROJ
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2,SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 
     +           'E-NORMAL, Error using angular separation information'
         ENDIF
         RETURN
      ENDIF
      ND = ND+1
      SD = SD+OC_PROJ**2
      END
      
      SUBROUTINE USE_APROJ(NFIT,LISTA,EL,ALPHA,BETA,CHI2,SD,ND,
     +     TIME,PIFX,PIFY,PROJ,S_PROJ,OC_PROJ,POSANG,QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the normal equation system for the linearised problem, 
C using constraints from one projected photocenter position, uncorrected
C for proper motion and parallax
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2,SD
      INTEGER ND
      REAL*8 TIME,PIFX,PIFY,PROJ,S_PROJ,OC_PROJ,POSANG
      LOGICAL QUIET,ERROR
*
      REAL SIG2I,B(MA),PHASE(MORBIT),DY
      REAL XX,YY,POS,CPOS,SPOS
      REAL GX(MA),GY(MA)
      INTEGER J
*
!      WRITE(6,*) 'W-FIT, Astrometric positions are experimental'
*
      CALL SET_TIME(1,IORB0,TIME,PHASE)
      POS = POSANG/GR 
      CPOS=COS(POS)
      SPOS=SIN(POS)
      IF (CPOS.NE.0) CALL GRAD_AX(PIFX,XX,GX)
      IF (SPOS.NE.0) CALL GRAD_AY(PIFY,YY,GY)
!      IF (CPOS.NE.0) CALL GRAD_AX(PIFY,XX,GX)
!      IF (SPOS.NE.0) CALL GRAD_AY(PIFX,YY,GY)
*
      OC_PROJ=PROJ-(XX*CPOS+YY*SPOS)
      SIG2I=1/(S_PROJ**2)
      DO J=1,MA
         B(J)=(CPOS*GX(J)+SPOS*GY(J))
      ENDDO
      DY=OC_PROJ
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2,SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 
     +           'E-NORMAL, Error using angular separation information'
         ENDIF
         RETURN
      ENDIF
      ND = ND+1
      SD = SD+OC_PROJ**2
      END
      
      SUBROUTINE USE_PARALLAX(NFIT,LISTA,EL,ALPHA,BETA,
     +     CHI2,SD,ND,
     +        PARALLAX,PARERR,OC_PAR,QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the normal equation system for the linearised problem, 
C using constraints from one projected relative separation
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2,SD
      INTEGER ND
      REAL PARALLAX,PARERR,OC_PAR
      LOGICAL QUIET,ERROR
*
      REAL SIG2I,PAR,B(MA),DY
*
      CALL GRAD_PARALLAX(PAR,B)
      OC_PAR=PARALLAX-PAR
      SIG2I=1/(PARERR**2)
      DY=OC_PAR
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2,SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'E-NORMAL, Error using parallax information'
            RETURN
         ENDIF
      ENDIF
      SD = SD+OC_PAR**2
      ND = ND+1
      END

      SUBROUTINE USE_VISI_SQUARED(NFIT,LISTA,EL,ALPHA,BETA,
     +           CHI2,SD,ND,TIME,IBAND,LAMBDA,U,V,W,
     +           VISI2,SIG_VISI2,OC_VISI2,
     +           QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: update the linearised normal equation system, using constraints
C from squared interferometric visibility.
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      REAL*8 EL(MA)
      INTEGER NFIT,LISTA(*)
      REAL ALPHA(MA,MA)
      REAL BETA(NFIT)
      REAL CHI2,SD
      INTEGER ND
      REAL*8 TIME,U,V,W,VISI2,SIG_VISI2,OC_VISI2
      REAL LAMBDA
      INTEGER IBAND
      LOGICAL QUIET,ERROR
*
      REAL VISI2_M,WT,DY
      REAL SIG2I,B(MA),PHASE(MORBIT)


*
* Will need to loop when triple/quadruple support will be added
      CALL SET_TIME(1,IORB0,TIME,PHASE)
*
*     Process squared visibility...
      CALL GRAD_VISI2(U,V,W,LAMBDA,IBAND,VISI2_M,B)
      WT=SNGL(VISI2)-VISI2_M
      OC_VISI2=WT
      SIG2I=1/(SIG_VISI2)**2
      DY=OC_VISI2
      SD = SD+OC_VISI2**2
      ND = ND+1
      CALL COVSUM(NFIT,MA,ALPHA,BETA,B,LISTA,DY,CHI2,SIG2I,
     +     QUIET,ERROR)
      IF (ERROR) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'E-NORMAL, Error using Squared ',
     +           'Visibility information'
         ENDIF
         RETURN
      ENDIF
      RETURN
      END


