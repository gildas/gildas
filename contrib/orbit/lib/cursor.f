      SUBROUTINE CURSOR(LINE,ERROR)
      USE GKERNEL_INTERFACES
C----------------------------------------------------------------------
C ORBIT	Support routine for command
C	CURSOR
C	Activates the cursor.
C Arguments :
C	LINE	C*(*)	Command line		Input
C	ERROR	L	Logical error flag	Output
C----------------------------------------------------------------------
      CHARACTER*(*) LINE
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'control.inc'
      INCLUDE 'data.inc'
      INCLUDE 'elements.inc'
*
      REAL*8 UCURSE(2),DUMMY(MA,1)
      REAL*4 XCURSE,YCURSE
      INTEGER IOBS,IVR,MITER
      CHARACTER CODE*1
      LOGICAL WARNING,QUIET,UPDATE
*
      SAVE XCURSE,YCURSE
*
      IF (.NOT.GTG_CURS()) THEN
         CALL GAGOUT (
     $        'E-DRAW,  Cursor not available on this device')
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      CODE = ' '
      DO WHILE(CODE.NE.'Q')
* Loop over with the cursor
         CALL GR_CURS(UCURSE(1),UCURSE(2),XCURSE,YCURSE,CODE)
         ERROR = GTERRTST()
         IF (ERROR) RETURN
*     map codes returned by ButtonPress events to keycodes
         IF (CODE.EQ.'^') THEN
            CODE = 'R'
         ELSEIF (CODE.EQ.'&') THEN
            CODE = 'L'
         ELSEIF (CODE.EQ.'*') THEN
            CODE = 'Q'
         ENDIF
*
         IF (CODE.EQ.'Q'.OR.CODE.EQ.'E') THEN
            GOTO 100  ! Quit
* Repeat the fit
         ELSEIF (CODE.EQ.'F') THEN
            MITER = 100
            QUIET  = .FALSE.
            UPDATE = .TRUE.
            CALL DO_FIT(MITER,EL,UPDATE,QUIET,WARNING,ERROR)
            IF (ERROR) ERROR = .FALSE.
* Print updated elements
            CALL PRINT_ELEMENTS(6,.FALSE.,1,.5,DUMMY,.FALSE.,ERROR)
            IF (ERROR) RETURN
* Refresh the plot
         ELSEIF (CODE.EQ.'R') THEN
            IF (PLOT_TYPE.EQ.'RADIAL VELOCITY') then
               CALL PLOT_RV(-1,'  ',ERROR)
            ELSEIF(PLOT_TYPE.EQ.'VISUAL ORBIT') then
               CALL PLOT_VISUAL(-1,ERROR)
            ELSEIF(PLOT_TYPE.EQ.'CORRELATION DIP') then
               WRITE(6,*) 'Action code not supported for',
     +              'correlation dips'
            ELSE
               WRITE(6,*) 'E-CURSOR,  Unknown plot type'
               ERROR = .TRUE.
               RETURN
            ENDIF
         ELSEIF (CODE.EQ.'O') THEN
            IF (PLOT_TYPE.EQ.'VISUAL ORBIT') then
               CALL SHOW_VISUAL_FIT(UCURSE(1),UCURSE(2))
            ELSEIF (PLOT_TYPE.EQ.'RADIAL VELOCITY') then
               CALL SHOW_RV_FIT(UCURSE(1))
            ELSEIF(PLOT_TYPE.EQ.'CORRELATION DIP') then
               WRITE(6,*) 'Action code not supported for',
     +              'correlation dips'
            ELSE
               WRITE(6,*) 'E-CURSOR,  Unknown plot type'
               ERROR = .TRUE.
               RETURN
            ENDIF
* Other codes are handled in plot-type dependent routines
         ELSE
            IF (PLOT_TYPE.EQ.'RADIAL VELOCITY') THEN
               CALL FIND_VR(UCURSE(1),UCURSE(2),IVR)
               IF (IVR.NE.0) THEN
                  CALL EDIT_VR(CODE,IVR,ERROR)
                  IF (ERROR) RETURN
               ENDIF
            ELSEIF (PLOT_TYPE.EQ.'VISUAL ORBIT') THEN
               CALL FIND_VISUAL(UCURSE(1),UCURSE(2),IOBS)
               IF (IOBS.NE.0) THEN
                  CALL EDIT_VISUAL(CODE,IOBS,ERROR)
                  IF (ERROR) RETURN
               ENDIF
            ELSEIF (PLOT_TYPE.EQ.'CORRELATION DIP') THEN
               WRITE(6,*) 'E-CURSOR,  Action is unsupported ',
     +              ' for correlation dips'
            ELSE
               WRITE(6,*) 'E-CURSOR,  Unknown plot type ',
     +              PLOT_TYPE
               ERROR = .TRUE.
               RETURN
            ENDIF
         ENDIF
* Help, also used as error handling for unsupported codes
         IF (CODE.EQ.'H'.OR.CODE.EQ.'?') THEN
            WRITE(6,*) 'Supported action codes are:'
            WRITE(6,*) 'S      Swap the A and B components'
            WRITE(6,*) 'F      Fit the orbital elements'
            WRITE(6,*) 'I      Ignore closest data point in ',
     +           'subsequent fits'
            WRITE(6,*) 'O      Show parameters of closest ',
     +           'orbit point'
            WRITE(6,*) 'Q      Quit the cursor mode'
            WRITE(6,*) 'R      Refresh the plot'
            WRITE(6,*) 'U      Use closest data point in ',
     +           'subsequent fits'
            WRITE(6,*) 'V      Show value of closest data point'
            WRITE(6,*) '?      Displays this text'
         ENDIF
*
      ENDDO
*
 100  RETURN
*
      END

      SUBROUTINE FIND_VR(PHASE,V,IVR)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Find radial velocity data point closest to a specified position
C Arguments :
C     PHASE   R*8     Orbital phase
C     V	      R*8     Radial velocity
C     IVR     I       Data point
C----------------------------------------------------------------------
      REAL*8 PHASE,V
      INTEGER IVR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
*
      INTEGER I
      REAL*8 PH
      REAL*4 D,MIN_D
      REAL*4 X1,Y1,X2,Y2
      REAL*4 SYMBSIZE
      REAL*8 DMODULO
*
      IVR = 0
      MIN_D = 1E37
* Look for minimum distance, in paper space.
      DO I=1,PLOT_NV
         PH = DMODULO(PHASE,1.D0)
         CALL GR8_USER_PHYS(PH,V,X1,Y1,1)
         CALL GR8_USER_PHYS(PLOT_XV(I),PLOT_V(I),X2,Y2,1)
         D = (X1-X2)**2+(Y1-Y2)**2
         IF (D.LT.MIN_D) THEN
            IVR = VR_INDEX(I)
            MIN_D = D
         ENDIF
      ENDDO
      MIN_D = SQRT(MIN_D)
      CALL INQSYM(SYMBSIZE)
      IF (MIN_D.GT.1.5*SYMBSIZE) THEN
         WRITE(6,*) 'W-CURSOR, No data point at cursor position. ',
     +        'No action taken.'
         IVR = 0
      ENDIF
*
      RETURN
      END

      SUBROUTINE SHOW_VISUAL_FIT(X,Y)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Find point of fitted visual orbit closest to a specified position,
C       and list its parameters
C Arguments :
C     X       R*8     X position
C     Y	      R*8     Y position
C----------------------------------------------------------------------
      REAL*8 X,Y
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I,IBEST
      REAL*4 D,MIN_D
      REAL*4 X1,Y1,X2,Y2
      REAL*4 SYMBSIZE
      REAL PHASE,RHO,THETA
*
      REAL*8 TODAY, T
      INTEGER ID
      CHARACTER*11 DATE
      REAL PHASE_TODAY, DPHI
      CHARACTER CHAIN*24
      LOGICAL ERROR
*
      IBEST = -1
      MIN_D = 1E37
* Look for minimum distance
      CALL GR8_USER_PHYS(X,Y,X1,Y1,1)
      DO I=1,NPTS
         CALL GR4_USER_PHYS(MODEL_X(I),MODEL_Y(I),X2,Y2,1)
         D = (X1-X2)**2+(Y1-Y2)**2
         IF (D.LT.MIN_D) THEN
            IBEST  = I
            MIN_D = D
         ENDIF
      ENDDO
      MIN_D = SQRT(MIN_D)
      CALL INQSYM(SYMBSIZE)
      IF (MIN_D.GT.1.5*SYMBSIZE) THEN
         WRITE(6,*) 'W-CURSOR, No orbit point at cursor position. '
      ELSE
         PHASE = MODEL_PHASE(IBEST) 
         WRITE(6,*) 'Parameters of selected orbital point: '
         CALL SET_PHASE(IORB0,PHASE)
         CALL KEPLER_RHOTHETA(RHO,THETA)
* Compute next date with corresponding phase
         CALL SIC_DATE(DATE)
         CALL CDATE(DATE,ID,ERROR)
         TODAY = ID+60549.5D0
         CALL SET_TIME(1,IORB0,TODAY,PHASE_TODAY)
         DPHI = PHASE-PHASE_TODAY
         IF (DPHI.LT.0) DPHI = DPHI+1
         T = TODAY + DPHI*EL(ELINDX(IORB0)+I_PERIOD)
         CALL FORMAT_DATE(T,CHAIN,ERROR)
         WRITE(6,100) CHAIN, 'Phase : ', PHASE, RHO, THETA
         T = T - EL(ELINDX(IORB0)+I_PERIOD)
         CALL FORMAT_DATE(T,CHAIN,ERROR)
         WRITE(6,100) CHAIN, 'Phase : ', PHASE, RHO, THETA

 100     FORMAT (1X,A,A,F4.2,1X,'Rho: ',F6.4,'"',1X,
     +        'Theta: ',F6.2,' degrees')
      ENDIF
*
*
      RETURN
      END

      SUBROUTINE SHOW_RV_FIT(DPHASE)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Find point of fitted radial velocity orbit closest to a specified 
C       phase and list its parameters
C Arguments :
C     PHASE   R*8     X position
C----------------------------------------------------------------------
      REAL*8 DPHASE
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
      INCLUDE 'control.inc'
      INCLUDE 'elements.inc'
*
* Radial velocity ephemeride
      REAL PHASE
      REAL VA        ! Radial velocity of primary star (in current orbit)
      REAL VB        ! Radial velocity of secundary star (in current orbit)
*

      REAL*8 TODAY, T
      INTEGER ID
      CHARACTER*11 DATE
      REAL PHASE_TODAY, DPHI
      CHARACTER CHAIN*24
      LOGICAL ERROR
* Bring phase from [-0.5,1.5] back to [0,1[
      IF (DPHASE.LT.0) THEN
         PHASE = SNGL(DPHASE+1)
      ELSEIF (DPHASE.GE.1) THEN
         PHASE=SNGL(DPHASE-1)
      ELSE
         PHASE=SNGL(DPHASE)
      ENDIF
*
      WRITE(6,*) 'Parameters of selected orbital point: '
      CALL SET_PHASE(PLOT_IORB,PHASE)
      CALL KEPLER_RV(PLOT_IORB,VA,VB)
*
*     Compute next/previous date with corresponding phase
      CALL SIC_DATE(DATE)
      CALL CDATE(DATE,ID,ERROR)
      TODAY = ID+60549.5D0
      CALL SET_TIME(1,PLOT_IORB,TODAY,PHASE_TODAY)
      DPHI = PHASE-PHASE_TODAY
      IF (DPHI.LT.0) DPHI = DPHI+1
      T = TODAY + DPHI*EL(ELINDX(PLOT_IORB)+I_PERIOD)
      CALL FORMAT_DATE(T,CHAIN,ERROR)
      WRITE(6,100) CHAIN, 'Phase : ', PHASE, VA, VB
      T = T-EL(ELINDX(PLOT_IORB)+I_PERIOD)
      CALL FORMAT_DATE(T,CHAIN,ERROR)
      WRITE(6,100) CHAIN, 'Phase : ', PHASE, VA, VB

 100  FORMAT ((1X,A),A,F4.2,1X,'VA: ',F8.3,' km/s',1X,
     +     'VB: ',F8.3,' km/s')
*
      RETURN
      END

      SUBROUTINE FIND_VISUAL(X,Y,IOBS)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Find visual data point closest to a specified position
C Arguments :
C     X       R*8     X position
C     Y	      R*8     Y position
C     IOBS    I       Data point
C----------------------------------------------------------------------
      REAL*8 X,Y
      INTEGER IOBS
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
*
      INTEGER I
      REAL*4 D,MIN_D
      REAL*4 X1,Y1,X2,Y2
      REAL*4 SYMBSIZE
*
      IOBS = 0
      MIN_D = 1E37
* Look for minimum distance
      DO I=1,NN
         CALL GR8_USER_PHYS(X,Y,X1,Y1,1)
         CALL GR4_USER_PHYS(XDISP(I),YDISP(I),X2,Y2,1)
         D = (X1-X2)**2+(Y1-Y2)**2
         IF (D.LT.MIN_D) THEN
            IOBS  = I
            MIN_D = D
         ENDIF
      ENDDO
      MIN_D = SQRT(MIN_D)
      CALL INQSYM(SYMBSIZE)
      IF (MIN_D.GT.1.5*SYMBSIZE) THEN
!         write(6,*) MIN_D,SYMBSIZE
         WRITE(6,*) 'W-CURSOR, No data point at cursor position. ',
     +        'No action taken.'
         IOBS = 0
      ENDIF
*
*
      RETURN
      END

      SUBROUTINE EDIT_VR(CODE,IVR,ERROR)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Data editing for radial velocities
C Arguments :
C	CODE	C*(1)	Requested action
C	IVR	I	Data index
C----------------------------------------------------------------------
      CHARACTER*1 CODE
      INTEGER IVR
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      CHARACTER CHAIN*17
      INTEGER LENC
*
      IF (IVR.LT.-NVCORR.OR.IVR.GT.NVR.OR.IVR.EQ.0) THEN
         WRITE(6,*) 'E-CURSOR,  Invalid index IVR: ',IVR
         WRITE(6,*) 'E-CURSOR,  Internal logic error'
         RETURN
      ENDIF
*
* Exchange A and B
      IF (CODE.EQ.'S') THEN
         IF (IVR.GT.0) THEN
            IF (CVR(IVR).EQ.'1') THEN
               CVR(IVR)= '2'
            ELSEIF (CVR(IVR).EQ.'2') THEN
               CVR(IVR)= '1'
            ENDIF
         ELSE
            WRITE(6,*) 'W-CURSOR,  Code S ignored for correaltions.'
         ENDIF
* Ignore data point in subsequent fits
      ELSEIF (CODE.EQ.'I') THEN
         IF (IVR.GT.0) THEN
            STATVR(IVR) = 'Ignored'
         ELSE
            STATVCORR(-IVR) = 'Ignored'
         ENDIF
* Use data point in subsequent fits
      ELSEIF (CODE.EQ.'U') THEN
         IF (IVR.GT.0) THEN
            STATVR(IVR) = 'OK'
         ELSE
            STATVCORR(-IVR) = 'OK'
         ENDIF
* Kill data point (no longer plot it nor save it)
      ELSEIF (CODE.EQ.'K') THEN
         IF (IVR.GT.0) THEN
            STATVR(IVR) = 'Killed'
         ELSE
            STATVCORR(-IVR) = 'Killed'
         ENDIF
* Print value
      ELSEIF (CODE.EQ.'V') THEN
         IF (IVR.GT.0) THEN
            CALL FORMAT_DATE(VR(IVR,I_TIME),CHAIN,ERROR)
            WRITE (6,122) CVR(IVR),CHAIN,VR(IVR,I_TIME)
     +           ,VR(IVR,I_VR),VR(IVR,I_S_VR),VR(IVR,I_OC_VR)
     +           ,STATVR(IVR)(1:LENC(STATVR(IVR)))
         ELSE
            CALL FORMAT_DATE(COR_TIME(-IVR),CHAIN,ERROR)
            WRITE (6,123) CHAIN,COR_TIME(-IVR)
     +           ,COR_SIGM(-IVR)
     +           ,STATVCORR(-IVR)(1:LENC(STATVCORR(-IVR)))
         ENDIF
 122     FORMAT (1X,'V',A2,1X,A,1X,F10.3,1X,
     +        F8.3,1X,F7.3,1X,F7.3,1X,A)
 123     FORMAT (1X,'Correl',1X,A,1X,F10.3,1X,
     +        F8.3,1X,A)
      ELSE
         WRITE(6,*) 'E-CURSOR,  Action code ',CODE,
     +        ' is unsupported'
         CODE = '?'
      ENDIF
      END

      SUBROUTINE EDIT_VISUAL(CODE,IOBS,ERROR)
C----------------------------------------------------------------------
C ORBIT	
C	CURSOR
C	Data editing for visual orbit data
C Arguments :
C	CODE	C*(1)	Requested action
C	IOBS	I	Data index
C----------------------------------------------------------------------
      CHARACTER*1 CODE
      INTEGER IOBS
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      CHARACTER CHAIN*17
      INTEGER LENC
      REAL*8 DMODULO
*
      IF (IOBS.LT.1.OR.IOBS.GT.NN) RETURN
*
* Exchange A and B
      IF (CODE.EQ.'S') THEN
         IF (COBS(IOBS).EQ.'rho-theta') THEN
            OBS(IOBS,I_THETA)=DMODULO(OBS(IOBS,I_THETA)+180.D0,360.D0)
         ELSEIF (COBS(IOBS).EQ.'XY'
     +           .OR.COBS(IOBS).EQ.'AXY'
     +           .OR.COBS(IOBS).EQ.'PXY'
     +           ) THEN
            OBS(IOBS,I_X)=-OBS(IOBS,I_X)
            OBS(IOBS,I_Y)=-OBS(IOBS,I_Y)
         ELSEIF (COBS(IOBS).EQ.'ZT') THEN
            OBS(IOBS,I_Z)=-OBS(IOBS,I_Z)
            OBS(IOBS,I_T)=-OBS(IOBS,I_T)
         ELSEIF (COBS(IOBS).EQ.'X') THEN
            OBS(IOBS,I_X)=-OBS(IOBS,I_X)
         ELSEIF (COBS(IOBS).EQ.'Y') THEN
            OBS(IOBS,I_Y)=-OBS(IOBS,I_Y)
         ELSEIF (COBS(IOBS).EQ.'Z') THEN
            OBS(IOBS,I_Z)=-OBS(IOBS,I_Z)
         ELSEIF (COBS(IOBS).EQ.'T') THEN
            OBS(IOBS,I_T)=-OBS(IOBS,I_T)
         ELSEIF (COBS(IOBS).EQ.'PROJ'
     +           .OR.COBS(IOBS).EQ.'APROJ'
     +           .OR.COBS(IOBS).EQ.'PPROJ') THEN
            OBS(IOBS,I_PROJ)=-OBS(IOBS,I_PROJ)
         ELSE
            WRITE(6,'(1X,A,A)') 'W-CURSOR, Unsupported data type',
     +           COBS(IOBS)
         ENDIF
* Ignore data point in subsequent fits
      ELSEIF (CODE.EQ.'I') THEN
         STATOBS(IOBS)='Ignored'
* Use data point in subsequent fits
      ELSEIF (CODE.EQ.'U') THEN
         STATOBS(IOBS)='OK'
* Kill data point (no longer plot it nor save it)
      ELSEIF (CODE.EQ.'K') THEN
         STATOBS(IOBS)='Killed'
* Print value
      ELSEIF (CODE.EQ.'V') THEN
         CALL FORMAT_DATE(OBS(IOBS,I_TIME),CHAIN,ERROR)
*
         IF (COBS(IOBS).EQ.'rho-theta') THEN
            WRITE (6,142) COBS(IOBS),CHAIN,OBS(IOBS,I_TIME)
     +           ,OBS(IOBS,I_RHO),OBS(IOBS,I_S_RHO)
     +           ,OBS(IOBS,I_OC_RHO)
     +           ,OBS(IOBS,I_THETA),OBS(IOBS,I_S_THETA)
     +           ,OBS(IOBS,I_OC_THETA)
     +           ,STATOBS(IOBS)(1:LENC(STATOBS(IOBS)))
 142        FORMAT (A,1X,A,1X,F10.3,3(1X,F5.3),3(1X,F5.1),1X,A)
         ELSEIF (COBS(IOBS).EQ.'XY') THEN
            WRITE (6,162) COBS(IOBS), CHAIN, OBS(IOBS,I_TIME)
     +           ,OBS(IOBS,I_X),OBS(IOBS,I_S_X),OBS(IOBS,I_OC_X)
     +           ,OBS(IOBS,I_Y),OBS(IOBS,I_S_Y),OBS(IOBS,I_OC_Y)
     +           ,STATOBS(IOBS)(1:LENC(STATOBS(IOBS)))
 162        FORMAT(A,1X,A,1X,F10.3,2(1X,F6.3,1X,F5.3,1X,F5.3),1X,A)
         ELSEIF (COBS(IOBS).EQ.'ZT') THEN
            WRITE (6,162) COBS(IOBS), CHAIN, OBS(IOBS,I_TIME)
     +           ,OBS(IOBS,I_Z),OBS(IOBS,I_S_Z),OBS(IOBS,I_OC_Z)
     +           ,OBS(IOBS,I_T),OBS(IOBS,I_S_T),OBS(IOBS,I_OC_T)
     +           ,STATOBS(IOBS)(1:LENC(STATOBS(IOBS)))
         ELSEIF (COBS(IOBS).EQ.'PROJ') THEN
            WRITE (6,163) COBS(IOBS), CHAIN, OBS(IOBS,I_TIME)
     +           ,OBS(IOBS,I_POSANG)
     +           ,OBS(IOBS,I_PROJ),OBS(IOBS,I_S_PROJ)
     +           ,OBS(IOBS,I_OC_PROJ)
     +           ,STATOBS(IOBS)(1:LENC(STATOBS(IOBS)))
 163        FORMAT(A,1X,A,1X,F10.3,F7.1,3(1X,F5.3),1X,A)
         ELSE
            WRITE(6,*) 'F-CURSOR,  Unknown visual orbit data type'
         ENDIF
      ELSE
         WRITE(6,*) 'E-CURSOR,  Action code ',CODE,
     +        ' is unsupported'
         CODE = '?'
      ENDIF
      END



