! Verifier les derivees...

C*******************************************************
C Collection of subroutines to calculate keplerian orbits.
C Initially programmed by A.A.Tokovinin. Some changes and 
C additions by F.Beck. Then restructured by T.Forveille
C (syntax mostly, some protection against imprecise computations; no really 
* significant change)
C Elements are passed in the REAL*8 array EL(i):
C   1    2    3    4    5    6   7   8    9   
C   P    T    e    a  node   w   i   K1   K2  
C V0 is now handled in the various routines of velocity.f, to allow
C fitting of higher multiplicity systems
C
C The following capabilities are provided:
C - initialize internal set of elements and functions of elements from
C   external set
C - calculate orbital phase and solve Kepler equation
C - solve Kepler equation
C - radial velocity ephemerid (Vel 1,2)
C - visual binary ephemerid   (TETA,RO)
C - visual binary ephemeride, (X,Y)
C - visual binary ephemeride, (projection along arbitrary direction)
C - Partial derivatives of TETA,RO,V1,V2,X,Y, etc over elements
C - Logarithmic partial derivatives of masses over elements
C*****************************************************
      SUBROUTINE INIT_KEPLER(EL,QUIET)
* Initialize internal elements and functions of elements from external
* set
      REAL*8 EL(*)   ! Orbital elements
      LOGICAL QUIET
*
      INCLUDE 'physical_constants.inc'
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      INCLUDE 'pi.inc'
      INTEGER I,IORB
*
      SILENT = QUIET
      DO IORB=1,MORBIT
         P(IORB)=EL(ELINDX(IORB)+I_PERIOD)
         PMU(IORB)=PI2/P(IORB)
         T0(IORB)=EL(ELINDX(IORB)+I_T0)
      
         SF(IORB)=SNGL(EL(ELINDX(IORB)+I_Ecc))
* Protect against |e|>=1 (Parabolic and hyperbolic orbits; would crash...)
         IF (SF(IORB).GT.+(1-10*EPSR4)) SF(IORB)=+(1-10*EPSR4)
         IF (SF(IORB).LT.-(1-10*EPSR4)) SF(IORB)=-(1-10*EPSR4)
         CF2(IORB)=1.-SF(IORB)*SF(IORB)
         CF(IORB)=SQRT(CF2(IORB))
         CF3(IORB)=CF(IORB)*CF2(IORB)
         EC(IORB)=SQRT((1.+SF(IORB))/(1.-SF(IORB)))
         A(IORB)=SNGL(EL(ELINDX(IORB)+I_as))
         WW(IORB)=SNGL(EL(ELINDX(IORB)+I_OMEGA_U)/GR  )
         CWW(IORB)=COS(WW(IORB))
         SWW(IORB)=SIN(WW(IORB))
      
         W(IORB)=SNGL(EL(ELINDX(IORB)+I_OMEGA_L)/GR)
         CW(IORB)=COS(W(IORB))
         SW(IORB)=SIN(W(IORB))
      
         SI(IORB)=SIN(EL(ELINDX(IORB)+I_i)/GR)
         CI(IORB)=COS(EL(ELINDX(IORB)+I_i)/GR)
         TI(IORB)=SI(IORB)/CI(IORB)
         K1(IORB)=SNGL(EL(ELINDX(IORB)+I_K1))
         K2(IORB)=SNGL(EL(ELINDX(IORB)+I_K2))
C Thiele-van-den-Bos elements
         AA(IORB)=SNGL(A(IORB)*(CW(IORB)*CWW(IORB)
     +        -SW(IORB)*SWW(IORB)*CI(IORB)))
         BB(IORB)=SNGL(A(IORB)*(CW(IORB)*SWW(IORB)
     +        +SW(IORB)*CWW(IORB)*CI(IORB)))
         FF(IORB)=SNGL(A(IORB)*(-SW(IORB)*CWW(IORB)
     +        -CW(IORB)*SWW(IORB)*CI(IORB)))
         GG(IORB)=SNGL(A(IORB)*(-SW(IORB)*SWW(IORB)
     +        +CW(IORB)*CWW(IORB)*CI(IORB)))
      ENDDO
* Velocity zero points and systemic velocity. Only one value for all orbits
      V0=SNGL(EL(I_V0))
      DO I=2,MVREF
         DV(I-1)= SNGL(EL(I_DV+I-1))
      ENDDO
* Position zero points, and proper motions
      X0   = SNGL(EL(I_X0))
      Y0   = SNGL(EL(I_Y0))
      Mu_X = SNGL(EL(I_MuX))
      Mu_Y = SNGL(EL(I_MuY))
      EPOCH = EL(I_EPOCH)
* Compute parallax from the elements of the outer orbit
      DO I=1,MA
         GRAD_PI(I)=0
      ENDDO
* Return zero rather than NaNs or Inf whenever parameters are undefined
      IF ((K1(IORB0)+K2(IORB0)).EQ.0
     +     .OR.P(IORB0).EQ.0
     +     .OR.A(IORB0).EQ.0
     +     .OR.CF(IORB0).EQ.0
     +     .OR.SI(IORB0).EQ.0) THEN
         PAR = 0
      ELSE
* Parallax
         PAR=SNGL(AU*2*PI*SI(IORB0)*A(IORB0)
     +        /(((K1(IORB0)+K2(IORB0))*KM_S)
     +        *CF(IORB0)*P(IORB0)*DAY))
* Logarithmic gradient of the parallax
         GRAD_PI(I_PERIOD+ELINDX(IORB0)) = SNGL(-1/P(IORB0))
         GRAD_PI(I_Ecc+ELINDX(IORB0)) = SF(IORB0)/CF(IORB0)**2
         GRAD_PI(I_as+ELINDX(IORB0)) = +1/A(IORB0)
         GRAD_PI(I_i+ELINDX(IORB0)) = SNGL(CI(IORB0)/SI(IORB0))
         GRAD_PI(I_K1+ELINDX(IORB0)) = -1/(K1(IORB0)+K2(IORB0))
         GRAD_PI(I_K2+ELINDX(IORB0)) = -1/(K1(IORB0)+K2(IORB0))
* Then scale the gradient
         DO I=1,MA
            GRAD_PI(I)=GRAD_PI(I)*PAR
         ENDDO
      ENDIF


*
* Differential convective+gravitational velocity shifts. Two per orbits, but 
* only Nstar-1 are actual free parameters.
      DO IORB=1,MORBIT
         DELTV(1,IORB) = SNGL(EL(ELINDX(IORB)+I_DELTV1))
         DELTV(2,IORB) = SNGL(EL(ELINDX(IORB)+I_DELTV2))
      ENDDO
* Fractional flux of secondary. Limited to binaries for the time beeing, but
* will need to be generalised
      DO I=1,MPHOT
         FLXFRAC(I) = SNGL(EL(I_RELFLUX+I))
      ENDDO
      RETURN
      END


      SUBROUTINE SET_TIME(NORB,LISTORB,T,PHASE)
* Compute phase for the moment T and set it
      INTEGER NORB,LISTORB(NORB)
      REAL*8 T
      REAL PHASE(NORB)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL*8 DMODULO
      REAL PH
      INTEGER I,IORB
*
*
      DT_EPOCH=T-EPOCH
      DO I=1,NORB
         IORB=LISTORB(I)
         DT(IORB)=T-T0(IORB)
         PH=SNGL(DMODULO((T-T0(IORB))/P(IORB),1.D0))
         IF (PH.LT.0.) PH=PH+1.
         CALL SET_PHASE(IORB,PH)
         PHASE(I) = PH
      ENDDO
      RETURN
      END

      SUBROUTINE SET_PHASE(IORB,PHASE)
C Solve Kepler equation for one orbital phase
      INTEGER IORB
      REAL PHASE
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'kepler.inc'
*
      REAL ANM
*
      REAL SPHASE(MORBIT)
      DATA SPHASE /-1E37,-1E37, -1E37/ 
      SAVE SPHASE
*
      IF (PHASE.EQ.SPHASE(IORB)) RETURN ! Small risk after new elements loading
      SPHASE(IORB) = PHASE
* Solve the Kepler equation
      ANM=SNGL(PHASE*PI2)
      E(IORB)=ANM
* Not robust to excentricities (stored in SF) that go too close to 1. Would
* need to rewrite that differently, or switch to double precision.
 31   E1(IORB)=E(IORB)+(ANM+SF(IORB)*SIN(E(IORB))-E(IORB))/
     +     (1.0-SF(IORB)*COS(E(IORB)))
! This should really return an error, but it then needs to be handled 
! downstream...
      if (e1(iorb).ne.e1(iorb)) then 
         if (ABS(SF(IORB)).GT.0.99999) then ! Highest known in star is ~0.975
            write(6,*) 'W-KEPLER, Quasi-parabolic orbit for orbit '
     +           ,IORB,'. Excentricity is ',SF(IORB)
!         else
!            write(6,*) 'E1 is a NaN for orbit ',iorb
!            write(6,*) 'E: ',E(IORB), 'denom',1.0-SF(IORB)*COS(E(IORB))
!            write(6,*) 'ANM: ',ANM,' SF: ', SF(IORB)  
         endif
      endif
      IF (ABS(E1(IORB)-E(IORB)).GT.1.E-5) THEN
         E(IORB)=E1(IORB)
         GOTO 31
      ENDIF
* Post solving computations
      V(IORB)=2.*ATAN(EC(IORB)*TAN(E1(IORB)/2.))
!      if (v(iorb).ne.v(iorb)) then  ! Test for NaNs...
!         write(6,*) 'V is a NaN for orbit ',iorb
!         write(6,*) 'ATAN arg is ',EC(IORB)*TAN(E1(IORB)/2.)
!         write(6,*) 'EC: ',EC(IORB),' E1: ',E1(IORB)
!         write(6,*) 'Phase: ',phase
!      endif 
      U(IORB)=V(IORB)+W(IORB)
      SU(IORB)=SIN(U(IORB))
      CU(IORB)=COS(U(IORB))
      RETURN
      END


      SUBROUTINE KEPLER_RV(IORB,VA,VB)
* Radial velocity ephemeride
      INTEGER IORB   ! Orbit number
      REAL VA        ! Radial velocity of primary star (in current orbit)
      REAL VB        ! Radial velocity of secundary star (in current orbit)
*
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL A1
*
      A1 = SF(IORB)*CW(IORB)+CU(IORB)
      VA = +K1(IORB)*A1
      VB = -K2(IORB)*A1
      RETURN
      END

      SUBROUTINE KEPLER_RHOTHETA(RHO,THETA)
C calc. TETA and RO
      REAL RHO,THETA
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      INCLUDE 'pi.inc'
      Integer iorb
      parameter (iorb=1)
*
      REAL*8 TMW
      REAL CTMW !,STMW
      REAL R,RO
      REAL CE !,SE
*
      CE=COS(E1(IORB))
!      SE=SIN(E1(IORB))
      R=A(IORB)*(1.-SF(IORB)*CE)
      TMW=ATAN(CI(IORB)*TAN(U(IORB)))
      IF (CU(IORB).LT.0.) TMW=TMW+PI
!      STMW=SNGL(SIN(TMW))
      CTMW=SNGL(COS(TMW))

      RO=R*CU(IORB)/CTMW
      THETA=SNGL((TMW+WW(IORB))*GR)
      IF (THETA.GT.360.) THETA=THETA-360.
      IF (THETA.LT.0.) THETA=THETA+360.
      RHO=RO
      RETURN
      END

      SUBROUTINE GRAD_THETA(THETA,B)
* B(j)=dTETA/dEL(j), RES1=TETA
* Caution: THETA in degrees, but B in radians...
      REAL THETA,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      INCLUDE 'pi.inc'
*
      Integer iorb
      parameter (iorb=1)
*
      REAL*8 TMW
      REAL CTMW,STMW
      REAL A1,A2,A3,A4
      REAL R,RO
      REAL CE,SE
*
      INTEGER I
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
* Compute rho/theta
      CE=COS(E1(IORB))
      SE=SIN(E1(IORB))
      R=A(IORB)*(1.-SF(IORB)*CE)
      TMW=ATAN(CI(IORB)*TAN(U(IORB)))
      IF (CU(IORB).LT.0.) TMW=TMW+PI
      STMW=SNGL(SIN(TMW))
      CTMW=SNGL(COS(TMW))

      RO=R*CU(IORB)/CTMW
      THETA=SNGL((TMW+WW(IORB))*GR)
* Minor massaging...
      IF (THETA.GT.360.) THETA=THETA-360.
      IF (THETA.LT.0.) THETA=THETA+360.
*
      A1=(A(IORB)/RO)**2
      A2=R/A(IORB)
      A3=SNGL(A1*CF(IORB)*CI(IORB))
      A4=CTMW*STMW
      
      B(I_PERIOD+ELINDX(IORB))=SNGL(A3*DT(IORB))
      B(I_T0+ELINDX(IORB))=SNGL(-A3*PMU(IORB))
      B(I_Ecc+ELINDX(IORB))=SNGL(A1*CI(IORB)*SE*(A2+
     +     CF2(IORB))/CF(IORB))
      B(I_OMEGA_U+ELINDX(IORB))=+1.
!      B(I_OMEGA_U+ELINDX(IORB))=-1.
      B(I_OMEGA_L+ELINDX(IORB))=SNGL(CI(IORB)*(CTMW/CU(IORB))**2)
      B(I_I+ELINDX(IORB))=SNGL(-TI(IORB)*A4)
* Change sign to match conversion of theta to/from astronomical convention,
!      do i=1,MA
!         b(i) = -b(i)
!      enddo
      RETURN
      END

      SUBROUTINE GRAD_RHO(RHO,B)
C B(j)=1/RO*dRO/dEL(j)
      REAL RHO,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'kepler.inc'
*
      Integer iorb
      parameter (iorb=1)
*
      REAL TMW
      REAL CTMW,STMW
      REAL A1,A2,A3,A4,A5
      REAL R,RO
      REAL CE,SE
*
      INTEGER I
*
      DO I=1,MA
         B(I)=0
      ENDDO
* Compute Rho
      CE=COS(E1(IORB))
      SE=SIN(E1(IORB))
      R=A(IORB)*(1.-SF(IORB)*CE)
      TMW=SNGL(ATAN(CI(IORB)*TAN(U(IORB))))
      IF (CU(IORB).LT.0.) TMW=TMW+SNGL(PI)
      STMW=SIN(TMW)
      CTMW=COS(TMW)
      RO=R*CU(IORB)/CTMW
      RHO=RO
*
      A1=(A(IORB)/R)**2
      A2=R/A(IORB)
      A4=CTMW*STMW
      A5=SNGL(A4*TI(IORB)*SI(IORB))
      A3=A1*(SF(IORB)*SE-A5*CF(IORB))
      
!      write(6,*) A4,A5,TI(IORB),ctmw,stmw
!      write(6,*) tmw

      B(I_PERIOD+ELINDX(IORB))=SNGL(A3*DT(IORB)*RO)
      B(I_T0+ELINDX(IORB))=SNGL(-A3*PMU(IORB)*RO)
C     B(I_Ecc+ELINDX(IORB))=-RO*A1*
C     +     ((CE-SF(IORB))*CF(IORB)+
C     +     A4*(A2+CF2(IORB))*SE)/CF(IORB)
      B(I_Ecc+ELINDX(IORB))=
     +     SNGL(-RO*A1*((CE-SF(IORB))*CF(IORB)+
     +     A4*TI(IORB)*SI(IORB)*
     +     (A2+CF2(IORB))*SE)/CF(IORB))
      B(I_as+ELINDX(IORB))=RO/A(IORB)
      B(I_OMEGA_L+ELINDX(IORB))=-A5*RO
      B(I_i+ELINDX(IORB))=SNGL(-TI(IORB)*STMW*STMW*RO)
      RETURN
      END

      SUBROUTINE GRAD_VA(IORB,VA,B)
C B(j)=dVA/dEL(j), RES1=VA
      INTEGER IORB   ! Orbit number
      REAL VA        ! Radial velocity of primary star (of current orbit),
                     ! relative to its barycenter
      REAL B(*)      ! Gradient of the radial velocity, relative to the 
                     ! orbital elements of the current orbit
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL A1
      REAL CV,SV
*
      SV=SIN(V(IORB))
      CV=COS(V(IORB))
!      if (cv.ne.cv) then
!         write(6,*) 'V ',V(IORB)
!      endif
      A1=(1.+SF(IORB)*CV)**2*SU(IORB)/CF3(IORB)
!      if (a1.ne.a1) then ! Watch for NaNs
!         write(6,*) 'A1!=A1',SF(IORB), CV, SU(IORB), CF3(IORB)
!      endif
*
*     B(j)=dVA/dEL(j), RES1=VA
      B(I_PERIOD+ELINDX(IORB))=SNGL(-A1*DT(IORB)*K1(IORB))
      B(I_T0+ELINDX(IORB))=SNGL(A1*PMU(IORB)*K1(IORB))
      B(I_Ecc+ELINDX(IORB))=(CW(IORB)-
     +     SU(IORB)*SV*(2.+SF(IORB)*CV)/CF2(IORB))*
     +     K1(IORB)
      B(I_OMEGA_L+ELINDX(IORB))=(-SU(IORB)-SF(IORB)*SW(IORB))*K1(IORB)
      B(I_K1+ELINDX(IORB))=CU(IORB)+SF(IORB)*CW(IORB)
      VA=+K1(IORB)*B(I_K1+ELINDX(IORB))
! Something badly wrong, usually because excentriciy got too close to 1...
      IF (VA.NE.VA.AND..NOT.SILENT) THEN ! 
         WRITE(6,*) 'W-KEPLER, VA is a NaN for orbit ',IORB
         WRITE(6,*) 'V:',V(IORB),'  A1:',A1,' K1:',K1
      ENDIF
      RETURN
      END

      SUBROUTINE GRAD_VB(IORB,VB,B)
      INTEGER IORB   ! Orbit number
      REAL VB        ! Radial velocity of primary star (in current orbit)
      REAL B(*)      ! Gradient of the radial velocity, relative to the 
                     ! orbital elements of the current orbit
C B(j)=dVB/dEL(j), RES1=VB
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL A1
      REAL CV,SV
*
      SV=SIN(V(IORB))
      CV=COS(V(IORB))
      A1=(1.+SF(IORB)*CV)**2*SU(IORB)/CF3(IORB)

*  B(j)=dVB/dEL(j), RES1=VB
      B(I_PERIOD+ELINDX(IORB))=SNGL(A1*DT(IORB)*K2(IORB))
      B(I_T0+ELINDX(IORB))=SNGL(-A1*PMU(IORB)*K2(IORB))
      B(I_Ecc+ELINDX(IORB))=(-CW(IORB)+
     +     SU(IORB)*SV*(2.+SF(IORB)*CV)/CF2(IORB))
     +     *K2(IORB)
      B(I_OMEGA_L+ELINDX(IORB))=(SU(IORB)+SF(IORB)*SW(IORB))*K2(IORB)
      B(I_K2+ELINDX(IORB))=-CU(IORB)-SF(IORB)*CW(IORB)  
      VB=+K2(IORB)*B(I_K2+ELINDX(IORB))
      IF (VB.NE.VB.AND..NOT.SILENT) THEN ! Something wrong...
         WRITE(6,*) 'W-KEPLER, VB is a NaN for orbit ',IORB
         WRITE(6,*) 'V:',V(IORB),' A1:',A1,' K1:',K1
      ENDIF
      RETURN
      END

      SUBROUTINE KEPLER_ALPHADELTA(DALPH,DDELT)
      REAL DALPH,DDELT
* Small wrapper routine that provides Delta(RA) and Delta(Dec) from (X,Y)
* (with the X North, Y East binary star conventions). This is just to 
* keep those axes convention conversions at one single place.
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      Integer iorb
      parameter (iorb=1)
      REAL XX,YY
*
      CALL KEPLER_XY(XX,YY)
      DALPH=-YY
      DDELT=+XX
      RETURN
      END

      SUBROUTINE KEPLER_XY(XX,YY)
      REAL XX,YY
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      Integer iorb
      parameter (iorb=1)
*
      REAL CV
      REAL X,Y,R
*
      CV=COS(V(IORB))
      R=CF2(IORB)/(1.+SF(IORB)*CV)
      X=R*CV
      Y=R*SIN(V(IORB))
      XX=AA(IORB)*X+FF(IORB)*Y
      YY=BB(IORB)*X+GG(IORB)*Y
*
      RETURN
      END

      SUBROUTINE KEPLER_PXY(XX,YY)
      REAL XX,YY
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      Integer iorb
      parameter (iorb=1)
*
      Integer SIGN
      PARAMETER (SIGN=-1)
*
      CALL KEPLER_XY(XX,YY)
* Assume zero light from the secundary, and so just rescale the relative
* orbit by its fractional mass.
      XX = SIGN*XX*K1(IORB)/(K1(IORB)+K2(IORB))
      YY = SIGN*YY*K1(IORB)/(K1(IORB)+K2(IORB))
      RETURN
      END

      SUBROUTINE GRAD_X(XX,B)
C---X, dx/d(EL(J)) 
      REAL XX,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
!      real b_rho(ma),b_theta(ma) ! Pour une verification, a enlever ensuite...
!      real rho,theta ! idem
      include 'pi.inc'
*
      Integer iorb
      parameter (iorb=1)
*
      INTEGER I
      REAL A1
      REAL AA1,AA2,AA3,FF1,FF2,FF3
      REAL CE1,SE1,CV
      REAL X,Y,R
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      CV=COS(V(IORB))
      R=CF2(IORB)/(1.+SF(IORB)*CV)
      X=R*CV
      Y=R*SIN(V(IORB))
      XX=AA(IORB)*X+FF(IORB)*Y

      CE1=COS(E1(IORB))
      SE1=SIN(E1(IORB))
      A1=1-SF(IORB)*CE1
      B(I_PERIOD+ELINDX(IORB))=SNGL((AA(IORB)*(-SE1)+
     +     FF(IORB)*CE1*CF(IORB))*DT(IORB)/A1)
      B(I_T0+ELINDX(IORB))=SNGL((AA(IORB)*SE1-
     +     FF(IORB)*CE1*CF(IORB))*PMU(IORB)/A1)
      B(I_Ecc+ELINDX(IORB))=AA(IORB)*(-SE1*SE1/A1 -1)+
     +     FF(IORB)*(-SF(IORB)*SE1/CF(IORB)+
     +     CF(IORB)*CE1*SE1/A1)
      B(I_as+ELINDX(IORB))=XX/A(IORB)
      AA1=A(IORB)*
     +     SNGL(-CW(IORB)*SWW(IORB)-SW(IORB)*CWW(IORB)*CI(IORB))
      FF1=A(IORB)*
     +     SNGL(SW(IORB)*SWW(IORB)-CW(IORB)*CWW(IORB)*CI(IORB))
! Swap astro/trigo angle convention
!      B(I_OMEGA_U+ELINDX(IORB))=-(AA1*X+FF1*Y)
      B(I_OMEGA_U+ELINDX(IORB))=+(AA1*X+FF1*Y)
      AA2=A(IORB)*SNGL(-SW(IORB)*CWW(IORB)-
     +     CW(IORB)*SWW(IORB)*CI(IORB))
      FF2=A(IORB)*SNGL(-CW(IORB)*CWW(IORB)+
     +     SW(IORB)*SWW(IORB)*CI(IORB))
      B(I_OMEGA_L+ELINDX(IORB))=AA2*X+FF2*Y
      AA3=A(IORB)*SW(IORB)*SWW(IORB)*SNGL(SI(IORB))
      FF3=A(IORB)*CW(IORB)*SWW(IORB)*SNGL(SI(IORB))
      B(I_i+ELINDX(IORB))=AA3*X+FF3*Y
!
!      XX = -XX
!      DO I=1,MA
!         B(I) = -B(I)
!      ENDDO
!* Test consistency between rho,theta and x,y gradients
!      call grad_rho(rho,b_rho)
!      call grad_theta(theta,b_theta)
!      write(6,*) 'XX',xx, sngl(rho*cos(theta*pi/180.)),
!     +     (xx-sngl(rho*cos(theta*pi/180.)))/epsr4
!      do i=2,8
!         write(6,*) '   ',i,b(i), 
!     +        sngl(b_rho(i)*cos(theta*pi/180.)
!     +        -rho*pi/180.*sin(theta*pi/180)*b_theta(i))
!         b(i) = sngl(b_rho(i)*cos(theta*pi/180.)
!     +        -rho*pi/180.*sin(theta*pi/180)*b_theta(i))
!      enddo
      RETURN
      END

      SUBROUTINE GRAD_Y(YY,B)
C------Y,dy/dEL(J)--------------------
      REAL YY,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      Integer iorb
      parameter (iorb=1)
*
      INTEGER I
      REAL A1
      REAL BB1,BB2,BB3,GG1,GG2,GG3
      REAL SE1,CE1,CV
      REAL X,Y,R
*
*
!      real b_rho(ma),b_theta(ma) ! Pour une verification, a enlever ensuite...
!      real rho,theta ! idem
      include 'pi.inc'
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      CV=COS(V(IORB))
      R=CF2(IORB)/(1.+SF(IORB)*CV)
      X=R*CV
      Y=R*SIN(V(IORB))
      YY=BB(IORB)*X+GG(IORB)*Y
      CE1=COS(E1(IORB))
      SE1=SIN(E1(IORB))
      A1=1-SF(IORB)*CE1
      B(I_PERIOD+ELINDX(IORB))=(BB(IORB)*(-SE1)+
     +     GG(IORB)*CE1*CF(IORB))*DT(IORB)/A1
      B(I_T0+ELINDX(IORB))=(BB(IORB)*SE1-
     +     GG(IORB)*CE1*CF(IORB))*PMU(IORB)/A1
      B(I_Ecc+ELINDX(IORB))=BB(IORB)*(-SE1*SE1/A1 -1)+
     +     GG(IORB)*(-SF(IORB)*SE1/CF(IORB)+
     +     CF(IORB)*CE1*SE1/A1)
      B(I_as+ELINDX(IORB))=YY/A(IORB)
      BB1=A(IORB)*
     +     (CW(IORB)*CWW(IORB)-SW(IORB)*SWW(IORB)*CI(IORB))
      GG1=A(IORB)*
     +     (-SW(IORB)*CWW(IORB)-CW(IORB)*SWW(IORB)*CI(IORB))
! Swap astro/trigo angle convention
      B(I_OMEGA_U+ELINDX(IORB))=+(BB1*X+GG1*Y)
!      B(I_OMEGA_U+ELINDX(IORB))=-(BB1*X+GG1*Y)
      BB2=A(IORB)*
     +     (-SW(IORB)*SWW(IORB)+CW(IORB)*CWW(IORB)*CI(IORB))
      GG2=A(IORB)*
     +     (-CW(IORB)*SWW(IORB)-SW(IORB)*CWW(IORB)*CI(IORB))
      B(I_OMEGA_L+ELINDX(IORB))=BB2*X+GG2*Y
      BB3=-A(IORB)*SW(IORB)*CWW(IORB)*SI(IORB)
      GG3=-A(IORB)*CW(IORB)*CWW(IORB)*SI(IORB)
      B(I_i+ELINDX(IORB))=BB3*X+GG3*Y
!
!* Test consistency between rho,theta and x,y gradients
!      call grad_rho(rho,b_rho)
!      call grad_theta(theta,b_theta)
!      write(6,*) 'YY',yy, sngl(rho*sin(theta*pi/180.)),
!     +     (yy-sngl(rho*sin(theta*pi/180.)))/epsr4
!      do i=2,8
!         write(6,*) '   ',i, b(i), 
!     +        sngl(b_rho(i)*sin(theta*pi/180.)
!     +        +rho*pi/180.*cos(theta*pi/180)*b_theta(i))
!         b(i) = sngl(b_rho(i)*sin(theta*pi/180.)
!     +        +rho*pi/180.*cos(theta*pi/180)*b_theta(i))
!      enddo
      RETURN
      END

      SUBROUTINE GRAD_PX(XX,B)
C---X, dx/d(EL(J)) 
      REAL XX,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
      REAL RB(MA)
      Integer iorb
      parameter (iorb=1)
      Integer SIGN
      PARAMETER (SIGN=-1)
*
*
      CALL GRAD_X(XX,RB)
* Assume zero light, and just rescale relative orbit by fractional mass of 2
      XX = SIGN*XX*K1(IORB)/(K1(IORB)+K2(IORB))
* Rescale the gradient too
      DO I=1,MA
         B(I) = SIGN*RB(I)*K1(IORB)/(K1(IORB)+K2(IORB))
      ENDDO
* Add contribution from derivative of K1/(K1+K2) to K1 and K2 derivatives
      B(I_K1+ELINDX(IORB))=B(I_K1+ELINDX(IORB))
     +     +SIGN*XX*K2(IORB)/(K1(IORB)+K2(IORB))**2
      B(I_K2+ELINDX(IORB))=B(I_K2+ELINDX(IORB))
     +     -SIGN*XX*K1(IORB)/(K1(IORB)+K2(IORB))**2

      RETURN
      END

      SUBROUTINE GRAD_PY(YY,B)
C------Y,dy/dEL(J)--------------------
      REAL YY,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL RB(MA)
      INTEGER I
      Integer iorb
      parameter (iorb=1)
      Integer SIGN
      PARAMETER (SIGN=-1)
*
      CALL GRAD_Y(YY,RB)
* Assume zero light, and just rescale relative orbit by fractional mass of 2
      YY = SIGN*YY*K1(IORB)/(K1(IORB)+K2(IORB))
* Rescale the gradient too
      DO I=1,MA
         B(I) = SIGN*RB(I)*K1(IORB)/(K1(IORB)+K2(IORB))
      ENDDO
* Add contribution from derivative of K1/(K1+K2) to K1 and K2 derivatives
      B(I_K1+ELINDX(IORB))=B(I_K1+ELINDX(IORB))
     +     +SIGN*YY*K2(IORB)/(K1(IORB)+K2(IORB))**2
      B(I_K2+ELINDX(IORB))=B(I_K2+ELINDX(IORB))
     +     -SIGN*YY*K1(IORB)/(K1(IORB)+K2(IORB))**2

      RETURN
      END
      
      SUBROUTINE KEPLER_PARPOSPM(PIFX,PIFY,XX,YY)
      REAL*8 PIFX,PIFY
      REAL XX,YY
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      REAL YEAR
*
!      write(6,*) pifx,pify
!      write(6,*) year,mu_x,Mu_Y
!      write(6,*) year
      YEAR = DT_EPOCH/365.25
!      XX = X0+Mu_X*YEAR+PIFX*PAR
!      YY = Y0+Mu_Y*YEAR+PIFY*PAR
      XX = X0+Mu_X*YEAR+PIFX*PAR
      YY = Y0+Mu_Y*YEAR+PIFY*PAR
      RETURN
      END

      SUBROUTINE GRAD_AX(PIFX,XX,B)
      REAL*8 PIFX
      REAL XX,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      Integer iorb
      parameter (iorb=1)
      REAL YEAR
      INTEGER I
*
* Call the PM and zero-point-less routine 
      CALL GRAD_PX(XX,B)
* Then fill in the values for the additional parameters
      YEAR = DT_EPOCH/365.25
      XX = XX+X0+Mu_X*YEAR+PIFX*PAR
      B(I_X0)  =+1
      B(I_MuX)=YEAR
      DO I=1,MA
         B(I)=B(I)+GRAD_PI(I)*PIFX
      ENDDO
      RETURN
      END
*
      SUBROUTINE GRAD_AY(PIFY,YY,B)
      REAL*8 PIFY
      REAL YY,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      Integer iorb
      parameter (iorb=1)
      REAL YEAR
      INTEGER I
*
* Call the PM and zero point-less routine 
      CALL GRAD_PY(YY,B)
* Then add in the rest
      YEAR = DT_EPOCH/365.25
!      write(6,*) pify,par,pify*par
      YY = YY+Y0+Mu_Y*YEAR+PIFY*PAR
      B(I_Y0)  =+1
      B(I_MuY)=YEAR
      DO I=1,MA
         B(I)=B(I)+GRAD_PI(I)*PIFY
      ENDDO
*
      RETURN
      END

      SUBROUTINE GRAD_LOGMTOT(B)
C-COMPUTE dMTOT/(MTOT*dEL(J))
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      Integer iorb
      parameter (iorb=1)
*
      INTEGER I
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=-1./PMU(IORB)
      B(I_Ecc+ELINDX(IORB))=-3*SF(IORB)/CF2(IORB)
      B(I_i+ELINDX(IORB))=-3*CI(IORB)/SI(IORB)
      B(I_K1+ELINDX(IORB))=3/(K1(IORB)+K2(IORB))
      B(I_K2+ELINDX(IORB))=3/(K1(IORB)+K2(IORB))
      RETURN
      END

      SUBROUTINE GRAD_LOGM1(B)
C-COMPUTE MASS A,dMAA/(MAA*dEL(J))
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
      Integer iorb
      parameter (iorb=1)
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=-1./PMU(IORB)
      B(I_Ecc+ELINDX(IORB))=-3.*SF(IORB)/CF2(IORB)
      B(I_i+ELINDX(IORB))=-3.*CI(IORB)/SI(IORB)
      B(I_K1+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))
      B(I_K2+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))+1./K2(IORB)
      RETURN
      END

      SUBROUTINE GRAD_LOGM1SIN3I(B)
C-COMPUTE MASS A,dMAA/(MAA*dEL(J))
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
      Integer iorb
      parameter (iorb=1)
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=-1./PMU(IORB)
      B(I_Ecc+ELINDX(IORB))=-3.*SF(IORB)/CF2(IORB)
      B(I_K1+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))
      B(I_K2+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))+1./K2(IORB)
      RETURN
      END

      SUBROUTINE GRAD_LOGM2(B)
C-COMPUTE MASS B,dMAB/(MAB*dEL(J))
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
      Integer iorb
      parameter (iorb=1)
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=-1./PMU(IORB)
      B(I_Ecc+ELINDX(IORB))=-3.*SF(IORB)/CF2(IORB)
      B(I_i+ELINDX(IORB))=-3*CI(IORB)/SI(IORB)
      B(I_K1+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))+1./K1(IORB)
      B(I_K2+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))
      RETURN
      END

      SUBROUTINE GRAD_LOGM2SIN3I(B)
C-COMPUTE MASS B,dMAB/(MAB*dEL(J))
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
      Integer iorb
      parameter (iorb=1)
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=-1./PMU(IORB)
      B(I_Ecc+ELINDX(IORB))=-3.*SF(IORB)/CF2(IORB)
      B(I_K1+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))+1./K1(IORB)
      B(I_K2+ELINDX(IORB))=2./(K1(IORB)+K2(IORB))
      RETURN
      END


      SUBROUTINE GRAD_PARALLAX(PPAR,B)
*---------------------------------------------------------------------------
* Orbit:
* Compute an orbital parallax and its gradient relative to the 
* current elements. 
*---------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      REAL PPAR,B(MA)
*
      INCLUDE 'pi.inc'
      INCLUDE 'physical_constants.inc'
      INCLUDE 'kepler.inc'
*
      INTEGER I
* Parallax and its gradient are pre-computed and just need copying
      PPAR = PAR 
      DO I=1,MA
         B(I)=GRAD_PI(I)
      ENDDO
      END


