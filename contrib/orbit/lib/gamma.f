      FUNCTION GAMMQ(A,X,ERROR)
      REAL*4 GAMMQ
      REAL*4 A,X
      LOGICAL ERROR
*
      REAL*4 GAMSER,GLN,GAMMCF
*
      IF(X.LT.0..OR.A.LE.0.) THEN
         ERROR = .TRUE.
         RETURN
      ENDIF
      IF(X.LT.A+1.)THEN
        CALL GSER(GAMSER,A,X,GLN,ERROR)
        GAMMQ=1.-GAMSER
      ELSE
        CALL GCF(GAMMCF,A,X,GLN,ERROR)
        GAMMQ=GAMMCF
      ENDIF
      RETURN
      END
C<FF>
      SUBROUTINE GCF(GAMMCF,A,X,GLN,ERROR)
      REAL*4 GAMMCF,A,X,GLN
      LOGICAL ERROR
*
      INTEGER ITMAX
      REAL*4 EPS
      PARAMETER (EPS=3.E-7)
      REAL*4 GOLD,A0,A1,B0,B1,FAC,AN,ANF,ANA,GAMMLN,G
      INTEGER N
*
      ITMAX = 10*SQRT(A)
      GLN=GAMMLN(A)
      GOLD=0.
      A0=1.
      A1=X
      B0=0.
      B1=1.
      FAC=1.
      DO 11 N=1,ITMAX
        AN=FLOAT(N)
        ANA=AN-A
        A0=(A1+A0*ANA)*FAC
        B0=(B1+B0*ANA)*FAC
        ANF=AN*FAC
        A1=X*A0+ANF*A1
        B1=X*B0+ANF*B1
        IF(A1.NE.0.)THEN
          FAC=1./A1
          G=B1*FAC
          IF(ABS((G-GOLD)/G).LT.EPS) GO TO 1
          GOLD=G
        ENDIF
11    CONTINUE
!      PAUSE 'A too large, ITMAX too small'
      ERROR = .TRUE.
      RETURN
1     GAMMCF=EXP(-X+A*ALOG(X)-GLN)*G
      RETURN
      END
C<FF>
      SUBROUTINE GSER(GAMSER,A,X,GLN,ERROR)
      REAL*4 GAMSER,A,X,GLN
      LOGICAL ERROR
*
      INTEGER ITMAX,N
      REAL*4 EPS
      PARAMETER (EPS=3.E-7)
      REAL*4 GAMMLN,SUM,DEL,AP
*
      ITMAX = 10*SQRT(A)
      GLN=GAMMLN(A)
      IF(X.LE.0.)THEN
        IF(X.LT.0.) THEN
           ERROR = .TRUE.
           RETURN
        ENDIF
        GAMSER=0.
        RETURN
      ENDIF
      AP=A
      SUM=1./A
      DEL=SUM
      DO 11 N=1,ITMAX
        AP=AP+1.
        DEL=DEL*X/AP
        SUM=SUM+DEL
        IF(ABS(DEL).LT.ABS(SUM)*EPS)GO TO 1
11    CONTINUE
!      PAUSE 'A too large, ITMAX too small'
      ERROR = .TRUE.
      RETURN
1     GAMSER=SUM*EXP(-X+A*LOG(X)-GLN)
      RETURN
      END
C<FF>
      FUNCTION GAMMLN(XX)
      REAL*4 GAMMLN,XX
*
      INTEGER J
      REAL*8 COF(6),STP,HALF,ONE,FPF,X,TMP,SER
      DATA COF,STP/76.18009173D0,-86.50532033D0,24.01409822D0,
     *    -1.231739516D0,.120858003D-2,-.536382D-5,2.50662827465D0/
      DATA HALF,ONE,FPF/0.5D0,1.0D0,5.5D0/
      X=XX-ONE
      TMP=X+FPF
      TMP=(X+HALF)*LOG(TMP)-TMP
      SER=ONE
      DO 11 J=1,6
        X=X+ONE
        SER=SER+COF(J)/X
11    CONTINUE
      GAMMLN=TMP+LOG(STP*SER)
      RETURN
      END
C<FF>
      FUNCTION ERFCC(X)
      REAL*4 ERFCC,X
*
      REAL*4 Z,T
*
      Z=ABS(X)      
      T=1./(1.+0.5*Z)
      ERFCC=T*EXP(-Z*Z-1.26551223+T*(1.00002368+T*(.37409196+
     *    T*(.09678418+T*(-.18628806+T*(.27886807+T*(-1.13520398+
     *    T*(1.48851587+T*(-.82215223+T*.17087277)))))))))
      IF (X.LT.0.) ERFCC=2.-ERFCC
      RETURN
      END
