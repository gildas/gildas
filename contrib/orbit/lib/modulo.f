!      program test
!      integer i
!      real r
!      do i=-4,5
!         r = i+0.5
!         write(6,*) r,modulo(r,2.),dmodulo(r*1.d0,2.d0),
!     +        i,imodulo(i,2)
!      enddo
!      end

      FUNCTION MODULO(A,B)
      REAL*4 MODULO,A,B
      IF (B.LE.0) THEN
         WRITE(6,*) 'F-MODULO,  ',
     +        'Not programmed for a negative second argument'
      ENDIF
      MODULO = MOD(A,B)
      IF (MODULO.LT.0) THEN
         MODULO = MODULO+B
      ENDIF
      END

      FUNCTION DMODULO(A,B)
      REAL*8 DMODULO,A,B
      IF (B.LE.0) THEN
         WRITE(6,*) 'F-MODULO,  ',
     +        'Not programmed for a negative second argument'
      ENDIF
      DMODULO = MOD(A,B)
      IF (DMODULO.LT.0) THEN
         DMODULO = DMODULO+B
      ENDIF
      END

      FUNCTION IMODULO(A,B)
      INTEGER IMODULO,A,B
      IF (B.LE.0) THEN
         WRITE(6,*) 'F-MODULO,  ',
     +        'Not programmed for a negative second argument'
      ENDIF
      IMODULO = MOD(A,B)
      IF (IMODULO.LT.0) THEN
         IMODULO = IMODULO+B
      ENDIF
      END
