* PI is defined with more digits than necessary to avoid losing
* the last few bits in the decimal to binary conversion
      REAL*8 PI
      PARAMETER (PI=3.14159265358979323846D0)
      REAL*4 PIS
      PARAMETER (PIS=3.141592653)
      REAL*8 PI2 
      PARAMETER (PI2=2.D0*PI)
      REAL*8 GR  ! Radian to degree conversion factor
      PARAMETER  (GR=180.D0/PI)
 
* Relative precision of REAL*4
      REAL*4 EPSR4
      PARAMETER (EPSR4=1E-7)
      REAL*4 MAXR4
      PARAMETER (MAXR4=3.1E+38)
* Relative precision of REAL*8
      REAL*8 EPSR8
      PARAMETER (EPSR8=5D-20)
      REAL*8 MAXR8
      PARAMETER (MAXR8=1.5D+308)
* Maximum acceptable integer
      INTEGER MAX_INTEG
      PARAMETER (MAX_INTEG=2147483647)
