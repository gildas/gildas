      SUBROUTINE GLINSERT(LINE,ERROR)
      USE GKERNEL_INTERFACES
C insert data from the Gliese catalog
      CHARACTER LINE*(*)
      logical error
*
      INCLUDE 'pi.inc'
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
!      INCLUDE 'file.inc'
*
!      LOGICAL SIC_PRESENT
!      LOGICAL OPTION
      CHARACTER*20    GLNAME,NAME
      CHARACTER*80    FILE
      CHARACTER*183  STRING
      INTEGER LUN,STATUS
      INTEGER LG,LN
      INTEGER CODE,IER
      LOGICAL FOUND
*
      FOUND = .FALSE.
      FILE = 'GLIESE_CATALOG'
      IER = SIC_GETLOG(FILE)
      STATUS = SIC_GETLUN (LUN)
      OPEN(UNIT=LUN, FILE=FILE,STATUS='OLD',FORM='FORMATTED',ERR=99)
      GLNAME=object(1)
      write(*,11) GLNAME
 11   format (1x,a10)
      DO WHILE (.NOT.FOUND)
         read(lun,'(A183)',end=98) string
         name = string(1:10)
         LG = LEN(GLNAME)
         CALL NO_SPACE(GLNAME,LG)
         LN = LEN(NAME)
         CALL NO_SPACE(NAME,LN)
         write(6,*) name,glname
         IF (NAME.EQ.GLNAME) THEN
            STRING(15:15) = ':'
            STRING(18:18) = ':'
            CALL SIC_SEXA(STRING(13:20),8,RA,ERROR)
            IF (ERROR) THEN
               WRITE(6,*) 'Invalid RA string',STRING(13:20)
               GOTO 99
            ENDIF
            RA = RA*PI/12.
            STRING(25:25) = ':'
            CALL SIC_SEXA(STRING(22:29),8,DEC,ERROR)
            IF (ERROR) THEN
               WRITE(6,*) 'Invalid Dec string',STRING(22:29)
               GOTO 99
            ENDIF
            DEC = DEC*PI/180.
            EQUINOX=1950.0
            SPECTRAL_TYPE(1) = STRING(55:66)
* V magnitude
            CALL FIND_PHOT_CODE('V',CODE,ERROR)
            IF (.NOT.ERROR) THEN
               READ(STRING(68:73),'(F6.2)') MAGNITUDE(CODE)
            ENDIF
            NPARA =1
            READ(STRING(109:114),'(F6.1)') PARALLAX(NPARA)
            PARALLAX(NPARA)=PARALLAX(NPARA)*1E-3 ! Convert mas to arcseconds
            READ(STRING(115:119),'(F5.1)') PARERR(NPARA)
            PARERR(NPARA)=PARERR(NPARA)*1E-3 ! Convert mas to arcseconds
            write (*,102) PARALLAX(NPARA),PARERR(NPARA)
 102        format(1x,'parallax= ',f6.4,' +-',f5.4)
            FOUND=.TRUE.
         ENDIF
      ENDDO
      CLOSE(LUN)        
      RETURN
*

 98   WRITE (*,*) 'Star ',GLNAME,' not found in Gliese catalog'
      npara = 0
      RETURN
 99   WRITE (*,*) 'Error opening Gliese catalog ',FILE
      END
