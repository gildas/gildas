!      PROGRAM TEST
!      INCLUDE 'astro.inc'
!*
!      REAL*8 JDATE,RRA,DDEC
!      REAL*4 EQUINOX
!      REAL PIFX,PIFY
!      LOGICAL ERROR
!*
!
!      RRA  = 0.0
!      DDEC = 0.0
!      EQUINOX=2000.0
!      EQUINOX=2002.0
!      JDATE =  52275.500        ! 01-JAN-2002 00:00 UT
!      CALL PARALLAX_FACTORS(JDATE,RRA,DDEC,EQUINOX,PIFX,PIFY,ERROR)
!      STOP 
! 99   STOP 'ERROR'
!      END


*
      SUBROUTINE PARALLAX_FACTORS(MJDATE,RRA,DDEC,EQUINOX,
     +     PIFX,PIFY,ERROR)
      REAL*8 MJDATE,RRA,DDEC
      REAL*4 EQUINOX
      REAL*8 PIFX,PIFY
      LOGICAL ERROR
*
      INCLUDE 'physical_constants.inc'
      REAL*8 J2000
      PARAMETER (J2000 = 2451545.0D0)
*
      REAL*8 CRA,CDEC,SRA,SDEC ! Trigonometric functions of RA+Dec
*
      REAL*8 JULDAY
      REAL*8 JNOW_TDT                    ! date julienne TDT
      REAL*8 D_TDT                       ! TDT - UTC (seconds)
      REAL*8 XG_0(3), XG_1(3)
      REAL*8 ANGLES(6), PSI, THE, PHI, JFIX, EPSM
      REAL*8 MAT1(9), MAT2(9) !, MAT3(9)
      REAL*8 TRFM_01(9), TRFM_10(9)
      INTEGER I
*
      REAL PREV_EQU
      LOGICAL INITIALISED
      DATA INITIALISED /.FALSE./, PREV_EQU /-1E35/
      SAVE INITIALISED, PREV_EQU, TRFM_01, TRFM_10
*
      REAL*8 OBLIMO   ! Function
*  
* Initialize the VSOP87 ephemerids   
      IF (.NOT.INITIALISED) THEN
         CALL EPHINI(ERROR)
         IF (ERROR) THEN
            CALL GAGOUT('F-ASTRO_INIT,  Error in EPHINI')
            GOTO 99
         ENDIF
         INITIALISED = .TRUE.
      ENDIF
*
*  Compute transformation matrix from equatorial (Equinox) to 
*  ecliptic (J2000.0), in which the VSOP87 ephemerids are stored. 
      IF (EQUINOX.NE.PREV_EQU) THEN
         JFIX=J2000+(EQUINOX-2000.0D0)*365.25D0 ! fixed equinox (julian days)
         EPSM=OBLIMO (JFIX)     ! mean obliquity at fixed equinox
         PSI=0.D0               ! Euler angles of transformation
         THE=EPSM
         PHI=0.D0
         CALL EULMAT (PSI,THE,PHI,MAT1) ! compute matrix from R1(EQ) to ecl (JFIX)
         CALL QPREC (JFIX,J2000,ANGLES) ! compute precession angles
         PSI=ANGLES(5)          ! Euler angles of transformation
         THE=ANGLES(4)
         PHI=-ANGLES(6)-ANGLES(5)
         CALL EULMAT (PSI,THE,PHI,MAT2) ! compute matrix from ecl (JFIX) to (R0)
         CALL MULMAT (MAT1,MAT2,TRFM_01) ! product TRFM01 = MAT2 x MAT1
         CALL TRANSP (TRFM_01, TRFM_10) ! We need the inverse/transpose
         PREV_EQU = EQUINOX
      ENDIF
*
* Transform time to standard julian day zero point
      JULDAY = MJDATE+2400000.
      D_TDT = 56.184D0
      JNOW_TDT = JULDAY+ D_TDT/86400.D0
*
* Get Earth motion (/ Sol Sys Barycenter)
      CALL EPHSTA (0, 0, ERROR)
      IF (ERROR) THEN
         CALL GAGOUT('F-PARALLAX,  Error in EPHSTA')
         RETURN
      ENDIF
      CALL EPHVEC (JNOW_TDT, 0, XG_0, ERROR)
      IF (ERROR) THEN
         CALL GAGOUT('E-PARALLAX, Error in solar system ephemeris')
      ENDIF
      DO I=1,3
         XG_0(I)=-XG_0(I)  ! We want G-->Earth, not Earth-->G
      ENDDO
*
* Rotate the position vector of the earth from Ecliptic-J2000 to the requested 
* equatorial frame.
      CALL MATVEC(XG_0, TRFM_10, XG_1)
      CRA  = COS(RRA)
      SRA  = SIN(RRA)
      CDEC = COS(DDEC)
      SDEC = SIN(DDEC)
!      PIFX = XG_1(1)*SRA-XG_1(2)*CRA
!      PIFY = XG_1(1)*CRA*SDEC+XG_1(2)*SRA*SDEC-XG_1(3)*CDEC
* X is along positive Declination, Y along positive RA, not xi-eta.
      PIFY = XG_1(1)*SRA-XG_1(2)*CRA
      PIFX = XG_1(1)*CRA*SDEC+XG_1(2)*SRA*SDEC-XG_1(3)*CDEC
      PIFX = PIFX/(au/km)
      PIFY = PIFY/(au/km)
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END


