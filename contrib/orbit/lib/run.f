      SUBROUTINE RUN_ORBIT (LINE, COMM, ERROR)
C----------------------------------------------------------------------
C	Commands for  orbit language
C
C	Context :
C
C	READ
C       VISU /VELOCITY
C       VISU /ORBIT
C       FIT
C       ADJUST
C       SAVE
C       LIST/DATA
C       LIST/EPHEM
C       GLIESE
C----------------------------------------------------------------------
      LOGICAL ERROR
      CHARACTER*(*) LINE, COMM
*
      if (comm.eq.'READ') then
         call read(line,error)
      elseif (comm.eq.'VISU') then
         call plot(line,error)
      elseif (comm.eq.'FIT') then
         call fit(line,error)
      elseif (comm.eq.'ADJUST') then
         call adjust(line,error)
      elseif (comm.eq.'SAVE') then
         call save(line,error)
      elseif (comm.eq.'LIST') then
         call orbit_list(line,error)
      elseif (comm.eq.'GLIESE') then
         call glinsert(line,error)
      elseif(comm.eq.'FIX') then
         call fix(line,error)
      elseif(comm.eq.'CURSOR') then
         call cursor(line,error)
      elseif(comm.eq.'PERIODOGRAM') then
         call PERIODOGRAM(line,error)
      elseif(comm.eq.'IGNORE') then
         call IGNORE(line,error)
      elseif(comm.eq.'USE') then
         call USE(line,error)
      elseif(comm.eq.'OPTIMISE') then
         call ORBIT_OPTIMISE(line,error)
      else
         WRITE(6,*) 'F-ORBIT, Unknown command ',comm
         goto 99
      endif
      return
*
 99   error = .true.
      return
      end
C<FF>
      SUBROUTINE EXEC_ORBIT(BUFFER)
      LOGICAL ERROR
      CHARACTER*(*) BUFFER
      CHARACTER LINE*255, COMM*12, LANG*12
      INTEGER STATUS, OCODE
*
      LINE = BUFFER
      STATUS = -1
      GOTO 10
*
      ENTRY PLAY_ORBIT(BUFFER)
      LINE = BUFFER
      STATUS = 2
      GOTO 10
*
      ENTRY ENTER_ORBIT
      STATUS = 1
      GOTO 10
*
* Return Ocode -1 : End of subroutine mode
*        Ocode  1 : End of interactive mode
 10   CALL SIC_RUN (LINE,LANG,COMM,ERROR,STATUS,OCODE)
      IF (OCODE.EQ.-1) RETURN
      IF (OCODE.EQ.1) RETURN
      STATUS = OCODE
*
      IF (LANG.EQ.'ORBIT') THEN
         CALL RUN_ORBIT (LINE,COMM,ERROR)
      ELSEIF (LANG.EQ.'SIC') THEN
         CALL RUN_SIC(LINE,COMM,ERROR)
      ELSEIF (LANG.EQ.'GREG1') THEN
         CALL RUN_GREG1 (LINE,COMM,ERROR)
      ELSEIF (LANG.EQ.'GREG2') THEN
         CALL RUN_GREG2 (LINE,COMM,ERROR)
      ELSEIF (LANG.EQ.'GTVL') THEN
         CALL RUN_GTVL(LINE,COMM,ERROR)
      ELSE
         WRITE(6,*) 'Unrecognized Language ',LANG
         ERROR = .TRUE.
      ENDIF
      GOTO 10
      RETURN
      END
