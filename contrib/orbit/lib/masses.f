      SUBROUTINE PRINT_MASSES(LUN,MONTECARLO,NSAMPLE,CONF_LEVEL,
     +           MC_EL,ERROR)
      USE GKERNEL_INTERFACES
* Print whichever astrophysical information can be deduced
* from the available orbital elements: total system mass, mass function,
* individual masses, projected semi-major axis....
      INCLUDE 'constant.inc'
      INTEGER LUN,NSAMPLE
      REAL*8 MC_EL(MA,NSAMPLE)
      REAL CONF_LEVEL
      LOGICAL MONTECARLO,ERROR
*
      INCLUDE 'elements.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc' ! Only needed to warn that a parallax could have 
                         ! been used to determine K2
*
      INTEGER I,LOW,HIGH
      REAL Mtot,dMtot,M_A,dM_A,M_B,dM_B
      REAL a1Sini, a2Sini   ! Projected semi major axes
      REAL f1m, f2m         ! Mass functions
      REAL M1sin3i, M2sin3i ! "projected masses"
      REAL dM1sin3i, dM2sin3i 
!      REAL a1, a2, a        ! Semi major axes
      REAL Orb_Par,dOrb_Par ! Orbital parallax and probable error
      REAL MAG_1,d_MAG1,MAG_2,dMAG_2
      REAL MASS_T(M_MC),MASS_A(M_MC),MASS_B(M_MC),PAR_ORB(M_MC)
      INTEGER IT(M_MC)  ! Unused sorting index
      INCLUDE 'physical_constants.inc'
*
*
      IF (SIG.GT.1.5.OR.SIG.LT.0.67) THEN
         IF (SIG.GT.1.5) THEN
            WRITE(6,'(A,A,F8.3)') 'WARNING: Standard errors have ',
     +           'been rescaled by a large reduced Chi:',SIG
         ELSEIF (SIG.GT.1.5.OR.SIG.LT.0.67) THEN
            WRITE(6,'(A,A,F8.3)') 'WARNING: Standard errors have ',
     +           'been rescaled by a small reduced Chi:',SIG
         ENDIF
            WRITE(6,'(A,A)') 'They maybe unreliable. '
            WRITE(6,'(A,A)') 'You should probably adjust your ',
     +        'measurement errors and repeat the fit'
      ENDIF
      IF (MONTECARLO) THEN
         DO I=1,NSAMPLE
            CALL COMPUTE_MASSES(MC_EL(1,I),MA,.FALSE.,
     +           a1sini,a2sini,f1m,f2m,
     +           M1sin3i,dM1sin3i,M2sin3i,dM2sin3i,
     +           MASS_T(I),dMtot,MASS_A(I),dM_A,
     +           MASS_B(I),dM_B,PAR_ORB(I),dOrb_Par,ERROR)
            IF (ERROR) GOTO 99
         ENDDO
*     Sort each item to determine confidence intervals
         LOW  = NINT(NSAMPLE*(1.-CONF_LEVEL)/2.)
         IF (LOW.EQ.1) THEN 
            WRITE(6,*) 'E-LIST, Number of montecarlo samples ',
     +           NSAMPLE,' is insufficient for requested confidence',
     +           ' level ',CONF_LEVEL
            GOTO 99
         ENDIF
         HIGH = NSAMPLE-LOW+1
      ENDIF
* Compute best estimate, and linearized standard errors
      CALL COMPUTE_MASSES(EL,MA,.TRUE.,a1sini,a2sini,f1m,f2m,
     +     M1sin3i,dM1sin3i,M2sin3i,dM2sin3i,
     +     Mtot,dMtot,M_A,dM_A,M_B,dM_B,
     +     Orb_Par,dOrb_Par,ERROR)
      IF (ERROR) GOTO 99
*     Projected semi-major axes and mass functions
      IF(EL(I_K1+ELINDX(IORB0)).NE.0) THEN       ! K1 is known
         WRITE(LUN,1010) 'a1 sin(i) = ',a1Sini/au,'au',
     +        NINT(a1Sini/km),'km',a1sini/Rsol,'Solar radii'
         WRITE(LUN,1020) 'f1(M) = ',f1m/Msol,'Solar masses'
     +        ,f1m*1E-3,'kg'
 1010    FORMAT(A,F8.5,1X,A,1X,I9,1X,A,1X,F7.2,1X,A)
 1020    FORMAT(A,G9.3,1X,A,1X,G10.3,1X,A)
      ENDIF
      IF(EL(I_K2+ELINDX(IORB0)).NE.0) THEN       ! K2 is known
         WRITE(LUN,1010) 'a2 sin(i) = ',a2Sini/au,'au',
     +        NINT(a2Sini/km),'km',a2sini/Rsol,'Solar radii'
         WRITE(LUN,1020) 'f2(M) = ',f2m/Msol,'Solar masses'
     +        ,f2m*1e-3,'kg'
      ENDIF
*     SB2 binary: print both projected masses
      IF((EL(I_K1+ELINDX(IORB0))*EL(I_K2+ELINDX(IORB0))).NE.0) THEN  
         WRITE(LUN,400) 'M1Sin3i = ',M1sin3i/Msol,dM1sin3i/Msol,
     +        100*dM1sin3i/M1sin3i
         WRITE(LUN,400) 'M2Sin3i = ',M2sin3i/Msol,dM2sin3i/Msol,
     +        100*dM2sin3i/M2sin3i
!         WRITE(LUN,1020) 'M1Sin3i = ',M1sin3i/Msol,'Solar masses'
!                                !     +        ,M1sin3i*1e-3,'kg'
!         WRITE(LUN,1020) 'M2Sin3i = ',M2sin3i/Msol,'Solar masses'
!                                !     +        ,M2sin3i*1e-3,'kg'
*     
*     Inclination is known and non zero: print everything
         IF (SIN(EL(I_i+ELINDX(IORB0))/GR).NE.0) THEN 
            IF (MONTECARLO) THEN
               CALL GR4_TRIE(MASS_T,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
               WRITE(LUN,501) Mtot,MASS_T(LOW),MASS_T(HIGH)
 501           FORMAT('Total Mass =  ',F6.3,1X,'[',F6.3,',',
     +              F6.3,']',' Solar masses')
*     Individual masses
               CALL GR4_TRIE(MASS_A,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
               WRITE(LUN,502) M_A,MASS_A(LOW),MASS_A(HIGH)
 502           FORMAT('Mass A =  ',F6.3,1X,'[',F6.3,',',
     +              F6.3,']',' Solar masses')
               CALL GR4_TRIE(MASS_B,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
               WRITE(LUN,503) M_B,MASS_B(LOW),MASS_B(HIGH)
 503           FORMAT('Mass B =  ',F6.3,1X,'[',F6.3,',',
     +              F6.3,']',' Solar masses')
*     Orbital parallax
               CALL GR4_TRIE(PAR_ORB,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
               WRITE(LUN,504) 'Parallax is ',Orb_Par,
     +              PAR_ORB(LOW),PAR_ORB(HIGH)
 504           FORMAT(A,F6.4,1X,'[',F5.4,',',F5.4,']')
            ELSE
               WRITE(LUN,400) 'Total Mass =',Mtot,dMtot,100*dMtot/Mtot
 400           FORMAT(A,1X,F9.6,1X,'+-',F8.6,1X,
     +              'Solar masses',1X,'(',F4.1,'%',')')
*     Individual masses
               WRITE(LUN,400) 'Mass A =',M_A,dM_A,100*dM_A/M_A
               WRITE(LUN,400) 'Mass B =',M_B,dM_B,100*dM_B/M_B
*     Orbital parallax
               write(LUN,410) 'Parallax is ',Orb_Par,dOrb_Par,
     +              100*dOrb_Par/Orb_Par
 410           FORMAT(A,F6.4,1X,'+-',F5.4,'"',1X,'(',F4.1,'%)')
            ENDIF
* Absolute magnitudes
            IF (NPHOT.GT.0) THEN
               WRITE(6,*) 'Absolute Magnitudes'
               DO I=1,NPHOT
                  IF (SIG_MAG(I).GE.0.AND.SIG_DMAG(I).GE.0) THEN
                     CALL COMPUTE_MAG(EL,MA,
     +                    MAGNITUDE(I),SIG_MAG(I),
     +                    DELTA_MAG(I),SIG_DMAG(I),
     +                    .TRUE.,
     +                    MAG_1,d_MAG1,MAG_2,dMAG_2,
     +                    ERROR)
                     WRITE(6,420) PHOT_BAND(I), 
     +                    MAG_1, d_MAG1, MAG_2, dMAG_2
 420                 FORMAT(1X,A,2(1X,F6.3,1X,'+-',F5.3))
                  ENDIF
               ENDDO
            ENDIF
         ENDIF
*     
*     Not an SB2, but parallax is known. Mass sum can be computed
      ELSEIF(NPARA.GT.0) THEN
*     When there is an SB1 orbit, this case is better handled as an SB2 
*     with the parallax(es) determining K2, so suggest that option...
*      and don't
*     call the ad hoc code (still available though).
         IF ((NVR+NVCORR).GE.2) THEN
            WRITE(6,*) 'I-MASSES,  SB1 orbit and parallax '
     +           //' is known: '
     +           //' you could have adjusted K2 and obtained '
     +           //' individual masses.'
         ENDIF
         CALL COMPUTE_MTOT_WITH_PAR(EL,MA,Mtot,dMtot,ERROR)
         IF (ERROR) GOTO 99
         WRITE(LUN,400) 'Total Mass =',Mtot,dMtot,100*dMtot/Mtot
      ELSE
         WRITE(*,*) 'NO TOTAL MASS DETERMINATION'
         WRITE(*,*) 'NO INDIVIDUAL MASS DETERMINATION'
      ENDIF
      RETURN
*     
 99   ERROR = .TRUE.
      RETURN
      END


      SUBROUTINE COMPUTE_MASSES(EL,NEL,COMPUTE_ERRORS,
     +     a1sini,a2sini,f1m,f2m,
     +     M1sin3i,dM1sin3i,M2sin3i,dM2sin3i,
     +     Mtot,dMtot,M_A,dM_A,M_B,dM_B,
     +     Orb_Par,dOrb_Par,ERROR)
* Compute whichever astrophysical information can be deduced
* from the available orbital elements: total system mass, mass function,
* individual masses, projected semi-major axis....
      INTEGER NEL           ! Number of orbital elements
      REAL*8 EL(NEL )       ! Orbital elements
      REAL a1Sini, a2Sini   ! Projected semi major axes
      REAL f1m, f2m         ! Mass functions
      REAL M1sin3i, M2sin3i ! "projected masses"
      REAL dM1sin3i, dM2sin3i 
      REAL Mtot, dMtot      ! System mass and its probable error
      REAL M_A,dM_A,M_B,dM_B! Component masses and their probable errors
      REAL Orb_Par,dOrb_Par ! Orbital parallax and probable error
      LOGICAL COMPUTE_ERRORS,ERROR
*
      INCLUDE 'constant.inc'
      REAL K1,K2,KRT
      REAL*8 P
      REAL K_TOT,KM3,CF,CF3,SI,SI3
*
      REAL B(MA)
      INCLUDE 'pi.inc'
      INCLUDE 'physical_constants.inc'
      REAL*8 CONST
      PARAMETER (CONST=DAY/(2*PI)/G/MSOL)
*
* Compute sqrt(1-e**2), which is needed almost everywhere
      CF=SNGL(SQRT(1.-EL(I_Ecc+ELINDX(IORB0))**2))
* Convert orbital elements to cgs units
      P  = EL(I_PERIOD+ELINDX(IORB0))*day
      K1 = SNGL(EL(I_K1+ELINDX(IORB0))*km_s)
      K2 = SNGL(EL(I_K2+ELINDX(IORB0))*km_s)
*
* Projected semi-major axes and mass functions
      IF(K1.NE.0) THEN  ! K1 is known
         a1Sini=SNGL(1/(2*PI)*K1*P*CF) ! cgs (cm))
!         f1m=1/G*(2*PI/P)**2*(a1sini)**3
         f1m=SNGL(1/G*(2*PI/P*a1sini)**2*a1sini)
      ENDIF
      IF(K2.NE.0) THEN  ! K2 is known
         a2Sini=SNGL(1/(2*PI)*K2*P*CF) ! cgs (cm))
!         f2m=1/G*(2*PI/P)**2*(a2sini)**3
         f2m=SNGL(1/G*(2*PI/P*a2sini)**2*a2sini)
      ENDIF
* SB2 binary
      IF((K1*K2).NE.0) THEN 
         M2sin3i=f1m*(1+K2/K1)**2
         M1sin3i=f2m*(1+K1/K2)**2
         IF (COMPUTE_ERRORS) THEN
            CALL GRAD_LOGM1sin3i(B)
            CALL EVAL_ERROR(dM1sin3i,B)
            dM1sin3i=M1sin3i*dM1sin3i
            CALL GRAD_LOGM2sin3i(B)
            CALL EVAL_ERROR(dM2sin3i,B)
            dM2sin3i=M2sin3i*dM2sin3i
         ENDIF        
         SI=SNGL(SIN(EL(I_i+ELINDX(IORB0))/GR))
* SB2 with known non zero inclination: get everything
         IF (SI.NE.0) THEN 
            K_TOT=K1+K2
* Compute total mass
            CF3=CF*CF*CF
            SI3=SI*SI*SI
            KM3=K_TOT**3
            Mtot=SNGL(CONST*KM3*CF3*EL(I_PERIOD+ELINDX(IORB0))/SI3)
* Compute SIGM**2/M**2
            IF (COMPUTE_ERRORS) THEN
               CALL GRAD_LOGMTOT(B)
               CALL EVAL_ERROR(dMtot,B)
               dMtot=Mtot*dMtot
            ENDIF
C Compute individual masses
            KRT=K1/K2
            M_A=Mtot/(1+KRT)
            IF (COMPUTE_ERRORS) THEN
               CALL GRAD_LOGM1(B)
               CALL EVAL_ERROR(dM_A,B)
               dM_A=M_A*dM_A
            ENDIF
            M_B=M_A*KRT
            IF (COMPUTE_ERRORS) THEN
               CALL GRAD_LOGM2(B)
               CALL EVAL_ERROR(dM_B,B)
               dM_B=M_B*dM_B
            ENDIF
* Orbital parallax
            CALL GRAD_PARALLAX(Orb_Par,B)
            IF (COMPUTE_ERRORS) THEN
               CALL EVAL_ERROR(dOrb_Par,B)
            ENDIF
         ENDIF
      ENDIF
      RETURN
*
      END

      SUBROUTINE COMPUTE_MTOT_WITH_PAR(EL,NEL,Mtot,dMtot,
     +     ERROR)
* Compute total mass for Visual+parallax case. 
      INTEGER NEL           ! Number of orbital elements
      REAL*8 EL(NEL )       ! Orbital elements
      REAL Mtot, dMtot      ! System mass and its probable error
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INTEGER I
      REAL PAR,EPAR
      REAL B(MA)
      
      INCLUDE 'pi.inc'
      INCLUDE 'physical_constants.inc'
      INCLUDE 'data.inc' ! Needed to access parallax
*
* Compute weighted mean of available parallax measurements.
* Should perhaps instead compute Chi^2 and scale by sqrt(chi^2-NPARA)
* to account for actual dispersion? Noisier for small number of 
* measurements, but robust to underestimated errors. Let's leave as
* is for now, and rely on user rescaling/reconciling errors+values. 
      PAR  = 0
      EPAR = 0
      DO I=1,NPARA
         PAR  = PAR+PARALLAX(I)/PARERR(I)**2
         EPAR = EPAR+1/PARERR(I)**2
      ENDDO
      PAR  = PAR/EPAR     ! Normalise by sum of weights
      EPAR = 1/SQRT(EPAR)
      IF (PAR.EQ.0.OR.EPAR.EQ.0) GOTO 99
*
* Compute total mass and its standard error
      Mtot = (EL(I_AS+ELINDX(IORB0))/PAR)**3/
     +     (EL(I_PERIOD+ELINDX(IORB0))/YEAR)**2
      dMtot=0.
      CALL GRAD_LOGMtot_WITH_PAR(B)
      CALL EVAL_ERROR(dMtot,B)
      dMtot=Mtot*SQRT(dMtot**2 + (3*EPAR/PAR)**2)
!      WRITE(*,401) Mtot,dMtot
*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE GRAD_LOGMTOT_WITH_PAR(B)
      REAL B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I
      Integer iorb
      parameter (iorb=1)
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
      B(I_PERIOD+ELINDX(IORB))=2./PMU(IORB)
      B(I_AS+ELINDX(IORB))=3/A(IORB)
      RETURN
      END










