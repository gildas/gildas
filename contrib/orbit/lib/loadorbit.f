      SUBROUTINE INIT_ORBIT()
C-------------------------------------------------------------------
C ORBIT initialization routine:
C 	SIC is loaded with the ORBIT language,
C	plot library is initialized, and SIC variables are defined.
C-------------------------------------------------------------------
      USE GILDAS_DEF
      USE GKERNEL_INTERFACES
*
*	ORBIT vocabulary
*
      INTEGER MCOM
      PARAMETER (MCOM=33)
      CHARACTER*12 VOCAB(MCOM)
      CHARACTER*20 VERSION
*
      logical error
!
      EXTERNAL RUN_ORBIT
*
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
      INCLUDE  'control.inc'
*

      DATA VOCAB / 
     +     ' ADJUST','/ALL','/VISUAL','/SB1','/SB2',
     +     ' CURSOR',
     +     ' FIT', 
     +     ' FIX', '/ALL',
     +     ' GLIESE',
     +     ' IGNORE',
     +     ' LIST', '/OUTPUT', '/MONTECARLO',
     +     '/SELECT',
     +     ' OPTIMISE', '/TIME', '/SB1', '/SB2', '/XY', 
     +     '/OUTPUT',  '/STORE',
     +     ' PERIODOGRAM',
     +     ' READ' , '/APPEND', '/OLD_FORMAT',
     +     ' SAVE',
     +     ' USE',
     +     ' VISU', '/ORBIT' , '/VELOCITY' , '/VISUAL',
     +              '/CORRELATION'
     +     /
* Code
      ERROR = GTERRTST()
*
      VERSION = '2.0 15-JUL-2007'
*
      CALL SIC_BEGIN('ORBIT','gag_help_orbit',MCOM,VOCAB,
     &VERSION//'  T.F..',run_orbit,gterrtst)
      CALL GTERFLAG(.TRUE.)
*
* Initialize some internal commons
      ERROR = .FALSE.
      CALL SETUP_ELEMENTS(ERROR)
      IF (ERROR) STOP 'E-ORBIT,  Error initializing elements'
*
* Define SIC user variables
      METHOD = 'DIAGONALISE'
      CALL SIC_DEF_CHAR('METHOD',METHOD,.FALSE.,ERROR)
      PROFILE_METHOD = 'RAW'
      CALL SIC_DEF_CHAR('PROFILE_METHOD',PROFILE_METHOD,.FALSE.,ERROR)
      CALL SIC_DEF_INTE('D_UT',D_UT,1,1,.FALSE.,ERROR)
!
      END
