      SUBROUTINE BLANC(C,N)
C----------------------------------------------------------------------
C 	External routine
C
C 	Reduce the character string C to the standard SIC internal syntax.
C	Suppress all unecessary separators.
C	Character strings "abcd... " are not modified.
C	Ignores and destroys comments.
C Arguments :
C	C	C*(*)	Character string to be formatted	Input/Output
C	N	I	Length of C				Input/Output
C----------------------------------------------------------------------
      CHARACTER*(*) C
      INTEGER N
      CHARACTER*512 TEMPO
*
      INTEGER I,NL,NC
      LOGICAL LO,LI,CHAINE,SOME
      INTEGER TAB
      PARAMETER (TAB=9)
*
      IF (N.EQ.0) RETURN
*
      LO = .TRUE.
      CHAINE = .FALSE.
      SOME = .FALSE.
      NL = N
      NC = LEN(C)
      N = 0
      I = 1
*
      DO WHILE (I.LE.NL)
*
* Skip character strings
         IF (C(I:I).EQ.'"') THEN
            IF (I.LT.NC) THEN      ! Protect against empty strings
               IF (C(I+1:I+1).EQ.'"') THEN
                  TEMPO = ' '//C(I+1:)
                  C(I+1:) = TEMPO
                  NL = MIN(NL+1,NC)
               ENDIF
            ENDIF
            CHAINE=.NOT.CHAINE
         ENDIF
         IF (CHAINE) GOTO 30
*
* Suppress redondant separators
         LI=.FALSE.
         IF (C(I:I).NE.' ' .AND. C(I:I).NE.CHAR(TAB)) GOTO 10
* Space separator
         LI=.TRUE.
* Not the first one
         IF (LO)  GO TO 100
* First one
         C(I:I)=' '
         GOTO 30
*
10       IF (C(I:I).NE.'/') GOTO 20
*
* Option separator
         LI=.TRUE.
         IF (N.EQ.0)  GOTO 30
* Not the first one
         IF (LO) THEN
            C(N:N)='/'
            GOTO 100
         ENDIF
* First one
         C(I:I)='/'
         GOTO 30
*
* Skip comment area
20       IF (C(I:I).EQ.'!') THEN
            SOME=.TRUE.
            GOTO 200
         ENDIF
*
* Reset input character string
30       N=N+1
         IF (I.NE.N) C(N:N)=C(I:I)
         IF (I.GT.N) C(I:I)=' '
100      LO=LI
*
* End of loop
         I = I+1
      ENDDO
*
* Remove trailing separators if any
200   IF (N.NE.0) THEN
         IF (C(N:N).EQ.' ' .OR. C(N:N).EQ.'/') N=N-1
      ENDIF
* Clean up end of line
      IF (N.LT.NC) C(N+1:) = ' '
      IF (SOME) N=MAX(N,1)
      END

      SUBROUTINE NO_SPACE(C,N)
C----------------------------------------------------------------------
C 	External routine
C
C	Suppress all separators.
C	Character strings "abcd... " are not modified.
C Arguments :
C	C	C*(*)	Character string to be formatted	Input/Output
C	N	I	Length of C				Input/Output
C----------------------------------------------------------------------
      CHARACTER*(*) C
      INTEGER N
      CHARACTER*512 TEMPO
*
      INTEGER I,NL,NC
      LOGICAL CHAINE
      INTEGER TAB
      PARAMETER (TAB=9)
*
      IF (N.EQ.0) RETURN
*
      CHAINE = .FALSE.
      NL = N
      NC = LEN(C)
      N = 0
      I = 1
*
      DO WHILE (I.LE.NL)
*
* Skip character strings
         IF (C(I:I).EQ.'"') THEN
            IF (I.LT.NC) THEN      ! Protect against empty strings
               IF (C(I+1:I+1).EQ.'"') THEN
                  TEMPO = ' '//C(I+1:)
                  C(I+1:) = TEMPO
                  NL = MIN(NL+1,NC)
               ENDIF
            ENDIF
            CHAINE=.NOT.CHAINE
         ENDIF
         IF (CHAINE) GOTO 30
*
* Suppress separators
         IF (C(I:I).NE.' ' .AND. C(I:I).NE.CHAR(TAB)) THEN
* Space separator
            N=N+1
            IF (I.NE.N) C(N:N)=C(I:I)
            IF (I.GT.N) C(I:I)=' '
         ENDIF
* End of loop
 30      I = I+1
      ENDDO
*
* Remove trailing separators if any
      IF (N.NE.0) THEN
         IF (C(N:N).EQ.' ' .OR. C(N:N).EQ.'/') N=N-1
      ENDIF
* Clean up end of line
      C(N+1:) = ' '
      N=MAX(N,1)
      END
