      REAl    SF(MORBIT)  ! Eccentricity
      REAL    EC(MORBIT)  ! sqrt((1+e)/(1-e))
      REAL    A(MORBIT)   ! Internal copy of semi-major axis
      REAL    WW(MORBIT)  ! OMEGA 
      REAL    CWW(MORBIT) ! Cos(OMEGA)
      REAL    SWW(MORBIT) ! Sin(OMEGA)
      REAL    W(MORBIT)   ! omega
      REAL    CW(MORBIT)  ! cos(omega)
      REAL    SW(MORBIT)  ! sin(omega)
* Double precision is needed for formulae using tg(i), if the orbit gets close
* to edge-on
      REAL*8    SI(MORBIT)  ! sin(i)
      REAL*8    CI(MORBIT)  ! cos(i)
      REAL*8    TI(MORBIT)  ! tgt(i)
      REAL    K1(MORBIT)  ! K1
      REAL    K2(MORBIT)  ! K2
      REAL    V0          ! Systemic velocity. Only once 	
      REAL    X0          ! Position zero point for astrometry
      REAL    Y0          ! 
      REAL    Mu_X        ! Proper motion
      REAL    Mu_Y        ! 
* Real*4 isn't accurate enough for times and frequencies in well observed
* short period binaries.
      REAL*8 EPOCH          ! Epoch of astrometric coordinate zero point.
                            ! Single precision is sufficient.
      REAL*8 T0(MORBIT)     ! Epoch of periastron
      REAL*8 P(MORBIT)      ! Period
      REAL*8 PMU(MORBIT)    ! Orbital pulsation
      REAL*8 DT(MORBIT)   ! Time since/to last periastron
      REAL*8 DT_EPOCH     ! Time since/to epoch of coordinate zero point.
*
      REAL CF(MORBIT)   ! Eccentricity correction (sqrt(1-e^2))
      REAL CF2(MORBIT)  ! Squared 
      REAL CF3(MORBIT)  ! Cubed 
      REAL U(MORBIT)    ! OK
      REAL CU(MORBIT)   ! OK
      REAL SU(MORBIT)   ! OK
      REAL V(MORBIT)    ! OK
      REAL E(MORBIT)    ! OK
      REAL e1(MORBIT)   ! OK
      REAL PAR          ! (Orbital) parallax
      REAL GRAD_PI(MA)  ! Gradient of parallax relative to orbital elements
*
      REAL    AA(MORBIT)  ! Thiele-van-den-Bos elements, needed
      REAL    BB(MORBIT)
      REAL    FF(MORBIT)
      REAL    GG(MORBIT)
* Local copy of the velocity zero points, instrumentals (DV), and 
* gravitational+convective (DELTV)
      REAL DV(MVREF-1),DELTV(2,MORBIT)
* Local copy of the fractional fluxes. Will need to be generalised to 
* triples/quadruples
      REAL FLXFRAC(MPHOT)
*
      LOGICAL SILENT
*
      COMMON /KEPLER/ P,PMU,T0,EPOCH,DT,DT_EPOCH
     +	   ,SF,CF2,CF,CF3,EC,A,WW,CWW,SWW
     +     ,W,CW,SW,SI,CI,TI,K1,K2
     +	   ,V0,X0,Y0,Mu_X,Mu_Y
     +	   ,AA,BB,FF,GG
     +     ,cu
     +     ,u,e,su
     +     ,e1,PAR,GRAD_PI
     +     ,V,DV,DELTV
     +     ,FLXFRAC
     +     ,SILENT


