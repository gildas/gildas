      SUBROUTINE COVSUM(MFIT,MA,ALPHA,BETA,B,LISTA,DY,
     +     CHISQ,SIG2I,QUIET,ERROR)
      INTEGER MFIT,MA
      INTEGER LISTA(MFIT)
      REAL ALPHA(MA,MA),BETA(MA),B(MA),CHISQ,SIG2I,DY
      LOGICAL QUIET,ERROR
*
      INTEGER J,K
      REAL WT
*
      DO J=1,MFIT
         
         WT=B(LISTA(J))*SIG2I
         DO K=1,J
            ALPHA(J,K)=ALPHA(J,K)+WT*B(LISTA(K))
         ENDDO
         BETA(J)=BETA(J)+DY*WT
      ENDDO
      CHISQ=CHISQ+DY*DY*SIG2I
      IF (CHISQ.NE.CHISQ) THEN
         IF (.NOT.QUIET) THEN
            WRITE(6,*) 'NaN in COVSUM',DY,(B(LISTA(J)),J=1,MFIT)
         ENDIF
         ERROR = .TRUE.
         RETURN
      ENDIF
      RETURN
      END
