        SUBROUTINE SAVE(LINE,ERROR)
        USE GKERNEL_INTERFACES
C------------------------------------------------------------------------------------
C  ORBIT:
C     Support routine for SAVE command
C          SAVE FileName
C------------------------------------------------------------------------------------
        CHARACTER*(*) LINE
        LOGICAL ERROR
        
        INCLUDE 'constant.inc'
        INCLUDE 'elements.inc'
        INCLUDE 'data.inc'
        INCLUDE 'pi.inc'
!        INCLUDE 'file.inc'
*
        INTEGER LUNSAV
        INTEGER I,J
        INTEGER L,LK,STATUS

        CHARACTER*24 KEYWORD

        CHARACTER CHAIN*80,FILE*80
*
* Get file name, and open 
        CALL SIC_CH(LINE,0,1,CHAIN,L,.TRUE.,ERROR)
        IF (ERROR) RETURN
        CALL SIC_PARSEF(CHAIN(1:L),FILE,' ','.DAT')
        STATUS=SIC_GETLUN(LUNSAV)
        IF (STATUS.NE.1) THEN
           WRITE(6,*) 'E-SAVE,  No logical unit left'
           ERROR = .TRUE.
           RETURN
        ENDIF
        OPEN(UNIT=LUNSAV,FILE=FILE,STATUS='UNKNOWN',
     +       IOSTAT=STATUS)
        IF (STATUS.NE.0) THEN
           CALL PUTIOS('E-SAVE,  ',STATUS)
           WRITE(6,*)'E-SAVE,  Cannot open ',FILE(1:LENC(FILE))
           CALL SIC_FRELUN(LUNSAV)
           GOTO 99
        ENDIF
* Check WRITE access
        WRITE(LUNSAV,'(A)',IOSTAT=STATUS) ' '
        IF (STATUS.NE.0) THEN
           WRITE(6,*) 'E-LIST,  Cannot write to file ',
     +          FILE(1:LENC(FILE))
           CALL PUTIOS('E-LIST,  ',STATUS)
           CLOSE(LUNSAV)
           CALL SIC_FRELUN(LUNSAV)
           GOTO 99
        ENDIF
        REWIND(LUNSAV)
*
* Write header information
        IF (GLIESE.NE.' ') WRITE(LUNSAV,1001) 
     +       'Gl',GLIESE(1:LENC(GLIESE))
        IF (HD.NE.' ') WRITE(LUNSAV,1001) 
     +       'HD',HD(1:LENC(HD))
        IF (OTHER_NAME.NE.' ') WRITE(LUNSAV,1001) 
     +       'NAME',OTHER_NAME(1:LENC(OTHER_NAME))
        IF (RA.NE.0.D0) THEN
           CALL SEXAG(CHAIN,RA,24)
           WRITE(LUNSAV,1001) 'RA', CHAIN
        ENDIF
        IF (DEC.NE.0.D0) THEN
           CALL SEXAG(CHAIN,DEC,360)
           WRITE(LUNSAV,1001) 'DEC',CHAIN
        ENDIF
        IF (EQUINOX.GT.0) WRITE(LUNSAV,1003) 
     +       'EQUINOX',EQUINOX
        DO I=1,NPHOT
           CHAIN = 'm_'//PHOT_BAND(I)
           WRITE(LUNSAV,1005) CHAIN(1:LENC(CHAIN)),MAGNITUDE(I),
     +          SIG_MAG(I)
           CHAIN = 'Delta_'//PHOT_BAND(I)
           WRITE(LUNSAV,1005) CHAIN(1:LENC(CHAIN)),DELTA_MAG(I),
     +          SIG_DMAG(I)
        ENDDO
        WRITE(LUNSAV,1001) 'TSA',SPECTRAL_TYPE(1)
        WRITE(LUNSAV,1001) 'TSB',SPECTRAL_TYPE(2)
        CALL SIC_DATE(CHAIN)
        WRITE(LUNSAV,1007) 'DATFITC',CHAIN
        DO I=1,NCOMMENT
           WRITE(LUNSAV,1009) 'COMMENT',COMMENT(I)
        ENDDO
!        WRITE(LUNSAV,1011)  'COMMENT SIGVR',SIGVR
* Orbital elements
        DO I=1,MA
           IF (NAMEL(I).NE.' '.AND.ELERR(I).GE.0) 
     +          WRITE(LUNSAV,1013) NAMEL(I),EL(I),ELERR(I),EL_STATUS(I)
        ENDDO
* Velocity referential
        IF (FORCED_VREF.NE.' ') THEN
           WRITE(LUNSAV,1001) 'VR_REF', FORCED_VREF
        ENDIF
 1001   FORMAT(A,T10,A20,28X,A12)
 1003   FORMAT(A,T10,F11.3,37X,A12)
 1005   FORMAT(A,T10,2(F11.3,1X),24X,A12)     
 1007   FORMAT(A,T10,A30,18X,A12)
 1009   FORMAT(A,T10,A70)
! 1011   FORMAT(A,T10,F11.4,37X,A12)
 1013   FORMAT(A,1X,2(F11.4,1X),1X,A)
! 1015   FORMAT(A,T10,2(F11.7,1X),24X,A12)
! 1017   FORMAT(A,T10,2(F11.4,1X),24X,A12)
* Radial velocities
        DO I=1,NVR
           IF (CVR(I).EQ.' ') CVR(I) = '1'
           LK = LENC(CVR(I))
           KEYWORD='V'//CVR(I)(1:LK)
           LK = LK+1
           IF (VR_REF(VREFCODE(I)).NE.' ') THEN
              L = LENC(VR_REF(VREFCODE(I)))
              KEYWORD=KEYWORD(1:LK)//'('//
     +             VR_REF(VREFCODE(I))(1:L)//')'
              LK = LK+L+2 
           ENDIF
           WRITE(LUNSAV,1019) KEYWORD(1:LK),
     +          VR(I,I_TIME),VR(I,I_VR),
     +          VR(I,I_S_VR),VR(I,I_OC_VR),VR(I,I_PHASE)
        ENDDO
 1019   FORMAT(A,1X,F13.6,1X,F9.4,1X,F7.4,1X,F8.4,1X,F8.6)
* Visual measurements
        DO I=1,NN
           IF ( COBS(I).EQ.'theta') THEN
              WRITE(LUNSAV,1020) COBS(I),OBS(I,I_TIME),
     +             OBS(I,I_THETA),
     +             OBS(I,I_S_THETA),
     +             OBS(I,I_OC_THETA)
 1020         FORMAT(A,T11,F13.6,1X,F6.2,1X,
     +             F6.2,1X,F6.2)
           ELSEIF ( COBS(I).EQ.'rho-theta') THEN
              WRITE(LUNSAV,1021) COBS(I),OBS(I,I_TIME),
     +             OBS(I,I_RHO),OBS(I,I_THETA),
     +             OBS(I,I_S_RHO),OBS(I,I_S_THETA),
     +             OBS(I,I_OC_RHO),OBS(I,I_OC_THETA)
 1021         FORMAT(A,T11,F13.6,1X,F6.4,1X,F6.2,1X,F6.4,1X,
     +             F6.2,1X,F5.2,1X,F6.2)
           ELSEIF (COBS(I).EQ.'PROJ') THEN
              WRITE(LUNSAV,1022) COBS(I),
     +             OBS(I,I_TIME),OBS(I,I_POSANG),
     +             OBS(I,I_PROJ),OBS(I,I_S_PROJ),
     +             OBS(I,I_OC_PROJ)
 1022         FORMAT(A,T11,F13.6,1X,F8.3,
     +             (1X,F7.4,1X,F5.4,1X,F7.4))
           ELSEIF (COBS(I).EQ.'XY'
     +         .OR.COBS(I).EQ.'PXY'
     +         .OR.COBS(I).EQ.'AXY') THEN
              WRITE(LUNSAV,1023) COBS(I),OBS(I,I_TIME),
     +             OBS(I,I_X),OBS(I,I_Y),
     +             OBS(I,I_S_X),OBS(I,I_S_Y),
     +             OBS(I,I_OC_X),OBS(I,I_OC_Y)
 1023         FORMAT(A,T10,F13.6,2(1X,F9.4),2(1X,F5.4),2(1X,F7.4))
           ELSEIF (COBS(I).EQ.'ZT') THEN
              WRITE(LUNSAV,1023) COBS(I),OBS(I,I_TIME),
     +             OBS(I,I_Z),OBS(I,I_T),
     +             OBS(I,I_S_T),OBS(I,I_S_Z),
     +             OBS(I,I_OC_Z),OBS(I,I_OC_T)
           ELSE
              WRITE(6,*) 'F-SAVE,  Unknown visual type. ',cobs(I),
     +             'Internal error.'
              ERROR = .TRUE.
              RETURN
           ENDIF
        ENDDO
        DO I=1,NVCORR
           WRITE(LUNSAV,1024) 'VCORREL',COR_TIME(I),NVPNTS(I),
     +          COR_REFPIX(I),COR_REFVAL(I),COR_VSTEP(I),COR_SIGM(I),
     +          (VCORR(J,I),J=1,NVPNTS(I))
        ENDDO
 1024   FORMAT(A,T10,F13.6,1X,I4,1X,F8.3,1X,F8.3,1X,F8.5,1X,G10.3,
     +       10000(1X,G9.4))
* Write parallax measurements
        DO I=1,NPARA
           WRITE(LUNSAV,1025) PARALLAX(I),PARERR(I),PAR_OC(I)
        ENDDO
 1025   FORMAT('PAR',T10,F6.4,1X,F5.4,1X,F5.4)
*
        CLOSE(LUNSAV)
        CALL SIC_FRELUN(LUNSAV)
        RETURN
*
 99     ERROR = .TRUE.
        RETURN
        END

