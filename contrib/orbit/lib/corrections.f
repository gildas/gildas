
      SUBROUTINE ACCUMULATE_PROFILE_CORRECTION(ICOMP,IORB,VEL,NPNT,
     +     REF,VAL,INC,CORRECTION,ERROR)
* Interpolate the profile correction for a specified component, centered
* at a requested velocity.
      INTEGER ICOMP, IORB
      REAL VEL
      INTEGER NPNT
      REAL*8 REF,VAL,INC
      REAL CORRECTION(NPNT)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'corrections.inc'
*
      INTEGER I,I1
      REAL*8 X
      REAL PIX
!      real*4 xx(1024), yy(1024)

*
      IF (NPNTS_CORRECTED(ICOMP,IORB).LE.0) RETURN
      DO I=1,NPNT
         X = VAL+(I-REF)*INC
         PIX = ((X-VEL)-CORRECTED_VAL(ICOMP,IORB))
     +        /CORRECTED_INC(ICOMP,IORB)
     +        +CORRECTED_REF(ICOMP,IORB)
         IF (PIX.GE.1.AND.PIX.LT.NPNTS_CORRECTED(ICOMP,IORB)) THEN
            I1 = INT(PIX)
            CORRECTION(I) = CORRECTION(I)+
     +           PROFILE_CORRECTIONS(I1,ICOMP,IORB)*(1-(PIX-I1))+
     +           PROFILE_CORRECTIONS(I1+1,ICOMP,IORB)*(PIX-I1)
!                     Write(6,*) I1,ICOMP,PROFILE_CORRECTIONS(I1,ICOMP,IORB),
!     +                    PROFILE_CORRECTIONS(I1+1,ICOMP,IORB),correction
         ELSE
            CONTINUE ! CORRECTION = CORRECTION+0
         ENDIF
!         xx(i) = x
!         yy(i) = correction(i)
      ENDDO
!      write(6,*) 'accum plot',icomp,iorb
!      call GR4_GIVE('X',NPNT,Xx)
!      if (icomp.eq.1.and.iorb.eq.1) then
!         call gr_exec('clear plot')
!         call gr_exec('limits * * -0.005 0.005')
!!               call GR4_GIVE('Y',NPNT,Y) ! Gaussian part
!!               call gr_exec('limits * * < >')
!!               call gr_exec('box')
!!               call gr_exec('connect')
!      endif
!      call GR4_GIVE('Y',NPNT,YY) ! Correction
!      call gr_exec('limits * * < >')
!      call gr_exec('box')
!      write(chain,'(A,I2)') 'PEN ',icomp+1
!!      call gr_exec('pen 2')
!      call gr_exec(chain)
!      call gr_exec('connect')
!      write(6,*) 'Type return to continue', icomp,iorb,vel
!      read(5,*) 
!      call gr_exec('pen /def')
      END
*
      SUBROUTINE AVERAGE_CORREL_PROFILE(ICOMP,NPNT,XREF,XVAL,XINC,
     +     Y,ZBAR,ERROR)
* Recenters on one component, and then averages, all available correlation 
* profiles.
      INTEGER ICOMP,NPNT
      REAL*8 XREF,XVAL,XINC
      REAL Y(NPNT),ZBAR(NPNT)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'pi.inc'
*
      INTEGER ICOR
      REAL*8 XMIN,XMAX
      REAL*8 TMP,X1,X2
      REAL VAL,PIX
      REAL V
      REAL X(MVPNTS),OTHER_PROFILE(MVPNTS)
      INTEGER I,I1,J,NOTHERS,OTHERS(NCOMPONENTS)
      INTEGER IW(2,MORBIT),IEW(2,MORBIT),ORIGIN
      PARAMETER (ORIGIN=1)
*
      CALL INIT_KEPLER(EL,.FALSE.)
      CALL SELECT_PROFILE_PARAMETERS(ORIGIN,IEW,IW,ERROR)


* 
* Determine range and sampling of the average profile: range intersection,
* and highest resolution.
      XMIN=-MAXR4
      XMAX=+MAXR4
      XINC=MAXR4
      DO ICOR=1,NVCORR
* Compute velocities at both ends of the array, and sort them
         X1 = COR_REFVAL(ICOR)+(1-COR_REFPIX(ICOR))*COR_VSTEP(ICOR)
         X2 = COR_REFVAL(ICOR)+
     +        (NVPNTS(ICOR)-COR_REFPIX(ICOR))*COR_VSTEP(ICOR)
         IF (X1.GT.X2) THEN
            TMP = X1
            X1 = X2
            X2 = TMP
         ENDIF
* Recenter computed range on selected component
         CALL GET_VELOCITY(COR_TIME(ICOR),ICOMP,
     +        CORREFCOD(ICOR),V,ERROR)
         IF (ERROR) GOTO 99
         X1=X1-V
         X2=X2-V
         XMIN=MAX(XMIN,X1)
         XMAX=MIN(XMAX,X2)
         XINC=MIN(XINC,ABS(COR_VSTEP(ICOR)))
      ENDDO
*
* Define the output grid, 
      XREF=1.
      XVAL=XMIN
      NPNT=MIN(INT((XMAX-XMIN)/XINC),NPNT)
      DO I=1,NPNT
         X(I) = XVAL+(I-XREF)*XINC
         Y(I) = 0
      ENDDO
* Build a list of all other components (excluding absent ones, as an
* optimisation...)
      NOTHERS = 0
!      write(6,*) 'nothers',nothers,'Others',others
      DO I=1,MORBIT
         DO J=1,2
            IF (EL(IEW(J,I)).GT.0) THEN
               IF (ICOMP.NE.COMPONENTS(J,I)) THEN
                  NOTHERS = NOTHERS+1
                  OTHERS(NOTHERS) = COMPONENTS(J,I)
               ENDIF
            ENDIF
         ENDDO
      ENDDO
!      write(6,*) 'nothers',nothers,'Others',others
* Now produce the average recentered correlation profile
      DO ICOR=1,NVCORR
* Correct for the contributions of all other fitted components
         CALL GET_MODEL_PROFILE(ICOR,NOTHERS,OTHERS,.FALSE.,.FALSE.,
     +        NVPNTS(ICOR),COR_REFPIX(ICOR),COR_REFVAL(ICOR),
     +        COR_VSTEP(ICOR),OTHER_PROFILE,ERROR)

         CALL GET_VELOCITY(COR_TIME(ICOR),ICOMP,
     +        CORREFCOD(ICOR),V,ERROR)
         IF (ERROR) GOTO 99
         call DEBUG_PLOT(NVPNTS(ICOR),X,OTHER_PROFILE,.TRUE.)
         call DEBUG_PLOT(NVPNTS(ICOR),X,VCORR(1,ICOR),.TRUE.)

* Resample (with a linear interpolation) onto the output grid, and average,
* at present with equal weights.
         DO I=1,NPNT
            PIX = (X(I)-(COR_REFVAL(ICOR)-V))/COR_VSTEP(ICOR)
     +           +COR_REFPIX(ICOR)
            IF (PIX.LT.1.OR.PIX.GT.NVPNTS(ICOR)) THEN
               IF (PIX.GT.0.999) THEN ! Accept a small roundup
                  PIX = 1.
               ELSE
                  WRITE(6,*) 'VISU,  INTERNAL LOGIC ERROR',
     +                 PIX,NVPNTS(ICOR)
                  ERROR = .TRUE.
                  GOTO 99
               ENDIF
            ENDIF
            I1 = INT(PIX)
            IF (I1.EQ.PIX) THEN ! Handle this as a special case to avoid acces
                                ! violations at end of array
               VAL = VCORR(I1,ICOR)-OTHER_PROFILE(I1)
            ELSE
               VAL = 
     +              (VCORR(I1,ICOR)-OTHER_PROFILE(I1))*(1-(PIX-I1))+
     +              (VCORR(I1+1,ICOR)-OTHER_PROFILE(I1+1))*(PIX-I1)
            ENDIF
            Y(I) = Y(I)+VAL
         ENDDO
      ENDDO
      DO I=1,NPNT
         Y(I) = Y(I)/NVCORR
      ENDDO
!      CALL DEBUG_PLOT(NPNT,X,Y,.TRUE.)
!      write(6,*)    NPNT
!      do i=1,NPNT
!         write(6,*) x(i),y(i)
!      enddo
!      write(6,*) ICOMP,'Type return to continue'
!      read(5,*)
!      write(6,*) 'nothers',nothers,'Others',others
!      write(6,'(12(F13.5))') nothers,others
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
*
