      SUBROUTINE AMEBSA(P,Y,MP,NP,NDIM,PBEST,YBEST,FTOL,
     +     FUNK,ITER,TEMPTR,ERROR)
      INTEGER ITER,MP,NDIM,NP
      REAL FTOL,TEMPTR,YBEST,P(MP,NP),PBEST(NP),Y(MP),FUNK
      LOGICAL ERROR
      EXTERNAL FUNK
C----------------------------------------------------------------------------
C Adapted from Numerical Recipes:
C Multidimensional minimization of the function FUNK(X) where X(1:NDIM) is 
C a vector in NDIM dimensions, by simulated annealing combined with the 
C downhill simplex method of Nelder and Mead. The input matrix 
C P(1:NDIM,1:NDIM+1) has NDIM+1 rows, each an NDIM-dimensional vector 
C which is a vertex of the starting simplex. Also input is the vector
C Y(1:NDIM+1) whose components must be pre-initialized to the values
C of FINK evaluated at the NDIM+1 vertices of the starting simplex.
C FTOL is the fractional convergence tolererance to be achieved in the
C function to justify an early return. The routine makes ITER iteration
C at an annealing temperature TEMPTR, then returns. You should then
C decrease TEMPTR according to your annealing schedule, reset ITER (now
C the number of unused allowed iterations, 0 unless early return occured), 
C and call the routine again. Other arguments should generally be leaved 
C unaltered between calls, unless you want to perform a restart. If you
C initialise YB to a very large value on the first call, then YB and 
C PB(1:NDIM) will subsequently return the best function value and point
C ever encountered, even if it is no longer a vertex of the simplex.
C----------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INTEGER I,IHI,ILO,INHI,J,N
      REAL RTOL,SUM,SWAP,TT,YHI,YLO,YNHI,YSAVE,YT,YTRY,WEIGHT
      REAL PSUM(MOBS+MVR+MVCORR)
      REAL AMOTSA,GAG_RANDOM
      LOGICAL SIC_CTRLC
      INTEGER LAST_PRINT,PRINT_FACTOR,ITER0
      PARAMETER (PRINT_FACTOR=10) ! Print best value every so many function
                                  ! calls
      COMMON /AMBSA/ TT
*
      LAST_PRINT = (ITER/PRINT_FACTOR)-1
      ITER0 = ITER
      TT = -TEMPTR
 1    CONTINUE   ! Come back here after overall contraction
      IF (SIC_CTRLC()) THEN ! Stop on Ctrl C interrupt
         GOTO 101
      ENDIF
* Recompute PSUM (simplex center of gravity)
      WEIGHT = 1.0/(NDIM+1)
      DO I=1,NDIM
         SUM = 0
         DO J=1,NDIM+1
            SUM = SUM+P(I,J)
         ENDDO
         PSUM(I) = SUM*WEIGHT
      ENDDO
 2    ILO  = 1 ! Come back here after changing a single point. Redetermine
      INHI = 1 ! which point is worst, next to worst, and best.
      IHI  = 2
      YLO  = Y(1)+TT*LOG(GAG_RANDOM())
      YNHI = YLO
      YHI  = Y(2)+TT*LOG(GAG_RANDOM())
      IF (YLO.GT.YHI) THEN ! Swap to proper ordering
         IHI  = 1
         INHI = 2
         ILO  = 2
         YNHI = YHI
         YHI  = YLO
         YLO  = YNHI
      ENDIF
      DO I=3,NDIM+1
         YT = Y(I)+TT*LOG(GAG_RANDOM())
         IF (YT.LE.YLO) THEN
            ILO = I
            YLO = YT
         ENDIF
         IF (YT.GT.YHI) THEN
            INHI = IHI
            YNHI = YHI
            IHI  = I
            YHI  = YT
         ELSEIF (YT.GT.YNHI) THEN
            INHI = I
            YNHI = YT
         ENDIF
      ENDDO
* Compute fractional range from worst to best, and check for early return
* possibility
      RTOL = 2.0*ABS(YHI-YLO)/(ABS(YHI)+ABS(YLO))
      IF (YHI.GE.MAXR4) RTOL = MAXR4
!      write(6,*) 'RTOL ',rtol,' FTOL ',ftol,'YHI',YHI,'YLO',YLO,YBEST
      IF (RTOL.LT.FTOL .OR. ITER.LE. 0) THEN ! Put best vertex in slot 1,
         SWAP   = Y(1)                       ! and then return
         Y(1)   = Y(ILO)
         Y(ILO) = SWAP
         DO N=1,NDIM
            SWAP     = P(N,1)
            P(N,1)   = P(N,ILO)
            P(N,ILO) = SWAP
         ENDDO
!         IF (Y(1).LT.YBEST) THEN
            
         RETURN
      ENDIF
* Begin a new iteration
      ITER = ITER - 2
* First try extrapolating by -1 from highest vertex across simplex face, 
* i.e. apply a reflexion from highest point.
      YTRY = AMOTSA(P,Y,PSUM,MP,NP,NDIM,PBEST,YBEST,
     +     FUNK,IHI,YHI,-1.0)
      IF (YTRY.LE.YLO) THEN ! Gave an improvement over best point, try
                            ! a further factor of 2 extrapolation
!         write(6,*) 'Successful reflexion:',ytry,ylo
         YTRY = AMOTSA(P,Y,PSUM,MP,NP,NDIM,PBEST,YBEST,
     +        FUNK,IHI,YHI,2.0)
      ELSEIF (YTRY.GE.YNHI) THEN  ! The reflected point is worse than the
                                  ! second worse point, so look for an 
                                  ! intermediate lower point, i.e. perform 
                                  ! a 1D contraction.
         YSAVE = YHI
         YTRY = AMOTSA(P,Y,PSUM,MP,NP,NDIM,PBEST,YBEST,
     +        FUNK,IHI,YHI,0.5)
!         write(6,*) '1D contraction: ',ysave,'-->',ytry
         IF (YTRY.GE.YSAVE) THEN  ! Can't easily get rid of that high point
                                  ! Contract simplex around its best point
            DO I=1,NDIM+1
               IF (I.NE.ILO) THEN
                  DO J=1,NDIM
                     P(J,I)  = 0.5*(P(J,ILO)+P(J,I))
                  ENDDO
                  Y(I) = FUNK(P(1,I))
                  IF (Y(I).LT.YBEST) THEN
                     YBEST = Y(I)
!                     write(6,*) 'YBEST improved during contraction:',
!     +                    YBEST
                     DO J=1,NDIM
                        PBEST(J) = P(J,I)
                     ENDDO
                  ENDIF
               ENDIF
            ENDDO
            ITER = ITER-NDIM  ! Contraction used NDIM function evaluations
            GOTO 1
         ENDIF
      ELSE ! Usual path: reflection improved over previous worst point, but not
           ! over best. Update iteration count
         ITER = ITER+1
      ENDIF
      IF (ITER/PRINT_FACTOR.LT.LAST_PRINT) THEN
         WRITE(6,'(1X,A,I4,A,F7.5,A,F7.5)') 
     +        'Best to date for iteration',
     +        ITER0-ITER,
     +        ' is ',YBEST,'. RTOL is ',RTOL
         LAST_PRINT = ITER/PRINT_FACTOR
      ENDIF
!      write(6,*) iter
      GOTO 2
*
* Error exit
 100  ERROR = .TRUE.
      write(6,*) 'amebsa: error',error
      RETURN
 101  write(6,*) 'Optimisation interrupted by ^C'
      RETURN
      END
c<FF>
      FUNCTION AMOTSA(P,Y,PSUM,MP,NP,NDIM,PB,YB,FUNK,IHI,YHI,FAC)
      INTEGER IHI,MP,NDIM,NP
      REAL AMOTSA,FAC,YB,YHI,P(MP,NP),PB(NP),PSUM(NP),Y(MP),FUNK
      EXTERNAL FUNK
C----------------------------------------------------------------------------
C Adapted from Numerical Recipes:
C Downhill simplex with relaxation.
C Extrapolates by a factor FAC through the face of the simplex opposite
C from the high point, tries it, and replace the high point if the new
C point is better.
C----------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INTEGER J
      REAL FAC1,TT,YFLU,YTRY,PTRY(MOBS+MVR+MVCORR)
      REAL GAG_RANDOM
!      integer i
      COMMON /AMBSA/ TT
*
* PTRY = PSUM + FAC*(P(IHI)-PSUM)
      FAC1 = (1-FAC)
      DO J=1,NDIM
         PTRY(J) = PSUM(J)*FAC1+P(J,IHI)*FAC
      ENDDO
      YTRY = FUNK(PTRY)
!      write(6,*) 'Trial with factor: ',fac, (ptry(I),I=1,Ndim)
!      write(6,*) 'Result',ytry,yb
      IF (YTRY.LE.YB) THEN ! Best ever, save
         DO J=1,NDIM
            PB(J) = PTRY(J)
         ENDDO
         YB = YTRY
!         write(6,*) 'YBEST improved during trial for',fac,':',YB
!         write(6,*) (PB(i),i=1,ndim)
      ENDIF
      YFLU = YTRY-TT*LOG(GAG_RANDOM()) ! Subtract a thermal fluctuation to trial
                                       ! point, so as to give it a brownian 
                                       ! motion and encourage to accept change
* Successful step: replace high point and update barycenter
      IF (YFLU.LT.YHI) THEN
         Y(IHI) = YTRY
         YHI = YFLU
         DO J=1,NDIM
            PSUM(J)  = PSUM(J)-P(J,IHI)+PTRY(J)
            P(J,IHI) = PTRY(J)
         ENDDO
      ENDIF
      AMOTSA = YFLU
      RETURN
      END
