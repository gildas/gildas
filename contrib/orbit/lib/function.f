      FUNCTION ERREUR_FUNCT(X)
      USE GKERNEL_INTERFACES
      REAL X(*)
      REAL ERREUR_FUNCT
C----------------------------------------------------------------------------
C Observing schedule optimisation:
C Merit function to be optimised. Can be the standard error on any of a number
C of parameters, depending on status of internal common.
C----------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'optimise.inc'
      INCLUDE 'data.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I,J,K
      INTEGER MITER
      PARAMETER (MITER=20)  ! Maximum number of non linear iterations in
                            ! least square fit.
      INTEGER NN_SAVE,NVR_SAVE
      REAL*8 EL_TEST(MA)
      LOGICAL WARNING,ERROR,QUIET,UPDATE
      PARAMETER (QUIET=.TRUE.)
!      PARAMETER (QUIET=.false.)
      PARAMETER (UPDATE=.TRUE.)
      INTEGER NTRIALS       ! Number of generated mesurement sets for each
      PARAMETER (NTRIALS=9) ! set of observing dates
*

      REAL B(MA)
      INTEGER ITRIAL
      REAL PRECISION(NTRIALS)
      INTEGER IT(NTRIALS)
! Only used in a cross-check
!      real Mtot,M_A,M_B ,dMtot,dM_A,dM_B 
!      real a1Sini, a2Sini   ! Projected semi major axes
!      real f1m, f2m         ! Mass functions
!      real M1sin3i, M2sin3i ! "projected masses"
!      real a1, a2, a        ! Semi major axes
      REAL Orb_Par !,dOrb_Par ! Orbital parallax and probable error

!      character*40 tmpmethod(2)
!      integer imethod
!      real*8 meth_result(2)
*
!      tmpmethod(1) = 'DIAGONALISE'
!      tmpmethod(2) = 'GAUSS-JORDAN'

      ERROR = .FALSE.
      ERREUR_FUNCT = 10.
!! Test:
!      opt_nv_sb1 = 0
!      opt_nv_sb2 = 0
* Check all dates in trial vector are within the specified interval. 
* Otherwise return a  huge number so that the trial is immediately 
* rejected.
      DO I=1,NDIM
         IF (X(I).LT.OPT_JDMIN.OR.X(I).GT.OPT_JDMAX) GOTO 100
      ENDDO
*
* Should add here an observability check: source above a minimum elevation
* (30 degrees?), and sun below -12 degrees or so. Low elevation could be a
* smooth penalty rather than an abrupt cut. Moon could similarly be taken
* into account if one so wishes...
* Penalty should not be such as to discourage exploration of the next
* local minimum: should have some scaling according to typical errors.
* Multiply error by 2-3? Add a few times best error yet? Or current 
* temperature. Think more about this...
!      DO I=1,NDIM
!         IF (TOO_LOW) GOTO 100
!         IF (DAY) GOTO 100
!      ENDDO
*
* Fill unused part of the observation arrays with the trial dates, and 
* generate synthetic data. Not optimised, some constant information is
* filled at each and every iteration.
      J = NVR
      K = 0
      DO I=1,OPT_NV_SB1
         J = J+1
         K = K+1
         VR(J,I_TIME)  = X(K)
         VR(J,I_S_VR)  = OPT_SIGV_SB1
         CVR(J) = '1'
         STATVR(J) = 'Simulated'
         VREFCODE(J) = 1
      ENDDO
      DO I=1,OPT_NV_SB2
         J = J+1
         K = K+1
         VR(J,I_TIME) = X(K)
         VR(J,I_S_VR)  = OPT_SIGV_SB2A
         CVR(J) = '1'
         STATVR(J) = 'Simulated'
         VREFCODE(J) = 1
         J = J+1
         VR(J,I_TIME) = X(K)
         VR(J,I_S_VR)  = OPT_SIGV_SB2B
         CVR(J) = '2'
         STATVR(J) = 'Simulated'
         VREFCODE(J) = 1
      ENDDO
      J = NN
      DO I=1,OPT_NXY
         J = J+1
         K = K+1
         OBS(J,I_TIME) = X(K)
         OBS(J,I_S_X) = OPT_SIGXY
         OBS(J,I_S_Y) = OPT_SIGXY
         COBS(J) = 'XY'
         STATOBS(J) = 'Simulated'
      ENDDO
*
!      do imethod=1,2
!      call      SIC_LET_CHAR ('METHOD',tmpMETHOD(Imethod),ERROR)
!      if (error) stop 'ERROR setting variable'
      DO ITRIAL=1,NTRIALS
         CALL GENERATE_DATA(NVR+1,OPT_NV_SB1+2*OPT_NV_SB2,NVCORR+1,0,
     +        NN+1,OPT_NXY,NPARA+1,0)
!      do i=nvr+1,nvr+opt_nv_sb1+2*opt_nv_sb2
!         write(6,*) VR(i,I_TIME), VR(I,I_VR), VR(i,I_S_VR), CVR(i)
!      enddo
* Least square adjustment to the synthetic data (and any real data that
* may exist). Start from true elements for fast convergence.
         NN_SAVE  = NN
         NVR_SAVE = NVR
         NN  = NN+OPT_NXY
         NVR = NVR+OPT_NV_SB1+2*OPT_NV_SB2
         DO I=1,MA
            EL_TEST(I) = EL(I)
         ENDDO
         ERROR = .FALSE.
         CALL DO_FIT(MITER,EL_TEST,UPDATE,QUIET,WARNING,ERROR)
         NN  = NN_SAVE
         NVR = NVR_SAVE
* Non converged fit must correspond to a bad schedule, so dump it to the
* bad value bin
         IF (ERROR.OR.WARNING) THEN
            PRECISION(ITRIAL) = MAXR4
         ELSE
!      CALL COMPUTE_MASSES(EL_TEST,MA,.TRUE.,
!     +     a1sini,a2sini,f1m,f2m,
!     +     M1sin3i,dM1sin3i,M2sin3i,dM2sin3i,
!     +     Mtot,dMtot,M_A,dM_A,M_B,dM_B,
!     +     Orb_Par,dOrb_Par,ERROR)
!      write(6,*) M_B, dM_B, dM_B/M_B
*     
* Compute all astrophysical parameters and their errors
            CALL INIT_KEPLER(EL_TEST,.TRUE.)
* Return selected item 
            IF (OPT_TARGET.EQ.'MASS_TOT') THEN
               CALL GRAD_LOGMTOT(B)
               CALL EVAL_ERROR(PRECISION(ITRIAL),B) ! Return relative mass error
            ELSEIF (OPT_TARGET.EQ.'MASS_A') THEN
               CALL GRAD_LOGM1(B)
               CALL EVAL_ERROR(PRECISION(ITRIAL),B) ! Return relative mass error
            ELSEIF (OPT_TARGET.EQ.'MASS_B') THEN
               CALL GRAD_LOGM2(B)
               CALL EVAL_ERROR(PRECISION(ITRIAL),B) ! Return relative mass error
            ELSEIF (OPT_TARGET.EQ.'PARALLAX') THEN
               CALL GRAD_PARALLAX(Orb_Par,B)
               CALL EVAL_ERROR(PRECISION(ITRIAL),B) ! Returns absolute parallax error
               PRECISION(ITRIAL) = PRECISION(ITRIAL)/ORB_PAR !Convert to relative for consistency
            ENDIF
!            write(6,*) M_B, dM_B, dM_B/M_B, precision(itrial)
         ENDIF
      ENDDO
!         meth_result(imethod) = erreur_funct
!      enddo
!     write(6,'(2(1X,F7.5))') meth_result
* Select median precision of the NTRIALS samplings used for current observing
* dates set
      CALL GR4_TRIE(PRECISION,IT,NTRIALS,ERROR)
      IF (ERROR) THEN
         ERREUR_FUNCT = MAXR4 
      ELSE
         IF (MOD(NTRIALS,2).EQ.1) THEN
            ERREUR_FUNCT = PRECISION(NTRIALS/2+1)
         ELSE
            ERREUR_FUNCT = PRECISION(NTRIALS/2)+
     +           PRECISION(NTRIALS/2+1)
         ENDIF
      ENDIF
* Check for NaNs, and replace them by a very large real number
      IF (ERREUR_FUNCT.NE.ERREUR_FUNCT) ERREUR_FUNCT = MAXR4 
!      write(6,*) erreur_funct
      RETURN
*
 100  CONTINUE
      ERREUR_FUNCT = MAXR4
      RETURN
      END

