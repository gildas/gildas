      SUBROUTINE FORMAT_DATE(JDATE,CDATE,ERROR)
C----------------------------------------------------------------------
C ORBIT	
C	Convert a julian day to a human readable character string
C Arguments :
C	JDATE	R*8	Julian date
C       CDATE   C*(*)   Human readable string
C	ERROR   L       Error flag
C----------------------------------------------------------------------
      REAL*8 JDATE
      CHARACTER*(*) CDATE
      LOGICAL ERROR
*
      INTEGER ID,HH,MM
      CHARACTER NAME*11
      REAL*8 TT
      INTEGER LL
*
      ERROR = .FALSE.
      TT = JDATE-60549.5       ! Convert to "radio julian days": 
      ID = INT(TT)             !-2^15 is Karl Jansky's first radio observation
      IF (ID.GT.TT) THEN       ! Work around the stupid fortran INT definition
         ID=ID-1               ! for negative numbers
      ENDIF
      CALL DATEC(ID,NAME,ERROR)! Format to day of year
      IF (ERROR) THEN
         WRITE(6,*) 'E-LIST,  Error in date conversion ',ID
         RETURN
      ENDIF
      TT = (TT-ID)*24.D0
      HH = INT(TT)
      TT = (TT-HH)*60.D0
      MM = INT(TT)
      TT = (TT-MM)*60.D0 
*
      CDATE = NAME
      LL = 11
      IF (LEN(CDATE).LT.LL+3) RETURN  ! No space for hours
      WRITE (CDATE(LL+1:),100) HH
      LL = LL+3
      IF (LEN(CDATE).LT.LL+3) RETURN  ! No space for minutes
      WRITE (CDATE(LL+1:),101) MM
      LL = LL+3
      IF (LEN(CDATE).LT.LL+3) RETURN  ! No space for seconds
      IF (LEN(CDATE).LT.LL+5) THEN
         WRITE (CDATE(LL+1:),101) INT(TT)
         LL = LL+3
      ELSE
         WRITE (CDATE(LL+1:),102) TT
         LL = LL+5
      ENDIF
*         
      RETURN
 100  FORMAT (1X,I2.2)
 101  FORMAT (':',I2.2)
 102  FORMAT (':',F4.1)
      END

      SUBROUTINE CDATE(DATE,IDATE,ERROR)
C----------------------------------------------------------------------
C ASTRO Utility routine
C	Converts the date '01-JAN-1984' in internal code
C Arguments
C	DATE 	C*(*)	String date			Input
C	IDATE 	I	Internal coded date value	Output
C	ERROR 	L	Logical error flag		Output
C Subroutines :
C	(SIC)
C	DATJ,JDAT
C No commons
C----------------------------------------------------------------------
      CHARACTER*(*) DATE
      INTEGER IDATE
      LOGICAL ERROR
*
      CHARACTER*3 CM(12),M
      INTEGER ID, IY, IM
*
      DATA CM/'JAN','FEB','MAR','APR','MAY','JUN',
     $'JUL','AUG','SEP','OCT','NOV','DEC'/
*
      IF (DATE.EQ.'*') RETURN
      READ(DATE,'(I2,1X,A,1X,I4)',ERR=900) ID,M,IY
      CALL SIC_UPPER(M)
      DO 10 IM=1,12
         IF (CM(IM).EQ.M) THEN
            CALL DATJ(ID,IM,IY,IDATE)
            RETURN
         ENDIF
10    CONTINUE
*
* Wrong date
900   WRITE(6,'(A)') 'E-DATE,  Date conversion error'
      ERROR=.TRUE.
      RETURN
      END
*
      SUBROUTINE DATEC(IDATE,DATE,ERROR)
      CHARACTER*(*) DATE
      INTEGER IDATE
*
      LOGICAL ERROR
      CHARACTER*3 CM(12)
      INTEGER ID, IY, IM
*
      DATA CM/'JAN','FEB','MAR','APR','MAY','JUN',
     $'JUL','AUG','SEP','OCT','NOV','DEC'/

C-------
C	Converts the internal code to formatted date
C-------
      CALL JDAT(IDATE,ID,IM,IY)
      WRITE(DATE,1000,ERR=900) ID,CM(IM),IY
1000  FORMAT(I2.2,'-',A3,'-',I4)
      RETURN
 900  ERROR = .TRUE.
      END
*<FF>
      SUBROUTINE DATJ(ID,IM,IY,JOUR)
C----------------------------------------------------------------------
C ASTRO	Utility routine
C	Convert a date (id/im/iy) in number of days till the
C	01/01/1984
C Arguments :
C	ID	I	Day number		Input
C	IM	I	Month number		Input
C	IY	I	Year number		Input
C	JOUR	I	Elapsed day count	Output
C Subroutine
C	JULDA
C----------------------------------------------------------------------
      INTEGER ID, IM, IY
      INTEGER JOUR
      INTEGER IDEB(13), IBISS, JULDA, J, M, JOUR4
      INTEGER IZERO
      PARAMETER (IZERO=2025)
*
      DATA IDEB/0,31,59,90,120,151,181,212,243,273,304,334,365/
*
      JOUR4=ID+IDEB(IM)
      IBISS=JULDA(IY+1)-JULDA(IY)-365
      IF (MOD(IY,100).EQ.0.AND.MOD(IY,400).NE.0) IBISS=0
      IF (IM.GE.3) JOUR4=JOUR4+IBISS
      JOUR4=JOUR4+JULDA(IY)
      JOUR=JOUR4
      RETURN
*
      ENTRY JDAT(JOUR,ID,IM,IY)
C-------
C 	Reverse conversion
C-------
      IY=IZERO+JOUR/365
1     J=JOUR-JULDA(IY)
      IF (J.LE.0) THEN
         IY=IY-1
         GO TO 1
      ELSEIF (J.GT.365) THEN
* Special case for 31 december of leap years...
         IF (J.EQ.366) THEN
            IBISS=JULDA(IY+1)-JULDA(IY)-365
            IF (MOD(IY,100).EQ.0.AND.MOD(IY,400).NE.0) 
     +           IBISS=0
            IF (IBISS.EQ.0) THEN
               IY=IY+1
               GO TO 1
            ENDIF
         ELSE
            IY=IY+1
            GO TO 1
         ENDIF
      ENDIF
      IBISS=JULDA(IY+1)-JULDA(IY)-365
      IF (MOD(IY,100).EQ.0.AND.MOD(IY,400).NE.0) IBISS=0
      DO 2 M=12,1,-1
         ID=J-IDEB(M)
         IF (M.GE.3) ID=ID-IBISS
         IF (ID.GT.0) GO TO 3
2     CONTINUE
3     IM=M
      END
*<FF>
      FUNCTION JULDA(NYR)
C----------------------------------------------------------------------
C ASTRO	Utility routine
C	Returns Julian date of 1 day of Year
C Arguments
C	NYR	I	Year				Input
C	JULDA	I	Date returned			Output
C----------------------------------------------------------------------
      INTEGER NYR, NYRM1, IC, NADD, JULDA
      INTEGER IZERO
      PARAMETER (IZERO=2025)
      NYRM1=NYR-1
      IC=NYRM1/100
      NADD=NYRM1/4-IC+IC/4
      JULDA=365.*(NYRM1-IZERO)+NADD
      END
*<FF>
      SUBROUTINE ADATE(CHAIN,ID,IM,IY,ERROR)
      CHARACTER*(*) CHAIN
      INTEGER ID,IM,IY
      LOGICAL ERROR
*
      INTEGER I,IER
      CHARACTER*3 CM(12),M
      DATA CM/'JAN','FEB','MAR','APR','MAY','JUN',
     $'JUL','AUG','SEP','OCT','NOV','DEC'/
*
      ERROR = .TRUE.
      READ(CHAIN(1:11),'(I2,1X,A,1X,I4)',IOSTAT=IER) ID,M,IY
      IF (IER.NE.0) THEN
         WRITE(6,*) 'E-DATE, Error decoding formatted date'
         ID = 0
         IM = 0
         IY = 0
         RETURN
      ENDIF
      CALL SIC_UPPER(M)
      DO I=1,12
         IF (CM(I).EQ.M) THEN
            IM = I
            ERROR = .FALSE.
            RETURN
         ENDIF
      ENDDO
      END
