      SUBROUTINE COMPUTE_MAG(EL,NEL,MAG,SIG_MAG,DELTA_MAG,SIG_DMAG,
     +              COMPUTE_ERRORS,MAG_A,sMAG_A,MAG_B,sMAG_B,ERROR)
* Compute absolute magnitudes from apparent magnitudes and orbital elements
      INTEGER NEL
      REAL*8 EL(NEL)
      REAL MAG,SIG_MAG         ! Apparent magnitude of the pair
      REAL DELTA_MAG,SIG_DMAG  ! Magnitude difference
      REAL MAG_A,sMAG_A,MAG_B,sMAG_B ! Absolute magnitudes and probable errors
      LOGICAL COMPUTE_ERRORS,ERROR
*
      INCLUDE 'constant.inc'
*
      REAL B(MA)
      REAL Orb_Par,dOrb_Par       ! Orbital parallax and probable error
      REAL APPARENT(2),SIG2_APP(2) ! Apparent magnitudes of individual stars
      INCLUDE 'pi.inc'
      INCLUDE 'physical_constants.inc'
*
* Compute apparent magnitude of each of the two stars from their magnitude 
* difference and the apparent magnitude of the pair
*  m(A) = m +2.5 log10(1+10^(-delta_m/2.5))
*  m(B) = m +2.5 log10(1+10^(+delta_m/2.5))
*  dm(A) = dm - 2.5/Log(10)*10^(-delta_m/2.5))/[1+10^(-delta_m/2.5))] *ddelta_m
*  dm(B) = dm + 2.5/Log(10)*10^(+delta_m/2.5))/[1+10^(+delta_m/2.5))] *ddelta_m
      APPARENT(1) = MAG+2.5*LOG10(1+10**(-DELTA_MAG/2.5))
      APPARENT(2) = MAG+2.5*LOG10(1+10**(+DELTA_MAG/2.5))
!         write(6,*) (10**(-DELTA_MAG/2.5))/(1+10**(-DELTA_MAG/2.5)),
!     +        (10**(+DELTA_MAG/2.5))/(1+10**(+DELTA_MAG/2.5))
      SIG2_APP(1) = SIG_MAG**2 +
     +     (2.5/LOG(10.)*(10**(-DELTA_MAG/2.5))
     +     /(1+10**(-DELTA_MAG/2.5)))**2
     +     * (SIG_DMAG)**2
      SIG2_APP(2) = SIG_MAG**2 +
     +     (2.5/LOG(10.)*(10**(+DELTA_MAG/2.5))
     +     /(1+10**(+DELTA_MAG/2.5)))**2
     +     * (SIG_DMAG)**2
!      write(6,'(2(F6.3,F5.3))') mag,sig_mag, delta_mag, sig_dmag
!      Write(6,'(2(F6.3,F5.3))') apparent(1),sqrt(SIG2_APP(1)),
!     +     apparent(2),sqrt(SIG2_APP(2))
* SB2 with known non zero inclination: compute orbital parallax and convert 
* from apparent magnitudes to absolute ones
      IF((EL(ELINDX(IORB0)+I_K1)*EL(ELINDX(IORB0)+I_K2)).NE.0
     +     .AND.SIN(EL(ELINDX(IORB0)+I_i)/GR).NE.0) THEN  
         CALL GRAD_PARALLAX(Orb_Par,B)
         MAG_A = APPARENT(1)+5*LOG10(Orb_Par/0.1)
         MAG_B = APPARENT(2)+5*LOG10(Orb_Par/0.1)
         IF (COMPUTE_ERRORS) THEN
            CALL EVAL_ERROR(dOrb_Par,B)
* dM = dm + 5/LOG(10) dPi/Pi
            sMAG_A = SIG2_APP(1)+(5./LOG(10.)*dOrb_Par/Orb_Par)**2
            sMAG_A = SQRT(sMAG_A)
            sMAG_B = SIG2_APP(2)+(5./LOG(10.)*dOrb_Par/Orb_Par)**2
            sMAG_B = SQRT(sMAG_B)
         ENDIF
      ENDIF

      RETURN
*
! 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE GRAD_MAGNITUDE(EL,PAR,B)
*---------------------------------------------------------------------------
* Orbit:
* Compute an orbital parallax and its gradient relative to the 
* elements
* CAUTION: the gradient routines must NEVER contain the element common, they
* are called with trial elements.
*---------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      REAL*8 EL(MA)
      REAL PAR,B(MA)
*
      INCLUDE 'pi.inc'
      INCLUDE 'physical_constants.inc'
*
      INTEGER I
      REAL SI,CI,CF
*
* Compute sqrt(1-e**2) and trigonometric functions of inclination, which 
* are needed everywhere
      SI=SIN(EL(I_i+ELINDX(IORB0))/GR)
      CI=COS(EL(I_i+ELINDX(IORB0))/GR)
      IF (ABS(EL(I_Ecc+ELINDX(IORB0))).GT.1) THEN
         CF = 0.
      ELSE
         CF=SQRT(1.-EL(I_Ecc+ELINDX(IORB0))**2)
      ENDIF
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
* Return zero rather than NaNs or Inf whenever parameters are undefined
      IF ((EL(I_K1+ELINDX(IORB0))+EL(I_K2+ELINDX(IORB0))).EQ.0
     +     .OR.EL(I_PERIOD+ELINDX(IORB0)).EQ.0
     +     .OR.EL(I_as+ELINDX(IORB0)).EQ.0
     +     .OR.CF.EQ.0
     +     .OR.SI.EQ.0) THEN
         PAR = 0
      ELSE
* First, evaluate the logarithmic gradient
         B(I_PERIOD+ELINDX(IORB0)) = -1/EL(I_PERIOD+ELINDX(IORB0))
         B(I_Ecc+ELINDX(IORB0)) = +EL(I_Ecc+ELINDX(IORB0))/CF**2
         B(I_as+ELINDX(IORB0)) = +1/EL(I_as+ELINDX(IORB0))
         B(I_i+ELINDX(IORB0)) = CI/SI
         B(I_K1+ELINDX(IORB0)) = -1/(EL(I_K1+ELINDX(IORB0))
     +        +El(I_K2+ELINDX(IORB0)))
         B(I_K2+ELINDX(IORB0)) = -1/(EL(I_K1+ELINDX(IORB0))
     +        +El(I_K2+ELINDX(IORB0)))
* Then compute parallax value, and scale the gradient
         PAR=AU*2*PI*SI*EL(I_as+ELINDX(IORB0))
     +        /(((EL(I_K1+ELINDX(IORB0))
     +        +EL(I_K2+ELINDX(IORB0)))*KM_S)*CF*
     +        EL(I_PERIOD+ELINDX(IORB0))*DAY)
         DO I=1,MA
            B(I)=B(I)*PAR
         ENDDO
      ENDIF
      END


