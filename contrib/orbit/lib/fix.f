      SUBROUTINE FIX(LINE,ERROR)
      USE GKERNEL_INTERFACES
      CHARACTER LINE*(*)
      logical error
*      
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
*
      CHARACTER*16 ARGUM,KEYWOR
      CHARACTER*32 DATE
      INTEGER NKEY
      LOGICAL IS_DATE
      INTEGER I,NC,ND,IDUM
*  FIX/ALL
      IF (SIC_PRESENT(1,0)) THEN 
         DO I=1,MA
            EL_STATUS(I) = 'Fixed'          
         ENDDO
*  FIX Something [Value [Std_Err]]
      ELSE                      
         CALL SIC_CH (LINE,0,1,ARGUM,NC,.TRUE.,ERROR)
         IF (ERROR) RETURN
         NC = MAX(NC+1,LEN(ARGUM))
         ARGUM(NC:NC) = ' '
         NKEY = 0
         DO I=1,MA
            IF (ARGUM(1:NC).EQ.NAMEL(I)) NKEY = I
         ENDDO
         IF (NKEY.EQ.0) THEN
            CALL SIC_AMBIGS('ADJUST',ARGUM,KEYWOR,NKEY,NAMEL,
     +           MA,ERROR)
            IF (ERROR) RETURN
         ENDIF
*
         EL_STATUS(NKEY) = 'Fixed'
*
         IS_DATE = .FALSE.
         DO I=1,MORBIT
            IF (NKEY.EQ.(I_T0+ELINDX(I))) IS_DATE = .TRUE.
         ENDDO
         IF (IS_DATE) THEN
            CALL SIC_CH (LINE,0,2,DATE,ND,.FALSE.,ERROR)
            IF (ND.GT.0) THEN
               CALL DECODE_DATE(DATE(1:ND),EL(NKEY),IDUM,ERROR)
            ENDIF
         ELSE
            CALL SIC_R8(LINE,0,2,EL(NKEY),.FALSE.,ERROR)
         ENDIF
         IF (ELERR(NKEY).LT.0) ELERR(NKEY) = 0.
         CALL SIC_R8(LINE,0,3,ELERR(NKEY),.FALSE.,ERROR)
      ENDIF
      RETURN
*
      END
