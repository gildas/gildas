      FUNCTION FGAUSS (XVEL,NLINE,X)
C----------------------------------------------------------------------
C LAS	Internal routine
C	Returns the gradient of the sum of NLINE gaussian profiles 
C       relative to their descriptive parameters (area, center and width), 
C       at position XVEL.
C       Computations have been carefully optimised, with some attention to
C       R*8/R*4 mixing.
C Arguments :
C	XVEL	R*8	X value				Input
C	NLINE	I	Number of gaussian profiles	Input
C	X	R*8(*)	Parameter values		Input
C (5-mar-1985)
C----------------------------------------------------------------------
      INTEGER NLINE
      REAL*8 XVEL,X(*),FGAUSS
      REAL*4 FF,TT,VV,DD,T1,T2,T3,T4,T5,V1,V2,V3,V4,V5,D1,D2,D3,D4,D5,
     $ARG1,ARG2,ARG3,ARG4,ARG5,F1,F2,F3,F4,F5
*
      FF=0.
      TT=X(1)
      VV=X(2)
      DD=X(3)
      IF (NLINE.GT.5) WRITE(6,*) 'F-FGAUSS,  Too many lines',NLINE
      GOTO (1,2,3,4,5) NLINE
      GOTO 1
*
5     T5=X(16)*TT
      V5=X(17)+VV
      D5=X(18)*DD
*
4     T4=X(13)*TT
      V4=X(14)+VV
      D4=X(15)*DD
*
3     T3=X(10)*TT
      V3=X(11)+VV
      D3=X(12)*DD
*
2     T2=X(7)*TT
      V2=X(8)+VV
      D2=X(9)*DD
*
1     T1=X(4)*TT
      V1=X(5)+VV
      D1=X(6)*DD
* First
      ARG1 = (XVEL - V1) / D1
!      write(6,*) 'ARG1 ',arg1,'xvel',xvel,'v1',v1,'d1',d1
      IF (ABS(ARG1).GT.4.) THEN
         F1 = 0.
         FF = 0.
      ELSE
         F1 = EXP(-ARG1**2)
         FF = F1 * T1 / D1
      ENDIF
      IF (NLINE.LE.1) GOTO 610
* Second
      ARG2 = (XVEL - V2) / D2
      IF (ABS(ARG2).GT.4.) THEN
         F2=0.
      ELSE
         F2 = EXP(-ARG2**2)
         FF = FF + F2 * T2 / D2
      ENDIF
      IF (NLINE.LE.2) GOTO 610
* Third
      ARG3 = (XVEL - V3) / D3
      IF (ABS(ARG3).GT.4.) THEN
         F3=0.
      ELSE
         F3 = EXP(-ARG3**2)
         FF = FF + F3 * T3 / D3
      ENDIF
      IF (NLINE.LE.3) GOTO 610
* Fourth
      ARG4 = (XVEL - V4) / D4
      IF (ABS(ARG4).GT.4.) THEN
         F4=0.
      ELSE
         F4 = EXP(-ARG4**2)
         FF = FF + F4 * T4 / D4
      ENDIF
      IF (NLINE.LE.4) GOTO 610
* Fifth
      ARG5 = (XVEL - V5) / D5
      IF (ABS(ARG5).GT.4.) THEN
         F5=0.
      ELSE
         F5 = EXP(-ARG5**2)
         FF = FF + F5 * T5 / D5
      ENDIF
610   CONTINUE
      FGAUSS = FF
      END
*<FF>
      SUBROUTINE DGAUSS (XVEL,NLINE,X,G)
C----------------------------------------------------------------------
C LAS	Internal routine
C	Returns the gradient of the sum of NLINE gaussian profiles 
C       relative to their descriptive parameters (area, center and width), 
C       at position XVEL.
C       Computations have been carefully optimised, with some attention to
C       R*8/R*4 mixing.
C Arguments :
C	XVEL	R*8	X value				Input
C	NLINE	I	Number of gaussian profiles	Input
C	X	R*8(*)	Parameter values		Input
C	G	R*8(*)	Array of derivatives		Output
C (5-mar-1985)
C----------------------------------------------------------------------
      INTEGER NLINE
      REAL*8 XVEL,X(*),G(*)
*
      REAL TT,VV,DD,GT,GV,GD,FF,
     $T1,V1,D1,T2,V2,D2,T3,V3,D3,T4,V4,D4,T5,V5,D5,
     $G1,G2,G3,G4,G5,G6,G7,G8,G9,G10,G11,G12,G13,G14,G15,
     $ARG1,ARG2,ARG3,ARG4,ARG5,F1,F2,F3,F4,F5,ARG
*
      TT=X(1)
      VV=X(2)
      DD=X(3)
      GT=0.
      GV=0.
      GD=0.
      IF (NLINE.GT.5) WRITE(6,*) 'F-FGAUSS,  Too many lines',NLINE
      GOTO (1,2,3,4,5) NLINE
      GOTO 1
5     T5=X(16)*TT
      V5=X(17)+VV
      D5=X(18)*DD
      G13=0.
      G14=0.
      G15=0.
*
4     T4=X(13)*TT
      V4=X(14)+VV
      D4=X(15)*DD
      G10=0.
      G11=0.
      G12=0.
*
3     T3=X(10)*TT
      V3=X(11)+VV
      D3=X(12)*DD
      G7=0.
      G8=0.
      G9=0.
*
2     T2=X(7)*TT
      V2=X(8)+VV
      D2=X(9)*DD
      G4=0.
      G5=0.
      G6=0.
*
1     T1=X(4)*TT
      V1=X(5)+VV
      D1=X(6)*DD
      G1=0.
      G2=0.
      G3=0.
*
      FF = 1.0
* First
      ARG1 = (XVEL - V1) / D1
      IF (ABS(ARG1).LE.4.) THEN
         F1 = EXP(-ARG1**2)
         ARG = F1*FF/D1
         G1  = G1 + ARG
         GT  = GT + ARG*T1
         ARG = T1/D1*ARG
         G3  = G3 - ARG
         GD  = GD - ARG*D1
         ARG = ARG*ARG1*2.
         G2  = G2 + ARG
         GV  = GV + ARG
         G3  = G3 + ARG*ARG1
         GD  = GD + ARG*ARG1*D1
      ENDIF
      IF (NLINE.LE.1) GOTO 620
* Second
      ARG2 = (XVEL - V2) / D2
      IF (ABS(ARG2).LE.4.) THEN
         F2 = EXP(-ARG2**2)
         ARG = F2*FF/D2
         G4  = G4 + ARG
         GT  = GT + ARG*T2
         ARG = ARG*T2/D2
         G6  = G6 - ARG
         GD  = GD - ARG*D2
         ARG = ARG*ARG2*2.
         G5  = G5 + ARG
         GV  = GV + ARG
         G6  = G6 + ARG*ARG2
         GD  = GD + ARG*ARG2*D2
      ENDIF
      IF (NLINE.LE.2) GOTO 620
* Third
      ARG3 = (XVEL - V3) / D3
      IF (ABS(ARG3).LE.4.) THEN
         F3 = EXP(-ARG3**2)
         ARG = F3*FF/D3
         G7  = G7 + ARG
         GT  = GT + ARG*T3
         ARG = ARG*T3/D3
         G9  = G9 - ARG
         GD  = GD - ARG*D3
         ARG = ARG*ARG3*2.
         G8  = G8 + ARG
         GV  = GV + ARG
         G9  = G9 + ARG*ARG3
         GD  = GD + ARG*ARG3*D3
      ENDIF
      IF (NLINE.LE.3) GOTO 620
* Fourth
      ARG4 = (XVEL - V4) / D4
      IF (ABS(ARG4).LE.4.) THEN
         F4 = EXP(-ARG4**2)
         ARG = F4*FF/D4
         G10 = G10 + ARG
         GT  = GT + ARG*T4
         ARG = ARG*T4/D4
         G12 = G12 - ARG
         GD  = GD - ARG*D4
         ARG = ARG*ARG4*2.
         G11 = G11 + ARG
         GV  = GV + ARG
         G12 = G12 + ARG*ARG4
         GD  = GD + ARG*ARG4*D4
      ENDIF
      IF (NLINE.LE.4) GOTO 620
* Fifth
      ARG5 = (XVEL - V5) / D5
      IF (ABS(ARG5).LE.4.) THEN
         F5 = EXP(-ARG5**2)
         ARG = F5*FF/D5
         G13 = G13 + ARG
         GT  = GT  + ARG*T5
         ARG = ARG*T5/D5
         G15 = G15 - ARG
         GD  = GD - ARG*D5
         ARG = ARG*ARG5*2.
         G14 = G14 + ARG
         GV  = GV  + ARG
         G15 = G15 + ARG*ARG5
         GD  = GD  + ARG*ARG5*D5
      ENDIF
620   CONTINUE
*
* Setup values and return
      G(1)=GT/TT
      G(2)=GV
      G(3)=GD/DD
*
      G(4)=G1*TT
      G(5)=G2
      G(6)=G3*DD
      GOTO (701,702,703,704,705) NLINE
701   RETURN
705   G(16)=G13*TT
      G(17)=G14
      G(18)=G15*DD
704   G(13)=G10*TT
      G(14)=G11
      G(15)=G12*DD
703   G(10)=G7*TT
      G(11)=G8
      G(12)=G9*DD
702   G(7)=G4*TT
      G(8)=G5
      G(9)=G6*DD
      END
