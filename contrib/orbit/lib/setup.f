      SUBROUTINE SETUP_ELEMENTS(ERROR)
C------------------------------------------------------------------------------
C   Initialize the element's internal common
C------------------------------------------------------------------------------
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I,IELM,IORB
*
* Initialize the description of the orbit setup for the various supported
* hierarchical multiple  configurations
* Position of the elements of each orbit in global element array
      DO I=1,MORBIT
         ELINDX(I) = 7+(11+4*MVREF)*(I-1) 
      ENDDO
* Orbit names
      ORBNAME(IORB0) = ' ' ! Outer orbit is unnamed
      ORBNAME(IORB1) = '_1'
      ORBNAME(IORB2) = '_2'
* Orbit list for each configuration
      LISTORB0(1) = IORB0
      LISTORB1(1) = IORB0
      LISTORB1(2) = IORB1
      LISTORB2(1) = IORB0
      LISTORB2(2) = IORB2
      LISTORBALL(1) = IORB0
      LISTORBALL(2) = IORB1
      LISTORBALL(3) = IORB2
* Names of points for which a velocity can be measured
      COMPNAME(1,IORB0) = '1'
      COMPNAME(2,IORB0) = '2'
      COMPNAME(1,IORB1) = '11'
      COMPNAME(2,IORB1) = '12'
      COMPNAME(1,IORB2) = '21'
      COMPNAME(2,IORB2) = '22'
      COMPONENTS(1,IORB0) = 1
      COMPONENTS(2,IORB0) = 2
      COMPONENTS(1,IORB1) = 11
      COMPONENTS(2,IORB1) = 12
      COMPONENTS(1,IORB2) = 21
      COMPONENTS(2,IORB2) = 22
      LISTCOMPALL(1) = 1
      LISTCOMPALL(2) = 2
      LISTCOMPALL(3) = 11
      LISTCOMPALL(4) = 12
      LISTCOMPALL(5) = 21
      LISTCOMPALL(6) = 22
* Define elements' names and display formats. 
      DO I=1,MORBIT
         IORB = LISTORBALL(I)
*
         IELM = I_PERIOD+ELINDX(IORB)
         ELFMT(IELM) = 'F10.5'
         NAMEL(IELM) =  'P'//ORBNAME(IORB)
         IELM = I_T0+ELINDX(IORB)
         ELFMT(IELM) = 'F10.3'
         NAMEL(IELM) = 'T0'//ORBNAME(IORB)
         IELM = I_Ecc+ELINDX(IORB)    
         ELFMT(IELM) = 'F10.3'
         NAMEL(IELM) = 'Ecc'//ORBNAME(IORB)
         IELM = I_as+ELINDX(IORB)
         ELFMT(IELM) = 'F10.3'
         NAMEL(IELM) = 'as'//ORBNAME(IORB)
         IELM = I_OMEGA_U+ELINDX(IORB)
         NAMEL(IELM) = 'OM'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  
         IELM = I_OMEGA_L+ELINDX(IORB)
         NAMEL(IELM) = 'om'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  
         IELM = I_i+ELINDX(IORB)
         NAMEL(IELM) = 'i'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  
         IELM = I_K1+ELINDX(IORB)
         NAMEL(IELM) = 'K1'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.3'  
         IELM = I_K2+ELINDX(IORB)
         NAMEL(IELM) = 'K2'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.3' 
         IELM = I_DELTV1+ELINDX(IORB)
         NAMEL(IELM) = 'DELTV1'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.3'  ! 
         IELM = I_DELTV2+ELINDX(IORB)
         NAMEL(IELM) = 'DELTV2'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.3'  ! 
         IELM = I_W1+ELINDX(IORB)
         NAMEL(IELM) = 'W1'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  ! Should be replaced
         IELM = I_W2+ELINDX(IORB)
         NAMEL(IELM) = 'W2'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  ! Should be replaced
         IELM = I_EW1+ELINDX(IORB)
         NAMEL(IELM) = 'EW1'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  ! Should be replaced
         IELM = I_EW2+ELINDX(IORB)
         NAMEL(IELM) = 'EW2'//ORBNAME(IORB)
         ELFMT(IELM) = 'F10.2'  ! Should be replaced
*
      ENDDO
      DO I = MELEM+1, MELEM+MVREF-1
      ENDDO
      NAMEL(I_V0) = 'V0 '
      NAMEL(I_X0) = 'X0 '
      NAMEL(I_Y0) = 'Y0 '
      NAMEL(I_MuX) = 'muX '
      NAMEL(I_MuY) = 'muY '
      NAMEL(I_EPOCH) = 'EPOCH '
      DO I=MELEM+1,MELEM+MVREF-1
         NAMEL(I) = ' '
         ELFMT(I) = 'F10.4'
      ENDDO
*
      END
