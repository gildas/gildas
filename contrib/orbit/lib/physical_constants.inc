      REAL*8 DAY
      PARAMETER (DAY=86400) ! One day in seconds
      REAL*8 MSOL 
      PARAMETER (MSOL=1.9891D33) ! Solar mass in grams
      REAL*8 RSOL 
      PARAMETER (RSOL=6.9599E10) ! Solar radius in cm
      REAL*8 G
      PARAMETER (G=6.6726E-8) ! CGS gravitation constant
      REAL*8 KM_S
      PARAMETER (KM_S=1E5) ! 1km/s in CGS 
      REAL*8 km
      PARAMETER (km=1E5) ! 1km in CGS 
      REAL*8 AU
      PARAMETER (AU=1.4959787061E13) ! CGS astronomical unit
      REAL*8 PARSEC
      PARAMETER (PARSEC=3.085678E18) ! cgs parsec
      REAL*8 YEAR
      PARAMETER (YEAR=365.24219) ! One year in days
