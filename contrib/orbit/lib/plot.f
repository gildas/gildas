      SUBROUTINE PLOT(LINE,ERROR)
      CHARACTER LINE*(*)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'control.inc'
      INCLUDE 'data.inc'
      LOGICAL SIC_PRESENT,VELOCITY,VISUAL,CORRELATION
      INTEGER L, LN,LC
      CHARACTER COMPONENT*4, CORB*4, NAME*4
*
* Get orbit number
      CORB = '0'
      CALL SIC_CH(LINE,1,1,CORB,L,.FALSE.,ERROR)
      L = MIN(MAX(L,1),LEN(CORB))
      IF (CORB(1:L).EQ.'0') THEN
         PLOT_IORB = IORB0
      ELSEIF (CORB(1:l).EQ.'1') THEN
         PLOT_IORB = IORB1
      ELSEIF (CORB(1:l).EQ.'2') THEN
         PLOT_IORB = IORB2
      ELSE
         WRITE(6,*) 'E-PLOT,  Orbit ',CORB(1:L),' not (yet) supported'
      ENDIF
*
      VELOCITY    = sic_present(2,0)
      VISUAL      = sic_present(3,0)
      CORRELATION = sic_present(4,0)
      if (.not.velocity.and..not.correlation) then
         VISUAL = .TRUE.
      elseif (velocity.and.visual) then
         write(6,*) 'Options /VELOCITY and /VISUAL are incompatible'
         goto 99
      ENDIF
*
      if (velocity) then
         PLOT_TYPE = 'RADIAL VELOCITY'
         COMPONENT = 'AB' ! By default plot both components
         CALL SIC_CH(LINE,2,1,COMPONENT,L,.FALSE.,ERROR)
         CALL PLOT_RV(PLOT_IORB,COMPONENT,ERROR)
      elseif(visual) then
         PLOT_TYPE = 'VISUAL ORBIT'
         CALL PLOT_VISUAL(PLOT_IORB,ERROR)
      elseif(CORRELATION) then
         PLOT_TYPE = 'CORRELATION DIP'
         CALL SIC_CH(LINE,4,1,NAME,LN,.TRUE.,ERROR)
         LN = MAX(1,LN)
         COMPONENT = '1'
         CALL SIC_CH(LINE,4,2,COMPONENT,LC,.FALSE.,ERROR)
         LC = MAX(1,LC)
         CALL PLOT_CORRELATION(NAME(1:LN),
     +        COMPONENT(1:LC),ERROR)
      else
         write(6,*) 'Unknown option for command visu'
         goto 99
      endif
      return

 99   error = .true.
      RETURN
      END
C ----------------------------------------------
*
      SUBROUTINE PLOT_CORRELATION(NAME,COMPONENT,ERROR)
* Plot coravel-like correlation velocity profile
      CHARACTER*(*) NAME,COMPONENT
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'control.inc'
      include 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
*
      REAL    X(2*MVPNTS),Y(2*MVPNTS),ZBAR(MVPNTS)
      REAL*8 XREF,XVAL,XINC
      REAL X1,XN
      INTEGER ICOR,ICOMP
      INTEGER I,NPNT
      CHARACTER*16 TYPE
*
      IF (NVCORR.LE.0) THEN
         WRITE (6,*) 'E-VISU,  No correlation velocity data'
         GOTO 99
      ENDIF
*
      IF (NAME(1:1).EQ.'A') THEN
         TYPE = 'AVERAGE'
         READ (COMPONENT,*,ERR=97) ICOMP
         ICOR = 0
      ELSE
         TYPE = 'INDIVIDUAL'
         READ (NAME,*,ERR=98) ICOR
         IF (ICOR.LT.1 .OR. ICOR.GT.NVCORR) THEN
            WRITE (6,*)
     +           'E-VISU,  Specified correlation velocity profile',
     +           ' is out of existing range'
            GOTO 99
         ENDIF
         ICOMP = 0
      ENDIF
*
* Fill velocity and error bar arrays
      NPNT=MVPNTS
      IF (TYPE.EQ.'INDIVIDUAL') THEN
         CALL GET_CORREL_PROFILE(ICOR,NPNT,X,Y,ZBAR)
      ELSEIF (TYPE.EQ.'AVERAGE') THEN
         CALL AVERAGE_CORREL_PROFILE(ICOMP,NPNT,XREF,XVAL,XINC,
     +        Y,ZBAR,ERROR)
         IF (ERROR) GOTO 99
         DO I=1,NPNT
            X(I) = SNGL(XVAL+(I-XREF)*XINC)
         ENDDO
      ELSE
         WRITE(6,*) 'E-VISU,  Unknown profile plot mode ',TYPE
      ENDIF
* Display data
      CALL GR_EXEC('CLEAR PLOT')
      CALL GR4_GIVE('X',NPNT,X)
      CALL GR4_GIVE('Y',NPNT,Y)
      CALL GR_EXEC('LIMITS  ')
      CALL GR_EXEC('SET BOX LANDSCAPE')
      CALL GR_EXEC('BOX')
      CALL GR_EXEC('HISTO')
* Display fitted profile as an overlay
      IF (TYPE.EQ.'INDIVIDUAL'.OR.TYPE.EQ.'AVERAGE') THEN
* Define a denser velocity grid for the fit
         X1 = X(1)
         XN = X(NPNT)
         NPNT = 2*MIN(NPNT,MVPNTS)
         XREF = 1.
         XVAL = X1
         XINC = (XN-X1)/NPNT
* Retrieve the full fitted profile, and plot it
         IF (TYPE.EQ.'INDIVIDUAL') THEN
            CALL GET_MODEL_PROFILE(ICOR,2*MORBIT,LISTCOMPALL,
     +           .TRUE.,PROFILE_METHOD.EQ.'CORRECTED',
     +           NPNT,XREF,XVAL,XINC,Y,ERROR)
         ELSEIF (TYPE.EQ.'AVERAGE') THEN
            CALL GET_MODEL_PROFILE(0,1,ICOMP,.TRUE.,
     +        PROFILE_METHOD.EQ.'CORRECTED',
     +        NPNT,XREF,XVAL,XINC,Y,ERROR)
         ENDIF
         IF (ERROR) GOTO 99
         DO I=1,NPNT
            X(I) = XN*((I-1.)/NPNT)+X1*(1-(I-1.)/NPNT)
         ENDDO
         CALL GR4_GIVE('X',NPNT,X)
         CALL GR4_GIVE('Y',NPNT,Y)
                                !      CALL GR_EXEC('LIMITS = = < > ')
         CALL GR_EXEC('PEN 2')
         CALL GR_EXEC('CONNECT')
         CALL GR_EXEC('PEN /DEFAULT ')
      ELSE
         WRITE(6,*) 'E-VISU,  Unknown profile plot mode ',TYPE
      ENDIF
      RETURN
*
 97   WRITE(6,*) 'E-VISU,  Unknown component ',COMPONENT
      GOTO 99
*
 98   WRITE(6,*) 'E-VISU,  Unavailable correlation profile mode '
     +     ,NAME
 99   CONTINUE
      ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE GET_MODEL_PROFILE(ICOR,NCOMP,COMP,
     +     ADD_ONE,APPLY_CORRECTION,
     +     NPNT,REF,VAL,INC,Y,ERROR)
C------------------------------------------------------------------------------
* Return fitted gaussian(s) for one correlation profile for a pre-defined
* velocity grid. By convention, the zeroth correlation profile is centered
* on zero.
*
** FIXME: much of this code is (quasi-)duplicated in USE_VCORR. Need to
* consolidate... Started, but more could be done.
C------------------------------------------------------------------------------
      INTEGER ICOR     ! Correlation profile number
      INTEGER NCOMP    ! Number of fitted components
      INTEGER COMP(*)  ! Fitted component numbers
                       ! One component with number 0 means all components
      INTEGER NPNT     ! Number of data points in velocity grid
      REAL*8 REF,VAl,INC
      REAL Y(NPNT)
      LOGICAL ADD_ONE,APPLY_CORRECTION,ERROR
*
      INCLUDE 'constant.inc'
      include 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
      INCLUDE 'control.inc'
      INCLUDE 'corrections.inc'
*
      REAL*8 PARG(3+3*NCOMPONENTS),FGAUSS
      REAL*8 X
      REAL*8 T
      INTEGER I,IC,ICOMP,CC
      INTEGER IW(2,MORBIT),IEW(2,MORBIT)
      INTEGER I_EW,I_WIDTH
      REAL VEL,FLUX
      INTEGER NLINE
      INTEGER IORB
      INTEGER CODE
      REAL CORRECTION(4*MVPNTS)
      REAL PROFILE_MEAN
*
* Prepare descriptive parameters of the N gaussians
      PARG(1)=1 ! Parameters for dependent gaussians. Not used here, set
      PARG(2)=0 ! to default.
      PARG(3)=1
*
      DO I=1,NPNT
         CORRECTION(I) = 0
      ENDDO
*
      IF (ICOR.GT.0.AND.ICOR.LE.NVCORR) THEN
         CALL INIT_KEPLER(EL,.FALSE.)
         T = COR_TIME(ICOR)
         CODE = CORREFCOD(ICOR)
      ELSEIF (ICOR.EQ.0) THEN ! Zero-centered profile, by convention
         CODE = 1   ! Show parameters of first origin for the time being,
                    ! but will need a control mechanism
      ELSE
         WRITE(6,'(1X,A,I7,A)') 'Correlation profile number ',ICOR,
     +        'is out of range'
         GOTO 99
      ENDIF
      IF (NPNT.GT.4*MVPNTS) THEN
         WRITE(6,'(1X,A,A,I7,A,I7)') 'E-GETMODEL,  Requested number',
     +        ' of points ',NPNT,' is greater than buffer size',4*MVPNTS
         WRITE(6,'(A)') 'E-GETMODEL,  Request an increase...'
         GOTO 99
      ENDIF
*
* Determine which parameters apply to current profile, looking first for
* specific parameters for the current radial velocity origin, and falling
* back to generic ones if the latter are absent. This is done on a per
* parameter basis.
      CALL SELECT_PROFILE_PARAMETERS(CODE,IEW,IW,ERROR)
      IF (ERROR) GOTO 99
*
      NLINE = 0
      DO IC=1,NCOMP
         ICOMP=COMP(IC)
* Look for requested components amongst the existing orbits, and fill in
* the parameter array with the description of the corresponding gaussians
         DO IORB=1,MORBIT
            DO I=1,2
               CC = COMPONENTS(I,IORB)
               I_EW    = IEW(I,IORB)
               I_WIDTH = IW(I,IORB)
               CC = COMPONENTS(I,IORB)
               IF (ICOMP.EQ.0.OR.ICOMP.EQ.CC) THEN
                  IF (EL(I_EW).NE.0) THEN
*     Area for component is non-zero, it is present in data
                     NLINE=NLINE+1
                     IF (ICOR.EQ.0) THEN
                        VEL = 0
                     ELSE
                        CALL GET_VELOCITY(T,CC,
     +                       CORREFCOD(ICOR),VEL,ERROR)
                        IF (ERROR) GOTO 99
                     ENDIF
                     PARG(NLINE*3+1) = EL(I_EW)
                     PARG(NLINE*3+2) = VEL
                     PARG(NLINE*3+3) = EL(I_WIDTH)
                     IF (APPLY_CORRECTION) THEN
                        CALL ACCUMULATE_PROFILE_CORRECTION(I,IORB,
     +                       VEL,NPNT,REF,VAL,INC,CORRECTION,ERROR)
                     ENDIF
                  ENDIF
               ENDIF
            ENDDO
         ENDDO
      ENDDO
!      do i=1,nline
!         write(6,'(3(F10.4))') parg(i*3+1),parg(i*3+2),parg(i*3+3)
!      enddo
* Determine normalisation factor (for Andrei's unnormalised profiles at least)
* See later if this should be kept or not
      IF (ICOR.GT.0) THEN
         FLUX = PROFILE_MEAN(NVPNTS(ICOR),
     +        COR_REFPIX(ICOR),COR_REFVAL(ICOR),COR_VSTEP(ICOR),
     +        VCORR(1,ICOR),NLINE,PARG)
      ELSE
         FLUX = 1
      ENDIF
!      write(6,'(A,12F8.3)') 'PARG: ',(PARG(I),I=1,3+3*nline)
*  Fill model profile array
      DO I=1,NPNT
         X = VAL+(I-REF)*INC
         Y(I) = -SNGL(FGAUSS(X,NLINE,PARG))
         Y(I) = Y(I)+CORRECTION(I)
      ENDDO
      IF (ADD_ONE) THEN
         DO I=1,NPNT
            Y(I) = Y(I)+1
         ENDDO
      ENDIF
* Normalize it
      DO I=1,NPNT
         Y(I) = FLUX*Y(I)
!      if (y(i).ne.y(i).or.abs(y(i)).gt.1.1) then
!         write(6,*) 'plot:',i,x,y(i),correction(i)
!         write(6,'(A,12F8.3)') 'PARG: ',(PARG(j),j=1,3+3*nline)
!      endif
      ENDDO

      RETURN
*
 99   ERROR = .TRUE.
      END
*
*
      SUBROUTINE GET_CORREL_PROFILE(ICOR,NPNT,X,Y,ZBAR)
      INTEGER ICOR,NPNT
      REAL X(NPNT),Y(NPNT),ZBAR(NPNT)
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INTEGER I
*
      NPNT=MIN(NPNT,NVPNTS(ICOR))
      DO I=1,NPNT
         X(I)=SNGL(COR_REFVAL(ICOR)
     +        +(I-COR_REFPIX(ICOR))*COR_VSTEP(ICOR))
         Y(I)= VCORR(I,ICOR)
         ZBAR(I)=COR_SIGM(ICOR)
      ENDDO
      RETURN
      END
*
      SUBROUTINE PLOT_RV(IORB,COMPO,ERROR)
      USE GILDAS_DEF
* Plot radial velocity curve
      INTEGER IORB ! Orbit number (0 for outer orbit, 1 or 2 for inner ones)
      CHARACTER*(*) COMPO ! Component of binary (A, B or AB for both)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      include 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
*
      REAL*8 ZBAR(2*MVR)
      INTEGER NSIDES(2*MVR),ISTYLE(2*MVR)
      REAL    X(NPTS_VR),Y1(NPTS_VR),Y2(NPTS_VR)
      INTEGER I,K
      INTEGER(KIND=SIZE_LENGTH) NELEM
      CHARACTER CHAIN*64,STRING*72
      REAL PHASE
*
      CHARACTER COMPONENT*16
      INTEGER PORB  ! Plotted orbit number
      SAVE COMPONENT,PORB
*
      IF (COMPO.NE.' ') COMPONENT = COMPO
      IF (IORB.GE.0) PORB=IORB
*
* Create phase and radial velocity arrays.
* Zero points offsets are removed when necessary, as well as any
* contribution from other orbits.
      CALL INIT_KEPLER(EL,.FALSE.)
      CALL PREPARE_VRPLOT(PORB,NVR,VR,VREFCODE,CVR,STATVR,
     +     NVCORR,COR_TIME,STATVCORR,
     +    PLOT_ NV,PLOT_V,PLOT_XV,ZBAR,NSIDES,ISTYLE,VR_INDEX,ERROR)
      IF (ERROR) GOTO 99
* Duplicate all data to cover the [-0.5,1.5] phase interval
      DO I=1,PLOT_NV
         PLOT_V(I+PLOT_NV)   = PLOT_V(I)
         ZBAR(I+PLOT_NV)= ZBAR(I)
         IF (PLOT_XV(I).LE.0.5) THEN
            PLOT_XV(I+PLOT_NV)=PLOT_XV(I)+1.0
         ELSE
            PLOT_XV(I+PLOT_NV)=PLOT_XV(I)-1.0
         ENDIF
         NSIDES(I+PLOT_NV)=NSIDES(I)
         ISTYLE(I+PLOT_NV)=ISTYLE(I)
         VR_INDEX(I+PLOT_NV) = VR_INDEX(I)
      ENDDO
*
*     Write star name and period
      WRITE (STRING(23:60),'(F10.3)') EL(I_PERIOD+1)
      STRING(17:22)='    P='
      STRING (1:16)=OBJECT(2)
* Generate a radial velocity curve
      K=NPTS_VR/4
      DO I=1,4*K
         PHASE=(I-1.-K)/(2*K)
         X(I)=PHASE
         CALL SET_PHASE(PORB,PHASE)
         IF (PORB.EQ.IORB0) THEN
            CALL VALUE_V1_V2(1,Y1(I),Y2(I))

         ELSE
            CALL KEPLER_RV(PORB,Y1(I),Y2(I))
         ENDIF
      ENDDO
      CALL GR_EXEC('CLEAR PLOT')
      CALL GR_EXEC('SET BOX LANDSCAPE')
* Determine limits where everything will fit, and plot a box
      IF (EL(I_V0).NE.0) THEN
         WRITE(CHAIN,'(A,2(1X,G15.8))') 'LIMITS -0.24 1.24',
     +        EL(I_V0)*(1-5*EPSR4),EL(I_V0)*(1+5*EPSR4)
      ELSE
         CHAIN = 'LIMITS -0.24 1.24 -1E-4 +1E-4'
      ENDIF
      CALL GR_EXEC(CHAIN)
*
      IF (INDEX(COMPONENT,'A').NE.0) THEN
         IF (Y1(1).NE.Y1(2)) THEN ! K1 must be non zero
            CALL GR4_GIVE('Y',NPTS_VR,Y1)
            CALL GR_EXEC('LIMITS = = < >')
         ENDIF
      ENDIF
      IF (INDEX(COMPONENT,'B').NE.0) THEN
         IF (Y2(1).NE.Y2(2)) THEN ! K2 must be non zero
            CALL GR4_GIVE('Y',NPTS_VR,Y2)
            CALL GR_EXEC('LIMITS = = < >')
         ENDIF
      ENDIF
      IF (PLOT_NV.GT.0) THEN
         CALL GR8_GIVE('Y',PLOT_NV,PLOT_V)
         CALL GR_EXEC('LIMITS = = < >')
      ENDIF
      CALL GR_EXEC('BOX')
* Fitted curves
      CALL GR4_GIVE('X',NPTS_VR,X)
      IF (INDEX(COMPONENT,'A').NE.0) THEN
         CALL GR4_GIVE('Y',NPTS_VR,Y1)
         CALL GR_EXEC('CONNECT')
      ENDIF
      IF (INDEX(COMPONENT,'B').NE.0) THEN
         CALL GR4_GIVE('Y',NPTS_VR,Y2)
         CALL GR_EXEC('CONNECT')
      ENDIF
* Data points
      CALL GR8_GIVE('X',2*PLOT_NV,PLOT_XV)
      CALL GR8_GIVE('Y',2*PLOT_NV,PLOT_V)
      CALL GR8_GIVE('Z',2*PLOT_NV,ZBAR)
      CALL GR_EXEC('SET MARKER /DEF ')

      CALL GR_SEGM('MARKERS',ERROR)
      NELEM = 2*PLOT_NV
      CALL POINTS(NSIDES,ISTYLE,-1.,0.5,PLOT_XV,PLOT_V,
     +     ZBAR,NELEM,.FALSE.)
      CALL GR_SEGM_CLOSE(ERROR)
      CALL GR_EXEC('ERRORBAR Y')
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE PREPARE_VRPLOT(IORB,NVR,VR,VREFCODE,CVR,STATVR,
     +     NVCORR,COR_TIME,STATVCORR,
     +     NV,V,XV,ZBAR,NSIDES,ISTYLE,INDEX,ERROR)
* Prepare plot arrays for a phased radial velocity plot: remove zero point
* contribution if any, as well as those from other orbits, compute phase
* for each measurement time, and select plot markers.
      INTEGER IORB
      INTEGER NVR,NV,NVCORR
      INTEGER VREFCODE(NVR)
      CHARACTER*(*) CVR(NVR),STATVR(NVR),STATVCORR(NVCORR)
      REAL*8 COR_TIME(NVCORR)
      REAL*8 V(NVR+2*NVCORR),XV(NVR+2*NVCORR),ZBAR(NVR+2*NVCORR)
      INTEGER ISTYLE(NVR+2*NVCORR),NSIDES(NVR+2*NVCORR)
      INTEGER INDEX(NVR+2*NVCORR)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      REAL*8 VR(MVR,5)
*
      INCLUDE 'elements.inc'
*
      INTEGER I, CODE
      REAL PHASE,PH
      REAL VEL,VA,VB
      INTEGER SIDES
      CHARACTER*8 STAR
      LOGICAL KEEP
*
      NV = 0
      DO I=1,NVR
         CALL SET_TIME(1,IORB,VR(I,I_TIME),PHASE)
         VEL = SNGL(VR(I,I_VR))
         STAR = CVR(I)
         KEEP = .TRUE.
         IF (IORB.EQ.IORB0) THEN
            IF     (STAR(1:1).EQ.'1') THEN
               SIDES=3      ! Triangles for primary components
               IF (STAR.NE.'1') THEN  ! Subtract inner orbit contribution
                  CALL SET_TIME(1,IORB1,VR(I,I_TIME),PH)
                  CALL KEPLER_RV(IORB1,VA,VB)
                  IF (STAR.EQ.'11') THEN
                     VEL = VEL-VA
                  ELSEIF (STAR.EQ.'12') THEN
                     VEL = VEL-VB
                  ELSE
                     KEEP = .FALSE.
                  ENDIF
               ENDIF
            ELSEIF (STAR(1:1).EQ.'2') THEN
               SIDES=4      ! Squares
               IF (STAR.NE.'2') THEN ! Subtract inner orbit contribution
                  CALL SET_TIME(1,IORB2,VR(I,I_TIME),PH)
                  CALL KEPLER_RV(IORB2,VA,VB)
                  IF (STAR.EQ.'21') THEN
                     VEL = VEL-VA
                  ELSEIF (STAR.EQ.'22') THEN
                     VEL = VEL-VB
                  ELSE
                     KEEP = .FALSE.
                  ENDIF
               ENDIF
            ELSE
               KEEP = .FALSE.
            ENDIF
         ELSEIF (IORB.EQ.IORB1) THEN
* Subtract outer orbit contribution
            CALL SET_TIME(1,IORB0,VR(I,I_TIME),PH)
            CALL VALUE_V1_V2(1,VA,VB)
            VEL = VEL-VA
            IF (STAR.EQ.'11') THEN
               SIDES=3      ! Triangles for primary components
            ELSEIF (STAR.EQ.'12') THEN
               SIDES=4
            ELSE
               KEEP = .FALSE.
            ENDIF
         ELSEIF (IORB.EQ.IORB2) THEN
* Subtract outer orbit contribution
            CALL SET_TIME(1,IORB0,VR(I,I_TIME),PH)
            CALL VALUE_V1_V2(1,VA,VB)
            VEL = VEL-VB
            IF (STAR.EQ.'21') THEN
               SIDES=3      ! Triangles for primary components
            ELSEIF (STAR.EQ.'22') THEN
               SIDES=4
            ELSE
               KEEP = .FALSE.
            ENDIF
         ELSE
            WRITE(6,*) 'E-PLOT,  Unsupported orbit number',IORB
            GOTO 99
         ENDIF
         IF (.NOT.KEEP) THEN
            WRITE(6,*) 'I-PLOT,  Unused velocity, type V',STAR
         ELSE
* Correct zero point difference
            CODE = VREFCODE(I)
            IF (CODE.GT.1) THEN
               VEL = VEL-SNGL(EL(MELEM+CODE-1))
            ENDIF
* Fill in plot arrays
            NV = NV+1
            V(NV) = VEL
            INDEX(NV) = I
            ZBAR(NV)  = VR(I,I_S_VR)
            VR(I,I_PHASE) = PHASE ! Needed??
            XV(NV)=PHASE
            NSIDES(NV) = SIDES
* Set polygon style according to measurement status
            IF (STATVR(I).EQ.'OK') THEN
               ISTYLE(NV) = 3   ! Filled polygon
            ELSEIF(STATVR(I).EQ.'Ignored') THEN
               ISTYLE(NV) = 0   ! Empty polygon
            ELSEIF(STATVR(I).EQ.'Simulated') THEN
               ISTYLE(NV) = 2   ! Starred polygon
            ELSEIF(STATVR(I).EQ.'Killed') THEN
               ISTYLE(NV) = 1   ! Vertex connected
            ELSE
               WRITE(6,*) 'E-PLOT,  Unknown status ',STATVR(I),
     +              ' for velocity',I
               ISTYLE(NV) = 0
            ENDIF
         ENDIF
      ENDDO
*
* Plot a marker ON the orbit for each correlation profile, to indicate
* the available phase coverage.
      DO I=1,NVCORR
         CALL SET_TIME(1,IORB,COR_TIME(I),PHASE)
         IF (IORB.EQ.IORB0) THEN
            CALL VALUE_V1_V2(1,VA,VB)
         ELSE
            CALL KEPLER_RV(IORB,VA,VB)
         ENDIF
* Component A
         NV = NV+1
         V(NV) = VA
         INDEX(NV) = -I
         ZBAR(NV)  = 0  ! No meaningful error bars
         XV(NV)=PHASE
         NSIDES(NV) = 10
* Set polygon style according to measurement status
         IF (STATVCORR(I).EQ.'OK') THEN
            ISTYLE(NV) = 3      ! Filled polygon
         ELSEIF(STATVCORR(I).EQ.'Ignored') THEN
            ISTYLE(NV) = 0      ! Empty polygon
         ELSEIF(STATVCORR(I).EQ.'Simulated') THEN
            ISTYLE(NV) = 2      ! Starred polygon
         ELSEIF(STATVCORR(I).EQ.'Killed') THEN
            ISTYLE(NV) = 1      ! Vertex connected
         ELSE
            WRITE(6,*) 'E-PLOT,  Unknown status ',STATVCORR(I),
     +           ' for velocity profile',I
            ISTYLE(NV) = 0
         ENDIF
* Component A
         NV = NV+1
         V(NV) = VB
         INDEX(NV) = -I
         ZBAR(NV)  = 0   ! No meaningful error bars
         XV(NV)=PHASE
         NSIDES(NV) = 10
* Set polygon style according to measurement status
         IF (STATVCORR(I).EQ.'OK') THEN
            ISTYLE(NV) = 3      ! Filled polygon
         ELSEIF(STATVCORR(I).EQ.'Ignored') THEN
            ISTYLE(NV) = 0      ! Empty polygon
         ELSEIF(STATVCORR(I).EQ.'Simulated') THEN
            ISTYLE(NV) = 2      ! Starred polygon
         ELSEIF(STATVCORR(I).EQ.'Killed') THEN
            ISTYLE(NV) = 1      ! Vertex connected
         ELSE
            WRITE(6,*) 'E-PLOT,  Unknown status ',STATVCORR(I),
     +           ' for velocity profile',I
            ISTYLE(NV) = 0
         ENDIF
      ENDDO


      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE PLOT_VISUAL(IORB,ERROR)
      USE GILDAS_DEF
C ---------------------------------
C Plot visual orbit
      INTEGER IORB
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      include 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
      INCLUDE 'plot.inc'
*
      REAL XX,YY,RHO,DUMMY,XPERI,YPERI
      REAL XMAX,XMIN,YMAX,YMIN
      REAL ZBAR(MOBS),ZBARP(MOBS),ORIENT(MOBS),XEXTR(MOBS),YEXTR(MOBS)
      INTEGER NSIDES(MOBS),ISTYLE(MOBS)
      REAL*8 DXDISP(MOBS),DYDISP(MOBS)
!      REAL AXY,AZT,AXZ,AXT,AYZ,AYT,AXYZT
      REAL FI
      REAL SQ2
      INTEGER I,J,K
      INTEGER(KIND=SIZE_LENGTH) NN_SL
      REAL PHASE
      REAL POSANG,CPOS,SPOS
      CHARACTER CLOC*1
      LOGICAL IS_1D
*
      INTEGER PORB  ! Plotted orbit. Memorised for refreshes
      SAVE PORB
*
      IF (IORB.NE.-1) PORB = IORB  ! -1 is conventional code for a refresh
*
      IF (PORB.EQ.IORB0) THEN
         CONTINUE
!      ELSEIF (PORB.EQ.IORB1) THEN
!         CONTINUE
!      ELSEIF (PORB.EQ.IORB2) THEN
!         CONTINUE
      ELSE
         WRITE(6,*) 'E-PLOT,  Inner visual orbits  not yet supported'
         ERROR = .TRUE.
         RETURN
      ENDIF

      SQ2=SQRT(2.0)
      CALL GR_EXEC('CLEAR PLOT')
*
      CALL INIT_KEPLER(EL,.FALSE.)
*
* Fill plotting arrays: complement projected separation measurements by the
* predicted position along the orthogonal direction, and transform rho+theta
* pairs to X+Y.
      DO I=1,NN
*
         IF  (IS_1D(COBS(I))) THEN ! Single coordinate datum
            CALL SET_TIME(1,PORB,OBS(I,I_TIME),PHASE)
*
            IF (COBS(I).EQ.'theta'.OR.COBS(I).EQ.'PROJ') THEN
!               CALL KEPLER_XY(XX,YY)
               CALL KEPLER_XY(YY,XX)
            ELSEIF(COBS(I).EQ.'PPROJ') THEN
               CALL KEPLER_PXY(YY,XX) !
            ELSE
               WRITE(6,'(1X,A,A,A)') 'W-PLOT, Unsupported ',
     +              '1D observation type ',COBS(I)
               XX = 0.0
               YY = 0.0
            ENDIF
*
            IF (COBS(I).EQ.'PROJ'.OR.COBS(I).EQ.'PPROJ') THEN
* Convert astronomical E of N Position Angle to trigonometric convention
* and radians
               POSANG = SNGL((90.D0-OBS(I,I_POSANG))/GR )
!               POSANG = OBS(I,I_POSANG)/GR
               CPOS = COS(POSANG)
               SPOS = SIN(POSANG)
               DXDISP(I)=OBS(I,I_PROJ)*CPOS-(-XX*SPOS+YY*CPOS)*SPOS
               DYDISP(I)=OBS(I,I_PROJ)*SPOS+(-XX*SPOS+YY*CPOS)*CPOS
               ZBAR(I)=SNGL(OBS(I,I_S_PROJ))
               ZBARP(I)=0.
               ORIENT(I)=SNGL(90.D0-OBS(I,I_POSANG))
            ELSE
               WRITE(6,*) 'W-PLOT,  Unexpected data type ',COBS(I),
     +              ', should have been converted to PROJ. Ignored...'
               ERROR = .TRUE.
               RETURN
            ENDIF
         ELSE
C store double coordinate data and their uncertainties in
C X,Y,TI ,ZBAR,ZBARP arrays
            IF (COBS(I).EQ.'XY') THEN
               DXDISP(I)=OBS(I,I_Y)
               DYDISP(I)=OBS(I,I_X)
               ZBAR(I) =SNGL(OBS(I,I_S_X))
               ZBARP(I)=SNGL(OBS(I,I_S_Y))
               ORIENT(I)=0.
            ELSEIF(COBS(I).EQ.'PXY') THEN
               DXDISP(I)=OBS(I,I_X)
               DYDISP(I)=OBS(I,I_Y)
               ZBAR(I) =SNGL(OBS(I,I_S_Y))
               ZBARP(I)=SNGL(OBS(I,I_S_X))
               ORIENT(I)=0.
            ELSEIF(COBS(I).EQ.'AXY') THEN
               CALL SET_TIME(1,PORB,OBS(I,I_TIME),PHASE)
               CALL KEPLER_PARPOSPM(OBS(I,I_PIFX),OBS(I,I_PIFY),
     +              YY,XX)
               DXDISP(I)= OBS(I,I_X)-XX
               DYDISP(I)= OBS(I,I_Y)-YY
!               write(6,*) OBS(I,I_X), XX, OBS(I,I_Y), YY
!               write(6,*) DXDISP(I), DYDISP(I)
               ZBAR(I) =SNGL(OBS(I,I_S_Y))
               ZBARP(I)=SNGL(OBS(I,I_S_X))
               ORIENT(I)=0.
            ELSEIF (COBS(I).EQ.'ZT') THEN
               DXDISP(I)=(OBS(I,I_Z)+OBS(I,I_T))/SQ2
               DYDISP(I)=(OBS(I,I_Z)-OBS(I,I_T))/SQ2
               ZBAR(I)=SNGL(MAX(OBS(I,I_S_Z),OBS(I,I_S_T)))/SQ2
               ZBARP(I)=ZBAR(I)
               ORIENT(I)=45.
            ELSEIF (COBS(I).EQ.'rho-theta'.OR.
     +              COBS(I).EQ.'theta') THEN
               IF (COBS(I).EQ.'theta') THEN ! Fill in Rho from model
                  CALL SET_TIME(1,PORB,OBS(I,I_TIME),PHASE)
                  CALL KEPLER_RHOTHETA(RHO,DUMMY)
                  OBS(I,I_S_RHO) = 0.0
               ELSE
                  RHO = SNGL(OBS(I,I_RHO))
               ENDIF
* Convert astronomical position angle to trigonometric convention
               ORIENT(I)=SNGL(90.D0-OBS(I,I_THETA))
!               ORIENT(I)=OBS(I,I_THETA)
               FI=ORIENT(I)/SNGL(GR )
               DXDISP(I)=RHO*COS(FI)
               DYDISP(I)=RHO*SIN(FI)
               ZBAR(I)=SNGL(OBS(I,I_S_RHO))
               ZBARP(I)=SNGL(OBS(I,I_S_THETA)/GR*RHO)
            ELSE
               WRITE(6,'(1X,A,A,A)') 'W-PLOT, Unsupported ',
     +              '2D observation type ',COBS(I)
               DXDISP(I)= 0 ! OBS(I,I_X)
               DYDISP(I)= 0 ! OBS(I,I_Y)
               ZBAR(I) =SNGL(OBS(I,I_S_Y))
               ZBARP(I)=SNGL(OBS(I,I_S_X))
               ORIENT(I)=0.
            ENDIF
         ENDIF
         XDISP(I) = SNGL(DXDISP(I))
         YDISP(I) = SNGL(DYDISP(I))
         IF (STATOBS(I).EQ.'OK') THEN
            ISTYLE(I) = 3         ! Filled polygon
         ELSEIF(STATOBS(I).EQ.'Ignored') THEN
            ISTYLE(I) = 0         ! Empty polygon
         ELSEIF(STATOBS(I).EQ.'Simulated') THEN
            ISTYLE(I) = 2         ! Starred polygon
         ELSEIF(STATOBS(I).EQ.'Killed') THEN
            ISTYLE(I) = 1         ! Vertex connected
         ELSE
            WRITE(6,*) 'E-PLOT,  Unknown status ',STATOBS(I),
     +           ' for position measurement',I
            ISTYLE(I) = 0
         ENDIF
         NSIDES(I) = 4
      ENDDO

* Compute NPTS points on orbit
      DO  I=1,NPTS
         PHASE=(I-1.)/NPTS
         CALL SET_PHASE(PORB,PHASE)
         MODEL_PHASE(I) = PHASE
         CALL KEPLER_XY(XX,YY)
!         MODEL_X(I)=XX
!         MODEL_Y(I)=YY
!         MODEL_X(I)=-YY
         MODEL_X(I)=YY
         MODEL_Y(I)=XX
      ENDDO
* Determine plot limits for all data points (with their error bars) plus the
* fitted orbit
      CALL GR_EXEC('LIMITS -1e-4 1e-4 -1e-4 1e-4')
      IF (NN.GT.0) THEN
         DO K=-1,1,2
            DO J=-1,1,2
               DO I=1,NN
                  CPOS = SNGL(COS(ORIENT(I)*PI/180.D0))
                  SPOS = SNGL(SIN(ORIENT(I)*PI/180.D0))
                  XEXTR(I) = XDISP(I)+J*CPOS*ZBAR(I)-K*SPOS*ZBARP(I)
                  YEXTR(I) = YDISP(I)+J*SPOS*ZBAR(I)+K*CPOS*ZBARP(I)
               ENDDO
               CALL GR4_GIVE('X',NN,XEXTR)
               CALL GR4_GIVE('Y',NN,YEXTR)
               CALL GR_EXEC('LIMITS < > < > ')
            ENDDO
         ENDDO
      ENDIF
      CALL GR4_GIVE('X',NPTS,MODEL_X)
      CALL GR4_GIVE('Y',NPTS,MODEL_Y)
      CALL GR_EXEC('LIMITS < > < >  /REVERSE X')
* Protect against extreme ratios, that GreG would not accept
      CALL SIC_GET_REAL('USER_YMAX',YMAX,ERROR)
      CALL SIC_GET_REAL('USER_YMIN',YMIN,ERROR)
      CALL SIC_GET_REAL('USER_XMAX',XMAX,ERROR)
      CALL SIC_GET_REAL('USER_XMIN',XMIN,ERROR)
      IF (ABS(XMAX-XMIN).LE.0.2*ABS(YMAX-YMIN)) THEN
         XMAX = (XMAX+XMIN)/2.+(XMAX-XMIN)/2.*
     +        ABS(YMAX-YMIN)/ABS(XMAX-XMIN)/5.
         XMIN = (XMAX+XMIN)/2.-(XMAX-XMIN)/2.*
     +        ABS(YMAX-YMIN)/ABS(XMAX-XMIN)/5.
         CALL GR_LIMI(4,XMIN,XMAX,YMIN,YMAX)
      ELSEIF (ABS(XMAX-XMIN).GE.5*ABS(YMAX-YMIN)) THEN
         YMAX = (YMAX+YMIN)/2.+(YMAX-YMIN)/2.*
     +        ABS(XMAX-XMIN)/ABS(YMAX-YMIN)/5.
         YMIN = (YMAX+YMIN)/2.-(YMAX-YMIN)/2.*
     +        ABS(XMAX-XMIN)/ABS(YMAX-YMIN)/5.
         CALL GR_LIMI(4,XMIN,XMAX,YMIN,YMAX)
      ENDIF
      CALL GR_EXEC('SET BOX MATCH')
      CALL GR_EXEC('BOX')
* Plot data points and their error-bars
      CALL GR_SEGM('MARKERS',ERROR)
      NN_SL = NN
      CALL POINTS(NSIDES,ISTYLE,-1.,0.5,DXDISP,DYDISP,dydisp,NN_SL,
     +  .FALSE.)
      CALL GR_SEGM_CLOSE(ERROR)
*
      CALL GR_SEGM('ERRORBARS',ERROR)
      CALL GR4_BARS ('O',NN,XDISP,YDISP,ZBAR,ORIENT,0,-1.)
      DO I=1,NN
         ORIENT(I)=ORIENT(I)+90
      ENDDO
      CLOC = 'O'
      CALL GR4_BARS (CLOC,NN,XDISP,YDISP,ZBARP,ORIENT,0,-1.)
      CALL GR_SEGM_CLOSE(ERROR)
* Plot computed orbit. Individual points are shown rather than a continuous
* curve, to give a feeling for relative velocity variations along the orbit.
      CALL GR4_GIVE('X',NPTS,MODEL_X)
      CALL GR4_GIVE('Y',NPTS,MODEL_Y)
      CALL GR_EXEC('SET MARKER 6 3 0.05')
      CALL GR_EXEC('POINTS')
*     Connect observed data points to the computed points for their date
      DO I=1,NN
         CALL SET_TIME(1,PORB,OBS(I,I_TIME),PHASE)
         IF (COBS(I).NE.'PXY'
     +        .AND.COBS(I).NE.'PPROJ'
     +        .AND.COBS(I).NE.'AXY'
     +        .AND.COBS(I).NE.'APROJ'
     +        ) THEN
!            CALL KEPLER_XY(XX,YY)
            CALL KEPLER_XY(YY,XX)
         ELSE
            CALL KEPLER_PXY(YY,XX)
         ENDIF
         CALL GR_DRAW_USER
         CALL GR_DRAW('RELOCATE',2,XX,YY)
         CALL GR_DRAW_USER
         CALL GR_DRAW('LINE',2,XDISP(I),YDISP(I))
      ENDDO
*
* Draw a marker at periastron, and indicate velocity there with an arrow
      CALL GR_EXEC('SET MARKER 4 0 0.4')
      PHASE = 0.
      CALL SET_PHASE(PORB,PHASE)
!      CALL KEPLER_XY(XPERI,YPERI)
      CALL KEPLER_XY(YPERI,XPERI)
      CALL GR_DRAW_USER
      CALL GR_DRAW('MARKER',2,XPERI,YPERI)
      PHASE=1./NPTS
      CALL SET_PHASE(PORB,PHASE)
!      CALL KEPLER_XY(XX,YY)
      CALL KEPLER_XY(YY,XX)
      CALL GR_DRAW_USER
      CALL GR_DRAW('ARROW',2,XX,YY)
* Draw a line from periastron to apoastron
      CALL GR_DRAW_USER
      CALL GR_DRAW('RELOCATE',2,XPERI,YPERI)
      PHASE = 0.5
      CALL SET_PHASE(PORB,PHASE)
!      CALL KEPLER_XY(XX,YY)
      CALL KEPLER_XY(YY,XX)
      CALL GR_DRAW_USER
      CALL GR_DRAW('LINE',2,XX,YY)
C Show primary star position
      XX = 0
      YY = 0
      CALL GR_EXEC('SET MARKER 4 0 0.4')
      CALL GR_DRAW_USER
      CALL GR_DRAW('MARKER',2,XX,YY)
      RETURN
      END


      LOGICAL FUNCTION IS_1D(CODE)
* Test if datatype code corresponds to a 1D or 2D position measurement
      CHARACTER*(*) CODE
*
      IS_1D = CODE.NE.'XY'.AND.
     +        CODE.NE.'PXY'.AND.CODE.NE.'AXY'.AND.
     +        CODE.NE.'ZT'.AND.
     +        CODE.NE.'theta'.AND.
     +        CODE.NE.'rho-theta'
      RETURN
      END
