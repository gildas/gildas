      SUBROUTINE ORBIT_LIST(LINE,ERROR)
      USE GKERNEL_INTERFACES
      CHARACTER LINE*(*)
      LOGICAL ERROR
*      
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
*     
      INTEGER MVOCAB
      PARAMETER (MVOCAB=6)
      CHARACTER*12 VOCAB(MVOCAB),ARGUM,KEYWORD
      CHARACTER*11 DATE
      CHARACTER*8 INFO_LIST(9)
      CHARACTER FILE*80,CHAIN*80
      INTEGER LUN,STATUS
      LOGICAL ALL,MONTECARLO
      INTEGER I,ID,NE,NCH,NKEY,NTRY,NINFO,IDUM
      REAL*8 T0
      REAL DT,CONFIDENCE
      REAL*8 MC_EL(MA,M_MC) ! Orbital elements resulting of montecarlo estimate
*
      DATA VOCAB /'DATA','EPHEMERID','ELEMENTS','MASSES',
     +     'COVARIANCES','STATISTICS'/
* Determine requested action type from command line
      ERROR = .FALSE.
      NCH = 1
      ARGUM = ' '
      CALL SIC_CH (LINE,0,1,ARGUM,NCH,.TRUE.,ERROR)
      CALL SIC_UPPER(ARGUM(1:NCH))
      CALL SIC_AMBIGS('LIST',ARGUM,KEYWORD,NKEY,VOCAB,MVOCAB,ERROR)
      IF (ERROR) RETURN
* Parameters of montecarlo error estimate
      MONTECARLO = SIC_PRESENT(2,0)
      IF (MONTECARLO) THEN
         CONFIDENCE = 0.683  ! 68.3% confidence interval (i.e. 1 sigma for 
                             ! normal errors) by default, for consistency with
                             ! the linear estimate
!         CONFIDENCE = 0.954  ! 2 sigma
!         CONFIDENCE = 0.9973 ! 3 sigma
         CALL SIC_R4(LINE,2,1,CONFIDENCE,.FALSE.,ERROR)
         IF (CONFIDENCE.LE.0.OR.CONFIDENCE.GE.1) THEN
            WRITE(6,*) 'E-LIST,  Requested confidence level ',
     +           ' outside ]0,1['
            GOTO 99
         ENDIF
         NTRY = NINT(50/(1-CONFIDENCE))  ! Try to keep decent statistics
         CALL SIC_I4(LINE,2,2,NTRY,.FALSE.,ERROR)
         IF (NTRY.GT.M_MC) THEN
            WRITE(6,*) 'E-LIST,  Too many montecarlo samples. ',
     +           NTRY,' requested, maximum is ',M_MC
            GOTO 99
         ENDIF
      ELSE
         NTRY = 0
      ENDIF
* Open explicit output file if requested
      IF (SIC_PRESENT(1,0)) THEN
         STATUS = SIC_GETLUN (LUN)
         IF (STATUS.NE.1) THEN
            WRITE(6,*) 'E-LIST,  No logical unit left'
            GOTO 99
         ENDIF
         CALL SIC_CH (LINE,1,1,CHAIN,NCH,.TRUE.,ERROR)
         CALL SIC_PARSEF(CHAIN(1:NCH),FILE,' ','.LIS')
         OPEN(UNIT=LUN,FILE=FILE,STATUS='UNKNOWN',
     +        IOSTAT=STATUS)
         IF (STATUS.NE.0) THEN
            WRITE(6,'(A)') 'E-LIST,  Cannot open file ',
     +           FILE(1:LENC(FILE))
            CALL PUTIOS('E-LIST,  ',STATUS)
            CALL SIC_FRELUN(LUN)
            GOTO 99
         ENDIF
         WRITE(LUN,'(A)',IOSTAT=STATUS) ' '
         IF (STATUS.NE.0) THEN
            WRITE(6,*) 'E-LIST,  Cannot write to file ',
     +           FILE(1:LENC(FILE))
            CALL PUTIOS('E-LIST,  ',STATUS)
            CLOSE(LUN)
            CALL SIC_FRELUN(LUN)
            GOTO 99
         ENDIF
         REWIND(LUN)
      ELSE
         LUN=6  
      ENDIF
* Check for inconsistent command lines     
      IF (KEYWORD.EQ.'DATA'.OR.KEYWORD.EQ.'STATISTICS') THEN
         IF (MONTECARLO) THEN
            WRITE(6,'(3(A))') 'E-LIST, Option /MONTECARLO is ',
     +           ' meaningless for LIST ',KEYWORD
            GOTO 99
         ENDIF
      ENDIF
      IF (KEYWORD.NE.'EPHEMERID'.AND.SIC_PRESENT(3,1)) THEN 
         WRITE(6,'(3(A))') 'E-LIST, Option /SELECT is ',
     +        ' restricted to LIST EPHEMERID'
         GOTO 99
      ENDIF
*
      IF (MONTECARLO) THEN
         CALL MONTE_CARLO_ELEMENTS(NTRY,MC_EL,ERROR)
         IF (ERROR) GOTO 99
         WRITE(LUN,'(A,F5.3,A)') 'Confidence intervals for ',
     +        CONFIDENCE,' confidence level'
      ENDIF
*
* Call the print subroutine for the requested action
      IF (KEYWORD.EQ.'DATA') THEN
         CALL PRINT_DATA(LUN,ERROR)
         IF (ERROR) GOTO 99
      ELSEIF(KEYWORD.EQ.'STATISTICS') THEN
         CALL PRINT_STATISTICS(LUN,ERROR)
         IF (ERROR) GOTO 99
      ELSEIF (KEYWORD.EQ.'EPHEMERID') THEN !
* Default ephemerid information: Phase v1 v2 rho theta
         IF (.NOT.SIC_PRESENT(3,1)) THEN 
            NINFO = 5
            INFO_LIST(1) = 'PHASE'
            INFO_LIST(2) = 'V1'
            INFO_LIST(3) = 'V2'
            INFO_LIST(4) = 'RHO'
            INFO_LIST(5) = 'THETA'
* Customised ephemerid. Retrieve information list from command
* line.
         ELSE
            NINFO = MAX(1,MIN(9,SIC_NARG(3)))
            DO I=1,NINFO
               CALL SIC_CH(LINE,3,I,INFO_LIST(I),NCH,.TRUE.,ERROR)
               IF (ERROR) GOTO 99
               CALL SIC_UPPER(INFO_LIST(I))
            ENDDO
         ENDIF
*
         NE = 20                ! 20 dates is a reasonable default
         CALL SIC_I4(LINE,0,2,NE,.FALSE.,ERROR)
         IF (ERROR) RETURN
         IF (NE.GT.1E3) THEN    ! Protect against typing mistakes...
            WRITE(6,*) 'Too many ephemeris dates, ',NE
            GOTO 99
         ENDIF
         CHAIN = '*'
         CALL SIC_CH(LINE,0,3,CHAIN,NCH,.FALSE.,ERROR)
         IF (ERROR) RETURN
         IF (CHAIN.NE.'*') THEN ! Starting epoch is specified
            CALL DECODE_DATE(CHAIN,T0,IDUM,ERROR)
         ELSE                   ! Default to today
            CALL SIC_DATE(DATE)
            CALL CDATE(DATE,ID,ERROR)
            T0 = ID+60549.5D0  ! Transform radio julian days to JD-2.4E6
         ENDIF
         DT = SNGL(EL(I_PERIOD+ELINDX(IORB0))/NE) ! Default range is one period
         CALL SIC_R4(LINE,0,4,DT,.FALSE.,ERROR)
         IF (ERROR) RETURN
* LIST EPH/SELECT v12  rho theta
         CALL PRINT_EPHEMERID(LUN,NE,T0,DT,MONTECARLO,NTRY,
     +        CONFIDENCE,MC_EL,NINFO,INFO_LIST,ERROR)
      ELSEIF(KEYWORD.EQ.'ELEMENTS') THEN ! Orbital elements
         ARGUM = ' '
         CALL SIC_CH (LINE,0,2,ARGUM,NCH,.FALSE.,ERROR)
         IF (NCH.EQ.0) THEN
            ALL = .FALSE.
         ELSE
            ALL = ARGUM(1:NCH).EQ.'ALL'
         ENDIF
         CALL PRINT_ELEMENTS(LUN,MONTECARLO,NTRY,
     +        CONFIDENCE,MC_EL,ALL,ERROR)
      ELSEIF(KEYWORD.EQ.'MASSES') THEN ! Astrophysical parameters
         CALL PRINT_MASSES(LUN,MONTECARLO,NTRY,CONFIDENCE,
     +        MC_EL,ERROR)
         IF (ERROR) GOTO 99
      ELSEIF(KEYWORD.EQ.'COVARIANCES') THEN ! Covariance matrix
         CALL PRINT_COVAR(LUN,ERROR)
         IF (ERROR) GOTO 99
      ELSE
         WRITE(6,*) 'Supported arguments for command LIST are',
     +        (VOCAB(I),I=1,MVOCAB)
      ENDIF
* Close output file
      IF (LUN.NE.6) THEN
         CLOSE(UNIT=LUN)
         CALL SIC_FRELUN(LUN)
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE PRINT_COVAR(LUN,ERROR)
* List the covariance matrix of the orbital elements
      INTEGER LUN
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
*
      REAL CORR(MA)
      INTEGER I,J,JJ
*
      WRITE(6,'(A)') 'Orbital elements and their errors:'
      DO I=1,MA
         IF (NAMEL(I).NE.' '.AND.ELERR(I).GT.0) THEN
            WRITE(LUN,'(1X,A,I2,1X,A,1X,A,F13.6,1X,F11.6,1X,F11.6)') 
     +           'Element: ',I,
     +           NAMEL(I),EL_STATUS(I),EL(I),ELERR(I),SQRT(ALPHA(I,I))
         ENDIF
      ENDDO
      WRITE(6,'(A)') ' '
*
!      WRITE(6,'(A)') 'Standard errors'
!      WRITE(6,*) (SQRT(ALPHA(I,I)),I=1,MA)
!      WRITE(6,'(A)') ' '
*      
      WRITE(6,'(A)') 'Correlation matrix:'
      DO I=1,MA
         IF (
     +        NAMEL(I).NE.' '.AND.
     +        ELERR(I).GE.0.AND.
     +        ALPHA(I,I).NE.0) THEN
            JJ = 0
            DO J=1,MA
               IF (
     +              NAMEL(J).NE.' '.AND.
     +              ELERR(J).GE.0.AND.
     +              ALPHA(J,J).NE.0) THEN
                  JJ=JJ+1
!                  CORR(JJ) = ALPHA(I,J)/ELERR(I)/ELERR(J)
                  CORR(JJ) = ALPHA(I,J)/SQRT(ALPHA(I,I))
     +                 /SQRT(ALPHA(J,J))
               ENDIF
            ENDDO
            WRITE(LUN,'(50(F5.2))') (CORR(J),J=1,JJ)
         ENDIF
      ENDDO

      RETURN
*
      RETURN
      END

*
      SUBROUTINE PRINT_ELEMENTS(LUN,MONTECARLO,NSAMPLE,
     +     CONF_LEVEL,MC_EL,ALL,ERROR)
      USE GKERNEL_INTERFACES
* List the orbital elements
      INCLUDE 'constant.inc'
      INTEGER LUN,NSAMPLE
      REAL*8 MC_EL(MA,NSAMPLE)
      REAL CONF_LEVEL
      LOGICAL MONTECARLO,ALL,ERROR
*
      INCLUDE 'elements.inc'
*
      INTEGER I,J,HIGH,LOW
      REAL*8 R8(M_MC)
      INTEGER IT(M_MC)
* Should probably adjust the output format to match the actual element 
* accuracy
      IF (.NOT.MONTECARLO) THEN
         DO I=1,MA
            IF (NAMEL(I).NE.' '
     +           .AND.(ELERR(I).GE.0.OR.ALL)) THEN
               WRITE(LUN,'(1X,A,I3,1X,A,1X,A,F13.6,1X,F11.6)') 
     +              'Element: ',I,
     +              NAMEL(I),EL_STATUS(I),EL(I),ELERR(I)
            ENDIF
         ENDDO
      ELSE  ! Montecarlo branch
         IF (NSAMPLE.LE.0) THEN
            WRITE(6,*) 'E-LIST, Number of montecarlo samples is ',
     +           'negative'
            GOTO 99
         ENDIF
         DO I=1,MA
* Sort each element to determine confidence intervals
            IF (NAMEL(I).NE.' '
     +           .AND.(ELERR(I).GE.0.OR.ALL)) THEN
               DO J=1,NSAMPLE
                  R8(J) = MC_EL(I,J)
               ENDDO
               CALL GR8_TRIE(R8,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
               LOW  = NINT(NSAMPLE*(1.-CONF_LEVEL)/2.)
               HIGH = NSAMPLE-LOW+1
               IF (LOW.LE.1) THEN 
                  WRITE(6,*) 'E-LIST, Number of montecarlo samples ',
     +                 NSAMPLE,' is insufficient for requested ',
     +                 'confidence level'
                  GOTO 99
               ENDIF
               WRITE(LUN,100)
     +              'Element: ',I,
     +              NAMEL(I),EL_STATUS(I),EL(I),R8(LOW),R8(HIGH)
            ENDIF
         ENDDO
      ENDIF
 100  FORMAT(1X,A,I2,1X,A,1X,A,F13.6,1X,'[',F13.6,',',F13.6,']')
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE PRINT_STATISTICS(LUN,ERROR)
      INTEGER LUN
      LOGICAL ERROR
* List basic statistics on model residuals
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'elements.inc'
* Radial velocities first (one has to recompute stats to display them by origin)
      CALL PRINT_VR_STAT(LUN,1,NVR,MVR,VR,CVR,STATVR,
     +     VREFCODE,NVREF,ERROR)
      IF (ERROR) GOTO 99
*
      IF (NMEAS(TYPE_VCORR).GT.0) THEN
         WRITE(LUN,*) 'Correlation profiles: '
         WRITE(LUN,101) ' ','ND','Sigma','Chi2'
         WRITE(LUN,102) 
     +        NMEAS(TYPE_VCORR),SD(TYPE_VCORR),CHI2(TYPE_VCORR)
      ENDIF
 101  FORMAT (T18,2(A8,A5,1X,A8,1X,A9))
 102  FORMAT (T18,2(8X,I5,1X,F8.3,1X,F9.1))

* Dump all visual origins together for the time being...
      IF (NMEAS(TYPE_THETA).GT.0) THEN
         WRITE(LUN,'(A)') 'Rho+Theta '
         WRITE(LUN,101) 'Rho ','ND','Sigma','Chi2'
     +        ,'Theta ','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_RHO),SD(TYPE_RHO),CHI2(TYPE_RHO)
     +        ,NMEAS(TYPE_THETA),SD(TYPE_THETA),CHI2(TYPE_THETA)
      ENDIF
*
      IF (NMEAS(TYPE_X).GT.0) THEN
         WRITE(LUN,'(A)') 'XY'
         WRITE(LUN,101) 'X ','ND','Sigma','Chi2'
     +        ,'Y ','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_X),SD(TYPE_X),CHI2(TYPE_X)
     +        ,NMEAS(TYPE_Y),SD(TYPE_Y),CHI2(TYPE_Y)
      ENDIF
*
      IF (NMEAS(TYPE_Z).GT.0) THEN
         WRITE(LUN,'(A)') 'ZT '
         WRITE(LUN,101) 'Z ','ND','Sigma','Chi2'
     +        ,'T ','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_Z),SD(TYPE_Z),CHI2(TYPE_Z)
     +        ,NMEAS(TYPE_T),SD(TYPE_T),CHI2(TYPE_T)
      ENDIF
*
      IF (NMEAS(TYPE_PROJ).GT.0) THEN
         WRITE(LUN,'(A)') 'Projected separation'
         WRITE(LUN,101) 'PROJ','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_PROJ),SD(TYPE_PROJ),CHI2(TYPE_PROJ)
      ENDIF

*
      IF (NMEAS(TYPE_PAR).GT.0) THEN
         WRITE(LUN,'(A)') 'Parallax'
         WRITE(LUN,101) 'Par','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_PAR),SD(TYPE_PAR),CHI2(TYPE_PAR)
      ENDIF
*
      IF (NMEAS(TYPE_VISI_SQUARED).GT.0) THEN
         WRITE(LUN,'(A)') 'Squared visibilities'
         WRITE(LUN,101) 'V^2','ND','Sigma','Chi2'
         WRITE(LUN,102) NMEAS(TYPE_VISI_SQUARED),SD(TYPE_VISI_SQUARED),
     +        CHI2(TYPE_VISI_SQUARED)
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      END

      SUBROUTINE PRINT_VR_STAT(LUN,FIRST_VR,LAST_VR,NVR,VR,CVR,
     +     STATUS,CODE,NVREF,ERROR)
C-----------------------------------------------------------------------------
C Orbit:
C   Print radial velocity statistics on requested logical unit, in a human
C   readable format (hopefully).
C Arguments:
C    LUN       I      Logical unit for output
C    FIRST_VR  I      First displayed radial velocity
C    LAST_VR   I      Last displayed radial velocity
C    NVR       I      Dimension of radial velocity array
C    VR        R*8    Radial velocity array
C    CVR       C*(*)  Radial velocity type
C    CODE      I(*)   Code for radial velocity referential
C    STATUS    C*(*)  Measurement status (OK or Ignored)
C-----------------------------------------------------------------------------
      INTEGER LUN,FIRST_VR,LAST_VR,NVR
      INTEGER CODE(NVR),NVREF
      REAL*8 VR(NVR,5)
      CHARACTER*(*) CVR(NVR),STATUS(NVR)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INTEGER ND(NCOMPONENTS,MVREF),NDTOT(NCOMPONENTS)
*
      REAL*8 SIGMA(NCOMPONENTS,MVREF),CHI2(NCOMPONENTS,MVREF)
      INTEGER I,J,K,IC,L
      CHARACTER ORIGIN*16, LINE*256 
      INTEGER LENC
*
      ERROR = .FALSE.
      IF (NVR.LE.0) THEN
         WRITE (LUN,*) ' No radial velocities'
      ELSE
         DO I=1,MVREF
            DO J=1,NCOMPONENTS
               ND(J,I)    = 0
               SIGMA(J,I) = 0.
               CHI2(J,I)  = 0.
            ENDDO
         ENDDO
         DO I=FIRST_VR,LAST_VR
            IC = 0
            DO J=1,MORBIT
               DO K=1,2
                  IF (CVR(I).EQ.COMPNAME(K,J)) THEN
                     IC = K+2*(J-1)
                  ENDIF
               ENDDO
            ENDDO
            IF (IC.EQ.0) THEN
               WRITE(6,'(3(A))') 'E-LIST,  Unknown component ',CVR(I)
               GOTO 99
            ENDIF
            IF (STATUS(I).EQ.'OK') THEN
               ND(IC,CODE(I))    = ND(IC,CODE(I))+1
               SIGMA(IC,CODE(I)) = SIGMA(IC,CODE(I))+VR(I,I_OC_VR)**2
               CHI2(IC,CODE(I))  = CHI2(IC,CODE(I))+
     +              (VR(I,I_OC_VR)/VR(I,I_S_VR))**2
            ENDIF
         ENDDO
         DO I=1,MVREF
            DO J=1,NCOMPONENTS
               IF (ND(J,I).GT.0) THEN
                  SIGMA(J,I) = SIGMA(J,I)/ND(J,I)
                  SIGMA(J,I) = SQRT(SIGMA(J,I))
               ELSE
                  SIGMA(J,I) = 0.
               ENDIF
            ENDDO
         ENDDO
* Compute total number of measurements for each component
         DO J=1,NCOMPONENTS
            NDTOT(J) = 0
            DO I=1,NVREF
               NDTOT(J) = NDTOT(J)+ND(J,I)
            ENDDO
         ENDDO
* Print out results
         WRITE(LUN,*) 'Radial velocities'
         LINE = 'Origin: '
         L = 8
         DO J=1,NCOMPONENTS
            IF(NDTOT(J).GT.0) THEN
               WRITE(LINE(L+2:),101) 
     +              'V',COMPNAME(MOD(J-1,2)+1,(J+1)/2),
     +              ': ND','Sigma','Chi2'
 101           FORMAT (A5,A2,A4,1X,A5,3X,A6)
               L = LENC(LINE)
            ENDIF
         ENDDO
         WRITE(LUN,'(A)') LINE(1:L)
         DO I=1,NVREF
            CALL GIVE_VREF_NAME(I,ORIGIN,ERROR)
            IF (ERROR) GOTO 99
            LINE = ORIGIN
            L = 8
            DO J=1,NCOMPONENTS
               IF (NDTOT(J).GT.0) THEN
                  IF (ND(J,I).GT.0) THEN ! Some measurements for component here
                     WRITE (LINE(L+2:),102) 
     +                    ND(J,I),SIGMA(J,I),CHI2(J,I)
 102                 FORMAT (8X,I3,1X,F7.4,1X,F6.1)
                     L = LENC(LINE)
                  ELSE ! Fill in with blanks
                     L = L+25
                  ENDIF
               ENDIF
            ENDDO
            WRITE(LUN,'(A)') LINE(1:L)
         ENDDO
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
*
      END




*
      SUBROUTINE PRINT_DATA(LUN,ERROR)
      INTEGER LUN
      LOGICAL ERROR
* List the data values
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INTEGER I
* Radial velocities first
      CALL PRINT_VR_DATA(LUN,1,NVR,MVR,VR,CVR,STATVR,VREFCODE,ERROR)
      IF (ERROR) GOTO 99
* Interferometric visibilities
*
      CALL PRINT_VISI_DATA(LUN,1,NVISI,MVISI,VISI,CVISI,STATVISI,
     +     VISI_BAND,ERROR)
      IF (ERROR) GOTO 99
*
      CALL PRINT_VISUAL_DATA(LUN,1,NN,MOBS,OBS,COBS,STATOBS)
*
      IF (NPARA.GT.0) THEN
         WRITE(LUN,*)
         WRITE(LUN,*) 'Parallaxes'
         DO I=1,NPARA
            WRITE(LUN,200) PARALLAX(I),PARERR(I),PAR_OC(I),STATPARA(I)
         ENDDO
      ENDIF
 200  FORMAT(1X,F6.4,1X,F5.4,1X,F6.4,1X,A)
      IF (NPHOT.GT.0) THEN
         WRITE(LUN,*)
         WRITE(LUN,*) 'Photometry'
         WRITE(LUN,'(A,A)') 'Band       mag     sig  ',
     +        'd(mag)   sig   Status'
         DO I=1,NPHOT
            WRITE(LUN,300) PHOT_BAND(I), MAGNITUDE(I), SIG_MAG(I),
     +           DELTA_MAG(I), SIG_DMAG(I), PHOT_STAT(I)
         ENDDO
      ENDIF
 300  FORMAT(A,2(1X,F7.3,1X,F5.3),5X,A)
* Should add radial velocity profiles
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE PRINT_VISUAL_DATA(LUN,FIRST_VISUAL,LAST_VISUAL,
     +     MVISUAL,VISUAL,CVISUAL,STATUS)
C-----------------------------------------------------------------------------
C Orbit:
C   Print some angular separation data on requested logical unit, in a human
C   readable format.
C Arguments:
C    LUN           I      Logical unit for output
C    FIRST_VISUAL  I      First displayed visual measurement
C    LAST_VISUAL   I      Last displayed visual measurement
C    MVISUAL       I      Dimension of visual measurement array
C    VISUAL        R*8    Visual measurement array
C    CVISUAL       C*(*)  Visual measurement type (XY, rho-theta...)
C    STATUS        C*(*)  Measurement status (OK or Ignored)
C-----------------------------------------------------------------------------
      INTEGER LUN,FIRST_VISUAL,LAST_VISUAL,MVISUAL
      REAL*8 VISUAL(MVISUAL,9)
      CHARACTER*(*) CVISUAL(MVISUAL),STATUS(MVISUAL)
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
*
      INTEGER I
      REAL PHASE
      LOGICAL ERROR,HAS_SOME
      CHARACTER CHAIN*17
      INTEGER LENC
*
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'rho-theta'.OR.
     +        CVISUAL(I).EQ.'theta') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE (LUN,*) 'RHO THETA OBSERVATIONS'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '     Rho SigRho OCRho',
     +              ' Theta SigThe OCThe Phase Status'
               HAS_SOME = .TRUE.
            ENDIF
*
            ERROR = .FALSE.
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            IF (CVISUAL(I).EQ.'rho-theta') THEN
               WRITE (LUN,142) CHAIN,VISUAL(I,I_TIME),
     +              VISUAL(I,I_RHO),VISUAL(I,I_S_RHO),
     +              VISUAL(I,I_OC_RHO),
     +              VISUAL(I,I_THETA),VISUAL(I,I_S_THETA),
     +              VISUAL(I,I_OC_THETA),
     +              PHASE,STATUS(I)(1:LENC(STATUS(I)))
!* Residuals as (X,Y), to search for an inner astrometric orbit
!               write(99,143) 'PXY',VISUAL(I,I_TIME),
!     +              VISUAL(I,I_RHO)*COS(VISUAL(I,I_S_RHO)*PI/180.D0)-
!     +              (VISUAL(I,I_RHO)-VISUAL(I,I_OC_RHO))*
!     +              COS((VISUAL(I,I_S_RHO)-VISUAL(I,I_OC_THETA))
!     +              *PI/180.D0),
!     +              VISUAL(I,I_RHO)*SIN(VISUAL(I,I_S_RHO)*PI/180.D0)-
!     +              (VISUAL(I,I_RHO)-VISUAL(I,I_OC_RHO))*
!     +              SIN((VISUAL(I,I_S_RHO)-VISUAL(I,I_OC_THETA))
!     +              *PI/180.D0),
!     +              SQRT(VISUAL(I,I_S_RHO)*
!     +              VISUAL(I,I_RHO)*VISUAL(I,I_S_THETA)*PI/180.D0),
!     +              SQRT(VISUAL(I,I_S_RHO)*
!     +              VISUAL(I,I_RHO)*VISUAL(I,I_S_THETA)*PI/180.D0)
            ELSEIF (CVISUAL(I).EQ.'theta') THEN
               WRITE (LUN,42) CHAIN,VISUAL(I,I_TIME),
     +              VISUAL(I,I_THETA),VISUAL(I,I_S_THETA),
     +              VISUAL(I,I_OC_THETA),
     +              PHASE,STATUS(I)(1:LENC(STATUS(I)))
            ENDIF
         ENDIF
      ENDDO
 142  FORMAT (A,1X,F10.3,3(1X,F5.3),3(1X,F5.1),1X,F5.3,3X,A)
  42  FORMAT (A,1X,F10.3,3(1X,'-----'),3(1X,F5.1),1X,F5.3,3X,A)
*
* Measurements of type XY
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'XY') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE (LUN,*) 'XY observations'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '     X  SigX OC_X ',
     +              ' Y    SigY OC_Y Phase Status'
               HAS_SOME = .TRUE.
            ENDIF
*
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            WRITE (LUN,162) CHAIN,VISUAL(I,I_TIME),
     +           VISUAL(I,I_X),VISUAL(I,I_S_X),
     +           VISUAL(I,I_OC_X),
     +           VISUAL(I,I_Y),VISUAL(I,I_S_Y),
     +           VISUAL(I,I_OC_Y),
     +           PHASE,STATUS(I)(1:LENC(STATUS(I)))
         ENDIF
      ENDDO
 162  FORMAT(1X,A,1X,F10.3,2(1X,SP,F6.3,1X,SS,F5.3,1X,SP,F5.3),
     +     1X,SS,F5.3,1X,A6,2(1X,SP,F5.2))
* Measurements of type PXY
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'PXY') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE (LUN,*) 
     +              'Photocenter XY observations (pos+pi+mu removed)'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '        X    SigX  OC_X ',
     +                   '   Y    SigY  OC_Y Phase Status '
               HAS_SOME = .TRUE.
            ENDIF
*
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            WRITE (LUN,162) CHAIN,VISUAL(I,I_TIME),
     +           VISUAL(I,I_X),VISUAL(I,I_S_X),
     +           VISUAL(I,I_OC_X),
     +           VISUAL(I,I_Y),VISUAL(I,I_S_Y),
     +           VISUAL(I,I_OC_Y),
     +           PHASE,STATUS(I)(1:LENC(STATUS(I)))
         ENDIF
      ENDDO
* Measurements of type AXY
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'AXY') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE (LUN,*) 
     +              'Photocenter XY observations (with pos+pi+mu in...)'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '        X    SigX  OC_X ',
     +                   '   Y    SigY  OC_Y Phase Status PiFX  PiFY'
               HAS_SOME = .TRUE.
            ENDIF
*
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            WRITE (LUN,162) CHAIN,VISUAL(I,I_TIME),
     +           VISUAL(I,I_X),VISUAL(I,I_S_X),
     +           VISUAL(I,I_OC_X),
     +           VISUAL(I,I_Y),VISUAL(I,I_S_Y),
     +           VISUAL(I,I_OC_Y),
     +           PHASE,STATUS(I)(1:LENC(STATUS(I))),
     +           VISUAL(I,I_PIFX),VISUAL(I,I_PIFY)
         ENDIF
      ENDDO
*
* X or Y obsolete, now converted to PROJ type at read time.
* Z,T (PA=45deg,135deg) as well.
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'X'.OR.CVISUAL(I).EQ.'Y'
     +        .OR.CVISUAL(I).EQ.'ZT'
     +        .OR.CVISUAL(I).EQ.'Z'.OR.CVISUAL(I).EQ.'T') THEN
            write(6,*) 'Obsolete data type X or Y'
         ENDIF
      ENDDO
* Projected separations
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'PROJ') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE(*,*)    'Projected separations'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '     PAng   Sep    ESep   O-CSep  Phase Status'
               HAS_SOME = .TRUE.
            ENDIF
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            WRITE (LUN,163) CHAIN, VISUAL(I,I_TIME)
     +           ,VISUAL(I,I_POSANG)
     +           ,VISUAL(I,I_PROJ),VISUAL(I,I_S_PROJ)
     +           ,VISUAL(I,I_OC_PROJ)
     +           ,PHASE,STATUS(I)
         ENDIF
      ENDDO
 163  FORMAT(1X,A,1X,F10.3,F7.1,3(1X,F6.3),1X,F5.3,2X,A)
* Projected astrometric movement (Hipparcos in practice)
      HAS_SOME = .FALSE.
      DO I=FIRST_VISUAL,LAST_VISUAL
         IF (CVISUAL(I).EQ.'PPROJ') THEN
            IF (.NOT.HAS_SOME) THEN
               WRITE(*,*)    'Projected astrometric wobble'
               WRITE (LUN,'(A,A,A)') '  Date       Time        ',
     +              '     PAng   Sep    ESep   O-CSep  Phase Status'
               HAS_SOME = .TRUE.
            ENDIF
            CALL FORMAT_DATE(VISUAL(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VISUAL(I,I_TIME),PHASE)
            WRITE (LUN,163) CHAIN, VISUAL(I,I_TIME)
     +           ,VISUAL(I,I_POSANG)
     +           ,VISUAL(I,I_PROJ),VISUAL(I,I_S_PROJ)
     +           ,VISUAL(I,I_OC_PROJ)
     +           ,PHASE,STATUS(I)
         ENDIF
      ENDDO
*
      RETURN
      END
*
      SUBROUTINE PRINT_VISI_DATA(LUN,FIRST,LAST,NVISI,
     +     VISI,TYPE,STATUS,CODE,ERROR)
C-----------------------------------------------------------------------------
C Orbit:
C   Print some interferometric visibility data on requested logical unit, 
C   in a human readable format.
C Arguments:
C    LUN       I      Logical unit for output
C    FIRST     I      First displayed interferometric visibility
C    LAST      I      Last displayed interferometric visibility
C    NVISI     I      Dimension of interferometric visibility array
C    VISI      R*8    Interferometric visibility array
C    TYPE      C*(*)  Interferometric visibility type
C    CODE      I(*)   Code to the name of the photometric filter
C    STATUS    C*(*)  Measurement status (OK or Ignored)
C-----------------------------------------------------------------------------
      INTEGER LUN,FIRST,LAST,NVISI
      INTEGER CODE(NVISI)
      REAL*8 VISI(NVISI,10)
      CHARACTER*(*) TYPE(NVISI),STATUS(NVISI)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      REAL PHASE
      INTEGER I,L,LL
      CHARACTER CHAIN*17, BAND*16
      INTEGER LENC
      LOGICAL FIRST_ONE
*
      ERROR = .FALSE.
      IF (NVISI.LE.0) THEN
         CONTINUE ! WRITE (LUN,*) ' No interferometric visibilities'
      ELSE
         FIRST_ONE = .TRUE.
         DO I=FIRST,LAST
            LL = LENC(TYPE(I))
            IF (TYPE(I)(1:LL).EQ.'VISI2') THEN
               IF (FIRST_ONE) THEN
                  WRITE(LUN,*) 
     +                 '   Squared interferometric visibilities'
                  WRITE (LUN,'(A,A,A)') '     Date       Time  ',
     +                 '    Band    u       v       w ',
     +                 '  Vis^2 SigV^2 o-cV^2 Phase Stat'
                  FIRST_ONE = .FALSE.
               ENDIF
               CALL FORMAT_DATE(VISI(I,I_TIME),CHAIN,ERROR)
               CALL SET_TIME(1,IORB0,VISI(I,I_TIME),PHASE)
               CALL GIVE_BAND_NAME(CODE(I),BAND,ERROR)
               IF (ERROR) GOTO 99
               L = LENC(BAND)
               WRITE (LUN,101) 
     +              CHAIN,VISI(I,I_TIME),BAND(1:L)
     +              ,VISI(I,I_U),VISI(I,I_V),VISI(I,I_W)
     +              ,VISI(I,I_VISI2),VISI(I,I_SIG_VISI2)
     +              ,VISI(I,I_OC_VISI2)
     +              ,PHASE,STATUS(I)(1:LENC(STATUS(I)))
            ENDIF
         ENDDO
      ENDIF
      RETURN
 101  FORMAT (A,1X,F9.3,1X,A,3(F8.3),(1X,F5.3),2(1X,F5.3),
     +     1X,F5.3,2X,A)
*
 99   ERROR = .TRUE.
      RETURN
*
      END


      SUBROUTINE PRINT_VR_DATA(LUN,FIRST_VR,LAST_VR,NVR,VR,CVR,
     +     STATUS,CODE,ERROR)
C-----------------------------------------------------------------------------
C Orbit:
C   Print some radial velocity data on requested logical unit, in a human
C   readable format.
C Arguments:
C    LUN       I      Logical unit for output
C    FIRST_VR  I      First displayed radial velocity
C    LAST_VR   I      Last displayed radial velocity
C    NVR       I      Dimension of radial velocity array
C    VR        R*8    Radial velocity array
C    CVR       C*(*)  Radial velocity type
C    CODE      I(*)   Code for radial velocity referential
C    STATUS    C*(*)  Measurement status (OK or Ignored)
C-----------------------------------------------------------------------------
      INTEGER LUN,FIRST_VR,LAST_VR,NVR
      INTEGER CODE(NVR)
      REAL*8 VR(NVR,5)
      CHARACTER*(*) CVR(NVR),STATUS(NVR)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      REAL PHASE
      INTEGER I,L,LL
      CHARACTER CHAIN*17,ORIGIN*16
      INTEGER LENC
*
      ERROR = .FALSE.
      IF (NVR.LE.0) THEN
         WRITE (LUN,*) ' NO RADIAL VELOCITIES'
      ELSE
         WRITE(LUN,*) '   RADIAL VELOCITIES'
         WRITE (LUN,'(A,A,A)') '     Date       Time            ',
     +        '    Vr     Sig(Vr)  O-C(Vr) Phase Status'
         DO I=FIRST_VR,LAST_VR
            CALL FORMAT_DATE(VR(I,I_TIME),CHAIN,ERROR)
            CALL SET_TIME(1,IORB0,VR(I,I_TIME),PHASE)
            CALL GIVE_VREF_NAME(CODE(I),ORIGIN,ERROR)
            IF (ERROR) GOTO 99
            L = LENC(ORIGIN)
            LL = LENC(CVR(I))
            IF (L.EQ.0) THEN
               WRITE (LUN,101) CVR(I)(1:LL),CHAIN,VR(I,I_TIME)
     +              ,VR(I,I_VR),VR(I,I_S_VR),VR(I,I_OC_VR)
     +              ,PHASE,STATUS(I)(1:LENC(STATUS(I)))
            ELSE
               WRITE (LUN,102) CVR(I)(1:LL),ORIGIN(1:L),
     +              CHAIN,VR(I,I_TIME)
     +              ,VR(I,I_VR),VR(I,I_S_VR),VR(I,I_OC_VR)
     +              ,PHASE,STATUS(I)(1:LENC(STATUS(I)))
            ENDIF
         ENDDO
      ENDIF
      RETURN
 101  FORMAT ('V',A,T18,A,1X,F10.3,1X,
     +     F8.3,2(1X,F7.3),1X,F5.3,2X,A)
 102  FORMAT ('V',A,'(',A,')',T18,A,1X,F10.3,1X,
     +     F8.3,2(1X,F7.3),1X,F5.3,2X,A)
*
 99   ERROR = .TRUE.
      RETURN
*
      END


*
      SUBROUTINE PRINT_EPHEMERID(LUN,N,T0,DT,MONTECARLO,NSAMPLE,
     +           CONF_LEVEL,MC_EL,NINFO,INFO_LIST,ERROR)
      USE GKERNEL_INTERFACES
      INCLUDE 'constant.inc'
      INTEGER LUN,N,NSAMPLE,NINFO
      REAL*8 T0
      REAL DT, CONF_LEVEL
      REAL*8 MC_EL(MA,NSAMPLE)
      CHARACTER*(*) INFO_LIST(NINFO)
      LOGICAL MONTECARLO,ERROR
*
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
*
      REAL*8 T,TMP
      REAL PHASE(MORBIT)
      REAL B(MA)
      REAL VAL,StdErr
      INTEGER I,J,K,HIGH,LOW
      CHARACTER HEADER*240,LINE*240,FORMAT*48
      INTEGER LH,LL,LF
      LOGICAL FIRST_LINE
* Work space for montecarlo. Should be dynamically allocated... 
      REAL MC_VAL(M_MC)! MC_V1(M_MC),MC_V2(M_MC),MC_RHO(M_MC),MC_THETA(M_MC)
      INTEGER IT(M_MC)
*
*
* Build up header line for requested predictions
      IF (D_UT.EQ.0) THEN
         HEADER = '  Date       UT             '
      ELSE
         WRITE(HEADER,'(2X,A4,6X,A2,SP,I3.2)') 'Date','UT',D_UT
      ENDIF
      LH = 27
*
*
      IF (MONTECARLO) THEN
         IF (NSAMPLE.LE.0) THEN
            WRITE(6,*) 'E-LIST, Number of montecarlo samples is ',
     +           'negative'
            GOTO 99
         ENDIF
         LOW  = NINT(NSAMPLE*(1.-CONF_LEVEL)/2.)
         HIGH = NSAMPLE-LOW+1
         IF (LOW.EQ.1) THEN 
            WRITE(6,*) 'E-LIST, Number of montecarlo samples ',
     +           NSAMPLE,' is insufficient for requested confidence',
     +           ' level'
            GOTO 99
         ENDIF
      ENDIF
* Initialize Kepler computations for current orbital elements
      CALL INIT_KEPLER(EL,.FALSE.)
*      
      DO I=1,N
         FIRST_LINE = I.EQ.1
* Generate a time grid, and compute phases and anomalies for all orbits at time T
         T = T0+DT*(I-1)
         CALL SET_TIME(MORBIT,LISTORBALL,T,PHASE)
* Format date to human readable conventions for screen output, and correct
* to local time in that process
         LL = 17
         TMP = T+D_UT/24.0
         CALL FORMAT_DATE(TMP,LINE(1:LL),ERROR)
         WRITE(LINE(LL+1:LL+10),'(1X,F9.3)') T
         LL = LL+10
         DO J=1,NINFO
* Phase
            IF (INFO_LIST(J).EQ.'PHASE') THEN
               WRITE(LINE(LL+1:LL+6),'(F6.3)') PHASE(IORB0)
               LL = LL+6
               IF (FIRST_LINE) THEN
                  HEADER(LH+1:) = ' Phase'
                  LH = LH+6
               ENDIF
            ELSEIF (INFO_LIST(J)(1:6).EQ.'PHASE_') THEN
               IF (INFO_LIST(J).EQ.'PHASE_1') THEN
                  WRITE(LINE(LL+1:LL+7),'(F6.3,1X)') PHASE(IORB1)
               ELSEIF (INFO_LIST(J).EQ.'PHASE_2') THEN
                  WRITE(LINE(LL+1:LL+7),'(F6.3,1X)') PHASE(IORB2)
               ELSE
                  WRITE(6,*) 'E-LIST,  Unknown or unsupported ',
     +                 'ephemerid item ',INFO_LIST(J)
                  GOTO 99
               ENDIF
               LL = LL+7
               IF (FIRST_LINE) THEN
                  HEADER(LH+1:) = 'Phase'//INFO_LIST(J)(6:7)
                  LH = LH+7
               ENDIF
            ELSEIF (MONTECARLO) THEN
               CALL INIT_KEPLER(EL,.FALSE.)
               CALL SET_TIME(MORBIT,LISTORBALL,T,PHASE)
               CALL COMPUTE_VALUE(INFO_LIST(J),VAL,ERROR)
               IF (ERROR) GOTO 99
               DO K=1,NSAMPLE
                  CALL INIT_KEPLER(MC_EL(1,K),.TRUE.)
                  CALL SET_TIME(MORBIT,LISTORBALL,T,PHASE)
                  CALL COMPUTE_VALUE(INFO_LIST(J),MC_VAL(K),ERROR)
               ENDDO
* Sort the ephemerid parameter to determine confidence intervals
               CALL GR4_TRIE(MC_VAL,IT,NSAMPLE,ERROR)
               IF (ERROR) GOTO 99
! Format should ideally match the accuracy and amplitude of the ephemerid.
               IF (INFO_LIST(J)(1:1).EQ.'V') THEN
                  FORMAT = 
     +                 '(F7.1,1X,''['',F7.1,'','',F7.1,'']'')'
                  LF = 25
               ELSEIF (INFO_LIST(J).EQ.'RHO') THEN
!                  FORMAT = 
!     +                 '(F5.3,1X,''['',F5.3,'','',F5.3,'']'')'
!                  LF = 19
                  FORMAT = 
     +                 '(F7.5,1X,''['',F7.5,'','',F7.5,'']'')'
                  LF = 25
               ELSEIF (INFO_LIST(J).EQ.'THETA') THEN
                  FORMAT = 
     +                 '(F6.1,1X,''['',F6.1,'','',F6.1,'']'')'
                  LF = 22
               ENDIF
               WRITE(LINE(LL+1:LL+LF),FORMAT) 
     +              VAL,MC_VAL(LOW),MC_VAL(HIGH)
               LL = LL+LF
*     
* Linearised standard errors
            ELSE
* Radial velocities 
               IF(INFO_LIST(J)(1:1).EQ.'V') THEN
                  IF (INFO_LIST(J).EQ.'V1') THEN
                     CALL GRAD_V1(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSEIF (INFO_LIST(J).EQ.'V2') THEN
                     CALL GRAD_V2(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSEIF (INFO_LIST(J).EQ.'V11') THEN
                     CALL GRAD_V11(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSEIF (INFO_LIST(J).EQ.'V12') THEN
                     CALL GRAD_V12(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSEIF (INFO_LIST(J).EQ.'V21') THEN
                     CALL GRAD_V21(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSEIF (INFO_LIST(J).EQ.'V22') THEN
                     CALL GRAD_V22(1,VAL,B)
                     CALL EVAL_ERROR(StdErr,B)
                  ELSE
                     WRITE(6,*) 'E-LIST,  Unknown or unsupported ',
     +                    'velocity type ',INFO_LIST(J)
                     GOTO 99
                  ENDIF
                  FORMAT = '(F8.2,''('',F4.2,'')'')'
                  LF = 14
                  IF (FIRST_LINE) HEADER(LH+1:) = '    '//INFO_LIST(J)
* Compute rho,theta 
               ELSEIF (INFO_LIST(J).EQ.'RHO') THEN
                  CALL GRAD_RHO(VAL,B)
                  CALL EVAL_ERROR(StdErr,B)
!                  FORMAT = '(F5.3,''('',F4.3,'')'')'
!                  LF = 11
                  FORMAT = '(F9.6,''('',F7.6,'')'')'
                  LF = 18
!                  write(6,*) (b(k),k=1,10)
!                  write(6,*) stderr
                  IF (FIRST_LINE) HEADER(LH+1:) = '    Rho  '
               ELSEIF (INFO_LIST(J).EQ.'THETA') THEN
                  CALL GRAD_THETA(VAL,B)
                  CALL EVAL_ERROR(StdErr,B)
                  StdErr = StdErr*SNGL(GR) ! Convert standard error to degrees
                  FORMAT = '(F6.1,''('',F3.1,'')'')'
                  LF = 11
                  IF (FIRST_LINE) HEADER(LH+1:) = '   Theta '
               ELSE
                  WRITE(6,*) 'E-LIST,  Unknown or unsupported ',
     +                 'ephemerid item ',INFO_LIST(J)
                  GOTO 99
               ENDIF
               WRITE(LINE(LL+1:LL+LF),FORMAT) VAL,StdErr
               LL = LL+LF
               LH = LH+LF
            ENDIF
         ENDDO
         IF (FIRST_LINE) WRITE (LUN,'(A)') HEADER(1:LH)
         WRITE (LUN,'(A)') LINE(1:LL)
*     
      ENDDO
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE COMPUTE_VALUE(CODE,VAL,ERROR)
* Return the value at current time of the item specified by code
      CHARACTER*(*) CODE
      REAL VAL
      LOGICAL ERROR
*
      REAL Dummy
*
      IF (CODE(1:1).EQ.'V') THEN
         IF (CODE.EQ.'V1') THEN
            CALL VALUE_V1_V2(1,VAL,Dummy)
         ELSEIF (CODE.EQ.'V2') THEN
            CALL VALUE_V1_V2(1,Dummy,VAL)
         ELSE
            WRITE(6,*) 'E-LIST,  Unknown or unsupported ',
     +           'velocity type ',CODE
            GOTO 99
         ENDIF
      ELSEIF(CODE.EQ.'RHO') THEN
         CALL KEPLER_RHOTHETA(VAL,Dummy)
      ELSEIF(CODE.EQ.'THETA') THEN
         CALL KEPLER_RHOTHETA(Dummy,VAL)
      ENDIF
      RETURN
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE GIVE_VREF_NAME(CODE,NAME,ERROR)
      INTEGER CODE
      CHARACTER*(*) NAME
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      IF (CODE.GT.NVREF)  THEN
         WRITE(6,*) 'F-LIST, Internal logic error: code for velocity ',
     +        'referential outside range',CODE,NVREF
         GOTO 99
      ENDIF
      NAME = VR_REF(CODE)
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END


      SUBROUTINE GIVE_BAND_NAME(CODE,NAME,ERROR)
      INTEGER CODE
      CHARACTER*(*) NAME
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      IF (CODE.GT.NPHOT)  THEN
         WRITE(6,*) 'F-LIST, Internal logic error: ',
     +        'code for photometric ',
     +        'band outside range',CODE,NPHOT
         GOTO 99
      ENDIF
      NAME = PHOT_BAND(CODE)
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END






