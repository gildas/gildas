      SUBROUTINE ADJUST(LINE,ERROR)
      USE GKERNEL_INTERFACES
      CHARACTER LINE*(*)
      logical error
      
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
      
      INTEGER I
      CHARACTER*16 ARGUM,KEYWOR
      CHARACTER*32 DATE
      INTEGER NKEY,NC,ND,IDUM
      LOGICAL IS_DATE

*
      IF (SIC_PRESENT(1,0)) THEN !  ADJUST/ALL
         DO I=1,MA
            EL_STATUS(I) = 'Adjustable'
         ENDDO
      ELSE
         IF (SIC_PRESENT(2,0)) THEN !  ADJUST/VISUAL
            EL_STATUS(I_PERIOD+ELINDX(IORB0)) = 'Adjustable' ! P
            IF (ELERR(I_PERIOD+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_PERIOD+ELINDX(IORB0)) = 0.
            EL_STATUS(I_T0+ELINDX(IORB0)) = 'Adjustable' ! T0
            IF (ELERR(I_T0+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_T0+ELINDX(IORB0)) = 0.
            EL_STATUS(I_ecc+Elindx(Iorb0)) = 'Adjustable' ! e
            IF (ELERR(I_ecc+Elindx(Iorb0)).LT.0) 
     +           ELERR(I_ecc+Elindx(Iorb0)) = 0.
            EL_STATUS(I_as+ELINDX(IORB0)) = 'Adjustable' ! as
            IF (ELERR(I_as+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_as+ELINDX(IORB0)) = 0.
            EL_STATUS(I_OMEGA_U+ELINDX(IORB0)) = 'Adjustable' ! OM
            IF (ELERR(I_OMEGA_U+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_OMEGA_U+ELINDX(IORB0)) = 0.
            EL_STATUS(I_OMEGA_L+ELINDX(IORB0)) = 'Adjustable' ! om
            IF (ELERR(I_OMEGA_L+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_OMEGA_L+ELINDX(IORB0)) = 0.
            EL_STATUS(I_i+ELINDX(IORB0)) = 'Adjustable' ! i
            IF (ELERR(I_i+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_i+ELINDX(IORB0)) = 0.
         ENDIF
         IF (SIC_PRESENT(4,0).OR.SIC_PRESENT(3,0)) THEN !  ADJUST/SB1/SB2
            EL_STATUS(I_PERIOD+ELINDX(IORB0))  = 'Adjustable' ! P
            IF (ELERR(I_PERIOD+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_PERIOD+ELINDX(IORB0)) = 0.
            EL_STATUS(I_T0+ELINDX(IORB0))  = 'Adjustable' ! T0
            IF (ELERR(I_T0+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_T0+ELINDX(IORB0)) = 0.
            EL_STATUS(I_ecc+Elindx(Iorb0))  = 'Adjustable' ! e
            IF (ELERR(I_ecc+Elindx(Iorb0)).LT.0) 
     +           ELERR(I_ecc+Elindx(Iorb0)) = 0.
            EL_STATUS(I_OMEGA_L+ELINDX(IORB0))  = 'Adjustable' ! om
            IF (ELERR(I_OMEGA_L+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_OMEGA_L+ELINDX(IORB0)) = 0.
            EL_STATUS(I_K1+ELINDX(IORB0))  = 'Adjustable' ! K1
            IF (ELERR(I_K1+ELINDX(IORB0)).LT.0) 
     +           ELERR(I_K1+ELINDX(IORB0)) = 0.
            IF (SIC_PRESENT(4,0)) THEN !  ADJUST/SB2
               EL_STATUS(I_K2+ELINDX(IORB0))  = 'Adjustable' ! K2
               IF (ELERR(I_K2+ELINDX(IORB0)).LT.0) 
     +              ELERR(I_K2+ELINDX(IORB0)) = 0.
            ENDIF
            EL_STATUS(I_V0) = 'Adjustable' ! V0
            IF (ELERR(I_V0).LT.0) ELERR(I_V0) = 0.
         ENDIF
* Adjust specific parameter(s)
*     
         IF (SIC_PRESENT(0,1)) THEN
            CALL SIC_CH (LINE,0,1,ARGUM,NC,.TRUE.,ERROR)
            IF (ERROR) RETURN
            IF (NC.LT.LEN(ARGUM)) THEN
               NC = NC+1
               ARGUM(NC:NC) = ' '
            ENDIF
            NKEY = 0
            DO I=1,MA
               IF (ARGUM(1:NC).EQ.NAMEL(I)) NKEY = I
            ENDDO
            IF (NKEY.EQ.0) THEN
               CALL SIC_AMBIGS('ADJUST',ARGUM,KEYWOR,NKEY,NAMEL,
     +              MA,ERROR)
               IF (ERROR) RETURN
            ENDIF
            EL_STATUS(NKEY) = 'Adjustable'
*
            IS_DATE = .FALSE.
            DO I=1,MORBIT
               IF (NKEY.EQ.(I_T0+ELINDX(I))) IS_DATE = .TRUE.
            ENDDO
            IF (IS_DATE) THEN
               CALL SIC_CH (LINE,0,2,DATE,ND,.FALSE.,ERROR)
               IF (ND.GT.0) THEN
                  CALL DECODE_DATE(DATE(1:ND),EL(NKEY),IDUM,ERROR)
               ENDIF
            ELSE
               CALL SIC_R8(LINE,0,2,EL(NKEY),.FALSE.,ERROR)
            ENDIF
            IF (ELERR(NKEY).LT.0) ELERR(NKEY) = 0.D0
            CALL SIC_R8(LINE,0,3,ELERR(NKEY),.FALSE.,ERROR)
         ENDIF
      ENDIF
*      
      RETURN
*
      END
