module orbit_interfaces_none
  interface
    subroutine orbit_pack_set(pack)
      use gpack_def
      !
      type(gpack_info_t), intent(out) :: pack
    end subroutine orbit_pack_set
  end interface
  !
  interface
    subroutine orbit_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! 
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine orbit_pack_init
  end interface
  !
  interface
    subroutine orbit_pack_clean(error)
      !----------------------------------------------------------------------
      ! Called at end of session. Might clean here for example global buffers
      ! allocated during the session
      !----------------------------------------------------------------------
      logical :: error
    end subroutine orbit_pack_clean
  end interface
  !
end module orbit_interfaces_none
