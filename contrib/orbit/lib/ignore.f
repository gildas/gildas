      SUBROUTINE IGNORE(LINE,ERROR)
      USE GKERNEL_INTERFACES
C-----------------------------------------------------------------------------
C ORBIT:
C  Temporarily ignore some data from the fit
C-----------------------------------------------------------------------------
      CHARACTER LINE*(*)
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER MVOCAB
      PARAMETER (MVOCAB=5)
      CHARACTER*12 VOCAB(MVOCAB),ARGUM1,ARGUM2,KEYWORD
      CHARACTER*10 STATUS
      INTEGER I,NCH,NKEY,CODE
*
      DATA VOCAB /'PARALLAX','VELOCITY','CORRELATION',
     +     'VISUAL', 'VISIBILITY'/
*
      STATUS= 'Ignored'
      GOTO 100
*
      ENTRY USE(LINE,ERROR)
      STATUS= 'OK'
      GOTO 100
*
* Determine requested action type from command line
 100  CALL SIC_CH (LINE,0,1,ARGUM1,NCH,.TRUE.,ERROR)
      NCH=MAX(NCH,LEN(ARGUM1))
      CALL SIC_UPPER(ARGUM1(1:NCH))
      CALL SIC_AMBIGS('LIST',ARGUM1(1:NCH),KEYWORD,NKEY,
     +     VOCAB,MVOCAB,ERROR)
*
      IF (KEYWORD.EQ.'PARALLAX') THEN
         DO I=1,NPARA
            STATPARA(I) = STATUS
         ENDDO
      ELSEIF (KEYWORD.EQ.'VELOCITY') THEN
         IF (.NOT.SIC_PRESENT(0,2)) THEN
            DO I=1,NVR
               STATVR(I) = STATUS
            ENDDO
         ELSE
            CALL SIC_CH (LINE,0,2,ARGUM2,NCH,.TRUE.,ERROR)         
            DO I=1,NVREF
               IF(ARGUM2.EQ.VR_REF(I)) THEN
                  CODE = I
                  GOTO 110
               ENDIF
            ENDDO
            WRITE(6,*) 'E-IGNORE,  Unknown velocity origin ',ARGUM2
            GOTO 99  ! ERROR
 110        CONTINUE
            DO I=1,NVR
               IF (VREFCODE(I).EQ.CODE) STATVR(I) = STATUS
            ENDDO
         ENDIF
      ELSEIF (KEYWORD.EQ.'CORRELATION') THEN
         DO I=1,NVCORR
            STATVCORR(I) = STATUS
         ENDDO
      ELSEIF (KEYWORD.EQ.'VISUAL') THEN
         DO I=1,NN
            STATOBS(I) = STATUS
         ENDDO
      ELSEIF (KEYWORD.EQ.'VISIBILITY') THEN
         DO I=1,NVISI
            STATVISI(I) = STATUS
         ENDDO
      ENDIF
*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END


