      SUBROUTINE EVAL_ERROR(SIGMA,B)
* Compute (linearised) error bars on any function of the elements, given
* its gradient relative to the orbital element, and the covariance matrix
      REAL SIGMA  ! Probable error on function
      REAL B(*)   ! Gradient of function relative to orbital elements
*
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I,K
*
      SIGMA=0.
      DO I=1,MA
         DO K=1,MA
            SIGMA=SIGMA+ALPHA(I,K)*B(I)*B(K)
         ENDDO
      ENDDO
* Detailed contributions of individual covariances. Make available through
* a verbose switch??
!      IF (SIGMA.GT.0) THEN
!         DO I=1,MA
!            if (b(i).ne.0) then
!               write(6,'(200(1X,F4.2))') 
!     +              (ALPHA(I,K)*B(I)*B(K)/SIGMA,K=1,MA)
!            endif
!         ENDDO
!      ENDIF
*
      SIGMA=SQRT(SIGMA)
      SIGMA=SIGMA*SIG   ! Rescale by reduced Chi to avoid overoptimistic
                        ! values when measurement erros are underestimated.
      RETURN
      END

