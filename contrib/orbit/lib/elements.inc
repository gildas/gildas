      REAL*8  EL(MA)      ! Values of elements
      REAL*8  ELERR(MA)   ! Probable error on element
      REAL*8  TMPZEROVAL(MVREF) ! Values of temporarily stored ZP adjustments
      REAL*8  TMPZEROERR(MVREF) ! Std errs of temporarily stored ZP adjustments
      INTEGER NVZPDIFF    ! Number of zero point differences in input file
      INTEGER D_UT        ! Difference between local time and UT, for ephemeris display
      INTEGER NMEAS(NDTYPE)
      REAL    SCLEL(MA)   ! Scale factor for element
      REAL    ALPHA(MA,MA)
      REAL CHISQ          ! Chi^2 of the least square adjustment
      REAL SIG            ! Reduced chi
      REAL SD(NDTYPE)
      REAL CHI2(NDTYPE)
      LOGICAL HAS_DATA(MVREF)
*
      COMMON/MINSQUARE/
* Real*8
     +     EL,ELERR
     +	,TMPZEROVAL,TMPZEROERR
* Integer
     +     ,NVZPDIFF, D_UT, NMEAS
* Real
     +     ,SCLEL,ALPHA,CHISQ,SIG,SD,CHI2
* Booleans
     +     ,HAS_DATA
 
      
      CHARACTER*16 EL_STATUS(MA) ! 'Fixed' or 'Adjustable'
      CHARACTER*8     ELFMT(MA) ! Formats used to print the orbital elements
      CHARACTER*16    NAMEL(MA) ! Human readable names of the orbital elements
      CHARACTER*16    TMPZEROSTAT(MVREF) ! Status of temporarily stored ZP 
                                         ! adjustments
      CHARACTER*16    TMPZERONAME(MVREF) ! Names of temporarily stored ZP 
                                         ! adjustments
      COMMON /C_ELEMENTS/ EL_STATUS, 
     +	ELFMT, NAMEL 
     +	,TMPZERONAME, TMPZEROSTAT

