      SUBROUTINE PERIODOGRAM(LINE,ERROR)
C----------------------------------------------------------------------
C ORBIT	Support routine for command
C	PERIODOGRAM
C	Compute a Scargle periodogram (as described in ApJ 263, 835)
C Arguments :
C	LINE	C*(*)	Command line		Input
C	ERROR	L	Logical error flag	Output
C----------------------------------------------------------------------
      CHARACTER*(*) LINE
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*
      INTEGER NX
      REAL XREF,XVAL,XINC,XMIN,XMAX
      INTEGER I,J
      REAL CI,SI
      REAL CIA,SIA,CI2A,SI2A,CIB,SIB,CI2B,SI2B
      REAL PERIOD(MFREQ),POWER(MFREQ)
      REAL OM,TAU
*
      CALL SIC_R4(LINE,0,1,XMIN,.TRUE.,ERROR)
      IF (ERROR) RETURN
      CALL SIC_R4(LINE,0,2,XMAX,.TRUE.,ERROR)
      IF (ERROR) RETURN
      CALL SIC_I4(LINE,0,3,NX,.TRUE.,ERROR)
      IF (ERROR) RETURN
      IF (NX.GT.MFREQ) THEN
         WRITE(6,*) 'E-PERIODOGRAM,  Too many frequency points.',
     +        'Maximum is ',MFREQ
         ERROR=.TRUE.
         RETURN
      ENDIF
      NX = MAX(NX,2)
*
      XREF = 1
      XVAL = XMIN
      XINC = (XMAX-XMIN)/(NX-1)
*
      CI = 0
      SI = 0
      DO I=1,NX
         PERIOD(I) = XVAL+(I-XREF)*XINC
         IF (PERIOD(I).LE.0) THEN
            POWER(I) = 0
         ELSE
            OM = 2*PI/PERIOD(I)
            DO J=1,NVR
               CI = CI+COS(2*OM*VR(J,I_TIME))
               SI = SI+SIN(2*OM*VR(J,I_TIME))
            ENDDO
            TAU=ATAN2(SI,CI)/(2*OM)
*     
            CIA = 0
            SIA = 0
            CI2A = 0
            SI2A = 0
            CIB = 0
            SIB = 0
            CI2B = 0
            SI2B = 0
            DO J=1,NVR
               IF (STATVR(J).EQ.'OK') THEN
                  IF (CVR(J).EQ.'1') THEN
                     CIA = CIA+VR(J,I_VR)*COS(OM*(VR(J,I_TIME)-TAU))
                     SIA = SIA+VR(J,I_VR)*SIN(OM*(VR(J,I_TIME)-TAU))
                     CI2A = CI2A+COS(OM*(VR(J,I_TIME)-TAU))**2
                     SI2A = SI2A+SIN(OM*(VR(J,I_TIME)-TAU))**2
                  ELSEIF (CVR(J).EQ.'2') THEN
                     CIB = CIB+VR(J,I_VR)*COS(OM*(VR(J,I_TIME)-TAU))
                     SIB = SIB+VR(J,I_VR)*SIN(OM*(VR(J,I_TIME)-TAU))
                     CI2B = CI2B+COS(OM*(VR(J,I_TIME)-TAU))**2
                     SI2B = SI2B+SIN(OM*(VR(J,I_TIME)-TAU))**2
                  ELSE
                     WRITE(6,*) 'E-PERIODOGRAM,  Unknown velocity',
     +                    ' type',CVR(J)
                  ENDIF
               ENDIF
            ENDDO
            POWER(I) = 0
            IF (CI2A.GT.0) POWER(I) = POWER(I)+(CIA**2/CI2A)/2
            IF (SI2A.GT.0) POWER(I) = POWER(I)+(SIA**2/SI2A)/2
            IF (CI2B.GT.0) POWER(I) = POWER(I)+(CIB**2/CI2B)/2
            IF (SI2B.GT.0) POWER(I) = POWER(I)+(SIB**2/SI2B)/2
         ENDIF
      ENDDO
* Display computed periodogram
      CALL GR_EXEC('CLEAR PLOT')
      CALL GR4_GIVE('X',NX,PERIOD)
      CALL GR4_GIVE('Y',NX,POWER)
      CALL GR_EXEC('LIMITS  ')
      CALL GR_EXEC('SET BOX LANDSCAPE')
      CALL GR_EXEC('BOX')
      CALL GR_EXEC('HISTO')
*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
