* Data information, ordered as used by the least square fit routine
* Radial velocities
      INTEGER NVR		! Number of radial velocity measurements
      INTEGER NVREF		! Number of radial velocity referentials
      REAL*8  VR(MVR,5),        ! VR(I,1) = time of measurement
     +	      VR_SAVE(MVR,5)	! VR(I,2) = radial velocity
				! VR(I,3) = uncertainty on radial velocity
				! VR(I,4) = O-C
				! VR(I,5) = Phase			   
      CHARACTER*4 CVR(MVR)	! CVR(I)='a' or 'b'
      CHARACTER*12 STATVR(MVR)   ! STATVR(I)='OK' or 'Ignored' or 'Killed'
      INTEGER VREFCODE(MVR)     ! Pointer to radial velocity ref name
      CHARACTER*16 VR_REF(MVREF)! 'CORAVEL', or 'ELODIE', or 'Marcy' or...
      CHARACTER*16 FORCED_VREF  ! Explicit choice of the VR system
*
*       Radial velocity correlation profiles
      INTEGER NVCORR		! Number of CORAVEL-like velocity profiles
      INTEGER NVPNTS(MVCORR)	! Number of points in correlation profile
      INTEGER CORREFCOD(MVCORR)! Pointer to radial velocity ref name
      REAL*8 COR_TIME(MVCORR)   ! Date of correlation profile
      REAL COR_SIGM(MVCORR)	! Noise level in correlation profile
      REAL*8 COR_REFPIX(MVCORR) ! Reference pixel in correlation profile
      REAL*8 COR_REFVAL(MVCORR) ! Velocity at reference pixel
      REAL*8 COR_VSTEP(MVCORR)  ! Velocity increment
      REAL VCORR(MVPNTS,MVCORR) ! Velocity profiles
      REAL VCORR_SAVE(MVPNTS,MVCORR) ! Temporary copies, used in Montecarlo
      CHARACTER*12 STATVCORR(MVCORR)  ! STATVCORRR(I)='OK' or 'Ignored' 
                                      ! or 'Killed'
* Relative positions
      INTEGER NN                ! Number of relative position measurements
      CHARACTER*10 COBS(MOBS)   ! 'XY', or 'rho-theta', or 'ZT', or 'X', or...
      CHARACTER*12 STATOBS(MOBS) ! Same as STATVR
      REAL*8  OBS(MOBS,9),      ! OBS(I,1) = Time
     +	      OBS_SAVE(MOBS,9)  ! OBS(I,2) = Theta/X/Z
                                ! OBS(I,3) = Rho/Y/T
                                ! OBS(I,4) = Sigma_rho/Sigma_X/Sigma_Z
                                ! OBS(I,5) = O-C theta/X/Z
                                ! OBS(I,6) = O-C rho/Y/T
                                ! OBS(I,7) = Sigma_theta/Sigma_Y/Sigma_T
                                ! OBS(I,8) = X parallax factor
                                ! OBS(I,9) = Y parallax factor
* Visibilities/squared visibilities
      INTEGER NVISI             ! Number of visibilities/squared visibilities
      CHARACTER*16 CVISI(MVISI)  ! 'V^2', or 'Visi', or 'ZT', or...
      CHARACTER*12 STATVISI(MVISI) ! Same as STATVR
      INTEGER VISI_BAND(MVISI)     ! Code of the photometric band
      REAL*8  VISI(MVISI,10),      ! VISI(I,1) = Time
     +	      VISI_SAVE(MVISI,10)  ! VISI(I,2) = u
                                   ! VISI(I,3) = v
                                   ! VISI(I,4) = w
                                   ! VISI(I,5) = V^2/Real part
                                   ! VISI(I,6) = Imaginary part
                                   ! VISI(I,7) = Sigma_V^2/Sigma_Real
                                   ! VISI(I,8) = O-C_V^2/O-C_Real
                                   ! VISI(I,9) = Sigma_Imag
                                   ! VISI(I,10) = O-C_Imag
* Date interval of measurement set
      REAL*8 FIRST_JDB,LAST_JDB
* Parallaxes
      INTEGER NPARA
      REAL PARALLAX(MPARA),PARERR(MPARA),PAR_OC(MPARA),PAR_SAVE(MPARA)
      CHARACTER*12 STATPARA(MPARA)  ! 
* Various ancillary header stuff
      CHARACTER*20    OBJECT(5)
      CHARACTER*20 GLIESE       ! Gliese name, if any
      CHARACTER*20 HD           ! HD number
      CHARACTER*20 OTHER_NAME   !
      EQUIVALENCE (OBJECT(1),GLIESE)
      EQUIVALENCE (OBJECT(2),HD)
      EQUIVALENCE (OBJECT(3),OTHER_NAME)
      REAL*8 RA, DEC
      REAL EQUINOX
* Photometry
      INTEGER NPHOT
      REAL LAMBDA_PHOT(MPHOT)   ! Central wavelength of the filter, in meters
! Will need to move to the data point category, with a separate dimensioning
! to accomodate multiple measurements. 
      REAL MAGNITUDE(MPHOT),SIG_MAG(MPHOT),DELTA_MAG(MPHOT), 
     +	SIG_DMAG(MPHOT)
      CHARACTER*8 PHOT_BAND(MPHOT),PHOT_STAT(MPHOT)
*
      CHARACTER*20 SPECTRAL_TYPE(2)
*
      INTEGER MCOMMENT,NCOMMENT
      PARAMETER (MCOMMENT=16)
      CHARACTER*72 COMMENT(MCOMMENT)
*
*
      COMMON /DATAN/ RA,DEC, VR, VR_SAVE, OBS, OBS_SAVE
     +     ,COR_TIME, COR_REFPIX, COR_REFVAL
     +     ,COR_VSTEP
     +     ,FIRST_JDB,LAST_JDB
     +     ,COR_SIGM
     +     ,VCORR, VCORR_SAVE, NVR, NN, NVCORR, NVPNTS 
     +     ,NPARA, PARALLAX, PAR_SAVE 
     +     ,PARERR,PAR_OC, VREFCODE, CORREFCOD, NVREF
     +	   ,NVISI, VISI, VISI_SAVE, VISI_BAND, LAMBDA_PHOT
*
      COMMON  /DATAC/CVR,COBS,STATVR,STATOBS,
     +	STATVCORR,STATPARA,VR_REF,FORCED_VREF,
     +  CVISI, STATVISI
*
      COMMON /HEADERN/ EQUINOX,NPHOT,MAGNITUDE,SIG_MAG,
     +	DELTA_MAG,SIG_DMAG,NCOMMENT
      COMMON /HEADERC/ OBJECT,PHOT_BAND,PHOT_STAT,
     +	SPECTRAL_TYPE,COMMENT




