* Image of the input file structure. Must be reordered and copied to
* other commons before the data can be used for the least square fit
        CHARACTER*20   TS(2)
        CHARACTER*80   TITLE1
        CHARACTER*84   TITLE2
        CHARACTER*10   STRING1(50)
        CHARACTER*12   STRING2(50)
        CHARACTER*30   DATM(10) ! Processing dates
        REAL EQUI               !
        REAL RENS(5),DRENS(5)
	REAL VA,VB,E_VA,E_VB
	EQUIVALENCE (RENS(2),VA),(DRENS(2),E_VA)
	EQUIVALENCE (RENS(3),VB),(DRENS(3),E_VB)
	REAL SIGVR
	REAL ELC(10),DELC(10)
        REAL ELB(20),DELB(20)   ! Temporary storage for orbital elements
                                ! (and additionnal stuff) in another order
	REAL ELB1(20),DELB1(20) ! Another set of the same (older?)
        REAL FMAS,DFMAS
        REAL*8 OBZ(NDAT,7)

        COMMON/FICH1/TS,TITLE1,TITLE2,STRING1,STRING2,DATM
        COMMON/FICH2/EQUI,RENS,DRENS,SIGVR,ELC,DELC,ELB,DELB
        COMMON/FICH3/FMAS,DFMAS,ELB1,DELB1,OBZ
