* Massaged radial velocity data
      REAL*8  PLOT_XV(2*MVR),PLOT_V(2*MVR)
      INTEGER VR_INDEX(2*MVR)
      INTEGER PLOT_NV
* Displayable visual data
      REAL XDISP(MOBS),YDISP(MOBS)     
* Fitted visual orbit
      REAL    MODEL_X(NPTS),MODEL_Y(NPTS), MODEL_PHASE(NPTS)
*
      COMMON /PLOT_DATA/ 
* Real*8
     +     PLOT_XV,PLOT_V,
* Real
     +     MODEL_X, MODEL_Y, MODEL_PHASE,
     +     XDISP, YDISP, 
* Integer
     +     VR_INDEX,PLOT_NV
