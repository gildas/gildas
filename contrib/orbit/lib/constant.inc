      INTEGER MORBIT	! Maximum number of simultaneously solved orbits
!      PARAMETER (MORBIT=1) ! Limited to double systems.
!      PARAMETER (MORBIT=2) ! Accepts triples
      PARAMETER (MORBIT=3) ! Accepts triples and symetric quadruples
* Numbering of the orbits in multiple systems
      INTEGER IORB0  ! Outer orbit
      PARAMETER (IORB0=1) 
      INTEGER IORB1  ! Inner orbit centered on primary of the outer orbit
      PARAMETER (IORB1=2)
      INTEGER IORB2  ! Inner orbit centered on secundary of the outer orbit
      PARAMETER (IORB2=3)
*
      INTEGER NCOMPONENTS ! Number of points for which a velocity can possibly 
      PARAMETER (NCOMPONENTS=2*MORBIT) ! be measured
*	
      INTEGER NORB0 ! Number of orbits in a simple binary
      PARAMETER (NORB0=1)
      INTEGER NORB1 ! Number of orbits in a (11-12)-2 triple
      PARAMETER (NORB1=2)
      INTEGER NORB2 ! Number of orbits in a 1-(21-22) triple	
      PARAMETER (NORB2=2) 
      INTEGER NORBALL ! Maximum supported number of orbits:
	              ! (11-12)-(21-22) quadruple 
      PARAMETER (NORBALL=3) 
* As there are no PARAMETER arrays, these constants are kept in a common
* and initialised in a startup routine
      INTEGER ELINDX(MORBIT) ! Index of first element of each orbit
      INTEGER LISTORB0(1)    ! List of orbits used for a simple binary 	
      INTEGER LISTORB1(2)    ! List of orbits used for a (11-12)-2 triple
      INTEGER LISTORB2(2)    ! List of orbits used for a 1-(21-22) triple
      INTEGER LISTORBALL(MORBIT)    ! Maximal list of orbits 
      INTEGER COMPONENTS(2,MORBIT)
      INTEGER LISTCOMPALL(2*MORBIT)   ! List of all possible components
      CHARACTER*4 COMPNAME(2,MORBIT),ORBNAME(MORBIT)	
      COMMON /CONSTANTS/ ELINDX,LISTORB0,LISTORB1,LISTORB2,LISTORBALL,
     +    COMPONENTS,LISTCOMPALL
      COMMON /CONSTANT_CHARS/ COMPNAME,ORBNAME
*
* Orbital elements and related dimensioning parameters
	INTEGER MVREF        ! Maximum number of radial velocity referentials
	PARAMETER (MVREF=8)
* Fixe systemic parameters (V0, X0, Y0, muX, muY), 
* 11 orbital elements per orbit (including one grav+conf velocity shift per 
* star), 
* and one set of 4 profile parameters per orbit and per RV origin.
        INTEGER MELEM        ! Maximum number of orbital elements
        PARAMETER (MELEM=6     ! System-wide: V0, X0, Y0, muX, muY, epoch
     +             +11*MORBIT       
     +             +4*MVREF*MORBIT) 
	INTEGER MPHOT        ! Maximum number of photometric bands
	PARAMETER (MPHOT=16)
	INTEGER MA           ! Maximum number of adjustable parameters
* Add to previous elements one zero point offset per radial velocity origin, 
* and one flux ratio per photometric band (the later eventually needs to be 
* generalised to multiples)
	PARAMETER (MA=MELEM+(MVREF-1)+MPHOT)
* 
* Number of supported data types, and their code: V1, V2, rho,theta, X...
*
	INTEGER NDTYPE       
	PARAMETER (NDTYPE=20)
	INTEGER TYPE_V1
	PARAMETER (TYPE_V1=1)
	INTEGER TYPE_V2
	PARAMETER (TYPE_V2=2)
	INTEGER TYPE_THETA
	PARAMETER (TYPE_THETA=3)
	INTEGER TYPE_RHO
	PARAMETER (TYPE_RHO=4)
	INTEGER TYPE_X
	PARAMETER (TYPE_X=5)
	INTEGER TYPE_Y
	PARAMETER (TYPE_Y=6)
	INTEGER TYPE_Z
	PARAMETER (TYPE_Z=7)
	INTEGER TYPE_T
	PARAMETER (TYPE_T=8)
	INTEGER TYPE_VCORR
	PARAMETER (TYPE_VCORR=9)
	INTEGER TYPE_PROJ
	PARAMETER (TYPE_PROJ=10)
	INTEGER TYPE_PAR
	PARAMETER (TYPE_PAR=11)
	INTEGER TYPE_V11
	PARAMETER (TYPE_V11=12)
	INTEGER TYPE_V12
	PARAMETER (TYPE_V12=13)
	INTEGER TYPE_V21
	PARAMETER (TYPE_V21=14)
	INTEGER TYPE_V22
	PARAMETER (TYPE_V22=15)
	INTEGER TYPE_AXY
	PARAMETER (TYPE_AXY=16)
	INTEGER TYPE_APROJ
	PARAMETER (TYPE_APROJ=17)
	INTEGER TYPE_VISIBILITY
	PARAMETER (TYPE_VISIBILITY=18)
	INTEGER TYPE_VISI_SQUARED
	PARAMETER (TYPE_VISI_SQUARED=19)
	INTEGER TYPE_DMAG
	PARAMETER (TYPE_DMAG=20)
*
        INTEGER NDAT         ! Maximum number of data points (of one type) 
        PARAMETER (NDAT=1024) ! used in fit
	INTEGER MPARA        ! Maximum number of parallaxes
	PARAMETER (MPARA=50) ! Wishful thinking...
	INTEGER MVR          ! Maximum number of radial velocities
	PARAMETER (MVR=NDAT)
	INTEGER MOBS         ! Maximum number of visual separations
	PARAMETER (MOBS=NDAT)
	INTEGER MVISI        ! Maximum numbber of visibilities
	PARAMETER (MVISI=512)
	INTEGER MVCORR       ! Maximum number of correlation profiles
	PARAMETER (MVCORR=NDAT)
	INTEGER MVPNTS       ! Maximum number of points in a correlation
	PARAMETER (MVPNTS=512) ! profile
	INTEGER MFREQ       ! Maximum number of frequencies in a periodogram
	PARAMETER (MFREQ=4196) 
	INTEGER M_MC           ! Maximum number of samples for montercarlo
	PARAMETER (M_MC=10000) ! error estimate
        INTEGER NPTS         ! Number of points used to plot the fitted visual
        PARAMETER (NPTS=100) ! orbit
        INTEGER NPTS_VR     ! Number of points used to plot the fitted RV curve
        PARAMETER (NPTS_VR=1024)
*
* Index for various items in arrays
* Time
	INTEGER I_TIME
	PARAMETER (I_TIME=1)
* Radial velocities
	INTEGER I_VR
	PARAMETER (I_VR=2)
	INTEGER I_S_VR
	PARAMETER (I_S_VR=3)
	INTEGER I_OC_VR
	PARAMETER (I_OC_VR=4)
	INTEGER I_PHASE
	PARAMETER (I_PHASE=5)
* Angular separations
	INTEGER I_THETA
	PARAMETER (I_THETA=2)
	INTEGER I_RHO
	PARAMETER (I_RHO=3)
	INTEGER I_S_RHO
	PARAMETER (I_S_RHO=4)
	INTEGER I_OC_THETA
	PARAMETER (I_OC_THETA=5)
	INTEGER I_OC_RHO
	PARAMETER (I_OC_RHO=6)
	INTEGER I_S_THETA
	PARAMETER (I_S_THETA=7)
* Cartesian positions
	INTEGER I_X
	PARAMETER (I_X=2)
	INTEGER I_Y
	PARAMETER (I_Y=3)
	INTEGER I_S_X
	PARAMETER (I_S_X=4)
	INTEGER I_OC_X
	PARAMETER (I_OC_X=5)
	INTEGER I_OC_Y
	PARAMETER (I_OC_Y=6)
	INTEGER I_S_Y
	PARAMETER (I_S_Y=7)
	INTEGER I_PIFX, I_PIFY ! Parallax factors along RA and Dec
	PARAMETER (I_PIFX=8)
	PARAMETER (I_PIFY=9)
* Measurement along 45 degree lines
        INTEGER I_Z
        PARAMETER (I_Z=2)
        INTEGER I_T
        PARAMETER (I_T=3)
        INTEGER I_S_Z
        PARAMETER (I_S_Z=4)
        INTEGER I_OC_Z
        PARAMETER (I_OC_Z=5)
        INTEGER I_OC_T
        PARAMETER (I_OC_T=6)
        INTEGER I_S_T
        PARAMETER (I_S_T=7)
* Projected separations
	INTEGER I_PROJ ! Projected separation
	PARAMETER (I_PROJ=2)
	INTEGER I_S_PROJ ! Standard error for projected separation
	PARAMETER (I_S_PROJ=3)
	INTEGER I_OC_PROJ ! Standard error for projected separation
	PARAMETER (I_OC_PROJ=4)
	INTEGER I_POSANG ! Position angle for projected separation (degrees)
	PARAMETER (I_POSANG=5)
* Visibilities
	INTEGER I_U
	PARAMETER (I_U=2)
	INTEGER I_V
	PARAMETER (I_V=3)
	INTEGER I_W
	PARAMETER (I_W=4)
	INTEGER I_VISI2
	PARAMETER (I_VISI2=5)
	INTEGER I_REAL
	PARAMETER (I_REAL=5)
	INTEGER I_IMAG
	PARAMETER (I_IMAG=6)
	INTEGER I_SIG_REAL
	PARAMETER (I_SIG_REAL=7)
	INTEGER I_SIG_VISI2
	PARAMETER (I_SIG_VISI2=7)
	INTEGER I_SIG_IMAG
	PARAMETER (I_SIG_IMAG=8)
	INTEGER I_OC_REAL
	PARAMETER (I_OC_REAL=9)
	INTEGER I_OC_VISI2
	PARAMETER (I_OC_VISI2=9)
	INTEGER I_OC_IMAG
	PARAMETER (I_OC_IMAG=10)
* Indices of the various orbital elements in the parameter array
* Global parameters
        INTEGER I_V0
        PARAMETER (I_V0=1)
        INTEGER I_X0
        PARAMETER (I_X0=2)
        INTEGER I_Y0
        PARAMETER (I_Y0=3)
        INTEGER I_MuX
        PARAMETER (I_MuX=4)
        INTEGER I_MuY
        PARAMETER (I_MuY=5)
        INTEGER I_EPOCH
        PARAMETER (I_EPOCH=6)
* The 13 parameters which repeat for every orbit (with some redundancy,
* which will have to be fixed once some inner orbits are resolved)
        INTEGER I_PERIOD
        PARAMETER (I_PERIOD=0)
        INTEGER I_T0
        PARAMETER (I_T0=1)
        INTEGER I_Ecc
        PARAMETER (I_Ecc=2)
        INTEGER I_AS
        PARAMETER (I_as=3)
        INTEGER I_OMEGA_U
        PARAMETER (I_OMEGA_U=4)
        INTEGER I_OMEGA_L
        PARAMETER (I_OMEGA_L=5)
        INTEGER I_i
        PARAMETER (I_i=6)
        INTEGER I_K1
        PARAMETER (I_K1=7)
        INTEGER I_K2
        PARAMETER (I_K2=8)
	INTEGER I_DELTV1
	PARAMETER (I_DELTV1=9)
	INTEGER I_DELTV2
	PARAMETER (I_DELTV2=10)
        INTEGER I_W1
        PARAMETER (I_W1=11)
        INTEGER I_W2
        PARAMETER (I_W2=12)
        INTEGER I_EW1
        PARAMETER (I_EW1=13)
        INTEGER I_EW2
        PARAMETER (I_EW2=14)
* Zero point adjustments for additional velocity data sets
        INTEGER I_DV
        PARAMETER (I_DV=MELEM)
* Relative fluxes
        INTEGER I_RELFLUX
        PARAMETER (I_RELFLUX=MELEM+MVREF-1)




