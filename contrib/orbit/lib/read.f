      SUBROUTINE READ(LINE,ERROR)
C----------------------------------------------------------------------------
C ORBIT: Read one data file (fixed or free ASCII format)
C
C----------------------------------------------------------------------------
      CHARACTER*(*) LINE
      LOGICAL ERROR
*
* Local variables
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
      CHARACTER*80    CHAIN,FILE
      CHARACTER*200   COMMAND
      INTEGER         NCH,LF,LENC
      REAL*8 DUMMY(MA,1)
      LOGICAL SIC_PRESENT, FOUND, FROM_RCS, QUIET
      INTEGER LUN
*
* Get name of data file
      IF (SIC_PRESENT(0,1)) THEN
         CALL SIC_CH (LINE,0,1,CHAIN,NCH,.TRUE.,ERROR)
         IF (ERROR) RETURN
      ELSE
         CALL SIC_WPRN ('Name of data file (.dat)--> ',CHAIN,NCH)
      ENDIF
      CALL SIC_PARSEF(CHAIN(1:NCH),FILE,' ','.dat')
      LF = LENC(FILE)
* Check if file exists, and otherwise look for an RCS archive from which 
* it could be checked out 
      INQUIRE (FILE=FILE(1:LF),EXIST=FOUND)
      IF (.NOT.FOUND) THEN
         IF (LF.LT.2) THEN
            FILE(LF+1:LF+2) = ',v'
            LF = LF+2
         ELSEIF (FILE(LF-1:LF).NE.',v') THEN
            FILE(LF+1:LF+2) = ',v'
            LF = LF+2
         ELSE                   ! The specified name was an RCS archive
            WRITE(6,*) 'E-READ, File ',FILE(1:LF), 'does not exist.'
            GOTO 97
         ENDIF            
         INQUIRE (FILE=FILE(1:LF),EXIST=FOUND)
         IF (.NOT.FOUND) THEN
            WRITE(6,*) 'E-READ, Files ',FILE(1:LF-2),' and ',
     +           FILE(1:LF),' both do not exist.'
            GOTO 97
         ENDIF
      ENDIF
      IF (LF.GE.2) THEN
         FROM_RCS = FILE(LF-1:LF).EQ.',v'
      ELSE
         FROM_RCS = .FALSE.
      ENDIF
* Check out from RCS archive to a temporary file
      IF (FROM_RCS) THEN
         FILE(LF+1:LF+4) = '.tmp'
         COMMAND = 'co -p '//FILE(1:LF)//' > '//FILE(1:LF+4)//CHAR(0)
         CALL GAG_SYSTEM(COMMAND)
         LF = LF+4
      ENDIF
* Open file 
      LUN=1
      OPEN(UNIT=LUN, FILE=FILE(1:LF),STATUS='OLD',ERR=97)
*     
* Reset all parameters, unless /APPEND was specified
      IF (.NOT.SIC_PRESENT(1,0)) CALL RESET_PARAMETERS
* Call apropriate read routine
      IF (SIC_PRESENT(2,0)) THEN
         WRITE(6,*) 'E-READ,  Old format is no longer supported'
         ERROR = .TRUE.
         RETURN
!         CALL READ_FIXED_FORMAT(LUN,ERROR)
      ELSE                 ! Free format as default 
         CALL READ_FREE_FORMAT(LUN,ERROR)
      ENDIF
*
      CLOSE(LUN)
      IF (ERROR) GOTO 99
*
      QUIET = .FALSE.
      CALL INIT_KEPLER(EL,QUIET)
* Print elements
      CALL PRINT_ELEMENTS(6,.FALSE.,1,0.5,DUMMY,.FALSE.,ERROR)
      IF (ERROR) GOTO 99
*
* Define SIC variables for the data arrays
      CALL DEFINE_VARIABLES(ERROR)
      IF (ERROR) THEN 
         WRITE(6,'(1X,6(A))') 'E-READ, ',
     +        'Error during definition of SIC variables '
         GOTO 99
      ENDIF
*
      IF (FROM_RCS) THEN
         COMMAND = 'rm '//FILE(1:LF)//CHAR(0)
         CALL GAG_SYSTEM(COMMAND)
      ENDIF
*
      return
*
 97   WRITE(6,'(1X,6(A))') 'E-READ,  Cannot  open file ',FILE(1:LF)
      GOTO 99
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE DEFINE_VARIABLES(ERROR)
      USE GKERNEL_INTERFACES
C------------------------------------------------------------------------------
C ORBIT: Define SIC variables corresponding to individual data item, for
C flexible plot production
C------------------------------------------------------------------------------
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      CALL SIC_DELVARIABLE('RV_TIME',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_DBLE('RV_TIME',VR(1,I_TIME),1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_DBLE('RV',VR(1,I_VR),1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_SIGMA',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_DBLE('RV_SIGMA',VR(1,I_S_VR),1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_RESIDUAL',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_DBLE('RV_RESIDUAL',VR(1,I_OC_VR),1,NVR,
     +     .TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_PHASE',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_DBLE('RV_PHASE',VR(1,I_PHASE),1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_COMP',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_CHARN('RV_COMP',CVR,1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_REF',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_CHARN('RV_REF',VR_REF,1,NVREF,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      CALL SIC_DELVARIABLE('RV_REFCODE',.FALSE.,ERROR)
      ERROR = .FALSE.
      CALL SIC_DEF_INTE('RV_REFCODE',VREFCODE,1,NVR,.TRUE.,ERROR)
      IF (ERROR) RETURN
*
      RETURN
      END

      SUBROUTINE RESET_PARAMETERS
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
      INCLUDE 'corrections.inc'
*
      INTEGER I,J,K
*
* Reset the elements
      DO I=1,MA
         EL(I)    = 0
         ELERR(I) = -999.99D0
         SCLEL(I) =1.
      ENDDO
      EL(I_EPOCH)    = 51544.500 ! Default epoch to J2000.0
      ELERR(I_EPOCH) = 0.0
* Special case: zero point shifts must have null errors when unspecified, to
* avoid meaningless error bars when absent.
      DO I=1,MORBIT
         ELERR(ELINDX(I)+I_DELTV1)=-EPSR4
         ELERR(ELINDX(I)+I_DELTV2)=-EPSR4
      ENDDO
*
      DO I=1,MORBIT
         SCLEL(ELINDX(I)+I_OMEGA_U)=SNGL(GR)
         SCLEL(ELINDX(I)+I_OMEGA_L)=SNGL(GR)
         SCLEL(ELINDX(I)+I_i)=SNGL(GR)
      ENDDO
* Reset header 
      DO I=1,5
         OBJECT(I)=' '
      ENDDO
      RA  = 0.D0
      DEC = 0.D0
      EQUINOX = 2000.0
      SPECTRAL_TYPE(1)=' '
      SPECTRAL_TYPE(2)=' '
      NPHOT   = 0
      NCOMMENT = 0
*
* Reset data (not quite necessary, but let's just play safe), and data 
* counters (needed)
      DO I=1,MOBS
         DO J=1,7
            OBS(I,J)=0
         ENDDO
         STATOBS(I)='OK'
      ENDDO
      NN=0

      DO I=1,MVR
         DO J=1,5
            VR(I,J) = 0
         ENDDO
         STATVR(I) = 'OK'
         CVR(I) = ' '
      ENDDO
      NVR=0
      FORCED_VREF = ' '
      
      DO I=1,MVCORR
         DO J=1,NVPNTS(I)
            VCORR(J,I)=0
         ENDDO
         NVPNTS(I)=0
         STATVCORR(I) = 'OK'
      ENDDO
      NVCORR=0

      DO I=1,MPARA
         STATPARA(I) = 'OK'
      ENDDO
      NPARA=0

!      DO I=1,MVISI
!         DO J=1,10
!            VISI(I,J) = 0
!         ENDDO
!         STATVISI(I) = 'OK'
!         CVISI(I) = ' '
!      ENDDO
      NVISI=0
*
* Reset date interval
      FIRST_JDB = +MAXR8
      LAST_JDB  = -MAXR8
* Photometric information
      DO I=1,MPHOT
         PHOT_BAND(I) = ' '
         PHOT_STAT(I) = 'OK'
         SIG_MAG(I)   = -999.0
         SIG_DMAG(I)  = -999.0
      ENDDO
      NPARA=0
* Reset velocity referential information
      DO I=1,MVREF
         VR_REF(I)=' '
         HAS_DATA(I) = .FALSE.
      ENDDO
      DO I=MELEM+1,MA
         NAMEL(I)=' '
      ENDDO
      NVREF=0
      NVZPDIFF = 0
*
* Fix all elements (is that really a good idea??)
      DO I=1,MA
         EL_STATUS(I) = 'Fixed'
      ENDDO
* Reset profile corrections
      DO J=1,MORBIT
         DO I=1,2
            NPNTS_CORRECTED(I,J) = 0
            CORRECTED_REF(I,J) = 0
            CORRECTED_VAL(I,J) = 0
            CORRECTED_INC(I,J) = 0
            DO K=1,MVPNTS
               PROFILE_CORRECTIONS(K,I,J) = 0
            ENDDO
         ENDDO
      ENDDO
         
*
      END

      SUBROUTINE READ_FREE_FORMAT(LUN,ERROR)
C------------------------------------------------------------------------------
C ORBIT:
C    
C------------------------------------------------------------------------------
      INTEGER LUN               ! Logical unit for input
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
!      INCLUDE 'file.inc'
*
      INTEGER LS,IP
      INTEGER N
      REAL P
      CHARACTER*8192   STRING
*
      DO WHILE (NVR.LT.MVR.AND.NN.LT.MOBS.AND.NVCORR.LT.MVCORR)
         READ(LUN,'(A)',END=1000) STRING
         LS = LEN(STRING)
         CALL BLANC(STRING,LS)  ! Shrink multiple blanks to one
         IF (LS.GT.0) THEN      ! Ignore blank lines
!            write(6,*) string(1:ls)
            IP = INDEX(STRING(1:LS),' ')
            IF (IP.NE.0) THEN
               IF (IP.LT.LS) THEN ! Something else than keyword in line
                  CALL DECODE_DATA_LINE(STRING(1:IP-1),
     +                 STRING(IP+1:LS),ERROR)
                  IF (ERROR) GOTO 99
               ENDIF
            ENDIF
         ENDIF
      ENDDO
* Array overflow
      WRITE (6,'(A,I6,I6,I6)') 
     +     'E-READ,  Number of observations too large !',
     +     NVR,NN,NVCORR
      ERROR = .TRUE.
      RETURN
* Normal flow
 1000 CONTINUE
* 
      CALL PROCESS_VR_REF(ERROR)
      IF (ERROR) GOTO 99
* Suppressed. Should not be needed.
c      CALL PROCESS_PHOT_BANDS(ERROR)
c      IF (ERROR) GOTO 99

* Automatic selection of N digits in period
      P=SNGL(EL(I_PERIOD+1))
      N=7-INT(ALOG10(P))
      WRITE (ELFMT(1)(5:5),'(I1)') N
* 
* Precompute parallax factors for all astrometric measurements
      CALL PREPARE_PIFACTORS(ERROR)
      IF (ERROR) GOTO 99
C Precession in TETA
C        PR=0.0057*SIN(15.*RAD(RA))/COS(RAD(DEC))

C Correct for precession 2000.0
C        DO I=1,NOBS
C          TOBS=(OBS(I,1)-46431.5D0)/365.25 +1986
C
C          OBS(I,2)=OBS(I,2)+PR*(2000.0-TOBS)
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE PREPARE_PIFACTORS(ERROR)
* Precompute the parallax factors for all astrometric measurements
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER I
*
      DO I=1,NN
         CALL PARALLAX_FACTORS(OBS(I,I_TIME),RA,DEC,EQUINOX,
     +     OBS(I,I_PIFX),OBS(I,I_PIFY),ERROR)
         IF (ERROR) GOTO 99 
! Caution: PIFX is currently Dec and PIFY is RA...
!         write(6,*)  OBS(I,I_TIME),OBS(I,I_PIFY),OBS(I,I_PIFX)
      ENDDO
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_DATA_LINE(KEYWORD,CHAIN,ERROR)
      CHARACTER*(*) KEYWORD,CHAIN
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
      INCLUDE 'data.inc'
      INTEGER CODE
      INTEGER I,LL,LENC
*
      LL = LEN(KEYWORD)
!      write(6,*) keyword, LL
* Data keywords first (hopefully more frequent than header ones...)
*
* Radial velocities
      IF     (KEYWORD.EQ.'VA'.OR.KEYWORD.EQ.'V1') THEN
         CALL DECODE_VR(' ','1',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(3,LEN(KEYWORD))).EQ.'VA('.OR.
     +        KEYWORD(1:MIN(3,LEN(KEYWORD))).EQ.'V1(') THEN
         CALL DECODE_VR(KEYWORD(4:LENC(KEYWORD)-1),'1',CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'VB'.OR.KEYWORD.EQ.'V2') THEN
         CALL DECODE_VR(' ','2',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(3,LEN(KEYWORD))).EQ.'VB('.OR.
     +        KEYWORD(1:MIN(3,LEN(KEYWORD))).EQ.'V2(') THEN
         CALL DECODE_VR(KEYWORD(4:LENC(KEYWORD)-1),'2',CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'V11') THEN
         CALL DECODE_VR(' ','11',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(4,LEN(KEYWORD))).EQ.'V11(') THEN
         CALL DECODE_VR(KEYWORD(5:LENC(KEYWORD)-1),'11',CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'V12') THEN
         CALL DECODE_VR(' ','12',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(4,LEN(KEYWORD))).EQ.'V12(') THEN
         CALL DECODE_VR(KEYWORD(5:LENC(KEYWORD)-1),'12',CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'V21') THEN
         CALL DECODE_VR(' ','21',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(4,LEN(KEYWORD))).EQ.'V21(') THEN
         CALL DECODE_VR(KEYWORD(5:LENC(KEYWORD)-1),'21',CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'V22') THEN
         CALL DECODE_VR(' ','22',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(4,LEN(KEYWORD))).EQ.'V22(') THEN
         CALL DECODE_VR(KEYWORD(5:LENC(KEYWORD)-1),'22',CHAIN,ERROR)
* Angular separations
      ELSEIF (KEYWORD.EQ.'rho-theta') THEN
         CALL DECODE_RHOTHETA(CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'theta') THEN
         CALL DECODE_THETA(CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'XY'.OR.KEYWORD.EQ.'AXY'
     +        .OR.KEYWORD.EQ.'PXY'
     +        .OR.KEYWORD.EQ.'xi-eta') THEN
         CALL DECODE_XY(KEYWORD,CHAIN,ERROR)
      ELSEIF (KEYWORD.EQ.'ZT') THEN  ! 45+135 degrees measurement pair
         CALL DECODE_ZT(CHAIN,ERROR)
* Projected separation
      ELSEIF (KEYWORD.EQ.'PROJ'
     +        .OR.KEYWORD.EQ.'APROJ'.OR.KEYWORD.EQ.'PPROJ') THEN
         CALL DECODE_PROJ(KEYWORD,CHAIN,ERROR)
* Single axis measurements (stored in the more general projected separation 
* framework)
      ELSEIF (KEYWORD.EQ.'X'.OR.KEYWORD.EQ.'Y'
     +        .OR.KEYWORD.EQ.'Z'.OR.KEYWORD.EQ.'T') THEN
         CALL DECODE_XYZT(KEYWORD,CHAIN,ERROR)
* Correlation profiles
      ELSEIF (KEYWORD.EQ.'VCORREL') THEN
         CALL DECODE_VCORR(' ',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:MIN(8,LEN(KEYWORD))).EQ.'VCORREL(') THEN
         LL = LENC(KEYWORD)
         IF (LL.LE.10.OR.KEYWORD(LL:LL).NE.')') GOTO 98
         CALL DECODE_VCORR(KEYWORD(9:LL-1),CHAIN,ERROR)
* Squared visibilities
      ELSEIF (KEYWORD(1:MIN(6,LEN(KEYWORD))).EQ.'VISI2(') THEN
         LL = LENC(KEYWORD)
         IF (LL.LT.8.OR.KEYWORD(LL:LL).NE.')') GOTO 98
         CALL DECODE_VISI2(KEYWORD(7:LL-1),CHAIN,ERROR)
* Parallax
       ELSEIF (KEYWORD.EQ.'PAR') THEN
         NPARA = NPARA+1
         IF (NPARA.GT.MPARA) THEN
            WRITE(6,*) 'E-FIT,  Too many parallaxes'
            ERROR = .TRUE.
            RETURN
         ENDIF
         READ(CHAIN,*,END=99) PARALLAX(NPARA),PARERR(NPARA)
         PAR_OC(NPARA) = 0
         IF (PARALLAX(NPARA).LE.0.OR.PARERR(NPARA).LE.0) THEN
            WRITE(6,*) 'E-READ,  Invalid parallax item ',NPARA
            ERROR = .TRUE.
            RETURN
         ENDIF
* Header keywords
      ELSEIF (KEYWORD.EQ.'Gl') THEN
         GLIESE = CHAIN(1:MIN(LEN(GLIESE),LEN(CHAIN)))
      ELSEIF (KEYWORD.EQ.'VR_REF') THEN
         FORCED_VREF = CHAIN(1:MIN(LEN(FORCED_VREF),LEN(CHAIN)))
      ELSEIF (KEYWORD.EQ.'HD') THEN
         HD     = CHAIN(1:MIN(LEN(HD),LEN(CHAIN)))
      ELSEIF (KEYWORD.EQ.'NOMS'.OR.
     +        KEYWORD.EQ.'NOM'.OR.
     +        KEYWORD.EQ.'OBJECT'.OR.
     +        KEYWORD.EQ.'NAMES'.OR.
     +        KEYWORD.EQ.'NAME') THEN
         OTHER_NAME = CHAIN(1:MIN(LEN(OTHER_NAME),LEN(CHAIN)))
      ELSEIF (KEYWORD.EQ.'RA') THEN
         CALL SIC_SEXA(CHAIN,LENC(CHAIN),RA,ERROR)
         IF (ERROR) THEN
            WRITE(6,*) 'E-READ,  Invalid RA string',
     +           CHAIN(1:LENC(CHAIN))
            GOTO 99
         ENDIF
         RA = RA*PI/12.
      ELSEIF (KEYWORD.EQ.'DEC'.OR.KEYWORD.EQ.'DEC') THEN
         CALL SIC_SEXA(CHAIN,LENC(CHAIN),DEC,ERROR)
         IF (ERROR) THEN
            WRITE(6,*) 'E-READ,  Invalid Dec string',
     +           CHAIN(1:LENC(CHAIN))
            GOTO 99
         ENDIF
         DEC = DEC*PI/180.
      ELSEIF (KEYWORD.EQ.'EQ'.OR.KEYWORD.EQ.'EQUINOX') THEN
         READ(CHAIN,*,END=99) EQUINOX
      ELSEIF (KEYWORD.EQ.'TSA') THEN  ! Spectral type of A
         SPECTRAL_TYPE(1) = CHAIN(1:MIN(LEN(SPECTRAL_TYPE(1)),
     +        LEN(CHAIN)))
      ELSEIF (KEYWORD.EQ.'TSB') THEN  ! Spectral type of B
         SPECTRAL_TYPE(2) = CHAIN(1:MIN(LEN(SPECTRAL_TYPE(2)),
     +        LEN(CHAIN)))
      ELSEIF (KEYWORD(1:MIN(7,LEN(KEYWORD))).EQ.'COMMENT'.OR.
     +        KEYWORD(1:1).EQ.'!') THEN
         IF (NCOMMENT.LT.MCOMMENT) THEN
            NCOMMENT=NCOMMENT+1
            COMMENT(NCOMMENT) = CHAIN
         ELSE
            WRITE(6,*) 'W-READ,  Too many comments, ignored.',CHAIN
         ENDIF
* Orbital elements
      ELSEIF (KEYWORD.EQ.'P') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_PERIOD+ELINDX(IORB0)),
     +        ELERR(I_PERIOD+ELINDX(IORB0)),
     +        EL_STATUS(I_PERIOD+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'T0') THEN
         CALL DECODE_ELEMENT(CHAIN,.TRUE.,
     +        EL(I_T0+ELINDX(IORB0)),
     +        ELERR(I_T0+ELINDX(IORB0)),
     +        EL_STATUS(I_T0+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'e'.OR.KEYWORD.eq.'Ecc') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_Ecc+ELINDX(IORB0)),
     +        ELERR(I_Ecc+ELINDX(IORB0)),
     +        EL_STATUS(I_Ecc+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'as') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_as+ELINDX(IORB0)),
     +        ELERR(I_as+ELINDX(IORB0)),
     +        EL_STATUS(I_as+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'OM') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_OMEGA_U+ELINDX(IORB0)),
     +        ELERR(I_OMEGA_U+ELINDX(IORB0)),
     +        EL_STATUS(I_OMEGA_U+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'om') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_OMEGA_L+ELINDX(IORB0)),
     +        ELERR(I_OMEGA_L+ELINDX(IORB0)),
     +        EL_STATUS(I_OMEGA_L+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'i') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_i+ELINDX(IORB0)),
     +        ELERR(I_i+ELINDX(IORB0)),
     +        EL_STATUS(I_i+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'K1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_K1+ELINDX(IORB0)),
     +        ELERR(I_K1+ELINDX(IORB0)),
     +        EL_STATUS(I_K1+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'K2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_K2+ELINDX(IORB0)),
     +        ELERR(I_K2+ELINDX(IORB0)),
     +        EL_STATUS(I_K2+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV1+ELINDX(IORB0)),
     +        ELERR(I_DELTV1+ELINDX(IORB0)),
     +        EL_STATUS(I_DELTV1+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV2+ELINDX(IORB0)),
     +        ELERR(I_DELTV2+ELINDX(IORB0)),
     +        EL_STATUS(I_DELTV2+ELINDX(IORB0)),ERROR)
      ELSEIF (KEYWORD.EQ.'V0') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_V0),ELERR(I_V0),
     +        EL_STATUS(I_V0),ERROR)
* Position zero point and proper motion
      ELSEIF (KEYWORD.EQ.'X0') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_X0),ELERR(I_X0),
     +        EL_STATUS(I_X0),ERROR)
      ELSEIF (KEYWORD.EQ.'Y0') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_Y0),ELERR(I_Y0),
     +        EL_STATUS(I_Y0),ERROR)
      ELSEIF (KEYWORD.EQ.'muX') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_MuX),ELERR(I_MuX),
     +        EL_STATUS(I_muX),ERROR)
      ELSEIF (KEYWORD.EQ.'muY') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_MuY),ELERR(I_MuY),
     +        EL_STATUS(I_MuY),ERROR)
      ELSEIF (KEYWORD.EQ.'EPOCH') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_EPOCH),ELERR(I_EPOCH),
     +        EL_STATUS(I_EPOCH),ERROR)
         EL_STATUS(I_EPOCH) = 'Fixed' ! Special case...
* Profile parameters for any of the orbits
      ELSEIF (KEYWORD(1:2).EQ.'W1') THEN
         CALL DECODE_PROFPAR(KEYWORD,'W1',CHAIN,ERROR)
       ELSEIF (KEYWORD(1:2).EQ.'W2') THEN
         CALL DECODE_PROFPAR(KEYWORD,'W2',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:3).EQ.'EW1') THEN
         CALL DECODE_PROFPAR(KEYWORD,'EW1',CHAIN,ERROR)
      ELSEIF (KEYWORD(1:3).EQ.'EW2') THEN
         CALL DECODE_PROFPAR(KEYWORD,'EW2',CHAIN,ERROR)
* Elements of inner orbit centered on star 1
      ELSEIF (KEYWORD.EQ.'P_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_PERIOD),
     +        ELERR(ELINDX(IORB1)+I_PERIOD),
     +        EL_STATUS(ELINDX(IORB1)+I_PERIOD),ERROR)
      ELSEIF (KEYWORD.EQ.'T0_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.TRUE.,
     +        EL(ELINDX(IORB1)+I_T0),
     +        ELERR(ELINDX(IORB1)+I_T0),
     +        EL_STATUS(ELINDX(IORB1)+I_T0),ERROR)
      ELSEIF (KEYWORD.eq.'Ecc_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_Ecc),
     +        ELERR(ELINDX(IORB1)+I_Ecc),
     +        EL_STATUS(ELINDX(IORB1)+I_Ecc),ERROR)
      ELSEIF (KEYWORD.EQ.'as_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_as),
     +        ELERR(ELINDX(IORB1)+I_as),
     +        EL_STATUS(ELINDX(IORB1)+I_as),ERROR)
      ELSEIF (KEYWORD.EQ.'OM_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_OMEGA_U),
     +        ELERR(ELINDX(IORB1)+I_OMEGA_U),
     +        EL_STATUS(ELINDX(IORB1)+I_OMEGA_U),ERROR)
      ELSEIF (KEYWORD.EQ.'om_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_OMEGA_L),
     +        ELERR(ELINDX(IORB1)+I_OMEGA_L),
     +        EL_STATUS(ELINDX(IORB1)+I_OMEGA_L),ERROR)
      ELSEIF (KEYWORD.EQ.'i_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_i),
     +        ELERR(ELINDX(IORB1)+I_i),
     +        EL_STATUS(ELINDX(IORB1)+I_i),ERROR)
      ELSEIF (KEYWORD.EQ.'K_11'.OR.KEYWORD.EQ.'K1_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_K1),
     +        ELERR(ELINDX(IORB1)+I_K1),
     +        EL_STATUS(ELINDX(IORB1)+I_K1),ERROR)
      ELSEIF (KEYWORD.EQ.'K_12'.OR.KEYWORD.EQ.'K2_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB1)+I_K2),
     +        ELERR(ELINDX(IORB1)+I_K2),
     +        EL_STATUS(ELINDX(IORB1)+I_K2),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV1_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV1+ELINDX(IORB1)),
     +        ELERR(I_DELTV1+ELINDX(IORB1)),
     +        EL_STATUS(I_DELTV1+ELINDX(IORB1)),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV2_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV2+ELINDX(IORB1)),
     +        ELERR(I_DELTV2+ELINDX(IORB1)),
     +        EL_STATUS(I_DELTV2+ELINDX(IORB1)),ERROR)
      ELSEIF (KEYWORD.EQ.'W11'.OR.KEYWORD.EQ.'W1_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_W1+ELINDX(IORB1)),
     +        ELERR(I_W1+ELINDX(IORB1)),
     +        EL_STATUS(I_W1+ELINDX(IORB1)),ERROR)
      ELSEIF (KEYWORD.EQ.'W12'.OR.KEYWORD.EQ.'W2_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_W2+ELINDX(IORB1)),
     +        ELERR(I_W2+ELINDX(IORB1)),
     +        EL_STATUS(I_W2+ELINDX(IORB1)),ERROR)
      ELSEIF (KEYWORD.EQ.'EW11'.OR.KEYWORD.EQ.'EW1_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_EW1+ELINDX(IORB1)),ELERR(I_EW1+ELINDX(IORB1)),
     +        EL_STATUS(I_EW1+ELINDX(IORB1)),ERROR)
      ELSEIF (KEYWORD.EQ.'EW12'.OR.KEYWORD.EQ.'EW2_1') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_EW2+ELINDX(IORB1)),
     +        ELERR(I_EW2+ELINDX(IORB1)),
     +        EL_STATUS(I_EW2+ELINDX(IORB1)),ERROR)
* Elements of inner orbit centered on star 2
      ELSEIF (KEYWORD.EQ.'P_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_PERIOD),
     +        ELERR(ELINDX(IORB2)+I_PERIOD),
     +        EL_STATUS(ELINDX(IORB2)+I_PERIOD),ERROR)
      ELSEIF (KEYWORD.EQ.'T0_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.TRUE.,
     +        EL(ELINDX(IORB2)+I_T0),
     +        ELERR(ELINDX(IORB2)+I_T0),
     +        EL_STATUS(ELINDX(IORB2)+I_T0),ERROR)
      ELSEIF (KEYWORD.eq.'Ecc_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_Ecc),
     +        ELERR(ELINDX(IORB2)+I_Ecc),
     +        EL_STATUS(ELINDX(IORB2)+I_Ecc),ERROR)
      ELSEIF (KEYWORD.EQ.'as_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_as),
     +        ELERR(ELINDX(IORB2)+I_as),
     +        EL_STATUS(ELINDX(IORB2)+I_as),ERROR)
      ELSEIF (KEYWORD.EQ.'OM_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_OMEGA_U),
     +        ELERR(ELINDX(IORB2)+I_OMEGA_U),
     +        EL_STATUS(ELINDX(IORB2)+I_OMEGA_U),ERROR)
      ELSEIF (KEYWORD.EQ.'om_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_OMEGA_L),
     +        ELERR(ELINDX(IORB2)+I_OMEGA_L),
     +        EL_STATUS(ELINDX(IORB2)+I_OMEGA_L),ERROR)
      ELSEIF (KEYWORD.EQ.'i_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_i),
     +        ELERR(ELINDX(IORB2)+I_i),
     +        EL_STATUS(ELINDX(IORB2)+I_i),ERROR)
      ELSEIF (KEYWORD.EQ.'K_21'.OR.KEYWORD.EQ.'K1_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_K1),
     +        ELERR(ELINDX(IORB2)+I_K1),
     +        EL_STATUS(ELINDX(IORB2)+I_K1),ERROR)
      ELSEIF (KEYWORD.EQ.'K_22'.OR.KEYWORD.EQ.'K2_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(ELINDX(IORB2)+I_K2),
     +        ELERR(ELINDX(IORB2)+I_K2),
     +        EL_STATUS(ELINDX(IORB2)+I_K2),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV1_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV1+ELINDX(IORB2)),
     +        ELERR(I_DELTV1+ELINDX(IORB2)),
     +        EL_STATUS(I_DELTV1+ELINDX(IORB2)),ERROR)
      ELSEIF (KEYWORD.EQ.'DELTV2_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_DELTV2+ELINDX(IORB2)),
     +        ELERR(I_DELTV2+ELINDX(IORB2)),
     +        EL_STATUS(I_DELTV2+ELINDX(IORB2)),ERROR)
      ELSEIF (KEYWORD.EQ.'W21'.OR.KEYWORD.EQ.'W1_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_W1+ELINDX(IORB2)),
     +        ELERR(I_W1+ELINDX(IORB2)),
     +        EL_STATUS(I_W1+ELINDX(IORB2)),ERROR)
      ELSEIF (KEYWORD.EQ.'W22'.OR.KEYWORD.EQ.'W2_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_W2+ELINDX(IORB2)),
     +        ELERR(I_W2+ELINDX(IORB2)),
     +        EL_STATUS(I_W2+ELINDX(IORB2)),ERROR)
      ELSEIF (KEYWORD.EQ.'EW21'.OR.KEYWORD.EQ.'EW1_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_EW1+ELINDX(IORB2)),ELERR(I_EW1+ELINDX(IORB2)),
     +        EL_STATUS(I_EW1+ELINDX(IORB2)),ERROR)
      ELSEIF (KEYWORD.EQ.'EW22'.OR.KEYWORD.EQ.'EW2_2') THEN
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I_EW2+ELINDX(IORB2)),
     +        ELERR(I_EW2+ELINDX(IORB2)),
     +        EL_STATUS(I_EW2+ELINDX(IORB2)),ERROR)
* Fraction of flux in secondary. Generalise to FLXFRAC_21, etc, for higher
* multiplicity systems. Or store at each hierarchy level the flux fraction
* in the secondary?? Or get back to flux ratio?
      ELSEIF (KEYWORD(1:MIN(8,LL)).EQ.'FLXFRAC(') THEN
         IF (LL.LT.10.OR.KEYWORD(LL:LL).NE.')') THEN
            ERROR = .TRUE.
            GOTO 98
         ENDIF
         IF (ERROR) THEN
            WRITE(6,*) 'E-READ,  Invalid photometric band syntax ',
     +           KEYWORD(9:LL-1)
            GOTO 98
         ENDIF
         CALL FIND_PHOT_CODE(KEYWORD(9:LL-1),I,ERROR)
         I = I+I_RELFLUX
         CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +        EL(I),ELERR(I),
     +        EL_STATUS(I),ERROR)
         IF (ERROR) GOTO 98
         NAMEL(I) = KEYWORD(1:LL)
* List of velocity origins is usually unknown at this point, and 
* always unprocessed: just store the zero point differences somewhere 
* for later processing. This mess is needed because the DVs are differential
* and the reference can change... When that happens it's probably better to 
* leave them initialised to zero than to the value for some other pair.
      ELSEIF (KEYWORD(1:MIN(3,LEN(KEYWORD))).EQ.'DV('.AND.
     +        KEYWORD(LL:LL).EQ.')') THEN
         IF (NVZPDIFF.GE.MVREF) THEN
            WRITE(6,*) 'W-READ, Too many Zero point differences. ',
     +           KEYWORD,' ignored.'
         ELSE
            NVZPDIFF = NVZPDIFF+1
            CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +           TMPZEROVAL(NVZPDIFF),
     +           TMPZEROERR(NVZPDIFF),TMPZEROSTAT(NVZPDIFF),
     +           ERROR)
            TMPZERONAME(NVZPDIFF) = KEYWORD
         ENDIF
* Photometric information
      ELSEIF (KEYWORD(1:MIN(7,LEN(KEYWORD))).EQ.'Lambda_') THEN
         CALL FIND_PHOT_CODE(KEYWORD(8:),CODE,ERROR)
         IF (.NOT.ERROR) THEN
            READ(CHAIN,*,END=99) LAMBDA_PHOT(CODE) ! FILTER_WIDTH(CODE)
         ELSE
            WRITE(6,*) 'W-READ,  Too many magnitude differences.',
     +           ' Ignored:',KEYWORD,CHAIN
         ENDIF
*
      ELSEIF (KEYWORD(1:MIN(2,LEN(KEYWORD))).EQ.'m_') THEN
         CALL FIND_PHOT_CODE(KEYWORD(3:),CODE,ERROR)
         IF (ERROR) THEN
            WRITE(6,*) 'W-READ,  Too many apparent magnitudes.',
     +           ' Ignored:',KEYWORD,CHAIN
         ELSE
            READ(CHAIN,*,END=99) MAGNITUDE(CODE),SIG_MAG(CODE)
         ENDIF
! Move to data category
      ELSEIF (KEYWORD(1:MIN(6,LEN(KEYWORD))).EQ.'Delta_') THEN
         CALL FIND_PHOT_CODE(KEYWORD(7:),CODE,ERROR)
         IF (.NOT.ERROR) THEN
            READ(CHAIN,*,END=99) DELTA_MAG(CODE),SIG_DMAG(CODE)
         ELSE
            WRITE(6,*) 'W-READ,  Too many magnitude differences.',
     +           ' Ignored:',KEYWORD,CHAIN
         ENDIF
      ELSEIF (KEYWORD.EQ.'time'.OR.KEYWORD.EQ.'DATFITC') THEN
         continue ! Comment-like info, ignore
      ELSE  ! Unknown or mistyped keyword
         GOTO 98
      ENDIF
      IF (ERROR) GOTO 99
      RETURN
*
 98   WRITE(6,*) 'Keyword "',KEYWORD,'" is unknown',
     +     ' or mistyped'
      RETURN
*
 99   WRITE(6,'(A,A)') 'E-READ,  Error decoding data line for ',KEYWORD
      WRITE(6,'(A)') CHAIN
      END
c<FF>
      SUBROUTINE DECODE_PROFPAR(KEYWORD,KEYTYPE,CHAIN,ERROR)
      CHARACTER*(*) KEYWORD,KEYTYPE,CHAIN
      LOGICAL ERROR
      INCLUDE 'constant.inc'
      INCLUDE 'elements.inc'
*
      INTEGER IO,IC
      INTEGER IORB,IEL,IZERO,IDX
      CHARACTER*16 ZERONAME
* Look for radial velocity zero point reference code
      IO = INDEX(KEYWORD,'(')
      IF (IO.EQ.0) THEN  ! Nothing specified, default frame
         ZERONAME = ' '
         IO = LEN(KEYWORD)+1
      ELSE
         IF (IO.GE.LEN(KEYWORD)) THEN
            WRITE(6,*) 'E-READ,  Missing closing parenthesis ',
     +           KEYWORD
            GOTO 99
         ENDIF
         IC = IO+INDEX(KEYWORD(IO+1:),')')
         IF (IO.GE.IC) THEN
            WRITE(6,*) 'E-READ,  Missing closing parenthesis ',
     +           KEYWORD
            GOTO 99
         ENDIF
         IF (IC.LE.(IO+1)) THEN
            ZERONAME = ' '
         ELSE
            ZERONAME = KEYWORD(IO+1:IC-1)
         ENDIF
      ENDIF
      CALL GET_VREF_CODE(ZERONAME,IZERO,.FALSE.,ERROR)
      IF (ERROR) GOTO 99
*
* Which orbit
      IF (IO.EQ.LEN(KEYTYPE)+1) THEN
         IORB = IORB0
      ELSEIF (KEYWORD(LEN(KEYTYPE)+1:IO-1).EQ.'_1') THEN
         IORB = IORB1
      ELSEIF (KEYWORD(LEN(KEYTYPE)+1:IO-1).EQ.'_2') THEN
         IORB = IORB2
      ELSE
         WRITE(6,'(2(A))') 
     +        'E-READ,  Unknown orbit numbering ',
     +        KEYWORD(LEN(KEYTYPE)+1:IO-1)
         write(6,*) keyword
         GOTO 99
      ENDIF
*
* Which parameter?
      IF (KEYTYPE.EQ.'W1') THEN
         IEL=I_W1
      ELSEIF (KEYTYPE.EQ.'W2') THEN
         IEL=I_W2
      ELSEIF (KEYTYPE.EQ.'EW1') THEN
         IEL=I_EW1
      ELSEIF (KEYTYPE.EQ.'EW2') THEN
         IEL=I_EW2
      ELSE
         WRITE(6,'(4(A))') 'E-READ,  Internal logic error.',
     +        'Unknown profile description',
     +        ' parameter ',KEYTYPE
         GOTO 99
      ENDIF
* Store values at their proper location
      IDX = IEL+ELINDX(IORB)+(IZERO-1)*4
      CALL DECODE_ELEMENT(CHAIN,.FALSE.,
     +     EL(IDX),ELERR(IDX),EL_STATUS(IDX),ERROR)
      NAMEL(IDX) = KEYWORD
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_ELEMENT(CHAIN,IS_DATE,EL,ELERR,STATUS,ERROR)
      REAL*8 EL,ELERR
      CHARACTER*(*) CHAIN,STATUS
      LOGICAL IS_DATE, ERROR
*
      INTEGER I,IP,LC,IDUM
* Set default values
      ELERR = -999.99D0   ! Flags undefined parameters
      STATUS = 'Fixed'
* Shrink blanks and tabs to one blank
      LC = LEN(CHAIN)
      CALL BLANC(CHAIN,LC)
      IF (LC.LE.0) GOTO 99 ! Unexpected, return on error
* Read element value
      IP = INDEX(CHAIN(1:LC),' ')
      IF (IP.EQ.0.OR.IP.EQ.LC) THEN ! Only the value on the line
         IF (IS_DATE) THEN
            CALL DECODE_DATE(CHAIN(1:LC),EL,IDUM,ERROR)
            IF (ERROR) GOTO 99
         ELSE
            READ (CHAIN(1:LC),*,ERR=99) EL
         ENDIF
         ELERR = 0.D0  ! Parameter is defined, and assumed known with 
                       ! arbitrary precision.
         RETURN
      ELSE
         IF (IS_DATE) THEN
            CALL DECODE_DATE(CHAIN(1:IP-1),EL,IDUM,ERROR)
            IF (ERROR) GOTO 99
         ELSE
            READ (CHAIN(1:IP-1),*,ERR=99) EL
         ENDIF
      ENDIF
* Read standard error
      I = INDEX(CHAIN(IP+1:),' ')
      IF (I.EQ.0.OR.IP+I.EQ.LC) THEN
         READ (CHAIN(IP+1:LC),*,ERR=99) ELERR
         RETURN 
      ELSE
         READ (CHAIN(IP+1:IP+I),*,ERR=99) ELERR
      ENDIF
      IP = IP+I
* Adjustability status
      I = INDEX(CHAIN(IP+1:),' ')
      IF (I.EQ.0.OR.IP+I.EQ.LC) THEN
         STATUS = CHAIN(IP+1:LC)
         RETURN              
      ELSE
         STATUS = CHAIN(IP+1:IP+I)
      ENDIF
      IP = IP+I
      RETURN
*
 99   ERROR=.TRUE.
      END
c<FF>
      SUBROUTINE DECODE_DATE(CHAIN,JDB,IP,ERROR)
* Decode observing date for one measurement and return pointer to rest of 
* data line
C------------------------------------------------------------------------------
C   CHAIN   C*(*)    Date string                           Input
C   JDB     R*8      Julian day for the formatted date     Output
C   IP      I        Index of end of date in string        Output
C   ERROR   L        Error flag                            Output 
C------------------------------------------------------------------------------
      CHARACTER*(*) CHAIN
      REAL*8 JDB
      INTEGER I,IP
      LOGICAL ERROR
*
      INCLUDE 'pi.inc'
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      REAL*8 YEAR,TIME
      INTEGER ID,IM,IY,IDATE,IS
*
* Parse position of the date string
      IP = INDEX(CHAIN,' ')
      IF (IP.EQ.0) THEN 
         IP = LEN(CHAIN)+1
      ELSE
         IP = IP+1
      ENDIF
*
* Decode one of the supported date formats
      IF (CHAIN(1:1).EQ.'Y'.OR.CHAIN(1:1).EQ.'y') THEN 
* Year and fraction of year (e.g. 1961.530) coding
         READ(CHAIN(2:IP-1),*,ERR=99) YEAR
         IY = INT(YEAR)
         CALL DATJ(1,1,IY,IDATE)       ! Radio day for beginning of year
         JDB = IDATE+60549.5           ! Radio days to julian days
         JDB = JDB+(YEAR-IY)*365.242   ! Add back fraction of year
      ELSEIF ((CHAIN(1:1).GE.'0'.AND.CHAIN(1:1).LE.'9')
     +        .OR.CHAIN(1:1).EQ.'+'
     +        .OR.CHAIN(1:1).EQ.'-') THEN 
         I = INDEX(CHAIN(1:IP-1),'-')
* Calendar date (e.g. 25-DEC-1997/00:00:00.1, or 1-JAN-1998)
         IF (I.EQ.2.OR.I.EQ.3) THEN 
            TIME = 0
            IS = INDEX(CHAIN(1:IP-1),'/')
            IF (IS.EQ.0) THEN
               CALL  CDATE(CHAIN(1:IP-1),IDATE,ERROR)
               IF (ERROR) GOTO 99
            ELSE
               CALL  CDATE(CHAIN(1:IS-1),IDATE,ERROR)
               IF (ERROR) GOTO 99
               IF (IS+1.LE.IP-1) THEN
                  CALL SIC_DECODE(CHAIN(IS+1:IP-1),TIME,24,ERROR)
                  IF (ERROR) GOTO 99
               ENDIF
            ENDIF
            JDB = IDATE+60549.5 ! Radio days to julian days
            JDB = JDB+(TIME/(2*PI))
* ISO-8601 format
         ELSEIF (I.EQ.5) THEN
            READ(CHAIN(1:4),'(I4)')  IY
            READ(CHAIN(6:7),'(I2)')  IM
            READ(CHAIN(9:10),'(I2)') ID
            CALL DATJ(ID,IM,IY,IDATE)
            IS = INDEX(CHAIN(1:IP-1),'T')
            IF (IS.EQ.0) THEN ! No time field
               TIME = 0
            ELSE
               IF (IS+1.LE.IP-1) THEN
                  CALL SIC_DECODE(CHAIN(IS+1:IP-1),TIME,24,ERROR)
                  IF (ERROR) GOTO 99
               ENDIF
            ENDIF
            JDB = IDATE+60549.5 ! Radio days to julian days
            JDB = JDB+(TIME/(2*PI))
* Do not accept the common replacement of T by / quasi-ISO8601...
         ELSEIF(INDEX(CHAIN(1:IP-1),'/').NE.0) THEN
            GOTO 99
* Julian day number coding
         ELSE
            READ(CHAIN(1:IP-1),*,ERR=99) JDB
         ENDIF
      ELSE
         GOTO 99
      ENDIF
      FIRST_JDB = MIN(FIRST_JDB,JDB)
      LAST_JDB  = MAX(LAST_JDB,JDB)
      RETURN
*
 99   ERROR = .TRUE.
      WRITE(6,*) 'Invalid date format: ',CHAIN(1:IP-1)
      END

      SUBROUTINE DECODE_XYZT(KEYWORD,CHAIN,ERROR)
      CHARACTER*(*) KEYWORD, CHAIN
      LOGICAL ERROR
*     Fill internal arrays for one separaration measurements
*     
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*     
      INTEGER ID
*
         
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      IF (KEYWORD.EQ.'X') THEN
!         OBS(NN,I_POSANG)=-90.D0 ! East of north
         OBS(NN,I_POSANG)=0.D0 
      ELSEIF (KEYWORD.EQ.'Y') THEN
!         OBS(NN,I_POSANG)=0.D0 ! East of north
         OBS(NN,I_POSANG)=90.D0 
      ELSEIF (KEYWORD.EQ.'Z') THEN
!         OBS(NN,I_POSANG)=-45.D0 ! East of north
         OBS(NN,I_POSANG)=+45.D0 ! East of north
      ELSEIF (KEYWORD.EQ.'T') THEN
!         OBS(NN,I_POSANG)=45.D0 ! East of north
         OBS(NN,I_POSANG)=135.D0 
      ENDIF
*
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +     OBS(NN,I_PROJ),OBS(NN,I_S_PROJ)
      OBS(NN,I_OC_PROJ) = 0
      COBS(NN)='PROJ'
      IF (OBS(NN,I_S_PROJ).LE.0) THEN
         WRITE(6,*) 'E-FIT,  Projection data item ',NN,
     +        ' has a negative or zero standard error'
         GOTO 99
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_PROJ(KEYWORD,CHAIN,ERROR)
      CHARACTER*(*) KEYWORD,CHAIN
      LOGICAL ERROR
*     Fill internal arrays for one XY separaration measurements
*     
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*     
      INTEGER ID
*
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +     OBS(NN,I_POSANG),OBS(NN,I_PROJ),OBS(NN,I_S_PROJ)
      OBS(NN,I_OC_PROJ) = 0
      COBS(NN) = KEYWORD ! 'PROJ'/'APROJ', for Relative/Astrometric measurement
      IF (OBS(NN,I_S_PROJ).LE.0) THEN
         WRITE(6,*) 'E-FIT,  Projection data item ',NN,
     +        ' has a negative or zero standard error'
         GOTO 99
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_ZT(CHAIN,ERROR)
      CHARACTER*(*) CHAIN
      LOGICAL ERROR
*     Fill internal arrays for one XY separaration measurements
*     
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*     
      INTEGER ID
*
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +     OBS(NN,I_Z),OBS(NN,I_T),
     +     OBS(NN,I_S_Z),OBS(NN,I_S_T)
      OBS(NN,I_OC_Z) = 0
      OBS(NN,I_OC_T) = 0
      COBS(NN)='ZT'
      IF (OBS(NN,I_S_Z).LE.0.OR.OBS(NN,I_S_T).LE.0) THEN
         WRITE(6,*) 'E-FIT,  ZT data item ',NN,
     +        ' has a negative or zero standard error'
         ERROR = .TRUE.
         RETURN
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_XY(KEYWORD,CHAIN,ERROR)
      CHARACTER*(*) KEYWORD,CHAIN
      LOGICAL ERROR
* Fill internal arrays for one XY separaration measurements
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*
      INTEGER ID
      REAL*8 TMP8
*
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +     OBS(NN,I_X),OBS(NN,I_Y),
     +     OBS(NN,I_S_X),OBS(NN,I_S_Y)
      OBS(NN,I_OC_X) = 0
      OBS(NN,I_OC_Y) = 0
      COBS(NN) = KEYWORD ! 'XY'/'AXY', for Relative/Astrometric measurement
      IF (OBS(NN,I_S_X).LE.0.OR.OBS(NN,I_S_Y).LE.0) THEN
         WRITE(6,*) 'E-FIT,  XY data item ',NN,
     +        ' has a negative or zero standard error'
         GOTO 99
      ENDIF
      IF (KEYWORD.EQ.'xi-eta') THEN
         COBS(NN) = 'AXY'
         TMP8        = OBS(NN,I_X)
         OBS(NN,I_X) = OBS(NN,I_Y)
         OBS(NN,I_Y) = TMP8
         TMP8          = OBS(NN,I_S_X)
         OBS(NN,I_S_X) = OBS(NN,I_S_Y)
         OBS(NN,I_S_Y) = TMP8
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
c<FF>
      SUBROUTINE DECODE_RHOTHETA(CHAIN,ERROR)
      CHARACTER*(*) CHAIN
      LOGICAL ERROR
* Fill internal arrays for one separation+position angle measurements
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*
      INTEGER ID
*
*
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +      OBS(NN,I_RHO),OBS(NN,I_THETA)
     +     ,OBS(NN,I_S_RHO),OBS(NN,I_S_THETA)
      OBS(NN,I_OC_RHO)   = 0
      OBS(NN,I_OC_THETA) = 0
      COBS(NN)='rho-theta'
*     Use sigma_theta=sigma_rho/rho when unspecified. Not always a good 
*     assumption, in particular when calibration residuals dominate the 
*     error budget, but certainly much better than dividing by zero
      IF (OBS(NN,I_S_THETA).EQ.0) THEN
         OBS(NN,I_S_THETA)=OBS(NN,I_S_RHO)/OBS(NN,I_RHO)
         OBS(NN,I_S_THETA)=OBS(NN,I_S_THETA)*GR ! Degrees
      ENDIF
      IF (OBS(NN,I_S_RHO).EQ.0) THEN
         WRITE(6,*) 'E-FIT,  Rho/Theta data item ',NN,
     +        ' has a zero standard error'
         ERROR = .TRUE.
         RETURN
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE DECODE_THETA(CHAIN,ERROR)
      CHARACTER*(*) CHAIN
      LOGICAL ERROR
* Fill internal arrays for one separation+position angle measurements
*
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'data.inc'
*
      INTEGER ID
*
*
      NN = NN+1
      IF (NN.GT.MOBS) THEN
         WRITE(6,*) 'E-FIT,  Too many visual orbit data'
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      CALL DECODE_DATE(CHAIN,OBS(NN,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +      OBS(NN,I_THETA)
     +     ,OBS(NN,I_S_THETA)
      OBS(NN,I_OC_THETA) = 0
      COBS(NN)='theta'
      IF (OBS(NN,I_S_THETA).EQ.0) THEN
         WRITE(6,*) 'E-FIT,  Rho/Theta data item ',NN,
     +        ' has a zero standard error'
         ERROR = .TRUE.
         RETURN
      ENDIF
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE DECODE_VCORR(ORIGIN,CHAIN,ERROR)
* Fill internal arrays for one radial velocity item
      CHARACTER*(*) ORIGIN,CHAIN
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER I, ID, NPT
*
      NVCORR = NVCORR+1
      IF (NVCORR.GT.MVCORR) THEN
         WRITE(6,*) 'E-FIT,  Too many radial velocity profiles'
         ERROR = .TRUE.
         RETURN
      ENDIF
*
      CALL DECODE_DATE(CHAIN,COR_TIME(NVCORR),ID,ERROR)
      IF (ERROR) GOTO 99
*
      READ(CHAIN(ID:),*,END=99) NPT
      NPT = MIN(NPT,MVPNTS)
      NVPNTS(NVCORR) = MAX(NPT,0)
*
      READ(CHAIN(ID:),*,ERR=99,END=99) NPT,
     +     COR_REFPIX(NVCORR),
     +     COR_REFVAL(NVCORR),
     +     COR_VSTEP(NVCORR),
     +     COR_SIGM(NVCORR),
     +     (VCORR(I,NVCORR),I=1,NVPNTS(NVCORR))
*
      CALL GET_VREF_CODE(ORIGIN,CORREFCOD(NVCORR),.TRUE.,ERROR)
      IF (ERROR) GOTO 99
*
*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END
C<FF>
      SUBROUTINE DECODE_VR(ORIGIN,COMPONENT,CHAIN,ERROR)
* Fill internal arrays for one radial velocity item
      CHARACTER*(*) ORIGIN,COMPONENT,CHAIN
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER ID
*
*
      NVR=NVR+1 
      IF (NVR.GT.MVR) THEN
         WRITE(6,*) 'E-FIT,  Too many radial velocity data'
         GOTO 99
      ENDIF
*
      CALL DECODE_DATE(CHAIN,VR(NVR,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
      READ(CHAIN(ID:),*,ERR=99,END=99) 
     +     VR(NVR,I_VR), VR(NVR,I_S_VR)
      IF (VR(NVR,I_S_VR).EQ.0) THEN
         WRITE(6,*) 'E-FIT,  Radial velocity data item ',NVR,
     +        ' has a zero standard error'
         WRITE(6,*) CHAIN
         GOTO 99
      ENDIF
*
      VR(NVR,I_OC_VR) = 0       ! Reset O-C

      CVR(NVR)        = COMPONENT
      CALL GET_VREF_CODE(ORIGIN,VREFCODE(NVR),.TRUE.,ERROR)
      IF (ERROR) GOTO 99
*
      RETURN
*
* Error processing: set flag and decrement data counter by one unit
 99   ERROR = .TRUE.
      NVR = NVR-1
      END
C<FF>

      SUBROUTINE DECODE_VISI2(BAND,CHAIN,ERROR)
* Fill internal arrays for one radial velocity item
      CHARACTER*(*) BAND,CHAIN
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER ID
*
      NVISI = NVISI+1
      IF (NVISI.GT.MVISI) THEN
         WRITE(6,*) 'E-FIT,  Too many squared visibilities'
         ERROR = .TRUE. 
         RETURN
      ENDIF
      CVISI(NVISI) = 'VISI2'
* Date
      CALL DECODE_DATE(CHAIN,VISI(NVISI,I_TIME),ID,ERROR)
      IF (ERROR) GOTO 99
* Photometric band used for the measurement
      CALL FIND_PHOT_CODE(BAND,VISI_BAND(NVISI),ERROR)
      IF (ERROR) THEN
         WRITE(6,*) 'E-READ, Unsupported photometric band: ',BAND
         GOTO 99
      ENDIF
* Read actual data
      READ(CHAIN(ID:),*,ERR=99) VISI(NVISI,I_U), VISI(NVISI,I_V), 
     +     VISI(NVISI,I_W),
     +     VISI(NVISI,I_VISI2), VISI(NVISI,I_SIG_VISI2)
      VISI(NVISI,I_OC_VISI2) = 0.
      STATVISI(NVISI) = 'OK'
*
*
*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END


C<FF>
      SUBROUTINE GET_VREF_CODE(NAME,CODE,IS_DATA,ERROR)
* Returns an internal code number for each radial velocity referential, such
* ELODIE, CORAVEL, Marcy, IAU-Faint....
      CHARACTER NAME*(*)
      INTEGER CODE
      LOGICAL ERROR,IS_DATA
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I
*
      CODE = 0
* Look for a match in existing referential name list. Could be optimised if
* MVREF is ever increased to a large number...
      DO I=1,NVREF
         IF (NAME.EQ.VR_REF(I)) THEN  ! Successfull match
            CODE = I
            GOTO 50
         ENDIF
      ENDDO
* Not previously defined. Check for referential list overflow, and add to
* the list.
      IF (NVREF.GE.MVREF) THEN
         WRITE(6,*) 'E-READ,  Too many velocity referentials in use, ',
     +        'maximum number is ',MVREF
         WRITE(6,*) '         If you do need them, increase MVREF ',
     +        'and recompile'
         GOTO 99
      ENDIF
      NVREF = NVREF+1
      VR_REF(NVREF) = NAME
      CODE = NVREF
* Update the data/pure parameter status of the velocity referential
 50   HAS_DATA(CODE) = HAS_DATA(CODE).OR.IS_DATA
      RETURN
* Error exit
 99   ERROR = .TRUE.
      RETURN
      END

      SUBROUTINE FIND_PHOT_CODE(NAME,CODE,ERROR)
*     Find the internal code corresponding to a photometric band name
      CHARACTER*(*) NAME
      INTEGER CODE
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
*
      INTEGER I
* Look for a match amongst already defined bands (not optimised, but NPHOT 
* is small)
      CODE = 0
      DO I=1,NPHOT
         IF (NAME.EQ.PHOT_BAND(I)) THEN
            CODE = I
            RETURN
         ENDIF
      ENDDO
* Not found, append new name to the band list
      IF (NPHOT.LT.MPHOT) THEN
         NPHOT = NPHOT+1
         CODE  = NPHOT
         PHOT_BAND(CODE) = NAME
      ELSE
         WRITE(6,*) 'E-READ, Too many photometric bands'
         ERROR = .TRUE.
      ENDIF
      END

      SUBROUTINE PROCESS_PHOT_BANDS(ERROR)
*     Set up the parameter names for the individual absolute magnitudes in
*     every mentioned photometric band, and look up their value and status 
*     in the temporary buffers.
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
*
      INTEGER I,LENC
      INTEGER OFFSET,IP
*
      OFFSET = MELEM+NVREF-1
*
      DO I=1,NPHOT
         IP = OFFSET+2*I-1
         NAMEL(IP) = 
     +        'M_'//PHOT_BAND(I)(1:MAX(1,LENC(PHOT_BAND(I))))
     +        //'(A)'
         EL(IP)    = 0.D0
         ELERR(IP) = 0.D0
         IP = OFFSET+2*I
         NAMEL(IP) = 
     +        'M_'//PHOT_BAND(I)(1:MAX(1,LENC(PHOT_BAND(I))))
     +        //'(B)'
         EL(IP)    = 0.D0
         ELERR(IP) = 0.D0
      ENDDO
      
      END
c<FF>
      SUBROUTINE PROCESS_VR_REF(ERROR)
* Swap velocity referentials to have the best (i.e. minimum mean 
* standard error) or explicitely specified one first in the list, and set 
* up the parameter names
      LOGICAL ERROR
*
      INCLUDE 'constant.inc'
      INCLUDE 'data.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'elements.inc'
*
      REAL*8 MEAN(MVREF),BEST
      CHARACTER BEST_NAME*16
      INTEGER ND(MVREF),IBEST
      INTEGER LENC
      INTEGER I,J
      LOGICAL DATA
*
      IF (NVREF.LE.1) RETURN ! No need to do anything
      IF (LENC(FORCED_VREF).EQ.0) THEN  ! Select the most accurate system as
                                     ! basic reference, unless one was forced
* Reset counters
         DO I=1,NVREF
            MEAN(I) = 0
            ND(I)   = 0
         ENDDO
         IBEST = 0
* Compute mean standard error for each data origin. Should perhaps be done
* separately for A and B??
         DO I=1,NVR
            IF (VREFCODE(I).GT.NVREF) THEN
               WRITE(6,*) 'F-READ, Internal logic error: Origin code ',
     +              'out of range'
               GOTO 99
            ENDIF
            MEAN(VREFCODE(I)) = MEAN(VREFCODE(I)) + VR(I,I_S_VR)
            ND(VREFCODE(I)) = ND(VREFCODE(I)) + 1
         ENDDO
         DO I=1,NVREF
            IF (ND(I).EQ.0) THEN ! Can happen if only correlation profile
               MEAN(I) = MAXR4
            ELSE
               MEAN(I) = MEAN(I)/ND(I)
            ENDIF
         ENDDO
* Look for best mean error
         BEST = MAXR4
         DO I=1,NVREF
            IF (MEAN(I).LT.BEST) THEN
               BEST  = MEAN(I)
               IBEST = I
            ENDIF
         ENDDO
* A velocity system was explicitly specified. Use it.
      ELSE
         IBEST = 0
         DO I=1,NVREF
            IF (FORCED_VREF.EQ.VR_REF(I)) THEN
               IBEST = I
            ENDIF
         ENDDO
         IF (IBEST.EQ.0) THEN
            WRITE(6,*) 'E-READ,  Specified reference velocity system "',
     +           FORCED_VREF,'" not present in data.'
            WRITE(6,*) 'E-READ,  Available systems are: '
            DO I=1,NVREF
               WRITE(6,*) '  ',VR_REF(I)(1:LENC(VR_REF(I)))
            ENDDO
            GOTO 99
         ENDIF
      ENDIF
      IF (IBEST.EQ.0) THEN ! Happens with only profiles 
         I = 0
         DO WHILE (IBEST.EQ.0.AND.I.LT.NVREF) 
            I = I+1
            IF (HAS_DATA(I)) IBEST = I
         ENDDO
      ENDIF
      IF (IBEST.EQ.0) IBEST = 1 !Should not happen, but...
      IF (IBEST.GT.NVREF.OR.IBEST.LE.0) THEN
         WRITE(6,*) 'F_READ,  Internal logic error, IBEST>NVREF',IBEST
         GOTO 99
      ENDIF
* Swap first and best
      IF (IBEST.NE.1) THEN
         BEST_NAME     = VR_REF(IBEST)
         VR_REF(IBEST) = VR_REF(1)
         VR_REF(1)     = BEST_NAME
         DATA            = HAS_DATA(1)
         HAS_DATA(1)     = HAS_DATA(IBEST)
         HAS_DATA(IBEST) = DATA
* Propagate the code swap to all radial velocity and correlation profile
* data
         DO I=1,NVR
            IF (VREFCODE(I).EQ.1) THEN
               VREFCODE(I) = IBEST
            ELSEIF(VREFCODE(I).EQ.IBEST) THEN
               VREFCODE(I) = 1
            ENDIF
         ENDDO
      ENDIF
*
      DO I=1,NVCORR
         IF (CORREFCOD(I).EQ.1) THEN
            CORREFCOD(I) = IBEST
         ELSEIF(CORREFCOD(I).EQ.IBEST) THEN
            CORREFCOD(I) = 1
         ENDIF
      ENDDO
* Setup names of adjustable offset parameters
      DO I=2,NVREF
         IF (HAS_DATA(I)) THEN ! Ignore referentials which have no associated
                               ! data  
            NAMEL(MELEM+I-1) = 
     +           'DV('//VR_REF(I)(1:MAX(1,LENC(VR_REF(I))))
     +           //'-'//VR_REF(1)(1:MAX(1,LENC(VR_REF(1))))//')'
            EL(MELEM+I-1)    = 0.D0
            ELERR(MELEM+I-1) = 0.D0
         ENDIF
      ENDDO
* Then look for values of these parameters in temporary storage area
      DO I=1,NVREF-1
         DO J=1,NVZPDIFF
            IF (NAMEL(MELEM+I).EQ.TMPZERONAME(J)) THEN
               EL(MELEM+I)        = TMPZEROVAL(J)
               ELERR(MELEM+I)     = TMPZEROERR(J)
               EL_STATUS(MELEM+I) = TMPZEROSTAT(J)
            ENDIF
         ENDDO
      ENDDO

*
      RETURN
*
 99   ERROR = .TRUE.
      RETURN
      END







