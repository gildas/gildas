      SUBROUTINE BUILD_NORMAL(MFIT,LISTA,EL,ALPHA,BETA,
     +     CHISQ,ND,SD,CHI2,SIG,NDATA,QUIET,ERROR)
C-------------------------------------------------------------------------
C ORBIT: Build up the linearised normal equation system from all available
C data.
C-------------------------------------------------------------------------
      INCLUDE 'constant.inc'
      INCLUDE 'pi.inc'
      INCLUDE 'control.inc'
      INCLUDE 'data.inc'
*
      INTEGER MFIT          ! Number of adjustable parameters
      INTEGER LISTA(*)      ! List of adjustable parameters
      REAL*8 EL(MA)         ! Trial values of the orbital elements
      REAL ALPHA(MA,MA)     ! Curvature matrix
      REAL BETA(MA)         ! Second member of normal equations
      REAL CHISQ            ! Chi**2
      INTEGER ND(NDTYPE)    ! Number of measurement of each type
      REAL SD(NDTYPE)       ! Standard deviations of each data type
      REAL CHI2(NDTYPE)     ! Chi**2 for each data type
      REAL SIG              ! Reduced Chi**2
      INTEGER NDATA         ! Total number of data points
      LOGICAL QUIET,ERROR
*      
*
      INTEGER I,J,K
*
* Update the internal sets of orbital elements
      CALL INIT_KEPLER(EL,QUIET)
* Reset the covariance matrix
      DO J=1,MFIT
         DO K=1,J
            ALPHA(J,K)=0.
         ENDDO
         BETA(J)=0.
      ENDDO
      DO J=1,NDTYPE
         ND(J)   = 0
         SD(J)   = 0.
         CHI2(J) = 0.
      ENDDO
C Radial velocity data
      DO I=1,NVR
         IF (STATVR(I).EQ.'OK'.OR.STATVR(I).EQ.'Simulated') THEN
            CALL USE_VR(MFIT,LISTA,EL,ALPHA,BETA,
     +           CHI2(TYPE_V1),SD(TYPE_V1),ND(TYPE_V1),
     +           CVR(I),VREFCODE(I),VR(I,I_TIME),
     +           VR(I,I_VR),VR(I,I_S_VR),VR(I,I_OC_VR),
     +           VR(I,I_PHASE),QUIET,ERROR)
            IF (ERROR) GOTO 99
         ENDIF
      ENDDO
* CORAVEL-like velocity profiles
      DO I=1,NVCORR
         IF  (STATVCORR(I).EQ.'OK'.OR.
     +        STATVCORR(I).EQ.'Simulated') THEN
            CALL USE_VCORR(MFIT,LISTA,EL,ALPHA,BETA,
     +           CHI2(TYPE_VCORR),SD(TYPE_VCORR),ND(TYPE_VCORR),
     +           COR_TIME(I),CORREFCOD(I),NVPNTS(I),COR_REFPIX(I),
     +           COR_REFVAL(I),COR_VSTEP(I),COR_SIGM(I),VCORR(1,I),
     +           QUIET,ERROR)
            IF (ERROR) GOTO 99
         ENDIF
      ENDDO
*     
* Interferometric visibilities
      DO I=1,NVISI
         IF  (STATVISI(I).EQ.'OK'.OR.
     +        STATVISI(I).EQ.'Simulated') THEN
            CALL USE_VISI_SQUARED(MFIT,LISTA,EL,ALPHA,BETA,
     +           CHI2(TYPE_VISI_SQUARED),SD(TYPE_VISI_SQUARED),
     +           ND(TYPE_VISI_SQUARED),
     +           VISI(I,I_TIME),
     +           VISI_BAND(I),LAMBDA_PHOT(VISI_BAND(I)),
     +           VISI(I,I_U),VISI(I,I_V),VISI(I,I_W),
     +           VISI(I,I_VISI2),VISI(I,I_SIG_VISI2),
     +           VISI(I,I_OC_VISI2),
     +           QUIET,ERROR)
            IF (ERROR) GOTO 99
         ENDIF
      ENDDO
*     
*     Data on visual binary orbit
      DO I=1,NN
         IF  (STATOBS(I).EQ.'OK'.OR.
     +        STATOBS(I).EQ.'Simulated') THEN
            IF (COBS(I).EQ.'rho-theta') THEN ! Rho+Theta pair
               CALL USE_RHOTHETA(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_THETA),SD(TYPE_THETA),ND(TYPE_THETA),
     +              OBS(I,I_TIME),
     +              OBS(I,I_THETA),OBS(I,I_S_THETA),OBS(I,I_OC_THETA),
     +              OBS(I,I_RHO),OBS(I,I_S_RHO),OBS(I,I_OC_RHO),
     +              QUIET,ERROR)
               IF (ERROR) GOTO 99
            ELSEIF (COBS(I).EQ.'theta') THEN ! Theta without rho
               CALL USE_THETA(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_THETA),SD(TYPE_THETA),ND(TYPE_THETA),
     +              OBS(I,I_TIME),
     +              OBS(I,I_THETA),OBS(I,I_S_THETA),OBS(I,I_OC_THETA),
     +              QUIET,ERROR)
               IF (ERROR) GOTO 99
            ELSEIF (COBS(I).EQ.'PROJ') THEN ! Projected separation
               CALL USE_PROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_PROJ),SD(TYPE_PROJ),ND(TYPE_PROJ),
     +              OBS(I,I_TIME),
     +              OBS(I,I_PROJ),OBS(I,I_S_PROJ),OBS(I,I_OC_PROJ),
     +              OBS(I,I_POSANG),
     +              QUIET,ERROR)
               IF (ERROR) GOTO 99

*     Data on visual binary orbit, x,y measurements
            ELSEIF (COBS(I).EQ.'XY')  THEN ! X+Y pairs
*     Process x as a projected separation along position angle 0
               CALL USE_PROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_X),SD(TYPE_X),ND(TYPE_X),
     +              OBS(I,I_TIME),
     +              OBS(I,I_X),OBS(I,I_S_X),OBS(I,I_OC_X),
     +              0.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
*     Process y as a projected separation along position angle 90
               CALL USE_PROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_Y),SD(TYPE_Y),ND(TYPE_Y),
     +              OBS(I,I_TIME),
     +              OBS(I,I_Y),OBS(I,I_S_Y),OBS(I,I_OC_Y),
     +              90.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
*     Visual orbit, 45 degree separation measurements
            ELSEIF (COBS(I).EQ.'ZT') THEN
*     Process z...
               CALL USE_PROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_Z),SD(TYPE_Z),ND(TYPE_Z),
     +              OBS(I,I_TIME),
     +              OBS(I,I_Z),OBS(I,I_S_Z),OBS(I,I_OC_Z),
     +              45.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
C Process tz...
               CALL USE_PROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_T),SD(TYPE_T),ND(TYPE_T),
     +              OBS(I,I_TIME),
     +              OBS(I,I_T),OBS(I,I_S_T),OBS(I,I_OC_T),
     +              +135.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
* Photocenter position, with proper motion and zero point removed. X axis
* towards positive RA, Y towards negative Dec...
* Process x and y as a projected separation along position angles 90 and 0,
            ELSEIF (COBS(I).EQ.'PXY')  THEN 
               CALL USE_PPROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_X),SD(TYPE_X),ND(TYPE_X),
     +              OBS(I,I_TIME),
     +              OBS(I,I_X),OBS(I,I_S_X),OBS(I,I_OC_X),
     +              90.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
*     Process y as a projected separation along position angle 0
               CALL USE_PPROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_Y),SD(TYPE_Y),ND(TYPE_Y),
     +              OBS(I,I_TIME),
     +              OBS(I,I_Y),OBS(I,I_S_Y),OBS(I,I_OC_Y),
     +              0.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
* Photocenter position, with proper motion, zero point, and parallax not 
* removed. X axis towards positive RA, Y towards negative Dec...
* Process x and y as a projected separation along position angles 90 and 0,
* X along 90
            ELSEIF (COBS(I).EQ.'AXY')  THEN 
               CALL USE_APROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_X),SD(TYPE_X),ND(TYPE_X),
     +              OBS(I,I_TIME),OBS(I,I_PIFX),OBS(I,I_PIFY),
     +              OBS(I,I_X),OBS(I,I_S_X),OBS(I,I_OC_X),
     +              90.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
*     Process y as a projected separation along position angle 0
               CALL USE_APROJ(MFIT,LISTA,EL,ALPHA,BETA,
     +              CHI2(TYPE_Y),SD(TYPE_Y),ND(TYPE_Y),
     +              OBS(I,I_TIME),OBS(I,I_PIFX),OBS(I,I_PIFY),
     +              OBS(I,I_Y),OBS(I,I_S_Y),OBS(I,I_OC_Y),
     +              0.D0,QUIET,ERROR)
               IF (ERROR) GOTO 99
            ELSE
               WRITE(6,*) 'E-FIT, Unknown visual data type ',COBS(I)
            ENDIF
         ENDIF
      ENDDO
*
* Parallax information (links major axis and velocity semi-amplitudes sum)
      DO I=1,NPARA
         IF  (STATPARA(I).EQ.'OK'.OR.
     +        STATPARA(I).EQ.'Simulated') THEN
            CALL USE_PARALLAX(MFIT,LISTA,EL,ALPHA,BETA,
     +           CHI2(TYPE_PAR),SD(TYPE_PAR),ND(TYPE_PAR),
     +           PARALLAX(I),PARERR(I),PAR_OC(I),QUIET,ERROR)
               IF (ERROR) GOTO 99
            NDATA=NDATA+1
         ENDIF
      ENDDO
*
      CHISQ = 0 
      NDATA = 0 
      DO I=1,NDTYPE
         NDATA=NDATA+ND(I)
         CHISQ=CHISQ+CHI2(I)
      ENDDO

      IF (CHISQ.NE.CHISQ) THEN
         WRITE(6,*) 'Chi**2 is a NaN',CHISQ
         GOTO 99  ! ERROR
      ENDIF
*
*     Symetrize the normal matrix
 360  CONTINUE
      DO J=2,MFIT
         DO K=1,J-1
            ALPHA(K,J)=ALPHA(J,K)
         ENDDO
      ENDDO
*
*     Compute a standard deviation for each data type
      DO J=1,NDTYPE
         IF (ND(J).GT.0) SD(J)=SQRT(SD(J)/ND(J))
      ENDDO
      SIG=SQRT(CHISQ/(NDATA-MFIT))
      RETURN
*
*
 99   ERROR = .TRUE.
      RETURN
      END


