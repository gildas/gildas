      SUBROUTINE GRAD_VISI2(UV_U,UV_V,UV_W,LAMBDA,IBAND,VISI2,B)
* Compute squared visibility and its gradient relative to the orbital elements
* for one position in the uv plane.
*
* B(j)=dVisi^2/dEL(j), RES=Visi^2
*
      REAL*8 UV_U,UV_V,UV_W
      INTEGER IBAND
      REAL LAMBDA
      REAL VISI2,B(*)
*
      INCLUDE 'constant.inc'
      INCLUDE 'kepler.inc'
      INCLUDE 'pi.inc'
*
      INTEGER I
      REAL RHO,THETA
      REAL CTHETA,STHETA
      REAL RR,CRR,SRR,FLX,FACT
      REAL B_RHO(MA),B_THETA(MA)
      REAL DD_RHO,DD_THETA,DD_FLUX
*
      DO I=1,MA
         B(I)=0
      ENDDO
*
* Call keplerian gradient routines (for the outer orbit) for separation and
* position angle. Will eventually need to be switched to cartesian coordinates
* to support higher multiplicity systems, but for now I am more confident in
* the orientation conventions for (rho,theta) than for XY. 
      CALL GRAD_THETA(THETA,B_THETA)
      CALL GRAD_RHO(RHO,B_RHO)

!      rho   =   0.073
!      theta = 239.6

      CTHETA = COS(THETA/180*PI)
      STHETA = SIN(THETA/180*PI)
* Compute from the relative positions and the relative fluxes the squared 
* visibility and its derivative realtive to those parameters.
*             2                                                           2
* mu2 := (1-f)  + 2 f(1-f) cos(2 Pi rho (sin(theta) u + cos(theta) v)) + f
*
*
* dmu2/df := (4f-2) (1 - cos(2 Pi rho (sin(theta) u + cos(theta) v)))
*
* dmu2/dtheta := -2f(1-f) sin(2 Pi rho (sin(theta) u + cos(theta) v)) 2 Pi rho (+cos(theta) u - sin(theta) v)
*
* dmu2/drho := -2f(1-f) sin(2 Pi rho (sin(theta) u + cos(theta) v)) 2 Pi (sin(theta) u + cos(theta) v)
*

!      write(6,'(F6.4,1X,F6.2,2(1X,F7.4),1X,1PG10.4)') 
!     +     rho, theta, u, v, lambda
!!!!      RR = CTHETA*UV_U+STHETA*UV_V  ! Initial incorrect orientation...
      RR = STHETA*UV_U+CTHETA*UV_V
!      write(6,*) 'Baseline along separation direction: ',RR
      RR  = RR/LAMBDA*2*PI*(RHO/3600/180*PI)
      CRR = COS(RR)
      SRR = SIN(RR)
      FLX = FLXFRAC(IBAND)
!      write(6,*) 'RR:',RR,'   COS(RR):',CRR,'  FF:',FLX
      VISI2 = (1-FLX)**2 +2*(1-FLX)*FLX*CRR+FLX**2
      DD_FLUX  = (4*FLX-2)*(1-CRR) 
      FACT =     -2*FLX*(1-FLX)*SRR*2*PI
      DD_THETA = FACT*RHO*(CTHETA*UV_U - STHETA*UV_V)
      DD_RHO   = FACT*(STHETA*UV_U + CTHETA*UV_V)
*
* Compute gradient relative to the orbital elements, by simple application
* of composite derivation.
      DO I=1,MELEM ! Could easily optimise out some more unneeded elements
         B(I)= B_RHO(I)*DD_RHO+B_THETA(I)*DD_THETA
      ENDDO
      B(I_RELFLUX+IBAND)= DD_FLUX ! No indirect dependency for that one
*
      RETURN
      END
*


