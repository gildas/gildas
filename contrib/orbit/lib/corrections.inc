      REAL PROFILE_CORRECTIONS(MVPNTS,2,MORBIT) ! Residuals of average
                                                ! profile from a gaussian
      INTEGER NPNTS_CORRECTED(2,MORBIT)
      REAL*8 CORRECTED_REF(2,MORBIT), ! Conversion formula for the profile
     +       CORRECTED_VAL(2,MORBIT), ! residuals from a gaussian.
     +       CORRECTED_INC(2,MORBIT)
      
	COMMON /CORRECTION/
* Real*8
     +     CORRECTED_REF
     +     ,CORRECTED_VAL,CORRECTED_INC
* Integer
     +     ,NPNTS_CORRECTED 
* Real*4
     +     ,PROFILE_CORRECTIONS
