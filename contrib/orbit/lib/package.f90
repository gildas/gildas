!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the ORBIT package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine orbit_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !
  type(gpack_info_t), intent(out) :: pack
  !
  external :: orbit_pack_init
  external :: orbit_pack_clean
  !
  pack%name='orbit'
  pack%ext='.orbit'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /) ! List here dependencies
  pack%init=locwrd(orbit_pack_init)
  pack%clean=locwrd(orbit_pack_clean)
  pack%authors='T.Forveille'
  !
end subroutine orbit_pack_set
!
subroutine orbit_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! 
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  ! Local
  character(len=1), parameter :: bksl=char(92)
  character(len=64) :: line
  !
  call init_orbit()
  !
  line = 'sic'//bksl//'sic greg2'//bksl//' off'
  call exec_command(line,error)
  !
end subroutine orbit_pack_init
!
subroutine orbit_pack_clean(error)
  !----------------------------------------------------------------------
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  logical :: error
  !
  ! print *,"Called orbit_pack_clean"
  !
end subroutine orbit_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
