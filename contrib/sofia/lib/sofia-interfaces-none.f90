module sofia_interfaces_none
  interface
    subroutine sofia_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! Public package definition routine.
      ! It defines package methods and dependencies to other packages.
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack ! Package info structure
    end subroutine sofia_pack_set
  end interface
  !
  interface
    subroutine sofia_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      integer(kind=4), intent(in)    :: gpack_id
      logical,         intent(inout) :: error
    end subroutine sofia_pack_init
  end interface
  !
  interface
    subroutine sofia_pack_clean(error)
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine sofia_pack_clean
  end interface
  !
  interface
    subroutine sofia_fromclass1(skdata,version)
      use class_api
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      type(sofia_kalibrate_t), intent(out) :: skdata   ! The data to be filled with the Class buffer
      integer(kind=4),         intent(in)  :: version  ! The version of the data
    end subroutine sofia_fromclass1
  end interface
  !
  interface
    subroutine sofia_dump1(version,error)
      !---------------------------------------------------------------------
      ! Hook for command LAS\DUMP
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: version  ! The version of the data
      logical,         intent(inout) :: error    ! Logical error flag
    end subroutine sofia_dump1
  end interface
  !
  interface
    subroutine sofia_setvar1(version,error)
      use gbl_message
      use class_api
      !---------------------------------------------------------------------
      ! Hook for command LAS\SET VARIABLE USER
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: version  ! The version of the data
      logical,         intent(inout) :: error    ! Logical error flag
    end subroutine sofia_setvar1
  end interface
  !
end module sofia_interfaces_none
