// Define the initialization entry point of the libsofia, for dynamical
// import with the command SIC\IMPORT. This is done automatically with
// a macro, letting a maximum of responsibilities to the Gildas kernel.

#include "sic/gpackage-sicimport.h"

GPACK_DEFINE_SIC_IMPORT(sofia);

