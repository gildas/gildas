!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the SOFIA package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sofia_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Public package definition routine.
  ! It defines package methods and dependencies to other packages.
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack ! Package info structure
  !
  external :: class_pack_set
  external :: sofia_pack_init
  external :: sofia_pack_clean
  !
  pack%name='sofia'
  pack%ext='.sofia'
  pack%depend(1:1) = (/ locwrd(class_pack_set) /)
  pack%init=locwrd(sofia_pack_init)
  pack%clean=locwrd(sofia_pack_clean)
  pack%authors='KOSMA Group, PH1, Cologne University'
  !
end subroutine sofia_pack_set
!
subroutine sofia_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  integer(kind=4), intent(in)    :: gpack_id
  logical,         intent(inout) :: error
  ! Global
  external :: sofia_dump1,sofia_setvar1
  !
  ! print *,">>> Entering sofia_pack_init"
  !
  ! Declare known hooks to section SOFIA/KALIBRATE
  call class_user_owner  ('SOFIA','KALIBRATE')
  call class_user_dump   (sofia_dump1)
  call class_user_setvar (sofia_setvar1)
  !
end subroutine sofia_pack_init
!
subroutine sofia_pack_clean(error)
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  ! print *,">>> Entering sofia_pack_clean"
  !
end subroutine sofia_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
