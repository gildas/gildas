program casa_to_uvfits
  use, intrinsic :: iso_fortran_env
  character(len=128) :: fileou,filein,line
  integer :: iunit, ounit, punit ! , eunit
  logical :: error
  integer :: ier
  character(len=20) :: v ! Version number
  v = '1.1-02 24-June-2019' ! Remove leading spaces in names
  !
  iunit = input_unit  ! Sdt IN
  ounit = output_unit ! Sdt OUT
  !!eunit = error_unit  ! Sdt Err
  iunit = 2
  punit = 1
  !
  write(*,*) 'casa_uvfits '//v//' S.Guilloteau, E.Chapillon'
  !
  if (iunit.ne.input_unit) then
    filein = 'listobs.txt'
    open(unit=iunit,file=filein,status='old',iostat=ier)
    if (ier.ne.0) then
      write(*,*) ' ERROR  listobs.txt file does not exists'
      error stop "CASA_UVFITS failed" !       call sysexi(fatale) 
    endif
    read(iunit,'(A)',iostat=ier) line
    if (ier.ne.0) then
      write(*,*) ' ERROR  listobs.txt file is empty'
      error stop " CASA_UVFITS failed" !      call sysexi(fatale) 
    endif
    rewind(iunit)
  endif
  !
  if (punit.ne.output_unit) then
    fileou = 'casa2uvfits.py'
    open(unit=punit,file=fileou)
    rewind(punit)
  endif
  !
  call casa_uvfits(iunit,punit,error)
  if (iunit.ne.input_unit) close(iunit)
  if (punit.ne.output_unit) close(punit)
  if (error) then
    write(*,*) ' ERROR  Could not interpret listobs.txt file'
    error stop " CASA_UVFITS failed" !      call sysexi(fatale) 
  endif
  if (punit.ne.output_unit) then
    write(*,*)  'Script "casa2uvfits.py" created'
    write(*,*) "Use execfile('casa2uvfits.py') to execute"
  endif
  !
contains

subroutine casa_uvfits(iunit,punit,error)
  integer, intent(in) :: iunit, punit
  logical, intent(out) :: error
  !
  ! Scan a "CASA listobs" output file
  ! to write a script for MSTRANSFORM + EXPORTUVFITS
  !
  character(len=1024) :: line, lstring
  character(len=512) :: msname
  integer, parameter :: m_sources=1024 ! More than enough even for ALMA
  integer :: ier,i,j,l,iwin,isour,n_sources,n_fields,n_spw,n_win,n_sou,id
  character(len=20) :: sname, source_name(m_sources), ctmp
  real, allocatable :: win_resol(:), spw_resol(:) 
  character(len=20), allocatable :: field_name(:), win_crest(:), spw_crest(:), spw_center(:)
  character(len=44), allocatable :: spw_name(:), win_name(:)
  character(len=80), allocatable :: win_string(:), names(:) 
  integer, allocatable :: spw_id(:), field_id(:)
  logical, allocatable :: spw_keep(:), spw_ignore(:)
  integer :: iresol, ichan, icenter, k
  !
  error = .true.
  !
  n_sources = 0
  read(iunit,'(A)',iostat=ier) line
  if (ier.ne.0) return
  ! 
  ! Find the MS name : locate the last /
  read(iunit,'(A)') line
  i = 1
  do 
    j = index(line(i:),"/")
    if (j.eq.0) then
      msname = line(i:)
      j = index(msname,' ')
      msname(j:) = ' '
      exit
    else
      i = i+j
    endif
  enddo
  write(ounit,*) "Measurement Set name "//trim(msname)
  !
  k = 2
  !
  ! Loop on input lines
  ! 1) Search for Fields:
  do 
    k = k+1
    read(iunit,'(A)',iostat=ier) line
    if (ier.ne.0) exit
    !
    if (line(1:8).eq."Fields: ") then
      read(line(9:),*) n_fields
      allocate(field_id(n_fields),field_name(n_fields),stat=ier)
      exit
    else if (index(line,'OBSERVE_TARGET').ne.0) then
!123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
!  Date        Timerange (UTC)          Scan  FldId FieldName             nRows     SpwIds   Average Interval(s)    ScanIntent
!  05-Aug-2016/11:28:49.9 - 11:29:06.0     2      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
!              11:29:26.0 - 11:34:28.4     3      0 J0510+1800              189200  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_BANDPASS#ON_SOURCE,CALIBRATE_FLUX#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
      sname = line(52:71)
      isour = 0
      do i=1,n_sources
        if (sname.eq.source_name(i)) then
          isour = i
          exit
        endif
      enddo
      if (isour.eq.0) then
        n_sources = n_sources+1
        source_name(n_sources) = sname
      endif
    endif
  enddo
  ! read the Field names
!123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
!Fields: 3
!  ID   Code Name                RA               Decl           Epoch   SrcId      nRows
!  0    none J0510+1800          05:10:02.369000 +18.00.41.58200 ICRS    0         853622
  read(iunit,'(A)',iostat=ier) line
  k = k+1
  if (ier.ne.0) then
    write(ounit,*) 'Read error at line ',k,ier
    return
  endif
  do i=1,n_fields
    read(iunit,'(A)') line
    read(line(1:8),*) field_id(i)
    field_name(i) = line(13:31)
  enddo
  !
  write(ounit,*) "Found ",n_sources," Sources and ",n_fields," fields"
  !
  ! 2) Search for Spectral Windows:
!         1         2         3         4         5         6         7         8         9         10        11
!123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234
!Spectral Windows:  (8 unique spectral windows and 1 unique polarization setups)
!  SpwID  Name                                     #Chans   Frame   Ch0(MHz)  ChanWid(kHz)  TotBW(kHz) CtrFreq(MHz) BBC Num  Corrs  
!  0      X54283480#ALMA_RB_06#BB_2#SW-01#FULL_RES    128   TOPO  231028.428     15625.000   2000000.0 232020.6156        2  XX  YY
!
  do 
    read(iunit,'(A)',iostat=ier) line
    k = k+1
    if (ier.ne.0) then
      write(ounit,*) 'Read error at line ',k,ier
      return
    endif
    !
    if (line(1:18).eq."Spectral Windows: ") then
      read(line(21:),*) n_spw
      allocate(spw_name(n_spw),spw_id(n_spw),win_string(n_spw),win_name(n_spw),spw_crest(n_spw),win_crest(n_spw),& 
      & win_resol(n_spw),spw_resol(n_spw),spw_center(n_spw),spw_keep(n_spw),spw_ignore(n_spw))
      exit
    endif
  enddo
  read(iunit,'(A)',iostat=ier) line
  k = k+1
  if (ier.ne.0) then
    write(ounit,*) 'Read error at line ',k,ier
    return
  endif
  write(ounit,*) "Found ",n_spw," Spectral Windows"
  !
  ! Caution: The Name field is of variable length, depending on which version of CASA is used. So use the #Chan and ChanWid has
  ! field position indicators
  ichan  = index(line,'#Chan')-1
  iresol = index(line,'ChanWid(kHz)')
  icenter = index(line,'CtrFreq')
  ! Some MS do not have the above information, use the "Ch0(MHz)" stuff just after the "Frame" keyword
  if (icenter.eq.0) icenter = index(line,'Frame')+5
  !
  do i=1,n_spw
    ! Skip blank lines
    j = 0
    do while(j.eq.0)
      read(iunit,'(A)') line      
      k = k+1
      j = len_trim(line)
    enddo
    !
    !!Print *,'SPW ',i,trim(line)
    if (len_trim(line).eq.0) then
      spw_keep(i) = .false.
      spw_ignore(i) = .true.
    else
      !
      read(line(1:8),*,iostat=ier) spw_id(i)
      if (ier.ne.0) then
        Print *,'Ignoring '//trim(line)
        spw_keep(i) = .false.
        spw_ignore(i) = .true.
      else
        spw_ignore(i) = .false.
        spw_name(i) = line(10:ichan)
        spw_crest(i) = " "
        read(line(iresol:iresol+9),*) spw_resol(i)
        !!Print *,'Resol ',i, line(iresol:iresol+9), spw_resol(i)
        spw_center(i) = line(icenter:icenter+12)
        spw_resol(i) = abs(spw_resol(i))
        !
        ! Retain only the Full Resolution windows
        if (index(line,'FULL_RES').ne.0) then
          spw_keep(i) = .true.
        else
          spw_keep(i) = .false.
        endif
      endif
    endif
    !!Print *,'SPW ',i,spw_keep(i)
  enddo
  !
  ! Special case if no window is selected
  if (.not.(any(spw_keep(1:n_spw)))) then
    Print *,'No FULL_RES windows, falling back on non-ignored windows ',n_spw
    spw_keep(1:n_spw) = .not.spw_ignore(1:n_spw)
    ! They have no name at that stage too... Give them a unique one.
    do i=1,n_spw
      spw_name(i) = char(i-1+ichar('A'))
    enddo
  endif
  if (.not.(any(spw_keep(1:n_spw)))) then
    Print *,'No valid window found '
    error = .true.
    return
  endif
  !
  ! 3) Search for Rest Frequencies
!         1         2         3         4
!123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
!Sources: 24
!  ID   Name                SpwId RestFreq(MHz)  SysVel(km/s) 
!  0    J0510+1800          0     232006.965027  0     
! or
!  0    The Name            0     -              -       
  do
    read(iunit,'(A)',iostat=ier) line
    k = k+1
    if (ier.ne.0) then
      write(ounit,*) 'Read error at line ',k,ier
      return
    endif
    !
    if (line(1:9).eq."Sources: ") then
      read(line(10:),*) n_sou
      exit
    endif
  enddo
  read(iunit,'(A)',iostat=ier) line
  if (ier.ne.0) return
  do i=1,n_sou
    read(iunit,'(A)') line
    read(line(28:32),*,iostat=ier) id
    if (ier.ne.0) then
      ! Some lines have no ID 
      !!Print *,'Ignoring '//line(1:46)
    else
      do j=1,n_spw
        if (spw_id(j).eq.id) then
          if (line(34:46).eq."-") then
            Print *,'Undefined frequency for ',j,' using ',spw_center(j)
            spw_crest(j) = spw_center(j)
          else if (spw_crest(j).eq." ") then
            Print *,'Void frequency for ',j,' using ',spw_center(j)
            spw_crest(j) = line(34:46)
          else if (spw_crest(j).ne.line(34:46)) then
            write(ounit,*) 'E-SCAN_CASA,  Inconsistent frequency setting for SPW ',spw_id(j) &
              & ,spw_crest(j),' .ne. ',line(34:46)
            spw_keep(i) = .false.
          endif
          exit
        endif
      enddo
    endif
  enddo
  !
  Print *,'SPW_CREST ',spw_crest
  !
  !
  ! Group the SPW by identical names
  n_win = 0
  do i=1,n_spw
    Print *,'#B Window ',i,' keep ',spw_keep(i),'Name: ',spw_name(i)
    if (spw_keep(i)) then
      iwin = 0
      do j=1,n_win
        if (win_name(j).eq.spw_name(i)) then
          l = len_trim(win_string(j))+1
          write(win_string(j)(l:),'(A,I0)') ",",spw_id(i)
          iwin = j
          !!Print *,'Joined window ',i,' ',spw_name(i),' with seq. ',j,' ',win_name(j)
          exit
        endif
      enddo
      if (iwin.eq.0) then
        n_win = n_win+1
        win_name(n_win) = spw_name(i)
        win_crest(n_win) = spw_crest(i)
        win_resol(n_win) = spw_resol(i)
        write(win_string(n_win),'(I0)') spw_id(i)
      endif
    endif
  enddo
  !
  ! Select only the Target Sources
  ! ...
  !
  ! Printout
  write(ounit,*) n_sources,' Target Sources '
  do i=1,n_sources
    write(ounit,*) source_name(i)
  enddo
  write(ounit,*) n_fields,'Fields '
  do i=1,n_fields
    write(ounit,*) field_id(i),field_name(i)
  enddo
  write(ounit,*) n_spw,'SPWs  '
  do i=1,n_spw
    write(ounit,*) spw_id(i),spw_name(i)
  enddo  
  write(ounit,*) n_win,'Windows  '
  do i=1,n_win
    write(ounit,*) win_crest(i),win_resol(i),trim(win_string(i))
  enddo  
  !
  allocate(names(max(n_win,n_sources)),stat=ier)
  !
  rewind(punit)
  write(punit,'(A)') "# Python MS to UVFITS conversion script"
  write(punit,'(A)') "# created by Task CASA_UVFITS"
  write(punit,'(A)') "#      Author S.Guilloteau 15-May-2018"
  write(punit,'(A)') " "
  write(punit,'(A)') "# Original Measurement Set"
  write(punit,'(A)') "my_invis='"//trim(msname)//"'"
  write(punit,'(A)') " "
  write(punit,'(A)') "# Measurement Set Content in Sources & Spectral setups"
  !  
  ! Build the field name Python tuple
  do i=1,n_sources
    names(i) = source_name(i)
    do j=1,len_trim(names(i))
      if (names(i)(j:j).eq.".") then
        names(i)(j:) = "*" ! The source parsing in Python does not handle "." properly
        exit
      endif
    enddo
  enddo
  call tuples("my_fields",n_sources,names,lstring)
  write(punit,'(A)') trim(lstring)
  !
  ! Build the file names from the truncated Field names
  do i=1,n_sources
    do j=1,len_trim(names(i))
      if (names(i)(j:j).eq." ") then
        names(i)(j:j) = "_"
      else if (names(i)(j:j).eq."*") then
        names(i)(j:) = " " ! Truncate the name there
        exit
      endif
    enddo
  enddo
  call tuples("my_names",n_sources,names,lstring)
  write(punit,'(A)') trim(lstring)
  !
  ! Build the window names Python tuple
  call tuples("my_windows",n_win,win_string,lstring)
  write(punit,'(A)') trim(lstring)
  !
  ! Build the frequency Python tuple
  do i=1,n_win
    names(i) = trim(adjustl(win_crest(i)))//"MHz"
  enddo
  call tuples("my_frequency",n_win,names,lstring)
  write(punit,'(A)') trim(lstring)
  !
  ! Build the resolution Python tuple
  do i=1,n_win
    write(ctmp,'(f11.3)')  win_resol(i) 
    names(i) = adjustl(ctmp)
  enddo
  call tupler("my_resol",n_win,names,lstring)
  write(punit,'(A)') trim(lstring)
  !
  do i=1,n_win
    names(i) = adjustl(win_crest(i))
    j = index(names(i),'.')
    if (j.ne.0) names(i)(j:) = ' ' ! We take the risk of duplication ?
  enddo
  call tuples("my_wids",n_win,names,lstring)
  write(punit,'(A)') trim(lstring)
  !
  write(punit,'(A)') ' '
  write(punit,'(A)') '# Remember current visibility '
  write(punit,'(A)') 'if globals().has_key("vis"):'
  write(punit,'(A)') '  old_vis=vis '
  write(punit,'(A)') 'else:'
  write(punit,'(A)') "  old_vis=' '"  !  "sys.exit('ERROR: Argument vis failed to verify')"
  write(punit,'(A)') ' '
  !
  ! Customization part
  write(punit,'(A)') ' '
  write(punit,'(A)') '# Obtain options from  SPACESAVING and DRYRUN globals'
  write(punit,'(A)') 'my_savings=2'
  write(punit,'(A)') 'if globals().has_key("SPACESAVING"):'
  write(punit,'(A)') "  print 'SPACESAVING =', SPACESAVING"
  write(punit,'(A)') '  if (type(SPACESAVING)!=int or SPACESAVING<0):'
  write(punit,'(A)') "      sys.exit('ERROR: SPACESAVING value not permitted, must be int.\n'"
  write(punit,'(A)') "            + 'Valid values: >2 = delete *.ms.tmp on the fly,\n'"
  write(punit,'(A)') "            + '              1 = delete *.ms.tmp at end,\n'"
  write(punit,'(A)') "            + '              0 = do not delete *.ms.tmp')"
  write(punit,'(A)') "  my_savings = SPACESAVING"
  write(punit,'(A)') ' '
  write(punit,'(A)') '# my_dryrun=0 indicates mstransform & exportuvfits should be executed'
  write(punit,'(A)') 'my_dryrun=0'
  write(punit,'(A)') 'if globals().has_key("DRYRUN"):'
  write(punit,'(A)') "  print 'DRYRUN =', DRYRUN"
  write(punit,'(A)') '  my_dryrun = DRYRUN'
  write(punit,'(A)') ' '
  write(punit,'(A)') '# my_savings>1 indicates .ms.tmp should be deleted in the loop'
  write(punit,'(A)') '# my_savings>0 indicates .ms.tmp should be deleted at end'
  write(punit,'(A)') '# my_savings=0 indicates .ms.tmp should be kept'
  !
  ! Now write up the default part of the script
  !
  ! mstransfrom  global parameters
  !
  write(punit,'(A)') '#'
  write(punit,'(A)') '# Main mstransform keywords'
  write(punit,'(A)') 'regridms=True'
  ! We do not know exactly where is the Calibrated data, but in recent versions
  ! it seems to be in 'corrected' column. Unfortunately "mstransform" does not
  ! automatically fall back to 'data'. So we must find out... And CASA is tricky:
  ! case matters in input for some tasks, but in the MS it is in upper case.
  ! Furthermore, the column name is 'corrected' in Input, but sometimes 
  ! 'CORRECTED_DATA' in the MS.
  write(punit,'(A)') "# find out if the data in is 'data' or 'corrected'"
  write(punit,'(A)') "my_datacolumn = 'data'"
  write(punit,'(A)') "tb.open(my_invis, nomodify=True)"
  write(punit,'(A)') "for cname in tb.colnames() :"
  write(punit,'(A)') "  if (cname=='CORRECTED') :"
  write(punit,'(A)') "    my_datacolumn = 'corrected'"
  write(punit,'(A)') "  elif (cname=='CORRECTED_DATA') :"
  write(punit,'(A)') "    my_datacolumn = 'corrected'"
  write(punit,'(A)') "tb.close()"
  ! The main goal in this step of "mstransform" is to re-align the input
  ! spectral window(S) into a single output one in the new Frame, in general LSRK
  ! So we must combine them. The number of combined channels will depend
  ! on the differential Doppler effect between the various days
  write(punit,'(A)') "# This is essential - Note that it is combinespwS (contrary to exportuvfits)"
  write(punit,'(A)') "combinespws=True"
  ! No need to keep totally flagged data...
  write(punit,'(A)') 'keepflags=False'
  write(punit,'(A)') ' '
  ! 
  ! exportuvfits  global parameters
  ! Fortunately, exportuvfits falls back to 'data' when 'corrected' is not found
  write(punit,'(A)') '# Main exportuvfits keywords'
  write(punit,'(A)') 'overwrite=True'
  write(punit,'(A)') '#'
  !
  ! Main body, with two outer loops
  write(punit,'(A)') '# Loop on fields & windows'
  write(punit,'(A)') 'for my_j in range(len(my_fields)):'
  write(punit,'(A)') '  my_field = my_fields[my_j]'
  write(punit,'(A)') '  my_name = my_names[my_j]'
  write(punit,'(A)') '  print "Current source", my_field'
  write(punit,'(A)') '  for my_i in range(len(my_windows)):'
  write(punit,'(A)') '      print "SPW ", my_i, " group :", my_windows[my_i]'
  write(punit,'(A)') '      my_vis=my_name + "-" + my_wids[my_i] +".ms.tmp"'
  write(punit,'(A)') '      print "My Vis ", my_vis'
  write(punit,'(A)') ' '
  write(punit,'(A)') '      vis       = my_invis'
  write(punit,'(A)') '      outputvis = my_vis'
  write(punit,'(A)') '      field     = my_field'
  write(punit,'(A)') '      spw       = my_windows[my_i]'
  write(punit,'(A)') '      restfreq  = my_frequency[my_i]'
  write(punit,'(A)') '      datacolumn = my_datacolumn'
  write(punit,'(A)') '      if (my_resol[my_i]>10000.):'
  write(punit,'(A)') "        outframe  = ''"
  write(punit,'(A)') "      else:"
  write(punit,'(A)') "        outframe  = 'LSRK'"
  write(punit,'(A)') ' '      
  write(punit,'(A)') '      inp(mstransform)'
  write(punit,'(A)') '      if (my_dryrun==0): '
  write(punit,'(A)') "         os.system('rm -rf '+my_vis)"
  write(punit,'(A)') '         mstransform()'
  write(punit,'(A)') '      print "Done      mstransform()"'
  write(punit,'(A)') ' '      
  write(punit,'(A)') '      my_fits  = my_name + "-" + my_wids[my_i] +".uvfits"'
  write(punit,'(A)') '      vis      = my_vis'
  write(punit,'(A)') '      fitsfile = my_fits'
  write(punit,'(A)') "      field    = ''"
  write(punit,'(A)') "      spw      = ''"
  write(punit,'(A)') "      combinespw = True"
  write(punit,'(A)') "      datacolumn = 'data'" 
  write(punit,'(A)') '      inp(exportuvfits)'
  write(punit,'(A)') '      if (my_dryrun==0):'
  write(punit,'(A)') '         exportuvfits()'
  write(punit,'(A)') '         if (my_savings>1): '
  write(punit,'(A)') "           os.system('rm -rf '+my_vis)"
  write(punit,'(A)') ' '
  write(punit,'(A)') '#'
  write(punit,'(A)') '# Final cleanup if needed'
  write(punit,'(A)') 'if (my_dryrun==0): '
  write(punit,'(A)') '   if (my_savings>0): '
  write(punit,'(A)') "      os.system('rm -rf *.ms.tmp')"
  write(punit,'(A)') '   else:'  
  write(punit,'(A)') '      print "To clean scratch MS, use: rm -rf *.ms.tmp"'
  write(punit,'(A)') ' '
  write(punit,'(A)') 'vis=old_vis '
  write(punit,'(A)') 'print "Done      mstransform() + exportuvfits()"'
  close(punit)
  !
  error = .false.
  !  
#if defined(BIDON)
! Sample result from casa 'listobs()' used to determine read formats
! and required operations 
!  
123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 
         1         2         3         4         5         6         7
================================================================================
           MeasurementSet Name:  /Volumes/Travail/TRAVAIL/DG_Tau-B/2015.1.01108.S/science_goal.uid___A001_X2fe_X49d/group.uid___A001_X2fe_X49e/member.uid___A001_X2fe_X49f/calibrated/calibrated.ms      MS Version 2
================================================================================
   Observer: flouvet     Project: uid://A001/X1ed/Xcb2  
Observation: ALMA

  Telescope Observation Date    Observer       Project        
  ALMA      [                   4.97711e+09, 4.97712e+09]flouvet        uid://A001/X1ed/Xcb2
  ALMA      [                   4.97797e+09, 4.97798e+09]flouvet        uid://A001/X1ed/Xcb2
Data records: 4369330       Total elapsed time = 864816 seconds
   Observed from   05-Aug-2016/11:28:49.9   to   15-Aug-2016/11:42:26.2 (UTC)

   ObservationID = 0         ArrayID = 0
  Date        Timerange (UTC)          Scan  FldId FieldName             nRows     SpwIds   Average Interval(s)    ScanIntent
  05-Aug-2016/11:28:49.9 - 11:29:06.0     2      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:29:26.0 - 11:34:28.4     3      0 J0510+1800              189200  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_BANDPASS#ON_SOURCE,CALIBRATE_FLUX#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:34:54.0 - 11:35:10.1     4      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:35:27.8 - 11:35:58.1     5      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:36:17.0 - 11:36:47.2     6      1 J0429+2724               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              11:37:04.3 - 11:37:20.4     7      2 DG_Tau_B                 19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:37:31.8 - 11:44:06.5     8      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:44:27.1 - 11:44:57.4     9      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:45:18.8 - 11:51:53.5    10      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:52:13.5 - 11:52:29.7    11      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:52:59.4 - 11:53:29.6    12      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:53:49.6 - 11:54:19.8    13      1 J0429+2724               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              11:54:36.8 - 11:54:52.9    14      2 DG_Tau_B                 19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:55:03.9 - 12:01:38.6    15      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:01:59.1 - 12:02:29.3    16      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              12:02:50.8 - 12:09:25.4    17      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:09:45.0 - 12:10:01.1    18      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              12:10:39.9 - 12:11:10.1    19      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              12:11:30.3 - 12:12:00.5    20      1 J0429+2724               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              12:12:18.2 - 12:12:34.4    21      2 DG_Tau_B                 19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              12:12:44.7 - 12:19:19.4    22      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:19:40.2 - 12:20:10.4    23      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              12:20:34.1 - 12:27:08.8    24      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:27:28.4 - 12:27:44.5    25      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              12:28:14.2 - 12:28:44.4    26      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              12:29:04.3 - 12:29:34.6    27      1 J0429+2724               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              12:29:51.9 - 12:30:08.0    28      2 DG_Tau_B                 19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              12:30:27.1 - 12:37:01.7    29      2 DG_Tau_B                245960  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:37:23.4 - 12:37:53.6    30      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              12:38:14.7 - 12:40:45.9    31      2 DG_Tau_B                 94600  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              12:41:07.8 - 12:41:23.9    32      0 J0510+1800               19866  [0]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              12:41:53.9 - 12:42:24.1    33      0 J0510+1800               18920  [0,1,2,3]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]

   ObservationID = 1         ArrayID = 0
  Date        Timerange (UTC)          Scan  FldId FieldName             nRows     SpwIds   Average Interval(s)    ScanIntent
  15-Aug-2016/10:26:08.2 - 10:26:24.3    34      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              10:26:53.9 - 10:31:56.3    35      0 J0510+1800              148200  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_BANDPASS#ON_SOURCE,CALIBRATE_FLUX#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              10:32:20.1 - 10:32:36.2    36      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              10:32:50.8 - 10:33:21.1    37      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              10:33:38.3 - 10:34:08.5    38      1 J0429+2724               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              10:34:26.1 - 10:34:42.2    39      2 DG_Tau_B                 15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              10:34:53.1 - 10:41:27.8    40      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              10:41:48.1 - 10:42:18.4    41      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              10:42:38.5 - 10:49:13.2    42      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              10:49:33.0 - 10:49:49.1    43      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              10:50:17.3 - 10:50:47.5    44      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              10:51:11.6 - 10:51:41.9    45      1 J0429+2724               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              10:51:58.3 - 10:52:18.4    46      2 DG_Tau_B                 15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              10:52:29.7 - 10:59:04.3    47      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              10:59:28.7 - 10:59:58.9    48      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:00:57.6 - 11:07:32.2    49      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:08:24.3 - 11:08:40.5    50      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:09:11.7 - 11:09:41.9    51      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:10:00.7 - 11:10:31.0    52      1 J0429+2724               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              11:11:00.3 - 11:11:16.5    53      2 DG_Tau_B                 15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:11:27.7 - 11:18:02.4    54      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:18:31.3 - 11:19:01.5    55      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:19:29.1 - 11:26:03.8    56      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:26:29.2 - 11:26:53.0    57      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:27:21.1 - 11:27:51.4    58      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:28:18.5 - 11:28:48.7    59      1 J0429+2724               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_WVR#ON_SOURCE,OBSERVE_CHECK_SOURCE#ON_SOURCE]
              11:29:14.6 - 11:29:38.4    60      2 DG_Tau_B                 15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:29:48.7 - 11:36:23.4    61      2 DG_Tau_B                192660  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:36:43.1 - 11:37:13.3    62      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
              11:38:10.9 - 11:40:42.1    63      2 DG_Tau_B                 74100  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [OBSERVE_TARGET#ON_SOURCE]
              11:41:03.5 - 11:41:25.1    64      0 J0510+1800               15561  [4]  [0.576] [CALIBRATE_ATMOSPHERE#AMBIENT,CALIBRATE_ATMOSPHERE#HOT,CALIBRATE_ATMOSPHERE#OFF_SOURCE,CALIBRATE_WVR#AMBIENT,CALIBRATE_WVR#HOT,CALIBRATE_WVR#OFF_SOURCE]
              11:41:56.0 - 11:42:26.2    65      0 J0510+1800               14820  [4,5,6,7]  [6.05, 6.05, 6.05, 6.05] [CALIBRATE_PHASE#ON_SOURCE,CALIBRATE_WVR#ON_SOURCE]
           (nRows = Total number of rows per scan) 
Fields: 3
  ID   Code Name                RA               Decl           Epoch   SrcId      nRows
  0    none J0510+1800          05:10:02.369000 +18.00.41.58200 ICRS    0         853622
  1    none J0429+2724          04:29:52.960773 +27.24.37.87624 ICRS    1         134960
  2    none DG_Tau_B            04:27:02.672318 +26.05.30.15109 ICRS    2        3380748
Spectral Windows:  (8 unique spectral windows and 1 unique polarization setups)
  SpwID  Name                                     #Chans   Frame   Ch0(MHz)  ChanWid(kHz)  TotBW(kHz) CtrFreq(MHz) BBC Num  Corrs  
  0      X54283480#ALMA_RB_06#BB_2#SW-01#FULL_RES    128   TOPO  231028.428     15625.000   2000000.0 232020.6156        2  XX  YY
  1      X54283480#ALMA_RB_06#BB_1#SW-01#FULL_RES    960   TOPO  230516.037        61.035     58593.8 230545.3035        1  XX  YY
  2      X54283480#ALMA_RB_06#BB_3#SW-01#FULL_RES    960   TOPO  219595.267       -61.035     58593.8 219566.0009        3  XX  YY
  3      X54283480#ALMA_RB_06#BB_4#SW-01#FULL_RES    960   TOPO  220433.555       -61.035     58593.8 220404.2882        4  XX  YY
  4      X54283480#ALMA_RB_06#BB_2#SW-01#FULL_RES    128   TOPO  231029.786     15625.000   2000000.0 232021.9734        2  XX  YY
  5      X54283480#ALMA_RB_06#BB_1#SW-01#FULL_RES    960   TOPO  230517.430        61.035     58593.8 230546.6967        1  XX  YY
  6      X54283480#ALMA_RB_06#BB_3#SW-01#FULL_RES    960   TOPO  219596.588       -61.035     58593.8 219567.3218        3  XX  YY
  7      X54283480#ALMA_RB_06#BB_4#SW-01#FULL_RES    960   TOPO  220434.876       -61.035     58593.8 220405.6091        4  XX  YY
Sources: 24
  ID   Name                SpwId RestFreq(MHz)  SysVel(km/s) 
  0    J0510+1800          0     232006.965027  0            
  0    J0510+1800          1     230538         0            
  0    J0510+1800          2     219560.358     0            
  0    J0510+1800          3     220398.6842    0            
  1    J0429+2724          1     230538         0            
  1    J0429+2724          0     232006.965027  0            
  1    J0429+2724          2     219560.358     0            
  1    J0429+2724          3     220398.6842    0            
  2    DG_Tau_B            1     230538         9            
  2    DG_Tau_B            0     232006.965027  9            
  2    DG_Tau_B            2     219560.358     9            
  2    DG_Tau_B            3     220398.6842    9            
  0    J0510+1800          4     232006.965027  0            
  0    J0510+1800          5     230538         0            
  0    J0510+1800          6     219560.358     0            
  0    J0510+1800          7     220398.6842    0            
  1    J0429+2724          5     230538         0            
  1    J0429+2724          4     232006.965027  0            
  1    J0429+2724          6     219560.358     0            
  1    J0429+2724          7     220398.6842    0            
  5    DG_Tau_B            5     230538         9            
  5    DG_Tau_B            4     232006.965027  9            
  5    DG_Tau_B            6     219560.358     9            
  5    DG_Tau_B            7     220398.6842    9            
Antennas: 46:
  ID   Name  Station   Diam.    Long.         Lat.                Offset from array center (m)                ITRF Geocentric coordinates (m)        
                                                                     East         North     Elevation               x               y               z
  0    DA41  A004      12.0 m   -067.45.15.9  -22.53.28.0         52.6609     -704.4171       21.7726  2225094.796703 -5440052.421785 -2481687.277348
  1    DA43  A071      12.0 m   -067.45.19.9  -22.53.23.5        -60.7901     -563.2558       23.8843  2225011.316321 -5440147.990974 -2481558.053457
  2    DA44  A134      12.0 m   -067.45.20.4  -22.53.45.6        -76.0034    -1249.4246       24.2039  2224896.299760 -5439906.959912 -2482190.301760
  3    DA46  A034      12.0 m   -067.45.18.4  -22.53.27.8        -18.8313     -698.8163       22.1780  2225029.594430 -5440081.847715 -2481682.275326
  4    DA47  A085      12.0 m   -067.45.10.5  -22.53.20.5        207.6857     -472.2092       16.7974  2225270.736289 -5440073.088670 -2481471.419366
  5    DA48  A074      12.0 m   -067.45.12.1  -22.53.32.0        161.8149     -828.6208       19.2691  2225176.656934 -5439964.246947 -2481800.725557
  6    DA49  A002      12.0 m   -067.45.16.3  -22.53.27.6         40.6333     -690.2503       21.8017  2225085.761085 -5440062.100230 -2481674.237502
  7    DA50  A089      12.0 m   -067.45.19.8  -22.53.39.4        -57.6230    -1056.0764       30.1154  2224943.848412 -5439974.666372 -2482014.484964
  8    DA51  A023      12.0 m   -067.45.17.8  -22.53.26.2         -1.3149     -648.2175       22.0990  2225053.229877 -5440093.366463 -2481635.630404
  9    DA52  A076      12.0 m   -067.45.20.5  -22.53.33.8        -77.9906     -882.7193       24.5705  2224948.593770 -5440040.068799 -2481852.625533
  10   DA53  A081      12.0 m   -067.45.23.9  -22.53.32.5       -174.5616     -842.8373       21.0870  2224863.872445 -5440088.013186 -2481814.529511
  11   DA54  A073      12.0 m   -067.45.22.9  -22.53.28.1       -147.0725     -705.3322       24.2558  2224910.667420 -5440129.817851 -2481689.086344
  12   DA55  A099      12.0 m   -067.45.33.4  -22.53.31.0       -445.2524     -796.0648       17.7405  2224619.055719 -5440204.457010 -2481770.138975
  13   DA56  A100      12.0 m   -067.45.00.9  -22.53.36.1        479.0804     -953.3921       14.3081  2225450.178696 -5439794.977217 -2481913.739571
  14   DA57  A088      12.0 m   -067.45.18.2  -22.53.18.4        -13.0905     -408.0119       24.1745  2225078.424400 -5440186.069988 -2481415.145947
  15   DA58  A110      12.0 m   -067.45.09.2  -22.53.54.8        244.7458    -1530.9470       13.4941  2225147.945277 -5439675.027094 -2482445.475052
  16   DA59  A001      12.0 m   -067.45.16.9  -22.53.27.7         24.1878     -693.3966       21.7931  2225070.073939 -5440067.185715 -2481677.132686
  17   DA60  A101      12.0 m   -067.45.24.0  -22.53.10.5       -177.3504     -163.1875       25.7535  2224962.986782 -5440337.728223 -2481190.209440
  18   DA61  A006      12.0 m   -067.45.15.0  -22.53.28.0         79.0342     -702.0939       21.7770  2225119.549542 -5440043.278017 -2481685.138868
  19   DA62  A103      12.0 m   -067.44.59.5  -22.53.17.7        519.2605     -384.4381        8.7466  2225569.223936 -5439979.852999 -2481387.427705
  20   DA63  A091      12.0 m   -067.45.28.7  -22.53.24.2       -312.9116     -584.7724       23.7284  2224774.741979 -5440235.546349 -2481577.815095
  21   DA64  A079      12.0 m   -067.45.13.6  -22.53.35.0        116.8374     -920.2891       22.6280  2225122.700654 -5439951.132882 -2481886.480533
  22   DA65  A096      12.0 m   -067.45.29.9  -22.53.15.7       -347.1459     -322.7967       23.2980  2224781.474488 -5440342.450209 -2481336.298820
  23   DV01  A065      12.0 m   -067.45.15.8  -22.53.24.6         56.5631     -597.4552       20.5167  2225113.721213 -5440088.382476 -2481588.249961
  24   DV03  A097      12.0 m   -067.45.15.1  -22.53.43.9         75.7014    -1195.0804       24.7063  2225044.882099 -5439869.531470 -2482140.434352
  25   DV04  A015      12.0 m   -067.45.15.3  -22.53.26.0         68.8261     -640.1825       21.0225  2225118.955532 -5440068.788774 -2481627.809377
  26   DV05  A072      12.0 m   -067.45.12.6  -22.53.24.0        147.1735     -580.5874       18.1837  2225199.254332 -5440058.163238 -2481571.802983
  27   DV06  A008      12.0 m   -067.45.15.4  -22.53.26.8         67.5587     -667.6873       20.9558  2225113.708967 -5440059.309351 -2481653.122280
  28   DV07  A084      12.0 m   -067.45.16.9  -22.53.37.5         23.9631     -996.3192       27.1379  2225027.119784 -5439962.760871 -2481958.276503
  29   DV08  A106      12.0 m   -067.45.14.0  -22.53.02.5        105.8162       86.4720       24.7040  2225261.467540 -5440319.499949 -2480959.792518
  30   DV09  A014      12.0 m   -067.45.15.1  -22.53.26.4         74.5074     -654.2095       20.9871  2225122.135828 -5440061.557800 -2481640.718087
  31   DV10  A087      12.0 m   -067.45.08.3  -22.53.33.2        269.0964     -864.0698       16.2364  2225269.668428 -5439908.282917 -2481832.202850
  32   DV11  A086      12.0 m   -067.45.27.0  -22.53.29.3       -262.5023     -744.4841       26.4666  2224798.837957 -5440161.299263 -2481726.014880
  33   DV12  A007      12.0 m   -067.45.15.1  -22.53.27.3         74.0153     -681.2928       21.3235  2225117.809415 -5440052.280278 -2481665.799458
  34   DV14  A104      12.0 m   -067.45.36.4  -22.53.21.2       -530.9015     -492.4911       20.3385  2224585.380723 -5440348.385963 -2481491.481586
  35   DV15  A048      12.0 m   -067.45.15.8  -22.53.30.1         56.5643     -769.5873       20.6083  2225088.406213 -5440026.487837 -2481746.862261
  36   DV16  A102      12.0 m   -067.45.22.9  -22.53.46.9       -145.7588    -1287.0493       22.7273  2224825.684392 -5439918.555820 -2482224.387683
  37   DV17  A092      12.0 m   -067.45.10.1  -22.53.39.1        217.6032    -1047.7089       20.8479  2225196.572435 -5439865.588559 -2482003.171197
  38   DV18  A009      12.0 m   -067.45.16.1  -22.53.26.1         48.2539     -644.4617       21.0136  2225099.282100 -5440075.028456 -2481631.748149
  39   DV19  A135      12.0 m   -067.45.02.2  -22.53.16.5        442.8916     -348.0848       12.4344  2225505.182937 -5440024.999751 -2481355.370832
  40   DV22  A017      12.0 m   -067.45.15.9  -22.53.26.8         51.3631     -665.5904       21.3591  2225099.168855 -5440066.539247 -2481651.347418
  41   DV24  A075      12.0 m   -067.45.17.9  -22.53.21.4         -4.5597     -499.7005       22.5310  2225072.246370 -5440148.431407 -2481498.976123
  42   DV25  A095      12.0 m   -067.45.00.7  -22.53.28.6        485.1070     -722.0920       10.6814  2225488.560223 -5439872.879660 -2481699.246039
  43   DA45  A094      12.0 m   -067.45.27.0  -22.53.37.1       -264.4596     -984.7304       19.8501  2224759.342944 -5440069.895267 -2481944.765337
  44   DV13  A064      12.0 m   -067.45.14.7  -22.53.31.4         85.6565     -808.0275       21.0170  2225109.813380 -5440001.983029 -2481782.434110
  45   DV15  A113      12.0 m   -067.44.48.6  -22.53.38.4        831.0666    -1024.4282        3.2480  2225761.609253 -5439626.691469 -2481974.877491
#endif
!  
end subroutine casa_uvfits
!
subroutine tuples(prefix,n,names,lstring)
  character(len=*), intent(in) :: prefix
  integer, intent(in) :: n
  character(len=*), intent(in) :: names(n)
  character(len=*), intent(out) :: lstring
  !
  integer :: l,i
  lstring = trim(prefix)//"=['"//trim(names(1))//"'"
  l = len_trim(lstring)+1
  do i=2,n
    lstring(l:) = ",'"//trim(names(i))//"'"
    l = len_trim(lstring)+1
  enddo
  lstring(l:l)= "]"
end subroutine tuples

subroutine tupler(prefix,n,names,lstring)
  character(len=*), intent(in) :: prefix
  integer, intent(in) :: n
  character(len=*), intent(in) :: names(n)
  character(len=*), intent(out) :: lstring
  !
  integer :: l,i
  lstring = trim(prefix)//"=["//trim(names(1))
  l = len_trim(lstring)+1
  do i=2,n
    lstring(l:) = ","//trim(names(i))
    l = len_trim(lstring)+1
  enddo
  lstring(l:l)= "]"
end subroutine tupler

end program
