!------------------------------------------------------------------------
! P_SPECTRE_SUB.IMA
!
!
! Procedure to plot spectra in box for a spectral line data cube.
! Global variables defined in DEFINE script. The first axis is
! assumed to be Velocity or Frequency
!
! Initial version p_spectre.greg  S.Guilloteau 18-jun-1991
!----------------------------------------------------------------------
!
begin procedure sub_pixels
  ! @ pixels Cube X|Y
  ! Uses CENTER and SIZE for the zone
  !
  define integer idim isign iaxi
  define real aval
  if ("&2".eq."x") then
    idim = &1%x_axis
    iaxi = 1
  else
    idim = &1%y_axis
    iaxi = 2
  endif
  if (&1%convert[3,idim].gt.0) then
    let isign 1
  else
    let isign -1
  endif
  !
  let &2pixel &2pix
  !
  if (&2pixel[1].eq.0) then
    if (size[1].eq.0) then
      if (&1%dim[&1%x_axis]*&1%dim[&1%y_axis].le.32*32) then
        let &2pixel[1] 1
      else
        let &2pixel[1] &1%dim[idim]/4
      endif
    else
      aval = (center[iaxi]-isign*size[iaxi]/2)*sec
      let &2pixel[1] &1%convert[1,idim]+(aval-&1%convert[2,idim])/&1%convert[3,idim]
    endif
  endif
  !
  if (&2pixel[2].eq.0) then
    if (size[2].eq.0) then
      if (&1%dim[2]*&1%dim[3].le.32*32) then
        let &2pixel[2] &1%dim[idim]
      else
        let &2pixel[2] 3*&1%dim[idim]/4
      endif
    else
      aval = (center[iaxi]+isign*size[iaxi]/2)*sec
      let &2pixel[2] &1%convert[1,idim]+(aval-&1%convert[2,idim])/&1%convert[3,idim]
    endif
  endif
  !
  if (&2pixel[3].eq.0) then
    if (&1%major.ne.0) then
      ! Average correlation length in pixels
      let &2pixel[3] int(sqrt(&1%major*&1%minor)/abs(&1%convert[3,2]))
    else
      let &2pixel[3] 1
    endif
  endif
  let &2pixel[1] max(1,&2pixel[1])
  let &2pixel[2] min(&2pixel[2],&1%dim[idim])
end procedure sub_pixels
!
let PopupName "SPECTRE"  ! Generic name of GTV sub-directories for POPUP
!
if (exist(xpixel)) dele /var xpixel
if (exist(ypixel)) dele /var ypixel
def real xpixel[3] ypixel[3] /global
!
def real x2 dx y2 dy
def double xmark ymark
def integer m n ix jy faxi xaxi yaxi
def character cmark*24
!
let xaxi &1%x_axis
let yaxi &1%y_axis
let faxi &1%f_axis
let m &1%dim[faxi]
define real xspec[m] yspec[m]
let xspec[i] (i-&1%convert[1,faxi])*&1%convert[3,faxi]+&1%convert[2,faxi]
if (box_limits.eq." ") then
  greg1\limits (-&1%convert[1,faxi])*&1%convert[3,faxi]+&1%convert[2,faxi]  -
    (m+1-&1%convert[1,faxi])*&1%convert[3,faxi]+&1%convert[2,faxi] -
    &1%min-0.05*(&1%max-&1%min) &1%max+0.05*(&1%max-&1%min)
else
  symbol alimits 'box_limits'
  greg1\limits 'alimits'
endif
let x2 box_xmax
let y2 box_ymax
!
@ sub_pixels &1 x
let n = int((xpixel[2]-xpixel[1])|xpixel[3])+1
!
@ sub_pixels &1 y
let m = int((ypixel[2]-ypixel[1])|ypixel[3])+1
!
@ window_tools       ! Use Window Tools
!
let aspect_ratio NO  ! Respect aspect ratio
!! let aspect ratio     ! New value of aspect ratio
let fixed_trc yes    ! Top right corner is fixed
let inter 0.0 0.0    ! No interspace between individual boxes (unit: % of box sizes)
@ plot_position      ! Definition of total available plotting place
let nx_box n
let ny_box m
@ window_xy 'nx_box' 'ny_box' 1 1  ! Box position
!
define integer ibox
let ibox 0
greg1\set expand min(6.0|(m+n),1.)
let dx (x2-box_xmin)|n
let dy (y2-box_ymin)|m
!
! Window Tools goes upside down when used with Sequential box number
! as happens in POPUP -- This needs to be cleaned up...
for j m to 1 by -1
  let jy ypixel[1]+(j-1)*ypixel[3]
  for i 1 to n
    let ix xpixel[1]+(i-1)*xpixel[3]
    let ibox ibox+1
    gtvl\create dir 'PopupName''ibox'
    gtvl\change dir 'PopupName''ibox'
    @ window_xy 'nx_box' 'ny_box' 'i' 'j'
    greg1\box n n
    if (faxi.eq.1) then
      yspec = &1[ix,jy]
    else if (faxi.eq.2) then
      yspec = &1[ix,,jy]
    else if (faxi.eq.3) then
      yspec = &1[ix,jy,]
    endif
    if (do_grey) then
      greg1\histogram xspec yspec /base 0 /fill 11
      if (do_contour) then
        greg1\histogram xspec yspec
      endif
    else
      greg1\histogram xspec yspec
    endif
    let xmark ((ix-&1%convert[1,xaxi])*&1%convert[3,xaxi]+&1%convert[2,xaxi])|sec
    let ymark ((jy-&1%convert[1,yaxi])*&1%convert[3,yaxi]+&1%convert[2,yaxi])|sec
    let xmark 0.1*nint(10*xmark)
    let ymark 0.1*nint(10*ymark)
    greg1\set coord box
    let cmark "("'xmark'","'ymark'")"
    greg1\draw text 0.1 -0.1 'cmark' 3 /box 7
    gtvl\change dir ..
  next
next
!
on error
if (do_header) then
  gtvl\change directory <greg
  gtvl\create directory hbox
  gtvl\change directory hbox
  define real dz
  define char place*8
  !
  ! Defined in p_header  -- Should be factorized
  greg1\set box xbox xbox+boxs ybox ybox+boxs
  !
  let x2 box_xmax
  let y2 box_ymax
  !
  greg1\set expand 0.6
  if (page_x.gt.page_y) then
    let dz min((page_x-x2)|aspect,0.5*page_y)
    ! Shifted in X, Centered around 0.35 in Y
    greg1\set box page_x-0.4-0.6*dz*aspect page_x-0.4  0.35*page_y-0.3*dz 0.35*page_y+0.3*dz
    let place "top"
  else
    let dz min((0.5*page_x|aspect),(page_y-y2))
    ! Right adjusted in X, Centered around in Y
    greg1\set box page_x-2.0-0.6*dz*aspect page_x-2.0 (page_y+y2-0.6*dz)*0.5 (page_y+y2+0.6*dz)*0.5
    let place "right"
  endif
  greg1\box
  greg1\label '&1%unit1' /x
  greg1\label '&1%unit' /y
  gtvl\change dir ..
endif
greg1\set expand 1.0
!
@ d_popup 1 'm*n' 1
