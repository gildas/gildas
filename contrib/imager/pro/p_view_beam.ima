! @ p_view_beams.map NAME KEY Value
!   Display the IMAGE-like variable NAME using the "VIEW" facility
!
if (.not.exist(&1)) then
  Message E VIEW "No such variable &1"
  let mapping_error yes
  return 
endif
if (rank(&1).lt.2)  then
  Message E VIEW "Variable &1 is less than 2-D"
  let mapping_error yes
  return 
endif
if (.not.exist(&1%EXTREMA)) then
  Message E VIEW "Variable &1 has no assigned extrema"
  let mapping_error yes
  return 
endif
!
let header_name_type "(&1)" /Upper
symbol HAS_CHANGED NO
!
! Extract the appropriate sub-cube if the Beam is 4-D
if (&1%NDIM.eq.4) then
  !
  ! Use the FIELD or PLANE argument...
  if (&1%UNIT4.eq."FIELD") then   ! This is the Dirty Beam layout
    if ("&2+".eq."FIELD+") then
      !
      ! Could be a direct subset here - Use a copy so far
      define image MyBeam['&1%dim[1]','&1%dim[2]','&1%dim[3]'] * REAL /global
      let MyBeam% &1%
      Let MyBeam &1[&3] 
    else
      ! Must use a Transposition
      define image MyBeam['&1%dim[1]','&1%dim[2]','&1%dim[4]'] * REAL /global
      let MyBeam% &1%
      let MyBeam%convert[3] &1%convert[4]
      Let MyBeam &1[,,&3,] 
    endif
  else if (&1%UNIT3.eq."FIELD") then  ! This a Primary Beam layout
    if ("&2+".eq."PLANE+") then
      !
      ! Could be a direct subset here - Use a copy so far
      define image MyBeam['&1%dim[1]','&1%dim[2]','&1%dim[3]'] * REAL /global
      let MyBeam% &1%
      Let MyBeam &1[&3] 
    else
      ! Must use a Transposition
      define image MyBeam['&1%dim[1]','&1%dim[2]','&1%dim[4]'] * REAL /global
      let MyBeam% &1%
      let MyBeam%convert[3] &1%convert[4]
      Let MyBeam &1[,,&3,] 
    endif
  else
    SAY "Missing PLANE or FIELD Keyword"
    return base
  endif
  !
  @ p_scale  MyBeam
  @ p_level  MyBeam
  @ p_view_sub MyBeam
  delete /var MyBeam
  symbol drawn TOTO ! Make sure we restart properly
  return
  !
endif
!
@ p_scale  &1       ! Scaling
@ p_level  &1       ! Contour levels
@ p_view_sub &1     ! Effective drawing
!
symbol drawn &1%
