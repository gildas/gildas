!
! Version working directly on MASK, through a Writeable Alias WRITE_MASK
if (exist(gosupport)) then
  !
  ! If definitions done, just execute the script
  @ mask-main &1 &2 &3 &4 &5 &6 &7 &8 &9
  return
endif
!
! First time definitions and script creation
if .not.exist(gosupport) then
  define structure gosupport /global
  ! Only used for direct calls to make-main, not documented so far
  define double gosupport%center[2] gosupport%major gosupport%minor gosupport%pa /global
  ! Current Mask plane and Channel range
  define integer gosupport%plane gosupport%cfirst gosupport%clast /global
  ! First & Last values  outside of the MASK command to be restored at end
  define integer gosupport%ofirst gosupport%olast /global
  define logical gosupport%worked /global      ! Has the Mask been filled ?
  define logical gosupport%aborted /global  ! Unclear role
endif
!
! First time, Create all procedures only once
!! begin procedure mask-visualize   MASK SHOW
!! begin procedure mask-read        MASK READ
!! begin procedure mask-errorinput  error message, normally not used
!! begin procedure mask-help        HELP Button or ? cursor
!! begin procedure mask-edit        Modify the mask
!! begin procedure mask-apply       Apply the support to the mask
!! begin procedure mask-interactive Enter interactive mode
!! begin procedure mask-interactive-action    Realize action from button
!! begin procedure mask-interface   Draw the control panel
!! begin procedure mask-plot        Plot the edge of current mask plane
!! begin procedure mask-drawfig     Draw the figure to be added / removed
!! begin procedure mask-init        Initialize the Mask
!! begin procedure mask-cleanup     Remove the control panel
!! begin procedure mask-main        Main entry point
!
begin procedure mask-visualize
  SAY "MASK Visualize"
  ! Show the MASK
  let do_header yes
  SHOW mask 'gosupport%ofirst' 'gosupport%olast'
  !
  ! Re-display the buttons...
  @ button-reset
  @ button-init 9.8 5.0 3.0 0.5 "back"  "Back" 3
  @ button-init 9.8 4.2 3.0 0.5 "exit"  "Exit" 1
  for /while yes
    @ button-click
    if (buttons%clicked.eq.0.0.or.buttons%clicked.eq.buttons%ids%exit) then
      let aborted yes
      break
    else if (buttons%clicked.eq.buttons%ids%back) then
      break
    endif
  next
  let do_header no
end procedure mask-visualize
!
begin procedure mask-read
  ! MASK READ File.msk
  !
  ! Use intermediate String to ask for filename if no argument
  define character file*256
  let file &1
  clean\read mask 'file'
end procedure mask-read
!
begin procedure mask-errorinput
  ! Same as .map version
  define char*10 mode
  let mode &1
  message e mask "Wrong usage of mode: "'mode'
  message i mask "Correct usage:"
  message i mask "MASK "'mode'" rectangle|ellipse cx cy major minor pa"
  message i mask "MASK "'mode'" circle cx cy diameter"
  message i mask "MASK "'mode'" polygon  [file]"
end procedure mask-errorinput
!
begin procedure mask-help-inter
  say "    MASK INTERACTIVE"
  say "    "
  say "    Interactive Mask definition"
  say "    "
  say "    Use the Right buttons in the graphic display to perform the desired action"
  say "             Include plane will consider the whole plane in the mask"
  say "             Exclude plane will ignore the whole plane frim the mask"
  say "             Visualize will show the mask"
  say "    + (blue) buttons will add the insided of the corresponding"
  say "             figure to the mask"
  say "    - (red) buttons will remove the inside of the corresponding"
  say "             figure from the mask"
  say " "
  say "     Save and Exit will return to Command line"
end procedure mask-help-inter
!
begin procedure mask-help
  say "    MASK"
  say "    "
  say "    Define a mask for further use (Masking, Cleaning, etc...)"
  say "    "
  say "    The mask can be defined through an interactive session:"
  say "    MASK"
  say "    "
  say "    Or created through the command line:"
  say "    MASK INIT 2D|3D                         Initializes a mask"
  say "    MASK READ                               Reads mask from disk"
  say "    MASK WRITE                              Writes mask to disk"
  say "    MASK ADD Circle|Ellipse|Rectangle|Polygon     Adds figure to mask"
  say "    MASK REMOVE Circle|Ellipse|Rectangle|Polygon  Removes figure from mask"
  say "    MASK [HELP|?]                           Shows this help"
  say "    "
  say "    MASK ADD|REMOVE ELLIPSE|RECTANGLE cx cy maj min pa"
  say "    MASK ADD|REMOVE CIRCLE cx cy diameter"
  say "    MASK ADD|REMOVE POLYGON [File]"
  say "    "
  say "    cx, cy: offset of figure center in arcseconds"
  say "    maj:    long  side|major axis of rectangle|ellipse in arcseconds"
  say "    min:    short side|minor axis of the rectangle|ellipse in arcseconds"
  say "    pa:     angle between the long side|major axis and the north in degrees"
  say "    "
  say "    To inspect a defined mask type:"
  say "    MASK SHOW"
  say "    To overlay it over the clean image type:"
  say "    MASK OVER"
  say "    "
end procedure mask-help
!
begin procedure mask-edit
  SAY "MASK Edit"
  !
  ! @ mask-edit PLOT MODE FIGURE [params ...]
  !     PLOT    YES or NO
  !     MODE    ADD or RM   Add or remove in current Mask
  !     FIGURE  Type of figure to be added or removed
  !     Params  Figure parameters (figure dependent)
  !
  if (.not.exist(mask)) then
    message e mask "There is no current Mask to edit"
    return
  endif
  !
  define integer fsave lsave
  define char*10 figure mode
  define logical invalid show
  let show &1
  !
  let figure &3
  let invalid (figure.ne."polygon".and.figure.ne."rectangle")
  let invalid invalid.and.(figure.ne."ellipse").and.(figure.ne."circle")
  if (invalid) then
    message e mask "Unknow figure: "'figure'
    @ mask-errorinput 'mode'
    return
  endif
  let mode &2
  !
  let fsave first
  let lsave last
  !!SAY "Mask Edit First "'first'" Last "'last'
  !
  ! Show the Integrated area or Single plane
  if (show) then
    if ('last_shown'%dim[3].eq.1) then
      SHOW 'last_shown'
    else
      ! Compute the Mean over the specified range
      define integer jf jl
      let jf max(1,first)
      let jf min(jf,'last_shown'%dim[3])
      if (last.le.0) then
        let jl max(jf,'last_shown'%dim[3]-last)
      else
        let jl min(last,'last_shown'%dim[3])
      endif
      define image m_mean * real /like 'last_shown'[1] /global
      let m_mean% 'last_shown'%
      compute m_mean sum 'last_shown'[jf:jl]
      compute m_mean%min min m_mean
      compute m_mean%max max m_mean
      SHOW m_mean 1 1
      !!PAUSE " exa first "'first'" last "'last'
      delete /var m_mean
    endif
  endif
  !
  if (figure.eq."polygon") then
    greg2\polygon &4
    if (show) poly /plot
  else if (figure.eq."circle") then
    @ p_poly 'show' ellipse  &4 &5 &6 &6 0
  else
    @ p_poly 'show' 'figure' &4 &5 &6 &7 &8
  endif
  let first fsave
  let last lsave
  @ mask-apply 'mode'
end procedure mask-edit
!
begin procedure mask-apply
  SAY "MASK &1"
  ! @ mask-apply add|rm
  !
  ! Add or remove current polygon in current Mask
  !
  define integer fplane lplane
  define real val
  define char*10 mode
  !
  let fplane gosupport%cfirst
  let lplane gosupport%clast
  let mode &1
  if (fplane.eq.0) let fplane 1
  if (lplane.eq.0) let lplane mask%dim[3]
  if (mode.eq."add") then
    let val 1.0
  else
    let val 0.0
  end if
  !
  if mask%dim[3].gt.1 then
    for ip fplane to lplane
      greg2\rgdata mask[,,ip] /var
      greg2\mask in /blank val 0.0
      let write_mask[,,ip] rg[,]
    next ip
  else
    !
    greg2\rgdata mask /var
    greg2\mask in /blank val 0.0
    ! Debug section, inactive
    if (NO) then
      define real a b
      compute a min mask
      compute b max mask
      say "MASK min "'a'" Max "'b'" Val "'val'
      compute a min rg
      compute b max rg
      say "RG min "'a'" Max "'b'
    endif
    let write_mask rg
  endif
end procedure mask-apply
!
begin procedure mask-interactive
  SAY "MASK Interactive: Step &1"
  ! Initialization
  let do_header no
  let do_contour no
  define integer step fsave lsave ip prevp nplanes
  let nplanes mask%dim[3]
  define logical carry port waszero
  if (.not.exist(aborted)) then
    define logical aborted mvback stay /global
  end if
  !
  !
  let aborted no
  let carry yes
  let step &1
  let fsave gosupport%ofirst
  let lsave gosupport%olast
  if (fsave.eq.0) let fsave 1
  if (lsave.eq.0) let lsave mask%dim[3]
  let ip fsave
  !
  @ button-tools
  ! Main loop over planes
  for /while ip.le.lsave ! fsave to lsave by step
    let mvback no
    let stay no
    @ mask-interactive-action 'ip' 'step'
    ! handle subroutine output
    if (aborted) return
    let prevp ip
    if (mvback) then
      let ip ip-step
      if (ip.lt.fsave) then
        message w mask "First channel reached"
        let ip fsave
      endif
    else if (stay) then
      continue
    else
      let ip ip+step
      if (ip.gt.lsave) then
        message w mask "Last channel reached"
        let ip lsave
      endif
    endif
    let port carry.and.ip.ne.prevp.and..not.gosupport%worked[ip]
    if (port) then
      let write_mask[,,ip] write_mask[,,prevp]
    endif
  next
  let do_header yes
  let do_contour yes
  delete /var mvback aborted stay
  @ button-delete
end procedure mask-interactive
!
begin procedure mask-interactive-action
  SAY "MASK Action First &1 Step &2"
  ! Dispatch the action according to which button was clicked
  !
  ! Initialization
  sic message greg -iw
  sic message sic -i
  define integer hfirst hlast sizex sizey
  define logical goon
  !
  let gosupport%plane &1  ! Remember current plane
  !
  let goon yes
  let hfirst &1
  let hlast &1+&2-1
  if (hlast.gt.mask%dim[3]) let hlast mask%dim[3]
  !
  let gosupport%cfirst hfirst
  let gosupport%clast  hlast
  !
  ! Plotting image
  if exist(l_area) delete /var l_area
  define image l_area * real /like 'last_shown'[1] /global
  let l_area% 'last_shown'%
  if (l_area%blank[2].lt.0) then
    if (mask%dim[3].eq.1) then
      let nnn = 'last_shown'%dim[3] /new integer
      compute l_area mean 'last_shown'[1:nnn]
      dele /var nnn
    else
      compute l_area mean 'last_shown'[hfirst:hlast]
    endif
    compute l_area%min min l_area
    compute l_area%max max l_area
  else
    let l_area 0
    if (mask%dim[3].eq.1) then
      let nnn = 'last_shown'%dim[3] /new integer
      compute l_area mean 'last_shown'[1:nnn] /blank l_area%blank[1] l_area%blank[2]
      dele /var nnn
    else
      compute l_area mean 'last_shown'[hfirst:hlast] /blank l_area%blank[1] l_area%blank[2]
    endif
    compute l_area%min min l_area  /blank l_area%blank[1] l_area%blank[2]
    compute l_area%max max l_area  /blank l_area%blank[1] l_area%blank[2]
  endif
  !
  for /while goon
    SHOW l_area 1 1   ! There is always only 1 plane in L_AREA
    @ mask-plot 'hfirst' 'hlast'
    greg1\draw text 0 -1 'hfirst'" "'hlast' /box 8
    @ mask-interface
    @ button-click
    if (buttons%clicked.eq.0.or.buttons%clicked.eq.buttons%ids%exit) then
      let aborted yes
      return
!    else if (buttons%clicked.eq.buttons%ids%abort) then
!      message w mask "Abort requested by user"
!      let gosupport%aborted yes
!      let aborted yes
!      return
    else if (buttons%clicked.eq.buttons%ids%help) then
      @ mask-help-inter
    else if (buttons%clicked.eq.buttons%ids%prect) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig add rectangle
    else if (buttons%clicked.eq.buttons%ids%pelli) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig add ellipse
    else if (buttons%clicked.eq.buttons%ids%pcirc) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig add circle
    else if (buttons%clicked.eq.buttons%ids%ppoly) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig add polygon
    else if (buttons%clicked.eq.buttons%ids%mrect) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig rm rectangle
    else if (buttons%clicked.eq.buttons%ids%melli) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig rm ellipse
    else if (buttons%clicked.eq.buttons%ids%mcirc) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig rm circle
    else if (buttons%clicked.eq.buttons%ids%mpoly) then
      SHOW l_area !! @ p_lmv.greg
      @ mask-plot 'hfirst' 'hlast'
      @ mask-drawfig rm polygon
    else if (buttons%clicked.eq.buttons%ids%visu) then
      @ mask-visualize
      let goon no
      let stay yes
    else if (buttons%clicked.eq.buttons%ids%excl) then
      let write_mask[,,hfirst:hlast] 0.0   ! Simpler replacement
    else if (buttons%clicked.eq.buttons%ids%incl) then
      ! What is that supposed to do ??
      let sizex mask%dim[1]
      let sizey mask%dim[2]
      for ip 'hfirst' to 'hlast'
        let write_mask[,,ip] 1.0
        let write_mask[1,,ip] 0.0
        let write_mask[sizex,,ip] 0.0
        let write_mask[,sizey,ip] 0.0
        let write_mask[,1,ip] 0.0
      next ip
      PAUSE
    else
      if (exist(buttons%ids%next)) then
        if (buttons%clicked.eq.buttons%ids%next) then
          message i mask "Moving to next group of planes"
          let goon no
        endif
      endif
      if (exist(buttons%ids%prev)) then
        if (buttons%clicked.eq.buttons%ids%prev) then
          message i mask "Moving to previous group of planes"
          let mvback yes
          let goon no
        endif
      endif
    endif
    let gosupport%worked[hfirst:hlast] yes      ! Simpler replacement
  next
  sic message greg +iw
  sic message sic +i
end procedure mask-interactive-action
!
begin procedure mask-interface
  !!SAY "MASK Interface"
  ! Create the interface to control the User actions
  ! as pseudo-buttons (areas in which the user can click in the plot)
  !
  ! as in .map version
  define real bwidth bheight vspace col addpos mainbot maintop rmpos
  define integer addcor rmcor maincor il
  let addcor 3
  let rmcor 1
  let maincor 0
  let bwidth 3.0
  let bheight 0.5
  let vspace 0.8
  let col 9.8
  let mainbot 1.25
  !
  let addpos 7.75  ! For 4 buttons, 7.25 for 3
  let rmpos  4.5   ! For 4 buttons, 4.25 for 3
  let maintop 10.25
  @ button-reset
  !
  ! Previous and Next buttons appear only if needed
  if gosupport%olast.gt.0 then
    il = min(mask%dim[3],gosupport%olast)
  else
    il = max(1,mask%dim[3]+gosupport%olast)
  endif
  if (gosupport%plane.lt.il) then
    @ button-init 'col' 'mainbot'          'bwidth' 'bheight' "next"  "Next"           'maincor'
!  else
!    SAY " Plane "'gosupport%plane'" DIM "'mask%dim[3]'" Last "'il'
  endif
  il = max(1,gosupport%ofirst)
  if (gosupport%plane.gt.il) then
    @ button-init 'col' 'mainbot-vspace'   'bwidth' 'bheight' "prev"  "Previous"       'maincor'
!  else
!    SAY " Plane "'gosupport%plane'" DIM "'mask%dim[3]'" First "'il'
  endif
  !
  @ button-init 'col' 'mainbot-2*vspace' 'bwidth' 'bheight' "exit"  "Save_and_Exit"  'maincor'
  ! Save is automatic
  !! @ button-init 'col' 'mainbot-3*vspace' 'bwidth' 'bheight' "abort" "Exit_w/o_save"  'maincor'
  ! Main interface buttons
  @ button-init 'col' 'maintop'          'bwidth' 'bheight' "incl"  "Include_plane"  'maincor'
  @ button-init 'col' 'maintop-vspace'   'bwidth' 'bheight' "excl"  "Exclude_plane"  'maincor'
  @ button-init 'col' 'maintop-2*vspace' 'bwidth' 'bheight' "visu"  "Visualize_mask" 'maincor'
  ! Add buttons
  @ button-init 'col' 'addpos'           'bwidth' 'bheight' "prect" "+_Rectangle"    'addcor'
  @ button-init 'col' 'addpos-vspace'    'bwidth' 'bheight' "pelli" "+_Ellipse"      'addcor'
  @ button-init 'col' 'addpos-2*vspace'  'bwidth' 'bheight' "ppoly" "+_Polygon"      'addcor'
  @ button-init 'col' 'addpos-3*vspace'  'bwidth' 'bheight' "pcirc" "+_Circle"       'addcor'
  ! Remove buttons
  @ button-init 'col' 'rmpos'            'bwidth' 'bheight' "mrect" "-_Rectangle"    'rmcor'
  @ button-init 'col' 'rmpos-vspace'     'bwidth' 'bheight' "melli" "-_Ellipse"      'rmcor'
  @ button-init 'col' 'rmpos-2*vspace'   'bwidth' 'bheight' "mpoly" "-_Polygon"      'rmcor'
  @ button-init 'col' 'rmpos-3*vspace'   'bwidth' 'bheight' "mcirc" "-_Circle"       'rmcor'
end procedure mask-interface
!
begin procedure mask-plot
  SAY "MASK Plot &1 to &2"
  greg1\pencil 8 /colo negative
  greg1\pencil 0
  !
  ! Plot the current Mask - optimized for IMAGER
  define image tmp_mask * real /like mask[,,1]
  let tmp_mask% mask%
  compute tmp_mask sum mask[&1:&2]
  greg2\rgdata tmp_mask /var
  greg2\levels  0.9
  greg2\rgmap quiet /pen 8
end procedure mask-plot
!
begin procedure mask-drawfig
  SAY "MASK drawfig"
  ! @ mask-drawfig Add|Rm Figure [Parameters]
  ! Draw the current figure
  define real click1[2] click2[2] click3[2] cx cy dx dy delx dely
  define double pa
  define char*20 figure mode
  let mode &1
  let figure &2
  greg1\pencil /w 5
  if (mode.eq."add") then
    greg1\draw text 1 2 "Adding "'figure'":" /box 6
  else
    greg1\draw text 1 2 "Removing "'figure'":" /box 6
  endif
  if (figure.eq."polygon") then
    greg1\draw text 1 0 "1- Left click to" /box 6
    greg1\draw text 1 -0.5 "add corners" /box 6
    greg1\draw text 1 -1.5 "2- Right click to" /box 6
    greg1\draw text 1 -2.0 "finish" /box 6
    greg1\pencil /def
  else if (figure.eq."circle") then
    greg1\draw text 1 0 "1- Click on center" /box 6
    greg1\draw relo
    let click1 use_curs/sec
    greg1\draw text 1 -1.5 "2- Click on radius" /box 6
    greg1\draw relo
    greg1\pencil /def
    let click2 use_curs/sec
    let cx click1[1]
    let cy click1[2]
    let dx 2*sqrt((click2[1]-cx)**2+(click2[2]-cy)**2)
    !
    let dy dx
    let pa 0
    let figure "ellipse"
  else if (figure.eq."rectangle") then
    greg1\draw text 1 0 "1- Click on bottom" /box 6
    greg1\draw text 1 -0.5 "left corner" /box 6
    greg1\draw relo
    let click1 use_curs/sec
    greg1\draw text 1 -1.5 "2- Click on top" /box 6
    greg1\draw text 1 -2.0 "right corner" /box 6
    greg1\draw relo
    greg1\pencil /def
    let click2 use_curs/sec
    let cx (click1[1]+click2[1])/2
    let cy (click1[2]+click2[2])/2
    let dy abs(click2[1]-click1[1])
    let dx abs(click2[2]-click1[2])
  else if (figure.eq."ellipse") then
    greg1\draw text 1  0.0 "1- Click to define " /box 6
    greg1\draw text 1 -0.5 "one of the edges of" /box 6
    greg1\draw text 1 -1.0 "the ellipse" /box 6
    greg1\draw relo
    let click1 use_curs/sec
    greg1\draw text 1 -2.0 "2- Click to define " /box 6
    greg1\draw text 1 -2.5 "the other edge of" /box 6
    greg1\draw text 1 -3.0 "the ellipse" /box 6
    greg1\draw relo 'click1[1]*sec' 'click1[2]*sec' /u
    greg1\pencil /w 3
    greg1\draw line
    greg1\pencil /w 5
    let click2 use_curs/sec
    greg1\draw text 1 -3.5 "3- Click to define " /box 6
    greg1\draw text 1 -4.0 "the semi-minor axis" /box 6
    greg1\draw text 1 -4.5 "of the ellipse" /box 6
    greg1\draw relo
    let click3 use_curs/sec
    greg1\pencil /def
    let cx (click1[1]+click2[1])/2 ! centers
    let cy (click1[2]+click2[2])/2
    let delx click2[1]-click1[1]
    let dely click2[2]-click1[2]
    ! major axis => distance between cl1 and cl2
    let dx sqrt(delx**2+dely**2)
    ! minor axis => twice distance between cl3 and the line between cl1 and cl2
    let dy dely*click3[1]-delx*click3[2]+click2[1]*click1[2]-click2[2]*click1[1]
    let dy 2*abs(dy)/dx
    if (dely.gt.0) then
      let pa delx/dx
    else
      let pa -(delx)/dx
    endif
    ! pa from deltax/major axis
    let pa asin(pa)*180/pi
  else
    Message E MASK "Unsupported figure "'figure'
  end if
  !
  if (figure.ne."polygon") then
    @ mask-edit NO 'mode' 'figure' 'cx' 'cy' 'dx' 'dy' 'pa'
  else
    @ mask-edit NO 'mode' 'figure' 
  endif
end procedure mask-drawfig
!
begin procedure mask-init
  ! @ mask-init Yes|No
  !
  ! Set the initialisation status of each Mask plane
  if exist(gosupport%worked) delete /var gosupport%worked
  define logical gosupport%worked['mask%dim[3]'] /global
  let gosupport%worked &1   ! yes or No
end procedure mask-init
!
begin procedure mask-cleanup
  SAY MASK Cleanup
  ! Reset all buttons
  @ button-delete
end procedure mask-cleanup
!
! Main program
begin procedure mask-main
  !
  ! Dispatch the command
  define character*16 mode
  define character*256 cmname
  let gosupport%plane first
  let gosupport%ofirst first
  let gosupport%olast last
  let gosupport%aborted no
  if (pro%narg.eq.0) then
    let mode "interactive"   ! Default behaviour
  else
    let mode &1 /lower
  endif
  if (mode.eq."?".or.mode.eq."help") then
    @ mask-help
    return
  else if (mode.eq."init") then
    @ mask-init &2
    return
  end if
  !
  ! That may be changed to the currently Displayed image
  if .not.exist('last_shown') then
    message e mask "No "'last_shown'" image, impossible to proceed"
    return
  end if
  !
  ! A trick is required to access the Mask in Write status
  define alias write_mask mask /global
  let write_mask /status write
  !
  define logical old_wedge
  let old_wedge do_wedge
  let do_wedge no
  !
  if (mode.eq."init") then
    @ mask-init &2
  else if (mode.eq."write") then
    say "Type SHOW MASK or VIEW MASK to inspect it"
  else if (mode.eq."read") then
    @ mask-read
    @ mask-init yes
  else if (mode.eq."polygon") then ! From widget polygon
    @ mask-edit YES add polygon
    @ mask-cleanup
  else if (mode.eq."rectangle") then
    @ mask-edit YES add rectangle 'gosupport%center[1]' 'gosupport%center[2]' 'gosupport%major' 'gosupport%minor' 'gosupport%pa'
    @ mask-cleanup
  else if (mode.eq."ellipse") then
    @ mask-edit YES add ellipse 'gosupport%center[1]' 'gosupport%center[2]' 'gosupport%major' 'gosupport%minor' 'gosupport%pa'
    @ mask-cleanup
  else if (mode.eq."visualize") then
    SHOW mask 'first' 'last'
  else if (mode.eq."over") then
    ! It works ...
    let nimages 2
    let nreference 1
    let names[1] CLEAN
    let names[2] MASK
    let spacings[1:2] 'spacing[1]' 0.5  ! There may be an issue with the use of 'spacing[1]' in case of units
    let types "%"
    @ p_over
  else if (mode.eq."add".or.mode.eq."rm") then
    if (pro%narg.eq.5).or.(pro%narg.eq.7) then
      @ mask-edit NO &1 &2 &3 &4 &5 &6 &7
    else if (pro%narg.eq.2).or.(pro%narg.eq.3) then
      @ mask-edit NO &1 &2 &3
    else
      message e MASK "Incorrect number of inputs. "'pro%narg'" "'pro%arg'
      @ mask-errorinput 'mode'
    end if
  else if (mode.eq."interactive") then
    ! @ mask-main interactive [Step]
    if (pro%narg.eq.2) then
      @ mask-interactive &2
    else
      @ mask-interactive 1
    end if
    @ mask-cleanup
  else if (mode.eq."?".or.mode.eq."help") then
    @ mask-help
  else
    message e mask "Unrecognized mode, nothing done"
  endif
  !
  ! Restore SHOW basics (First, Last, Header, Contour and Wedge)
  let first gosupport%ofirst
  let last gosupport%olast
  let do_header yes
  let do_contour yes
  let do_wedge old_wedge
  !
  delete /var write_mask
end procedure mask-main
!
! First pass: initialize and loop
message I MASK "Version 1.2 -- Working for 2D and 3D, any step"
!
@ mask-main &1 &2 &3 &4 &5 &6 &7 &8 &9
