!------------------------------------------------------------------------
! P_COMPOSITE.IMA
!   Plot 3 panel moments + Integrated spectrum
! S.Guilloteau 28-Nov-2018
!------------------------------------------------------------------------
if (.not.exist(m_area)) then
  message e "SHOW COMPOSITE" "No moments in memory"
  return
endif
if (.not.exist(flux)) then
  message e "SHOW COMPOSITE" "No Flux in memory"
  return
endif
!
gtvl\destroy all
def real x2 dx y2 aamin aamax 
def double xmark ratio
def integer m n n1 n2 imark nmap nadd
def character ctext*10[4] cmark*32[4] amark*10 save_type*12
def character save_spacing*16[2]
!
let save_spacing 'spacing[1]' 'spacing[2]'
let spacing 'area_spacing'
! let save_scale scale
!
sic\@ p_scale m_area       ! Compute scaling
sic\@ p_level m_area       ! Compute levels
sic\@ p_header m_area      ! Draw header
sic\if do_color then
  sic\@ p_color            ! Setup nice colour table
endif
!
define real hbox_x[2] hbox_y[2]
let hbox_x box_xmax page_x-0.1
let hbox_y box_ymin page_y-0.1
!
greg2\rgdata m_area /variable
if (m_area%angle.ne.0) then
  symbol option "/UNIT SEC"
else
  symbol option "/ABSOLUTE"
endif
if size[1].eq.0 then
  greg1\limits /rg
else if (size[1].gt.0) then
  greg1\limits size[1]|2 -size[1]|2 -size[1]|2 size[1]|2 seconds
endif
if (aspect.eq.0) then
  let ratio abs((user_xmax-user_xmin)|(user_ymax-user_ymin))
else
  let ratio aspect
endif
let box_aspect ratio
let x2 box_xmax
let y2 box_ymax
let m 2
let n 2
greg1\set expand 1.0
let dx (x2-box_xmin)|n|ratio
let dx min((y2-box_ymin)|m,dx)
!
delete /variable mbox nbox ubox
define integer mbox nbox /global
define integer k
define char unit*12
let k 0
let mbox m
let nbox n
define real ubox[5,nbox,mbox] /global
define char dirnam*12[4]
let dirnam M_AREA M_FLUX M_VELO M_WIDTH
on error break
for j 1 to m
  for i 1 to n
    let k k+1
    gtvl\change directory <greg
    gtvl\create directory 'dirnam[k]'
    gtvl\change directory 'dirnam[k]'
    !
    let ubox[1,i,j] x2+(i-n-1)*dx*ratio
    let ubox[2,i,j] x2+(i-n)*dx*ratio
    let ubox[3,i,j] y2-j*dx
    let ubox[4,i,j] y2+(1-j)*dx
    greg1\set box x2+(i-n-1)*dx*ratio x2+(i-n)*dx*ratio y2-j*dx y2+(1-j)*dx
    if (k.eq.1) then
      let amark "Intensity "
      let unit 'm_area%unit'
    else if (k.eq.2) then
      break
    else if (k.eq.3) then
      let spacing velo_spacing
      sic\@ p_scale m_velo               ! Compute scaling
      sic\@ p_level m_velo               ! Compute levels
      let amark "Velocity "
      greg2\rgdata m_velo /variable
      let unit 'm_velo%unit'
    else if (k.eq.4) then
      let spacing width_spacing
      sic\@ p_scale m_width               ! Compute scaling
      sic\@ p_level m_width               ! Compute levels
      let amark "Width "
      greg2\rgdata m_width /variable
      let unit 'm_width%unit'
    endif
    let cmark[k] 'level_min'" to "'level_max'" by "'true_spacing'" "'unit'
    let ctext[k] 'amark'
    let ubox[5,i,j] 1.0*k
    if (do_bit) then          ! Plot the RGDATA array...
      greg2\plot /scaling linear true_scale[1] true_scale[2]
    endif
    if (do_grey) then
      greg2\rgmap quiet /abs 1.0 /grey
    endif
    if (do_contour) then
      greg2\rgmap quiet /abs 1.0
    endif
    if (cross.ne.0) then
      greg1\draw relo 0 cross*sec /user
      greg1\draw line 0 -cross*sec /user
      greg1\draw relo cross*sec 0 /user
      greg1\draw line -cross*sec 0 /user
    endif
    if (do_nice) @ p_beam m_area
    if (i.eq.1).and.(j.eq.m) then
      greg1\box 'option'
    else
      greg1\box n n 'option'
    endif
    if (exist(do_mask)) then
      if (do_mask) @ p_hide 7 0.30 0.12
      endif
    endif
    !
    if (exist(flux%nxy)) then
      greg1\connect flux%x flux%y
      greg1\draw line flux%x[1] flux%y[1] /user
      greg1\set coord box
      greg1\draw text 0.4 -0.3 'amark' 3 /box 7
    endif
  next
next
!
! Here this is for the FLUX display
greg1\set expand 0.8
define real x_margin y_margin
let x_margin 2
let y_margin 1.5
gtvl\change directory <greg
gtvl\change directory 'dirnam[2]'
greg1\set box x2+(2-n-1)*dx*ratio x2+(2-n)*dx*ratio y2-dx y2
greg1\set box box_xmin+x_margin box_xmax-0.2 box_ymin+y_margin box_ymax
greg1\limits /var flux%velocities flux%values[1]
for i 1 to flux%nf
  limi = = < > /var flux%velocities flux%values[i]
next
greg1\box
greg1\draw text 0 -2 "Velocity (km/s)" 5 0 /char 2
greg1\draw text -3 0 "Flux (Jy)" 5 90 /char 4
for i 1 to flux%nf
  greg1\pencil i-1
  greg1\histogram flux%velocities flux%values[i]
next
greg1\pencil 0
!
greg1\set expand 0.8
gtvl\change directory <greg
greg1\set box hbox_x[1] hbox_x[2] hbox_y[1] hbox_y[2]
let k 0
for i 1 3 4
  let k k+1
  greg1\draw text .5 -8-1.8*k "\\i"'ctext[i]'" levels" 6 /box 7
  greg1\draw text .5 -8.8-1.8*k 'cmark[i]' 6 /box 7
next
!
greg1\set expand 1.0
!
let spacing 'save_spacing[1]' 'save_spacing[2]'
!
