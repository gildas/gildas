!
defin log already_done
if (.not.exist(mom)) then
  define structure mom /global
  define char mom%method*12 mom%type*12 /global
  define real mom%minmax[2] mom%thre /global
  let mom%type "CHANNEL"
  let mom%method "PARABOLA"
else
  let already_done yes
endif
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
gui\panel "Image Analysis control panel" gag_scratch:x_analyze.hlp 
!
gui\button "@ p_flux" FLUX "Flux measurement" gag_scratch:p_flux.hlp "FLUX parameters"
!
gui\button "@ p_noise" NOISE "Noise measurement" gag_scratch:p_noise.hlp "NOISE parameters"
  let first 'first' /prompt "First channel"
  let last 'last' /prompt "Last channel"
!
gui\button "@ p_moments" MOMENTS "Moment images" gag_scratch:p_moment.hlp "MOMENTS parameters"
  let mom%method 'mom%method' /choice MEAN PEAK PARABOLA
  let mom%minmax 'mom%minmax[1]' 'mom%minmax[2]' /prompt "Range"
  let mom%type  'mom%type' /choice CHANNEL FREQUENCY VELOCITY
  let mom%thre  'mom%thre' /prompt "Detection threshold"
!
gui\button "SHOW MOMENTS" "SHOW MOMENTS"  "Display moments" gag_scratch:p_velocity.hlp "SHOW MOMENTS parameters"
  let area_spacing 'area_spacing' /prompt "Contour spacing for Area"
  let peak_spacing 'peak_spacing' /prompt "Contour spacing for Peak"
  let velo_spacing 'velo_spacing' /prompt "Contour spacing for Velocity"
  let width_spacing 'width_spacing' /prompt "Contour spacing for Width"
  say 
  let size 'size[1]' 'size[2]' /prompt "Size of displayed area"
  let cross 'cross' /prompt "Size of cross at center"
!
on error return
gui\go
!
if (already_done) then
  return
endif
!
begin procedure p_moments
  if (any(mom%minmax.ne.0)) then
    moments /method 'mom%method' /range 'mom%minmax[1]' 'mom%minmax[2]' 'mom%type' /threshold 'mom%thre'
  else
    moments /method 'mom%method' /threshold 'mom%thre'
  endif
end procedure
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
begin data gag_scratch:p_moments.init
!
! Moment.init
TASK\FILE "Input file name" IN$ 'name'.'type'
TASK\FILE "Output files name (no extension)" OUT$ 'name'
TASK\REAL "Velocity range" VELOCITY$[2] mom%velocity[1] mom%velocity[2]
TASK\REAL "Detection threshold" THRESHOLD$ mom%threshold
TASK\LOGICAL "Smooth before detetction ?" SMOOTH$ mom%smooth
TASK\GO
end data gag_scratch:p_moments.init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Panel for Image Analysis actions
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin data gag_scratch:p_noise.hlp
1 GO NOISE

Compute noise statistics on an image or cube. We build an histogram of the
image pixels for each plane, by counting the number of pixels as a function
of intensity. The a Gaussian is fit to this histogram. The Gaussian width
is the noise level.

True signal will appear as outliers of the intensity distribution, and 
left-over sidelobes as negative non Gaussian wings.  

Dynamic range effects will in general be incorporated in the noise estimate,
but may also appear as non Gaussian tails.

end data gag_scratch:p_noise.hlp
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin data gag_scratch:p_moment.hlp
1 GO MOMENT

  Compute the first 3 moments of a data cube along the third axis (presumably  
  the data cube is a Position-Positin-Velocity cube).

2 NAME
  Input file name. This is also used as output file name, with extensions
  .mean  .velo .width respectively

2 TYPE
  Input file type.

2 MOM%VELOCITY[2]

  Velocity range over which the moments should be computed

2 MOM%THRESHOLD
 
  Minimum intensity to select a pixel.

2 MOM%SMOOTH
 
  Apply a smoothing in velocity to test against the threshold.
end data gag_scratch:p_moment.hlp
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin data gag_scratch:p_velocity.hlp
1 GO VELOCITY
  VELOCITY will plot a contour map of
    - Integrated intensity
    - Mean velocity
    - Line width
   (Summary header information is also plotted)
  Map name is controlled by character variable NAME
  A beam may be plotted (as one extra channel map), controlled by variable 
	BEAM_TYPE                  [NONE, CLEAN or DIRTY]
  Size of square area plotted is controlled by variable SIZE
       [>0 == limits are SIZE/2 -SIZE/2 -SIZE/2 SIZE/2 SECONDS]
       [0 == whole map  ;    <0 == manually use LIMITS command]
  Spacing of contour levels is controlled by variables
       AREA_SPACING,    VELO_SPACING,  WIDTH_SPACING
  for Integrated area, Velocity,      Line Width respectively
       [>0 ==> levels equally spaced by the value of SPACING]
       [0 ==> automatic determination; <0 ==> manually use LEVEL command]
  CROSS controls the size of a cross at phase center

2 NAME
  Generic file name
2 BEAM_TYPE
  Extra beam to be displayed in the 4th panel. Can be NONE, CLEAN or DIRTY
2 SIZE[2]
  Size (in arcsec) of the displayed area.
  0 means automatic guess.
2 AREA_SPACING
  Contour spacing of the integrated intensity map, in current map units 
  (usually Jy.km/s or K.km/s)
  0 means automatic guess.
2 VELO_SPACING
  Contour spacing of the mean velocity map (in km/s). 
  0 means automatic guess.
2 WIDTH_SPACING
  Contour spacing of the line width map (in km/s)
  0 means automatic guess.
2 CROSS
  Size (in arcsecond) of the cross at (0,0). No cross drawn if 0
end data gag_scratch:p_velocity.hlp
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
begin data gag_scratch:p_flux.hlp
Not yet written
end data gag_scratch:p_flux.hlp
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
begin data gag_scratch:x_analyze.hlp
Some tools for image analysis. More will come...
end data gag_scratch:x_analyze.hlp
