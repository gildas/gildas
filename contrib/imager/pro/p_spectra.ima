!------------------------------------------------------------------------
! P_SPECTRE.IMA
!
! Procedure to plot spectra in box for a spectral line data cube.
! Global variables defined in DEFINE.GRAPHIC
! Calls
! 	P_HEADER 	(to plot header information)
! 	P_LOAD    (to load an image)
!   P_BOX   	(to plot a box in the header with limits)
!
! Initial version p_spectre.greg  S.Guilloteau 18-jun-1991
!----------------------------------------------------------------------
!
let Mapping_Error YES
if (pro%narg.eq.0) then
  if .not.exist(SpeVariable) then
    Message E SPECTRA "No previous SPECTRA display, use command SHOW SPECTRA Variable"
    return
  else
    Message W SPECTRA "Re-displaying "'SpeVariable'" "
  endif
else if exist(&1) then
  if (.not.exist(&1%f_axis)) then
    Message E SPECTRA "&1 has no Frequency axis"
    return
  else if (&1%f_axis.eq.0) then
    Message E SPECTRA "&1 has no Frequency axis"
    return
  else if (&1%x_axis.eq.0) then
    Message E SPECTRA "&1 has no X coordinate axis"
    return
  else if (&1%y_axis.eq.0) then
    Message E SPECTRA "&1 has no Y coordinate axis"
    return
  endif
  if .not.exist(SpeVariable) define character*32 SpeVariable /global
  let SpeVariable &1
else
  Message E SPECTRA "No such variable &1"
  let Mapping_Error YES
  return
endif
!
let Mapping_Error NO
!
gtvl\destroy all
!
if (do_header) then
  gtvl\create dir HEADER
  gtvl\change dir HEADER
  @ p_header 'SpeVariable'
endif
on error
!
@ p_spectre_sub 'SpeVariable'
