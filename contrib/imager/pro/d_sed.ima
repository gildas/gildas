if exist(sed) return
!
define structure sed /global
define character*256 sed%filter /global
define character*16 sed%type /global
define character*12 sed%model /global
define character*64 sed%name sed%uv sed%image sed%pos /global
define double sed%freq /global
define logical sed%color /global
let sed%color YES
let sed%model E_GAUSS
let sed%image "sed-image.txt"
let sed%uv "sed-uv.txt"
let sed%pos "position.txt"
!
begin procedure flux_sed_ima
	!
	! @ flux_sed_ima  
	!		SPECTRAL_INDEX  determination
	!
	!   Inputs:
	!     a file filter             sed%filter
	!     a file type (image data)	sed%type
	!     a Mask or Support 
	!     an output filename 		sed%name
	!
	!   Steps
	!     Compute the source flux for all channels of all data sets
	!     using the FLUX command, store them in 'sed%name'
	!
	let sed%type 'sed%type' /lower
	if (sed%type.eq."uvt").or.(sed%type.eq."uvfit") then
	  message e SED 'sed%type'" extension invalid for Image mode"
	  return error  
  endif
  message w SED "Image mode under debug"
  @ find-flux-cube 'sed%filter'.'sed%type'   'sed%image'
  let sed%name 'sed%image'
end procedure
!
begin procedure flux_sed_uvfit
	!
	! @ flux_sed_uvt
	!		SPECTRAL_INDEX  determination
	!
	!   Inputs:
	!     a file filter (default *)
	!     a source model (E_GAUSS by default)
	!     an output filename 
	!     a reference frequency
	!
	!   Steps
	!      Fit the source model (UV data) or compute the source flux for 
	!      all channels of all data sets, and store them in 'sed%name'
	!
  define char*16 stype
  let stype 'sed%type'
  let sed%type uvfit
	!
	@ find-flux-uvt 'sed%filter'    'sed%name'
  let sed%type 'stype'
end procedure flux_sed_uvfit
!
begin procedure flux_sed_uvt
	!
	! @ flux_sed_uvt
	!		SPECTRAL_INDEX  determination
	!
	!   Inputs:
	!     a file filter (default *)
	!     a source model (E_GAUSS by default)
	!     an output filename 
	!     a reference frequency
	!
	!   Steps
	!      Fit the source model (UV data) or compute the source flux for 
	!      all channels of all data sets, and store them in 'sed%name'
	!
  define char*16 stype 
  let stype 'sed%type'
  let sed%type uvt
	!
	! Setup UV_FIT parameters
  @ uvfit_setup
  @ uvfit_subpar 1
	!
	@ find-flux-uvt 'sed%filter'    'sed%uv'
  let sed%type 'stype'
  let sed%name 'sed%uv'
end procedure
!
begin procedure plot_sed.ima
  if (sed%color) then
    @ p_show_sed 'sed%name' 'sed%freq' YES
  else
    @ p_show_sed 'sed%name' 'sed%freq' 
  endif  
end procedure
!
begin procedure position_uvfit
	!
	! @ position_uvt
	!		POSITION  determination
	!
	!   Inputs:
	!     a file filter (default *)
	!     a source model (E_GAUSS by default)
	!     an output filename 
	!     a reference frequency
	!
	!   Steps
	!      Fit the source model (UV data) or compute the source flux for 
	!      all channels of all data sets, and store them in 'sed%name'
	!
  define char*16 stype 
  let stype 'sed%type'
  let sed%type uvt
	!
	! Setup UV_FIT parameters
	!
	@ find-pos-uvt 'sed%filter'
  @ p_show_position position.txt YES
  let sed%type 'stype'
end procedure
!
let sed%filter "*"
let sed%type uvt
let sed%name sed.txt
!
begin data gag_scratch:x_sed.hlp
1 SED and Spectral index tools

  Compute an SED and fit the spectral index using selected files.
  The files can be UV tables or Images. UV_FIT with a simple
  model (e.g. E_GAUSS) is used for UV Tables, while command FLUX
  is used for Images.
  
2 SED%FILTER

  CHARACTER SED%FILTER*256

  Filter to select the files (with no file type). A typical value
  is TABLES/*+C to select only Continuum UV tables in the default
  layout from the "all-in-one" widget (or MAPS/*+C for the Images).
  
2 SED%FREQ

  DOUBLE SED%FREQ
  
  Reference frequency (in GHz) for the flux. Default is 0, indicating
  to take the mean value from the SED data file.
  
2 SED%TYPE

  CHARACTER SED%TYPE*16

  File extension for Images, without the initial dot (e.g. "lmv-clean" 
  or "lmv-sky").
    
2 SED%MODEL

  CHARACTER SED%MODEL*12
  
  Source model used for UV_FIT

2 SED%IMAGE

  CHARACTER SED%IMAGE*64

  Name of the data file to store the flux values as a function of frequency.
  The default is "sed-image.txt"
  
  Each line in the data file will have the followig format:
    Frequency  Flux Error Code 'FileName'
  where
    Frequency   is the frequency in MHz
    Flux        is the flux value in Jy
    Error       is the estimated uncertainty on the flux value
    Code        is a (non-negative) integer code to color code the plot
    FileName    is the data filename from where the flux was derived

2 SED%POS
  CHARACTER SED%POS *64

  Name of the data file to store the position values as a function of frequency.
  The default is "sed-position.txt"
  
  Each line in the data file will have the followig format:
    Frequency  Flux Error Code 'FileName'
  where
    Frequency   is the frequency in MHz
    Flux        is the flux value in Jy
    Error       is the estimated uncertainty on the flux value
    Code        is a (non-negative) integer code to color code the plot
    FileName    is the data filename from where the flux was derived

2 SED%UV
  CHARACTER SED%UV*64

  Name of the data file to store the flux values as a function of frequency.
  The default is "sed-uv.txt"
  
  Each line in the data file will have the followig format:
    Frequency  Flux Error Code 'FileName'
  where
    Frequency   is the frequency in MHz
    Flux        is the flux value in Jy
    Error       is the estimated uncertainty on the flux value
    Code        is a (non-negative) integer code to color code the plot
    FileName    is the data filename from where the flux was derived

2 SED%NAME

  CHARACTER SED%NAME*64
  
  Name of the data file handling the flux and position values as a 
  function of frequency. Each line in the data file must have the 
  following format:
    Frequency  Flux Error Code 'FileName'
  where
    Frequency   is the frequency in MHz
    Flux        is the flux value in Jy
    Error       is the estimated uncertainty on the flux value
    Code        is a (non-negative) integer code to color code the plot
    FileName    is the data filename from where the flux was derived
	
  SED%NAME will take the values of SED%IMAGE, SED%UV or SED%POSITION,
  depending on the last operation performed.
  
end data gag_scratch:x_sed.hlp
!
!---------------------------------------
!
begin data gag_scratch:x_sed_ima.hlp
1 Compute SED from Images

  Derive and SED using the selected image-like files (data cubes).
     If existing, the current SUPPORT is used to derive the flux using 
  the FLUX command.
     If not, variables CENTER and SIZE (that control the displayed region) 
  are used to define a temporary support.
  
  The noise estimate is approximate, since correlations between synthesized 
  beams are not properly handled.
  
2 SED%FILTER

  CHARACTER SED%FILTER*256

  Filter to select the files (with no file type). A typical value may be 
  MAPS/*+C to select only Continuum images in the default layout from 
  the "all-in-one" widget.
 
  The default is *.
  
2 SED%TYPE

  CHARACTER SED%TYPE*16

  File extension for Images, without the initial dot 
  (e.g. "lmv-clean" or "lmv-sky")

2 SIZE

  REAL SIZE[2]
  
  Size of the region used to compute the integrated flux (in arcsec).
  
    Caution: this is the same variable that is used for SHOW and VIEW 
  commands, i.e. the flux is computed on the area that you will display
  or have displayed.
  
2 CENTER

  REAL CENTER[2]
  
  Center of the region used to compute the integrated flux (in arcsec).
  
    Caution: this is the same variable that is used for SHOW and VIEW 
  commands, i.e. the flux is computed on the area that you will display
  or have displayed.
  
end data
!
begin data gag_scratch:x_sed_uvt.hlp
1 Compute SED from UV data

  Compute an SED using the selected UV tables. The flux and 
  errors are derived using command UV_FIT, with the specified
  model. 

  The noise estimate is good if the data is not dynamic range
  limited and the model adequate.

  
2 SED%FILTER

  CHARACTER SED%FILTER*256

  Filter to select the UV tables (with no file type). A typical value
  may be TABLES/*+C (or even better for NOEMA default PolyFix mode 
  TABLES/*Wide*+C) to select only Continuum images in the default 
  layout from the "all-in-one" widget

  Default is *
  
2 SED%MODEL

  CHARACTER SED%MODEL*12
  
  Type of model being used (e.g. POINT, C_GAUSS, E_GAUSS, etc...)
  in the UV_FIT fitting process.
   
end data
!
begin data gag_scratch:x_sed_show.hlp
1 Show SED

  Display the SED and fit the spectral index using the specified
  SED data file.
  
  For UV-based SED, the noise estimate is good if the data is not 
  dynamic range limited and the model adequate.
  
  For IMAGE-based SED, the noise estimate is approximate, but accounts
  for dynamic range limitations. However, the values may be incorrect
  if the region over which the SED is computed is inappropriate (e.g.
  too small, of far too large).

2 SED%FREQ  
  DOUBLE SED%FREQ
  
  Reference frequency (in GHz) for the flux. Default is 0, indicating
  to take the mean value from the data file.
  
2 SED%NAME

  CHARACTER SED%NAME*256
  
  Name of data file that stores the SED. 
  (Default is sed.txt)
  
end data gag_scratch:x_sed_show.hlp
!
!---------------------------------
!
