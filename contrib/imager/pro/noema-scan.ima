!
! @ noema-scan
!
if "A&1".eq."A?" then
  SAY " @ noema-scan Filter [FORCE]"
  SAY " "
  SAY " To be used after UV_PREVIEW. "
  SAY " It will search all UV tables of name 'filter'.uvt (use * to select all"
  SAY "   tables) and identify whether their frequency coverage falls into"
  SAY "   the band displayed by UV_PREVIEW. The table names and their"
  SAY "   frequency coverage will appear below the UV_PREVIEW plot."
  SAY " In addition, if a catalog is present, spectral lines falling in each"
  SAY "   UV table will be listed."
  SAY " "
  SAY " The script ignores broad-band UV tables, and only consider thoses"
  SAY "   with high spectral resolution"
  SAY " "
  SAY " If present, the FORCE keyword will also consider UV Tables that are beyond"
  SAY "   the frequency coverage"
  SAY "  "
  !! SAY " V1.0  6-Dec-2018  S.Guilloteau (LAB)"
  !! SAY " V1.1  15-Jan-2020  S.Guilloteau (LAB)"
  SAY " V1.2  22-Nov-2021  S.Guilloteau (LAB)"
  return
else if pro%narg.eq.0 then
  SAY "Use   ""@ noema-scan ?"" for help"
  return
endif
!
begin procedure scan-one
  define logical force
  let force &2
  define header a &1 read
  !
  define logical out_of_band
  define double fmin fmax f1 f2
  let f1 a%convert[2,'a%f_axis']+(1-a%convert[1,'a%f_axis'])*a%freqres
  let f2 a%convert[2,'a%f_axis']+(a%nchan-a%convert[1,'a%f_axis'])*a%freqres
  let fmin min(f1,f2)
  let fmax max(f1,f2)
  !
  ! Ignore out of band data
  out_of_band = .false.
  if (user_xmin.lt.user_xmax) then
    if (fmin.gt.user_xmax).or.(fmax.lt.user_xmin) let out_of_band = .true.
  else
    if (fmin.gt.user_xmin).or.(fmax.lt.user_xmax) let out_of_band = .true.
  endif
  if (out_of_band) then
    if .not.force RETURN
    message W SCAN 'ansi%blue'"File &1 is out of Frequency range of Self-Calibration"'ansi%none'
  endif
  !
  ! Avoid Bands that are really too wide
  if ((fmax-fmin).gt.0.5*abs(user_xmax-user_xmin)) return
  !
  ! The current limits of the PREVIEW plot are in Frequency
  greg1\set coor user
  define double fvals[4] yvals[4]
  let fvals fmin-20 fmin fmax fmax+20
  let yvals 0 1 1 0.
  !
  define char sname*12
  sic parse &1 sname
  !
  ! Plot low Resolution bands in Yellow
  if (abs(a%freqres).gt.1.5) then
    let yvals 0 -0.25 -0.25 0.
    greg1\histogram fvals yvals /base 0 /fill 5
    greg1\draw text (fmin+fmax)/2 0.5 'sname' 5 60 /user
    greg1\histogram fvals yvals ! To mark the boundaries
    return
  endif
  !
  ! High resolution ones in Green
  if (.not.out_of_band) greg1\histogram fvals yvals /base 0 /fill 2
  !
  ! Update Table list
  let noema%ntables noema%ntables+1
  let noema%ttmp[noema%ntables] &1
  let noema%ltmp[noema%ntables] 'a%line'
  let noema%ftmp[noema%ntables] a%restfre
  !
  ! Select lines if any
  if exist(linedb%dbin) FIND 'fmin' 'fmax'
  if exist(lines%n) then
    define integer plines
    let plines abs(lines%n)
    if (plines.gt.0) then
      say 'plines'" lines identified in UV table "'sname'
      for i 1 to plines
        say Lines%lines[i] " at " lines%frequency[i] /format A A F10.3
      next
      let noema%ltmp[noema%ntables] 'lines%lines[plines]'
      let noema%ftmp[noema%ntables] lines%frequency[plines]
    else
      say "No line identified in UV table "'sname'
    endif
  else
    ! Nothing if no catalog...
  endif
  if out_of_band return
  greg1\draw text (fmin+fmax)/2 0.5 'sname' 5 60 /user
  !
end procedure scan-one
!
greg1\set box box_xmin box_xmax 1.0 4
limi = = -0.25 1
!
if exist(noema) delete /var noema
define logical force
let force = pro%narg.gt.1
!
sic find &1*.uvt
if (dir%nfile.eq.0) then
  MESSAGE E NOEMA "No such file(s) &1*.uvt"
  return error
endif
define structure noema /global
define integer noema%ntables /global
define character*256 noema%ttmp[dir%nfile] /global
define character*16 noema%ltmp[dir%nfile] /global
define double noema%ftmp[dir%nfile] /global
let noema%ntables 0
!
for i 1 to dir%nfile
  !! say "Scanning "'dir%file[i]'
  @ scan-one 'dir%file[I]' 'FORCE'
next
!
if noema%ntables.gt.0 then
  define character*256 noema%table[noema%ntables] /global
  define character*16 noema%line[noema%ntables] /global
  define double noema%freq[noema%ntables] /global
  for i 1 to noema%ntables
    let noema%table[i] 'noema%ttmp[i]'
    let noema%line[i] 'noema%ltmp[i]'
    let noema%freq[i] 'noema%ftmp[i]'
  next
endif
delet /var noema%ftmp noema%ltmp noema%ttmp
