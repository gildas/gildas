!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Plot cumulative flux as a function of the number of components.
! Totally re-written for IMAGER by S.Guilloteau
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
@ window_tools.greg
!
gtvl\clear directory
let PopupName "CCT"  ! Generic name of GTV sub-directories for POPUP
!
if exist(comp) then
  delete /var comp
endif
!
define integer mcomp ncomp nplanes
define integer ucomp jfirst jlast
let ucomp &2+1
!
if (.not.exist(CCT_FLUX)) then
  ! All is now in the code. This is too slow in a script
  @ p_cct_flux &1
endif
let ncomp size(cct_flux,1)
let nplanes size(cct_flux,2)
!
! All set now...
!
! Compute the limit
define real comp[ncomp] /global
let comp[i] i-1
@ xy_limits comp cct_flux
!
! Define plotting window
if first.le.0 then
  let jfirst 1
else
  let jfirst min(first,nplanes)
endif
!
if last.le.0 then
  let jlast max(1,nplanes+last)
else
  let jlast min(last,nplanes)
endif
!
@ plot_position
let aspect_ratio yes
let aspect 1
@ find_nx_ny 'jlast-jfirst+1'
if (expand.eq.0) then
  let PopupExpand min(6.0/(nx_box+ny_box),1.) ! Automatic definition
else
  let PopupExpand 'expand'                    ! User defined expansion
endif
greg1\set expand 'PopupExpand'
!
! Plot
for iplane jfirst to jlast
  gtvl\create dir 'PopupName''iplane'
  gtvl\change dir 'PopupName''iplane'
  @ window_xy 'nx_box' 'ny_box' 'iplane-jfirst+1'
  let mcomp cct_ncomp[iplane]
  if (ucomp.gt.1) then
    let mcomp min(ucomp,mcomp)
  endif
  if (mcomp.gt.1) then
    greg1\connect comp[1:mcomp] cct_flux[1:mcomp,iplane]
    greg1\draw text 1 -1 'mcomp-1'" Comp." 6 /char 7
  else
    greg1\draw relo 0 0 /user
    greg1\draw line 1 0 /user
    greg1\draw text 1 -1 "No Comp." 6 /char 7
  endif
  greg1\box n n
  gtvl\change dir <greg
next
!
! Make labels: locally or not according to number of boxes
!
if (nx_box.le.3.and.ny_box.le.3) then
  @ window_xy 'nx_box' 'ny_box' 1 1
  greg1\box
  greg1\set expand 1.0
  @ window_xy 1 1 1 1
  greg1\label "Clean component number" /x
  greg1\label "Cumulated flux (Jy)"    /y
  ! Make sure we stay with the right number of boxes
  @ window_xy 'nx_box' 'ny_box' 1 1
endif
!
! Make header
!
@ header_position
@ p_header &1
!
! Plot the box
if (nx_box.le.3.and.ny_box.le.3) then
  continue
else
  greg1\set expand 0.6
  define real dx dy x2 y2
  x2 = box_xmax
  y2 = box_ymax
  if (page_x.gt.page_y) then
    let dx min((page_x-x2),0.15*page_y)
    let dy dx
    greg1\set box (page_x+x2)|2.-2*dx|5. (page_x+x2)|2.+3*dx|5. page_y*0.5-dy page_y*0.5
  else
    let dx min((0.15*page_x),(page_y-y2))
    let dy dx
    greg1\set box page_x*0.95-dx page_x*0.95 page_y*0.95-dy page_y*0.95
  endif
  greg1\box
  greg1\label "Clean component number" /x
  greg1\draw text -3 0 "Cumulated flux (Jy)" 5 90 /char 4
endif
!
greg1\set expand 1.0
@ d_popup 'jfirst' 'jlast' 1
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
