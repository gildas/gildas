! @ preview_lines  [Fmin Fmax]
!
! Mode 1)  No arguments:
!   Use UV_PREVIEW (if not already done) to identify regions with
!   line emission and create the structure PREVIEW%
!
!   Identifies the spectral lines in these regions, and augments
!   the PREVIEW% structure by the list of lines, frequencies
!   and range of channels where they are found.
!
! Mode 2)  Two arguments
!   Just select lines between Fmin and Fmax and put them
!   in the PREVIEW% structure
!
! Preview data if not already done
if .not.exist(preview)  then
  if pro%narg.eq.2 then
    define structure preview /global
  else
    SAY "E-PREVIEW_LINES,   Use command UV_PREVIEW before, or use "
    SAY "                   @ preview_lines Fmin Fmax"
    return error
  endif
else
  if exist(preview%detected) delet /var preview%detected
  if exist(preview%found) delet /var preview%found
  if exist(preview%nfound) delete /var  preview%nfound
  if exist(preview%ndetect) delete /var  preview%ndetect
endif
if .not.exist(preview%tolerance) then
  define real preview%tolerance /global
  let preview%tolerance 2.0  ! ±2 MHz margin
endif
let verif sic%verify /new logical
!
if .not.exist(preview%freq) then
  define double preview%freq /global
endif
if .not.exist(preview%line) then
  define char*32 preview%line /global
endif
if .not.exist(preview%nfound) then
  define integer preview%nfound /global
  let preview%nfound 0
endif
!
define double fmin fmax f_scale
!
! First, search for Lines in specified range
if pro%narg.eq.2 then
  fmin = min(&1,&2)  !! -preview%tolerance
  fmax = max(&1,&2)  !! +preview%tolerance
else
  fmin = preview%fmin
  fmax = preview%fmax
endif
!
! Allow for a global Redshift by introducing a scale factor
! on frequencies f_scale
if exist(redshift) then
  f_scale = 1.d0+redshift
else
  f_scale = 1.d0
endif
!
! Fall back to default frequency if needed
if .not.exist(linedb%dbin) then
  SAY "No selected Line Data Base, using default frequency"
  let preview%freq (fmin+fmax)/2.d0
  RETURN
endif
!
!  Here, a data base has been opened - No need to repeat
!! @ catalog
!
define integer nlines klines
!
if (verif) then
  sic verif off
  SAY "FIND "'fmin*f_scale'" "'fmax*f_scale'
  find 'fmin*f_scale' 'fmax*f_scale'
  sic verify on
else
  find 'fmin*f_scale' 'fmax*f_scale'
endif
!
if .not.exist(lines%n) return   !! Nothing found
!! if lines%n.eq.0 return       !! the case is treated later
!
let nlines abs(lines%n)
let preview%nfound nlines
!
if (nlines.gt.0) then
  define structure preview%found /global
  define chara*32 preview%found%lines[nlines] preview%found%species[nlines] /global
  define double preview%found%freq[nlines] /global
  for k 1 to nlines
    let preview%found%lines[k] 'lines%lines[k]'
    let preview%found%species[k] 'lines%species[k]'
    let preview%found%freq[k]  'lines%frequency[k]'
  next
  let preview%freq 'lines%frequency[nlines]'
  let preview%line 'lines%lines[nlines]'
else
  let preview%freq (fmin+fmax)/2.0
  if exist(uv%line) let preview%line 'uv%line'" "
endif
!
! If boundaries are given, return...
if pro%narg.eq.2 return
! If no lines, return...
if (lines%n.eq.0) return
!
! Draw lines with the f_scale scale factor
lim user_xmin*f_scale user_xmax*f_scale = =
! Draw all lines in range in BLUE
!
greg1\pencil 3
for k 1 to nlines
  greg1\draw text lines%frequency[k] user_ymax-0.25*(user_ymax-user_ymin) -
  'lines%plot[k]' 9 60 /USER
next
greg1\pencil 0
!
define integer preview%ndetect /global
preview%ndetect = 0
!
! No signal detected, return...
if (.not.exist(preview%frequencies)) then
  lim user_xmin/f_scale user_xmax/f_scale = =
  return
endif
!
! Search for detected lines, and Draw them in RED
let nlines size(preview%frequencies,2)
define chara*32 clines[nlines] slines[nlines]
define double  flines[nlines]
define integer jlines[nlines] plines
greg1\pencil 1 /we 3
!
for i 1 to nlines
  !
  ! The margin is here to allow for small velocity shifts
  ! but it can result in overlaps of windows
  fmin = min(preview%frequencies[1,i],preview%frequencies[2,i])-preview%tolerance
  fmax = max(preview%frequencies[1,i],preview%frequencies[2,i])+preview%tolerance
  sic verif off
  find 'fmin*f_scale' 'fmax*f_scale'
  !! LINEDB\SELECT /freq 'fmin*f_scale' 'fmax*f_scale' /ENERGY 300 /SORT aij
  if verif sic verify on
  plines = abs(lines%n)
  !
  if (plines.gt.0) then
    for k 1 to plines
      greg1\draw text lines%frequency[k] user_ymax-0.25*(user_ymax-user_ymin) -
      'lines%plot[k]' 9 60 /USER
    next
    let klines klines+1
    slines[klines] = 'lines%lines[plines]'
    if pro%narg.eq.0 then
      if (plines.eq.1) then
        SAY "Found transition "'slines[klines]'
      else
        SAY "Most likely transition is "'slines[klines]'
      endif
    endif
    let clines[klines] 'lines%species[plines]'
    let flines[klines] 'lines%frequency[plines]'
    jlines[klines] = i
  endif
next
!
greg1\pencil 1 /wei 1
greg1\pencil 0
! Restore the Redshifted frequency scale
limi user_xmin/f_scale user_xmax/f_scale = =
!
if (klines.eq.0) then
  return
else if (klines.eq.1) then
  preview%ndetect = 1
else
  ! Compact the list of different frequencies, as consecutive windows
  ! may overlap and yield the same spectral line
  preview%ndetect = 1
  define integer kfound
  for i 2 to klines
    kfound = i
    for j 1 to preview%ndetect
      if (flines[i].eq.flines[j]) then
        kfound = j
        break
      endif
    next
    !
    if (kfound.eq.i) then
      preview%ndetect = preview%ndetect+1
      flines[preview%ndetect] = 'flines[i]'
      slines[preview%ndetect] = 'slines[i]'
      clines[preview%ndetect] = 'clines[i]'
      jlines[preview%ndetect] = 'jlines[i]'
    endif
  next
endif
klines = preview%ndetect
!
if (klines.gt.0) then
  define structure preview%detected /global
  define chara*32 preview%detected%lines[klines] preview%detected%species[klines] /global
  define double preview%detected%freq[klines] /global
  if (exist(preview%edges)) then
    define integer preview%detected%ranges[2,klines] /global
    for i 1 to klines
      let preview%detected%lines[i] 'slines[i]'
      let preview%detected%species[i] 'clines[i]'
      let preview%detected%ranges[1,i] max(1,preview%edges[1,'jlines[i]']-4)
      if exist(uv%nchan) then
        let preview%detected%ranges[2,i] min(uv%nchan,preview%edges[2,'jlines[i]']+4)
      else
        let preview%detected%ranges[2,i] preview%edges[2,'jlines[i]']
      endif
      let preview%detected%freq[i]  'flines[i]'
    next
  else
    for i 1 to klines
      let preview%detected%lines[i] 'slines[i]'
      let preview%detected%species[i] 'clines[i]'
      let preview%detected%freq[i]  'flines[i]'
    next
  endif
  !
  ! Take the first in list (it is arbitrary at this stage)
  let preview%freq 'flines[1]'
  let preview%line 'slines[1]'
endif
