! i_selfcal.map,  p_selfcal.map, x_selfcal.map, d_selfcal.map
!   Tools for Self Calibration
!   S.Guilloteau LAB   2013-2018
!-----------------------------------------------------------------------------------------
!
! i_selfcal.map:: Support for INPUT selfcal
!
if .not.exist(self_mode) then
  @ d_selfcal
endif
begin procedure add_item
  let &1 " "
  define integer dims[8] nloop
  compute dims dimof &2
  let nloop dims[1]
  !
  if (nloop.eq.0) then
   let &1 '&1'" "'&2'
  else
    for i 1 to nloop
     let &1 '&1'" "'&2[i]'
    next
  endif
end procedure add_item
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
let seve E /new char*1 
if (pro%narg.eq.0) then
  say " SELFCAL computes and applies self-calibration to a UV Table"
!  say "   V 2.0: - allows several self-calibration loops in case of complex object"
!  say "   V 2.1: - allows self-calibrating using a channel range of a line table"
!  say "   V 2.2: - Control reference antenna                            "   Nov-2015
!  say "   V 2.3: - Limit SNR level                                      "   Nov-2016
!  say "   V 2.4: - Automatic number of Loops & Display of convergence   "   Nov-2017
!  say "   V 2.5: - Flag or Keep uncalibrated data                       "   Jan-2018
!  say "   V 3.0: - In Memory version using current UV data              "   Mar-2018
!  say "   V 3.1: - Option /MINFLUX in MODEL                             "   Mar-2018
!  say "   V 3.2: - Full support of bandwidth range and MAP_CENTER       "   Mar-2018
!  say "   V 4.0: - First support of AMPLITUDE self calibration          "   Apr-2018
!  say "   V 4.1: - Standardize interface with SELF_* variables          "   May-2018
!  say "   V 4.2: - Gather results in one single SELF_SNAME file         "   Jun-2018
  say "   V 4.3: - Allow DELAY mode to APPLY Phase correction           "   May-2019
  say " "
  @ sub_nloops W ! Compute the number of loops
  seve = W
endif
!
let self_mode 'self_mode' /UPPER
!
define double temp
define character r*5 n*5 b*5
r = 'ansi%red'
n = 'ansi%none'
b = 'ansi%blue'
!
say "SELFCAL  Parameters" 
say "  Mode                           "'b'"SELF_MODE       "'n'"[ "'r''self_mode''n' "]"
say "  Number of SelfCal loops        "'b'"SELF_LOOP       "'n'"[ "'r''self_loop''n' "]"
@ add_item self_string self_times 
say "  Integration time for solution  "'b'"SELF_TIMES      "'n'"[ "'r''self_string''n' "]"
@ add_item self_string self_niter 
say "  Number of selected components  "'b'"SELF_NITER      "'n'"[ "'r''self_string''n' "]"
@ add_item self_string self_minflux 
say "  Minimum flux level in pixels   "'b'"SELF_MINFLUX    "'n'"[ "'r''self_string''n' "]"
say "  Channel range for gains        "'b'"SELF_CHANNEL    "'n'"[ "'r''self_channel[1]'" "'self_channel[2]''n' "]"
say "  Minimum SNR                    "'b'"SELF_SNR        "'n'"[ "'r''self_snr''n' "]"
say "  Flag data with no solution     "'b'"SELF_FLAG       "'n'"[ "'r''self_flag''n' "]"
!
say "  Reference antenna              "'b'"SELF_REFANT     "'n'"[ "'r''self_refant''n' "]"
say "  Noise scale factor             "'b'"SELF_SNOISE     "'n'"[ "'r''self_snoise''n' "]"
say " "
say "  Max Flux for display           "'b'"SELF_FLUX       "'n'"[ "'r''self_flux''n' "]"
if self_restore then
  say "  Use UV_RESTORE at end          "'b'"SELF_RESTORE    "'n'"[ "'r'"YES"'n' "]"
else
  say "  Do not use UV_RESTORE at end   "'b'"SELF_RESTORE    "'n'"[ "'r'"NO"'n' "]"
endif
if self_display then
  say "  Display CLEAN image at loop    "'b'"SELF_DISPLAY    "'n'"[ "'r'"YES"'n' "]"
  say "  Display Color Threshold        "'b'"SELF_COLOR      "'n'"[ "'r''self_color''n' "]"
else
  say "  No image display               "'b'"SELF_DISPLAY    "'n'"[ "'r'"NO"'n' "]"
endif
!
say " "
say "CLEAN    Parameters                                             Short name"
@ i_clean   BRIEF
!
say "UV_MAP   Parameters"
@ i_uv_map  BRIEF
!
if exist(self_message) then
  SAY " "
  message 'seve' SELFCAL 'Self_message'
  delete /var self_message
endif
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

