! @ p_poly Plot Type Parameters ...
!
! &1: Yes or No   Plot the polygon
! &2: rectangle or ellipse
! &3: xcenter [arcsec]
! &4: ycenter [arcsec]
! &5: major   [arcsec]
! &6: minor   [arcsec]
! &7: pa      [degree]
!
! Define and optionally draw a polygon
!
! Adapted from p_poly.map (author J.Pety IRAM) by S.Guilloteau
!   (p_poly.map has the Plot argument at end, with a default being YES)
!   (Optimized by avoid explicit loop)
!-----------------------------------------------------------------------
!
! Go to radian from input units
define double center[2] major minor pa
define logical show
let show &1
let center '&3*sec' '&4*sec'
let major '(&5/2.0)*sec'
let minor '(&6/2.0)*sec'
let pa    'pi*(90-&7)/180.0'
!
! Define number of gons
define integer ngons
if ("&2".eq."ellipse") then
  let ngons 100
else if ("&2".eq."rectangle") then
  let ngons 4
else
  message e poly "Unknown polygon type: &2"
  return base
endif
!
define real xygons[ngons,2] cospa sinpa cangle
let cospa cos(pa)
let sinpa sin(pa)
!
if ("&2".eq."ellipse") then
  let cangle (2*pi)/ngons
  let cangle (2*pi)/ngons
  define real rx[ngons] ry[ngons]
  let rx[i] major*cos(i*cangle)
  let ry[i] minor*sin(i*cangle)
  let xygons[1] center[1]+rx*cospa-ry*sinpa
  let xygons[2] center[2]+rx*sinpa+ry*cospa
else
  let xygons[1,1:2] 'center[1]+major*cospa+minor*sinpa' 'center[2]+major*sinpa-minor*cospa'
  let xygons[2,1:2] 'center[1]+major*cospa-minor*sinpa' 'center[2]+major*sinpa+minor*cospa'
  let xygons[3,1:2] 'center[1]-major*cospa-minor*sinpa' 'center[2]-major*sinpa+minor*cospa'
  let xygons[4,1:2] 'center[1]-major*cospa+minor*sinpa' 'center[2]-major*sinpa-minor*cospa'
endif
!
!set marker 4 1 1 0
!draw marker 'center[1]' 'center[2]' /user radians
if (show) then
  greg2\polygon xygons /plot /var
else
  greg2\polygon xygons /var
end if
!
