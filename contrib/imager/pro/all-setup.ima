!
! The velocity range should be suitable for your sources 
! and large enough to give a minimum spectral baseline around
! the signal, but final image size is proportional to this range !...
!
! @ all-setup Vmin Vmax [Vres]
!
!   Setup values for all control variables of the
!   all-in-one suite, including all-pipeline.ima
!
! Sanity check: create the variables if needed
!
if (exist(all%done)) then
  @ all-vrange &1 &2 &3
  return
endif
!
! First call, make sure all scripts are created 
!
define logical do_default
do_default = .not.exist(all)
!
if (do_default) @ all-create
!
! if there was a Memory file, default values are no longer needed
let do_default .not.file("all-memory.ima")
!
begin procedure all-vrange
  if (pro%narg.eq.3) then
    let all%range &1 &2 &3
  else if (pro%narg.eq.2) then
    let all%range[1:2] &1 &2
  endif
  if exist(linedb%dbin) then
    !
    ! If null range, ask for one
    if (all%range[1].eq.all%range[2]) then
      let all%range      /prompt "Velocity Range: Vmin Vmax Vres (km/s)"
    endif  
    !
    ! Sanity check
    if (all%range[1].ge.all%range[2]) then
      message E ALL-SETUP "Invalid velocity range "'all%range[1]'" >= "'all%range[2]'
      return error
    endif
    if (all%range[3].lt.0) then
      message E ALL-SETUP "Invalid velocity resolution "'all%range[3]'
      return error
    endif
  endif
end procedure all-vrange
!
begin procedure all-default
  !
  let all%filter   *   ! "File filter (NO extension !..)"
  !
  ! data with spectral resolution coarser than all%minfres will be considered as Continuum only
  let all%minfres  1.5      /prompt "Maximum Frequency Resolution (MHz)"
  !
  ! Default layout - Change only if needed
  let all%uvfits UVFITS/   ! /prompt "Initial UVFITS files sub-directory"
  let all%raw    RAW/      ! /prompt "Raw UV Tables sub-directory"
  let all%self   SELF/     ! /prompt "Self calibrated or Time compressed UV Tables sub-directory"
  let all%maps   MAPS/     ! /prompt "Images sub-directory"
  let all%tables  TABLES/  ! /prompt "Final UV Tables sub-directory"
  !
  ! Other defaults
  let all%minband  500  ! /prompt "Minimum bandwith for self calibration (MHz)"
  let all%drop 5 5      ! Number of channels dropped at each edge
  !
  ! Self calibration defaults appropriate for NOEMA - Change only if needed
  let all%prefix_self "s-"            ! Prefix filenames by s- 
  let all%phase_niter 0 0 0 /resize   ! /prompt "PHASE: Number of clean components for each loop" /resize
  let all%phase_times 45 45 45 /resize ! /prompt "PHASE: Integration time for each loop" /resize
  let all%ampli_niter 0 0 /resize     ! /prompt "AMPLI: Number of clean components for each loop" /resize
  let all%ampli_times 135 135         ! /prompt "AMPLI: Integration time for each loop" /resize
  let all%collect  0                  ! /prompt "Phase Stability Rejection Threshold"
!
! Time compression of Self-Calibrated data
  let all%itime 0      ! Will guess according to field of view and resolution
!
! Imaging step
! You may restrict the velocity range to what is really visible, with
! minimum baseline for this stage, contrary to the UV table creation
  let all%range  0 0 0     ! This can be optimized source per source for space saving
  let all%restore YES      ! Use UV_RESTORE at end (allows to use a bit smaller images)
!
! Imaging step is controlled by usual parameters
! Specifying values can help you to optimize the image size and processing time
! (the smaller, the faster !...)
  let map_field 0          ! Field of view   ==> guess
  let map_cell  0          ! Pixel size      ==> guess
  let map_size  0          ! Number of pixels ==> guess 
  let map_robust   'map_robust'   ! "Robust" factor, Use the current value: see help UV_MAP
end procedure all-default
!
begin procedure add_argument
  let &1 '&1'" "'&2'
end procedure add_argument
!
begin procedure make_say_command
  define integer nloop
  let nloop size(&1)
  !
  define chara*256 long_string /global
  !
  let long_string " "  
  if (nloop.eq.0) then
    @ add_argument long_string '&1'
  else
    for i 1 to nloop
      @ add_argument long_string '&1[i]'
    next
  endif
  symbol &2  'long_string'
  delete /variab long_string 
end procedure
!
! These may have been created by the /WIDGET option before
if symbol(pha_niter) delete /symbol pha_niter pha_times amp_niter amp_times
begin procedure all-display
  define character r*5 n*5 b*5
  r = 'ansi%red'
  n = 'ansi%none'
  b = 'ansi%blue'
  !
  @ make_say_command all%phase_niter pha_niter
  @ make_say_command all%phase_times pha_times
  @ make_say_command all%ampli_niter amp_niter
  @ make_say_command all%ampli_times amp_times
  !
  if (all%mode.eq."CONTINUUM") then
    say "  Pipeline Mode                         "'r''all%mode''n'"   Produce only Continuum images  "
  else if (all%mode.eq."SPLIT") then
    say "  Pipeline Mode                         "'r''all%mode''n'"   Split Line from Continuum             "
  else
    say "  Pipeline Mode                         "'r''all%mode''n'"   Image Line with Continuum             "
  endif
  say "  Current catalog                       "'r''all%catalog''n'
  say "  File Filter                           "'b'"ALL%FILTER      "'n'"[" 'r' 'all%filter''n'  "]"
  say "  Velocity Range & Resolution (km/s)    "'b'"ALL%RANGE       "'n'"[" 'r' 'all%range[1]' 'all%range[2]' 'all%range[3]''n' "]" 
  say "  Edge channels dropped                 "'b'"ALL%DROP        "'n'"[" 'r' 'all%drop[1]' 'all%drop[2]''n' "]" 
  say "  Line/Cont Resolution Threshold (MHz)  "'b'"ALL%MINFRES     "'n'"[" 'r' 'all%minfres''n' "]"
  if exist(redshift) then
    say "  Source Redshift                       "'b'"REDSHIFT        "'n'"[" 'r' 'redshift''n' "]"
  else
    say "  NO Source Redshift"
  endif
  say " "
  say "---  Self Calibration parameters"
  say "  Phase Selfcal Niter                   "'b'"ALL%PHASE_NITER "'n'"[" 'r''pha_niter''n' "]" 
  say "  Phase Selfcal Times (s)               "'b'"ALL%PHASE_TIMES "'n'"[" 'r''pha_times''n' "]" 
  say "  Ampli Selfcal Niter                   "'b'"ALL%AMPLI_NITER "'n'"[" 'r''amp_niter''n' "]" 
  say "  Ampli Selfcal Times (s)               "'b'"ALL%AMPLI_TIMES "'n'"[" 'r''amp_times''n' "]" 
  say "  Flagging Threshold                    "'b'"ALL%COLLECT     "'n'"[" 'r''all%collect''n' "]"
  say "  Combine Wide Band solutions           "'b'"ALL%COMBINE     "'n'"[" 'r''all%combine''n' "]"
  !
  say "  Use UV_RESTORE                        "'b'"ALL%RESTORE     "'n'"[" 'r''all%restore''n' "]"
  say "---  Basic imaging parameters                       Selected       Recommended"
  say "  Map Center                            "'b'"MAP_CENTER       "'n'"[" 'r''map_center''n' "]" -
  say "  Map Size (pixels)                     "'b'"MAP_SIZE        "'n'"[" 'r''map_size[1]' 'map_size[2]''n' "]" -
    'n'"  [" 'r''rec_size[1]' 'rec_size[2]''n' "]"
  say "  Field of view (arcsec)                "'b'"MAP_FIELD       "'n'"[" 'r''map_field[1]' 'map_field[2]''n' "]" -
    'n'"  [" 'r''rec_field[1]' 'rec_field[2]''n' "]" 
  say "  Pixel size (arcsec)                   "'b'"MAP_CELL        "'n'"[" 'r''map_cell[1]' 'map_cell[2]''n' "]" -
    'n'"  [" 'r''rec_cell[1]' 'rec_cell[2]''n' "]" 
  say "  Robust weighting parameter            "'b'"MAP_ROBUST      "'n'"[" 'r''map_robust''n' "]"
  say " "
  say "  Clean beam size  (User)               "'b'"BEAM_SIZE       [ "'r''beam_size[1]'""" by "'beam_size[2]'""" at PA "'beam_size[3]''n'"° ]"
  delete /symbol pha_niter amp_niter pha_times amp_times 
end procedure all-display
!
define logical all%done /global
!
! Initialize defaults only if needed
if (do_default)  @ all-default
!! @ all-vrange &1 &2 &3
