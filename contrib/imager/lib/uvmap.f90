subroutine old_uvmap(task,line,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>old_uvmap
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !------------------------------------------------------------------------
  ! @ private
  !
  ! "Historical" version of UV_MAP, deprecated in IMAGER,
  ! only used for debugging and comparison.
  !
  ! TASK  Compute a map from a CLIC UV Sorted Table
  ! by Gridding and Fast Fourier Transform, using adequate
  ! scratch space for optimisation. Will work for
  ! up to 128x128x128 cube data size, may be more...
  !
  ! Input :
  !     a precessed UV table
  ! Output :
  !     a precessed, rotated, shifted UV table, sorted in V,
  !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
  !     a beam image or cube
  !     a LMV cube
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: task ! Caller (MX or UV_MAP)
  character(len=*), intent(in) :: line ! Command line
  logical, intent(out) :: error
  !
  character(len=1), parameter :: question_mark='?'
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  !
  real, allocatable :: w_mapu(:), w_mapv(:), w_grid(:,:)
  real, allocatable :: res_uv(:,:)
!  type (uvmap_par), save :: map
  real(8) new(3)
  real(4) rmega,uvmax,uvmin,uvma
  integer wcol,mcol(2),nfft,sblock
  integer n,ier, nsizes(3)
  logical one, sorted, shift, abort
  character(len=24) ra_c,dec_c
  character(len=message_length) :: chain
  real cpu0, cpu1
  real(8) :: freq
  real, allocatable :: fft(:)
  integer nx,ny,nu,nv,nc,np,nb
  !
  character(len=4) :: argum
  logical limits, needed
  real ylimn,ylimp
  integer ipen
  !
  call imager_tree('OLD_UVMAP')
  !
  call map_prepare(task,huv,themap,error)
  if (error) return
  !
  if (task.eq.'MX') then
    ! Test the ? argument
    if (sic_narg(0).eq.1) then
      call sic_ch(line,0,1,argum,n,.false.,error)
      if (argum(1:1).eq.question_mark) then
        call exec_program("@ i_mx "//argum)
        return
      endif
    endif
    !
    if (themap%beam.ne.0) then
      call map_message(seve%e,task,'Only works of 1 beam in total (so far)')
      error = .true.
      return
    endif
  endif
  !
  one = .true.
  call sic_get_inte('WCOL',wcol,error)
  call sic_get_inte('MCOL[1]',mcol(1),error)
  call sic_get_inte('MCOL[2]',mcol(2),error)
  !
  call sic_get_logi('UV_SHIFT',shift,error)
  if (shift) then
     call sic_get_char('MAP_RA',ra_c,n,error)
     call sic_get_char('MAP_DEC',dec_c,n,error)
     call sic_get_dble('MAP_ANGLE',new(3),error)
  else
     new = 0.d0
  endif
  !
  ! First sort the input UV Table, leaving UV Table in UV_*
  if (shift) then
     call sic_decode(ra_c,new(1),24,error)
     if (error) then
        write(chain,'(A)') 'Input conversion error on phase center'
        call map_message(seve%e,task,chain)
        return
     endif
     call sic_decode(dec_c,new(2),360,error)
     if (error) then
        write(chain,'(A)') 'Input conversion error on phase center'
        call map_message(seve%e,task,chain)
        return
     endif
     new(3) = new(3)*pi/180.0d0
  endif
  call gag_cpu(cpu0)
  needed = themap%uniform(2).ne.0
  call uv_sort (huv,duv,error,sorted,shift,new,uvmax,uvmin,needed)
  if (error) return
  if (.not.sorted) then
    ! Redefine SIC variables (mandatory)
    call map_uvgildas('UV',huv,error,duv)
  endif
  call gag_cpu(cpu1)
  write(chain,102) 'Finished sorting ',cpu1-cpu0
  call map_message(seve%i,task,chain)
  !
  call map_parameters(task,themap,huv,freq,uvmax,uvmin,error) ! huv%gil%majo)
  if (error) return
  uvma = uvmax/(freq*f_to_k)
  !
  themap%xycell = themap%xycell*pi/180.0/3600.0
  !
  ! Get work space, ideally before mapping first image, for
  ! memory contiguity reasons.
  !
  nx = themap%size(1)
  ny = themap%size(2)
  nu = huv%gil%dim(1)
  nv = huv%gil%nvisi ! not %%dim(2)
  !
  ! Define the number of output channels
  nc = huv%gil%nchan
  if (mcol(1).eq.0) then
    mcol(1) = 1
  else
    mcol(1) = max(1,min(mcol(1),nc))
  endif
  if (mcol(2).eq.0) then
    mcol(2) = nc
  else
    mcol(2) = max(1,min(mcol(2),nc))
  endif
  nc = mcol(2)-mcol(1)+1
  !
  write(chain,'(A,I0,A,I0,A)') 'Imaging channel range [',mcol(1),',',mcol(2),']'
  call map_message(seve%i,task,chain)
  !
  ! Check if Weights have changed by MCOL choice
  if (any(saved_mcol.ne.mcol)) do_weig = .true.
  saved_mcol = mcol
  !
  !
  if (method%method.eq.'MX') do_weig = .true. ! Test
  if (do_weig) then
    call map_message(seve%i,task,'Computing weights ')
    if (allocated(g_weight)) deallocate(g_weight)
    if (allocated(g_v)) deallocate(g_v)
    allocate(g_weight(nv),g_v(nv),stat=ier)
    if (ier.ne.0) goto 98
  else
    call map_message(seve%d,task,'Re-using weight space')
  endif
  !
  rmega = 8.0
  ier = sic_ramlog('SPACE_MAPPING',rmega)
  sblock = max(int(256.0*rmega*1024.0)/(nx*ny),1)
  !
  ! New Beam place
  if (allocated(dbeam)) then
     call sic_delvariable ('BEAM',.false.,error)
     deallocate(dbeam)
  endif
  call gildas_null(hbeam)
  !
  ! New dirty image
  if (allocated(ddirty)) then
     call sic_delvariable ('DIRTY',.false.,error)
     deallocate(ddirty)
  endif
  allocate(ddirty(nx,ny,nc),stat=ier)
  !
  call gildas_null(hdirty)
  hdirty%gil%ndim = 3
  hdirty%gil%dim(1:3) = (/nx,ny,nc/)
  call sic_mapgildas('DIRTY',hdirty,error,ddirty)
  !
  hdirty%r3d => ddirty
  !
  ! Find out how many beams are required
  call map_beams(task,themap%beam,huv,nx,ny,nb,nc)
  !
  ! Process sorted UV Table according to the number of beams produced
  if (map_version.eq.-1 .or. task.eq.'MX') then
    ! The MX patch is temporary
    !
    ! Use old code only when explicitely requested
    hbeam%gil%ndim = 2
    hbeam%gil%dim(1:2)=(/nx,ny/)
    allocate(dbeam(nx,ny,1,1),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'Memory allocation error on DBEAM')
      error =.true.
      return
    endif
    !
    nfft = 2*max(nx,ny)
    allocate(w_mapu(nx),w_mapv(ny),w_grid(nx,ny),fft(nfft),stat=ier)
    if (ier.ne.0) goto 98
    !
    hbeam%r3d => dbeam(:,:,:,1)
    call one_beam (task,themap,   &
       &    huv, hbeam, hdirty,   &
       &    nx,ny,nu,nv, duv,   &
       &    w_mapu, w_mapv, w_grid, &
       &    g_weight, g_v, do_weig,  &
       &    wcol,mcol,fft,   &
       &    sblock,cpu0,error,uvma)
    if (error) return
    !
    call sic_mapgildas('BEAM',hbeam,error,dbeam)
  else
    ! The MX part still needs debugging
    !
    hbeam%gil%ndim = 3
    hbeam%gil%dim(1:4)=[nx,ny,nb,1]
    if (nb.gt.1) then
      allocate(hbeam%r3d(nx,ny,nb),dbeam(nx,ny,1,nb),stat=ier)
    else
      allocate(dbeam(nx,ny,1,1),stat=ier)
      hbeam%r3d => dbeam(:,:,:,1)
    endif
    if (ier.ne.0) then
      call map_message(seve%e,task,'Memory allocation error on DBEAM')
      error =.true.
      return
    endif
    !
    call many_beams_para (task,themap,   &
       &    huv, hbeam, hdirty,   &
       &    nx,ny,nu,nv, duv,   &
       &    g_weight, g_v, do_weig,  &
       &    wcol,mcol,sblock,cpu0,error,uvma,0,abort,0)
    if (abort) then
      call map_message(seve%w,task,'Aborted by user')
      error = .true.
      return
    endif
    !
    hdirty%loca%addr = locwrd(ddirty)
    call gdf_get_extrema (hdirty,error)
    !
    ! Re-shape the beam, and reset the 4-D pointer, 
    ! but show it as a 3-D array in SIC
    if (nb.gt.1) then
      dbeam(:,:,:,:) = reshape(hbeam%r3d,[nx,ny,1,nb])
      deallocate(hbeam%r3d)
    endif
    call sic_mapgildas('BEAM',hbeam,error,dbeam)
    !
    hbeam%r4d => dbeam
    hbeam%gil%dim(1:4)=[nx,ny,1,nb]
    hbeam%gil%ndim = 4
    !
    ! Transpose the header appropriately
    hbeam%gil%convert(:,4) = hbeam%gil%convert(:,3)
    hbeam%gil%faxi = 4
    hbeam%char%code(4) = 'VELOCITY' ! Frequency would be better...
    hbeam%gil%convert(:,3) = 1.d0
    hbeam%char%code(3) = 'FIELD'    ! Pseudo-mosaic
    hbeam%gil%ndim = 4
    !
    hbeam%loca%addr = locwrd(dbeam)
    call gdf_get_extrema (hbeam,error)
  endif
  save_data(code_save_beam) = .true.
  save_data(code_save_dirty) = .true.
  !
  call new_dirty_beam
  !
  ! Define Min Max
  d_max = hdirty%gil%rmax
  if (hdirty%gil%rmin.eq.0) then
     d_min = -0.03*hdirty%gil%rmax
  else
     d_min = hdirty%gil%rmin
  endif
  !
  if (task.ne.'MX') goto 99
  !
!!!---------------------------------------------------
  !
  ! Prepare MX part
  limits = sic_present(1,1)
  if (limits) then
     call sic_r4 (line,1,1,ylimn,.true.,error)
     if (error) return
     call sic_r4 (line,1,2,ylimp,.true.,error)
     if (error) return
  else
     ylimp = sqrt (float(method%m_iter+200) *   &
          &      log(float(method%m_iter+1)) ) * method%gain
     if (-hdirty%gil%rmin.gt.1.3*hdirty%gil%rmax) then
        ylimn = ylimp*hdirty%gil%rmin
        ylimp = 0.0
     elseif (-1.3*hdirty%gil%rmin.gt.hdirty%gil%rmax) then
        ylimn = 0.0
        ylimp = ylimp*hdirty%gil%rmax
     else
        ylimn = ylimp*hdirty%gil%rmin
        ylimp = ylimp*hdirty%gil%rmax
     endif
  endif
  np = max(1,hprim%gil%dim(1))
  !
  ! Data checkup
  call clean_data (error)
  if (error) return
  !
  ! Copy the UV Data (eh eh)
  allocate (res_uv(nu,nv),stat=ier)
  res_uv(:,:) = duv
  !
  ! Get the right pointers before starting...
  hclean%r3d => dclean
  hresid%r3d => dresid
  dresid(:,:,:) = ddirty
  hbeam%r4d => dbeam ! Also required for MX
  !
  ! Parameter Definitions
  call beam_unit_conversion(user_method)
  call copy_method(user_method,method)
  method%method = 'MX'
  method%pflux = sic_present(1,0)
  method%pcycle = sic_present(2,0)
  method%qcycle = sic_present(3,0)
  method%pclean = .false.
  method%pmrc = .false.
  !
  call sic_get_inte('FIRST',method%first,error)
  call sic_get_inte('LAST',method%last,error)
  if (method%first.eq.0) method%first = 1
  if (method%last.eq.0) method%last = hdirty%gil%dim(3)
  method%first = max(1,min(method%first,hdirty%gil%dim(3)))
  method%last = max(method%first,min(method%last,hdirty%gil%dim(3)))
  !
  ! Other parameters
  if (user_method%patch(1).ne.0) then
     method%patch(1) = min(user_method%patch(1),nx)
  else
     method%patch(1) = min(nx,max(32,nx/4))
  endif
  if (user_method%patch(2).ne.0) then
     method%patch(2) = min(user_method%patch(2),nx)
  else
     method%patch(2) = min(nx,max(32,nx/4))
  endif
  method%bzone = (/1,1,nx,ny/)
  !
  call check_area(method,hdirty,.false.)
  call check_mask(method,hdirty)
  user_method%do_mask = method%do_mask
  !
  ! Clean Component Structure (once it is defined, i.e. after check)
  allocate(tcc(method%m_iter),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,task,'Memory allocation error on MX CCT')
    error = .true.
    return
  endif
  !
  ! Prepare the CCT data
  nsizes = [nx,ny,nc]
  call cct_prepare(line,nsizes,method,task,error)
  if (error) return
  !
  if (method%pflux) call init_flux90(method,hdirty,ylimn,ylimp,ipen)
  !
  if (method%pcycle) call init_plot(method,hdirty,dresid)
  !
  ! Perform the cleaning
  call mx_clean (themap,huv,res_uv,g_weight,g_v,       &
       &    method,hdirty,hbeam,hclean,hresid,hprim,   &
       &    w_grid,w_mapu,w_mapv,tcc,dcct,d_mask,d_list,      &
       &    sblock, cpu0, uvma)
  !
  if (method%pflux) then
    call close_flux90(ipen,error)
  else
    call gr_execl('CHANGE DIRECTORY <GREG')
  endif
  !
  ! Reset extrema
  hresid%gil%extr_words = 0
  hclean%gil%extr_words = 0
  !
  ! Specify clean beam parameters
  hclean%gil%reso_words = 3
  hclean%gil%majo = method%major
  hclean%gil%mino = method%minor
  hclean%gil%posa = pi*method%angle/180.0
  ! Specify clean beam parameters
  hbeam%gil%reso_words = 3
  hbeam%gil%majo = method%major
  hbeam%gil%mino = method%minor
  hbeam%gil%posa = pi*method%angle/180.0
  save_data(code_save_clean) = .true.
  user_method%nlist = method%nlist
  !
  ! Defines the CCT variable
  call sic_mapgildas ('CCT',hcct,error,dcct)
  !
  ! Cleanup
  deallocate(tcc)
  error = .false.
  !
99 continue
  if (allocated(w_mapu)) deallocate(w_mapu)
  if (allocated(w_mapv)) deallocate(w_mapv)
  if (allocated(w_grid)) deallocate(w_grid)
  if (allocated(fft)) deallocate(fft)
  return
  !
98 call map_message(seve%e,task,'Memory allocation failure')
  error = .true.
  return
  !
102 format(a,f9.2)
end subroutine old_uvmap
