module language_versions
  character(len=20) :: v_display   ! Display
  character(len=20) :: v_clean     ! CLEAN
  character(len=20) :: v_cal       ! CALIBRATE
  character(len=20) :: v_advanced  ! ADVANCED
  character(len=20) :: v_bundles   ! BUNDLES
  character(len=20) :: v_imager    ! IMAGER
  character(len=20) :: v_program = '3.6-00  15-Nov-2022'   ! Global IMAGER Version (matching Documentation)
end module language_versions
!
subroutine load_display
  use gkernel_interfaces
  use language_versions
  use imager_interfaces, only : run_display
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER & VIEWER
  !   Load Imager Language:
  !   DISPLAY\
  !----------------------------------------------------------------------
  integer, parameter :: mshow=31
  character(len=16) vshow(mshow) 
  data vshow / ' CATALOG', '*COLOR', &
      & ' EXPLORE', '/ADD', '/NOPAUSE',                           &
      & ' EXTRACT',                                               & 
      & ' FIND', '/SPECIES',                         & ! Find lines in a given range
      & ' INSPECT_3D', &
      & ' LOAD', '/FREQUENCY', '/PLANES', '/RANGE',  & ! READ DATA command equivalent
      & ' POPUP', &
      & ' SET', '/DEFAULT',       &  ! The /DEFAULT option is for compatibility with GreG 
      & ' SHOW', '/SIDE',         &
      & ' SLICE',                 & 
      & ' SPECTRUM', '/CORNER', '/MEAN', '/PLANES', '/SUM', & 
      & ' STATISTIC', '/EDGE', '/NOISE', '/UPDATE',  & ! Image statistic
      & ' VIEW', '/NOPAUSE', '/OVERLAY' /
  logical, parameter :: old=.false.
  !
  ! DISPLAY\ Language versions  v_display
  if (old) then
  v_display = '1.0-00 21-Oct-2021' ! First DISPLAY\ Language
  v_display = '1.2-00 23-Oct-2021' ! Also move EXPLORE here
  v_display = '1.3-00 23-Oct-2021' ! Also move STATISTIC here
  v_display = '1.3-01 10-Nov-2021' ! Improve CATALOG command
  v_display = '1.4-00 18-Nov-2021' ! STATISTIC /UPDATE command
  v_display = '1.4-01 23-Nov-2021' ! Allow re-entrant calls for STATISTIC
  v_display = '1.5-00 10-Jan-2022' ! LOAD command
  v_display = '1.5-01 30-Jan-2022' ! Improve CATALOG command
  v_display = '1.5-06 16-May-2022' ! Ensure STATISTIC works correctly on big data
  v_display = '1.6-00 14-Oct-2022' ! Include the SPECTRUM command calling GreG code
  v_display = '1.6-01 15-Oct-2022' ! Include the POPUP command
  v_display = '1.6-02 04-Nov-2022' ! Include the SET command
  v_display = '1.6-03 07-Nov-2022' ! Add the /DEFAULT option to the SET command
  endif
  v_display = '1.7-00 08-Nov-2022' ! Move the EXTRACT and SLICE command here
  !
  call sic_begin('DISPLAY','gag_doc:hlp/imager-help-display.hlp', mshow, vshow, &
    & v_display//'S.Guilloteau', run_display, gr_error)
end subroutine load_display
!
subroutine init_display 
  call load_display
  call setup_display
  call gprompt_set('VIEWER')
end subroutine init_display
!
subroutine setup_display
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER
  !   Initialize Imager Language DISPLAY\ variables
  !----------------------------------------------------------------------
  use clean_def
  use clean_arrays
  use clean_default
  use clean_types
  logical :: error
  !
  error = .false.
  MappingError = .false.
  call sic_def_logi('MAPPING_ERROR',MappingError,.false.,error)
  !
  ! Re-define the DO_LOOP variable for VIEW
  call sic_delvariable('DO_LOOP',.false.,error)
  call sic_def_logi('DO_LOOP',view_do_loop,.false.,error)
  view_do_loop = .true.
  !
  call sic_delvariable('SHOW_SIDE',.false.,error)
  call sic_def_logi('SHOW_SIDE',show_side,.false.,error)
  show_side = .false.
  !
  ! Angular Unit of Angle-like axes
  call sic_def_inte('ANGLE_UNIT',iangle_unit,0,0,.false.,error)
  call sic_delvariable('SIZE',.false.,error)
  call sic_delvariable('CENTER',.false.,error)
  call sic_def_dble('SIZE',area_size,1,2,.false.,error)
  call sic_def_dble('CENTER',area_center,1,2,.false.,error)
  !
end subroutine setup_display
!
subroutine init_clean
  use gkernel_interfaces
  use language_versions
  use gbl_message
  use imager_interfaces, except_this=>init_clean
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER
  !   Initialize Imager Languages:
  !   CLEAN\
  !   CALIBRATE\
  !   ADVANCED\
  !   BUNDLES\
  !----------------------------------------------------------------------
  use clean_def
  use clean_arrays
  use clean_default
  use clean_types
  use preview_mod
  !
  ! vocabulaire de CLEAN
  integer, parameter :: mclean=142
  character(len=16) :: vclean(mclean) 
  data vclean /' DUMP',   &
       ' ALMA', '/FLUX', '/PLOT', '/QUERY', '/NOISE', '/METHOD', &
       ' BUFFERS', &
       ' CCT_CLEAN', ' CCT_CONVERT', ' CCT_MERGE', &
       ' CLEAN', '/FLUX', '/PLOT', '/QUERY', '/NITER', '/ARES', '/RESTART', &  !No conflict with ALMA command
       ' DISCARD',  & 
       ' FIT', '/JVM', '/THRESHOLD', &
       ' MAP_COMBINE', '/BLANKING', '/FACTOR', '/THRESHOLD',         &
       ' MAP_COMPRESS', ' MAP_INTEGRATE',                            &
       ' MAP_REPROJECT', '/BLANKING', '/LIKE','/PROJECTION', '/SYSTEM', '/X_AXIS', '/Y_AXIS', & 
       ' MAP_RESAMPLE', '/LIKE',          & 
       ' MAP_SMOOTH', '/ASYMMETRIC', &
       ' MOSAIC', &
       ' MX', '/FLUX', '/PLOT', '/QUERY', &
       ' PRIMARY', '/TRUNCATE', &
       ' READ', '/COMPACT', '/FREQUENCY', '/PLANES', '/RANGE',  '/NOTRAIL',  &
       ' SPECIFY', '/FOR', &
       ' SUPPORT', '/CURSOR', '/MASK', '/PLOT', '/RESET', '/THRESHOLD', '/VARIABLE', &
       ' UV_BASELINE', '/CHANNELS', '/FILE', '/FREQUENCY', '/RANGE', '/VELOCITY', '/WIDTH', &
       ' UV_CHECK', '/FILE', ' UV_COMPRESS', '/CONTINUUM', '/FILE', & 
       ' UV_CONTINUUM','/INDEX', '/RANGE', &
       ' UV_EXTRACT', '/FIELDS', '/FILE', '/RANGE', &
       ' UV_FLAG', '/ANTENNA', '/DATE', '/FILE', '/RESET', &
       ' UV_FIELDS', '/CENTER', '/FILE', &
       ' UV_FILTER', '/CHANNELS', '/FILE', '/FREQUENCY', '/RANGE', '/VELOCITY', '/WIDTH', '/RESET', '/ZERO', &
       ' UV_HANNING', '/FILE', &
       ' UV_MAP', '/TRUNCATE',  '/FIELDS', '/RANGE', '/CONTINUUM', '/INDEX', &
       ' UV_RESAMPLE', '/FILE', '/LIKE', & 
       ' UV_RESIDUAL', '/FIELDS', '/OUTPUT', ' UV_RESTORE', '/COPY', '/SPEED', &
       ' UV_REWEIGHT', '/FLAG', '/FILE', '/RANGE', &
       ' UV_SHIFT',  '/FILE', & 
       ' UV_SMOOTH', '/ASYMMETRIC', '/FILE', &
       ' UV_SPLIT', '/CHANNELS', '/FILE', '/FREQUENCY', '/RANGE', '/VELOCITY', '/WIDTH', &
       ' UV_STAT',   &
       ' UV_TIME', '/FILE', '/WEIGHT',      &
       ' UV_TRIM', '/FILE', ' UV_TRUNCATE', &
       ' WRITE', '/APPEND ', '/RANGE', '/REPLACE', '/TRIM'/
  !
  integer, parameter :: nself=20
  character(len=16) vself(nself)
  data vself/  & 
       ' APPLY', '/FILE',  '/FLAG',      & ! Apply Self calibration
       ' COLLECT', '/FLAG',              & ! Gather Independent Calibrations
       ' DERIVE',                        & ! Antenna to Baseline gain
       ' MODEL', '/MINVAL',              & ! UV model from UV_FIT or CLEAN
                 '/OUTPUT', '/MODE',     & !  - /OUTPUT must be second option
       ' SCALE_FLUX',                    & ! Verify day-to-day Flux consistency
       ' SOLVE', '/MODE',                & ! Solve for Amplitude and/or Phase
       ' TRANSFORM',                     & ! Image transformations
       ' UV_SELECT',                     & ! Select UV_DATA 
       ' UV_SELF', '/RANGE', '/RESTORE', & ! Image for Self-calibration
       ' UV_SORT', '/FILE'    /            ! Sort UV data
  !
  integer, parameter :: nadv=55+2
  character(len=16) vadv(nadv)
  data vadv/  & 
    & ' FEATHER', '/FILE', '/REPROJECT',                        &
    & '*FLUX', '/CCT', ' HOW_TO',                               &
    & ' MAP_POLAR', '/BACKGROUND', '/COMPUTE', '/STEP',         &
    & ' MAP_CONTINUUM', '/METHOD',  ' MASK', ' MFS',            & 
    & ' MOMENTS', '/CUT', '/MASK', '/METHOD', '/RANGE', '/THRESHOLD',  &
    & ' PROPER_MOTION', '/FILE',                                &
    & ' STOKES', '/FILE',                                       & 
    & ' UV_ADD', '/FILE',                                       &
    & ' UV_CIRCLE', '/FIT', '/SAMPLING', '/ZERO',               & 
    & ' UV_CORRELATE', '/FILE',                                 &
    & ' UV_DEPROJECT', '/FIT',                                  &
    & '*UV_FIT', '/QUIET', '/SAVE', '/WIDGET', '/RESULT', '/UVRANGE',  &
    & ' UV_MERGE', '/FILES', '/MODE', '/SCALES', '/WEIGHTS',    &
    & ' UV_MOSAIC', &
    & '*UV_PREVIEW', '/BROWSE', '/FILE',                        &  
    & ' UV_RADIAL', '/FIT', '/SAMPLING', '/ZERO',               & 
    & ' UV_SHORT', '/CHECK', '/REMOVE', ' XY_SHORT' /
  ! 
  integer, parameter  :: ncomp=16
  character(len=16) vcomp(ncomp)
  data vcomp/ & 
    & ' COMBINE', '/BLANKING', '/FACTOR', '/THRESHOLD',         &
    & ' KEPLER', '/HFS', '/MASK', '/RESET', '/VELOCITY',  '/VSYSTEM',  '/WIDGET', &
    & '*SPECTRAL_CLEAN',                                        &
    & '*SELFCAL', '/WIDGET',                                    &
    & ' UV_DETECT', '/FILE' /                                  
  !
  integer, parameter :: nimag=15
  character(len=16) vimag(nimag)
  data vimag/ &
    & ' CLARK', '/FLUX', &
    & ' HOGBOM', '/FLUX', &
    & ' MRC', '/FLUX', &
    & ' MULTISCALE', '/FLUX', &
    & ' PIPELINE', '/MODE', '/WIDGET', &
    & ' SDI', '/FLUX', '/PLOT', &
    & ' TIPS' /
  ! 
  logical :: error
  integer :: iout
  logical, parameter :: old=.false.
  !
  ! CLEAN Language versions   v_clean
  if (old) then
  v_clean = '4.0-01 08-Mar-2018' ! First IMAGER release with new Variable names (may still change)
  v_clean = '4.1-01 08-Mar-2018' ! IMAGER release with Self Calibration commands
  v_clean = '4.1-02 15-Mar-2018' ! Bug correction in Self Calibration commands
  v_clean = '4.1-02 02-May-2018' ! Split with CALIBRATE\ language 
  v_clean = '4.2-01 03-Jul-2018' ! Add /RANGE options to UV_FILTER and UV_BASELINE
  v_clean = '4.3-01 05-Sep-2018' ! Minor change in SHOW NOISE to identify properly Dirty and Clean data
  v_clean = '4.3-02 05-Nov-2018' ! Debug MX, improve MULTI and cleanup messages  
  v_clean = '4.3-03 14-Nov-2018' ! Add the UV_TRIM command, support READ CCT /RANGE
  v_clean = '4.4-01 14-Jan-2019' ! Add the UV_HANNING command
  v_clean = '4.4-02 17-Jan-2019' ! Add the UV_EXTRACT command
  v_clean = '4.4-03 04-Feb-2019' ! Add the /FILE option to UV_BASELINE,UV_FILTER, UV_SPLIT and UV_SORT
  v_clean = '4.4-04 07-Feb-2019' ! Bug correction in UV_MAP for Phase offset mosaics 
  v_clean = '4.4-05 08-Feb-2019' ! UV_TRIM /FILE syntax
  v_clean = '4.4-06 12-Feb-2019' ! Protect against incomplete UV data (Nvisi < dim(2))
  v_clean = '4.4-07 13-Feb-2019' ! Operational version of UV_SORT /FILE (still slow)
  v_clean = '4.4-08 14-Feb-2019' ! Operational version of UV_SORT /FILE (reasonable now) 
  v_clean = '4.4-09 15-Feb-2019' ! UV_TIME /FILE syntax
  v_clean = '4.4-10 25-Feb-2019' ! UV_RESAMPLE, UV_COMPRESS, UV_HANNING /FILE syntax
  v_clean = '4.5-01 27-Feb-2019' ! Move STOKES command to ADVANCED\ language
  v_clean = '4.5-02 14-Mar-2019' ! Improve the STATISTIC and SUPPORT commands behaviour 
  v_clean = '4.5-03 29-Mar-2019' ! Support for SHOW SOURCES
  v_clean = '5.0-01 05-Apr-2019' ! Remove CLARK, ..., SDI commands, leave only CLEAN
  v_clean = '5.0-02 05-Apr-2019' ! Move COLOR to CLEAN\
  v_clean = '5.0-03 29-Apr-2019' ! Option /CONT for UV_MAP
  v_clean = '5.0-04 02-May-2019' ! BUFFERS command
  v_clean = '5.0-05 06-May-2019' ! /NOPAUSE option to VIEW command
  v_clean = '5.0-06 07-May-2019' ! UV_STAT DEFAULT and associated variables
  v_clean = '5.0-07 25-May-2019' ! /INDEX option for UV_MAP
  v_clean = '5.1-01 05-Jun-2019' ! Command UV_REWEIGHT re-written
  v_clean = '5.1-02 15-Jun-2019' ! UV_EXTRACT /FIELDS
  v_clean = '5.1-03 25-Jun-2019' ! UV_FIELDS command 
  v_clean = '5.1-04 03-Jul-2019' ! UV_COMPRESS /CONTINUUM option
  v_clean = '5.1-05 04-Jul-2019' ! Remove obsolete /SELF option of UV_MAP & UV_RESTORE
  v_clean = '5.2-01 03-Sep-2019' ! Move STATISTIC command out of this language
  v_clean = '5.2-02 13-Sep-2019' ! Move UV_SORT command out of this language
  v_clean = '5.2-03 25-Sep-2019' ! UV_FLAG bug correction
  v_clean = '5.2-04 07-Oct-2019' ! UV_MAP 'Map_Center' bug correction (header not updated)
  v_clean = '5.2-05 10-Oct-2019' ! WRITE does not accept ambiguities
  v_clean = '5.2-06 10-Oct-2019' ! VIEW with no argument just re-displays
  v_clean = '5.3-01 10-Oct-2019' ! Support for the CONTINUUM image
  v_clean = '5.3-02 11-Oct-2019' ! MAP_REPROJECT, MAP_COMBINE commands (+syntax MAP_RESAMPLE)
  v_clean = '5.3-03 13-Oct-2019' ! SHOW /NOPAUSE and VIEW /OVERLAY commands
  v_clean = '5.3-04 16-Oct-2019' ! SHOW /SIDE instead of /NOPAUSE and new MASK THRESHOLD command
  v_clean = '5.3-05 06-Nov-2019' ! /FILE option of UV_CHECK
  v_clean = '5.3-06 20-Nov-2019' ! /FILE option of UV_SHIFT
  v_clean = '5.3-07 06-Feb-2020' ! Bug correction for MAP_COMPRESS syntax
  v_clean = '5.3-08 20-Feb-2020' ! Size computation for Large data set may exceed 2GBytes
  v_clean = '5.3-09 21-Feb-2020' ! Bug correction for Mosaic in Robust weighting 
  v_clean = '5.3-10 24-Feb-2020' ! /LIKE option of MAP_RESAMPLE
  v_clean = '5.3-11 05-Mar-2020' ! Bug correction in MULTI
  v_clean = '5.3-12 05-Mar-2020' ! Bug correction about buffers selected by UV_SELECT and/or UV_MAP /CONT
  v_clean = '5.3-13 11-Mar-2020' ! Bug correction in MAP_RESAMPLE (overflow of channel ranges)
  v_clean = '5.3-14 25-Mar-2020' ! Remove a restriction in MAP_RESAMPLE (overflow of channel ranges)
  v_clean = '5.3-15 25-Mar-2020' ! Improve the /FLUX display in CLEAN command
  v_clean = '5.3-16 13-May-2020' ! Improve the WRITE MODEL command 
  v_clean = '5.4-01 21-May-2020' ! /FOR option of SPECIFY
  v_clean = '5.4-02 09-Jul-2020' ! Command DISCARD
  v_clean = '5.4-03 01-Sep-2020' ! Option /THRESHOLD for command FIT
  v_clean = '5.4-04 03-Sep-2020' ! Improve UV_TIME command : more consistent defaults
  v_clean = '5.4-05 11-Sep-2020' ! Bug correction in MASK APPLY (Blanking was not updated)
  v_clean = '5.4-06 15-Sep-2020' ! Optimize MAP_RESAMPLE if no resampling needed
  v_clean = '5.5-01 26-Oct-2020' ! Command UV_SMOOTH
  v_clean = '5.6-01 07-Dec-2020' ! Command CCT_MERGE and CCT_CONVERT
  v_clean = '5.6-02 14-Dec-2020' ! STATISTIC /EDGE option
  v_clean = '5.6-03 14-Dec-2020' ! /ARES Argument must be Real array
  v_clean = '5.6-04 14-Jan-2021' ! Debug VIEW and SHOW 4-D Beams
  v_clean = '5.6-05 19-Jan-2021' ! Make sure UV_MAP works on Mosaics even in Memory limited conditions
  v_clean = '5.7-01 31-Jan-2021' ! Command CCT_CLEAN
  v_clean = '5.7-02 31-Mar-2021' ! FIT command make one fit per pointing
  v_clean = '5.7-03 01-Apr-2021' ! MULTISCALE is now operational for Mosaics
  v_clean = '5.7-04 01-Jun-2021' ! Bug correction for MULTISCALE in 4-D Beams
  v_clean = '5.7-05 01-Jun-2021' ! More flexible FIT command
  v_clean = '5.8-01 17-Jun-2021' ! SPECIFY FREQUENCIES command for VLA data
  v_clean = '5.8-02 28-Jun-2021' ! UV_SHIFT bug correction for "false" mosaics (ID and MOFF columns)
  v_clean = '5.8-03 28-Jun-2021' ! UV_SORT bug correction for /FILE option
  v_clean = '6.0-00 21-Oct-2021' ! Split Language CLEAN\ into CLEAN\ and DISPLAY\ (no \at end please...)
  v_clean = '6.0-01 09-Nov-2021' ! Bug correction in READ UV: undefine PREVIEW%, avoid write UV data in WRITE *
  v_clean = '6.1-00 17-Nov-2021' ! Add /JVM option to command FIT
  v_clean = '6.1-01 17-Nov-2021' ! CLEAN Variable ? syntax
  v_clean = '6.1-02 17-Nov-2021' ! UV_MAP Variable ? syntax
  v_clean = '6.1-03 19-Nov-2021' ! UV_REWEIGHT syntax bug correction
  v_clean = '6.1-04 23-Nov-2021' ! Use the BEAM_JVM value in UV_RESTORE - (Defaults to 1 if not available)
  v_clean = '6.2-00 02-Dec-2021' ! Add the /LIKE option to UV_RESAMPLE 
  v_clean = '6.2-01 16-Dec-2021' ! Optimize parallel processing
  v_clean = '6.2-02 17-Dec-2021' ! Debug MAP_BEAM_STEP = 1 mode
  v_clean = '6.2-03 17-Dec-2021' ! UV_CHECK beam works better
  v_clean = '6.2-04 18-Dec-2021' ! UV_TIME now works for UV tables with per-channel weights
  v_clean = '6.2-05 20-Dec-2021' ! UV_RESAMPLE must reset BEAM_RANGES variable
  v_clean = '6.3-00 21-Dec-2021' ! Use new way of defining Dirty Beams number and properties.
  v_clean = '6.3-01 03-Jan-2021' ! Parallel programming optimization for CLEAN
  v_clean = '6.3-02 06-Jan-2022' ! Allow direct READ of FITS data cubes.
  v_clean = '6.3-03 07-Jan-2022' ! READ /PLANES option now works as expected
  v_clean = '6.3-04 11-Jan-2022' ! UV_FIELD bug correction 
  v_clean = '6.3-05 18-Jan-2022' ! READ SINGLE bug correction
  v_clean = '6.3-06 20-Jan-2022' ! READ  with FITS format improvement
  v_clean = '6.3-07 08-Feb-2022' ! Allow to use Fixed beamsize in UV_RESTORE
  v_clean = '6.3-08 08-Feb-2022' ! CLEAN_STOP syntax in CLEAN
  v_clean = '6.3-09 07-Mar-2022' ! Allow WRITE BEAM /APPEND in all cases
  v_clean = '6.3-10 07-Mar-2022' ! Bug correction in beam derivation if ANGLE is specified
  v_clean = '6.3-11 09-Mar-2022' ! Bug correction in CCT_MERGE : Concatenation was improper
  v_clean = '6.4-00 18-Mar-2022' ! Implement the /RESTART option of CLEAN
  v_clean = '6.4-01 22-Mar-2022' ! Flag out of band channels in UV_RESAMPLE
  v_clean = '6.4-02 03-May-2022' ! /RESET option of UV_FILTER
  v_clean = '6.4-03 10-May-2022' ! Bug correction in UV_SHIFT and PROPER_MOTION
  v_clean = '6.4-04 02-Jun-2022' ! Make SPECIFY TELESCOPE work on Images too
  v_clean = '6.4-05 09-Jun-2022' ! Debug UV_FLAG command
  v_clean = '6.4-06 29-Jun-2022' ! Make UV_EXTRACT work with Random Frequency Axis
  v_clean = '6.4-07 29-Jun-2022' ! Make READ handle properly Random Frequency Axis in UVFITS format
  endif
  v_clean = '6.5-00 05-Sep-2022' ! Add the /ASYMMETRIC option of MAP_SMOOTH and UV_SMOOTH
  v_clean = '6.5-01 27-Sep-2022' ! Add the UNIT specifier to MAP_REPROJECT
  v_clean = '6.5-02 07-Oct-2022' ! Bug correction in Beam Consistency checking
  v_clean = '6.5-03 10-Oct-2022' ! Use a Channel mask array to hide line-emitting parts of the spectrum
  v_clean = '6.5-04 07-Nov-2022' ! Make sure MAP_CENTER is also used for Mosaics
  v_clean = '6.5-05 14-Nov-2022' ! Bug in UV_SHIFT: Pointing center was incorrectly reset in /FILE option for single fields
  v_clean = '6.5-06 14-Nov-2022' ! Bug in UV_MOSAIC: Wrong identification of arguments
  v_clean = '6.5-07 14-Nov-2022' ! Make sure MAP_CENTER is never forgotten (Mosaic stills needs checking)
  v_clean = '6.5-08 15-Nov-2022' ! UV_TIME crashed on Mosaics
  !
  ! ADVANCED\ Language versions   v_advanced
  if (old) then
  v_advanced = '1.0-01 01-Dec-2017' ! Command UV_PREVIEW 
  v_advanced = '1.0-02 08-Mar-2018' ! Commands UV_DEPROJECT  and UV_RADIAL
  v_advanced = '1.1-02 08-Mar-2018' ! Commands MODEL, SOLVE /MODE, CALIBRATE and UV_MAP /SELF 
  v_advanced = '1.2-01 10-Apr-2018' ! Template for Command UV_SHORT
  v_advanced = '1.2-02 26-Apr-2018' ! Operational Command UV_SHORT - Still problems with repetitions.
  v_advanced = '1.3-01 03-Jul-2018' ! Rename CALIBRATE into APPLY -- Split with CALIBRATE\ language
  v_advanced = '1.3-02 26-Sep-2018' ! Add SLICE and EXTRACT commands
  v_advanced = '1.3-03 26-Sep-2018' ! allow support for VIEW (or SHOW) AnySicImage
  v_advanced = '1.3-04 26-Sep-2018' ! UV_PREVIEW bug correction
  v_advanced = '1.4-01 23-Nov-2018' ! Rename language to ADVANCED, and insert SELFCAL command
  v_advanced = '1.4-02 26-Nov-2018' ! Add UV_FIT command 
  v_advanced = '1.4-03 28-Nov-2018' ! Add MOMENTS command 
  v_advanced = '1.4-04 28-Nov-2018' ! Add CATALOG command 
  v_advanced = '1.4-05 15-Jan-2019' ! Better version of the CATALOG command - Drives a script
  v_advanced = '1.4-06 23-Jan-2019' ! Correction of a bug in UV_PREVIEW
  v_advanced = '1.5-01 24-Jan-2019' ! Add the ADVANCED\MASK command
  v_advanced = '1.5-02 04-Feb-2019' ! Debug the UV_FIT command -- Add the /FILE option for UV_PREVIEW
  v_advanced = '1.6-01 26-Feb-2019' ! Add the UV_ADD command (similar to UV_DOPPLER in ASTRO)
  v_advanced = '1.6-02 27-Feb-2019' ! Move the STOKES command from the Clean\ language
  v_advanced = '1.6-03 19-Mar-2019' ! Add the UV_MERGE command (simpler than the task...)
  v_advanced = '1.6-04 20-Mar-2019' ! Add the COLOR and FLUX commands  
  v_advanced = '1.6-05 28-Mar-2019' ! /SAVE option of UV_FIT 
  v_advanced = '1.6-06 14-May-2019' ! HOW_TO command
  v_advanced = '1.7-01 19-May-2019' ! FEATHER command
  v_advanced = '1.7-02 24-May-2019' ! XY_SHORT command
  v_advanced = '1.8-01 20-Jul-2019' ! UV_CIRCLE command
  v_advanced = '1.8-02 22-Jul-2019' ! /RESULT option of UV_FIT command
  v_advanced = '1.8-03 31-Jul-2019' ! PROPER_MOTION command
  v_advanced = '1.8-04 23-Sep-2019' ! Improved UV_FIT command
  v_advanced = '1.8-05 16-Oct-2019' ! COMBINE command
  v_advanced = '1.8-06 17-Oct-2019' ! FLUX and MAP_CONTINUUM commands bug correction
  v_advanced = '1.9-00 29-Nov-2019' ! Move the COMBINE and SELFCAL to the BUNDLES\ language
  v_advanced = '1.9-01 20-Jan-2020' ! Speed-up (considerably) UV_PREVIEW
  v_advanced = '1.9-02 20-Jan-2020' ! Add the UV_CORRELATE command
  v_advanced = '1.9-03 06-Feb-2020' ! Change name of SLICE buffer to SLICED for Python
  v_advanced = '1.9-04 12-Feb-2020' ! Allow UV_MERGE to merge Line tables in CONTINUUM mode
  v_advanced = '1.9-05 17-Feb-2020' ! UV_PREVIEW bug correction. Also affects MAP_CONTINUUM
  v_advanced = '1.9-06 18-Feb-2020' ! Improved STATISTIC command
  v_advanced = '2.0-00 19-Feb-2020' ! MOMENTS /CUTS option and improved SHOW MOMENTS
  v_advanced = '2.0-01 20-Feb-2020' ! UV_SHORT ? proper support
  v_advanced = '2.0-02 20-Feb-2020' ! UV_SHORT with non-circular Short-Spacing beam
  v_advanced = '2.0-03 26-Feb-2020' ! UV_SHORT /CHECK option
  v_advanced = '2.1-00 27-Feb-2020' ! UV_SHORT with minimum baseline and single-fields
  v_advanced = '2.1-01 29-Mar-2020' ! Improvements in MOMENTS
  v_advanced = '2.1-02 13-May-2020' ! Improvements in UV_CORRELATE
  v_advanced = '2.1-03 14-May-2020' ! Bug correction in UV_PREVIEW and UV_CORRELATE
  v_advanced = '2.1-04 28-May-2020' ! UV_PREVIEW /BROWSE command
  v_advanced = '2.1-05 10-Jun-2020' ! UV_PREVIEW /FILE Fich [Drop] command
  v_advanced = '2.1-07 26-Oct-2020' ! Debug completely UV_MERGE command (or 2021 ?)
  v_advanced = '2.2-00 26-Oct-2021' ! Move CATALOG and FIND to DISPLAY\ Language
  v_advanced = '2.2-01 18-Nov-2021' ! UV_SHORT V 3.9 syntax and Help improvement
  v_advanced = '2.3-00 26-Nov-2021' ! MAP_CONTINUUM /METHOD option
  v_advanced = '2.4-00 02-Dec-2021' ! UV_MOSAIC command
  v_advanced = '2.4-01 19-Dec-2021' ! UV_MERGE /MODE CONCATENATE command
  v_advanced = '2.4-02 19-Dec-2021' ! EXTRACT and SLICE syntax improvements.
  v_advanced = '2.4-03 19-Dec-2021' ! EXTRACT now allows expansion
  v_advanced = '2.4-04 08-Feb-2022' ! UV_FIT prints primary beam corrected flux
  v_advanced = '2.4-05 17-Apr-2022' ! STOKES can now work on internal buffer
  v_advanced = '2.4-06 22-Apr-2022' ! UV_PREVIEW syntax change & improvement
  v_advanced = '2.4-07 25-May-2022' ! MOMENTS SMOOTH keyword
  v_advanced = '2.4-08 07-Jun-2022' ! UV_FIT /UVRANGE Min Max
  v_advanced = '2.4-09 29-Jun-2022' ! STOKES: Correct derivation from H-V polarization
  endif
  v_advanced = '2.5-00 05-Jul-2022' ! First operational version of MAP_POLAR
  v_advanced = '2.5-01 08-Sep-2022' ! Bug correction in EXTRACT command
  v_advanced = '2.5-02 13-Oct-2022' ! Allow adding columns in UV_ADD /FILE
  v_advanced = '2.6-00 08-Nov-2022' ! Move EXTRACT and SLICE to DISPLAY\ language
  !
  ! CALIBRATE\ Language versions  v_cal
  if (old) then
  v_cal = '1.0-01 03-Jul-2018' ! Split with CALIBRATE\ language 
  v_cal = '1.1-01 07-Jul-2018' ! /MINVAL option instead of /MINFLUX for MODEL 
  v_cal = '1.2-01 20-Mar-2019' ! Rename command FLUX_SCALE into SCALE_FLUX 
  v_cal = '1.3-01 16-Jul-2019' ! Command COLLECT 
  v_cal = '1.3-02 16-Jul-2019' ! Command DERIVE  
  v_cal = '1.4-01 03-Sep-2019' ! Move STATISTIC command to this language
  v_cal = '1.4-02 06-Sep-2019' ! Implement UV_SELECT selection command
  v_cal = '1.4-03 13-Sep-2019' ! Move UV_SORT command to this language
  v_cal = '1.4-04 26-Sep-2019' ! APPLY command modification ((weights and flags, r1.11)
  v_cal = '1.5-01 08-Nov-2019' ! COLLECT /FLAG option and bug correction
  v_cal = '1.6-01 17-Jan-2020' ! FIND Fmin Fmax command
  v_cal = '1.7-01 19-Mar-2020' ! MODEL /MODE IMAGE  implemented. 
  v_cal = '1.7-04 13-May-2020' ! MODEL command defines UV_MODEL
  v_cal = '1.7-05 26-May-2020' ! Finish MODEL /MODE IMAGE implementation
  v_cal = '1.7-06 15-Sep-2020' ! Add time tolerance in command APPLY
  v_cal = '1.8-00 28-Oct-2021' ! Move STATISTIC command to DISPLAY\ language
  v_cal = '1.8-01 11-Jan-2022' ! Protect MODEL against undefined data units.
  endif
  v_cal = '1.8-02 17-Jan-2022' ! Add the Frequency keyword to MODEL /MODE 
  !
  ! BUNDLES\  Language versions  v_bundles
  if (old) then
  v_bundles = '1.0-00 29-Nov-2019' ! Move the COMBINE and SELFCAL to the BUNDLES\ language
  v_bundles = '1.1-00 11-Mar-2020' ! Add the EXPLORE tool
  v_bundles = '1.2-00 25-Mar-2020' ! Add the KEPLER tool
  v_bundles = '1.2-01 29-Mar-2020' ! Add the KEPLER_VELO variable
  v_bundles = '1.2-02 13-May-2020' ! Add the KEPLER /RESET option
  v_bundles = '1.3-01 22-May-2020' ! Add the UV_DETECT /COMPRESS command
  v_bundles = '1.3-02 26-May-2020' ! Better UV_DETECT command
  v_bundles = '1.3-03 30-May-2020' ! First correct UV_DETECT command
  v_bundles = '1.3-04 10-Jun-2020' ! Handle KEPLER_X0 and KEPLER_Y0 properly
  v_bundles = '1.3-05 09-Jun-2020' ! Re-size CONTINUUM data if needed in UV_RESTORE
  v_bundles = '1.4-01 30-Jul-2020' ! /VDISK option for KEPLER
  v_bundles = '1.4-02 01-Sep-2020' ! /VELOCITY option for KEPLER
  v_bundles = '1.4-03 09-Sep-2020' ! /MASK option for KEPLER
  v_bundles = '1.4-04 11-Sep-2020' ! /VDISK renamed to /VSYSTEM in KEPLER
  v_bundles = '1.5-05 14-Dec-2020' ! SPECTRAL_CLEAN command
  v_bundles = '1.5-06 14-Dec-2020' ! /HFS option to KEPLER SHOW
  v_bundles = '1.6-00 28-Oct-2021' ! Move command EXPLORE to DISPLAY\  Language
  endif
  v_bundles = '1.6-01 18-Nov-2021' ! Finish moving command EXPLORE to DISPLAY\  Language
  v_bundles = '1.6-02 29-Sep-2022' ! /WIDGET option to KEPLER
  !
  ! IMAGER\ Language version v_imager
  v_imager =  '1.0-00 23-Mar-2022' ! First built-in release of IMAGER\ language
  v_imager =  '1.0-01 10-May-2022' ! Insert the PIPELINE command in stack
  v_imager =  '1.0-02 30-May-2022' ! Add the /PLOT option to SDI
  v_imager =  '1.1-00 17-Oct-2022' ! Move the POPUP command to DISPLAY\
  v_imager =  '1.1-01 02-Nov-2022' ! Add the /MODE option to PIPELINE
  !
  call load_display                ! Load DISPLAY\ language
  !
  ! Load the other languages
  call sic_begin('CLEAN','gag_doc:hlp/imager-help-clean.hlp',mclean, vclean, v_clean//'S.Guilloteau', run_clean, gr_error)
  call sic_begin('CALIBRATE','gag_doc:hlp/imager-help-calib.hlp',nself, vself, v_cal//'S.Guilloteau', run_self, gr_error)
  call sic_begin('ADVANCED','gag_doc:hlp/imager-help-news.hlp',nadv, vadv, v_advanced//'S.Guilloteau', run_advanced, gr_error)
  call sic_begin('BUNDLES','gag_doc:hlp/imager-help-bundles.hlp',ncomp, vcomp, v_bundles//'S.Guilloteau', run_bundles, gr_error)
  call sic_begin('IMAGER','gag_doc:hlp/imager-help-imager.hlp',nimag,vimag,v_imager//'S.Guilloteau', run_imager, gr_error)
  !
  call setup_display  ! Setup the DISPLAY\ language
  ! 
  call omp_setup      ! Setup Open-MP context
  !
  call sic_def_logi('CALL_TREE',call_debug,.false.,error)
  !
  user_method%do_mask = .true.  ! Unclear why ...
  call sic_def_logi('METHOD_DOMASK',user_method%do_mask,.false.,error) ! To check
  nullify(duvr)
  nullify(duvs)
  call define_var(error)
  !
  call sic_def_inte('MAP_VERSION',map_version,0,0,.false.,error)
  call sic_def_real('MAP_ROUNDING',map_rounding,0,0,.false.,error)
  call sic_def_inte('MAP_POWER   ',map_power,0,0,.false.,error)
  call gi4_round_forfft(64,iout,error,map_rounding,map_power)
  !
  call sic_def_login('SAVE_DATA',save_data,1,mtype,.false.,error)
  call gprompt_set('IMAGER')
  !
  call sic_get_inte('SIC%RAMSIZE',sys_ramsize,error)
  call feather_comm(' ',error) ! Initialize
  !
  call sic_def_inte('BROWSE_LAST',browse_last,0,0,.false.,error)
  !
end subroutine init_clean
!
subroutine imager_version
  use gbl_ansicodes
  use language_versions
  use imager_interfaces, only : map_message
  use gkernel_types
  use gkernel_interfaces, only : gagout, sic_findfile
  !
  ! Test last IMAGER usage
  !
  character(len=filename_length) :: chain,file
  character(len=20) :: v
  logical :: exist
  integer :: ier
  !
  call gagout(' ')
  call gagout(c_blue//c_bold//'I-IMAGER,  Program version is '//v_program//c_clear)
  call gagout(' ')
  !
  chain = '.imager_last'
  exist = sic_findfile(chain,file,'$HOME/',' ')
  if (exist) then
    open(unit=1,file=file,action='READWRITE')
    read(1,'(A)')
    read(1,'(A)') v
    if (v.ne.v_display) Call gagout(c_red//'W-IMAGER,  DISPLAY\ Language version now '//v_display &
      & //' (last version used '//v//')'//c_clear)
    read(1,'(A)') v
    if (v.ne.v_clean) Call gagout(c_red//'W-IMAGER,  CLEAN\ Language version now '//v_clean &
      & //' (last version used '//v//')'//c_clear)
    read(1,'(A)') v
    if (v.ne.v_cal) Call gagout(c_red//'W-IMAGER,  CALIBRATE\ Language version now '//v_cal &
      & //' (last version used '//v//')'//c_clear)
    read(1,'(A)') v
    if (v.ne.v_advanced) Call gagout(c_red//'W-IMAGER,  ADVANCED\ Language version now '//v_advanced &
      & //' (last version used '//v//')'//c_clear)
    read(1,'(A)') v
    if (v.ne.v_bundles) Call gagout(c_red//'W-IMAGER,  BUNDLES\ Language version now '//v_bundles &
      & //' (last version used '//v//')'//c_clear)
    read(1,'(A)',iostat=ier) v
    if (ier.ne.0) then
      if (v.ne.v_bundles) Call gagout(c_red//'W-IMAGER,  IMAGER\ Language version now '//v_bundles &
        & //' (last version used '//v//')'//c_clear)
    endif
    read(1,'(A)',iostat=ier) 
    if (ier.ne.0) then
      if (v.ne.v_program) Call gagout(c_red//'W-IMAGER,  Main Program version changed, now '//v_program &
        & //' (last version used '//v//')'//c_clear)
    endif
    rewind(1)
  else
    open(unit=1,file=file,status='NEW')  
  endif
  write(1,'(A)') ' '
  write(1,'(A)') v_display
  write(1,'(A)') v_clean
  write(1,'(A)') v_cal
  write(1,'(A)') v_advanced
  write(1,'(A)') v_bundles
  write(1,'(A)') v_imager
  write(1,'(A)') v_program
  close(unit=1)
end subroutine imager_version
!
subroutine run_self (line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_self
  use gkernel_interfaces, only : sic_present, sic_mapgildas, sic_delvariable, sic_lire
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER   Main routine
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  integer, save :: icall=0
  integer :: i
  logical :: quiet
  real(8) :: elapsed
  !
  if (icall.ne.0) then
    Print *,'Rentrant call to RUN_SELF ',comm
    read(5,*) i
  endif
  icall = icall+1
  !
  call report_init(elapsed)
  call quiet_message(line,quiet)
  !
  MappingError = .false.
  call map_message(seve%c,'CALIBRATE',line)
  !
  select case (comm)
  case ('UV_SELF')
    Print *,'Calling UV_MAP_COMM'
    call uv_map_comm(line,comm,error)
  case ('APPLY')
    call uv_calibrate(line,error)
  case ('MODEL') 
    call uv_model_comm(line,error)
  case ('SOLVE') 
    call solve_gain(line,error)
  case ('COLLECT')
    call gather_self(line,comm,error)
  case ('DERIVE')
    call derive_base(line,comm,error)
  case ('SCALE_FLUX')
    call flux_dispatch (line,error)
  case ('UV_SELECT')
    call select_uvdata(line,comm,error)
  case ('UV_SORT')
    if (sic_present(1,0)) then
      call uv_sort_file(line,error)
    else
      call uv_sort_comm(line,error)
    endif
  case ('FIND')
    call catalog_find(line,error)
    quiet = .true.
  case ('TRANSFORM')
    call transform_comm(line,error)
  case default
    call map_message(seve%e,'CALIBRATE','No code for '//comm)
    error = .true.
  end select
  icall = icall-1  
  if (MappingError) error = .true.
  !
  call end_message(comm,elapsed,quiet,error)
  !
end subroutine run_self
!
subroutine run_clean (line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_clean
  use gkernel_types
  use gkernel_interfaces, only : sic_present, sic_mapgildas, sic_delvariable
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER   Main routine for Language CLEAN\
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  type(sic_descriptor_t) :: desc   ! Descriptor
  integer, save :: icall=0
  integer :: i
  logical :: err, found, quiet
  real(8) :: elapsed
  !
  if (icall.ne.0) then
     Print *,'Rentrant call to RUN_CLEAN ',comm
     read(5,*) i
  endif
  icall = icall+1
  !
  call report_init(elapsed)
  call quiet_message(line,quiet)
  !
  MappingError = .false.
  call map_message(seve%c,'CLEAN',line)
  !
  ! Analyze command
  ! First cases do not require to update variables
  select case (comm)
  case ('FIT')
    call clean_beam(line,error)
    quiet = .true.
  case ('DISCARD')
    call comm_discard(line,comm,error)
    quiet = .true.
  case ('COLOR')
    call color_comm(line,error)
    quiet = .true.
  case ('WRITE')
    call write_image(line,error)
  case ('SUPPORT')
    call com_support(line,error)
  case ('UV_STAT')
    call uv_stat_comm(line,error)
  case ('DUMP')
    call debug_all(line,error)
    quiet = .true.
  case ('BUFFERS')
    call buffers_comm(line,error)
    quiet = .true.
  case ('MOSAIC')
    call map_message(seve%w,comm,'Command is obsolescent',3)
    call mode_mosaic (line,error)
    quiet = .true.
  case ('CCT_MERGE')
    call cct_merge_comm(line,error)
  !
  ! Other cases should update their own variables
  case default
    select case (comm)
    case ('READ')
      call read_image(line,error)
    case ('CLEAN')
      call dispatch_clean(line,error)
    case ('MX')
      if (themap%nfields.ne.0) then
        call map_message(seve%e,comm,'UV data is a Mosaic, not supported')
        error = .true.
      else
        call map_message(seve%w,comm,'MX is deprecated, use at your own risk',1)
        call old_uvmap('MX',line,error)
      endif
    case ('ALMA')
      call alma_clean(line,error)
    case ('UV_FLAG')
      call uv_flag_comm(line,error)
    case ('UV_MAP')
      call uv_map_comm(line,comm,error)
    case ('UV_RESAMPLE','UV_COMPRESS','UV_HANNING','UV_SMOOTH')
      call uv_resample_comm(line,comm,error)
    case ('UV_RESTORE')
      call uv_map_comm(line,comm,error)
    case ('UV_SHIFT')
      call uv_shift_comm(line,comm,error)
    case ('UV_CONTINUUM')
      call uv_line2cont (line,error)
    case ('UV_FILTER')
      call uv_filter (line,error)
    case ('UV_BASELINE')
      call uv_baseline (line,error)
    case ('UV_TIME')
      if (themap%nfields.ne.0) call map_message(seve%w,comm,'UV data is a Mosaic')
      call uv_time_comm (line,error)
    case ('UV_TRUNCATE')
      call uv_truncate_comm (line,error)
    case ('UV_TRIM')
      call uv_trim_comm (line,error)
    case ('UV_REWEIGHT')
      call uv_reweight_comm (line,comm,error)
    case ('UV_CHECK')
      call uv_check_comm (line,error)
    case ('UV_FIELDS')
      call uv_fields_comm (line,comm,error)
    case ('PRIMARY')
      call primary_comm(line,error)
    case ('SPECIFY')
      call com_modify (line,error)
    case ('UV_RESIDUAL')
      call uv_residual_comm(line,error)
    case ('MAP_RESAMPLE','MAP_COMPRESS','MAP_INTEGRATE','MAP_SMOOTH') 
      call map_resample_comm(line,comm,error)
    case ('MAP_COMBINE')
      call map_combine_comm(line,error)
    case ('MAP_REPROJECT')
      call reproject_comm(line,error)
    case ('UV_SPLIT')
      call uv_split_comm(line,error)
    case ('UV_EXTRACT')
      call uv_extract_comm(line,error)
    case ('CCT_CONVERT')
      call cct_convert_comm(line,error)
    case ('CCT_CLEAN')
      call cct_clean_comm(line,error)
    case default
      call map_message(seve%i,'CLEAN',comm//' not yet implemented')
      icall = icall-1
      error = .true.
      return
    end select
    ! Delete variables that may have been affected
    call sic_descriptor('SPECTRA',desc,found)  
    err = .false.
    if (found) call sic_delvariable('SPECTRA',.false.,err)
  end select
  icall = icall-1
  if (MappingError) error = .true.
  !
  call end_message(comm,elapsed,quiet,error)
end subroutine run_clean
!
subroutine run_display (line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_display
  use gkernel_types
  use gkernel_interfaces, only : sic_present, sic_lire
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER   Main routine
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  integer, save :: icall=0
  integer :: i, ia
  logical :: quiet
  real(8) :: elapsed
  !
  if (icall.ne.0) then
    if  ( (icall.eq.1) .and. ( &
      & (comm.eq.'FIND').or.(comm.eq.'STATISTIC').or. &
      & (comm.eq.'LOAD').or.(comm.eq.'SPECTRUM').or. &
      & (comm.eq.'SET') .or.(comm.eq.'EXTRACT').or. &
      & (comm.eq.'SLICE') ) ) then
      continue
    else
      Print *,'Unforeseen Rentrant call to RUN_DISPLAY ',comm
      read(5,*) i
    endif
  endif
  icall = icall+1
  !
  MappingError = .false.
  quiet = .true.  ! In general here, as these are interactive commands
  call report_init(elapsed)
  call map_message(seve%c,'DISPLAY',line)
  !
  ! Dispatch command
  select case (comm)
  case ('CATALOG')
    call catalog_comm(line,quiet,error)
  case ('COLOR')
    call color_comm(line,error)
  case ('LOAD')
    call view_load_comm (line,error)
  case ('STATISTIC')
    quiet = .false.
    call statistic(line,error)
    quiet = sic_lire().ne.0 .or. icall.gt.1
  case ('SHOW','VIEW','INSPECT_3D')
    call display_buffer(comm,line,error)
    quiet = .true.
  case ('EXPLORE')
    call explore_comm(line,comm,error)
  case ('EXTRACT') 
    call extract_comm(line,error)
  case ('FIND')
    call catalog_find(line,error)
  case ('POPUP')
    ia = index(line,' ')
    call exec_program('@ p_popup_all '//line(ia:)) !SG
  case ('SPECTRUM')
    call greg3_spectrum_compute(line,error)
  case ('SET')
    call display_set_comm(line,comm,error)
    quiet = .true.
  case ('SLICE') 
    call slice_comm(line,error)
  case default
    call map_message(seve%i,'DISPLAY',comm//' not yet implemented')
    icall = icall-1
    error = .true.
    return
  end select
  icall = icall-1
  if (MappingError) error = .true.
  !
  call end_message(comm,elapsed,quiet,error)
end subroutine run_display
!
subroutine end_message(comm,elapsed,quiet,error)
  use gbl_ansicodes
  use gbl_message
  use imager_interfaces, only : map_message
  !
  character(len=*), intent(in) :: comm ! Command line name
  logical, intent(in) :: error         ! Error flag
  logical, intent(in) :: quiet         ! Quiet flag
  real(8), intent(inout) :: elapsed    ! Elapsed time
  !
  character(len=80) :: chain
  integer :: lt
  !
  if (error) then
    call message_colour(1)
    call map_message(seve%e,comm,'failed to complete '//c_clear)  
  else if (.not.quiet) then
    call report_time(elapsed)
    call message_colour(2)
    if (elapsed.gt.0) then
      write(chain,'(a,a,f9.2,a)') 'Successful completion','  - Elapsed time ',elapsed,' sec'
      lt = len_trim(chain)
      chain(lt+1:) = c_clear
    else
      chain = 'Successful completion '//c_clear
    endif
    call map_message(seve%i,comm,chain)
  endif
  !
end subroutine end_message
!
module time_support
  use gsys_types
  real(8), save :: elapsed_s, elapsed_e
  logical, save :: nopara
  type(cputime_t), save :: time  
end module time_support
!
subroutine report_time(elapsed)
  !$ use omp_lib
  use time_support
  ! @ private
  real(8), intent(inout) :: elapsed
  !
  if (nopara) then
    call gag_cputime_get(time)
    elapsed = time%diff%elapsed
  endif
  !$ elapsed_e = omp_get_wtime()
  !$ elapsed = elapsed_e - elapsed_s
end subroutine report_time
!
subroutine report_init(elapsed)
  !$ use omp_lib
  use time_support
  ! @ private
  real(8) :: elapsed
  !
  elapsed = 0
  !
  nopara = .true.
  !$ nopara = .false.
  if (nopara) call gag_cputime_init(time)
  !$ elapsed_s = omp_get_wtime()
end subroutine report_init
!
subroutine run_advanced(line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_advanced
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER   Main routine for Language ADVANCED\
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  integer, save :: icall=0
  integer :: i
  real(8) :: elapsed
  logical :: quiet
  !
  if (icall.ne.0) then
    Print *,'Rentrant call to RUN_ADVANCED ',comm
    read(5,*) i
  endif
  icall = icall+1
  !
  quiet = .true.
  call report_init(elapsed)
  !
  MappingError = .false.
  call map_message(seve%c,'ADVANCED',line)
  !
  ! Analyze command
  select case (comm)
  case ('UV_PREVIEW')
    call uv_preview(line,error)
    quiet = .false.
  case ('PROPER_MOTION')
    call comm_proper_motion(line,comm,error)
    quiet = .false.
  case ('MAP_CONTINUUM')
    call map_continuum(line,error)
    quiet = .false.
  case ('MAP_POLAR')
    call map_polar(line,comm,error)
  case ('MFS')
    call clean_mfs (line,error)
  case ('UV_DEPROJECT','UV_RADIAL','UV_CIRCLE') 
    call uv_radial(line,comm,error)
    quiet = .false.
  case ('UV_SHORT')
    call uvshort_com(line,comm,error,quiet)
  case ('XY_SHORT')
    call uvshort_com(line,comm,error,quiet)
  case ('UV_FIT')
    call uvfit_comm(line,error)
    quiet = .false.
  case ('STOKES')
    call stokes_comm(line,error)
    quiet = .false.
  case ('EXTRACT') 
    call extract_comm(line,error)
  case ('MOMENTS')
    call moments_comm(line,error)
    quiet = .false.
  case ('MASK') 
    call mask_comm(line,error)
  case ('UV_ADD') 
    call uv_add_comm(line,error)
    quiet = .false.
  case ('UV_MERGE') 
    call uv_merge_many(line,error)
    quiet = .false.
  case ('FLUX')
    call flux_comm(line,error)
  case ('HOW_TO')
    call howto_comm(line,error)
  case ('FEATHER')
    call feather_comm(line,error)
    quiet = .false.
  case ('UV_CORRELATE')
    call correlate_comm(line,comm,error)
    quiet = .false.
  case ('UV_MOSAIC')
    call uv_mosaic_comm(line,comm,error)
    quiet = .false.
  case default
    call map_message(seve%i,'ADVANCED',comm//' not yet implemented')
  end select
  icall = icall-1
  if (MappingError) error = .true.
  !
  call end_message(comm,elapsed,quiet,error)
  !
end subroutine run_advanced
!
!
subroutine run_bundles(line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_bundles
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER   Main routine
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  integer, save :: icall=0
  integer :: i
  real(8) :: elapsed
  logical :: quiet
  !
  quiet = .true.
  call report_init(elapsed)
  !
  if (icall.ne.0) then
    Print *,'Rentrant call to RUN_BUNDLES ',comm
    read(5,*) i
  endif
  icall = icall+1
  !
  MappingError = .false.
  call map_message(seve%c,'BUNDLES',line)
  !
  ! Analyze command
  select case (comm)
  case ('SELFCAL') 
    call selfcal(line,comm,error)
  case ('COMBINE')
    call combine_comm(line,error)
    quiet = .false.
!  case ('EXPLORE')
!    call explore_comm(line,comm,error)
  case ('KEPLER')
    call kepler_comm(line,comm,quiet,error)
  case ('UV_DETECT')
    call detect_comm(line,comm,error)
    quiet = .false.
  case ('SPECTRAL_CLEAN')
    call spectral_comm(line,comm,error)
  case default
    call map_message(seve%i,'BUNDLES',comm//' not yet implemented')
  end select
  icall = icall-1
  if (MappingError) error = .true.
  !
  call end_message(comm,elapsed,quiet,error)
  !
end subroutine run_bundles
!
subroutine run_imager (line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use imager_interfaces, except_this=>run_imager
  use gkernel_types
  use gkernel_interfaces, only : sic_present, sic_lire
  !----------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER  Dispatch routine for Language IMAGER\
  !   Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Command name
  logical,          intent(out)   :: error ! Logical error flag
  !
  ! Local variables
  integer, save :: icall=0
  integer :: i, ia,n
  logical :: do_insert
  integer, parameter :: mvoc=4
  character(len=12) :: argum,voca(mvoc),name
  data voca/'ALL', 'CONTINUUM', 'SPLIT','SURVEY'/

  !
  if (icall.ne.0) then
    Print *,'Unforeseen Rentrant call to RUN_IMAGER ',comm
    read(5,*) i
  endif
  icall = icall+1
  !
  MappingError = .false.
  call map_message(seve%c,'IMAGER',line)
  !
  ! Dispatch command
  select case (comm)
  case ('CLARK','HOGBOM','MRC','MULTISCALE','SDI') 
    if (comm.eq.'MULTISCALE') then
      call sic_let_char('METHOD','MULTI',error)
    else
      call sic_let_char('METHOD',comm,error)
    endif
    call dispatch_clean(line,error)
  case ('PIPELINE')
    do_insert = sic_lire().eq.0
    if (sic_present(2,0)) then        ! /WIDGET option
      if (sic_present(1,0)) then
        call map_message(seve%e,comm,'/MODE and /WIDGET options are conflicting')
        icall = icall-1
        error = .true.
        return
      endif
      call exec_program('@ all-widget')
    else if (sic_present(1,0)) then   ! /MODE option
      !
      icall = icall-1
      call sic_ke(line,1,1,argum,n,.true.,error)
      if (error) return
      call sic_ambigs (comm,argum,name,n,voca,mvoc,error)
      if (error) return
      argum = ' '
      call sic_ke(line,0,1,argum,n,.false.,error)
      n = max(n,1)
      !
      icall = icall+1
      call exec_program('@ all-pipeline "'//argum(1:n)//'" '//name)
    else
      ia = index(line,' ')
      call exec_program('@ all-pipeline'//line(ia:))
    endif
    if (do_insert) call sic_insert_log(line)
  case ('TIPS')
    call exec_program('@ gag_pro:tips')
  case default
    call map_message(seve%i,'IMAGER',comm//' not yet implemented')
    icall = icall-1
    error = .true.
    return
  end select
  icall = icall-1
  if (MappingError) error = .true.
  !
end subroutine run_imager
!
subroutine save_result(error)
  use gkernel_interfaces
  use gildas_def
  use clean_types
  use gbl_message
  use imager_interfaces, only : map_message
  logical error
  character(len=filename_length) :: ans
  character(len=filename_length) :: autosave='autosave'  ! Default name when saving
  character(len=commandline_length) line
  character(len=message_length) :: chain
  character(len=*), parameter :: rname = 'IMAGER'
  integer :: n, icode
  logical :: do_write,noprompt
  !
  ans = ' '
  error = .false.
  do_write = .false.
  call gmaster_get_exitnoprompt(noprompt)  ! Check for SIC\EXIT /NOPROMPT
  !
  if (any(save_data) .and. .not.noprompt) then
    call map_message(seve%w,rname,'There are unsaved data, should we save them?')
    do while (.true.)
      call sic_wprn('Y)es, N)o, C)ancel exit (default No): ',ans,n)
      call sic_upper(ans)
      if (ans(1:1).eq.'C') then
        error = .true.
        return
      elseif (ans(1:1).eq.'N' .or. ans(1:1).eq.' ') then
        do_write = .false.
      elseif (ans(1:1).ne.'Y') then
        call map_message(seve%e,rname,'Unrecognized answer '//ans)
        cycle
      else 
        do_write = .true.
      endif
      exit
    enddo
  endif
  !
  call sic_delvariable('DIRTY',.false.,error)
  call sic_delvariable('CLEAN',.false.,error)
  call sic_delvariable('RESIDUAL',.false.,error)
  call sic_delvariable('PRIMARY',.false.,error)
  call sic_delvariable('SKY',.false.,error)
  if (.not.do_write) return
  !
  do icode=1,mtype
    ans = ' '
    error = .false.
    do while (save_data(icode))
       write(chain,100) vtype(icode)
       call sic_wprn(trim(chain)//' ',ans,n)
       if (ans.eq.' ') ans = autosave
       line = 'CLEAN'//char(92)//'WRITE '//vtype(icode)//' "'//trim(ans)//'"'
       call exec_command (line,error)
    enddo
  enddo
  !
100 format('Enter a filename for ',a,' (default ''autosave''): ')
end subroutine save_result
!
subroutine debug_all(line,error)
  !$ use omp_lib
  use gkernel_interfaces
  use imager_interfaces, except_this => debug_all
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !----------------------------------------------------------------------
  ! @  private
  !
  ! IMAGER     Print debug information
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical, intent(out) :: error
  !
  integer, parameter :: mvoc=5
  character(len=8) :: name,argum,voca(mvoc)
  integer :: n, nc, ier, mthread
  integer, allocatable :: mic(:)
  data voca /'BUFFERS ', 'METHOD  ', 'SG      ', 'THREADS ', 'USER    '/
  ! 
  argum = '?'
  call sic_ke(line,0,1,argum,n,.false.,error)
  if (error) return
  call sic_ambigs ('MOSAIC',argum,name,n,voca,mvoc,error)
  if (error) return
  !
  select case(name)
  case( 'USER') 
     call mapping_print_debug(user_method)
  case ('METHOD') 
     call mapping_print_debug(method)
  case ('BUFFERS') 
    call uv_dump_buffers('DUMP')
  case ('SG') 
    !
    nc = huv%gil%nchan
    if (nc.ne.hcct%gil%dim(2)) then
      Print *,'Mismatch number of channels between HUV ',nc,' and CCT ',hcct%gil%dim(2)
    endif
    !
    allocate(mic(nc),stat=ier)
    call uv_clean_sizes(hcct,dcct,mic,1,nc)
    Print *,'SG_DEBUG -- MIC ',mic
    deallocate(mic)
  case ('THREADS') 
    mthread = 0
    !$ mthread = omp_get_max_threads()
    Print *,'Number of Threads ',mthread
  end select
  !
end subroutine debug_all
!
subroutine mode_mosaic (line,error)
  use gkernel_interfaces
  use imager_interfaces, only : sub_mosaic
  !----------------------------------------------------------------------
  ! @  private
  !
  ! IMAGER      MOSAIC ON|OFF
  !             Activates or desactivates the mosaic mode
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical, intent(out) :: error
  !
  integer na,iv
  character(len=8) name,argum,voc1(2)
  data voc1/'OFF','ON'/
  !
  ! Mosaic mode. Default = .true.
  argum = 'ON'
  call sic_ke (line,0,1,argum,na,.false.,error)
  if (error) return
  call sic_ambigs ('MOSAIC',argum,name,iv,voc1,2,error)
  if (error) return
  call sub_mosaic(name,error)
end subroutine mode_mosaic
!
subroutine sic_insert_log(line)
  use gkernel_interfaces, only : sic_insert, sic_log
  !----------------------------------------------------------------------
  ! @ private
  !   Insert command line in Stack and Log file
  !----------------------------------------------------------------------
  character(len=*), intent(in) :: line
  !
  integer :: nl
  !
  nl = len_trim(line)
  call sic_insert(line(1:nl))
  call sic_log(line,nl,0)
end subroutine sic_insert_log
!
subroutine quiet_message(line,quiet)
  use gildas_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ private
  !   Test if command should be "Quiet", i.e. is only a Help-like command
  !----------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(out) :: quiet
  !
  logical :: error
  character(len=filename_length) :: ctest
  integer :: i
  !
  if (sic_present(0,1)) then
    ctest = ' '    
    call sic_ch(line,0,1,ctest,i,.false.,error)
    quiet = (ctest(1:1).eq.'?')
    if (.not.quiet) then
      if (sic_present(0,2)) then
        call sic_ch(line,0,2,ctest,i,.false.,error)
        quiet = (ctest(1:1).eq.'?')
      endif
    endif
  else
    quiet = .false.
  endif
end subroutine quiet_message
