subroutine map_doppler(line,error)
  use clean_def
  use clean_arrays
  use gbl_message
  use map_dependencies_interfaces
!-------------------------------------------------------------------
! MAPPING
!  @ private
!
!   Support for command DOPPLER 
! Task to compute the Doppler correction and apply it (by changing
! the u,v coordinates) to an UV Table. 
!-------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(inout) :: error
  !
  character(len=filename_length) :: file
  character(len=12) :: telescope
  !
  call sic_ke(line,0,1,telescope,nf,.true.,error)
  call astro_observatory(telescope,error)
  if (error) return
  !
  call map_doppler_uv(telescope,huv,error)  
end subroutine map_doppler
!
!
subroutine map_doppler_uv(telescope,huv,error)
  use image_def
  use map_dependencies_interfaces
  use gbl_format
  use gbl_message
!-------------------------------------------------------------------
! @ private
!
!   Code to compute the Doppler correction and apply it (by changing
!   the u,v coordinates) to an UV Table. 
! 
!-------------------------------------------------------------------
  type (gildas), intent(inout) :: huv
  logical, intent(inout) :: error
  character(len=*), intent(in) :: telescope
  !
  character(len=filename_length) :: file
  character(len=*), parameter :: rname='ASTRO_UV_DOPPLER'
  integer :: idate, itime, i
  real :: rdate, rtime, doppler
  real(8), parameter :: clight_kms=299792.458d0
  character(len=2) :: coord
  real(4) :: equinox
  real(8) :: lambda, beta
  real(4) :: daps(5)
  real(8) :: factor
  !
  if (huv%gil%version_uv.ge.code_version_uvt_syst) then
    call gag_message(seve%w,'UV_DOPPLER', & 
    & 'UV table already has Doppler information')
    return
  endif
  !
  call astro_observatory(telescope,error)
  if (error) return
  !
  coord = huv%char%syst
  if (coord.eq.'EQ') then
    equinox = huv%gil%epoc
    lambda = huv%gil%ra 
    beta   = huv%gil%dec 
  else if (coord.eq.'GA') then
    equinox = huv%gil%epoc
    lambda = huv%gil%lii
    beta   = huv%gil%bii
  else
    coord = 'EQ'
    call gag_message(seve%w,rname,'Unsupported system '//coord)
    call gag_message(seve%w,rname,'Assuming EQUATORIAL') 
    equinox = 2000.0
    lambda = huv%gil%ra 
    beta   = huv%gil%dec 
  endif
  !
  rdate = -1e38
  rtime = -1e38
  idate = huv%gil%column_pointer(code_uvt_date)
  itime = huv%gil%column_pointer(code_uvt_time)
  !
  do i=1,huv%gil%nvisi
    !
    if (huv%r2d(idate,i).ne.rdate .or. huv%r2d(itime,i).ne.rtime) then
      rdate = huv%r2d(idate,i)
      rtime = huv%r2d(itime,i)
      call sub_uv_doppler(huv%gil%vtyp,rdate,rtime,doppler,& 
        coord,equinox,lambda,beta,error)
      factor = (1.d0 + doppler)
    endif
    huv%r2d(1:2,i)  = huv%r2d(1:2,i) * factor
  enddo
  huv%gil%version_uv = code_version_uvt_syst
  huv%gil%dopp = 0.d0
end subroutine map_doppler_uv  
