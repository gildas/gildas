!
! Definitions used by UV_INVERT / UV_MAP
!
module uvmap_def
  type par_uvmap
     real taper(4)    ! UV taper
     character(len=4) mode  ! Weighting mode
     real field(2)     ! Field of view
     integer size(2)   ! Image size
     real xycell(2)    ! Image cell
     real uniform(2)   ! Robust weighting parameter
     real support(2)   ! Support size in meters
     real uvcell(2)    ! Cell size in meters
     integer channels(3)  ! First, Last and Weight channels
     integer ctype     ! Convolution type
     integer beam      ! One beam every N channels
     logical shift     ! Shift phase center
     logical blocked   ! Use blocking factor, or not (Debug)
     character(len=16) ra_c
     character(len=16) dec_c
     real(kind=8) new(3)     ! New position and angle of shift/rotation
     real(kind=8) off(3)     ! Offset and angle of shift/rotation
     real(kind=8) xy(2)      ! Phase rotation ! R*4 for SortUV
     real(kind=8) cs(2)      ! Angle offsets  ! R*4 for LoadUV
     real(kind=8) freq       ! Frequency for U,V values
     integer uvcode    ! UVT or TUV order ?
  end type
  !
  type gridding
     real ubias            ! Bias of U coordinates
     real ubuff(4096)      ! Grid of values of the convolution
     real vbias            ! Bias of V coordinates
     real vbuff(4096)      ! Grid of values of the convolution
  end type gridding
  integer, parameter :: code_uvt=1  ! This MUST NOT be changed
  integer, parameter :: code_tuv=2  ! This MUST NOT be changed
end module uvmap_def
!
! Used by Mapping CLEAN tools
!
module clean_def
  !------------------------------------------------------------------------------
  ! Parameters of CLEAN deconvolution.
  !------------------------------------------------------------------------------
  !
  type gridding
     real ubias            ! Bias of U coordinates
     real ubuff(4096)      ! Grid of values of the convolution
     real vbias            ! Bias of V coordinates
     real vbuff(4096)      ! Grid of values of the convolution
  end type gridding
  !
  type uvmap_par
    real taper(4)              ! UV Taper
    integer size(2)            ! Map size
    real xycell(2)             ! Map cell
    real uvcell(2)             ! UV cell
    real uniform(2)            ! Weighting parameters
    integer wcol               ! Weighting channel
    integer ctype              ! Convolution mode
    real support(2)            ! Support of gridding function
    integer beam               ! One beam every N channels
    real field(2)              ! Field of view in arcsecond
    real precis                ! Precision at map edges
    real truncate              ! Truncation level in %
    character mode*8           ! Weighting mode
    logical shift              ! Shift or rotate UV data
    character ra_c*16          ! Right ascension
    character dec_c*16         ! Declination
    real angle                 ! New position angle of UV data
    integer nfields            ! Number and type of fields (<0 = phase, >0 = pointing)
    real bsize                 ! Primary Beam size (for Mosaics)
    real, allocatable :: offxy(:,:)    ! Pointing or Phase offset values
    real(8), allocatable :: centers(:,:) ! Ra Dec absolute pointings
    ! real, pointer :: uvdata(:,:)       ! UV Data (not used)
  end type
  !-----
  type clean_par
    sequence
    integer first,last         ! First & Last plane
    integer nlist              ! List size
    integer iplane             ! Current image plane
    integer ibeam              ! Current beam plane
    integer imask              ! Current mask plane 
    !
    real thresh                ! Threshold for beam fit
    real gain                  ! Loop gain
    real fres                  ! Fractional residual
    real ares                  ! Absolute residual
    real ratio                 ! Smoothing ratio for MRC
    real spexp                 ! Speed up factor in Clark
    real phat                  ! Prussian hat
    real flux                  ! Cleaned Flux
    real smooth                ! Multi-Scale smoothing factor
    real search                ! Threshold for searching
    real restor                !           for restoration
    real trunca                ! Beam truncation value
    real worry                 ! Worry factor in MultiScale
    integer nker(3)            ! Kernel for Multi-Scale clean
    real gains(3)              ! Clean gains
    integer ninflate           ! Component "inflation" factor
    integer ngoal              ! Maximum number of minor cycle components
    integer converge           ! Convergence criterium: 0 = Infty
    integer m_iter             ! Maximum number of iterations
    integer n_iter             ! Current number of iterations
    integer p_iter             ! Positive number of iterations
    integer n_major            ! Maximum number of major cycle
    !
    real major                 ! Major axis
    real minor                 ! Minor axis
    real angle                 ! Clean beam angle
    real beam_min              ! Beam minimum
    real beam_max              ! Beam maximum
    real bgain                 ! Outer sidelobe
    !
    integer blc(2)             ! Bottom Left Corner
    integer trc(2)             ! Top Right Corner
    integer box(4)             ! Cleaning Box
    integer beam0(2)           ! Beam center X,Y
    integer patch(2)           ! Beam patch sizes
    integer bzone(4)           ! Beam patch
    integer bshift(3)          ! Beam shift
    integer residual        ! Add back the residual ?
    !
    logical mosaic             ! Mosaic flag
    logical pflux              ! Flux display
    logical pcycle             ! Residual display in cycle
    logical pmrc               ! MRC display in cycle
    logical qcycle             ! Query mode
    logical pclean             ! Clean display in cycle
    logical do_mask            ! Re-compute the mask ?
    logical verbose            ! Lots of printout ?
    character method*12        ! METHOD
    !
    real, pointer :: atten(:,:,:)   ! Mosaic weights (as function of freq)
!    integer, pointer :: list(:)    ! List of selected pixels
!    logical, pointer :: mask(:,:)  ! Selection mask
  end type
  !-----
  type cct_par                 ! Clean Component definition
    real influx                ! Current flux
    real value                 ! Component value
    integer ix                 ! X pixel
    integer iy                 ! Y pixel
    integer type               ! Component kernel number
  end type
end module clean_def
!
module mod_fitbeam
  integer :: nv                ! Number of values
  real :: par(6)  ! Fitted parameters
  real :: err(6)  ! Errorbars
  real :: spar(6) ! Last fitted values
  real :: sigbas  ! rms on baseline
  real :: sigrai  ! rms on line
  integer :: kpar(6)  ! Code for parameter
  real, allocatable :: ip_values(:)    ! Values
  real, allocatable :: ip_coords(:,:)  ! Coordinates
end module mod_fitbeam
!
!
module short_def
  integer, parameter :: code_short_old=0    ! Old method: one file per field
  integer, parameter :: code_short_phase=1  ! Mosaic UV table with phase offsets
  integer, parameter :: code_short_point=2  ! Mosaic UV table with pointing offsets
  integer, parameter :: code_short_auto=3   ! Automatic derivation of UV table offsets type
  !
  type short_spacings
    integer                           :: mode=3
    real                              :: sd_factor=0.0
    real                              :: minw=0.01
    real                              :: ptole=0.0
    real                              :: ftole=0.01
    real                              :: uv_minr=0.
    real                              :: uv_maxr=0.
    real                              :: sd_weight=0.
    integer                           :: xcol=0
    integer                           :: ycol=0
    integer                           :: wcol=0
    integer                           :: mcol(2)=[0,0]
    real                              :: sd_beam(3)=[0.,0.,0.]
    real                              :: sd_diam=0.
    real                              :: ip_beam=0.
    real                              :: ip_diam=0.
    logical                           :: do_single=.true.
    logical                           :: do_primary=.true.
    character(len=4)                  :: weight_mode='NATU' 
    character(len=16)                 :: chra=' '
    character(len=16)                 :: chde=' '
    integer                           :: nf=0
    real, allocatable    :: raoff(:)
    real, allocatable    :: deoff(:)
  end type 
  !
end module short_def

