subroutine uv_mosaic_comm(line,comm,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use imager_interfaces, only : map_message, uv_mosaic_group, uv_mosaic_split
  !----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support for command 
  !     UV_MOSAIC Mosaic MERGE|SPLIT [Fields...]
  !
  ! Make a mosaic UV Tables from a set of single field UV tables, 
  ! or vice-versa
  !
  ! The original UV Tables are assumed to have the same number of channels and
  ! 1) either to have the same phase tracking center
  ! 2) or a phase tracking center corresponding to the pointing center
  !
  ! It is assumed that the names of the original field UV tables
  ! the simple sequence 'NAME'-'I'.uvt
  !----------------------------------------------------------------------
  character(len=*), intent(in) :: line
  character(len=*), intent(in) :: comm
  logical, intent(inout) :: error
  !
  integer(kind=4) :: nf
  character(len=12) :: direction
  character(len=12) :: merge='MERGE'
  character(len=12) :: split='SPLIT'
  !
  call sic_ke(line,0,2,direction,nf,.true.,error)
  if (error) return
  !
  if (direction(1:nf).eq.merge(1:nf)) then
    call uv_mosaic_group(line,comm,error)
  else if (direction(1:nf).eq.split(1:nf)) then
    call uv_mosaic_split(line,comm,error)
  else
    call map_message(seve%e,comm,'Second argument must be MERGE or SPLIT')
    error = .true.
  endif
end subroutine uv_mosaic_comm
!
subroutine uv_mosaic_group(line,rname,error)
  use phys_const
  use image_def
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use iso_c_binding
  use imager_interfaces, only : map_message
  !----------------------------------------------------------------------
  ! @ private
  !
  !   IMAGER
  ! Support for command
  !     UV_MOSAIC Mosaic MERGE Fields [...]
  !
  ! Build a Mosaic from an ensemble of independent fields.
  ! Argument Fields can be a SIC variable handling all the fields filenames.
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  character(len=*), intent(in)    :: rname
  logical,          intent(inout) :: error
  !
  type(gildas) :: field
  type(gildas) :: mosaic
  real(kind=4), allocatable :: dmos(:,:)
  real(kind=4), allocatable :: dfield(:,:)
  !
  integer(kind=4) :: ier,nvisi,mvisi
  integer(kind=4) :: jf,nf,iv,i_xoff,i_yoff,luv
  real(kind=8) :: doffx,doffy
  type(projection_t) :: proj
  !
  integer(kind=4) :: code,code_x,code_y
  integer(kind=4), parameter :: code_mosaic_void=0   ! Mosaic UV table with undecided offsets
  integer(kind=4), parameter :: code_mosaic_phase=1  ! Mosaic UV table with phase offsets
  integer(kind=4), parameter :: code_mosaic_point=2  ! Mosaic UV table with pointing offsets
  real(kind=4), parameter :: point_accuracy=0.1*rad_per_sec   ! Pointing accuracy
  real(kind=4), parameter :: phase_accuracy=0.001*rad_per_sec ! Phase accuracy
  real(kind=4), parameter :: spectral_accuracy=0.01
  !
  real(kind=8) :: a0,d0
  character(len=15) :: chde,chra
  integer :: nchan
  real :: tole
  !
  ! Local
  integer(kind=address_length) :: addr
  character(len=varname_length) :: listname
  character(len=filename_length) :: table_out
  character(len=filename_length), allocatable :: table_in(:)
  type(sic_descriptor_t) :: desc
  real, allocatable :: weight(:), factor(:)
  integer :: i, n, nt, ns
  logical :: err, found
  !
  ! Initialization
  call gildas_null(field,type='UVT')
  call gildas_null(mosaic,type='UVT')
  !
  ! Sanity check
  ns = sic_narg(0)
  if (ns.lt.3) then
    call map_message(seve%e,rname,'Requires at least 3 arguments')
    error = .true.
    return
  endif
  ns = ns-2
  !
  ! Get the output file name
  call sic_ch(line,0,1,table_out,n,.true.,error)
  if (error) return
  call sic_parse_file(table_out,' ','.uvt',mosaic%file)
  !
  ! Get the input table nameS (2 at least...)
  if (ns.eq.1) then
    ! This may be a character array handling the file names
    call sic_ch(line,0,3,listname,n,.true.,error)
    if (error) return
    call sic_descriptor(listname,desc,found)    
    ! Names from Character array
    if (.not.found) then
      call map_message(seve%e,rname,'Requires at least 2 input fields')
      error = .true.
      return
    else
      !
      ! Check the Descriptor: it must be a character array
      if (desc%type.lt.1 .or. desc%ndim.ne.1) then
        call map_message(seve%e,rname,trim(listname)//' is not a character array')
        error = .true.
        return
      endif
      ! and it must refer to more than 1 file
      nt = desc%dims(1)
      if (nt.eq.1) then
        call map_message(seve%e,rname,'Requires at least 2 input fields')
        error = .true.
        return
      endif
    endif
    !
    allocate(table_in(nt),weight(nt),factor(nt),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      return
    endif
    addr = desc%addr
    do i=1,nt
      call destoc(desc%type,addr,table_in(i))
      addr = addr + desc%type
    enddo
  else
    ! Names from Command line
    nt = ns 
    allocate(table_in(nt),weight(nt),factor(nt),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      return
    endif
    do i=1,nt
      call sic_ch(line,0,i+2,table_in(i),n,.true.,err)
      if (err) return
    enddo
  endif
  !
  ! Get first header and copy it in output image
  field%file = table_in(1)
  call gdf_read_header(field,error)
  if (error) then
    call map_message(seve%e,rname,'Cannot open '//trim(field%file))
    return
  endif
  call gdf_close_image(field,error)
  if (error) return
  call gdf_copy_header(field,mosaic,error)
  !
  ! Check consistency of headers
  code = code_mosaic_void
  if (abs(field%gil%ra-field%gil%a0).gt.point_accuracy .or. &
    & abs(field%gil%dec-field%gil%d0).gt.point_accuracy) then
    ! Phase & Pointing center differ:
    ! we must check that all fields have a common phase center
    code = code_mosaic_point
  endif
  nvisi = 0
  mvisi = 0
  a0 = 0d0
  d0 = 0d0
  !
  nf = nt
  do jf=1,nf
    ! Read header of field #jf
    field%file = table_in(jf)
    call gdf_read_header(field,error)
    if (error) then
      call map_message(seve%e,rname,'Cannot open '//trim(field%file))
      return
    endif
    call gdf_close_image(field,error)
    if (error) return
    ! Check spectral axis
    if (field%gil%dim(1).ne.mosaic%gil%dim(1) .or. &
        field%gil%nchan.ne.mosaic%gil%nchan) then
      call map_message(seve%e,rname,'Inconsistent channel number at '//trim(field%file))
      Print *,'Dim ',field%gil%dim(1),mosaic%gil%dim(1)
      Print *,'Dim ',field%gil%nchan,mosaic%gil%nchan
      error = .true.
    endif
    nchan = max(field%gil%nchan,mosaic%gil%nchan)
    if (abs(field%gil%ref(1)-mosaic%gil%ref(1)).gt.spectral_accuracy) then
      Print *,'Reference       ',field%gil%ref(1), mosaic%gil%ref(1)
      error = .true.
    endif
    tole = spectral_accuracy * max(abs(field%gil%vres),abs(mosaic%gil%vres))
    if (abs(field%gil%voff-mosaic%gil%voff).gt.tole) then
      Print *,'Velocity        ',field%gil%voff, mosaic%gil%voff
      error = .true.
    endif
    tole = tole/nchan
    if (abs(field%gil%vres-mosaic%gil%vres).gt.tole) then
      Print *,'Velo. resolution',field%gil%vres,mosaic%gil%vres
      error = .true.
    endif
    tole = spectral_accuracy * max(abs(field%gil%fres),abs(mosaic%gil%fres))
    if (abs(field%gil%freq-mosaic%gil%freq).gt.tole) then
      Print *,'Frequency       ',field%gil%freq,mosaic%gil%freq
      error = .true.
    endif
    tole = 2*tole/nchan
    if (abs(field%gil%fres-mosaic%gil%fres).gt.tole) then
      Print *,'Freq. resolution',field%gil%fres,mosaic%gil%fres
      error = .true.
    endif
    if (error) then
      call map_message(seve%e,rname,'Inconsistent spectral axis at '//trim(field%file))
      error = .true.
    endif
    !
    ! Check projection
    if (code.eq.code_mosaic_point) then
      if (abs(mosaic%gil%a0-field%gil%a0).gt.phase_accuracy .or. &
           & abs(mosaic%gil%d0-field%gil%d0).gt.phase_accuracy) then
        call map_message(seve%e,rname,'#1 Inconsistent phase center at '//trim(field%file))
        error = .true.
      endif
    else if (code.eq.code_mosaic_phase) then
      if (field%gil%ra.ne.field%gil%a0 .or. field%gil%dec.ne.field%gil%d0) then
        call map_message(seve%e,rname,'Phase & Pointing center mismatch at '//trim(field%file))
        error = .true.
      endif
    else if (abs(field%gil%ra-field%gil%a0).gt.point_accuracy .or. &
        abs(field%gil%dec-field%gil%d0).gt.point_accuracy) then
      ! Previously undefined, but phase center differ from pointing center
      ! - must have common phase center
      code = code_mosaic_point
      if (abs(mosaic%gil%a0-field%gil%a0).gt.phase_accuracy .or. &
           abs(mosaic%gil%d0-field%gil%d0).gt.phase_accuracy) then
        Print *,'A0 ',mosaic%gil%a0, field%gil%a0, 180*3600/pi*abs(mosaic%gil%a0-field%gil%a0)
        Print *,'D0 ',mosaic%gil%d0, field%gil%d0, 180*3600/pi*abs(mosaic%gil%d0-field%gil%d0)
        call map_message(seve%e,rname,'#2 Inconsistent phase center at '//trim(field%file))
        error = .true.
      endif
    else
      code = code_mosaic_phase
    endif
    a0 = a0+field%gil%ra
    d0 = d0+field%gil%dec
    if (error) return
    ! Iterate visibility sizes
    nvisi = nvisi + field%gil%nvisi
    mvisi = max(mvisi,field%gil%dim(2))
  enddo ! jf
  a0 = a0/nf
  d0 = d0/nf
  !
  ! Prepare the mosaic UV table
  mosaic%gil%nvisi = nvisi
  mosaic%gil%dim(2) = nvisi
  ! There may be problems if a column of the other type is already
  ! present in the initial UV table...
  if (code.eq.code_mosaic_point) then
    code_x = code_uvt_xoff
    code_y = code_uvt_yoff
    if ((mosaic%gil%column_pointer(code_uvt_loff).ne.0) .or. &
        (mosaic%gil%column_pointer(code_uvt_moff).ne.0)) then
      error = .true.
    endif
  else
    code_x = code_uvt_loff
    code_y = code_uvt_moff
    if ((mosaic%gil%column_pointer(code_uvt_xoff).ne.0) .or. &
        (mosaic%gil%column_pointer(code_uvt_yoff).ne.0)) then
      error = .true.
    endif
  endif
  if (error) then
    call map_message(seve%e,rname,'Combination of PHASE_OFF and POINT_OFF not allowed')
    return
  endif
  ! Add or reuse the offset columns
  if (mosaic%gil%column_pointer(code_x).eq.0) then
    mosaic%gil%dim(1) = mosaic%gil%dim(1)+1
    mosaic%gil%column_pointer(code_x) = mosaic%gil%dim(1)
    mosaic%gil%column_size(code_x) = 1
  endif
  if (mosaic%gil%column_pointer(code_y).eq.0) then
    mosaic%gil%dim(1) = mosaic%gil%dim(1)+1
    mosaic%gil%column_pointer(code_y) = mosaic%gil%dim(1)
    mosaic%gil%column_size(code_y) = 1
  endif
  i_xoff = mosaic%gil%column_pointer(code_x)
  i_yoff = mosaic%gil%column_pointer(code_y)
  ! Define the projection center for the offset columns
  if (code.eq.code_mosaic_phase) then
     mosaic%gil%a0 = a0
     mosaic%gil%d0 = d0
  endif
  mosaic%gil%ra = mosaic%gil%a0
  mosaic%gil%dec = mosaic%gil%d0
  call gwcs_projec(mosaic%gil%a0,mosaic%gil%d0,mosaic%gil%pang,mosaic%gil%ptyp,proj,error)
  !
  ! Allocate visibility buffers
  allocate(dmos(mosaic%gil%dim(1),nvisi), stat=ier)
  if (failed_allocate(rname,'Mosaic uv table',ier,error)) return
  allocate(dfield(field%gil%dim(1),mvisi), stat=ier)
  if (failed_allocate(rname,'Field uv table',ier,error)) return
  !
  ! Create, fill, and write the mosaic UV table
  call gdf_create_image(mosaic,error)
  if (error) return
  field%blc = 0
  field%trc = 0
  mosaic%blc = 0
  mosaic%trc = 0
  do jf=1,nf
    field%file = table_in(jf)
    call gdf_read_header(field,error)
    if (error) return
    call abs_to_rel(proj,field%gil%ra,field%gil%dec,doffx,doffy,1)
    call gdf_read_data(field,dfield,error)
    if (error) return
    call gdf_close_image(field,error)
    if (error) return
    luv = field%gil%dim(1)
    !
    do iv=1,field%gil%nvisi
      dmos(1:luv,iv) = dfield(1:luv,iv)
      dmos(i_xoff,iv) = doffx
      dmos(i_yoff,iv) = doffy
    enddo
    !
    mosaic%blc(2) = mosaic%trc(2)+1
    mosaic%trc(2) = mosaic%blc(2)+field%gil%nvisi-1
    !
    call gdf_write_data(mosaic,dmos,error)
    if (error) return
  enddo
  call gdf_close_image(mosaic,error)
  if (error) return
  !
  print *,""
  print *,"New projection center:"
  call rad2sexa (mosaic%gil%a0,24,chra)
  call rad2sexa (mosaic%gil%d0,360,chde)
  print *,'A0: ',chra
  print *,'D0: ',chde
  print *,""
end subroutine uv_mosaic_group
!
subroutine uv_mosaic_split(line,rname,error)
  use phys_const
  use gkernel_interfaces
  use gkernel_types
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message
  !----------------------------------------------------------------------
  ! @ private
  !
  !   IMAGER
  ! Support for command
  !     UV_MOSAIC Mosaic SPLIT [Fields]
  ! Explode a Mosaic to an ensemble of NF single fields
  ! with a name following the convention 'Fields'-'i'
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: rname  ! Command name
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Errorflag
  !
  type(gildas) :: field
  type(gildas) :: mosaic
  real(kind=4), allocatable :: dmos(:,:)
  real(kind=4), allocatable :: dfield(:,:)
  character(len=filename_length) :: name
  character(len=filename_length) :: generic
  !
  integer(kind=4), parameter :: code_mosaic_phase=1  ! Mosaic UV table with phase offsets
  integer(kind=4), parameter :: code_mosaic_point=2  ! Mosaic UV table with pointing offsets
  integer(kind=4) :: code,ier
  integer(kind=4) :: nlast,mvisi
  integer(kind=4) :: jv,iv,ifi,kfi,nfi,mfi
  integer(kind=4) :: loff,moff,xoff,yoff,xcol,ycol
  real(kind=8) :: doffx,doffy,ra,dec
  real(kind=8), allocatable :: doff(:,:),dtmp(:,:)
  type(projection_t) :: proj
  !
  ! Initialization
  call gildas_null(field,type='UVT')
  call gildas_null(mosaic,type='UVT')
  !
  ! Get mosaic header and copy it to the field header
  call sic_ch(line,0,1,generic,jv,.true.,error)
  if (error) return
  call sic_parse_file(generic,' ','.uvt',mosaic%file)
  call gdf_read_header(mosaic,error)
  if (error) then
    call map_message(seve%e,rname,'Cannot open '//trim(mosaic%file))
    return
  endif
  !
  ! Check mosaic kind
  loff = mosaic%gil%column_pointer(code_uvt_loff)
  moff = mosaic%gil%column_pointer(code_uvt_moff)
  xoff = mosaic%gil%column_pointer(code_uvt_xoff)
  yoff = mosaic%gil%column_pointer(code_uvt_yoff)
  if ((loff.ne.0).and.(moff.ne.0)) then
    call map_message(seve%i,rname,'Input UV table is a phase offset mosaic')
    xcol = loff
    ycol = moff
    code = code_mosaic_phase
  else if ((xoff.ne.0).and.(yoff.ne.0)) then
    call map_message(seve%i,rname,'Input UV table is a pointing offset mosaic')
    xcol = xoff
    ycol = yoff
    code = code_mosaic_point
  else
    call map_message(seve%e,rname,'Input UV table is not a mosaic (No XY or LM offset columns)')
    error = .true.
    return
  endif
  !
  ! Prepare the Field headers
  call gdf_copy_header(mosaic,field,error)
  if (error) return
  call sic_ch(line,0,3,generic,jv,.false.,error)
  if (error) return
  !
  ! Get rid of the trailing columns if possible. Just verify
  ! that ntrail is 2, and that these are the two appropriate
  ! columns
  if (field%gil%ntrail.eq.2) then
    if (((xcol.eq.field%gil%lcol+1) .and. &
        (ycol.eq.field%gil%lcol+2)) .or. &
       ((xcol.eq.field%gil%lcol+2) .and. &
        (ycol.eq.field%gil%lcol+1))) then
      if (code.eq.code_mosaic_point) then
        field%gil%column_pointer(code_uvt_xoff) = 0
        field%gil%column_pointer(code_uvt_yoff) = 0
        field%gil%column_size(code_uvt_xoff) = 0
        field%gil%column_size(code_uvt_yoff) = 0
      else
        field%gil%column_pointer(code_uvt_loff) = 0
        field%gil%column_pointer(code_uvt_moff) = 0
        field%gil%column_size(code_uvt_loff) = 0
        field%gil%column_size(code_uvt_moff) = 0
      endif
    endif
    field%gil%ntrail = 0
    field%gil%dim(1) = field%gil%lcol
    nlast = field%gil%lcol
  else
    nlast = field%gil%dim(1)
  endif
  !
  ! Read data
  allocate(dmos(mosaic%gil%dim(1),mosaic%gil%dim(2)),stat=ier)
  if (failed_allocate(rname,'Mosaic uv table',ier,error)) return
  call gdf_read_data(mosaic,dmos,error)
  if (error) return
  !
  ! Scan the list of offset
  doffx = dmos(xcol,1)
  doffy = dmos(ycol,1)
  nfi = 1
  mfi = 100
  allocate(doff(3,mfi),stat=ier)
  doff(1,1) = doffx
  doff(2,1) = doffy
  doff(3,1) = 1
  do iv=2,mosaic%gil%nvisi
    kfi = 0
    do ifi=1,nfi
      if (dmos(xcol,iv).eq.doff(1,ifi) .and. &
          dmos(ycol,iv).eq.doff(2,ifi)) then
         kfi = ifi
         doff(3,kfi) = doff(3,kfi)+1
         exit
      endif
    enddo ! ifi
    ! New field => Initialize
    if (kfi.eq.0) then
      if (nfi.eq.mfi) then
        allocate(dtmp(3,2*mfi),stat=ier)
        dtmp(:,1:mfi) = doff(:,:)
        deallocate(doff)
        allocate(doff(3,2*mfi),stat=ier)
        doff(:,:) = dtmp
        deallocate(dtmp)
        mfi = 2*mfi
      endif
      nfi = nfi+1
      doff(1,nfi) = dmos(xcol,iv)
      doff(2,nfi) = dmos(ycol,iv)
      doff(3,nfi) = 1
    endif
  enddo ! iv
  !
  ! Allocate enough space for field uv table
  mvisi = 0
  do ifi=1,nfi
    mvisi = max(nint(doff(3,ifi)),mvisi)
  enddo
  allocate(dfield(nlast,mvisi),stat=ier)
  if (failed_allocate(rname,'Field uv table',ier,error)) return
  !
  ! Create, fill, and write the output UV tables
  call gwcs_projec(mosaic%gil%a0,mosaic%gil%d0,mosaic%gil%pang,mosaic%gil%ptyp,proj,error)
  if (error) return
  do ifi=1,nfi
    write(name,'(A,I0)') trim(generic)//'-',ifi
    call sic_parse_file(name,' ','.uvt',field%file)
    field%gil%nvisi = doff(3,ifi)
    field%gil%dim(2) = field%gil%nvisi
    call rel_to_abs(proj,doff(1,ifi),doff(2,ifi),ra,dec,1)
    field%gil%ra = ra
    field%gil%dec = dec
    if (code.eq.code_mosaic_phase) then
      field%gil%a0 = ra
      field%gil%d0 = dec
    endif
    !
    jv = 0
    do iv=1,mosaic%gil%nvisi
      if ((dmos(xcol,iv).eq.doff(1,ifi)) .and. &
        & (dmos(ycol,iv).eq.doff(2,ifi))) then
        jv = jv+1
        dfield(1:nlast,jv) = dmos(1:nlast,iv)
      endif
    enddo
    field%gil%nvisi = jv
    !
    call gdf_create_image(field,error)
    if (error) return
    field%blc = 0
    field%trc = 0
    call gdf_write_data(field,dfield,error)
    if (error) return
    call gdf_close_image(field,error)
    if (error) return
  enddo ! ifi
end subroutine uv_mosaic_split
!
