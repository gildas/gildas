subroutine color_comm(line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support for command COLOR Span HueMin HueMax
  !
  !   calls "@ p_color.ima" to derive a lut
  !   with unsaturated colours up to Span around 0,
  !   and a Hue range between Min and Max 
  ! 
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(inout) :: error
  !
  real :: colour
  logical :: do_insert
  !
  call sic_get_real('COLOR[3]',colour,error)
  call sic_r4(line,0,1,colour,.false.,error)
  call sic_let_real('COLOR[3]',colour,error)
  !
  call sic_get_real('COLOR[1]',colour,error)
  call sic_r4(line,0,2,colour,.false.,error)
  call sic_let_real('COLOR[1]',colour,error)
  !
  call sic_get_real('COLOR[2]',colour,error)
  call sic_r4(line,0,3,colour,.false.,error)
  call sic_let_real('COLOR[2]',colour,error)
  !
  do_insert = sic_lire().eq.0
  call exec_program('@ p_color')
  if (do_insert) call sic_insert_log(line)  
end subroutine color_comm
