
subroutine sub_major(method,hdirty,hresid,hclean,   &
     &    hbeam,hprim,hmask,dcct,mask,list,error,        &
     &    major_plot, next_flux)
  use gkernel_interfaces
  use imager_interfaces, except_this=>sub_major
  use clean_def
  use image_def
  use gbl_message
  !$ use omp_lib
  !--------------------------------------------------------------
  ! @ private
  !
  ! IMAGER Clean/Mosaic
  !     Perfom a CLEAN based on all CLEAN algorithms,
  !     except the MRC (Multi Resolution CLEAN)
  !     which requires a different tool
  !     Works for mosaic also, except for the Multi Scale clean
  !     (not yet implemented for this one, but feasible...)
  !--------------------------------------------------------------
  external :: major_plot
  external :: next_flux
  !
  type (clean_par), intent(inout) :: method
  type (gildas), intent(in) :: hdirty
  type (gildas), intent(inout) :: hbeam
  type (gildas), intent(inout) :: hclean
  type (gildas), intent(inout) :: hresid
  type (gildas), intent(in) :: hprim
  type (gildas), intent(in) :: hmask
  real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
  logical, intent(in), target :: mask(:,:)
  integer, intent(in), target :: list(:)
  logical, intent(inout) ::  error
  !
  character(len=*), parameter :: rname='SUB_MAJOR'
  integer, parameter :: opt_start=6 ! /RESTART option
  !
  character(len=64) :: mess
  integer :: ith
  real(4) :: rmega
  logical :: parallel, old_code
  integer :: mthread, ithread, nplane
  !
  ! For very big images, it may be interesting to switch from the 
  ! Outer parallelism to an Inner parallelism.  This is currently
  ! coded in a simple, probably not efficient, way, while it should
  ! depend on the available Memory, and also on the number of Planes.
  !
  ith = 2*8192 ! By default, use outer parallel mode even on large images.
  ! The two-level parallelism will adapt the ressources properly.
  parallel = .false.
  !$ parallel = .true.
#ifndef GAG_USE_STATICLINK
  !$ call sic_get_inte('OMP_SIZE',ith,error)
  !!Print *,'ith ',ith
  !!read(5,*) ith
#endif
  !
  ! Check if more possible Threads than Planes
  mthread = 1
  ithread = 1
  !$  mthread = omp_get_max_threads()
  nplane = method%last-method%first+1
  !
  error = .false.
  old_code = (hdirty%gil%dim(1)*hdirty%gil%dim(2).gt.ith**2) 
  if (old_code) then
    if (sic_present(opt_start,0)) then
      call map_message(seve%e,method%method,'Forcing use of Parallel code for /RESTART option')
      old_code = .false.
    endif
  endif
  !
  if (old_code) then
    ! Older, deprecated code, replaced by Two-Level parallelism
    if (parallel) then
      mess = 'Using sequential code with Open-MP in-plane parallel mode'
      Print *,'Rmega ',rmega,' ITH ',ith
    else
      mess = 'Using non-parallel mode'
    endif
    call map_message(seve%i,method%method,mess)
    call sub_major_lin(method,hdirty,hresid,hclean,   &
     &    hbeam,hprim,hmask,dcct,mask,list,error,        &
     &    major_plot, next_flux)
  else
    if (parallel) then
      mess = 'Using Open-MP parallel code'
    else
      mess = 'Using Open-MP capable code in Non-Parallel mode'
    endif
    call map_message(seve%i,method%method,mess)
    call sub_major_omp(method,hdirty,hresid,hclean,   &
     &    hbeam,hprim,hmask,dcct,mask,list,error,        &
     &    major_plot, next_flux)
  endif
end subroutine sub_major
!
subroutine sub_major_omp(inout_method,hdirty,hresid,hclean,   &
     &    hbeam,hprim,hmask,dcct,mask,list,error,        &
     &    major_plot, next_flux)
  use gkernel_interfaces
  use imager_interfaces, except_this=>sub_major_omp
  use clean_def
  use clean_default
  use clean_support
  use image_def
  use gbl_message
  !$ use omp_lib
  use omp_control
  !--------------------------------------------------------------
  ! @ private
  !
  ! IMAGER Clean/Mosaic
  !     Perfom a CLEAN based on all CLEAN algorithms,
  !     except the MRC (Multi Resolution CLEAN)
  !     which requires a different tool
  !     Works for mosaic also, except for the Multi Scale clean
  !     (not yet implemented for this one, but feasible...)
  !--------------------------------------------------------------
  external :: major_plot
  external :: next_flux
  !
  type (clean_par), intent(inout) :: inout_method
  type (gildas), intent(in) :: hdirty
  type (gildas), intent(inout) :: hbeam
  type (gildas), intent(inout) :: hclean
  type (gildas), intent(inout) :: hresid
  type (gildas), intent(in) :: hprim
  type (gildas), intent(in) :: hmask
  real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
  logical, intent(in), target :: mask(:,:)
  integer, intent(in), target :: list(:)
  logical, intent(inout) ::  error
  !
  integer, parameter :: opt_start=6 ! /RESTART option
  !
  real, pointer :: dirty(:,:)  ! Dirty map
  real, pointer :: resid(:,:)  ! Iterated residual
  real, pointer :: clean(:,:)  ! Clean Map
  real, pointer :: d3prim(:,:,:)    ! Primary beam
  real, pointer :: d3beam(:,:,:)    ! Dirty beam (per field)
  real, pointer :: atten(:,:) ! Mosaic weight
  !
  real, allocatable :: w_fft(:)    ! TF work area
  complex, allocatable :: w_work(:,:)  ! Work area
  type(cct_par), allocatable :: w_comp(:)
  real, allocatable :: w_cct(:,:)
  logical, allocatable :: s_mask(:,:)
  real, allocatable :: s_beam(:,:,:), t_beam(:,:), s_resi(:,:)
  real, allocatable :: tfbeam(:,:,:)
  integer, allocatable :: mymask(:,:)
  integer :: f_iter, m_iter
  !
  type (clean_par), save :: method
  real, target :: dummy_prim(1,1,1), dummy_atten(1,1)
  integer iplane
  integer nx,ny,np,nl,mx,my,nc, icct, mcct
  integer ip, ier, ix, iy, i, jcode
  real fhat, limit, flux
  logical do_fft
  character(len=message_length) :: chain
  character(len=16) :: cmethod
  character(len=48) :: cthread
  character(len=24) :: cname
  character(len=*), parameter :: rname = 'CLEAN'
  integer :: ibeam, ithread, mthread, nplane, max_thread
  integer :: fitted_beam_plane, nfits
  logical :: omp_nested
  ! Mask & List per thread
  integer :: nmask, j, idum
  logical, allocatable, target :: masks(:,:,:)
  integer, allocatable, target :: lists(:,:)
  logical, pointer :: lmask(:,:)
  integer, pointer :: llist(:)
  !
  type (cct_par), allocatable :: tcc(:)
  real, save :: major,minor,angle
  ! Multi Kernel 
  integer, parameter :: ms=3
  integer, parameter :: mk=11
  integer nker(ms)                   ! Kernel size
  real :: kernel(mk,mk,ms)           ! Smoothing kernels
  !
  ! /RESTART option
  integer :: start_iter
  real :: start_flux
  logical :: start_cont
  !
  ! FFTW plan
  integer ndim, nn(2)
  !
  !
  error = .false.
  call imager_tree('SUB_MAJOR_OMP in sub_major.f90')
  !
  if (support_type.eq.support_mask) then    
    if ( hmask%gil%dim(1).ne.hdirty%gil%dim(1) .or. &
      &  hmask%gil%dim(2).ne.hdirty%gil%dim(2) ) then
      call map_message(seve%e,rname,'MASK and DIRTY do not match')
      error =.true.
      return
    endif
  endif
  !
  method = inout_method
  !
  cmethod = method%method
  start_cont = sic_present(opt_start,0)
  if (start_cont) then
    if (method%mosaic) then
      call map_message(seve%w,rname,'Option /RESTART under test for Mosaics (so far)',1)
    endif
    select case(cmethod)
    case ('HOGBOM')
      cmethod = 'SIMPLE'    
    case ('MRC') 
      call map_message(seve%e,rname,'Option /RESTART invalid for MRC',1)
      error = .true.
    case ('SDI')
      call map_message(seve%w,rname,'Option /RESTART not yet validated for SDI',3)
    end select
    if (error) return
  endif
  !
  do_fft = cmethod.ne.'HOGBOM' 
  !
  ! Local variables
  nx = hclean%gil%dim(1)
  ny = hclean%gil%dim(2)
  mx = hbeam%gil%dim(1)
  my = hbeam%gil%dim(2)
  nl = method%nlist
  nc = nx*ny
  np = max(1,hprim%gil%dim(1))
  !
  ! TFBEAM cannot be on the Call sequence here because 
  ! it must be PRIVATE in parallel sections
  if (do_fft) then
    allocate(w_work(nx,ny),w_fft(2*max(nx,ny)),tfbeam(nx,ny,np),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error for TFBEAM')
      error = .true.
      return
    endif
    if (cmethod.eq.'HOGBOM') cmethod = 'SIMPLE'
    ! Set the FFT plan for speed 
    ndim = 2
    nn(1) = nx
    nn(2) = ny
    call fourt_plan(w_work,nn,ndim,-1,1)
    call fourt_plan(w_work,nn,ndim,+1,1)
    !
  else
    allocate(w_work(1,1),w_fft(1),tfbeam(1,1,1),stat=ier)  
    if (ier.ne.0) then
      call map_message(seve%e,rname,'FFT Memory allocation failure')
      error = .true.
      return
    endif
  endif
  !
  ier = 0
  select case (cmethod)
  case ('CLARK')
    allocate(w_comp(nc),w_cct(1,1),mymask(1,1), &
    & s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
    !
    ! Inner Parallelism is only Per-Field (and not yet coded)
    max_thread = np
  case ('SDI')
    allocate(w_comp(nc),w_cct(nx,ny),mymask(nx,ny), &
    & s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
    max_thread = np
  case ('MULTI')
    allocate (w_comp(1),w_cct(nx,ny),mymask(nx,ny), &
      & s_mask(nx,ny),s_resi(nx,ny),t_beam(nx,ny), &
      & s_beam(nx,ny,3),stat=ier)
    max_thread = max(np,32)
  case default
    allocate(w_comp(1), w_cct(1,1),mymask(1,1), &
    & s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
    max_thread = 1
    if (cmethod.eq.'HOGBOM'.or.cmethod.eq.'SIMPLE') then
      max_thread = max(16*nx*ny/1024/1024,np)
    endif
  end select
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Work Arrays Memory allocation failure')
    error = .true.
    return
  endif
  call sic_get_inte('OMP_MAP%INNER',max_thread,error)
  error = .false.
  !
  ! Clean component work array
  allocate(tcc(method%m_iter),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error for TCC')
    error = .true.
    return
  endif
  !
  mthread = 1
  ithread = 1
  ! Define the optimum number of inner threads according to
  !	1) Method  and 2) Images size
  nplane = method%last-method%first+1
  call ompset_thread_nesting(rname,nplane, max_thread, omp_nested)
  mthread = ompget_outer_threads()  !... For the MASKS !...
  !
  ! Global aliases: SHARED here
  if (support_type.eq.support_mask) then    
    !
    ! Allocate the Masks and List per-thread
    nmask = min(hmask%gil%dim(3),nplane,mthread)
    if (nmask.gt.1) then
      ! If not degenerate, allocate enough to handle any case.
      nmask = max(hmask%gil%dim(3),min(nplane,mthread))
      allocate(masks(nx,ny,nmask),lists(nx*ny,nmask),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Mask & List memory allocation error')
        return
      else
        call map_message(seve%i,rname,'Mask & List allocation success')
      endif
    endif
  else
    !
    ! Keep the global association
    nmask = 0
  endif
  !
  cname = method%method
  if (method%mosaic) then
    write(chain,'(A,1pg10.3,A,1pg10.3,A,1pg10.3)') &
        &   'Thresholds: Search ',method%search,'; Restore ', &
        &   method%restor,'; Primary beam ',method%trunca
    call map_message(seve%i,rname,chain)
  endif
  ! 
  !$ if (omp_debug)  Print *,'Number of planes ',nplane
  !
  !
  ! We need some CLEAN beam parameters already at this stage
  !$ if (omp_debug) Print *,'Getting SOME beam parameters '
  iplane = method%first
  ibeam = beam_for_channel(iplane,hdirty,hbeam)
  !
  if (method%mosaic) then
    if (hbeam%gil%faxi.eq.3) then
      allocate(d3beam(hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(4))) !!
      d3beam = hbeam%r4d(:,:,ibeam,:)    ! Contiguous copy
    else
      d3beam => hbeam%r4d(:,:,:,ibeam)    ! Contiguous pointer
    endif
  else
    d3beam => hbeam%r4d(:,:,ibeam,1:1)
  endif
  !
  ! Beam fit
  fitted_beam_plane = 0   ! Set the Clean Beam as unknown
  nfits = 0
  major = 0.
  minor = 0.
  angle = 0.
  !
  ! Loop here if needed
  !$OMP PARALLEL IF (nplane.gt.1) DEFAULT(none) NUM_THREADS(omp_outer_thread) & 
  !$OMP & SHARED(hdirty,hclean,hbeam,hprim,hresid,hmask) &         ! Headers
  !$OMP & SHARED(dummy_prim,dummy_atten,dcct) &                    ! Big arrays
  !$OMP & SHARED(fhat, nx,ny,mx,my,np,nc, mcct,omp_debug) &
  !$OMP & SHARED(inout_method)  &                                  ! A modified structure
  !$OMP & SHARED(major,minor,angle,cmethod, fitted_beam_plane, nfits) &
  !$OMP & SHARED(nmask,masks,lists,mask,list, mthread) &
  !$OMP & PRIVATE(method)                     &                    ! A modified structure
  !$OMP & PRIVATE(iplane, ibeam, error, nl, chain, cthread, cname) &
  !$OMP & PRIVATE(tfbeam,w_work,w_fft,w_comp,w_cct,tcc) &          ! Arrays
  !$OMP & PRIVATE(flux,f_iter,m_iter,limit,ithread,jcode,icct) &
  !$OMP & PRIVATE(ix,iy,i,j) &  ! These were NOT diagnosed by the DEFAULT(none)
  !$OMP & PRIVATE(d3beam,d3prim, idum) &
  !$OMP & PRIVATE(nker,kernel) &                                   ! These could be computed once only ?
  !$OMP & PRIVATE(mymask,s_mask,s_beam,s_resi,t_beam)  &           ! Arrays
  !$OMP & SHARED(start_cont) PRIVATE(start_iter, start_flux) &
  !$OMP & PRIVATE(dirty,resid,clean) PRIVATE(atten,llist,lmask)    ! Pointers
  !
  !$ if (omp_debug)  Print *,'Number of threads ',omp_get_num_threads()
  !
  ! Re-Define the method (in parallel mode, private entities are uninitialized)
  !   Method comes first
  method = inout_method ! And NOT  call copy_method(inout_method, method)
  ! Then its Global aliases, which are PRIVATE inside
  if (.not.inout_method%mosaic) then
    atten => dummy_atten
  endif
  if (nmask.gt.1) then
    method%imask = 0 ! Mask is undefined at beginning if more than 1 plane
  endif
  !
  ithread = 1
  !$ ithread = omp_get_thread_num()+1
  !
  ! Loop on planes
  !$OMP DO SCHEDULE(STATIC,1) 
  do iplane = inout_method%first, inout_method%last
    !
    method%iplane = iplane
    !
    call get_stopping(method%m_iter,method%ares,iplane)
    !$ write(cthread,'(A,I0)') ', Thread ',ithread
    !$ write(cname,'(A,A,I0,A)') trim(cmethod),'(',iplane,')' 
    !
    !$ if (omp_debug) then
      !$OMP CRITICAL
      !$ Print *,'Calling beam_plane '//cthread
      !$ call mapping_print_debug(method)
      !$ Print *,'Done beam_plane '//cthread
      !$OMP END CRITICAL
    !$ endif
    !
    !THAT WAS CRASHING ! call beam_plane(method,hbeam,hdirty)
    ibeam = beam_for_channel(iplane,hdirty,hbeam)
    method%ibeam = ibeam
    !
    ! Get the new mask
    if (nmask.gt.1) then
      if (ithread.gt.mthread) then
        Print *,'Big problem ITHREAD ',ithread,' > ',mthread,' NTHREAD'
      endif
      lmask => masks(:,:,ithread)
      llist => lists(:,ithread)
    else
      lmask => mask
      llist => list
    endif
    !$ if (omp_debug) Print *,'Calling get_maskplane - OMP'
    call get_maskplane(method,hmask,hdirty,lmask,llist)    
    nl = method%nlist
    !$ if (omp_debug) Print *,'Thread ',ithread,' NL ',nl,' Mask plane ',method%imask
    !
    ! Local aliases
    if (method%imask.ge.1) then
      write(chain,'(A,A,I0,A,I0,A,I0,1x,I0)') 'Planes: ', & 
          & '  Image ',method%iplane, & 
          & ', - Beam ', method%ibeam, & 
          & ', - Mask ', method%imask, nl
    else
      write(chain,'(A,A,I0,A,I0,A,I0)') 'Planes: ', & 
          & '  Image ',method%iplane, & 
          & ', - Beam ', method%ibeam
    endif
    !$ chain = trim(chain)//cthread
    call map_message(seve%d,cname,chain)
    !
    ! Local aliases
    dirty => hdirty%r3d(:,:,iplane)
    resid => hresid%r3d(:,:,iplane)
    clean => hclean%r3d(:,:,iplane)
    !
    ! Initialize to Dirty map
    resid = dirty
    !$ if (omp_debug) Print *,'BEAM shape ',ubound(hbeam%r4d)
    !
    if (method%mosaic) then
      if (hbeam%gil%faxi.eq.3) then
        !!Print *,'FAXI = 3 Copy array ',hbeam%gil%dim(1:4)
        d3beam  = hbeam%r4d(:,:,ibeam,:)    ! Contiguous copy
      else
        !!Print *,'FAXI # 3 Pointer to array ',hbeam%gil%dim(1:4)
        d3beam => hbeam%r4d(:,:,:,ibeam)    ! Contiguous pointer
      endif
      d3prim => hprim%r4d(:,:,:,ibeam)
    else
      d3beam  => hbeam%r4d(:,:,ibeam,1:1)  ! Contiguous pointer
      d3prim => dummy_prim
    endif
    !
    if (method%pcycle) call init_plot (method,hdirty,resid)
    !
    ! Prepare beam parameters - subroutine is not Thread safe though...
    !$OMP CRITICAL
    !$ if (omp_debug) Print *,'Critical get_clean '//cthread
    error = .false.
    if (fitted_beam_plane.ne.method%ibeam) then
      !!Print *,'last fitted beam plane ',fitted_beam_plane,' current beam plane ',method%ibeam
      method%major = inout_method%major
      method%minor = inout_method%minor
      method%angle = inout_method%angle
      call get_clean (method, hbeam, d3beam, error)
      if (.not.error) then
        ! Average beam characteristics - Better than nothing, but clearly
        ! not fully realistic when beam differs. However, this only
        ! happens under User Choice.
        fitted_beam_plane = method%ibeam
        major = major + method%major
        minor = minor + method%minor
        angle = angle + method%angle
        nfits = nfits+1
      endif
    else if (nfits.ne.0) then
      method%major = major/nfits
      method%minor = minor/nfits
      method%angle = angle/nfits
      !!Print *,'Method Beam ',method%major,method%minor,method%angle,nfits
    else
      write(chain,'(A,I0,A,I0)') 'No Clean Beam for channel ',method%iplane,' beam plane ',method%ibeam
      call map_message(seve%w,cname,chain)
    endif
    !$ if (omp_debug) Print *,'end Critical get_clean '//cthread
    !$OMP END CRITICAL
    if (error) then
      !return !Oops, cannot do that in a DO parallel...
      cycle
    endif
    !$ if (omp_debug) Print *,'start get_beam '//cthread
    method%method = cmethod ! Needed to have SIMPLE instead of HOGBOM in case of START
    call get_beam (method,hbeam,hresid,hprim,   &
        &        tfbeam,w_work,w_fft,fhat,error, lmask)
    !$ if (omp_debug) Print *,'end get_beam '//cthread
    ! Empty beam case
    if (error) then
      error = .false.
      clean = resid
      !return !Oops, cannot do that in a DO parallel...
      cycle
    endif
    !
    ! Mosaic case
    if (method%mosaic) then
      !$ if (omp_debug) Print *,'Setting weight for ',method%ibeam
      ! Reset search list as the mask may have been altered
      call lmask_to_list (lmask,nx*ny,llist,method%nlist)
      nl = method%nlist
      atten => method%atten(:,:,method%ibeam)
      !$ if (omp_debug) Print *,'Done weight for ',method%ibeam
      resid = resid * atten
    endif
    !
    ! Remove starting model
    if (start_cont) then
      !
      ! Remove the Continuum or Line CCT before Cleaning
      call cct_remove_start(hdirty,iplane,resid,tfbeam,dcct,tcc, &
        & np,d3prim,atten,method%trunca,start_iter,start_flux)
      write(chain,'(A,I0,A,1PG10.3,A)') 'Restarting from iteration ',start_iter,', Flux ',start_flux,' Jy'
      call map_message(seve%i,cname,chain)
    else
      start_iter = 1
      start_flux = 0.
    endif
    !
    ! Performs decomposition into components only
    ! if NL # 0 
    if (nl.ne.0) then
      !$ if (omp_debug) Print *,'Select case '//cmethod//cthread
      select case (cmethod)
      case('HOGBOM','SIMPLE')
        flux = 0
        call hogbom_cycle90 (cname,& 
          &  method%pflux,   &   ! Plot flux
          &  d3beam,mx,my,   &   ! Beam and size
          &  resid,nx,ny,    &   ! Residual and size
          &  method%beam0(1),method%beam0(2),   & ! Beam center
          &  method%box, method%fres, method%ares,   &
          &  method%m_iter, method%p_iter, method%n_iter,   &
          &  method%gain, method%converge,   &    !
          &  tcc(start_iter:),            &   ! Component Structure
          &  lmask,          &   ! Search mask
          &  llist,          &   ! Search list
          &  nl,             &   ! and its size
          &  np,             &   ! Number of fields
          &  d3prim,         &   ! Primary beams
          &  atten,          &   ! Weight
          &  method%trunca, flux, jcode, next_flux)
      case('CLARK')
        !
        ! Find components
        call major_cycle90 (cname,method,hclean,   &   !
          &  clean,          &   ! Final CLEAN image
          &  d3beam,         &   ! Dirty beams
          &  resid,nx,ny,    &   ! Residual and size
          &  tfbeam, w_work, &   ! FT of dirty beam + Work area
          &  w_comp, nc,     &   ! Component storage + Size
          &  method%beam0(1),method%beam0(2),   & ! Beam center
          &  method%patch(1), method%patch(2), method%bgain,   &
          &  method%box,     &
          &  w_fft,          &   ! Work space for FFTs
          &  tcc(start_iter:),            &   ! Component Structure
          &  llist, nl,      &   ! Search list (truncated...)
          &  np,             &   ! Number of fields
          &  d3prim,         &   ! Primary beams
          &  atten,          &   ! Weight
          &  major_plot,     &   ! Plotting routine
          &  next_flux)
      case('SDI')
        !
        ! Find components
        call major_sdi90 (cname,method,hclean,   &
          &  clean,          &   ! Final CLEAN image
          &  d3beam,         &   ! Dirty beams
          &  resid,nx,ny,    &   ! Residual and size
          &  tfbeam, w_work, &   ! FT of dirty beam + Work area
          &  w_comp, nc,     &   ! Component storage + Size
          &  method%beam0(1),method%beam0(2),   & ! Beam center
          &  method%patch(1), method%patch(2), method%bgain,   &
          &  method%box,     &
          &  w_fft,          &   ! Work space for FFTs
          &  w_cct,          &   ! Clean Component Image
          &  llist, nl,      &   ! Search list (truncated...)
          &  np,             &   ! Number of fields
          &  d3prim,         &   ! Primary beams
          &  atten,          &   ! Weight
          &  major_plot)         ! Plotting routine
      case('MULTI')
        !
        ! Performs decomposition into components
        call amaxmask (resid,lmask,nx,ny,ix,iy)
        limit = max(method%ares,method%fres*abs(resid(ix,iy)))
        if (limit.eq.method%ares) then
          write (chain,'(A,1PG10.3,A)')  'Cleaning down to ',limit,' from ARES'
        else
          write (chain,'(A,1PG10.3,A,I0,A,I0,A)')  'Cleaning down to ',limit,' from FRES at (',ix,',',iy,')'
        endif
        call map_message(seve%i,cname,chain)
!!        if (start_cont) dirty = resid  ! Copy residual to local (allocated) "dirty" image
        !
        clean = 0. ! Just in case
        call major_multi90 (cname,method,hclean,   &
          &  dirty,          &   ! hdirty%r3d(:,:,iplane),   &
          &  resid,          &   ! hresid%r3d(:,:,iplane),   &
          &  d3beam,         &   ! hbeam%r4d(:,:,:,method%ibeam),                  &
          &  lmask,          &   ! Check definition of this mask...
          &  clean,          &   ! hclean%r3d(:,:,iplane),   &
          &  nx,ny,          &
          &  tcc(:),         &   ! Component Structure
          &  start_iter,     &   ! Starting iteration
          &  method%m_iter,  &   ! Maximum number of components
          &  limit,          &   ! Residual
          &  method%n_iter,  &   ! Number of components
          &  s_mask,         &   ! Smoothed mask
          &  s_resi,         &   ! Smoothed residual,
          &  t_beam,         &   ! Translated beam
          &  w_work,         &   ! Complex work space
          &  s_beam,         &   ! Smoothed beams
          &  tfbeam,         &   ! Beam Fourier Transform (real)
          &  w_fft, icct,    & 
          &  nker, kernel,   &   ! Kernel sizes & values
          &  np,             &   ! Number of fields
          &  d3prim,         &   ! Primary beams
          &  atten)              ! Weight
        w_cct(:,:) = clean
      end select
      !$ if (omp_debug) Print *,'End Select case '//cmethod//cthread
    else
      ! Empty search area: No Clean components...
      method%n_iter = 0
      flux = 0.0
    endif
    !
    method%n_iter = method%n_iter + start_iter - 1
    !
    if (method%n_iter.ne.0) then
      !$ if (omp_debug) Print *,'Critical clean_make '//cthread
      if (method%residual.ge.0) then
        call clean_make90 (method, hclean, clean, tcc)
      else
        call cct_to_clean (method, hclean, clean, tcc)
      endif
      !$ if (omp_debug) Print *,'End clean_make '//cthread
      if (method%residual.eq.0) then
        if (np.le.1) then
          clean = clean + resid
        else
          clean = clean + resid*atten
          where (atten.eq.0) clean = hclean%gil%bval ! Undefined pixel there
        endif
      endif
    else if (method%residual.eq.0) then
      if (np.le.1) then
        clean = resid
      else
        clean = resid*atten
        where (atten.eq.0) clean = hclean%gil%bval ! Undefined pixel there
      endif
    else
      clean = 0
    endif
    !$ if (omp_debug) Print *,'Finishing '//cthread
    !
    ! Put the TCC structure into its final place
    if (cmethod.eq.'SDI' .or. cmethod.eq.'MULTI') then
      if (method%n_iter.eq.0) then
        dcct(3,iplane,1) = 0
        write (chain,'(A,1PG10.3,A,I6,A,A,I6)')  'Cleaned ',0.0,   &
            &        ' Jy with ',method%n_iter,' components ' &
            &       ,' Plane ',iplane
      else
        if (cmethod.eq.'MULTI') then
          m_iter = method%ninflate*method%m_iter
          mcct = max(icct,mcct) ! 
        else
          m_iter = method%m_iter
        endif
        where (w_cct.ne.0)
          mymask = 1
        elsewhere
          mymask = 0
        end where
        f_iter = sum(mymask)
        if (f_iter.gt.m_iter) then
          write(chain,'(A,I8,A,I8)') 'Iterations overflow ',f_iter, &
              &  ' > ',m_iter
          call map_message(seve%w,cname,chain)
          dcct(3,iplane,1) = 0
          chain = 'UV_RESTORE will not work, consider increasing CLEAN_INFLATE'
        else
          i = 0
          flux = 0.0
          do iy=1,ny
            do ix=1,nx
              if (w_cct(ix,iy).ne.0) then
                i = i+1
                dcct(1,iplane,i) = (dble(ix) -   &
                 & hclean%gil%convert(1,1)) * hclean%gil%convert(3,1) + &
                 & hclean%gil%convert(2,1)
                dcct(2,iplane,i) = (dble(iy) -   &
                 & hclean%gil%convert(1,2)) * hclean%gil%convert(3,2) + &
                 & hclean%gil%convert(2,2)
                dcct(3,iplane,i) = w_cct(ix,iy)
                flux = flux+w_cct(ix,iy)
              endif
            enddo
          enddo
          method%n_iter = i
          if (i.lt.m_iter) dcct(:,iplane,i+1) = 0
          write (chain,'(A,1PG10.3,A,I6,A,A,I6)')  'Cleaned ',flux,   &
              &        ' Jy with ',method%n_iter,' components ' &
              &       ,' Plane ',iplane
        endif
      endif
      !$ chain = trim(chain)//cthread
      call map_message(seve%i,cname,chain)
      !
    else if (cmethod.ne.'MRC') then
      do i=1,method%n_iter
        dcct(1,iplane,i) = (dble(tcc(i)%ix) -   &
            & hclean%gil%convert(1,1)) * hclean%gil%convert(3,1) + &
            & hclean%gil%convert(2,1)
        dcct(2,iplane,i) = (dble(tcc(i)%iy) -   &
            & hclean%gil%convert(1,2)) * hclean%gil%convert(3,2) + &
            & hclean%gil%convert(2,2)
        dcct(3,iplane,i) = tcc(i)%value
      enddo
      if (method%n_iter.lt.method%m_iter) then
        dcct(:,iplane,method%n_iter+1) = 0
      endif
      !
      ! Could be for all Methods there...
      if (cmethod.eq.'HOGBOM'.or.cmethod.eq.'SIMPLE') then
        if (start_flux.eq.0) then
          write (chain,'(A,1PG10.3,A,I6,A,A,I6)')  'Cleaned ',flux,   &
            &        ' Jy with ',method%n_iter,' components ' &
            &       ,' Plane ',iplane
        else
          write (chain,'(A,1PG10.3,A,I6,A,1PG10.3,A,I6)')  'Cleaned ',flux,   &
            &        ' Jy with ',method%n_iter,' components (total ', &
            &       flux+start_flux,' Jy),  Plane ',iplane
        endif
        !$ chain = trim(chain)//cthread
        call map_message(seve%i,cname,chain)
      endif      
      !
    endif
    !$ if (omp_debug) Print *,'End loop '//cthread
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  if (nfits.ne.0) then
    inout_method%major = major/nfits
    inout_method%minor = minor/nfits
    inout_method%angle = angle/nfits
  else
    call map_message(seve%e,cname,'No valid Clean beam in data')
  endif
  !$  call omp_set_nested(omp_nested)
  !$  if (nplane.lt.mthread) call omp_set_num_threads(mthread)
  !
  if (allocated(masks)) then
    deallocate(masks,lists)
  endif
  !
  ! PHAT
  !!Print *,'sub_major PHAT ',method%phat
  ! This logic is only valid for One beam for all...
  if (method%phat.ne.0) then
    fhat = 1.0/fhat
    if (method%mosaic) then
      d3beam = d3beam*fhat
      do ip=1,np
        d3beam(method%beam0(1),method%beam0(2),ip) =   &
            &          d3beam(method%beam0(1),method%beam0(2),ip) -   &
            &          method%phat
      enddo
    else
      d3beam = d3beam*fhat
    endif
  endif
  !
  ! Set the blanking value for Mosaics
  if (method%mosaic) then
    hclean%gil%eval = 0
  endif
  !
  ! Clean work space: in principle, Fortran 95 does it for you
  deallocate(w_comp,w_cct,mymask, &
    &  s_mask,s_resi,t_beam,s_beam, &
    &  w_work,w_fft, stat=ier)
  !
end subroutine sub_major_omp
!
!
subroutine sub_major_lin(method,hdirty,hresid,hclean,   &
     &    hbeam,hprim,hmask,dcct,mask,list,error,        &
     &    major_plot, next_flux)
  use gkernel_interfaces
  use imager_interfaces, except_this=>sub_major_lin
  use clean_def
  use image_def
  use gbl_message
  !--------------------------------------------------------------
  ! @ private
  !
  ! IMAGER Clean/Mosaic
  !     Perfom a CLEAN based on all CLEAN algorithms,
  !     except the MRC (Multi Resolution CLEAN)
  !     which requires a different tool
  !     Works for mosaic also, except for the Multi Scale clean
  !     (not yet implemented for this one, but feasible...)
  !--------------------------------------------------------------
  external :: major_plot
  external :: next_flux
  !
  type (clean_par), intent(inout) :: method
  type (gildas), intent(in) :: hdirty
  type (gildas), intent(inout) :: hbeam
  type (gildas), intent(inout) :: hclean
  type (gildas), intent(inout) :: hresid
  type (gildas), intent(in) :: hprim
  type (gildas), intent(in) :: hmask
  real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
  logical, intent(in), target :: mask(:,:)
  integer, intent(in), target :: list(:)
  logical, intent(inout) ::  error
  !
  real, pointer :: dirty(:,:)  ! Dirty map
  real, pointer :: resid(:,:)  ! Iterated residual
  real, pointer :: clean(:,:)  ! Clean Map
  real, pointer :: d3prim(:,:,:)   ! Primary beam (for one frequency)
  real, pointer :: d3beam(:,:,:)   ! Dirty beam (for one frequency)
  real, pointer :: atten(:,:)     ! Mosaic atten
  !
  real, allocatable :: tfbeam(:,:,:)
  real, allocatable :: w_fft(:)    ! TF work area
  complex, allocatable :: w_work(:,:)  ! Work area
  type(cct_par), allocatable :: w_comp(:)
  real, allocatable :: w_cct(:,:)
  logical, allocatable :: s_mask(:,:)
  real, allocatable :: s_beam(:,:,:), t_beam(:,:), s_resi(:,:)
  integer, allocatable :: mymask(:,:)
  integer :: f_iter, m_iter
  !
  real, target :: dummy_prim(1,1,1), dummy_atten(1,1)
  integer iplane
  integer nx,ny,np,nl,mx,my,nc, icct
  integer ip, ier, ix, iy, i, jcode
  real fhat, limit, flux
  logical do_fft
  character(len=message_length) :: chain
  character(len=12) :: cname 
  integer :: nplane
  !
  type (cct_par), allocatable :: tcc(:)
  !
  integer, pointer :: llist(:)
  logical, pointer :: lmask(:,:)
  ! Multi Kernel 
  integer, parameter :: ms=3
  integer, parameter :: mk=11
  integer nker(ms)                   ! Kernel size
  real :: kernel(mk,mk,ms)           ! Smoothing kernels
  !
  call imager_tree('SUB_MAJOR_LIN in sub_major.f90')
  error = .false.
  do_fft = method%method.ne.'HOGBOM'
  cname = method%method
  !
  llist => list
  lmask => mask
  !
  ! Local variables
  nx = hclean%gil%dim(1)
  ny = hclean%gil%dim(2)
  mx = hbeam%gil%dim(1)
  my = hbeam%gil%dim(2)
  nl = method%nlist
  nc = nx*ny
  np = max(1,hprim%gil%dim(1)) ! or ! np = hbeam%gil%dim(3)
  !
  if (do_fft) then
    allocate(w_work(nx,ny),w_fft(2*max(nx,ny)),tfbeam(nx,ny,np),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,cname,'Memory allocation error for TFBEAM')
      error = .true.
      return
    endif
  else
    allocate(w_work(1,1),w_fft(1),tfbeam(1,1,1),stat=ier)  
    if (ier.ne.0) then
      call map_message(seve%e,cname,'FFT Memory allocation failure')
      error = .true.
      return
    endif
  endif
  !
  if (method%method.eq.'CLARK') then
    allocate(w_comp(nc), &
    & w_cct(1,1),mymask(1,1),s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
  elseif  (method%method.eq.'SDI') then
    allocate(w_comp(nc),w_cct(nx,ny),mymask(nx,ny), &
    & s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
  elseif  (method%method.eq.'MULTI') then
    allocate (s_mask(nx,ny),s_resi(nx,ny),t_beam(nx,ny), &
      & s_beam(nx,ny,3),w_cct(nx,ny),mymask(nx,ny), &
      & w_comp(1), stat=ier)
  else
    allocate(w_comp(1), &
    & w_cct(1,1),mymask(1,1),s_mask(1,1),s_resi(1,1),t_beam(1,1), &
    & s_beam(1,1,3),stat=ier)
  endif
  if (ier.ne.0) then
    call map_message(seve%e,cname,'Work Arrays Memory allocation failure')
    error = .true.
    return
  endif
  !
  ! Clean component work array
  allocate(tcc(method%m_iter),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,cname,'Memory allocation error for TCC')
    error = .true.
    return
  endif
  !
  nplane = method%last-method%first+1
  !
  ! Global aliases
  if (method%mosaic) then
    d3prim => dummy_prim
  else
    d3prim => dummy_prim
    atten => dummy_atten
  endif
  !
  cname = method%method
  !
  do iplane = method%first, method%last
    !
    method%iplane = iplane
    call beam_plane(method,hbeam,hdirty)
    ! Get the new mask (if any...)
    call get_maskplane(method,hmask,hdirty,lmask,llist)    
    nl = method%nlist
    !
    ! Local aliases
    if (method%imask.ge.1) then
      write(chain,'(A,I6,I6,I6,A,I2)') 'Image, Beam & Mask planes ',   &
          &      method%iplane,method%ibeam,method%imask
    else
      write(chain,'(A,I6,I6,A,I2)') 'Image & Beam planes ',   &
          &      method%iplane,method%ibeam
    endif
    call map_message(seve%i,cname,chain)
    dirty => hdirty%r3d(:,:,iplane)
    resid => hresid%r3d(:,:,iplane)
    clean => hclean%r3d(:,:,iplane)
    d3beam => hbeam%r4d(:,:,:,method%ibeam)
    if (method%mosaic) d3prim => hprim%r4d(:,:,:,method%ibeam)
    !
    ! Initialize to Dirty map
    resid = dirty
    if (method%pcycle) call init_plot (method,hdirty,resid)
    !
    ! Prepare beam parameters - subroutine is not Thread safe though...
    call get_clean (method, hbeam, d3beam, error)
    if (error) then
      !return !Oops, cannot do that in a DO parallel...
      cycle
    endif
    call get_beam (method,hbeam,hresid,hprim,   &
        &        tfbeam,w_work,w_fft,fhat,error, lmask)
    ! Empty beam case
    if (error) then
      error = .false.
      clean = resid
      !return !Oops, cannot do that in a DO parallel...
      cycle
    endif
    !
    ! Mosaic case
    if (method%mosaic) then
      ! Reset search list as the mask may have been altered
      call lmask_to_list (lmask,nx*ny,llist,method%nlist)
      atten=> method%atten(:,:,method%ibeam)
      resid = resid * atten
    endif
    !
    !
    ! Performs decomposition into components
    select case (method%method)
    case('HOGBOM')
      call hogbom_cycle90 (cname,method%pflux,   &   ! Plot flux
           &        d3beam,mx,my,   & ! Beam and size
           &        resid,nx,ny,   & ! Residual and size
           &        method%beam0(1),method%beam0(2),   & ! Beam center
           &        method%box, method%fres, method%ares,   &
           &        method%m_iter,  method%p_iter, method%n_iter,   &
           &        method%gain, method%converge,   &    !
           &        tcc,   &         ! Component Structure
           &        lmask,   & ! Search mask
           &        llist,   & ! Search list
           &        nl,   &          ! and its size
           &        np,   &          ! Number of fields
           &        d3prim,   &       ! Primary beams
           &        atten,   &      ! Weight
           &        method%trunca, flux, jcode, next_flux)
    case('CLARK')
      !
      ! Find components
      call major_cycle90 (cname,method,hclean,   &   !
           &        clean,   &       ! Final CLEAN image
           &        d3beam,   &       ! Dirty beams
           &        resid,nx,ny,   & ! Residual and size
           &        tfbeam, w_work,   &  ! FT of dirty beam + Work area
           &        w_comp, nc,       &  ! Component storage + Size
           &        method%beam0(1),method%beam0(2),   & ! Beam center
           &        method%patch(1), method%patch(2), method%bgain,   &
           &        method%box,   &
           &        w_fft,   &       ! Work space for FFTs
           &        tcc,   &         ! Component table
           &        llist, nl,   &   ! Search list (truncated...)
           &        np,                & ! Number of fields
           &        d3prim,            & ! Primary beams
           &        atten,            & ! Weight
           &        major_plot,        & ! Plotting routine
           &        next_flux)
    case('SDI')
      !
      ! Find components
      call major_sdi90 (cname,method,hclean,   &
           &        clean,             & ! Final CLEAN image
           &        d3beam,            & !BUG  (:,:,method%ibeam),   & ! Dirty beams
           &        resid,nx,ny,       & ! Residual and size
           &        tfbeam, w_work,    & ! FT of dirty beam + Work area
           &        w_comp, nc,        & ! Component storage + Size
           &        method%beam0(1),method%beam0(2),   & ! Beam center
           &        method%patch(1), method%patch(2), method%bgain,   &
           &        method%box,   &
           &        w_fft,             & ! Work space for FFTs
           &        w_cct,             & ! Clean Component Image
           &        llist, nl,         & ! Search list (truncated...)
           &        np,                & ! Number of fields
           &        d3prim,            & ! Primary beams
           &        atten,             & ! Weight
           &        major_plot)          ! Plotting routine
    case('MULTI')
      !
      ! Performs decomposition into components
      call amaxmask (resid,lmask,nx,ny,ix,iy)
      limit = max(method%ares,method%fres*abs(resid(ix,iy)))
      call map_message(seve%i,method%method,chain)
      if (limit.eq.method%ares) then
        write (chain,'(A,1PG10.3,A)')  'Cleaning down to ',limit,' from ARES'
      else
        write (chain,'(A,1PG10.3,A,I7,I7)')  'Cleaning down to ',limit,' from FRES at ',ix,iy
      endif
      call map_message(seve%i,cname,chain)
      !
      clean = 0 ! Reset
      call major_multi90 (cname,method,hclean,   &
           &        hdirty%r3d(:,:,iplane),   &
           &        hresid%r3d(:,:,iplane),   &
           &        d3beam,             & ! BUG hbeam%r4d(:,:,:,method%ibeam),                  &
           &        lmask,              & ! Check definition of this mask...
           &        hclean%r3d(:,:,iplane),   &
           &        nx,ny,                    &
           &        tcc,   &         ! Clean component
           &        1,     &         ! Starting iteration
           &        method%m_iter, & ! Maximum number of components
           &        limit,   &       ! Residual
           &        method%n_iter, & ! Number of components
           &        s_mask,   &      ! Smoothed mask
           &        s_resi,   &      ! Smoothed residual,
           &        t_beam,   &      ! Translated beam
           &        w_work,   &      ! Complex work space
           &        s_beam,   &      ! Smoothed beams
           &        tfbeam,   &      ! Beam Fourier Transform (real)
           &        w_fft, icct,  &  !
           &        nker, kernel, &  ! Kernel sizes & values
           &        np,           &  ! Number of fields
           &        d3prim,       &  ! Primary beams
           &        atten)           ! Weight
      w_cct(:,:) = clean
    end select
    !
    ! Add clean components and residuals to produce clean map
    if (method%n_iter.ne.0) then
      call clean_make90 (method, hclean, clean, tcc)
      if (np.le.1) then
        clean = clean + resid
      else
        clean = clean + resid*atten
        where (atten.eq.0) clean = hclean%gil%bval ! Undefined pixel there
      endif
    else
      if (np.le.1) then
        clean = resid
      else
        clean = resid*atten
        where (atten.eq.0) clean = hclean%gil%bval ! Undefined pixel there
      endif
    endif
    !
    ! Put the TCC structure into its final place
    if (method%method.eq.'MULTI' .or. method%method.eq.'SDI') then
      if (method%method.eq.'MULTI') then
        m_iter = method%ninflate*method%m_iter
      else
        m_iter = method%m_iter
      endif
      where (w_cct.ne.0)
        mymask = 1
      elsewhere
        mymask = 0
      end where
      f_iter = sum(mymask)
      if (f_iter.gt.m_iter) then
        write(chain,'(A,I8,A,I8)') 'Iterations overflow ',f_iter, &
            &  ' > ',m_iter
        call map_message(seve%w,cname,chain)
        dcct(3,iplane,1) = 0
        chain = 'UV_RESTORE will not work, consider increasing CLEAN_INFLATE'
        call map_message(seve%i,cname,chain)
      else
        i = 0
        flux = 0
        do iy=1,ny
          do ix=1,nx
            if (w_cct(ix,iy).ne.0) then
              i = i+1
              dcct(1,iplane,i) = (dble(ix) -   &
               & hclean%gil%convert(1,1)) * hclean%gil%convert(3,1) + &
               & hclean%gil%convert(2,1)
              dcct(2,iplane,i) = (dble(iy) -   &
               & hclean%gil%convert(1,2)) * hclean%gil%convert(3,2) + &
               & hclean%gil%convert(2,2)
              dcct(3,iplane,i) = w_cct(ix,iy)
              flux = flux+w_cct(ix,iy)
            endif
          enddo
        enddo
        method%n_iter = i
        write (chain,'(A,1PG10.3,A,I6,A,A,I6,A,I2)')  'Cleaned ',flux,   &
            &        ' Jy with ',method%n_iter,' components ' &
            &       ,' Plane ',iplane
        call map_message(seve%i,cname,chain)
      endif
    else if (method%method.ne.'MRC') then
      do i=1,method%n_iter
        dcct(1,iplane,i) = (dble(tcc(i)%ix) -   &
            & hclean%gil%convert(1,1)) * hclean%gil%convert(3,1) + &
            & hclean%gil%convert(2,1)
        dcct(2,iplane,i) = (dble(tcc(i)%iy) -   &
            & hclean%gil%convert(1,2)) * hclean%gil%convert(3,2) + &
            & hclean%gil%convert(2,2)
        dcct(3,iplane,i) = tcc(i)%value
      enddo
      if (method%n_iter.lt.method%m_iter) then
        dcct(3,iplane,method%n_iter+1) = 0
      endif
      if (method%method.eq.'HOGBOM') then
        write (chain,'(A,1PG10.3,A,I6,A,A,I6,A,I2)')  'Cleaned ',flux,   &
          &        ' Jy with ',method%n_iter,' components ' &
          &       ,' Plane ',iplane
        call map_message(seve%i,cname,chain)
      endif
      !
    endif
  enddo
  !
  ! PHAT
  if (method%phat.ne.0) then
    fhat = 1.0/fhat
    if (method%mosaic) then
      d3beam = d3beam*fhat
      do ip=1,np
        d3beam(method%beam0(1),method%beam0(2),ip) =   &
            &          d3beam(method%beam0(1),method%beam0(2),ip) -   &
            &          method%phat
      enddo
    else
      d3beam = d3beam*fhat
    endif
  endif
  !
  ! Set the blanking value for Mosaics
  if (method%mosaic) then
    hclean%gil%eval = 0
  endif
  !
  ! Clean work space: in principle, Fortran 95 does it for you
  if (method%method.eq.'CLARK') then
    deallocate(w_comp,stat=ier)
  elseif  (method%method.eq.'SDI') then
    deallocate(w_comp,w_cct,mymask)
  elseif  (method%method.eq.'MULTI') then
    deallocate (s_mask,s_resi,t_beam,s_beam,w_cct,mymask)
  endif
  if (do_fft) then
    deallocate(w_work,w_fft)
  endif
  !
end subroutine sub_major_lin
!
!
subroutine get_beam(method,hbeam,hresid,hprim,  &
     &    tfbeam,w_work,w_fft,fhat,error, mask)
  use gkernel_interfaces
  use imager_interfaces, except_this => get_beam
  use clean_def
  use image_def
  use gbl_message
  !-----------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !    Get beam related information
  !-----------------------------------------------------------------
  type (clean_par), intent(inout) :: method
  type (gildas), intent(in)  :: hbeam
  type (gildas), intent(in)  :: hresid
  type (gildas), intent(in)  :: hprim
  real, intent(inout) :: tfbeam(hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(3))
  complex, intent(inout) :: w_work(hbeam%gil%dim(1),hbeam%gil%dim(2))
  real, intent(inout) :: fhat,w_fft(:)
  logical, intent(inout) :: error
  logical, optional, intent(inout) :: mask(:,:)
  !
  ! Note that HRESID is unused here...
  integer ip,ib,nx,ny,np,nb
  real beam_min,beam_max,beam_area,f
  integer ix_min,ix_max,iy_min,iy_max
  logical do_fft
  character(len=message_length) :: chain
  character(len=*), parameter :: rname = 'CLEAN'
  real, pointer :: d2beam(:,:)      ! Beam (per single field & frequency)
  real, pointer :: d3prim(:,:,:)    ! Primary beams (per frequency)
  real, pointer :: d3beam(:,:,:)    ! Dirty beam (per frequency)
  real, pointer :: atten(:,:,:)
  integer :: ier
  !
  nx = hbeam%gil%dim(1)
  ny = hbeam%gil%dim(2)
  np = hbeam%gil%dim(3)
  nb = hbeam%gil%dim(4)
  !
  atten => method%atten
  do_fft = method%method.ne.'HOGBOM'   &
       &    .and. method%method.ne.'MX'
  !
  f = 1.0   ! No Prussian Hat
  !
  if (method%mosaic) then
    if (method%verbose)  Print *,'Testing MOSAIC case ',hbeam%gil%dim(1:hbeam%gil%ndim),' Method ',method%method
    if (.not.present(mask)) then
      call map_message(seve%f,rname,'Programming error: Missing MASK argument with MOSAIC mode',1)
      error = .true.
      return
    endif
    !
    ! At this stage, the Beam may have beam re-shaped to 3 dimensions
    if (hbeam%gil%faxi.eq.3) then
      allocate(d3beam(hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(4)),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'BEAM memory allocation error',2)
        error = .true.
        return
      endif
      !
      ! Copy the relevant beams at the appropriate frequency
      d3beam = hbeam%r4d(:,:,method%ibeam,:)   ! and not (:,:,:,method%ibeam)
    else
      ! Copy the relevant beams at the appropriate frequency
      d3beam => hbeam%r4d(:,:,:,method%ibeam)   ! and not (:,:,:,method%ibeam)
    endif
    !
    np = hprim%gil%dim(1)
    !
    ! Analyze beam
    do ip = 1,np
      if (method%verbose) then
        write(chain,101) 'Field ',ip,'/',np
        call map_message(seve%i,rname,chain)
        Print *,'NX,NY,IP ',nx,ny,ip,' Zone ',method%bzone
      endif
      call maxmap(d3beam(:,:,ip),nx,ny,method%bzone,   &
           &        beam_max,ix_max,iy_max,beam_min,ix_min,iy_min)
      if (method%verbose) then
        write(chain,'(A,1PG10.3,A,I6,I6,A,1PG10.3,A,I6,I6)') &
          &   'Beam max. ',beam_max,' at ',ix_max,iy_max,  &
          &   ', Min. ',beam_min,' at ',ix_min,iy_min
        call map_message(seve%i,rname,chain)
      endif
      !
      if (do_fft) then
        call init_convolve (ix_max,iy_max,nx,ny,   &
            &          d3beam(:,:,ip),w_work,beam_area,w_fft)
        tfbeam(:,:,ip) = real(w_work)
        if (method%verbose) then
          write(chain,102) 'Beam area is ',beam_area
          call map_message(seve%i,rname,chain)
        endif
      endif
      !
      ! Prussian Hat - NOT SUPPORTED HERE
    enddo
    !
    if (method%method.ne.'HOGBOM' .and. method%method.ne.'MULTI' .and. method%method.ne.'SIMPLE') then
      method%bgain = 0
      call mos_sidelobe (d3beam,nx,ny,ix_max,iy_max,   &
           &        method%patch(1),method%patch(2),method%bgain,np)
      write(chain,102) 'Sidelobe is ',method%bgain
      call map_message(seve%i,rname,chain)
    endif
    !
    ! Define the weight function and truncate the mask
    do ib=1,nb
      d3prim => hprim%r4d(:,:,:,ib)
      call compute_atten(nx,ny,np,atten(:,:,ib),d3prim,mask,   &
          &      method%search,method%restor,method%trunca)
    enddo
    if (hbeam%gil%faxi.eq.3) deallocate (d3beam)    !    (hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(3))
    !
  else
    d2beam  => hbeam%r4d(:,:,method%ibeam,1)    !Single field case, contiguous pointer
    !
    ! Simple case
    call maxmap(d2beam,nx,ny,method%bzone,   &
        &      beam_max,ix_max,iy_max,   &
        &      beam_min,ix_min,iy_min)
    if (method%verbose) then
      write(chain,'(A,1PG10.3,A,I6,I6,A,1PG10.3,A,I6,I6)') &
        &   'Beam max. ',beam_max,' at ',ix_max,iy_max,  &
        &   ', Min. ',beam_min,' at ',ix_min,iy_min
      call map_message(seve%i,rname,chain)
    endif
    if (beam_max.eq.0.0) then
      call map_message(seve%w,rname,'Beam is empty')
      error = .true.
      return
    endif
    call comshi (d2beam,nx,ny,ix_max,iy_max,method%bshift)
    if (do_fft) then
      call init_convolve (ix_max,iy_max,nx,ny,   &
           &        d2beam,w_work,beam_area,w_fft)
      tfbeam(:,:,1)  = real(w_work)
      if (method%verbose) then
         write(chain,102) 'Beam area is ',beam_area
         call map_message(seve%i,rname,chain)
      endif
    endif
    !
    ! Prussian Hat
    if (method%phat.ne.0) then
      d2beam(ix_max,iy_max) = d2beam(ix_max,iy_max)   &
           &         + method%phat
      f = 1.0/d2beam(ix_max,iy_max)
      d2beam(:,:) = d2beam*f
    endif
    !
    if (method%method.ne.'HOGBOM' .and. method%method.ne.'MULTI') then
      call find_sidelobe (d2beam,nx,ny,ix_max,iy_max,   &
           &        method%patch(1),method%patch(2),method%bgain)
      write(chain,102) 'Sidelobe is ',method%bgain
      call map_message(seve%d,rname,chain)
    endif
  endif
  method%beam0 = (/ix_max,iy_max/)
  if (method%phat.ne.0) fhat = f
  error = .false.
  return
  !
101 format(a,i3,a,i3)
102 format(a,1pg10.3,a,i6,i6)
end subroutine get_beam
!
subroutine get_stopping(miter,ares,iplane)
  use clean_arrays
  !
  integer, intent(inout) :: miter
  real, intent(inout) :: ares
  integer, intent(in) :: iplane
  !
  if (iplane.ge.1 .and. iplane.le.niter_listsize) then
    miter = niter_list(iplane)
  endif
  if (iplane.ge.1 .and. iplane.le.ares_listsize) then
    ares = ares_list(iplane)
  endif
end subroutine get_stopping
