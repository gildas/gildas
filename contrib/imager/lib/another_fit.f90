!
subroutine get_clean_para (method,hbeam,dbeam,error)
  use gkernel_interfaces
  use imager_interfaces, only : fibeam
  use clean_def
  use image_def
  !----------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Get Clean beam parameters
  !----------------------------------------------------------------------
  type (gildas), intent(in) :: hbeam
  type (clean_par), intent(inout) :: method
  real, intent(in) :: dbeam(hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(3))
  logical, intent(out) :: error
  !
  integer :: nx,ny
  !
  ! Use beam patch to search for maximum
  error = .false.
  nx =  hbeam%gil%dim(1)
  ny =  hbeam%gil%dim(2)
  call fibeam_para ('CLEAN',dbeam,nx,ny,   &
       &    method%patch(1),method%patch(2),method%thresh,   &
       &    method%major,method%minor,method%angle,   &
       &    hbeam%gil%convert,error)
end subroutine get_clean_para
!
subroutine fibeam_para (name,dirty,nx,ny,nbx,nby,thre,majo,mino,pa,convert,error)
  use gildas_def
  use gkernel_interfaces
  use imager_interfaces, except_this => fibeam_para
  use mod_fitbeam
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !
  ! MAPPING
  !   Find beam parameters for deconvolution
  !   Fit an elliptical gaussian clean beam to main beam
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: name          ! Caller's name
  integer, intent(in) :: nx                     ! X size
  integer, intent(in) :: ny                     ! Y size
  real, intent(in) :: dirty(nx,ny)              ! Dirty beam
  integer, intent(in) :: nbx                    ! X patch
  integer, intent(in) :: nby                    ! Y patch
  real, intent(inout) :: thre                   ! Threshold
  real, intent(inout) :: majo                   ! Major axis
  real, intent(inout) :: mino                   ! Minor axis
  real, intent(inout) :: pa                     ! Position Angle
  real(8), intent(in) :: convert(3,2)           ! Conversion formula
  logical, intent(inout) :: error               ! Error flag
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  !
  real, allocatable :: rp_values(:)    ! Values
  real, allocatable :: rp_coords(:,:)  ! Coordinates
  real, allocatable :: rp_x(:)         ! One axis only
  ! Local
  integer, allocatable :: ipl(:), ipk(:), ipw(:)
  character(len=80) :: mess
  integer :: blc(2),trc(2),ml,nf,mx,my,ier
  real :: dmaj,dmin,fact
  real(8) :: xinc
  logical :: liter
  !
  error = .false.
  !
  ! Do nothing if specified
  if (majo.ne.0.0) then
    if (mino.eq.0.0) then
      mino = majo
    endif
    return
  else
    if (mino.ne.0.0) then
      majo = mino
      return
    endif
  endif
  liter = .false.
  mx = min(nbx,nx/6)
  my = min(nby,ny/6)
  mx = max(32,mx)
  my = max(32,my)
  !
  ! Assume beam maximum is near NX/2 NY/2
  blc(1) = nx/2+1-mx
  blc(2) = ny/2+1-my
  mx = 2*mx
  my = 2*my
  trc(1) = blc(1)+mx-1
  trc(2) = blc(2)+my-1
  ! 30 % sidelobes define the main beam
  if (thre.le.0. .or. thre.ge.1.0) thre = 0.30
  !
  ! Load beam patch into work array
  ml = mx*my
  allocate (rp_values(ml), rp_coords(2,ml), ipl(ml), ipk(ml), ipw(ml), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',ier
    call map_message(seve%e,name,mess)
    error = .true.
    return
  endif
  !
  ! Make a segmentation to define "Main" beam
  ! IPK is used here as a work array...
  call threshold (dirty,nx,ny,blc,trc,ipl,mx,my,   &
     &    nf,ipk,ipw,ml,thre,   &
     &    0.0, -1.0)
  !
  ! Load values and coordinates
  call loadv (dirty,nx,ny,blc,trc,ipl,mx,my,   &
     &    nv,rp_values,rp_coords)
  !
  ! Convert to user coordinates
  xinc = max(abs(convert(3,1)),abs(convert(3,2)))
  call userc (nv,rp_coords,   &
     &    convert(1,1),convert(2,1),convert(3,1)/xinc,   &
     &    convert(1,2),convert(2,2),convert(3,2)/xinc)
  !
  ! That would work on 1-D, but we need the 2-D Gaussian
  !
  ! Apply Caruana algorithm : the Log of a Gaussian is a Parabola
  rp_values = log(rp_values)
  !
  do k=1,2
    rp_x(:) = rp_coords(k,:)
    call fit_parabola(nv,rp_x,rp_values,a,b,c,d)
    !
    ! Now back to Gaussian characteristics
    mu = -b/(2*c)           ! Position
    sigma = sqrt(-1/(2*c))  ! Sigma
    a = exp(a-b**2/(4*c))   ! Intensity
    Print *,'Mu ',mu,' sigma ',sigma,' A ',a
  enddo
end

subroutine fit_parabola(n,x,y,a,b,c,d)
  !---------------------------------------------------------------------
  ! @ public
  !
  ! Find the characteristics of a parabola
  ! Parabola definition a + b.x + c.x^2
  !---------------------------------------------------------------------
  integer, intent(in) :: n
  real, intent(in) :: x(:)  ! X coordinates
  real, intent(in) :: y(:)  ! Y values
  real, intent(out) :: a
  real, intent(out) :: b
  real, intent(out) :: c
  real, intent(out) :: d    ! rms of fit
  !
  real :: a0, a1, a2, a3, a4
  real :: b0, b1, b2, d1
  integer :: m
  !
  a0 = 1; a1 = 0; a2 = 0; a3 = 0; a4 = 0
  b0 = 0; b1 = 0; b2 = 0
  do m = 1, n
    a1 = a1 + x(m)
    a2 = a2 + x(m) * x(m)
    a3 = a3 + x(m) * x(m) * x(m)
    a4 = a4 + x(m) * x(m) * x(m) * x(m)
    b0 = b0 + y(m)
    b1 = b1 + y(m) * x(m)
    b2 = b2 + y(m) * x(m) * x(m)
  enddo
  a1 = a1 / n; a2 = a2 / n; a3 = a3 / n; a4 = a4 / n
  b0 = b0 / n; b1 = b1 / n; b2 = b2 / n
  d = a0 * (a2 * a4 - a3 * a3) - a1 * (a1 * a4 - a2 * a3) + a2 * (a1 * a3 - a2 * a2)
  a = b0 * (a2 * a4 - a3 * a3) + b1 * (a2 * a3 - a1 * a4) + b2 * (a1 * a3 - a2 * a2)
  a = a / d
  b = b0 * (a2 * a3 - a1 * a4) + b1 * (a0 * a4 - a2 * a2) + b2 * (a1 * a2 - a0 * a3)
  b = b / d
  c = b0 * (a1 * a3 - a2 * a2) + b1 * (a2 * a1 - a0 * a3) + b2 * (a0 * a2 - a1 * a1)
  c = c / d
  !
  ! Evaluation of standard deviation d
  d = 0.
  do m = 1, n
    d1 = y(m) - a - b * x(m) - c * x(m) * x(m)
    d = d + d1 * d1
  enddo
  d = SQRT(d / (n - 3))
end subroutine fit_parabola
