subroutine cct_prepare(line,nsizes,a_method,task,error)
  use gkernel_interfaces
  use imager_interfaces, only : get_i4list_fromsic, get_r4list_fromsic, & 
    & map_message, sub_read_image
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !---------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Prepare the Clean Component Table for further use
  !---------------------------------------------------
  character(len=*), intent(in) :: line      ! Command line
  integer, intent(in) :: nsizes(3)          ! Cube Size
  type(clean_par), intent(in) :: a_method   ! Input method
  character(len=*), intent(in) :: task      ! Caller name
  logical, intent(out) :: error  !
  !
  integer, parameter :: opt_ares=5   ! /ARES 
  integer, parameter :: opt_start=6  ! /RESTART 
  integer, parameter :: opt_range=10 ! No /RANGE option there
  !
  ! Local
  integer :: nc                 ! Number of channels
  character(len=filename_length) :: name
  character(len=120) :: mess
  integer(kind=4) :: ier,x_iter,ns,i,n
  integer, parameter :: opt_iter=4
  real, allocatable :: tcct(:,:,:)
  logical :: restart
  real(8) :: drange(2)          ! Range to be read (0,0)
  character(len=12) :: crange  ! Type of range
  logical :: compact
  integer, parameter :: cct_type=10
  !
  nc = nsizes(3)
  error = .false.
  x_iter = a_method%m_iter
  !
  ! Use up to nc = hdirty%gil%dim(3)
  if (sic_present(opt_iter,0)) then
    niter_listsize = hdirty%gil%dim(3)
    if (allocated(niter_list)) deallocate(niter_list)
    allocate(niter_list(niter_listsize),stat=ier)
    call get_i4list_fromsic(task,line,opt_iter,niter_listsize, &
      & niter_list,error)
    if (error) return
    ! 
    x_iter = maxval(niter_list)
  endif
  !
  if (sic_present(opt_ares,0)) then
    ares_listsize = hdirty%gil%dim(3)
    if (allocated(ares_list)) deallocate(ares_list)
    allocate(ares_list(ares_listsize),stat=ier)
    call get_r4list_fromsic(task,line,opt_ares,ares_listsize, &
      & ares_list,error)
    if (error) return
  endif
  !
  ! /RESTART [FileName]  option
  if (sic_present(opt_start,0)) then
    !
    if (sic_present(opt_start,1)) then
      !
      ! Load new CCT table if specified
      call sic_ch(line,opt_start,1,name,n,.true.,error)
      if (error) return
      !
      ! Read specified data file as Clean Component Table
      drange = 0.d0
      crange = 'CHANNEL'
      compact = .false.
      call sub_read_image (name,cct_type,drange,crange,compact,opt_range,error)
      if (error) return      
    else
      ! 
      ! Use Current CCT as Start Model
      if (.not.allocated(dcct)) then
        call map_message(seve%e,task,'No starting Clean Component table')
        error = .true.
        return
      endif
    endif
    !
    ns = ubound(dcct,2)
    !
    ! Must match the number of channels, or be one...
    ! This may be incompatible with the FIRST and LAST stuff...
    if ((ns.ne.nc).and.(ns.ne.1)) then
      call map_message(seve%e,task,'Number of channels do not match ')
      error = .true.
      return
    endif
    !
    ! Here, save the available CCT as Start Model.
    restart = .true.
    write(mess,'(A,I0,A)') 'Re-using ',ns,' channels from previous Clean Component Table'
    call map_message(seve%i,task,mess,3)
  else 
    restart = .false.
  endif
  !
  if (a_method%method.eq.'MULTI') then
    x_iter = a_method%ninflate*x_iter
    x_iter = min(x_iter,nsizes(1)*nsizes(2))
    !!print *,'CCT-Prepare, Method MULTI ',a_method%m_iter,x_iter
  endif
  !print *,'CCT-Prepare, FIRST ',a_method%first,' LAST ',a_method%last
  call sic_delvariable('CCT',.false.,error)
  error = .false.  ! Not an error if no such variable
  !
  x_iter = max(1,x_iter)
  if (.not.restart) then
    call reallocate_cct(3,nc,x_iter,dcct,error)
    if (error)  return
    ! Nullify the channels to be used: a 0 flux component indicates 
    ! the last valid one.
    dcct(:,a_method%first:a_method%last,1) = 0.  
    call map_message(seve%i,task,'CCT nullified')
  else 
    if (x_iter.lt.hcct%gil%dim(3)) then
      write(mess,'(A,I0,A,I0,A)') 'More components in start model (',hcct%gil%dim(3), &
        & ' than allowed total (',x_iter,')'
      call map_message(seve%w,task,mess)
      x_iter = x_iter+hcct%gil%dim(3)
    endif
    if (ns.ne.nc) then
      allocate(tcct(3,1,hcct%gil%dim(3)),stat=ier)
      tcct(:,:,:) = dcct
      call reallocate_cct(3,nc,x_iter,dcct,error)
      do i=1,nc
        dcct(:,i,1:hcct%gil%dim(3)) = tcct(:,1,:)
      enddo
      !
      ! Nullify the next Clean Component
      if (x_iter.gt.hcct%gil%dim(3)) then
        dcct(:,:,hcct%gil%dim(3)+1:x_iter) = 0
      endif
    else
      ! Reallocate_cct does the job properly
      call reallocate_cct(3,nc,x_iter,dcct,error)
      if (error)  return
    endif
  endif
  !
  ! Define the image header
  call gildas_null(hcct)
  call gdf_copy_header (hdirty,hcct,error)
  hcct%gil%ndim = 3
  hcct%char%unit = 'Jy'
  !
  hcct%gil%dim(1) = 3
  ! Keep the same axis description
  hcct%gil%xaxi = 1
  !
  hcct%gil%convert(:,2) = hdirty%gil%convert(:,3)
  hcct%gil%convert(:,3) = hdirty%gil%convert(:,2)
  hcct%gil%dim(2) = hdirty%gil%dim(3)
  hcct%char%code(2) = hdirty%char%code(3)
  hcct%gil%faxi = 2
  hcct%gil%dim(3) = x_iter
  hcct%char%code(3) = 'COMPONENT'
  hcct%gil%yaxi = 3
  hcct%loca%size = 3*hcct%gil%dim(2)*hcct%gil%dim(3)
  !
  ! Initialize BLCs...
  hcct%blc = 0
  hcct%trc = 0
  !
contains
  subroutine reallocate_cct(n1,n2,n3,array,error)
    use gbl_message
    use imager_interfaces, only : map_message
    !-------------------------------------------------------------------
    ! (re)allocate the CCT allocatable array.
    ! If only the last dimension is enlarged, the previous contents are
    ! kept. Otherwise, the data is initialized to 0.
    !-------------------------------------------------------------------
    integer(kind=4),           intent(in)    :: n1,n2,n3      ! New dimensions
    real(kind=4), allocatable, intent(inout) :: array(:,:,:)  !
    logical,                   intent(inout) :: error         !
    ! Local
    character(len=message_length) :: mess
    logical :: keep
    integer(kind=4) :: ier,oldn1,oldn2,oldn3
    real(kind=4), allocatable :: tmp(:,:,:)
    !
    if (n1.le.0 .or. n2.le.0 .or. n3.le.0) then
      write(mess,'(A,3(I0,A))')  &
        'CCT size can not be zero nor negative (got n1 x n2 x n3 = ',  &
        n1,'x',n2,'x',n3,')'
      call map_message(seve%e,task,mess)
      error = .true.
      return
    endif
    !
    keep = .false.
    if (allocated(array)) then
      oldn1 = ubound(array,1)
      oldn2 = ubound(array,2)
      oldn3 = ubound(array,3)
      if (oldn1.eq.n1 .and.  &  ! Strict equality
          oldn2.eq.n2 .and.  &  ! Strict equality
          oldn3.ge.n3) then     ! Greater or equal
        write(mess,'(a,i0,a,i0,a,i0)')  &
          'Re-using current CCT table of size ',oldn1,' x ',oldn2,' x ',oldn3
        call map_message(seve%i,task,mess)
        return
      endif
      !
      write(mess,'(a,i0,a,i0,a,i0,a)')  &
        'Current CCT table has size ',oldn1,' x ',oldn2,' x ',oldn3,', increasing'
      call map_message(seve%i,task,mess)
      if (oldn1.eq.n1 .and.  &  ! Strict equality
          oldn2.eq.n2) then     ! Strict equality
        ! The 2 first dimensions are not modified, only the third is enlarged.
        ! Data will be kept.
        keep = .true.
        call move_alloc(from=array,to=tmp)
        ! 'array' is now deallocated
      else
        deallocate(array)
      endif
    endif
    !
    ! Allocation
    allocate(array(n1,n2,n3),stat=ier)
    if (failed_allocate(task,'ARRAY',ier,error))  return
    write(mess,'(a,i0,a,i0,a,i0)')  &
      'Allocated new CCT table of size ',n1,' x ',n2,' x ',n3
    call map_message(seve%i,task,mess)
    !
    ! Back copy
    if (keep) then
      array(:,:,1:oldn3) = tmp(:,:,1:oldn3)
      array(:,:,oldn3+1:n3) = 0.
      deallocate(tmp)
      call map_message(seve%i,task,'Previous CCT copied back')
    else
      array(:,:,1) = 0.
      call map_message(seve%i,task,'CCT Table initialized')
    endif
    !
  end subroutine reallocate_cct
  !
end subroutine cct_prepare
!
subroutine get_i4list_fromsic(rname,line,opt,nf,list,error)
  use iso_c_binding
  use gkernel_interfaces
  use gkernel_types
  use gbl_message
  use gbl_format
  use imager_interfaces, only : map_message
  !
  ! @ public-mandatory
  !
  ! Return in the allocatable array the content of the 1-D
  ! SIC variable given as argument of Option # opt.
  ! Return the size of the array in NF
  ! If NF is non zero on entry, the size must match and the
  ! array is pre-allocated.
  !
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: line
  integer, intent(in) :: opt
  integer, intent(inout) :: nf
  integer, allocatable, intent(inout) :: list(:)
  logical, intent(out) :: error
  !
  character(len=64) :: listname
  logical :: found
  type(sic_descriptor_t) :: desc
  integer :: na, ier
  type(c_ptr) :: cptr
  integer(4), pointer :: i1ptr(:)
  !
  call sic_ch(line,opt,1,listname,na,.true.,error)
  if (error) return
  call sic_descriptor(listname,desc,found)
  if (.not.found) then
    call sic_i4(line,opt,1,na,.true.,error)
    if (error) then
      call map_message(seve%e,rname,'Variable '//trim(listname)//' does not exists.')
      return 
    endif
    if (nf.eq.0) then
      nf = 1
      allocate(list(nf),stat=ier)
    else if (.not.allocated(list)) then
      call map_message(seve%e,rname,'List is not allocated')
      error = .true.
    endif
    if (error) return
    list(1:nf) = na
    return
  else if (desc%type.ne.fmt_i4) then
    call map_message(seve%e,rname,'Variable '//trim(listname)//' must be Integer ')
    error = .true.
    return
  endif
  if (desc%ndim.ne.1) then
    call map_message(seve%e,rname,'Variable '//trim(listname)//' must have rank 1')
    error = .true.
    return
  endif
  !
  if (nf.eq.0) then
    nf = desc%dims(1)
    allocate(list(nf),stat=ier)
  else if (nf.ne.desc%dims(1)) then
    call map_message(seve%e,rname,'Number of elements mismatch in List')
    error = .true.
  else if (.not.allocated(list)) then
    call map_message(seve%e,rname,'List is not allocated')
    error = .true.
  endif
  if (error) return
  !
  call adtoad(desc%addr,cptr,1)
  call c_f_pointer(cptr,i1ptr,[nf])
  list(1:nf) = i1ptr
end subroutine get_i4list_fromsic
!
subroutine get_r4list_fromsic(rname,line,opt,nf,list,error)
  use iso_c_binding
  use gkernel_interfaces
  use gkernel_types
  use gbl_message
  use gbl_format
  use imager_interfaces, only : map_message
  !
  ! @ public-mandatory
  !
  ! Return in the allocatable array the content of the 1-D
  ! SIC variable given as argument of Option # opt.
  ! Return the size of the array in NF
  ! If NF is non zero on entry, the size must match and the
  ! array must be pre-allocated.
  !
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: line
  integer, intent(in) :: opt
  integer, intent(inout) :: nf
  real, allocatable, intent(inout) :: list(:)
  logical, intent(out) :: error
  !
  character(len=64) :: listname
  logical :: found
  type(sic_descriptor_t) :: desc
  integer :: na, ier
  real(4) :: re
  type(c_ptr) :: cptr
  real(4), pointer :: r1ptr(:)
  !
  call sic_ch(line,opt,1,listname,na,.true.,error)
  if (error) return
  call sic_descriptor(listname,desc,found)
  if (.not.found) then
    call sic_r4(line,opt,1,re,.true.,error)
    if (error) then
      call map_message(seve%e,rname,'Variable '//trim(listname)//' does not exists.')
      return 
    endif
    if (nf.eq.0) then
      nf = 1
      allocate(list(nf),stat=ier)
    else if (.not.allocated(list)) then
      call map_message(seve%e,rname,'List is not allocated')
      error = .true.
    endif
    if (error) return
    list(1:nf) = re
    return
  else if (desc%type.ne.fmt_r4) then
    call map_message(seve%e,rname,'Variable '//trim(listname)//' must be Real ')
    error = .true.
    return
  endif
  if (desc%ndim.ne.1) then
    call map_message(seve%e,rname,'Variable '//trim(listname)//' must have rank 1')
    error = .true.
    return
  endif
  if (nf.eq.0) then
    nf = desc%dims(1)
    allocate(list(nf),stat=ier)
  else if (nf.ne.desc%dims(1)) then
    call map_message(seve%e,rname,'Number of elements mismatch in List')
    error = .true.
  else if (.not.allocated(list)) then
    call map_message(seve%e,rname,'List is not allocated')
    error = .true.
  endif
  if (error) return
  !
  call adtoad(desc%addr,cptr,1)
  call c_f_pointer(cptr,r1ptr,[nf])
  list(1:nf) = r1ptr
end subroutine get_r4list_fromsic
!
subroutine cct_remove_start(head,iplane,resid,tfbeam,dcct,tcc, &
  & nfields,primary,weight,wtrun,next_iter,start_flux)
  use image_def
  use clean_def
  !----------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER
  !   Subtract a Starting List of Clean Components
  !----------------------------------------------------------
  type(gildas), intent(in) :: head        ! Imager Header
  integer, intent(in) :: iplane           ! Current plane number
  real, intent(inout) :: resid(:,:)       !
  real, intent(in) :: dcct(:,:,:)         ! Clean Component List (from READ CCT)
  real, intent(in) :: tfbeam(:,:,:)       ! Dirty Beam FT (Gridded UV coverage)
  type(cct_par), intent(inout) :: tcc(:)  ! Clean Component Table
  integer, intent(in) :: nfields          ! Number of fields
  real, intent(in) :: primary(:,:,:)      ! Primary beams
  real, intent(in) :: weight(:,:)         ! Mosaic weights
  real, intent(in) :: wtrun               ! Mosaic primary beam truncation
  integer, intent(out) :: next_iter       ! Next available CC
  real, intent(out) :: start_flux
  !
  integer :: isc  ! Component number
  integer :: isx, isy ! Pixel of Clean Component
  integer :: ksp  ! Plane number of Clean Component
  integer :: nx, ny   ! Image size
  integer :: np   ! Number of channels in Clean Components
  integer :: nc   ! Number of Clean Components
  integer :: ier
  integer :: i,j,k,ip ! For mosaics
  integer :: nn(2), ndim
  complex, allocatable :: fcomp(:,:)
  real, allocatable :: clean(:,:)
  real, allocatable :: wfft(:)
  !
  nx = head%gil%dim(1) 
  ny = head%gil%dim(2) 
  np = ubound(dcct,2)   ! Number of planes in CCT
  nc = ubound(dcct,3)   ! Maximum number of Clean Components in CCT
  if (nfields.le.1) then
    allocate(fcomp(nx,ny),wfft(2*max(nx,ny)),stat=ier)
  else
    allocate(fcomp(nx,ny),clean(nx,ny),wfft(2*max(nx,ny)),stat=ier)
  endif
  !
  next_iter = nc+1 
  !
  start_flux = 0
  do isc=1,nc
    ksp = min(np, iplane)  ! Plane number
    isx = nint( (dcct(1,ksp,isc)-head%gil%val(1)) / head%gil%inc(1) + head%gil%ref(1) )
    isy = nint( (dcct(2,ksp,isc)-head%gil%val(2)) / head%gil%inc(2) + head%gil%ref(2) )
    tcc(isc)%value = dcct(3,ksp,isc)
    tcc(isc)%type = 0
    !! if (isc.gt.nc-2) Print *,nc,'ISC ',isc,isx,isy,tcc(isc)%value, tcc(isc)%type
    if (tcc(isc)%value.eq.0) then
      next_iter = isc
      exit
    endif
    tcc(isc)%ix = isx
    tcc(isc)%iy = isy
    tcc(isc)%type = 0
    start_flux = start_flux+tcc(isc)%value
    fcomp(isx,isy) = fcomp(isx,isy) + cmplx(tcc(isc)%value,0.0)
  enddo
  tcc(next_iter)%ix = 0
  tcc(next_iter)%iy = 0
  tcc(next_iter)%value = 0
  tcc(next_iter)%type = 0
  !
  ndim = 2
  nn(1) = nx
  nn(2) = ny
  if (nfields.le.1) then
    fcomp = 0.0   ! Initialize
    do isc=1,next_iter-1
       fcomp(tcc(isc)%ix,tcc(isc)%iy) = fcomp(tcc(isc)%ix,tcc(isc)%iy) + cmplx(tcc(isc)%value,0.0)
    enddo    
    ! 
    ! Remove this by Convolution with Dirty Beam
    call fourt(fcomp,nn,ndim,-1,0,wfft)
    fcomp(:,:) = fcomp*tfbeam(:,:,1)
    call fourt(fcomp,nn,ndim,1,1,wfft)
    resid = resid-real(fcomp)
  else
    ! Optimized by using CLEAN to store sum of components before multiplying
    ! by the weights to subtract from the Residuals
    clean = 0.0
    do ip=1,nfields
      fcomp = 0.0
      do k=1,next_iter-1
        fcomp(tcc(k)%ix,tcc(k)%iy) = tcc(k)%value   &
     &          * primary(ip,tcc(k)%ix,tcc(k)%iy)
      enddo
      call fourt(fcomp,nn,ndim,-1,0,wfft)
      fcomp(:,:) = fcomp*tfbeam(:,:,ip)
      call fourt(fcomp,nn,ndim,1,1,wfft)
      do j=1,ny
        do i=1,nx
          if (primary(ip,i,j).gt.wtrun) then
            clean(i,j) = clean(i,j) + real(fcomp(i,j))   &
     &              *primary(ip,i,j)
          endif
        enddo
      enddo
    enddo
    resid = resid - clean*weight
  endif
  !
end subroutine cct_remove_start
    

