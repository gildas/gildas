subroutine doqfft (np,nv,visi,jx,jy,jw,   &
     &    ,nx,ny,map,mapx,mapy)
  !----------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Support for UV_STAT
  !   Compute FFT of beam by gridding UV data for Natural
  !   weighting only. Uses simple in-cell gridding
  !----------------------------------------------------------------------
  ! Call
  integer, intent(in) :: nv                   ! number of values
  integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
  integer, intent(in) :: nx,ny                ! map size
  integer, intent(in) :: jx,jy,jw             ! X coord, Y coord & Weight location in VISI
  real, intent(in) :: visi(np,nv)             ! values
  complex, intent(out) :: map(nx,ny)          ! gridded visibilities
  real, intent(in) :: mapx(nx),mapy(ny)       ! Coordinates of grid
  !
  ! Local
  integer :: ix,iy,iv,my,ky,kx
  real :: u,v
  real(8) :: xinc,xref,yinc,yref
  !
  ! Initialize
  map = 0.0
  xinc = mapx(2)-mapx(1)
  xref = nx/2+1
  yinc = mapy(2)-mapy(1)
  yref = ny/2+1
  my = ny/2+1
  !
  ! Start with loop on observed visibilities
  do iv=1,nv
    u = visi(jx,iv)
    v = visi(jy,iv)
    ! Make sure V is < 0
    if (v.gt.0) then
      u = -u
      v = -v
    endif
    !
    ! Define map cell
    ix = nint(u/xinc+xref)
    iy = nint(v/yinc+yref)
    map (ix,iy) = map (ix,iy) + visi(jw,iv) 
    !
    iy = nint(-v/yinc+yref)
    if (iy.eq.my) then
      ix = nint(-u/xinc+xref)
      map (ix,iy) = map (ix,iy) + visi(jw,iv) 
    endif
  enddo
  !
  ! Apply symmetry
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      map(ix,iy) = map(kx,ky)
    enddo
  enddo
end subroutine doqfft
