subroutine do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec,x_0,  &
  parang,error)
  use gbl_message
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ public
  !  Reduce coordinates of a fixed object
  !---------------------------------------------------------------------
  character(len=2), intent(in)    :: coord    ! Coordinate system
  real(kind=4),     intent(in)    :: equinox  ! Equinox (ignored if not EQ)
  real(kind=8),     intent(in)    :: lambda   ! Longitude like coordinate
  real(kind=8),     intent(in)    :: beta     !
  real(kind=8),     intent(out)   :: s_2(2)   !
  real(kind=8),     intent(out)   :: s_3(3)   ! ra, dec & distance of Sun at current epoch
  real(kind=8),     intent(out)   :: dop      !
  real(kind=8),     intent(out)   :: lsr      !
  real(kind=8),     intent(out)   :: svec(3)  !
  real(kind=8),     intent(out)   :: x_0(3)   !
  real(kind=8),     intent(out)   :: parang   ! Parallactic angle
  logical,          intent(inout) :: error    !
  ! Global
  real(kind=8) :: sun_distance
  ! Local
  real(kind=8) :: x_2(3), v_2(3), x_3(3), x_1(3)
  real(kind=8) :: degrad, psi, the, phi, jfix, epsm, mat1(9), mat2(9), mat3(9)
  real(kind=8) :: angles(6), trfm_03(9), trfm_01(9), oblimo, stv, y_2(3), n_3(3)
  real(kind=8) :: n_2(3), den
  integer(kind=4) :: i
  parameter (degrad = pi /180.d0, stv = 2d0* pi / 86400d0*366.25d0 / 365.25d0)
  real(kind=8) :: apx1, apx2, apx3
  parameter (apx1 = 0.289977970217382d0, apx2 = -11.9099497383444d0,  &
             apx3 = 16.0645264529100d0)
  !
  !      SAVE
  !------------------------------------------------------------------------
  ! Code :
  if (coord.eq.'HO') then           ! horizontal coordinates
    s_2(1) = lambda * 180.d0 /pi
    s_2(2) = beta * 180.d0 /pi
    call rect(s_2,x_2)              ! To prepare the SUN_DISTANCE
    s_3(3) = sun_distance(x_2)      ! This is the sun distance
    return                          ! Nothing else there
    !
    !  Case : present day "true" coordinates, no transformation matrix, (R1) = (R3)
    !  considered as fixed coordinates in fixed frame, but formally maps
    !  are meaningless
  elseif (coord.eq.'DA') then
    s_3(1) = lambda
    s_3(2) = beta
    call rect (s_3, x_3)
    call matvec (x_3, trfm_23, x_2) ! needed to compute ...
    call transp (trfm_30, trfm_03)  ! The LSR correction.
    call matvec (x_3, trfm_03, x_0) ! needed for Horizon command
    !
    !  Case : galactic II coordinates, transformation (R1) to (R0)
  elseif (coord.eq.'GA') then       ! galactic coordinates
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  transformation matrix from galactic II to ecliptic (J2000.0)
    !  ### Euler angles could be computed once for all ...
    !  Computation in 3 steps
    psi = 33.d0 * degrad            ! well known Euler angles
    the = -62.6d0 * degrad          ! for transformation
    phi = -282.25d0 * degrad        ! matrix from
    call eulmat (psi,the,phi,mat1)  ! gal. (II) to equ. (1950.0)
    jfix=j2000+(1950.d0-2000.d0)*365.25d0  ! fixed equinox (julian days)
    epsm=oblimo (jfix)              ! mean obliquity at fixed equinox
    psi=0.d0                        ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat2)  ! equ. (1950.0) to ecl. (1950.0)
    call mulmat (mat1,mat2,mat3)    ! product MAT3 = MAT2 x MAT1
    !                               ! gal. (II) to ecl. (1950.0)
    call qprec (jfix,j2000,angles)  ! compute precession angles
    psi=angles(5)                   ! Euler angles
    the=angles(4)                   ! of transformation
    phi=-angles(6)-angles(5)        ! matrix from
    call eulmat (psi,the,phi,mat1)  ! ecl. (1950.0) to ecl. (2000.0)
    call mulmat (mat3,mat1,trfm_01) ! product TRFM01 = MAT1 x MAT3
    !                               ! gal. (II) to ecl. (1950.0)
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3) ! needed for Horizon command
    call spher (x_3, s_3)
    !
    ! Case: mean equatorial coordinates at fixed equinox, transformation (R1) to (R0)
  elseif (coord.eq.'EQ') then
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  Transformation matrix from equatorial (JFIX) to ecliptic (J2000.0)
    jfix=j2000+(equinox-2000.0d0)*365.25d0   ! fixed equinox (julian days)
    epsm=oblimo (jfix)              ! mean obliquity at fixed equinox
    psi=0.d0                        ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat1)  ! compute matrix from (R1) to ecl (JFIX)
    call qprec (jfix,j2000,angles)  ! compute precession angles
    psi=angles(5)                   ! Euler angles of transformation
    the=angles(4)
    phi=-angles(6)-angles(5)
    call eulmat (psi,the,phi,mat2)  ! compute matrix from ecl (JFIX) to (R0)
    call mulmat (mat1,mat2,trfm_01) ! product TRFM01 = MAT2 x MAT1
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3) ! needed for Horizon command
    call spher (x_3, s_3)
  else
    call astro_message(seve%e,'OBJECT','Unsupported coordinates')
    return
  endif
  call matvec (vg_0, trfm_20, v_2)
  !
  ! Check Sun Distance
  s_3(3) = sun_distance(x_2)        ! This is the sun distance
  !
  ! Doppler correction
  v_2(2) = v_2(2) - cos(pi*(lonlat(2))/180.0d0) * stv * (radius + altitude)
  dop = (v_2(1) * x_2(1) + v_2(2) * x_2(2) + v_2(3) * x_2(3))
  lsr = - (apx1 * x_0(1) + apx2 * x_0(2) + apx3 * x_0(3))
  !
  ! Aberration
  do i = 1, 3
    x_2(i) = x_2(i) - v_2(i) / light
  enddo
  call spher(x_2, s_2)
  s_2(1) = - s_2(1) * 180.d0/pi
  s_2(2) = s_2(2) * 180.d0/pi
  !
  ! XYZ
  call matvec (x_3, trfm_43, svec)
  !
  ! Compute parallactic angle
  ! parallactic angle
  ! x_3 is the source vector in eq coordinates (3)
  ! n_3 is the normal to the source meridian plane
  ! TRFM_23 goes from (3) to (2)
  ! n_2 is the normal to the source meridian plane, in horiz coords.(2)
  ! x_2 is the source vector in in horiz coords.
  ! y_2 is the intersection of the vertical plane of the source and
  ! the plane of the sky
  den = sqrt(x_3(1)**2+x_3(2)**2)
  n_3(1) = -x_3(2)/den
  n_3(2) = x_3(1)/den
  n_3(3) = 0
  call matvec (n_3, trfm_23, n_2)
  den = sqrt(x_2(1)**2+x_2(2)**2)
  y_2(1) = x_2(1)*x_2(3) / den
  y_2(2) = x_2(2)*x_2(3) / den
  y_2(3) = -den
  parang = -(pi/2-acos(y_2(1)*n_2(1)+y_2(2)*n_2(2)+y_2(3)*n_2(3)))
  return
end subroutine do_object
!
function sun_distance (x_2)
  use ast_constant
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !	Returns angular distance from SUn
  !	X_2: apparent horizontal coordinates of the source
  !	Xsun_2: apparent horizontal coordinates of the sun
  ! The angle between the 2 directions is derived from the scalar product 
  ! of the vectors in the horizontal coordinate frame:
  ! Vobj.Vsun=(XOXSun+YOYSun+ZOZSun)=||Vobj||*||Vsun||*cos(SunObserverTarget)
  !---------------------------------------------------------------------
  real(kind=8) :: sun_distance      !
  real(kind=8) :: x_2(3)            !
  sun_distance = 180.d0/pi*acos( (x_2(1)*xsun_2(1)+x_2(2)*xsun_2(2)+  &
    x_2(3)*xsun_2(3)) /sqrt(x_2(1)**2+x_2(2)**2+x_2(3)**2) /          &
    sqrt(xsun_2(1)**2+xsun_2(2)**2+xsun_2(3)**2) )
end function sun_distance
