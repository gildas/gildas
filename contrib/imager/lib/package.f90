!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the IMAGER package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine imager_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !
  type(gpack_info_t), intent(out) :: pack
  !
  external :: imager_pack_init
  external :: imager_pack_on_exit
  external :: greglib_pack_set    ! If not declared in gkernel_interfaces
  !
  pack%name='imager'
  pack%ext = '.ima'
  pack%depend(1:1) = (/ locwrd(greglib_pack_set) /)
  pack%init=locwrd(imager_pack_init)
  pack%on_exit=locwrd(imager_pack_on_exit)
  pack%authors="S.Guilloteau"
  !
end subroutine imager_pack_set
!
subroutine imager_pack_init(gpack_id,error)
  use gkernel_interfaces
  use sic_def
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  ! Local
  integer(kind=4) :: ier
  character(len=filename_length) :: root, filename
  logical :: local
  !
  ! Local language
  call init_clean
  !
  ! One time initialization
  call map_message_set_id(gpack_id)  ! Set library id
  !
  ! Procedure extensions
  call exec_program('SIC'//backslash//'SIC EXTENSION .ima .greg ')
  ! Language priorities
  call exec_program('SIC'//backslash//'SIC PRIORITY 1 ADVANCED BUNDLES CLEAN DISPLAY')
  !
  ! Specific initializations
  !
  ! IMAGER ad-hoc patch: override symbol from kernel/etc/gag.dico.gbl.src
  ! which sets 'gag_help_clean' pointing to Mapping one.
  ier = sic_setlog('gag_help_clean','gag_doc:hlp/imager-help-clean.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  ier = sic_setlog('gag_help_display','gag_doc:hlp/imager-help-display.hlp')
  ier = sic_setlog('gag_help_advanced','gag_doc:hlp/imager-help-advanced.hlp')
  ier = sic_setlog('gag_help_calibrate','gag_doc:hlp/imager-help-calibrate.hlp')
  ier = sic_setlog('gag_help_bundles','gag_doc:hlp/imager-help-bundles.hlp')
  ier = sic_setlog('gag_help_imager','gag_doc:hlp/imager-help-imager.hlp')
  !
  root = 'gag_doc:html/imager-html/'
  call sic_parse_file (root,' ',' ',filename)
  inquire(file=filename,exist=local)
  !
  if (local) then
    ! Use Local HTML file
    ier = sic_setlog('gag_html_clean:','gag_doc:html/imager-html/')
    ier = sic_setlog('gag_html_display:','gag_doc:html/imager-html/')
    ier = sic_setlog('gag_html_advanced:','gag_doc:html/imager-html/')
    ier = sic_setlog('gag_html_calibrate:','gag_doc:html/imager-html/')
    ier = sic_setlog('gag_html_bundles:','gag_doc:html/imager-html/')
    ier = sic_setlog('gag_html_imager:','gag_doc:html/imager-html/')
  else
    ! Try to access remote HTML help at OASU
    ier = sic_setlog('gag_html_clean:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
    ier = sic_setlog('gag_html_display:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
    ier = sic_setlog('gag_html_advanced:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
    ier = sic_setlog('gag_html_calibrate:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
    ier = sic_setlog('gag_html_bundles:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
    ier = sic_setlog('gag_html_imager:','https://imager.oasu.u-bordeaux.fr/wp-content/uploads/doc/html/')
  endif
  !
  call imager_version
end subroutine imager_pack_init
!
subroutine imager_pack_on_exit(error)
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  logical :: error
  !
  call save_result(error)
  !
end subroutine imager_pack_on_exit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! The following routines are to use GreG Library WITHOUT the GREG3\ language
! or at least with this language in Library-Only mode.
!   They should be in the GreG package, with their proper interfaces.
! However, it seems to work properly here provided the "external" 
! declarations with the proper types.
!
subroutine greglib_pack_set(pack)
  !use greg_dependencies_interfaces
  !use greg_interfaces, except_this=>greglib_pack_set
  use gpack_def
  !----------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  ! Public package definition routine.
  ! It defines package methods and dependencies to other packages.
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack ! Package info structure
  !
  external :: sic_pack_set,greglib_pack_init,greg_pack_clean ! if no "use"
  integer(8), external :: locwrd    ! if no "use"
  !
  !! Print *,'Using Local greglib_pack_set '
  pack%name='greg'
  pack%ext='.greg'
  pack%depend(1:1) = (/ locwrd(sic_pack_set) /)
  pack%init=locwrd(greglib_pack_init)
  pack%clean=locwrd(greg_pack_clean)
  pack%authors="J.Pety, S.Bardeau, S.Guilloteau, E.Reynier"
  !
end subroutine greglib_pack_set
!
subroutine greglib_pack_init(gpack_id,error)
  !use greg_dependencies_interfaces
  !use greg_interfaces, except_this=>greglib_pack_init
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Private routine to set the GREG environment.
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  logical, external :: gmaster_hide_gui ! if no "use"
  logical, external :: gr_error         ! ff no "use"
  !
  ! Set libraries id's
  call gchar_message_set_id(gpack_id)
  call gcont_message_set_id(gpack_id)
  call gtv_message_set_id(gpack_id)
  call gtv_c_message_set_id(gpack_id)
  call greg_message_set_id(gpack_id)
  call gcore_c_message_set_id(gpack_id)
#ifdef GAG_USE_GTK
  call gui_c_message_set_id(gpack_id)
#endif
  !
  ! Local language
  call load_greg('INTERACTIVE')
  ! Do not load GREG3!  call load_greg('LIBRARY GREG3')
  !
  if (.not.gmaster_hide_gui()) then
     call gr_exec('DEVICE IMAGE WHITE')
     error = gr_error()
     if (error) return
  endif
  !
end subroutine greglib_pack_init
