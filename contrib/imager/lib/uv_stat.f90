!
subroutine map_parameters(task,map,hiuv,freq,uvmax,uvmin,error,print)
  use gkernel_interfaces
  use imager_interfaces, only : map_message, telescope_beam
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !----------------------------------------------------------------------
  !   @ private-mandatory
  !
  ! IMAGER
  !   Prepare the MAP parameters for UV_MAP, MX or UV_RESTORE
  ! Reads these from SIC variables if present, or guess them from UV data
  !-----------------------------------------------------------------------
  character(len=*), intent(in)    :: task ! Input task name (UV_MAP or MX)
  type (uvmap_par), intent(inout) :: map  ! Map parameters
  type (gildas), intent(in) :: hiuv       ! UV header
  real(8), intent(inout) :: freq          ! Observing frequency
  real(4), intent(in) :: uvmax, uvmin     ! Min & Max UV in m
  logical, intent(inout) :: error
  logical, optional :: print
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: sec_in_rad = pi/3600d0/180d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  real(4), parameter :: pixel_per_beam=5.0
  real(4) :: map_range(2)  ! Range of offsets
  character(len=96) :: chain
  character(len=12) :: prefix
  real :: uvmi, uvma, xp, xm, hppb, bsize
  integer :: i, np
  type (uvmap_par), save :: last_map
  logical :: do_print
  integer :: osize(2)
  !
  character(len=32) :: rname
  integer(kind=8) :: ram_uv, ram_map, ram_need
  !
  do_print = present(print)
  !
  ! For UV_STAT, reset all parameters to Zero
  rname = task
  if (task.eq.'UV_STAT') then
    map%size = 0
    map%field = 0
    map%xycell = 0
  else if (task.eq.'UV_SETUP') then
    rname = 'UV_STAT'
  endif
  !
  freq = gdf_uv_frequency(hiuv)
  if (do_print) then                                            
    write(chain,'(A,F12.6,A)') "Observed rest frequency       ", &
      & freq*1d-3," GHz"
    call map_message(seve%i,rname,chain)
  endif
  !
  uvma = uvmax/(freq*f_to_k)
  uvmi = uvmin/(freq*f_to_k)
  if (do_print) then
    write(chain,100) hiuv%gil%nvisi,hiuv%gil%nchan
    call map_message(seve%i,rname,chain)
    write(chain,101) uvmi,uvma,' meters'
    call map_message(seve%i,rname,chain)
    write(chain,101) uvmin*1e-3/2.0/pi,uvmax*1e-3/2.0/pi,' kiloWavelength'
    call map_message(seve%i,rname,chain)
  endif
  !
  !
  ! Get beam size (in radians)
  bsize = telescope_beam(rname,hiuv)
  !
  if (bsize.eq.0) then
    !
    ! Default to IRAM Plateau de Bure case
    hppb = 63.0*(12.0/15.0)*(100.e3/freq) ! [arcsec] Primary beam size
    ! 1.22 Lambda/D for NOEMA 15 m antennas
    write(chain,'(A,F12.1,A)') "Half power primary beam defaulted to ", &
      & hppb," arcsec (15-m diameter antennas)"
    call map_message(seve%w,rname,chain)                         
    bsize = hppb*sec_in_rad
  else 
    if (hiuv%gil%nteles.gt.0) then
      recmap_uvcell = hiuv%gil%teles(1)%diam/2.0       
    else
      ! Derive first order UV cell size
      recmap_uvcell = 1.22*3E8/(freq*1E6) / 2. / bsize
    endif
    if (do_print .or. map%size(1).eq.0) then                                           
      write(chain,'(A,F12.1,A)') "Half power primary beam       ", &
        & bsize/sec_in_rad," arcsec"
      call map_message(seve%i,rname,chain) 
    endif
    !
  endif
  hppb = bsize/sec_in_rad 
  !
  ! Only 2 out of the 3 MAP_FIELD, MAP_SIZE and MAP_CELL
  !   can be non-zero. This should be tested in some way
  !
  if (map%xycell(1)*map%size(1)*map%field(1).ne.0) then
    if (abs(map%field(1)-map%size(1)*map%xycell(1)).gt.0.01*map%field(1)) then
      call map_message(seve%w,rname,'User specified MAP_FIELD, MAP_SIZE and MAP_CELL do not match')
    else
      call map_message(seve%w,rname,'MAP_FIELD, MAP_SIZE and MAP_CELL specified by user')    
    endif
  endif
  !
  ! Now get MAP_CELL and MAP_SIZE if Zero.
  ! Default Map_Cell, in ", rounded to 0.02 or 0.001"
  recmap_xycell = 0.01*nint(360.0*3600.0*100.0/uvmax/pixel_per_beam)
  if (recmap_xycell(1).le.0.02) then
    recmap_xycell = 0.001*nint(360.0*3600.0*1000.0/uvmax/pixel_per_beam)
  endif
  if (map%xycell(1).eq.0) then
    if (map%size(1)*map%field(1).eq.0) then
      map%xycell = recmap_xycell
    else
      map%xycell(1:2) = map%field(1)/map%size(1)
    endif
  endif
  !
  ! Get a wide enough field of view (in arcsec)
  if (map%nfields.ne.0) then
    ! The factor 2.5 on the primary beam size is to go down 
    ! low enough by default
    recmap_field = 2.5*hppb
    ! Enlarge the field of view by the Offset range
    np = abs(map%nfields)
    xm = minval(map%offxy(1,1:np))
    xp = maxval(map%offxy(1,1:np))
    map_range(1) = xp-xm
    xm = minval(map%offxy(2,1:np))
    xp = maxval(map%offxy(2,1:np))
    map_range(2) = xp-xm
    map_range = map_range/sec_in_rad
    !
    recmap_field = recmap_field + map_range
  else
    ! The factor 2 on the primary beam size is to go down to 6.25 % level by default
    recmap_field = 2*hppb
    ! Map Field = 1.13*Lambda/Dmin - but round to 1.3
    if (uvmin.ne.0) then
      ! The Min is to avoid going to very large images when uvmin is close to 0
      ! e.g. when short or Zero spacings have been added
      recmap_field = min(recmap_field,2.6*360*3600/uvmin) ! In arcsec
    endif
  endif
  !
  ! Round recmap_field to nearest 0.1 arcsec
  recmap_field = 0.1d0*nint(10.d0*recmap_field)
  !
  ! Get MAP_SIZE if non Zero to define the actual Field of view
  if (map%size(1).ne.0) then
    if (map%size(2).eq.0) map%size(2) = map%size(1)
    if (map%field(1).eq.0) map%field = map%size * map%xycell
  endif
  if (map%field(1).ne.0) then
    if (map%field(2).eq.0) map%field(2) = map%field(1)
  else
    !
    map%field = recmap_field
  endif
  !
  ! Default MAP_SIZE
  recmap_size = recmap_field/recmap_xycell
  call gi4_round_forfft(recmap_size(1),osize(1),error,map_rounding,map_power)
  call gi4_round_forfft(recmap_size(2),osize(2),error,map_rounding,map_power)
  recmap_size = osize
  !
  if (map%size(1).eq.0) then
    ! At this stage, map%field and map%xycell are non zero. 
    map%size = map%field/map%xycell
    call gi4_round_forfft(map%size(1),osize(1),error,map_rounding,map_power)
    call gi4_round_forfft(map%size(2),osize(2),error,map_rounding,map_power)
    map%size = osize
    !
  else if ( (mod(map%size(1),2).ne.0) .or. (mod(map%size(2),2).ne.0) ) then
    call map_message(seve%e,rname,'Number of pixels (MAP_SIZE) must be even')
    error = .true.
  endif
  ! Round MAP%FIELD now
  map%field = 0.1*nint(10.0*map%size*map%xycell)
  recmap_field = 0.1*nint(recmap_field*10.0)
  !
  if (task.eq.'UV_STAT') then
    prefix = 'Recommended '
  else if (task.eq.'UV_SETUP') then
    prefix = 'Adopted '
  else
    if (error) return
    prefix = 'Current '
  endif
  write(chain,'(A,A,F12.1,a,F12.1,A)') prefix,'Field of view / Largest Scale ' &
    & ,map%field(1),' x ',map%field(2),' "'
  call map_message(seve%i,rname,chain)
  write(chain,'(A,A,F11.1,A,F11.1,A)') prefix,'Image size ', &
    & map%size(1)*map%xycell(1),' x ',map%size(2)*map%xycell(2),'" '
  call map_message(seve%i,rname,chain)
  !
  write(chain,'(A,A,I0,A,I0,A)') prefix,'Map size ', &
    & map%size(1),' x ',map%size(2)
  call map_message(seve%i,rname,chain)
  write(chain,'(A,A,F8.3,A,F8.3,A)') prefix,'Pixel size ', &
    & map%xycell(1),' x ',map%xycell(2),' "'
  call map_message(seve%i,rname,chain)
  !
  ! Do NOT adjust the Field while keeping the Cell size 
  !     map%field = map%size * map%xycell
  ! it is useless or incorrect...
  !
  if (rname.eq.'UV_STAT') then
    !
    ! Evaluate RAM pressure
    call sic_get_inte('SIC%RAMSIZE',sys_ramsize,error)
    ram_uv = (huv%gil%dim(1)*huv%gil%dim(2))/(512*512)   ! Size of RAM requested
    ram_map = (2*map%size(1)*map%size(2))/(512*512)*huv%gil%nchan
    ram_need = ram_map+ram_uv
    Print *,'UV size ',ram_uv, huv%gil%dim(1:2), huv%gil%dim(1)*huv%gil%dim(2)
    Print *,'Map size ',ram_map, huv%gil%nchan, map%size, map%size(1)*map%size(2)
    if (ram_need.gt.sys_ramsize) then
        write(chain,'(A,F8.1,A,F8.1,A,F8.1,A)') 'Data size (UV ',1d-3*ram_uv,& 
        & 'GB + Map ',1D-3*ram_map, &
        & 'GB) exceeds available RAM (',1d-3*sys_ramsize,' GB)'
      call map_message(seve%e,rname,chain,1)
      error = .true.
      return
    else if (ram_need.gt.sys_ramsize/3) then
      write(chain,'(A,F8.1,A,F8.1,A,F8.1,A)') 'Data size (UV ',1d-3*ram_uv,& 
        & 'GB + Map ',1D-3*ram_map, &
        & 'GB) exceeds 1/3rd of available RAM (',1d-3*sys_ramsize,' GB)'
      call map_message(seve%w,rname,chain,3)
    else
      write(chain,'(A,F8.1,A,F8.1,A,F8.1,A)') 'Data size (UV ',1d-3*ram_uv,& 
        & 'GB + Map ',1D-3*ram_map, &
        & 'GB)  for a RAM of (',1d-3*sys_ramsize,' GB)'
      call map_message(seve%i,rname,chain,3)
    endif
    !  
  else
    ! This part is only for UV_MAP / UV_MOSAIC, not for UV_STAT
    !
    ! Check whether Weights have to be recomputed
    if (map%uniform(1).ne.last_map%uniform(1)) do_weig = .true.
    if (map%uniform(2).ne.last_map%uniform(2)) do_weig = .true.
    if (map%ctype.ne.last_map%ctype) do_weig = .true.
    do i=1,4
      if (map%taper(i).ne.last_map%taper(i)) do_weig = .true.
    enddo
    last_map = map
  endif
  !
100 format('Found ',i12,' Visibilities, ',i4,' channels')
101 format('Baselines ',f9.1,' - ',f9.1,a)
end subroutine map_parameters
!
subroutine uv_stat_comm(line,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>uv_stat_comm
  use clean_def
  use clean_default
  use clean_arrays
  use gbl_message
  !------------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !    Support for command UV_STAT
  !       Analyse a UV data set to define approximate beam size,
  !       field of view, expected "best beam", etc...
  !------------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical, intent(out) :: error
  !
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  real(kind=8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  !
  integer, parameter :: mt=100
  integer datelist(mt)
  !
  type (uvmap_par), save :: amap    ! To get the New variable names
  type (uvmap_par), save :: oldmap  ! To save the previous MAP_...
  logical, save :: old_exist=.false.
  !
  integer ndates
  character(len=8) mode, argum
  real, allocatable :: beams(:,:), fft(:)
  real uv_taper(3),map_cell(2),uniform(2),uvmax,uvmin,uvm
  real start,step,taper,robust
  integer map_size(2),wcol,ctype,n,mcol(2),ier
  integer(kind=index_length) :: dim(4)
  logical sorted, shift
  real(kind=8) :: freq, new(3)
  real(4), save :: bmax, bmin
  logical, save :: first = .true.
  real, save :: result(10,8)
  logical :: print
  character(len=message_length) :: chain
  character(len=7) :: rname = 'UV_STAT'
  integer, parameter :: mmode=12
  character(len=8) :: smode(mmode)
  data smode /'ADVISE','ALL','BEAM','BRIGGS','CELL','DEFAULT','HEADER', &
    & 'RESET','ROBUST','SETUP','TAPER','WEIGHT'/
  !
  if (sic_present(0,1)) then
    call sic_ke(line,0,1,argum,n,.false.,error)
    if (error) return
    call sic_ambigs (rname,argum,mode,n,smode,mmode,error)
    if (error) return
  else
    mode = 'ALL'
  endif
  !
  freq = gdf_uv_frequency(huv)
  !
  select case (mode)
  case ('DEFAULT')
    default_map%size = 0
    default_map%xycell = 0.
    default_map%field = 0.
    default_map%uvcell = 0 
    return
  case ('CELL','TAPER','WEIGHT','BRIGGS','ROBUST')
    wcol = 0
    mcol = 0
    !
    start = 0.0
    call sic_r4(line,0,2,start,.false.,error)
    start = abs(start)
    step = 0.0
    call sic_r4(line,0,3,step,.false.,error)
    step = abs(step)
    !
    ctype = 1
    !
    ! Must do like UV_MAP now
    robust = default_map%uniform(2) ! Save state
    if ((mode.eq.'WEIGHT').or.(mode.eq.'ROBUST')) then
      default_map%uniform(2) = 1.0  ! Non zero to have the "Robust" message
    else if (mode.eq.'BRIGGS') then
      default_map%uniform(2) = -1.0 ! Non zero to have the "Robust" message
    endif
    call map_prepare(rname,huv,amap,error)
    default_map%uniform(2) = robust ! Restore state
    if (error) return
    !
    uniform(:) = amap%uniform(:)
    uv_taper(:) = amap%taper(1:3) 
    call sic_get_inte('WCOL',wcol,error)
    if (error) return
    !
    ! First sort the input UV Table, leaving UV Table in UV_*
    shift = .false.
    call uv_sort (huv,duv,error,sorted,shift,new,uvmax,uvmin,.false.)
    if (error) return
  case ('BEAM')
    wcol = 0
    mcol = 0
    call map_prepare(rname,huv,amap,error)
    uniform(:) = amap%uniform(:)
    uv_taper(:) = amap%taper(1:3) 
    call sic_get_inte('WCOL',wcol,error)
    if (error) return
    !
    ! First sort the input UV Table, leaving UV Table in UV_*
    shift = .false.
    call uv_sort (huv,duv,error,sorted,shift,new,uvmax,uvmin,.false.)
    if (error) return
  case ('ADVISE','ALL','HEADER','SETUP')
    ! For HEADERS, do the minimum work for speed...
    ! The UV table is available in HUV%
    if (huv%loca%size.eq.0) then
      call map_message(seve%e,rname,'No UV data loaded')
      error = .true.
      return
    endif
    call uvgmax (huv,duv,uvmax,uvmin)
    ! Now transform UVMAX in kiloWavelength (including 2 pi factor)
    uvmax = uvmax*freq*f_to_k
    uvmin = uvmin*freq*f_to_k
    error = .false.
  end select
  !
  ! New code: For the time being, this is somewhat inconsistent
  ! with the use of uv_sort which uses a slightly different frequency.
  ! It does not matter, since this routine provides hints, not
  ! exact values.
  !
  bmax = uvmax/(freq*f_to_k)
  bmin = uvmin/(freq*f_to_k)
  if (first) then
!    call sic_defstructure('UVSTAT',.true.,error)
!    call sic_def_real('UVSTAT%BMAX',bmax,0,dim,.false.,error)
!    call sic_def_real('UVSTAT%BMIN',bmin,0,dim,.false.,error)
    call sic_def_real('UV_BMAX',bmax,0,dim,.false.,error)
    call sic_def_real('UV_BMIN',bmin,0,dim,.false.,error)
    !
    call sic_defstructure('DEFMAP',.true.,error)
    call sic_def_inte('DEFMAP%SIZE',themap%size,1,2,.true.,error)
    call sic_def_real('DEFMAP%CELL',themap%xycell,1,2,.true.,error)
    call sic_def_real('DEFMAP%FIELD',themap%field,1,2,.true.,error)
    !
    first = .false.
  endif
  !
  select case (mode)
  case ('BEAM') 
    call map_beamsize(amap,huv,duv,bmin,bmax,error)
  case ('HEADER')
    call gdf_print_header(huv)
    call uv_listheader(huv,duv,mt,datelist,ndates,freq)
    call uv_printoffset('HEADER',themap)
  case ('ADVISE')
    call map_copy_par(default_map,themap)
    call map_parameters(rname,themap,huv,freq,uvmax,uvmin,error,print)
    error = .false.
  case ('ALL')
    call gdf_print_header(huv)
    call uv_listheader(huv,duv,mt,datelist,ndates,freq)
    call uv_printoffset('HEADER',themap)
    call map_copy_par(default_map,themap)
    call map_parameters(rname,themap,huv,freq,uvmax,uvmin,error,print)
  case ('RESET')
    if (old_exist) then
      call map_copy_par(oldmap,default_map) 
      old_exist = .false.
    endif
  case ('SETUP')
    call map_copy_par(default_map,themap)
    call map_parameters('UV_SETUP',themap,huv,freq,uvmax,uvmin,error,print)
    if (error) return
    call map_copy_par(default_map,oldmap)
    old_exist = .true.
    call map_copy_par(themap,default_map) ! Copy suggested values to Current
    error = .false.
  case default
    !
    ! back to usual code
    uvm = uvmax/(freq*f_to_k)
    !
    if (mode.eq.'TAPER') then
      if (step.eq.0.) step = sqrt(2.0)
      if (start.eq.0.) start = 10*nint(uvm/160.0)
      taper = 16*start
    else
      taper = sqrt(uv_taper(1)*uv_taper(2))
      if (taper.ne.0) then
        taper = min(taper,uvm)
      else
        taper = uvm
      endif
    endif
    !
    ! Define MAP_CELL and MAP_SIZE
    taper = uvm*freq*f_to_k
    map_cell(1) = 0.02*nint(180.0*3600.0*50.0/taper/4.0)
    if (map_cell(1).le.0.02) then
      map_cell(1) = 0.002*nint(180.0*3600.0*500.0/taper/4.0)
    endif
    map_cell(2) = map_cell(1)    ! In ", rounded to 0.01"
    if (mode.eq.'TAPER') then
      map_size(1) = 256
      map_size(2) = 256
    else
      map_size(1) = 64
      map_size(2) = 64
    endif
    !
    write(chain,102) uvm,uvmax*1e-3/2.0/pi
    call map_message(seve%i,rname,chain)
    write(chain,103) map_size
    call map_message(seve%i,rname,chain)
    write(chain,104) map_cell
    call map_message(seve%i,rname,chain)
    !
    ! Redefine some parameters
    map_cell(1) = map_cell(1)*pi/180.0/3600.0   ! In radians
    map_cell(2) = map_cell(2)*pi/180.0/3600.0
    !
    ! Process sorted UV Table according to the type of beam produced
    n = 2*max(map_size(1),map_size(2))
    allocate (beams(map_size(1),map_size(2)),fft(n),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation failure')
      error = .true.
      return
    endif
    call uniform_beam (' ',uv_taper,   &
       &    map_size,map_cell,uniform,wcol,mcol,fft,   &
       &    error,mode,beams,1,start,step,uvm,result,huv,duv)
    deallocate (beams,fft)
    !
    call sic_delvariable('BEAM_SHAPE',.false.,error)
    dim(1) = 10
    dim(2) =  8
    call sic_def_real('BEAM_SHAPE',result,2,dim,.true.,error)
    error = .false.
  end select
  !
  102   format('Maximum baseline is   ',f8.1,' m,  ',f8.1,' kWavelength')
  103   format('Map size is   ',i6,' by ',i6)
  104   format('Pixel size is ',f6.3,' by ',f6.3,'"')
end subroutine uv_stat_comm
!
subroutine uv_printoffset(rname,map)
  use clean_def
  use gbl_message
  use imager_interfaces, only : map_message
  !-----------------------------------------------------------------  
  ! @ private
  !
  !   IMAGER
  !     Print the list of Mosaic Offsets
  !-----------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(uvmap_par), intent(in) :: map
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  real(4), parameter :: rad_to_sec = 180.0*3600.0/pi
  integer :: n,j
  character(len=60) :: chain
  !
  if (map%nfields.eq.0) return
  !
  n = abs(map%nfields)
  if (map%nfields.lt.0) then
    write(chain,'(I5,A)') n, ' Phase offsets in mosaic'
  else
    write(chain,'(I5,A)') n, ' Pointing offsets in mosaic'
  endif
  call map_message(seve%i,rname,chain)
  do j=1,n-1,2
    write(*,'(2(A,F12.4,A,F12.4,A))') &
    &   '(',map%offxy(1,j)*rad_to_sec,' ,',map%offxy(2,j)*rad_to_sec,' )', &
    &   '(',map%offxy(1,j+1)*rad_to_sec,' ,',map%offxy(2,j+1)*rad_to_sec,' )'
  enddo
  if (mod(n,2).ne.0)     write(*,'(A,F12.2,A,F12.2,A)') &
    &   '(',map%offxy(1,n)*rad_to_sec,' ,',map%offxy(2,n)*rad_to_sec,' )'
end subroutine uv_printoffset
!
subroutine map_beamsize(map,huv,duv,uvmin,uvmax,error)
  use image_def
  use clean_def
  use clean_default
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => map_beamsize
  !---------------------------------------------------
  ! @ private
  !
  !  IMAGER
  !     Get the synthesized beam size according to
  !   current imaging parameters. Performs a quick 
  !   gridding on a representative channel.
  !---------------------------------------------------
  type(uvmap_par), intent(in) :: map  ! MAP parameters
  type(gildas), intent(in) :: huv   ! UV Header
  real, intent(in) :: duv(:,:)      ! UV data
  real, intent(in) :: uvmin         ! Minimum UV value
  real, intent(in) :: uvmax         ! Maximum UV value
  logical, intent(out) :: error     ! Error flag
  !
  character(len=*), parameter :: rname='MAP_BEAMSIZE'
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  !
  ! Customized values
  real(4) :: pixel_per_beam=4.0     ! A good compromise
  integer, parameter :: msize=128   ! Large enough for most cases
  integer, save :: osize=0
  integer :: nsize, isize
  real, allocatable, save :: abeam(:,:)
  real, allocatable :: work(:) !  work(2*nsize)
  ! FFT arrays
  complex, allocatable :: ipft(:), ipf(:) !nsize*nsize), ipf(nsize*nsize)
  integer :: lx,ly,lw, nu,nv, nx,ny, ix_patch, iy_patch
  integer :: ndim, nn(2)
  real :: uvcell, map_cell, thre, rtmp
  real(kind=8) :: convert(6)
  real :: major_axis, minor_axis, pos_angle 
  !
  real, allocatable :: ipw(:), ipv(:)
  integer :: ier,jc
  integer :: grid_code = 0
  logical :: do_weig, local_error
  character(len=80) :: chain
  !
  ! Evaluate NSIZE
  isize = nint(uvmax/uvmin)
  call gi4_round_forfft(isize,nsize,error,map_rounding,map_power)
  nsize = max(msize,nsize)
  !
  if (nsize.eq.osize) then
    allocate(work(2*nsize),ipft(nsize*nsize),ipf(nsize*nsize),stat=ier)
  else
    if (allocated(abeam)) deallocate(abeam)
    osize = 0
    allocate(abeam(nsize,nsize),work(2*nsize),ipft(nsize*nsize),ipf(nsize*nsize),stat=ier)
  endif
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  osize = nsize
  !
  nu = huv%gil%dim(1)
  nv = huv%gil%nvisi
  lx = nsize  ! Grid size
  ly = nsize
  nx = nsize  ! Map size
  ny = nsize
  !
  do_weig = all(map%uniform.ne.0)
  if (do_weig) then
    allocate(ipw(nv),ipv(nv),stat=ier)
  else
    allocate(ipw(nv),stat=ier)
  endif
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! That is for Natural Weighting
  jc = max(1,(2*huv%gil%nchan/3))  ! Weight channel
  lw = 7 + 3*jc                    ! Weight column 
  ipw(:) = duv(lw,:)
  !
  !
  if (do_weig) then
    write(chain,'(A,F8.2,A,F8.2)') 'Robust weighting ',map%uniform(2),' UV Cell ',map%uniform(1)
    call map_message(seve%i,rname,chain)
    ipv(:) = duv(2,:)
    local_error = .false.
    call doweig (nu,nv,   &
       &    duv,    &            ! Visibilities
       &    1,2,    &            ! U, V pointers
       &    jc,     &            ! Weight channel
       &    map%uniform(1),   &  ! Uniform UV cell size
       &    ipw,    &            ! Weight array
       &    map%uniform(2),   &  ! Fraction of weight
       &    ipv,              &  ! V values
       &    local_error,grid_code)
    if (local_error)  then
      error = .true.
      return
    endif
  endif
  !
  do_weig = any(map%taper(1:2).ne.0)
  if (do_weig) then
    write(chain,'(A,3(F8.2))') 'Taper ',map%taper(1:3)
    call map_message(seve%i,rname,chain)
    !
    ! Should also plug the TAPER here, rather than in DOFFT later  !
    call dotape (nu,nv,   &
       &    duv,   &             ! Visibilities
       &    1,2,   &             ! U, V pointers
       &    map%taper,  &        ! Taper
       &    ipw)                 ! Weight array
  endif
  !
  ! Compute FFTs
  !
  ! Define MAP_CELL
  rtmp = uvmax*huv%gil%freq*f_to_k
  map_cell = 0.002*nint(180.0*3600.0*500.0/rtmp/pixel_per_beam)
  map_cell = map_cell*pi/180.0/3600.0
  convert = (/dble(nx/2+1),0.d0,-dble(map_cell),   &
     &    dble(ny/2+1),0.d0,dble(map_cell)/)
  !
  ! Define UV Cell size
  uvcell = 299792458D-6/huv%gil%freq/(map_cell*nx)
  !
  ! Quick, simple minded gridding
  call doqfft (nu,nv,   &      ! Size of visibility array
     &    duv,          &      ! Visibilities
     &    1,2,lw,       &      ! U, V pointers
     &    lx,ly,        &      ! Cube size
     &    ipft,         &      ! FFT cube
     &    ipw,          &      ! Weight array
     &    uvcell)              ! UV cell size
  !
  ! Make beam, not normalized
  ndim = 2
  nn(1) = nx
  nn(2) = ny
  call extracs(1,nx,ny,1,ipft,ipf,lx,ly)
  call fourt  (ipf,nn,ndim,-1,1,work)
  call cmtore (ipf,abeam,nx,ny)
  !
  ! Normalize beam - no grid correction needed here
  rtmp = abeam(nx/2+1,ny/2+1)
  abeam = abeam/rtmp
  !
  ! Get Fitting Threshold 
  rtmp  = minval(abeam(:,:)) 
  thre = max(min(abs(-1.5*rtmp),0.7),0.3)
  !
  ! Fit beam
  ix_patch = nx/2
  iy_patch = ny/2
  major_axis = 0.
  minor_axis = 0.
  pos_angle  = 0.
  call fibeam (rname,abeam,nx,ny,   &
   &      ix_patch,iy_patch,thre,   &
   &      major_axis,minor_axis,pos_angle,   &
   &      convert,error)
  !
  if (sic_present(0,2)) then
    call gr_exec('CLEAR')
    call gr4_rgive(nx,ny,convert,abeam)
    call gr_exec('LIMITS /RG')
    call gr_exec('SET BOX SQUARE')
    call gr_exec('PLOT')
    call gr_exec('BOX /UNIT SEC')
    call gr_exec('LABEL "Offset (`)" /X')
    call gr_exec('LABEL "Offset (`)" /Y')
    call gr_exec('WEDGE')
    write(chain,*) 'LET TRUE_SCALE ',rtmp,1.0
    call exec_command(chain,local_error)
  endif
end subroutine map_beamsize

