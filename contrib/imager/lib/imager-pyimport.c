
/* Define entry point "initpyimager" for command "import pyimager" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(imager);
