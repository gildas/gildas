subroutine comm_discard(line,comm,error)
  use clean_def
  use clean_arrays
  use clean_types
  use gbl_message
  !
  use gkernel_interfaces, only : sic_delvariable, gildas_null
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !   Support for command DISCARD
  !
  ! Delete some internal buffer and its associated SIC variable
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  character(len=*), intent(in) :: comm
  logical, intent(out) :: error
  !
  character(len=32) :: key
  integer :: nk,ier, atype
  !
  error = .false.
  ier = 0
  !
  call sic_ke(line,0,1,key,nk,.true.,error)
  if (error) goto 100
  !
  if ((key.eq.'UV').or.(key.eq.'UV_DATA').or.(key.eq.'*')) then 
    !
    ! Free the previous zone
    call uv_free_buffers
    nullify(duv)
    if (allocated(duvi)) deallocate(duvi,stat=ier)
    !
    save_data(code_save_uv) = .false.
    call gildas_null(huv, type='UVT')
    ! No UV_PREVIEW done, then...
    call sic_delvariable('PREVIEW',.false.,error)
    error = .false.
    !
    call sic_delvariable ('UV',.false.,error)
    call sic_delvariable ('UVS',.false.,error)  
    !
    ! Unset optimization (this seems done automatically on other buffers ?)
    atype = 2
!    Print *,'Optimize status ',optimize(atype)%change, optimize(atype)%lastnc, ' change ',optimize(atype)%modif%modif 
!    Print *,'         file ',trim(optimize(atype)%modif%file)
!    
!      type mfile_t
!    character(len=filename_length) :: file=''  ! File name
!    ! Last modification time (to the file system granularity)
!    integer(kind=8) :: mtime
!    ! Last time a modification was proved (to the OS granularity)
!    integer(kind=8) :: ptime
!    ! Was the file modified before last evaluation?
!    logical         :: modif
!  end type mfile_t
!
    optimize(atype)%change = 2          ! Make sure it has changed
    optimize(atype)%modif%file = ' '    ! Reset the filename
    !
    
    if (key.ne.'*') goto 100
  endif
  !
  if (key.eq.'UVCONT') then
    ! Delete the CONTINUUM UV data if defined
    call sic_delvariable ('UVCONT',.false.,error)
    if (allocated(duvc)) deallocate(duvc,stat=ier)
    huvc%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'UVSELF') then
    !
    ! Delete the SELF array if defined
    call sic_delvariable ('UVSELF',.false.,error)
    if (allocated(duvself)) deallocate(duvself,stat=ier)
    hself%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'CONTINUUM') then
    ! Delete the CONTINUUM image if defined
    save_data(code_save_cont) = .false.
    call sic_delvariable ('CONTINUUM',.false.,error)
    if (allocated(dcont)) deallocate(dcont,stat=ier)
    hcont%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'UV_MODEL') then
    ! Free the previous zone
    call sic_delvariable ('UV_MODEL',.false.,error)
    if (allocated(duvm)) then
      deallocate(duvm,stat=ier)
      huvm%loca%size = 0
    endif
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'DIRTY') then
    save_data(code_save_dirty) = .false.
    call sic_delvariable ('DIRTY',.false.,error)
    if (allocated(ddirty)) deallocate(ddirty,stat=ier)
    hdirty%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'RESIDUAL') then
    save_data(code_save_resid) = .false.
    call sic_delvariable ('RESIDUAL',.false.,error)
    if (allocated(dresid)) deallocate(dresid,stat=ier)
    hresid%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'CLEAN') then
    save_data(code_save_clean) = .false.
    call sic_delvariable ('CLEAN',.false.,error)
    if (allocated(dclean)) deallocate(dclean,stat=ier)
    hclean%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'SKY') then
    save_data(code_save_sky) = .false.
    call sic_delvariable ('SKY',.false.,error)
    if (allocated(dsky)) deallocate(dsky,stat=ier)
    hsky%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'MASK') then
    save_data(code_save_mask) = .false.
    call sic_delvariable ('MASK',.false.,error)
    if (allocated(dmask)) deallocate(dmask,stat=ier)
    hmask%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'PRIMARY') then
    save_data(code_save_primary) = .false.
    call sic_delvariable ('PRIMARY',.false.,error)
    if (allocated(dprim)) deallocate(dprim,stat=ier)
    hprim%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'CCT') then
    save_data(code_save_cct) = .false.
    call sic_delvariable ('CCT',.false.,error)
    if (allocated(dcct)) deallocate(dcct,stat=ier)
    hcct%loca%size = 0
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.eq.'SINGLEDISH') then
    save_data(code_save_single) = .false.
    call sic_delvariable ('SINGLE',.false.,error)
    call sic_delvariable ('SHORT',.false.,error)
    if (key.ne.'*') goto 100 
  endif
  !
  if (key.ne.'*') then
    call map_message(seve%w,comm,'No code for '//key)
    error = .true.
    return
  endif
  !
100 continue
  if (ier.ne.0) then
    call map_message(seve%e,comm,trim(key)//' deallocation error')
    error = .true.
  endif
end subroutine comm_discard
