subroutine uv_map_comm(line,comm,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => uv_map_comm
  !---------------------------------------------------------------------
  !
  ! @ private
  !
  ! IMAGER
  !    Support for commands
  ! UV_MAP or UV_RESTORE /FIELDS /RANGE /TRUNCATE  /CONTINUUM /INDEX
  !   or 
  ! UV_SELF /RANGE /RESTORE
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: comm
  character(len=*), intent(inout) :: line
  logical, intent(inout) :: error
  !
  character(len=1), parameter :: question_mark='?'
  !
  logical :: use_self, do_cct
  integer :: nline, n, ier, nt
  character(len=2) :: code
  character(len=16) :: acomm, argum, topic
  ! ' UV_MAP', '/TRUNCATE',  '/FIELDS', '/RANGE', '/CONTINUUM', '/INDEX'
  integer, parameter :: o_trunc=1
  integer, parameter :: o_field=2
  integer, parameter :: o_range=3
  integer, parameter :: o_cont=4
  integer, parameter :: o_index=5
  ! ' UV_SELF', '/RANGE', '/RESTORE'
  integer, parameter :: opt_rang=1  ! /RANGE Option for UV_SELF
  integer, parameter :: opt_rest=2  ! /RESTORE Option for UV_SELF
  type(gildas) :: htmp
  !
  logical, save :: use_cont=.false.
  logical, save :: do_self=.false.
  logical :: err, goon
  character(len=80) :: chain, string
  !
  call imager_tree('UV_MAP_COMM from selfcal.f90')
  !
  err = .false.
  use_self = comm.eq.'UV_SELF'      ! Self Calibration image !
  call map_message(seve%d,comm,'Calling UV_MAP_COMM in selfcal.f90')
  !
  if (themap%nfields.ne.0) then
    !
    ! Mosaic case
    if (use_self) then
      call map_message(seve%i,'UV_SELF','not implemented for Mosaics')
      error =.true.
    else
      if (comm.eq.'UV_MAP') then
        ! Test the ? argument
        if (sic_narg(0).eq.1) then
          call sic_ch(line,0,1,argum,n,.false.,error)
          if (argum(1:1).eq.question_mark) then
            if (sic_present(0,2)) then
              call sic_ch(line,0,2,topic,nt,.true.,error)
              chain = 'HELP '//comm//topic
              call exec_program(chain)
              return        
            endif
            ! Get parameters first
            call map_prepare(comm,huv,themap,error)
            if (error) return
            !
            call exec_program("@ i_uv_map "//argum)
            return
          endif
        endif
        call map_message(seve%i,'UV_MOSAIC','UV data is a Mosaic')
        !
        ! Check arguments
        call map_query (line,'UV_MOSAIC',themap,goon,error) 
        if (.not.goon) return
        !
        call uv_shift_mosaic(line,'UV_MOSAIC',error)  ! Convert to Pointing offsets
        if (error) return
        !
        use_cont = sic_present(o_cont,0)
        if (use_cont) then
          call map_message(seve%i,comm,'/CONT option still experimental for Mosaics')
          ! Select Continuum data set (UV_MAP /CONT or UV_RESTORE after a UV_MAP /CONT)
          call switch_uv_cont(line,comm,.false.,code,htmp,error)
          if (error) return
          !
          call mosaic_uvmap('UV_MOSAIC',line,error)     ! Image
          call uv_buffer_resetuv(code) 
          call gdf_copy_header(htmp,huv,err)
          if (error) return          
          do_cont = .true.   ! Next CLEAN will be CONTINUUM
        else
          call mosaic_uvmap('UV_MOSAIC',line,error)     ! Image          
          do_cont = .false.  ! Next CLEAN will be no CONTINUUM
        endif
      else
        !
        ! Here this is UV_RESTORE
        if (use_cont) then    ! Remembered from last UV_MAP command
          call uv_shift_mosaic(line,'UV_MOSAIC',error)  ! Convert to Pointing offsets
          if (error) return
          call map_message(seve%w,comm,'Cannot yet restore Continuum Mosaic')
          ! Select Continuum data set (UV_MAP /CONT or UV_RESTORE after a UV_MAP /CONT)
          call switch_uv_cont(line,comm,.false.,code,htmp,error)
          if (error) return
          !
          call mosaic_restore(line,error)     ! Image
          call uv_buffer_resetuv(code) 
          call gdf_copy_header(htmp,huv,err)
          if (error) return          
          !
          ! There can be a size mis-match here ...
          if (any(hcont%gil%dim(1:2).ne.hsky%gil%dim(1:2))) then
            write(chain,'(A,I0,A,I0,A)') 'Re-sizing continuum image to [', &
              &   hsky%gil%dim(1),',',hsky%gil%dim(2),']'
            call map_message(seve%w,comm,chain)
            deallocate(dcont,stat=ier)
            deallocate(dcont,stat=ier)
            allocate(dcont(hsky%gil%dim(1),hsky%gil%dim(2)),stat=ier)
            call gdf_copy_header(hsky,hcont,error)
            hcont%gil%ndim = 2
            hcont%loca%size = hcont%gil%dim(1)*hcont%gil%dim(2)
            call sic_delvariable('CONTINUUM',.false.,error)
            call sic_mapgildas ('CONTINUUM',hcont,error,dcont)
          endif          
          !
          dcont(:,:) = dsky(:,:,1) ! Update CONTINUUM image too
          hcont%gil%minloc = hsky%gil%minloc
          hcont%gil%maxloc = hsky%gil%maxloc
          hcont%gil%rmin = hsky%gil%rmin
          hcont%gil%rmax = hsky%gil%rmax
        else
          call map_message(seve%w,comm,'UV data is a Mosaic, under test...')
          call uv_shift_mosaic(line,'UV_MOSAIC',error)  ! Convert to Pointing offsets
          if (error) return
          call mosaic_restore(line,error)      
        endif
      endif
    endif
    !
  else if (map_version.eq.0) then
    !
    ! New version
    if (use_self) then
      string = defmap_center
      if (.not.do_self) do_weig = .true.
      !
      ! Self-calibrated data 
      do_cct = sic_present(opt_rest,0) ! /RESTORE option
      if (.not.do_cct) then
        if (hself%loca%size.eq.0 .or. sic_present(opt_rang,0)) then
          call init_selfcal('UV_SELF',line,error)
          if (error) return
          !! call uv_dump_buffers('UV_SELF *')
          line = 'UV_SELF'
          nline = len_trim(line)
          call sic_analyse(acomm,line,nline,error)
        endif
      else 
        if (hself%loca%size.eq.0) then
          call map_message(seve%e,comm,'/RESTORE  Self calibrated UV data not defined')
          error = .true.
        else if (sic_narg(opt_rang).ge.0) then
          call map_message(seve%e,comm,'/RESTORE  conflicts with /RANGE')
          error = .true.
        endif
      endif
      if (error) return  
      call gildas_null(htmp,type='UVT')
      call uv_buffer_finduv(code) 
      !!    call uv_dump_buffers('SELFCAL')  !!
      call gdf_copy_header(huv,htmp,error)
      call gdf_copy_header(hself,huv,error)
      duv => duvself
      !
      ! Here, SG_MAP works because the UV table is already sorted...
      call sg_map(comm,line,do_cct,error)
      defmap_center = string          ! Restore MAP_CENTER
      call uv_buffer_resetuv(code) 
      !!    call uv_dump_buffers('SG_MAP')
      call gdf_copy_header(htmp,huv,err)
      if (error) return
      do_self = .true.
    else
      string = defmap_center
      do_self = .false.
      !
      ! Select current data set or possibly its Continuum sibbling 
      do_cct = comm.eq.'UV_RESTORE'
      ! In a UV_RESTORE command, USE_CONT is remembered from last UV_MAP command
      ! In a UV_MAP command, USE_CONT is defined by the /CONT option
      if (.not.do_cct) use_cont = sic_present(o_cont,0)
      !
      if (use_cont) then
        !
        ! Select Continuum data set (UV_MAP /CONT or UV_RESTORE after a UV_MAP /CONT)
        call switch_uv_cont(line,comm,do_cct,code,htmp,error)
        defmap_center = string
        if (error) return
        !
        ! Perform the imaging
        if (do_cct) defmap_center = ' '   ! The Continuum UV data has already been shifted
        call sg_map(comm,line,do_cct,error)
        defmap_center = string
        !
        ! Reset HUV buffer
        call uv_buffer_resetuv(code) 
        call gdf_copy_header(htmp,huv,err)
        if (error) return
        !
        if (do_cct) then
          ! There can be a size mis-match here ...
          if (any(hcont%gil%dim(1:2).ne.hclean%gil%dim(1:2))) then
            write(chain,'(A,I0,A,I0,A)') 'Re-sizing continuum image to [', &
              &   hclean%gil%dim(1),',',hclean%gil%dim(2),']'
            call map_message(seve%w,comm,chain)
            deallocate(dcont,stat=ier)
            deallocate(dcont,stat=ier)
            allocate(dcont(hclean%gil%dim(1),hclean%gil%dim(2)),stat=ier)
            call gdf_copy_header(hclean,hcont,error)
            hcont%gil%ndim = 2
            hcont%loca%size = hcont%gil%dim(1)*hcont%gil%dim(2)
            call sic_delvariable('CONTINUUM',.false.,error)
            call sic_mapgildas ('CONTINUUM',hcont,error,dcont)
          endif          
          !
          dcont(:,:) = dclean(:,:,1) ! Update CONTINUUM image too
          hcont%gil%minloc = hclean%gil%minloc
          hcont%gil%maxloc = hclean%gil%maxloc
          hcont%gil%rmin = hclean%gil%rmin
          hcont%gil%rmax = hclean%gil%rmax
        else
          do_cont = .true.  ! Next CLEAN, there will be a CONTINUUM image
        endif
      else
        ! Select the data set. Can be UV_DATA, UV_MODEL, or UV_RESIDUAL
        !
        if (current_uvdata.eq.'DATA_UV') then
          if (do_cct) defmap_center = ' '
          call sg_map(comm,line,do_cct,error) 
          defmap_center = string
        else                
          ! We may have an issue about data sorting here, if
          ! the data is to be rotated. Shifting only cause problem
          ! if the data has never been sorted...
          !
          call gildas_null(htmp,type='UVT')
          call uv_buffer_finduv(code) 
          call gdf_copy_header(huv,htmp,error)
          !
          call uvdata_select(comm,error)
          if (error) return
          !
          ! Now the Imaging Proper
          if (do_cct) defmap_center = ' '
          call sg_map(comm,line,do_cct,error)   
          defmap_center = string
          call uv_buffer_resetuv(code) 
          call gdf_copy_header(htmp,huv,err)
          if (error) return
          !
        endif
        do_cont = .false.  ! Next CLEAN will be no CONTINUUM
      endif
      if (error) return
    endif
    !
    if (.not.do_cct) call no_mosaic(comm)
    !
    ! End of Single-Field section
    !
  else if (map_version.ne.0) then
    !
    ! Old or intermediate version - Obsolescent or unsupported...
    if (use_self) then
      call map_message(seve%i,'UV_SELF','Not available with Old code (MAP_VERSION # 0)')
      error =.true.
    else if (sic_present(o_cont,0)) then
      call map_message(seve%i,comm,'/CONT option not available with Old code (MAP_VERSION # 0)')
      error =.true.
    else if (comm.eq.'UV_MAP') then
      call old_uvmap('OLDUV_MAP',line,error)
      call no_mosaic(comm) 
    else if (comm.eq.'UV_RESTORE') then
      call map_message(seve%i,comm,'Not available with Old code (MAP_VERSION # 0)')
      error =.true.
      return
    endif
    do_cont = .false.  ! Next CLEAN will be no CONTINUUM
  endif
  !
  if (comm.eq.'UV_RESTORE') then
    if (themap%nfields.ne.0) then
      last_shown = 'SKY'
    else
      last_shown = 'CLEAN'
    endif
  else
    last_shown = 'DIRTY'
  endif
  call check_view(1,last_shown)  ! Force VIEW re-computation for this
end subroutine uv_map_comm
!
subroutine no_mosaic(rname)
  use clean_def
  use clean_default
  use clean_arrays
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------  !
  ! @ private
  !
  ! IMAGER
  !   Switch back prompt to IMAGER>
  !---------------------------------------------------------------------  !
  character(len=*), intent(in) :: rname
  !
  logical :: error
  !
  if (user_method%mosaic) then
    call map_message(seve%i,rname,'Switch to NORMAL mode')
    call gprompt_set('IMAGER')
    user_method%trunca = 0.0
    call sic_delvariable('PRIMARY',.false.,error)
    hprim%gil%dim(1) = 1
    user_method%mosaic = .false.
  endif
end subroutine no_mosaic
!
subroutine init_selfcal(rname,line,error)
  use clean_arrays
  use clean_default
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => init_selfcal
  !---------------------------------------------------------------------  !
  ! @ private
  !
  ! IMAGER
  !    Support for command
  ! UV_MAP /SELF Min Max Type
  !
  ! Create the Self-Calibration UV table from the current one
  ! by averaging the specified range of channels.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(inout) :: line
  logical, intent(inout) :: error
  !
  integer, parameter :: opt_rang=1
  character(len=12) :: ctype
  integer :: ndim, ier
  integer, parameter :: mranges=50
  real(8) :: drange(mranges)
  integer :: nc(mranges),numchan,ntmp,i
  integer(kind=4), parameter :: mtype=3
  integer :: itype
  character(len=12) :: types(mtype),mytype
  data types /'CHANNEL','VELOCITY','FREQUENCY'/
  !
  real :: cpu0, cpu1
  real :: uvmax, uvmin
  integer :: nline, n, narg
  logical :: sorted, shift, needed
  real(kind=8) :: new(3)
  character(len=80) :: chain, acomm, string
  !
  ! Erase current Self-Calibration UV table
  if (allocated(duvself)) deallocate(duvself)
  if (allocated(duvraw)) deallocate(duvraw)
  call sic_delvariable('UVSELF',.false.,error) 
  error = .false.
  !
  ! Decode the command line arguments
  numchan = max(sic_narg(opt_rang),0)
  nc = 0
  if (mod(numchan,2).eq.0) then
    !  Default CTYPE is CHANNEL
    mytype = 'CHANNEL'
  else if (numchan.eq.1) then
    call sic_ch(line,opt_rang,numchan,ctype,ndim,.true.,error)
    if (ctype.ne.'*') then
      call map_message(seve%e,rname,'/RANGE option syntax error')
      error = .true.
      return
    endif
    numchan = 0
  else
    call sic_ke(line,opt_rang,numchan,ctype,ndim,.true.,error)
    if (error) return
    call sic_ambigs(rname,ctype,mytype,itype,types,mtype,error)
    if (error)  return
    numchan = numchan-1
  endif
  !
  do i=1,numchan
    call sic_r8(line,opt_rang,i,drange(i),.true.,error)
    if (error) return
  enddo
  !
  ! Then, shift, rotate and sort the initial UV Table
  narg = sic_narg(0)
  string = defmap_center
  n = len_trim(string)
  !
  if (narg.eq.0) then
    if (n.ne.0) then
      line = 'CALIBRATE'//char(92)//trim(rname)//' '//string(1:n)
      nline = len_trim(line)
      call sic_analyse(acomm,line,nline,error)
      if (error) return
    endif
  endif
  call map_center(line,rname,huv,shift,new,error)
  if (error) return
  !
  call gag_cpu(cpu0)
  needed = themap%uniform(2).ne.0
  call uv_sort (huv,duv,error,sorted,shift,new,uvmax,uvmin,needed)
  if (error) return
  if (.not.sorted) then
    ! Redefine SIC variables (mandatory)
    call map_uvgildas ('UV',huv,error,duv)
  endif
  call gag_cpu(cpu1)
  write(chain,'(A,F9.2)') 'Finished sorting ',cpu1-cpu0
  call map_message(seve%i,rname,chain)
  !
  ! Now, define the Raw UV table
  call gildas_null(hraw,'UVT')
  !
  huv%r2d => duv
  ndim = huv%gil%nlead+3+huv%gil%ntrail
  allocate(duvraw(ndim,huv%gil%dim(2)),duvself(ndim,huv%gil%dim(2)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Self Calibration UV table allocation error')
    error = .true.
    return
  endif
  hraw%r2d => duvraw
  !
  ! At this stage, we should reset MAP_CENTER...
  acomm = 'LET MAP_CENTER " "'
  call exec_command(acomm,error)
  !
  ! Convert range to channels
  nc = 0
  if (numchan.eq.0) then
    numchan = 2
    nc(1) = 1
    nc(2) = huv%gil%nchan 
    !
    ! Could use here the same limit as defined by UV_CHECK BEAM
    call channel_restrict(rname,nc(1),nc(2),error)
    error = .false.
  else if (mytype.eq.'CHANNEL') then
    nc(1:numchan) = nint(drange(1:numchan))
  else if (mytype.eq.'VELOCITY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - huv%gil%voff) / huv%gil%vres + huv%gil%ref(huv%gil%faxi)
  else if (mytype.eq.'FREQUENCY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - huv%gil%freq) / huv%gil%fres + huv%gil%ref(huv%gil%faxi)
  else
    call map_message(seve%e,rname,"Type of value '"//trim(mytype)//"' not supported")
    error = .true.
    defmap_center = string
    return
  endif
  !
  do i=2,numchan,2
    if (nc(i).lt.nc(i-1)) then
      ntmp = nc(i)
      nc(i) = nc(i-1)
      nc(i-1) = ntmp
    endif
  enddo
  !
  call do_uv_average(rname,huv,hraw,numchan,nc,error)
  !
  ! Copy to UVSELF
  call gildas_null(hself,type='UVT')
  call gdf_copy_header(hraw,hself,error)
  duvself(:,:) = duvraw(:,:)
  call map_uvgildas ('UVSELF',hself,error,duvself)
  !
  ! At this stage, we should reset MAP_CENTER...
  defmap_center = string
end subroutine init_selfcal
!
subroutine do_uv_average(rname,hin,hou,numchan,nc,error)
  use gildas_def
  use gkernel_interfaces
  use imager_interfaces, only : map_message, sub_uv_average
  use image_def
  use gbl_format
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !    Support for command SELFCAL
  !   Average all selected line channels to 1 continuum channel
  !   Driver routine: prepare header and call per-visibility processing
  !   subroutine
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(gildas), intent(in) :: hin
  type(gildas), intent(inout) :: hou
  integer, intent(in) :: numchan
  integer, intent(in) :: nc(numchan)
  logical, intent(out) :: error
  ! Local
  character(len=filename_length) :: text
  integer(4) :: nchan, nsum, i
  real(8) :: freq
  !
  error = .false.
  !
  if (hin%loca%size.eq.0 .or. .not.associated(hin%r2d)) then
    call map_message(seve%e,rname,'Input UV data is not allocated')
    error = .true.
    return
  endif
  !
  ! Define number of channels being added and effective mean channel
  nchan = 0
  nsum = 0
  do i=2,numchan,2
    nchan = nc(i)-nc(i-1)+1+nchan
    nsum = (nc(i)+nc(i-1))*(nc(i)-nc(i-1)+1)/2+nsum
    if (i.eq.2) then
      write (text,'(2(A,I0))') 'Averaging channels from ',   &
              nc(i-1),' to ',nc(i)
    else
      write (text,'(2(A,I0))') '              and  from ',   &
              nc(i-1),' to ',nc(i)
    endif
    call map_message(seve%i,rname,text)
  enddo
  !
  ! Define output image
  call gdf_copy_header (hin, hou, error)
  !
  ! Do not forget trailing columns
  hou%gil%dim(1) = 10+hou%gil%ntrail
  hou%gil%inc(1) = hou%gil%inc(1)*nchan
  hou%gil%ref(1) = 1.d0-(float(nsum)/nchan-hou%gil%ref(1))/nchan
  hou%gil%vres = nchan*hou%gil%vres
  hou%gil%fres = nchan*hou%gil%fres
  hou%gil%nchan = 1
  call gdf_uv_shift_columns(hin,hou)
  call gdf_setuv (hou, error)
  if (error) return
  !
  ! Bring reference channel to 1
  freq = (1.d0-hou%gil%ref(1))*hou%gil%inc(1) + hou%gil%val(1)
  hou%gil%ref(1) = 1.
  hou%gil%val(1) = freq
  ! Set Frequency accordingly
  hou%gil%freq = freq
  !
  call sub_uv_average (hou%r2d, hou%gil%dim(1),hou%gil%dim(2),   &
   &    hou%gil%nlead,  hou%gil%ntrail,               &
   &    hin%r2d ,hin%gil%dim(1),nc,numchan)
  hou%loca%size = hou%gil%dim(1) * hou%gil%dim(2)
end subroutine do_uv_average
!
subroutine sub_uv_average (out,nx,nv,nlead,ntrail,inp,ny,nc,num)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !    Support for command SELFCAL
  !   Average all selected line channels to 1 continuum channel
  !---------------------------------------------------------------------
  integer(kind=index_length) :: nx  ! Size of output visibility
  integer(kind=index_length) :: nv  ! Number of visibilities
  integer, intent(in) :: nlead      ! Leading columns
  integer, intent(in) :: ntrail     ! Trailing columns
  real :: out(nx,nv)                ! Output visibilities
  integer(kind=index_length) :: ny  ! Size of input visibility
  real :: inp(ny,nv)                ! Input visibilities
  integer :: num                    ! Number of ranges X 2
  integer :: nc(num)                ! Range boundaries
  ! Local
  integer :: k,kk,l
  integer(kind=index_length) :: j
  real :: a,b,c
  !
  do j=1,nv
    out(1:nlead,j) = inp(1:nlead,j)
    a = 0.0
    b = 0.0
    c = 0.0
    do l=2,num,2
      do k=nc(l-1),nc(l)
        kk = nlead+3*k
        if (inp(kk,j).gt.0) then
          a = a+inp(kk-2,j)*inp(kk,j)
          b = b+inp(kk-1,j)*inp(kk,j)
          c = c+inp(kk  ,j)
        endif
      enddo
    enddo
    if (c.ne.0.0) then
      out(8,j) =a/c
      out(9,j) =b/c
      out(10,j)=c              ! time*band
    else
      out(8,j) =0.0
      out(9,j) =0.0
      out(10,j)=0.0
    endif
    if (ntrail.gt.0) out(11:nx,j) = inp(ny-ntrail+1:ny,j)
  enddo
end subroutine sub_uv_average
!
subroutine selfcal(line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !    Support for command SELFCAL
  !
  !   This actually cannot be in the CLEAN language, because it implies
  !   re-entrancy of RUN_CLEAN - Not a big issue, but currently forbidden 
  !   by our coding practice, so we place it the ADVANCED\ language
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: comm
  character(len=*), intent(inout) :: line
  logical, intent(inout) :: error
  !
  character(len=256) :: argum
  integer :: na
  logical :: do_insert
  !
  do_insert = sic_lire().eq.0
  if (sic_present(1,0)) then
    call exec_program('@ p_selfcal /WIDGET')
  else
    if (sic_narg(0).eq.0) then
      call sic_get_char('SELF_MODE',argum,na,error)
    else
      ! Pass all arguments "as they are"
      na = index(line,trim(comm))+8
      argum = line(na:)
    endif
    call exec_program('@ p_selfcal '//argum)
  endif
  if (do_insert) call sic_insert_log(line)
end subroutine selfcal
!
subroutine switch_uv_cont(line,comm,do_cct,code,htmp,error)
  use gildas_def 
  use clean_arrays
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => switch_uv_cont
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  !   Support for command UV_MAP /CONT
  ! Temporarily point towards the "continuum" UV data
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line   ! Command line
  character(len=*), intent(in)    :: comm   ! Command name
  character(len=2), intent(inout) :: code   ! UV buffer code
  type(gildas), intent(out) :: htmp
  logical, intent(in) :: do_cct
  logical, intent(inout) :: error
  !
  ! ' UV_MAP', '/TRUNCATE',  '/FIELDS', '/RANGE', '/CONTINUUM', '/INDEX'
  integer, parameter :: o_trunc=1
  integer, parameter :: o_field=2
  integer, parameter :: o_range=3
  integer, parameter :: o_cont=4
  integer, parameter :: o_index=5
  !
  integer :: channels(3)
  integer :: ier
  !
  call gildas_null(htmp,type='UVT')
  call uv_buffer_finduv(code)           ! Get next UV buffer code 
  call gdf_copy_header(huv,htmp,error)
  !
  ! Select the data set. Can be UV_DATA, UV_MODEL, or UV_RESIDUAL
  ! We may have an issue about data sorting here,
  ! as well as Weight re-computation
  call uvdata_select(comm,error)
  if (error) return
  !
  if (do_cct) then
    ! The UV data may have changed since last UV_MAP /CONT command
    if (huvc%loca%size.eq.0) then
      call map_message(seve%e,comm,'Continuum UV data not defined')
      error = .true.
      return
    endif
  else
    call sub_uvcont_header(line,error,huvc,channels,o_cont,o_index,o_range)
    if (error) return
    !
    if (allocated(duvc)) deallocate(duvc)
    allocate(duvc(huvc%gil%dim(1),huvc%gil%dim(2)),stat=ier)
    huvc%loca%size = huvc%gil%dim(1)*huvc%gil%dim(2)
    huvc%r2d => duvc
    huv%r2d => duv
    call sub_uvcont_data(line,huvc,huv,channels,dchanflag,o_index,error)
    if (error) return
    !
    ! Weights must be recomputed
    do_weig = .true.
    call sic_delvariable ('UVCONT',.false.,error)
    call sic_mapgildas('UVCONT',huvc,error,duvc)
  endif
  !
  ! Point to this UV data set
  call gdf_copy_header(huvc,huv,error)
  duv => duvc
  !
end subroutine switch_uv_cont
 
