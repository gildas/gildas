!
subroutine no_check_mask(method,head)
  use clean_def
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !     Dummy replacement for "check_mask"
  !---------------------------------------------------------------------
  type(gildas),    intent(in)    :: head   !
  type(clean_par), intent(inout) :: method !
  !
  Print *,'Called check_mask'
end subroutine no_check_mask
!
subroutine check_mask(amethod,head)
  use clean_def
  use image_def
  use gbl_message
  use gkernel_types
  use imager_interfaces, except_this=>check_mask
  use clean_support
  use clean_arrays
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER   
  !     Check that the search mask is defined, and computes its
  !     Box boundaries, returned in amethod%box
  !---------------------------------------------------------------------
  type(gildas),    intent(in)    :: head       ! Image   header
  type(clean_par), intent(inout) :: amethod    ! Clean method parameters
  !
  ! Local variables
  integer, save :: last_box(4)
  integer nx,ny
  character(len=10) :: rname = 'CHECK_MASK'
  !
  ! Mask
  nx = head%gil%dim(1)
  ny = head%gil%dim(2)
  if (amethod%do_mask) then
    !
    if (support_type.eq.support_mask) then
      if (hmask%gil%dim(1).ne.nx .or. hmask%gil%dim(2).ne.ny) then
        call map_message(seve%w,rname,'Mask size differ')
      endif
      !
      ! Setup the Logical mask from the Real mask and its bounding box
      call get_lmask (hmask,dmask(:,:,1),head,d_mask,amethod%box)
      call lmask_to_list(d_mask,nx*ny,d_list,amethod%nlist)
      amethod%imask = 1
    else
      !
      ! Compute the logical mask from the bounding polygon
      call gr8_glmsk(supportpol,d_mask,nx,ny,   &
                     head%gil%convert(1,1),head%gil%convert(1,2),amethod%box)
      call lmask_to_list(d_mask,nx*ny,d_list,amethod%nlist)
      amethod%imask = -1
    endif
    amethod%do_mask = .false.
    last_box = amethod%box
  else
    amethod%box = last_box
  endif
end subroutine check_mask
