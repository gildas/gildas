subroutine get_weightmode(task,mode,error)
  use gkernel_interfaces
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Decode the weighting mode
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: task    ! Caller name
  character(len=*), intent(inout) :: mode ! Weighting mode
  logical, intent(out) :: error           ! Error flag
  !
  integer i,n
  character(len=8) :: argum
  character(len=8) :: vweight(3)=(/'NATURAL ','UNIFORM ','ROBUST  '/)
  !
  argum = mode
  call sic_get_char('MAP_WEIGHT',argum,n,error)
  call sic_upper (argum)
  call sic_ambigs (task,argum,mode,i,vweight,3,error)
  if (error) then
    call map_message(seve%e,task,'Invalid weight mode '//argum)
  else
    call map_message(seve%i,task,'Using '//vweight(i)//' weighting')
  endif
end subroutine get_weightmode
