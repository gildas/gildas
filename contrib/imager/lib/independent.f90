subroutine rmask_to_list(rmask,m,list,n)
  !---------------------------------------------------------------------
  ! @ public
  ! 
  !   IMAGER public routine
  !   Get list of positions from a Real mask (1-D index)
  !---------------------------------------------------------------------
  integer, intent(in) :: m         ! Number of entries
  real, intent(in) :: rmask(m)     ! Mask values
  integer, intent(out) :: n        ! Number of TRUE values
  integer, intent(out) :: list(m)  ! Position of entries 
  ! 
  integer i,j
  !
  j = 0
  do i=1,m
    if (rmask(i).gt.0) then
      j = j+1
      list(j) = i
    endif
  enddo
  n = j
  do i=n+1,m
    list(i) = 0
  enddo
end subroutine rmask_to_list
!
subroutine lmask_to_list(lmask,m,list,n)
  !---------------------------------------------------------------------
  ! @ public
  ! 
  !   IMAGER public routine
  !   Get list of positions from a Logical mask (1-D index)
  !---------------------------------------------------------------------
  integer, intent(in) :: m         ! Size of mask 
  logical, intent(in) :: lmask(m)  ! Mask values
  integer, intent(out) :: n        ! Number of TRUE values
  integer, intent(out) :: list(m)  ! Position of entries 
  ! 
  integer i,j
  !
  j = 0
  do i=1,m
    if (lmask(i)) then
      j = j+1
      list(j) = i
    endif
  enddo
  n = j
  do i=n+1,m
    list(i) = 0
  enddo
end subroutine lmask_to_list
!
subroutine get_listsize (mask,m,n)
  !---------------------------------------------------------------------
  ! @ public
  ! 
  !   IMAGER public routine
  !   Count number of TRUE values (Rank 1 arrays)
  !---------------------------------------------------------------------
  integer, intent(in) :: m        ! Number of entries
  integer, intent(out) :: n       ! Number of TRUE values
  logical, intent(in) :: mask(m)  ! Mask values
  !
  integer i
  !
  n = 0
  do i=1,m
    if (mask(i)) n = n+1
  enddo
end subroutine get_listsize
!
subroutine compute_atten (nx,ny,np,atten,primary,mask,   &
     &    wsear,wrest,wmin)
  !---------------------------------------------------------------------
  ! @ public
  ! 
  !   IMAGER public routine
  !   Set the mosaic weights
  !---------------------------------------------------------------------
  integer, intent(in) :: nx               ! X size
  integer, intent(in) :: ny               ! Y size 
  integer, intent(in) :: np               ! Number of pointings
  real, intent(out) ::  atten(nx,ny)      ! Weight map
  real, intent(in) ::  primary(np,nx,ny)  ! Primary beams
  real, intent(in) ::  wsear              ! Search threshold
  real, intent(in) ::  wrest              ! Restore threshold   
  real, intent(in) ::  wmin               ! Minimum beal value  
  logical, intent(inout) ::  mask(nx,ny)  ! Search mask
  !
  real wr,ws2,wr2
  integer i,j,ip
  !
  wr2 = wrest*wrest  ! It is a square quantity 
  ws2 = wsear*wsear  ! Also 
  !
  ! Compute the "atten" function 1/N
  do j=1,ny
    do i=1,nx
      wr = 0.0
      do ip=1,np
        if (primary(ip,i,j).gt.wmin) then
          wr = wr+primary(ip,i,j)*primary(ip,i,j)
        endif
      enddo
      ! Cut the search mask
      if (wr.le.ws2) mask(i,j) = .false.
      ! Cut the restore area if it is not a search area also
      if (wr.le.wr2 .and. .not.mask(i,j)) wr = 0.0
      ! Convert to the 1/N function
      if (wr.ne.0.0) then
        atten(i,j) = 1.0/sqrt(wr)
      else
        atten(i,j) = 0.0
      endif
    enddo
  enddo
end subroutine compute_atten
!
subroutine cmplx_mul (out,in,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  ! 
  !   IMAGER support routine
  !   Complex multiplication, using pseudo-complexes     OUT = OUT*IN
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in) :: n     ! Size of arrays
  real, intent(inout) :: out(2,n)                ! Result array
  real, intent(in) :: in(2,n)                    ! Second input array
  ! Local
  integer(kind=size_length) :: i
  real ar,ai
  do i=1,n
    ar = in(1,i)*out(1,i)-in(2,i)*out(2,i)
    ai = in(1,i)*out(2,i)+in(2,i)*out(1,i)
    out(1,i) = ar
    out(2,i) = ai
  enddo
end subroutine cmplx_mul
!
subroutine check_box(nx,ny,blc,trc)
  !---------------------------------------------------------------------
  ! @ public
  ! 
  !   IMAGER public routine
  !   Define the search box corners 
  !   The default is the inner quarter of the image
  !---------------------------------------------------------------------
  integer, intent(in) :: nx,ny         ! Size of image
  integer, intent(inout) :: blc(2)     ! Bottom left corner
  integer, intent(inout) :: trc(2)     ! Top right corner
  !
  ! Check inner quarter if not specified
  if (blc(1).eq.0) then
    blc(1) = nx/4+1
  else
    blc(1) = max(blc(1),1)
  endif
  if (blc(2).eq.0) then
    blc(2) = ny/4+1
  else
    blc(2) = max(blc(2),1)
  endif
  if (trc(1).eq.0) then
    trc(1) = 3*nx/4
  else
    trc(1) = min(trc(1),nx)
  endif
  if (trc(2).eq.0) then
    trc(2) = 3*ny/4
  else
    trc(2) = min(trc(2),ny)
  endif
end subroutine check_box
