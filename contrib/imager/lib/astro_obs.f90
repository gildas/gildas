subroutine astro_observatory_byname(arg,error)
  use gbl_message
  use gkernel_interfaces
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !  Set the Astro observatory given its name
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: arg    ! Telescope name
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='OBSERVATORY'
  character(len=4) :: teles  ! Comparison is based on 4 characters
                             ! Hide this "detail" here
  real(4) :: diam
  !
  teles = arg  ! arg can be smaller or larger than 4 characters!
  freq = 100.d0
  !
  call gwcs_observatory(arg,lonlat,altitude,slimit,diam,error)
  select case (teles)
  case ('PARA','VLT ','VLTI')
    call astro_message(seve%i,rname,  &
      &  'Frequency set to 299.792458 THz = 1 micron')
    freq = 299792.458d0
  case ('IOTA')
    call astro_message(seve%i,rname,  &
      &  'Frequency set to 187.370286 THz = 1.6 micron')
    freq = 187370.286d0
  case ('PTI ')
    call astro_message(seve%i,rname,'Frequency set to 136.269299 THz = '//  &
       & '2.2 micron')
    freq = 136269.299d0
  case ('GI2T')
    call astro_message(seve%i,rname,'Frequency set to 500 THz = '//  &
       & '0.6 micron')
    freq = 499653.8397d0
  end select
end subroutine astro_observatory_byname
!
subroutine astro_message(seve,comm,mess)
  use imager_interfaces, only : map_message
  integer :: seve
  character(len=*) :: comm
  character(len=*) :: mess
  !
  call map_message(seve,comm,mess)
end subroutine astro_message
