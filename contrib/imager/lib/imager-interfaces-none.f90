module imager_interfaces_none
  interface
    subroutine imager_write_data(head,data,error)
      use image_def
      use gkernel_types
      use clean_types
      ! 
      type(gildas), intent(inout) :: head
      real, intent(in) :: data(*)
      logical, intent(inout) :: error
    end subroutine imager_write_data
  end interface
  !
  interface
    subroutine compact_data(error)
      use clean_def
      use clean_arrays
      use gbl_message
      logical error
    end subroutine compact_data
  end interface
  !
  interface
    subroutine new_dirty_beam
      use clean_def
      use clean_arrays
      use clean_beams
      !
      ! needed when a new dirty map is computed by command uv_map:
    end subroutine new_dirty_beam
  end interface
  !
  interface
    subroutine define_var (error)
      use gildas_def
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use clean_support
      use clean_beams
      use grid_control
      !
      logical :: error  ! Logical error flag
    end subroutine define_var
  end interface
  !
  interface
    subroutine big_wait(n,b)
      real, intent(inout) :: b
      integer, intent(in) :: n
    end subroutine big_wait
  end interface
  !
  interface
    subroutine map_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine map_message_set_id
  end interface
  !
  interface
    subroutine message_attribute(cattr)
      use gbl_ansicodes
      !
      character(len=*), intent(in) :: cattr
    end subroutine message_attribute
  end interface
  !
  interface
    subroutine cossin(phase,rcos,rsin)
      !-------------------------------------------------------
      ! Semi-Fast,  Semi-accurate Sin/Cos pair computation
      ! using (not yet clever) interpolation from a precise
      ! loop-up table
      !
      ! A solution using Taylor expansion and symmetries
      ! would be faster and more accurate
      !-------------------------------------------------------
      real(8), intent(inout) :: phase
      real(8), intent(out) :: rcos
      real(8), intent(out) :: rsin
    end subroutine cossin
  end interface
  !
  interface
    subroutine get_stopping(miter,ares,iplane)
      use clean_arrays
      !
      integer, intent(inout) :: miter
      real, intent(inout) :: ares
      integer, intent(in) :: iplane
    end subroutine get_stopping
  end interface
  !
  interface
    subroutine maxcmp(nx,ny,wl,a)
      integer, intent(in) :: nx,ny
      real, intent(in) :: wl(2,nx,ny)
      real, intent(out) :: a
    end subroutine maxcmp
  end interface
  !
  interface
    subroutine userc (nv,coords,xref,xval,xinc,yref,yval,yinc)
      integer, intent(in) :: nv                     !
      real(4), intent(out) :: coords(2,nv)          !
      real(8), intent(in) :: xref                   !
      real(8), intent(in) :: xval                   !
      real(8), intent(in) :: xinc                   !
      real(8), intent(in) :: yref                   !
      real(8), intent(in) :: yval                   !
      real(8), intent(in) :: yinc                   !
    end subroutine userc
  end interface
  !
  interface
    subroutine progress_report(action,iv,nv,mv,percentage_step)
      character(len=*), intent(in) :: action ! Action name
      integer, intent(in) :: iv    ! Current index
      integer, intent(in) :: nv    ! Number of values per block
      integer, intent(in) :: mv    ! Total number of values
      integer, intent(in) :: percentage_step ! Frequency of report
    end subroutine progress_report
  end interface
  !
  interface
    subroutine mos_primary (prim,primary,bsize)
      !
      ! Mosaic and Primary beam support
      ! Compute a Gaussian primary beam
      !
      use image_def
      use gkernel_types
      type(gildas), intent(in) :: prim         ! Primary beam header
      real, intent(inout) :: primary(prim%gil%dim(1),prim%gil%dim(2)) ! Values
      real, intent(in) :: bsize                ! Primary beam size in radian
    end subroutine mos_primary
  end interface
  !
  interface
    subroutine mos_inverse (n,weight,thre)
      integer, intent(in) :: n
      real, intent(inout) :: weight(n)
      real, intent(in) :: thre
    end subroutine mos_inverse
  end interface
  !
  interface
    subroutine mos_addsq (n,nf,weight,lobe)
      integer, intent(in) :: n                      ! Size of arrays
      integer, intent(in) :: nf                     ! Number of fields
      real, intent(inout) :: weight(n)              ! Total weight
      real, intent(in) :: lobe(nf,n)                ! Primary beam array
    end subroutine mos_addsq
  end interface
  !
  interface
    subroutine mrc_clear
    end subroutine mrc_clear
  end interface
  !
  interface
    subroutine douvext(jc,nv,visi,jx,jy,umin,umax,vmin,vmax)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Compute extrema of UV coverage. No assumption,
      !   but may return 0 is all negative or all Positive
      !---------------------------------------------------------------------
      integer, intent(in) :: nv  ! Size of a visibility
      integer, intent(in) :: jc  ! Number of visibilities
      integer, intent(in) :: jx  ! X coordinate pointer
      integer, intent(in) :: jy  ! Y coordinate pointer
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      real, intent(out) :: umin,umax ! U extrema
      real, intent(out) :: vmin,vmax ! V extrema
    end subroutine douvext
  end interface
  !
  interface
    function minimum(n,a)
      integer n,i
      real a(*)
      real minimum
    end function minimum
  end interface
  !
  interface
    subroutine uv_free_buffers
      use clean_def
      use clean_arrays
      !---------------------------------------------------------------------
      ! IMAGER
      !   Deallocate all UV buffers.
      !---------------------------------------------------------------------
    end subroutine uv_free_buffers
  end interface
  !
  interface
    subroutine uv_buffer_finduv(code) 
      use clean_arrays
      character(len=1), intent(out) :: code
    end subroutine uv_buffer_finduv
  end interface
  !
  interface
    subroutine uv_buffer_resetuv(code) 
      use clean_arrays
      character(len=1), intent(in) :: code
    end subroutine uv_buffer_resetuv
  end interface
  !
  interface
    subroutine buffers_comm(line,error)
      use clean_types
      !
      character(len=*), intent(in) :: line    
      logical :: error
    end subroutine buffers_comm
  end interface
  !
  interface
    subroutine uvdata_select(comm,error)
      use clean_arrays
      use clean_default
      use gbl_message
      !
      character(len=*), intent(in):: comm
      logical, intent(inout) :: error
    end subroutine uvdata_select
  end interface
  !
  interface
    function continuum_emg(x, amp, mu, sig, lamb)
      real(4) :: continuum_emg ! intent(out)
      real(4), intent(in) :: x
      real(4), intent(in) :: amp ! Amplitude of function
      real(4), intent(in) :: mu  ! Position
      real(4), intent(in) :: sig ! Width
      real(4), intent(in) :: lamb ! Exponential factor
    end function continuum_emg
  end interface
  !
  interface
    function continuum_gauss(x, amp, mu, sig)
      real(4) :: continuum_gauss ! intent(out)
      real(4), intent(in) :: x
      real(4), intent(in) :: amp ! Amplitude of function
      real(4), intent(in) :: mu  ! Position
      real(4), intent(in) :: sig ! Width
    end function continuum_gauss
  end interface
  !
  interface
    subroutine parabola(x,y,a,b,c) 
      !----------------------------------------------------------------------
      !	Solve for Y = a + b*x() + c*x()^2
      !----------------------------------------------------------------------
      real, intent(in) :: x(3)
      real, intent(in) :: y(3)
      real, intent(out) :: a,b,c
    end subroutine parabola
  end interface
  !
  interface
    function zant(i,r)
      !
      ! Return the apparent number of Antenna #i when reference
      ! antenna is #r. Since #r is not in the list of antennas for
      ! which a solution is to be searched, this number
      ! is #i for #i < #r, and #i-1 for #i > #r
      !
      integer :: zant                   ! intent(out)
      integer, intent(in) :: i          !
      integer, intent(in) :: r          !
    end function zant
  end interface
  !
  interface
    subroutine mth_dpotrf (name, uplo, n, a, lda, error)
      character(len=*) :: name           !
      character(len=*) :: uplo           !
      integer :: n                       !
      integer :: lda                     !
      real(8) :: a(lda,*)                 !
      logical :: error                   !
    end subroutine mth_dpotrf
  end interface
  !
  interface
    subroutine mth_dpotrs (name,   &
         &    uplo, n, nrhs, a, lda, b, ldb, info )
      character(len=*) :: name           !
      character(len=*) :: uplo           !
      integer :: n                       !
      integer :: nrhs                    !
      integer :: lda                     !
      real(8) :: a(lda,*)                 !
      integer :: ldb                     !
      real(8) :: b(ldb,*)                 !
      ! Local
      integer :: info
      logical :: error
      !
      !  Purpose
      !  =======
      !
      !  DPOTRS solves a system of linear equations A*X = B with a symmetric
      !  positive definite matrix A using the Cholesky factorization
      !  A = U**T*U or A = L*L**T computed by DPOTRF.
      !
      !  Arguments
      !  =========
      !
      !  UPLO    (input) CHARACTER*1
      !          = 'U':  Upper triangle of A is stored;
      !          = 'L':  Lower triangle of A is stored.
      !
      !  N       (input) INTEGER
      !          The order of the matrix A.  N >= 0.
      !
      !  NRHS    (input) INTEGER
      !          The number of right hand sides, i.e., the number of columns
      !          of the matrix B.  NRHS >= 0.
      !
      !  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
      !          The triangular factor U or L from the Cholesky factorization
      !          A = U**T*U or A = L*L**T, as computed by DPOTRF.
      !
      !  LDA     (input) INTEGER
      !          The leading dimension of the array A.  LDA >= max(1,N).
      !
      !  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
      !          On entry, the right hand side matrix B.
      !          On exit, the solution matrix X.
      !
      !  LDB     (input) INTEGER
      !          The leading dimension of the array B.  LDB >= max(1,N).
      !
      !  INFO    (output) INTEGER
      !          = 0:  successful exit
      !          < 0:  if INFO = -i, the i-th argument had an illegal value
      !
      ! Call LAPACK routine
    end subroutine mth_dpotrs
  end interface
  !
  interface
    subroutine mth_dpbtrf (name, uplo, n, kd, ab, ldab, error)
      character(len=*) :: name             !
      character(len=*) :: uplo             !
      integer :: n                         !
      integer :: kd                        !
      integer :: ldab                      !
      real(8) :: ab(ldab,*)                 !
      logical :: error                     !
    end subroutine mth_dpbtrf
  end interface
  !
  interface
    subroutine mth_dpbtrs (name,   &
         &    uplo, n, kd, nrhs, ab, ldab, b, ldb, error)
      character(len=*) :: name             !
      character(len=*) :: uplo             !
      integer :: n                         !
      integer :: kd                        !
      integer :: nrhs                      !
      integer :: ldab                      !
      real(8) :: ab(ldab,*)                 !
      integer :: ldb                       !
      real(8) :: b(ldb,*)                   !
      logical :: error                     !
    end subroutine mth_dpbtrs
  end interface
  !
  interface
    subroutine mth_fail (fac,prog,ifail,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! Error handling routine
      !---------------------------------------------------------------------
      character(len=*) :: fac           !
      character(len=*) :: prog          !
      integer :: ifail                  !
      logical :: error                  !
    end subroutine mth_fail
  end interface
  !
  interface
    subroutine overlap(a,n,b,m,c,k)
      !
      ! Find the intersection of ensembles A and B
      !
      integer, intent(in) :: n
      integer, intent(in) :: a(n)
      integer, intent(in) :: m
      integer, intent(in) :: b(m)
      integer, intent(inout) :: k
      integer, intent(inout) ::c(*)
    end subroutine overlap
  end interface
  !
  interface
    subroutine union(a,n,b,m,c,k)
      !
      ! Find the Union of ensembles A and B
      !
      integer, intent(in) :: n
      integer, intent(in) :: a(n)
      integer, intent(in) :: m
      integer, intent(in) :: b(m)
      integer, intent(inout) :: k
      integer, intent(inout) ::c(*)
    end subroutine union
  end interface
  !
  interface
    subroutine do_fft (nx,ny,nf,fft,work)
      integer, intent(in)  :: nx                     !
      integer, intent(in)  :: ny                     !
      integer, intent(in)  :: nf                     !
      complex, intent(inout) :: fft(nx,ny,nf)
      real, intent(inout)  :: work(2*max(nx,ny))
    end subroutine do_fft
  end interface
  !
  interface
    subroutine uvshort_convol (du,dv,resu,conv)
      use clean_def
      !------------------------------------------------------------------------  
      !
      ! Compute convolving factor resu
      ! resu is the result of the multiplication of the convolution functions,
      ! for u and v axes, at point (du,dv)
      !
      ! Convolution function is defined in "conv"
      !------------------------------------------------------------------------  
      real, intent(out) :: resu
      real, intent(in) :: du,dv
      type(gridding), intent(in) :: conv
    end subroutine uvshort_convol
  end interface
  !
  interface
    subroutine uvshort_doconv (nd,np,visi,jx,jy,jo,we,gwe,&
      &   nc,nx,ny,map,mapx,mapy,sup,cell,maxw,conv)
      use clean_def
      !------------------------------------------------------------------------  
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Convolution of 'map' by function defined by "conv" 
      ! Used for gridding of Single Dish data.
      !------------------------------------------------------------------------  
      integer, intent(in) :: np              ! number of visibilities
      integer, intent(in) :: nd              ! visibility size
      integer, intent(in) :: nc              ! number of channels
      integer, intent(in) :: nx,ny           ! map size
      integer, intent(in) :: jx,jy           ! x coord, y coord location in visi
      real, intent(in) :: we(np)             ! weights
      integer, intent(in) :: jo              ! offset for data in visi
      real, intent(in) :: visi(nd,np)        ! values
      real, intent(out) :: gwe(nx,ny)        ! gridded weights
      real, intent(out) :: map(nc,nx,ny)     ! gridded values
      real, intent(in) :: mapx(nx),mapy(ny)  ! coordinates of grid
      real, intent(in) :: sup(2)             ! support of convolving function in user units
      real, intent(in) :: cell(2)            ! cell size in user units
      real, intent(out) :: maxw              ! maximum weight
      type(gridding), intent(in) :: conv
    end subroutine uvshort_doconv
  end interface
  !
  interface
    subroutine uvshort_dosmoo (raw,we,nc,nx,ny,map,mapx,mapy,sup,cell,conv)
      use clean_def
      !------------------------------------------------------------------------  
      ! Task UV_SHORT
      !   Internal routine
      !
      ! Smooth an input data cube raw in vlm along l and m by convolution
      ! by a gaussian function defined in "conv"
      !------------------------------------------------------------------------  
      integer, intent(in) :: nc,nx,ny          ! map size
      real, intent(in) :: we(nx,ny)            ! weights
      real, intent(in) :: raw(nc,nx,ny)        ! raw map
      real, intent(out) :: map(nc,nx,ny)       ! smoothed map
      real, intent(in) :: mapx(nx),mapy(ny)    ! coordinates of grid
      real, intent(in) :: sup(2)               ! support of convolving function in user units
      real, intent(in) :: cell(2)              ! cell size in user units
      type(gridding), intent(in) :: conv
    end subroutine uvshort_dosmoo
  end interface
  !
  interface
    subroutine uvshort_dowei (visi,nd,np,we,iw)
      !------------------------------------------------------------------------  
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Fill in weights array from the input table
      !------------------------------------------------------------------------  
      integer, intent(in) :: nd       ! Visibility size
      integer, intent(in) :: np       ! Number of visibilities
      integer, intent(in) :: iw       ! Weight column
      real, intent(in) :: visi(nd,np) ! Visibilities
      real, intent(out) :: we(np)     ! Weight values
    end subroutine uvshort_dowei
  end interface
  !
  interface
    subroutine uvshort_findr (nv,nc,ic,xx,xlim,nlim)
      !------------------------------------------------------------------------  
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Find nlim such as
      !   xx(ic,nlim-1) < xlim < xx(ic,nlim)
      ! for input data ordered, retrieved from memory
      ! assumes nlim already preset so that xx(ic,nlim-1) < xlim
      !
      !------------------------------------------------------------------------  
      integer, intent(in) :: nv       ! Number of visibilities
      integer, intent(in) :: nc       ! Size of a visibility
      integer, intent(in) :: ic       ! COlumn to be tested
      integer, intent(inout) :: nlim
      real, intent(in) ::  xx(nc,nv),xlim
    end subroutine uvshort_findr
  end interface
  !
  interface
    subroutine uvshort_finsiz (x,nd,np,ix,iy,we,xmin,xmax,ymin,ymax)
      !------------------------------------------------------------------------  
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Find extrema xmin, xmax in ix column values
      !          and ymin, ymax in iy column values,
      ! in table x(nd,np) for points where weight is not null
      ! taking in account that table x is ordered on iy column values.
      !
      !------------------------------------------------------------------------  
      integer, intent(in) :: nd,np    ! Table size
      integer, intent(in) :: ix,iy    ! X and Y column pointers
      real, intent(in) :: x(nd,np)    ! Table data
      real, intent(in) :: we(np)      ! Weights
      real, intent(out) :: xmin,xmax,ymin,ymax  ! Min-Max
    end subroutine uvshort_finsiz
  end interface
  !
  interface
    subroutine uvshort_dosor (rname,visi,nd,np,we,iy,error)
      use gildas_def
      use gbl_message
      !------------------------------------------------------------------------  
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Output Visi(nd,np) will contain Ycol column values in increasing order
      ! Uses procedure trione
      ! WE is used as work space only
      !------------------------------------------------------------------------  
      character(len=*), intent(in) :: rname
      integer, intent(in) :: nd,np,iy
      real, intent(inout) :: visi(nd,np)   ! Visibilities,we(nd)
      real, intent(inout) :: we(nd)        ! Work space for sorting
      logical, intent(out) :: error
    end subroutine uvshort_dosor
  end interface
  !
  interface
    subroutine uvshort_doapod (xmin,xmax,ymin,ymax,tole,beam,&
         nc,nx,ny,map,raw,mapx,mapy, weight,wmin)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Replace map edges and bad quality values of raw input data cube
      ! with smoothed values contains in mapx data cube.
      ! Map edges corresponding to the part of the map between
      ! max SD observations locations xmin,xmax,ymin,ymax, and map size nx,ny
      ! Bad quality values corresponding to weights < wmin
      !---------------------------------------------------------------------
      integer nc,nx,ny
      real mapx(nx),mapy(ny)
      real map(nc,nx,ny)
      real raw(nc,nx,ny)
      real beam,tole,xmin,xmax,ymin,ymax
      real weight(nx,ny),wmin
    end subroutine uvshort_doapod
  end interface
  !
  interface
    subroutine uvshort_dosdft(beam,diam,f,nx,ny,lmv)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! computes inverse of ft of single-dish beam
      ! (uses a gaussian truncated at dish size)
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: lmv     ! LMV header
      integer, intent(in) ::  nx, ny      ! Problem size
      real, intent(out) ::  f(nx,ny)      ! (real part of the) TF of beam
      real, intent(in) ::  beam(3)        ! Beam size in radian
      real, intent(in) ::  diam           ! Antenna diameter in meter
    end subroutine uvshort_dosdft
  end interface
  !
  interface
    subroutine uvshort_dointft(beam,diam,f,nx,ny,fact,lmv)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! computes ft of single-dish beam
      ! (uses a gaussian truncated at dish size)
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: lmv     ! LMV header
      integer, intent(in) ::  nx, ny      ! Problem size
      real, intent(out) ::  f(nx,ny)      ! (real part of the) TF of beam
      real, intent(in) ::  beam           ! Beam size in radian
      real, intent(in) ::  diam           ! Antenna diameter in meter
      real, intent(in) ::  fact           ! Scale factor
    end subroutine uvshort_dointft
  end interface
  !
  interface
    subroutine uvshort_shift(f,nx,ny,offra,offdec,lmv)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Centered interferometer beam f(nx,ny) of a mosaic field
      ! on is right position : offra and offdec shifted in uv plane
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: lmv
      integer, intent(in) :: nx, ny
      complex, intent(inout) :: f(nx,ny)
      real, intent(in) :: offra, offdec
    end subroutine uvshort_shift
  end interface
  !
  interface
    subroutine uvshort_dotrans (a,b,n,m)
      !
      ! Output table "b" is table "a" transposed in line/column order
      !
      integer, intent(in) :: n,m
      real, intent(in) :: a(n,m)
      real, intent(out) ::  b(m,n)
    end subroutine uvshort_dotrans
  end interface
  !
  interface
    subroutine uvshort_prmult(z,f,nx,ny)
      !
      ! Update z(nx,ny) table with z*f, calculate in uv plane
      ! Used to multiply by interferometer primary beam
      !
      integer, intent(in) :: nx, ny         ! Problem size
      complex, intent(inout) :: z(nx, ny)   ! Complex values
      real, intent(in) :: f(nx,ny)          ! Multiplication function
    end subroutine uvshort_prmult
  end interface
  !
  interface
    subroutine uvshort_uvcount(nx,ny,nvis,dmax,dmin,lmv)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Compute number of visibilities nvis sampled on
      ! a regular grid of steps dx,dy inside the dish defined by diam
      ! nx and ny needed because uvcount is working in Fourier plane
      !---------------------------------------------------------------------
      !
      type(gildas), intent(in) :: lmv   ! Input GILDAS image
      integer, intent(in) :: nx         ! X image size
      integer, intent(in) :: ny         ! Y image size
      integer, intent(out) :: nvis      ! Number of visibilities
      real, intent(in) :: dmax          ! Max baseline
      real, intent(in) :: dmin          ! Min baseline
    end subroutine uvshort_uvcount
  end interface
  !
  interface
    subroutine uvshort_uvtable(nx,ny,nd,nc,v,w,ww,nvis,dmax,dmin,wfactor,factor,lmv)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Tabulate the visibilities
      !---------------------------------------------------------------------
      !
      integer, intent(in) :: nx   ! X Image size
      integer, intent(in) :: ny   ! Y Image size
      integer, intent(in) :: nc   ! Number of channels
      integer, intent(in) :: nd   ! Visibility size
      integer, intent(in) :: nvis ! Number of visibilities
      complex, intent(in) :: v(nx,ny,nc)  ! Gridded visibilities
      complex, intent(in) :: ww(nx,ny)    ! Complex weights
      real, intent(out) :: w(nd,nvis)     ! Resulting visibilities
      real, intent(in) :: dmax    ! Maximum baseline
      real, intent(in) :: dmin    ! Minimum baseline
      real, intent(in) :: wfactor ! Weight factor
      real, intent(in) :: factor  ! Intensity factor
      type(gildas), intent(in) :: lmv     ! Input image
    end subroutine uvshort_uvtable
  end interface
  !
  interface
    subroutine uvshort_dopoint(data,nd,np,xcol,ycol,old,new)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Recompute data(nd,np) xcol and ycol coordinates values
      ! in case of changing reference position a0 d0 from old to new
      !---------------------------------------------------------------------
      !
      integer, intent(in) :: nd         ! Size of a visibility
      integer, intent(in) :: np          ! Number of visibilities
      integer, intent(in) :: xcol        ! RA offset pointer
      integer, intent(in) :: ycol        ! Dec offset pointer
      real, intent(inout) :: data(nd,np) ! Visibilities
      real(8), intent(in) :: old(2)      ! Old RA and Dec center
      real(8), intent(in) :: new(2)      ! New Ra and Dec center
    end subroutine uvshort_dopoint
  end interface
  !
  interface
    subroutine uvfit_comm(line,error)
      use gbl_message
      use clean_default
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command UV_FIT [Func1 ... FuncN] [/QUIET] 
      !     [/WIDGET Nfunc] [/SAVE File] [/RESULT]
      !     [/UVRANGE Min [Max]]
      !---------------------------------------------------------------------
      !
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine uvfit_comm
  end interface
  !
  interface
    subroutine diagonal(m,n,r,c)
      integer :: m                      !
      integer :: n                      !
      real(kind=8) :: r(m,n)                  !
      real(kind=8) :: c(n)                    !
    end subroutine diagonal
  end interface
  !
  interface
    subroutine fitfcn(iflag,m,nvpar,x,f,fjac,ljc)
      use gildas_def
      use uvfit_data
      use gbl_message
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for UV_FIT
      ! FITFCN is called by DNLS1E
      !---------------------------------------------------------------------
      integer, intent(in) :: iflag          ! Code for print out
      integer, intent(in) :: m              ! Number of data points
      integer, intent(in) :: nvpar          ! Number of variables
      real(8), intent(in) :: x(nvpar)       ! Variable parameters
      real(8), intent(out) :: f(m)          ! Function value at each point
      integer, intent(in) :: ljc            ! First dimension of FJAC
      real(8), intent(out) :: fjac(ljc,nvpar) ! Partial derivatives
    end subroutine fitfcn
  end interface
  !
  interface
    subroutine getvisi(n,uvriw,k,u,v,r,i,w)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for UV_FIT
      !     Obtain U, V, and the visibility from the array UVRIW.
      !
      !     N      Integer     Number of visibilities     Input
      !     UVRIW  real(8)      Input array                Input
      !     K      Integer     Visibility number          Input
      !     U      real(8)      U coord.                   Output
      !     V      real(8)      V coord.                   Output
      !     R      real(8)      Real part                  Output
      !     I      real(8)      Imaginary part.            Output
      !     W      real(8)      Weight                     Output
      !---------------------------------------------------------------------
      integer, intent(in) :: n         ! Number of visibilities
      real, intent(in) :: uvriw(5,n)   ! Visibilities
      integer, intent(in) :: k         ! Desired visibility number
      real(8), intent(out) ::  u       ! U coordinate
      real(8), intent(out) ::  v       ! V coordinate
      real(8), intent(out) ::  r       ! Real part
      real(8), intent(out) ::  i       ! Imaginary part
      real(8), intent(out) ::  w       ! Weight
    end subroutine getvisi
  end interface
  !
  interface
    subroutine outpar(id,kfunc,pars,errs)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for UV_FIT
      ! Process fitted parameters to be saved, according to fitted functions
      !---------------------------------------------------------------------
      integer, intent(in) :: id    ! Function code
      integer, intent(in) :: kfunc ! Number of parameters
      real(8), intent(inout) :: pars(kfunc)  ! Parameters
      real(8), intent(inout) :: errs(kfunc)  ! Uncertainties
    end subroutine outpar
  end interface
  !
  interface
    subroutine  uvfit_model(ifunc,kfunc,uu,vv,x,y,dy)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support routine for UV_FIT
      ! Compute a model function.
      !---------------------------------------------------------------------
      integer, intent(in) :: ifunc       ! Fuction type
      integer, intent(in) :: kfunc       ! Number of parameters
      real(8), intent(in) :: uu          ! U coordinate in arcsec^-1
      real(8), intent(in) :: vv          ! V coordinate in arcsec^-1
      real(8), intent(in) :: x(kfunc)    ! Parameters
      real(8), intent(inout) :: y(2)        ! Function (real, imag.)
      real(8), intent(inout) :: dy(2,kfunc) ! Partial derivatives.
    end subroutine uvfit_model
  end interface
  !
  interface
    function z_exp(x)
      !---------------------------------------------------------------------
      ! protect again underflows in exp(x)
      !---------------------------------------------------------------------
      real(8) :: z_exp                   !
      real(8) :: x                       !
    end function z_exp
  end interface
  !
  interface
    subroutine sub_uvfit_results(line,error)
      use uvfit_data
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command 
      !     UV_FIT [Func1 ... FuncN] /RESULT
      ! Only works for
      !  - all functions
      !  - 1 channel only
      ! Test functionality only. May disappear at any time...
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine sub_uvfit_results
  end interface
  !
  interface
    subroutine get_uvfit_model(nvisi,uu,vv,rr,ii,error)
      use uvfit_data
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command 
      !     UV_FIT [Func1 ... FuncN] /RESULT
      !---------------------------------------------------------------------
      integer, intent(in) :: nvisi    ! Number of visibilities
      real, intent(in) :: uu(nvisi)   ! U coordinates
      real, intent(in) :: vv(nvisi)   ! V coordinates
      real, intent(out) :: rr(nvisi)  ! Real part
      real, intent(out) :: ii(nvisi)  ! Imaginary part
      logical, intent(inout) :: error ! Error flag
    end subroutine get_uvfit_model
  end interface
  !
  interface
    subroutine moments_comm(line,error)
      use clean_arrays
      use moment_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command
      !       MOMENT  [SMOOTH]
      !         [/MASK]
      !         [/METHOD Mean Peak Parabolic]
      !         [/RANGE Min Max TYPE] [/THRESHOLD Min]
      ! or
      !         MOMENTS /CUTS Threshold [Regions]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine moments_comm
  end interface
  !
  interface
    subroutine moments_cuts(line,error)
      use clean_arrays
      use moment_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command
      !       MOMENT /CUTS Threshold [Regions]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine moments_cuts
  end interface
  !
  interface
    subroutine sub_moments(horigin,line,error)
      use clean_arrays
      use moment_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command
      !       MOMENT 
      !         [/MASK]
      !         [/METHOD Mean Peak Parabolic]
      !         [/RANGE Min Max TYPE] [/THRESHOLD Min]
      !
      !	A dedicated routine to compute the moment of an input data cube.
      !	Input: a LMV cube
      !	Output: any or all of 4 images
      !           1) the integrated intensity map
      !           2) the Peak brightness
      !           3) The mean velocity
      !           4) The line width
      !	Method:
      !     AREA      Integrated intensity in Velocity range
      !
      !     PEAK      Peak intensity in Velocity range
      !     VELOCITY  Velocity of peak  
      !     WIDTH     2nd moment: line width, using the Velocity
      !               assigned 
      ! The peak intensity is obtained either as a per-channel basis,
      ! or a parabolic fit around the maximum. The Velocity is obtained
      ! in a similar way, or intensity weighted.
      !     
      !       For each spectrum, make a gate in velocity and clip based on
      !       intensity. Sum up weighted velocity and mean intensity.
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: horigin ! Input image, CLEAN or SKY
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine sub_moments
  end interface
  !
  interface
    subroutine set_velo(nv,av,ref,val,inc)
      use gildas_def
      integer(kind=index_length), intent(in)  :: nv
      real(kind=4),               intent(out) :: av(nv)
      real(kind=8),               intent(in)  :: ref,val,inc
    end subroutine set_velo
  end interface
  !
  interface
    subroutine catalog_init(imol,error)
      !
      integer, intent(inout) :: imol
      logical, intent(out) :: error
    end subroutine catalog_init
  end interface
  !
  interface
    subroutine big_stupid_uvsort(uvdata,uvtri,type,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! GILDAS
      !       Sort an input UV table
      !   All 
      !       Add CC code to implement precession
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: type
      character(len=*), intent(in) :: uvdata,uvtri
      logical, intent(out) :: error
    end subroutine big_stupid_uvsort
  end interface
  !
  interface
    subroutine color_comm(line,error)
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command COLOR Span HueMin HueMax
      !
      !   calls "@ p_color.ima" to derive a lut
      !   with unsaturated colours up to Span around 0,
      !   and a Hue range between Min and Max 
      ! 
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine color_comm
  end interface
  !
  interface
    subroutine howto_comm(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   Support for command HOW_TO
      !   HOW_TO Keywords...
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine howto_comm
  end interface
  !
  interface
    subroutine extract_real (c,mx,my,r,nx,ny)  ! checked with cmtore ...
      !-----------------------------------------------------------------
      !     Extract a Real array from a larger Complex array
      !-----------------------------------------------------------------
      integer nx,ny                      ! size of input array
      real r(nx,ny)                      ! input real array
      integer mx,my                      ! size of output array
      complex c(mx,my)                   ! output complex array
    end subroutine extract_real
  end interface
  !
  interface
    subroutine scale_factor(nx,ny,cbig,chigh,rmask,fscale,flux) 
      integer, intent(in) :: nx,ny
      complex, intent(in) :: cbig(nx,ny)
      complex, intent(in) :: chigh(nx,ny)
      real, intent(in) :: rmask(nx,ny)
      real, intent(out) :: fscale, flux
    end subroutine scale_factor
  end interface
  !
  interface
    subroutine polar_order(nstok,astoke,cstoke,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------------------------------
      ! IMAGER
      !   Return the ordering of the Polarization / Stokes states described by array ASTOK
      !
      !   Supported Cases are 
      !   Nstoke = 2    the two parallel hand polarization, any order
      !   Nstoke = 4    all four cross-polarization, any order
      !---------------------------------------------------------------------------------------------
      integer, intent(in) :: nstok
      integer, intent(in) :: astoke(nstok)
      integer, intent(out) :: cstoke(5)
      logical, intent(out) :: error
    end subroutine polar_order
  end interface
  !
  interface
    subroutine init_display 
    end subroutine init_display
  end interface
  !
  interface
    subroutine imager_version
      use gbl_ansicodes
      use gkernel_types
      !
      ! Test last IMAGER usage
      !
    end subroutine imager_version
  end interface
  !
  interface
    subroutine end_message(comm,elapsed,quiet,error)
      use gbl_ansicodes
      use gbl_message
      !
      character(len=*), intent(in) :: comm ! Command line name
      logical, intent(in) :: error         ! Error flag
      logical, intent(in) :: quiet         ! Quiet flag
      real(8), intent(inout) :: elapsed    ! Elapsed time
    end subroutine end_message
  end interface
  !
  interface
    subroutine save_result(error)
      use gildas_def
      use clean_types
      use gbl_message
      logical error
    end subroutine save_result
  end interface
  !
  interface
    subroutine get_sys_code(in,out,sys_code,error)
      use gbl_format
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! Get old-system to new-system conversion code
      ! Trap illegal/impossible conversions.
      !
      ! Caution: ICRS not yet supported here
      !---------------------------------------------------------------------
      type(gildas),    intent(in)  :: in        ! Input header
      type(gildas),    intent(in)  :: out       ! Desired output header
      integer(kind=4), intent(out) :: sys_code  ! Conversion code
      logical,         intent(out) :: error
    end subroutine get_sys_code
  end interface
  !
  interface
    subroutine sub_combine(hx,hy,hz,n,m,dx,dy,dz,code,ay,ty,az,tz,c,error)
      use gkernel_types
      !----------------------------------------------------------------------
      !
      !   IMAGER  Support routine for command
      !
      ! COMBINE Out CODE In1 In2 /factor A1 A2 /THRESHOLD T1 T2 /BLANK Bval
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: code  ! Combination code
      integer(kind=index_length), intent(in) :: n
      integer(kind=index_length), intent(in) :: m
      !
      type(gildas), intent(in) :: hx  ! Output header
      type(gildas), intent(in) :: hy  ! Input Cube header
      type(gildas), intent(in) :: hz  ! Input Cube or Plane header
      !
      real, intent(in) :: dx(n,m)     ! Output data
      real, intent(in) :: dy(n,m)     ! Input data
      real, intent(in) :: dz(n)       ! Input data
      real, intent(in) :: c           ! Offset
      real, intent(in) :: ay          ! Y Factor
      real, intent(in) :: ty          ! Y Threshold
      real, intent(in) :: az          ! Z Factor
      real, intent(in) :: tz          ! Z Threshold
      logical, intent(out) :: error
    end subroutine sub_combine
  end interface
  !
  interface
    subroutine add002(z,y,x,n,m,bz,ez,az,tz,by,ey,ay,ty,bx,c)
      !$ use omp_lib
      use gkernel_types
      !---------------------------------------------------------------------
      ! GDF	Internal routine
      !	  Linear combination of input arrays
      !	X = Ay*Y + Az*Z + C
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: n
      integer(kind=index_length), intent(in) :: m
      real :: z(n)                      ! Plane array
      real :: y(n,m)                    ! Input Cube
      real :: x(n,m)                    ! Output Cube
      real :: bz                        ! Z Blanking value
      real :: ez                        ! Z Tolerance on blanking
      real :: az                        ! Z factor
      real :: tz                        ! Z threshold
      real :: by                        ! Y Blanking value
      real :: ey                        ! Y Tolerance on blanking
      real :: ay                        ! Y factor
      real :: ty                        ! Y Threshold
      real :: bx                        ! X Blanking
      real :: c                         ! Constant Offset
    end subroutine add002
  end interface
  !
  interface
    subroutine div002(z,y,x,n,m,bz,ez,az,tz,by,ey,ay,ty,bx,c)
      !$ use omp_lib
      use gkernel_types
      !---------------------------------------------------------------------
      ! GDF	Internal routine
      !	  Division of 2 input arrays
      !	X = Ay*Y / Az*Z + C
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: n
      integer(kind=index_length), intent(in) :: m
      real :: z(n)                      ! Plane array
      real :: y(n,m)                    ! Input Cube
      real :: x(n,m)                    ! Output Cube
      real :: bz                        ! Z Blanking value
      real :: ez                        ! Z Tolerance on blanking
      real :: az                        ! Z factor
      real :: tz                        ! Z threshold
      real :: by                        ! Y Blanking value
      real :: ey                        ! Y Tolerance on blanking
      real :: ay                        ! Y factor
      real :: ty                        ! Y Threshold
      real :: bx                        ! X Blanking
      real :: c                         ! Constant Offset
    end subroutine div002
  end interface
  !
  interface
    subroutine mul002(z,y,x,n,m,bz,ez,az,tz,by,ey,ay,ty,bx,c)
      !$ use omp_lib
      use gkernel_types
      !---------------------------------------------------------------------
      ! GDF	Internal routine
      !	Multiplication of input arrays
      !	  X = Ay*Y * Az*Z + C
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: n
      integer(kind=index_length), intent(in) :: m
      real :: z(n)                      ! Plane array
      real :: y(n,m)                    ! Input Cube
      real :: x(n,m)                    ! Output Cube
      real :: bz                        ! Z Blanking value
      real :: ez                        ! Z Tolerance on blanking
      real :: az                        ! Z factor
      real :: tz                        ! Z threshold
      real :: by                        ! Y Blanking value
      real :: ey                        ! Y Tolerance on blanking
      real :: ay                        ! Y factor
      real :: ty                        ! Y Threshold
      real :: bx                        ! X Blanking
      real :: c                         ! Constant Offset
    end subroutine mul002
  end interface
  !
  interface
    subroutine opt002(z,y,x,n,m,bz,ez,az,tz,by,ey,ay,ty,bx,c)
      !$ use omp_lib
      use gkernel_types
      !---------------------------------------------------------------------
      ! GDF	Internal routine
      !	Optical depth from input arrays
      !	X = - LOG ( Ay*Y / Az*Z + C )
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: n
      integer(kind=index_length), intent(in) :: m
      real :: z(n)                      ! Plane array
      real :: y(n,m)                    ! Input Cube
      real :: x(n,m)                    ! Output Cube
      real :: bz                        ! Z Blanking value
      real :: ez                        ! Z Tolerance on blanking
      real :: az                        ! Z factor
      real :: tz                        ! Z threshold
      real :: by                        ! Y Blanking value
      real :: ey                        ! Y Tolerance on blanking
      real :: ay                        ! Y factor
      real :: ty                        ! Y Threshold
      real :: bx                        ! X Blanking
      real :: c                         ! Constant Offset
    end subroutine opt002
  end interface
  !
  interface
    subroutine uv_shift_comm(line,comm,error)
      use clean_types
      use clean_arrays
      use gbl_message
      !-------------------------------------------------------------------
      !  IMAGER 
      !   Support routine for command
      !       UV_SHIFT [Xpos Ypos UNIT] [ANGLE [Angle]]  [/FILE FileIn]
      !   Dispatch according to option presenced
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name 
      logical error                         ! Logical error flag
    end subroutine uv_shift_comm
  end interface
  !
  interface
    subroutine explore_comm(line,comm,error)
      use gkernel_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      !
      ! EXPLORE [DataCube] [/ADD Ux Uy [Ibox]] [/NOWAIT]
      !
      !    Call the "explore" scripts that allow to display from a data cube
      ! - a 2-D image (channel or integrated area)
      ! - spectra at selected positions around it
      !
      ! The display is organized in a Keypad layout
      !     7 8 9
      !     4 5 6
      !     1 2 3
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(out) :: error
    end subroutine explore_comm
  end interface
  !
  interface
    subroutine sub_cct_collect(ncx,ncy,ncz,ny,nz,x,y,z)
      use gildas_def
      integer(kind=address_length), intent(in) :: ncx, ncy, ncz
      integer(kind=address_length), intent(in) :: ny
      integer(kind=address_length), intent(in) :: nz
      !
      real, intent(in) :: y(3,ncy,ny)
      real, intent(in) :: z(3,ncz,nz)
      real, intent(out) :: x(3,ncx,ny+nz)
    end subroutine sub_cct_collect
  end interface
  !
  interface
    subroutine uvmap_headers(huv,nx,ny,nb,ns,map,mcol,hbeam,hdirty,error)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !
      !   Define the Beam and Dirty image headers 
      !---------------------------------------------------------------------
      integer, intent(in) :: nx,ny   ! Map size
      integer, intent(in) :: ns      ! Number of channels per single beam
      integer, intent(in) :: nb      ! Number of beams
      integer, intent(in) :: mcol(2) ! Channel range
      type (uvmap_par), intent(in) :: map    ! Mapping parameters
      type(gildas), intent(in) :: huv        ! UV headers
      type(gildas), intent(inout) :: hbeam   ! Beam headers
      type(gildas), intent(inout) :: hdirty  ! Dirty image header
      logical, intent(inout) :: error
    end subroutine uvmap_headers
  end interface
  !
  interface
    function mth_bessj1 (x)
      !-------------------------------------------------------------------
      ! Compute Bessel function J1
      !-------------------------------------------------------------------
      real*8 :: mth_bessj1            !
      real*8 :: x                     !
    end function mth_bessj1
  end interface
  !
  interface
    subroutine imager_pack_set(pack)
      use gpack_def
      !
      type(gpack_info_t), intent(out) :: pack
    end subroutine imager_pack_set
  end interface
  !
  interface
    subroutine imager_pack_init(gpack_id,error)
      use sic_def
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine imager_pack_init
  end interface
  !
  interface
    subroutine imager_pack_on_exit(error)
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      logical :: error
    end subroutine imager_pack_on_exit
  end interface
  !
  interface
    subroutine omp_setup
      use omp_control
      !$ use omp_lib
      !
    end subroutine omp_setup
  end interface
  !
  interface
    subroutine astro_message(seve,comm,mess)
      integer :: seve
      character(len=*) :: comm
      character(len=*) :: mess
    end subroutine astro_message
  end interface
  !
end module imager_interfaces_none
