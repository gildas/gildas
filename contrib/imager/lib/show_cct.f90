subroutine show_sources_comm(line,error)
  use image_def
  use clean_arrays
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => show_sources_comm
!-----------------------------------------------------------------------
! @ private
!
! IMAGER
!   Support for command SHOW SOURCES  
!
!   SHOW the position of Clean Components and their sizes proportional
!   to the component Flux. We  must compact the CCT list before
!   and Transpose it
!-----------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(inout) :: error
  !
  character(len=*), parameter :: rname='SHOW SOURCES'
  type(gildas) :: occt
  type(gildas), save :: htcc
  real, allocatable :: ccou(:,:,:)
  real, allocatable, save :: dtcc(:,:,:)
  !
  character(len=4) :: code
  !! integer(kind=index_length) :: nfirst,nsecon,nmiddl,nelems,nlast,iblock(5)
  integer :: ier
  integer :: maxic, mic
  integer :: i,j,k
  !
  if (hcct%loca%size.eq.0) then
    call map_message(seve%e,rname,'No Clean Component Table')
    error = .true.
    return
  endif
  call sic_delvariable('TCC',.false.,error)
  if (allocated(dtcc)) deallocate(dtcc)
  !
  call gildas_null (occt)
  call gildas_null (htcc)
  call gdf_copy_header(hcct,occt, error) ! Copy headers
  !
  ! We may truncate to a smaller number by parsing the command line
  maxic = hcct%gil%dim(3)   
  call uv_clean_size(hcct,dcct,mic)
  if (mic.eq.0) then
    call map_message(seve%w,rname,'No valid Clean Component')
    error = .true.
    return
  endif
  mic = min(mic,maxic)
  occt%gil%dim(3) = mic
  allocate(ccou(occt%gil%dim(1),occt%gil%dim(2),occt%gil%dim(3)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%i,rname,'Component Allocation error')
    error = .true.
    return
  endif
  call uv_compact_clean(hcct,dcct,occt,ccou, mic)
  !
  ! Transpose it
  code = '312'
  call gdf_transpose_header(occt,htcc,code,error)
  ! Reset the projection, and Type of First Axis so that
  ! Absolute coordinates can be used (even though it is not an Image)
  htcc%gil%xaxi = 1
  htcc%gil%yaxi = 2
  htcc%char%code(1) = 'RA'
  htcc%char%code(2) = 'DEC'  
  ! Determine chunk sizes from code and dimensions. Unclear here
  ! because we are truncating the CCT Table
  if (error) return
  !
  ! Make sure this is a 3-D array, even if last dimension is 1
  htcc%gil%dim(1) = mic
  htcc%gil%ndim = 3
  allocate(dtcc(htcc%gil%dim(1),htcc%gil%dim(2),htcc%gil%dim(3)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Transpose (simply, this should not be big arrays)
  do k=1,htcc%gil%dim(3)
    do j=1,htcc%gil%dim(2)
      do i=1,htcc%gil%dim(1)
        dtcc(i,j,k) = ccou(j,k,i)
      enddo
    enddo
  enddo
!! Big arrays solution, but with some issue about the CCT Table size ?
!! call transpose_getblock(occt%gil%dim,gdf_maxdims,code,iblock,error)
!!  nelems = iblock(1)
!!  nfirst = iblock(2)
!!  nmiddl = iblock(3)
!!  nsecon = iblock(4)
!!  nlast  = iblock(5)
!!  Print *,'Transposing ',iblock
!!  call trans4all(dtcc,ccou,nelems,nfirst,nmiddl,nsecon,nlast)
  !
  ! Make it accessible to Scripts as a SIC Image
  call sic_mapgildas ('TCC',htcc,error,dtcc)
  !
  ! Now display it
  call exec_program('@ p_show_tcc')
  ! 
end subroutine show_sources_comm
