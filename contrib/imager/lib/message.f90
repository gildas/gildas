!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage MAPPING messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module map_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: map_message_id = gpack_global_id  ! Default value for startup message
  !
end module map_message_private
!
subroutine map_message_set_id(id)
  use map_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  map_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',map_message_id
  call gmessage_write(map_message_id,seve%d,'map_message_set_id',mess)
  !
end subroutine map_message_set_id
!
subroutine map_message(mkind,procname,message,colour)
  use gbl_ansicodes
  use gkernel_interfaces
  use map_message_private
  !---------------------------------------------------------------------
  ! @ public-mandatory
  !
  ! IMAGER
  !   Messaging facility for the current library. Calls the low-level
  !   (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  integer, intent(in), optional :: colour   ! Colour of message 
  !
  if (present(colour)) then
    call message_colour(colour)
    call gmessage_write(map_message_id,mkind,procname,message)
    write(*,'(A)',advance='NO') c_clear
  else
    call gmessage_write(map_message_id,mkind,procname,message)
  endif
  !
end subroutine map_message
!
subroutine map_iostat(mkind,procname,ier)
  use gkernel_interfaces
  use map_message_private
  !---------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER
  !   Output system message corresponding to IO error code IER, and
  !   using the Message facility
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message severity
  character(len=*), intent(in) :: procname  ! Calling routine name
  integer,          intent(in) :: ier       ! IO error number
  ! Local
  character(len=message_length) :: msg
  !
  if (ier.eq.0) return
  call gag_iostat(msg,ier)
  call gmessage_write(map_message_id,mkind,procname,msg)
  !
end subroutine map_iostat
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine message_colour(colour)
  use gbl_ansicodes
  !---------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER
  !   Change the colour
  !---------------------------------------------------------------------
  integer, intent(in) :: colour   !
  !
  character(len=12) :: chain
  !
  select case (colour)
    case(0)
      chain = c_bold//c_black
    case(1)
      chain = c_bold//c_red
    case(2)
      chain = c_bold//c_green
    case(3)
      chain = c_bold//c_blue
    case(4)
      chain = c_bold//c_cyan 
    case(5)
      chain = c_bold//c_yellow
    case(6)
      chain = c_bold//c_magenta
    case(7)
      chain = c_bold//c_white
    case default
      chain = c_clear
  end select
  write(*,'(A)',advance='NO') trim(chain)
  !
end subroutine message_colour
!
subroutine message_attribute(cattr)
  use gbl_ansicodes
  !
  character(len=*), intent(in) :: cattr
  !
  !
  write(*,'(A)',advance='NO') c_start//cattr//c_end
end subroutine message_attribute
