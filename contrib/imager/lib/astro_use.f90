module ast_constant
  real(kind=8),  parameter :: pi=3.141592653589793d0
  real(kind=8),  parameter ::  halfpi=pi/2d0
  real(kind=8),  parameter :: twopi = pi*2d0
  real(kind=8),  parameter :: j2000 = 2451545.0d0
  real(kind=8),  parameter :: light = 299792.458d0   ! km/s
  real(kind=8),  parameter :: radius=6367.435d0      ! earth radius in kilometers
  real(kind=8), parameter :: au_m=149597870700d0 !astronomical unit in m
  real(kind=8), parameter :: astro_nullr8=-1.2345d36
  real(kind=4), parameter :: astro_nullr4=-1.2345e36
  real(kind=8), parameter :: astro_nulli8=-12345678
  real(kind=4), parameter :: astro_nulli4=-12345678
  character(len=4), parameter :: astro_nulla='NULL'
end module ast_constant
!
module ast_params
  integer(kind=8), parameter :: sourcename_length=128
  !
end module ast_params
!
module ast_astro
  use gildas_def
  use ast_params
  !
  logical :: library_mode  ! Astro loaded in Library or Interactive mode?
  !
  ! astro.inc
  real(kind=8) :: jnow_utc              ! date julienne utc
  real(kind=8) :: jnow_ut1              ! date julienne ut1
  real(kind=8) :: jnow_tdt              ! date julienne tdt
  real(kind=8) :: d_ut1                 ! ut1 - utc (seconds)
  real(kind=8) :: d_tdt                 ! tdt - utc (seconds)
  real(kind=8) :: lst                   ! local "true" sidereal time
  real(kind=8) :: lonlat(2)             ! e-longitude, latitude in degrees
  real(kind=8) :: altitude              ! altitude in kilometers
  real(kind=8) :: xsun_0(3)             ! earth_sun vector, in r0 , in km
  real(kind=8) :: xsun_2(3)             ! sun az-el at current time
  real(kind=8) :: xg_0(3)               ! earth_g vector, in r0 , in km
  real(kind=8) :: vg_0(3)               ! earth_g vector derivative, in r0 , in km/s
  real(kind=8) :: trfm_30(9)            ! matrix r0 to r3
  real(kind=8) :: trfm_20(9)            ! matrix r0 to r2
  real(kind=8) :: trfm_23(9)            ! matrix r3 to r2
  real(kind=8) :: trfm_43(9)            ! matrix r3 to r4
  real(kind=8) :: trfm_05(9)            ! matrix r5 (day ecliptic) to r0 (j2000 ecliptic)
  real(kind=8) :: s_1(2)                ! source reference coords.
  real(kind=8) :: azimuth, elevation    ! horizontal coords
  real(kind=8) :: ra, dec               ! present_day equatorial coords
  real(kind=8) :: parallactic_angle     ! parallactic angle
  real(kind=8) :: azimuth_old, elevation_old  ! horizontal coords
  real(kind=8) :: ra_old, dec_old       ! present_day equatorial coords
  real(kind=8) :: slimit                ! sun avoidance radius
  real(kind=8) :: fshift                ! frequency ratio due to doppler effect
  real(kind=8) :: freq                  ! frequency (rest) in ghz
  real(kind=8) :: primbeam              ! telescope primary beam width in radian (value used in computations)
  real(kind=4) :: z_axis
  logical :: projection
  integer(kind=4) :: name_out
  character(len=sourcename_length) :: source_name
  ! Possible values for sourcetype
  integer(kind=4), parameter :: msoukind=4
  integer(kind=4), parameter :: soukind_none=1      ! no source defined
  integer(kind=4), parameter :: soukind_full=2      ! source defined, full computations
  integer(kind=4), parameter :: soukind_vlsr=3      ! use only LSR
  integer(kind=4), parameter :: soukind_red=4      ! use only Redshift
  character(len=12), parameter ::soukinds(msoukind)= (/ 'NO SOURCE   ','ASTRO SOURCE', &
                                                        'LSR         ','REDSHIFT    ' /)
  integer(kind=4) :: soukind=soukind_none
!
! Support for ASTRO%SOURCE% variable
  character(len=60) :: source_alpha, source_delta
  real(kind=8):: source_az, source_el
  real(kind=8):: source_ra, source_dec
  character(len=sourcename_length) :: astro_source_name
  real(kind=8) :: source_vlsr, source_dop, source_vshift, source_lsr
  real(kind=8) :: source_redshift
! Support for ASTRO%SOURCE%IN variable
  character(len=2)  :: source_incoord
  character(len=2)  :: source_invtype
  character(len=14) :: source_inbetasexa,source_inlambdasexa
  real(kind=4)      :: source_ineq
  real(kind=8)      :: source_invelocity,source_inredshift     
! To save the current astro source      
  type :: astro_source_mem
    integer(kind=4)   :: soukind
    character(len=sourcename_length) :: name
    character(len=2)  :: coord
    real(kind=4)   :: equinox
    real(kind=8)      :: lambda  ! radians
    real(kind=8)      :: beta  ! radians
    character(len=2)  :: vtype
    real(kind=8) :: velocity
    real(kind=8) :: redshift
  end type astro_source_mem

!       Planet stuff
  real(kind=4) :: planet_tmb            ! planet main beam brightness
  real(kind=4) :: planet_flux           ! planet flux
  real(kind=4) :: planet_size(3)        ! planet size
!
  character(len=2) :: z_axis_type
  character(len=12) :: frame           ! 'horizontal','equatorial' display
  character(len=filename_length) :: catalog_name(2) ! for sources
  character(len=1) :: azref
!       character(len=2) :: vtype           ! velocity system ('ls', 'ea', 'he','re')
  character(len=16) :: obsname         ! observatory name
!
! For plots
  integer(kind=4) :: msplot, nsplot
  parameter (msplot=2000)
  real(kind=8) :: xsplot(msplot), ysplot(msplot), zsplot(msplot)
  character(len=sourcename_length) :: splot(msplot)
  character(len=80) :: ccplot(msplot)
  ! For UV tracks
  integer(kind=4), parameter :: msize=20  ! max number of differnt sizes for an observatory
  integer(kind=4), parameter :: msta=100  ! max number of selected stations
  !
  type :: uv_track_in_t
    ! Describe input of uvtrack command
    logical :: three_digit_name_compatible
    logical :: do_frame,do_hour,do_table,do_ho,do_int,do_stations,do_size,do_offset,do_weight
    logical :: do_all
    real(kind=4) :: in_h1,in_h2 !  /HOUR_ANGLE option user input
    real(kind=4) :: h1,h2 ! /HOUR_ANGLE option actually used value
    real(kind=4) :: horizon !  /HOUR_ANGLE option
    real(kind=4) :: integ_time ! /INT option
    real(kind=4) :: user_array ! if argument of /FRAME option
    ! For / WEight
    integer(kind=4) :: wkey
    ! For /SIZE
    integer(kind=4) :: nsize
    real(kind=4) :: sizes(msize)      ! size
    integer(kind=4) :: nant_size(msize)  ! number of antennas of this size
    real(kind=4) :: offset            ! option /OFFSET (delay line)
    ! For the stations
  end type uv_track_in_t
  !
  type:: uv_track_station_t
    character(len=12) :: name
    real(kind=4) :: x,y,z ! station coordinates
    real(kind=4) :: t ! alma case
    real(kind=4) :: asize ! antenna size
    logical :: found
  end type uv_track_station_t
  !
  type :: uv_track_angles_t
    real(kind=4) :: sd ! sine of declination
    real(kind=4) :: cd ! cosine of dec
    real(kind=4) :: sp ! sine of lat
    real(kind=4) :: cp ! cosine of lat
    real(kind=4) :: zsh ! depends on h
    real(kind=4) :: zch ! depends on h
  end type uv_track_angles_t
  !
  type :: uv_track_t
    type(uv_track_in_t) :: in
    type(uv_track_angles_t) :: ang
    integer(kind=4)   :: n_stations
    type(uv_track_station_t) :: stations(msta)
  end type uv_track_t
  !
end module ast_astro
