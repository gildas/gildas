subroutine explore_comm(line,comm,error)
  use gkernel_interfaces
  use gkernel_types
  use clean_default
  use imager_interfaces, only : map_message
  use gbl_message
  !---------------------------------------------------------------------
  !
  ! EXPLORE [DataCube] [/ADD Ux Uy [Ibox]] [/NOWAIT]
  !
  !    Call the "explore" scripts that allow to display from a data cube
  ! - a 2-D image (channel or integrated area)
  ! - spectra at selected positions around it
  !
  ! The display is organized in a Keypad layout
  !     7 8 9
  !     4 5 6
  !     1 2 3
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  character(len=*), intent(in) :: comm
  logical, intent(out) :: error
  !
  integer, parameter :: o_add=1
  integer, parameter :: o_nowait=2
  integer :: o_args
  !
  logical :: found
  character(len=24) :: cx,cy,cb
  character(len=256) :: chain
  character(len=256) :: argu
  character(len=256), save :: file
  type(sic_descriptor_t) :: descr
  integer :: nx,ny,nb
  integer, save :: nc=0
  !
  cb = ' '
  call sic_ch(line,0,1,cb,nb,.false.,error)
  if (cb.eq.'?') then
    call exec_program('@ i_explore')
    return
  endif
  !
  ! /ADD option
  if (sic_present(o_add,0)) then
    if (nc.eq.0) then
      call map_message(seve%e,comm,'No map to explore, use "EXPLORE Argument" first')
      error = .true.
      return
    endif
    !
    call sic_ch(line,o_add,1,cx,nx,.true.,error)
    if (error) return
    call sic_ch(line,o_add,2,cy,ny,.true.,error)
    if (error) return
    call sic_ch(line,o_add,3,cb,nb,.false.,error)
    if (error) return
    nb = nb+1
    call exec_program('@ add_spectrum '//cx(1:nx)//' '//cy(1:ny)//' '//cb(1:nb))
    return
  endif
  !
  ! Fall back to last Displayed item if no Argument is given
  o_args = 0
  if (.not.sic_present(o_args,1)) then
    if (len_trim(last_shown).eq.0) then
      call map_message(seve%e,comm,'No data to re-EXPLORE yet')
      error = .true.
      return
    endif
    file = last_shown
    nc = len_trim(file)
  else
    !
    call sic_ch(line,o_args,1,argu,nc,.true.,error)
    if (error) return
    !
    call sic_descriptor (argu,descr,found)    ! Checked
    if (.not.found) then
      call sic_ch(line,0,1,argu,nc,.true.,error)
      found = sic_findfile(argu,file,' ',' ')
      if (found) then
        chain = 'LOAD '//trim(file)
        call exec_command(chain,error)
        if (error) return
        file = 'DATA'
        nc = 4
        call check_view(1,file) ! Force VIEW re-computation
      else
        call map_message(seve%e,comm, &
        'Cannot '//trim(comm)//' '//trim(file)//' (no such file or SIC variable)')
        error = .true.
        nc = 0
        return
      endif
    else
      file = argu
    endif
  endif
   !
  if (sic_present(o_nowait,0)) then
    call exec_program('@ p_explore '//file(1:nc)//' nowait')
  else
    call exec_program('@ p_explore '//file(1:nc))
  endif
  last_shown = file(1:nc)
  call sic_let_logi('VIEW%FIRST',.true.,error)
  !
end subroutine explore_comm
