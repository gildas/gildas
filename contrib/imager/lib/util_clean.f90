subroutine check_area(method,head,quiet)
  use clean_def
  use clean_default
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message, check_box
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Check search area for Cleaning, and guess the maximum number of 
  !   iterations
  !---------------------------------------------------------------------
  type (gildas), intent(in) :: head           ! Image header
  type (clean_par), intent(inout) :: method   ! Cleaning parameters  
  logical, intent(in) :: quiet                ! Printout level
  !
  real ares,hmax
  integer max_iter0, nx, ny
  character(len=message_length) :: chain
  character(len=5) :: rname = 'CLEAN'
  character(len=16) :: citer = 'ITERATIONS'
  character(len=16) :: ccomp = 'COMPONENTS'
  character(len=16) :: cnois = 'NOISE'
  character(len=16) :: csigm = 'SIGMA'
  character(len=16) :: cstop = 'STOP'
  character(len=16) :: ctype
  integer :: ier, nc, kc
  real :: scale, factor, anoise
  !
  ! BLC & TRC
  nx = head%gil%dim(1)
  ny = head%gil%dim(2)
  call check_box(nx,ny,method%blc,method%trc)
  !
  ! Support for Clean
  method%box = (/method%blc(1),method%blc(2),   &
       &    method%trc(1),method%trc(2)/)
  !
  ier = -1
  hmax = max(abs(head%gil%rmax),abs(head%gil%rmin))
  anoise = head%gil%noise
  if (head%gil%rms.gt.0) then
    if (head%gil%rms.lt.head%gil%noise) then
      call map_message(seve%w,rname, &
      & 'RMS noise of DIRTY image is lower than theoretical noise')
      anoise = head%gil%rms
    endif
  endif
  if (clean_stop(1).ne." ") then
    read(clean_stop(1),*,iostat=ier) factor
  endif  
  !
  if (ier.eq.0) then
    ! Default stopping always based on Noise
    ares = anoise
    !
    ctype = clean_stop(2)
    call sic_upper(ctype)
    nc = len_trim(ctype)
    if ((nc.eq.1).and.(ctype.eq.'S')) then
      if (.not.quiet) call map_message(seve%w,rname, &
        & 'CLEAN_STOP[2] is ambiguous, using SIGMA')
      clean_stop(2) = 'Sigma'
      ctype = 'SIGMA'
      nc = 5
    endif
    !
    scale = 0
    if ((ctype.eq.cnois(1:nc)) .or. &
      & (ctype.eq.csigm(1:nc)) ) then
      scale = anoise 
      ! ARES and FRES have no meaning then
      user_method%ares = 0
      user_method%fres = 0
    else if (ctype.eq.'%') then
      scale = hmax * 0.01
      user_method%fres = factor*0.01  ! Make it remanent
    else if (ctype(nc-1:nc).eq.'JY') then
      kc = nc-2
      if (kc.gt.0) then
        select case (ctype(1:kc))
        case ('MILLI','M')
          scale = 1E-3
        case ('MICRO')
          scale = 1E-6
        case ('KILO','K')
          scale = 1E3
        case ('MEGA')
          scale = 1E6
        case default
          scale = 0
        end select
      else
        scale = 1.0
      endif
    else if ( (ctype.eq.citer(1:nc)) .or. &
      & (ctype.eq.ccomp(1:nc)) ) then
      method%m_iter = max(0,nint(factor))
      scale = anoise
      if (method%m_iter.ne.0) then
        factor = 0.
        user_method%m_iter = method%m_iter    ! Make it remanent
      else
        factor = 1.
      endif
    else if (ctype.eq.cstop(1:nc)) then
      method%converge = max(0,nint(factor))
      scale = anoise
      if (method%converge.ne.0) then
        factor = 0.
      else
        factor = 1.
      endif
      user_method%converge = method%converge  ! Make it remanent
      clean_stop = " "                        ! Reset CLEAN_STOP string
    endif
    !
    if (scale.ne.0) then
      ares = factor*scale
    else
      ier = 1
    endif
  endif
  !
  if (ier.gt.0) then
    if (.not.quiet) call map_message(seve%w,rname, &
    & 'Invalid CLEAN_STOP, using ARES, FRES or NITER')
  endif
  if (ier.ne.0) then
    ares = method%ares
    if (ares.eq.0) ares = method%fres*hmax
    if (ares.eq.0) ares = anoise
  else if (.not.quiet) then
    call map_message(seve%i,rname,'Using CLEAN_STOP '// &
      & trim(clean_stop(1))//' '//trim(clean_stop(2)))
  endif
  !
  if (.not.quiet) then
    write(chain,'(A,1pg11.3)') 'Cleaning down to ',ares
    call map_message(seve%i,rname,chain)
  endif
  !
  if (hmax.lt.ares) then
    max_iter0 = 0
    if (.not.quiet) then
      write(chain,'(A,1pg11.3,A)') 'Peak flux ',hmax,' is smaller, no Cleaning needed'
      call map_message(seve%w,rname,chain)
    endif
  else if (ares.ne.0) then
    max_iter0 = log(ares/hmax)/log(1.0-method%gain)*  &
         &(method%trc(1)-method%blc(1)+1)*(method%trc(2)-method%blc(2)+1)/50.0
    if (max_iter0.lt.0) then
      chain = 'Unable to guess Iteration number - set to 100 '
      call map_message(seve%w,rname,chain)
      write(chain,*) 'CLEAN_Ares ',ares,'  Hmax',hmax,'  CLEAN_Gain ',method%gain
      call map_message(seve%w,rname,chain)
      max_iter0 = 100
    endif
    !
    if (.not.quiet) then
      if (ares.lt.head%gil%noise) then
        write(chain,'(A,1pg11.3)') 'Expected noise is larger... ',head%gil%noise
        call map_message(seve%w,rname,chain)
      endif
      write(chain,'(A,i12,a)') 'May need ',max_iter0,' iterations'
      call map_message(seve%i,rname,chain)
    endif
  endif
  !
  if (method%m_iter.eq.0) then
    method%m_iter = max_iter0
    method%ares = ares
  endif
end subroutine check_area
!
subroutine beam_plane(method,hbeam,hdirty)
  use clean_def
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message, beam_for_channel
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Define beam plane and check if fit is required
  !---------------------------------------------------------------------
  type (clean_par), intent(inout) :: method   ! Clean method parameters
  type (gildas), intent(in) :: hbeam          ! Beam header
  type (gildas), intent(in) :: hdirty         ! Used to figure out which plane
  !
  integer :: ibeam
  !
  ! Check if beam parameters must be recomputed
  if (hbeam%gil%dim(4).le.1) then
    method%ibeam = 1
  else
    ! Cannot set method%ibeam  directly ?
    ibeam = beam_for_channel(method%iplane,hdirty,hbeam)
    !!call map_message(seve%w,'CLEAN','Recomputing the beam parameters')
    method%ibeam = ibeam
    !! Print *,'IBEAM ',method%ibeam,'... '
  endif
end subroutine beam_plane
!
function beam_for_channel(iplane,hdirty,hbeam)
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  !
  ! IMAGER
  !   Find out the appropriate beam channel for an image channel
  !---------------------------------------------------------------------  
  integer :: beam_for_channel !! intent(out)  ! Beam channel
  type(gildas), intent(in) :: hdirty          ! Dirty header
  type(gildas), intent(in) :: hbeam           ! Beam header
  integer, intent(in) :: iplane               ! Dirty channel
  !
  real(8) :: i_freq
  integer ibeam, nstep
  integer v_axis
  !
  if (hbeam%gil%faxi.eq.0) then
    v_axis = 3
  else
    v_axis =hbeam%gil%faxi  ! Which axis is the Velocity Axis ?
  endif
  !
  if (hbeam%gil%dim(v_axis).le.1) then
    beam_for_channel = 1
  else
    nstep = hdirty%gil%dim(3) / hbeam%gil%dim(v_axis)
    if (hbeam%gil%dim(v_axis)*nstep.ne.hdirty%gil%dim(3)) nstep = nstep+1
    ibeam = (iplane+nstep-1)/nstep
!    Print *,'Approximate Beam channel ',ibeam,' for ',iplane,' step ',nstep
    ! This may be the better way of doing it:
    i_freq = (iplane-hdirty%gil%ref(3))*hdirty%gil%fres + hdirty%gil%freq
    ibeam = nint((i_freq-hbeam%gil%freq)/hbeam%gil%fres + hbeam%gil%ref(v_axis))
!    Print *,'Nearest Beam channel ',ibeam,' for ',iplane
    ibeam = min(max(1,ibeam),hbeam%gil%dim(v_axis)) ! Just  in case
    !
    beam_for_channel = ibeam
  endif
end function beam_for_channel
!
subroutine clean_make90(method,hclean,clean,tcc)
  use clean_def
  use image_def
  use gkernel_interfaces
  use imager_interfaces, only : mulgau
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Build clean image from Component List
  !---------------------------------------------------------------------
  type (clean_par), intent(inout) :: method    ! Clean method parameters
  type (gildas), intent(inout) :: hclean       ! Clean header
  real, intent(inout) :: clean(hclean%gil%dim(1),hclean%gil%dim(2))
  type (cct_par), intent(in) :: tcc(method%n_iter)  ! Clean components
  !
  real(8), parameter :: pi=3.141592653589793d0
  !
  integer nx,ny,nc,ix,iy,ic,imax,ndim,dim(2),ier
  real xinc, yinc,rmax,flux,r,fact
  real, allocatable :: wfft(:)
  complex, allocatable :: ft(:,:)
  !
  nc = method%n_iter
  nx = hclean%gil%dim(1)
  ny = hclean%gil%dim(2)
  allocate (wfft(2*max(nx,ny)),ft(nx,ny),stat=ier)
  if (ier.ne.0) then
    Print *,'F-IMAGER,  Memory allocation error in CLEAN_MAKE90'
    STOP
  endif
  xinc = hclean%gil%convert(3,1)
  yinc = hclean%gil%convert(3,2)
  !
  if (method%method.eq.'SDI'.or.method%method.eq.'MULTI') then
    ft(:,:) = cmplx(clean,0.0)
  else
    !
    ! Check minimum flux
    ft = 0.0
    rmax = abs(tcc(1)%value)
    imax = 1
    do ic=1,nc
      r = abs(tcc(ic)%value)
      if (r.lt.rmax) then
        imax = ic
        rmax = r
      endif
    enddo
    if (imax.lt.nc-10) then
    !            WRITE(CHAIN,*)
    !     $      'I-CLEAN,  Truncating CLEAN component to '
    !     $      ,IMAX,' / ',NC
    !            CALL GAGOUT(CHAIN)
    !            NC = IMAX
    endif
    !
    ! Convolve source list into residual map ---
    if (method%bshift(3).eq.0) then
      do ic=1,nc
        ix = tcc(ic)%ix
        iy = tcc(ic)%iy
        ft(ix,iy) = ft(ix,iy) + tcc(ic)%value
      enddo
    else
      do ic=1,nc
        flux = 0.5*tcc(ic)%value
        ix = tcc(ic)%ix
        iy = tcc(ic)%iy
        ft(ix,iy) = ft(ix,iy) + flux
        ix = ix+method%bshift(1)
        iy = iy+method%bshift(2)
        ft(ix,iy) = ft(ix,iy) + flux
      enddo
    endif
  endif
  !
  ndim = 2
  dim(1) = nx
  dim(2) = ny
  !D Print *,'Calling FOURT'
  call fourt(ft,dim,ndim,-1,0,wfft)
  !
  ! Beam Area = PI * BMAJ * BMIN / (4 * LOG(2) ) for flux density
  ! normalisation
  fact = method%major*method%minor*pi/(4.0*log(2.0))   &
       &    /abs(xinc*yinc)/(nx*ny)
  !D Print *,'Calling MULGAU ',method%major, method%minor, method%angle
  call mulgau(ft,nx,ny,   &
       &    method%major,method%minor,method%angle,   &
       &    fact,xinc,yinc,-1)
  !D Print *,'Calling FOURT'
  call fourt(ft,dim,ndim,1,1,wfft)
  !
  ! Extract Real part
  !D Print *,'Done CLEAN'
  clean = real(ft)
  deallocate (wfft,ft)
end subroutine clean_make90
!
!
subroutine plunge_real (r,nx,ny,c,mx,my)
  !------------------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING   Support for UV_RESTORE and others
  !     Plunge a Real array into a Complex array
  !-----------------------------------------------------------------
  integer, intent(in)  :: nx,ny              ! Size of input array
  real, intent(in)     :: r(nx,ny)           ! Input real array
  integer, intent(in)  :: mx,my              ! Size of output array
  complex, intent(out) :: c(mx,my)           ! Output complex array
  !
  integer kx,ky,lx,ly
  integer i,j
  !
  kx = nx/2+1
  lx = mx/2+1
  ky = ny/2+1
  ly = my/2+1
  !
  c = 0.0
  do j=1,ny
    do i=1,nx
      c(i-kx+lx,j-ky+ly) = cmplx(r(i,j),0.0)
    enddo
  enddo
end subroutine plunge_real
!
subroutine recent(nx,ny,z)
  !------------------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING   Support for UV_RESTORE and others
  !   Recenters the Fourier Transform, for easier display. The present version
  !   will only work for even dimensions.
  !------------------------------------------------------------------------------
  integer, intent(in) :: nx
  integer, intent(in) :: ny
  complex, intent(inout) :: z(nx,ny)
  !
  complex :: tmp
  integer :: i, j
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i+nx/2,j+ny/2)
      z(i+nx/2,j+ny/2) = z(i,j)
      z(i,j) = tmp
    enddo
  enddo
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i,j+ny/2)
      z(i,j+ny/2) = z(i+nx/2,j)
      z(i+nx/2,j) = tmp
    enddo
  enddo
  !
  do j=1,ny
    do i=1,nx
      if (mod(i+j,2).ne.0) then
        z(i,j) = -z(i,j)
      endif
    enddo
  enddo
end subroutine recent
!
