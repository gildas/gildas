subroutine gather_self(line,comm,error)
  use gkernel_interfaces
  use gkernel_types
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support for command
  !     COLLECT FileFilter MergedFile [ReferenceAntenna] [/FLAG Threshold] 
  !
  !   Average Antenna-Based self-calibration solutions from different 
  ! frequencies into a  single one, assuming the self-calibration "phases"
  ! are actually Pathlengths (delays).
  !   Flag unstable solutions if more than 1 iteration is present.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  character(len=*), intent(in) :: comm
  logical, intent(inout) :: error
  !
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  logical :: err
  !
  character(len=filename_length) :: dirstring, filestring
  character(len=filename_length), allocatable :: filename(:)
  integer :: nfile  ! Number of files to gather
  type(gildas) :: head  ! Header of 1st file
  type(gildas) :: htmp  ! Header of any other file
  type(gildas) :: hres  ! Result file 
  !
  real, allocatable :: freqs(:), phase(:), dphase(:), snr(:), ampli(:), &
    & weight(:), pf(:), work(:), datas(:,:,:,:)
  real :: pp, ps, dp
  integer :: ier, n, i, j    
  integer, allocatable :: aref(:)
  integer :: ntime, nante, nloop
  integer :: kp, ks, kr, ka
  integer :: iant, iloop, itime, iampli, iphase, jref
  !
  real :: reject 
  integer :: ireject
  character(len=80) :: mess
  !
  reject = 0.0
  if (sic_present(1,0)) then
    reject = 2.00 ! 2.0 sigma rejection threshold
    call sic_r4(line,1,1,reject,.false.,error)
    if (error) return
  endif
  !
  ! Reference antenna
  jref = 0
  call sic_i4(line,0,3,jref,.false.,error)
  if (error) return
  !
  call sic_ch(line,0,1,filestring,n,.true.,error)
  !
  dirstring = ' '
  call gag_directory(dirstring,filestring,filename,nfile,error)
  if (error) return
  if (nfile.eq.0) then
    call map_message(seve%e,comm,'No such files '//trim(filestring))
    error = .true.
    return
  endif
  !
  call sic_ch(line,0,2,filestring,n,.true.,error)
  if (error) return
  !
  ! Read headers
  call gildas_null(head)
  call gildas_null(htmp)
  !
  call sic_parse_file(filename(1),' ','.gdf',head%file)
  call gdf_read_header(head,error)
  if (error) return
  call gdf_trim_header(head,3,error)
  if (error) return
  if (head%char%code(1).ne.'TIME' .or. head%char%code(2).ne.'(ANTENNAS)') then
    call map_message(seve%e,comm,'Data file '//trim(head%file)//' is of wrong type')
    error = .true.
    return
  endif
  !
  ! Make the overall allocation. This are small files anyhow
  allocate(datas(head%gil%dim(1),head%gil%dim(2),head%gil%dim(3),nfile), &
    & freqs(nfile), phase(nfile), dphase(nfile), snr(nfile), &
    & ampli(nfile), weight(nfile), pf(nfile), work(nfile), stat=ier)
  if (ier.ne.0) then
    call gdf_close_image(head,error)
    call map_message(seve%e,comm,'Data memory allocation error')
    error = .true.
    return
  endif  
  freqs(1) = head%gil%freq
  !
  ! Read the first data
  call gdf_read_data(head,datas(:,:,:,1),error)
  call gdf_close_image(head,err)
  if (error) return
  !
  ! Check and read the other data files
  do i=2,nfile
    call sic_parse_file(filename(i),' ','.gdf',htmp%file)
    call gdf_read_header(htmp,error)
    if (.not.error) call gdf_trim_header(htmp,3,error)
    if (error) then
      continue
    else if (htmp%char%code(1).ne.'TIME' .or. htmp%char%code(2).ne.'(ANTENNAS)') then
      call map_message(seve%e,comm,'Data file '//trim(htmp%file)//' is of wrong type')
      error = .true.
    else if (any(htmp%gil%dim.ne.head%gil%dim)) then
      call map_message(seve%e,comm,'Inconsistent data set '//trim(htmp%file))      
      if (htmp%gil%dim(1).ne.head%gil%dim(1)) then
        call map_message(seve%e,comm,'Different number of time steps')
      endif
      if (htmp%gil%dim(2).ne.head%gil%dim(2)) then
        call map_message(seve%e,comm,'Different number of antennas')
      endif
      if (htmp%gil%dim(3).ne.head%gil%dim(3)) then
        call map_message(seve%e,comm,'Different number of Self-calibration cycles')
      endif
      error = .true.
    else
      htmp%blc = 0
      htmp%trc = 0
      call gdf_read_data(htmp,datas(:,:,:,i),error)
      freqs(i) = htmp%gil%freq
    endif
    err = .false.
    call gdf_close_image(htmp,err)
    if (error) return
  enddo
  !
  !
  ! Prepare the output file
  call gildas_null(hres)
  call gdf_copy_header(head,hres,error)
  if (error) return
  call sic_parse_file(filestring,' ','.gdf',hres%file)
  call gdf_allocate(hres,error)
  if (error) return
  !
  ! Now, do the job
  ntime = head%gil%dim(1)
  nante = (head%gil%dim(2)-4)/3
  nloop = head%gil%dim(3)
  !!Print *,'NTIME ',ntime,' NANTE ',nante,' NLOOP ',nloop
  allocate(aref(ntime),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,comm,'Reference antenna buffer allocation error')
    error = .true.
    return
  endif
  !
  hres%r3d(:,:,:) = datas(:,:,:,1)
  ! We have to adjust the mean frequency and the effective bandwidth
  hres%gil%freq = sum(freqs)/nfile
  hres%gil%fres = nfile*hres%gil%fres
  !
  if (jref.eq.0) then
    aref(:) = datas(:,4,1,1)    ! Reference antenna
  else
    aref(:) = jref
  endif
  !
  iampli = 5 ! For Amplitude
  iphase = 6 ! For Phase  
  !
  do iloop = 1,nloop
    do itime = 1,ntime
      do iant = 1,nante
        ka = iampli+3*(iant-1)       ! Amplitude 
        kp = iphase+3*(iant-1)       ! Phase
        ks = kp+1                    ! SNR
        kr = iphase+3*(aref(itime)-1) ! Reference Phase 
        do j=1,nfile
          ampli(j) = datas(itime,ka,iloop,j)
          phase(j) = datas(itime,kp,iloop,j)-datas(itime,kr,iloop,j)
          snr(j) = datas(itime,ks,iloop,j)
        enddo
        !
        if (any(snr.gt.0)) then
          ! Ignore Spectral index for Amplitude : it normally goes
          ! into the SNR, not in the correction.
          weight(:) = snr**2
          work(:) = ampli*weight
          pp = sum(work)
          ps = sum(weight)
          pp = pp/ps
          hres%r3d(itime,ka,iloop) = pp
          !
          ! Phase now
          pf(:) = phase/freqs
          ! dphase = 180/pi/snr 
          ! weight = (freqs/dphase)**2
          !  ... so ...
          ! weight = (freqs/(180/pi/snr))**2
          !
          weight(:) = (freqs*snr*pi/180)**2
          work(:) = pf*weight
          pp = sum(work)     ! Sum V_i/Sigma_i
          ps = sum(weight)   ! Sum 1/Sigma_i
          pp = pp/ps ! Weighted mean...
          hres%r3d(itime,kp,iloop) = pp*hres%gil%freq
          !
          ! Neglect the (small) frequency dependency here
          pf(:) = snr**2
          pp = sum(pf)
          pp = sqrt(pp)
          hres%r3d(itime,ks,iloop) = pp
        else
          hres%r3d(itime,ks,iloop) = 0
        endif
      enddo   ! IANT
      ! Reset new reference antenna
      hres%r3d(itime,4,iloop) = aref(itime)
    enddo ! ITIME
  enddo ! ILOOP
  !
  ! At this stage, we filter the really unstable
  ! solutions.  We give them "negative" SNRs, so that they
  ! could be highlighted by SHOW SELFCAL, and ignored
  ! in the APPLY command.
  !
  if (nloop.gt.1 .and. reject.gt.0) then
    ireject = 0
    do itime=1,ntime
      do iant=1,nante
        ka = iampli+3*(iant-1)         ! Amplitude 
        kp = iphase+3*(iant-1)         ! Phase
        ks = kp+1                      ! SNR
        kr = iphase+3*(aref(itime)-1)  ! Reference Phase 
        pp = hres%r3d(itime,kp,nloop)-hres%r3d(itime,kp,nloop-1) ! Phase difference between last 2 iterations
        !
        dp = 180.0/pi/hres%r3d(itime,ks,nloop) ! Phase Error from SNR
        if (abs(pp).gt.reject*dp) then  ! Flag when above REJECT*sigma 
            !!Print *,'Rejecting ',itime,iant,' Phase ',abs(pp),reject,dp
            hres%r3d(itime,ks,:) = -abs(hres%r3d(itime,ks,:))
            ireject = ireject+1
        endif
      enddo
    enddo
    if (ireject.ne.0) then
      write(mess,'(A,I0,A)') 'Rejected ',ireject,' unstable solutions'
      call map_message(seve%w,comm,mess)
    endif
  endif
  !
  ireject = 0
  do itime=1,ntime
    do iant=1,nante
      ka = iampli+3*(iant-1)         ! Amplitude 
      kp = iphase+3*(iant-1)         ! Phase
      ks = kp+1                      ! SNR
      if (hres%r3d(itime,ks,nloop).le.0) then
        ireject = ireject+1
      endif
    enddo
  enddo
  if (ireject.ne.0) then
    write(mess,'(A,I0,A,I0,A)') 'Found ',ireject, & 
    & ' missing solutions (out of ',ntime*nante,')'
    call map_message(seve%w,comm,mess)
  endif
  !
  call gdf_write_image(hres,hres%r3d,error)  
  !
end subroutine gather_self
!
subroutine derive_base(line,comm,error)
  use gkernel_interfaces
  use gkernel_types
  use gbl_message
  use imager_interfaces, only : map_message
  use clean_arrays
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support for command
  !     DERIVE Agains [CgainsFile]
  !
  !   Derive Baseline-based (Cgains) solution from an Antenna-Based gain
  ! solution.  The Baseline-based solution is stored in buffer
  ! CGAINS, unless a file name is given by argument CgainsFile.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  character(len=*), intent(in) :: comm
  logical, intent(inout) :: error
  !
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  !
  type(gildas) :: hante  ! Antenna based gains
  type(gildas) :: hbase  ! Baseline based gains
  character(len=filename_length) :: filestring
  integer :: n, ier
  integer :: nloop,ntime,nante,nbase,itime,iant,jant,nant
  integer :: kt, kd, kn, iv,   iampli,iphase
  integer :: ka_j, kp_j, ks_j, ka_i, kp_i, ks_i
  real :: snr_j, snr_i
  real :: phase, ampli, breal, bimag
  !
  integer :: iprint
  !
  call sic_ch(line,0,1,filestring,n,.true.,error)
  !
  call gildas_null(hante)
  call sic_parse_file(filestring,' ','.gdf',hante%file)
  call gdf_read_header(hante,error)
  if (error) return
  call gdf_trim_header(hante,3,error)
  if (error) return  
  call gdf_allocate(hante,error)
  if (error) return
  call gdf_read_data(hante,hante%r3d,error)
  if (error) return
  !
  ! Now, do the job
  ntime = hante%gil%dim(1)
  nante = (hante%gil%dim(2)-4)/3
  nloop = hante%gil%dim(3)
  nbase = (nante*(nante-1))/2
  !! Print *,'Ntime',ntime,' Nante',nante,' Nbase ',nbase,' Nloop',nloop,' Nvisi ',ntime*nbase
  !
  call gildas_null(hbase,type='UVT')
  call gdf_copy_header(hante,hbase,error)
  hbase%gil%ndim = 2
  hbase%gil%dim(1) = 10
  hbase%gil%dim(2) = ntime * nbase
  hbase%blc = 0
  hbase%trc = 0
  !
  hbase%gil%ref(1) = 1
  hbase%gil%val(1) = hbase%gil%freq
  hbase%gil%inc(1) = hbase%gil%fres
  !
  call gdf_allocate(hbase,error)
  if (error) return
  !
  kt = 1 ! Time pointer
  kd = 2 ! Date pointer
  kn = 3 ! Number of antennas 
  iampli = 5 ! For Amplitude
  iphase = 6 ! For Phase  
  !
  iv = 0 !  Visibility Counter ...
  !
  iprint = 0 !! Set to 1 to debug
  do itime = 1,ntime
    ! NO: this was a bug !    nant = hante%r3d(itime,kn,nloop)
    nant = nante  ! Use all stored antennas, to use the right antenna number
    !
    do iant = 1,nant
      ka_i = iampli+3*(iant-1)        ! Amplitude 
      kp_i = iphase+3*(iant-1)        ! Phase
      ks_i = kp_i+1                   ! SNR
      snr_i = hante%r3d(itime,ks_i,nloop)
      do jant = iant+1,nant
        iv = iv+1
        ka_j = iampli+3*(jant-1)        ! Amplitude 
        kp_j = iphase+3*(jant-1)        ! Phase
        ks_j = kp_j+1                   ! SNR
        snr_j = hante%r3d(itime,ks_j,nloop)
        if (snr_i.le.0 .or. snr_j.le.0) then
          if (iprint.gt.0) print *,'Flagging ',iant,jant,' SNR_I ',snr_i,' SNR_J ',snr_j
          hbase%r2d(8,iv) = 1.0
          hbase%r2d(9,iv) = 0.0
          hbase%r2d(10,iv) = -1.0
        else
          phase = hante%r3d(itime,kp_j,nloop)-hante%r3d(itime,kp_i,nloop)
          phase = phase*pi/180.
          ampli = hante%r3d(itime,ka_j,nloop)*hante%r3d(itime,ka_i,nloop)
          breal = ampli*cos(phase)
          bimag = ampli*sin(phase)
          hbase%r2d(8,iv) = breal
          hbase%r2d(9,iv) = bimag
          hbase%r2d(10,iv) = 1.0
        endif
        !
        hbase%r2d(1,iv) = 0 ! u 
        hbase%r2d(2,iv) = 0 ! v
        hbase%r2d(3,iv) = 0 ! w 
        hbase%r2d(4,iv) = hante%r3d(itime,kd,nloop)+hante%gil%val(1) ! Date 
        hbase%r2d(5,iv) = hante%r3d(itime,kt,nloop)                  ! Time 
        hbase%r2d(6,iv) = iant ! First antenna
        hbase%r2d(7,iv) = jant ! Second antenna 
        !
      enddo 
    enddo
    if (iprint.ne.0) then
      Print *,'End time ',itime
      read(5,*) iprint
    endif
  enddo
  !
  hbase%gil%dim(2) = iv
  hbase%gil%nvisi = iv ! Mandatory
  hbase%gil%nchan = 1  ! Mandatory
  hbase%gil%nstokes = 1 ! Also
  hbase%char%code(1) = 'UV-DATA'
  hbase%char%code(2) = 'RANDOM'
  hbase%gil%faxi = 1
  !
  ! Move to CGAINS array or Write on file ?  
  if (sic_narg(0).gt.1) then
    call sic_ch(line,0,2,filestring,n,.true.,error)
    call sic_parse_file(filestring,' ','.gdf',hbase%file)
    call gdf_write_image(hbase,hbase%r2d,error)
  else
    call sic_delvariable('CGAINS',.false.,error)
    call gildas_null(hbgain,type='UVT')
    call gdf_copy_header(hbase,hbgain,error)
    if (allocated(duvbg)) deallocate(duvbg)
    allocate(duvbg(hbase%gil%dim(1),hbase%gil%dim(2)),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,comm,'Memory allocation error')
      error = .true.
      return
    endif
    duvbg(:,:) = hbase%r2d(:,1:iv)
    call sic_mapgildas ('CGAINS',hbgain,error,duvbg)
  endif
end subroutine derive_base  

