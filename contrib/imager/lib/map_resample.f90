subroutine map_resample_comm(line,comm,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>map_resample_comm, &
    & no_interface => map_operation
  use clean_def
  use clean_types
  use clean_arrays
  use gbl_message 
  use phys_const 
  use iso_c_binding
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  !   Resample in velocity the Images, or Compress them 
  !   Support for commands
  !     MAP_RESAMPLE WhichOne NC [Ref Val Inc] [Result] [/LIKE Mold]
  !     MAP_COMPRESS WhichOne NC [Start RType] [Result]
  !     MAP_INTEGRATE WhichOne Rmin Rmax RType [Result]
  !     MAP_COMPRESS WhichOne NC [Result]
  !
  ! WhichOne    Name of map to be processed
  !             Allowed values are DIRTY, CLEAN, SKY, * 
  !             or any 3-D Image (File or SIC variable)
  ! Result      Indicates the name of the Result file 
  !             or SIC Variable (not .)
  !   Result is mandatory if WhichOne is not a recognized name.
  !   Result cannot be present if WhichOne is *.
  !   When not present, Result = WhichOne.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line  ! Command line
  character(len=*), intent(in) :: comm  ! Calling command
  logical, intent(out) :: error         ! Error flag
  !
  integer, parameter :: o_like=1
  !
  ! Local
  character(len=128) :: chain
  real(8) :: convert(3)
  integer :: nc, n, narg
  type (gildas) :: mapout, mapin, maptmp, hl
  integer :: ier, i
  logical :: do_sky, do_clean, do_dirty, do_image, do_all, do_single, do_view
  logical :: rdonly, is_image
  logical :: i_file, o_file  ! Is Input or Output a file ?
  character(len=12) :: uname
  character(len=filename_length) :: iname, oname
  real, allocatable, target :: dout(:,:,:)
  !
  type(c_ptr) :: cptr
  real(4), pointer :: r3inptr(:,:,:), r3ouptr(:,:,:)
  integer(kind=index_length) :: mrange(2) 
  type (sic_descriptor_t) :: desc
  logical :: found
  integer :: i_out
  ! For RESAMPLE
  character(len=filename_length) :: namel
  logical :: l_image
  integer(kind=index_length) :: ipix1,ipixn
  integer(kind=size_length) :: isize
  integer, allocatable :: ipi(:,:)
  real, allocatable :: ipr (:,:)
  real, allocatable, target :: din(:,:,:)
  real :: pix1,pixn,rstart
  ! For COMPRESS
  integer :: istart
  ! For INTEGRATE
  real(4) :: dv=1.0
  real(8) :: drange(2), xposi, yposi
  character(len=12) :: csort
  integer(kind=4), parameter :: msort=3
  character(len=12) :: types(msort),mysort
  integer :: mc(2), naver, isort
  data types /'CHANNEL','VELOCITY','FREQUENCY'/
  !
  error = .false.
  !
  call sic_ch(line,0,1,iname,nc,.true.,error) ! Case sensitive
  if (error) return
  uname = iname
  call sic_upper(uname)                       ! Upper case
  call gildas_null(mapout, type = 'IMAGE')
  call gildas_null(mapin, type = 'IMAGE')
  call gildas_null(maptmp, type = 'IMAGE')
  !
  isize = 0
  do_clean = .false.
  do_sky   = .false.
  do_dirty = .false.
  do_image = .false.
  do_all = .false.
  do_single = .false.
  do_view = .false.
  i_file = .false.
  o_file = .false.
  !
  ! Find out action and command syntax
  select case (comm)
  case ('MAP_RESAMPLE')
    if (sic_present(o_like,0)) then
      i_out = 2
    else
      i_out = 6     ! Output file argument
    endif
  case ('MAP_COMPRESS') 
    narg = sic_narg(0)
    if (narg.eq.2) then
      ! MAP_COMPRESS In Nc [Output]
      i_out = 3 ! Present or Not, will be tested later
      istart = 1
    else if (narg.eq.4 .or. narg.eq.5) then
      i_out = 5 ! Present or Not, will be tested later
    else 
      call map_message(seve%e,comm, &
          ' Invalid number of arguments, see HELP MAP_COMPRESS')
      error = .true.
      return
    endif
  case ('MAP_SMOOTH') 
    i_out = 3
    istart = 1
  case ('MAP_INTEGRATE') 
    !
    i_out = 5 ! Output file argument
  end select
  !
  ! Select input Data set (Buffer, *, or other image)
  !
  select case(uname)
  case('DIRTY')
    isize = hdirty%loca%size
    call gdf_copy_header(hdirty,mapin,error)
    do_dirty = .true.
  case('CLEAN')
    isize = hclean%loca%size
    call gdf_copy_header(hclean,mapin,error)
    do_clean = .true.
  case('SKY')
    isize = hsky%loca%size
    call gdf_copy_header(hsky,mapin,error)
    do_sky = .true.
  case('DATA') 
    isize = hview%loca%size
    call gdf_copy_header(hview,mapin,error)
    do_view = .true.
  case('*') 
    if (hclean%loca%size.ne.0) then
      do_clean = .true.
      isize = hclean%loca%size
      if (hsky%loca%size .ne. 0) then
        if (hsky%loca%size .ne. isize) then
          call map_message(seve%e,comm,'CLEAN and SKY do not match')
          error = .true.
          return
        endif
        do_sky = .true.
      endif
      call gdf_copy_header(hclean,mapin,error)
    else if (hsky%loca%size .ne. 0) then
      isize = hsky%loca%size
      call gdf_copy_header(hsky,mapin,error)
      do_sky = .true.
    endif
    do_all = .true.
  case('SINGLE')
    isize = hsingle%loca%size
    call gdf_copy_header(hsingle,mapin,error)
    do_single = .true.
  case default
    !
    ! Make sure this is not an internal buffer
    do i=1,mbuffer
      if (uname.eq.cbuffer(i)) then
        call map_message(seve%e,comm,'Cannot modify buffer '//trim(uname))
        error = .true.
        return
      endif
    enddo
    !
    is_image = .false.
    call sub_readhead(comm,iname,mapin,is_image,error,rdonly,fmt_r4)
    if (error) return
    if (mapin%gil%ndim.ne.3) then
      call map_message(seve%e,comm,trim(iname)//' is not a GILDAS 3-D cube')
      error = .true.
      return
    endif    
    do_image = .true.
    i_file = .not.is_image
  end select
  !
  ! Check known buffers allocation status
  if (.not.do_image) then
    if (isize.eq.0) then
      call map_message(seve%e,comm,'Specified data '//trim(iname)//' is not allocated')
      error = .true.
      return
    endif    
  endif
  !
  if (i_file .or. .not.do_image) then
    allocate(din(mapin%gil%dim(1),mapin%gil%dim(2),mapin%gil%dim(3)),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,comm,'Input file Memory allocation failure')
      error = .true.
      return  
    endif
  endif
  !
  call gdf_copy_header(mapin,mapout,error)
  if (error) return  
  !
  ! FInd out action and parameters
  select case (comm)
  case ('MAP_RESAMPLE')
    !
    if (sic_present(o_like,0)) then
      i_out = 2
      ! /LIKE option
      call gildas_null(hl)
      l_image = .false.
      call sic_ch(line,o_like,1,namel,nc,.true.,error)
      if (error) return
      !
      call sub_readhead(comm,namel,hl,l_image,error) !! ,rdonly,fmt)
      if (error) return
      !
      if (hl%gil%faxi.ne.0) then
        if (abs(hl%gil%freq-mapin%gil%freq).gt.1d-5) then  !10 Hz tolerance
          write(chain,'(A,F15.6,A,F15.6,A)') 'Rest frequency of template ', &
          & hl%gil%freq,' does not match that of data ',mapin%gil%freq
          call map_message(seve%w,comm,chain,3)
        endif
        nc = hl%gil%dim(hl%gil%faxi)             ! For Data cubes
        if (hl%gil%nchan.ne.0) nc = hl%gil%nchan ! For UV data
        convert = hl%gil%convert(:,hl%gil%faxi)
        convert(2) = hl%gil%voff
        convert(3) = hl%gil%vres
        
      else
        call map_message(seve%e,comm,'Reference image has no Velocity axis')
      endif
    else
      i_out = 6     ! Output file argument
      call sic_i4(line,0,2,nc,.true.,error)
      if (error) return
      if (nc.le.0) then
        call map_message(seve%e,comm,'Invalid number of channels')
        error = .true.
        return
      endif
      !
      ! Set default resampling
      convert = mapin%gil%convert(:,3)
      !
      call sic_r8(line,0,3,convert(1),.false.,error)
      if (error) return
      call sic_r8(line,0,4,convert(2),.false.,error)
      if (error) return
      call sic_r8(line,0,5,convert(3),.false.,error)
      if (error) return
    endif
    !
    ! Convert(3) is initially on a Velocity Scale.
    mapout%gil%voff = convert(2)
    mapout%gil%vres = convert(3)
    mapout%gil%fres = - mapout%gil%vres * 1d3 / clight * mapout%gil%freq
    mapout%gil%convert(:,3) = convert
    mapout%gil%dim(3) = nc
    !
    !
    ! Check limits
    pix1 = mapout%gil%ref(3) + ( mapin%gil%val(3) + (1.d0-mapin%gil%ref(3))  &
        & *mapin%gil%inc(3) - mapout%gil%val(3) )/mapout%gil%inc(3)
    pixn = mapout%gil%ref(3) + ( mapin%gil%val(3) + (mapin%gil%dim(3)-mapin%gil%ref(3))  &
        & *mapin%gil%inc(3) - mapout%gil%val(3) )/mapout%gil%inc(3)
    if (pix1.lt.pixn) then
      ipix1 = int(pix1)
      if (ipix1.ne.pix1) ipix1 = ipix1+1
      ipix1 = max(ipix1,1)
      ipixn = min(int(pixn),mapout%gil%dim(3))
    else
      ipixn = min(int(pix1),mapout%gil%dim(3))
      ipix1 = int(pixn)
      if (ipix1.ne.pixn) ipix1 = ipix1+1
      ipix1 = max(ipix1,1)
    endif
    !
    allocate (ipi(2,mapout%gil%dim(3)), ipr(4,mapout%gil%dim(3)), stat=ier)
    !
    mrange(1) = ipix1
    mrange(2) = ipixn
  case ('MAP_COMPRESS') 
    !
    call sic_i4(line,0,2,nc,.true.,error)
    if (error) return
    !
    ! Cannot compress by more channels than available
    if (nc.gt.mapin%gil%dim(3)) nc = mapin%gil%dim(3)
    !
    ! Set offset channel
    istart = 1
    narg = sic_narg(0)
    if (narg.eq.2) then
      ! MAP_COMPRESS In Nc [Output]
      i_out = 3 ! Present or Not, will be tested later
      istart = 1
    else if (narg.eq.4 .or. narg.eq.5) then
      ! MAP_COMPRESS In Nc Rstart TYPE [Output]
      call sic_ke(line,0,4,csort,n,.true.,error)
      if (error) return
      call sic_r4(line,0,3,rstart,.true.,error)
      if (error) return
      call sic_ambigs(comm,csort,mysort,isort,types,msort,error)
      if (error) return
      !
      ! Convert Rstart to Istart
      select case (mysort)
      case ('CHANNEL')
        istart = nint(rstart)
      case ('VELOCITY')
        istart = (rstart-mapin%gil%ref(3))/mapin%gil%vres + mapin%gil%voff
      case ('FREQUENCY')
        istart = (rstart-mapin%gil%ref(3))/mapin%gil%fres + mapin%gil%freq
      end select
      !
      i_out = 5 ! Present or Not, will be tested later
    else 
      call map_message(seve%e,comm, &
          ' Invalid number of arguments, see HELP MAP_COMPRESS')
      error = .true.
      return
    endif
    if (istart.lt.1 .or. istart.gt.nc) then
      write(chain,'(A,I0,1X,I0)') 'Offset channel out of range ',istart,nc
      call map_message(seve%e,comm,chain)
      error = .true.
      return
    endif
    write(chain,'(A,I0,A)') 'Averaging by chunks of ',nc,' channels'
    call map_message(seve%i,comm,chain)
    !
    mapout%gil%inc(3) = mapin%gil%inc(3)*nc
    mapout%gil%ref(3) = (2.0*mapin%gil%ref(3)+nc-1.0)/2/nc
    mapout%gil%vres = nc*mapin%gil%vres
    mapout%gil%fres = nc*mapin%gil%fres
    ! Change the number of channels
    mapout%gil%dim(3) = (mapin%gil%dim(3)+1-istart)/nc
    if (mapout%gil%dim(3).lt.1) then
      call map_message(seve%e,comm,'Not enough channels')
      error = .true.
      return
    endif
    !
    mrange(1) = nc
    mrange(2) = istart
  case ('MAP_SMOOTH')
    !
    call sic_i4(line,0,2,nc,.true.,error)
    if (error) return
    !
    ! Cannot (reasonably) compress by more than 25 % of the available channels 
    if (nc.lt.2 .or. nc.gt.mapin%gil%dim(3)/4) then
      call map_message(seve%e,comm,'Smoothing number out of range')
      error = .true.
    endif
    write(chain,'(A,I0,A)') 'Smoothing by ',nc,' channels'
    call map_message(seve%i,comm,chain)
    !
    mrange = nc
  case ('MAP_INTEGRATE') 
    !
    i_out = 5 ! Output file argument
    !
    call sic_r8 (line,0,2,drange(1),.true.,error)
    if (error) return
    call sic_r8 (line,0,3,drange(2),.true.,error)
    if (error) return
    csort = mapin%char%code(3)
    call sic_ke (line,0,4,csort,nc,.false.,error)
    if (error) return
    error = .false.
    call sic_ambigs(comm,csort,mysort,isort,types,msort,error)
    if (error)  return
    !
    ! Define number of channels
    call out_range(comm,csort,drange,mc,mapin,error)
    if (error) return
    ier = gdf_range (mc, mapin%gil%dim(3))
    naver = mc(2)-mc(1)+1
    !
    ! Build a pseudo axis aligned on the input one:
    !  - Number of channels set to 1,
    !  - Increment set to the extracted range,
    !  - Value at reference unchanged (same spectroscopic axis, e.g. regarding
    !    associated Rest Freq),
    !  - Center of averaged range (old axis) is aligned on the center of the
    !    output channel #1 (new axis)
    ! This writes as:
    mapout%gil%ndim = 3
    mapout%gil%dim(3) = 1
    ! mapout%gil%val(3) = unchanged
    yposi = (mc(1)+mc(2))*0.5d0
    xposi = 1.d0
    mapout%gil%inc(3) = mapin%gil%inc(3)*naver
    mapout%gil%ref(3) = xposi - (yposi-mapin%gil%ref(3))/naver
    ! Do not forget the SPECTRO section
    mapout%gil%vres = mapin%gil%vres*naver
    mapout%gil%fres = mapin%gil%fres*naver
    ! mapout%gil%voff = unchanged
    ! mapout%gil%freq = unchanged
    ! mapout%gil%fima = unchanged
    !
    mapout%loca%size = mapout%gil%dim(1)*mapout%gil%dim(2)
    if (mapin%char%code(3).eq.'VELOCITY') then
      mapout%char%unit = trim(mapin%char%unit)//'.km/s'
    elseif (mapin%char%code(3).eq.'FREQUENCY') then
      mapout%char%unit = trim(mapin%char%unit)//'.MHz'
    else
      mapout%char%unit = 'UNKNOWN'
    endif
    mapout%gil%extr_words = 0
    mapout%gil%blan_words = 2
    mapout%gil%bval = 0.
    mapout%gil%eval = 0.    
    dv = abs(mapin%gil%inc(3))
    mrange(1) = mc(1)
    mrange(2) = mc(2)
  end select
  !
  if (do_all) then
    if (sic_present(0,i_out)) then
      call map_message(seve%e,comm,'Output specifier invalid in this context')
      error = .true.
      return
    endif    
  else
    if (sic_present(0,i_out)) then
      if (do_clean) then
        call gdf_copy_header(hclean,mapin,error)
        mapin%loca%addr = locwrd(dclean)
      else if (do_sky) then
        call gdf_copy_header(hsky,mapin,error)
        mapin%loca%addr = locwrd(dsky)
      else if (do_dirty) then
        call gdf_copy_header(hdirty,mapin,error)
        mapin%loca%addr = locwrd(ddirty)
      else if (do_single) then
        call gdf_copy_header(hsingle,mapin,error)
        mapin%loca%addr = locwrd(dsingle)
      else if (do_view) then
        call gdf_copy_header(hview,mapin,error)
        mapin%loca%addr = locwrd(dview)      
      endif
      do_clean = .false.
      do_sky = .false.
      do_dirty = .false.
      do_single = .false.
      do_view = .false.
      do_image = .true.
    endif
  endif    
  !
  ! Now do the job
  if (do_clean) then
    call map_resample_buffer('CLEAN',dclean,code_save_clean, &
    & comm,mapin,mapout,hclean, din,mrange,dv,error)
  endif
  !
  if (do_sky) then
    call map_resample_buffer('SKY',dsky,code_save_sky, &
    & comm,mapin,mapout,hsky, din,mrange,dv,error)
  endif
  !
  if (do_dirty) then
    call map_resample_buffer('DIRTY',ddirty,code_save_dirty, &
    & comm,mapin,mapout,hdirty, din,mrange,dv,error)
    !
    hdirty%gil%reso_words = 3
    hdirty%gil%majo= 0.0
    hdirty%gil%mino= 0.0
    hdirty%gil%posa = 0.0
  endif
  !
  if (do_single) then
    call map_resample_buffer('SINGLE',dsingle,code_save_single, &
    & comm,mapin,mapout,hsingle, din,mrange,dv,error)
  endif
  !
  if (.not.do_image) return
  !
  ! Work from an external file or non built-in SIC variable
  oname = iname
  call sic_ch(line,0,i_out,oname,n,.false.,error)
  if (error) return
  !
  o_file = (index(oname,'.').ne.0) .or. (index(oname,':').ne.0) 
  narg = ichar(oname(1:1))-ichar('0')
  if (narg.ge.0 .and. narg.le.9) then ! It cannot be a Variable
    if (o_file) then
      call map_message(seve%w,comm,'Output file name starts with a digit')
    else
      call map_message(seve%e,comm,'Invalid Output Variable name')
      error = .true.
      return
    endif
  endif
  !
  if (o_file) then
    !
    ! With an output file, this is easy
    allocate (dout(mapout%gil%dim(1),mapout%gil%dim(2),mapout%gil%dim(3)) ,stat=ier)   
    if (ier.ne.0) then
      call map_message(seve%e,comm,'Output Memory allocation error')
      error = .true.
      return
    endif
    !
    call sic_parse_file(oname,' ','.gdf',mapout%file)
    r3ouptr => dout
  else
    !
    call sic_upper(oname)
    if (.not.i_file) then
      if (iname.eq.oname) then
        call map_message(seve%e,comm,'Input and Output SIC variables must differ')
        error = .true.
        return
      endif
    endif
    !
    ! Verify output SIC variable match
    is_image = .true.
    call sub_readhead(comm,oname,maptmp,is_image,error,rdonly,fmt_r4)
    if (error) return
    if (rdonly) then
      call map_message(seve%e,comm,'Ouput SIC Variable is Read-Only')
      error = .true.
      return
    endif
    do i=1,max(maptmp%gil%ndim,mapout%gil%ndim)
      if (maptmp%gil%dim(i).ne.mapout%gil%dim(i)) then
        !!Print *,'TMP ',maptmp%gil%ndim, maptmp%gil%dim(1:4)
        !!Print *,'OUT ',mapout%gil%ndim, mapout%gil%dim(1:4)
        call map_message(seve%e,comm,'Ouput SIC Variable size mismatch')
        error = .true.
        return
      endif
    enddo
    !
    ! I need the descriptor as output in any case...
    call sic_descriptor(oname,desc,found)      
    ! OK, we can do the job
    !! Print *,'Addr ',desc%addr,' tmp ',maptmp%loca%addr
    call adtoad(maptmp%loca%addr, cptr, 1)
    call c_f_pointer(cptr,r3ouptr,mapout%gil%dim(1:3))
  endif
  !
  ! Now, the input
  if (i_file) then  
    ! The Input data is a GILDAS file
    !
    ! Read the input file
    ! We could even do the job by reading only the required range
    ! through proper blc(3) trc(3) and appropriate shifting of mrange
    ! in the case of MAP_INTEGRATE
    call gdf_read_data(mapin,din,error)
    call gdf_close_image(mapin,error)
    r3inptr => din
  else
    !
    ! The Input data is a SIC Image Variable
    if (.not.sic_present(0,i_out)) then
      call map_message(seve%e,comm,'An output File or SIC variable is needed')
      error = .true.
      return
    endif
    !
    call sic_upper(iname)
    call adtoad(mapin%loca%addr,cptr,1)
    call c_f_pointer(cptr, r3inptr, mapin%gil%dim(1:3))
    !
  endif
  !
  call map_operation(comm,mapout,r3ouptr,mapin,r3inptr,mrange,dv)
  !
  ! Flush data and Header
  if (o_file) then
    call gdf_write_image(mapout,dout,error)
  else
    call gdf_copy_header(mapout,desc%head,error)      
  endif
  !
end subroutine map_resample_comm
!
!
subroutine map_operation(comm,mapout,dout,mapin,din,mrange,dv)
  use gkernel_types
  !---------------------------------------------------------------------
  ! @ public-mandatory
  !
  !   IMAGER
  !     Support for commands MAP_COMPRESS, MAP_INTEGRATE and MAP_RESAMPLE
  !   Apply the specified Command to Mapin,Din to update Mapout,Dout.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: comm    ! Operation
  type(gildas), intent(in) :: mapin       ! Input image
  type(gildas), intent(inout) :: mapout   ! Output image
  real, intent(in) :: din(mapin%gil%dim(1),mapin%gil%dim(2),mapin%gil%dim(3))  ! Input data
  real, intent(out) :: dout(mapout%gil%dim(1),mapout%gil%dim(2),mapout%gil%dim(3))  ! Output data
  integer(kind=index_length), intent(in) :: mrange(2)   ! Range of channels
  real, intent(in) :: dv                  ! Scale factor
  !
  integer(kind=index_length) :: ipix1, ipixn
  integer :: mc(2)
  integer :: istart, nc, ix, iy
  !
  logical :: blanked, odd
  integer, allocatable :: ipi(:,:)
  real, allocatable :: ipr (:,:), count(:,:)
  integer :: ier, ifi, ic, kc, lc, k, im, ip
  real :: fnoise, r
  !
  fnoise = 1.0   ! Avoid warning about initialization
  if (comm.eq.'MAP_RESAMPLE') then
    !!Print *,'MRANGE ',mrange
    ipix1 = mrange(1)
    ipixn = mrange(2)
    allocate (ipi(2,mapout%gil%dim(3)), ipr(4,mapout%gil%dim(3)), stat=ier)
    !
    blanked = .false.
    call all_inter(mapout%gil%dim(1)*mapout%gil%dim(2),ipix1,ipixn, &
      & dout,mapout%gil%dim(3),mapout%gil%inc(3),mapout%gil%ref(3),mapout%gil%val(3), &
      & din,mapin%gil%dim(3),mapin%gil%inc(3),mapin%gil%ref(3),mapin%gil%val(3), &
      & mapout%gil%bval, blanked, ipi, ipr) 
    !
    fnoise = sqrt(abs(mapin%gil%inc(3)/mapout%gil%inc(3)))
    !
    if (blanked) then
      mapout%gil%eval = 0.d0
      mapout%gil%blan_words = 2
    endif
    !
  else if (comm.eq.'MAP_COMPRESS') then
    istart = mrange(2)
    nc = mrange(1)
    !
    ifi = istart
    do ic=1,mapout%gil%dim(3)
      do k=ifi,min(ifi+nc-1,mapin%gil%dim(3))
        dout(:,:,ic) = dout(:,:,ic) + din(:,:,k)
      enddo
      kc = min(ifi+nc-1,mapin%gil%dim(3))-ifi+1
      dout(:,:,ic) = dout(:,:,ic)/kc
      !
      ifi = ifi+nc
    enddo
    !
    fnoise = 1.0/sqrt(real(nc)) 
  else if (comm.eq.'MAP_INTEGRATE') then  
    !
    ! Protect against Blanked values (e.g. in Sky cubes)
    mc(:) = mrange(:)
    dout(:,:,:) = 0. 
    if (mapin%gil%eval.lt.0) then
      do ic=mc(1),mc(2)
        dout(:,:,1) = dout(:,:,1) + din(:,:,ic)
      enddo
    else
      allocate(count(mapin%gil%dim(1),mapin%gil%dim(2)),stat=ier)
      count = 0
      do ic=mc(1),mc(2)
        do iy=1,mapin%gil%dim(2)
          do ix = 1,mapin%gil%dim(1)
            if (abs(din(ix,iy,ic)-mapin%gil%bval).gt.mapin%gil%eval) then
              dout(ix,iy,1) = dout(ix,iy,1) + din(ix,iy,ic)
              count(ix,iy) = count(ix,iy)+1
            endif
          enddo
        enddo
      enddo
      where (count.ne.0) dout(:,:,1) = (dout(:,:,1)*(mc(2)-mc(1)+1))/count(:,:)
    endif
    dout = dout*dv
    !
    fnoise = sqrt(mrange(2)-mrange(1)+1.0)*dv
  else if (comm.eq.'MAP_SMOOTH') then
    kc = mrange(1)
    nc = kc/2
    odd = mod(kc,2).ne.0 
    kc = 2*nc+1
    !
    do ic=1,mapin%gil%dim(3)
      dout(:,:,ic) = 0
      ! We must ensure that the smoothing is symmetric, as we want to preserve the
      ! spectral sampling of the initial data.
      !
      ! This is a symmetric smoothing, but limited at edges by available data.
      im = ic-nc
      ip = ic+kc-nc-1
      if (im.lt.1) then
        lc = ic-1
        im = 1
        ip = ic+lc
      else if (ip.gt.mapin%gil%dim(3)) then
        lc = mapin%gil%dim(3)-ic
        im = ic-lc
        ip = ic+lc
      endif
      !
      if (odd) then
        ! Naturally symmetric, very simple
        r = 1./(ip-im+1)
        do k=im,ip
          dout(:,:,ic) =  dout(:,:,ic) + r*din(:,:,k)
        enddo
      else
        ! Only consider half of the extreme channels to preserve centering, and
        ! beware of edge channels that may not be smoothed...
        if (im.eq.ip) then
          dout(:,:,ic) =  din(:,:,ip)
        else
          r = 1./(ip-im)
          dout(:,:,ic) =  dout(:,:,ic) + 0.5*r*din(:,:,im)
          do k=im+1,ip-1
            dout(:,:,ic) =  dout(:,:,ic) + r*din(:,:,k)
          enddo
          dout(:,:,ic) =  dout(:,:,ic) + 0.5*r*din(:,:,ip)
        endif
      endif
    enddo 
    ! 
    fnoise = 1./sqrt(real(mrange(1)))
  endif
  !
  mapout%gil%noise = mapout%gil%noise*fnoise
  mapout%gil%rms = mapout%gil%rms*fnoise
end subroutine map_operation
!
subroutine all_inter (n,ic1,icn,x,xdim,xinc,xref,xval, &
  & y,ydim,yinc,yref,yval,bval,blanked,iwork,rwork)
  use gkernel_types
  !---------------------------------------------------------------------
  ! @ private
  ! 
  ! IMAGER
  !	   Performs the linear interpolation/integration along last
  !    axis. All other axes compressed to 1.
  !
  !    Derived from CLASS Internal routine.
  !---------------------------------------------------------------------
  integer(kind=index_length) :: n     ! Map size
  integer(kind=index_length) :: ic1   ! First channel               
  integer(kind=index_length) :: icn   ! Last channel                 !
  integer(kind=index_length) :: xdim  ! Number of Output channels
  real :: x(n,xdim)                   ! Output Cube
  real(8) :: xinc                     ! Channel conversion formula
  real(8) :: xref                     !
  real(8) :: xval                     !
  integer(kind=index_length) :: ydim  ! Number of Input channels
  real :: y(n,ydim)                   !
  real(8) :: yinc                     !
  real(8) :: yref                     !
  real(8) :: yval                     !
  real(4) :: bval                     ! Blanking value
  logical :: blanked 
  integer :: iwork(2,xdim)            ! Input Boundaries for each Output channel 
  real :: rwork(4,xdim)               ! Weights for each Output channel
  !	iwork = imin,imax
  !	rwork = rmin_1, rmin, rmax, rmax_1
  !
  integer i,imax,imin, j, k
  real(8) minpix, maxpix, pix, val, expand
  real rmin_1, rmin, rmax_1, rmax, smin, smax
  real scale
  !
  ! Special case: output axis = input axis
  if ((xdim.eq.ydim).and.(xref.eq.yref).and.(xval.eq.yval).and.(xinc.eq.yinc)) then
    x = y
    return
  endif
  !
  x = 0
  !
  expand = abs(xinc/yinc)
  scale  = 1.d0/expand
  do i = ic1, icn
    !
    ! Compute interval
    !
    val = xval + (i-xref)*xinc
    pix = (val-yval)/yinc + yref
    maxpix = pix + 0.5d0*expand
    minpix = pix - 0.5d0*expand
    imin = int(minpix+1.0d0)
    imax = int(maxpix)
    iwork(1,i) = imin
    iwork(2,i) = imax
    if (imax.ge.imin) then
      if (imin.gt.1) then
        !
        ! Lower end
        !
        ! YMIN = Y(IMIN-1)*(IMIN-MINPIX)+Y(IMIN)*(MINPIX-IMIN+1)
        !     and
        ! X(I) = X(I) + 0.5 * ( (IMIN-MINPIX)*(YMIN+Y(IMIN)) )
        !   
        ! Coefficient de IMIN-1: RMIN_1 = 0.5 * (IMIN-MINPIX)*(IMIN-MINPIX)
        ! Coefficient de IMIN:   RMIN   = 0.5 * (MINPIX-IMIN+2)*(IMIN-MINPIX)
        !
        smin = imin-minpix
        rwork(1,i) = 0.5*smin*smin
        rwork(2,i) = smin-rwork(1,i)
      else
        ! Coefficient de IMIN:      RMIN   = (IMIN-MINPIX)
        ! Coefficient de IMIN-1:    RMIN_1 = 0
        rwork(1,i) = 0.0
        rwork(2,i) = imin-minpix
      endif
      if (imax.lt.ydim) then
        ! Upper end
        smax = maxpix-imax
        rwork(4,i) = 0.5*smax*smax
        rwork(3,i) = smax-rwork(4,i)
      else
        rwork(3,i) = maxpix-imax
        rwork(4,i) = 0.0
      endif
      if (imax.eq.imin) then
        ! Point Case
        rwork(2,i) = rwork(2,i)+rwork(3,i)
        rwork(3,i) = 0.0
      else
        ! General Case: Add 1/2 of IMIN & IMAX + 1 of all intermediate
        ! channels
        rwork(2,i) = rwork(2,i)+0.5
        rwork(3,i) = rwork(3,i)+0.5
      endif
    else
      if (imin.gt.1) then
        rwork(1,i) = (imin-pix)
        rwork(2,i) = (pix+1-imin)
      else
        rwork(1,i) = 0.0
        rwork(2,i) = 1.0
      endif
    endif
  enddo
  !
  ! OK, now do it...
  do i = ic1, icn
    imin = iwork(1,i)
    imax = iwork(2,i)
    rmin_1 = rwork(1,i)
    rmin   = rwork(2,i)
    rmax   = rwork(3,i)
    rmax_1 = rwork(4,i)
    !
    if (imin.lt.1 .or. imax.gt.ydim) then
      ! Never extrapolate
      x(:,i) = bval
      blanked = .true.
    else if (imax.ge.imin) then
      ! General Trapezium integration
      if (imin.gt.1) then
        do j=1,n
          x(j,i) = y(j,imin-1)*rmin_1
        enddo
        do j=1,n
          x(j,i) = x(j,i) + y(j,imin)*rmin
        enddo
      else
        do j=1,n
          x(j,i) = y(j,imin)*rmin
        enddo
      endif
      if (imax.gt.imin) then
        do k = imin+1,imax-1
          do j=1,n
            x(j,i) = x(j,i) + y(j,k)
          enddo
        enddo
        do j=1,n
          x(j,i) = x(j,i) + y(j,imax)*rmax
        enddo
      endif
      if (imax.lt.ydim) then
        do j=1,n
          x(j,i) = x(j,i) + y(j,imax+1)*rmax_1
        enddo
      endif
      ! Normalise
      do j=1,n
        x(j,i) = x(j,i)*scale
      enddo
      !
    else
      ! Interpolation
      if (imin.gt.1) then
        do j=1,n
          x(j,i) = y(j,imin-1)*rmin_1
        enddo
        do j=1,n
          x(j,i) = x(j,i) + y(j,imin)*rmin
        enddo
      else
        do j=1,n
          x(j,i) = y(j,imin)
        enddo
      endif
    endif
  enddo
  !
end subroutine all_inter
!
subroutine map_resample_buffer(name,dres,code_save,comm, &
  & hin,hout,hres,din, mrange,dv,error)
  use gkernel_types
  use gkernel_interfaces
  use clean_types
  use imager_interfaces, only : map_message, map_operation
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  !   IMAGER
  !     Support for commands MAP_COMPRESS, MAP_INTEGRATE and MAP_RESAMPLE
  !   Apply the specified Command to the specified internal buffer
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: name  ! Buffer name
  integer, intent(in) :: code_save      ! Buffer code
  character(len=*), intent(in) :: comm  ! Command name
  real, allocatable, intent(inout) :: dres(:,:,:) ! Destination data
  type(gildas), intent(in) :: hin       ! Input header  
  type(gildas), intent(inout) :: hout   ! Output header
  type(gildas), intent(inout) :: hres   ! Destination header
  real, intent(out) :: din(:,:,:)       ! Work buffer
  integer(kind=index_length), intent(in) :: mrange(2)  ! Channel range
  real, intent(in) :: dv                ! Channel width
  logical, intent(out) :: error         ! Error flag
  !
  integer :: ier
  !
  error = .false.
  din(:,:,:) = dres
  deallocate(dres)
  call sic_delvariable (name,.false.,error)
  !
  allocate (dres(hout%gil%dim(1),hout%gil%dim(2),hout%gil%dim(3)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,comm,name//' memory allocation failure')
    error = .true.
    return  
  endif
  !
  call map_operation(comm,hout,dres,hin,din,mrange,dv)
  !
  call gdf_copy_header(hout,hres,error)
  hres%loca%size = hres%gil%dim(1)*hres%gil%dim(2)*hres%gil%dim(3)
  !
  ! Set "new" internal flag 
  if (code_save.ne.0) then
    save_data(code_save) = .true.
    optimize(code_save)%change = 2  ! Data has been changed, and no Buffer 
  endif
  call sic_mapgildas (name,hres,error,dres)
end subroutine map_resample_buffer

