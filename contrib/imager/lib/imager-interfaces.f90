module imager_interfaces
  !---------------------------------------------------------------------
  ! IMAGER interfaces, all kinds (private or public).
  !---------------------------------------------------------------------
  !
  use imager_interfaces_public
  use imager_interfaces_private
  !
end module imager_interfaces
