subroutine histos (r,nx,ny, list, nl, hx,nh,hmin,hstep)
  !----------------------------------------------------------------------
  ! @ public
  !
  ! GDF	Internal routine
  !
  !	Computes the histogram of part of an image
  !----------------------------------------------------------------------
  integer, intent(in) :: nx        ! X size
  integer, intent(in) :: ny        ! Y size
  real, intent(in)  :: r(nx*ny)    ! Input array 
  integer, intent(in) :: nl        ! List size
  integer, intent(in) :: list(nl)  ! List of values
  integer, intent(in) :: nh        ! Histogram size
  integer, intent(out) :: hx(nh)   ! Histogram
  real, intent(in) :: hmin         ! Min bin
  real, intent(in) :: hstep        ! Bin size
  !
  real :: steph
  integer :: i,k,nin,nout,n
  !
  nin = 0
  nout = 0
  hx = 0
  steph = 1.0/hstep
  !
  do i=1,nl
    k = list(i)
    n = nint((abs(r(k))-hmin)*steph + 1.0)
    if (n.ge.1 .and. n.le.nh) then
      hx(n) = hx(n)+1
      nin = nin+1
    else
      nout= nout+1
    endif
  enddo
  !
  ! Convert to cumulative form
  do n=nh-1,1,-1
    hx(n) = hx(n)+hx(n+1)
  enddo
end subroutine histos
!
subroutine choice (r,nx,ny,list,nl,limite,   &
     &    mcl,wcl,ncl,rmax,ngoal)
  use clean_def
  !----------------------------------------------------------------------
  ! @ public
  !
  !  GILDAS:	CLEAN 	Internal routine
  !	Select possible clean components in residual map.
  !	Components are returned ordered in increasing I and J.
  ! 
  ! Search list is built if ngoal>0, re-used otherwise.
  !----------------------------------------------------------------------
  integer, intent(in) :: nx          ! X size
  integer, intent(in) :: ny          ! Y size
  integer, intent(in) :: nl          ! List size
  integer, intent(in) :: mcl         ! Maximum number of components
  integer, intent(out) :: ncl        ! Number of components found
  real, intent(in) :: r(nx*ny)       ! Input array
  type(cct_par), intent(out) :: wcl(mcl)      ! Components
  integer, intent(in) :: list(nl)    ! List of pixels 
  real, intent(inout) ::  limite     ! selection threshold
  real, intent(in) :: rmax           ! Maximum value
  integer, intent(in) :: ngoal       ! Intended number   
  !
  ! Local variables
  integer i,k
  integer, parameter :: nh=128
  integer hx(nh)
  real hmin, hstep
  !
  if (ngoal.gt.0) then
    hmin = 0.0
    hstep = rmax/(nh-2)
    call histos(r,nx,ny,   &
     &      list,nl,hx,nh,hmin,hstep)
    !
    ! Locate a "reasonable" number of components
    hmin = 0
    do i=1,nh
      if (hmin.eq.0) then
        if (hx(i).lt.ngoal) then
          hmin = (i-1)*hstep
          ncl = hx(i)
        endif
      endif
    enddo
    limite = max(limite,hmin)
  endif
  !
  ! Select possible components in Clean Box
  ncl = 0
  do i=1,nl
    k = list(i)
    if (abs(r(k)).ge.limite) then
      ncl=ncl+1
      wcl(ncl)%influx =r(k)
      wcl(ncl)%iy = (k-1)/nx+1
      wcl(ncl)%ix = k-(wcl(ncl)%iy-1)*nx
    endif
  enddo
end subroutine choice
!
subroutine find_sidelobe (beam,nx,ny,i0,j0,nxp,nyp,bgain)
  !----------------------------------------------------------------------
  ! @ public
  !
  ! GILDAS:	CLEAN	Internal routine
  !	Find the maximum sidelobe outside the beam patch defined
  !	by the beam center (I0,J0) and patch size (NXP,NYP)
  !----------------------------------------------------------------------
  integer, intent(in) :: nx       ! X size
  integer, intent(in) :: ny       ! Y size
  real, intent(in) :: beam(nx,ny) ! Input beam
  integer, intent(in) :: i0       ! X beam center
  integer, intent(in) :: j0       ! Y beam center
  integer, intent(in) :: nxp      ! X beam patch
  integer, intent(in) :: nyp      ! Y beam patch
  real, intent(out) :: bgain      ! Maximum sidelobe
  !
  integer i,j
  real bmax,bmin
  !
  ! Compute maximum sidelobe
  bmax = beam(1,1)
  bmin = beam(1,1)
  do j = 1,j0-nyp
    do i = 1,nx
      if (beam(i,j).gt.bmax) then
        bmax = beam(i,j)
      elseif (beam(i,j).lt.bmin) then
        bmin = beam(i,j)
      endif
    enddo
  enddo
  do j = max(1,j0-nyp+1),min(ny,j0+nyp-1)
    do i = 1,i0-nxp
      if (beam(i,j).gt.bmax) then
        bmax = beam(i,j)
      elseif (beam(i,j).lt.bmin) then
        bmin = beam(i,j)
      endif
    enddo
    do i = i0+nxp,nx
      if (beam(i,j).gt.bmax) then
        bmax = beam(i,j)
      elseif (beam(i,j).lt.bmin) then
        bmin = beam(i,j)
      endif
    enddo
  enddo
  do j = j0+nyp,ny
    do i = 1,nx
      if (beam(i,j).gt.bmax) then
        bmax = beam(i,j)
      elseif (beam(i,j).lt.bmin) then
        bmin = beam(i,j)
      endif
    enddo
  enddo
  bgain = max(abs(bmax),abs(bmin))
  bgain = bgain/abs(beam(i0,j0))
end subroutine find_sidelobe
!
subroutine mos_sidelobe (beam,nx,ny,i0,j0,nxp,nyp,bgain,nf)
  !----------------------------------------------------------------------
  ! @ public
  !
  ! GILDAS:	CLEAN	Internal routine
  !	Find the maximum sidelobe outside the beam patch defined
  !	by the beam center (I0,J0) and patch size (NXP,NYP)
  ! Adapted for Mosaic
  !----------------------------------------------------------------------
  integer nx,ny,i0,j0,nxp,nyp,nf
  real beam(nx,ny,nf),bgain
  !
  integer i,j,k
  real bmax,bmin
  !
  ! Compute maximum sidelobe
  bmax = 0
  bmin = 0
  do k = 1,nf
    do j = 1,j0-nyp
      do i = 1,nx
        if (beam(i,j,k).gt.bmax) then
          bmax = beam(i,j,k)
        elseif (beam(i,j,k).lt.bmin) then
          bmin = beam(i,j,k)
        endif
      enddo
    enddo
    do j = max(1,j0-nyp+1),min(ny,j0+nyp-1)
      do i = 1,i0-nxp
        if (beam(i,j,k).gt.bmax) then
          bmax = beam(i,j,k)
        elseif (beam(i,j,k).lt.bmin) then
          bmin = beam(i,j,k)
        endif
      enddo
      do i = i0+nxp,nx
        if (beam(i,j,k).gt.bmax) then
          bmax = beam(i,j,k)
        elseif (beam(i,j,k).lt.bmin) then
          bmin = beam(i,j,k)
        endif
      enddo
    enddo
    do j = j0+nyp,ny
      do i = 1,nx
        if (beam(i,j,k).gt.bmax) then
          bmax = beam(i,j,k)
        elseif (beam(i,j,k).lt.bmin) then
          bmin = beam(i,j,k)
        endif
      enddo
    enddo
  enddo
  bgain = max(abs(bmax),abs(bmin))
  bgain = bgain/abs(beam(i0,j0,1))
end subroutine mos_sidelobe
