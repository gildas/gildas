subroutine uvshort_com(line,comm,error,quiet)
  use image_def
  use clean_def
  use clean_arrays
  use short_def
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_com
  !------------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Driving routine for command  
  !       UV_SHORT [?] [/REMOVE]
  !
  !     compute a short spacings uv table from a single-dish table
  !     by gridding, extending to 0, filtering in uv plane, multiplication
  !     by interferometer primary beam, and sampling in uv plane.
  !
  ! input :
  !   a Single-Dish table or Data Cube, and a mosaic UV Table
  ! output :
  !   another Mosaic UV table (the merged one, or the short one,
  !   depending on short_mode, a "debug" level control).
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: line  ! Command line
  character(len=*), intent(in) :: comm  ! Command name
  logical, intent(inout) :: error       ! Error flag
  logical, intent(inout) :: quiet       ! Quiet flag
  !
  !
  !------------------------------------------------------------------------
  !
  !   UV_SHORT TASK versions:
  ! V2.0  Support for Mosaic UV tables, automatic determination of parameters
  !       as much as possible in this case (using the Telescope section if
  !       present)
  ! 'Version 2.0   15-Nov-2016'
  ! V2.1  Reorder parameters and provide sensible defaults
  !       to allow a simpler input file
  ! 'Version 2.1   06-Jun-2017'
  ! V2.2  Fall back on Zero Spacings when SD_DIAM = IP_DIAM
  ! 'Version 2.2   06-Oct-2017'
  ! V2.3  Revise interfaces
  ! 'Version 2.3   06-Apr-2018'!
  ! V2.4  No support for old or debug modes - Trimmed code
  ! 'Version 2.4    10-Apr-2018'
  !
  !   UV_SHORT Command version
  ! V3.0  First working version - Interface through SHORT_* variables
  ! 'Version 3.0    11-Apr-2018'
  ! V3.1  Working version with Zero spacings and Single field 
  ! 'Version 3.1    25-Apr-2018'
  ! V3.2  Define the SHORT data set
  ! 'Version 3.2    20-May-2019'
  ! V3.3  Allow computation of the SHORT data set only (XY_SHORT command)
  ! 'Version 3.3    20-May-2019'
  ! V3.4  Allow non-circular "Short-Spacing" beam 
  ! 'Version 3.4   24-Feb-2020'
  ! V3.5  /CHECK option to verify consistency 
  ! 'Version 3.5   26-Feb-2020'
  ! V3.6  Treat the single field correctly  
  ! 'Version 3.6   27-Feb-2020'
  ! V3.7  Allow minimum Baseline (useful for ACA only data as short spacings)
  ! 'Version 3.7   27-Feb-2020'
  !------------------------------------------------------------------------
  character(len=*), parameter :: version = 'Version 3.9   10-Nov-2021'
  character(len=1), parameter :: question_mark='?'
  !
  integer, parameter :: o_check=1
  integer, parameter :: o_remove=2
  !
  type (short_spacings) :: short  ! Driving parameters
  type (gildas), save :: uvt ! Output UV table (Short spacings only or complete)
  type (gildas), save :: hin ! Input UV table when used to append
  type (gildas), save :: sdt ! Single Dish Table for input (Zero spacings only or complete)
  logical :: lmv_file   ! Is the SHORT data cube already available ?
  logical :: do_zero    ! Should we do only Zero spacings ?
  real, allocatable :: gr_im_w(:)        ! Weights for XY_MAP
  character(len=24) :: argum, topic
  character(len=80) :: chain
  character(len=8) :: rname
  integer :: na, nt
  logical :: change
  logical, save, target :: lresult
  logical, save :: first=.true.
  !
  lresult = .false.  ! By default, data is assumed to be inconsistent 
  !
  if (first) then
    call sic_def_logi('SHORT_STATUS',lresult,.false.,error)
    first = .false.
  endif
  !
  rname = comm
  call map_message(seve%i,rname,version)
  !
  if (comm.eq.'XY_SHORT') then
    if (hsingle%gil%ndim.eq.3) then
      call map_message(seve%i,rname,'Single dish data is already a SHORT image')
      return
    else if (hshort%gil%ndim.eq.3 .and. associated(hshort%r3d)) then
      call map_message(seve%i,rname,'SHORT image has already been computed')
      return
    endif
  else
    if (sic_present(o_remove,0)) then
      !
      ! /REMOVE option
      call uvshort_trim_short(rname, huv, duv, change)
      return
    endif
  endif
  lmv_file = .false.  
  do_zero = .false.
  !
  ! Get the parameters
  call uvshort_params(short,  error)
  if (error) return 
  !
  if (sic_present(0,1)) then
    call sic_ch(line,0,1,argum,na,.true.,error)
    if (argum(1:1).eq.question_mark) then
      if (sic_present(0,2)) then
        call sic_ch(line,0,2,topic,nt,.true.,error)
        chain = 'HELP '//comm//topic
        call exec_program(chain)
        return        
      else if (argum.eq."?") then
        call uvshort_list(short, 1, error)
        return
      else if (argum.eq."??") then
        call uvshort_list(short, 2, error)
        return
      else if (argum.eq."???") then
        call uvshort_list(short, 3, error)
        return
      endif
    endif
  endif
  !
  quiet = .false.
  !
  ! This must be done before other checks, as the number
  ! of visibilities in HUV is used later
  call uvshort_trim_short(rname, huv, duv, change)
  !
  ! Temporary to find out why it does not work...
  if (change) then
    if (sic_present(0,1)) then !S.Guilloteau for tests
      call map_message(seve%w,rname,'  UV data already has Short-Spacings - Re-reading data is recommended')
    else
      call map_message(seve%e,rname,'  UV data already has Short-Spacings - Please Re-read data')
      error = .true.
      return
    endif
  endif
  !
  !
  call uvshort_datas(short, hin, uvt, sdt, hshort, lmv_file, error)
  if (error) return
  !
  ! Consistency check
  if (lmv_file) then
    call uv_short_consistency(rname,uvt,hshort,short%ftole,error)
  else
    call uv_short_consistency(rname,uvt,sdt,short%ftole,error)
  endif
  !
  ! /CHECK only
  lresult = .not.error
  if (sic_present(o_check,0)) then
    error = .false.
    return
  endif
  if (error) return
  !
  ! Parameter checks
  call uvshort_checks(short, hin, uvt, sdt, hshort, lmv_file, do_zero, error)
  if (error) return
  !
  if (comm.eq.'XY_SHORT') then
    if (lmv_file) then
      call map_message(seve%f,rname,'Programming error: lmv_file set in  XY_SHORT')
      error = .true.
      return
    endif
    !
    ! Input file is a single-dish table
    !
    ! Set valid defaults for Class Tables
    if (short%xcol.eq.0) short%xcol = 1
    if (short%ycol.eq.0) short%ycol = 2
    if (short%wcol.eq.0) short%wcol = 3
    if (short%mcol(1).eq.0) short%mcol(1) = 4
    !
    call uvshort_create_lmv(comm,sdt,hshort,gr_im_w,error, &
    & uvt,short)
    if (error) return
    !
    ! Copy back Phase Center ?
    hshort%gil%a0 = sdt%gil%a0
    hshort%gil%d0 = sdt%gil%d0
  else
    !
    uvt%r2d => duv
    call uvshort_fields(short, uvt, error)
    if (error) return 
    nullify(uvt%r2d) 
    !
    if (do_zero) then
      call map_message(seve%w,rname,'Falling back to Zero Spacings')
      rname = 'UV_ZERO'
      call uvshort_dozeros(short, uvt, sdt, hshort, lmv_file, error)
    else
      call uvshort_doshorts(short, hin, uvt, sdt, hshort, lmv_file, error)
      if (error) return
    endif
    if (error) return 
    !
    if (short%mode.gt.0) then
      call uvshort_merge(rname, hin, uvt, error)
      if (error) return
    else
      call uvshort_plug (rname, hin, uvt, error)
    endif
  endif
  !
  ! Define the SHORT variable if it was not already done
  if (lmv_file) return
  !
  ! Convert to Jy/Beam
  hshort%r3d = hshort%r3d * short%sd_factor
  hshort%char%unit = 'Jy/beam'
  call sic_mapgildas('SHORT',hshort,error,hshort%r3d)
end subroutine uvshort_com
!
!--------------------------------------------------------------------------
subroutine uvshort_merge(rname, hin, uvt, error)
  use clean_arrays
  use clean_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_merge
  use gbl_message
  !------------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Command UV_SHORT  - Final merging of Short spacings and UV table
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type (gildas), intent(in) :: uvt    ! Short Spacings UV table 
  type (gildas), intent(inout) :: hin ! Input UV table when used to append
  logical, intent(inout) :: error
  !
  character(len=256) :: mess
  integer :: nu, mvisi
  real, pointer :: duv_previous(:,:), duv_next(:,:)
  !
  ! What can be the logic here ? 
  ! Use the next buffer as usual...
  !
  ! But we have two inputs and one output
  !
  nu = uvt%gil%dim(1)
  mvisi = hin%gil%nvisi + uvt%gil%nvisi
  nullify (duv_previous, duv_next)
  call uv_find_buffers (rname,nu,mvisi,duv_previous,duv_next,error)
  if (error) return
  !
  duv_next(:,1:hin%gil%nvisi) = duv(:,:)  ! or hin%r2d(:,:)
  duv_next(:,hin%gil%nvisi+1:) = uvt%r2d(:,:)
  !
  write(mess,'(i12,a,i12,a)') uvt%gil%nvisi,' Visibilities added (', &
    & hin%gil%nvisi,' before)'
  call map_message(seve%i,rname,mess)
  call uv_clean_buffers (duv_previous, duv_next,error)
  if (error) return
  !
  ! Copy back to UV data set
  call gdf_copy_header(hin,huv,error)
  huv%gil%nvisi = mvisi
  huv%gil%dim(2) = mvisi
  !
  ! Indicate UV data has changed, and weight must be computed
  call uv_new_data(weight=.true.)
  !
end subroutine uvshort_merge
!
!--------------------------------------------------------------------------
subroutine uvshort_plug(rname, hin, uvt, error)
  use clean_arrays
  use clean_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_plug
  use gbl_message
  !------------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Command UV_SHORT  - Put Short Spacings as current UV table
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type (gildas), intent(in) :: uvt ! Short Spacings UV table 
  type (gildas), intent(inout) :: hin ! Input UV table 
  logical, intent(inout) :: error
  !
  character(len=256) :: mess
  integer :: nu, mvisi
  real, pointer :: duv_previous(:,:), duv_next(:,:)
  !
  ! Use the next buffer as usual...
  nu = uvt%gil%dim(1)
  mvisi = uvt%gil%nvisi
  nullify (duv_previous, duv_next)
  call uv_find_buffers (rname,nu,mvisi,duv_previous,duv_next,error)
  if (error) return
  !
  duv_next(:,:) = uvt%r2d(:,:)
  !
  write(mess,'(i12,a,i12,a)') uvt%gil%nvisi,' Visibilities created'
  call map_message(seve%i,rname,mess)
  call uv_clean_buffers (duv_previous, duv_next,error)
  if (error) return
  !
  ! Copy back to UV data set
  call gdf_copy_header(hin,huv,error)
  huv%gil%nvisi = mvisi
  huv%gil%dim(2) = mvisi
  !
  ! Indicate UV data has changed, and weight must be computed
  call uv_new_data(weight=.true.)
end subroutine uvshort_plug
!  
subroutine uvshort_params(short, error)
  use gildas_def
  use short_def
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, only : map_message
  !------------------------------------------------------------------------ 
  ! @ private
  !
  ! IMAGER    Command UV_SHORT
  !   Define input parameters
  !------------------------------------------------------------------------  
  type (short_spacings), intent(out) :: short
  logical, intent(out) :: error
  !
  logical :: err
  integer :: lc
  real :: a_beam
  !
  ! Input parameters
  ! ----------------
  !
  error = .false.
  call sic_get_inte('SHORT_MODE',short%mode,err)
  !
  call sic_get_real('SHORT_SD_FACTOR', short%sd_factor,err)     ! unit conversion factor. 
  !                     If zero, will assume the Jy/K factor from the 
  !                     single dish beam size and observing frequency
  !                     if the unit is K. No default otherwise.
  !
  ! All other beyond can in general be default to 0 in MODE # 0
  !
  call sic_get_real('SHORT_UV_MAX',short%uv_maxr,err)        ! outer truncation radius (m)
  !                     If zero, will be sd_diam - ip_diam
  call sic_get_real('SHORT_UV_MIN',short%uv_minr,err)        ! inner truncation radius (m)
  !                     If zero, will be 0. 
  !
  call sic_get_real('SHORT_SD_WEIGHT', short%sd_weight,err)     ! single dish weight factor, normally 1
  !                     If zero, 1.0 
  !
  !
  call sic_get_real('SHORT_TOLE',short%ptole,err)               ! Pointing tolerance in radians
  !                     If zero, will assume 1/10th of single dish beam  TBD
  call sic_get_real('SHORT_MIN_WEIGHT',short%minw,err)          ! Something like a few 0.01
  !                     If zero, 0.01 
  short%weight_mode = 'NATU'
  lc = 4
  call sic_get_char('SHORT_WEIGHT_MODE',short%weight_mode,lc,err)  ! Normally UNIF or NATU, but little effect
  !
  ! These are only used in the TABLE mode, and can be defaulted to zero
  call sic_get_inte('SHORT_XCOL',short%xcol,err)                ! 1
  call sic_get_inte('SHORT_YCOL',short%ycol,err)                ! 2
  call sic_get_inte('SHORT_WCOL',short%wcol,err)                ! 3
  call sic_get_inte('SHORT_MCOL[1]',short%mcol(1),err)          ! 4 
  call sic_get_inte('SHORT_MCOL[2]',short%mcol(2),err)          ! 0
  !
  ! These normally should be defaulted to 0 if MODE$ # 0 
  a_beam = 0.
  call sic_get_real('SHORT_SD_BEAM', a_beam,err)                ! single dish beam (arcsec)
  if (a_beam.ne.0) then
    short%sd_beam = [a_beam,a_beam,0.0]
  endif
  call sic_get_real('SHORT_SD_DIAM', short%sd_diam,err)         ! single dish diam. (m)
  call sic_get_real('SHORT_IP_BEAM', short%ip_beam,err)         ! interf. primary beam (arcsec)
  call sic_get_real('SHORT_IP_DIAM', short%ip_diam,err)         ! interf. diam. (m)
  !
  ! These are for tests only
  call sic_get_logi('SHORT_DO_SINGLE', short%do_single, err)    ! Correct for Single dish beam
  call sic_get_logi('SHORT_DO_PRIMARY', short%do_primary, err)  ! Correct for Interferometer Primary beam
  !
  if (short%mode.eq.code_short_old .or. abs(short%mode).gt.10) then !SG Begin
    call map_message(seve%e,'UV_SHORT','This mode is not available in this version')
    error = .true.
  else
    short%nf = 0
  endif           ! SG End
  !
end subroutine uvshort_params
!
subroutine uvshort_list(short, quick, error)
  use gildas_def
  use short_def
  use gbl_message
  use gbl_ansicodes
  use imager_interfaces, only : map_message
  use gkernel_interfaces
  !------------------------------------------------------------------------ 
  ! @ private
  !
  ! IMAGER    Command UV_SHORT
  !   List input parameters
  !------------------------------------------------------------------------  
  type (short_spacings), intent(in) :: short
  integer, intent(in)  :: quick
  logical, intent(out) :: error
  !
  real :: a_beam
  !
  ! Input parameters
  ! ----------------
  !
  error = .false.
  write(*,*) '       Purpose               ', &
    '   Variable              Value                Default (Unit)'
  !
  write(*,*) '  Conversion factor          ', &
    c_blue//'   SHORT_SD_FACTOR   '//c_clear//'['//c_red,short%sd_factor,c_clear//' ]    0   (Jy/K, 0=guess)'
  write(*,*) '  Truncation UV distance     ', & 
    c_blue//'   SHORT_UV_MAX      '//c_clear//'['//c_red,short%uv_maxr, c_clear//' ]    0.0  '
  write(*,*) '  Shortest UV baseline       ', & 
    c_blue//'   SHORT_UV_MIN      '//c_clear//'['//c_red,short%uv_minr, c_clear//' ]    0.0  '
  write(*,*) '  Weight factor              ', &
    c_blue//'   SHORT_SD_WEIGHT   '//c_clear//'['//c_red,short%sd_weight,c_clear//' ]    1.0 '
  !
  if (quick.eq.1) return
  write(*,*) '   --- Advanced control parameters ---'
  write(*,*) '  Position tolerance         ', &
    c_blue//'   SHORT_TOLE        '//c_clear//'['//c_red,short%ptole,   c_clear//' ]    0.1'
  write(*,*) '  Minimal relative weight    ', &
    c_blue//'   SHORT_MIN_WEIGHT  '//c_clear//'['//c_red,short%minw,    c_clear//' ]    0.01'
  !
  write(*,*) '   --- In case they are missing... ---'
  a_beam = short%sd_beam(1)
  write(*,*) '  Single dish beamsize       ', &
    c_blue//'   SHORT_SD_BEAM     '//c_clear//'['//c_red,       a_beam,  c_clear//' ]    0.0    (arcsec)'
  write(*,*) '  Interferometer dish beam   ', &
    c_blue//'   SHORT_IP_BEAM     '//c_clear//'['//c_red,short%ip_beam,  c_clear//' ]    0.0    (arcsec)'
  write(*,*) '  Single Dish diameter       ', &
    c_blue//'   SHORT_SD_DIAM     '//c_clear//'['//c_red,short%sd_diam,  c_clear//' ]    0.0    (m)'
  write(*,*) '  Interferometer dish diame. ', &
    c_blue//'   SHORT_IP_DIAM     '//c_clear//'['//c_red,short%ip_diam,  c_clear//' ]    0.0    (m)'
  !
  if (quick.eq.2) return
  write(*,*) '   --- For tests and debug only   ---'
  write(*,*) '  Mode                       ', &
    c_blue//'   SHORT_MODE        '//c_clear//'['//c_red,short%mode,c_clear//'      ]    3   '
  write(*,*) '  Weighting mode             ', &
    c_blue//'   SHORT_WEIGHT_MODE '//c_clear//'[   ',short%weight_mode, c_clear//'            ]    NATURAL'
  write(*,*) '  X column                   ', &
    c_blue//'   SHORT_XCOL        '//c_clear//'['//c_red,short%xcol,     c_clear//'      ]    0 => 1'
  write(*,*) '  Y column                   ', &
    c_blue//'   SHORT_YCOL        '//c_clear//'['//c_red,short%ycol,     c_clear//'      ]    0 => 2'
  write(*,*) '  Weight column              ', &
    c_blue//'   SHORT_WCOL        '//c_clear//'['//c_red,short%wcol,     c_clear//'      ]    0 => 3'
  write(*,'(A,A,I7,A,I9,A)') '   Column range              ', &
    c_blue//'    SHORT_MCOL        '//c_clear//'['//c_red, &
    & short%mcol(1),',',short%mcol(2),  c_clear//'  ]    0,0 => 4,*'
  !
  ! These are for tests only
  if (short%do_single) then
    write(*,*) '  Correct for S.-D. beam     ', &
      c_blue//'   SHORT_DO_SINGLE   '//c_clear//'[   YES             ]    YES'
  else
    write(*,*) '  Correct for S.-D. beam     ', &
      c_blue//'   SHORT_DO_SINGLE   '//c_clear//'[    NO             ]    YES'
  endif
  if (short%do_primary) then
    write(*,*) '  Correct for Interf. Beam   ', &
      c_blue//'   SHORT_DO_PRIMARY  '//c_clear//'[   YES             ]    YES'
  else
    write(*,*) '  Correct for Interf. Beam   ', &
      c_blue//'   SHORT_DO_PRIMARY  '//c_clear//'[    NO             ]    YES'
  endif
  !
end subroutine uvshort_list
!
subroutine uvshort_datas(short, hin, uvt, sdt, lmv, lmv_file, error)
  use image_def
  use clean_def
  use clean_arrays
  use gbl_format
  use gbl_message
  use short_def
  use gkernel_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_datas 
  !------------------------------------------------------------------------  
  ! @ private
  !
  ! IMAGER      Command UV_SHORT
  !   Retrieve data headers 
  !------------------------------------------------------------------------  
  type(short_spacings), intent(inout)   :: short
  !
  type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
  type (gildas), intent(inout) :: uvt ! Input/Output UV table (Short spacings only or complete)
  type (gildas), intent(inout) :: hin ! Input UV table when used to append
  type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
  logical, intent(out) :: lmv_file  
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_SHORT'
  !
  if (huv%loca%size.eq.0) then
    call map_message(seve%f,rname,'No UV table')
    error = .true. 
    return
  endif
  !
  if (hsingle%loca%size.eq.0) then
    call map_message(seve%f,rname,'No input Single Dish data')
    error = .true.
    return
  endif
  !
  !
  ! Open the input UV table and read its header
  call gildas_null(uvt, type='UVT')
  call gdf_copy_header(huv,uvt,error)
  if (error) return
  !
  ! Test whether UV table is to be appended or not
  call gildas_null(hin, type='UVT')
  call gdf_copy_header(huv,hin,error)
  !
  ! Find out if LMV or SDT file
  call gildas_null(lmv)
  call gildas_null(sdt)
  !
  call gdf_copy_header(hsingle,lmv,error)  ! Always needed
  if (hsingle%gil%ndim.eq.3) then
    lmv_file = .true.
    lmv%r3d => dsingle  
  else if (hsingle%gil%ndim.eq.2) then
    lmv_file = .false.
    call gdf_copy_header(hsingle,sdt,error)    
    sdt%r2d => dsingle(:,:,1)
  endif
  !
end subroutine uvshort_datas
!
subroutine uvshort_fields(short, uvt, error)
  use image_def
  use gbl_format
  use gbl_message
  use short_def
  use clean_arrays ! For DUV global
  use gkernel_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_fields
  !------------------------------------------------------------------------  
  ! @ private
  !
  ! IMAGER      Command UV_SHORT
  !   Retrieve number of fields and fields coordinates
  !------------------------------------------------------------------------  
  type(short_spacings), intent(inout)   :: short
  type (gildas), intent(inout) :: uvt ! Input UV table header
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: f_to_k=2.d0*pi/299792458.d-6
  real(8), parameter :: clight=299792458d0
  real(8), parameter :: kbolt=1.3806488d-23
  real, parameter :: rad_to_sec=180.0*3600.0/pi
  !
  ! Local variables
  !
  integer np,i,ier,k,l
  character(len=80) mess
  !
  character(len=*), parameter :: rname='UV_SHORT'
  !
  integer :: xoff, yoff, loff, moff, ioff, joff, nv, nf
  real, allocatable :: offxy(:,:) 
  real(8) :: dx, dy
  type(projection_t) :: uv_proj
  logical :: do_off
  !
  error = .false.
  !
  ! Check if the UV Table is given as a Mosaic already
  !
  ! Recover the number of Fiels
  loff = uvt%gil%column_pointer(code_uvt_loff)
  moff = uvt%gil%column_pointer(code_uvt_moff)
  !
  xoff = uvt%gil%column_pointer(code_uvt_xoff)
  yoff = uvt%gil%column_pointer(code_uvt_yoff)
  !
  do_off = .true.
  if (abs(short%mode).eq.code_short_phase) then
    if (loff.ne.0 .or. moff.ne.0) then
      if (moff.ne.loff+1) then
        call map_message(seve%f,rname,'Improper Mosaic UV table')
        error = .true.
        return
      endif
      ioff = loff
      joff = moff
      write(mess,'(A,I0,A,I0)') 'Phase offset columns at Loff ',loff,', Moff ',moff
      call map_message(seve%i,rname,mess)
    else
      call map_message(seve%f,rname,'Mosaic UV table does not have Phase offsets')
      error = .true.
      return
    endif
  else if (abs(short%mode).eq.code_short_point) then
    if (xoff.ne.0 .or. yoff.ne.0) then
      if (yoff.ne.xoff+1) then
        call map_message(seve%f,rname,'Improper Mosaic UV table')
        error = .true.
        return
      endif
      ioff = xoff
      joff = yoff
      write(mess,'(A,I0,A,I0)') 'Pointing offset columns at Xoff ',xoff,', Yoff ',yoff
      call map_message(seve%i,rname,mess)
    else
      call map_message(seve%f,rname,'Mosaic UV table does not have Pointing offsets')
      error = .true.
      return
    endif
  else
    !
    ! Automatic determination
    if (loff.ne.0 .or. moff.ne.0) then
      if (moff.ne.loff+1) then
        call map_message(seve%f,rname,'Improper Mosaic UV table')
        error = .true.
        return
      endif
      ioff = loff
      joff = moff
      write(mess,'(A,I0,A,I0)') 'Phase offset columns at Loff ',loff,', Moff ',moff
      call map_message(seve%i,rname,mess)
      short%mode = sign(code_short_phase,short%mode) ! Preserve Sign 
    else if (xoff.ne.0 .or. yoff.ne.0) then
      ioff = xoff
      joff = yoff
      write(mess,'(A,I0,A,I0)') 'Pointing offset columns at Xoff ',xoff,', Yoff ',yoff
      call map_message(seve%i,rname,mess)
      short%mode = sign(code_short_point,short%mode) ! Preserve Sign 
    else
      call map_message(seve%w,rname,'Input UV table is a Single field')
      do_off = .false.
      nf = 1
      allocate(offxy(2,1),stat=ier)
      ! Find off the corresponding offset
      call gwcs_projec(uvt%gil%a0,uvt%gil%d0,uvt%gil%pang,uvt%gil%ptyp,uv_proj,error)
      call abs_to_rel(uv_proj,uvt%gil%ra,uvt%gil%dec,dx,dy,1)
      offxy(:,1) =[dx,dy]
    endif      
  endif
  !
  np = uvt%gil%dim(1)
  nv = uvt%gil%nvisi
  if (do_off) then
    call mosaic_getfields (duv,np,nv,ioff,joff,nf,offxy)
  endif
  !
  ! nf is the number of fields
  ! offxy(2,nf) are the offsets
  nf = max(nf,1)
  !
  allocate(short%raoff(nf),short%deoff(nf),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for Offsets')
    error = .true.
    return
  endif
  short%raoff(:) = offxy(1,:)
  short%deoff(:) = offxy(2,:)
  !
  ! Get the RA and DEC - One may use variable "new", but currently, just fill chra & chde
  ! for simplicity, just fill CHRA & CHDE as "empty"
  short%chra = " "
  short%chde = " "
  !
  ! We may take the IP_BEAM and IP_DIAM from the UV Table here
  write(mess,'(A,I0,A)') 'Found ',nf,' fields at [Ra, Dec] offsets'
  !
  call map_message(seve%i,rname,mess)
  do i = 3,nf,3
    k = i-2
    l = i-1
    write(6,100)  &
    & '     [',short%raoff(k)*180*3600/pi,', ',short%deoff(k)*180*3600/pi,'] ', &
    & '     [',short%raoff(l)*180*3600/pi,', ',short%deoff(l)*180*3600/pi,'] ', &
    & '     [',short%raoff(i)*180*3600/pi,', ',short%deoff(i)*180*3600/pi,'] '
  enddo
  if (mod(nf,3).eq.2) then
    k = nf-1
    l = nf
    write(6,100) &
    & '     [',short%raoff(k)*180*3600/pi,', ',short%deoff(k)*180*3600/pi,'] ', &
    & '     [',short%raoff(l)*180*3600/pi,', ',short%deoff(l)*180*3600/pi,'] '
  else if (mod(nf,3).eq.1) then
    k = nf
    write(6,100) &
    & '     [',short%raoff(k)*180*3600/pi,', ',short%deoff(k)*180*3600/pi,'] '
  endif
  short%nf = nf 
  !
100   format(A,f8.2,a,F8.2,A,A,f8.2,a,F8.2,A,A,f8.2,a,F8.2,A)
end subroutine uvshort_fields
!
subroutine uvshort_checks(short, hin, uvt, sdt, lmv, lmv_file, do_zero, error)
  use image_def
  use gbl_format
  use gbl_message
  use short_def
  use gkernel_types
  use gkernel_interfaces
  use imager_interfaces, only : mosaic_getfields, map_message, telescope_beam
  !------------------------------------------------------------------------  
  ! @ private
  !
  ! IMAGER      Command UV_SHORT
  !   Check parameters
  !------------------------------------------------------------------------  
  type(short_spacings), intent(inout)   :: short
  type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
  type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
  type (gildas), intent(inout) :: hin ! Input UV table when used to append
  type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
  logical, intent(in) :: lmv_file
  logical, intent(out) :: do_zero
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: f_to_k=2.d0*pi/299792458.d-6
  real(8), parameter :: clight=299792458d0
  real(8), parameter :: kbolt=1.3806488d-23
  !
  ! Local variables
  real :: diam_sd, beam_sd(3), diam_ip, beam_ip, beam_te(2)
  real, parameter :: beam_tole=0.05
  real, parameter :: all_tole=0.001
  real, parameter :: diam_tole=0.02
  integer :: sever
  logical :: allow_diam_overrun
  logical :: wrong_diam
  !
  character(len=128) mess
  !
  character(len=*), parameter :: rname='UV_SHORT'
  !
  error = .false.
  !
  call map_message(seve%i,rname,'Weighting mode is '//short%weight_mode)
  if (.not.short%do_single) then
    call map_message(seve%w,rname,' ')
    call map_message(seve%w,rname,'  *** DO_SINGLE is not set *** Test mode only !')
    call map_message(seve%w,rname,' ')
  endif
  if (.not.short%do_primary) then
    call map_message(seve%w,rname,' ')
    call map_message(seve%w,rname,'  *** DO_PRIMARY is not set *** Test mode only !')
    call map_message(seve%w,rname,' ')
  endif
  !
  allow_diam_overrun = abs(short%mode).gt.code_short_auto ! 4 or -4
  !
  ! Single-Dish Table or LMV cube
  !
  ! Check Beam size
  wrong_diam = .false.
  beam_sd = 0.
  if (.not.lmv_file) then
    ! Class Table: gridding required
    ! Beam from telescope description
    beam_sd(1:2) = telescope_beam(rname,sdt)
  else  if (lmv%gil%reso_words.ne.0) then
    ! Data cube: Beam from Angular resolution section
    beam_sd = [lmv%gil%majo,lmv%gil%mino,lmv%gil%posa]
    !
    ! Check if it is larger than the one from the Telescope section. 
    !     If yes, then the LMV data is probably a smoothed image
    !   made in single-dish from this Telescope
    !     If not, the LMV data is probably an interferometric image
    !   made from the specified Telescope
    if (lmv%gil%nteles.ne.0) then
      beam_te(1:2) = telescope_beam(rname,lmv)
      if (abs(beam_te(1)-beam_sd(1)).gt.beam_tole*beam_sd(1)) wrong_diam = .true.
      if (abs(beam_te(2)-beam_sd(2)).gt.beam_tole*beam_sd(2)) wrong_diam = .true.
    endif      
  endif
  if (wrong_diam) allow_diam_overrun = .true.
  !
  if (beam_sd(1).ne.0) then
    beam_sd(1:2) = beam_sd(1:2)*3600*180/pi
    !
    ! OK we have a value...
    if (short%sd_beam(1).eq.0) then
      write(mess,'(A,F6.1,A,F6.1,A,F6.1,A)') 'Using SD Beam derived from data: ', &
        &   beam_sd(1),' by',beam_sd(2),'" at PA ',beam_sd(3)*180./pi,' deg'
      short%sd_beam = beam_sd
      sever = seve%i
    else
      ! The logic should be changed..
      if (abs(beam_sd(1)-short%sd_beam(1)).gt.short%sd_beam(1)*beam_tole) then
        if (allow_diam_overrun) then
          write(mess,'(A,F6.1,A,F6.1,A)') 'SD beam: ',short%sd_beam(1), &
            & ' m overrides Telescope beam: ',beam_sd(1),'"'
          sever = seve%w
        else
          write(mess,'(A,F6.1,A,F6.1,A)') 'SD beam: ',short%sd_beam(1), &
            & ' m does not match Telescope beam: ',beam_sd(1),'"'
          sever = seve%w
        endif
      else
        write(mess,'(A,F6.1,A,F6.1,A)') 'Used SD beam: ',short%sd_beam(1), &
        & ' m does not match Telescope beam: ',beam_sd(1),'"'
        sever = seve%w
      endif
    endif
  else
    write(mess,'(A,F6.1,A)') 'Using SD beam: ',short%sd_beam(1),'"'
    sever = seve%i
  endif
  !
  short%sd_beam(1:2) = short%sd_beam(1:2)*pi/180/3600 ! Go back to Radians
  call map_message(sever,rname,mess)
  !
  ! Check Diameter and truncation
  if (lmv%gil%nteles.ne.0) then
    diam_sd = lmv%gil%teles(1)%diam
    if (short%sd_diam.eq.0) then
      if (wrong_diam) then
        write(mess,'(A,F6.1,A)') &
          &   'SD diameter derived from Telescope section: ', &
          &   diam_sd,' m, is inappropriate for this usage'
        call map_message(seve%w,rname,mess,3)
        !
        write(mess,'(A,F6.1,A)') 'Using SD diameter: ',short%sd_diam,' m'
        sever = seve%w
      else
        write(mess,'(A,F6.1,A)') & 
          &   'Using SD diameter derived from Telescope section: ', &
          &   diam_sd,' m'
        sever = seve%i
        short%sd_diam = diam_sd
      endif
    else if (abs(diam_sd-short%sd_diam).gt.short%sd_diam*all_tole) then
      if (abs(diam_sd-short%sd_diam).gt.short%sd_diam*diam_tole) then
        if (allow_diam_overrun) then
          write(mess,'(A,F6.1,A,F6.1,A)') 'SD diameter: ',short%sd_diam, &
            & ' m overrides Telescope diameter: ',diam_sd,' m'
          sever = seve%w
        else
          write(mess,'(A,F6.1,A,F6.1,A)') 'SD diameter: ',short%sd_diam, &
            & ' m does not match Telescope diameter: ',diam_sd,' m'
          error = .true.
          sever = seve%w
        endif
      else
        write(mess,'(A,F6.1,A,F6.1,A)') 'Used SD diameter: ',short%sd_diam, &
        & ' m does not match Telescope diameter: ',diam_sd,' m'
        sever = seve%w
      endif
    else
      write(mess,'(A,F6.1,A)') 'Using SD diameter: ',short%sd_diam,' m'
      sever = seve%i
    endif
  else
    write(mess,'(A,F6.1,A)') 'Using SD diameter: ',short%sd_diam,' m'
    sever = seve%i
  endif
  call map_message(sever,rname,mess)
  !
  ! Interferometer Dish Diameter
  !
  if (uvt%gil%nteles.ne.0) then
    diam_ip = uvt%gil%teles(1)%diam
    if (short%ip_diam.eq.0) then
      write(mess,'(A,F6.1,A)') 'Using IP diameter derived from Telescope section: :', &
          & diam_ip,' m'
      sever = seve%i
      short%ip_diam = diam_ip
    else if (abs(diam_ip-short%ip_diam).gt.short%ip_diam*all_tole) then
      write(mess,'(A,F5.1,A,F5.1,A)') 'Used IP diameter:',short%ip_diam, &
        & ' m does not match Telescope diameter:',diam_ip,' m'
      if (abs(diam_ip-short%ip_diam).gt.short%ip_diam*diam_tole) then
        error = .true.
        sever = seve%e
      else
        sever = seve%w
      endif
    else
      write(mess,'(A,F6.1,A)') 'Using IP diameter: ',short%ip_diam,' m'
      sever = seve%i
    endif
  else
    write(mess,'(A,F6.1,A)') 'Using IP diameter: ',short%ip_diam,' m'
    sever = seve%i
  endif
  call map_message(sever,rname,mess)
  !
  ! Interferometer Primary Beam size
  beam_ip = telescope_beam(rname,uvt)
  if (beam_ip.eq.0.0) then
    write(mess,'(A,F8.2,A)') 'Using specified IP beam: ',short%ip_beam,'"'
    sever = seve%i
  else
    short%ip_beam = beam_ip*180*3600/pi
    write(mess,'(A,F8.2,A)') 'Using IP Beam derived from data: ', &
      &   short%ip_beam,'"'
    sever = seve%i
  endif
  short%ip_beam = short%ip_beam*pi/180/3600
  call map_message(sever,rname,mess)
  !
  !
  ! Verify all parameters are known and reasonable
  if (short%sd_beam(1).eq.0) then
    call map_message(seve%e,rname,'SD_BEAM must be specified',1)
    error = .true.
  endif
  if (short%ip_beam.eq.0) then
    call map_message(seve%e,rname,'IP_BEAM must be specified',1)
    error = .true.
  endif
  if (short%sd_diam.eq.0) then
    call map_message(seve%e,rname,'SD_DIAM must be specified',1)
    error = .true.
  endif
  if (short%ip_diam.eq.0) then
    call map_message(seve%e,rname,'IP_DIAM must be specified',1)
    error = .true.
  endif
  if (error) return
  !
  ! Verify if Short or Zero spacings can be computed
  if (abs(short%sd_diam-short%ip_diam).le.all_tole*short%sd_diam) then
    call map_message(seve%i,rname,'SD diameter = IP diameter, using Zero spacing only')
    do_zero = .true.
  else if (short%sd_diam.lt.short%ip_diam) then
    call map_message(seve%f,rname,'SD diameter too small')
    error = .true.
    return
  else  
    do_zero = .false.
  endif
  !
  ! Check the Unit conversion factor
  if (short%sd_factor.eq.0.0) then
    if (lmv%gil%freq.eq.0.0) then
      mess = 'SHORT_SD_FACTOR not specified, and no frequency in input data'
      call map_message(seve%e,rname,mess)
      error = .true.
    else if (lmv%char%unit.ne.'K') then
      if (lmv%char%unit(1:2).eq.'K ') then
        short%sd_factor = 2.0*kbolt*1d26*pi*short%sd_beam(1)*short%sd_beam(2) &
          & / (4.d0*log(2.d0))*(lmv%gil%freq*1d6/clight)**2
        if (lmv%char%unit.ne.'K (Tmb)') then
          mess = 'SHORT_SD_FACTOR not specified, and unit is not K, but '//lmv%char%unit 
          call map_message(seve%e,rname,mess)
          error = .true.
        endif
        write(mess,'(A,1PG11.3,A)') 'Unit conversion factor would be ',short%sd_factor,' Jy per K (Tmb)'
        call map_message(seve%w,rname,mess)
      else if (lmv%char%unit.eq.'Jy/beam') then
        short%sd_factor = 1.0
        call map_message(seve%w,rname,'Single dish data already in Jy/beam')
      else 
        mess = 'SHORT_SD_FACTOR not specified, and unit is not K or Jy/beam, but '//lmv%char%unit 
        call map_message(seve%e,rname,mess)
        error = .true.
      endif
    else
      short%sd_factor = 2.0*kbolt*1d26*pi*short%sd_beam(1)*short%sd_beam(2) &
        & / (4.d0*log(2.d0))*(lmv%gil%freq*1d6/clight)**2
      write(mess,'(A,1PG11.3,A)') 'Unit conversion factor set to ',short%sd_factor,' Jy per K'
      call map_message(seve%i,rname,mess)
    endif
    if (error) return
  endif
  !
  ! Check UV_MAX
  if (.not.do_zero) then
    if (short%uv_maxr.eq.0.0) then
      call map_message(seve%w,rname,'Setting default truncation radius')
      short%uv_maxr = short%sd_diam-short%ip_diam
    else if (short%uv_maxr-short%sd_diam+short%ip_diam.gt.0.0) then
      call map_message(seve%w,rname,'Incoherent input parameters, re-setting SHORT_UV_')
      short%uv_maxr = short%sd_diam-short%ip_diam
    endif
    write(mess,'(A,F5.1,A)') 'Using UV truncation radius:  ',short%uv_maxr,' m'
    call map_message(seve%i,rname,mess)
  endif
  !
  ! Check TOLE
  if (short%ptole.eq.0.0) then
    short%ptole = short%sd_beam(2)/16.0 
    write(mess,'(A,F8.2,A)') 'Position tolerance SHORT_TOLE set to SHORT_SD_BEAM/16.0 = ', &
    & short%ptole*180.*3600./pi,' "'
    call map_message(seve%i,rname,mess)
  endif
  !
  ! Check SD_WEIGHT
  if (short%sd_weight.eq.0.0) then
    short%sd_weight = 1.0
    call map_message(seve%i,rname,'Weight scaling factor SHORT_SD_WEIGHT set to 1.0')
  endif
  !
  ! Check MIN_WEIGHT
  if (short%minw.eq.0.0) then
    short%minw = 0.01
    call map_message(seve%i,rname,'Minimum weight SHORT_MIN_WEIGHT set to 0.01')
  endif
  !
  !-----------------------------------------------------------------------
end subroutine uvshort_checks
!
subroutine uvshort_dozeros(short, uvt, sdt, lmv, lmv_file, error)
  use image_def
  use gbl_format
  use gbl_message
  use short_def
  use gkernel_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_dozeros
  !------------------------------------------------------------------------  
  ! @ private
  !
  ! IMAGER      Command UV_SHORT
  !   Treat the Zero spacing case
  !------------------------------------------------------------------------  
  type(short_spacings), intent(inout)   :: short
  type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
  type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
  type (gildas), intent(inout) :: sdt ! Single Dish Table for input
  logical, intent(in) :: lmv_file
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: f_to_k=2.d0*pi/299792458.d-6
  real(8), parameter :: clight=299792458d0
  real(8), parameter :: kbolt=1.3806488d-23
  !
  character(len=120) :: mess
  character(len=*), parameter :: rname='UV_ZERO'
  !
  ! Zero spacings
  integer :: ier, if, nc, nx, ny
  type(projection_t) :: uv_proj, lm_proj  
  real(8) :: alpha,delta,dx,dy
  integer :: ix,iy,is,icol,ocol,jf
  integer :: gdate, last, ixcol, iycol
  real, pointer :: zero_uvdata(:,:)
  real, pointer :: dish_data(:,:)
  real, pointer :: cube_data(:,:,:)
  real, allocatable :: spectrum(:)
  real :: ws, dist, dmin, total_weight, scale_weight,weight
  real, parameter :: rad_to_sec=180.0*3600.0/pi
  !
  ! Consistency check
  if (lmv_file) then
    nc = lmv%gil%dim(3)
  else
    nc = uvt%gil%nchan
  endif
  !
  ! We already have the reference UV table header here
  ! Allocate its work space and a spectrum
  !
  allocate(uvt%r2d(uvt%gil%dim(1),short%nf),spectrum(nc),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Zero spacing Memory allocation failure')
    error = .true.
    return
  endif
  !
  call gwcs_projec(uvt%gil%a0,uvt%gil%d0,uvt%gil%pang,uvt%gil%ptyp,uv_proj,error)
  call sic_gagdate(gdate)
  ! ...
  last = uvt%gil%fcol + uvt%gil%natom * uvt%gil%nchan - 1
  ixcol = last+1
  iycol = last+2
  !
  zero_uvdata => uvt%r2d
  !
  if (lmv_file) then
    ! Input file is a lmv Cube
    !
    nx=lmv%gil%dim(1)
    ny=lmv%gil%dim(2)
    cube_data => lmv%r3d
    !
    ! We can get the weight from the Noise in the map
    weight = short%sd_factor*max(lmv%gil%rms, lmv%gil%noise)
    if (weight.ne.0.0) then
      weight = 1d-6/weight**2
      ! weight = sd_weight * weight ! Is that useful ?...
    else
      call map_message(seve%w,rname,'No weight in Single Dish data cube')
      weight = 1.0
    endif
    !
    ! Extract appropriate directions from it: loop on fields
    call gwcs_projec(lmv%gil%a0,lmv%gil%d0,lmv%gil%pang,lmv%gil%ptyp,lm_proj,error)
    do if=1,short%nf
      call rel_to_abs(uv_proj,dble(short%raoff(if)),dble(short%deoff(if)),alpha,delta,1)
      call abs_to_rel(lm_proj,alpha,delta,dx,dy,1)
      ix = nint((dx-lmv%gil%val(1))/lmv%gil%inc(1)+lmv%gil%ref(1))
      iy = nint((dy-lmv%gil%val(2))/lmv%gil%inc(2)+lmv%gil%ref(2))
      if (ix.lt.1 .or. ix.gt.nx .or. iy.lt.1 .or. iy.gt.ny) then
        write(mess,*) 'Field ',if,' is out of map ',ix,iy
        call map_message(seve%e,rname,mess)
        error = .true.
        return
      endif
      !
      ! Put it in place...
      spectrum(:) = short%sd_factor * cube_data(ix,iy,:) 
      call spectrum_to_zero(nc,spectrum,zero_uvdata(:,if),gdate,weight)
      zero_uvdata(ixcol,if) = short%raoff(if)
      zero_uvdata(iycol,if) = short%deoff(if)        
    enddo
    !
    total_weight = short%nf*weight
  else
    !
    !
    ! Point towards the data section of the Single Dish Table
    dish_data => sdt%r2d
    !      
    ! Set valid defaults for Class Tables
    if (short%xcol.eq.0) short%xcol = 1
    if (short%ycol.eq.0) short%ycol = 2
    if (short%wcol.eq.0) short%wcol = 3
    if (short%mcol(1).eq.0) short%mcol(1) = 4
    icol = short%mcol(1)
    if (short%mcol(2).ne.0) then
      ocol = short%mcol(2)
      if (ocol-icol+1.ne.nc) then
        Print *,'MCOL ',short%mcol,' ICOL OCOL ',icol,ocol,' NC ',nc
        call map_message(seve%e,rname,'Number of channels mismatch')
        error = .true.
        return
      endif
    else
      ocol = icol+nc-1
      if (ocol.gt.sdt%gil%dim(1)) then
        Print *,'MCOL ',short%mcol,' ICOL OCOL ',icol,ocol,' Sdt ',sdt%gil%dim(1)
        call map_message(seve%e,rname,'Number of channels mismatch')
        error = .true.
        return
      endif
    endif
    !
    ! Find all spectra towards the appropriate directions,
    ! with the specified tolerance
    total_weight =0.0
    call gwcs_projec(sdt%gil%a0,sdt%gil%d0,sdt%gil%pang,sdt%gil%ptyp,lm_proj,error)
    do if=1,short%nf
      spectrum(:) = 0.0
      weight = 0.0
      jf = 0
      dmin = 1e36
      !
      call rel_to_abs(uv_proj,dble(short%raoff(if)),dble(short%deoff(if)),alpha,delta,1)
      call abs_to_rel(lm_proj,alpha,delta,dx,dy,1)
      !
      do is=1,sdt%gil%dim(2)
        dist = (dish_data(short%xcol,is)-dx)**2+(dish_data(short%ycol,is)-dy)**2
        if (dist .le. short%ptole**2) then
          jf = jf+1
            !Print *,'Field ',if,' spec ',is, &
            ! & '  X',(dish_data(short%xcol,is)-dx)*rad_to_sec,        & 
            ! & '  Y',(dish_data(short%ycol,is)-dy)*rad_to_sec,         &
            ! & ' Tole ',short%ptole*rad_to_sec
          ws = dish_data(short%wcol,is)
          weight = weight + ws
          spectrum(:) = spectrum(:) + ws * dish_data(icol:ocol,is)
        endif 
        dmin = min(dmin,dist)
      enddo
      dmin = sqrt(dmin)*rad_to_sec
      if (weight.eq.0) then
        write(mess,'(A,I0,A,1PG10.2,A)') 'Field ',if,' has no Zero spacing' &
        &  //' Min distance ',dmin,'"'
        call map_message(seve%e,rname,mess)
        error = .true.
      else
        write(mess,'(I0,A,I0,A,1PG10.2,A)') jf,' spectra added to Field ',if &
        &  ,'; Min distance ',dmin,'"'
        call map_message(seve%i,rname,mess)
        total_weight = total_weight + weight
      endif
      !
      ! Put it in place...
      spectrum = spectrum/weight
      call spectrum_to_zero(nc,spectrum,zero_uvdata(:,if),gdate,weight)
      zero_uvdata(ixcol,if) = short%raoff(if)
      zero_uvdata(iycol,if) = short%deoff(if)        
    enddo
  endif
  if (error) return
  !
  ! Re-normalize the Weight to have a nice beam, including
  ! User-specified Weight factor SD_WEIGHT
  scale_weight = weight*short%sd_weight/4.0/total_weight
  do if=1,short%nf
    do is = 10,7+3*nc,3
      zero_uvdata(is,if) = zero_uvdata(is,if) * scale_weight
    enddo
  enddo
  !
  write(mess,'(I0,A)') short%nf,' Zero spacings computed'
  call map_message(seve%i,rname,mess)
  uvt%gil%nvisi = short%nf     ! Update the number of visibilities
  !  
end subroutine uvshort_dozeros

subroutine uvshort_doshorts(short, hin, uvt, sdt, lmv, lmv_file, error)
  use image_def
  use gbl_format
  use gbl_message
  use short_def
  use gkernel_types
  use clean_arrays
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_doshorts
  !$ use omp_lib
  !------------------------------------------------------------------------  
  ! @ private
  !
  ! IMAGER    Command UV_SHORT
  !   Treat the Short spacings case
  !------------------------------------------------------------------------  
  type(short_spacings), intent(inout)   :: short
  type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
  type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
  type (gildas), intent(inout) :: hin ! Input UV table when used to append
  type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
  logical, intent(in) :: lmv_file
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: f_to_k=2.d0*pi/299792458.d-6
  real(8), parameter :: clight=299792458d0
  real(8), parameter :: kbolt=1.3806488d-23
  !
  real, allocatable :: tmp_data(:,:)
  real, pointer :: uvt_data(:,:)
  real, allocatable :: gr_im_w(:)            ! gridded weights in image plane
  complex, allocatable :: gr_uv_w(:,:)       ! gridded weights in UV plane
  real, allocatable :: sd_lobe(:,:)          ! Single dish primary beam
  real, allocatable :: int_lobe(:,:)         ! Interferometer primary beam
  real, allocatable :: fftws(:)              ! FFT work space
  complex, allocatable :: ws_data(:,:,:)     ! Work space of SD UV table values
  complex, allocatable :: int_lobe_comp(:,:) ! Interferometer beam in complex plane
  !
  character(len=*), parameter :: rname='UV_SHORT'
  !
  integer :: nvis, ovis
  integer :: i, ier, if, nc, ndim, nf, nu, nx, ny, nn(2)
  character(len=120) :: mess
  character(len=8) :: string
  !
  integer :: xoff, yoff, loff, moff, nprec, j, jcol
  real(8) :: freq
  real(4) :: umax, umax2, u2
  real(4) :: tweight, scale_weight
  real(8), allocatable :: rpos(:,:)
  real(4) :: cs(2)
  logical :: precise = .true.
  !
  real, pointer :: lmv_data(:,:,:)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! From here, this is only for Short Spacings
  !
  if (short%mode.gt.0) then
    string = 'appended'
  else
    string = 'written'
  endif
  ! 
  if (lmv_file) then
    !
    ! Input file is a lmv cube
    ! What will we do about weights ?  Set them to 1.0 (Uniform...)
    allocate(gr_im_w(2*lmv%gil%dim(1)*lmv%gil%dim(2)),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate memory for work space')
      error = .true.
      return
    endif
    gr_im_w=1.0
  else
    !
    ! Input file is a single-dish table
    !
    ! Set valid defaults for Class Tables
    if (short%xcol.eq.0) short%xcol = 1
    if (short%ycol.eq.0) short%ycol = 2
    if (short%wcol.eq.0) short%wcol = 3
    if (short%mcol(1).eq.0) short%mcol(1) = 4
    !
    call uvshort_create_lmv(rname,sdt,lmv,gr_im_w,error, &
    & uvt,short)
    !
    ! Copy back Phase Center
    hsingle%gil%a0 = sdt%gil%a0
    hsingle%gil%d0 = sdt%gil%d0
  endif
  if (error) return
  !
  lmv_data => lmv%r3d 
  !
  !
  nx=lmv%gil%dim(1)
  ny=lmv%gil%dim(2)
  nc=lmv%gil%dim(3)
  nf=short%nf
  !
  !=====================================================================
  !
  ! Processing for all types .lmv or .tab of input file
  !
  !=====================================================================
  !
  ! Memory allocations
  !--------------------
  !
  error = .true.
  allocate(ws_data(nx,ny,nc),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for work space')
    return
  endif
  allocate(sd_lobe(nx,ny),stat=ier)       ! for SD primary beam
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for SD primary beam')
    return
  endif
  allocate(int_lobe(nx,ny),stat=ier)      ! for interferometer beam
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for interferometer beam')
    return
  endif
  allocate(gr_uv_w(nx,ny),stat=ier)     ! for gridded weights in uv plane
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for gridded weights')
    return
  endif
  allocate(fftws(2*max(nx,ny)),stat=ier)         ! for fft
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for FFT computation')
    return
  endif
  allocate(int_lobe_comp(nx,ny),stat=ier)    ! for int. Primary Beam
  if (ier.ne.0) then
    call map_message(seve%w,rname,'Cannot allocate memory for primary beam')
    return
  endif
  !
  error = .false.
  !
  !-----------------------------------------------------------------------
  !
  ! Visibilities extraction
  ! -----------------------
  !
  ! Update parameters for FFT computation
  !
  ndim = 2
  nn(1) = nx
  nn(2) = ny
  !
  ! Get inverse of FT of (primary beam + convolving function)
  ! CAUTION: output of dosdft function is a real, but with
  ! complex convention
  !
  call uvshort_dosdft(short%sd_beam,short%sd_diam,sd_lobe,nx,ny,lmv)
  !
  ! Start loop on mosaic fields
  !
  ! Single UV Table as output
  ! The output UV Header is taken from input UV table
  ! Matching with the Class Table file has been checked
  !
  cs = [1.0,0.0]
  !
  tweight = 1.0
  if (short%mode.gt.0) then
    ! short_mode > 0
    !
    ! Compute the Weights
    !   We attempt to match the weight density found in the UV table
    ! up to the Single Dish diameter, which is OK for 30-m / NOEMA
    ! pair with compact configuration included, but not for other cases.
    !   This will need to be refined, e.g. by computing the weight density
    ! from the shortest baseline up to the shortest baseline + IP dish diameter
    !
    ! Scaling factor for Weights
    !   In addition, SHORT%SD_WEIGHT provides a user controlled fudge factor,
    ! with 1 as a reasonable default
    short%wcol = (nc+2)/3
    jcol = 7+3*short%wcol
    umax = short%sd_diam
    umax2 = umax*umax
    tweight = 0.
    do j=1,uvt%gil%nvisi
      u2 = duv(1,j)**2+duv(2,j)**2
      if (u2.lt.umax2) then
        tweight = tweight+duv(jcol,j)
      endif
    enddo
    !
    scale_weight = short%sd_weight/4.0/(short%sd_diam/short%uv_maxr)**2
    write(mess,'(A,1PG11.4)') 'Weight scaling factor ',scale_weight
    call map_message(seve%i,rname,mess)
  endif
  !
  if (abs(short%mode).eq.code_short_phase) then
    if (precise) then
      nprec = nc
    else
      nprec = 1
    endif
    allocate(rpos(2,nprec),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'cannot allocate phase shift memory')
      error = .true.
      return
    endif
  else
    allocate(rpos(1,1))   ! Avoid compiler warning
  endif
  !
  ! Recover the number of Fields
  loff = uvt%gil%column_pointer(code_uvt_loff)
  moff = uvt%gil%column_pointer(code_uvt_moff)
  !
  xoff = uvt%gil%column_pointer(code_uvt_xoff)
  yoff = uvt%gil%column_pointer(code_uvt_yoff)
  !
  ovis = 0
  !
  do if=1,nf
    !
    ! 1) Compute interferometer primary beam
    ! --------------------------------------
    !
    ! 1.a) Int. Primary Beam = gaussian, computed in image plane
    !
    ! call uvshort_doprim(ip_beam,int_lobe,nx,ny,raoff(if),deoff(if),lmv)
    !
    ! 1.b) Int. Primary Beam = truncated gaussian in the uv plane then FT
    !
    call uvshort_dointft(short%ip_beam,short%ip_diam,int_lobe,nx,ny,1.0,lmv)
    int_lobe_comp(:,:) = cmplx(int_lobe)
    call uvshort_shift(int_lobe_comp,nx,ny,short%raoff(if),short%deoff(if),lmv)
    call fourt(int_lobe_comp,nn,ndim,1,1,fftws)
    call cmtore(int_lobe_comp,int_lobe,nx,ny)
    !
    ! Normalize peak to 1
    !
    int_lobe(:,:) = int_lobe/maxval(int_lobe)
    !
    ! 2) Compute pseudo-visibilities
    ! ------------------------------
    !
    write(mess,'(A,i0,A,i0)') 'Filtering  field ',if,' / ',nf
    call map_message(seve%i,rname,mess)
    !
    ! Start loop on channels
    !$OMP PARALLEL DEFAULT(none)  &
    !$OMP SHARED(nc,short,nn,ndim,nx,ny,sd_lobe,int_lobe) &
    !$OMP SHARED(lmv_data,ws_data) &
    !$OMP PRIVATE(fftws,i)
    !$OMP DO
    do i = 1,nc
      !
      ! FT of SD image
      call retocm (lmv_data(:,:,i),ws_data(:,:,i),nx,ny)
      call fourt  (ws_data(:,:,i),nn,ndim,1,1,fftws)
      !
      ! SD beam correction (in uv plane)
      if (short%do_single) then
        ws_data(:,:,i) = ws_data(:,:,i)*sd_lobe
      endif
      !
      ! Apply interferometer primary beam (in image plane)
      if (short%do_primary) then
        call fourt  (ws_data(:,:,i),nn,ndim,-1,1,fftws)
        call uvshort_prmult (ws_data(:,:,i),int_lobe,nx,ny)
        call fourt  (ws_data(:,:,i),nn,ndim,1,1,fftws)
        ws_data(:,:,i) = ws_data(:,:,i)/(nx*ny)
      endif
      !
    enddo        ! End loop on channels
    !$OMP END DO
    !$OMP END PARALLEL
    !
    ! 3) Compute weights
    ! ------------------
    !
    ! Weights: do something
    if (short%weight_mode.eq.'UNIF') then
      gr_uv_w = 1.
    else
      call retocm (gr_im_w,gr_uv_w,nx,ny)
      call fourt  (gr_uv_w,nn,ndim,1,1,fftws)
      gr_uv_w(:,:) = gr_uv_w/(nx*ny)
    endif
    if (short%do_single) then
!        gr_uv_w = 1
!        call wcorr(gr_uv_w,sd_lobe,nx*ny)
      where (sd_lobe.ne.0)
        gr_uv_w = 1.0/sd_lobe**2
      else where
        gr_uv_w = 0.0
      end where
    endif
    !
    ! 4) Create or Append uv table
    ! ----------------------------
    !
    ! Compute number of visibilities < uv_maxr
    call uvshort_uvcount(nx,ny,nvis,short%uv_maxr,short%uv_minr,lmv)
    !
    ! Allocate memory for UV table
    nu = uvt%gil%dim(1)
    if (ovis.gt.0) then
      allocate(tmp_data(nu,ovis),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%i,rname,'Cannot allocate memory for UV table ')
        error = .true.
        return
      endif
      tmp_data(:,:) = uvt%r2d
      deallocate(uvt%r2d)
      !
      uvt%gil%dim(2) = ovis+nvis
      allocate(uvt%r2d(uvt%gil%dim(1),uvt%gil%dim(2)),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%i,rname,'Cannot allocate memory for UV table ')
        error = .true.
        return
      endif
      !
      ! Push at end
      uvt%r2d(:,nvis+1:) = tmp_data
      deallocate(tmp_data)
    else
      uvt%gil%dim(2) = nvis
      allocate(uvt%r2d(uvt%gil%dim(1),uvt%gil%dim(2)),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%i,rname,'Cannot allocate memory for UV table ')
        error = .true.
        return
      endif
    endif
    !
    ! Point only towards the appropriate subset !...
    uvt_data => uvt%r2d(:,1:nvis)
    !
    ! Fill in uv table
    if (short%mode.gt.0) then
      ! Scale the weight to have a reasonable beam.
      short%sd_weight = tweight*scale_weight
    endif
    call uvshort_uvtable(nx,ny,nu,nc,ws_data,uvt_data,gr_uv_w, nvis, &
      &    short%uv_maxr, short%uv_minr, short%sd_weight, short%sd_factor, lmv)
    !
    if (abs(short%mode).eq.code_short_phase) then
      !
      ! PHASE mosaic:
      !   Shift phase center to current pointing center
      if (nprec.gt.1) then
        do i=1,nprec
          freq = gdf_uv_frequency(uvt,dble(i))
          rpos(1,i) = freq * f_to_k * short%raoff(if)
          rpos(2,i) = freq * f_to_k * short%deoff(if)
        enddo
      else
        freq = gdf_uv_frequency(uvt)
        rpos(1,1) = freq * f_to_k * short%raoff(if)
        rpos(2,1) = freq * f_to_k * short%deoff(if)
      endif
      rpos = -rpos    ! Use the correct phase shift sign
      !
      ! Initialize the loff & moff columns
      uvt_data(loff,:) = 0
      uvt_data(moff,:) = 0
      !
      ! then shift the UV data set
      call shift_uvdata (uvt,nu,nvis,uvt_data,cs,nprec,rpos)
      !
      ! an only ultimately put the final loff & moff values
      uvt_data(loff,:) = short%raoff(if)
      uvt_data(moff,:) = short%deoff(if)
    else if (abs(short%mode).eq.code_short_point) then
      !
      ! POINTING mosaic:
      !   Just set the xoff & yoff column values
      uvt_data(xoff,:) = short%raoff(if)
      uvt_data(yoff,:) = short%deoff(if)
    else
      ! Single field - Nothing to do
    endif
    !
    write(mess,'(I0,A,I0)') nvis,' visibilities '//string//' for field ',if
    call map_message(seve%i,rname,mess)
    !
    ovis = ovis+nvis
  enddo
  ! Finish the job. Update the number of visibilities
  uvt%gil%nvisi = uvt%gil%dim(2)
  !
  deallocate(ws_data,fftws,gr_im_w,gr_uv_w,sd_lobe,int_lobe)
  !
end subroutine uvshort_doshorts
!
subroutine uvshort_create_lmv(rname,sdt,lmv,gr_im_w,error,uvt,short) 
  use gbl_message
  use image_def
  use short_def
  use clean_def
  use gkernel_interfaces
  use imager_interfaces, except_this => uvshort_create_lmv
  !------------------------------------------------------------------------  
  ! @ private-mandatory
  !
  ! IMAGER      Support routine for command UV_SHORT
  !
  !   Creates a "well behaved" Single Dish map from a Single Dish table
  !   for derivation of the short spacings of an interferometer mosaic
  !
  !   Use convolution by a fraction of the beam, and smooth extrapolation
  !   to zero beyond the mosaic edge
  !------------------------------------------------------------------------  
  character(len=*), intent(in) :: rname     ! Caller task name
  type(gildas), intent(inout) :: lmv        ! LMV data set
  type(gildas), intent(inout) :: sdt        ! SDT data set
  type(gildas), intent(inout) :: uvt        ! UVT data set
  real, allocatable, intent(out) :: gr_im_w(:)        ! Weights
  logical, intent(out) :: error             ! Error flag
  type(short_spacings)  ,intent(inout) :: short
  !
  real(8), parameter :: pi=3.141592653589793d0
  type(gridding) :: conv
  !
  real, pointer :: sdt_data(:,:)
  real, pointer :: lmv_data(:,:,:)
  !
  integer :: np, nd, nc, lcol, ocol, nx, ny, nxmore, nymore
  integer :: ier, i
  real, allocatable :: sdw(:)     ! weights table
  real, allocatable :: rawcube(:) ! work space for sdt gridded values
  real, allocatable :: xcoord(:)  ! lmv X axis gridded coordinates
  real, allocatable :: ycoord(:)  ! lmv Y axis gridded coordinates
  real :: xmin,xmax,ymin,ymax
  real(kind=8) :: xconv(3),yconv(3),new(2),old(2),tmp
  real :: smooth
  integer ctypx,ctypy
  real maxw,minw,xparm(10),yparm(10),support(2),cell(2)
  character(len=256) :: mess
  !
  real, parameter :: map_rounding=0.05
  integer, parameter :: map_power=2  
  !
  if (sdt%gil%form.ne.fmt_r4) then
    call map_message(seve%f,rname,'Only real format supported')
    error = .true.
    return
  endif
  !
  nd = sdt%gil%dim(1)
  np = sdt%gil%dim(2)
  sdt_data => sdt%r2d
  !
  ! Check xcol,ycol input parameters
  if ((short%xcol.gt.nd).or. (short%ycol.gt.nd)) then
    call map_message(seve%e,rname,'X or Y column does not exist')
    error = .true.
    return
  endif
  !
  ! Allocate memory for weight table
  allocate(sdw(2*max(np,nd)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate memory for weigths table')
    error = .true.
    return
  endif
  !
  !-----------------------------------------------------------------------
  !
  ! Arrange data
  ! ------------
  !
  ! Recompute offsets if reference position is to be modified
  if (short%chra.eq.'0' .and. short%chde.eq.'0') then
    continue
  else
    if (short%chra.eq.' ' .and. short%chde.eq.' ') then
      new(1) = uvt%gil%a0
      new(2) = uvt%gil%d0
    else
      print *,'RA  '//short%chra
      print *,'Dec '//short%chde
      call sic_decode(short%chra,new(1),24,error)
      if (error) return
      call sic_decode(short%chde,new(2),360,error)
      if (error) return
    endif
    ! Declinations should be between [-pi/2,pi/2] and RA between [0,2*pi]
    if (new(1).lt.0 .or.new(1).gt.2d0*pi) then
       call map_message(seve%w,rname,'RA value outside [0,2*pi] => Using modulo')
       new(1) = modulo(new(1),2d0*pi)
    endif
    if (new(2).lt.-0.5d0*pi .or.new(2).gt.0.5d0*pi) then
       call map_message(seve%e,rname,'Dec value outside [-pi/2,pi/2] => Using modulo')
       new(2) = modulo(new(2)+0.5d0*pi,pi)-0.5d0*pi
    endif
    !
    old(1) = sdt%gil%a0
    old(2) = sdt%gil%d0
    !
    ! Recompute offsets
    !
    call uvshort_dopoint (sdt_data,nd,np,short%xcol,short%ycol,old,new)
    sdt%gil%a0 = new(1)
    sdt%gil%d0 = new(2)
  endif
  !
  ! Set the sinus type projection
  sdt%gil%ptyp = p_azimuthal
  !
  ! Order sdt_data with Y in increasing order
  ! sdw is used as work space only
  call uvshort_dosor (rname,sdt_data,nd,np,sdw,short%ycol,error)
  if (error) return
  !
  ! Read weights in sdw table
  call uvshort_dowei (sdt_data,nd,np,sdw,short%wcol)
  !
  !-----------------------------------------------------------------------
  !
  ! Find lmv image size
  ! -------------------
  !
  ! Find min,max offsets of the SD observations
  ! (subroutine working only on Y ordered array)
  !
  call uvshort_finsiz (sdt_data,nd,np,short%xcol,short%ycol,sdw,xmin,xmax,ymin,ymax)
  !
  ! Find increment: 4 pixels per SD beam
  !
  xconv(3) = -short%sd_beam(1)/4.0
  yconv(3) = short%sd_beam(1)/4.0
  !
  ! ! Old code: used if sd_beam = 0
  ! ! pixel size = min distance between SD points
  ! xinc = xmax-xmin
  ! yinc = ymax-ymin
  ! call fininc (sdt_data,nd,np,xcol,ycol,sdw,xinc,yinc,tole)
  ! xconv(3) = -xinc
  ! yconv(3) = +yinc
  !
  ! Find size of output SD image
  !
  nx = 2 * max ( nint(abs(xmax/xconv(3))+1), nint(abs(xmin/xconv(3))+1) )
  ny = 2 * max ( nint(abs(ymax/yconv(3))+1), nint(abs(ymin/yconv(3))+1) )
  nxmore = nint(4*short%sd_beam(1)/abs(xconv(3)))+1
  nymore = nint(4*short%sd_beam(1)/abs(yconv(3)))+1
  nx = nx+2*nxmore
  ny = ny+2*nymore
  !
  ! Extend nx,ny to nearest power of two
  i = max(nx,32)
  call gi4_round_forfft(i,nx,error,map_rounding,map_power)
  i = max(ny,32)
  call gi4_round_forfft(i,ny,error,map_rounding,map_power)
  !
  ! Reference position for lmv cube header
  !
  xconv(1) = nx/2+1
  xconv(2) = 0.0
  yconv(1) = ny/2+1
  yconv(2) = 0.0
  !
  tmp = 0.1*nint(yconv(3)*10*180*3600/pi)
  write(mess,'(A,I0,A,I0,A)') 'Creating a cube with ',nx,' by ',ny,' pixels'
  call map_message(seve%i,rname,mess)
  write(mess,'(A,F8.3,A)') 'Pixel size: ',tmp,' arcsec'
  call map_message(seve%i,rname,mess)
  !
  ! Warn for big images
  if (nx.gt.512 .or. ny.gt.512) then
    if (nx.gt.8192 .or. ny.gt.8192) then
      call map_message(seve%e,rname,'More than 8192 pixels in X or Y')
      error = .true.
    else
      call map_message(seve%w,rname,'More than 512 pixels in X or Y')
    endif
    write(mess,*) 'Offset extrema are: ',xmin,xmax,ymin,ymax
    call map_message(seve%i,rname,mess)
    write(mess,*) 'Pixel sizes are', xconv(3),yconv(3)
    call map_message(seve%i,rname,mess)
    if (error) return
  endif
  !
  ! Number of channels
  if (short%mcol(2).eq.0) short%mcol(2) = nd
  short%mcol(1) = max(1,min(short%mcol(1),nd))
  short%mcol(2) = max(1,min(short%mcol(2),nd))
  ocol = min(short%mcol(1),short%mcol(2))       ! first channel to grid
  lcol = max(short%mcol(1),short%mcol(2))       ! last channel to grid
  nc = lcol-ocol+1
  write(mess,'(A,I0,A,I0,A,I0,A)') 'Creating ',nc,' channels from [',short%mcol(1),',',short%mcol(2),']'
  call map_message(seve%i,rname,mess)
  ocol = ocol-1 ! Because it is used as such in "doconv"
  !
  !-----------------------------------------------------------------------
  !
  ! Create lmv image
  ! ----------------
  !
  ! Fill in header
  lmv%gil%ndim = 3
  lmv%gil%dim(1) = nx
  lmv%gil%dim(2) = ny
  lmv%gil%dim(3) = nc
  lmv%gil%dim(4) = 1
  lmv%loca%size = lmv%gil%dim(1)*lmv%gil%dim(2)*lmv%gil%dim(3)
  lmv%gil%ref(3) = sdt%gil%ref(1)-ocol
  lmv%gil%val(3) = lmv%gil%voff
  lmv%gil%inc(3) = lmv%gil%vres
  lmv%gil%convert(:,1) = xconv
  lmv%gil%convert(:,2) = yconv
  lmv%char%code(1) = sdt%char%code(2)
  lmv%char%code(2) = sdt%char%code(3)
  ! Patch a bug in Header transmission:
  !   the table has only 2 dimensions, so the 3rd axis is undefined
  if (len_trim(lmv%char%code(2)).eq.0) lmv%char%code(2)='DEC'
  lmv%char%code(3) = sdt%char%code(1)
  lmv%gil%coor_words = 6*gdf_maxdims             ! not a table
  lmv%gil%extr_words = 0                   ! extrema not computed
  lmv%gil%xaxi = 1                         ! reset projected axis
  lmv%gil%yaxi = 2
  lmv%gil%faxi = 3
  lmv%gil%form = fmt_r4
  !
  !-----------------------------------------------------------------------
  !
  ! Memory allocations
  ! ------------------
  !
  error = .true.
  allocate(lmv%r3d(lmv%gil%dim(1),lmv%gil%dim(2),lmv%gil%dim(3)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate memory for Single Dish Cube')
    return
  endif
  allocate(rawcube(nc*nx*ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate memory for raw cube lmv')
    return
  endif
  allocate(gr_im_w(2*nx*ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate memory for work space')
    return
  endif
  allocate(xcoord(nx),ycoord(ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate memory for lmv X or Y axis')
    return
  endif
  error = .false.
  !
  !-----------------------------------------------------------------------
  !
  ! Gridding of the SD data
  ! -----------------------
  !
  ! Resampling in space of the original spectra on a regular grid,
  ! using a convolution kernel
  !
  ! Compute gridding function: a small (1/3 of SD beam) gaussian
  ! Note: since xconv and yconv depends on SD beam, all gridding
  ! parameters do depend on SD beam value...
  !
  smooth = short%sd_beam(2)/3.0
  !
  ctypx = 2
  ctypy = 2
  support(1) = 5*smooth              ! go far enough...
  support(2) = 5*smooth
  xparm(1) = support(1)/abs(xconv(3))
  yparm(1) = support(2)/abs(yconv(3))
  xparm(2) = smooth/(2*sqrt(log(2.0)))/abs(xconv(3))
  yparm(2) = smooth/(2*sqrt(log(2.0)))/abs(yconv(3))
  xparm(3) = 2
  yparm(3) = 2
  !
  call convfn (ctypx, xparm, conv%ubuff, conv%ubias)
  call convfn (ctypy, yparm, conv%vbuff, conv%vbias)
  cell(1) = xconv(3)
  cell(2) = yconv(3)
  !
  ! Compute grid coordinates
  xcoord(:) = (/(real((dble(i)-lmv%gil%ref(1))*lmv%gil%inc(1)+lmv%gil%val(1)),i=1,nx)/)
  ycoord(:) = (/(real((dble(i)-lmv%gil%ref(2))*lmv%gil%inc(2)+lmv%gil%val(2)),i=1,ny)/)
  !
  ! Grid data: output = rawcube
  ! **WW** poids sdw --> poids gr_im_w = pour chaque pixel, somme des
  !        [poids des points pris en compte * gaussienne]
  !        (NON normalise)
  !        --> a normaliser par somme des gaussiennes ('results')?
  !
  gr_im_w = 0.
  call uvshort_doconv (&
    & nd,np,&                             ! number of input points
    & sdt_data,&                          ! input values
    & short%xcol,short%ycol,ocol,&        ! pointers to special values
    & sdw,&                               ! weights
    & gr_im_w,&                           ! gridded weights
    & nc,nx,ny,&                          ! cube size
    & rawcube,&                           ! gridded data (output)
    & xcoord,ycoord,&                     ! cube coordonates
    & support,cell,maxw, conv)
  !
  ! Min and max weights
  minw = maxw * short%minw
  call map_message(seve%i,rname,'Done Single Dish table gridding')
  !
  !
  !---------------------------------------------------------------------
  !
  ! Extrapolation to zero outside the convex hull of the mapped region
  ! ------------------------------------------------------------------
  !
  ! Compute smoothing function = SD primary beam
  !
  ctypx = 2
  ctypy = 2
  support(1) = 3*short%sd_beam(1)
  support(2) = 3*short%sd_beam(1)
  xparm(1) = support(1)/abs(xconv(3))
  yparm(1) = support(2)/abs(yconv(3))
  xparm(2) = short%sd_beam(1)/(2*sqrt(log(2.0)))/abs(xconv(3))
  yparm(2) = short%sd_beam(1)/(2*sqrt(log(2.0)))/abs(yconv(3))
  xparm(3) = 2
  yparm(3) = 2
  !
  call convfn (ctypx, xparm, conv%ubuff, conv%ubias)
  call convfn (ctypy, yparm, conv%vbuff, conv%vbias)
  cell(1) = xconv(3)
  cell(2) = yconv(3)
  !
  ! Smooth data by gaussian = SD beam
  !       input = rawcube
  !       output = lmv_data (used as work space)
  !
  ! Gridded weights are in the call sequence, but unused 
  lmv_data => lmv%r3d
  !
  call uvshort_dosmoo (&
    & rawcube,&                   ! raw gridded values
    & gr_im_w,&                   ! gridded weights
    & nc,nx,ny,&                  ! cube size
    & lmv_data,&                  ! smoothed cube
    & xcoord,ycoord,&             ! cube coordinates
    & support,cell,conv)
  call map_message(seve%i,rname,'Done image smoothing')
  !
  ! Apodisation: - smooth image (lmv_data) is apodised by gaussian
  !                at image edges
  !              - replace input image (rawcube) by smoothed-apodisated
  !                image at map edges
  !              - replace input image (rawcube) by smoothed image
  !                at points where weights < minw
  ! Output = rawcube
  !
  ! Gridded weights are used but not modified
  call uvshort_doapod (xmin,xmax,ymin,ymax,& 
    & short%ptole,short%sd_beam(1),&
    & nc,nx,ny,&
    & lmv_data,&              ! input smoothed cube
    & rawcube,&               ! output after apodisation
    & xcoord,ycoord,&         ! cube coordinates
    & gr_im_w, minw)
  call map_message(seve%i,rname,'Done image apodisation')
  !
  ! Transpose to the lmv order and put the result in lmv_data
  call uvshort_dotrans(rawcube,lmv_data,nc,nx*ny)
  call map_message(seve%i,rname,'Done transposition to lmv order')
  !
  ! Free memory
  deallocate(sdw,rawcube,xcoord,ycoord)
  !
  ! Update the angular resolution, since we have used a Gridding function
  smooth = short%sd_beam(2)/3
  short%sd_beam(1) = sqrt(short%sd_beam(1)**2 + smooth**2)
  short%sd_beam(2) = sqrt(short%sd_beam(2)**2 + smooth**2)
  ! 
end subroutine uvshort_create_lmv
!
subroutine uvshort_convol (du,dv,resu,conv)
  use clean_def
  !------------------------------------------------------------------------  
  !
  ! Compute convolving factor resu
  ! resu is the result of the multiplication of the convolution functions,
  ! for u and v axes, at point (du,dv)
  !
  ! Convolution function is defined in "conv"
  !------------------------------------------------------------------------  
  real, intent(out) :: resu
  real, intent(in) :: du,dv
  type(gridding), intent(in) :: conv
  !
  ! Local variables
  integer iu,iv
  !
  ! Would need protection against Overflow of the array - TBD
  !
  ! convolving functions values are tabulated every 1/100 cell
  iu = nint(100.0*du+conv%ubias)
  iv = nint(100.0*dv+conv%vbias)
  !
  ! Participation of u and v axes convolution functions
  resu = conv%ubuff(iu)*conv%vbuff(iv)
  if (resu.lt.1e-20) resu = 0.0
end subroutine uvshort_convol
!
!
!=========================================================================
!
subroutine uvshort_doconv (nd,np,visi,jx,jy,jo,we,gwe,&
  &   nc,nx,ny,map,mapx,mapy,sup,cell,maxw,conv)
  use clean_def
  !------------------------------------------------------------------------  
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Convolution of 'map' by function defined by "conv" 
  ! Used for gridding of Single Dish data.
  !------------------------------------------------------------------------  
  integer, intent(in) :: np              ! number of visibilities
  integer, intent(in) :: nd              ! visibility size
  integer, intent(in) :: nc              ! number of channels
  integer, intent(in) :: nx,ny           ! map size
  integer, intent(in) :: jx,jy           ! x coord, y coord location in visi
  real, intent(in) :: we(np)             ! weights
  integer, intent(in) :: jo              ! offset for data in visi
  real, intent(in) :: visi(nd,np)        ! values
  real, intent(out) :: gwe(nx,ny)        ! gridded weights
  real, intent(out) :: map(nc,nx,ny)     ! gridded values
  real, intent(in) :: mapx(nx),mapy(ny)  ! coordinates of grid
  real, intent(in) :: sup(2)             ! support of convolving function in user units
  real, intent(in) :: cell(2)            ! cell size in user units
  real, intent(out) :: maxw              ! maximum weight
  type(gridding), intent(in) :: conv
  !
  ! Local variables
  !
  integer ifirs,ilast                ! range to be considered
  integer ix,iy,i
  real result,weight
  real u,v,du,dv,um,up,vm,vp
  !
  ! Code
  !
  maxw = 0.0
  !
  ! Loop on Y
  !
  ifirs = 1
  do iy=1,ny
    v = mapy(iy)
    !
    ! sup is the support of the gridding function
    vm = v-sup(2)
    vp = v+sup(2)
    !
    ! Find points to be considered.
    ! Optimized dichotomic search, taking into account the
    ! fact that mapy is an ordered array.
    call uvshort_findr (np,nd,jy,visi,vm,ifirs)
    ilast = ifirs
    call uvshort_findr (np,nd,jy,visi,vp,ilast)
    ilast = ilast-1
    !
    ! Initialize x column
    map(1:nc,1:nx,iy) = 0.0
    !
    if (ilast.ge.ifirs) then
      !
      ! Loop on x cells
      do ix=1,nx
        u = mapx(ix)
        um = u-sup(1)
        up = u+sup(1)
        weight = 0.0
        !
        ! Loop on relevant data points
        do i=ifirs,ilast
          !
          ! Test if X position is within the range to be
          ! considered
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            !
            ! Compute convolving factor
            du = (u-visi(jx,i))/cell(1)
            dv = (v-visi(jy,i))/cell(2)
            call uvshort_convol (du,dv,result,conv)
            if (result.ne.0.0) then
              !
              ! Do the convolution: map(pixel) = sum of
              ! relevant values * convolving factor * weight
              result = result*we(i)
              weight = weight + result
              map (1:nc,ix,iy) = map (1:nc,ix,iy) + visi((1+jo):(nc+jo),i)*result
            endif
          endif
        enddo
        !
        ! gwe is the sum of the (convolving factor * weight) ie
        ! the sum of the weighting factors applied to the data
        gwe(ix,iy) = weight
        maxw = max(maxw,weight)
        !
        ! Normalization (only in cells where some data exists)
        if (weight.ne.0) then
          map (1:nc,ix,iy) = map(1:nc,ix,iy)/weight
        endif
      enddo
    endif
  enddo
end subroutine uvshort_doconv
!
!---------------------------------------------------------------------------------------
!
subroutine uvshort_dosmoo (raw,we,nc,nx,ny,map,mapx,mapy,sup,cell,conv)
  use clean_def
  !------------------------------------------------------------------------  
  ! Task UV_SHORT
  !   Internal routine
  !
  ! Smooth an input data cube raw in vlm along l and m by convolution
  ! by a gaussian function defined in "conv"
  !------------------------------------------------------------------------  
  integer, intent(in) :: nc,nx,ny          ! map size
  real, intent(in) :: we(nx,ny)            ! weights
  real, intent(in) :: raw(nc,nx,ny)        ! raw map
  real, intent(out) :: map(nc,nx,ny)       ! smoothed map
  real, intent(in) :: mapx(nx),mapy(ny)    ! coordinates of grid
  real, intent(in) :: sup(2)               ! support of convolving function in user units
  real, intent(in) :: cell(2)              ! cell size in user units
  type(gridding), intent(in) :: conv
  !
  ! Local variables
  integer yfirs,ylast                ! range to be considered
  integer xfirs,xlast                ! range to be considered
  integer ix,iy
  integer jx,jy                      ! x coord, y coord location in raw
  real result,weight
  real u,v,du,dv,um,up,vm,vp,dx,dy
  !
  ! Code
  dx = abs(mapx(2)-mapx(1))
  dy = abs(mapy(2)-mapy(1))
  !
  ! Loop on y rows
  do iy=1,ny
    !
    ! Compute extrema positions on axe y
    ! of gaussian function centered on map(1:nc,ix,iy)
    v = mapy(iy)
    vm = v-sup(2)
    vp = v+sup(2)
    !
    ! Compute extrema positions on axe y
    ! of relevant data points for map(1:nc,ix,iy) convolution
    yfirs = max(1,nint((iy-sup(2)/dy)))
    ylast = min(ny,nint((iy+sup(2)/dy)))
    !
    ! Initialize x colum
    map(1:nc,1:nx,iy) = 0.0
    !
    ! Loop on x cells
    !
    if (yfirs.le.ylast) then
      do ix=1,nx
        !
        ! Compute extrema positions on axe x, idem y
        u = mapx(ix)
        um = u-sup(1)
        up = u+sup(1)
        weight = 0.0
        xfirs = max(1,nint(ix-sup(1)/dx))
        xlast = min(nx,nint(ix+sup(1)/dx))
        !
        ! Loop on relevant data points
        if (xfirs.le.xlast) then
          do jy=yfirs,ylast
            dv = (v-mapy(jy))/cell(2)
            do jx=xfirs,xlast
              du = (u-mapx(jx))/cell(1)
              !
              ! Compute convolving factor
              call uvshort_convol (du,dv,result,conv)
              if (result.ne.0.0) then
                !
                ! Do the convolution: map(pixel) = sum of
                ! relevant values * convolving factor
                !
                weight = weight + result
                map (1:nc,ix,iy) = map (1:nc,ix,iy) + raw(1:nc,jx,jy)*result
              endif
            enddo
          enddo
          !
          ! Normalize weight only in cells where some data exists...
          if (weight.ne.0) then
            map (1:nc,ix,iy) = map(1:nc,ix,iy)/weight
          endif
        endif
      enddo
    endif
  enddo
end subroutine uvshort_dosmoo
!
!-----------------------------------------------------------------------
!
subroutine uvshort_dowei (visi,nd,np,we,iw)
  !------------------------------------------------------------------------  
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Fill in weights array from the input table
  !------------------------------------------------------------------------  
  integer, intent(in) :: nd       ! Visibility size
  integer, intent(in) :: np       ! Number of visibilities
  integer, intent(in) :: iw       ! Weight column
  real, intent(in) :: visi(nd,np) ! Visibilities
  real, intent(out) :: we(np)     ! Weight values
  !
  ! Code
  if (iw.le.0 .or. iw.gt.nd) then
    !
    ! Weight column does not exist...
    we(1:np) = 1.0
  else
    !
    ! Weight colum do exist
    we(1:np) = visi(iw,1:np)
  endif
end subroutine uvshort_dowei
!
!-----------------------------------------------------------------------
!
subroutine uvshort_findr (nv,nc,ic,xx,xlim,nlim)
  !------------------------------------------------------------------------  
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Find nlim such as
  !   xx(ic,nlim-1) < xlim < xx(ic,nlim)
  ! for input data ordered, retrieved from memory
  ! assumes nlim already preset so that xx(ic,nlim-1) < xlim
  !
  !------------------------------------------------------------------------  
  integer, intent(in) :: nv       ! Number of visibilities
  integer, intent(in) :: nc       ! Size of a visibility
  integer, intent(in) :: ic       ! COlumn to be tested
  integer, intent(inout) :: nlim
  real, intent(in) ::  xx(nc,nv),xlim
  !
  ! Local variables
  integer ninf,nsup,nmid
  !
  ! Code
  if (nlim.gt.nv) return
  !
  ! Define limits of searching area in the table
  !
  if (xx(ic,nlim).gt.xlim) then
    return
  elseif (xx(ic,nv).lt.xlim) then
    nlim = nv+1
    return
  endif
  ninf = nlim
  nsup = nv
  !
  ! Loop while : dichotomic search for input data ordered
  do while(nsup.gt.ninf+1)
    !
    ! Define middle of the searching area on the table
    nmid = (nsup + ninf)/2
    !
    ! If it's not in the last part, it's in the first one...
    ! then defined new searching limits area
    if (xx(ic,nmid).lt.xlim) then
      ninf = nmid
    else
      nsup = nmid
    endif
  enddo
  !
  ! Output
  nlim = nsup
end subroutine uvshort_findr
!
!------------------------------------------------------------------------
!
subroutine uvshort_finsiz (x,nd,np,ix,iy,we,xmin,xmax,ymin,ymax)
  !------------------------------------------------------------------------  
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Find extrema xmin, xmax in ix column values
  !          and ymin, ymax in iy column values,
  ! in table x(nd,np) for points where weight is not null
  ! taking in account that table x is ordered on iy column values.
  !
  !------------------------------------------------------------------------  
  integer, intent(in) :: nd,np    ! Table size
  integer, intent(in) :: ix,iy    ! X and Y column pointers
  real, intent(in) :: x(nd,np)    ! Table data
  real, intent(in) :: we(np)      ! Weights
  real, intent(out) :: xmin,xmax,ymin,ymax  ! Min-Max
  !
  ! Local variables
  integer i,j
  !
  ! Code
  i = 1
  !
  ! Loop to start after null weights measurements
  do while (we(i).eq.0)
    i = i+1
  enddo
  !
  ! ymin is first y value with weight not null
  ymin = x(iy,i)
  !
  ! initialize xmin and xmax for searching loop
  xmin = x(ix,i)
  xmax = x(ix,i)
  !
  ! Loop on table lines to find xmin and xmax
  i = i+1
  do j=i,np
    if (we(j).ne.0) then
      if (x(ix,j).lt.xmin) then
        xmin = x(ix,j)
      elseif (x(ix,j).gt.xmax) then
        xmax = x(ix,j)
      endif
    endif
  enddo
  !
  ! Loop to find ymax = last y values with weight not null
  i = np
  do while (we(i).eq.0)
    i = i-1
  enddo
  ymax = x(iy,i)
  !
end subroutine uvshort_finsiz
!
!-------------------------------------------------------------------------
subroutine uvshort_dosor (rname,visi,nd,np,we,iy,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, only : map_message, trione
  !------------------------------------------------------------------------  
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Output Visi(nd,np) will contain Ycol column values in increasing order
  ! Uses procedure trione
  ! WE is used as work space only
  !------------------------------------------------------------------------  
  character(len=*), intent(in) :: rname
  integer, intent(in) :: nd,np,iy
  real, intent(inout) :: visi(nd,np)   ! Visibilities,we(nd)
  real, intent(inout) :: we(nd)        ! Work space for sorting
  logical, intent(out) :: error
  !
  ! Local variables
  integer i,ier ! ,trione
  !
  ! Code
  error = .false.
  do i=1,np-1
    if (visi(iy,i).gt.visi(iy,i+1)) then
      call map_message(seve%i,rname,'Sorting input table')
      ier = trione (visi,nd,np,iy,we)
      if (ier.ne.1) then
        call map_message(seve%e,rname,'Insufficient sorting space')
        error = .true.
      endif
      return
    endif
  enddo
  call map_message(seve%i,rname,'Input table is sorted')
  !
end subroutine uvshort_dosor
!
!-------------------------------------------------------------------------
function trione (x,nd,n,ix,work)
  !---------------------------------------------------------------------
  ! @ private
  !
  !   IMAGER
  !   sorting program that uses a quicksort algorithm.
  ! sort on one row
  ! x r*4(*)  unsorted array        input
  ! nd  i first dimension of x      input
  ! n i second dimension of x     input
  ! ix  i x(ix,*) is the key for sorting    input
  ! work  r*4(nd) work space for exchange     input
  !---------------------------------------------------------------------
  !
  integer, intent(in) :: nd   ! First dimension
  integer, intent(in) :: n    ! Second dimension
  integer, intent(in) :: ix   ! Sorting key in first dimension
  real(4), intent(inout) :: x(nd,n)  ! Array for sorting
  real(4), intent(inout) :: work(nd) ! Work space for sorting
  !
  ! Local variables
  integer trione, maxstack, nstop
  parameter (maxstack=1000,nstop=15)
  integer*4 i, j, k, l1, r1, l, r, m
  integer*4 lstack(maxstack), rstack(maxstack), sp
  real*4 key
  logical mgtl, lgtr, rgtm
  !
  ! Code
  trione = 1
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! set key = median of x(l), x(m), x(r)
  ! no! this is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! to fix this problem, i found (but i cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. p.v.
  !
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  l1=(2*l+r)/3
  r1=(l+2*r)/3
  !
  mgtl = x(ix,m) .gt. x(ix,l)
  rgtm = x(ix,r) .gt. x(ix,m)
  !
  ! Algorithm to select the median key. the original one from mongo
  ! was completely wrong. p. valiron, 24-jan-84 .
  !
  !       mgtl  rgtm  lgtr  mgtl.eqv.lgtr median_key
  !
  ! kl < km < kr  t t * *   km
  ! kl > km > kr  f f * *   km
  !
  ! kl < km > kr  t f f f   kr
  ! kl < km > kr  t f t t   kl
  !
  ! kl > km < kr  f t f t   kl
  ! kl > km < kr  f t t f   kr
  !
  if (mgtl .eqv. rgtm) then
     key = x(ix,m)
  else
     lgtr = x(ix,l) .gt. x(ix,r)
     if (mgtl .eqv. lgtr) then
        key = x(ix,l)
     else
        key = x(ix,r)
     endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
  !
10 if (x(ix,i).ge.key) goto 11
  i = i + 1
  goto 10
11 continue
  !
  ! Find a small record on the right
  !
20 if (x(ix,j).le.key) goto 21
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  !
  call r4tor4 (x(1,i),work,nd)
  call r4tor4 (x(1,j),x(1,i),nd)
  call r4tor4 (work,x(1,j),nd)
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! push the two halves on the stack
  !
2 continue
  if (j-l+1 .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(6,*) 'E-UV_SHORT, Stack overflow ',sp
        trione = 0
        return
     endif
     lstack(sp) = l
     rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(6,*) 'E-UV_SHORT, Stack overflow ',sp
        trione = 0
        return
     endif
     lstack(sp) = j+1
     rstack(sp) = r
  endif
  !
  ! anything left to process?
  !
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do 110 j = n-1,1,-1
     k = j
     do i = j+1,n
        if (x(ix,j).le.x(ix,i)) goto 121
        k = i
     enddo
121  continue
     if (k.eq.j) goto 110
     call r4tor4 (x(1,j),work,nd)
     do i = j+1,k
        call r4tor4 (x(1,i),x(1,i-1),nd)
     enddo
     call r4tor4 (work,x(1,k),nd)
110  continue
  !
end function trione
!
!-------------------------------------------------------------------------
!
subroutine uvshort_doapod (xmin,xmax,ymin,ymax,tole,beam,&
     nc,nx,ny,map,raw,mapx,mapy, weight,wmin)
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Replace map edges and bad quality values of raw input data cube
  ! with smoothed values contains in mapx data cube.
  ! Map edges corresponding to the part of the map between
  ! max SD observations locations xmin,xmax,ymin,ymax, and map size nx,ny
  ! Bad quality values corresponding to weights < wmin
  !---------------------------------------------------------------------
  integer nc,nx,ny
  real mapx(nx),mapy(ny)
  real map(nc,nx,ny)
  real raw(nc,nx,ny)
  real beam,tole,xmin,xmax,ymin,ymax
  real weight(nx,ny),wmin
  !
  ! Local variables
  !
  integer ix,iy
  real lobe,apod,disty,distx
  !
  ! Code
  !
  ! Tests
  !
  ! pi = acos(-1.0)
  ! write(6,*) 'I-UV_SHORT, do apodisation :'
  ! write(6,*) 'min-max ',xmin,xmax,ymin,ymax
  ! write(6,*) 'beam et inc ',beam*180*3600/pi,tole*180*3600/pi&
  !      ,(mapx(1)-mapx(2))*180*3600/pi
  !
  ! Apodisation by a gaussian, twice as large than the SD beam
  !
  lobe = log(2.0)/beam**2
  !
  ! Loop on pixels
  do iy=1,ny
    !
    ! Compute disty : distance between map size ny
    ! and ymin or ymax SD observations limits
    if (mapy(iy).le.ymin-tole) then
      disty = ymin-mapy(iy)
    elseif (mapy(iy).ge.ymax+tole) then
      disty = mapy(iy)-ymax
    else
      disty = 0.0
    endif
    !
    do ix=1,nx
      !
      ! Idem on X, compute distx
      if (mapx(ix).le.xmin-tole) then
        distx = xmin-mapx(ix)
      elseif (mapx(ix).ge.xmax+tole) then
        distx = mapx(ix)-xmax
      else
        distx = 0.0
      endif
      !
      ! Apodisation factor
      apod = (distx**2+disty**2)*lobe
      !
      ! 'raw' is replaced by something else only in two cases
      if (apod.gt.80) then
        !
        ! Map edges
        raw(1:nc,ix,iy) = 0.0
      elseif (apod.ne.0.0) then
        !
        ! Map edges
        apod = exp(-apod)
        raw(1:nc,ix,iy) = map(1:nc,ix,iy)*apod
      elseif (weight(ix,iy).lt.wmin) then
        !
        ! Low weight point within the map
        raw(1:nc,ix,iy) = map(1:nc,ix,iy)
      endif
    enddo
  enddo
  !
end subroutine uvshort_doapod
!
!-------------------------------------------------------------------------
!
subroutine uvshort_dosdft(beam,diam,f,nx,ny,lmv)
  use image_def
  use imager_interfaces, only : mulgau
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! computes inverse of ft of single-dish beam
  ! (uses a gaussian truncated at dish size)
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: lmv     ! LMV header
  integer, intent(in) ::  nx, ny      ! Problem size
  real, intent(out) ::  f(nx,ny)      ! (real part of the) TF of beam
  real, intent(in) ::  beam(3)        ! Beam size in radian
  real, intent(in) ::  diam           ! Antenna diameter in meter
  !
  ! Local variables
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: clight=299792458d-6    ! frequency in mhz
  !
  ! We use a code that is also valid for bi-dim Gaussian
  !
  complex, allocatable :: g(:,:)
  real :: xinc,yinc,bmaj,bmin,pa,scale, a
  integer :: isign
  ! 
  allocate(g(nx,ny))
  g(:,:) = cmplx(1.0,0.0)
  xinc = lmv%gil%inc(1)
  yinc = lmv%gil%inc(2)
  bmaj = beam(1)
  bmin = beam(2)
  if (bmin.eq.0) bmin = bmaj
  pa = beam(3)
  if (bmin.eq.bmaj) pa = 0.
  scale = 1.0 
  isign = +1
  !
  ! To get the gaussian
  !
  call mulgau(g,nx,ny,bmaj,bmin,pa,scale,xinc,yinc,isign,20.)
  !
  ! Equivalent beam area in square pixels ...
  a = abs(4*alog(2.)/pi/(beam(1)*beam(2))*lmv%gil%inc(2)*lmv%gil%inc(1))
  !
  ! No masking. It is absorbed by the last argument of MULGAU
  ! Masking will anyhow occur due to the UV truncation radius later
  f = a*real(g)
  !
end subroutine uvshort_dosdft
!
!--------------------------------------------------------------------------
!
subroutine uvshort_dointft(beam,diam,f,nx,ny,fact,lmv)
  use image_def
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! computes ft of single-dish beam
  ! (uses a gaussian truncated at dish size)
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: lmv     ! LMV header
  integer, intent(in) ::  nx, ny      ! Problem size
  real, intent(out) ::  f(nx,ny)      ! (real part of the) TF of beam
  real, intent(in) ::  beam           ! Beam size in radian
  real, intent(in) ::  diam           ! Antenna diameter in meter
  real, intent(in) ::  fact           ! Scale factor
  !
  ! Local variables
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: clight=299792458d-6    ! frequency in MHz
  integer :: i,j, ii, jj
  real :: a, b, xx, yy
  real(8) :: dx, dy
  !
  ! Code
  !
  dx = clight/lmv%gil%freq/(lmv%gil%inc(1)*lmv%gil%dim(1))
  dy = clight/lmv%gil%freq/(lmv%gil%inc(2)*lmv%gil%dim(2))
  b = (pi*beam/2/clight*lmv%gil%freq)**2/alog(2.)
  !
  ! Equivalent beam area in square pixels ...
  !
  a = abs(pi*beam**2/lmv%gil%inc(2)/lmv%gil%inc(1))*fact/abs(4*alog(2.))
  !
  ! Loop on pixels
  !
  do j = 1, ny
    !
    ! Loop on Y, pixels locations on Fourier plane
    jj = mod(j-1+ny/2,ny)-ny/2
    yy = ( jj*dy )**2
    !
    do i = 1, nx
      !
      ! Loop on X, pixels locations on Fourier plane
      ii = mod(i-1+nx/2,nx)-nx/2
      xx = ( ii*dx )**2
      !
      ! Truncation of the gaussian at diam
      if (xx+yy.le.diam**2) then
        f(i,j) = exp(-b*(xx+yy))*a
      else
        f(i,j) = 0.0
      endif
    enddo
  enddo
  !
end subroutine uvshort_dointft
!
!-------------------------------------------------------------------------
!
subroutine uvshort_shift(f,nx,ny,offra,offdec,lmv)
  use image_def
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Centered interferometer beam f(nx,ny) of a mosaic field
  ! on is right position : offra and offdec shifted in uv plane
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: lmv
  integer, intent(in) :: nx, ny
  complex, intent(inout) :: f(nx,ny)
  real, intent(in) :: offra, offdec
  !
  real(8), parameter :: pi=3.141592653589793d0
  !
  ! Local variables
  integer i, j, ii, jj
  real phi, sp, cp, xx, yy, re, im
  real(8) :: du, dv
  !
  ! Code
  !
  du = 1.d0/(lmv%gil%inc(1)*lmv%gil%dim(1))
  dv = 1.d0/(lmv%gil%inc(2)*lmv%gil%dim(2))
  !
  ! Loop on pixels
  do j = 1, ny
    !
    ! Loop on Y, pixels locations on Fourier plane
    jj = mod(j-1+ny/2,ny)-ny/2
    yy = jj*dv
    !
    do i = 1, nx
      !
      ! Loop on X, pixels locations on Fourier plane
      ii = mod(i-1+nx/2,nx)-nx/2
      xx = ii*du
      !
      phi = -2.d0*pi*(offra*xx + offdec*yy)
      cp = cos(phi)
      sp = sin(phi)
      !
      re = real(f(i,j))*cp - imag(f(i,j))*sp
      im = real(f(i,j))*sp + imag(f(i,j))*cp
      f(i,j) = cmplx(re,im)
    enddo
  enddo
  !
end subroutine uvshort_shift
!
!-------------------------------------------------------------------------
!
subroutine uvshort_dotrans (a,b,n,m)
  !
  ! Output table "b" is table "a" transposed in line/column order
  !
  integer, intent(in) :: n,m
  real, intent(in) :: a(n,m)
  real, intent(out) ::  b(m,n)
  !
  ! Local variables
  integer i,j
  !
  ! Code
  do i=1,m
    do j=1,n
      b(i,j) = a(j,i)
    enddo
  enddo
end subroutine uvshort_dotrans
!
!-------------------------------------------------------------------------
!
subroutine uvshort_prmult(z,f,nx,ny)
  !
  ! Update z(nx,ny) table with z*f, calculate in uv plane
  ! Used to multiply by interferometer primary beam
  !
  integer, intent(in) :: nx, ny         ! Problem size
  complex, intent(inout) :: z(nx, ny)   ! Complex values
  real, intent(in) :: f(nx,ny)          ! Multiplication function
  !
  ! Local variables
  integer i, j, ii, jj
  !
  ! Code
  do j = 1, ny
    jj = mod(j+ny/2-1,ny)+1
    do i = 1, nx
      ii = mod(i+nx/2-1,nx)+1
      z(ii,jj) = z(ii,jj) * f(i,j)
    enddo
  enddo
  !
end subroutine uvshort_prmult
!
!-------------------------------------------------------------------------
!
subroutine uvshort_uvcount(nx,ny,nvis,dmax,dmin,lmv)
  use image_def
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Compute number of visibilities nvis sampled on
  ! a regular grid of steps dx,dy inside the dish defined by diam
  ! nx and ny needed because uvcount is working in Fourier plane
  !---------------------------------------------------------------------
  !
  type(gildas), intent(in) :: lmv   ! Input GILDAS image
  integer, intent(in) :: nx         ! X image size
  integer, intent(in) :: ny         ! Y image size
  integer, intent(out) :: nvis      ! Number of visibilities
  real, intent(in) :: dmax          ! Max baseline
  real, intent(in) :: dmin          ! Min baseline
  !
  ! Local variables
  !
  integer :: i, j, ii, jj
  real :: uu, vv, duv, max2, min2
  real(8), parameter :: clight=299792458d-6    ! frequency in mhz
  real(8) :: dx, dy
  !
  ! Code
  !
  dx = clight/lmv%gil%freq/(lmv%gil%inc(1)*lmv%gil%dim(1))
  dy = clight/lmv%gil%freq/(lmv%gil%inc(2)*lmv%gil%dim(2))
  max2 = dmax**2
  min2 = dmin**2
  !
  nvis = 0
  do j = 1, ny
    jj = mod(j-1+ny/2,ny)-ny/2
    vv = jj*dy
    do i = 1, nx/2
      ii = mod(i-1+nx/2,nx)-nx/2
      uu = ii*dx
      duv = uu*uu+vv*vv
      if (duv.ge.min2 .and. duv.le.max2) then
        nvis = nvis + 1
      endif
    enddo
  enddo
  !
end subroutine uvshort_uvcount
!
!--------------------------------------------------------------------------
!
subroutine uvshort_uvtable(nx,ny,nd,nc,v,w,ww,nvis,dmax,dmin,wfactor,factor,lmv)
  use image_def
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Tabulate the visibilities
  !---------------------------------------------------------------------
  !
  integer, intent(in) :: nx   ! X Image size
  integer, intent(in) :: ny   ! Y Image size
  integer, intent(in) :: nc   ! Number of channels
  integer, intent(in) :: nd   ! Visibility size
  integer, intent(in) :: nvis ! Number of visibilities
  complex, intent(in) :: v(nx,ny,nc)  ! Gridded visibilities
  complex, intent(in) :: ww(nx,ny)    ! Complex weights
  real, intent(out) :: w(nd,nvis)     ! Resulting visibilities
  real, intent(in) :: dmax    ! Maximum baseline
  real, intent(in) :: dmin    ! Minimum baseline
  real, intent(in) :: wfactor ! Weight factor
  real, intent(in) :: factor  ! Intensity factor
  type(gildas), intent(in) :: lmv     ! Input image
  !
  ! Local variables
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: clight=299792458d-6    ! frequency in mhz
  integer :: i, j, k ,kk, ii, jj, kvis, k00
  integer :: gdate
  real(4) :: uu, vv, sw, we, duv, wfact, max2, min2
  real(8) :: dx, dy
  !
  ! Code
  !
  dx = clight/lmv%gil%freq/(lmv%gil%inc(1)*lmv%gil%dim(1))
  dy = clight/lmv%gil%freq/(lmv%gil%inc(2)*lmv%gil%dim(2))
  max2 = dmax**2
  min2 = dmin**2
  !
  wfact = 1.0/(factor**2)
  kvis = 0
  sw = 0.0
  !
  !!Print *,' Wfactor ',wfactor,'     Wfact ',wfact
  !
  call sic_gagdate(gdate)
  !
  ! Loop on pixels of the visibility map
  !
  do j = 1, ny
    jj = mod(j-1+ny/2,ny)-ny/2
    vv = jj*dy
    do i = 1, nx/2
      ii = mod(i-1+nx/2,nx)-nx/2
      uu = ii*dx
      duv = uu**2+vv**2
      !
      ! Keep only points inside circle defined by diam
      !
      if (duv.ge.min2 .and. duv.le.max2) then
        kvis = kvis + 1
        w(1,kvis) = uu
        w(2,kvis) = vv
        w(3,kvis) = 0
        w(4,kvis) = gdate   ! Current date
        w(5,kvis) = 0
        w(6:7,kvis) = -1.0  ! Convention: Antenna # -1 for Short spacings
        kk = 7
        !
        ! Weight
        !
        we = real(ww(i,j))
        if (i.eq.1 .and. j.ne.1) then
          we = we*0.5
        endif
        if (we.lt.0) we = -we
        !
        ! u=0 v=0 point
        !
        if (duv.eq.0) k00 = kvis
        !
        ! Extract visibilities
        ! - apply K-to-Jy conversion factor
        ! - wfact = wfactor/factor**2
        !
        do k=1, nc
          w(kk+1,kvis) = real(v(i,j,k))*factor
          w(kk+2,kvis) = imag(v(i,j,k))*factor
          w(kk+3,kvis) = we*wfact
          kk = kk + 3
        enddo
        sw = sw+we*wfact
      endif
    enddo
  enddo
  !
  ! Test number of visibilities
  !
  if (kvis.ne.nvis) then
    write(6,*) 'W-UV_SHORT, Inconsistent number of visibilities'
  endif
  !
  ! Normalize the weights **WW**
  !
  if (sw.ne.0.) then
    sw = 1/sw
    do i=1,nvis
      do k=1,nc
        w(7+k*3,i) = w(7+k*3,i)*sw*wfactor
      enddo
    enddo
  endif
end subroutine uvshort_uvtable
!
!-------------------------------------------------------------------------
!
subroutine uvshort_dopoint(data,nd,np,xcol,ycol,old,new)
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Recompute data(nd,np) xcol and ycol coordinates values
  ! in case of changing reference position a0 d0 from old to new
  !---------------------------------------------------------------------
  !
  integer, intent(in) :: nd         ! Size of a visibility
  integer, intent(in) :: np          ! Number of visibilities
  integer, intent(in) :: xcol        ! RA offset pointer
  integer, intent(in) :: ycol        ! Dec offset pointer
  real, intent(inout) :: data(nd,np) ! Visibilities
  real(8), intent(in) :: old(2)      ! Old RA and Dec center
  real(8), intent(in) :: new(2)      ! New Ra and Dec center
  !
  ! Local variables
  !
  real(8) :: dra,dde,ra,de,uncde,cde
  integer :: i
  !
  ! The code here is only for the "Radio" projection.  Things
  ! should be done better
  !
  uncde = 1.d0/cos(old(2))
  cde = cos(new(2))
  !
  do i=1,np
    ra = old(1) + dble(data(xcol,i))*uncde
    de = old(2) + dble(data(ycol,i))
    dra = (ra - new(1)) * cde
    dde = de - new(2)
    data(xcol,i) = dra
    data(ycol,i) = dde
  enddo
  !
end subroutine uvshort_dopoint
!
subroutine uvshort_fill(lmv,uvt,error,nvis,nc,ra_off,de_off,positions,last)
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  !   Fill in header of short spacings UV data
  !---------------------------------------------------------------------
  !
  type(gildas), intent(in) :: lmv       ! Header of low resolution data cube
  type(gildas), intent(inout) :: uvt    ! Header of short spacings UV table
  logical, intent(out) :: error
  integer, intent(in) :: nvis ! Number of visibilities
  integer, intent(in) :: nc   ! Number of channels
  real, intent(in) :: ra_off  ! RA Offset
  real, intent(in) :: de_off  ! Declination Offset
  logical, intent(in) :: positions ! Do we add offset position columns ?
  integer, intent(out) :: last  ! Size of a visibility
  !
  integer :: i
  !
  call gdf_copy_header(lmv,uvt,error)
  uvt%char%code(2) = 'RANDOM'
  uvt%char%code(1) = 'UV-RAW'
  uvt%gil%coor_words = 6*gdf_maxdims
  uvt%gil%blan_words = 2
  uvt%gil%extr_words = 10
  uvt%gil%desc_words = 18
  uvt%gil%posi_words = 12
  uvt%gil%proj_words = 9
  uvt%gil%spec_words = 12
  uvt%gil%reso_words = 3
  uvt%gil%dim(2) = nvis
  uvt%gil%dim(1) = 3*nc+7        ! 7 daps + (real, imag, weight)*nchannels
  uvt%gil%convert = 0
  uvt%gil%ref(1) = lmv%gil%ref(3)
  uvt%gil%inc(1) = lmv%gil%fres
  uvt%gil%val(1) = lmv%gil%freq
  uvt%gil%inc(2) = 1.              ! needed to avoid funny crash in graphic...
  uvt%gil%ndim = 2
  uvt%gil%dim(3) = 1
  uvt%gil%dim(4) = 1
  !
  ! Here we could change the logic, keep A0,D0 and set the offsets
  ! in the UV table.  But this has consequences in the whole package.
  !
  ! The formula below is WRONG, especially at high declinations
  uvt%gil%ra = uvt%gil%a0+ra_off/cos(uvt%gil%d0)
  ! a better conversion should be used...
  uvt%gil%dec = uvt%gil%d0+de_off
  uvt%char%type = 'GILDAS_UVFIL'
  uvt%char%unit = 'Jy'
  uvt%gil%nchan = nc
  !
  ! Here define the order in which you want the extra "columns"
  uvt%gil%column_pointer = 0
  uvt%gil%column_size = 0
  uvt%gil%column_pointer(code_uvt_u) = 1
  uvt%gil%column_pointer(code_uvt_v) = 2
  uvt%gil%column_pointer(code_uvt_w) = 3
  uvt%gil%column_pointer(code_uvt_date) = 4
  uvt%gil%column_pointer(code_uvt_time) = 5
  uvt%gil%column_pointer(code_uvt_anti) = 6
  uvt%gil%column_pointer(code_uvt_antj) = 7
  uvt%gil%natom = 3
  uvt%gil%nstokes = 1
  uvt%gil%fcol = 8
  last = uvt%gil%fcol + uvt%gil%natom * uvt%gil%nchan - 1
  !
  uvt%gil%form = fmt_r4
  !
  if (positions) then
    last = last+1
    uvt%gil%column_pointer(code_uvt_loff) = last
    last = last+1
    uvt%gil%column_pointer(code_uvt_moff) = last
  endif
  uvt%gil%dim(1) = last
  !
  do i=1,code_uvt_last
    if (uvt%gil%column_pointer(i).ne.0) uvt%gil%column_size(i) = 1
  enddo
  !
  uvt%gil%nvisi = nvis
  uvt%gil%type_gdf = code_gdf_uvt
  call gdf_setuv (uvt,error)
  uvt%loca%size = uvt%gil%dim(1) * uvt%gil%dim(2)
end subroutine uvshort_fill
!
subroutine uv_short_consistency(rname,uvt,short,tole,error)
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  !   Verify spectral axis consistency
  !---------------------------------------------------------------------
  !
  character(len=*), intent(in) :: rname
  type(gildas), intent(in) :: uvt
  type(gildas), intent(in) :: short
  real, intent(in) :: tole
  logical, intent(out) :: error
  !
  real(8) :: va, vb
  real :: atole
  character(len=message_length) :: mess
  integer :: nc, ioff, faxi
  !
  error = .false.
  !
  ! Number of channels
  faxi = short%gil%faxi
  if (faxi.eq.1) then
    ioff = 3
  else
    ioff = 0
  endif
  nc = short%gil%dim(short%gil%faxi)-ioff
  !
  ! Number of channels
  if (uvt%gil%nchan.ne.nc) then
    write(mess,'(A,I0,A,I0)') 'in number of channels: UV ',uvt%gil%nchan,' SD ',nc
    call map_message(seve%w,rname,'Mismatch '//mess)
    error = .true.
    nc = min(nc,uvt%gil%nchan)
  endif
  !
  atole = 2*tole/nc
  !
  ! Check here the spectral axis mismatch
  if (abs(uvt%gil%vres-short%gil%vres).gt.abs(short%gil%vres*atole)) then
    write(mess,*) 'in spectral resolution: UV ',uvt%gil%vres,' SD ',short%gil%vres,atole
    call map_message(seve%w,rname,'Mismatch '//mess)
    error = .true.
  endif
  if (abs(uvt%gil%freq-short%gil%freq).gt.abs(short%gil%fres*atole)) then
    write(mess,*) 'in frequency axis: UV ',uvt%gil%freq,' SD ',short%gil%freq,atole
    call map_message(seve%w,rname,'Mismatch '//mess)
    error = .true.
  endif
  !
  ! Velocity should be checked too
  va = (1.d0-uvt%gil%ref(1))*uvt%gil%vres + uvt%gil%voff
  vb = (1.d0+ioff-short%gil%ref(faxi))*short%gil%vres + short%gil%voff
  if (abs(va-vb).gt.abs(short%gil%vres*tole)) then
    write(mess,*) 'in velocity axis, low end UV ',va, ', SD ', vb, tole 
    call map_message(seve%w,rname,'Mismatch '//mess)
    error = .true.
  endif
  va = (nc-uvt%gil%ref(1))*uvt%gil%vres + uvt%gil%voff
  vb = (nc+ioff-short%gil%ref(faxi))*short%gil%vres + short%gil%voff
  if (abs(va-vb).gt.abs(short%gil%vres*tole)) then
    write(mess,*) 'in velocity axis, high end  UV ',va,', SD ', vb, tole 
    call map_message(seve%w,rname,'Mismatch '//mess)
    error = .true.
  endif
  !
end subroutine uv_short_consistency
!
subroutine spectrum_to_zero(nc,spectrum,uvdata,date,weight)
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  ! Convert a spectrum into a Zero spacing
  !---------------------------------------------------------------------
  !
  integer, intent(in) :: nc
  real, intent(in) :: spectrum(nc) 
  real, intent(out) :: uvdata(:)
  integer, intent(in) :: date
  real, intent(in) :: weight
  !
  integer :: ic
  !
  uvdata = 0
  uvdata(4) = date
  uvdata(6:7) = -1.0      ! Conventional antenna number
  do ic=1,nc
    uvdata(5+3*ic) = spectrum(ic)
    uvdata(7+3*ic) = weight
  enddo
end subroutine spectrum_to_zero
!
subroutine uvshort_trim_short(rname,huv,duv,change)
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ public
  ! IMAGER
  !     Remove all short spacing visibilities
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(gildas), intent(inout) :: huv
  real, intent(inout) :: duv(:,:)
  logical, intent(out) :: change
  !
  integer(kind=index_length) :: iv,ov
  character(len=80) :: chain
  !
  ov = 0
  !
  do iv=1,huv%gil%nvisi
    if (duv(6,iv).eq.-1.0 .and. duv(7,iv).eq.-1.0) then
      continue
    else
      ov = ov+1
      if (ov.ne.iv) duv(:,ov) = duv(:,iv)
    endif
  enddo
  !
  if (ov.ne.huv%gil%nvisi) then 
    write(chain,'(A,I0,A)') 'Trimmed ',huv%gil%nvisi-ov+1,' Short spacing visibilities'
    call map_message(seve%i,rname,chain)
    huv%gil%nvisi = ov
    change = .true.
  else
    change = .false.
  endif
  huv%gil%dim(2) = huv%gil%nvisi ! Sinon y a un os
end subroutine uvshort_trim_short

