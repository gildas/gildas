subroutine define_var (error)
  use gildas_def
  use gkernel_interfaces
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use clean_support
  use clean_beams
  use grid_control
  !
  logical :: error  ! Logical error flag
  logical :: warning
  ! Local
  integer :: ier
  integer(kind=index_length) :: dim(4) = 0
  !
  ! CLEAN program variables
  clean_stop = ' '
  call sic_def_charn('CLEAN_STOP',clean_stop,1,2,.false.,error)
  user_method%m_iter = 0
  call sic_def_inte ('NITER',user_method%m_iter,0,dim,.false.,error)
  call sic_def_inte ('CLEAN_NITER',user_method%m_iter,0,dim,.false.,error)
  user_method%p_iter = 0
  call sic_def_inte ('CLEAN_POSITIVE',user_method%p_iter,0,dim,.false.,error)
  user_method%n_major = 50
  call sic_def_inte ('CLEAN_NCYCLE',user_method%n_major,0,dim,.false.,error)
  user_method%gain = 0.2
  call sic_def_real ('GAIN',user_method%gain,0,dim,.false.,error)
  call sic_def_real ('CLEAN_GAIN',user_method%gain,0,dim,.false.,error)
  user_method%fres = 0.01
  call sic_def_real ('FRES',user_method%fres,0,dim,.false.,error)
  call sic_def_real ('CLEAN_FRES',user_method%fres,0,dim,.false.,error)
  user_method%ares = 0.0
  call sic_def_real ('ARES',user_method%ares,0,dim,.false.,error)
  call sic_def_real ('CLEAN_ARES',user_method%ares,0,dim,.false.,error)
  user_method%converge = 0
  call sic_def_inte ('CLEAN_NKEEP',user_method%converge,0,dim,.false.,error)
  user_method%spexp = 1.0
  call sic_def_real ('CLEAN_SPEEDY',user_method%spexp,0,dim,.false.,error)
  user_method%ngoal = 10000
  call sic_def_inte ('CLEAN_NGOAL',user_method%ngoal,0,dim,.false.,error)
  !
  ! MultiScale
  user_method%nker = (/1,7,11/)
  dim(1) = 3
  call sic_def_inte ('CLEAN_KERNEL',user_method%nker,1,dim,.false.,error)
  user_method%ninflate = 121   ! Absolutely safe method, but uses Memory...
  call sic_def_inte ('CLEAN_INFLATE',user_method%ninflate,0,0,.false.,error)
  user_method%smooth = sqrt(3.0)
  call sic_def_real ('CLEAN_SMOOTH',user_method%smooth,0,dim,.false.,error)
  user_method%worry = 0.0
  call sic_def_real ('CLEAN_WORRY',user_method%worry,0,dim,.false.,error)
  !
  ! Search area
  dim(1) = 2
  user_method%blc = 0
  call sic_def_inte ('BLC',user_method%blc,1,dim,.false.,error)
  user_method%trc = 0
  call sic_def_inte ('TRC',user_method%trc,1,dim,.false.,error)
  !
  ! Beam fitting
  user_method%thresh = 0.35
  call sic_def_real ('CLEAN_SIDELOBE',user_method%thresh,0,dim,.false.,error)
  beam_size = 0.0
  dim(1) = 3
  call sic_def_real ('BEAM_SIZE',beam_size,1,dim,.false.,error)
!  call sic_def_real ('MAJOR',beam,0,dim,.false.,error)
!  call sic_def_real ('MINOR',minor_sec,0,dim,.false.,error)
  ! Delete the ANGLE variable defined by define.greg and recreate it
  ! such as it maps "angle_deg"
  call sic_delvariable('ANGLE',.true.,warning)
!  angle_deg = 0.0
!  call sic_def_real ('ANGLE',angle_deg,0,dim,.false.,error)  
  !
  ! Restoration
  user_method%residual = 0
  call sic_def_inte ('CLEAN_RESIDUAL',user_method%residual,0,dim,.false.,error)
  !
  ! MRC
  user_method%ratio = 0
  call sic_def_real ('CLEAN_RATIO',user_method%ratio,0,dim,.false.,error)
  call sic_def_real ('CLEAN_DMIN',d_min,0,dim,.false.,error)
  call sic_def_real ('CLEAN_DMAX',d_max,0,dim,.false.,error)
  !
  ! Mosaic mode
  user_method%search = 0.2
  call sic_def_real ('CLEAN_SEARCH',user_method%search,0,dim,.false.,error)
  ! Sky restoration threshold (CLEAN on Mosaics, or PRIMARY)
  user_method%restor = 0.2
  call sic_def_real ('CLEAN_TRUNCATE',user_method%restor,0,dim,.false.,error)
  ! Delete the MOSAIC variable defined by define.greg and recreate it
  ! such as it maps 'user_method%mosaic'
  call sic_delvariable('MOSAIC',.true.,warning)
  user_method%mosaic = .false.
  call sic_def_logi ('MOSAIC',user_method%mosaic,.false.,error)
  !
  ! Rather obscure parameters
  ! Beam subset used in CLARK method
  dim(1) = 2
  user_method%patch = 0
  call sic_def_inte ('BEAM_PATCH',user_method%patch,1,dim,.false.,error)
  !
  user_method%verbose = .false.
  call sic_def_logi ('VERBOSE',user_method%verbose,.false.,error)
  ! Compact Array parameters
  cuse_method%gain = 0.4
  call sic_def_real ('C_GAIN',cuse_method%gain,0,dim,.false.,error)
  !
  ! UV_MAP variables
  nbeam_ranges = -1
  call sic_def_inte('NBEAM_RANGES',nbeam_ranges,0,dim,.false.,error)
  default_map%precis = 0.1       ! Requested precision
  call sic_def_real ('MAP_PRECIS',default_map%precis,0,dim,.false.,error)
  default_map%beam = 0           ! Number of channels per beam
  call sic_def_inte ('BEAM_STEP',default_map%beam,0,dim,.false.,error)
  dim = [2,0,0,0]
  default_map%field = 0          ! Map field in arcsecond
  call sic_def_real ('MAP_FIELD',default_map%field,1,dim,.false.,error)
  default_map%size = 0           ! Map size in Pixels
  call sic_def_inte ('MAP_SIZE',default_map%size,1,dim,.false.,error)
  default_map%xycell = 0         ! Map cell size in arcsecond
  call sic_def_real ('MAP_CELL',default_map%xycell,1,dim,.false.,error)
  dim = [3,0,0,0]
  default_map%taper = 0          ! UV Taper in m and Degree
  call sic_def_real ('MAP_UVTAPER',default_map%taper,1,dim,.false.,error)
  default_map%taper(4) = 2       ! Taper exponent
  call sic_def_real ('MAP_TAPEREXPO',default_map%taper(4),0,dim,.false.,error)
  !
  default_map%uniform(1) = 0.0      ! UV cell size
  call sic_def_real ('MAP_UVCELL',default_map%uniform(1),0,dim,.false.,error)
  default_map%uniform(2) = 1.0      ! Robustness coefficient
  call sic_def_real ('MAP_ROBUST',default_map%uniform(2),0,dim,.false.,error)
  ! No longer used 
  ! default_map%mode = 'NATURAL'   ! Weighting mode
  ! call sic_def_char ('MAP_WEIGHT',default_map%mode,.false.,error)
  !
  default_map%truncate = 0.0       ! Truncation level (Primary beam in Mosaic)
  call sic_def_real ('MAP_TRUNCATE',default_map%truncate,0,dim,.false.,error)
  !
  default_map%ctype = 5    ! Convolution code
  call sic_def_inte ('MAP_CONVOLUTION',default_map%ctype,0,dim,.false.,error)
  !
  ! Recommended Values
  recmap_field = 0          ! Map field in arcsecond
  call sic_def_real ('REC_FIELD',recmap_field,1,dim,.false.,error)
  recmap_size = 0           ! Map size in Pixels
  call sic_def_inte ('REC_SIZE',recmap_size,1,dim,.false.,error)
  recmap_xycell = 0         ! Map cell size in arcsecond
  call sic_def_real ('REC_CELL',recmap_xycell,1,dim,.false.,error)
  recmap_uvcell = 0         ! Map cell size in arcsecond
  call sic_def_real ('REC_UVCELL',recmap_uvcell,0,0,.false.,error)
  !
  ! Old names for compatibility
  default_map%shift = .false.
  call sic_def_logi ('MAP_SHIFT',default_map%shift,.false.,error)
  default_map%ra_c = ' '   ! Weighting mode
  call sic_def_char ('MAP_RA',default_map%ra_c,.false.,error)
  default_map%dec_c = ' '   ! Weighting mode
  call sic_def_char ('MAP_DEC',default_map%dec_c,.false.,error)
  default_map%angle = 0.0      ! Robustness coefficient
  call sic_def_real ('MAP_ANGLE',default_map%angle,0,dim,.false.,error)
  !
  !   define logical uv_shift /global
  !   define integer convolution /global
  !   define double uv_cell[2] uv_taper[3] taper_expo /global
  old_map = default_map
  call sic_def_inte ('CONVOLUTION',old_map%ctype,0,dim,.false.,error)
  call sic_def_logi ('UV_SHIFT',old_map%shift,.false.,error)
  old_map%mode = 'NATURAL'     ! Make sur this matches define.map
  call sic_def_char ('WEIGHT_MODE',old_map%mode,.false.,error)
  dim = [2,0,0,0]
  old_map%uniform = [7.5,1.0]  ! Make sure this matches define.map
  call sic_def_real ('UV_CELL',old_map%uniform(1),1,dim,.false.,error)
  dim = [3,0,0,0]
  call sic_def_real ('UV_TAPER',old_map%taper,1,dim,.false.,error)
  call sic_def_real ('TAPER_EXPO',old_map%taper(4),0,dim,.false.,error)
  ! Initialize status
  save_map = old_map
  !
  ! File types and Extensions
  dim(1) = mtype
  call sic_def_charn('FILE_TYPE',vtype,1,dim,.true.,error)
  call sic_def_charn('FILE_EXT',etype,1,dim,.false.,error)
  !
  ! Delete the TYPE variable defined by define.greg and recreate it
  ! such as it maps 'display_type'
  call sic_delvariable('TYPE',.true.,warning)
!  dim(1) = 0
  display_type = ' '  ! Make sure it is initialized
!  call sic_def_char('TYPE',display_type,.false.,error)
  !
  call gildas_null(hbeam)
  call gildas_null(hclean)
  call gildas_null(hresid)
  call gildas_null(hdirty)
  !
  call gildas_null(hprim)
  call gildas_null(huv, type = 'UVT')
  call gildas_null(huvi, type = 'UVT')
  call gildas_null(huvt, type = 'UVT')
  call gildas_null(huvm, type = 'UVT')
  call gildas_null(huvf, type = 'UVT')
  call gildas_null(hcct)
  call gildas_null(hmask)
  call gildas_null(hsky)
  call gildas_null(hview)
  call gildas_null(hsingle)
  !
  ! Read/Write Optimization
  rw_optimize = 0
  ier = sic_getlog ('MAPPING_OPTIMIZE',rw_optimize)
  call sic_def_inte('MAPPING_OPTIMIZE',rw_optimize,0,dim,.false.,error)
  !
  ! Debug
  call sic_defstructure('GRIDDING',.true.,error)
  call sic_def_logi('GRIDDING%WEIGHT',do_weig,.false.,error)
  call sic_def_inte('GRIDDING%BIG',grid_bigvisi,0,1,.false.,error)
  call sic_def_inte('GRIDDING%CELL',grid_subcell,0,1,.false.,error)
  call sic_def_inte('GRIDDING%MODE',grid_weighting,0,1,.false.,error)
  !
  ! Last type of active Image (CLEAN or SKY)
  call sic_def_char('LAST_SHOWN',last_shown,.false.,error)
  !
  ! Type of current UV_DATA
  call sic_def_char('CURRENT_UV',current_uvdata,.false.,error)
  !
  ! Default MAP_CENTER
  defmap_center = ' '
  call sic_def_char('MAP_CENTER',defmap_center,.false.,error)
end subroutine define_var
!
