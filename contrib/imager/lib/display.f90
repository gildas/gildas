subroutine display_buffer(comm,line,error)
  use gkernel_interfaces
  use imager_interfaces, only : display_buffer_sub, map_message
  use clean_arrays
  use clean_def
  use clean_types
  use clean_support
  use clean_default
  use uvfit_data
  use gbl_message
  !----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER support routine for command
  !     SHOW Name
  !----------------------------------------------------------------------
  character(len=*), intent(in)  :: comm  ! Command (SHOW or VIEW)
  character(len=*), intent(in)  :: line  ! Command line
  logical,          intent(out) :: error ! Logical error flag
  !
  character(len=1), parameter :: question_mark='?'
  integer, parameter :: o_side=1
  integer, parameter :: o_nopause=1
  integer, parameter :: o_overlay=2
  real(8), parameter :: pi=acos(-1d0)
  !
  ! This Boolean indicates if the LUT should be switched back to Dynamic
  ! logical, save :: reset_lut=.false.
  ! Local variables
  type(gildas) :: head
  integer :: o_args
  integer(kind=size_length) :: locasize
  type(sic_descriptor_t) :: descr
  type(gildas) :: htmp
  logical :: found, do_insert, done, do_loop, do_color, err, do_nopause, do_side
  integer, parameter :: mnoise=3
  integer ntype,nc,is,istart,ic,i, code, iover, nt
  real :: colour
  character(len=filename_length) :: argu,file,chain 
  character(len=filename_length), save :: sed_file=' '
  real(4), save :: sed_freq=0
  character(len=80) :: mess
  character(len=40) :: argu1,argu2,vover, dtype, topic !
  character(len=12) :: dnoise 
  character(len=8) :: cshort
  ! integer :: nnoise
  ! character(len=6) :: vnoise(mnoise)=['DIRTY ', 'CLEAN ', 'SKY   ']
  character(len=6) :: vuvfit='RESET '
  !
  integer :: iarg
  integer :: l_axis, ioff, ier
  real :: soff
  !
  o_args = 0
  iarg = 1
  do_nopause = .false.
  do_side = .false.
  !
  if (comm.eq.'VIEW') then
    ! Allow VIEW /NOPAUSE Args    as well as VIEW Args /NOPAUSE
    if (sic_narg(o_nopause).gt.0) then
      if (sic_narg(0).ne.0) then
        call map_message(seve%e,comm,'Invalid syntax in VIEW /NOPAUSE command')
        error = .true.
        return
      else
        o_args = o_nopause
      endif
      do_nopause = .true.
    else if (sic_narg(o_nopause).eq.0) then
      do_nopause = .true.
    endif
    if (sic_present(o_overlay,0)) then
      ! /OVERLAY Variable [Channel]
      ! view%cplane,  view%cont and view%rplane
      vover = 'CONTINUUM'
      call sic_ke(line,o_overlay,1,vover,iover,.false.,error)
      if (error) return
      call sic_let_char('VIEW%CONT',vover,error)
      iover = 0
      call sic_i4(line,o_overlay,2,iover,.false.,error)
      if (error) return
      call sic_let_inte('VIEW%CPLANE',iover,error)      
    else
      vover = ' '
      call sic_let_char('VIEW%CONT',vover,error)
    endif
  else 
    if (comm.eq.'SHOW') then
      ! Allow SHOW Args /SIDE as well as SHOW /SIDE Args
      if (sic_narg(o_side).gt.0) then
        if (sic_narg(0).ne.0) then
          call map_message(seve%e,comm,'Invalid syntax in SHOW /SIDE command')
          error = .true.
          return
        else
          o_args = o_side
        endif
      endif
    endif
    call sic_let_logi('VIEW%FIRST',.true.,error)
  endif  
  !
  ! Fall back to last Displayed item if no Argument is given
  if (.not.sic_present(o_args,1)) then
    if (len_trim(last_shown).eq.0) then
      cshort = comm(1:7)
      call map_message(seve%e,comm,'No data to re-'//trim(cshort)//' yet')
      error = .true.
      return
    endif
    argu = last_shown
    iarg = 0
  endif
  !
  ! Parse input line
  if (iarg.ne.0) then
    call sic_ke(line,o_args,iarg,argu,nc,.true.,error)
    if (error) return
  endif
  !
  ! Should we support 
  !   COMM Topic ?  
  ! as equal to 
  !   COMM ? Topic
  !
  topic = ' '
  call sic_ch(line,0,2,topic,nt,.false.,error)
  if (argu(1:1).eq.question_mark) then
    if (nt.gt.0) then
      chain = 'HELP '//comm//topic
      call exec_program(chain)
      return        
    endif
    argu1 = '@ i_'//comm
    call sic_lower(argu1)
    argu1 = trim(argu1) //' '//argu
    call exec_program(argu1) 
    call sic_ambigs_list(comm,seve%i,'Choices are:',vtype)
    return
  else if (topic.eq.question_mark) then
    select case (argu)
    case ('UV') 
      argu1 = '@ i_show_uv'
      call exec_program(argu1)
      return
    case ('KEPLER','MOMENTS','PV') ! ,'COMPOSITE') ! Special cases
      continue
    case default
      chain = 'HELP '//comm//argu
      call exec_program(chain)
      return        
    end select
  else if (argu.eq.'UV') then
    argu = 'UV_DATA'    
  endif
!
! Code for non-ambiguous abbreviations (dangerous with variables...)
!      
!  call sic_ambigs(comm,argu,dtype,ntype,vtype,mtype,error)
!  if (error) then
!    error = .false.
!    ntype = 0
!  endif
  !
  ! Code for exact name matching - Will avoid taking an abbreviation
  ! of a keyword in place of a SIC variable...
  !
  dtype = argu
  ntype = 0
  if (dtype.ne.'PV') then
    do is=1,mtype
      if (argu.eq.vtype(is)) then
        ntype = is
        dtype = vtype(ntype)
        exit
      endif
    enddo
    if (ntype.eq.0) then
      call sic_descriptor (argu,descr,found)    ! Checked
      if ((.not.found).and.(iarg.ne.0)) then
        call sic_ch(line,o_args,iarg,argu,nc,.true.,error)
        found = sic_findfile(argu,file,' ',' ')
        if (found) then
          dtype = 'FILENAME'
        else
          call map_message(seve%e,comm, &
          'Cannot '//trim(comm)//' '//trim(file)//' (no such file or SIC variable)')
          last_shown = ' '
          error = .true.
          return
        endif
      endif
    endif
  endif
  !
  !
  ! Special cases first
  do_insert = sic_lire().eq.0
  done = .false.
  do_color = .false.  ! No Color LUT in most actions
  !
  select case (dtype)
  case ('UV_FIT')
    if (sic_present(o_args,2)) then
      call sic_ke(line,o_args,2,argu,nc,.false.,error)
      if (error) return
      if (argu(1:1).eq.question_mark) then
        call exec_program('@ i_plotfit '//argu)
      else
        nc = min(nc,6)
        if (argu(1:nc).eq.vuvfit(1:nc)) then
          call exec_program('@ d_plotfit ')
        else
          call map_message(seve%e,comm,'Invalid SHOW UV_FIT argument '//trim(argu))
          error = .true.
          return
        endif
      endif
    else
      if (huvfit%loca%size.eq.0) then
        call map_message(seve%e,comm,'No UV_FIT performed')
        error = .true.
      else 
        call exec_program('@ p_plotfit UV_FIT')
      endif
    endif
    done = .true. 
  case ('SED')
    call sic_r4(line,0,2,sed_freq,.false.,error)
    if (error) return
    call sic_ch(line,0,3,sed_file,nc,.false.,error)
    if (error) return
    nc = len_trim(sed_file)
    if (nc.eq.0) then
      call map_message(seve%e,comm,'No file name specified')
      error = .true.
      return
    endif
    write(argu2,*) sed_freq
    call exec_program('@ p_show_sed '//sed_file(1:nc)//argu2//' COLOR')
    done = .true.
  case ('FLUX') 
    ! Pass arguments as they are given - DISPLAY\SHOW FLUX is 18 chars
    call exec_program('@ p_show_flux '//line(19:))
    done = .true.
  case ('COMPOSITE') 
    call exec_program('@ p_show_composite')
    done = .true.
  case ('FIELDS') 
    if (hprim%loca%size.eq.0 .or. comm.eq.'SHOW') then
      if (themap%nfields.eq.0) then
        call map_message(seve%e,comm,'No Mosaic loaded')
        error = .true.
      else
        call exec_program('@ p_plot_fields FIELDS')
        done = .true.
      endif
    else
      call create_fields(error)
      if (error) return
      dtype = 'FIELDS'
    endif
  case ('PRIMARY') 
    !! Print *,'Calling Create Fields'
    call create_fields(error)
    if (error) return
    dtype = 'FIELDS'
    !
  case ('SELFCAL')
    is = sic_start(0,2)
    if (is.eq.0) then
      call exec_program('@ p_show_selfcal ')
    else
      call exec_program('@ p_show_selfcal '//line(is:))
    endif
    done = .true.
  case('MOMENTS') 
    if (sic_present(o_args,2)) then
      call sic_ke(line,o_args,2,argu,nc,.true.,error)
      if (argu(1:1).eq.'?') then
        call exec_program('@ i_show_moments '//argu)
        done = .true.
        return
      endif
    endif
    call exec_program('@ p_show_moments ')
    done = .true.
    do_color = .false. ! ? unclear here...
  case ('NOISE','PV')
    if ((sic_narg(o_args).lt.2).and.(dtype.eq.'PV')) then
      call exec_program('@ p_show_map PV')   
      done = .true.
    else
      !
      dnoise = ' '
      istart = 2
      if (sic_present(o_args,2)) then
        call sic_ke(line,o_args,2,argu,nc,.true.,error)
        if (error) return
        if (argu(1:1).eq.'?') then
          call exec_program('@ i_show_pv '//argu)
          done = .true.
          return
        endif
        !
        if (argu.eq.'CLEAN' .or. argu.eq.'DIRTY' .or. argu.eq.'SKY') then
          dnoise = argu
          istart = 3
        endif
      endif
      !
      if (dnoise.eq.' ') then
        !
        ! Fall back on the last displayed or computed image
        if (last_shown.eq.'CLEAN' .or. last_shown.eq.'DIRTY' .or. last_shown.eq.'SKY') then
          dnoise = last_shown
        else  if (hsky%loca%size.ne.0) then
          dnoise = 'SKY'
        else  if (hclean%loca%size.ne.0) then
          dnoise = 'CLEAN'
        else if (hdirty%loca%size.ne.0) then
          dnoise = 'DIRTY'
        else
          call map_message(seve%e,comm,'No SKY, CLEAN or DIRTY image')
          error = .true.
          return
        endif
      endif
      !
      if (dtype.eq.'PV') then
        ! Next argument must be X or Y
        call sic_ke(line,o_args,istart,argu,nc,.true.,error)
        if (error) return
        if (argu.eq.'X') then
          l_axis = 1
        else if (argu.eq.'Y') then
          l_axis = 2
        else
          call map_message(seve%e,comm,'Invalid axis, must be X or Y')
          error = .true.
          return      
        endif
        !
        ! Then Offset along the other X or Y direction
        istart = istart+1
        call sic_r4(line,o_args,istart,soff,.true.,error)   
        if (error) return   
      else
        ! Standard FIRST + LAST ...
        !
        chain = ' 0'
        if (sic_narg(o_args).ge.istart) then
          ic = 2
          do i=istart, sic_narg(o_args)
            call sic_ch(line,o_args,i,argu,nc,.true.,error)
            chain(ic:) = argu(1:nc)
            ic = ic+nc+2
          enddo
        endif      
      endif
      !   
      call gildas_null(head) 
      if ((dtype.eq.'PV').and.(hpv%gil%ndim.ne.0)) then
        deallocate(hpv%r2d)
        hpv%gil%ndim = 0
        call sic_delvariable('PV',.false.,error)
        error = .false.
      endif
      !
      if (dnoise.eq.'CLEAN') then
        locasize = hclean%loca%size
        if (dtype.eq.'PV') call gdf_copy_header(hclean,head,error)
        head%r3d => dclean
      else if (dnoise.eq.'SKY') then
        locasize = hsky%loca%size
        if (dtype.eq.'PV') call gdf_copy_header(hclean,head,error)
        head%r3d => dclean
      else if (dnoise.eq.'DIRTY') then
        locasize = hdirty%loca%size
        if (dtype.eq.'PV') call gdf_copy_header(hclean,head,error)
        head%r3d => dclean
      else if (sic_varexist(dnoise)) then
        locasize = -1
      else
        locasize = 0
      endif
      if (locasize.eq.0) then
        call map_message(seve%e,comm,'No '//trim(dnoise)//' image')
        error = .true.
        return
      endif
      if (dtype.eq.'NOISE') then
        if (comm.eq.'SHOW') then
          call exec_program('@ p_show_noise '//dnoise//chain)
          done = .true.
        else if (comm.eq.'VIEW') then
          call exec_program('@ p_view_noise '//dnoise//chain)
          done = .true.
        endif
      else if (dtype.eq.'PV') then
        if (locasize.eq.-1) then
          call map_message(seve%e,comm,'PV Only valid for DIRTY, CLEAN or SKY')
          error = .true.
          return
        endif
        !
        ! Extract from current data...
        ioff = (soff*pi/180/3600 - head%gil%val(l_axis)) / head%gil%inc(l_axis) + head%gil%ref(l_axis)
        if (ioff.lt.1 .or. ioff.gt.head%gil%dim(l_axis)) then
          call map_message(seve%e,comm,'Offset out of range')
          error = .true.
          return
        endif
        write(mess,'(A,I0,A,F10.2,A,I0)') 'Extracting along Axis ',l_axis,' at Offset ',soff,'", Plane ',ioff
        call map_message(seve%i,comm,mess)
        call gildas_null(hpv)
        if (l_axis.eq.1) then
          call gdf_transpose_header(head, hpv, '231', error)
          hpv%gil%ndim = 2
          allocate(hpv%r2d(hpv%gil%dim(1),hpv%gil%dim(2)),stat=ier)
          hpv%r2d(:,:) = head%r3d(ioff,:,:)
        else
          call gdf_transpose_header(head, hpv, '132', error)
          hpv%gil%ndim = 2
          allocate(hpv%r2d(hpv%gil%dim(1),hpv%gil%dim(2)),stat=ier)
          hpv%r2d(:,:) = head%r3d(:,ioff,:)
        endif
        hpv%gil%dim(3) = 1
        call sic_mapgildas('PV',hpv,error,hpv%r2d)
        call exec_program('@ p_show_map PV')   
        !
        done = .true.
      endif
    endif
  case ('SUPPORT') 
    if (supportpol%ngon.ge.2) call greg_poly_plot(supportpol,error) 
    done = .true.
  case ('SNR')
    if (method%mosaic) then
      call exec_program('@ p_snr '//comm)
      done = .true.
    else
      call map_message(seve%e,comm,trim(comm)//' SNR only available in Mosaic mode')
      error = .true.
    endif
    do_color = .true.
  case ('SPECTRA') 
    argu = ' '
    call sic_ch(line,o_args,2,argu,nc,.false.,error)
    call exec_program('@ p_spectra '//argu)
    done = .true.
  case ('FILENAME') 
    !
    ! General [First [Last]] channel selection
    argu1 = ' '
    call sic_ch(line,o_args,2,argu1,nc,.false.,error)
    argu2 = argu1
    call sic_ch(line,o_args,3,argu2,nc,.false.,error)
    !
    chain = 'LOAD '//trim(file)
    call exec_command(chain,error)
    if (error) return
    !!    last_shown = ' ' ! May be Needed ???
    !
    chain = 'DATA '//trim(argu1)//' '//trim(argu2)
    if (comm.eq.'SHOW') then
      do_loop  = show_side 
      if (do_side) show_side = .true.
      call exec_program('@ p_show_map '//chain)   
      show_side = do_loop 
    else if (comm.eq.'INSPECT_3D') then
      call exec_program('@ p_3view_map '//chain)
    else  
      if (iarg.ne.0) then
        call sic_let_logi('VIEW%DO%WINDOW',.true.,error)
        call sic_let_logi('VIEW%DO%EXTREMA',.true.,error)
        call sic_let_logi('VIEW%DO%FLUX',.true.,error)
        call sic_let_logi('VIEW%DO%LINE',.true.,error)
        error = .false.
      endif
      do_loop = view_do_loop
      if (do_nopause) view_do_loop = .false.
      call exec_program('@ p_view_map '//chain)
      view_do_loop = do_loop
    endif
    last_shown = 'DATA'
    done = .true.
    do_color = .true.
  case ('SOURCES') 
    call show_sources_comm(line,error)
    done = .true.
  case ('CLEAN') 
    !
    ! Fall back onto SKY if CLEAN does not exist...
    argu = dtype
    call sic_descriptor (argu,descr,found)    ! Checked
    if (.not.found) then
      argu = 'SKY'
      call sic_descriptor (argu,descr,found)    ! Checked
      if (found) then
        dtype = 'SKY'
        call map_message(seve%w,comm,'No CLEAN data, falling back on SKY')
      endif
    endif
    do_color = .true.
  case ('UV_DATA')
    !
    call gildas_null(htmp,type='UVT')
    call uv_buffer_finduv(code) 
    call gdf_copy_header(huv,htmp,error)
    call uvdata_select(comm,error)
    if (error) return
    !
    call display_uv(current_uvdata,line,error)
    done = .true.
    do_color = .false.
    ! Reset the previous UV data pointers
    call uv_buffer_resetuv(code) 
    call gdf_copy_header(htmp,huv,err)
  case ('COVERAGE')
    ntype = 1
    done = .false.
  case ('KEPLER')
    argu = ' '
    call sic_ch(line,o_args,2,argu,nc,.false.,error)
    call exec_program('@ p_kepler_show '//argu)
    done = .true.  
  case default
    ! Any unknown variable - We must check what sort of data this is
    ! before calling any script (UV or datacube)
    !! Print *,'Case DEFAULT '//dtype
    do_color = .true.
    argu = dtype
    call sic_descriptor (argu,descr,found)    
    if (.not.found) then
      call map_message(seve%e,comm,'No such Buffer or SIC variable ' &
      & //trim(dtype))
      error = .true.
      return
    endif
    !
    do_color = .true.
    if (associated(descr%head)) then    
      if (abs(descr%head%gil%type_gdf).eq.abs(code_gdf_uvt)) then  
        ! This is a SHOW UV like plot...
!        argu1 = ' '
!        call sic_ch(line,o_args,2,argu1,nc,.false.,error)
!        argu2 = argu1
!        call sic_ch(line,o_args,3,argu2,nc,.false.,error)
        !
        chain = trim(dtype)//' '//trim(argu1)//' '//trim(argu2)
        call display_uv(chain,line,error)
        done = .true.
        do_color = .false.
      endif
    endif
  end select
  !
  ! Check error status
  if (error) return
  !
  if (.not.done) then
    if (comm.eq.'VIEW') then
      if (iarg.ne.0) then
        call sic_let_logi('VIEW%DO%WINDOW',.true.,error)
        call sic_let_logi('VIEW%DO%EXTREMA',.true.,error)
        call sic_let_logi('VIEW%DO%FLUX',.true.,error)
      endif
    endif
    ! Display the buffer if not already done
    call display_buffer_sub(comm,ntype,dtype,line,o_args,error)
    if (dtype.eq.'CLEAN' .or. dtype.eq.'DIRTY' .or. dtype.eq.'SKY') then
      last_shown = dtype
    else if (dtype.eq.'DATA') then
      last_shown = 'DATA'
    endif
    !
  endif
  !
  ! Set the Color LUT if needed
  if (do_color) then
    call sic_get_real('SHOW_COLOR',colour,error)
    if (colour.ne.0) then
      call sic_let_real('COLOR[3]',colour,error)
      call exec_program('@ p_color')
    endif
  endif
  !
  ! Insert command if needed
  if (do_insert) call sic_insert_log(line)
  !
end subroutine display_buffer
!
subroutine display_buffer_sub(comm,ntype,dtype,line,o_args,error)
  use gkernel_interfaces
  use imager_interfaces, only : display_uv, cct_integrate, map_message
  use clean_types
  use clean_arrays
  use clean_default
  use gbl_message
  !----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER support routine for command
  !     SHOW or VIEW Name
  !
  !----------------------------------------------------------------------
  character(len=*), intent(in) :: comm
  integer, intent(in)  :: ntype ! Buffer type
  character(len=*), intent(in) :: dtype
  character(len=*), intent(in) :: line
  integer, intent(in) :: o_args ! Where are the arguments ?
  logical, intent(out) :: error ! Logical error flag
  !
  integer, parameter :: o_nopause=1
  integer, parameter :: o_side=1
  !
  character(len=40) :: argu1,argu2,c_more,chain
!  character real(kind=8) :: space
  integer :: nc,first,last,ic
  logical :: do_dirty, do_loop, needed
  character(len=12) :: xtype,ytype
  !
  ! Load into SIC buffer
  if (comm.eq.'SHOW' .and. ntype.eq.1) then
    !
    ! SHOW Coverage
    call sic_get_inte('FIRST',first,error)
    call sic_get_inte('LAST',last,error)
    call sic_get_char('XTYPE',xtype,nc,error)
    call sic_get_char('YTYPE',ytype,nc,error)
    call sic_let_char('XTYPE','u',error)
    call sic_let_char('YTYPE','v',error)
    if (first.eq.0 .and. last.eq.0) then
      ic = huv%gil%nchan/3
      call sic_let_inte('FIRST',ic,error)
      call sic_let_inte('LAST',ic,error)
    endif
    chain = 'UV'
    call display_uv (chain,line,error)
    call sic_let_char('XTYPE',xtype,error)
    call sic_let_char('YTYPE',ytype,error)
    call sic_let_inte('FIRST',first,error)
    call sic_let_inte('LAST',last,error)
    return
  endif
  !
  ! General [First [Last]] channel selection
  argu1 = ' '
  call sic_ch(line,o_args,2,argu1,nc,.false.,error)
  argu2 = argu1
  call sic_ch(line,o_args,3,argu2,nc,.false.,error)
  !
  ! Plot SIC buffer with adequate plotting procedure
  if (comm.eq.'SHOW' .or. comm.eq.'UV_FLAG') then
    if (dtype.eq.'UV_DATA') then
      if (argu1.eq."?" .and. argu2.eq.argu1) then
        call exec_program('@ i_show_uv')
        return
      endif
      chain = 'UV '! //trim(argu1)//' '//trim(argu2)
      call display_uv(chain,line,error)
    else if (dtype.eq.'CCT') then
      if (hcct%loca%size.eq.0) then
        call map_message(seve%e,comm,'No CCT buffer')
        error = .true.
        return
      endif
      argu1 = ' '
      call sic_ch(line,o_args,2,argu1,nc,.false.,error)
      call cct_integrate(comm,hcct,dcct,error)
      call exec_program('@ p_show_cct cct '//argu1)
    else if (dtype.eq.'BEAM' .or. dtype.eq.'FIELDS') then
      !
      ! SHOW BEAM [Field F|Plane P] [First Last]
      ! @ p_show_beam BEAM FIELD 16
      !   or
      ! @ p_show_beam BEAM PLANE 1 [First Last]
      !
      if (dtype.eq.'BEAM') then
        if (hbeam%loca%size.eq.0) then
          call map_message(seve%e,comm,'No BEAM buffer')
          error = .true.
          return
        endif
        needed = .false.
        if (hbeam%gil%ndim.eq.4) then
          needed = (hbeam%gil%dim(4).gt.1).and.(hbeam%gil%dim(3).gt.1)
        endif
      else
        if (hfields%loca%size.eq.0) then
          call map_message(seve%e,comm,'No FIELDS buffer')
          error = .true.
          return
        endif
        needed = .false.
        if (hfields%gil%ndim.eq.4) then
          needed = (hfields%gil%dim(4).gt.1).and.(hfields%gil%dim(3).gt.1)
        endif      
      endif
      if (needed.and.sic_narg(0).lt.3) then
        call map_message(seve%e,comm,'Missing PLANE or FIELD key and number')
        error = .true.
        return
      endif
      argu1 = 'PLANE'
      call sic_ke(line,o_args,2,argu1,nc,needed,error)
      argu2 = '1'
      call sic_ch(line,o_args,3,argu2,nc,needed,error)
      if (error) return
      !      
      call exec_program('@ p_show_beam '//dtype//trim(argu1)//' '//trim(argu2))
    else
      c_more = ' '
      call sic_get_logi('DO_DIRTY',do_dirty,error)
      if (do_dirty) then
        c_more = 'BEAM'
      endif
      !
      chain = dtype//trim(c_more)//' '//trim(argu1)//' '//trim(argu2)
      !
      do_loop = show_side
      if (sic_present(o_side,0)) show_side = .true.
      chain = dtype//trim(argu1)//' '//trim(argu2)
      call exec_program('@ p_show_map '//chain)
      show_side = do_loop
      !
    endif
  else if (comm.eq.'VIEW') then
    select case (dtype)
    case ('UV_DATA')
      continue
    case ('BEAM','FIELDS','PRIMARY')
      !
      ! VIEW BEAM|FIELDS|PRIMARY [Field F|Plane P] [First Last]
      ! @ p_view_beam BEAM FIELD 16
      !   or
      ! @ p_view_beam BEAM PLANE 1 
      !
      if (hbeam%loca%size.eq.0) then
        call map_message(seve%e,comm,'No BEAM buffer')
        error = .true.
        return
      endif
      needed = .false.
      if ((hbeam%gil%ndim.eq.4).and.(dtype.eq.'BEAM')) then
        needed = hbeam%gil%dim(4)*hbeam%gil%dim(3) .gt.1
      endif
      if (needed.and.sic_narg(0).lt.3) then
        call map_message(seve%e,comm,'Missing PLANE or FIELD key and number')
        error = .true.
        return
      endif
      argu1 = 'PLANE'
      call sic_ke(line,o_args,2,argu1,nc,needed,error)
      argu2 = '1'
      call sic_ch(line,o_args,3,argu2,nc,needed,error)
      if (error) return
      !      
      do_loop = view_do_loop
      if (sic_present(o_nopause,0)) view_do_loop = .false.
      call exec_program('@ p_view_beam '//dtype//trim(argu1)//' '//trim(argu2))
      view_do_loop = do_loop
    case ('CCT') 
      if (hcct%loca%size.eq.0) then
        call map_message(seve%e,comm,'No CCT buffer')
        error = .true.
        return
      endif
      chain = dtype//trim(argu1)//' '//trim(argu2)
      call cct_integrate(comm,hcct,dcct,error)
      call exec_program('@ p_view_cct '//chain)
    case default
      do_loop = view_do_loop
      if (sic_present(o_nopause,0)) view_do_loop = .false.
      chain = dtype//trim(argu1)//' '//trim(argu2)
      call exec_program('@ p_view_map '//chain)
      view_do_loop = do_loop
    end select
  else if (comm.eq.'INSPECT_3D') then
    if (dtype.eq.'UV_DATA') then
      continue
    else if (dtype.eq.'CCT') then
      continue
    else
      chain = dtype//trim(argu1)//' '//trim(argu2)
      call exec_program('@ p_3view_map '//chain)
    endif
  endif
  !
end subroutine display_buffer_sub
!
subroutine create_fields(error)
  use gkernel_interfaces
  use clean_arrays
  use clean_types
  use imager_interfaces, only : define_fields
  !----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER internal routine for command
  !     Create the FIELDS array (transposed version of the PRIMARY array)
  !----------------------------------------------------------------------
  logical, intent(out) :: error
  ! Local variables
  !
  character(len=4) :: code
  integer(kind=index_length) :: nfirst,nsecon,nmiddl,nelems,nlast,iblock(5)
  integer :: ier
  logical :: found
  !
  if (hprim%loca%size.eq.0) then
    error = .true.
    return
  endif
  error = .false.
  !
  ! If it is allocated, it should be the right one, but play safe...
  if (allocated(dfields)) then
    found = .true.
    !!Print *,'dfiels is already allocated ',hfields%loca%size, hprim%loca%size
    if (hfields%loca%size.ne.hprim%loca%size) then
      !!Print *,'FIELDS is already allocated ',hfields%loca%size, hprim%loca%size
      !!Print *,'   but it does not have the right size'
      deallocate(dfields)
      !
      found = .false.
    endif
  else
    !!Print *,'Dfield not allocated '
    found = .false.
  endif
  !
  call gildas_null(hfields)
  !
  code = '231'
  call gdf_transpose_header(hprim,hfields,code,error)
  ! Determine chunk sizes from code and dimensions.
  call transpose_getblock(hprim%gil%dim,gdf_maxdims,code,iblock,error)
  if (error) return
  !
  ! With only one beam per frequency so far
  if (.not.allocated(dfields)) then
    allocate(dfields(hprim%gil%dim(2),hprim%gil%dim(3),hprim%gil%dim(1),hprim%gil%dim(4)),stat=ier)
    if (ier.ne.0) then
      error = .true.
      return
    endif
  endif
  !
  nelems = iblock(1)
  nfirst = iblock(2)
  nmiddl = iblock(3)
  nsecon = iblock(4)
  nlast  = iblock(5)
  call trans4all(dfields,dprim,nelems,nfirst,nmiddl,nsecon,nlast)
  !
  ! Set Type of First Axis
  hfields%gil%inc(3) = 1.0
  hfields%char%code(3) = 'FIELD'
  !
  ! If array was already there, the variables were there too
  if (found) return
  !
  ! That is a little too much. It also destroys other FIELDS% variables...
  ! FIELDS%N, FIELDS%CENTERS[2,Fields%N], and FIELDS%PRIMARY 
  call sic_delvariable('FIELDS',.false.,error)
  ! So re-create them
  call define_fields(themap,error)  
  call sic_mapgildas ('FIELDS',hfields,error,dfields)
  !
end subroutine create_fields
!
subroutine cct_integrate(rname,hcct,dcct,error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER  Support for SHOW and VIEW CCT
  !   Compute the cumulative flux in Clean Components, and give access
  !   to it through the CCT_NCOMP and CCT_FLUX arrays.
  !---------------------------------------------------------------------  
  character(len=*), intent(in) :: rname   ! Command name
  type(gildas), intent(in) :: hcct        ! Header
  real, intent(in) :: dcct(:,:,:)         ! CCT array
  logical, intent(inout) :: error         ! Error flag
  !
  real, allocatable, save :: fcct(:,:)
  integer, allocatable, save :: mcomp(:)
  integer :: ic,jc,ncomp,nchan,ier
  integer(kind=index_length) :: dim(2)
  !
  call sic_delvariable('CCT_NCOMP',.false.,error)
  call sic_delvariable('CCT_FLUX',.false.,error)    
  if (allocated(fcct)) then
    deallocate(fcct,mcomp)
  endif
  !
  ! The FCCT array has a dummy Clean component number 0
  ! to start from Null flux value
  ncomp = hcct%gil%dim(3)+1
  nchan = hcct%gil%dim(2)
  allocate(fcct(ncomp,nchan),mcomp(nchan),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  fcct = 0.0  ! Initialize
  do ic=1,nchan
    fcct(1,ic) = 0 
    mcomp(ic) = ncomp
    do jc=1,ncomp
      if (dcct(3,ic,jc).eq.0) then
        mcomp(ic) = jc
        exit
      else
        fcct(jc+1,ic) = dcct(3,ic,jc) + fcct(jc,ic)
      endif
    enddo
  enddo
  !
  dim(1) = ncomp
  dim(2) = nchan
  !
  call sic_def_real('CCT_FLUX',fcct,2,dim,.true.,error)
  dim(1) = nchan
  call sic_def_inte('CCT_NCOMP',mcomp,1,dim,.true.,error)
end subroutine cct_integrate
! 
!
subroutine display_set_comm(line,comm,error)
  use gkernel_interfaces
  use clean_default
  use gbl_message
  use imager_interfaces, only : map_message
  !----------------------------------------------------------------------
  ! @ private
  ! 
  ! IMAGER / VIEWER
  !      Support for the SET ANGLE_UNIT command
  ! Fall back to GREG1\SET command for any other argument
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  character(len=*), intent(in) :: comm
  logical :: error
  !
  character(len=32) :: key, avoc
  integer :: ia,n,ounit, narg
  logical :: default
  character(len=12) :: argu, caunit(0:5)
  real(kind=8) :: factor
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: bunit(4)=[180d0/pi,180.d0/pi/60d0,180d0/pi/3600d0,1.d0]
  data caunit /'ABSOLUTE','DEGREE','MINUTE','SECOND','RADIAN','RELATIVE'/
  data avoc /'ANGLE_UNIT '/
  !
  default = sic_present(1,0)
  narg = sic_narg(0)
  if (default.and.(narg.eq.0)) then
    ! SET /DEFAULT
    !   Reset GreG and Imager defaults
    ounit = abs(iangle_unit)
    iangle_unit = -3 
    factor = bunit(ounit)/bunit(abs(iangle_unit))
    area_size = area_size*factor
    area_center = area_center*factor
    !
    call gr_exec1('SET /DEFAULT')
    return
  endif
  !
  call sic_ke(line,0,1,key,n,.true.,error)
  if (error) return
  !
  if (key(1:n).eq.avoc(1:n)) then
    !
    ounit = abs(iangle_unit)
    !
    if (default.and.(narg.eq.1)) then
      iangle_unit = -3
    else 
      key = '?'
      call sic_ke(line,0,2,key,n,.false.,error)
      if (error) return
      !
      if (key.eq.'?')  then
        if (iangle_unit.lt.0) then
          call map_message(seve%i,'SET', &
          &   'Current display angle unit is ABSOLUTE if possible, ' &
          &   //caunit(-iangle_unit)//' otherwise')
        else
          call map_message(seve%i,'SET', &
          &   'Current display angle unit is '//caunit(iangle_unit))
        endif
        return
      else 
        if (default) call map_message(seve%w,'SET', &
          & '/DEFAULT option ignored with argument')
        !
        call sic_ambigs ('SET ANGLE_UNIT',key,argu,n,caunit,6,error)
        if (error) return
        n = n-1
        if (n.eq.0) then
          iangle_unit = -abs(iangle_unit)
        else if (n.eq.5) then
          iangle_unit =  abs(iangle_unit)
        else
          iangle_unit = n
        endif
      endif
    endif
    !
    ! Reset unit and convert SIZE and CENTER
    factor = bunit(ounit)/bunit(abs(iangle_unit))
    area_size = area_size*factor
    area_center = area_center*factor
    !
    ! Reset all conversion formulas and displayed Units
    call exec_program('@ d_box')
  else
    ! GREG\SET fall back
    ia = index(line,' ') 
    call gr_exec1('SET'//line(ia:))
  endif
end subroutine display_set_comm
 
  
