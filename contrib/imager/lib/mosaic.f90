subroutine sub_mosaic(name,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>sub_mosaic
  use clean_def
  use clean_default
  use clean_arrays
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER      MOSAIC ON|OFF
  !             Activates or desactivates the mosaic mode
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: name
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  !
  integer nf
  logical mosaic, old_mosaic
  real prim_beam
  character(len=message_length) :: mess
  character(len=6) :: rname = 'MOSAIC'
  !
  old_mosaic = user_method%mosaic
  mosaic = name.eq.'ON'
  !
  if (mosaic) then
    if (old_mosaic) then
      call map_message(seve%i,rname,'Already in MOSAIC mode')
    else
      call map_message(seve%i,rname,'Switch to MOSAIC mode')
      call gprompt_set('MOSAIC')
    endif
    if (user_method%trunca.ne.0.) then
      nf = hprim%gil%dim(1)
      ! This has been replaced by the Telescope Section 
      prim_beam = 0.0 !! hprim%gil%convert(3,4)   ! old convention
      call get_bsize(hprim,rname,name,prim_beam,error) !! ,otrunc,btrunc)
      write(mess,100) 'Last mosaic loaded: ', nf,' fields'
      call map_message(seve%i,rname,mess)
      write(mess,101) 'Primary beam (arcsec) = ',prim_beam*180*3600/pi
      call map_message(seve%i,rname,mess)
      write(mess,101) 'Beam Truncation level MAP_TRUNCATE = ',user_method%trunca
      call map_message(seve%i,rname,mess)
    else
      call map_message(seve%w,rname,'No mosaic loaded so far')
    endif
    write(mess,101) 'Searching Clean Component down to CLEAN_SEARCH = ',user_method%search
    call map_message(seve%i,rname,mess)
    write(mess,101) 'Restoring Sky brightness down to CLEAN_TRUNCATE = ',user_method%restor
    call map_message(seve%i,rname,mess)
    user_method%mosaic = .true.
  else
    if (.not.old_mosaic) then
      call map_message(seve%i,rname,'Already in NORMAL mode')
    else
      call map_message(seve%i,rname,'Switch to NORMAL mode')
      call gprompt_set('IMAGER')
      user_method%trunca = 0.0
      call sic_delvariable('PRIMARY',.false.,error)
      hprim%gil%dim(1) = 1
    endif
    user_method%mosaic = .false.
  endif
  !
100 format(a,i3,a)
101 format(a,f5.2)
end subroutine sub_mosaic
!
subroutine mosaic_uvmap(task,line,error)
  !$ use omp_lib
  use gkernel_interfaces
  use imager_interfaces, except_this=>mosaic_uvmap
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use clean_beams 
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !     Support for command UV_MAP
  !   Compute a Mosaic from a UV Table with pointing offset information.
  !
  ! Input :
  !     a UV table with pointing offset information
  !
  ! Ouput
  !   NX NY are the image sizes
  !   NC is the number of channels
  !   NF is the number of different frequencies
  !   NP is the number of pointing centers
  !   NB is the number of frequency-dependent beam cubes
  !
  ! In the original task, we had obtained:
  ! 'NAME'.LMV  a 3-d cube containing the uniform noise
  !     combined mosaic, i.e. the sum of the product
  !     of the fields by the primary beam. (NX,NY,NC)
  ! 'NAME'.LOBE the primary beams pseudo-cube (NP,NX,NY,NB)
  ! 'NAME'.WEIGHT the sum of the square of the primary beams (NX,NY,NB)
  ! 'NAME'.BEAM a 4-d cube where each cube contains the synthesised
  !     beam for one field (NX,NY,NB,NP)
  !
  ! Now, we obtain
  !   HDirty      a 3-d cube containing the uniform noise
  !              combined mosaic, i.e. the sum of the product
  !               of the fields by the primary beam. (NX,NY,NC)
  !   HBeam       a 4-d cube where each cube contains the synthesised
  !               beam for one field (NX,NY,NB,NP)
  !   HPrim       the primary beams pseudo-cube (NP,NX,NY,NB)
  !
  !   lweight     the sum of the square of the primary beams (NX,NY,NB)
  !               only for NB=1 (local Weight array, actually unused)
  !
  ! All images have the same X,Y sizes
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: task ! Caller (MOSAIC)
  character(len=*), intent(in) :: line ! Command line
  logical, intent(out) :: error
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  character(len=*), parameter :: rname='UV_MOSAIC'
  !
  real, allocatable :: w_mapu(:), w_mapv(:), w_grid(:,:)
  real(8) newabs(3)
  real(4) rmega,uvmax,uvmin,uvma
  integer wcol,mcol(2),nfft,sblock
  integer ier
  logical one, sorted, shift
  character(len=message_length) :: chain
  real cpu0, cpu1
  real(8) :: freq
  real, allocatable :: fft(:)
  real, allocatable :: noises(:)
  integer nx,ny,nu,nv,nc,np,mp
  !
  logical :: debug, abort
  type(gildas) :: htmp, pdirty, pbeam
  real :: thre, btrunc, bsize ! To be initialized
  real :: beamx, beamy
  !
  real, allocatable, target :: dmap(:,:,:), dtmp(:,:,:,:)
  real, allocatable, target :: lweight(:,:,:)   !! Unused
  real, allocatable :: dtrunc(:,:)
  real, allocatable :: doff(:,:)
  real, pointer :: my_dirty(:,:,:)
  !
  integer, allocatable :: voff(:)
  real, allocatable :: factorx(:)
  real :: offx, offy, factory, xm, xp, off_range(2)
  integer :: ifield, jfield, ic, j, fstart, fend
  integer :: ib, nb, old_ib
  integer, parameter :: o_trunc=1
  integer, parameter :: o_field=2
  type(projection_t) :: proj
  real(8) :: pos(2)
  !
  logical :: per_field
  integer(kind=8) :: ram_map, ram_uv, ram_beam
  integer :: nthread, othread, ithread
  logical :: omp_nested
  logical :: do_jvm
  real :: jvm_factor
  !
  integer :: ndim, nn(2)
  integer(kind=index_length) :: dim(4)
  complex, allocatable :: comp(:,:)
  !
  call imager_tree('MOSAIC_UVMAP')
  !
  wcol = 0
  debug = .false.
  abort = .false.
  call sic_get_logi('DEBUG',debug,error)
  !
  do_jvm = task.eq.'MOSAIC_RESTORE' .and. beam_defined
  !
  ! Get beam size from data or command line
  call sic_get_real('MAP_TRUNCATE',btrunc,error)
  if (error) return
  write(chain,'(A,F4.2,1X,F4.2)') 'Truncation level ',default_map%truncate, btrunc
  call map_message(seve%i,task,chain,3)
  bsize = 0  ! Must be initialized
  if (sic_present(o_trunc,0)) then
    call get_bsize(huv,rname,line,bsize,error,OTRUNC=o_trunc,BTRUNC=btrunc)
  else
    call get_bsize(huv,rname,line,bsize,error,BTRUNC=btrunc)
  endif
  if (error) return
  write(chain,'(a,f10.2,a,f6.0,a)') 'Correcting for a beam size of ',&
    & bsize/pi*180*3600,'" down to ',100*btrunc,'% '
  call map_message(seve%i,rname,chain)
  !
  call map_prepare(task,huv,themap,error)
  if (error) return
  !
  one = .true.  
  call uvmap_cols(rname,line,huv,mcol,wcol,error)
  if (error) return 
  !
  ! Select Fields first
  call sic_delvariable('FIELDS%N_SELECT',.false.,error)
  call sic_delvariable('FIELDS%SELECTED',.false.,error)
  error = .false.
  !
  ! Get the field lists from the /FIELDS option if any
  mp = abs(themap%nfields) ! Number of pointings
  if (allocated(selected_fields)) deallocate(selected_fields)
  if (sic_present(o_field,0)) then
    call select_fields(rname,line,o_field,mp,np,selected_fields,error)
    if (error) return
    call sic_def_inte('FIELDS%N_SELECT',selected_fieldsize,0,0,.true.,error)
    dim(1) = selected_fieldsize
    call sic_def_inte('FIELDS%SELECTED',selected_fields,1,dim,.true.,error) 
  else
    np = mp
    allocate(selected_fields(mp),stat=ier)
    if (ier.ne.0) then
      Print *,'MP ',mp, ier
      call map_message(seve%e,rname,'Memory allocation on Selected Fields')
    endif
    do jfield=1,np
      selected_fields(jfield) = jfield
    enddo
    selected_fieldsize = 0    ! Means all
  endif
  !
  call gag_cpu(cpu0)
  !
  ! Shifting to Phase center Offsets has been done by calling UV_SHIFT_MOSAIC before
  newabs = [huv%gil%a0,huv%gil%d0,huv%gil%pang]
  shift = .false. 
  !
  ! Note: the sorting should have FIELD ID as primary (slowest varying) key
  !
  if (allocated(doff)) deallocate(doff)
  if (allocated(voff)) deallocate(voff)
  allocate(doff(2,mp),voff(mp+1),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  call mosaic_sort (error,sorted,shift,newabs,uvmax,uvmin, &
    & huv%gil%column_pointer(code_uvt_xoff), &
    & huv%gil%column_pointer(code_uvt_yoff), &
    & mp,doff,voff)
  if (sic_ctrlc()) then
    call map_message(seve%e,rname,'Aborted by user')
    error = .true.
  endif
  if (error) return
  !
  xm = minval(doff(1,1:mp))
  xp = maxval(doff(1,1:mp))
  off_range(1) = xp-xm
  xm = minval(doff(2,1:mp))
  xp = maxval(doff(2,1:mp))
  off_range(2) = xp-xm
  !
  if (.not.sorted) then
    !! Print *,'Done mosaic_sort UV range ',uvmin,uvmax,' sorted ',sorted
    !! call uv_dump_buffers('UV_MOSAIC')
    ! Redefine SIC variables (mandatory)
    ! Caution: this overrides command line pointers
    call map_uvgildas ('UV',huv,error,duv) 
  else
    !! Print *,'Mosaic was sorted ',uvmin,uvmax,' sorted ',sorted
  endif
  !
  call gag_cpu(cpu1)
  write(chain,102) 'Finished sorting ',cpu1-cpu0
  call map_message(seve%i,task,chain)
  !
  ! Get map parameters
  call map_parameters(task,themap,huv,freq,uvmax,uvmin,error)
  if (error) return
  uvma = uvmax/(freq*f_to_k)
  !
  themap%xycell = themap%xycell*pi/180.0/3600.0
  !
  ! Get work space, ideally before mapping first image, for
  ! memory contiguity reasons.
  !
  nx = themap%size(1)
  ny = themap%size(2)
  nu = huv%gil%dim(1)  
  nv = huv%gil%nvisi     ! not huv%gil%dim(2)
  nc = mcol(2)-mcol(1)+1 ! not huv%gil%nchan
  !
  allocate(w_mapu(nx),w_mapv(ny),w_grid(nx,ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,task,'Gridding allocation error')
    goto 98
  endif
  !
  do_weig = .true.
  if (do_weig) then
    call map_message(seve%i,task,'Computing weights ')
    if (allocated(g_weight)) deallocate(g_weight)
    if (allocated(g_v)) deallocate(g_v)
    allocate(g_weight(nv),g_v(nv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'Weight allocation error')
      goto 98
    endif
  else
    call map_message(seve%i,task,'Re-using weights')
  endif
  nfft = 2*max(nx,ny)
  allocate(fft(nfft),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,task,'FFT allocation error')
    goto 98
  endif
  !
  rmega = 8.0
  ier = sic_ramlog('SPACE_MAPPING',rmega)
  sblock = max(int(256.0*rmega*1024.0)/(nx*ny),1)
  !
  ! New Beam place
  if (allocated(dbeam)) then
    call sic_delvariable ('BEAM',.false.,error)
    deallocate(dbeam)
  endif
  call gildas_null(hbeam)
  !
  ! New dirty image
  allocate(dmap(nx,ny,nc),dtrunc(nx,ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,task,'Map allocation error')
    goto 98
  endif
  !
  if (task.ne.'MOSAIC_RESTORE') then 
    if (allocated(ddirty)) then
      call sic_delvariable ('DIRTY',.false.,error)
      deallocate(ddirty)
    endif
    allocate(ddirty(nx,ny,nc),stat=ier)
    my_dirty => ddirty
  else
    if (allocated(dresid)) then
      call sic_delvariable ('RESIDUAL',.false.,error)
      deallocate(dresid)
    endif
    allocate(dresid(nx,ny,nc),stat=ier)
    my_dirty => dresid
  endif
  if (ier.ne.0) then
    call map_message(seve%e,task,'Map allocation error')
    goto 98
  endif
  !
  call gildas_null(hdirty)
  hdirty%gil%ndim = 3
  hdirty%gil%dim(1:3) = (/nx,ny,nc/)
  !
  ! Compute the primary beams and weight image
  call gildas_null(hprim)
  if (allocated(dprim)) then
    call sic_delvariable ('PRIMARY',.false.,error)
    deallocate(dprim)
  endif
  if (allocated(dfields)) then
    deallocate(dfields)
  endif
  !
  !!Print *,'Done MAP_BEAMS ',themap%beam,nb
  !
  ! Find out how many beams are required
  call define_beams(rname,themap%beam,nx,ny,huv,mcol,nb,error)
  if (error) return
  ! Define the map characteristics
  call mosaic_headers (rname,themap,huv,hbeam,hdirty,hprim,nb,np,mcol)
  !
  write(chain,'(A,I0,A,I0,A)') 'Imaging channel range [',mcol(1),',',mcol(2),']'
  call map_message(seve%i,task,chain)
  !
  ! Check if Weights have changed by MCOL choice
  if (any(saved_mcol.ne.mcol)) do_weig = .true. ! Useless ???? NEW
  saved_mcol = mcol
  !
  ! Define the projection about the Phase center
  call gwcs_projec(huv%gil%a0,huv%gil%d0,huv%gil%pang,huv%gil%ptyp,proj,error)
  !
  ! POS is here the Offset of the Pointing center relative to the Phase center
  ! (This may be Zero in most cases) 
  call abs_to_rel (proj,huv%gil%ra,huv%gil%dec,pos(1),pos(2),1)
  !
  !
  if (map_version.lt.0) then
    ! Remove the Lweight array if allocated
    if (allocated(lweight)) deallocate(lweight)
    !
    ! Older MOSAIC code
    call map_message(seve%w,task,'Using an obsolete, deprecated code',1)
    call map_message(seve%i,task,'Producing a single beam for all channels')
    !
    hbeam%gil%ndim = 3
    hbeam%gil%dim(1:3)=(/nx,ny,np/)
    hbeam%char%code(3) = 'FIELD'
    hbeam%gil%convert(:,3) = 1.d0
    allocate(dbeam(nx,ny,np,1),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'Beam allocation error')
      goto 98
    endif
    call sic_mapgildas('BEAM',hbeam,error,dbeam)
    !
    hbeam%r4d => dbeam
    !
    allocate (lweight(nx,ny,1), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'Weight allocation error')
      goto 98
    endif
    !
    allocate (factorx(nx), dprim(np,nx,ny,1), dtmp(nx,ny,1,1), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'Primary beam allocation error')
      goto 98
    endif
    hprim%r4d => dprim
    beamx = hprim%gil%inc(2)/bsize*2.0*sqrt(log(2.0))
    beamy = hprim%gil%inc(3)/bsize*2.0*sqrt(log(2.0))
    !
    my_dirty = 0.0    !! ddirty = 0.0
    !
    ! Create the primary beams, lobe and weight images
    do jfield = 1,np
      if (sic_ctrlc()) then
        error = .true.
        call map_message(seve%e,rname,'Aborted by user')
        return
      endif
      !
      ifield = jfield
      if (selected_fieldsize.ne.0) ifield = selected_fields(jfield)    
      !
      ! POS is here the Offset of the Pointing center relative to the Phase center
      ! DOFF is the Offset of the Field relative to the Pointing center
      ! So OFFX/Y is the offset of the Field relative to the Phase center in pixel units
      offx = (doff(1,ifield)+pos(1))/hprim%gil%inc(2)
      offy = (doff(2,ifield)+pos(2))/hprim%gil%inc(3)
      !
      do j=1,nx
        factorx(j) = exp(-((j-hprim%gil%ref(2)-offx)*beamx)**2)
      enddo
      do j=1,ny
        factory = exp(-((j-hprim%gil%ref(3)-offy)*beamy)**2)
        dprim(jfield,:,j,1) = factorx(:) * factory
      enddo
    enddo
    deallocate (factorx)
    !
    ! Loop on fields for imaging
    ! Use Dtmp and Dmap as work arrays for beam and image
    hbeam%r3d => dtmp(:,:,:,1)
    hbeam%gil%dim(3) = 1
    hdirty%r3d => dmap
    call map_message(seve%i,task,'Producing a single beam for all channels')
    !
    do jfield = np,1,-1
      ifield = jfield
      if (selected_fieldsize.ne.0) ifield = selected_fields(jfield)    
      !
      if (sic_ctrlc()) then
        abort = .true.
      endif
      if (abort) exit ! Only valid in sequential mode
      !
      do_weig = .true.
      fstart = voff(ifield)      ! Starting Visibility of field
      fend   = voff(ifield+1)-1  ! Ending Visibility of field
      nv = fend-fstart+1
      !
      ! Process sorted UV Table according to the type of beam produced
      call one_beam (task,themap,   &
         &    huv, hbeam, hdirty,   &
         &    nx,ny,nu,nv, duv(:,fstart:fend),   &
         &    w_mapu, w_mapv, w_grid, g_weight, g_v, do_weig,  &
         &    wcol,mcol,fft,   &
         &    sblock,cpu0,error,uvma)
      !
      ! Add it to the "mosaic dirty" image, by multiplying by
      ! the truncated primary beam
      dtrunc(:,:) = dprim(jfield,:,:,1)
      where(dtrunc.lt.btrunc) dtrunc = 0
      !
      do ic=1,nc
        my_dirty(:,:,ic) = my_dirty(:,:,ic) + dmap(:,:,ic)*dtrunc(:,:)
      enddo
      !
      ! Save the beam
      dbeam(:,:,jfield,1) = dtmp(:,:,1,1)
    enddo
    if (abort) then
      call map_message(seve%e,rname,'Aborted by user')
      error = .true.
      return
    endif    
    !
    ! Reset the good pointers and sizes
    hdirty%r3d => my_dirty
    hbeam%r4d => dbeam
    hbeam%gil%ndim = 3
    hbeam%gil%dim(3) = np
    !
    ! Create the Weight image
    !!Print *,'BMIN ',btrunc
    lweight = 0
    call mos_addsq (nx*ny,np,lweight(:,:,1),dprim)
    thre = btrunc**2
    call mos_inverse (nx*ny,lweight(:,:,1),thre)
    !!Print *,'Done MOS_INVERSE'
    !
  else
    !!Print *,'Using untested code, use BEAM_STEP = -2 if failed'
    !
    ! Code ready and now tested for several channels per Beam
    hbeam%gil%ndim = 4
    hbeam%gil%dim(1:4)=(/nx,ny,nb,np/)
    !
    !
    allocate (dtmp(nx,ny,nb,1), dbeam(nx,ny,np,nb), dprim(np,nx,ny,nb), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,task,'NB>1 Primary beam allocation error')
      goto 98
    endif
    !
    hprim%r4d => dprim
    call primary_mosaic(line,np,hprim,hdirty,selected_fields,selected_fieldsize,doff,pos,bsize,error)
    if (error) return
    call map_message(seve%i,rname,'Done Primary Beams')
    !
    ! Loop on fields for imaging
    ! Use Dtmp and Dmap as work arrays for beam and image
    hbeam%r3d => dtmp(:,:,:,1)
    hbeam%gil%dim(4) = 1
    !
    ! IN Parallel part, DMAP will be copied per Thread
    ! So we have to verify that the Parallel mode actually fits into Memory
    !
    othread = 1  
    !$ othread = omp_get_max_threads()
    !
    ! Verify memory requirements
    ram_uv = huv%gil%dim(1)*huv%gil%dim(2)/512/512
    ram_map = 2*hdirty%gil%dim(1)*hdirty%gil%dim(2)*hdirty%gil%dim(3)/512/512
    ram_beam = 2*hbeam%gil%dim(1)*hbeam%gil%dim(2)*hbeam%gil%dim(3)*hbeam%gil%dim(4)/512/512
!    Print *,'   RAM Map ',ram_map,hdirty%gil%dim(1:4)    Print *,'   RAM Beam ',ram_beam,hbeam%gil%dim(1:4)
    ram_map = ram_map+ram_beam
    !
    nthread = min(othread,np)
    if (ram_map*nthread.gt.sys_ramsize) nthread = nint(real(sys_ramsize)/real(ram_map))
    nthread = max(nthread,1) ! Just in case
    !
!!    if (np.le.othread/2) nthread = 1
    !
    ram_map = ram_map * min(nthread, np)
    if (ram_map.gt.0.2*sys_ramsize) then
      write(chain,'(A,F8.1,A,F8.1,A,F8.1,A)') 'Data size (UV ',1d-3*ram_uv,& 
        & 'GB + Map ',1D-3*ram_map, &
        & 'GB), available RAM (',1d-3*sys_ramsize,' GB)'
      call map_message(seve%w,rname,chain,3)
      if (ram_map.gt.sys_ramsize) then
        call map_message(seve%e,'Data size exceeds RAM',chain,3)
        error = .true.
        return
      endif
    endif
    !
    !$ call ompset_thread_nesting(rname, nthread, othread, omp_nested)    
    !$ nthread = ompget_outer_threads()
    if (nthread.gt.1) then
      per_field = .true.    
      write(chain,'(A,I0,A,I0,A)') 'Using per-field parallel mode. - Threads ',nthread,' Fields ',np
      call map_message(seve%i,rname,chain,3)
    endif
    !
    hdirty%r3d => dmap
    !
    my_dirty = 0
    dbeam = 0
    allocate(noises(np))  ! To remember the noise
    abort = .false.
    if (sic_ctrlc()) then
      error = .true.
      call map_message(seve%e,rname,'Aborted by user')
      return
    endif
    !
    if (per_field) then
      write(chain,'(A,I0,A,I0,A)') 'Starting per-field parallel loop on channel range [', &
        & mcol(1),'-',mcol(2),']'
      call map_message(seve%i,rname,chain)
    else
      call map_message(seve%i,rname,'Using per-Plane parallelism')
    endif
    !
    ! FFTW plan
    ndim = 2
    nn(1) = hdirty%gil%dim(1)
    nn(2) = hdirty%gil%dim(2)
    allocate(comp(nx,ny),stat=ier)
    call fourt_plan(comp,nn,ndim,-1,1)
    !
    !
    !$OMP PARALLEL DEFAULT(none) if (per_field) NUM_THREADS (nthread) &
    !$OMP   & SHARED(np,debug,task,themap,huv,dprim,dbeam,my_dirty) & 
    !$OMP   & PRIVATE(pdirty,pbeam) &
    !$OMP   & SHARED(hdirty,hbeam) &
    !$OMP   & SHARED(voff, selected_fields, noises, nthread) &
    !$OMP   & SHARED(nx,ny,nu,nc,duv,wcol,mcol,sblock,cpu0,uvma,btrunc,abort) &
    !$OMP   & SHARED(g_weight,g_v) PRIVATE(ifield,jfield) &
    !$OMP   & PRIVATE(fstart,fend,nv,do_weig,error,chain) &
    !$OMP   & PRIVATE(old_ib,ic,ib,dtrunc,dmap,dtmp, ithread) &
    !$OMP   & SHARED(beams_param,do_jvm) PRIVATE(jvm_factor)
    ithread = 1
    !$ ithread = omp_get_thread_num()+1
    call gildas_null(pdirty)
    call gildas_null(pbeam)
    call gdf_copy_header(hdirty,pdirty,error)
    call gdf_copy_header(hbeam,pbeam,error)
    !$OMP DO
    do jfield = 1,np
      if (sic_ctrlc()) then
        abort = .true.
      endif
      if (abort) cycle ! Quick jump if Abort (EXIT not allowed in Parallel mode)
      !
      ifield = selected_fields(jfield)
      ! Pour le mode parallele
      pdirty%r3d => dmap
      pbeam%r3d => dtmp(:,:,:,1)
      !
      do_weig = .true.
      fstart = voff(ifield)      ! Starting Visibility of field
      fend   = voff(ifield+1)-1  ! Ending Visibility of field
      nv = fend-fstart+1
      if (debug) then
        Print *,'Ifield ',ifield,fstart,fend
        Print *,'Cols ',wcol,mcol
        Print *,'Sizes ',nx,ny,nu,nv,np,nc
        Print *,'Calling many_beams_para with SBLOCK ',sblock
      endif
      ! We could write the Thread or Field number in "task" argument...
      call many_beams_para (task,themap, huv, pbeam, pdirty,   &
         &    nx,ny,nu,nv,duv(:,fstart:fend),   &
         &    g_weight(fstart:fend), g_v(fstart:fend), do_weig,  &
         &    wcol,mcol,sblock,cpu0,error,uvma,ifield,abort,ithread)
      !
      noises(jfield) = pdirty%gil%noise   ! Remember the noise
      if (abort) cycle                    ! Cannot Return
      !
      old_ib = 0
      !
      do ic=1,nc
        ib = beam_for_channel(ic,pdirty,pbeam)
        if (do_jvm) then
          jvm_factor = beams_param(4,ib,jfield)
          if (jvm_factor.eq.0.) jvm_factor = 1.
          write(chain,'(A,I0,A,I0,A,F7.3,A,I0)')  &
            & 'Field ',jfield,', Beam ',ib,', JvM factor ',jvm_factor,'; Thread ',ithread
          call map_message(seve%i,task,chain)
        else
          jvm_factor = 1.
        endif
        if (debug) Print *,'Selected beam ',ib, jvm_factor
        ! Add it to the "mosaic dirty" image, by multiplying by
        ! the truncated primary beam
        if (ib.ne.old_ib) then
          dtrunc(:,:) = dprim(jfield,:,:,ib)
          if (debug) Print *,'Set DTRUNC ',ib,' # ',old_ib
          where (dtrunc.lt.btrunc) dtrunc = 0
          old_ib = ib
        endif
        !$OMP CRITICAL
        ! We use here the JvM factor for the Residual image 
        my_dirty(:,:,ic) = my_dirty(:,:,ic) + dmap(:,:,ic)*dtrunc(:,:)*jvm_factor
        !$OMP END CRITICAL
      enddo
      !
      ! Save the beam - Transposition could be done here if needed
      !! dbeam(:,:,:,jfield) = dtmp(:,:,:,1)
      dbeam(:,:,jfield,:) = dtmp(:,:,:,1) ! Transpose      
      if (.not.do_jvm) then
        write(chain,'(A,I0,A,I0)') 'Ending Field ',ifield,' Thread ',ithread
        call map_message(seve%i,task,chain)
      endif
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
    if (per_field) then
      !$  call omp_set_nested(omp_nested)
      !$  call omp_set_num_threads(othread)
    endif
    if (abort) then
      call map_message(seve%w,task,'Aborted by user')
      error = .true.
      return
    endif
    hdirty%gil%noise = sum(noises)/np
    !
    ! Set the BEAM header 
    call gildas_null(htmp)
    call gdf_copy_header(hbeam,htmp,error)
    htmp%gil%ndim = 4
    call gdf_transpose_header(htmp,hbeam,'1243',error)
    if (error) return
    hbeam%r4d => dbeam  
    hbeam%gil%dim(1:4)=(/nx,ny,np,nb/)
    hbeam%gil%ndim = 4
    !! if (nb.eq.1) hbeam%gil%ndim = 3
    call sic_mapgildas('BEAM',hbeam,error,dbeam)
  endif
  !
  ! OK we are done (apart from details like Extrema)
  !
  hprim%gil%inc(1) = btrunc  ! Convention to store the truncation level
  call sic_mapgildas('PRIMARY',hprim,error,dprim)
  !
  ! Reset the Dirty pointer
  hdirty%r3d => my_dirty
  hdirty%loca%addr = locwrd(my_dirty)
  !
  ! Nullify Filtered channels and Compute Dirty extrema 
  call cube_flag_extrema(huv%gil%nchan,'DIRTY',mcol,hdirty)
  !
  ! Correct the noise for the approximate gain at mosaic center
  ! for HWHM hexagonal spacing (normally it is sqrt(1+6/4)) 
  hdirty%gil%noise = hdirty%gil%noise/sqrt(2.5)
  if (task.ne.'MOSAIC_RESTORE') then 
    call sic_mapgildas('DIRTY',hdirty,error,ddirty)
    !
    save_data(code_save_beam) = .true.
    save_data(code_save_dirty) = .true.
    save_data(code_save_primary) = .true.
    save_data(code_save_fields) = .true.
    !
    call new_dirty_beam
    !
    ! Define Min Max
    call cube_minmax('DIRTY',hdirty,error)
    !
    d_max = hdirty%gil%rmax
    if (hdirty%gil%rmin.eq.0) then
      d_min = -0.03*hdirty%gil%rmax
    else
      d_min = hdirty%gil%rmin
    endif
  else
    ! Restore the DIRTY image pointer
    hdirty%r3d => ddirty
    call cube_minmax('DIRTY',hdirty,error)
    ! And define the RESIDUAL
    call gdf_copy_header(hdirty,hresid,error)
    hresid%r3d => dresid
    call cube_minmax('RESIDUAL',hresid,error)
    call sic_mapgildas('RESIDUAL',hresid,error,dresid)
  endif
  !
  error = .false.
  !
  ! Backward compatibility with previous methods
  user_method%trunca = btrunc     ! By convention
  hprim%gil%convert(3,4) = bsize  ! Primary beam size convention
  call sub_mosaic('ON',error)
  !
  if (allocated(w_mapu)) deallocate(w_mapu)
  if (allocated(w_mapv)) deallocate(w_mapv)
  if (allocated(w_grid)) deallocate(w_grid)
  if (allocated(fft)) deallocate(fft)
  return
  !
98 call map_message(seve%e,task,'Memory allocation failure')
  error = .true.
  return
  !
102 format(a,f9.2)
end subroutine mosaic_uvmap
!
subroutine mosaic_headers (rname,map,huv,hbeam,hdirty,hprim,nb,nf,mcol)
  use gkernel_interfaces
  use imager_interfaces, except_this=>mosaic_headers
  use clean_def
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Define the image headers for a Mosaic
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname   ! Calling Task name
  type (uvmap_par), intent(in) :: map     ! Mapping parameters
  type (gildas), intent(inout) :: huv     ! UV data set
  type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
  type (gildas), intent(inout) :: hdirty  ! Dirty image data set
  type (gildas), intent(inout) :: hprim   ! Primary beam data set
  integer, intent(in) :: nb   ! Number of beams per field
  integer, intent(in) :: nf   ! Number of fields
  integer, intent(in) :: mcol(2)  ! First and last channel
  !
  ! Global variables:
  real(kind=8), parameter :: clight=299792458d-6 ! Frequency in  MHz
  !
  ! Local variables:
  type(gildas) :: htmp
  integer :: nx   ! X size
  integer :: ny   ! Y size
  integer :: nc   ! Number of channels
  real vref,voff,vinc
  integer :: schunk ! Number of channels per beam
  real(kind=4) :: loff,boff
  integer :: fcol
  logical :: error
  character(len=4) :: code
  !
  ! Code:
  nx = map%size(1)
  ny = map%size(2)
  !
  vref = huv%gil%ref(1)
  voff = huv%gil%voff
  vinc = huv%gil%vres
  !
  nc = mcol(2)-mcol(1)+1
  fcol = mcol(1)
  !
  ! Make beam, not normalized
  call gdf_copy_header(huv,hbeam,error)
  hbeam%gil%dopp = 0.0 ! Nullify the Doppler factor
  !
  ! Is that right ?
  schunk = nc/nb      ! No, of course
  ! This is the correct value
  schunk = (nc+nb-1)/nb
  !
  hbeam%gil%ndim = 4
  hbeam%gil%dim(1) = nx
  hbeam%gil%dim(2) = ny
  hbeam%gil%dim(3) = nb
  hbeam%gil%dim(4) = nf
  hbeam%gil%convert(1,1) = nx/2+1
  hbeam%gil%convert(1,2) = ny/2+1
  hbeam%gil%convert(2,1) = 0
  hbeam%gil%convert(2,2) = 0
  hbeam%gil%convert(3,1) = -map%xycell(1)  ! Assume EQUATORIAL system
  hbeam%gil%convert(3,2) = map%xycell(2)
  !
  ! Frequency axis 
  hbeam%gil%vres = hbeam%gil%vres*schunk
  hbeam%gil%fres = hbeam%gil%fres*schunk
  ! in Velocity
  hbeam%gil%convert(1,3) = (2.d0*(vref-fcol)+schunk+1.d0)/2/schunk ! Correct
  hbeam%gil%convert(2,3) = voff
  hbeam%gil%convert(3,3) = vinc*schunk    ! 
  hbeam%gil%faxi = 3
  ! We actually would like it in Frequency ...
  
  ! Field axis
  hbeam%gil%convert(:,4) = 1.d0
  hbeam%gil%blan_words = 0
  hbeam%gil%proj_words = 0
  hbeam%gil%extr_words = 0
  hbeam%gil%reso_words = 0
  hbeam%gil%uvda_words = 0
  hbeam%gil%type_gdf = code_gdf_image
  !
  hbeam%char%code(1) = 'ANGLE'
  hbeam%char%code(2) = 'ANGLE'
  hbeam%char%code(3) = 'VELOCITY'
  hbeam%char%code(4) = 'FIELD'
  hbeam%gil%majo = 0.0
  hbeam%loca%size = hbeam%gil%dim(1)*hbeam%gil%dim(2)*hbeam%gil%dim(3)*hbeam%gil%dim(4)
  !
  ! Prepare the dirty map header
  call gdf_copy_header(hbeam,hdirty,error)
  hdirty%gil%ndim = 3
  hdirty%gil%dim(1) = nx
  hdirty%gil%dim(2) = ny
  hdirty%gil%dim(3) = nc
  hdirty%gil%dim(4) = 1
  hdirty%gil%convert(1,3) = vref-fcol+1
  hdirty%gil%convert(2,3) = voff
  hdirty%gil%convert(3,3) = vinc
  !
  ! Frequency axis --- Caution resolution change compared to HBeam
  hdirty%gil%vres = hbeam%gil%vres/schunk
  hdirty%gil%fres = hbeam%gil%fres/schunk
  !
  hdirty%gil%blan_words = 0
  hdirty%gil%proj_words = def_proj_words
  hdirty%gil%uvda_words = 0
  hdirty%gil%type_gdf = code_gdf_image
  hdirty%char%code(1) = 'RA'
  hdirty%char%code(2) = 'DEC'
  hdirty%char%code(3) = 'VELOCITY'
  call equ_to_gal(hdirty%gil%ra,hdirty%gil%dec,0.0,0.0,   &
                  hdirty%gil%epoc,hdirty%gil%lii,hdirty%gil%bii,loff,boff,error)
  if (huv%gil%ptyp.eq.p_none) then
    hdirty%gil%ptyp = p_azimuthal  ! Azimuthal (Sin)
    hdirty%gil%pang = 0.d0     ! Defined in table.
    hdirty%gil%a0 = hdirty%gil%ra
    hdirty%gil%d0 = hdirty%gil%dec
  else
    hdirty%gil%ptyp = p_azimuthal
    hdirty%gil%pang = huv%gil%pang ! Defined in table.
    hdirty%gil%a0 = huv%gil%a0
    hdirty%gil%d0 = huv%gil%d0
  endif
  hdirty%char%syst = 'EQUATORIAL'
  hdirty%gil%xaxi = 1
  hdirty%gil%yaxi = 2
  hdirty%gil%faxi = 3
  hdirty%gil%extr_words = 0          ! extrema not computed
  hdirty%gil%reso_words = 0          ! no beam defined
  hdirty%gil%nois_words = 2
  hdirty%gil%majo = 0
  hdirty%char%unit = 'Jy/beam'
  hdirty%loca%size = hdirty%gil%dim(1)*hdirty%gil%dim(2)*hdirty%gil%dim(3)
  !
  call gildas_null(hprim)
  if (nf.ge.1) then
    call gildas_null(htmp)
    ! Prepare the primary beam cube header
    call gdf_copy_header(hdirty,htmp,error)
    htmp%gil%dim(4) = nf
    htmp%gil%convert(1:3,4) = 1.d0
    htmp%char%unit = ' '
    htmp%char%code(4) = 'FIELD'
    ! Also reset the Number of Beams in Frequency
    htmp%gil%dim(3) = nb
    code = '4123'
    call gdf_transpose_header(htmp,hprim,code,error)   
  endif
end subroutine mosaic_headers
!!
subroutine mosaic_sort (error,sorted,shift,newabs,uvmax,uvmin, &
  & ixoff,iyoff,nf,doff,voff)
  use gkernel_interfaces
  use imager_interfaces, except_this=>mosaic_sort
  use clean_def
  use clean_arrays
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER
  !   Sort the input Mosaic UV table
  !---------------------------------------------------------------------
  logical, intent(inout) :: sorted         ! Is table sorted ?
  logical, intent(inout) :: shift          ! Do we shift phase center ?
  logical, intent(out) :: error
  real(kind=8), intent(inout) :: newabs(3) ! New phase center and PA
  real, intent(out) :: uvmin               ! Min baseline
  real, intent(out) :: uvmax               ! Max baseline
  integer, intent(in) :: ixoff, iyoff      ! Offset pointers
  integer, intent(inout) :: nf             ! Number of fields
  real, intent(inout) :: doff(:,:)         ! Field offsets
  integer, intent(inout) :: voff(:)        ! Field visibility offsets
  !
  ! Global variables:
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  character(len=*), parameter :: rname='UV_MOSAIC'
  real(kind=8) :: freq, off(3)
  real :: pos(2), cs(2)
  integer :: nu,nv
  real, pointer :: duv_previous(:,:), duv_next(:,:)
  !
  call imager_tree('MOSAIC_SORT')
  if (newabs(3).ne.0) then
    Print *,'Non zero angle, NEW ABS ',newabs
  endif
  ! The UV table is available in HUV%
  if (huv%loca%size.eq.0) then
    call map_message(seve%e,rname,'No UV data loaded')
    error = .true.
    return
  endif
  nu = huv%gil%dim(1)
  nv = huv%gil%nvisi ! not %dim(2)
  !
  ! Correct for new phase center if required
  if (shift) then
    if (huv%gil%ptyp.eq.p_none) then
      call map_message(seve%w,rname,'No previous phase center info')
      huv%gil%a0 = huv%gil%ra
      huv%gil%d0 = huv%gil%dec
      huv%gil%pang = 0.d0
      huv%gil%ptyp = p_azimuthal
    elseif (huv%gil%ptyp.ne.p_azimuthal) then
      call map_message(seve%w,rname,'Previous projection type not SIN')
      huv%gil%ptyp = p_azimuthal
    endif
    call uv_shift_header (newabs,huv%gil%a0,huv%gil%d0,huv%gil%pang,   &
        &      off,shift)
    huv%gil%posi_words = def_posi_words
    huv%gil%proj_words = def_proj_words
  endif
  !
  sorted = .false.
  if (.not.shift) then
    call check_order_mosaic (duv,nu,nv,ixoff,iyoff,sorted)
  endif
  !
  ! Get center frequency
  freq = gdf_uv_frequency(huv,huv%gil%ref(1))
  !
  if (sorted) then
    !
    ! If already sorted, use it
    call map_message(seve%i,rname,'UV table is already sorted')
    !
    ! Load Field coordinates and compute UVMAX
    call mosaic_loadfield (duv,nu,nv,ixoff,iyoff,nf,doff,voff,uvmax,uvmin)
  else
    !
    ! Else, create another copy
    call map_message(seve%i,rname,'Sorting UV table...')
    !
    ! Compute observing frequency, and new phase center in wavelengths
    if (shift) then
      huv%gil%a0 = newabs(1)
      huv%gil%d0 = newabs(2)
      huv%gil%pang = newabs(3)
      cs(1)  =  cos(off(3))
      cs(2)  = -sin(off(3))
      ! Note that the new phase center is counter-rotated because rotations
      ! are applied before phase shift.
      pos(1) = - freq * f_to_k * ( off(1)*cs(1) - off(2)*cs(2) )
      pos(2) = - freq * f_to_k * ( off(2)*cs(1) + off(1)*cs(2) )
    else
      pos(1) = 0.0
      pos(2) = 0.0
      cs(1) = 1.0
      cs(2) = 0.0
    endif
    !
    ! OK, rotate, shift, sort and copy...
    !
    nullify (duv_previous, duv_next)
    !
    call uv_find_buffers (rname,nu,nv,duv_previous,duv_next,error)
    if (error) return
    !!call uv_dump_buffers ('UV_MOSAIC - After Find')
    !
    ! DUV may NOT be associated to DUV_PREVIOUS, it must be used directly
    call mosaic_sortuv (nu,nv,huv%gil%ntrail,duv,duv_next,   &
           &        pos,cs,uvmax,uvmin,error,ixoff,iyoff,nf,doff,voff)
    call uv_clean_buffers (duv_previous, duv_next, error)
    if (error) return
    !!call uv_dump_buffers ('UV_MOSAIC- After Clean')
  endif
  !
  ! Now transform UVMAX in kiloWavelength (including 2 pi factor)
  uvmax = uvmax*freq*f_to_k
  uvmin = uvmin*freq*f_to_k
  error = .false.
end subroutine mosaic_sort
!
subroutine mosaic_sortuv (np,nv,ntrail,vin,vout,xy,cs,uvmax,uvmin, &
  & error,ixoff,iyoff,nf,doff,voff)
  use gildas_def
  use gkernel_interfaces
  use imager_interfaces, except_this=> mosaic_sortuv
  !---------------------------------------------------------------------
  ! @ public-mandatory
  !
  ! IMAGER
  !     Sort a UV table by fields
  !     Rotate, Shift and Sort a UV table for map making
  !     Differential precession should have been applied before.
  !---------------------------------------------------------------------
  integer, intent(in) :: np        ! Size of a visibility
  integer, intent(in) :: nv        ! Number of visibilities
  integer, intent(in) :: ntrail    ! Number of trailing daps
  real, intent(in) :: vin(np,nv)   ! Input visibilities
  real, intent(out) :: vout(np,nv) ! Output visibilities
  real, intent(in) :: xy(2)        ! Phase shift
  real, intent(in) :: cs(2)        ! Frame Rotation
  real, intent(out) :: uvmax       ! Max UV value
  real, intent(out) :: uvmin       ! Min UV value
  integer, intent(in) :: ixoff, iyoff
  integer, intent(out) :: nf
  real, intent(out) :: doff(:,:)
  integer, intent(out) :: voff(:)
  logical, intent(out) :: error    !
  ! Global
  real(8), parameter :: pi=3.14159265358979323846d0
  integer, parameter :: mprint=0
  ! Local
  logical, allocatable :: ips(:)       ! Sign of visibility
  real, allocatable :: rpu(:), rpv(:)  ! U,V coordinates
  real, allocatable :: spv(:)          ! Sorted V coordinates
  integer, allocatable :: ipi(:)       ! Index
  real(8), allocatable :: dtr(:)       ! Sorting number
  logical :: sorted
  integer :: ier, ifi, iv
  !
  ! Load U,V coordinates, applying possible rotation (CS),
  ! and making all V negative
  allocate (ips(nv),rpu(nv),rpv(nv),ipi(nv),dtr(nv),stat=ier)
  if (ier.ne.0) then
    error = .true.
    return
  endif
  call loaduv (vin,np,nv,cs,rpu,rpv,ips,uvmax,uvmin)
  !!Print *,'UVMIN ',uvmin,' UVMAX ',uvmax,' NP ',np,' NV ',nv
  !
  ! Modify the uv coordinates to minimize
  ! the projection errors ... See Sault et al 1996 Appendix 1
  ! Key question here
  ! - modification must be done before sorting
  ! - but should we use the modified or intrinsic UV coordinates ?
  !
  ! For the rotation above, it does not matter, actually: the
  ! matrix commutes (I think so - That can be check later...)
  !
  !  call remapuv(nv,cs,rpu,rpv,ixoff,iyoff,uvmax,uvmin)
  !
  ! Identify number of fields
  call loadfiuv (vin,np,nv,dtr,ipi,sorted,ixoff,iyoff,rpv,nf,doff)
  !
  ! Sort by fields (major number) then V (fractionary part)
  if (.not.sorted) then
    !!Print *,'Sorting UV data '
    call gr8_trie (dtr,ipi,nv,error)
    if (error) return
    deallocate (dtr,stat=ier)
    allocate (spv(nv),stat=ier)
    if (ier.ne.0) then
      error = .true.
      return
    endif
    !
    ! One must sort RPV here to use SORTUV later...
    do iv=1,nv
      spv(iv) = rpv(ipi(iv))
    enddo
    rpv(:) = spv(:)
    deallocate (spv,stat=ier)
  else
    deallocate (dtr,stat=ier)
    !!Print *,'UV Data is already sorted '
  endif
  !!Read(5,*) ifi
  !
  ! Apply phase shift and copy to output visibilities
  call sortuv (vin,vout,np,nv,ntrail,xy,rpu,rpv,ips,ipi)
  !
  ifi = 1
  voff(ifi) = 1
  do iv=1,nv
    if ( (doff(1,ifi).ne.vout(ixoff,iv)) .or. &
      &  (doff(2,ifi).ne.vout(iyoff,iv)) ) then
      ifi = ifi+1
      voff(ifi) = iv
    endif
  enddo
  voff(nf+1) = nv+1
  !
  if (mprint.eq.0) return
  !
  !!Print *,'XOFF ',ixoff,' YOFF ',iyoff
  do ifi=1,min(nf,mprint)
    write(*,'(I4,A,2F12.4,2I10)') ifi,' DOFF ', &
      & doff(1,ifi)*180.*3600./pi, &
      & doff(2,ifi)*180.*3600./pi, &
      & voff(ifi), voff(ifi+1)-1
  enddo
  if (nf.gt.mprint) write(*,*) 'and ',nf-mprint,' more fields used but not printed above'
  !
  error = .false.
  !
end subroutine mosaic_sortuv
!
subroutine check_order_mosaic(visi,np,nv,ixoff,iyoff,sorted)
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Check if visibilites are sorted.
  !   Chksuv does a similar job, but using V values and an index.
  !---------------------------------------------------------------------
  integer, intent(in) :: np       ! Size of a visibility
  integer, intent(in) :: nv       ! Number of visibilities
  real, intent(in) :: visi(np,nv) ! Visibilities
  integer, intent(in) :: ixoff    ! X pointing column
  integer, intent(in) :: iyoff    ! Y pointing column
  logical, intent(out) :: sorted
  !
  real vmax,xoff,yoff
  integer iv
  !
  vmax = visi(2,1)
  xoff = visi(ixoff,1)
  yoff = visi(iyoff,1)
  !
  do iv=2,nv
    if (visi(2,iv).lt.vmax) then
      if (visi(ixoff,iv).eq.xoff .and. visi(iyoff,iv).eq.yoff) then
        !!Print *,'Unsorted V at ',iv,visi(2,iv),vmax
        sorted = .false.
        return
      endif
      ! else, this is a new offset
      xoff = visi(ixoff,iv)
      yoff = visi(iyoff,iv)
    else if (visi(ixoff,iv).eq.xoff .and. visi(iyoff,iv).eq.yoff) then
      ! ok, things progress normally
      continue
    else
      ! Unsorted offset
      !!Print *,'Unsorted Position offset at ',iv
      sorted = .false.
      return
    endif
    vmax = visi(2,iv)
  enddo
  sorted = .true.
end subroutine check_order_mosaic
!
subroutine loadfiuv (visi,np,nv,dtr,it,sorted,ixoff,iyoff,rpv,nf,doff)
  !---------------------------------------------------------------------
  ! @ public-mandatory
  !
  ! IMAGER    UVSORT routines
  !     Load field numbers into work arrays for sorting.
  !---------------------------------------------------------------------
  integer, intent(in)  :: np                   ! Size of a visibility
  integer, intent(in)  :: nv                   ! Number of visibilities
  real, intent(in) :: visi(np,nv)              ! Input visibilities
  real(8), intent(out) :: dtr(nv)              ! Output field number
  integer, intent(out) :: it(nv)               ! Indexes
  logical, intent(out) :: sorted               !
  integer, intent(in)  :: ixoff                ! X pointer
  integer, intent(in)  :: iyoff                ! Y pointer
  real(4), intent(in)  :: rpv(nv)              ! V Values
  integer, intent(inout) :: nf                 ! Number of fields
  real(kind=4), intent(out) :: doff(:,:)       ! Fields offsets
  !
  integer :: iv, j
  integer :: ifi, mfi, kfi, nfi
  real(8) :: vmax
  !
  ! Scan how many fields
  nfi = 1
  mfi = ubound(doff,2)
  if (nf.ne.mfi) Print *,'Warning Number of field mismatch ',nf, mfi
  !
  ! V are negative values, so this 1 + max(abs(V))
  vmax = 1.0d0-minval(rpv)
  !
  doff(1,1) = visi(ixoff,1)
  doff(2,1) = visi(iyoff,1)
  dtr(1) = 1.d0+rpv(1)/vmax ! We have here 0 =< dtr < 1
  !
  do iv=2,nv
    kfi = 0
    if (rpv(iv).gt.0) then
      Print *,'Unsorted Visibility with V > 0 ',iv,rpv(iv)
    endif
    do ifi=1,nfi
      if (visi(ixoff,iv).eq.doff(1,ifi) .and. &
      & visi(iyoff,iv).eq.doff(2,ifi) ) then
        dtr(iv) = dble(ifi)+rpv(iv)/vmax
        kfi = ifi
        exit
      endif
    enddo
    !
    ! New field
    if (kfi.eq.0) then
      if (nfi.eq.mfi) then
        Print *,'Programming error: More fields than expected ',mfi
        Print *,'Invalid number of Fields ',mfi,' at ',iv
        Print *,visi(ixoff,iv),visi(iyoff,iv)
        do j=1,mfi
          print *,doff(:,j)
        enddo
        return
      endif
      nfi = nfi+1
      doff(1,nfi) = visi(ixoff,iv)
      doff(2,nfi) = visi(iyoff,iv)
      dtr(iv) = dble(nfi)+rpv(iv)/vmax   ! nfi-1 =< dtr < nfi
      !!Print *,'New field ',nfi,' at ',doff(1:2,nfi),' Visi ',iv,dtr(iv)
    endif
  enddo
  !
  nf = nfi
  !
  do iv=1,nv
    it(iv) = iv
  enddo
  !
  ! DTR must in the end be ordered and increasing.
  vmax = dtr(1)
  do iv = 1,nv
    if (dtr(iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = dtr(iv)
  enddo
  sorted = .true.
  !
end subroutine loadfiuv
!
subroutine select_fields(rname,line,o_field,mp,np,fields,error)
  use gkernel_interfaces
  use imager_interfaces, only : get_i4list_fromsic, map_message
  use clean_arrays
  use gkernel_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER
  !   Select a list of fields from a Mosaic
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: line
  integer, intent(in) :: o_field
  integer, intent(in) :: mp ! Number of fields in UV data
  integer, intent(out) :: np ! Number of fields selected 
  logical, intent(inout) :: error
  integer, intent(inout), allocatable :: fields(:)
  !
  real, parameter :: rad_to_sec=180*3600/acos(-1.0)
  type(sic_descriptor_t) :: desc
  integer :: ifield, jfield, n, i, ier
  character(len=80) :: chain
  logical :: found
  !
  np = sic_narg(o_field)
  if (np.le.1) then
    call sic_ch(line,o_field,1,chain,n,.true.,error)
    if (error) return
    call sic_descriptor(chain,desc,found)
    if (found) then
      np = 0
    else
      np = 1
    endif
  endif
  !
  if (np.ne.0) then
    allocate(fields(np), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    do i=1,np
      call sic_i4(line,o_field,i,fields(i),.true.,error)
      if (error) return
    enddo
  else 
    call get_i4list_fromsic(rname,line,o_field,np,fields,error) 
  endif
  !
  if (np.gt.mp) then
    call map_message(seve%e,rname,'More selected fields than available')
    error = .true.
  else 
    do jfield=1,np
      ifield = fields(jfield)
      if (ifield.le.0 .or. ifield.gt.mp) then
        write(chain,'(A,I0,I0,A,I0,A)') 'Selected field ',jfield,& 
        & ifield,' out of range [1,',mp,']'
        call map_message(seve%e,rname,chain)
        error = .true.
      endif
    enddo
  endif
  if (error) return
  write(chain,'(I0,A,I0,A)') np,' fields selected:' 
  call map_message(seve%i,rname,chain)
  !
  do jfield=1,np
    ifield = fields(jfield)
    write(*,'(I0,1X,F10.2,F10.2)') ifield, themap%offxy(1,ifield)*rad_to_sec, themap%offxy(2,ifield)*rad_to_sec
  enddo
  !
end subroutine select_fields
!
