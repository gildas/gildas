!
subroutine eq_planet(i_planet, s_3, error)
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  !  @ private
  !  Get Apparent RA and DEC of planet I_PLANET
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: i_planet  !
  real(kind=8),    intent(out) :: s_3(3)    !
  logical,         intent(out) :: error     !
  ! Local
  real(kind=8) :: sun_distance
  integer(kind=4) :: m_body
  parameter (m_body = 9)
  integer(kind=4) :: vector(m_body), i
  real(kind=8) :: x_0(3), x_2(3), v_0(3), v_2(3), result(6), trfm_32(9)
  real(kind=8) :: x_3(3), distance
  equivalence (result(1), x_0(1)), (result(4),v_0(1))
  ! Data
  data vector /1, 2, 3, 3, 3, 3, 3, 3, 3/
  !------------------------------------------------------------------------
  ! Code :
  ! Save Current RA & DEC in case
  ra_old=ra
  dec_old=dec
  ! Compute new values
  error = .false.
  call ephsta(vector(i_planet), i_planet, error)
  if (error) return
  !
  ! Get ecliptic J2000 coordinates
  call ephvec(jnow_tdt, 1, result, error)
  !
  ! Get Apparent Horizontal coordinates
  call matvec(x_0, trfm_20, x_2)
  call matvec(v_0, trfm_20, v_2)
  !
  ! Light-time correction
  distance = dsqrt(x_2(1)**2 + x_2(2)**2 + x_2(3)**2)
  do i = 1, 3
    x_2(i) = x_2(i) - distance / light*v_2(i)
  enddo
  s_3(3) = sun_distance(x_2)
  !
  ! Apparent equatorial coordinates (without parallax)
  call transp(trfm_23, trfm_32)
  call matvec(x_2, trfm_32, x_3)
  call spher(x_3, s_3)
  ra = s_3(1)
  dec = s_3(2)
end subroutine eq_planet
!
subroutine do_astro_time(jutc,jut1,jtdt,error)
  use gbl_message
  use gkernel_interfaces
!!  use ast_horizon
  use ast_astro
  use ast_constant
  ! use imager_interfaces, except_this=>do_astro_time
  use imager_interfaces, only : oblimo, tsmg 
  !---------------------------------------------------------------------
  ! @ public
  !  Compute Earth rotation parameters and related quantities giving
  !  - orientation of Earth rotation axis in space (i.e. precession and nutation)
  !  - position angle around this axis (i.e. sidereal time)
  !	Earth rotation velocity (J2000.0) = 7.2921151467e-5 rd/s
  !	see Aoki et al. (1982) or Astron. Almanach Suppl. (1984)
  !	if considered constant : error 1.e-8 (i.e. 0.01" after 24 hours)
  !  - orientation of the CIO (Conventional International Origin) with respect
  !	to the Celestial Ephemeris frame (i.e. pole motion)
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: jutc              !
  real(kind=8), intent(in) :: jut1              !
  real(kind=8), intent(in) :: jtdt              !
  logical, intent(inout) :: error                  !
  ! Local
  real(kind=8) :: dpsi,deps             ! nutation parameters
  real(kind=8) :: gst                   ! Greenwich mean sidereal time (radians)
  real(kind=8) :: epsm
  real(kind=8) :: result(6), angles(6), psi, the, phi, ct, st, mat1(9), mat2(9)
  real(kind=8) :: mat3(9), trfm_24(9), l(2), g(2), x_5(3), v_5(3), s_5(2)
  real(kind=8) :: distance, n
  integer(kind=4) :: i, j
  real(kind=8) :: ua
  parameter (ua=149597870.d0)
  real(kind=8) :: sunrise_ut, sunset_ut
  save sunrise_ut, sunset_ut
  logical :: first
  character(len=10) :: rname
  ! Data
  data rname /'ASTRO_TIME'/
  data first/.true./
  !------------------------------------------------------------------------
  jnow_utc = jutc
  d_tdt = jtdt
  d_ut1 = jut1
  jnow_tdt = jnow_utc + d_tdt/86400.d0
  jnow_ut1 = jnow_utc + d_ut1/86400.d0
  call qprec (j2000,jnow_utc,angles)  ! compute precession angles
  psi=angles(5)                       ! Euler angles of transformation
  the=angles(4)                       ! mean ecliptic J2000.0 to JNOW
  phi=-angles(6)-angles(5)
  call eulmat (psi,the,phi,mat1)      ! compute matrix
  epsm=oblimo (jnow_utc)              ! mean obliquity at JNOW
  call nuta (jnow_utc,dpsi,deps)      ! compute nutation at JNOW
  psi=-dpsi                           ! Euler angles of transformation
  the=0.d0                            ! mean ecliptic to "true" ecliptic
  phi=0.d0                            ! reference system
  call eulmat (psi,the,phi,mat2)      ! compute matrix
  call mulmat (mat1,mat2,mat3)        ! product MAT2 x MAT1
  psi=0.d0                            ! Euler angles of transformation
  the=-epsm-deps                      ! "true" ecliptic to true equatorial
  phi=0.d0                            ! reference system
  call eulmat (psi,the,phi,mat1)      ! compute matrix
  call mulmat (mat3,mat1,trfm_30)     ! product MAT1 x MAT3
  !
  !  Compute sidereal time
  !  The true sidereal time is equal to the mean sidereal time plus a nutation
  !  correction DPSI * cos (EPSM+DEPS) + terms smaller than 0.003"
  !  (see Guinot's BIH internal note and Aoki + al 1983, Celest. mech. 29, 335).
  !  According to G. Francou one should use DPSI * ( cosEPSM - 1/2 DEPS sinEPSM)
  !  This point has to be checked.
  !  %%% Take DUT1 = UT1 - UTC from TO$DUT	RL 21-oct-85
  !
  gst=tsmg(jnow_ut1)                  ! Greenwich mean ST at UTC=0. (radians)
  gst=gst+dpsi*dcos(epsm+deps)        ! nutation correction (already computed)
  ! East longitude is positive
  lst=gst+ pi/180d0*lonlat(1)         ! Local "true" sidereal time
  if (lst .lt. 0d0) lst = lst + twopi
  if (lst .ge. twopi) lst = lst - twopi
  !
  ! Earth rotation matrix (M43)
  do i=1, 3
    do j=1,3
      trfm_43(i+3*(j-1)) = 0
    enddo
  enddo
  ct = cos(lst)
  st = sin(lst)
  trfm_43(1) = ct
  trfm_43(5) = ct
  trfm_43(4) = st
  trfm_43(2) = - st
  trfm_43(9) = 1.d0
  !
  !  Compute Earth orientation, with respect to the instantaneous axis of rotation
  !  ### Pole motion should be included in TRFM24
  psi=halfpi                   ! 90 degrees
  the=halfpi-pi/180d0*lonlat(2)    ! 90 degrees minus latitude
  phi=-halfpi                  ! -90 degrees
  call eulmat (psi,the,phi,trfm_24)    ! compute transformation matrix
  call mulmat(trfm_43, trfm_24, trfm_23)   ! (M23) = M24 * M43
  call mulmat(trfm_30, trfm_23, trfm_20)   ! M20 = (M23) * M30
  ! Default G and Sun ephemeris ... (Page C24)
  do i=1,2
    n = jnow_tdt-j2000+i-1
    l(i) = mod(280.460d0 + 0.9856474*n + 36000.d0,360.d0)
    g(i) = mod(357.528d0 + 0.9856003*n + 36000.d0,360.d0)
    s_5(1) = (l(i) + 1.915*sin(pi*(g(i))/180.d0) + 0.020*sin(pi*(2*g(i))/  &
             180.0d0))*pi/180.
    s_5(2) = 0.0
    if (i.eq.1) then
      call rect(s_5, x_5)
    else
      call rect(s_5, v_5)
    endif
  enddo
  distance = (1.00014d0-0.01671d0*cos(pi*(g(1))/180.0d0)- 0.00014d0*   &
             cos(pi*(2*g(1))/180.0d0))*ua
  do i=1,3
    v_5(i) = (v_5(i)-x_5(i)) / 86400.d0 * distance
    x_5(i) = x_5(i) * distance
  enddo
  call qprec (jnow_tdt,j2000,angles)   ! compute precession angles
  psi=angles(5)                ! Euler angles of transformation
  the=angles(4)
  phi=-angles(6)-angles(5)
  ! compute matrix from ecl (now) to (R0)
  call eulmat (psi,the,phi,trfm_05)
  call matvec (x_5, trfm_05, xsun_0)
  call matvec (v_5, trfm_05, vg_0) ! use for velocity determination
  do i=1, 3
    xg_0(i) = xsun_0(i)
  enddo
  !
  ! Start ephemeride
  error = .false.
  !
  ! Get Earth motion (/ Sol Sys Barycenter)
  call ephsta (0, 0, error)
  if (error) then
    call astro_message(seve%f,rname,'Error in EPHSTA')
    return
  endif
  call ephvec (jnow_tdt, 1, result, error)
  if (.not.error) then
    do i=1, 3
      xg_0(i) = result(i)
      vg_0(i) = result(i+3)
    enddo
  else
    error = .false.
    call astro_message(seve%w,rname,'Using approximate Earth motion')
  endif
  !
  ! Get Sun position
  call ephsta( 2, 2, error)
  if (error) then
    call astro_message(seve%f,rname,'Error in EPHSTA')
    return
  endif
  !
  ! Get ecliptic J2000 coordinates
  call ephvec(jnow_tdt, 1, result, error)
  if (error) then
    error = .false.
    call astro_message(seve%w,rname,'Using approximate Sun position')
  else
    do i=1, 3
      xsun_0(i) = result(i)
    enddo
  endif
  call matvec(xsun_0, trfm_20, xsun_2)
  nsplot = 0
  !
  !call sunrise(error)
  !sunrise_ut= (mod(jnow_utc+0.5d0,1d0)+(sunriz(1)-lst)/twopi)*24.
  !sunset_ut= (mod(jnow_utc+0.5d0,1d0)+(sunset(1)-lst)/twopi)*24.
  !if (first) then
  !  call sic_def_dble ('SUNSET',sunset_ut,1,1,.true.,error)
  !  call sic_def_dble ('SUNRISE',sunrise_ut,1,1,.true.,error)
  !  first = .false.
  !endif
  !return
end subroutine do_astro_time
!
