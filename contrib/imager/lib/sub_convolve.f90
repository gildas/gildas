subroutine convolve (image,dirty,nx,ny,fbeam,fcomp,wfft)
  use gkernel_interfaces, only : fourt
  !----------------------------------------------------------------------
  ! @ public
  !
  ! MAPPING
  !	  Convolve an image by a beam through Fourier Transform
  !----------------------------------------------------------------------
  integer, intent(in) :: nx,ny          ! Problem size
  complex, intent(in) :: fbeam(nx,ny)    ! Beam TF
  complex, intent(inout) :: fcomp(nx,ny) ! TF workspace
  real, intent(in) :: image(nx,ny)      ! Input image
  real, intent(out) :: dirty(nx,ny)     ! Convolved image
  real, intent(inout) :: wfft(*)        ! Work area
  !
  integer ndim,nn(2)
  !
  ! Reset
  ndim = 2
  nn(1) = nx
  nn(2) = ny
  fcomp = cmplx(image,0.0)
  !
  ! Direct TF
  call fourt(fcomp,nn,ndim,-1,0,wfft)
  !
  ! Multiplication (note normalisation done in FBEAM)
  fcomp = fcomp*fbeam
  !!do j=1,ny
  !!  do i=1,nx
  !!    reel = fbeam(1,i,j)*fcomp(1,i,j) - fbeam(2,i,j)*fcomp(2,i,j)
  !!    imag = fbeam(2,i,j)*fcomp(1,i,j) + fbeam(1,i,j)*fcomp(2,i,j)
  !!    fcomp(1,i,j) = reel
  !!    fcomp(2,i,j) = imag
  !!  enddo
  !!enddo
  !
  ! Reverse TF
  call fourt(fcomp,nn,ndim,1,1,wfft)
  !
  ! Dirty map
  dirty = real(fcomp)
end subroutine convolve
!
subroutine init_convolve (i0,j0,nx,ny,beam,fbeam,area,work)
  !----------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING   FFT Utility routine
  !	  Compute the FT of a beam centered on pixel I0,J0
  !----------------------------------------------------------------------
  integer, intent(in) :: nx,ny          ! Problem size
  integer, intent(in) :: i0,j0          ! Position of peak
  complex, intent(out) :: fbeam(nx,ny)  ! TF of beam
  real, intent(in) :: beam(nx,ny)       ! Beam
  real, intent(inout) :: work(*)        ! Work space
  real, intent(out) :: area             ! Beam area
  !
  integer i,j,nn(2),ndim
  real fact
  !
  ndim = 2
  nn(1) = nx
  nn(2) = ny
  fact = 1.0/float(nx*ny)
  do j=1,j0-1
    do i=1,i0-1
      fbeam(i+nx-i0+1,j+ny-j0+1) = cmplx(beam(i,j)*fact,0.0)
    enddo
    do i=i0, nx
      fbeam(i-i0+1,j+ny-j0+1) = cmplx(beam(i,j)*fact,0.0)
    enddo
  enddo
  do j=j0,ny
    do i=1,i0-1
      fbeam(i+nx-i0+1,j-j0+1) = cmplx(beam(i,j)*fact,0.0)
    enddo
    do i=i0,nx
      fbeam(i-i0+1,j-j0+1) = cmplx(beam(i,j)*fact,0.0)
    enddo
  enddo
  call fourt(fbeam,nn,ndim,-1,0,work)
  area = real(fbeam(1,1))/beam(i0,j0)
end subroutine init_convolve
