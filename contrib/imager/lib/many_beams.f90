!
subroutine many_beams_para (rname,map,huv,hbeam,hdirty,   &
     &    nx,ny,nu,nv,uvdata,   &
     &    r_weight, w_v, do_weig,    &
     &    wcol,mcol,sblock,cpu0,error,uvmax,jfield,abort,cthread)
  use gkernel_interfaces
  use imager_interfaces, except_this=>many_beams_para
  use clean_def
  use image_def
  use gbl_message
  !$ use omp_lib
  !------------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Compute a map from a CLIC UV Sorted Table
  !   by Gridding and Fast Fourier Transform, with
  !   a different beam per channel.
  !
  ! Input :
  ! a precessed UV table, sorted in V, ordered in
  ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
  ! Output :
  ! a beam image
  ! a VLM cube
  ! Work space :
  ! a  VLM complex Fourier cube (first V value is for beam)
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: rname   ! Calling Task name
  type (uvmap_par), intent(inout) :: map  ! Mapping parameters
  type (gildas), intent(inout) :: huv     ! UV data set
  type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
  type (gildas), intent(inout) :: hdirty  ! Dirty image data set
  integer, intent(in) :: nx   ! X size
  integer, intent(in) :: ny   ! Y size
  integer, intent(in) :: nu   ! Size of a visibilities
  integer, intent(in) :: nv   ! Number of visibilities
  real, intent(in) :: uvdata(nu,nv)
  real, intent(inout), target :: r_weight(nv)    ! Weight of visibilities
  real, intent(inout) :: w_v(nv)         ! V values
  logical, intent(inout) :: do_weig
  integer, intent(in) :: jfield      ! Field number (for mosaic)
  !
  real, intent(inout) :: cpu0        ! CPU
  real, intent(inout) :: uvmax       ! Maximum baseline
  integer, intent(inout) :: sblock   ! Blocking factor
  integer, intent(inout) :: wcol     ! Weight channel
  integer, intent(inout) :: mcol(2)  ! First and last channel
  logical, intent(inout) :: error, abort
  integer, intent(in)    :: cthread    ! Calling Thread number
  ! Global variables:
  !
  real(kind=8), parameter :: clight=299792458d-6 ! Frequency in  MHz
  !
  type (gridding) :: conv
  !
  integer :: nc   ! Number of channels
  integer :: nd   ! Size of data
  integer :: nb   ! Number of beams
  integer :: ns   ! Number of channels per single beam
  integer ier
  real(kind=8) :: freq
  integer ctypx,ctypy
  integer lcol,fcol
  real rmi,rma,wall,cpu1
  real xparm(10),yparm(10)
  real(8) :: vref,voff,vinc
  integer ndim, nn(2), lx, ly, kz1
  integer kz,iz,ic,kc,kb,jc,lc
  character(len=message_length) :: chain
  !
  real :: rms, null_taper(4), wold
  complex, allocatable :: ftbeam(:,:)
  complex, allocatable :: tfgrid(:,:,:)
  real, allocatable :: w_xgrid(:),w_ygrid(:), w_w(:),  w_grid(:,:), walls(:)
  real, allocatable :: w_weight(:)
  real, allocatable :: beam(:,:)
  real, allocatable :: w_mapu(:), w_mapv(:)
  real, allocatable :: local_wfft(:)
  real uvcell(2)
  real support(2)
  real(8) local_freq
  !
  integer :: ithread, othread, nthread
  real(8) :: elapsed_s, elapsed_e, elapsed
  !
  real :: toto
  logical :: local_error
  integer :: grid_code
  !
  !------------------------------------------------------------------------
  !
  call imager_tree('MANY_BEAMS_PARA in many_beams.f90')
  !
  ! Code:
  grid_code = 0
  error = .false.
  abort = .false.
  nd = nx*ny
  nc = huv%gil%nchan
  nb = hbeam%gil%dim(3)
  !
  ns = map%beam
  null_taper = 0
  !
  if (ns.ne.1 .and. nb.gt.1) then
    write(chain,'(a,i6,a)') 'Processing ',ns,' channels per beam'
    call map_message(seve%w,rname,chain)
  endif
  !
  ! Reset the parameters
  xparm = 0.0
  yparm = 0.0
  !
  vref = huv%gil%ref(1)
  voff = huv%gil%voff
  vinc = huv%gil%vres
  if (mcol(1).eq.0) then
    mcol(1) = 1
  else
    mcol(1) = max(1,min(mcol(1),nc))
  endif
  if (mcol(2).eq.0) then
    mcol(2) = nc
  else
    mcol(2) = max(1,min(mcol(2),nc))
  endif
  fcol = min(mcol(1),mcol(2))
  lcol = max(mcol(1),mcol(2))
  if (wcol.eq.0) then
    wcol = (fcol+lcol)/3
  endif
  wcol = max(1,wcol)
  wcol = min(wcol,nc)
  nc = lcol-fcol+1
  !
  ! Compute observing sky frequency for U,V cell size
  freq = gdf_uv_frequency(huv, 0.5d0*dble(lcol+fcol) )
  !
  ! Compute gridding function
  ctypx = map%ctype
  ctypy = map%ctype
  call grdflt (ctypx, ctypy, xparm, yparm)
  call convfn (ctypx, xparm, conv%ubuff, conv%ubias)
  call convfn (ctypy, yparm, conv%vbuff, conv%vbias)
  map%uvcell = clight/freq/(map%xycell*map%size)
  map%support(1) = xparm(1)*map%uvcell(1)  ! In meters
  map%support(2) = yparm(1)*map%uvcell(2)
  !
  ! Process sorted UV Table according to the type of beam produced
  !
  allocate (w_w(nv),w_weight(nv),walls(nb),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate Weight arrays')
    error = .true.
    return
  endif
  w_v(:) = uvdata(2,1:nv)
  !
  !
  lx = (uvmax+map%support(1))/map%uvcell(1) + 2
  ly = (uvmax+map%support(2))/map%uvcell(2) + 2
  lx = 2*lx
  ly = 2*ly
  if (ly.gt.ny) then
    write(chain,'(A,A,F8.3)') 'Map cell is too large ',   &
        &      ' Undersampling ratio ',float(ly)/float(ny)
    call map_message(seve%w,rname,chain,3)
    ly = min(ly,ny)
    lx = min(lx,nx)
  endif
  !
  ! Get FFTs and beam work spaces
  allocate (tfgrid(ns+1,lx,ly),ftbeam(nx,ny),beam(nx,ny),&
    & w_mapu(lx),w_mapv(ly),local_wfft(2*max(nx,ny)), &
    & w_xgrid(nx),w_ygrid(ny),w_grid(nx,ny),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate TF arrays')
    error = .true.
    return
  endif
  !
  call docoor (lx,-map%uvcell(1),w_mapu)
  call docoor (ly,map%uvcell(2),w_mapv)
  !
  ! Prepare grid correction,
  call grdtab (ny, conv%vbuff, conv%vbias, w_ygrid)
  call grdtab (nx, conv%ubuff, conv%ubias, w_xgrid)
  !
  ! Make beam, not normalized
  call uvmap_headers(huv,nx,ny,nb,ns,map,mcol,hbeam,hdirty,error)
  if (error) return
  if (sic_ctrlc()) then
    abort = .true. ! Quick abort
    return
  endif
  !
  ndim =2
  nn(1) = nx
  nn(2) = ny
  !
  nthread = 1
  if (nb.gt.1) then
    !$ nthread = ompget_inner_threads()
  endif
  !!Print *,'MANY_BEAMS_PARA Thread ',nthread,' NB ',nb,' NS ',ns
  !
  ! Loop over blocks
  !
  !$OMP PARALLEL IF (nb.gt.1) DEFAULT(none) NUM_THREADS (nthread) &
  !$OMP PRIVATE(tfgrid,ftbeam,beam,w_weight,w_w) &  ! Big arrays
  !$OMP PRIVATE(w_mapu,w_mapv,w_grid) &
  !$OMP PRIVATE(local_wfft,chain) &
  !$OMP PRIVATE(local_freq,support,wall,wold,rms,uvcell,local_error) &
  !$OMP PRIVATE(kz,kb,kc,iz,ic, kz1, toto, jc,lc) &
  !$OMP SHARED(walls,ns,nb) &
  !$OMP SHARED(nu,nv,nx,ny,nc,nd,fcol,lcol,lx,ly, nthread) &
  !$OMP SHARED(map,null_taper,error,abort,grid_code) &
  !$OMP SHARED(conv,freq,do_weig, r_weight) &
  !$OMP SHARED(nn,ndim,huv,hbeam,hdirty,rname) &
  !$OMP SHARED(w_xgrid,w_ygrid,w_v,uvdata) &
  !$OMP SHARED(cpu0,cpu1) PRIVATE(elapsed_s, elapsed_e, elapsed, ithread)
  !
  kz = 1 ! test for bug below
  !
  ! Print *,'KZ ',kz,' NC ',nc,' nd ',nd
  !$OMP DO
  do ic = fcol,lcol,ns ! ,ns or ,1
    if (sic_ctrlc()) abort = .true. ! Quick abort
    if (abort) cycle
    !
    kz = min(ns,lcol-ic+1)
    jc = min(huv%gil%nchan,ic+ns/3)   ! The default weight channel here...
    !!Print *,'IC ',ic,' JC ',jc,'MCOL ',fcol,lcol,' NS ',ns
    ithread = 1
    !$ elapsed_s = omp_get_wtime()
    !$ ithread = omp_get_thread_num()+1
    !Print *,'Thread ',ithread,' IC ',ic,', KZ ',kz,', NS ',ns
    !
    kb = (ic-fcol)/ns+1
    if (kb.gt.nb .or. kb.lt.1) then
      Print *,'Programming error, expected 0 < ',kb,' < ',nb+1
      kb = nb
    endif
    !
    w_w(:) = uvdata(7+3*jc,:)
    wold = sump(nv,w_w)
    !
    ! Search for a non empty weight channel
    if (wold.eq.0) then
      do lc=ic,min(ic+ns,lcol) ! not  ,huv%gil%nchan)
        if (lc.ne.jc) then
          w_w(:) = uvdata(7+3*lc,:)
          wold = sump(nv,w_w)
          if (wold.ne.0) then
            jc = lc
            exit
          endif
        endif
      enddo
    endif
    !
    if (wold.eq.0) then
      write(chain,'(A,I6,A)') 'Channel ',jc, ' has zero weight'
      hbeam%r3d(:,:,kb) = 0
      hdirty%r3d(:,:,kb) = 0
      walls(kb) = 0.0
      if (nb.eq.1) then
        call map_message(seve%e,rname,chain)
        error = .true.
      else
        call map_message(seve%w,rname,chain)
      endif
      cycle
    else
      wall = 1e-3/sqrt(wold)
      !!write(chain,'(a,i6,a)') 'Plane ',ic,' Natural '
      !!call prnoise('UV_MAP',trim(chain),wall,rms)
      walls(kb) = wall
    endif
    !
    ! Compute the weights from this
    if (do_weig) then
      local_error = .false.
      call doweig (nu,nv,   &
         &    uvdata,   &          ! Visibilities
         &    1,2,    &            ! U, V pointers
         &    jc,     &            ! Weight channel
         &    map%uniform(1),   &  ! Uniform UV cell size
         &    w_weight,   &        ! Weight array
         &    map%uniform(2),   &  ! Fraction of weight
         &    w_v,              &  ! V values
         &    local_error,      &
         &    grid_code)
         
      if (sic_ctrlc()) abort = .true. ! Quick abort
      if (abort) cycle
      if (local_error)  then
        error = .true.
        cycle
      endif
  !    Print *,ic,'doweig',w_v
  !    read(5,*) toto
      !
      ! Should also plug the TAPER here, rather than in DOFFT later  !
      call dotape (nu,nv,   &
         &    uvdata,   &          ! Visibilities
         &    1,2,   &             ! U, V pointers
         &    map%taper,  &        ! Taper
         &    w_weight)            ! Weight array
         !!Print *,ic,'dotape'
      if (sic_ctrlc()) abort = .true. ! Quick abort
      if (abort) cycle
    else
      call map_message(seve%i,rname,'Reusing weights')
      w_weight(:) = r_weight
    endif
    !
    ! Re-normalize the weights and re-count the noise
    wall = sump(nv,w_weight)
    if (wall.ne.wold) then
      call scawei (nv,w_weight,w_w,wall)
      wall = 1e-3/sqrt(wall)
      !!write(chain,'(a,i6,a)') 'Plane ',ic,' Expected '
      !!call prnoise('UV_MAP',trim(chain),wall,rms)
      walls(kb) = wall
    endif
    !
    ! Then compute the Dirty Beam
    local_freq = gdf_uv_frequency(huv, dble(ic))
    uvcell = map%uvcell * (freq / local_freq)
    support = map%support * (freq / local_freq)
    call docoor (lx,-uvcell(1),w_mapu)
    call docoor (ly,uvcell(2),w_mapv)
       !!Print *,ic,'docoor'
    if (sic_ctrlc()) abort = .true. ! Quick abort
    if (abort) cycle
    !
    ! Compute FFTs
    call dofft (nu,nv,          &   ! Size of visibility array
         &    uvdata,           &   ! Visibilities
         &    1,2,              &   ! U, V pointers
         &    ic,               &   ! First channel to map
         &    kz,lx,ly,         &   ! Cube size
         &    tfgrid,           &   ! FFT cube
         &    w_mapu,w_mapv,    &   ! U and V grid coordinates
         &    support,uvcell,null_taper, &  ! Gridding parameters
         &    w_weight,w_v,     &    ! Weight array + V Visibilities
         &    conv%ubias,conv%vbias,conv%ubuff,conv%vbuff,map%ctype)
       !!Print *,ic,'dofft'
    if (sic_ctrlc()) abort = .true. ! Quick abort
    if (abort) cycle
    !
    kz1 = kz+1
    call extracs(kz1,nx,ny,kz1,tfgrid,ftbeam,lx,ly)
    call fourt  (ftbeam, nn,ndim,-1,1,local_wfft)
    beam = 0.0
    call cmtore (ftbeam, beam ,nx,ny)
    call chkfft (beam, nx,ny, error)
    !!   Print *,ic,'BEAM ',nx,ny,beam(nx/2+1,ny/2+1)
    !!Print *,'NU,NV ',nu,nv
    !!Print *,'IC ',ic
    !!Print *,'KZ,LX,LY ',kz,lx,ly
    !!Print *,'support ',support,' uvcell ',uvcell, ' null_taper ',null_taper
    !!Print *,'conv%ubias,conv%vbias,map%ctype ',conv%ubias,conv%vbias,map%ctype
    !!Print *, ' '
    if (error) then
      Print *,ic,'BEAM ',nx,ny,beam(nx/2+1,ny/2+1)
      Print *,'Local freq ',local_freq
      Print *,'KZ, LX, LY ', kz,lx,ly, ' nx,ny ',nx,ny, ' NS ',ns
      call gagout('E-UV_MAP,  Inconsistent pixel size')
!      read(5,*) ians
!      if (ians.eq.1) then
!        Print *,tfgrid(kz+1,:,ly/2)
!      endif
      cycle
    endif
    !
    ! Compute grid correction,
    ! Normalization factor is applied to grid correction, for further
    ! use on channel maps.
    !
    ! Make beam, not normalized
    call dogrid (w_grid,w_xgrid,w_ygrid,nx,ny,beam)  ! grid correction
    if (sic_ctrlc()) abort = .true. ! Quick abort
    if (abort) cycle
    !
    ! Normalize and Free beam
    call docorr (beam,w_grid,nx*ny)
       !!Print *,ic,'docorr'
    !
    ! Write beam
    hbeam%r3d(:,:,kb) = beam
       !!Print *,ic,'Done Beam ',kc,nc
    ! --- Done beam
    !
    ! Now extracts the Image planes...
    do iz=1,kz
      if (sic_ctrlc()) abort = .true. ! Quick abort
      if (abort) cycle
      call extracs(kz+1,nx,ny,iz,tfgrid,ftbeam,lx,ly)
      call fourt  (ftbeam,nn,ndim,-1,1,local_wfft)
      call cmtore (ftbeam,beam,nx,ny)
      call docorr (beam,w_grid,nd)
      ! Write the subset
      kc = ic-fcol+iz
      hdirty%r3d(:,:,kc) = beam
    enddo
    if (sic_ctrlc()) abort = .true. ! Quick abort
    if (abort) cycle
    !
    !$  elapsed_e = omp_get_wtime()
    elapsed = elapsed_e - elapsed_s
    write(chain,103) 'End plane ',kc,' Time ',elapsed &
      & ,' Thread ',ithread
    call map_message(seve%d,rname,chain)
    if (do_weig .and. nb.eq.1) then
      do_weig = .false.
      r_weight = w_weight
    endif
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
!!  !$ if (nb.gt.1) call omp_set_num_threads(othread)
  if (error) return
  if (abort) return
  !
  call gag_cpu(cpu1)
  if (jfield.eq.0) then
    write(chain,102) 'Finished maps ',cpu1-cpu0
    call map_message(seve%i,rname,chain)
  endif
  !
  wall = maxval(walls(1:nb))
  if (jfield.eq.0) then
    chain = 'Expected'
  else
    write(chain,'(A,I0,A)') 'Field ',jfield,'; Expected'
  endif
  call prnoise(rname,trim(chain),wall,rms)
  hdirty%gil%noise = wall
  !  !
  ! Delete scratch space
  error = .false.
  if (nb.ne.1)  deallocate(w_grid)
  if (allocated(tfgrid)) deallocate(tfgrid)
  if (allocated(ftbeam)) deallocate(ftbeam)
  if (allocated(w_xgrid)) deallocate(w_xgrid)
  if (allocated(w_ygrid)) deallocate(w_ygrid)
  !
  return
  !
102 format(a,f9.2)
103 format(a,i5,a,f9.2,a,i2,a,i2)
end subroutine many_beams_para
