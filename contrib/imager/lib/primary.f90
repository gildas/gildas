!
subroutine primary_comm(line,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>primary_comm
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER  Support for command
  !   PRIMARY [BeamSize] [/TRUNCATE Percent]
  !
  ! Compute primary beam and Use it as "flat field" correction
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line ! Command line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='PRIMARY'
  real(8), parameter :: pi=3.14159265358979323846d0
  integer, parameter :: o_trunc=1
  !
  real, allocatable :: dflat(:,:)
  integer :: i, ier
  integer :: ix, iy
  real :: btrunc
  character(len=80) :: mess
  character(len=8) :: key
  integer :: nk
  !
  ! Verify if any Deconvolved data 
  if (hclean%loca%size.eq.0) then
    call map_message(seve%e,rname,'No CLEAN image')
    error = .true.
    return
  endif
  !
  key = 'NONE'
  call sic_ke(line,0,1,key,nk,.false.,error)
  if (key.eq.'APPLY') then
    !
    ! Verify Beam compatibility
    if (.not.allocated(dprim)) then
      call map_message(seve%e,rname,'No PRIMARY beam to APPLY')
      error = .true.
    else if ( &
      & (hprim%gil%dim(1).ne.1) .or. &
      & (hprim%gil%dim(2).ne.hclean%gil%dim(1)) .or. &
      & (hprim%gil%dim(3).ne.hclean%gil%dim(2)) .or. &
      & (hprim%gil%inc(2).ne.hclean%gil%inc(1)) .or. &
      & (hprim%gil%inc(3).ne.hclean%gil%inc(2)) ) then
      call map_message(seve%e,rname,'PRIMARY beam and CLEAN image do not match')
      error = .true.
    else
      btrunc = hprim%gil%inc(1)
    endif
    if (error) return
    hprim%r3d => dprim(:,:,:,1)
  else
    !
    ! Compute Primary Beam first
    call primary_single(line,error)
    if (error) return
    !
    ! Get truncation level
    btrunc = 0.2
    call sic_get_real('CLEAN_TRUNCATE',btrunc,error)
    if (error) return 
  endif
  !
  ! Override truncation level if needed
  if (sic_present(o_trunc,0)) then
    call sic_r4(line,o_trunc,1,btrunc,.true.,error)
    if (error) return
    btrunc = btrunc*0.01
  endif
  !
  write(mess,'(a,f6.0,a)') 'Correcting for primary down to ',100*btrunc,'% '
  call map_message(seve%i,rname,mess)
  hprim%gil%inc(1) = btrunc ! Increment must  be the Beam Truncation 
  !
  if (allocated(dsky)) then
    if (any(hsky%gil%dim.ne.hclean%gil%dim)) then
      ! That may happen if some READ SKY has been done
      ! Print *,'Programming error - Sky & Clean mismatch'
      deallocate(dsky)
    else
      call map_message(seve%i,rname,'Re-using sky memory')
    endif
    call sic_delvariable('SKY',.false.,error)
  endif
  !
  if (.not.allocated(dsky)) then
    allocate (dsky(hclean%gil%dim(1), hclean%gil%dim(2), hclean%gil%dim(3)), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
  endif
  !
  call gildas_null(hsky)
  call gdf_copy_header(hclean,hsky,error)
  !
  allocate (dflat(hclean%gil%dim(1),hclean%gil%dim(2)),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  do iy=1,hclean%gil%dim(2)
    do ix = 1,hclean%gil%dim(1)
      if (hprim%r3d(1,ix,iy).lt.btrunc) then
        dflat(ix,iy) = -1.
      else
        dflat(ix,iy) = 1.0/hprim%r3d(1,ix,iy)
      endif
    enddo
  enddo
  !
  if (hclean%gil%rmin.lt.0) then
    hsky%gil%bval = 2*hclean%gil%rmin/btrunc
    hsky%gil%eval = -0.01*hclean%gil%rmin/btrunc
  else
    hsky%gil%bval = -1.0
    hsky%gil%eval = 0.1
  endif
  hsky%gil%blan_words = 2
  !
  dsky = hsky%gil%bval
  do i=1,hclean%gil%dim(3)
    where ( (dflat.ge.0).and.(abs(dclean(:,:,i)-hclean%gil%bval).gt.hclean%gil%eval) )
      dsky(:,:,i) = dclean(:,:,i) * dflat
    end where
  enddo
  hsky%loca%addr = locwrd(dsky)
  call gdf_get_extrema(hsky,error)
  hsky%gil%extr_words = def_extr_words
  !
  call sic_mapgildas('SKY',hsky,error,dsky)
  save_data(code_save_sky) = .true.
end subroutine primary_comm
!
subroutine primary_single(line,error)
  use gkernel_interfaces
  use imager_interfaces, except_this=>primary_single
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER  Support for command
  !   PRIMARY [BeamSize] 
  !
  ! Compute primary beam for a single field
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line ! Command line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='PRIMARY'
  real(8), parameter :: pi=3.14159265358979323846d0
  integer, parameter :: o_trunc=1
  !
  integer :: nr, ir,ix,iy, ier
  real(8), allocatable :: profile(:,:) ! Radial profile
  real(8) :: doffx, doffy, dr, r, x, y, offx, offy
  real(4) :: bsize
  type(projection_t) :: proj
  type(gildas) :: htmp
  !
  bsize = 0
  if (sic_present(0,1)) then
    call sic_r4(line,0,1,bsize,.true.,error)
    if (error) return
    bsize = bsize*pi/180/3600
  endif
  !
  call primary_radial(line,bsize,hclean,nr,profile,error)
  if (error) return
  !
  if (allocated(dprim)) then
    if (any(hprim%gil%dim(1:2).ne.hclean%gil%dim(1:2))) then
      ! That may happen if some READ  has been done
      ! Print *,'Programming error - Sky & Clean mismatch'
      deallocate(dprim)
    else
      call map_message(seve%i,rname,'Re-using PRIMARY memory')
    endif
    call sic_delvariable('PRIMARY',.false.,error)
  endif
  if (.not.allocated(dprim)) then
    allocate (dprim(1, hclean%gil%dim(1), hclean%gil%dim(2), 1), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
  endif
  !
  call gildas_null(hprim)
  call gildas_null(htmp)
  call gdf_copy_header(hclean,htmp,error)
  htmp%gil%ndim = 3
  htmp%gil%dim(3:4) = 1
  call gdf_transpose_header(htmp,hprim,'312',error)
  !
  hprim%r3d => dprim(:,:,:,1)
  ! The increment should be the Beam Truncation level, defined later
  hprim%gil%convert(:,1) = 1.d0
  hprim%char%code(1) = 'FIELDS'
  hprim%gil%rmin = 0.0 
  hprim%gil%rmax = 1.0
  !
  ! Handle  Pointing offsets 
  error = .false.
  call gwcs_projec(hprim%gil%a0,hprim%gil%d0,hprim%gil%pang,hprim%gil%ptyp,proj,error)
  call abs_to_rel(proj,hprim%gil%ra,hprim%gil%dec,doffx,doffy,1)
  offx = doffx/hprim%gil%inc(2)
  offy = doffy/hprim%gil%inc(3)
  dr = profile(2,1) - profile(1,1) ! Increment
  !
  do iy=1,hprim%gil%dim(3)
    y = (iy-hprim%gil%ref(3)-offy)*hprim%gil%inc(3) + hprim%gil%val(3)
    do ix=1,hprim%gil%dim(2)
      x = (ix-hprim%gil%ref(2)-offx)*hprim%gil%inc(2) + hprim%gil%val(2)
      r = sqrt(x**2+y**2)
      !
      ! Locate by interpolation
      ir = int(r/dr)+1
      if (ir.lt.nr) then
        hprim%r3d(1,ix,iy) = ((profile(ir+1,1)-r)*profile(ir,2) + (r-profile(ir,1))*profile(ir+1,2) ) /dr
      else
        hprim%r3d(1,ix,iy) = 0
      endif
    enddo
  enddo
  call sic_mapgildas('PRIMARY',hprim,error,dprim)
  !
end subroutine primary_single
!
subroutine primary_radial(line,bsize,head,nr,profile,error)
  use gkernel_interfaces
  use image_def
  use imager_interfaces, except_this=>primary_radial
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER  
  !
  ! Compute primary beam radial profile
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line ! Command line
  real(4), intent(inout) :: bsize      ! Force Beam Size if non zero
  type(gildas), intent(in) :: head     ! Header of image data cube
  integer, intent(out) :: nr           ! Number of radial points
  real(8), allocatable, intent(out) :: profile(:,:) ! Radial profile
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='PRIMARY'
  real(8), parameter :: pi=3.14159265358979323846d0
  integer, parameter :: o_trunc=1
  !
  integer :: ir, ier
  real(8) :: dr
  character(len=80) :: mess  
  !
  ! Compute radial profile of primary beam beyond Map limits
  nr = 8*max(head%gil%dim(1),head%gil%dim(2))
  allocate (profile(nr,2),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  dr = min(abs(head%gil%inc(1)),abs(head%gil%inc(2)))/4
  do ir=1,nr
    profile(ir,1) = (ir-1)*dr
  enddo
  !
  if (bsize.eq.0) then
    if (head%gil%nteles.gt.1) then
      if (head%gil%teles(1)%ctele.eq.'ALMA') then
        call map_message(seve%i,rname,'Using ALMA beam shape')
        call primary_alma(head,nr,profile) 
        return
      endif
    endif
  endif
  ! Get or Check beam size
  call get_bsize(head,rname,line,bsize,error) 
  !
  if (bsize.gt.0) then
    write(mess,'(a,f10.2,a)') 'Using a Gaussian beam of size ',&
      & bsize/pi*180*3600,'"'
    call map_message(seve%i,rname,mess)
    call primary_gauss(bsize,nr,profile)
  endif
  !
end subroutine primary_radial
!
subroutine primary_mosaic(line,np,hprim,head,selected_fields, &
    & selected_fieldsize,doff,pos,bsize,error)
  use gkernel_types
  use gkernel_interfaces
  use gbl_message
  use imager_interfaces, only : map_message, primary_radial
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER  
  !
  ! Compute primary beam for a Mosaic
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line    ! Command line
  integer, intent(in) :: np                   ! Number of fields
  integer, intent(in) :: selected_fields(np)  ! Field numbers 
  integer, intent(in) :: selected_fieldsize   ! Number of selected fields
  type(gildas), intent(inout) :: hprim    ! Primary beam header
  type(gildas), intent(in)    :: head     ! Dirty image header
  real, intent(in) :: doff(:,:)           ! Offset of pointings
  real(8), intent(in) :: pos(2)           ! Center offset
  real, intent(inout) :: bsize            ! Size of primary beam
  logical, intent(out) :: error           ! Error flag
  !
  character(len=*), parameter :: rname='MOSAIC_PRIMARY_BEAMS'
  logical :: OLD=.FALSE.
  !
  integer :: nx, ny, nb
  real, allocatable :: factorx(:)
  real :: factory
  real(8), allocatable :: profile(:,:)
  real :: beamx, beamy
  real :: offx, offy
  integer :: ifield, jfield, j, ier
  integer :: ix, iy, ib, ir, nr
  real(8) :: x, y, r, dr
  !
  call imager_tree('PRIMARY_MOSAIC')
  !
  nx = hprim%gil%dim(2)     ! X size
  ny = hprim%gil%dim(3)     ! Y size
  nb = hprim%gil%dim(4)     ! Number of frequency planes
  ! np = hprim%gil%dim(1)   ! Number of fields
  !
  error = .false.
  call sic_get_logi('OLD',old,error)
  error = .false.
  !
  IF (OLD) THEN
    allocate(factorx(nx),stat=ier)  
    !
    ! Create the primary beams, lobe and weight images
    !$OMP PARALLEL DEFAULT(none)  &
    !$OMP   & SHARED(np,hprim,selected_fields,error,nb,nx,ny, bsize)  & 
    !$OMP   & SHARED(selected_fieldsize, doff, pos) &
    !$OMP   & PRIVATE(ifield, jfield, ib, j, beamx, beamy, offx, offy, factorx, factory) 
    ! 
    !$OMP DO
    do jfield = 1,np
      ifield = jfield
      if (selected_fieldsize.ne.0) ifield = selected_fields(jfield)    
      if (sic_ctrlc()) then
        error = .true.
        call map_message(seve%e,rname,'Aborted by user')
        cycle
      endif
      ! bsize is Frequency dependent TBC
      !
      do ib=1,nb
        beamx = hprim%gil%inc(2)/bsize*2.0*sqrt(log(2.0))
        beamy = hprim%gil%inc(3)/bsize*2.0*sqrt(log(2.0))
        offx = (doff(1,ifield)+pos(1))/hprim%gil%inc(2)
        offy = (doff(2,ifield)+pos(2))/hprim%gil%inc(3)
        !
        do j=1,nx
          factorx(j) = exp(-((j-hprim%gil%ref(2)-offx)*beamx)**2)
        enddo
        do j=1,ny
          factory = exp(-((j-hprim%gil%ref(3)-offy)*beamy)**2)
          hprim%r4d(jfield,:,j,ib) = factorx(:) * factory
        enddo
      enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
    deallocate (factorx)
    if (error) return
    !
  ELSE
    !
    ! Create the primary beams, lobe and weight images
    !
    ! Primary beam and BSIZE can be Frequency dependent in size,
    ! but not in shape, see below.
    bsize = 0 ! Make sure this is initialized
    call primary_radial(line,bsize,head,nr,profile,error)
    if (error) return
    dr = profile(2,1) - profile(1,1) ! Increment
    !
    !$OMP PARALLEL DEFAULT(none)  &
    !$OMP   & SHARED(dr, profile, nr) &
    !$OMP   & PRIVATE(ir,ix,iy, r,x,y) &
    !$OMP   & SHARED(np,hprim,selected_fields,error,nb,nx,ny, bsize)  & 
    !$OMP   & SHARED(selected_fieldsize, doff, pos) &
    !$OMP   & PRIVATE(ifield, jfield, ib, j, offx, offy) 
    ! 
    !$OMP DO
    do jfield = 1,np
      ifield = jfield
      if (selected_fieldsize.ne.0) ifield = selected_fields(jfield)    
      if (sic_ctrlc()) then
        error = .true.
        call map_message(seve%e,rname,'Aborted by user')
        cycle
      endif
      !
      ! Frequency dependent Beam could be handled at this stage by scaling DR
      ! inversely with Frequency
      do ib=1,nb
        offx = (doff(1,ifield)+pos(1))/hprim%gil%inc(2)
        offy = (doff(2,ifield)+pos(2))/hprim%gil%inc(3)
        !
        do iy=1,ny 
          y = (iy-hprim%gil%ref(3)-offy)*hprim%gil%inc(3) + hprim%gil%val(3)
          do ix=1,nx
            x = (ix-hprim%gil%ref(2)-offx)*hprim%gil%inc(2) + hprim%gil%val(2)
            r = sqrt(x**2+y**2)
            !
            ! Locate by interpolation
            ir = int(r/dr)+1
            if (ir.lt.nr) then
              hprim%r4d(jfield,ix,iy,ib) = ((profile(ir+1,1)-r)*profile(ir,2) + (r-profile(ir,1))*profile(ir+1,2) ) /dr
            endif
          enddo ! IX
        enddo   ! IY
      enddo ! Frequency dependent beam
    enddo   ! Field
    !$OMP END DO
    !$OMP END PARALLEL
    deallocate (profile)
    if (error) return
    !
  ENDIF
end subroutine primary_mosaic
!
subroutine primary_gauss(bsize,nr,rb)
  !-----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Compute the primary beam as a Gaussian of FWHM bsize
  !-----------------------------------------------------------------------
  real, intent(in) :: bsize
  integer, intent(in) :: nr
  real(8), intent(inout) :: rb(nr,2)
  !
  real(8) :: beam
  integer :: ir
  !
  beam = bsize/(2.0*sqrt(log(2.0)))
  do ir=1,nr
    rb(ir,2) = exp(-(rb(ir,1)/beam)**2)
  enddo
end subroutine primary_gauss
!  
subroutine primary_alma(h,nr,rb)
  use gkernel_types
  !-----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER 
  !   Compute the primary beam of an ALMA 12-m antenna,
  !   as a Uniform Disk of 10.4m with 0.7 m blockage
  !-----------------------------------------------------------------------
  type(gildas), intent(in) :: h
  integer, intent(in) :: nr
  real(8), intent(inout) :: rb(nr,2)
  !
  real(kind=4), save :: d_out=10.4  ! Diameter
  real(kind=4), save :: d_block=0.7 ! Secondary blockage
  real(8) :: par(3),r,lambda,f
  integer :: ir
  !
  !
  ! The Fourier Transform of a uniform disk of radius 1 is 
  !   J1(2 pi Rho) / Rho 
  ! where J1 is the Bessel function of first kind, order 1
  ! 
  ! Using the similarity theorem, with f(x) the function and F(m) its
  ! Fourier transform
  ! F(f(ax))(m) = 1/a F(m/a)
  !
  ! So for the uniform disk, since the FT is defined by
  !   f(u,v) = int_x int_y f(x,y) exp(-2i pi (u.x + v.y) dx dy
  ! with u = X/lambda 
  !  We change to u' = X/R so a factor u=a u' with a=R/lambda
  !   f(u',v') is thus a uniform disk of radius 1
  ! so the Fourier Transform ("Airy disk") of our big disk is
  !
  !   B(x,y) = lambda/R J1(2 pi r lambda/R) /  (r lambda/R) 
  !   B(x,y) = J1 (2 pi r lambda/R) / r
  ! with R = a_out/2. Same with R = a_block/2 for the blockage
  !
  ! Thus our primary beam is
  !   P(x,y) = (J1( 4 pi r lambda/a_out) - J1( 4 pi r lambda/a_block)) / r 
  ! We use the U_RING code that implements this and has been tested
  ! in the UV_FITC task.
  !
  lambda = 299792458D0/(h%gil%freq*1D6)
  par(1) = 1.0
  par(2) = d_out/2/lambda 
  par(3) = d_block/2/lambda
  !
  do ir=1,nr
    r = rb(ir,1)
    call u_ring(par,r,f)
    rb(ir,2) = f*f   ! Convert into Power Beam
  enddo
  !
contains
!
subroutine u_ring(par,q,f)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Ring "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [3] of the 3 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	Not Here - DF	: Vector [3] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(3)                !
  real*8 :: q                     !
  real*8 :: f                     !
  ! Global
  real*8, external :: mth_bessj1
  ! Local
  real*8 :: a, b, c
  real*8 :: pi, j1, jb1, jc1, den
  real*8 :: ksi, ksib, ksic
  !
  pi = 3.1415926535897932384626d0
  a = par(1)
  b = par(2)
  c = par(3)
  !
  ! I. Compute F(Q).
  ! ----------------
  if (q .eq. 0.) then
    j1 = 1.
  else
    ksi = 2. * pi * q
    ksib = b * ksi
    ksic = c * ksi
    den = ksib**2 - ksic**2
    jb1 = mth_bessj1(ksib)
    jc1 = mth_bessj1(ksic)
    j1 = 2. * (ksib*jb1 - ksic*jc1) / den
  endif
  f = a * j1
end subroutine u_ring
!
end subroutine primary_alma
!
function mth_bessj1 (x)
  !-------------------------------------------------------------------
  ! Compute Bessel function J1
  !-------------------------------------------------------------------
  real*8 :: mth_bessj1            !
  real*8 :: x                     !
  ! Local
  real*8 :: ax,z,xx
  real*8 :: y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6
  real*8 :: s1,s2,s3,s4,s5,s6
  data r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,   &
   &      242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0/
  data s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,   &
   &      18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
  data p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,   &
   &      .2457520174d-5,-.240337019d-6/
  data q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,   &
   &      .8449199096d-5,-.88228987d-6,.105787412d-6/
  !
  if (abs(x).lt.8.d0) then
    y=x*x
    mth_bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))   &
   &        /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
  else
    ax=abs(x)
    z=8.d0/ax
    y=z*z
    xx=ax-2.356194491d0
    mth_bessj1=sqrt(.636619772d0/ax)*(cos(xx)*   &
   &        (p1+y*(p2+y*(p3+y*(p4+y*p5))))   &
   &        -z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))   &
   &        *sign(1.d0,x)
  endif
end function mth_bessj1
!
subroutine get_bsize(h,rname,line,bsize,error,otrunc,btrunc)
  use image_def
  use gkernel_interfaces
  use imager_interfaces, except_this => get_bsize
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !
  ! IMAGER     Support for command
  !   PRIMARY [BeamSize] [/TRUNCATE Percent]
  !   UV_MAP  [BeamSize] [/TRUNCATE Percent]
  !
  ! Return the primary beam size in radian
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: h          ! UV data header
  character(len=*), intent(in) :: rname  ! Caller name
  character(len=*), intent(in) :: line   ! Command line
  real(4), intent(inout) :: bsize        ! Beam size in radian
  logical, intent(out) :: error          ! Error flag
  integer, optional, intent(in) ::  otrunc   ! Truncation option number
  real(4), optional, intent(out) :: btrunc   ! Truncation level [0,1]
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  character(len=60) :: chain
  integer :: nc
  real :: beamsize
  !
  ! Verify if any data
  !
  if (h%loca%size.eq.0) then
    call map_message(seve%e,rname,'No input data')
    error = .true.
    return
  endif
  !
  beamsize = telescope_beam (rname,h)
  !
  if (beamsize.eq.0) then
    if (bsize.eq.0) then
      nc = len_trim(rname)+6
      chain(1:nc) = ' '
      chain(nc:) = 'Use command "SPECIFY TELESCOPE Name" to add one'
      call map_message(seve%e,rname,'No primary beam from data') 
      call map_message(seve%r,rname,chain) 
      error = .true.
      return
    else
      beamsize = bsize 
    endif
  else if (bsize.eq.0) then
    bsize = beamsize
  endif
  !
  !!Print *,'Bsize ',bsize,' Beamsize ',beamsize
  !
  if (abs(beamsize-bsize).gt.0.02*bsize) then
    write(chain,'(A,F8.1,A)') 'Specified beam differs from value in data ', &
    & beamsize*180*3600/pi,'"'
    call map_message(seve%w,rname,chain)
  endif
  write(chain,'(A,F8.1,A)') 'Primary beam ',bsize*180*3600/pi,'"'
  call map_message(seve%i,rname,chain)
  !
  ! Truncation
  if (present(btrunc)) then
    if (present(otrunc)) then
      call sic_r4(line,otrunc,1,btrunc,.true.,error)
      if (error) return
      btrunc = btrunc*0.01
    endif
  else if (present(otrunc)) then
    call map_message(seve%f,rname,'Programming Error: OTRUNC present, but not BTRUNC')
    error = .true.
  endif
  !
end subroutine get_bsize

