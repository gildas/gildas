subroutine mulgau(data,nx,ny,bmaj,bmin,pa,scale,xinc,yinc,isign,smax)
  !----------------------------------------------------------------------
  ! @ public-mandatory
  !
  ! IMAGER
  ! 	Multiply or Divide (according to isign) the TF of an image by the 
  ! TF of a convolving gaussian function. BMAJ and BMIN are the
  !	widths of the original gaussian. PA is the position angle of major
  !	axis (from north towards east), in Degrees
  !----------------------------------------------------------------------
  integer, intent(in) :: nx,ny    ! X,Y size
  real, intent(in) :: bmaj        ! Major axis in Radian
  real, intent(in) :: bmin        ! Minor axis in Radian 
  real, intent(in) :: pa          ! Position Angle in Degree
  real, intent(in) :: scale       ! Flux scale factor
  real, intent(in) :: xinc        ! X pixel size
  real, intent(in) :: yinc        ! Y pixel size
  complex, intent(inout) :: data(nx,ny)  ! TF of Data to be smoothed
  integer, intent(in) :: isign    ! Mulitply (< 0) or Divide (>0)
  real, intent(in), optional :: smax ! Maximum distance for correction
  !
  real(8), parameter :: pi=3.141592653589793d0
  real(4), parameter :: eps=1.e-7
  integer i,j,nx1,nx2
  real amaj,amin,fact,cx,cy,sx,sy
  real seuil, gain
  logical norot,rot90
  real(8) rpa, dpa
  !!logical :: debug=.false.
  !
  dpa = pa         ! in degrees
  norot = ( abs(mod(dpa,180.d0)).le.eps)
  rot90 = ( abs(mod(dpa,180.d0)-90.d0).le.eps)
  amaj = bmaj*pi/(2.*sqrt(log(2.)))
  amin = bmin*pi/(2.*sqrt(log(2.)))
  rpa = pa*pi/180.d0   ! in radians here...  
  !
  gain = scale
  !!Print *,'major & minor ',bmaj*3600*180/pi,bmin*3600*180/pi
  !!Print *,'position angle ',pa*180/pi
  !!Print *,'isign ',isign
  !
  if (isign.gt.0) then
    seuil = 10.0
  else
    seuil = 80.0
  endif
  if (present(smax)) seuil = smax
  !
  cx = cos(rpa)/nx*amin
  cy = cos(rpa)/ny*amaj
  sx = sin(rpa)/nx*amaj
  sy = sin(rpa)/ny*amin
  !!Print *,'CX CY SX SY user ',cx,cy,sx,sy
  !
  ! convert map units to pixels
  cx = cx / xinc
  cy = cy / yinc
  sx = sx / xinc
  sy = sy / yinc
  !!Print *,'CX CY SX SY pixels ',cx,cy,sx,sy
  nx2 = nx/2
  nx1 = nx2+1
  !
  ! optimised code for position angle 0 degrees
  if (norot) then
    do j=1,ny/2
      do i=1,nx2
        fact = (float(j-1)*cy)**2 + (float(i-1)*cx)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(j-1)*cy)**2 + (float(i-nx-1)*cx)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(j-ny-1)*cy)**2 + (float(i-1)*cx)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(j-ny-1)*cy)**2 + (float(i-nx-1)*cx)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    !
    ! optimised code for position angle 90 degrees
  elseif (rot90) then
    do j=1,ny/2
      do i=1,nx2
        fact = (float(i-1)*sx)**2 +(float(j-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx)**2 + (float(j-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(i-1)*sx)**2 + (float(j-ny-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx)**2 + (float(j-ny-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    !
    ! general case of a rotated elliptical gaussian
  else
    do j=1,ny/2
      do i=1,nx2
        fact = (float(i-1)*sx + float(j-1)*cy)**2 + &
            (-float(i-1)*cx + float(j-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx + float(j-1)*cy)**2 + &
            ( -float(i-nx-1)*cx + float(j-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(i-1)*sx + float(j-ny-1)*cy)**2 + &
            ( -float(i-1)*cx + float(j-ny-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx & 
          + float(j-ny-1)*cy)**2 + &
          ( -float(i-nx-1)*cx + float(j-ny-1)*sy)**2
        if (fact.lt.seuil) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact*gain
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
  endif
end subroutine mulgau
!
