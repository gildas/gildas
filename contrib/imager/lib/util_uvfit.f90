subroutine uv_fibeam (name,freq,uv,nu,nvis,wcol,majo,mino,pa,error)
  use gildas_def
  use gkernel_interfaces
  use imager_interfaces, except_this => uv_fibeam
  use mod_fitbeam
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !
  ! MAPPING
  !   Find beam parameters for deconvolution
  !   Fit an elliptical gaussian clean beam to UV data weight
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: name          ! Caller name
  real(8), intent(in) :: freq                   ! Observation frequency
  integer, intent(in) :: nu                     ! Visibility size 
  integer, intent(in) :: nvis                   ! Number of visibilities
  integer, intent(in) :: wcol                   ! Weight column  
  real, intent(in) :: uv(nu,nvis)               ! Visibilities
  real, intent(inout) :: majo                   ! Major axis
  real, intent(inout) :: mino                   ! Minor axis
  real, intent(inout) :: pa                     ! Position Angle
  logical, intent(inout) :: error               ! Error flag
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  ! Local
  character(len=80) :: mess
  integer :: ml,ier,nc,jcol
  real :: dmaj,dmin,minor,uvmax
  logical :: liter
  !
  error = .false.
  !
  nc = (nu-7)/3
  if (wcol.eq.0) then
    jcol = (nc+2)/3
  else
    jcol = min(wcol,nc)
  endif
  jcol = 7+3*jcol
  !  
  liter = .false.
  !
  ! Load data  into work array
  ml = nvis
  allocate (ip_values(ml), ip_coords(2,ml), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',ier
    call map_message(seve%e,name,mess)
    error = .true.
    return
  endif
  ip_coords(1:2,:) = uv(1:2,:)
  !
  ! Find initial guesses
  ip_values(:) = ip_coords(1,:)**2 + ip_coords(2,:)**2
  uvmax = sqrt(maxval(ip_values))
  ip_values(:) = uv(jcol,:)
  !
  ! Assume Half power size of Weight distribution is 1/3rd of uvmax
  !
  spar(2:3) = 0.0
  spar(1) = maxval(ip_values)
  spar(4) = uvmax/3.
  spar(5) = uvmax/5.
  spar(6) = 90.0
  par = spar
  err(1) = spar(1)/10.0 
  err(2:3) = 0
  err(2) = 0
  err(4) = spar(4)/5.0
  err(5) = err(4)
  err(6) = 50.0
  !
  Print *,'Par ',par
  Print *,'Err ',err
  !
  nv = nvis !!! 
  liter = .false.
  call fit2d (min2d,liter,error)
  !
  Print *,'Par ',par
  Print *,'Err ',err
  !
  spar = par
  liter = .true.
  call fit2d (min2d,liter,error)
  !
  Print *,'Par ',par
  Print *,'Err ',err
  !
  ! Go from UV distance to Beam sizes...
  if (par(4).le.par(5)) then
    majo = 1.0/(freq*f_to_k*par(4))
    mino = 1.0/(freq*f_to_k*par(5))
    dmaj = err(4)/par(4)*majo
    dmin = err(5)/par(5)*mino
    pa = 90.0-par(6) ! Pas clair...
  else
    majo = 1.0/(freq*f_to_k*par(5))
    mino = 1.0/(freq*f_to_k*par(4))
    dmaj = err(5)/par(5)*majo
    dmin = err(4)/par(4)*mino
    pa = -par(6)
  endif
  deallocate (ip_values, ip_coords, stat=ier)
  !
  if (name.eq.' ') return
  !
  minor = mino*3600*180/pi
  if (minor.gt.10.) then
    write(mess,100) 'Beam is ',majo*3600*180/pi,mino*3600*180/pi,pa
    call map_message(seve%i,name,mess)
    write(mess,101) 'Errors (',dmaj*3600*180/pi,dmin*3600*180/pi,err(6)
    call map_message(seve%i,name,mess)
  else if (minor.gt.0.2) then
    write(mess,110) 'Beam is ',majo*3600*180/pi,mino*3600*180/pi,pa
    call map_message(seve%i,name,mess)
    write(mess,111) 'Errors (',dmaj*3600*180/pi,dmin*3600*180/pi,err(6)
    call map_message(seve%i,name,mess)
  else
    write(mess,120) 'Beam is ',majo*3600*180/pi,mino*3600*180/pi,pa
    call map_message(seve%i,name,mess)
    write(mess,121) 'Errors (',dmaj*3600*180/pi,dmin*3600*180/pi,err(6)
    call map_message(seve%i,name,mess)
  endif
  !
  100   format (a,f8.2,'" by ',f8.1,'" at PA ',f8.1,' deg.')
  101   format (a,f8.2,')   (',f8.1,')      (',f8.1,')    ')
  110   format (a,f8.2,'" by ',f8.2,'" at PA ',f8.2,' deg.')
  111   format (a,f8.2,')   (',f8.2,')      (',f8.2,')    ')
  120   format (a,f8.2,'" by ',f8.3,'" at PA ',f8.3,' deg.')
  121   format (a,f8.2,')   (',f8.3,')      (',f8.3,')    ')
end subroutine uv_fibeam
!
