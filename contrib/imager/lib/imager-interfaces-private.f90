module imager_interfaces_private
  interface
    subroutine mrc_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !   Implementation of Multi Resolution Clean
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine mrc_clean
  end interface
  !
  interface
    subroutine multi_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !   Implementation of Multi Scale Clean
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine multi_clean
  end interface
  !
  interface
    subroutine sdi_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !   Implementation of Steer Dewdney Ito Clean
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine sdi_clean
  end interface
  !
  interface
    subroutine hogbom_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !   Implementation of Hogbom Clean
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine hogbom_clean
  end interface
  !
  interface
    subroutine clark_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !   Implementation of Barry Clark Clean
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine clark_clean
  end interface
  !
  interface
    subroutine sub_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use clean_types
      use gbl_message
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Internal routine
      !     Implementation of all standard CLEAN deconvolution algorithms,
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine sub_clean
  end interface
  !
  interface
    subroutine cct_truncate(hcct,dcct)
      use image_def
      use clean_def
      !----------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER Internal routine
      !     Truncate the CCT variable
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: hcct ! CCT Header
      real, intent(in) :: dcct(:,:,:)     ! CCT data
    end subroutine cct_truncate
  end interface
  !
  interface
    subroutine clean_data(error)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !--------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Prepare Clean parameters
      !   Prepare Clean arrays: CLEAN or SKY, RESIDUAL  
      !--------------------------------------------------------
      logical, intent(out) :: error
    end subroutine clean_data
  end interface
  !
  interface
    subroutine write_image (line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_format
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   WRITE Type File [/RANGE Start End Kind] [/TRIM]
      !       [/APPEND] [/REPLACE]
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      logical, intent(out) :: error         ! Error return code
    end subroutine write_image
  end interface
  !
  interface
    subroutine sub_write_image (name,atype,ntype,nc,quiet,error)
      use clean_def
      use clean_arrays
      use clean_default
      use clean_support
      use clean_types
      use uvfit_data
      use gkernel_types
      use gbl_format
      use gbl_message
      use iso_c_binding
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   WRITE Type File [/RANGE Start End Kind] [/TRIM]
      !
      ! Dispatch the writing operation to specific subroutines depending
      ! on the Type of data to be written
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: name    ! File name 
      character(len=*), intent(in) :: atype   ! TYPE of data
      integer, intent(in) :: nc(2)            ! Channel range
      integer, intent(in) :: ntype            ! Type of file
      logical, intent(in) :: quiet            ! Do not return error if set
      logical, intent(out) :: error           ! Error return code
    end subroutine sub_write_image
  end interface
  !
  interface
    subroutine sub_setmodif (file,opti,nc)
      use gkernel_types
      use clean_types
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Update the Read / Write optimization status
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: file  ! Filename
      type(readop_t), intent(inout) :: opti ! Status of corresponding buffer
      integer, intent(in) :: nc(2)          ! Range to be read
    end subroutine sub_setmodif
  end interface
  !
  interface
    subroutine write_cct(file,error)
      use clean_def
      use clean_arrays
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   WRITE CCT File [/RANGE Start End Kind]
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: file  ! Input file name
      logical, intent(out) :: error         ! Error return code
    end subroutine write_cct
  end interface
  !
  interface
    subroutine sub_write_uvdata(file,nc,uvin,duv,error, mv, ivis)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Write a subset of the loaded UV data
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: file  ! Input file name
      integer, intent(in) :: nc(2)          ! Channel range
      type(gildas), intent(in) :: uvin      ! UV data header
      real, intent(in) :: duv(:,:)          ! Visibilities
      logical, intent(out) :: error         ! Logical error flag
      !
      integer, intent(in), optional :: mv       ! Number of visibilities
      integer, intent(in), optional :: ivis(:)  ! Visibility pointers ?
    end subroutine sub_write_uvdata
  end interface
  !
  interface
    subroutine sub_extract_block(out, dout, in, din, nvisi, nc)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Extract a subset block of UV data
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: out                 ! Output UV header
      type(gildas), intent(in) :: in                  ! Input UV header
      integer(kind=index_length) :: nvisi             ! Number of visibilities
      real, intent(in) :: din(in%gil%dim(1),nvisi)    ! Input visibilities
      real, intent(out) :: dout(out%gil%dim(1),nvisi) ! Output visibilities
      integer, intent(in) :: nc(2)                    ! Channel range
    end subroutine sub_extract_block
  end interface
  !
  interface
    subroutine sub_replace_image (name,ntype,nc,error)
      use clean_def
      use clean_arrays
      use clean_support
      use clean_types
      use gbl_format
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   WRITE Type File /REPLACE 
      !
      ! Dispatch the writing operation to specific subroutines depending
      ! on the Type of data to be written
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: name    ! Short file name
      integer, intent(in) :: nc(2)            ! Channel range
      integer, intent(in) :: ntype            ! Type of data
      logical, intent(out) :: error           ! Error return flag
    end subroutine sub_replace_image
  end interface
  !
  interface
    subroutine sub_replace(name,file,data,head,error)
      use image_def
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   Support routine for WRITE /REPLACE
      !-------------------------------------------------------
      character(len=*), intent(in) :: name    ! Name of buffer
      character(len=*), intent(in) :: file    ! File name
      type(gildas), intent(inout) :: head     ! Header of buffer
      real(kind=4), intent(in) :: data(:,:,:) ! Data
      logical, intent(inout) :: error         ! Error return flag
    end subroutine sub_replace
  end interface
  !
  interface
    subroutine sub_append_image (name,ntype,nc,error)
      use clean_def
      use clean_arrays
      use clean_support
      use clean_types
      use gbl_format
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   WRITE Type File /APPEND  
      !
      ! Dispatch the writing operation to specific subroutines depending
      ! on the Type of data to be written
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: name    ! Short file name
      integer, intent(in) :: nc(2)            ! Channel range
      integer, intent(in) :: ntype            ! Type of buffer
      logical, intent(out) :: error           ! Error return flag
    end subroutine sub_append_image
  end interface
  !
  interface
    subroutine sub_append(name,file,data,head,error)
      use image_def
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   Support routine for WRITE /APPEND
      !-------------------------------------------------------
      character(len=*), intent(in) :: name    ! Name of buffer
      character(len=*), intent(in) :: file    ! File name
      type(gildas), intent(inout) :: head     ! Header of buffer
      real(kind=4), intent(in) :: data(:,:,:) ! Data
      logical, intent(inout) :: error         ! Error return flag
    end subroutine sub_append
  end interface
  !
  interface
    subroutine sub_write_moments(name,error)
      use moment_arrays
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Support routine for WRITE MOMENTS 
      !-------------------------------------------------------
      character(len=*), intent(in) :: name  ! Short file name
      logical, intent(inout) :: error       ! Error return flag
    end subroutine sub_write_moments
  end interface
  !
  interface
    subroutine sub_write_flux(name,error)
      use moment_arrays
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Support routine for WRITE FLUX  
      !-------------------------------------------------------
      character(len=*), intent(in) :: name  ! Short file name
      logical, intent(inout) :: error       ! Error return flag
    end subroutine sub_write_flux
  end interface
  !
  interface
    subroutine sub_write_uvset(file, nfields, nc, huv, duv, trim_uv, trim_uvany, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Write the Selected UV data set
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: file
      integer, intent(in) :: nc(2)
      type(gildas), intent(in) :: huv
      real, intent(in) :: duv(:,:)
      integer, intent(in) :: nfields
      logical, intent(in) :: trim_uv, trim_uvany
      logical, intent(out) :: error
    end subroutine sub_write_uvset
  end interface
  !
  interface
    subroutine alma_clean (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use clean_types
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER:  Internal routine
      !     Implementation of a Clean Method for Multi-Array
      !     Mosaic observations
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine alma_clean
  end interface
  !
  interface
    subroutine beam_jvm(hbeam,dbeam,jvm,circle,print,error)
      use image_def
      !-------------------------------------------------------------
      ! IMAGER
      !   @ private
      !
      !   Support for FIT /JVM commands
      !
      !   Evaluate the Jorsater - van Moorsel residual scaling factor.
      !   from the dirty beam shape.
      !-------------------------------------------------------------
      type(gildas), intent(in) :: hbeam     ! Beam header
      real, intent(in) :: dbeam(:,:)        ! 2-D beam
      real, intent(out) :: jvm              ! Scaling factor
      logical, intent(in) :: circle         ! Method of circular averaging
      logical, intent(in) :: print          ! Save results for display
      logical, intent(out) :: error         ! Error return
    end subroutine beam_jvm
  end interface
  !
  interface
    subroutine clean_beam (line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use clean_beams
      use gbl_message
      !-------------------------------------------------------------
      ! IMAGER
      !   @ private
      !
      ! Code for COMMAND
      !   FIT [First Last] [PLANE|CHANNEL|FIELD First Last] 
      !       [PLANE|CHANNEL|FIELD First Last] [/JVM_FACTOR  [NoCircle]]
      !       [/THRESHOLD Value]
      !-------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine clean_beam
  end interface
  !
  interface
    subroutine map_beams(task,map_beam,huv,nx,ny,nb,nc)
      use gbl_message
      use image_def
      use clean_default
      !-------------------------------------------------------------
      ! @ private
      !   
      ! IMAGER
      !   Define dirty beams number according to user specification
      !   and UV table characteristics. The control code indicates
      !   the goal.
      !     -1    Automatic guess to avoid resolution effects
      !      0    One beam for all, if possible
      !     >0    Number of adjacents channels sharing same beam
      !  Code -2 was for debugging only
      !
      !-------------------------------------------------------------
      character(len=*), intent(in) :: task    ! Caller's name
      integer, intent(out) :: map_beam        ! Number of channels per beam 
      type(gildas), intent(in) :: huv         ! UV Header
      integer, intent(in) :: nx               ! X size
      integer, intent(in) :: ny               ! Y size
      integer, intent(in) :: nc               ! Number of channels
      integer, intent(out) :: nb              ! Number of beams
    end subroutine map_beams
  end interface
  !
  interface
    subroutine channel_range(rname,fchan,lchan,icode)
      use gkernel_types
      use gbl_message
      use clean_beams
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      !   If UV_CHECK BEAM has been used, restrict the range to the 
      ! most significant (to avoid issues with bad edge recognition)
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname ! Caller name
      integer, intent(inout) :: fchan       ! First channel
      integer, intent(inout) :: lchan       ! Last channel
      integer, intent(inout) :: icode       ! Error code
    end subroutine channel_range
  end interface
  !
  interface
    subroutine define_beams(task,map_beam,nx,ny,huv,mcol,nb,error)
      use image_def
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !   @ private
      !
      ! Define the number of beams according to the BEAM_STEP variable
      ! and number of data channels.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task  ! Caller name
      integer, intent(in) :: nx             ! X size
      integer, intent(in) :: ny             ! Y size
      integer, intent(inout) :: map_beam    ! Desired number of channels per beams
      type(gildas), intent(in) :: huv       ! UV data
      integer, intent(inout) :: mcol(2)     ! Channel range
      integer, intent(out) :: nb            ! Number of beams
      logical, intent(out) :: error         ! Error code
    end subroutine define_beams
  end interface
  !
  interface
    subroutine verify_beam_ranges(task,mbeam,mcol,icode)
      use gbl_message
      use clean_beams 
      !---------------------------------------------------------------------
      ! IMAGER
      !   @ private
      !
      ! Verify selected channel range is consistent with required number
      ! of beams.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task  ! Calling command
      integer, intent(in) :: mbeam          ! Number of planes per beam
      integer, intent(inout) :: mcol(2)     ! Channel range
      integer, intent(out) :: icode         ! Error code
    end subroutine verify_beam_ranges
  end interface
  !
  interface
    subroutine uv_calibrate(line,error)
      use gildas_def
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for 
      !     CALIBRATE\APPLY [Mode [Gain]] 
      !         [/FILE UvIN [UvOut]] [/FLAG] Threshold
      ! 
      ! Apply gains from the GAIN data set to the UV data set.
      ! If no available solution, keep the data as it was or flag it,
      ! according to the /FLAG option.
      ! Work on current UV data if no /FILE option.
      ! Work in place if /FILE is specified, and no UvOut argument is given
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine uv_calibrate
  end interface
  !
  interface
    subroutine coutput(iv_first,iv_last,cflag,lc)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for commands APPLY and SELFCAL APPLY
      !   Printout list of flagged visibilities 
      !---------------------------------------------------------------------  
      integer, intent(inout) :: iv_first, iv_last
      integer, intent(inout) :: lc
      character(len=*), intent(inout) :: cflag
    end subroutine coutput  
  end interface
  !
  interface
    subroutine display_buffer(comm,line,error)
      use clean_arrays
      use clean_def
      use clean_types
      use clean_support
      use clean_default
      use uvfit_data
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for command
      !     SHOW Name
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: comm  ! Command (SHOW or VIEW)
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Logical error flag
    end subroutine display_buffer
  end interface
  !
  interface
    subroutine display_buffer_sub(comm,ntype,dtype,line,o_args,error)
      use clean_types
      use clean_arrays
      use clean_default
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for command
      !     SHOW or VIEW Name
      !
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: comm
      integer, intent(in)  :: ntype ! Buffer type
      character(len=*), intent(in) :: dtype
      character(len=*), intent(in) :: line
      integer, intent(in) :: o_args ! Where are the arguments ?
      logical, intent(out) :: error ! Logical error flag
    end subroutine display_buffer_sub
  end interface
  !
  interface
    subroutine create_fields(error)
      use clean_arrays
      use clean_types
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER internal routine for command
      !     Create the FIELDS array (transposed version of the PRIMARY array)
      !----------------------------------------------------------------------
      logical, intent(out) :: error
    end subroutine create_fields
  end interface
  !
  interface
    subroutine cct_integrate(rname,hcct,dcct,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  Support for SHOW and VIEW CCT
      !   Compute the cumulative flux in Clean Components, and give access
      !   to it through the CCT_NCOMP and CCT_FLUX arrays.
      !---------------------------------------------------------------------  
      character(len=*), intent(in) :: rname   ! Command name
      type(gildas), intent(in) :: hcct        ! Header
      real, intent(in) :: dcct(:,:,:)         ! CCT array
      logical, intent(inout) :: error         ! Error flag
    end subroutine cct_integrate
  end interface
  !
  interface
    subroutine display_set_comm(line,comm,error)
      use clean_default
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! 
      ! IMAGER / VIEWER
      !      Support for the SET ANGLE_UNIT command
      ! Fall back to GREG1\SET command for any other argument
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      character(len=*), intent(in) :: comm
      logical :: error
    end subroutine display_set_comm
  end interface
  !
  interface
    subroutine dofft_quick_debug (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick_debug
  end interface
  !
  interface
    subroutine dofft_quick_para (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick_para
  end interface
  !
  interface
    subroutine dofft_parallel_v_pseudo_out (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_pseudo_out
  end interface
  !
  interface
    subroutine dofft_parallel_v_true_out (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true_out
  end interface
  !
  interface
    subroutine dofft_parallel_v_pseudo (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_pseudo
  end interface
  !
  interface
    subroutine dofft_parallel_v_true (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true
  end interface
  !
  interface
    subroutine dofft_parallel_v_true2 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true2
  end interface
  !
  interface
    subroutine dofft_parallel_y (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      use gbl_message
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_y
  end interface
  !
  interface
    subroutine dofft_parallel_x (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      use gbl_message
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_x
  end interface
  !
  interface
    subroutine major_cycle90 (rname,method,head,   &
         &    clean,beam,resid,nx,ny,tfbeam,fcomp,   &
         &    wcl,mcl,ixbeam,iybeam,ixpatch,iypatch,bgain,   &
         &    box, wfft, tcc, list, nl, np, primary, weight,       &
         &    major_plot, next_flux)
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !_
      ! IMAGER
      !   Major cycle loop according to B.Clark idea
      !----------------------------------------------------------------------
      external :: major_plot                      ! Major cycle Display
      external :: next_flux                       ! Cumulative flux display
      !
      character(len=*), intent(in) :: rname       ! Calling command
      type (clean_par), intent(inout) :: method   ! Method parameters
      type (gildas), intent(in)  :: head          ! Data header
      !
      integer, intent(in) :: nx                   ! X size
      integer, intent(in) :: ny                   ! Y size
      integer, intent(in) :: np                   ! Number of pointings
      integer, intent(in) :: mcl                  ! Maximum number of clean components
      real, intent(inout) :: clean(nx,ny)         ! Clean map
      real, intent(inout) :: resid(nx,ny)         ! Residual map
      real, intent(in) ::    beam(nx,ny,np)       ! Dirty beams (per pointing)
      real, intent(in) ::    tfbeam(nx,ny,np)     ! T.F. du beam
      complex, intent(inout) :: fcomp(nx,ny)      ! T.F. du vecteur modification
      real, intent(in) :: bgain                   ! Maximum sidelobe level
      integer, intent(in) :: ixbeam, iybeam       ! Beam maximum position
      integer, intent(in) :: ixpatch, iypatch     ! Beam patch radius
      integer, intent(in) :: box(4)               ! Cleaning box
      real, intent(inout) :: wfft(*)              ! Work space for FFT
      type(cct_par), intent(inout) :: tcc(method%m_iter) ! Clean components array
      type(cct_par), intent(inout) :: wcl(mcl)    ! Work space for Clean components
      integer, intent(inout) :: list(nx*ny)       ! list of searchable pixels
      integer, intent(inout) :: nl   ! List size
      !
      real, intent(in) :: primary(np,nx,ny)       ! Primary beams
      real, intent(in) :: weight (nx,ny)          ! Flat field response
    end subroutine major_cycle90
  end interface
  !
  interface
    subroutine minor_cycle90 (method, wcl, ncl,           &
         &    beam,nx,ny,ixbeam,iybeam,ixpatch,iypatch,   &
         &    clarkmin,limite,converge,   &
         &    tcc, np, primary, weight, wtrun, cum, pflux, next_flux )
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Internal routine
      !   B.Clark minor cycles
      !   Deconvolve as in standard clean a list of NCL points
      !   selected in the map until the residuals is less than CLARKMIN
      !----------------------------------------------------------------------
      external :: next_flux                       ! Cumulative flux display
      type (clean_par), intent(inout) :: method   ! Method parameters
      integer, intent(in) :: np                   ! Number of fields in mosaic
      integer, intent(in) :: ncl                  ! Number of pixels selected
      integer, intent(in) :: nx,ny,ixbeam,iybeam  ! Size and cente pixel of beam
      integer, intent(in) :: ixpatch,iypatch      ! Used size of beam
      real, intent(in) :: beam(nx,ny,np)          ! Dirty beam
      type(cct_par), intent(inout) :: wcl(*)      ! Clean components
      real, intent(in) :: clarkmin                ! Stopping criterium for minor cycles
      real, intent(in) :: limite                  ! Clean Stopping criterium
      logical, intent(out) :: converge            ! Convergence indicator
      type (cct_par), intent(out) :: tcc(*)       ! Effective clean components
      real, intent(in) :: primary(np,nx,ny)       ! Primary beams of mosaics
      real, intent(in) :: weight(nx,ny)           ! Effective weights on sky
      real, intent(in) :: wtrun                   ! Threshold of primary beam
      real, intent(inout) :: cum                  ! Cumulative flux
      logical, intent(in) :: pflux                ! Plot cumulative flux
      !
      ! Local
      real gain                    ! CLEAN gain 
      logical goon                 ! Continue after convergence
      integer kcl                  ! Current Clean component
      integer nomax, nomin         ! Clean component of Max and Min 
      real rmax, rmin, sign, cdif  ! current Max and Min 
      real worry, xfac             ! Conservative and speedup factor
      integer kiter
      integer :: dimcum            ! Flux convergence control
      real, allocatable :: oldcum(:)
      real f, bmax
      integer n,ier,i,jiter
      logical abor
      character(len=20) comm
      !
    end subroutine minor_cycle90
  end interface
  !
  interface
    subroutine major_sdi90 (rname,method,head,clean,beam,resid,nx,ny,   &
         &    tfbeam,fcomp,wcl,mcl,ixbeam,iybeam,ixpatch,iypatch,bgain,   &
         &    box, wfft, comp, list, nl, np, primary, weight, &
         &    major_plot )
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Major cycle loop according to Steer Dewdney and Ito idea
      !----------------------------------------------------------------------
      external :: major_plot                      ! Plotting routine
      !
      character(len=*), intent(in) :: rname       ! Caller name
      type (clean_par), intent(inout) :: method   ! Deconvolution method parameterss
      type (gildas), intent(in)  :: head          ! Data header
      !
      integer, intent(in) :: np                   ! Number of fields
      integer, intent(in) :: nx                   ! X size
      integer, intent(in) :: ny                   ! Y size
      integer, intent(in) :: mcl                  ! Maximum number of clean components
      real, intent(inout) :: clean(nx,ny)         ! Clean map
      real, intent(inout) :: resid(nx,ny)         ! Residual map
      real, intent(in) ::    beam(nx,ny,np)       ! Dirty beams (per field)
      real, intent(in) ::    tfbeam(nx,ny,np)     ! T.F. du beam
      complex, intent(inout) :: fcomp(nx,ny)      ! T.F. du vecteur modification
      type(cct_par), intent(inout) :: wcl(mcl)    ! Work space for Clean components
      real, intent(in) :: bgain                   ! Maximum sidelobe level
      integer, intent(in) :: ixbeam, iybeam       ! Beam maximum position
      integer, intent(in) :: ixpatch, iypatch     ! Beam patch radius
      integer, intent(in) :: box(4)               ! Cleaning box
      real, intent(inout) :: wfft(*)              ! Work space for FFT
      integer, intent(inout) :: list(nx*ny)       ! list of searchable pixels
      integer, intent(inout) :: nl                ! List size
      real, intent(inout) :: comp(nx,ny)          ! Clean components array
      !
      real, intent(in) :: primary(np,nx,ny)       ! Primary beams
      real, intent(in) :: weight (nx,ny)
      !
      real    maxc,minc,maxabs     ! max and min of data, absolute max value
      integer imax,jmax,imin,jmin  ! coordinates of the Max and Min pixels
      real    borne                ! Fraction of initial data
      real    limite               ! Minimal intensity retained
      real flux                    ! Total clean flux density
      integer ncl                  ! Number of selected data points
      logical fini                 ! Stopping criterium 
      logical converge             ! Stop by flux convergence
      integer k
      real factor                  ! Scaling factor
      character(len=message_length) :: chain
      !
      type (cct_par) :: tcc(1)     ! Dummy argument for Major_PLOT
      !
      real :: sign, cdif
      integer :: dimcum, jiter
      real, allocatable :: oldcum(:)
      !
    end subroutine major_sdi90
  end interface
  !
  interface
    subroutine normal (fcomp,tfbeam,nx,ny, wcl,ncl,wfft,factor)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Support routine for SDI
      ! Subtract last major cycle components from residual map.
      !----------------------------------------------------------------------
      integer, intent(in) :: nx             ! X size
      integer, intent(in) :: ny             ! Y size
      integer, intent(in) :: ncl            ! Number of clean components
      type(cct_par), intent(in) :: wcl(ncl) ! Clean components
      real, intent(in) :: tfbeam(nx,ny)     ! Beam TF
      complex, intent(out) :: fcomp(nx,ny)  ! TF of clean components
      real, intent(inout) :: wfft(*)        ! Work array
      real, intent(out) :: factor           ! Max of clean
    end subroutine normal
  end interface
  !
  interface
    subroutine scalec(wcl,ncl,f,s,compon,nx,ny)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   Support routine for SDI
      ! Subtract last major cycle components from residual map.
      !----------------------------------------------------------------------
      integer, intent(in) :: nx             ! X size
      integer, intent(in) :: ny             ! Y size
      integer, intent(in) :: ncl            ! Number of clean components
      type(cct_par), intent(inout) :: wcl(ncl)     ! Clean comp.
      real, intent(in) :: f                 ! Gain factor
      real, intent(inout) :: s              ! Cumulative flux
      real, intent(inout) :: compon(nx,ny)  ! Cumulative clean component
    end subroutine scalec
  end interface
  !
  interface
    subroutine hogbom_cycle90 (rname,pflux, beam,mx,my,resid,nx,ny,               &
         &    ixbeam,iybeam, box, fracres, absres, miter, piter, niter,          &
         &    gainloop, converge, cct, msk, list, nl, np, primary, weight, wtrun, &
         &    cflux, jcode, next_flux)
      use clean_def
      use clean_default
      use gbl_message
      use omp_control
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Support routine for HOGBOM
      !     Deconvolve map into residual map and source list
      !-----------------------------------------------------------------------
      external :: next_flux                       ! Cumulative flux display
      character(len=*), intent(in) :: rname       ! Calling command
      logical, intent(in) :: pflux                ! Plot cumulative flux ?
      integer, intent(in) :: mx                   ! X size of beam
      integer, intent(in) :: my                   ! Y size of beam
      integer, intent(in) :: nx                   ! X size of image
      integer, intent(in) :: ny                   ! Y size of image
      integer, intent(in) :: np                   ! Number of fields
      real, intent(in) :: beam(mx,my,np)          ! Primary beam(s)
      real, intent(inout) :: resid(nx,ny)         ! residual image
      real, intent(in) :: fracres                 ! Fractional residual
      real, intent(in) :: absres                  ! Absolute residual
      integer, intent(in) :: miter                ! Maximum number of clean components
      integer, intent(in) :: ixbeam, iybeam       ! Beam maximum position
      integer, intent(in) :: box(4)               ! Cleaning box
      real, intent(in) :: gainloop                ! Clean loop gain
      integer, intent(in) :: converge             ! Convergence iteration number 
      integer, intent(out) :: niter               ! Iterations
      integer, intent(in) :: piter                ! Positive Iterations
      logical, intent(in) :: msk(nx,ny)           ! Mask for clean search
      integer, intent(in) :: nl                   ! Size of search list
      integer, intent(in) :: list(nl)             ! Search list
      real, intent(in) :: primary(np,nx,ny)       ! Primary beams
      real, intent(in) :: weight(nx,ny)           ! Weight function
      real, intent(in) :: wtrun                   ! Safety threshold on primary beams
      type (cct_par), intent(out) :: cct(miter)   ! Clean Component Table
      integer, intent(out) :: jcode               ! Stopping code
      real, intent(out) :: cflux                  ! Cleaned Flux
    end subroutine hogbom_cycle90
  end interface
  !
  interface
    subroutine many_beams_para (rname,map,huv,hbeam,hdirty,   &
         &    nx,ny,nu,nv,uvdata,   &
         &    r_weight, w_v, do_weig,    &
         &    wcol,mcol,sblock,cpu0,error,uvmax,jfield,abort,cthread)
      use clean_def
      use image_def
      use gbl_message
      !$ use omp_lib
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute a map from a CLIC UV Sorted Table
      !   by Gridding and Fast Fourier Transform, with
      !   a different beam per channel.
      !
      ! Input :
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      ! Output :
      ! a beam image
      ! a VLM cube
      ! Work space :
      ! a  VLM complex Fourier cube (first V value is for beam)
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(inout) :: map  ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      integer, intent(in) :: nx   ! X size
      integer, intent(in) :: ny   ! Y size
      integer, intent(in) :: nu   ! Size of a visibilities
      integer, intent(in) :: nv   ! Number of visibilities
      real, intent(in) :: uvdata(nu,nv)
      real, intent(inout), target :: r_weight(nv)    ! Weight of visibilities
      real, intent(inout) :: w_v(nv)         ! V values
      logical, intent(inout) :: do_weig
      integer, intent(in) :: jfield      ! Field number (for mosaic)
      !
      real, intent(inout) :: cpu0        ! CPU
      real, intent(inout) :: uvmax       ! Maximum baseline
      integer, intent(inout) :: sblock   ! Blocking factor
      integer, intent(inout) :: wcol     ! Weight channel
      integer, intent(inout) :: mcol(2)  ! First and last channel
      logical, intent(inout) :: error, abort
      integer, intent(in)    :: cthread    ! Calling Thread number
    end subroutine many_beams_para
  end interface
  !
  interface
    subroutine map_resample_comm(line,comm,error)
      use clean_def
      use clean_types
      use clean_arrays
      use gbl_message 
      use phys_const 
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      !   Resample in velocity the Images, or Compress them 
      !   Support for commands
      !     MAP_RESAMPLE WhichOne NC [Ref Val Inc] [Result] [/LIKE Mold]
      !     MAP_COMPRESS WhichOne NC [Start RType] [Result]
      !     MAP_INTEGRATE WhichOne Rmin Rmax RType [Result]
      !     MAP_COMPRESS WhichOne NC [Result]
      !
      ! WhichOne    Name of map to be processed
      !             Allowed values are DIRTY, CLEAN, SKY, * 
      !             or any 3-D Image (File or SIC variable)
      ! Result      Indicates the name of the Result file 
      !             or SIC Variable (not .)
      !   Result is mandatory if WhichOne is not a recognized name.
      !   Result cannot be present if WhichOne is *.
      !   When not present, Result = WhichOne.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Calling command
      logical, intent(out) :: error         ! Error flag
    end subroutine map_resample_comm
  end interface
  !
  interface
    subroutine all_inter (n,ic1,icn,x,xdim,xinc,xref,xval, &
      & y,ydim,yinc,yref,yval,bval,blanked,iwork,rwork)
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      ! 
      ! IMAGER
      !	   Performs the linear interpolation/integration along last
      !    axis. All other axes compressed to 1.
      !
      !    Derived from CLASS Internal routine.
      !---------------------------------------------------------------------
      integer(kind=index_length) :: n     ! Map size
      integer(kind=index_length) :: ic1   ! First channel               
      integer(kind=index_length) :: icn   ! Last channel                 !
      integer(kind=index_length) :: xdim  ! Number of Output channels
      real :: x(n,xdim)                   ! Output Cube
      real(8) :: xinc                     ! Channel conversion formula
      real(8) :: xref                     !
      real(8) :: xval                     !
      integer(kind=index_length) :: ydim  ! Number of Input channels
      real :: y(n,ydim)                   !
      real(8) :: yinc                     !
      real(8) :: yref                     !
      real(8) :: yval                     !
      real(4) :: bval                     ! Blanking value
      logical :: blanked 
      integer :: iwork(2,xdim)            ! Input Boundaries for each Output channel 
      real :: rwork(4,xdim)               ! Weights for each Output channel
    end subroutine all_inter
  end interface
  !
  interface
    subroutine map_resample_buffer(name,dres,code_save,comm, &
      & hin,hout,hres,din, mrange,dv,error)
      use gkernel_types
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      !   IMAGER
      !     Support for commands MAP_COMPRESS, MAP_INTEGRATE and MAP_RESAMPLE
      !   Apply the specified Command to the specified internal buffer
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name  ! Buffer name
      integer, intent(in) :: code_save      ! Buffer code
      character(len=*), intent(in) :: comm  ! Command name
      real, allocatable, intent(inout) :: dres(:,:,:) ! Destination data
      type(gildas), intent(in) :: hin       ! Input header  
      type(gildas), intent(inout) :: hout   ! Output header
      type(gildas), intent(inout) :: hres   ! Destination header
      real, intent(out) :: din(:,:,:)       ! Work buffer
      integer(kind=index_length), intent(in) :: mrange(2)  ! Channel range
      real, intent(in) :: dv                ! Channel width
      logical, intent(out) :: error         ! Error flag
    end subroutine map_resample_buffer
  end interface
  !
  interface
    subroutine one_beam (rname,map,huv,hbeam,hdirty,   &
         &    nx,ny,nu,nv,uvdata,   &
         &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
         &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
      use clean_def
      use image_def
      use gbl_message
      use clean_default
      use omp_control
      !------------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Compute a map from a CLIC UV Sorted Table
      !   by Gridding and Fast Fourier Transform, with
      !   one single beam for all channels.
      !
      ! Input :
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      ! Output :
      ! a beam image
      ! a VLM cube
      ! Work space :
      ! a  VLM complex Fourier cube (first V value is for beam)
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(inout) :: map  ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      integer, intent(in) :: nx   ! X size
      integer, intent(in) :: ny   ! Y size
      integer, intent(in) :: nu   ! Size of a visibilities
      integer, intent(in) :: nv   ! Number of visibilities
      real, intent(inout) :: uvdata(nu,nv)
      real, intent(inout) :: w_mapu(nx)      ! U grid coordinates
      real, intent(inout) :: w_mapv(ny)      ! V grid coordinates
      real, intent(inout) :: w_grid(nx,ny)   ! Gridding space
      real, intent(inout) :: w_weight(nv)    ! Weight of visibilities
      real, intent(inout) :: w_v(nv)         ! V values
      logical, intent(inout) :: do_weig
      !
      real, intent(inout) :: wfft(*)     ! Work space
      real, intent(inout) :: cpu0        ! CPU
      real, intent(inout) :: uvmax       ! Maximum baseline
      integer, intent(inout) :: sblock   ! Blocking factor
      integer, intent(inout) :: wcol     ! Weight channel
      integer, intent(inout) :: mcol(2)  ! First and last channel
      logical, intent(inout) :: error
    end subroutine one_beam
  end interface
  !
  interface
    subroutine check_order(visi,np,nv,sorted)
      !----------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Check if visibilites are sorted.
      !   Chksuv does a similar job, but using V values and an index.
      !----------------------------------------------------------
      integer, intent(in) :: np       ! Size of a visibility
      integer, intent(in) :: nv       ! Number of visibilities
      real, intent(in) :: visi(np,nv) ! Visibilities
      logical, intent(out) :: sorted
    end subroutine check_order
  end interface
  !
  interface
    subroutine clean_mfs(line,error)
      use image_def
      use clean_def
      use clean_default
      use clean_arrays
      use clean_types
      !
      !-----------------------------------------------
      ! @ private
      !
      !-----------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine clean_mfs
  end interface
  !
  interface
    subroutine sub_clean_mfs(ixoff,iyoff,error)
      use image_def
      use clean_def
      use clean_arrays
      use gbl_message
      !-----------------------------------------------
      ! @ private
      !
      !-----------------------------------------------
      integer, intent(inout) :: ixoff, iyoff
      logical, intent(out) :: error
    end subroutine sub_clean_mfs
  end interface
  !
  interface
    subroutine com_modify(line,error)
      use phys_const
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   Support for command
      !   SPECIFY KeyWord Value [/FOR ImageVariable] 
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine com_modify
  end interface
  !
  interface
    subroutine modify_blanking(desc,bval,error)
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      ! 
      !   IMAGER
      !     Support routine for SPECIFY BLANKING
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(inout) :: desc
      real, intent(in) :: bval
      logical, intent(out) :: error
    end subroutine modify_blanking
  end interface
  !
  interface
    subroutine gdf_setfreqs(rname,desc,huv,error)
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   Support for command
      !   SPECIFY FREQUENCIES Array [/FOR ImageVariable] 
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(sic_descriptor_t), intent(in) :: desc
      type(gildas), intent(inout) :: huv
      logical, intent(inout) :: error
    end subroutine gdf_setfreqs
  end interface
  !
  interface
    subroutine gdf_setteles(head,chain,value,error)
      use image_def
      use gbl_message
      ! @ private
      type(gildas), intent(inout) :: head
      character(len=*), intent(in) :: chain
      real(8), intent(in) :: value(*)
      logical, intent(inout) :: error
    end subroutine gdf_setteles
  end interface
  !
  interface
    subroutine sub_mosaic(name,error)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER      MOSAIC ON|OFF
      !             Activates or desactivates the mosaic mode
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name
      logical, intent(out) :: error
    end subroutine sub_mosaic
  end interface
  !
  interface
    subroutine mosaic_uvmap(task,line,error)
      !$ use omp_lib
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use clean_beams 
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for command UV_MAP
      !   Compute a Mosaic from a UV Table with pointing offset information.
      !
      ! Input :
      !     a UV table with pointing offset information
      !
      ! Ouput
      !   NX NY are the image sizes
      !   NC is the number of channels
      !   NF is the number of different frequencies
      !   NP is the number of pointing centers
      !   NB is the number of frequency-dependent beam cubes
      !
      ! In the original task, we had obtained:
      ! 'NAME'.LMV  a 3-d cube containing the uniform noise
      !     combined mosaic, i.e. the sum of the product
      !     of the fields by the primary beam. (NX,NY,NC)
      ! 'NAME'.LOBE the primary beams pseudo-cube (NP,NX,NY,NB)
      ! 'NAME'.WEIGHT the sum of the square of the primary beams (NX,NY,NB)
      ! 'NAME'.BEAM a 4-d cube where each cube contains the synthesised
      !     beam for one field (NX,NY,NB,NP)
      !
      ! Now, we obtain
      !   HDirty      a 3-d cube containing the uniform noise
      !              combined mosaic, i.e. the sum of the product
      !               of the fields by the primary beam. (NX,NY,NC)
      !   HBeam       a 4-d cube where each cube contains the synthesised
      !               beam for one field (NX,NY,NB,NP)
      !   HPrim       the primary beams pseudo-cube (NP,NX,NY,NB)
      !
      !   lweight     the sum of the square of the primary beams (NX,NY,NB)
      !               only for NB=1 (local Weight array, actually unused)
      !
      ! All images have the same X,Y sizes
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task ! Caller (MOSAIC)
      character(len=*), intent(in) :: line ! Command line
      logical, intent(out) :: error
    end subroutine mosaic_uvmap
  end interface
  !
  interface
    subroutine mosaic_headers (rname,map,huv,hbeam,hdirty,hprim,nb,nf,mcol)
      use clean_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define the image headers for a Mosaic
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(in) :: map     ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      type (gildas), intent(inout) :: hprim   ! Primary beam data set
      integer, intent(in) :: nb   ! Number of beams per field
      integer, intent(in) :: nf   ! Number of fields
      integer, intent(in) :: mcol(2)  ! First and last channel
    end subroutine mosaic_headers
  end interface
  !
  interface
    subroutine mosaic_sort (error,sorted,shift,newabs,uvmax,uvmin, &
      & ixoff,iyoff,nf,doff,voff)
      use clean_def
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Sort the input Mosaic UV table
      !---------------------------------------------------------------------
      logical, intent(inout) :: sorted         ! Is table sorted ?
      logical, intent(inout) :: shift          ! Do we shift phase center ?
      logical, intent(out) :: error
      real(kind=8), intent(inout) :: newabs(3) ! New phase center and PA
      real, intent(out) :: uvmin               ! Min baseline
      real, intent(out) :: uvmax               ! Max baseline
      integer, intent(in) :: ixoff, iyoff      ! Offset pointers
      integer, intent(inout) :: nf             ! Number of fields
      real, intent(inout) :: doff(:,:)         ! Field offsets
      integer, intent(inout) :: voff(:)        ! Field visibility offsets
    end subroutine mosaic_sort
  end interface
  !
  interface
    subroutine check_order_mosaic(visi,np,nv,ixoff,iyoff,sorted)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Check if visibilites are sorted.
      !   Chksuv does a similar job, but using V values and an index.
      !---------------------------------------------------------------------
      integer, intent(in) :: np       ! Size of a visibility
      integer, intent(in) :: nv       ! Number of visibilities
      real, intent(in) :: visi(np,nv) ! Visibilities
      integer, intent(in) :: ixoff    ! X pointing column
      integer, intent(in) :: iyoff    ! Y pointing column
      logical, intent(out) :: sorted
    end subroutine check_order_mosaic
  end interface
  !
  interface
    subroutine select_fields(rname,line,o_field,mp,np,fields,error)
      use clean_arrays
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Select a list of fields from a Mosaic
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(in) :: line
      integer, intent(in) :: o_field
      integer, intent(in) :: mp ! Number of fields in UV data
      integer, intent(out) :: np ! Number of fields selected 
      logical, intent(inout) :: error
      integer, intent(inout), allocatable :: fields(:)
    end subroutine select_fields
  end interface
  !
  interface
    subroutine mosaic_restore(line,error)
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !-------------------------------------int--------------------------------
      ! @ private
      !
      ! Restore (like in UV_RESTORE) a mosaic image
      !
      ! Required input
      !   the CCT table
      !   the RESIDUAL image (obtained by imaging the UV table
      !     obtained by UV_RESIDUAL)
      !   the PRIMARY beams
      ! Output 
      !   the restored SKY image
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine mosaic_restore
  end interface
  !
  interface
    subroutine sky_as_clean(rname,error)
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Create the SKY brightness like the CLEAN image
      !---------------------------------------------------------------------  
      character(len=*), intent(in) :: rname
      logical, intent(inout) :: error
    end subroutine sky_as_clean
  end interface
  !
  interface
    subroutine mx_clean (map,huv,uvdata,uvp,uvv,   &
         &    method,hdirty,hbeam,hclean,hresid,hprim,   &
         &    w_grid,w_mapu,w_mapv,p_cct, dcct, smask, slist,   &
         &    sblock, cpu0, uvmax)
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! GILDAS: CLEAN Internal routine
      ! Implementation of MX CLEAN deconvolution algorithm.
      !----------------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (uvmap_par), intent(inout) :: map
      type (gildas), intent(inout) :: huv
      type (gildas), intent(inout) :: hdirty
      type (gildas), intent(inout) :: hbeam
      type (gildas), intent(inout) :: hclean
      type (gildas), intent(inout) :: hresid
      type (gildas), intent(inout) :: hprim
      real, intent(inout) :: dcct(3,hclean%gil%dim(3),*)
      logical, intent(inout) :: smask(:,:)
      integer, intent(in) :: slist(*)
      integer, intent(in) :: sblock
      real, intent(inout) :: uvdata(huv%gil%dim(1),huv%gil%dim(2))
      real, intent(in) :: uvp(*)
      real, intent(in) :: uvv(*)
      real, intent(inout) :: w_grid(*)
      real, intent(inout) :: w_mapu(*)
      real, intent(inout) :: w_mapv(*)
      real, intent(in) :: cpu0
      real, intent(inout) :: uvmax
      type (cct_par), intent(out) :: p_cct(method%m_iter)
    end subroutine mx_clean
  end interface
  !
  interface
    subroutine loadxy (method,huv,head,mapx,nx,mapy,ny)
      use clean_def
      use image_def
      !--------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Load x,y coordinates
      !--------------------------------------------------
      type (clean_par), intent(in) :: method
      type (gildas), intent(in) :: huv,head
      integer, intent(in) :: nx
      integer, intent(in) :: ny
      real, intent(out) :: mapx(nx)
      real, intent(out) :: mapy(ny)
    end subroutine loadxy
  end interface
  !
  interface
    subroutine mx_major_cycle90 (map,uvdata,nu,nv,   &
         &    w_weight,w_v,   &
         &    method,head,   &
         &    clean,beam,resid,nx,ny,nb,   &
         &    wcl,mcl,tcc,ncct,   &
         &    list, nl, nf, primary, weight,   &
         &    grid, mapu, mapv, mapx, mapy,   &
         &    wfft, cpu0, uvmax)
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for MX
      !   Major cycle loop according to B.Clark idea
      !   Treat only one "plane/channel" at a time
      !   Plane is specified by method%iplane
      !----------------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (uvmap_par), intent(in) :: map
      type (gildas), intent(inout) :: head
      !
      integer, intent(in) :: nu                 ! Visibility size
      integer, intent(in) :: nv                 ! Number of visibilities
      integer, intent(in) :: nb                 ! Number of beams
      real, intent(inout) :: uvdata(nu,nv)      ! Visibilities (iterated residuals)
      real, intent(in) ::  w_weight(nv)         ! Weight array
      real, intent(in) ::  w_v(nv)              ! V values
      real, intent(in) ::  uvmax                ! Max baseline
      !
      integer, intent(in) ::  nx,ny             ! X,Y size
      real, intent(in) ::  mapx(nx),mapy(ny)    ! X,Y coordinates
      real, intent(out) ::  mapu(*),mapv(*)      ! U,V coordinates
      real, intent(in) ::  grid(nx,ny)          ! Grid correction
      !
      real, intent(inout) ::  clean(nx,ny)      ! Clean image
      real, intent(inout) ::  resid(nx,ny)      ! Residuals
      real, intent(in) ::     beam(nx,ny,nb)    ! Synthesized beams
      integer, intent(in)  ::  nl               ! Search list size
      integer, intent(in)  ::  mcl              ! Maximum number of clean components
      integer, intent(out) ::  ncct             ! Number of clean components / plane
      real, intent(inout)  ::  wfft(*)          ! Work space for FFT
      type (cct_par), intent(out) :: tcc(method%m_iter) ! Clean components array
      integer, intent(in)  ::  list(nx*ny)      ! List of valid pixels
      !
      integer, intent(in) ::  nf                ! Number of fields
      real, intent(inout) ::  primary(nf,nx,ny) ! Primary beams
      real, intent(inout) ::  weight (nx,ny)    ! Flat field
      !
      type(cct_par), intent(inout) :: wcl(mcl)  ! Work array for Clean Components
      !
      real    maxc,minc,maxabs     ! max and min of data, absolute max value
      integer imax,jmax,imin,jmin  ! coordinates of the Max and Min pixels
      real    borne                ! Fraction of initial data
      real    limite               ! Minimal intensity retained
      real    clarkl               ! Clark worry limit
      real flux                    ! Total clean flux density
      integer ncl                  ! Number of selected data points
      logical fini                 ! Stopping criterium 
      logical converge             ! Stop by flux convergence
      integer k, kcl
      character(len=message_length) :: chain
      character(len=32) :: str
      !
      complex, allocatable :: tfgrid(:,:,:)
      complex, allocatable :: ftbeam(:,:)
      !! real, allocatable :: my_resid(:,:)
      !
      real cpu1,cpu0
    end subroutine mx_major_cycle90
  end interface
  !
  interface
    subroutine mx_uvsub90 (nx,ny,mapx,mapy,   &
         &    wcl,ncl,nu,nv,visi,ip)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for MX
      !   Subtract last major cycle components from UV table.
      !   Treats only one channel.
      !----------------------------------------------------------------------
      integer, intent(in) ::  nx,ny             ! X,Y size
      real, intent(in) ::  mapx(nx),mapy(ny)    ! X,Y coordinates
      integer, intent(in) :: nu                 ! Visibility size
      integer, intent(in) :: nv                 ! Number of visibilities
      integer, intent(in) :: ncl                ! Number of clean components
      real, intent(inout) :: visi(nu,nv)        ! Visibilities
      integer, intent(in) :: ip                 ! Current "plane/channel"
      type(cct_par), intent(inout) :: wcl(ncl)  ! Clean comp. value
    end subroutine mx_uvsub90
  end interface
  !
  interface
    subroutine one_beam_para (rname,map,huv,hbeam,hdirty,   &
         &    nx,ny,nu,nv,uvdata,   &
         &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
         &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
      use clean_def
      use image_def
      use gbl_message
      !$ use omp_lib
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute a map from a CLIC UV Sorted Table
      !   by Gridding and Fast Fourier Transform, with
      !   one single beam for all channels.
      !
      ! Input :
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      ! Output :
      ! a beam image
      ! a VLM cube
      ! Work space :
      ! a  VLM complex Fourier cube (first V value is for beam)
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(inout) :: map  ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      integer, intent(in) :: nx   ! X size
      integer, intent(in) :: ny   ! Y size
      integer, intent(in) :: nu   ! Size of a visibilities
      integer, intent(in) :: nv   ! Number of visibilities
      real, intent(inout) :: uvdata(nu,nv)
      real, intent(inout) :: w_mapu(nx)      ! U grid coordinates
      real, intent(inout) :: w_mapv(ny)      ! V grid coordinates
      real, intent(inout) :: w_grid(nx,ny)   ! Gridding space
      real, intent(inout) :: w_weight(nv)    ! Weight of visibilities
      real, intent(inout) :: w_v(nv)         ! V values
      logical, intent(inout) :: do_weig
      !
      real, intent(inout) :: wfft(*)     ! Work space
      real, intent(inout) :: cpu0        ! CPU
      real, intent(inout) :: uvmax       ! Maximum baseline
      integer, intent(inout) :: sblock   ! Blocking factor
      integer, intent(inout) :: wcol     ! Weight channel
      integer, intent(inout) :: mcol(2)  ! First and last channel
      logical, intent(inout) :: error
    end subroutine one_beam_para
  end interface
  !
  interface
    subroutine one_beam_serial (rname,map,huv,hbeam,hdirty,   &
         &    nx,ny,nu,nv,uvdata,   &
         &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
         &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
      use clean_def
      use image_def
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute a map from a CLIC UV Sorted Table
      !   by Gridding and Fast Fourier Transform, with
      !   one single beam for all channels.
      !
      ! Input :
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      ! Output :
      ! a beam image
      ! a VLM cube
      ! Work space :
      ! a  VLM complex Fourier cube (first V value is for beam)
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(inout) :: map  ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      integer, intent(in) :: nx   ! X size
      integer, intent(in) :: ny   ! Y size
      integer, intent(in) :: nu   ! Size of a visibilities
      integer, intent(in) :: nv   ! Number of visibilities
      real, intent(inout) :: uvdata(nu,nv)
      real, intent(inout) :: w_mapu(nx)      ! U grid coordinates
      real, intent(inout) :: w_mapv(ny)      ! V grid coordinates
      real, intent(inout) :: w_grid(nx,ny)   ! Gridding space
      real, intent(inout) :: w_weight(nv)    ! Weight of visibilities
      real, intent(inout) :: w_v(nv)         ! V values
      logical, intent(inout) :: do_weig
      !
      real, intent(inout) :: wfft(*)     ! Work space
      real, intent(inout) :: cpu0        ! CPU
      real, intent(inout) :: uvmax       ! Maximum baseline
      integer, intent(inout) :: sblock   ! Blocking factor
      integer, intent(inout) :: wcol     ! Weight channel
      integer, intent(inout) :: mcol(2)  ! First and last channel
      logical, intent(inout) :: error
    end subroutine one_beam_serial
  end interface
  !
  interface
    subroutine view_load_comm(line,error)
      use clean_default
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! VIEWER
      !
      !   Support routine for command
      !   DISPLAY\LOAD File [[/RANGE Start End Type]
      !   [/FREQUENCY RestFreqMHz] and [/PLANE Start End]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine view_load_comm
  end interface
  !
  interface
    subroutine read_image (line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !   READ Type File [/COMPACT] [/NOTRAIL] [/RANGE Start End Type]
      !   [/FREQUENCY RestFreqMHz] and [/PLANE Start End]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine read_image
  end interface
  !
  interface
    subroutine sub_read_image (name,ntype,drange,crange,compact,o_range,error, freq)
      use gkernel_types
      use clean_def
      use clean_arrays
      use clean_default
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support routine for command
      !   READ Type File [/COMPACT] [/FREQUENCY RestFreqMHz] [/RANGE Start End Type]
      !   [/NOTRAIL] and, for transposed data,  [/PLANE Start End]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name    ! Name of file
      integer, intent(in) :: ntype            ! Type of data
      real(8), intent(in) :: drange(2)        ! Range to be read
      character(len=*), intent(in) :: crange  ! Type of range
      logical, intent(in) :: compact          ! ACA buffer flag
      integer, intent(in) :: o_range          ! Number of Option /RANGE
      logical, intent(inout) :: error
      real(8), intent(in), optional :: freq   ! Desired rest frequency
    end subroutine sub_read_image
  end interface
  !
  interface
    subroutine sub_modified(atype,file,opti,nc,nochange)
      use gkernel_types
      use clean_types
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Check if the file of type "atype" needs to be read again.
      !---------------------------------------------------------------------
      integer, intent(in) :: atype          ! Code of type of file
      character(len=*), intent(in) :: file  ! Filename
      type(readop_t), intent(inout) :: opti ! Status of corresponding buffer
      integer, intent(in) :: nc(2)          ! Range to be read
      logical, intent(out) :: nochange      ! Change status
    end subroutine sub_modified
  end interface
  !
  interface
    subroutine map_read (head,fd,is_fits,out,nc,compact,error,count)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_beams
      use clean_default
      use uvfit_data
      use gbl_message
      !------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !  Read some type of input data.
      !------------------------------------------------------------------
      type (gildas), intent(inout) :: head    ! Header of input data
      type(gfits_hdesc_t), intent(in) :: fd   ! FITS descriptor 
      logical, intent(in) :: is_fits          ! Input FITS file
      character(len=*), intent(in) :: out     ! Desired data
      integer, intent(in) :: nc(2)            ! Channel range
      logical, intent(in) :: compact          ! Put in ACA space ?
      logical, intent(out) :: error
      integer, intent(in), optional :: count
    end subroutine map_read
  end interface
  !
  interface
    function map_range (nc,hin,hou)
      use image_def
      use gbl_message
      !---------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define channel range and set output Map header
      ! accordingly.
      !---------------------------------------------------
      logical :: map_range ! intent(out)
      integer, intent(in) :: nc(2)         ! Input channel range
      type (gildas), intent(inout) :: hin     ! Input header
      type (gildas), intent(inout) :: hou     ! Output header
    end function map_range
  end interface
  !
  interface
    subroutine out_range(rname,atype,drange,nc,head,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Utility routine to define a channel range from a
      !   Velocity, Frequency or Channel range
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname  ! Caller name
      character(len=*), intent(in) :: atype  ! Type of range
      real(8), intent(in) :: drange(2)     ! Input range
      integer, intent(out) :: nc(2)          ! Ouput channel number
      type(gildas), intent(in) :: head       ! Reference GILDAS data frame
      logical, intent(out) :: error          ! Error flag
    end subroutine out_range
  end interface
  !
  interface
    subroutine check_uvdata_type(huv,duv,map,error)
      use image_def
      use clean_def
      use gbl_message
      use gkernel_types
      !-------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Internal routine
      ! Check number and types of fields
      !-------------------------------------------------------
      type(gildas), intent(inout) :: huv
      real, intent(inout) :: duv(:,:)
      type(uvmap_par), intent(inout) :: map
      logical, intent(inout) :: error
    end subroutine check_uvdata_type
  end interface
  !
  interface
    subroutine define_fields(map,error)  
      use image_def
      use clean_def
      !---------------------------------------------------
      ! @ private
      !   IMAGER
      ! Define the FIELDS% structure for Mosaics
      !---------------------------------------------------
      type(uvmap_par), intent(in) :: map
      logical, intent(inout) :: error
    end subroutine define_fields
  end interface
  !
  interface
    function tab_range (nc,hin,hou,subset)
      use image_def
      !---------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define channel range and set output Table header
      ! accordingly. Valid for a CLASS Table of Spectra
      !---------------------------------------------------
      logical :: tab_range ! intent(out)
      integer, intent(in) :: nc(2)         ! Input channel range
      type (gildas), intent(inout) :: hin     ! Input header
      type (gildas), intent(inout) :: hou     ! Output header
      logical, intent(out) :: subset
    end function tab_range
  end interface
  !
  interface
    subroutine parse_range_opt(line,o_range,head,nc,error)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !    @ private
      ! Parse the /RANGE option in a general way, once the data Header is
      ! defined.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line    ! Command line
      integer, intent(in) :: o_range          ! /RANGE option number
      type(gildas), intent(in) :: head        ! Data header
      integer, intent(inout) :: nc(2)         ! Channel range
      logical, intent(out) :: error           ! Error flag
    end subroutine parse_range_opt
  end interface
  !
  interface
    subroutine sub_read_flux(name,error)
      use moment_arrays
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Support routine for WRITE FLUX  
      !-------------------------------------------------------
      character(len=*), intent(in) :: name  ! Short file name
      logical, intent(inout) :: error       ! Error return flag
    end subroutine sub_read_flux
  end interface
  !
  interface
    subroutine map_read_data(fd,isfits,head,rdata,error)
      use gkernel_types
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Read data in an internal buffer, from a GDF file
      !   or a FITS file, as indicated by "is_fits"
      !-------------------------------------------------------
      type(gfits_hdesc_t), intent(in) :: fd   ! FITS descriptor
      logical, intent(in) :: isfits           ! Is a FITS file ?
      type(gildas), intent(inout) :: head     ! GILDAS header
      real, intent(out) :: rdata(*)           ! Data array
      logical, intent(inout) :: error         ! Error flag
    end subroutine map_read_data
  end interface
  !
  interface
    subroutine get_more_header(isfits,file,head)
      use image_def
      use iso_c_binding
      use gbl_message
      use gkernel_types
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Update an internal Gildas Header by converting
      ! some possibly non-standard units. If the origin is
      ! a FITS file, update missing information from that found
      ! in specific FITS Table extensions.  The SIC command 
      ! DEFINE FITS is used for this purpose.
      !-------------------------------------------------------
      type(gildas), intent(inout) :: head   ! Header to be updated
      character(len=*), intent(in) :: file  ! Input file name
      logical, intent(in) :: isfits         ! Is it FITS ?
    end subroutine get_more_header
  end interface
  !
  interface
    subroutine get_beam_fits_value(r,key,error)
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Update a variable from a FITS  Beam%Col% column
      !   of specified name. Use SIC commands for this.
      !-------------------------------------------------------
      real, intent(inout) :: r            ! Variable
      logical, intent(inout) :: error   
      character(len=*), intent(in) :: key ! FITS Column name
    end subroutine get_beam_fits_value
  end interface
  !
  interface
    subroutine is_fitsfile(file,isfits,error)
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Test if a File is a valid FITS (image) file.
      !   No assumption is made about the file extension.
      !-------------------------------------------------------
      character(len=*), intent(in) :: file  ! File name
      logical, intent(out) :: isfits        ! Return value
      logical, intent(inout) :: error       ! Error flag
    end subroutine is_fitsfile
  end interface
  !
  interface
    subroutine map_read_uvdataset(fd,isfits,pcount,htmp,head,local_nc,rdata,error)
      use gkernel_types
      use gbl_message
      !-------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Read data in an internal buffer, from a GDF file
      !   or a FITS file, as indicated by "is_fits"
      !-------------------------------------------------------
      type(gfits_hdesc_t), intent(in) :: fd   ! FITS descriptor
      logical, intent(in) :: isfits           ! Is a FITS file ?
      integer, intent(in) :: pcount
      type(gildas), intent(inout) :: htmp
      type(gildas), intent(inout) :: head     ! GILDAS header
      integer, intent(in) :: local_nc(2)      ! Channel range
      real, intent(out) :: rdata(:,:)         ! Data array
      logical, intent(inout) :: error         ! Error flag
    end subroutine map_read_uvdataset
  end interface
  !
  interface
    subroutine map_read_uvfits(fits,astoke,error,eot,pcount,outarray)
      use image_def
      use gbl_format
      use gbl_message
      use image_def
    !!  use gio_dependencies_interfaces
    !!  use gio_interfaces, except_this=>read_uvfits
    !!  use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      !   Read all the input FITS UV table
      !---------------------------------------------------------------------
      type(gildas),    intent(inout) :: fits    !
      integer(kind=4), intent(in)    :: astoke  ! Stokes parameter
      logical,         intent(out)   :: error   ! Error flag
      logical,         intent(out)   :: eot     ! End ot Tape flag
      integer(kind=4), intent(in)    :: pcount  ! Daps count
      real, intent(out) :: outarray(:,:)
    end subroutine map_read_uvfits
  end interface
  !
  interface
    subroutine sg_map(task,line,do_cct,error)
      !$ use omp_lib
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      !     Support for commands
      !       UV_RESTORE  [/SPEED FFT | PRECISION | Number]
      !       UV_MAP
      !       UV_SELF
      ! and   MX (eventually, not coded yet)
      !
      ! Compute a map from a CLIC UV Sorted Table  by Gridding and Fast Fourier
      ! Transform, using adequate virtual memory space for optimisation. Also
      ! allows removal of Clean Components in the UV data and restoration of
      ! the clean image.
      !
      ! Input :
      !     a precessed UV table
      !     a list of Clean components
      ! Output :
      !     a precessed, rotated, shifted UV table, sorted in V,
      !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      !     a beam image or cube
      !     a LMV cube  (dirty or clean)
      !
      ! To be implemented
      !     Optionally, a Residual UV table
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task ! Caller (MX or UV_MAP or UV_RESTORE)
      character(len=*), intent(inout) :: line ! Command line
      logical, intent(in) :: do_cct        ! Remove Clean Components before
      logical, intent(out) :: error  
    end subroutine sg_map
  end interface
  !
  interface
    subroutine uvmap_and_restore (rname,map,huv,hbeam,hdirty,   &
         &    nx,ny,nu,nv,uvdata,r_weight, w_v, do_weig,    &
         &    rcol,wcol,mcol,sblock,cpu0,error,uvmax, &
         &    method, do_cct,hcct,hclean,do_slow,do_copy,sccou,smic)
      use clean_def
      use clean_default
      use clean_beams
      use image_def
      use gbl_message
      use omp_control
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute a map from a CLIC UV Sorted Table
      !   by Gridding and Fast Fourier Transform, with
      !   a different beam per channel.
      !
      ! Input :
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      !
      ! (optionally) a list of Clean components
      !
      ! Output :
      ! a beam image
      ! a DIRTY or RESIDUAL and CLEAN cube
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Calling Task name
      type (uvmap_par), intent(inout) :: map  ! Mapping parameters
      type (gildas), intent(inout) :: huv     ! UV data set
      type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
      type (gildas), intent(inout) :: hdirty  ! Dirty image data set
      integer, intent(in) :: nx               ! X size
      integer, intent(in) :: ny               ! Y size
      integer, intent(in) :: nu               ! Size of a visibility
      integer, intent(in) :: nv               ! Number of visibilities
      real, intent(inout) :: uvdata(nu,nv)
      real, intent(inout), target :: r_weight(nv)    ! Weight of visibilities
      real, intent(inout) :: w_v(nv)          ! V values
      logical, intent(inout) :: do_weig
      !
      real, intent(inout) :: cpu0            ! CPU
      real, intent(inout) :: uvmax           ! Maximum baseline
      integer, intent(inout) :: sblock       ! Blocking factor
      integer, intent(inout) :: rcol         ! Reference frequency channel
      integer, intent(inout) :: wcol         ! Weight channel
      integer, intent(inout) :: mcol(2)      ! First and last channel
      logical, intent(inout) :: error
      logical, intent(in) :: do_cct
      type (clean_par), intent(inout) :: method ! Cleaning Method
      type (gildas), intent(inout) :: hcct      ! Clean component data set
      type (gildas), intent(inout) :: hclean    ! Clean image data set
      logical, intent(in) :: do_slow
      logical, intent(in) :: do_copy
      real, intent(in), target :: sccou(:,:,:)  ! Transpose, compressed Clean Component List
      integer, intent(in), target :: smic(:)    ! Number of clean components per channel
    end subroutine uvmap_and_restore
  end interface
  !
  interface
    subroutine uv_removes_clean(nv,duv,ouv,nc,mic,dcct,freq,first,last)
      use image_def
      use gbl_message
      !$ use omp_lib
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_RESTORE
      !     Compute visibilities for a UV data set according to a
      !     set of Clean Components , and remove them from the original
      !     UV table.
      !       Semi-Slow version using interpolation from pre-tabulated
      !       Sin/Cos, which could still be optimized
      !-----------------------------------------------------------------
      integer, intent(in) :: nv         ! Number of visibilities
      real, intent(in) :: duv(:,:)      ! UV data set
      integer, intent(in) :: nc         ! Number of channels
      integer, intent(in) :: mic(:)     ! Number of Clean Components
      real, intent(out) :: ouv(:,:)     ! Extracted UV data set
      real, intent(in) :: dcct(:,:,:)   ! Clean component
      real(8), intent(in) :: freq       ! Apparent observing frequency
      integer, intent(in) :: first      ! First channel
      integer, intent(in) :: last       ! Last channel
    end subroutine uv_removes_clean
  end interface
  !
  interface
    subroutine uv_squeeze_clean(nc,ccin,ccou, mic, first, last)
      use image_def
      !$ use omp_lib
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_RESTORE
      !   Compact the component list by summing up all values at the
      !   same position in a range of channels.
      !   The output list has a transposed order
      !-----------------------------------------------------------------
      integer, intent(in) :: nc         ! Number of channels
      real, intent(in) :: ccin(:,:,:)   ! Initial Clean Component List
      real, intent(out) :: ccou(:,:,:)  ! Resulting list
      integer, intent(inout) :: mic(:)  ! Number of Clean component per channel
      integer, intent(in) :: first      ! First channel
      integer, intent(in) :: last       ! Last channel
    end subroutine uv_squeeze_clean
  end interface
  !
  interface
    subroutine uv_removef_clean(hcct,duv,ouv,nc,mic,dcct,freq,first,last)
      use image_def
      use gbl_message
      !$ use omp_lib
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_RESTORE
      !     Compute visibilities for a UV data set according to a
      !     set of Clean Components , and remove them from the original
      !     UV table
      !   This version is for tranpose CCT data (3,ncomp,nchannels)
      !   and uses an intermediate FFT for speed
      !-----------------------------------------------------------------
      type(gildas), intent(in) :: hcct  ! header of Clean Components Table
      integer, intent(in) :: nc         ! Number of channels
      integer, intent(in) :: mic(:)     ! Number of Clean Components
      real, intent(in) :: duv(:,:)      ! Input visibilities
      real, intent(out) :: ouv(:,:)     ! Output visibilities
      real, intent(in) :: dcct(:,:,:)   ! Clean components
      real(8), intent(in) :: freq       ! Apparent observing frequency
      integer, intent(in) :: first      ! First
      integer, intent(in) :: last       ! and last channel
    end subroutine uv_removef_clean
  end interface
  !
  interface
    subroutine uv_extract_clean(duv,ouv,nc,first,last)
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_MAP  &  UV_RESTORE
      !     Extract a subset of visibilities
      !-----------------------------------------------------------------
      integer, intent(in) :: nc         ! Number of channels
      real, intent(in) :: duv(:,:)      ! Input visbilities
      real, intent(out) :: ouv(:,:)     ! Output visibilities
      integer, intent(in) :: first      ! First channel
      integer, intent(in) :: last       ! Last channel
    end subroutine uv_extract_clean
  end interface
  !
  interface
    subroutine uv_clean_sizes(hcct,ccin, mic, first, last)
      use image_def
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_RESTORE
      !   Compute the actual number of components
      !-----------------------------------------------------------------
      type(gildas), intent(in) :: hcct  ! header of CCT data set
      real, intent(in) :: ccin(:,:,:)   ! Clean component table
      integer, intent(out) :: mic(:)    ! Number of iterations per channel
      integer, intent(in) :: first      ! First
      integer, intent(in) :: last       ! and last channel
    end subroutine uv_clean_sizes
  end interface
  !
  interface
    subroutine do_smodel (visi,nc,nv,a,nx,ny,nf,freq,xinc,yinc,factor,mthread)
      !$ use omp_lib
      !-----------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER   Support for UV_RESTORE
      !
      !     Remove model visibilities from UV data set.
      !     Interpolate from a grid of values obtained by FFT before.
      !-----------------------------------------------------------------
      integer, intent(in) :: nc ! Visibility size
      integer, intent(in) :: nv ! Number of visibilities
      integer, intent(in) :: nx ! X size
      integer, intent(in) :: ny ! Y size
      integer, intent(in) :: nf ! Number of frequencies
      real, intent(inout) :: visi(:,:)    ! Computed visibilities
      real(8), intent(in) :: freq         ! Effective frequency
      real, intent(in)  :: factor         ! Flux factor
      complex, intent(in) ::  a(:,:,:)    ! FFT
      real(8), intent(in) :: xinc,yinc    ! Pixel sizes
      integer, intent(in) :: mthread
    end subroutine do_smodel
  end interface
  !
  interface
    subroutine cube_flag_extrema(nchan,varname,mcol,hvar)
      use clean_arrays
      ! @ private
      integer, intent(in) :: nchan
      integer, intent(in) :: mcol(2)
      character(len=*), intent(in) :: varname
      type(gildas), intent(inout) :: hvar
    end subroutine cube_flag_extrema
  end interface
  !
  interface
    subroutine uv_shift_mosaic(line,comm,error)
      use clean_def
      use clean_default
      use clean_arrays
      use phys_const
      use gkernel_types
      use gbl_message
      !-------------------------------------------------------------------
      ! @ private
      !
      !  IMAGER 
      !   Support routine for command
      !       UV_SHIFT  MAP_CENTER 
      !                 [Xpos Ypos UNIT] [ANGLE Angle]
      !
      !   Phase shift a Mosaic or Single Dish UV Table to a new
      !   common phase center and orientation
      !     Offx OffY are offsets in Angle UNIT 
      !               (default 0,0)
      !     Angle     is the final position angle from North in Degree
      !               (default: no change)
      !
      !   Also called implicitely (with no command line arguments, i.e.
      !   no change of Phase Center unless the UV Table is with Phase Offsets)
      !   by command UV_MAP for Mosaics
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name 
      logical error                         ! Logical error flag
    end subroutine uv_shift_mosaic
  end interface
  !
  interface
    subroutine uv_sort (huvx,duvx,error,sorted,shift,newabs,uvmax,uvmin,needed)
      use clean_def
      use clean_arrays
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Sort the input UV table
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: huvx      ! Header
      real, intent(inout) :: duvx(:,:)         ! Data to be sorted
      !
      logical, intent(inout) :: sorted         ! Is table sorted ?
      logical, intent(inout) :: shift          ! Do we shift phase center ?
      logical, intent(in) :: needed            ! Need sorting ?
      logical, intent(out) :: error
      real(kind=8), intent(inout) :: newabs(3) ! New phase center and PA
      real, intent(out) :: uvmin               ! Min baseline
      real, intent(out) :: uvmax               ! Max baseline
    end subroutine uv_sort
  end interface
  !
  interface
    subroutine statistic (line,error)
      use gkernel_types
      use clean_def
      use clean_arrays
      use clean_default
      use clean_types
      use clean_support
      use gbl_message
      use iso_c_binding
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for command
      !   STATISTIC Image [PlaneStart PlaneEnd] [/EDGE Edge] [/NOISE Variable]
      !     [/UPDATE [NOISE|EXTREMA]]
      !
      !     Compute Statistics on internal image, and optionally
      !   return the noise level in the specified Variable.
      !   Image is the name of one of the internal images (DIRTY, CLEAN,
      !   RESIDUAL, SKY, etc...) or any SIC Image variable
      ! 
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine statistic
  end interface
  !
  interface
    subroutine compute_stat(nx,ny,nv,amap,box, &
         &    vmin,ipmin,jpmin,kpmin,vmax,ipmax,jpmax,kpmax, &
         &    mean,rms,noise,np,nn, &
         &    bval,eval, support)
      use gildas_def
      !$ use omp_lib
      !--------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Statistics on image
      !--------------------------------------------------------------
      integer(kind=size_length), intent(out) :: np ! Number of source data points
      integer(kind=size_length), intent(out) :: nn ! Number of empty sky data points
      integer, intent(in) :: nx,ny,nv     ! Image size
      real, intent(in) :: amap(nx,ny,nv)  ! Input data cube
      integer, intent(out) :: ipmin, jpmin, kpmin  ! Position of Min
      integer, intent(out) :: ipmax, jpmax, kpmax  ! Position of Max
      integer, intent(in) ::  box(4)      ! 2-D subset being considered 
      real, intent(out) :: vmin, vmax     ! Min Max
      real, intent(out) :: mean, rms, noise  ! Statistic values
      real, intent(in) :: bval, eval      ! Blanking values
      logical, intent(in), optional :: support(:,:)  ! 2-D support
    end subroutine compute_stat
  end interface
  !
  interface
    subroutine sub_alma_quad (   &
         &    l_method,l_hdirty,l_hresid,l_hclean,   &
         &    l_hbeam,l_hprim,l_tfbeam, l_list,  &
         &    c_method,c_hdirty,c_hresid,c_hclean,   &
         &    c_hbeam,c_hprim,c_tfbeam, c_list,  &
         &    error,tcc)
      use clean_def
      use image_def
      use gbl_message
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the Multi Resolution which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: l_method, c_method
      type (gildas), intent(inout)  :: l_hdirty, l_hbeam, l_hresid, l_hprim
      type (gildas), intent(inout)  :: l_hclean
      type (gildas), intent(inout)  :: c_hdirty, c_hbeam, c_hresid, c_hprim
      type (gildas), intent(inout)  :: c_hclean
      real, intent(inout) :: l_tfbeam(l_hbeam%gil%dim(2),l_hbeam%gil%dim(3),l_hbeam%gil%dim(1))
      real, intent(inout) :: c_tfbeam(c_hbeam%gil%dim(2),c_hbeam%gil%dim(3),c_hbeam%gil%dim(1))
      logical, intent(inout) :: error
      type (cct_par), intent(inout) :: tcc(l_method%m_iter)
      integer, intent(in) :: l_list(:)  ! List of searched pixels
      integer, intent(in) :: c_list(:)  ! List of searched pixels (compact array)
      !
      real, pointer :: c_dirty(:,:)    ! Dirty map
      real, pointer :: c_resid(:,:)    ! Iterated residual
      real, pointer :: c_clean(:,:)    ! Clean Map
      real, pointer :: c_dprim(:,:,:)  ! Primary beam
      real, pointer :: c_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: c_atten(:,:)
      !
      real, pointer :: l_dirty(:,:)    ! Dirty map
      real, pointer :: l_resid(:,:)    ! Iterated residual
      real, pointer :: l_clean(:,:)    ! Clean Map
      real, pointer :: l_beam(:,:) ! Beam for fit
      real, pointer :: l_dprim(:,:,:)  ! Primary beam
      real, pointer :: l_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: l_atten(:,:)
      !
      real, allocatable :: w_fft(:) ! TF work area
      complex, allocatable :: w_work(:,:)  ! Work area
      complex, allocatable :: c_work(:,:)  ! Expansion of residual
      real, allocatable :: r_work(:,:) ! Expansion of residual
      type(cct_par), allocatable :: w_comp(:)
      real, allocatable :: w_cctw(:,:), w_cctf(:,:)
      real, allocatable :: w_resid(:,:)
      type (cct_par), allocatable :: w_tcc(:)
      !
      integer icase, iext
      real last_flux, max_var, cpu0, cpu1
      integer iplane,ibeam
      integer nx,ny,np,mx,my,mp,nc
      integer ier, ix, iy, jx, jy, ncc
      real l_max, c_max
      logical do_fft, ok
      character(len=message_length) :: chain
      character(len=4) :: rname = 'ALMA'
      character(2) ans
      integer nker,mker
    end subroutine sub_alma_quad
  end interface
  !
  interface
    subroutine label_to_cct (image,nx,ny,c_method,label,mx,my,   &
         &    tcc,nv,ix,iy, icase)
      use clean_def
      use gbl_message
      !
      ! @ private
      !
      type (clean_par), intent(in) :: c_method
      type (cct_par), intent(inout) :: tcc(*)
      integer, intent(in) :: nx,ny,mx,my,ix,iy
      integer, intent(out) :: nv
      real(4), intent(in) :: image(nx,ny)
      integer, intent(in) :: label(mx,my)
      integer, intent(in) :: icase
    end subroutine label_to_cct
  end interface
  !
  interface
    subroutine long_to_image (tcc,ncc,image,nx,ny)
      use clean_def
      ! @ private
      integer, intent(in) :: nx,ny,ncc
      type (cct_par), intent(in) :: tcc(ncc)
      real, intent(inout) :: image(nx,ny)
    end subroutine long_to_image
  end interface
  !
  interface
    subroutine image_to_long (image,nx,ny,tcc,mcc,ncc)
      use clean_def
      ! @ private
      integer, intent(in) :: nx,ny,mcc
      integer, intent(out) :: ncc
      type (cct_par), intent(out) :: tcc(mcc)
      real, intent(in) :: image(nx,ny)
    end subroutine image_to_long
  end interface
  !
  interface
    subroutine display_cct(method,tcc,ncc,ioff)
      use clean_def
      ! @ private
      integer, intent(in) :: ncc
      type(clean_par), intent(inout) :: method
      type(cct_par), intent(in) :: tcc(ncc)
      integer, intent(in) :: ioff
    end subroutine display_cct
  end interface
  !
  interface
    subroutine cct_normal (tcc,nc,fcomp,tfbeam,nx,ny,wfft,factor)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS: CLEAN   Internal routine
      !   Compute the SDI normalisation factor
      !----------------------------------------------------------------------
      use clean_def
      integer, intent(in) :: nx,ny,nc
      type (cct_par), intent(in) :: tcc(nc)
      real, intent(in) :: tfbeam(nx,ny)
      complex, intent(inout) :: fcomp(nx,ny)
      real, intent(inout) :: wfft(*)
      real factor
    end subroutine cct_normal
  end interface
  !
  interface
    subroutine choice_to_cct (c_method,tcc,nc,xcoord,ycoord,values)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Internal routine
      !   Put the selected components into the TCC structure
      !----------------------------------------------------------------------
      type (clean_par), intent(in) :: c_method
      integer, intent(in) :: nc
      type (cct_par), intent(inout) :: tcc(nc)
      integer, intent(in) :: xcoord(nc), ycoord(nc)
      real, intent(in) :: values(nc)
    end subroutine choice_to_cct
  end interface
  !
  interface
    subroutine compact_to_image (c_method,mx,my,tcc,ncc,image,nx,ny)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Internal routine
      !   Put the TCC values on the map grid
      !----------------------------------------------------------------------
      type (clean_par) :: c_method
      integer nx,ny,ncc,mx,my
      type (cct_par) :: tcc(ncc)
      real image(nx,ny)
    end subroutine compact_to_image
  end interface
  !
  interface
    subroutine sub_alma_bis (   &
         &    l_method,l_hdirty,l_hresid,l_hclean,   &
         &    l_hbeam,l_hprim,l_tfbeam,l_list,   &
         &    c_method,c_hdirty,c_hresid,c_hclean,   &
         &    c_hbeam,c_hprim,c_tfbeam,c_list,   &
         &    error,tcc)
      use clean_def
      use image_def
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the Multi Resolution which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: l_method, c_method
      type (gildas), intent(inout)  :: l_hdirty, l_hbeam, l_hresid, l_hprim
      type (gildas), intent(inout)  :: l_hclean
      type (gildas), intent(inout)  :: c_hdirty, c_hbeam, c_hresid, c_hprim
      type (gildas), intent(inout)  :: c_hclean
      real, intent(inout) :: l_tfbeam(l_hbeam%gil%dim(2),l_hbeam%gil%dim(3),l_hbeam%gil%dim(1))
      real, intent(inout) :: c_tfbeam(c_hbeam%gil%dim(2),c_hbeam%gil%dim(3),c_hbeam%gil%dim(1))
      logical, intent(inout) :: error
      type (cct_par), intent(inout) :: tcc(l_method%m_iter)
      integer, intent(in) :: l_list(:)
      integer, intent(in) :: c_list(:)
      !
      real, pointer :: c_dirty(:,:)    ! Dirty map
      real, pointer :: c_resid(:,:)    ! Iterated residual
      real, pointer :: c_clean(:,:)    ! Clean Map
      real, pointer :: c_dprim(:,:,:)  ! Primary beam
      real, pointer :: c_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: c_atten(:,:)
      !
      real, pointer :: l_dirty(:,:)    ! Dirty map
      real, pointer :: l_resid(:,:)    ! Iterated residual
      real, pointer :: l_clean(:,:)    ! Clean Map
      real, pointer :: l_beam(:,:) ! Beam for fit
      real, pointer :: l_dprim(:,:,:)  ! Primary beam
      real, pointer :: l_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: l_atten(:,:)
      !
      real, allocatable :: w_fft(:) ! TF work area
      complex, allocatable :: w_work(:,:)  ! Work area
      complex, allocatable :: c_work(:,:)  ! Expansion of residual
      real, allocatable :: r_work(:,:) ! Expansion of residual
      type(cct_par), allocatable :: w_comp(:)
      real, allocatable :: w_resid(:,:)
      !
      integer iplane,ibeam
      integer nx,ny,np,mx,my,mp,nc
      integer ier, ix, iy
      real l_max, c_max
      logical do_fft, ok
      character(len=2) ans
      integer nker,mker
    end subroutine sub_alma_bis
  end interface
  !
  interface
    function imaxlst (method,list,resid,nx,ny,ix,jy)
      use clean_def
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for ALMA deconvolution
      !--------------------------------------------------------------
      real imaxlst  ! intent(out)
      type (clean_par), intent(in) :: method
      integer, intent(in) :: nx,ny
      integer, intent(out) :: ix,jy
      integer, intent(in) :: list(:)
      real, intent(in) :: resid(nx,ny)
    end function imaxlst
  end interface
  !
  interface
    subroutine remove_incompact(method,resid,nx,ny,tfbeam,wfft,   &
         &    np,primary,weight,tcc,first,last,mx,my)
      use clean_def
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Remove components in "compact" array
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      !
      integer, intent(in) ::  np,nx,ny,mx,my,first,last
      real, intent(inout) ::  resid(nx,ny)
      real, intent(inout) ::  tfbeam(nx,ny,np)     ! T.F. du beam  Complex ?
      real, intent(in) :: primary(np,nx,ny)
      real, intent(in) :: weight(nx,ny)
      type(cct_par), intent(inout) :: tcc(last)
      real, intent(inout) :: wfft(*)
    end subroutine remove_incompact
  end interface
  !
  interface
    subroutine remove_inlong(method,resid,nx,ny,tfbeam,wfft,   &
         &    np,primary,weight,tcc,first,last)
      use clean_def
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Remove components in "long baseline" array
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      !
      integer, intent(in) ::  np,nx,ny,first,last
      real, intent(inout) ::  resid(nx,ny)
      real, intent(inout) ::  tfbeam(nx,ny,np)     ! T.F. du beam  Complex ?
      real, intent(in) :: primary(np,nx,ny)
      real, intent(in) :: weight(nx,ny)
      type(cct_par), intent(inout) :: tcc(last)
      real, intent(inout) :: wfft(*)
    end subroutine remove_inlong
  end interface
  !
  interface
    subroutine expand_kernel(c_method,mx,my,ix,iy,c_max,   &
         &    tcc,first,last,nx,ny,kernel,nker)
      use clean_def
      ! @ private
      type (clean_par), intent(inout) :: c_method
      integer, intent(in) :: nx,ny
      integer, intent(in) :: mx,my
      integer, intent(in) :: ix,iy
      integer, intent(in) :: nker
      integer, intent(in) :: first
      integer, intent(inout) :: last
      real, intent(in) :: kernel(nker,nker)
      real, intent(in) :: c_max
      type (cct_par), intent(inout) :: tcc(*)
    end subroutine expand_kernel
  end interface
  !
  interface
    subroutine alma_make (method, l_hclean, tcc)
      use clean_def
      use image_def
      ! @ private
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: l_hclean
      type (cct_par), intent(in) :: tcc(method%n_iter)
    end subroutine alma_make
  end interface
  !
  interface
    subroutine sub_alma (   &
         &    l_method,l_hdirty,l_hresid,l_hclean,   &
         &    l_hbeam,l_hprim,l_tfbeam,l_list,   &
         &    c_method,c_hdirty,c_hresid,c_hclean,   &
         &    c_hbeam,c_hprim,c_tfbeam,c_list,   &
         &    error,tcc)
      use clean_def
      use image_def
      use gbl_message
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the Multi Resolution which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: l_method, c_method
      type (gildas), intent(inout)  :: l_hdirty, l_hbeam, l_hresid, l_hprim
      type (gildas), intent(inout)  :: l_hclean
      type (gildas), intent(inout)  :: c_hdirty, c_hbeam, c_hresid, c_hprim
      type (gildas), intent(inout)  :: c_hclean
      real, intent(inout) :: l_tfbeam(l_hbeam%gil%dim(2),l_hbeam%gil%dim(3),l_hbeam%gil%dim(1))
      real, intent(inout) :: c_tfbeam(c_hbeam%gil%dim(2),c_hbeam%gil%dim(3),c_hbeam%gil%dim(1))
      logical, intent(inout) :: error
      type (cct_par), intent(inout) :: tcc(l_method%m_iter)
      integer, intent(in) :: l_list(:)
      integer, intent(in) :: c_list(:)
      !
      real, pointer :: c_dirty(:,:)    ! Dirty map
      real, pointer :: c_resid(:,:)    ! Iterated residual
      real, pointer :: c_clean(:,:)    ! Clean Map
      real, pointer :: c_dprim(:,:,:)  ! Primary beam
      real, pointer :: c_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: c_atten(:,:)
      !
      real, pointer :: l_dirty(:,:)    ! Dirty map
      real, pointer :: l_resid(:,:)    ! Iterated residual
      real, pointer :: l_clean(:,:)    ! Clean Map
      real, pointer :: l_beam(:,:) ! Beam for fit
      real, pointer :: l_dprim(:,:,:)  ! Primary beam
      real, pointer :: l_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: l_atten(:,:)
      !
      real, allocatable :: w_fft(:) ! TF work area
      complex, allocatable :: w_work(:,:)  ! Work area
      complex, allocatable :: c_work(:,:)  ! Expansion of residual
      real, allocatable :: r_work(:,:) ! Expansion of residual
      type(cct_par), allocatable :: w_comp(:)
      real, allocatable, target :: sc_beam(:,:,:)
      real, allocatable :: w_resid(:,:)
      !
      character(len=*), parameter :: rname='SUB_ALMA'
      !
      integer iplane,ibeam
      integer nx,ny,np,mx,my,mp,nc,kx,ky
      integer ip, ier
      real l_max, c_max
      logical do_fft, ok
      character(len=80) :: chain
      character(len=2) ans
      integer nker,mker
    end subroutine sub_alma
  end interface
  !
  interface
    function amaxlst (method,list,resid,nx,ny)
      use clean_def
      !-----------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !-----------------------------------------------------
      real amaxlst      ! intent(out)
      type (clean_par), intent(inout) :: method
      integer, intent(in) :: nx,ny
      integer, intent(in) :: list(:)
      real, intent(in) :: resid(nx,ny)
    end function amaxlst
  end interface
  !
  interface
    subroutine one_cycle90 (method,head,   &
         &    clean,beam,resid,nx,ny,tfbeam,fcomp,   &
         &    wcl,mcl,ixbeam,iybeam,ixpatch,iypatch,bgain,   &
         &    box, work, tcc, list, nl, nf, primary, weight, maxabs)
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Major cycle loop according to B.Clark idea
      !----------------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: head
      !
      integer, intent(in) ::  nf,nx,ny,mcl,nl
      real, intent(inout) ::  clean(nx,ny)
      real, intent(inout) ::  resid(nx,ny)
      real, intent(in) ::     beam(nx,ny,nf)
      real, intent(inout) ::  tfbeam(nx,ny,nf)     ! T.F. du beam  Complex ?
      complex, intent(inout) :: fcomp(nx,ny)       ! T.F. du vecteur modification
      type(cct_par), intent(inout) ::  wcl(mcl)
      real, intent(in) ::  bgain                   ! Maximum sidelobe level
      integer, intent(in) ::  ixbeam, iybeam       ! Beam maximum position
      integer, intent(in) ::  ixpatch, iypatch     ! Beam patch radius
      integer, intent(in) ::  box(4)               ! Cleaning box
      real, intent(inout) ::  work(*)                 ! Work space for FFT
      type(cct_par), intent(inout) :: tcc(method%m_iter) ! Clean components array
      integer, intent(in) ::  list(nl)
      !
      real, intent(in) ::  primary(nf,nx,ny)       ! Primary beams
      real, intent(in) ::  weight (nx,ny)
      !
      real    maxabs     ! max and min of data, absolute max value
    end subroutine one_cycle90
  end interface
  !
  interface
    subroutine remove_inother(method,resid,nx,ny,tfbeam,wfft,   &
         &    np,primary,weight,   &
         &    tcc,first,last,mx,my,kernel,nker)
      use clean_def
      !--------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for ALMA deconvolution
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      !
      integer, intent(in) ::  np,nx,ny,mx,my,nker,first,last
      real, intent(inout) ::  resid(nx,ny)
      real, intent(inout) ::  tfbeam(nx,ny,np)     ! T.F. du beam  Complex ?
      real, intent(in) :: primary(np,nx,ny)
      real, intent(in) :: weight(nx,ny)
      real, intent(inout) :: kernel(nker,nker)
      real, intent(inout) :: wfft(*)
      type(cct_par), intent(inout) :: tcc(last)
    end subroutine remove_inother
  end interface
  !
  interface
    subroutine add_primker (clean,nx,ny,np,prim,value,kx,ky,kp,ker,nk)
      !-----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Smooth an array using a kernel, with Primary Beams
      !   For ALMA deconvolution
      !-----------------------------------------------------------------------
      integer, intent(in) :: nx,ny,np             ! Image size
      real, intent(in) :: value                   ! Input value
      real, intent(inout) :: clean(nx,ny)         ! Summed output array
      real, intent(in) :: prim(np,nx,ny)
      integer, intent(in) :: kp                   ! Field
      integer, intent(in) :: kx,ky                ! Center of value
      integer, intent(in) :: nk                   ! Kernel size
      real, intent(in) :: ker(nk,nk)
    end subroutine add_primker
  end interface
  !
  interface
    subroutine alma_make90 (method, l_hclean, c_hclean,   &
         &    tcc, kernel,nker)
      use clean_def
      use image_def
      !-------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Make clean map
      !-------------------------------------------------------------
      integer, intent(in) :: nker
      real, intent(in) :: kernel(nker,nker)
      type (clean_par), intent(inout) :: method
      type (gildas), intent(inout) :: l_hclean, c_hclean
      type (cct_par), intent(in) :: tcc(method%n_iter)
    end subroutine alma_make90
  end interface
  !
  interface
    subroutine sub_alma_ter (   &
         &    l_method,l_hdirty,l_hresid,l_hclean,   &
         &    l_hbeam,l_hprim,l_tfbeam,l_list,   &
         &    c_method,c_hdirty,c_hresid,c_hclean,   &
         &    c_hbeam,c_hprim,c_tfbeam,c_list,   &
         &    error,tcc)
      use clean_def
      use image_def
      use gbl_message
      !--------------------------------------------------------------
      ! @ private
      !
      ! Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the Multi Resolution which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      type (clean_par), intent(inout) :: l_method, c_method
      type (gildas), intent(inout) :: l_hdirty, l_hbeam, l_hresid, l_hprim
      type (gildas), intent(inout) :: l_hclean
      type (gildas), intent(inout) :: c_hdirty, c_hbeam, c_hresid, c_hprim
      type (gildas), intent(inout) :: c_hclean
      real, intent(inout) :: l_tfbeam(l_hbeam%gil%dim(2),l_hbeam%gil%dim(3),l_hbeam%gil%dim(1))
      real, intent(inout) :: c_tfbeam(c_hbeam%gil%dim(2),c_hbeam%gil%dim(3),c_hbeam%gil%dim(1))
      logical, intent(inout) :: error
      type (cct_par), intent(inout) :: tcc(l_method%m_iter)
      integer, intent(in) :: l_list(:)
      integer, intent(in) :: c_list(:)
      !
      real, pointer :: c_dirty(:,:)    ! Dirty map
      real, pointer :: c_resid(:,:)    ! Iterated residual
      real, pointer :: c_clean(:,:)    ! Clean Map
      real, pointer :: c_dprim(:,:,:)  ! Primary beam
      real, pointer :: c_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: c_atten(:,:)
      !
      real, pointer :: l_dirty(:,:)    ! Dirty map
      real, pointer :: l_resid(:,:)    ! Iterated residual
      real, pointer :: l_clean(:,:)    ! Clean Map
      real, pointer :: l_beam(:,:) ! Beam for fit
      real, pointer :: l_dprim(:,:,:)  ! Primary beam
      real, pointer :: l_dbeam(:,:,:)  ! Dirty beam (per field)
      real, pointer :: l_atten(:,:)
      !
      real, allocatable :: w_fft(:) ! TF work area
      complex, allocatable :: w_work(:,:)  ! Work area
      complex, allocatable :: c_work(:,:)  ! Expansion of residual
      real, allocatable :: r_work(:,:) ! Expansion of residual
      type(cct_par), allocatable :: w_comp(:)
      real, allocatable :: w_resid(:,:)
      !
      integer iplane,ibeam
      integer nx,ny,np,mx,my,mp,nc
      integer ier, ix, iy, jx, jy
      real l_max, c_max
      logical do_fft, ok
      character(len=message_length) :: chain
      character(len=7) :: rname = 'COMPACT'
      character(len=2) ans
      integer nker,mker
    end subroutine sub_alma_ter
  end interface
  !
  interface
    subroutine load_cct (image,nx,ny,blc,trc,label,mx,my,   &
         &    nv,wcl,ix,iy,gain,tmax,ngoal)
      use clean_def
      ! @ private
      integer, intent(in) :: nx,ny,mx,my,blc(2),trc(2)
      integer, intent(out) :: nv
      integer, intent(in) :: ix,iy
      real(4), intent(in) :: image(nx,ny)
      integer, intent(inout) :: label(mx,my)
      type(cct_par), intent(inout) :: wcl(mx*my)
      integer, intent(in) :: ngoal
      real, intent(in) :: gain
      real, intent(out) :: tmax
    end subroutine load_cct
  end interface
  !
  interface
    subroutine init_convolve (i0,j0,nx,ny,beam,fbeam,area,work)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   FFT Utility routine
      !	  Compute the FT of a beam centered on pixel I0,J0
      !----------------------------------------------------------------------
      integer, intent(in) :: nx,ny          ! Problem size
      integer, intent(in) :: i0,j0          ! Position of peak
      complex, intent(out) :: fbeam(nx,ny)  ! TF of beam
      real, intent(in) :: beam(nx,ny)       ! Beam
      real, intent(inout) :: work(*)        ! Work space
      real, intent(out) :: area             ! Beam area
    end subroutine init_convolve
  end interface
  !
  interface
    subroutine sub_major(method,hdirty,hresid,hclean,   &
         &    hbeam,hprim,hmask,dcct,mask,list,error,        &
         &    major_plot, next_flux)
      use clean_def
      use image_def
      use gbl_message
      !$ use omp_lib
      !--------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the MRC (Multi Resolution CLEAN)
      !     which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      external :: major_plot
      external :: next_flux
      !
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: hdirty
      type (gildas), intent(inout) :: hbeam
      type (gildas), intent(inout) :: hclean
      type (gildas), intent(inout) :: hresid
      type (gildas), intent(in) :: hprim
      type (gildas), intent(in) :: hmask
      real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
      logical, intent(in), target :: mask(:,:)
      integer, intent(in), target :: list(:)
      logical, intent(inout) ::  error
    end subroutine sub_major
  end interface
  !
  interface
    subroutine sub_major_omp(inout_method,hdirty,hresid,hclean,   &
         &    hbeam,hprim,hmask,dcct,mask,list,error,        &
         &    major_plot, next_flux)
      use clean_def
      use clean_default
      use clean_support
      use image_def
      use gbl_message
      !$ use omp_lib
      use omp_control
      !--------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the MRC (Multi Resolution CLEAN)
      !     which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      external :: major_plot
      external :: next_flux
      !
      type (clean_par), intent(inout) :: inout_method
      type (gildas), intent(in) :: hdirty
      type (gildas), intent(inout) :: hbeam
      type (gildas), intent(inout) :: hclean
      type (gildas), intent(inout) :: hresid
      type (gildas), intent(in) :: hprim
      type (gildas), intent(in) :: hmask
      real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
      logical, intent(in), target :: mask(:,:)
      integer, intent(in), target :: list(:)
      logical, intent(inout) ::  error
    end subroutine sub_major_omp
  end interface
  !
  interface
    subroutine sub_major_lin(method,hdirty,hresid,hclean,   &
         &    hbeam,hprim,hmask,dcct,mask,list,error,        &
         &    major_plot, next_flux)
      use clean_def
      use image_def
      use gbl_message
      !--------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Clean/Mosaic
      !     Perfom a CLEAN based on all CLEAN algorithms,
      !     except the MRC (Multi Resolution CLEAN)
      !     which requires a different tool
      !     Works for mosaic also, except for the Multi Scale clean
      !     (not yet implemented for this one, but feasible...)
      !--------------------------------------------------------------
      external :: major_plot
      external :: next_flux
      !
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: hdirty
      type (gildas), intent(inout) :: hbeam
      type (gildas), intent(inout) :: hclean
      type (gildas), intent(inout) :: hresid
      type (gildas), intent(in) :: hprim
      type (gildas), intent(in) :: hmask
      real, intent(inout) :: dcct(:,:,:) ! (3,hclean%gil%dim(3),*)
      logical, intent(in), target :: mask(:,:)
      integer, intent(in), target :: list(:)
      logical, intent(inout) ::  error
    end subroutine sub_major_lin
  end interface
  !
  interface
    subroutine get_beam(method,hbeam,hresid,hprim,  &
         &    tfbeam,w_work,w_fft,fhat,error, mask)
      use clean_def
      use image_def
      use gbl_message
      !-----------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Get beam related information
      !-----------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in)  :: hbeam
      type (gildas), intent(in)  :: hresid
      type (gildas), intent(in)  :: hprim
      real, intent(inout) :: tfbeam(hbeam%gil%dim(1),hbeam%gil%dim(2),hbeam%gil%dim(3))
      complex, intent(inout) :: w_work(hbeam%gil%dim(1),hbeam%gil%dim(2))
      real, intent(inout) :: fhat,w_fft(:)
      logical, intent(inout) :: error
      logical, optional, intent(inout) :: mask(:,:)
    end subroutine get_beam
  end interface
  !
  interface
    subroutine sub_mrc(rname,method,hdirty,hresid,hclean,   &
         &    hbeam,hprim,mask,error, plot_routine)
      use clean_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  CLEAN Internal routine
      !   Implementation of Multi-Resolution CLEAN deconvolution algorithm.
      !   A smooth and a difference map are deconvolved using Clark CLEAN
      !   with smooth and difference dirty beams.
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: hdirty, hbeam, hclean, hresid, hprim
      logical, target, intent(in) :: mask(:,:)
      logical, intent(out) :: error
      external :: plot_routine
    end subroutine sub_mrc
  end interface
  !
  interface
    subroutine mrc_maps (nx,ny,full,diff,wl,mx,my,smooth,ws,ratio,   &
         &    bmin,xinc,yinc,wfft)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING CLEAN
      !   Multi-Resolution Clean
      !   Compute smooth and difference maps.
      !   Smoothing is done in UV plane, by Gauss convolution and
      !   Fourier truncation. Smooth map is smaller than original map
      !----------------------------------------------------------------------
      integer, intent(in) :: nx,ny,mx,my
      real, intent(in) :: full(nx,ny)
      real, intent(inout) :: diff(nx,ny),smooth(mx,my)
      real, intent(inout) :: wfft(*)
      real, intent(in) :: ratio,bmin,xinc,yinc
      complex, intent(inout) :: ws(mx,my),wl(nx,ny)
    end subroutine mrc_maps
  end interface
  !
  interface
    subroutine mrc_make_clean (nx,ny,beam,bmaj,bmin,pa,xinc,yinc)
      !--------------------------------------------------------------------
      ! @ private
      !
      ! MRC
      !     Make Clean Beam
      !--------------------------------------------------------------------
      integer, intent(in) :: nx,ny
      real, intent(out) :: beam(nx,ny)
      real, intent(in) :: bmaj,bmin,pa,xinc,yinc
    end subroutine mrc_make_clean
  end interface
  !
  interface
    subroutine mrc_setup (ratio,beam,bmin,xinc,yinc,   &
         &    nx,ny,dbeam,dbeam_fft,dwork,dmax,dxmax,dymax,   &
         &    mx,my,sbeam,sbeam_fft,swork,smax,sxmax,symax,   &
         &    type,work)
      use gbl_message
      !------------------------------------------------------------------
      ! @ private
      !
      ! MRC
      !     Compute Smooth and Difference BEAMS (dirty or clean) ie
      !        CALL MRC_MAPS + normalisations + TF
      !------------------------------------------------------------------
      character(len=*), intent(in) :: type
      integer, intent(in) :: nx,ny,mx,my
      real, intent(in) :: ratio,beam(nx,ny)
      real, intent(in) :: bmin,xinc,yinc
      real, intent(inout) :: dbeam(nx,ny),dmax
      integer, intent(inout) :: dxmax,dymax
      real, intent(inout) :: dbeam_fft(nx,ny)
      complex, intent(inout) :: dwork(nx,ny)
      real, intent(inout) :: sbeam(mx,my),smax
      integer, intent(inout) :: sxmax,symax
      real, intent(inout) :: sbeam_fft(mx,my)
      complex, intent(inout) :: swork(mx,my)
      real, intent(inout) :: work(*)
    end subroutine mrc_setup
  end interface
  !
  interface
    subroutine cmpmsk (s,mx,my,d,nx,ny,irat)
      !-----------------------------------------------------------------
      ! @ private
      !
      ! MRC
      !     Convert difference (ie initial) mask in smooth mask
      !-----------------------------------------------------------------
      integer, intent(in) ::  nx,ny,mx,my,irat
      logical, intent(out) :: s(mx,my)
      logical, intent(in) :: d(nx,ny)
    end subroutine cmpmsk
  end interface
  !
  interface
    subroutine restore_clean90 (method,clean,nx,ny,ft,   &
         &    tfbeam,scale,wfft,tcc,nc)
      !-----------------------------------------------------------------
      ! @ private
      !
      ! GILDAS: CLEAN   Internal routine
      ! Convolve source list into residual map using the Fourier method.
      ! The normalisation should be correct for flux or brightness maps,
      ! depending on value of SCALE.
      !-----------------------------------------------------------------
      use clean_def
      type (clean_par), intent(in) :: method
      integer, intent(in) :: nx, ny, nc
      real, intent(inout) :: clean(nx,ny),wfft(*)
      complex, intent(inout) :: ft(nx,ny)
      real, intent(in) :: tfbeam(nx,ny)
      real, intent(in) :: scale
      type (cct_par), intent(in) :: tcc(nc)
    end subroutine restore_clean90
  end interface
  !
  interface
    subroutine com_support(line,error)
      use gildas_def
      use clean_default
      use clean_arrays
      use clean_support
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  
      !   Support routine for command
      !	  SUPPORT [/PLOT] [/CURSOR] [/RESET] [/MASK] [/VARIABLE]
      !           [/THRESHOLD  Raw Smooth [SmoothingLength]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Commandand line
      logical,          intent(out) :: error ! Logical error flag
    end subroutine com_support
  end interface
  !
  interface
    subroutine check_area(method,head,quiet)
      use clean_def
      use clean_default
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Check search area for Cleaning, and guess the maximum number of 
      !   iterations
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: head           ! Image header
      type (clean_par), intent(inout) :: method   ! Cleaning parameters  
      logical, intent(in) :: quiet                ! Printout level
    end subroutine check_area
  end interface
  !
  interface
    subroutine beam_plane(method,hbeam,hdirty)
      use clean_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define beam plane and check if fit is required
      !---------------------------------------------------------------------
      type (clean_par), intent(inout) :: method   ! Clean method parameters
      type (gildas), intent(in) :: hbeam          ! Beam header
      type (gildas), intent(in) :: hdirty         ! Used to figure out which plane
    end subroutine beam_plane
  end interface
  !
  interface
    subroutine clean_make90(method,hclean,clean,tcc)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Build clean image from Component List
      !---------------------------------------------------------------------
      type (clean_par), intent(inout) :: method    ! Clean method parameters
      type (gildas), intent(inout) :: hclean       ! Clean header
      real, intent(inout) :: clean(hclean%gil%dim(1),hclean%gil%dim(2))
      type (cct_par), intent(in) :: tcc(method%n_iter)  ! Clean components
    end subroutine clean_make90
  end interface
  !
  interface
    subroutine plunge_real (r,nx,ny,c,mx,my)
      !------------------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Support for UV_RESTORE and others
      !     Plunge a Real array into a Complex array
      !-----------------------------------------------------------------
      integer, intent(in)  :: nx,ny              ! Size of input array
      real, intent(in)     :: r(nx,ny)           ! Input real array
      integer, intent(in)  :: mx,my              ! Size of output array
      complex, intent(out) :: c(mx,my)           ! Output complex array
    end subroutine plunge_real
  end interface
  !
  interface
    subroutine recent(nx,ny,z)
      !------------------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Support for UV_RESTORE and others
      !   Recenters the Fourier Transform, for easier display. The present version
      !   will only work for even dimensions.
      !------------------------------------------------------------------------------
      integer, intent(in) :: nx
      integer, intent(in) :: ny
      complex, intent(inout) :: z(nx,ny)
    end subroutine recent
  end interface
  !
  interface
    subroutine cube_minmax(comm,hmap,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Compute the extrema of a data Cube
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: comm
      type(gildas), intent(inout) :: hmap
      logical, intent(inout) :: error
    end subroutine cube_minmax
  end interface
  !
  interface
    subroutine get_clean (method,hbeam,dbeam,error)
      use clean_def
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Get Clean beam parameters
      !----------------------------------------------------------------------
      type (gildas), intent(in) :: hbeam
      type (clean_par), intent(inout) :: method
      real, intent(in) :: dbeam(hbeam%gil%dim(1),hbeam%gil%dim(2))
      logical, intent(out) :: error
    end subroutine get_clean
  end interface
  !
  interface
    subroutine pribeam(name,majo,mino,pa, field, plane, jvm)
      use gbl_message
      ! @ private
      character(len=*), intent(in) :: name
      real, intent(in) :: majo
      real, intent(in) :: mino 
      real, intent(in) :: pa
      integer, intent(in) :: field
      integer, intent(in) :: plane
      real, intent(in) :: jvm
    end subroutine pribeam
  end interface
  !
  interface
    subroutine guess (nv,values,coords,par)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !   Setup starting values for beam fit
      !----------------------------------------------------------------------
      integer, intent(in)  :: nv         ! Number of data
      real, intent(in)  :: values(nv)    ! Data values
      real, intent(in)  :: coords(2,nv)  ! X and Y coordinates of data
      real, intent(out) :: par(6)        ! Guessed parameters
    end subroutine guess
  end interface
  !
  interface
    subroutine min2d (npar,g,f,var,iflag)
      use gildas_def
      use mod_fitbeam
      !------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !   minimizing function
      !------------------------------------------------------------------
      integer, intent(in) :: npar        ! Number of parameters
      real(8), intent(out)  :: g(npar)   ! First derivatives
      real(8), intent(out)  :: f         ! Function value
      real(8), intent(in)  :: var(npar)  ! Parameter values
      integer, intent(in)  :: iflag      ! Operation code
    end subroutine min2d
  end interface
  !
  interface
    subroutine mn2d (npar,g,f,var,iflag,values,coords,nv,   &
         &    sigbas,sigrai)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !---------------------------------------------------------------------
      integer, intent(in) :: npar        ! Number of parameters
      real(8), intent(out)  :: g(npar)   ! First derivatives
      real(8), intent(out)  :: f         ! Function value
      real(8), intent(in)  :: var(npar)  ! Parameter values
      integer, intent(in)  :: iflag      ! Operation code
      integer, intent(in)  :: nv         ! Number of data
      real, intent(in)  :: values(nv)    ! Data values
      real, intent(in)  :: coords(2,nv)  ! X and Y coordinates of data
      real, intent(out)  :: sigbas       ! Baseline noise
      real, intent(out)  :: sigrai       ! On Source noise
    end subroutine mn2d
  end interface
  !
  interface
    subroutine fit2d (fcn,liter,error)
      use gildas_def
      use fit_minuit
      use mod_fitbeam
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !   Setup and starts a GAUSS fit minimisation using MINUIT
      !---------------------------------------------------------------------
      external :: fcn                   !   Function to be minimized
      logical, intent(in) :: liter      !   Iterate ?
      logical, intent(out) :: error     !   Error flag
    end subroutine fit2d
  end interface
  !
  interface
    subroutine mid2d(fit,ifatal,liter)
      use gildas_def
      use mod_fitbeam
      use fit_minuit
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !   Start a gaussian fit by building the PAR array and internal
      !   variable used by Minuit
      !---------------------------------------------------------------------
      type(fit_minuit_t), intent(inout) :: fit  ! Fitting variables
      integer, intent(out) :: ifatal            ! Error code
      logical, intent(in) :: liter              ! Iterate ?
    end subroutine mid2d
  end interface
  !
  interface
    subroutine loadv (image,nx,ny,blc,trc,label,mx,my,   &
         &    nv,values,coords)
    !---------------------------------------------------------
    ! @ private
    !
    ! MAPPING
    !   Support for FIT of beam
    !   Load the values from the region around the peak
    !   of the primary beam (assumed centered).
    !---------------------------------------------------------
      integer, intent(in) :: nx               ! X size
      integer, intent(in) :: ny               ! Y size
      real(4), intent(in) :: image(nx,ny)     ! Image
      integer, intent(in) :: blc(2)           ! BLC
      integer, intent(in) :: trc(2)           ! TRC
      integer, intent(in) :: mx               ! Output X size
      integer, intent(in) :: my               ! Output Y size
      integer, intent(in) :: label(mx,my)     ! Field identification
      integer, intent(out) :: nv              ! Number of values
      real(4), intent(out) :: values(mx*my)   ! Selected values
      real(4), intent(out) :: coords(2,mx*my) ! X and Y pixel numbers
    end subroutine loadv
  end interface
  !
  interface
    subroutine sub_threshold (image,nx,ny,blc,trc,labelo,mx,my,   &
         &    nf,labeli,labelf,mf,thre,   &
         &    blank, eblank)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for FIT of beam
      !   Divide an image in connex labelled areas above (or below)
      !   a given threshold
      !---------------------------------------------------------------------
      integer, intent(in) :: nx              ! X size
      integer, intent(in) :: ny              ! Y size
      real(4), intent(in) :: image(nx,ny)    ! Image
      integer, intent(in) :: blc(2)          ! BLC
      integer, intent(in) :: trc(2)          ! TRC
      integer, intent(in) :: mx              ! Output X size
      integer, intent(in) :: my              ! Output Y size
      integer, intent(out) :: labelo(mx,my)  ! Field identification
      integer, intent(out) :: nf             ! Number of fields
      integer, intent(in) :: mf              ! Maximum number of fields
      integer, intent(out) :: labeli(mf)     ! work area: intermediate numbers
      integer, intent(out) :: labelf(mf)     ! Final field number
      real(4), intent(in) :: thre            ! Threshold for field id
      real(4), intent(in) :: blank           ! Blanking value
      real(4), intent(in) :: eblank          ! Tolerance on blanking
    end subroutine sub_threshold
  end interface
  !
  interface
    subroutine descen(label,labin,labout)
      !----------------------------------------------------
      ! @ private
      !   Identify connex areas and propagate label
      !----------------------------------------------------
      integer, intent(in) :: label(*)     ! Labels
      integer, intent(in) :: labin        ! Starting label
      integer, intent(out) :: labout      ! Ending label
    end subroutine descen
  end interface
  !
  interface
    subroutine fast_fit2d(nv,values,coords,spar)
      !----------------------------------------------------
      ! @ private
      !
      ! Fast Gaussian fit
      ! Anthony & Granick 2009, Langmuir 2009, 25(14),8152–8160
      !   Vaut pas grand chose: ne traite pas l orientation...
      !
      ! Methode de la covariance bi-dim: une Gaussienne est définie par
      ! sa matrice de covariance
      !
      ! p(X) = 1/(2.pi.|C|) exp[ -1/2 (X-mu)^ C (X-mu) ]
      ! where mu is the Centroid vector
      !   C is the correlation matrix
      ! and the quantity within brackets is the dot product of X(-mu) by
      ! its transform through matrix C.
      !
      ! The covariance matrix can be taken from the data as
      !  Cov_xx = Sum_ij (i-m_i)**2*p(i,j)
      !  Cov_xy = Cov_yx = Sum_ij ((i-m_i)*(i-m_j)*p(i,j)
      !  Cov_yy = Sum_ij (j-m_j)**2*p(i,j)
      ! with p(i,j) normalized to a total sum of 1
      !  and
      !  m_i = Sum_ij (i*p(i,j))
      !  m_j = Sum_ij (j%p(i,j))
      !
      ! It works well if the noise is low enough...
      !
      !-----------------------------------------------------
      integer, intent(in) :: nv   ! Number of selected pixels
      real, intent(in) :: values(nv)   ! Pixel values
      real, intent(in) :: coords(2,nv) ! Pixel coordinates
      real, intent(inout) :: spar(6)
    end subroutine fast_fit2d
  end interface
  !
  interface
    function patch_size(patch,size,maxr,minv)
      ! @ private
      integer :: patch_size ! Intent(out)
      integer, intent(in) :: patch, size, maxr, minv
    end function patch_size
  end interface
  !
  interface
    subroutine dofft_quick (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Gridding routine for UV_MAP and UV_RESTORE
      !
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick
  end interface
  !
  interface
    subroutine dofft_quick_omp (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff,ipara)
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Gridding routine for UV_MAP and UV_RESTORE
      !
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !
      !  Parallelized version to be used for Continuum (nc=1) data
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
      integer, intent(in) :: ipara
    end subroutine dofft_quick_omp
  end interface
  !
  interface
    subroutine dofft_fast (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   Taper before gridding
      !   Only for "visibility in cell" gridding.
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
    end subroutine dofft_fast
  end interface
  !
  interface
    subroutine dofft_slow (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_slow
  end interface
  !
  interface
    subroutine comshi (beam,nx,ny,ix,iy,shift)
      use gbl_message
      !-----------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Shift beam if needed
      !-----------------------------------------------------
      integer, intent(in) :: nx,ny     ! X,Y Size
      integer, intent(in) :: ix,iy     ! Position of maximum
      real, intent(in) :: beam(nx,ny)  ! Beam
      integer, intent(out) :: shift(3) ! Shift information
    end subroutine comshi
  end interface
  !
  interface
    subroutine no_check_mask(method,head)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Dummy replacement for "check_mask"
      !---------------------------------------------------------------------
      type(gildas),    intent(in)    :: head   !
      type(clean_par), intent(inout) :: method !
    end subroutine no_check_mask
  end interface
  !
  interface
    subroutine check_mask(amethod,head)
      use clean_def
      use image_def
      use gbl_message
      use gkernel_types
      use clean_support
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   
      !     Check that the search mask is defined, and computes its
      !     Box boundaries, returned in amethod%box
      !---------------------------------------------------------------------
      type(gildas),    intent(in)    :: head       ! Image   header
      type(clean_par), intent(inout) :: amethod    ! Clean method parameters
    end subroutine check_mask
  end interface
  !
  interface
    subroutine translate (in, nx, ny, trans, ix, iy)
      !-----------------------------------------------------------------------
      ! @ private
      !
      !     Translate the beam to new position IX, IY
      !-----------------------------------------------------------------------
      integer, intent(in) :: nx          ! X size
      integer, intent(in) :: ny          ! Y size
      real, intent(in) :: in(nx,ny)      ! Input beam
      real, intent(out) :: trans(nx,ny)  ! Translated beam
      integer, intent(in) :: ix          ! X shift
      integer, intent(in) :: iy          ! Y shift
    end subroutine translate
  end interface
  !
  interface
    subroutine get_maskplane(amethod,hmask,hdirty,mask,list)
      use clean_def
      use image_def
      use gbl_message
      use clean_support
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define beam plane and check if fit is required
      !---------------------------------------------------------------------
      type (clean_par), intent(inout) :: amethod   ! Clean method parameters
      type (gildas), intent(in) :: hmask           ! Mask Header
      type (gildas), intent(in) :: hdirty          ! Used to figure out which plane
      logical, intent(inout) :: mask(:,:)          ! Current logical mask
      integer, intent(inout) :: list(:)            ! Selected list of pixels
    end subroutine get_maskplane
  end interface
  !
  interface
    subroutine init_flux90 (meth,head,ylimn,ylimp,ipen)
      use clean_def
      use clean_default
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for deconvolution
      !   Create or reuse the <FLUX window
      !---------------------------------------------------------------------
      type (clean_par), intent(in) :: meth
      type (gildas), intent(in)    :: head
      real, intent(in)             :: ylimn, ylimp
      integer, intent(out)         :: ipen
    end subroutine init_flux90
  end interface
  !
  interface
    subroutine next_flux90(niter,cum,is)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for deconvolution
      !   Plot in the <FLUX window
      !---------------------------------------------------------------------
      integer, intent(in) :: is
      integer, intent(in) :: niter
      real, intent(in) :: cum
    end subroutine next_flux90 
  end interface
  !
  interface
    subroutine close_flux90(ipen,error)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Close the <FLUX segments
      !---------------------------------------------------------------------
      integer, intent(in)    :: ipen   ! New pen to be used
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine close_flux90
  end interface
  !
  interface
    subroutine re_mask(method,head,nl,error)
      use clean_def
      use image_def
      use clean_support
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for Clean
      !   Re-compute mask in Major Cycles
      ! OBSOLETE - Unique call in mx_cycle.f90 is commented out
      !---------------------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (gildas), intent(in) :: head
      integer, intent(inout) :: nl
      logical, intent(inout) :: error
    end subroutine re_mask
  end interface
  !
  interface
    subroutine init_plot(method,head,pdata)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Create or reuse the <CLARK window, and draw in it. Then go back
      !   to <FLUX if needed.
      !---------------------------------------------------------------------
      type (clean_par), intent(in) :: method
      type (gildas), intent(in)    :: head
      real, intent(in)             :: pdata(head%gil%dim(1),head%gil%dim(2))
    end subroutine init_plot
  end interface
  !
  interface
    subroutine major_plot90 (method,head   &
         &    ,conv,niter,nx,ny,np,tcc  &
         &    ,clean,resid,poids)
      use clean_def
      use image_def
      use gbl_message
      !---------------------------------------------------------
      ! @ private
      !
      ! MAPPING 
      !   Plot result of Major Cycle
      !---------------------------------------------------------
      type (clean_par), intent(inout) :: method
      type (gildas), intent(inout) :: head
      !
      logical, intent(inout) :: conv        ! Convergence status
      integer, intent(in) ::  niter         ! Number of iterations
      integer, intent(in) ::  nx            ! X size
      integer, intent(in) ::  ny            ! Y size
      integer, intent(in) ::  np            ! Number of planes
      real, intent(inout) :: clean(nx,ny)      ! Clean image
      real, intent(inout) :: resid(nx,ny)      ! Residuals
      real, intent(in) :: poids(nx,ny)         ! Weight image
      type (cct_par), intent(in) :: tcc(niter)
    end subroutine major_plot90
  end interface
  !
  interface
    subroutine plot_mrc(method,head,array,code)
      use image_def
      use clean_def
      !----------------------------------------------------------------
      ! 
      ! @ private
      !
      ! MAPPING
      !   Dispatch the various plotting actions in MRC
      !----------------------------------------------------------------
      type(clean_par), intent(in) :: method
      type(gildas), intent(in) :: head
      integer, intent(in) :: code
      real, intent(in) :: array(head%gil%dim(1),head%gil%dim(2))
    end subroutine  plot_mrc
  end interface
  !
  interface
    subroutine mrc_plot(image,nx,ny,type,name)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Plot the smooth and difference final clean image in <MRC
      !   so as to keep them visible
      !---------------------------------------------------------------------
      integer, intent(in) :: nx,ny,type
      real, intent(in) :: image(nx*ny)
      character(len=*) name
    end subroutine mrc_plot
  end interface
  !
  interface
    subroutine uv_shift_header (new,ra,dec,ang,off,doit)
      use gkernel_types
      !-------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute shift parameters
      !-------------------------------------------------
      real(kind=8), intent(inout) :: new(3)   ! New phase center and angle
      real(kind=8), intent(in) :: ra,dec,ang  ! Old center and angle
      real(kind=8), intent(out) :: off(3)     ! Required Offsets and Angle
      logical, intent(inout) :: doit          ! Is shift Required ? 
    end subroutine uv_shift_header
  end interface
  !
  interface
    subroutine shift_uvdata(hx,nu,nv,visi,cs,nc,xy)
      use image_def
      use gkernel_types
      use phys_const
      !$ use omp_lib
      !-------------------------------------------------------------------
      ! @ private
      !
      !  IMAGER 
      !   Support routine for commands UV_SHIFT and UV_MAP
      !   Shift phase center and apply U,V coordinates rotation if needed
      !   Note that Offsets are not shifted, neither rotated by this
      !   subroutine
      !-------------------------------------------------------------------
      type(gildas), intent(inout) :: hx   ! Input UV header
      integer, intent(in) :: nu           ! Size of a visibility
      integer, intent(in) :: nv           ! Number of visibilities
      real, intent(inout) :: visi(nu,nv)  ! Visibilities
      real, intent(in) :: cs(2)           ! Cos/Sin of Rotation
      integer, intent(in) :: nc           ! Number of Channels
      real(8), intent(in) :: xy(2,nc)     ! Position Shift per channel
    end subroutine shift_uvdata
  end interface
  !
  interface
    subroutine uv_listheader(huv,visi,mt,tf,nt,freq)
      use image_def
      use gbl_message
      !-----------------------------------------------------
      ! @ private
      ! MAPPING
      !
      ! Give a brief summary of the content of an UV Table
      !-----------------------------------------------------
      type(gildas), intent(in) :: huv
      integer, intent(in) :: mt         ! Maximum number of dates
      integer, intent(out) :: nt        ! Number of dates
      integer, intent(out) :: tf(mt)    ! Dates (in Gildas code)
      real(4), intent(in) :: visi(huv%gil%dim(1),huv%gil%dim(2)) ! Visbilities
      real(8), intent(in) :: freq       ! Observing frequency
    end subroutine uv_listheader
  end interface
  !
  interface
    subroutine do2weig (jc,nv,visi,jx,jy,iw,unif,we,wm,nw,taper,s,vv)
      use gildas_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute weights of the visibility points.
      !   Explore here the robust parameters.
      !----------------------------------------------------------------------
      integer, intent(in) :: nv  ! Size of a visibility
      integer, intent(in) :: jc  ! Number of visibilities
      integer, intent(in) :: jx  ! X coordinate pointer
      integer, intent(in) :: jy  ! Y coordinate pointer
      integer, intent(in) :: iw  ! Weight pointer
      integer, intent(in) :: nw  ! Number of weighting schemes
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      real, intent(in) :: unif         ! Cell size in meters
      real, intent(out) :: we(nv,nw)   ! Weight arrays
      real, intent(inout) :: wm(nw)    ! on input: robust factors
      real, intent(in) :: taper(3)     ! Taper
      real, intent(out) :: s           ! Sum of weights (natural noise)
      real, intent(in) :: vv(nv)       ! V values
    end subroutine do2weig
  end interface
  !
  interface
    subroutine do3weig (jc,nv,visi,jx,jy,iw,cunif,we,cwm,nw,taper,s,vv)
      use gildas_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Compute weights of the visibility points.
      !   TAPER mode
      !----------------------------------------------------------------------
      integer, intent(in) :: nv  ! Size of a visibility
      integer, intent(in) :: jc  ! Number of visibilities
      integer, intent(in) :: jx  ! X coordinate pointer
      integer, intent(in) :: jy  ! Y coordinate pointer
      integer, intent(in) :: iw  ! Weight pointer
      integer, intent(in) :: nw  ! Number of weighting schemes
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      real, intent(in) :: cunif(1)     ! Cell size in meters
      real, intent(out) :: we(nv,nw)   ! Weight arrays
      real, intent(in) :: cwm(1)       ! on input: robust factors
      real, intent(in) :: taper(nw)    ! Tapers
      real, intent(out) :: s           ! Sum of weights (natural noise)
      real, intent(in) :: vv(nv)       ! V values
    end subroutine do3weig
  end interface
  !
  interface
    subroutine dodate(nc,nv,visi,id,it)
      use gbl_message
      !-----------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   List the dates
      !-----------------------------------------------------
      integer, intent(in) :: id  ! Date pointer
      integer, intent(in) :: it  ! Time pointer
      integer, intent(in) :: nc  ! Size of a visibility
      integer, intent(in) :: nv  ! Number of visibilities
      real, intent(in) :: visi(nc,nv)
    end subroutine dodate
  end interface
  !
  interface
    subroutine do4weig (jc,nv,visi,jx,jy,iw,unif,we,wm,nw,taper,s,vv)
      use gbl_message
      use grid_control
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for UV_STAT
      !   Compute weights of the visibility points.  CELL mode
      !----------------------------------------------------------------------
      integer, intent(in) :: nv  ! Size of a visibility
      integer, intent(in) :: jc  ! Number of visibilities
      integer, intent(in) :: jx  ! X coordinate pointer
      integer, intent(in) :: jy  ! Y coordinate pointer
      integer, intent(in) :: iw  ! Weight pointer
      integer, intent(in) :: nw  ! Number of weighting schemes
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      real, intent(in) :: unif(nw)     ! Cell size in meters
      real, intent(out) :: we(nv,nw)   ! Weight arrays
      real, intent(inout) :: wm(nw)    ! on input: robust factors
      real, intent(in) :: taper(3)     ! Tapers
      real, intent(out) :: s           ! Sum of weights (natural noise)
      real, intent(in) :: vv(nv)       ! Sorted V<0 values
    end subroutine do4weig
  end interface
  !
  interface
    subroutine sidelo (map,nx,ny,thre,majo,mino,pa,convert)
      !------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for UV_STAT 
      !   Compute sidelobes level
      !------------------------------------------------------------
      integer, intent(in) :: nx,ny        ! X,Y sizes
      real, intent(in) :: map(nx,ny)      ! Beam map
      real, intent(in) :: majo            ! Major axis
      real, intent(in) :: mino            ! Minor axis
      real, intent(in) :: pa              ! PA
      real, intent(out) :: thre(2)        ! Pos and Neg sidelobe
      real(8), intent(in) :: convert(3,2) ! Conversion formula
    end subroutine sidelo
  end interface
  !
  interface
    subroutine do0weig (jc,nv,visi,jx,jy,jw,unif,we,vv)
      use gildas_def
      use gbl_message
      use grid_control
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for UV_STAT
      !   Compute weights of the visibility points.
      !   Use gridded or non-gridded version according to 
      !   grid_bigvisi, i.e. Gridding%Big
      !----------------------------------------------------------------------
      integer, intent(in) :: nv  ! Size of a visibility
      integer, intent(in) :: jc  ! Number of visibilities
      integer, intent(in) :: jx  ! X coordinate pointer
      integer, intent(in) :: jy  ! Y coordinate pointer
      integer, intent(in) :: jw  ! Weight pointer
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      real, intent(in) :: unif         ! Cell size in meters
      real, intent(out) :: we(nv)      ! Weight arrays
      real, intent(in) :: vv(nv)       ! V values
    end subroutine do0weig
  end interface
  !
  interface
    subroutine do2fft (np,nv,visi,jx,jy,nx,ny,nw,map,mapx,mapy,we) 
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for UV_STAT
      !   Compute FFT of image by gridding UV data for several weights
      !   at once. Uses simple in-cell gridding
      !----------------------------------------------------------------------
      ! Call
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      integer, intent(in) :: nw                   ! number of weight schemes
      integer, intent(in) :: nx,ny                ! map size
      integer, intent(in) :: jx,jy                ! X coord, Y coord location in VISI
      real, intent(in) :: visi(np,nv)             ! values
      complex, intent(out) :: map(nw,nx,ny)       ! gridded visibilities
      real, intent(in) :: mapx(nx),mapy(ny)       ! Coordinates of grid
      real, intent(in) :: we(nv,nw)               ! Weight array
    end subroutine do2fft
  end interface
  !
  interface
    subroutine prnoise(prog,which,noise,rms)
      use gbl_message
      !--------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Printout the noise value
      !--------------------------------------------------
      character(len=*), intent(in) :: prog  ! Caller name
      character(len=*), intent(in) :: which ! Type of image
      real, intent(in) :: noise             ! Noise value
      real, intent(out) :: rms              ! Unit of noise
    end subroutine prnoise
  end interface
  !
  interface
    subroutine doqfft (np,nv,visi,jx,jy,jw   &
         &    ,nx,ny,map,weight,uvcell)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support for UV_STAT    (Quick Gridding)
      !   Compute FFT of beam by gridding UV data for Natural
      !   weighting only. Uses simple in-cell gridding for speed
      !----------------------------------------------------------------------
      ! Call
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      integer, intent(in) :: nx,ny                ! map size
      integer, intent(in) :: jx,jy,jw             ! X coord, Y coord & Weight location in VISI
      real, intent(in) :: visi(np,nv)             ! values
      complex, intent(out) :: map(nx,ny)          ! gridded visibilities
      real, intent(in) :: weight(nv)              ! Weights
      real, intent(in) :: uvcell                  ! UV cell size 
    end subroutine doqfft
  end interface
  !
  interface
    subroutine sphfn (ialf, im, iflag, eta, psi, ier)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !     SPHFN is a subroutine to evaluate rational approximations to se-
      !  lected zero-order spheroidal functions, psi(c,eta), which are, in a
      !  sense defined in VLA Scientific Memorandum No. 132, optimal for
      !  gridding interferometer data.  The approximations are taken from
      !  VLA Computer Memorandum No. 156.  The parameter c is related to the
      !  support width, m, of the convoluting function according to c=
      !  pi*m/2.  The parameter alpha determines a weight function in the
      !  definition of the criterion by which the function is optimal.
      !  SPHFN incorporates approximations to 25 of the spheroidal func-
      !  tions, corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
      !  and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
      !
      !  Input:
      !    IALF    I*4   Selects the weighting exponent, alpha.  IALF =
      !                  1, 2, 3, 4, and 5 correspond, respectively, to
      !                  alpha = 0, 1/2, 1, 3/2, and 2.
      !    IM      I*4   Selects the support width m, (=IM) and, correspond-
      !                  ingly, the parameter c of the spheroidal function.
      !                  Only the choices 4, 5, 6, 7, and 8 are allowed.
      !    IFLAG   I*4   Chooses whether the spheroidal function itself, or
      !                  its Fourier transform, is to be approximated.  The
      !                  latter is appropriate for gridding, and the former
      !                  for the u-v plane convolution.  The two differ on-
      !                  by a factor (1-eta**2)**alpha.  IFLAG less than or
      !                  equal to zero chooses the function appropriate for
      !                  gridding, and IFLAG positive chooses its F.T.
      !    ETA     R*4   Eta, as the argument of the spheroidal function, is
      !                  a variable which ranges from 0 at the center of the
      !                  convoluting function to 1 at its edge (also from 0
      !                  at the center of the gridding correction function
      !                  to unity at the edge of the map).
      !
      !  Output:
      !    PSI      R*4  The function value which, on entry to the subrou-
      !                  tine, was to have been computed.
      !    IER      I*4  An error flag whose meaning is as follows:
      !                     IER = 0  =>  No evident problem.
      !                           1  =>  IALF is outside the allowed range.
      !                           2  =>  IM is outside of the allowed range.
      !                           3  =>  ETA is larger than 1 in absolute
      !                                     value.
      !                          12  =>  IALF and IM are out of bounds.
      !                          13  =>  IALF and ETA are both illegal.
      !                          23  =>  IM and ETA are both illegal.
      !                         123  =>  IALF, IM, and ETA all are illegal.
      !
      !---------------------------------------------------------------------
      integer(4), intent(in)  :: ialf     ! Exponent
      integer(4), intent(in)  :: im       ! Width of support
      integer(4), intent(in)  :: iflag    ! Gridding function
      real(4), intent(in)     :: eta      ! For spheroidals only
      real(4), intent(out)    :: psi      ! Result
      integer(4), intent(out) :: ier      ! Error code
    end subroutine sphfn
  end interface
  !
  interface
    subroutine dowfact(nv,we,wfact)
      use gildas_def
      use grid_control
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute renormalisation factor WFACT for Robust Weighting
      !---------------------------------------------------------------------
      integer , intent(in) :: nv  ! Number of visibilities
      real, intent(inout)  :: we(nv)  ! Weights
      real, intent(out) :: wfact  ! Normalisation factor
    end subroutine dowfact
  end interface
  !
  interface
    subroutine dogrid_smooth (nx,ny,mapn,maps,unif)!
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  
      !   Smooth the natural weights to get the local weight density.
      !----------------------------------------------------------------------
      integer, intent(in) :: nx
      integer, intent(in) :: ny
      real, intent(in)  :: mapn(nx,ny)
      real, intent(out) :: maps(nx,ny)
      real, intent(in) :: unif          ! In pixel units
    end subroutine dogrid_smooth
  end interface
  !
  interface
    subroutine doweig_robust(jc,nv,visi,jw,we,wm)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  
      !   Correct the weights from the local density according to the
      !   Robustness factor.
      !----------------------------------------------------------------------
      integer, intent(in) :: jc ! Size of a visibility
      integer, intent(in) :: nv ! Number of visibilities
      real, intent(in) :: visi(jc,nv)  ! Visibilities
      integer, intent(in) :: jw        ! Weight column
      real, intent(inout) :: we(nv)    ! Weights
      real, intent(in) :: wm           ! Robust factor
    end subroutine doweig_robust
  end interface
  !
  interface
    subroutine dogrid_fast (np,nv,visi,jx,jy   &
         &    ,nx,ny,map,mapx,mapy,we)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  
      !   Grid the weights. Do not apply taper 
      !   Only for "visibility in cell" gridding.
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! Number of visibilies
      integer, intent(in) :: np                   ! Size of a visibility 
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(nx,ny)             ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: we(nv)                  ! Weight array
    end subroutine dogrid_fast
  end interface
  !
  interface
    subroutine doweig_getsum (jc,nv,visi,jx,jy,nx,ny,w_mapu,w_mapv,maps,we)
      ! @ private
      integer, intent(in) ::  nv          ! number of visibilities
      integer, intent(in) ::  jc          ! Size of a visibilities
      integer, intent(in) ::  jx          ! X coord location in VISI
      integer, intent(in) ::  jy          ! Y coord location in VISI
      integer, intent(in) ::  nx          ! Grid size in X
      integer, intent(in) ::  ny          ! Grid size in Y
      real, intent(in) ::  visi(jc,nv)    ! Visibilities
      real, intent(in) :: w_mapu(nx)      ! X coordinates
      real, intent(in) :: w_mapv(ny)      ! Y coordinates
      real, intent(in) :: maps(nx,ny)     ! Smoothed gridded weights
      real, intent(inout) ::  we(nv)      ! Weight array
    end subroutine doweig_getsum
  end interface
  !
  interface
    subroutine gridless_density (npts,sizecell,distmax,evex,evey,eveweight,&
      & evesumweight,xmin,xmax,ymin,ymax,error)
      use gildas_def
      use gbl_message
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for Robust weighting
      !
      ! Stephane Paulin   OASU CNRS / U.Bordeaux  2016
      !
      ! Algorithm to compute the weighted neighborhood of each point in a large
      ! catalog. This algorithm was described, among other sources, by Daniel Briggs in
      ! his thesis. The idea is to avoid npts**2 calculations of the distance between
      ! each pair of points (npts is the # points). For that, one defines a grid only used
      ! to speed-up computation (result is independent of the grid).
      ! Step 1: In a given cell of the grid, all events are linked and the total weight
      !   of the cell is computed and kept in memory.
      ! Step 2: a pattern is computed, that is the same all over the field, whose radius
      !   is the maximum distance. Grid boxes can have 3 states: entirely inside the
      !   maximum distance, entirely outside, or in between (boxes noted 'intermediate'
      !   or 'mixed').
      ! The idea is to compute the distances only for events in these intermediate boxes.
      ! For other events, or the total weight of the box is added (if the box is inside),
      ! or the box is ignored (if outside). Next, the main loop consists on a loop on every
      ! event. For a given event, all boxes in the pattern are checked and treated accordingly.
      ! With this algorithm, one can have CPU times prop. to factor*npts*log(npts) with
      ! factor~2 (ie. when npts is multiplied by 10, the CPU is multiplied by ~45-50)
      !
      ! Remarks/notes:
      ! - this is a preliminary approach for testing, where symmetries are not
      !   taken into account !!! ==> a given couple of coordinates (evex,evey) corresponds
      !   to one event
      !
      ! Changes:
      !   Stephane Guilloteau   Parallel programming Mar-2019
      !                         Optimize parallel programming Oct-2022
      !----------------------------------------------------------------------
      real, intent(in) :: sizecell                ! size of a cell.
      ! this is a control parameter that does not change the result but
      ! change the calculation time: to minimize the CPU, the larger the
      ! event density, the smaller sizecell. ex: sizecell~20 (10, 5 resp.)
      ! for a catalog of 10**5 (10**6, 3x10**7) events
      integer, intent(in) :: npts                 ! total # of events
      real, intent(in) :: distmax                 ! maximum distance
      real(kind=4), intent(in) :: evex(npts)      ! X coordinates
      real(kind=4), intent(in) :: evey(npts)      ! Y coordinates
      real(kind=4), intent(in) :: eveweight(npts) ! Weight of event
      real(kind=4), intent(out) :: evesumweight(npts) ! Sum of the weights of all
      ! the events closer nearer than the maximum distance from the current event.
      real(kind=4), intent(in) :: xmin,ymin,xmax,ymax ! Min Max
      logical, intent(out) :: error               ! Error flag
    end subroutine gridless_density
  end interface
  !
  interface
    subroutine chkfft (a,nx,ny,error)
      !---------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Check if FFT is centered...
      !---------------------------------------------------------
      integer, intent(in)  ::  nx,ny   ! X,Y size
      logical, intent(out)  ::  error  ! Error flag
      real, intent(in)  ::  a(nx,ny)   ! Array
    end subroutine chkfft
  end interface
  !
  interface
    subroutine doweig_quick (jc,nv,visi,jx,jy,jw,unif,we,wm,vv,mv, &
         &     umin,umax,vmin,vmax,nbcv)
      use gildas_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Compute weights of the visibility points.
      !----------------------------------------------------------------------
      integer, intent(in) ::  nv          ! number of values
      integer, intent(in) ::  jc          ! Number of "visibilities"
      integer, intent(in) ::  jx          ! X coord location in VISI
      integer, intent(in) ::  jy          ! Y coord location in VISI
      integer, intent(in) ::  jw          ! Location of weights. If .LE.0, uniform weight
      integer, intent(in) ::  mv          ! Size of work arrays
      integer, intent(in) ::  nbcv        ! Buffering factor
      real, intent(in) ::  visi(jc,nv)    ! Visibilities
      real, intent(in) ::  unif           ! uniform cell size in Meters
      real, intent(inout) ::  we(nv)      ! Weight array
      real, intent(in) ::  wm             ! on input: % of uniformity
      real, intent(in) ::  vv(nv)         ! V values, pre-sorted
      real, intent(in) :: umin,umax,vmin,vmax ! UV boundaries
    end subroutine doweig_quick
  end interface
  !
  interface
    subroutine doweig_sub (nv,uu,vv,ww,we,unif)
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Compute weights of the visibility points.
      !----------------------------------------------------------------------
      integer, intent(in) :: nv          ! number of values
      real, intent(in) ::  uu(nv)        ! U coordinates
      real, intent(in) ::  vv(nv)        ! V coordinates
      real, intent(in) ::  ww(nv)        ! Input Weights
      real, intent(out) ::  we(nv)       ! Output weights
      real, intent(in) ::  unif          ! Cell size
    end subroutine doweig_sub
  end interface
  !
  interface
    subroutine doweig_slow (jc,nv,visi,jx,jy,jw,unif,we,wm)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER & MAPPING
      !     Compute weights of the visibility points.
      !   Stupid, slow, version kept here for reference, but unused
      !----------------------------------------------------------------------
      integer, intent(in) ::  nv          ! number of values
      integer, intent(in) ::  jc          ! Number of "visibilities"
      integer, intent(in) ::  jx          ! X coord location in VISI
      integer, intent(in) ::  jy          ! Y coord location in VISI
      integer, intent(in) ::  jw          ! Location of weights. If .LE.0, uniform weight
      real, intent(in) ::  visi(jc,nv)    ! Visibilities
      real, intent(in) ::  unif           ! uniform cell size in Meters
      real, intent(inout) ::  we(nv)      ! Weight array
      real, intent(in) ::  wm             ! on input: % of uniformity
    end subroutine doweig_slow
  end interface
  !
  interface
    subroutine dowei_icode(umax,umin,vmax,vmin,unif,mv,icode)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Get the sub-division for the UV cells for Robust weighting
      !---------------------------------------------------------------------
      real, intent(in) :: umax,umin     ! U range
      real, intent(in) :: vmax,vmin     ! V range
      real, intent(in) :: unif          ! Uniform cell size
      integer, intent(in) :: mv         ! Number of visibilities
      integer, intent(inout) :: icode   ! Number of sub-cells
    end subroutine dowei_icode
  end interface
  !
  interface
    subroutine uv_select_buffer(rname,nu,nv,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      !   Select the next available UV buffer (UVR or UVS) for
      ! commands using them.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname  ! Caller name
      integer, intent(in) :: nu              ! Visibility size
      integer, intent(in) :: nv              ! Number of visibilities
      logical, intent(out) :: error          ! Error flag
    end subroutine uv_select_buffer
  end interface
  !
  interface
    subroutine uv_reset_buffer(rname)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Reset pointers to UV datasets (back to UVI). Deallocate
      ! UVR or UVS if needed, as well as the "transposed" buffer UVT
      !---------------------------------------------------------------------
      character(len=*) :: rname
    end subroutine uv_reset_buffer
  end interface
  !
  interface
    subroutine uv_dump_buffers(rname)
      use clean_def
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Dump the allocation status of the UV buffers. (Debugging only)
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname  ! Caller name
    end subroutine uv_dump_buffers
  end interface
  !
  interface
    subroutine uv_find_buffers (rname,nu,nv,duv_previous, duv_next,error)
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Find the next available UV buffer (UVR or UVS).
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname  ! Caller name
      integer, intent(in) :: nu   ! Size of a visiblity
      integer, intent(in) :: nv   ! Number of visibilities
      real, pointer, intent(out) :: duv_previous(:,:)
      real, pointer, intent(out) :: duv_next(:,:)
      logical, intent(out) :: error
    end subroutine uv_find_buffers
  end interface
  !
  interface
    subroutine uv_clean_buffers(duv_previous,duv_next,error)
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Take care of freeing the unused UV buffer, and set UV to point to
      ! the new one.
      !---------------------------------------------------------------------
      real, pointer, intent(inout) :: duv_previous(:,:)
      real, pointer, intent(inout) :: duv_next(:,:)
      logical, intent(in) :: error
    end subroutine uv_clean_buffers
  end interface
  !
  interface
    subroutine uv_discard_buffers(duv_previous,duv_next,error)
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Take care of freeing the last used UV buffer, and reset 
      !   UV to point to the previous one.
      !---------------------------------------------------------------------
      real, pointer, intent(inout) :: duv_previous(:,:)
      real, pointer, intent(inout) :: duv_next(:,:)
      logical, intent(in) :: error
    end subroutine uv_discard_buffers
  end interface
  !
  interface
    subroutine uv_new_data (weight,resample)
      use clean_arrays
      use clean_types
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Internal routine
      !
      ! Declare that the UV data has been modified 
      ! so that 
      ! - the sorting must be redone
      ! - the transposed UV data is no longer consistent
      ! - Data should be read again when asked by the user
      ! - (optionally) Weights should be re-computed by UV_MAP
      ! - and SIC variables must be re-defined
      !---------------------------------------------------------------------
      logical, intent(in), optional :: weight
      logical, intent(in), optional :: resample
    end subroutine uv_new_data
  end interface
  !
  interface
    subroutine map_uvgildas(name,uvh,error,duv)
      use image_def
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      !   IMAGER
      !
      !   (re-)Define the NAME% SIC header variable and corresponding
      !   NAME data area.
      !
      !   Also define additional variables in the NAME% SIC variable
      !   to handle the pointers towards extra columns for UV data.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name             ! SIC variable name
      type(gildas), intent(inout), target :: uvh       ! Header
      logical, intent(inout) :: error                  ! Error return 
      real, intent(in), target :: duv(:,:)             ! Data area
    end subroutine map_uvgildas  
  end interface
  !
  interface
    subroutine uv_check_comm(line,error)
      use gbl_message
      use clean_types
      use clean_arrays
      use clean_beams
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for command
      !     UV_CHECK [Beams|Nulls|Integration] [/FILE File]
      !----------------------------------------------------------------------
      logical, intent(out) :: error ! Logical error flag
      character(len=*), intent(in) :: line
    end subroutine uv_check_comm
  end interface
  !
  interface
    subroutine sub_get_inte(huv,duv,nv,dmax,dtol,inte,kv,error)
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for command
      !     UV_CHECK Integration [/FILE File]
      ! Attempt to derive integration times, by differencing time stamps
      ! of consecutive visibvilities.
      !----------------------------------------------------------------------
      type(gildas), intent(in) :: huv   ! UV data header
      real, intent(in) :: duv(:,:)      ! UV data
      integer, intent(in) :: nv         ! Number of visibilities
      real, intent(inout) :: dmax       ! Longest allowed time step
      real, intent(in) :: dtol          ! Time stamp tolerance
      real, intent(inout) :: inte(:)    ! Possible integration times
      integer, intent(inout) :: kv      ! Number of possible times
      logical, intent(out) :: error
    end subroutine sub_get_inte
  end interface
  !
  interface
    subroutine sub_get_nulls(duv,nv,nc,bad,kv)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for command
      !     UV_CHECK Nulls [/FILE File]
      !  Find out Null visibilities with non zero weights
      ! (occasionally happens with CASA data)
      !----------------------------------------------------------------------
      real, intent(inout) :: duv(:,:)
      integer, intent(in) :: nv, nc
      integer, intent(inout) :: bad(0:nv)
      integer, intent(inout) :: kv
    end subroutine sub_get_nulls
  end interface
  !
  interface
    subroutine check_beams_mem(error)
      use clean_beams
      use clean_arrays
      use gbl_message
      ! @ private
      logical, intent(inout) :: error  
    end subroutine check_beams_mem
  end interface
  !
  interface
    subroutine display_check_uv(comm,chain,error)
      use clean_def
      use clean_arrays
      use clean_default
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for commands 
      !     SHOW UV and UV_FLAG
      !
      !   TIME-BASE sort the UV table before displaying it
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: comm 
      character(len=*), intent(inout) :: chain
      logical, intent(out) :: error ! Logical error flag
    end subroutine display_check_uv
  end interface
  !
  interface
    subroutine select_uvdata(line,comm,error)
      use clean_default
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! IMAGER
      !
      ! @ private
      !
      !   Support for command
      !       UV_DATA [NewOne]
      !   Select the UV_DATA to be Imaged (and Plotted ?)
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine select_uvdata
  end interface
  !
  interface
    subroutine display_uv(chain,line,error)
      use clean_def
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     SHOW UV
      !   TIME-BASE sort and transpose the UV data and display it.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: chain
      character(len=*), intent(in) :: line
      logical, intent(out) :: error ! Logical error flag
    end subroutine display_uv
  end interface
  !
  interface
    subroutine uv_sort_comm(line,error)
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Support routine for command
      !   UV_SORT [TIME|BASE|UV]
      ! Sort the UV data in the specified order
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Logical error flag
    end subroutine uv_sort_comm
  end interface
  !
  interface
    subroutine uv_tri(code,huvin,duvin,error)
      use clean_def
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Support routine for command
      !   UV_SORT [TIME|BASE|UV]
      !
      !     BASE is code 1
      !     TIME is code 2
      !     UV is code 3
      !---------------------------------------------------------------------
      integer, intent(in)  :: code  ! Sorting code
      type(gildas), intent(in) :: huvin   ! UV data to be sorted
      real, intent(in) :: duvin(huvin%gil%dim(1),huvin%gil%dim(2))
      logical, intent(out) :: error ! Logical error flag
    end subroutine uv_tri
  end interface
  !
  interface
    subroutine uv_findtb(code,uv,mv,nv,order,it,ot,sorted)
      !---------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support for SHOW UV
      !---------------------------------------------------------------
      integer, intent(in)       :: code      ! Code for sort operation
      integer, intent(in)       :: mv,nv     ! Size of visibility table
      real, intent(in)          :: uv(mv,nv) ! Input UV data
      real(kind=8), intent(out) :: order(nv) ! Sorting array
      integer, intent(out)      :: it(nv)    ! Sort index
      integer, intent(out)      :: ot(nv)    ! Reverse order
      logical, intent(out)      :: sorted    ! Is already sorted
    end subroutine uv_findtb
  end interface
  !
  interface
    subroutine triuv8(x,it,n,error)
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      !   Sorting program that uses a quicksort algorithm.
      ! Applies for an input array of real*8 values, which are left
      ! unchanged. Returns an array of indexes sorted for increasing
      ! order of X. Use GR8_SORT to reorder X.
      !------------------------------------------------------------------------
      integer,      intent(in)    :: n     ! Array length
      real(kind=8), intent(inout) :: x(n)  ! Unsorted array
      integer,      intent(out)   :: it(n) ! Sort index
      logical,      intent(out)   :: error ! Logical return flag
    end subroutine triuv8
  end interface
  !
  interface
    subroutine flux_dispatch (line,error)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Dispatching routine for command
      !  SCALE_FLUX FIND [DateInterval [MinBase MaxBase]]
      !  SCALE_FLUX APPLY OutputVariable
      !  SCALE_FLUX CALIBRATE
      !  SCALE_FLUX LIST
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_dispatch
  end interface
  !
  interface
    subroutine flux_apply  (line,error)
      use gildas_def
      use clean_arrays
      use gbl_message
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !   SCALE_FLUX APPLY OutputVariable
      !-----------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_apply
  end interface
  !
  interface
    subroutine flux_calib (line,error)
      use gildas_def
      use clean_arrays
      use gbl_message
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Support for command
      !  SCALE_FLUX CALIBRATE 
      !
      ! Apply the factors to the UV data set..
      !-----------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_calib
  end interface
  !
  interface
    subroutine flux_find (line,error)
      use gildas_def
      use clean_arrays
      use gbl_message
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Support routine for command
      !  SCALE_FLUX FIND [DateInterval [MinBase MaxBase]]
      !
      !  Scan the current UV table (obtained by READ UV) to determine
      !  how many independent dates exist
      !
      !  Compare it date by date with the model UV table (obtained by 
      !  READ MODEL or MODEl commands) and compute, through linear 
      !  regression, the best scaling factors to match the two tables
      !
      !  Return these flux factors as variables
      !  SCALE_FLUX and D_FLUX
      !-----------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_find
  end interface
  !
  interface
    subroutine flux_list (line,error)
      use gbl_message
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Support routine for command
      !  SCALE_FLUX LIST 
      !
      !  Printout the latest results from SCALE_FLUX FIND
      !-----------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_list
  end interface
  !
  interface
    subroutine flux_factor (hduv,duv,hcuv,cuv,date_spacing,uvmin2,uvmax2,error)
      use image_def
      use gbl_message
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   SCALE_FLUX FIND or SOLVE
      ! Find the flux scale factors for the Observations
      !
      ! The observations are defined by  Obs = Factor * Model + noise
      ! so the calibration factor to apply is the inverse of this solution
      !-----------------------------------------------------------
      real(kind=4), intent(in) :: date_spacing
      type (gildas), intent(in) :: hduv
      type (gildas), intent(in) :: hcuv
      real(kind=4), intent(in) :: duv(hduv%gil%dim(1),hduv%gil%dim(2))
      real(kind=4), intent(in) :: cuv(hcuv%gil%dim(1),hcuv%gil%dim(2))
      real(kind=4), intent(in) :: uvmin2 ! Min baseline ^ 2
      real(kind=4), intent(in) :: uvmax2 ! Max baseline ^ 2
      logical, intent(out) :: error
    end subroutine flux_factor
  end interface
  !
  interface
    subroutine my_finddat(nc,nv,visi,rtol,nt) 
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Find how many dates
      !-----------------------------------------------------------
      integer, intent(in) :: nc  ! Visibility size 
      integer, intent(in) :: nv  ! Number of visibilities
      integer, intent(out) :: nt ! Number of time stamps
      real, intent(in) :: rtol  ! Tolerance to check dates 
      real, intent(in) :: visi(nc,nv)   ! Visibilities
    end subroutine my_finddat
  end interface
  !
  interface
    subroutine my_listdat(nc,nv,visi,nt,tf,rtol,chain) 
      !-----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   List the different dates 
      !-----------------------------------------------------------
      integer, intent(in) :: nc  ! Visibility size 
      integer, intent(in) :: nv  ! Number of visibilities
      integer, intent(in) :: nt  ! Number of time stamps
      integer, intent(out) :: tf(nt)              ! Time stamp values
      character(len=*), intent(out) :: chain(nt)  ! Associated message
      real, intent(in) :: rtol  ! Tolerance to check dates 
      real, intent(in) :: visi(nc,nv)   ! Visibilities
    end subroutine my_listdat
  end interface
  !
  interface
    subroutine uv_line2cont(line,error)
      use clean_def
      use clean_default
      use clean_types
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  Support routine for command 
      !     UV_CONT [Step] [/RANGE Start End TYPE] /INDEX Alpha [Frequency]
      !
      ! Create a continuum UV table from a Line one
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
    end subroutine uv_line2cont
  end interface
  !
  interface
    subroutine sub_uvcont_header(line,error,hcuv,channels,o_step,o_index,o_range)
      use clean_def
      use clean_default
      use clean_types
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  Support routine for commands 
      !   UV_CONT [Step] /INDEX Alpha [Frequency] [/RANGE Min Max Type]
      ! and
      !   UV_MAP [Map Center Args] /CONT [Step] /INDEX Alpha [Frequency] [/RANGE Min Max Type]
      !
      ! Create a continuum UV table from the Line one
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
      type (gildas), intent(inout) :: hcuv     ! UV header
      integer, intent(out) :: channels(3)      ! Channels (first, last, step)
      integer, intent(in) :: o_step            ! Location of channel step
      integer, intent(in) :: o_index           ! Location of /INDEX option
      integer, intent(in) :: o_range           ! Location of /RANGE option
    end subroutine sub_uvcont_header
  end interface
  !
  interface
    subroutine uv_cont_header(rname,hluv,hcuv,channels,error)
      use gbl_message
      use image_def
      !-----------------------------------------------------------------------------
      ! @ private
      !   Derive the Continuum UV header from the Line UV header
      !   once the channel range has been specified
      !-----------------------------------------------------------------------------
      character(len=*), intent(in) :: rname ! Caller name
      type(gildas), intent(in) :: hluv      ! Line UV header
      type(gildas), intent(inout) :: hcuv   ! Continuum UV header
      integer, intent(in) :: channels(3)    ! Channel range
      logical, intent(out) :: error
    end subroutine uv_cont_header
  end interface
  !
  interface
    subroutine sub_uvcont_data(line,hcuv,hiuv,channels,chflag,o_index,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      !   IMAGER
      !     Transform the Line UV data into a Continuum UV data
      !---------------------------------------------------------------------  
      character(len=*), intent(in) :: line
      type(gildas), intent(inout) :: hcuv
      type(gildas), intent(inout) :: hiuv
      integer, intent(in) :: channels(3)
      integer, intent(in) :: chflag(:)
      integer, intent(in) :: o_index
      logical, intent(inout) :: error
    end subroutine sub_uvcont_data
  end interface
  !
  interface
    subroutine opt_filter_base(line,rname,error)
      use uvsplit_mod
      use clean_def
      use clean_arrays
      use clean_default
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  support routine for
      !   UV_FILTER  /CHANNEL ListVariable [/ZERO]
      !   UV_FILTER  /FREQUENCIES ListFreq /WIDTH Width [UNIT]
      !   UV_FILTER  /VELOCITY ListVelo /WIDTH Width [UNIT]
      !   UV_FILTER  /RANGE Min Max [TYPE]
      !      [/FILE FileIn [FileOut]]
      !
      ! Parse the /FILE option
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      character(len=*), intent(in) :: rname    ! Command name
      logical, intent(out) :: error            ! Error flag
    end subroutine opt_filter_base
  end interface
  !
  interface
    subroutine uv_split_comm(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for commands
      !   UV_SPLIT [Degree] /CHANNEL ListVariable [/ZERO]
      !   UV_SPLIT [Degree] /FREQUENCIES ListFreq /WIDTH Width
      !   UV_SPLIT [Degree] /VELOCITIES ListVelo /WIDTH Width
      !   UV_SPLIT [Degree] /RANGE Min Max [TYPE]
      !      /FILE FileIn [FileLine [FileCont]
      !
      ! Subtract a continuum baseline, ignoring a list of channels
      ! in UV data set. Create two output UV tables from this:
      !   - a line free one
      !   - a continuum free one
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
    end subroutine uv_split_comm
  end interface
  !
  interface
    subroutine uv_baseline(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER support routine for commands
      !   UV_BASELINE [Degree] /CHANNEL ListVariable [/ZERO]
      !   UV_BASELINE [Degree] /FREQUENCIES ListFreq /WIDTH Width
      !   UV_BASELINE [Degree] /VELOCITIES ListVelo /WIDTH Width
      !   UV_BASELINE [Degree] /RANGE Min Max [TYPE]
      !      [/FILE FileIn [FileOut]]
      !
      ! Subtract a continuum baseline, ignoring a list of channels
      ! in the current UV data set.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
    end subroutine uv_baseline
  end interface
  !
  interface
    subroutine uv_filter(line,error)
      use gbl_message
      use uvsplit_mod
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  support routine for
      !   UV_FILTER  /CHANNEL ListVariable [/ZERO]
      !   UV_FILTER  /FREQUENCIES ListFreq /WIDTH Width [UNIT]
      !   UV_FILTER  /VELOCITY ListVelo /WIDTH Width [UNIT]
      !   UV_FILTER  /RANGE Min Max [TYPE]
      !      [/FILE FileIn [FileOut]]
      !
      ! "Filter", i.e. flag, a list of channels in the current UV
      ! data set. Flagging is reversible, unless the /ZERO option is
      ! present. With /ZERO, the "filtered" visibilities are set to zero.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
    end subroutine uv_filter
  end interface
  !
  interface
    subroutine uv_filter_base(line,error,rname,t_routine,zero)
      use gildas_def
      use gkernel_types
      use gbl_format
      use clean_types
      use clean_arrays
      use uvsplit_mod
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  support for
      !   UV_FILTER or UV_BASELINE  /CHANNEL ListVariable [/ZERO]
      !   UV_FILTER or UV_BASELINE  /FREQUENCIES ListFreq /WIDTH Width [UNIT]
      !   UV_FILTER or UV_BASELINE  /VELOCITY ListVelo /WIDTH Width [UNIT]
      !   UV_FILTER or UV_BASELINE  /RANGE Min Max [TYPE]
      !      [/FILE FileIn [FileOut]]
      !
      ! "Filter", i.e. flag, a list of channels in the current UV
      ! data set. Flagging is reversible, using the /RESET option
      ! With /ZERO, the "filtered" visibilities are set to zero.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out) :: error            ! Error flag
      external :: t_routine
      character(len=*), intent(in) :: rname
      integer, intent(in) :: zero
    end subroutine uv_filter_base
  end interface
  !
  interface
    subroutine t_filter(mf,filter,zero,error)
      use gildas_def
      use image_def
      use clean_arrays
      use uvsplit_mod
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for UV_FILTER
      !
      !   Filter / Flag a list of channels
      !---------------------------------------------------------------------
      integer, intent(in) :: mf          ! Number of values
      integer, intent(in) :: filter(mf)  ! Channel list
      integer, intent(in) :: zero        ! Zero or not
      logical, intent(out) :: error
    end subroutine t_filter
  end interface
  !
  interface
    subroutine filter_line(nc,nf,nv,duv,filtre,zero)
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support routine for UV_FILTER 
      !
      !   Flag or set to Zero the line regions
      !---------------------------------------------------------------------
      integer, intent(in) :: nf ! Number of good channels
      integer, intent(in) :: nv ! Number of visibilities
      integer, intent(in) :: nc ! Number of channels
      integer, intent(in) :: zero  ! Set to zero or Flag ?
      integer, intent(in) :: filtre(nf)
      real, intent(inout) :: duv(:,:)
    end subroutine filter_line
  end interface
  !
  interface
    subroutine t_baseline(mf,filter,degree,error)
      use gildas_def
      use image_def
      use clean_arrays
      use uvsplit_mod
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support routine for UV_BASELINE
      !
      !    Subtract a baseline with a list of channels to be ignored
      !---------------------------------------------------------------------
      integer, intent(in) :: mf          ! Number of values
      integer, intent(in) :: filter(mf)  ! Channel list
      integer, intent(in) :: degree      ! Polynomial degree
      logical, intent(out) :: error
    end subroutine t_baseline
  end interface
  !
  interface
    subroutine remove_base(nf,nv,nc,duv,degree,filtre,nb,filter,error)
      !---------------------------------------------------------------------
      ! @ private-mandatory  
      !
      ! IMAGER
      !   Support routine for UV_BASELINE
      !
      !   Remove a baseline in a UV data
      !---------------------------------------------------------------------
      integer, intent(in) :: nf ! Number of good channels
      integer, intent(in) :: nv ! Number of visibilities
      integer, intent(in) :: nc ! Number of channels
      integer, intent(in) :: degree ! Degree of polynomial
      integer, intent(in) :: filtre(:)    ! The channels to be considered
      integer, intent(in) :: nb           ! Number of filtered channels
      integer, intent(in) :: filter(:)    ! The filtered channels
      real, intent(inout) :: duv(:,:)
      logical, intent(out) :: error
    end subroutine remove_base
  end interface
  !
  interface
    subroutine t_split(mf,filter,degree,error)
      use gildas_def
      use image_def
      use clean_arrays
      use gbl_message
      use uvsplit_mod
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support routine for UV_SPLIT
      !
      !    Subtract a baseline with a list of channels to be ignored
      !---------------------------------------------------------------------
      integer, intent(in) :: mf          ! Number of values
      integer, intent(in) :: filter(mf)  ! Channel list
      integer, intent(in) :: degree      ! Polynomial degree
      logical, intent(out) :: error
    end subroutine t_split
  end interface
  !
  interface
    subroutine split_base_line(nf,nv,nc,duv,mc,cuv,degree,filtre,error)
      use uvsplit_mod
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support routine for UV_SPLIT
      !
      !   Remove a baseline in a UV data, and produce the
      !   Continuum free and Line free UV tables from it.
      !   The Spectral Windows can be indicated by the options, including
      !   the default one derived by UV_PREVIEW.
      !---------------------------------------------------------------------
      integer, intent(in) :: nf         ! Number of good channels
      integer, intent(in) :: nv         ! Number of visibilities
      integer, intent(in) :: nc         ! Number of line channels
      integer, intent(in) :: mc         ! Number of continuum channels
      integer, intent(in) :: degree     ! Degree of polynomial
      integer, intent(in) :: filtre(nf) ! Channels with line
      real, intent(inout) :: duv(:,:)   ! Input line + continuum, output line
      real, intent(out) :: cuv(:,:)     ! Output continuum
      logical, intent(out) :: error
    end subroutine split_base_line
  end interface
  !
  interface
    subroutine old_uvmap(task,line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! "Historical" version of UV_MAP, deprecated in IMAGER,
      ! only used for debugging and comparison.
      !
      ! TASK  Compute a map from a CLIC UV Sorted Table
      ! by Gridding and Fast Fourier Transform, using adequate
      ! scratch space for optimisation. Will work for
      ! up to 128x128x128 cube data size, may be more...
      !
      ! Input :
      !     a precessed UV table
      ! Output :
      !     a precessed, rotated, shifted UV table, sorted in V,
      !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      !     a beam image or cube
      !     a LMV cube
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: task ! Caller (MX or UV_MAP)
      character(len=*), intent(in) :: line ! Command line
      logical, intent(out) :: error
    end subroutine old_uvmap
  end interface
  !
  interface
    subroutine uv_resample_comm(line,comm,error)
      use gbl_message
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Resample in velocity UV Tables or compress them
      !   Support for commands
      !     UV_RESAMPLE NC [Ref Val Inc] [/FILE FileIn FileOut] [/LIKE Mold]
      !     UV_COMPRESS NC [/CONTINUUM] [/FILE FileIn FileOut] 
      !     UV_HANNING     [/FILE FileIn FileOut]
      !     UV_SMOOTH   NC [/ASYMMETRIC] [/FILE FileIn FileOut] 
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Calling command
      logical, intent(out) :: error         ! Error flag
    end subroutine uv_resample_comm
  end interface
  !
  interface
    subroutine uv_resample_args(line,comm,hiuv,houv,error)
      use gkernel_types
      use gbl_message
      use clean_def
      use clean_default
      use clean_types
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Resample in velocity UV Tables or compress them
      !   Support for commands
      !     UV_RESAMPLE NC [Ref Val Inc] [/FILE FileIn FileOut] [/LIKE Mold]
      !     UV_COMPRESS [NC] [/CONTINUUM] [/FILE FileIn FileOut] 
      !     UV_HANNING     [/FILE FileIn FileOut]
      !     UV_SMOOTH   NC [/ASYMMETRIC] [/FILE FileIn FileOut]
      !
      !   Decode the arguments and fill the output header accordingly
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Calling command
      logical, intent(out) :: error         ! Error flag
      type(gildas), intent(inout) :: hiuv
      type(gildas), intent(inout) :: houv
    end subroutine uv_resample_args
  end interface
  !
  interface
    subroutine uv_resample_mem(line,comm,error)
      use gkernel_types
      use clean_def
      use clean_beams
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Resample in velocity the UV Table or compress it
      !   Support for commands
      !     UV_RESAMPLE NC [Ref Val Inc] 
      !     UV_COMPRESS [NC] [/CONTINUUM]
      !     UV_HANNING 
      !     UV_SMOOTH NC    
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Calling command
      logical, intent(out) :: error         ! Error flag
    end subroutine uv_resample_mem
  end interface
  !
  interface
    subroutine hanning_uv (duvout, nu, nv, nchan, duvin, ni, ntrail) 
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     UV_HANNING     
      !   This is equivalent to UV_SMOOTH 2
      !---------------------------------------------------------------------
      integer, intent(in) :: nu           ! Ouput Visibility size
      integer(kind=index_length), intent(in) :: nv           ! Number of visibilities
      integer, intent(in) :: nchan        ! Number of ouput channels
      integer, intent(in) :: ntrail       ! Trailing columns
      integer, intent(in) :: ni           ! Input Visibility size
      real, intent(in) :: duvin(ni,nv)    ! Input visibilities
      real, intent(out) :: duvout(nu,nv)  ! Output visibilities
    end subroutine hanning_uv
  end interface
  !
  interface
    subroutine smooth_uv (duvout, nu, nv, nchan, duvin, mc, ntrail, asym) 
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     UV_SMOOTH  Nc [/ASYMMETRIC]
      !---------------------------------------------------------------------
      integer, intent(in) :: nu           ! Ouput Visibility size
      integer(kind=index_length), intent(in) :: nv  ! Number of visibilities
      integer, intent(in) :: nchan        ! Number of channels
      integer, intent(in) :: ntrail       ! Trailing columns
      integer, intent(in) :: mc           ! Smoothing factor
      real, intent(in) :: duvin(nu,nv)    ! Input visibilities
      real, intent(out) :: duvout(nu,nv)  ! Output visibilities
      logical, intent(in) :: asym         ! Asymmetric smoothing ?
    end subroutine smooth_uv
  end interface
  !
  interface
    subroutine sub_cont_average(line,comm,hiuv,houv,nc,error)
      use image_def
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   
      ! IMAGER
      !   Support for command
      !     UV_COMPRESS [NC] [/CONTINUUM]  
      ! Derive the compression factor NC and set the output Header
      ! properly
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name
      type(gildas), intent(inout) :: hiuv   ! Original UV data
      type(gildas), intent(inout) :: houv   ! Resampled UV data header
      integer, intent(out) :: nc            ! Compression factor
      logical, intent(out) :: error
    end subroutine sub_cont_average
  end interface
  !
  interface
    subroutine uvflag_edges(huvin, huvout, nu, nv, duv)  
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for UV Resampling: flag edge channels if not
      !   fully covered in the operation
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: huvin
      type(gildas), intent(in) :: huvout
      integer, intent(in) :: nu, nv
      real, intent(inout) :: duv(nu,nv) 
    end subroutine uvflag_edges
  end interface
  !
  interface
    subroutine get_cols(nchan,ucol,fcol,lcol,wcol)
      ! @ private
      integer, intent(in) :: ucol(2)
      integer, intent(inout) :: nchan
      integer, intent(out) :: fcol
      integer, intent(out) :: lcol
      integer, intent(out) :: wcol
    end subroutine get_cols
  end interface
  !
  interface
    subroutine uv_residual_clean(line,task,iarg,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   Support for routine UV_RESIDUAL [FIELDS List]
      !     Remove all Clean Components from a UV Table
      !
      ! Input :
      !     a precessed UV table
      !     a list of Clean Components, in DCCT format
      !      i.e. (x,y,v)(iplane,icomponent)
      !     this organisation is not efficient, and one may need to switch to
      !           (x,y,v,)(icomponent,iplane)
      !     which is more easily transmitted
      ! Output :
      !     a precessed, rotated, shifted UV table, sorted in V,
      !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      !
      !------------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      character(len=*), intent(in) :: task
      integer, intent(in) :: iarg
      logical, intent(out) :: error
    end subroutine uv_residual_clean
  end interface
  !
  interface
    subroutine sub_uv_residual(task,line,iarg,duv_previous,duv_next,do_clean,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use clean_beams
      use gbl_message
      !$ use omp_lib
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER   Support for routine UV_RESIDUAL
      !     Remove all Clean Components from a UV Table
      !
      ! Input :
      !     a precessed UV table
      !     a list of Clean Components, in DCCT format
      !      i.e. (x,y,v)(iplane,icomponent)
      !     this organisation is not efficient, and one may need to switch to
      !           (x,y,v,)(icomponent,iplane)
      !     which is more easily transmitted
      ! Output :
      !     a precessed, rotated, shifted UV table, sorted in V,
      !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      !
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: task            ! Calling Command
      character(len=*), intent(inout) :: line         ! Command line
      integer, intent(in) :: iarg                     ! First useful argument
      real, pointer, intent(inout) :: duv_previous(:,:) ! Input buffer
      real, pointer, intent(inout) :: duv_next(:,:)   ! May not be defined here
      logical, intent(in) :: do_clean                 ! Re-image ?
      logical, intent(out) :: error
    end subroutine sub_uv_residual
  end interface
  !
  interface
    subroutine uv_clean_size(hcct,ccin, mic)
      use image_def
      !-----------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Support for UV_RESTORE
      !   Compute the actual number of components
      !-----------------------------------------------------------------
      type(gildas), intent(in) :: hcct  ! header of CCT data set
      real, intent(in) :: ccin(hcct%gil%dim(1),hcct%gil%dim(2),hcct%gil%dim(3))
      integer, intent(out) :: mic
    end subroutine uv_clean_size
  end interface
  !
  interface
    subroutine uv_compact_clean(hcct,ccin,occt,ccou, mic)
      use image_def
      !-----------------------------------------------------------------
      ! @ private
      !
      ! MAPPING   Support for UV_RESTORE
      !   Compact the component list by summing up all values at the
      !   same position
      !-----------------------------------------------------------------
      type(gildas), intent(in) :: hcct  ! header of CCT data set
      type(gildas), intent(in) :: occt  ! header of CCT data set
      real, intent(in) :: ccin(hcct%gil%dim(1),hcct%gil%dim(2),hcct%gil%dim(3))
      real, intent(out) :: ccou(occt%gil%dim(1),occt%gil%dim(2),occt%gil%dim(3))
      integer, intent(inout) :: mic
    end subroutine uv_compact_clean
  end interface
  !
  interface
    subroutine generate_clean(amethod,hmap,first,last,ccou,mic)
      !$ use omp_lib
      use clean_def
      use image_def
      !
      ! @ private-mandatory
      !
      type(clean_par), intent(in) :: amethod
      type(gildas), intent(inout) :: hmap
      integer, intent(in) :: first
      integer, intent(in) :: last
      real, intent(in) :: ccou(:,:,:)  !  (x,y,Value),Component,Plane
      integer, intent(in) :: mic(:)    ! Niter per plane
    end subroutine generate_clean
  end interface
  !
  interface
    subroutine attenuate_clean(nchan,ccou,doff,bsize,fcou,mic)
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Apply primary beam attenuation to Clean Components
      ! at specified offset
      !   Input   CCOU(3,Nclean,Nchan)
      !   Output  FCOU(3,Ncomp,Nchan)
      ! Ncomp is lower than Nclean: Clean components at same position
      !   have been averaged first
      ! MIC(nchan) contains the number of effective Clean Components
      ! for this channel (after attenuation).
      !---------------------------------------------------------------------
      integer, intent(in) :: nchan      ! Number of channels
      real, intent(in) :: ccou(:,:,:)   ! Shape (3,Ncomp,Nplane)
      real, intent(out) :: fcou(:,:,:)  ! Same shape, but different sizes 
      real, intent(in) :: doff(2)       ! Offset value
      real, intent(in) :: bsize         ! Beam size
      integer, intent(inout) :: mic(nchan)  ! Effective number of Clean components
    end subroutine attenuate_clean
  end interface
  !
  interface
    subroutine map_parameters(task,map,hiuv,freq,uvmax,uvmin,error,print)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !----------------------------------------------------------------------
      !   @ private-mandatory
      !
      ! IMAGER
      !   Prepare the MAP parameters for UV_MAP, MX or UV_RESTORE
      ! Reads these from SIC variables if present, or guess them from UV data
      !-----------------------------------------------------------------------
      character(len=*), intent(in)    :: task ! Input task name (UV_MAP or MX)
      type (uvmap_par), intent(inout) :: map  ! Map parameters
      type (gildas), intent(in) :: hiuv       ! UV header
      real(8), intent(inout) :: freq          ! Observing frequency
      real(4), intent(in) :: uvmax, uvmin     ! Min & Max UV in m
      logical, intent(inout) :: error
      logical, optional :: print
    end subroutine map_parameters
  end interface
  !
  interface
    subroutine uv_stat_comm(line,error)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support for command UV_STAT
      !       Analyse a UV data set to define approximate beam size,
      !       field of view, expected "best beam", etc...
      !------------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine uv_stat_comm
  end interface
  !
  interface
    subroutine uv_printoffset(rname,map)
      use clean_def
      use gbl_message
      !-----------------------------------------------------------------  
      ! @ private
      !
      !   IMAGER
      !     Print the list of Mosaic Offsets
      !-----------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(uvmap_par), intent(in) :: map
    end subroutine uv_printoffset
  end interface
  !
  interface
    subroutine map_beamsize(map,huv,duv,uvmin,uvmax,error)
      use image_def
      use clean_def
      use clean_default
      use gbl_message
      !---------------------------------------------------
      ! @ private
      !
      !  IMAGER
      !     Get the synthesized beam size according to
      !   current imaging parameters. Performs a quick 
      !   gridding on a representative channel.
      !---------------------------------------------------
      type(uvmap_par), intent(in) :: map  ! MAP parameters
      type(gildas), intent(in) :: huv   ! UV Header
      real, intent(in) :: duv(:,:)      ! UV data
      real, intent(in) :: uvmin         ! Minimum UV value
      real, intent(in) :: uvmax         ! Maximum UV value
      logical, intent(out) :: error     ! Error flag
    end subroutine map_beamsize
  end interface
  !
  interface
    subroutine uv_time_comm(line,error)
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! Time-Average UV Tables, either on the "current" UV data set
      ! or on files  (usefull for ALMA data, which may be big)
      !
      ! UV_TIME Time /Weight Wcol [/FILE FileIn FileOut]
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine uv_time_comm
  end interface
  !
  interface
    subroutine uv_time_mem(line,error)
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! UV_TIME Time /Weight Wcol
      !
      ! Time-Average the UV Table.   !
      ! Works on the "current" UV data set: Uses UVS or UVR as needed.
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine uv_time_mem
  end interface
  !
  interface
    subroutine add_visiw(out,ovisi,avisi,sw)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! 
      ! IMAGER
      !   Support for command UV_TIME
      !
      !   Add a visibility to current output one
      !   Pre-channel weights are used, and the mean u,v coordinates
      !   are weighted according to the sum of weights on channels
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: out   ! Input image header
      real, intent(inout) :: ovisi(:)   ! Output visibilities
      real, intent(inout) :: avisi(:)   ! Input visibilities
      real, intent(inout) :: sw         ! Output weight
    end subroutine add_visiw
  end interface
  !
  interface
    subroutine mean_visiw(out,ovisi,sw)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      !   Support for command UV_TIME
      !   Normalize the final visibility
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: out   ! Input image header
      real, intent(inout) :: ovisi(:)   ! Output visibilities
      real, intent(inout) :: sw         ! Output weight
    end subroutine mean_visiw
  end interface
  !
  interface
    subroutine uvtime_disk_otf(line,error)
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! UV_TIME Time /FILE FileIn FileOut [/WEIGHT Wcol]
      ! 
      ! Time-Average the UV Table found in FileIn and put it
      ! into FileOut. Very usefull for ALMA data which can 
      ! have very small integration times.
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine uvtime_disk_otf
  end interface
  !
  interface
    subroutine uvtime_disk_pre (nami,namo,arg,myuv,wcol,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! UV_TIME Time /FILE FileIn FileOut [/WEIGHT Wcol]
      ! 
      ! Time-Average the UV Table found in FileIn and put it
      ! into FileOut. Very usefull for ALMA data which can 
      ! have very small integration times.
      !
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: nami  ! Input file name
      character(len=*), intent(inout) :: namo  ! Output file name
      character(len=*), intent(in) :: arg      ! Desired Integration Time
      real(4), intent(in) :: myuv    ! Maximum UV length change
      integer, intent(inout) :: wcol ! Weight channel
      logical, intent(out) :: error
    end subroutine uvtime_disk_pre
  end interface
  !
  interface
    subroutine get_nyquist_time(rname,huv,duv,mytime,eps,error)
      use image_def
      use gbl_message
      use clean_default
      !---------------------------------------------------------------------
      ! @ private
      !
      !  IMAGER
      !     Get Maximum Smoothing time given Field of view,
      !     longest UV baseline and Required Precision
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(gildas), intent(inout) :: huv
      real, intent(in) :: duv(:,:)
      real, intent(inout) :: mytime
      real, intent(in) :: eps
      logical, intent(inout) :: error
    end subroutine get_nyquist_time
  end interface
  !
  interface
    subroutine uv_truncate_comm(line,error)
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !
      ! Truncate a UV Table, by removing baselines out of
      ! a given range (Min and Max)
      !
      ! UV_TRUNCATE Max [Min]
      !
      ! Works on the "current" UV data set: Uses UVS or UVR as needed.
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine uv_truncate_comm
  end interface
  !
  interface
    subroutine get_weightmode(task,mode,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Decode the weighting mode
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task    ! Caller name
      character(len=*), intent(inout) :: mode ! Weighting mode
      logical, intent(out) :: error           ! Error flag
    end subroutine get_weightmode
  end interface
  !
  interface
    subroutine beam_unit_conversion(amethod)
      use clean_def
      use clean_default
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Major and Minor axes of beam are in seconds but the method structure
      !   assumes that its major and minor fields are in radian. So we need
      !   a conversion...
      !----------------------------------------------------------------------
      type (clean_par) :: amethod
    end subroutine beam_unit_conversion
  end interface
  !
  interface
    subroutine copy_method(in,out)
      use clean_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Copy method
      !----------------------------------------------------------------------
      type (clean_par), intent(in) ::  in
      type (clean_par), intent(inout) ::  out
    end subroutine copy_method
  end interface
  !
  interface
    subroutine copy_param(in,out)
      !--------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Copy only the parameters of the method, not the pointers to the
      !   work arrays
      !   Do not copy the Loop Gain, which is Method specific
      !--------------------------------------------------------------------
      use clean_def
      type (clean_par), intent(in) ::  in
      type (clean_par), intent(inout) ::  out
    end subroutine copy_param
  end interface
  !
  interface
    subroutine mapping_print_debug(m)
      ! @ private
      use clean_def
      type (clean_par) :: m
    end subroutine mapping_print_debug
  end interface
  !
  interface
    subroutine uv_preview(line,error)
      use image_def
      use gkernel_types
      use gbl_message
      use clean_arrays
      use preview_mod
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !      Support for command
      !   UV_PREVIEW  [TAPER Ntaper] [THRESHOLD Threshold] [HISTO Nhisto] [
      !     SMOOTH Nsmooth] [/BROWSE] [/FILE UvData.uvt [Drop]]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line 
      logical, intent(inout) :: error       ! Error flag
    end subroutine uv_preview
  end interface
  !
  interface
    subroutine uv_preview_sub (huv,line,error)
      use image_def
      use gkernel_types 
      use gbl_message
      use preview_mod
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support for command
      !   UV_PREVIEW  [TAPER Ntaper] [THRESHOLD Threshold] [HISTO Nhisto] 
      !     [SMOOTH Nsmooth] [/BROWSE] [/FILE UvData.uvt [Drop]]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      logical, intent(inout) :: error       ! Error flag
      type(gildas), intent(inout) :: huv    ! Input UV data set
    end subroutine uv_preview_sub
  end interface
  !
  interface
    subroutine comp_r4_rms_blank (x,n,out,vblank4,eblank4)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar RMS InVar  (single precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !
      !   (Duplicated from Kernel code because symbol is private there)
      !---------------------------------------------------------------------
      real*4,                    intent(in)  :: x(*)  ! Data values to compute
      integer(kind=size_length), intent(in)  :: n     ! Number of data values
      real*4,                    intent(out) :: out   ! Output scalar value
      real*4,                    intent(in) :: vblank4, eblank4
    end subroutine comp_r4_rms_blank
  end interface
  !
  interface
    subroutine clip_expand(nc,inlines,inb,oulines,onb,iextent)
      use gildas_def
      !---------------------------------------------------------------------  
      ! @ private
      !
      !   IMAGE
      !
      !   Expand the list of "bad" channels by continuity to account for 
      ! possible line wings
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nc ! Number of channels
      integer, intent(in) :: inlines(nc)  ! List of bad channels
      integer, intent(in) :: inb          ! Number of bad channels
      integer, intent(out) :: oulines(nc)  ! List of bad channels
      integer, intent(out) :: onb         ! Number of bad channels
      integer, intent(in) :: iextent      ! Side channel Extension 
    end subroutine clip_expand
  end interface
  !
  interface
    subroutine channel_to_edges(clist,nc,edges,nl)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Convert a list of channels into a list of edges of the
      ! independent (continuous) ranges.
      !---------------------------------------------------------------------
      integer, intent(in) :: nc         ! Number of selected channels
      integer, intent(in) :: clist(nc)          ! Channel numbers
      integer, intent(out) :: edges(2,nc)  ! Start and End channels 
      integer, intent(out) :: nl        ! Number of ranges found
    end subroutine channel_to_edges
  end interface
  !
  interface
    subroutine channel_restrict(rname,fchan,lchan,error)
      use gkernel_types
      use gbl_message
      use clean_beams
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      !   If UV_CHECK BEAM has been used, restrict the range to the 
      ! most significant (to avoid issues with bad edge recognition)
      !
      ! Obsolescent routine, also used in SELFCAL
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname ! Caller name
      integer, intent(inout) :: fchan       ! First channel
      integer, intent(inout) :: lchan       ! Last channel
      logical, intent(inout) :: error       ! Error flag
    end subroutine channel_restrict
  end interface
  !
  interface
    subroutine smooth_array(nchan,raw,smooth,ksmoo,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! 
      ! Smooth an array by K channels, accounting for Blanking
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nchan        ! Number of channels
      real, intent(in) :: raw(nchan)      ! Input array
      real, intent(out) :: smooth(nchan)  ! Smoothed array 
      integer, intent(in):: ksmoo         ! Smoothing length
      real, intent(in) :: bval, eval      ! Blanking value
    end subroutine smooth_array
  end interface
  !
  interface
    subroutine map_continuum(line,error)
      use image_def
      use clean_def
      use clean_default
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      !   Support for MAP_CONTINUUM  /METHOD command
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! command line
      logical, intent(inout) :: error       ! logical error flag
    end subroutine map_continuum
  end interface
  !
  interface
    subroutine comp_r4_shape_blank (x,n,mean,rms,skew,vblank4,eblank4)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory 
      !
      ! Compute Mean, Rms and Skewness of an array
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !
      !---------------------------------------------------------------------
      real(4),          intent(in)  :: x(*)  ! Data values to compute
      integer(kind=size_length), intent(in)  :: n     ! Number of data values
      real(4),          intent(out) :: mean  ! Output scalar value
      real(4),          intent(out) :: rms   ! Output scalar value
      real(4),          intent(out) :: skew  ! Output scalar value
      real(4),          intent(in)  :: vblank4, eblank4
    end subroutine comp_r4_shape_blank
  end interface
  !
  interface
    subroutine get_logv(logv,logwings,temp,nchan,hist,nhist,mcount,amin,amax,bval,eval)
      use gildas_def
      !-------------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Support for command MAP_CONTINUUM 
      !   Derive the histgram of the intensity distribution
      !   and make sure its logarithm is suitable for Gaussian fitting 
      !   (i.e. of parabolic shape)
      !-------------------------------------------------------------------------
      integer, intent(in) :: nchan  ! Number of data points
      integer, intent(in) :: nhist                    ! Number of histogram bins
      real, intent(out) :: logv(nhist)                ! Log of bin contents
      real, intent(in) :: logwings                    ! Truncation threshold
      real, intent(in) :: temp(nchan)                 ! Data points
      real, intent(out) :: hist(nhist,2)              ! Histogram
      real, intent(out) :: amin, amax                 ! Range of selected values
      real, intent(in) :: bval, eval                  ! Blanking
      integer, intent(in) :: mcount                   ! Maximum number of loops
    end subroutine get_logv
  end interface
  !
  interface
    subroutine check_logv(logv,logwings,temp,nchan,hist,nhist,amin,amax,bval,eval,mcount,code)
      use gildas_def
      !-------------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Support for command MAP_CONTINUUM 
      !
      !   Make sure its logarithm of the Intensity distribution histogram
      !   is suitable for Gaussian fitting  (i.e. of parabolic shape)
      !   Re-adjust range and (optionally) recompute histogram if needed.
      !-------------------------------------------------------------------------
      integer, intent(in) :: nchan  ! Number of data points
      integer, intent(in) :: nhist                    ! Number of histogram bins
      real, intent(out) :: logv(nhist)                ! Log of bin contents
      real, intent(in) :: logwings                    ! Truncation threshold
      real, intent(in) :: temp(nchan)                 ! Data points
      real, intent(out) :: hist(nhist,2)              ! Histogram
      real, intent(out) :: amin, amax                 ! Range of selected values
      real, intent(in) :: bval, eval                  ! Blanking
      integer, intent(in) :: mcount                   ! Maximum number of loops
      integer, intent(inout) :: code                  ! Operation code
    end subroutine check_logv
  end interface
  !
  interface
    subroutine debug_plot(ix,jy,nhist,hist,logv,logf,nchan,temp, i, ch)
      use gildas_def
      !-------------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Debug routine for command MAP_CONTINUUM 
      !
      !   Create a graphic display of spectra whose noise distribution
      !   cannot be well fit by a Gaussian.
      !-------------------------------------------------------------------------
      integer, intent(in) :: ix,jy
      integer, intent(in) :: nhist
      integer, intent(in) :: nchan
      real, intent(in) :: temp(nchan)
      real, intent(in) :: hist(nhist,2), logv(nhist), logf(nhist)
      integer, intent(inout) :: i
      character(len=*) :: ch
    end subroutine debug_plot
  end interface
  !
  interface
    subroutine my_histo44(a,na,b,nb,mb,min,max,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      !   Perform computations in a temporary integer array since float
      ! values can not be incremented by 1 beyond a given limit (16777216.
      ! for REAL*4). Use INTEGER*8 because number of values per bin can be
      ! larger than 2**31-1.
      !---------------------------------------------------------------------
      integer :: na   !
      real*4 :: a(na)                   !
      integer(kind=4) :: nb  !
      integer(kind=4) :: mb  !
      real*4 :: b(nb,mb)                !
      real*4 :: min                     !
      real*4 :: max                     !
      real*4 :: bval                    !
      real*4 :: eval                    !
    end subroutine my_histo44
  end interface
  !
  interface
    subroutine medgauss (temp,nchan,bval,eval,aoff, &
      &  debug,code,ndata,noise, rms)
      ! @ private
      integer, intent(in) :: nchan        ! Number of channels
      real, intent(inout) :: temp(nchan)  ! Input spectrum
      real, intent(in) :: bval, eval      ! Blanking
      real, intent(inout) :: aoff         ! Fitted value
      logical, intent(in) :: debug
      integer, intent(out) :: code        ! Return code
      integer, intent(out) :: ndata       ! Number of used data
      real, intent(out) :: noise          ! Noise estimate
      real, intent(in) :: rms             ! Initial guess of RMS
    end subroutine medgauss
  end interface
  !
  interface
    subroutine uv_radial(line,rname,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gkernel_types
      use phys_const
      use gbl_message
      !---------------------------------------------------------------------------
      ! Imager / Mapping
      !
      ! @ private
      !
      ! Support for commands
      !   UV_DEPROJECT X0 Y0 ROTA INCLI 
      ! and
      !   UV_RADIAL X0 Y0 ROTA INCLI [/SAMPLING QSTEP [Unit [QMIN QMAX]] [/ZERO]
      ! and
      !   UV_CIRCLE X0 Y0 ROTA INCLI [/SAMPLING QSTEP [Unit [QMIN QMAX]] [/ZERO]
      ! 
      !   Deproject from inclination effects and Compute the radial 
      !   profile of visibilities (or azimutal average of)
      !   using the specified values of X0, Y0, ROTA and INCLI
      !
      ! This combines
      !  - a u,v shift to X0,Y0
      !  - a rotation of the u,v coordinates by ROTA
      !  - a compression of the v axis by cos(INCLI)
      !  - a circular averaging
      !  - an azimutal spreading of the circularly averaged values
      !
      ! which were available in tasks UV_SHIFT, a script and UV_CIRCLE in an
      ! rather unconvenient way (see sg_create_circular.map script)
      !
      !---------------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: rname
      logical, intent(out) :: error
    end subroutine uv_radial
  end interface
  !
  interface
    subroutine solve_gain(line,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      use clean_def
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     S.Guilloteau & V.Pietu, from R.Lucas CLIC code
      !
      ! Support for command
      !   SOLVE Time SNR [Reference] 
      !       /MODE [Phase|Amplitude] [Antenna|Baseline] [Flag|Keep]
      !
      ! Compute Gains by comparing the UV data and the UV model.
      ! Input : a UV data (observed)
      !         a UV model
      ! Output: a UV table, with the gains.
      !         a UV self-calibrated table
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine solve_gain
  end interface
  !
  interface
    subroutine do_normalize_v(ncol,nvis,duvbg,scale_gain)
      use gbl_message
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Normalize the per-baseline amplitude gains so that the Weighted 
      !   mean is 1.0
      !---------------------------------------------------------------------
      integer, intent(in) :: ncol           ! Visibility size
      integer, intent(in) :: nvis           ! Number of visibilities
      real, intent(in) :: duvbg(ncol,nvis)  ! Visibilities
      real, intent(out) :: scale_gain       ! Computed Scaling factor
    end subroutine do_normalize_v
  end interface
  !
  interface
    subroutine uv_model_comm(line,error)
      use gildas_def
      use gbl_message
      use clean_default
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !     MODEL [Args] [/MINVAL Value [Unit]] [/OUTPUT]
      !           [/MODE CCT|UV_FIT|IMAGE [Frequency]]
      !
      ! Dispatch to adequate routine as specified by /MODE option to   
      !  - Compute the UV_MODEL data set from the current CCT table
      ! or
      !  - Compute the UV_MODEL data set from the current UV_FIT results
      ! or
      !  - Compute the UV_MODEL data set from the specified IMAGE variable
      !
      !   If no /MODE option is specified, the operation depends on whether 
      ! CLEAN or UV_FIT was executed last.
      !
      !   The /OUTPUT option is dummy, being present only for compatibility
      ! with the UV_RESIDUAL command
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line        ! Command line
      logical, intent(inout) :: error             ! Error flag
    end subroutine uv_model_comm
  end interface
  !
  interface
    subroutine map_fast_uvmodel(line,error)
      use gildas_def
      use gbl_message
      use clean_arrays
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !     MODEL ImageName [/MINVAL Value [Unit]] /MODE IMAGE [Frequency]
      !
      ! Compute the UV_MODEL data set from the specified image 
      !
      ! Uses an intermediate FFT with further interpolation for 
      !    better speed than UV_CCT
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line    ! Command line
      logical, intent(inout) :: error         ! Error flag
    end subroutine map_fast_uvmodel
  end interface
  !
  interface
    subroutine cct_fast_uvmodel(line,error)
      use gildas_def
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !     MODEL [MaxIter] [/MINVAL Value [Unit]] [/MODE CCT [Frequency]]
      !
      ! Compute the MODEL UV data set from the current CCT table
      !
      !     Uses an intermediate FFT with further interpolation for
      !     better speed than UV_CCT
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine cct_fast_uvmodel
  end interface
  !
  interface
    subroutine mod_min_image(line,hmap,huv,freq,dmap,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! Support routine for command MODEL
      !   Define output frequency and truncate the data range if needed
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      type(gildas), intent(in) :: hmap      ! Header of input data set
      type(gildas), intent(in) :: huv       ! Header of input UV set
      real(8), intent(inout) :: freq        ! Frequency of output    
      real, intent(inout) :: dmap(:,:,:)    ! Image data
      logical, intent(inout) :: error
    end subroutine mod_min_image
  end interface
  !
  interface
    subroutine cct_def_image (hima,mx,my,nf,xinc,yinc,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! Define Image size from CCT information.
      ! Supports both layouts of CCT tables.
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: hima ! Input CCT Table
      integer, intent(out) :: mx,my,nf  ! Ouput data cube size
      real(8), intent(out) :: xinc,yinc ! Pixel size
      logical, intent(out) :: error     ! Error flag
    end subroutine cct_def_image
  end interface
  !
  interface
    subroutine cct_set_image (hcct,clean,mx,my,mc,xinc,yinc,nf,image,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! Fill an Image from the list of Clean Components 
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: hcct         ! CCT Table header
      real clean(hcct%gil%dim(1),hcct%gil%dim(2),hcct%gil%dim(3))
      integer, intent(in) :: mx,my,nf       ! Output data cube size
      integer, intent(in) :: mc             ! Max component number
      real(8), intent(in) :: xinc,yinc      ! Pixel size
      real, intent(out) :: image(mx,my,nf)  ! Output data
      logical, intent(out) :: error         ! Error flag
    end subroutine cct_set_image
  end interface
  !
  interface
    subroutine mod_fft_size(large,mx,my,nx,ny)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Define the intermediate FFT size - Make it square by default
      !---------------------------------------------------------------------
      logical, intent(in) :: large    ! Should size be enlarged ?
      integer, intent(in) :: mx,my    ! Input map size
      integer, intent(out) :: nx,ny   ! Output fft size
    end subroutine mod_fft_size
  end interface
  !
  interface
    subroutine uv_map_comm(line,comm,error)
      use clean_arrays
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      !
      ! @ private
      !
      ! IMAGER
      !    Support for commands
      ! UV_MAP or UV_RESTORE /FIELDS /RANGE /TRUNCATE  /CONTINUUM /INDEX
      !   or 
      ! UV_SELF /RANGE /RESTORE
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: comm
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine uv_map_comm
  end interface
  !
  interface
    subroutine no_mosaic(rname)
      use clean_def
      use clean_default
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------  !
      ! @ private
      !
      ! IMAGER
      !   Switch back prompt to IMAGER>
      !---------------------------------------------------------------------  !
      character(len=*), intent(in) :: rname
    end subroutine no_mosaic
  end interface
  !
  interface
    subroutine init_selfcal(rname,line,error)
      use clean_arrays
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------  !
      ! @ private
      !
      ! IMAGER
      !    Support for command
      ! UV_MAP /SELF Min Max Type
      !
      ! Create the Self-Calibration UV table from the current one
      ! by averaging the specified range of channels.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine init_selfcal
  end interface
  !
  interface
    subroutine do_uv_average(rname,hin,hou,numchan,nc,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support for command SELFCAL
      !   Average all selected line channels to 1 continuum channel
      !   Driver routine: prepare header and call per-visibility processing
      !   subroutine
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(gildas), intent(in) :: hin
      type(gildas), intent(inout) :: hou
      integer, intent(in) :: numchan
      integer, intent(in) :: nc(numchan)
      logical, intent(out) :: error
    end subroutine do_uv_average
  end interface
  !
  interface
    subroutine sub_uv_average (out,nx,nv,nlead,ntrail,inp,ny,nc,num)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support for command SELFCAL
      !   Average all selected line channels to 1 continuum channel
      !---------------------------------------------------------------------
      integer(kind=index_length) :: nx  ! Size of output visibility
      integer(kind=index_length) :: nv  ! Number of visibilities
      integer, intent(in) :: nlead      ! Leading columns
      integer, intent(in) :: ntrail     ! Trailing columns
      real :: out(nx,nv)                ! Output visibilities
      integer(kind=index_length) :: ny  ! Size of input visibility
      real :: inp(ny,nv)                ! Input visibilities
      integer :: num                    ! Number of ranges X 2
      integer :: nc(num)                ! Range boundaries
    end subroutine sub_uv_average
  end interface
  !
  interface
    subroutine selfcal(line,comm,error)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support for command SELFCAL
      !
      !   This actually cannot be in the CLEAN language, because it implies
      !   re-entrancy of RUN_CLEAN - Not a big issue, but currently forbidden 
      !   by our coding practice, so we place it the ADVANCED\ language
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: comm
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine selfcal
  end interface
  !
  interface
    subroutine switch_uv_cont(line,comm,do_cct,code,htmp,error)
      use gildas_def 
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      !   Support for command UV_MAP /CONT
      ! Temporarily point towards the "continuum" UV data
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Command line
      character(len=*), intent(in)    :: comm   ! Command name
      character(len=2), intent(inout) :: code   ! UV buffer code
      type(gildas), intent(out) :: htmp
      logical, intent(in) :: do_cct
      logical, intent(inout) :: error
    end subroutine switch_uv_cont
  end interface
  !
  interface
    subroutine uvshort_com(line,comm,error,quiet)
      use image_def
      use clean_def
      use clean_arrays
      use short_def
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Driving routine for command  
      !       UV_SHORT [?] [/REMOVE]
      !
      !     compute a short spacings uv table from a single-dish table
      !     by gridding, extending to 0, filtering in uv plane, multiplication
      !     by interferometer primary beam, and sampling in uv plane.
      !
      ! input :
      !   a Single-Dish table or Data Cube, and a mosaic UV Table
      ! output :
      !   another Mosaic UV table (the merged one, or the short one,
      !   depending on short_mode, a "debug" level control).
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name
      logical, intent(inout) :: error       ! Error flag
      logical, intent(inout) :: quiet       ! Quiet flag
    end subroutine uvshort_com
  end interface
  !
  interface
    subroutine uvshort_merge(rname, hin, uvt, error)
      use clean_arrays
      use clean_types
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Command UV_SHORT  - Final merging of Short spacings and UV table
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type (gildas), intent(in) :: uvt    ! Short Spacings UV table 
      type (gildas), intent(inout) :: hin ! Input UV table when used to append
      logical, intent(inout) :: error
    end subroutine uvshort_merge
  end interface
  !
  interface
    subroutine uvshort_plug(rname, hin, uvt, error)
      use clean_arrays
      use clean_types
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Command UV_SHORT  - Put Short Spacings as current UV table
      !------------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type (gildas), intent(in) :: uvt ! Short Spacings UV table 
      type (gildas), intent(inout) :: hin ! Input UV table 
      logical, intent(inout) :: error
    end subroutine uvshort_plug
  end interface
  !
  interface
    subroutine uvshort_params(short, error)
      use gildas_def
      use short_def
      use gbl_message
      !------------------------------------------------------------------------ 
      ! @ private
      !
      ! IMAGER    Command UV_SHORT
      !   Define input parameters
      !------------------------------------------------------------------------  
      type (short_spacings), intent(out) :: short
      logical, intent(out) :: error
    end subroutine uvshort_params
  end interface
  !
  interface
    subroutine uvshort_list(short, quick, error)
      use gildas_def
      use short_def
      use gbl_message
      use gbl_ansicodes
      !------------------------------------------------------------------------ 
      ! @ private
      !
      ! IMAGER    Command UV_SHORT
      !   List input parameters
      !------------------------------------------------------------------------  
      type (short_spacings), intent(in) :: short
      integer, intent(in)  :: quick
      logical, intent(out) :: error
    end subroutine uvshort_list
  end interface
  !
  interface
    subroutine uvshort_datas(short, hin, uvt, sdt, lmv, lmv_file, error)
      use image_def
      use clean_def
      use clean_arrays
      use gbl_format
      use gbl_message
      use short_def
      use gkernel_types
      !------------------------------------------------------------------------  
      ! @ private
      !
      ! IMAGER      Command UV_SHORT
      !   Retrieve data headers 
      !------------------------------------------------------------------------  
      type(short_spacings), intent(inout)   :: short
      !
      type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
      type (gildas), intent(inout) :: uvt ! Input/Output UV table (Short spacings only or complete)
      type (gildas), intent(inout) :: hin ! Input UV table when used to append
      type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
      logical, intent(out) :: lmv_file  
      logical, intent(out) :: error
    end subroutine uvshort_datas
  end interface
  !
  interface
    subroutine uvshort_fields(short, uvt, error)
      use image_def
      use gbl_format
      use gbl_message
      use short_def
      use clean_arrays ! For DUV global
      use gkernel_types
      !------------------------------------------------------------------------  
      ! @ private
      !
      ! IMAGER      Command UV_SHORT
      !   Retrieve number of fields and fields coordinates
      !------------------------------------------------------------------------  
      type(short_spacings), intent(inout)   :: short
      type (gildas), intent(inout) :: uvt ! Input UV table header
      logical, intent(out) :: error
    end subroutine uvshort_fields
  end interface
  !
  interface
    subroutine uvshort_checks(short, hin, uvt, sdt, lmv, lmv_file, do_zero, error)
      use image_def
      use gbl_format
      use gbl_message
      use short_def
      use gkernel_types
      !------------------------------------------------------------------------  
      ! @ private
      !
      ! IMAGER      Command UV_SHORT
      !   Check parameters
      !------------------------------------------------------------------------  
      type(short_spacings), intent(inout)   :: short
      type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
      type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
      type (gildas), intent(inout) :: hin ! Input UV table when used to append
      type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
      logical, intent(in) :: lmv_file
      logical, intent(out) :: do_zero
      logical, intent(out) :: error
    end subroutine uvshort_checks
  end interface
  !
  interface
    subroutine uvshort_dozeros(short, uvt, sdt, lmv, lmv_file, error)
      use image_def
      use gbl_format
      use gbl_message
      use short_def
      use gkernel_types
      !------------------------------------------------------------------------  
      ! @ private
      !
      ! IMAGER      Command UV_SHORT
      !   Treat the Zero spacing case
      !------------------------------------------------------------------------  
      type(short_spacings), intent(inout)   :: short
      type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
      type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
      type (gildas), intent(inout) :: sdt ! Single Dish Table for input
      logical, intent(in) :: lmv_file
      logical, intent(out) :: error
    end subroutine uvshort_dozeros
  end interface
  !
  interface
    subroutine uvshort_doshorts(short, hin, uvt, sdt, lmv, lmv_file, error)
      use image_def
      use gbl_format
      use gbl_message
      use short_def
      use gkernel_types
      use clean_arrays
      !$ use omp_lib
      !------------------------------------------------------------------------  
      ! @ private
      !
      ! IMAGER    Command UV_SHORT
      !   Treat the Short spacings case
      !------------------------------------------------------------------------  
      type(short_spacings), intent(inout)   :: short
      type (gildas), intent(inout) :: lmv ! Input or Output Single Dish data cube in LMV order
      type (gildas), intent(inout) :: uvt ! Output UV table (Short spacings only or complete)
      type (gildas), intent(inout) :: hin ! Input UV table when used to append
      type (gildas), intent(inout) :: sdt ! Single Dish Table for input (Zero spacings only or complete)
      logical, intent(in) :: lmv_file
      logical, intent(out) :: error
    end subroutine uvshort_doshorts
  end interface
  !
  interface
    subroutine uvshort_create_lmv(rname,sdt,lmv,gr_im_w,error,uvt,short) 
      use gbl_message
      use image_def
      use short_def
      use clean_def
      !------------------------------------------------------------------------  
      ! @ private-mandatory
      !
      ! IMAGER      Support routine for command UV_SHORT
      !
      !   Creates a "well behaved" Single Dish map from a Single Dish table
      !   for derivation of the short spacings of an interferometer mosaic
      !
      !   Use convolution by a fraction of the beam, and smooth extrapolation
      !   to zero beyond the mosaic edge
      !------------------------------------------------------------------------  
      character(len=*), intent(in) :: rname     ! Caller task name
      type(gildas), intent(inout) :: lmv        ! LMV data set
      type(gildas), intent(inout) :: sdt        ! SDT data set
      type(gildas), intent(inout) :: uvt        ! UVT data set
      real, allocatable, intent(out) :: gr_im_w(:)        ! Weights
      logical, intent(out) :: error             ! Error flag
      type(short_spacings)  ,intent(inout) :: short
    end subroutine uvshort_create_lmv
  end interface
  !
  interface
    function trione (x,nd,n,ix,work)
      !---------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      !   sorting program that uses a quicksort algorithm.
      ! sort on one row
      ! x r*4(*)  unsorted array        input
      ! nd  i first dimension of x      input
      ! n i second dimension of x     input
      ! ix  i x(ix,*) is the key for sorting    input
      ! work  r*4(nd) work space for exchange     input
      !---------------------------------------------------------------------
      !
      integer, intent(in) :: nd   ! First dimension
      integer, intent(in) :: n    ! Second dimension
      integer, intent(in) :: ix   ! Sorting key in first dimension
      real(4), intent(inout) :: x(nd,n)  ! Array for sorting
      real(4), intent(inout) :: work(nd) ! Work space for sorting
      !
      ! Local variables
      integer trione, maxstack, nstop
    end function trione
  end interface
  !
  interface
    subroutine uvshort_fill(lmv,uvt,error,nvis,nc,ra_off,de_off,positions,last)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      !   Fill in header of short spacings UV data
      !---------------------------------------------------------------------
      !
      type(gildas), intent(in) :: lmv       ! Header of low resolution data cube
      type(gildas), intent(inout) :: uvt    ! Header of short spacings UV table
      logical, intent(out) :: error
      integer, intent(in) :: nvis ! Number of visibilities
      integer, intent(in) :: nc   ! Number of channels
      real, intent(in) :: ra_off  ! RA Offset
      real, intent(in) :: de_off  ! Declination Offset
      logical, intent(in) :: positions ! Do we add offset position columns ?
      integer, intent(out) :: last  ! Size of a visibility
    end subroutine uvshort_fill
  end interface
  !
  interface
    subroutine uv_short_consistency(rname,uvt,short,tole,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      !   Verify spectral axis consistency
      !---------------------------------------------------------------------
      !
      character(len=*), intent(in) :: rname
      type(gildas), intent(in) :: uvt
      type(gildas), intent(in) :: short
      real, intent(in) :: tole
      logical, intent(out) :: error
    end subroutine uv_short_consistency
  end interface
  !
  interface
    subroutine spectrum_to_zero(nc,spectrum,uvdata,date,weight)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      ! Convert a spectrum into a Zero spacing
      !---------------------------------------------------------------------
      !
      integer, intent(in) :: nc
      real, intent(in) :: spectrum(nc) 
      real, intent(out) :: uvdata(:)
      integer, intent(in) :: date
      real, intent(in) :: weight
    end subroutine spectrum_to_zero
  end interface
  !
  interface
    subroutine extract_comm(line,error)
      use image_def
      use gkernel_types
      use gbl_message
      use iso_c_binding
      use clean_default
      !---------------------------------------------------------------------
      ! @ private  
      !
      ! IMAGER
      !   Support for command EXTRACT Name [BLC TRC]
      !	  Extract a subset from an input n-Dim image (n<4) 
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine extract_comm
  end interface
  !
  interface
    subroutine slice_comm(line,error)
      use image_def
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private  
      !
      ! IMAGER
      !   Support routine for command 
      !   SLICE  DataSet  Xblc Xtrc Yblc Ytrc Type
      !     or
      !   SLICE  Dataset  Xc Yc Unit ANGLE PosAngle
      !
      !   Arbitrary slice in a 3-D data set, using bilinear interpolation
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine slice_comm
  end interface
  !
  interface
    subroutine fill_gu(ipgu, ds, dt, np)
      ! @ private
      !   Fill the grid
      integer, intent(in) :: np                      ! Number of points
      real, intent(out) :: ipgu(np)                  ! Output grid
      real(8), intent(in) :: ds                      ! Starting value
      real(8), intent(in) :: dt                      ! Step
    end subroutine fill_gu
  end interface
  !
  interface
    function pix_axis (head, user, iaxis)
      use image_def
      ! @ private
      !   Pixel corresponding to a user coordinate
      real(8), intent(in) :: user       ! User coordinates
      type(gildas), intent(in) :: head  ! Image header
      integer, intent(in) :: iaxis      ! Axis
      integer :: pix_axis ! intent(out) ! corresponding pixel 
    end function pix_axis
  end interface
  !
  interface
    subroutine sub_extract(a,na1,na2,na3,na4,   &
         &    b,nb1,nb2,nb3,nb4,ni,nu,np,blank)
      use gildas_def
      ! @ private
      !   4-D extraction
      integer(kind=index_length) :: na1                    !
      integer(kind=index_length) :: na2                    !
      integer(kind=index_length) :: na3                    !
      integer(kind=index_length) :: na4                    !
      real, intent(in) :: a(na1,na2,na3,na4)               !
      integer(kind=index_length) :: nb1                    !
      integer(kind=index_length) :: nb2                    !
      integer(kind=index_length) :: nb3                    !
      integer(kind=index_length) :: nb4                    !
      real, intent(inout) :: b(nb1,nb2,nb3,nb4)            !
      integer(kind=index_length) :: ni(4)                  !
      integer(kind=index_length) :: nu(4)                  !
      integer(kind=index_length) :: np(4)                  !
      real, intent(in) :: blank                            !
    end subroutine sub_extract 
  end interface
  !
  interface
    subroutine parabola_3pt8(x,y,aa,bb,cc) 
      !----------------------------------------------------------------------
      ! @ private
      !	Solve for Y = aa*x^2 + bb*x + cc with 3 points input data
      !----------------------------------------------------------------------
      real(8), intent(in) :: x(3)
      real(8), intent(in) :: y(3)
      real(8), intent(out) :: cc,bb,aa
    end subroutine parabola_3pt8
  end interface
  !
  interface
    subroutine uv_residual_comm(line,error)
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! IMAGER
      !   Support for command UV_RESIDUAL [CLEAN|CCT|[Func1 ... FuncN] [/QUIET] 
      !     [/WIDGET Nfunc] [/SAVE File] [/RESULT]
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine uv_residual_comm
  end interface
  !
  interface
    subroutine uvfit_sub(line,verbose,error)
      use gkernel_types
      use image_def
      use gbl_format
      use gbl_message
      use uvfit_data
      !
      use clean_def
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      ! IMAGER
      !   Support for command UV_FIT 
      !
      !   The functions have been defined previously by subroutine uvfit_comm
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(in) :: verbose
      logical, intent(inout) :: error
    end subroutine uvfit_sub
  end interface
  !
  interface
    subroutine uvfit_residual_model(line,rname,iarg,error)
      use clean_def
      use clean_default
      use clean_arrays
      use uvfit_data
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command UV_RESIDUAL and MODEL 
      !   after a UV_FIT command operation
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: rname 
      integer, intent(in) :: iarg
      logical, intent(inout) :: error
    end subroutine uvfit_residual_model
  end interface
  !
  interface
    subroutine outfit(nc,ic,ncol,y,rms,vit,nbpar,par,epar)
      use gildas_def
      use uvfit_data
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for UV_FIT
      !
      ! Store the fitted parameters into the output table Y
      !
      ! Format of table is, for each channel:
      ! 1-4  RMS of fit, number of functions, number of parameters, velocity
      ! then for each function, 3+mpin*2 columns:
      !        Function number, type of function, number of parameters
      !        then 6 times (parameter, error)
      !---------------------------------------------------------------------
      integer, intent(in) :: nc         ! Number of channels
      integer, intent(in) :: ic         ! Current channel
      integer, intent(in) :: ncol       ! Size of Y
      real, intent(out) :: y(nc,ncol)   ! Output table
      real(8), intent(in) :: rms        ! RMS of fit
      real, intent(in) :: vit           ! Velocity
      integer, intent(in) :: nbpar      ! Number of parameters
      real(8), intent(in) :: par(nbpar) ! Parameters
      real(8), intent(in) :: epar(nbpar)! Errors
    end subroutine outfit
  end interface
  !
  interface
    subroutine load_data(ndata,nx,ic,fact,visi,np,uvriw,uv_min,uv_max)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for UV_FIT
      ! Load Data of channel IC from the input UV table into array UVRIW.
      !---------------------------------------------------------------------
      integer, intent(in) :: ndata        ! Number of visibilities
      integer, intent(in) :: nx           ! Size of a visibility
      integer, intent(in) :: ic           ! Channel number
      real, intent(in) :: fact            ! Conversion factor of baselines
      real, intent(in) :: visi(nx,ndata)  ! Visibilities
      integer, intent(out) :: np          ! Number of non zero visibilities
      real, intent(out) :: uvriw(5,ndata) ! Output array
      real, intent(in) :: uv_min          ! Min UV
      real, intent(in) :: uv_max          ! Max UV
    end subroutine load_data
  end interface
  !
  interface
    subroutine model_data(huv,nd,nx,nc,ic1,ic2,ncol,vin,vy,if,subtract)
      use image_def
      use uvfit_data
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for UV_FIT
      ! Subtract the model uv data for component function number IF
      ! from the input visibility data.
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: huv  ! Input UV Table header
      integer, intent(in) :: nd         ! Number of visibilities
      integer, intent(in) :: nx         ! Size of a visbility
      integer, intent(in) :: nc         ! Number of channels
      integer, intent(in) :: ic1        ! First channel
      integer, intent(in) :: ic2        ! Last channel
      integer, intent(in) :: ncol       ! Size of VY
      real, intent(inout) :: vin(nx,nd) ! Visibilities
      real, intent(in) :: vy(nc,ncol)   ! Model parameters
      integer, intent(in) :: if         ! Function number
      logical, intent(in) :: subtract   ! Subtract (Residual) or Add (Model)
    end subroutine model_data
  end interface
  !
  interface
    subroutine primary_atten(bsize,head,freq,rmax,nr,profile,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER  Support for command
      !   PRIMARY [BeamSize] 
      !
      ! Compute primary beam radial profile
      !---------------------------------------------------------------------
      real(4), intent(inout) :: bsize      ! Force Beam Size if non zero
      type(gildas), intent(in) :: head     ! Header of UV data
      real(8), intent(in)  :: freq
      real(8), intent(in)  :: rmax         ! Maximum Angular Distance
      integer, intent(out) :: nr           ! Number of radial points
      real(8), allocatable, intent(out) :: profile(:,:) ! Radial profile
      logical, intent(out) :: error
    end subroutine primary_atten
  end interface
  !
  interface
    subroutine compute_moments(method,a,b,nxy,nv,a0,a1,a2,a3,v, &
      & ivmin,ivmax,s,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for MOMENTS
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: method
      integer(kind=size_length),  intent(in)  :: nxy
      integer(kind=index_length), intent(in)  :: nv
      real(kind=4),               intent(in)  :: a(nxy,nv)  ! Original data
      real(kind=4),               intent(in)  :: b(nxy,nv)  ! Smoothed data
      real(kind=4),               intent(out) :: a0(nxy)  ! 0th moment
      real(kind=4),               intent(out) :: a1(nxy)  ! 1st moment
      real(kind=4),               intent(out) :: a2(nxy)  ! 2nd moment
      real(kind=4),               intent(out) :: a3(nxy)  ! Peak value
      real(kind=4),               intent(in)  :: v(nv)    ! Velocities
      integer(kind=4), intent(in)  :: ivmin
      integer(kind=4), intent(in)  :: ivmax
      real(kind=4),               intent(in)  :: s
      real(kind=4),               intent(in)  :: bval,eval
    end subroutine compute_moments
  end interface
  !
  interface
    subroutine uv_trim_comm(line,error)
      use gildas_def
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !       Support routine for command
      !     UV_TRIM [Action] [/FILE File [FileOUT] 
      !
      ! Trim the current UV data or specified UV table for Trailing
      ! columns (Action = TRAIL) or flagged for all channels (Action = DATA),
      ! or flagged for any channel (Action = ANY) .
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine uv_trim_comm
  end interface
  !
  interface
    subroutine uv_trim_sub (cuvin, cuvou, code, wcol, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER 
      !   Trim a UV table,
      !       - remove the flagged data
      !   or  - trim trailing columns
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: cuvin
      character(len=*), intent(in) :: cuvou
      character(len=*), intent(in) :: code
      integer, intent(in) :: wcol
      logical, intent(out) :: error
    end subroutine uv_trim_sub
  end interface
  !
  interface
    subroutine uv_trim_flag(in,invis,out,outvis,wcol,error)
      use gildas_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !       Trim an input UV table: get rid off all flagged visibilities
      !
      ! WCOL < 0    Trim if any channel is flagged
      ! WCOL = 0    Trim if all channels are flagged
      ! WCOL > 0    Trim if that channel is flagged
      !----------------------------------------------------------------------
      type (gildas), intent(inout) :: in, out
      integer, intent(in) :: invis
      integer, intent(out) :: outvis
      integer, intent(in) :: wcol
      logical, intent(out) :: error
    end subroutine uv_trim_flag
  end interface
  !
  interface
    subroutine catalog_comm(line,quiet,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! IMAGER
      !
      !   List or define Spectral Line Data Base(s)
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: quiet
      logical, intent(inout) :: error
    end subroutine catalog_comm    
  end interface
  !
  interface
    subroutine catalog_find(line,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command 
      !     FIND Fmin Fmax [/SPECIES Name]
      ! Returns in the LINES% structure the frequencies and names of lines
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line        ! Command line
      logical, intent(inout) :: error
    end subroutine catalog_find
  end interface
  !
  interface
    subroutine catalog_readastro (molfile,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !    IMAGER
      !  Read an Astro catalog
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: molfile        ! Catalog name
      logical, intent(inout) :: error
    end subroutine catalog_readastro
  end interface
  !
  interface
    subroutine mask_comm(line,error)
      use gildas_def
      use gbl_message
      use gkernel_types
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      ! Support routine for command
      !   ADVANCED\MASK Arguments... 
      !
      ! Several modes
      !   MASK  ADD Figure Description
      !                 Add the corresponding figure to mask
      !   MASK  APPLY Variable  
      !                 Apply the current mask to 3-D Variable
      !   MASK  INIT [2D|3D]
      !                 Initialize a mask
      !   MASK  OVERLAY
      !                 Overlay the mask on the current Image
      !   MASK  READ File
      !                 Read a Mask from disk (is that a .msk or a .pol ?) 
      !                 (redundant with READ MASK )
      !   MASK  REGIONS Nregions
      !                 Only keep the most important domains
      !   MASK  REMOVE  Fig_Description
      !                 Removed the figure from mask
      !   MASK  USE   
      !                 Activate the current mask as a Support
      !                 (redundant with SUPPORT /MASK)
      !   MASK  SHOW    
      !                 as SHOW MASK
      !   MASK  THRESHOLD Value Unit [SMOOTH Smooth Length] [GUARG Guard]
      !                 [REGIONS Nregions]
      !                 automatic Mask builder by Thresholding
      !   MASK  WRITE File
      !                 Write a mask to disk 
      !                 (almost redundant with WRITE MASK ? )
      !
      !   MASK          Launch interactive mask definition
      !          (would be better on currently displayed image, rather
      !           than only the CLEAN data cube. Rank could depend
      !           on how many channels are displayed)
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine mask_comm
  end interface
  !
  interface
    subroutine mask_apply(line,error)
      use gildas_def
      use gbl_message
      use gkernel_types
      use clean_arrays
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support routine for command
      !   ADVANCED\MASK APPLY Variable
      !---------------------------------------------------------------------  
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine mask_apply
  end interface
  !
  interface
    subroutine sub_mask_apply(hin,hmask,din,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !   Apply a mask
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: hin
      type(gildas), intent(in) :: hmask
      real, intent(inout) :: din(hin%gil%dim(1),hin%gil%dim(2),hin%gil%dim(3))
      logical :: error
    end subroutine sub_mask_apply
  end interface
  !
  interface
    subroutine gdf_compare_2d(hone,htwo,equal)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !   Check 2D consistency of data cubes
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: hone
      type(gildas), intent(in) :: htwo
      logical, intent(out) :: equal
    end subroutine gdf_compare_2d  
  end interface
  !
  interface
    subroutine mask_threshold(line,error)
      use clean_arrays
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command
      !   ADVANCED\MASK  THRESHOLD Value Unit [SMOOTH Smooth Length] 
      !                  [GUARG Guard] [REGIONS Nregions]
      !                 automatic Mask builder by Thresholding
      !
      !  Value    Thresholding (in Unit) of the Clean image
      !  Smooth   Thresholding (in Unit) after smoothing
      !  Length   Smoothing length (in arcsec): default is Clean beam major axis
      !  Guard    Guard band ignored at edges
      !  Nregions Number of separate areas kept in mask.
      !
      ! Select CLEAN or SKY depending on last available
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine mask_threshold
  end interface
  !
  interface
    subroutine mask_regions(line,error)
      use clean_arrays
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command
      !   ADVANCED\MASK  REGIONS Nregions
      !
      ! Keep only the requested number of Regions
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine mask_regions
  end interface
  !
  interface
    subroutine sub_mask_threshold(head,name,line,error)
      use clean_default
      use clean_arrays
      use clean_support
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command
      !   ADVANCED\MASK  THRESHOLD Value Unit [SMOOTH Smooth Length] 
      !                  [GUARG Guard] [REGIONS Nregions]
      !                 automatic Mask builder by Thresholding
      !
      !  Value    Thresholding (in Unit) of the Clean image
      !  Smooth   Thresholding (in Unit) after smoothing
      !  Length   Smoothing length (in arcsec): default is Clean beam major axis
      !  Guard    Guard band ignored at edges
      !  Nregions Number of separate areas kept in mask.
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: head      ! Header of data used for threshold
      character(len=*), intent(in) :: name  ! 
      character(len=*), intent(in) :: line  ! Command line
      logical, intent(inout) :: error       ! Error code
    end subroutine sub_mask_threshold
  end interface
  !
  interface
    subroutine mask_clean (head,mask,data,raw,smo,length,margin, &
      icode,error) 
      !$ use omp_lib
      use clean_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support routine for MASK THRESHOLD 
      !     (and the now obsolete SUPPORT /THRESHOLD)
      !
      !   Builds a Mask from a Threshold and Smoothing 
      !
      !   icode indicates whether the mask is from Positive, Negative
      !   or Both values compared to the Threshold.
      ! 
      !   The mask is the .OR. of the Raw and Smooth version of the image.
      !   Blanking are set to Zero.
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: head
      real, intent(out), target :: mask(head%gil%dim(1),head%gil%dim(2),head%gil%dim(3))
      real, intent(in) :: data (head%gil%dim(1),head%gil%dim(2),head%gil%dim(3))
      real, intent(in) :: raw
      real, intent(in) :: smo
      real, intent(in) :: length
      real, intent(in) :: margin
      integer, intent(in) :: icode      ! Code for sign
      logical, intent(out) :: error
    end subroutine mask_clean
  end interface
  !
  interface
    subroutine mask_prune (head,mask,nregions,error) 
      use clean_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Support routine for 
      !     MASK THRESHOLD Args... REGIONS Regions
      ! and 
      !     MASK REGIONS Regions
      !   commands
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: head
      real, intent(inout), target :: mask(:,:,:)
      integer, intent(inout) :: nregions
      logical, intent(out) :: error
    end subroutine mask_prune
  end interface
  !
  interface
    subroutine mask_init(key,error)
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Support for command
      !   MASK INIT [2D|3D]
      !--------------------------------------------------------------------- 
      character(len=*), intent(in) :: key
      logical, intent(inout) :: error
    end subroutine mask_init
  end interface
  !
  interface
    subroutine mask_check(all,error)
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command MASK
      !---------------------------------------------------------------------
      logical, intent(in) :: all
      logical, intent(inout) :: error
    end subroutine mask_check
  end interface
  !
  interface
    subroutine uv_extract_comm(line, error)
      use gbl_message
      use clean_arrays
      !----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      ! Command line interaction for UV_EXTRACT command
      !   UV_EXTRACT /RANGE Min Max TYPE /FIELD Number
      !----------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out)            :: error ! Error flag
    end subroutine uv_extract_comm
  end interface
  !
  interface
    subroutine uv_extract_fields(line,error)
      use gkernel_types
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for Command 
      !   UV_EXTRACT /FIELDS NumberList
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      logical, intent(out)            :: error ! Error flag
    end subroutine uv_extract_fields
  end interface
  !
  interface
    subroutine uv_extract_header(huv, nc)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for Command    UV_EXTRACT /RANGE   
      !   Setup output header consistently
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: huv    ! Header to be modified
      integer, intent(in) :: nc(2)          ! Desired channel range
    end subroutine uv_extract_header
  end interface
  !
  interface
    subroutine uv_extract_sub(hiuv,houv,use_file,nblock,channels,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !    Support routine for command UV_EXTRACT /RANGE 
      !    Extract a range of channels from a UV table
      !----------------------------------------------------------
      type(gildas), intent(inout) :: hiuv
      type(gildas), intent(inout) :: houv
      logical, intent(in)         :: use_file
      integer, intent(in)         :: nblock
      integer, intent(in)         :: channels(2)  ! Limits for the new UV table in channels
      logical, intent(inout)      :: error        ! Error flag
    end subroutine uv_extract_sub
  end interface
  !
  interface
    subroutine uv_sort_file(line,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER 
      !     Support routine for command
      !   UV_SORT [TIME|BASE|FIELDS|FREQUENCY] /FILE FileIn FileSort
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Logical error flag
    end subroutine uv_sort_file
  end interface
  !
  interface
    subroutine uvsort_tb (hin,hou,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Sort a UV data set by Time-Baseline order.
      !     Data must be in the %R2D pointer
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: hin
      type(gildas), intent(inout) :: hou
      logical, intent(out) :: error
    end subroutine uvsort_tb
  end interface
  !
  interface
    subroutine sortiv (vin,vout,np,nv,it)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support routine for UV_SORT
      !     Sort an input UV table into an output one, according
      !     to an index array
      !----------------------------------------------------------------------
      integer, intent(in) :: np         ! Size of a visibility
      integer, intent(in) ::  nv        ! Number of visibilities
      real, intent(in) ::  vin(np,nv)   ! Input visibilities
      real, intent(out) ::  vout(np,nv) ! Output visibilities
      integer, intent(in) ::  it(nv)    ! Sorting pointer
    end subroutine sortiv
  end interface
  !
  interface
    subroutine uvsort_bt (hin,hou,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support routine for UV_SORT
      !     Sort by Baseline - Time
      !     Data must be in the %R2D pointers
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: hin
      type(gildas), intent(inout) :: hou
      logical, intent(out) :: error
    end subroutine uvsort_bt
  end interface
  !
  interface
    subroutine loadbt (visi,np,nv,tb,it,sorted,idate)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support routine for UV_SORT 
      !     Load Baseline / Time combination into work arrays for sorting.
      !----------------------------------------------------------------------
      integer, intent(in)  :: np                   ! Size of a visibility
      integer, intent(in)  :: nv                   ! Number of visibilities
      real, intent(in) :: visi(np,nv)              ! Input visibilities
      real(8), intent(out) :: tb(nv)               ! Output Date+Time
      integer, intent(out) :: it(nv)               ! Indexes
      logical, intent(out) :: sorted               !
      integer, intent(in)  :: idate                ! Date pointer
    end subroutine loadbt
  end interface
  !
  interface
    subroutine loadtb (visi,np,nv,tb,it,sorted,idate)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Support routine for UV_SORT
      !     Load new U,V coordinates and sign indicator into work arrays
      !     for sorting.
      !----------------------------------------------------------------------
      integer, intent(in)  :: np                   ! Size of a visibility
      integer, intent(in)  :: nv                   ! Number of visibilities
      real, intent(in) :: visi(np,nv)              ! Input visibilities
      real(8), intent(out) :: tb(nv)               ! Output Date+Time
      integer, intent(out) :: it(nv)               ! Indexes
      logical, intent(out) :: sorted               !
      integer, intent(in)  :: idate                ! Date pointer
    end subroutine loadtb
  end interface
  !
  interface
    subroutine uvsort_single_mem(hiuv,houv,ctype,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      !   Support for UV_SORT /FILE
      !   Sort the UV data files by loading all them in Memory 
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: hiuv
      type(gildas), intent(inout) :: houv
      character(len=*), intent(in) :: ctype
      logical, intent(out) :: error
    end subroutine uvsort_single_mem
  end interface
  !
  interface
    subroutine uvsort_mosaic_disk(hiuv,houv,ctype,ioff,joff,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! 
      ! IMAGER
      !   Support for UV_SORT /FILE
      !   Sort a Mosaic UV table - Each field must be independently
      !   sorted.
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: hiuv       ! Input UV header
      type(gildas), intent(inout) :: houv       ! Output UV header
      character(len=*), intent(in) :: ctype     ! Type of sorting
      integer, intent(in) :: ioff               ! Offset column pointer
      integer, intent(in) :: joff               ! Offset column pointer
      logical, intent(out) :: error             ! Error flag
    end subroutine uvsort_mosaic_disk
  end interface
  !
  interface
    subroutine uvsort_single_disk(hiuv,houv,ctype,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      !   Support for UV_SORT /FILE
      ! Sort a Single Field UV table on disk
      !
      ! A more intelligent way of sorting that strictly follows
      ! the algorithm for the Mosaics.
      !
      ! 1) Define the number of input Blocks
      ! 2) Define the Time and/or Base range of the input file
      !     and relative percentiles of the distribution...
      ! 3) Partition the Time and/or Base range in a number
      !   of output files equal to the number of input Blocks
      ! 4) Collect the temporary output files
      !
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: hiuv       ! Input UV header
      type(gildas), intent(inout) :: houv       ! Output UV header
      character(len=*), intent(in) :: ctype     ! Type of sorting
      logical, intent(out) :: error             ! Error flag
    end subroutine uvsort_single_disk
  end interface
  !
  interface
    integer function uvshort_basenum(iant,jant)
      ! @ private
      real, intent(in) :: iant
      real, intent(in) :: jant
    end function uvshort_basenum
  end interface
  !
  interface
    subroutine uvsort_frequency_file(nami,namo,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command UV_SORT FREQUENCY /FILE FileIn FileOut
      !---------------------------------------------------------------------------------
      character(len=*), intent(inout) :: nami  ! Input file name
      character(len=*), intent(inout) :: namo  ! Output file name
      logical, intent(out) :: error
    end subroutine uvsort_frequency_file
  end interface
  !
  interface
    subroutine uvsort_frequency_mem(huv,duv,error)
      use image_def
      use gbl_message
      !-------------------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !   Support for UV_SORT FREQUENCY in memory. Applies to UV data.
      !   Only works for 1 Stokes parameter
      !-------------------------------------------------------------------------------
      type(gildas), intent(inout) :: huv      ! UV Header
      real, intent(inout) :: duv(huv%gil%dim(1),huv%gil%dim(2))         ! UV data
      logical, intent(inout) :: error         ! Error flag
    end subroutine uvsort_frequency_mem
  end interface
  !
  interface
    subroutine uv_add_comm(line,error)
      !$ use omp_lib
      use image_def
      use clean_arrays
      use gbl_format
      use gbl_message
      !-----------------------------------------------------------------------
      ! @ private
      !
      !  Support for command
      !
      !   UV_ADD ITEM [Mode] [/FILE FileIn FileOut]
      !
      ! Task to compute and add some missing information in a UV
      ! Table, such as the Doppler correction and the Parallactic Angle 
      !
      ! Gets the Observatory from the Telescope name in the input UV table,
      ! suggests to use SPECIFY command if not available.
      !-----------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine uv_add_comm
  end interface
  !
  interface
    subroutine sub_uv_doppler_para(vtype,rdate,rtime,doppler,parang, &
            & coord,equinox,lambda,beta,error)
      use gbl_constant
      use phys_const
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! Compute the Doppler correction, calling ASTRO routine, according to:
      ! * observatory location
      ! * reference frame (LSR, Helio...)
      ! Radio convention is used
      !
      ! Also get the Parallactic Angle 
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: vtype    ! Type of referential
      real(kind=4),     intent(in)  :: rdate    ! GAG Date
      real(kind=4),     intent(in)  :: rtime    ! UT Time
      real(kind=4),     intent(out) :: doppler  ! Doppler factor
      real(kind=4),     intent(out) :: parang   ! Parallactic Angle
      character(len=2), intent(in)  :: coord    !
      real(kind=4),     intent(in)  :: equinox  !
      real(kind=8),     intent(in)  :: lambda   !
      real(kind=8),     intent(in)  :: beta     !
      logical,          intent(out) :: error    ! flag
    end subroutine sub_uv_doppler_para
  end interface
  !
  interface
    subroutine find_dummy_column(uvou,icode,mode)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !   Locate a re-usable dummy or empty column
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: uvou
      integer, intent(inout) :: icode
      integer, intent(in)    :: mode
    end subroutine find_dummy_column
  end interface
  !
  interface
    subroutine uv_merge_many(line,error)
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !
      ! UV_MERGE OutFile /FILES In1 In2 ... Inn /WEIGHTS W1 ... Wn /
      !   /SCALES F1 ... Fn [/MODE STACK|CONTINUUM|CONCATENATE|LINE 
      !   [Index [Frequency]]
      !
      ! Merge many UV data files, with calibration factor and weight factors
      ! and spectral resampling as in the first  one.
      !
      ! With /CONTINUUM option, merge continuum UV Tables. A spectral index
      ! can then be automatically applied to the scales and weights.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line ! Command line
      logical, intent(out) :: error
    end subroutine uv_merge_many
  end interface
  !
  interface
    subroutine flux_comm(line,error)
      use image_def
      use gkernel_types
      use clean_arrays
      use moment_arrays
      use clean_support
      use gbl_message
      !
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !       FLUX CURSOR|MASK|SUPPORT [/CCT] [/IMAGE DataCube]
      ! Compute the Flux in the specified region(s)
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine flux_comm
  end interface
  !
  interface
    subroutine sub_flux_comm(line,hmap,error)
      use image_def
      use gkernel_types
      use clean_arrays
      use moment_arrays
      use clean_support
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !       FLUX CURSOR|MASK|SUPPORT [/CCT] [/IMAGE DataCube]
      ! Compute the Flux in the specified region(s)
      !
      ! The output should be a SIC Structure named FLUX
      !   FLUX%NF   The number of fields
      !   FLUX%NC   The number of channels
      !   FLUX%VELOCITIES     The channel velocities
      !   FLUX%FREQUENCIES     The channel frequencies
      !   FLUX%VALUES[Flux%Nc,Flux%Nf]   The spectra for every field
      !   FLUX%ERRORS[Flux%Nc,Flux%Nf]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      type(gildas), intent(in) :: hmap
      logical, intent(inout) :: error
    end subroutine sub_flux_comm
  end interface
  !
  interface
    subroutine label_field(imagein,ncolumns,nlines,labelout,nfields,threshold,  &
      blank,eblank,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! Modifie S.Guilloteau LAB 20 Janv 2013
      !    Order the field numbers by number of pixels
      ! Modifie S.Guilloteau
      !   Groupe d Astrophysique de Grenoble 22 Octobre 1985
      ! Valeurs non definies et declaration LabelOut en reel
      !
      ! Origine : A.Bijaoui 25 octobre 1984 Observatoire de Nice
      ! Saidi1 Permet l'etiquettage des domaines a partir d'une
      ! segmentation avec seuil de flux
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in)    :: ncolumns                   ! Number of "Columns"
      integer(kind=index_length), intent(in)    :: nlines                     ! Number of "lines"
      real(kind=4),               intent(in)    :: imagein(ncolumns,nlines)   ! Input image
      real(kind=4),               intent(out)   :: labelout(ncolumns,nlines)  ! Labels of fields
      integer(kind=4),            intent(out)   :: nfields                    ! Number of fields
      real(kind=4),               intent(in)    :: threshold                  ! Threshold
      real(kind=4),               intent(in)    :: blank                      ! Blanking
      real(kind=4),               intent(in)    :: eblank                     ! and tolerance
      logical,                    intent(inout) :: error                      ! Logical error flag
    end subroutine label_field
  end interface
  !
  interface
    subroutine label_stat(nx,ny,label,data,nf,npix,sums,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      !   @ private
      ! IMAGER
      !     Compute statistics from an image and an associated
      !   label fields.
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nx  ! Number of pixels in X
      integer(kind=index_length), intent(in) :: ny  ! Number of pixels in Y
      real, intent(in) :: label(nx,ny)              ! Label array
      real, intent(in) :: data(nx,ny)               ! Data array
      integer, intent(in) :: nf                     ! Number of fields
      integer, intent(out) :: npix(nf)              ! Number of pixels per field
      real, intent(out) :: sums(nf)                 ! Sum per field
      real, intent(in) :: bval, eval                ! Blanking values
    end subroutine label_stat
  end interface
  !
  interface
    subroutine my_moments(z,head,poly,clip,center,range, &
      sum,aire,npix,mean,sigma,minmax)
      use phys_const
      use image_def
      use greg_types
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! IMAGER
      !   Compute statistics about a 2-D array within a N-gon
      ! Derived from gr8_moments, but using image header instead
      ! of regular grid description
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: z(:,:)     ! Data array
      type(gildas),    intent(in)  :: head       ! Data Header
      type(polygon_t), intent(in)  :: poly       ! Current polygon
      logical,         intent(in)  :: clip       ! Clip out of range values
      real(kind=4),    intent(in)  :: center     ! Center of accepted interval
      real(kind=4),    intent(in)  :: range      ! Half width of accepted interval
      real(kind=4),    intent(out) :: sum        ! Integrated map value
      real(kind=4),    intent(out) :: aire       ! Area of polygon
      integer(kind=4), intent(out) :: npix       ! Number of interior pixels
      real(kind=4),    intent(out) :: mean       ! Mean map value
      real(kind=4),    intent(out) :: sigma      ! RMS value
      real(kind=4),    intent(out) :: minmax(2)  ! Min and max values
    end subroutine my_moments
  end interface
  !
  interface
    subroutine show_sources_comm(line,error)
      use image_def
      use clean_arrays
      use gbl_message
    !-----------------------------------------------------------------------
    ! @ private
    !
    ! IMAGER
    !   Support for command SHOW SOURCES  
    !
    !   SHOW the position of Clean Components and their sizes proportional
    !   to the component Flux. We  must compact the CCT list before
    !   and Transpose it
    !-----------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine show_sources_comm
  end interface
  !
  interface
    subroutine feather_comm(line,error)
      use image_def
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !
      ! FEATHER [/FILE FileMerge FileHigh FileLow] [/REPROJECT]
      !   Uses  FEATHER_RADIUS
      !         FEATHER_SCALE
      !         FEATHER_RANGE[2]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine feather_comm
  end interface
  !
  interface
    subroutine t_uv_feather(nameh,namel,name_out,uvradius,scale,expo,range, &
      & auto,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !
      ! FEATHER /FILE FileMerge FileHigh FileLow [/REPROJECT]
      !   Uses  FEATHER_RADIUS
      !         FEATHER_SCALE
      !         FEATHER_RANGE[2]
      ! "Feather" (in the UV plane) two data cubes
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: nameh    ! HIGHres file
      character(len=*), intent(in) :: namel    ! LOWres file
      character(len=*), intent(in) :: name_out ! Merged file
      real, intent(in) ::  uvradius            ! Transition radius
      real, intent(in) ::  scale               ! Scale factor LOW / HIGH
      real, intent(in) ::  expo                ! Sharpness of transition
      real, intent(in) ::  range(2)            ! Range of overlap (m) 
      logical, intent(in) :: auto              ! Auto reproject
      logical, intent(out) :: error            ! Error flag
    end subroutine t_uv_feather
  end interface
  !
  interface
    subroutine c_uv_feather(all,high,low,uvradius,scale,expo,range, &
      & auto,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !
      ! FEATHER [/REPROJECT]
      !   Uses  FEATHER_RADIUS
      !         FEATHER_SCALE
      !         FEATHER_RANGE[2]
      ! "Feather" (in the UV plane) the SKY and SHOW data cubes
      !   and put the result in FEATHERED 
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: high
      type(gildas), intent(in) :: low 
      type(gildas), intent(inout) :: all
      real, intent(in) ::  uvradius            ! Transition radius
      real, intent(in) ::  scale               ! Scale factor LOW / HIGH
      real, intent(in) ::  expo                ! Sharpness of transition
      real, intent(in) ::  range(2)            ! Range of overlap (m) 
      logical, intent(in) :: auto              ! Auto reproject
      logical, intent(out) :: error            ! Error flag
    end subroutine c_uv_feather
  end interface
  !
  interface
    subroutine spectrum_consistency(rname,ima,imb,tole,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command UV_SHORT
      !
      !   Verify spectral axis consistency
      !---------------------------------------------------------------------
      !
      character(len=*), intent(in) :: rname
      type(gildas), intent(in) :: ima
      type(gildas), intent(in) :: imb
      real, intent(in) :: tole
      logical, intent(out) :: error
    end subroutine spectrum_consistency
  end interface
  !
  interface
    subroutine uv_reweight_comm(line,comm,error)
      use gildas_def
      use gbl_message
      use image_def
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !	  Derive the weights of the UV table from the noise
      !     statistics over many channels
      ! UV_REWEIGHT APPLY Value
      ! UV_REWEIGHT ESTIMATE [Tolerance] [PrintLevel]] [/RANGE Min Max Type] 
      ! UV_REWEIGHT DO [Tolerance] [PrintLevel]] [/RANGE Min Max Type] -
      !     [/FLAG Threshold] [/FILE FileIn FileOut]
      !---------------------------------------------------------------------
      ! Local
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical :: error
    end subroutine uv_reweight_comm
  end interface
  !
  interface
    subroutine uv_get_reweight (out,dout,nvisi,numchan,nc,ishow,thre, &
        & original,ratio,scale,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !	  Support for 
      ! UV_REWEIGHT [PrintLevel] /RANGE  [/NOFLAG]
      !   Derive the weights of the UV table from the noise
      !   statistic over many channels
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: out       ! Visibility header
      real, intent(in) :: dout(:,:)          ! Visibilities
      integer(kind=size_length), intent(in) :: nvisi ! Number of visibilities
      integer, intent(in) :: numchan         ! Number of windows 
      integer, intent(in) :: nc(2*numchan)   ! Channel windows
      integer, intent(in) :: ishow           ! Show changes period
      real, intent(in) :: thre               ! Threshold for choice of noise estimate
      real, intent(out) :: original(nvisi)   ! Original weights
      real, intent(out) :: ratio(nvisi)      ! Ratio of weights
      real, intent(out) :: scale             ! Global scale factor
      logical, intent(out) :: error          ! Error flag
    end subroutine uv_get_reweight
  end interface
  !
  interface
    subroutine uv_do_reweight(out, dout, nvisi, flag, nbad, original, & 
      & ratio, scale, fthre)
      use image_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !   Support routine for command
      ! UV_REWEIGHT DO [/FLAG Threshold]
      !
      ! Apply scaling factor which has been derived, and return the
      ! number of "anomalous" visibilities.
      !---------------------------------------------------------------------  
      type (gildas), intent(inout) :: out    ! Visibility header
      real, intent(inout) :: dout(:,:)       ! Visibilities
      integer(kind=size_length), intent(in) :: nvisi   ! Number of visibilities
      logical, intent(in) :: flag            ! Flag anomalous re-scale ?
      integer, intent(inout) :: nbad         ! Number of potentially "bad" visibilities
      real, intent(in) :: original(nvisi)    ! Original weights
      real, intent(in) :: ratio(nvisi)       ! Ratio of weights
      real, intent(in) :: scale              ! Global scale factor
      real, intent(in) :: fthre              ! Worry factor
    end subroutine uv_do_reweight
  end interface
  !
  interface
    subroutine get_uvranges(pname,uvchannel,ctype,mranges,numchan,nc,y,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Convert ranges defined in a String to (integer) Channel ranges
      !   for a UV data.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: pname     ! Caller name
      character(len=*), intent(in) :: uvchannel ! String to be decoded
      character(len=*), intent(inout) :: ctype  ! Type of values
      integer, intent(in) :: mranges            ! Maximum Number of ranges
      integer, intent(out) :: numchan           ! Actual number of ranges
      integer, intent(out) :: nc(mranges)       ! Channel ranges
      type(gildas), intent(in) :: y             ! Header of UV data
      logical, intent(out) :: error             ! Error flag
    end subroutine get_uvranges
  end interface
  !
  interface
    subroutine uv_time_reweight (out,dout,nvisi,numchan,nc,diam,thre, &
        & original,ratio,scale,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !	  Support for 
      ! UV_REWEIGHT TIME [PrintLevel] /RANGE  [/NOFLAG]
      !   Derive the weights of the UV table from the noise
      !   statistic over many consecutive visibilities 
      !
      ! This assumes that Visibilities are sufficiently well sampled so
      ! that the differences between adjacent ones (in time) only differ
      ! by the noise, not by the source structure.
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: out       ! Visibility header
      real, intent(in) :: dout(:,:)          ! Visibilities
      integer(kind=size_length), intent(in) :: nvisi ! Number of visibilities
      integer, intent(in) :: numchan         ! Number of windows 
      integer, intent(in) :: nc(2*numchan)   ! Channel windows
      real, intent(in) :: diam               ! Tolerance radius in UV space
      real, intent(in) :: thre               ! Threshold for choice of noise estimate
      real, intent(out) :: original(nvisi)   ! Original weights
      real, intent(out) :: ratio(nvisi)      ! Ratio of weights
      real, intent(out) :: scale             ! Global scale factor
      logical, intent(out) :: error          ! Error flag
    end subroutine uv_time_reweight
  end interface
  !
  interface
    subroutine map_center(line,task,huv,shift,newabs,error)
      use gkernel_types
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !
      ! Support for commands which can interpret MAP_CENTER-like string syntax.
      !   UV_MAP,  UV_SHIFT,  UV_DEPROJECT
      ! Decode the phase center and rotation parameters, either from
      ! the MAP_CENTER string, or (if present) from the command 
      ! line arguments.
      !
      ! The MAP_CENTER string can be specified by LET MAP_CENTER,
      !
      ! The syntax can be
      !   PosX PosY UNIT [[ANGLE] AngleValue]
      !   ANGLE AngleValue [PosX PosY UNIT]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line     ! Command line
      character(len=*), intent(in) :: task     ! Task name
      type(gildas), intent(inout) :: huv       ! UV data set
      logical, intent(out) :: shift            ! Must shift data ?
      real(kind=8), intent(inout) :: newabs(3) ! Shifting coordinates
      logical, intent(inout) :: error          ! Error flag
    end subroutine map_center
  end interface
  !
  interface
    subroutine print_change_header(task,huv,newabs,shift)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Debugging routine
      !---------------------------------------------------------------------  
      character(len=*), intent(in) :: task
      type(gildas), intent(in) :: huv
      real(kind=8), intent(in) :: newabs(3)
      logical, intent(in) :: shift
    end subroutine print_change_header
  end interface
  !
  interface
    subroutine map_query (line,task,map,goon,error) 
      use gbl_message
      use gkernel_types
      use image_def
      use clean_arrays
      use clean_default
      !---------------------------------------------------------------------
      ! @ private
      !
      ! Support for commands
      !   UV_MAP ?
      !     Display the imaging parameters
      ! or
      !   UV_MAP 
      !     Set the arguments for the phase center and rotation parameters
      !     if MAP_CENTER is not empty
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  ! Command line
      character(len=*), intent(in) :: task  ! Task name
      type(uvmap_par), intent(in) :: map    ! Mapping parameters 
      logical, intent(inout) :: goon        ! Continue or Return after ?
      logical, intent(inout) :: error       ! Error flag
    end subroutine map_query
  end interface
  !
  interface
    subroutine uv_flag_comm(line,error)
      use clean_def
      use clean_arrays
      use clean_support
      use clean_types
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER Support routine for command
      !     UV_FLAG [/ANTENNA] [/DATE] [/RESET] [/FILE FileIn [FileOut]]
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Logical error flag
    end subroutine uv_flag_comm
  end interface
  !
  interface
    subroutine get_uvflag_ant(uvs,nv,nd,iant)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Set uvflag array from antenna number
      !----------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nv,nd        ! Size of UV data
      real, intent(inout) :: uvs(nv,nd)
      integer, intent(in) :: iant
    end subroutine get_uvflag_ant
  end interface
  !
  interface
    subroutine get_uvflag_date(uvs,nv,nd,idate)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Set uvflag array from (gildas) Date number
      !----------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nv,nd        ! Size of UV data
      real, intent(inout) :: uvs(nv,nd)
      integer, intent(in) :: idate
    end subroutine get_uvflag_date
  end interface
  !
  interface
    subroutine get_uvflag(spol,uvs,nv,nd)
      use gildas_def
      use clean_support
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Get uvflag array from polygon (0 => Flag, 1 => Keep)
      !----------------------------------------------------------------------
      type(polygon_t), intent(in) :: spol                ! User defined region polygon
      integer(kind=index_length), intent(in) :: nv,nd    ! Size of UV data
      real, intent(inout) :: uvs(nv,nd)
    end subroutine get_uvflag
  end interface
  !
  interface
    subroutine reset_uvflag(uvs,nv,nd)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Reset uvflag array to 1
      !----------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nv,nd        ! Size of UV data
      real, intent(inout) :: uvs(nv,nd)   ! UV data set
    end subroutine reset_uvflag
  end interface
  !
  interface
    subroutine apply_uvflag(uvs,nv,nd,uv)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Apply uvflag array by making negative the weight of the flagged data
      !----------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nv,nd    ! Size of UV data
      real, intent(inout) :: uvs(nv,nd+3)   ! UV data set with Flags
      real, intent(inout) :: uv(nd,nv)      ! Transposed version
    end subroutine apply_uvflag
  end interface
  !
  interface
    subroutine uv_flag_file(line,error)
      use gildas_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !	  Support for command 
      !   UV_FLAG /File FileIn [FileOut] [/DATE] [/ANTENNA] [/RESET] 
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine uv_flag_file
  end interface
  !
  interface
    subroutine sub_doflag (out,nx,nv,ibase, iant, nc, rflag, ut_start, ut_end)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !
      ! Support routine for UV_FLAG
      ! Conditionally change sign of weights according to RFLAG
      !---------------------------------------------------------------------
      integer(kind=index_length) :: nx    ! Size of a visibility
      integer(kind=index_length) :: nv    ! Number of visibilities
      real :: out(nx,nv)                  ! Visibilities
      integer :: ibase                    ! Baseline number (0 = any)
      integer :: iant(:)                  ! Antenna number  (0 = any)
      integer :: nc(2)                    ! First and Last channel (0,0 = all)
      real :: rflag                       ! Flag (<0) or Unflag (>0)
      real(8), optional :: ut_start               ! Start time
      real(8), optional :: ut_end                 ! End time
    end subroutine sub_doflag
  end interface
  !
  interface
    integer function basant(ri,rj)
      ! @ private
      real, intent(in) :: ri,rj
    end function basant
  end interface
  !
  interface
    subroutine stokes_comm(line,error)
      use image_def
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !
      ! STOKES Value [/FILE FileIn FileOut]
      !
      ! Extract a Polarization state from a UV table
      !
      !---------------------------------------------------------------------
      character(len=*) :: line
      logical error
    end subroutine stokes_comm
  end interface
  !
  interface
    subroutine sub_splitpolar(nami,namo,mystoke,hin,error)
      use image_def
      use gbl_message
      !
      ! @ private
      !
      character(len=*), intent(inout) :: nami  ! Input file name
      character(len=*), intent(inout) :: namo  ! Output file name
      character(len=*), intent(in) :: mystoke  ! Desired Stoke parameter
      type (gildas), intent(inout) :: hin
      logical, intent(out) :: error
    end subroutine sub_splitpolar
  end interface
  !
  interface
    subroutine stokes_extract(inorder,din,dou,nvisi,nlead,natom,nchan,nstok, & 
      & ntrail,ivisi)
      use image_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !
      ! Support routine for STOKES command: extract the relevant
      ! Stokes parameter from the input visibilities. 
      !---------------------------------------------------------------------  
      real(kind=4), intent(in) :: din(:,:)
      real(kind=4), intent(out) :: dou(:,:)
      integer, intent(in) :: inorder ! Code order
      integer, intent(in) :: nvisi  ! number of visibilities
      integer, intent(in) :: nlead  ! number of leading columns
      integer, intent(in) :: natom  ! Atomic length of a visibility (2 or 3)
      integer, intent(in) :: nchan  ! number of channels
      integer, intent(in) :: nstok  ! number of input Stokes
      integer, intent(in) :: ntrail ! number of trailing columns
      integer, intent(in) :: ivisi  ! Pointer into Stokes parameter
    end subroutine stokes_extract
  end interface
  !
  interface
    subroutine stokes_derive_stok(din,dou,nvisi,nlead,natom,nchan,nstok,ntrail, &
      & istoke,astoke,ipara,isign,error, cstoke)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !
      ! Support routine for STOKES command: Derive the relevant
      ! Stokes parameter from the input visibilities. This routine works
      ! for CODE_STOKE_CHAN order, and 2 or 4 input Stokes parameters.
      ! Not all combinations are allowed, however.
      !
      ! Stokes vary first,  Channel next
      !---------------------------------------------------------------------  
      real(kind=4), intent(in) :: din(:,:)
      real(kind=4), intent(out) :: dou(:,:)
      integer, intent(in) :: nvisi  ! Number of visibilities
      integer, intent(in) :: nlead  ! number of leading columns
      integer, intent(in) :: natom  ! Atomic length of a visibility (2 or 3)
      integer, intent(in) :: nchan  ! number of channels
      integer, intent(in) :: nstok  ! number of input Stokes
      integer, intent(in) :: ntrail ! number of trailing columns
      integer, intent(in) :: istoke ! desired Stokes parameter
      integer, intent(in) :: astoke(nstok)   ! Input Stokes parameters
      integer, intent(in) :: ipara  ! Parallactic angle column
      integer, intent(in) :: isign  ! test integer for PA angle
      logical, intent(out) :: error ! Error flag
      integer, intent(in) :: cstoke(5)
    end subroutine stokes_derive_stok
  end interface
  !
  interface
    subroutine stokes_derive_chan(din,dou,nvisi,nlead,natom,nchan,nstok,ntrail, &
      & istoke,astoke,ipara,isign,error, cstoke)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !
      ! Support routine for STOKES command: Derive the relevant
      ! Stokes parameter from the input visibilities. This routine works
      ! for CODE_CHAN_STOKE order, and 2 or 4 input Stokes parameters.
      !
      ! CSTOKE handles the ordering of the Polarization states.
      !---------------------------------------------------------------------  
      real(kind=4), intent(in) :: din(:,:)
      real(kind=4), intent(out) :: dou(:,:)
      integer, intent(in) :: nvisi  ! Number of visibilities
      integer, intent(in) :: nlead  ! number of leading columns
      integer, intent(in) :: natom  ! Atomic length of a visibility (2 or 3)
      integer, intent(in) :: nchan  ! number of channels
      integer, intent(in) :: nstok  ! number of input Stokes
      integer, intent(in) :: ntrail ! number of trailing columns
      integer, intent(in) :: istoke ! desired Stokes parameter
      integer, intent(in) :: astoke(nstok)   ! Input Stokes parameters
      integer, intent(in) :: ipara  ! Parallactic angle column
      integer, intent(in) :: isign  ! test integer for PA angle
      logical, intent(out) :: error ! Error flag
      integer, intent(in) :: cstoke(5)  ! Order of Polarization components
    end subroutine stokes_derive_chan
  end interface
  !
  interface
    subroutine sub_splitpolar_mem(mystoke,huv,error)
      use image_def
      use clean_types
      use gbl_message
      !
      ! @ private
      !
      character(len=*), intent(in) :: mystoke  ! Desired Stoke parameter
      type (gildas), intent(inout) :: huv
      logical, intent(out) :: error
    end subroutine sub_splitpolar_mem
  end interface
  !
  interface
    function findloc(array,value,dim)
      ! @ private-mandatory
      integer :: findloc
      integer, intent(in) :: array(:)
      integer, intent(in) :: value
      integer, intent(in) :: dim
    end function findloc
  end interface
  !
  interface
    subroutine uv_fields_comm(line,comm,error)
      use gildas_def
      use image_def
      use gkernel_types
      use gbl_message
      use clean_arrays
      !
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGE
      !   Support command for @ fits_to_uvt.ima,
      !   the UVFITS to UV Table conversion script.
      !
      !   UV_FIELDS RaVar DecVar [Unit] [/FILE FileIn FileOut]
      !       [/CENTER Ra Dec]
      !
      !     Replace fields ID by Fields offsets.
      !
      ! For files imported from UVFITS, the Fields offsets are found in 
      !   the AIPS SU Table which is not decoded by the FITS command, 
      !   but only through the DEFINE FITS command.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(out) :: error
    end subroutine uv_fields_comm
  end interface
  !
  interface
    subroutine sub_file_fields (huvin, nf, offs, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for @ fits_to_uvt.map
      !     Replace fields ID by Fields offsets
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: huvin    ! Input Header
      integer, intent(in) :: nf                ! Number of fields
      real(kind=8), intent(in) :: offs(2,nf)   ! Offset values
      logical, intent(out) :: error            ! Error flag
    end subroutine sub_file_fields
  end interface
  !
  interface
    subroutine sub_memory_fields (huvin, duv, nf, offs, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for @ fits_to_uvt.map
      !     Replace fields ID by Fields offsets
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: huvin     ! Input / Output file UV header
      real, intent(inout) :: duv(:,:)          ! UV data
      integer, intent(in) :: nf                ! Number of fields
      real(kind=8), intent(in) :: offs(2,nf)   ! Offset values
      logical, intent(out) :: error            ! Error flag
    end subroutine sub_memory_fields
  end interface
  !
  interface
    subroutine map_polar(line,comm,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !     Support for command
      !   MAP_POLAR NameBegin NameEnd /COMPUTE Threshold
      !     [/BACKGROUND Type Min Max] [/STEP Step Xfirst Yfirst]
      !
      ! Withe /COMPUTE, derive 
      !   1) Polarized fraction
      !   2) Polarization angle
      ! from I,Q,U (and V ?) polarization images
      !
      ! Without, display the polarization maps as controlled by the options
      !   /BACKGROUND and /STEP
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line, comm
      logical, intent(inout) :: error  
    end subroutine map_polar
  end interface
  !
  interface
    subroutine gather_self(line,comm,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     COLLECT FileFilter MergedFile [ReferenceAntenna] [/FLAG Threshold] 
      !
      !   Average Antenna-Based self-calibration solutions from different 
      ! frequencies into a  single one, assuming the self-calibration "phases"
      ! are actually Pathlengths (delays).
      !   Flag unstable solutions if more than 1 iteration is present.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine gather_self
  end interface
  !
  interface
    subroutine derive_base(line,comm,error)
      use gkernel_types
      use gbl_message
      use clean_arrays
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     DERIVE Agains [CgainsFile]
      !
      !   Derive Baseline-based (Cgains) solution from an Antenna-Based gain
      ! solution.  The Baseline-based solution is stored in buffer
      ! CGAINS, unless a file name is given by argument CgainsFile.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine derive_base  
  end interface
  !
  interface
    subroutine report_time(elapsed)
      !$ use omp_lib
      ! @ private
      real(8), intent(inout) :: elapsed
    end subroutine report_time
  end interface
  !
  interface
    subroutine report_init(elapsed)
      !$ use omp_lib
      ! @ private
      real(8) :: elapsed
    end subroutine report_init
  end interface
  !
  interface
    subroutine debug_all(line,error)
      !$ use omp_lib
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !----------------------------------------------------------------------
      ! @  private
      !
      ! IMAGER     Print debug information
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine debug_all
  end interface
  !
  interface
    subroutine mode_mosaic (line,error)
      !----------------------------------------------------------------------
      ! @  private
      !
      ! IMAGER      MOSAIC ON|OFF
      !             Activates or desactivates the mosaic mode
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(out) :: error
    end subroutine mode_mosaic
  end interface
  !
  interface
    subroutine sic_insert_log(line)
      !----------------------------------------------------------------------
      ! @ private
      !   Insert command line in Stack and Log file
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line
    end subroutine sic_insert_log
  end interface
  !
  interface
    subroutine quiet_message(line,quiet)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !   Test if command should be "Quiet", i.e. is only a Help-like command
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: quiet
    end subroutine quiet_message
  end interface
  !
  interface
    subroutine comm_proper_motion(line,comm,error)
      use clean_arrays
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command
      !     PROPER_MOTION PmRa PmDec [/FILE In Out]
      !   Apply specified proper motion to current or specified UV Table
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine comm_proper_motion
  end interface
  !
  interface
    subroutine proper_motion_file (rname, cuvin, cuvou, proper, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !      Apply specified proper motion to an input UV Table
      !   and write it on an output UV table
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname 
      character(len=*), intent(in) :: cuvin
      character(len=*), intent(in) :: cuvou
      real(8), intent(in) :: proper(2)
      logical, intent(out) :: error
    end subroutine proper_motion_file
  end interface
  !
  interface
    subroutine proper_motion (mu,huv,nvisi,visi)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !      Support for command PROPER_MOTION
      !   Apply specified proper motion to an input UV Table
      !   and write it on an output UV table
      !---------------------------------------------------------------------
      real(8), intent(in) :: mu(2)      ! Proper motion mas/yr
      type (gildas), intent(in) :: huv  ! UV Header
      integer, intent(in) :: nvisi      ! Number of visibilities
      real(4), intent(inout) :: visi(huv%gil%dim(1),nvisi)  ! Visibility array
    end subroutine proper_motion
  end interface
  !
  interface
    subroutine reproject_comm(line,error)
      use image_def
      use gkernel_types
      use gbl_message
      use clean_arrays
      use iso_c_binding
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support routine for command
      !
      ! REPROJECT Out In 
      !   /BLANKING Bval Eval
      !   /LIKE Template
      !   /PROJECTION Type Cx Cy [Unit] [Angle]
      !   /SYSTEM Type [Equinox]
      !   /X_AXIS Nx Ref Val Inc
      !   /Y_AXIS Ny Ref Val Inc
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine reproject_comm
  end interface
  !
  interface
    subroutine s_reproject_init(in,tem,out,sys_code,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Prepare Spatial resampling of a data cube as specified by 
      !       a Template
      !     Prepare the Ouput header and Code
      !---------------------------------------------------------------------  
      type(gildas), intent(in) :: in      ! Input data cube
      type(gildas), intent(in) :: tem     ! Grid template
      type(gildas), intent(inout) :: out  ! Output data cube
      integer, intent(out) :: sys_code    ! Reprojection code
      logical :: error
    end subroutine s_reproject_init
  end interface
  !
  interface
    subroutine s_reproject_do(in,din,out,dout,sys_code,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !     Spatial resampling of a data cube, according to Header and Code
      !     Use s_reproject_init to prepare the Output header
      !---------------------------------------------------------------------  
      type(gildas), intent(in) :: in
      type(gildas), intent(inout) :: out
      real, intent(in) :: din(in%gil%dim(1),in%gil%dim(2),in%gil%dim(3))
      real, intent(out) :: dout(out%gil%dim(1),out%gil%dim(2),out%gil%dim(3))
      integer, intent(in) :: sys_code
      logical :: error
    end subroutine s_reproject_do
  end interface
  !
  interface
    subroutine sanity_check(rname,x,error)
      use image_def
      use gbl_message
      !-------------------------------------------------------------------
      ! @ private
      ! Sanity check for input or template file
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(gildas), intent(in)    :: x
      logical,      intent(inout) :: error
    end subroutine sanity_check
  end interface
  !
  interface
    subroutine position_check(rname,x,y,result)
      use image_def
      use gbl_message
      !-------------------------------------------------------------------
      ! @ private
      ! Sanity check for input or template file
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(gildas), intent(in)    :: x,y
      logical,      intent(inout) :: result
    end subroutine position_check
  end interface
  !
  interface
    subroutine gridlin(a,mx,my,mz,aconv,atype,aproj,aepoc,ablank,  &
                       b,nx,ny,   bconv,btype,bproj,bepoc,bblank,  &
                       code,error)
      use gildas_def
      use gkernel_types
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      !     Extracted from GREG Stand alone subroutine
      !  Regrid an input map in a given projection to an output map in
      !  another projection, bilinear interpolation
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: mx  ! Size of A
      integer(kind=index_length), intent(in) :: my  ! Size of A
      integer(kind=index_length), intent(in) :: mz  ! Size of A
      real(kind=4)    :: a(mx,my,mz)  ! Input map of dimensions MX MY MZ
      real(kind=8)    :: aconv(6)     ! Pixel conversion formulae: CONV(1) = Xref, CONV(2)=Xval, CONV(3)=Xinc
      integer(kind=4) :: atype        ! Type of projection
      real(kind=8)    :: aproj(3)     ! Projection constants PROJ(1)=A0, PROJ(2)=D0, PROJ(3)=Angle
      real(kind=4)    :: aepoc        ! Epoch if Needed
      real(kind=4)    :: ablank(2)    !
      integer(kind=index_length), intent(in) :: nx   ! Size of B
      integer(kind=index_length), intent(in) :: ny   ! Size of B
      real(kind=4)    :: b(nx,ny,mz)  ! Output map of dimensions NX,NY,MZ
      real(kind=8)    :: bconv(6)     !
      integer(kind=4) :: btype        !
      real(kind=8)    :: bproj(3)     !
      real(kind=4)    :: bepoc        ! Epoch if Needed
      real(kind=4)    :: bblank       ! Blanking value
      integer(kind=4) :: code         ! Galactic to Equatorial (-1) or Equatorial to Galactic (1)
      logical, intent(out) :: error   !
    end subroutine gridlin
  end interface
  !
  interface
    subroutine combine_comm(line,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! COMBINE Out Code Line Cont /FACTOR  Fl Fc /THRESHOLD  Tl Tc /BLANKING
      !   Combination with automatic reprojection (and later resampling)
      !   to match the two input data cubes
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical, intent(inout) :: error
    end subroutine combine_comm
  end interface
  !
  interface
    subroutine map_combine_comm(line,error)
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !   IMAGER, from Task COMBINE code
      !
      ! MAP_COMBINE Out CODE In1 In2 /factor A1 A2 /THRESHOLD T1 T2 /BLANKING Bval
      !     Combine in different ways two input images
      !        (or data cubes)...
      !
      !     Out can be a file name or an existing Image variable. 
      !         The distinction is made by the existence of a "." in the name
      !         If it is a file, it is created like the In1 Variable
      !         If it is an Image variable, it must match the shape of 
      !           the In1 Variable
      !     In1 can be a file name or an existing Image variable.
      !     In2 can be a file name or an existing Image variable.
      !         The rank of In2 must be smaller than that of In1, and
      !         other dimensions must match          
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Input command line
      logical, intent(out) :: error         ! Return error flag
    end subroutine map_combine_comm
  end interface
  !
  interface
    subroutine do_combine(namex,namey,namez,code,c,ay,ty,az,tz,error,b)
      use gkernel_types
      use gbl_message
      use iso_c_binding
      !----------------------------------------------------------------------
      ! @ private-mandatory
      !
      !   IMAGER  Support routine for command
      !
      ! COMBINE Out CODE In1 In2 /factor A1 A2 /THRESHOLD T1 T2 /BLANK Bval
      !     Combine in different ways two input images
      !        (or data cubes)...
      !
      !     Out can be a file name or an existing Image variable. 
      !         The distinction is made by the existence of a "." in the name
      !         If it is a file, it is created like the In1 Variable
      !         If it is an Image variable, it must match the shape of 
      !           the In1 Variable
      !     In1 can be a file name or an existing Image variable.
      !     In2 can be a file name or an existing Image variable.
      !         The rank of In2 must be smaller than that of In1, and
      !         other dimensions must match          
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: namex ! Output file
      character(len=*), intent(inout) :: namey ! Input file 1
      character(len=*), intent(inout) :: namez ! Input file 2
      character(len=*), intent(inout) :: code  ! Combination code
      real, intent(in) :: c  ! Offset
      real, intent(in) :: ay ! Y Factor
      real, intent(in) :: ty ! Y Threshold
      real, intent(in) :: az ! Z Factor
      real, intent(in) :: tz ! Z Threshold
      logical, intent(out) :: error
      real, intent(in), optional :: b  ! Blanking
    end subroutine do_combine
  end interface
  !
  interface
    subroutine sub_readhead(rname,namez,hz,z_image,error,rdonly,fmt,type)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !     IMAGER
      !   Return the Header of a GILDAS File or SIC Image variable
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname   ! Caller name
      character(len=*), intent(in) :: namez   ! Name of File or Variable
      type(gildas), intent(out) :: hz         ! GILDAS Header
      logical, intent(inout) :: z_image       ! Is it an image ?
      logical, intent(out) :: error           ! Error flag
      logical, intent(out), optional :: rdonly
      integer, intent(in), optional :: fmt
      character(len=*), intent(in), optional :: type
    end subroutine sub_readhead
  end interface
  !
  interface
    subroutine uv_shift_file(line,comm,error)
      use clean_def
      use clean_default
      use phys_const
      use gkernel_types
      use gbl_message
      !-------------------------------------------------------------------
      ! @ private
      !
      !  IMAGER 
      !   Support routine for command
      !       UV_SHIFT [Xpos Ypos UNIT] [ANGLE [Angle]]  [/FILE FileIn]
      !
      !   Phase shift a Mosaic or Single Field UV Table to a new
      !   common phase center and orientation
      !     Offx OffY are offsets in Angle UNIT or ABSOLUTE values
      !               (default 0,0)
      !     Angle     is the final position angle from North in Degree
      !               (default: no change)
      !
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name 
      logical error                         ! Logical error flag
    end subroutine uv_shift_file
  end interface
  !
  interface
    subroutine detect_comm(line,comm,error)
      use gbl_message
      use clean_default
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command
      !   UV_DETECT Result [Data[.uvt] Model[.lmv-clean]]
      ! Compute Frequency Cross-Correlation spectrum of a UV data set
      ! with that derived from the same UV coverage from a image data cube
      !   This routine encapsulate the spectral resampling between the 
      ! image and the UV data and the derivation of the UV model before 
      ! calling UV_CORRELATE to perform the UV plane correlation.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
      character(len=*), intent(in) :: comm  ! Command name
      logical, intent(inout) :: error       ! Error flag
    end subroutine detect_comm
  end interface
  !
  interface
    subroutine correlate_comm(line,comm,error)
      use image_def
      use clean_arrays
      use gbl_message
      !$ use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Support for command
      !   UV_CORRELATE Result [/FILE Data Model]
      ! Compute Frequency Cross-Correlation spectrum of two UV data sets
      ! This routine assumes proper velocity matching and Visibility
      ! matching between the UV data sets.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine correlate_comm
  end interface
  !
  interface
    subroutine uvcorrel(uvdata,nc,kernel,nk,norm,current,mc)
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      ! Compute complex cross-correlation of two visibilities
      !---------------------------------------------------------------------  
      real, intent(in) :: uvdata(:)   ! UV data to be matched
      real, intent(in) :: kernel(:)   ! Matching Kernel 
      integer, intent(in) :: nc       ! Number of channels of uvdata
      integer, intent(in) :: nk       ! Kernel size. 
      real, intent(out) :: norm(:)    ! Normalisation factor (per channel)
      real, intent(out) :: current(:) ! Correlation spectrum
      integer, intent(in) :: mc       ! Size  of correlation spectrum
    end subroutine uvcorrel
  end interface
  !
  interface
    subroutine kepler_comm(line,comm,quiet,error)
      use gkernel_types
      use clean_default
      use clean_arrays
      use gbl_message
      use iso_c_binding
    !-----------------------------------------------------------------------
    ! @ private
    ! IMAGER
    !
    !   Support for command
    !   KEPLER [?|DataCube|SHOW Arg] [/VSYSTEM Value] [/RESET [Script]
    !     [/VELOCITY R function(R)] [/MASK [File]] [/HFS [File.hfs]]
    !     [/WIDGET]
    !
    ! Input parameters
    !     KEPLER_X0   KEPLER_Y0     Center of disk (arcsec)
    !     KEPLER_ROTA KEPLER_INCLI  Orientation, Inclination in °
    !     KEPLER_VMASS              Rotation velocity at 100 au (km/s)
    !     KEPLER_VDISK              Disk velocity (km/s)
    !     KEPLER_DIST               Distance (pc)
    !     KEPLER_ROUT               Outer radius (au) for the Spectrum
    !     KEPLER_RINT               Inner radius (au) ...
    !     KEPLER_RMIN               Minimum radius (au) for the profile
    !     KEPLER_RMAX               Maximum radius (au) for the profile
    !     KEPLER_STEP               Radial sampling (au)
    !     KEPLER_THETA              Maximum angle from major axis in °
    !     KEPLER_AZIMUT[2]          Valid Azimut range (°)
    ! Input data
    !     CLEAN Image (by default) or specified data cube
    ! Output data
    !     Integrated spectral line profile, with error bars
    !     Radial profile at line center
    !     Radius-Velocity diagram ("teardrop" plot)
    !
    ! Open issues:
    !     Should the profile be radially weighted ?
    !     How ?
    !-----------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: quiet
      logical, intent(out) :: error
    end subroutine kepler_comm
  end interface
  !
  interface
    subroutine kepler_compute(line,hmap,data,error)
      use gkernel_types
      use clean_arrays
      use gbl_message
      !$ use omp_lib
    !-----------------------------------------------------------------------
    ! @ private
    ! IMAGER
    !
    !   Support for command
    !   KEPLER [Arg] [/VSYSTEM Value] [/VELOCITY R function(R)]
    !
    ! Output data
    !     Integrated spectral line profile, with error bars
    !     Radial profile at line center
    !     Spectral profiles as a function of radius, with error bars
    !
    !-----------------------------------------------------------------------
      character(len=*), intent(in) :: line ! Command line
      type(gildas), intent(inout) :: hmap
      real, intent(in) :: data(hmap%gil%dim(1),hmap%gil%dim(2),hmap%gil%dim(3))
      logical, intent(out) :: error
    end subroutine kepler_compute
  end interface
  !
  interface
    subroutine kepler_rms(t,ie,is,z)
      !---------------------------------------------------------------------
      ! @ private
      ! IMAGER
      !
      !   Support for command
      !   KEPLER [Arg] [/VSYSTEM Value] [/VELOCITY R function(R)]
      !
      ! Estimate noise in the spectrum
      !---------------------------------------------------------------------
      real(4), intent(in) :: t(*)     ! Input data array
      integer, intent(in) :: ie,is    ! Range to be considered
      real(4), intent(out) :: z       ! RMS value
    end subroutine kepler_rms
  end interface
  !
  interface
    subroutine kepler_init(error)
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      ! IMAGER
      !
      !   Support for command KEPLER
      !
      ! Initialize all variables
      !---------------------------------------------------------------------
      logical, intent(out) :: error
    end subroutine kepler_init
  end interface
  !
  interface
    subroutine kepler_mask(line,hmap,data,error)
      !$ use omp_lib
      use gkernel_types
      use clean_arrays
      use gbl_message
    !-----------------------------------------------------------------------
    ! @ private
    ! IMAGER
    !
    !   Support for command
    !   KEPLER /MASK File [Threshold] [/VELOCITY R Function(R) ] [/VSYSTEM Value]
    !
    ! Create a Mask for further use (Deconvolution, Flux computation)
    !
    ! Output data
    !     Keplerian Mask for deconvolution
    !
    !-----------------------------------------------------------------------
      character(len=*), intent(in) :: line ! Command line
      type(gildas), intent(inout) :: hmap
      real, intent(inout) :: data(hmap%gil%dim(1),hmap%gil%dim(2),hmap%gil%dim(3))
      logical, intent(out) :: error
    end subroutine kepler_mask
  end interface
  !
  interface
    subroutine comm_discard(line,comm,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !
      !---------------------------------------------------------------------
      ! @ private
      !   Support for command DISCARD
      !
      ! Delete some internal buffer and its associated SIC variable
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(out) :: error
    end subroutine comm_discard
  end interface
  !
  interface
    subroutine cct_merge_comm(line,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER  Support for command 
      !
      ! CCT_MERGE OutFile In1 In2 
      !
      !     Merge two Clean Component Tables.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line ! Command line
      logical, intent(out) :: error
    end subroutine cct_merge_comm
  end interface
  !
  interface
    subroutine cct_convert_comm(line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Support for command 
      !
      !         CCT_CONVERT [Threshold]
      !
      !   Convert the CLEAN image into the CCT table
      !
      !     Theshold is the minimum (asbolute value of) flux per pixel 
      !   retained. Default is 0
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine cct_convert_comm
  end interface
  !
  interface
    subroutine cct_to_clean(method,hclean,clean,tcc)
      use clean_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Build clean image from Component List
      !---------------------------------------------------------------------
      type (clean_par), intent(inout) :: method    ! Clean method parameters
      type (gildas), intent(inout) :: hclean       ! Clean header
      real, intent(inout) :: clean(hclean%gil%dim(1),hclean%gil%dim(2))
      type (cct_par), intent(in) :: tcc(method%n_iter)  ! Clean components
    end subroutine cct_to_clean
  end interface
  !
  interface
    subroutine cct_mask_comm(line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Support for command 
      !
      !         CCT_MASK  or rather MASK APPLY CCT
      !
      !   Apply the MASK to the CCT table
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine cct_mask_comm
  end interface
  !
  interface
    subroutine cct_clean_comm(line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER    Support for command 
      !
      !         CCT_CLEAN [MaxIter]
      !
      !   Convert the CCT table into the CLEAN image
      !
      !     MaxIter is the last iteration retained.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine cct_clean_comm
  end interface
  !
  interface
    subroutine spectral_comm(line,comm,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      ! Support routine for 
      !   SPECTRAL_CLEAN Method Control
      ! Drive a multi-spectral Clean deconvolution on a Data Cube
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm  
      logical, intent(inout) :: error
    end subroutine spectral_comm
  end interface
  !
  interface
    subroutine transform_comm(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   IMAGER
      !
      ! Support routine for 
      !   TRANSFORM Operation In Out [Key]
      ! Transform a data cube along the Frequency axis, by several methods
      !   - Wavelet
      !   - Fourier
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line
      logical, intent(out) :: error
    end subroutine transform_comm
  end interface
  !
  interface
    subroutine imager_tree(caller)
      use gbl_message
      use clean_default 
      !---------------------------------------------------------------------
      ! IMAGER
      !   @ private
      ! Printout debug message for callers when asked for
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: caller
    end subroutine imager_tree
  end interface
  !
  interface
    subroutine uvmap_cols(task,line,huv,mcol,wcol,error)
      use image_def
      !---------------------------------------------------------------------
      ! IMAGER
      !   @ private
      !     Define the channel range and Weight column
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task  ! Callers name
      character(len=*), intent(in) :: line  ! Command line with /RANGE option
      type(gildas), intent(in) :: huv       ! UV data header
      integer, intent(inout) :: mcol(2)     ! Ordered channel min max
      integer, intent(inout) :: wcol        ! Weight channel
      logical, intent(out) :: error         ! Error flag
    end subroutine uvmap_cols
  end interface
  !
  interface
    subroutine map_copy_par(in,out)
      use clean_def
      !---------------------------------------------------------------------
      !
      ! @ private
      !   Copy a MAP structure to another one, but avoid erasing
      !   the number of fields in it. The result must have an intent(inout)
      !   to avoid erasing the allocatable array in the derived type.
      !
      !---------------------------------------------------------------------
      type(uvmap_par), intent(in) :: in
      type(uvmap_par), intent(inout) :: out
    end subroutine map_copy_par
  end interface
  !
  interface
    subroutine map_prepare(task,huv,map,error)
      use image_def
      use clean_def
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER      
      !             Prepares the imaging parameters from the UV data header
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task  ! Caller name
      type(gildas), intent(in) :: huv       ! UV data header
      type(uvmap_par), intent(inout) :: map ! Imaging parameters
      logical, intent(out) :: error         ! Error flag
    end subroutine map_prepare
  end interface
  !
  interface
    subroutine primary_comm(line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  Support for command
      !   PRIMARY [BeamSize] [/TRUNCATE Percent]
      !
      ! Compute primary beam and Use it as "flat field" correction
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line ! Command line
      logical, intent(out) :: error
    end subroutine primary_comm
  end interface
  !
  interface
    subroutine primary_single(line,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER  Support for command
      !   PRIMARY [BeamSize] 
      !
      ! Compute primary beam for a single field
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line ! Command line
      logical, intent(out) :: error
    end subroutine primary_single
  end interface
  !
  interface
    subroutine primary_radial(line,bsize,head,nr,profile,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER  
      !
      ! Compute primary beam radial profile
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line ! Command line
      real(4), intent(inout) :: bsize      ! Force Beam Size if non zero
      type(gildas), intent(in) :: head     ! Header of image data cube
      integer, intent(out) :: nr           ! Number of radial points
      real(8), allocatable, intent(out) :: profile(:,:) ! Radial profile
      logical, intent(out) :: error
    end subroutine primary_radial
  end interface
  !
  interface
    subroutine primary_mosaic(line,np,hprim,head,selected_fields, &
        & selected_fieldsize,doff,pos,bsize,error)
      use gkernel_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER  
      !
      ! Compute primary beam for a Mosaic
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line    ! Command line
      integer, intent(in) :: np                   ! Number of fields
      integer, intent(in) :: selected_fields(np)  ! Field numbers 
      integer, intent(in) :: selected_fieldsize   ! Number of selected fields
      type(gildas), intent(inout) :: hprim    ! Primary beam header
      type(gildas), intent(in)    :: head     ! Dirty image header
      real, intent(in) :: doff(:,:)           ! Offset of pointings
      real(8), intent(in) :: pos(2)           ! Center offset
      real, intent(inout) :: bsize            ! Size of primary beam
      logical, intent(out) :: error           ! Error flag
    end subroutine primary_mosaic
  end interface
  !
  interface
    subroutine primary_gauss(bsize,nr,rb)
      !-----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Compute the primary beam as a Gaussian of FWHM bsize
      !-----------------------------------------------------------------------
      real, intent(in) :: bsize
      integer, intent(in) :: nr
      real(8), intent(inout) :: rb(nr,2)
    end subroutine primary_gauss
  end interface
  !
  interface
    subroutine primary_alma(h,nr,rb)
      use gkernel_types
      !-----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER 
      !   Compute the primary beam of an ALMA 12-m antenna,
      !   as a Uniform Disk of 10.4m with 0.7 m blockage
      !-----------------------------------------------------------------------
      type(gildas), intent(in) :: h
      integer, intent(in) :: nr
      real(8), intent(inout) :: rb(nr,2)
    end subroutine primary_alma
  end interface
  !
  interface
    subroutine get_bsize(h,rname,line,bsize,error,otrunc,btrunc)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER     Support for command
      !   PRIMARY [BeamSize] [/TRUNCATE Percent]
      !   UV_MAP  [BeamSize] [/TRUNCATE Percent]
      !
      ! Return the primary beam size in radian
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: h          ! UV data header
      character(len=*), intent(in) :: rname  ! Caller name
      character(len=*), intent(in) :: line   ! Command line
      real(4), intent(inout) :: bsize        ! Beam size in radian
      logical, intent(out) :: error          ! Error flag
      integer, optional, intent(in) ::  otrunc   ! Truncation option number
      real(4), optional, intent(out) :: btrunc   ! Truncation level [0,1]
    end subroutine get_bsize
  end interface
  !
  interface
    subroutine dofft_test (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv   &
         &    ,ubias,vbias,ubuff,vbuff,ctype)
      !----------------------------------------------------------------------
      ! @  private
      !
      ! GILDAS  UV_MAP
      !   Compute FFT of image by gridding UV data
      !   Test version to compare speed of various methods
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
      integer, intent(in) :: ctype                ! type of gridding
    end subroutine dofft_test
  end interface
  !
  interface
    subroutine dofft_quick1 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   For any gridding support
      !   Taper before gridding
      !   Does not use symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick1
  end interface
  !
  interface
    subroutine dofft_fast1 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   Only for "visibility in cell"
      !   Taper before gridding
      !   Do not use symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
    end subroutine dofft_fast1
  end interface
  !
  interface
    subroutine greglib_pack_init(gpack_id,error)
      !use greg_dependencies_interfaces
      !use greg_interfaces, except_this=>greglib_pack_init
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Private routine to set the GREG environment.
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine greglib_pack_init
  end interface
  !
  interface
    subroutine uv_mosaic_comm(line,comm,error)
      use gildas_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Support for command 
      !     UV_MOSAIC Mosaic MERGE|SPLIT [Fields...]
      !
      ! Make a mosaic UV Tables from a set of single field UV tables, 
      ! or vice-versa
      !
      ! The original UV Tables are assumed to have the same number of channels and
      ! 1) either to have the same phase tracking center
      ! 2) or a phase tracking center corresponding to the pointing center
      !
      ! It is assumed that the names of the original field UV tables
      ! the simple sequence 'NAME'-'I'.uvt
      !----------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(inout) :: error
    end subroutine uv_mosaic_comm
  end interface
  !
  interface
    subroutine uv_mosaic_group(line,rname,error)
      use phys_const
      use image_def
      use gbl_message
      use gkernel_types
      use iso_c_binding
      !----------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      ! Support for command
      !     UV_MOSAIC Mosaic MERGE Fields [...]
      !
      ! Build a Mosaic from an ensemble of independent fields.
      ! Argument Fields can be a SIC variable handling all the fields filenames.
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: rname
      logical,          intent(inout) :: error
    end subroutine uv_mosaic_group
  end interface
  !
  interface
    subroutine uv_mosaic_split(line,rname,error)
      use phys_const
      use gkernel_types
      use image_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      !   IMAGER
      ! Support for command
      !     UV_MOSAIC Mosaic SPLIT [Fields]
      ! Explode a Mosaic to an ensemble of NF single fields
      ! with a name following the convention 'Fields'-'i'
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: rname  ! Command name
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Errorflag
    end subroutine uv_mosaic_split
  end interface
  !
  interface
    subroutine ompset_thread_nesting(rname, nplane, max_inner, omp_nested)
      !$ use omp_lib
      use omp_control
      use gbl_message
      ! @ private
      character(len=*), intent(in) :: rname
      integer, intent(in) :: nplane
      integer, intent(in) :: max_inner
      logical, intent(out) :: omp_nested
    end subroutine ompset_thread_nesting
  end interface
  !
  interface
    function ompget_outer_threads()
      use omp_control
      ! @ private
      integer :: ompget_outer_threads ! Intent(out)
    end function ompget_outer_threads
  end interface
  !
  interface
    function ompget_inner_threads()
      use omp_control
      ! @ private
      integer :: ompget_inner_threads ! Intent(out)
    end function ompget_inner_threads
  end interface
  !
  interface
    function ompget_grid_code()
      use omp_control
      ! @ private
      integer :: ompget_grid_code    ! Intent(out)
    end function ompget_grid_code
  end interface
  !
  interface
    function ompget_debug_code()
      use omp_control
      ! @ private
      logical :: ompget_debug_code    ! Intent(out)
    end function ompget_debug_code
  end interface
  !
  interface
    function ompget_outer_goal()
      use omp_control
      ! @ private
      integer :: ompget_outer_goal    ! Intent(out)
    end function ompget_outer_goal
  end interface
  !
  interface
    subroutine cct_prepare(line,nsizes,a_method,task,error)
      use clean_def
      use clean_arrays
      use clean_types
      use clean_default
      use gbl_message
      !---------------------------------------------------
      ! @ private
      !
      ! IMAGER
      !   Prepare the Clean Component Table for further use
      !---------------------------------------------------
      character(len=*), intent(in) :: line      ! Command line
      integer, intent(in) :: nsizes(3)          ! Cube Size
      type(clean_par), intent(in) :: a_method   ! Input method
      character(len=*), intent(in) :: task      ! Caller name
      logical, intent(out) :: error  !
    end subroutine cct_prepare
  end interface
  !
  interface
    subroutine cct_remove_start(head,iplane,resid,tfbeam,dcct,tcc, &
      & nfields,primary,weight,wtrun,next_iter,start_flux)
      use image_def
      use clean_def
      !----------------------------------------------------------
      ! @ private-mandatory
      !
      ! IMAGER
      !   Subtract a Starting List of Clean Components
      !----------------------------------------------------------
      type(gildas), intent(in) :: head        ! Imager Header
      integer, intent(in) :: iplane           ! Current plane number
      real, intent(inout) :: resid(:,:)       !
      real, intent(in) :: dcct(:,:,:)         ! Clean Component List (from READ CCT)
      real, intent(in) :: tfbeam(:,:,:)       ! Dirty Beam FT (Gridded UV coverage)
      type(cct_par), intent(inout) :: tcc(:)  ! Clean Component Table
      integer, intent(in) :: nfields          ! Number of fields
      real, intent(in) :: primary(:,:,:)      ! Primary beams
      real, intent(in) :: weight(:,:)         ! Mosaic weights
      real, intent(in) :: wtrun               ! Mosaic primary beam truncation
      integer, intent(out) :: next_iter       ! Next available CC
      real, intent(out) :: start_flux
    end subroutine cct_remove_start
  end interface
  !
  interface
    function sun_distance (x_2)
      use ast_constant
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !	Returns angular distance from SUn
      !	X_2: apparent horizontal coordinates of the source
      !	Xsun_2: apparent horizontal coordinates of the sun
      ! The angle between the 2 directions is derived from the scalar product 
      ! of the vectors in the horizontal coordinate frame:
      ! Vobj.Vsun=(XOXSun+YOYSun+ZOZSun)=||Vobj||*||Vsun||*cos(SunObserverTarget)
      !---------------------------------------------------------------------
      real(kind=8) :: sun_distance      !
      real(kind=8) :: x_2(3)            !
    end function sun_distance
  end interface
  !
  interface
    function tsmg (tjj)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul du temps sideral moyen de Greenwich (radian)
      !  from EPHAUT / BDL / A002 / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  reference Aoki et al. (1982, Astron. Astrophys. 105, 359)
      !  en entree : TJJ date (jours juliens)
      !  remarques : on utilise le formulaire UAI84
      !              l'epoque de reference est J2000.0 (JJ2451545.0)
      !              le temps est compte en siecle julien
      !---------------------------------------------------------------------
      real(kind=8) :: tsmg              !
      real(kind=8) :: tjj               !
    end function tsmg
  end interface
  !
  interface
    function oblimo (tjj)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul de l'obliquite moyenne (radian), precession selon Lieske et al. (1977)
      !  from EPHAUT / BDL / C004 / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  en entree : TJJ date (jours juliens)
      !---------------------------------------------------------------------
      real(kind=8) :: oblimo            !
      real(kind=8) :: tjj               !
    end function oblimo
  end interface
  !
  interface
    subroutine nuta (tjj,dpsi,deps)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul de la nutation en longitude et en obliquite a la date TJJ
      !  d'apres les formules de WAHR (1981)
      !  from EPHAUT / BDL / D003 / 81-1
      !  modified 29 november 1984 by Michel Perault
      !  only the terms with amplitude larger than 0.004 arcsec are kept
      !  en entree :  TJJ date (jours juliens)
      !  en sortie :  DPSI nutation en longitude (radian)
      !		DEPS nutation en obliquite (radian)
      !  remarques :  l'epoque de reference pour le calcul de la nutation
      !		est TF1 : 1 janvier 2000 12h (J2000.0)
      !		l'epoque de reference pour le calcul des arguments
      !		fondamentaux est TF2 : 0 janvier 1900 12h
      !---------------------------------------------------------------------
      real(kind=8) :: tjj               !
      real(kind=8) :: dpsi              !
      real(kind=8) :: deps              !
    end subroutine nuta
  end interface
  !
  interface
    subroutine ctcheb (values,degree,coeff,error)
      !---------------------------------------------------------------------
      ! @ private
      !     Approximation d'une fonction par un developpement en polynomes de
      !     Tchebychev sur un intervalle de variation donne
      !  Loosely inspired from EPHAUT / BDL / J002 / 81-1
      !  Modified 5 October 1985 - Michel Perault - POM Version 2.0
      !  Input :
      !	VALUES(0:DEGREE) function at Tchebychev abscissa
      !	DEGREE	degree of representation
      !  Output :
      !	COEFF(0:DEGREE)	coefficients of Tchebychev polynomes
      !	ERROR	logical, if improper value of DEGREE
      !  Notes :
      !	VALUES et COEFF sont exprimes dans la meme unite.
      !	Les abscisses de tchebychev sur l'intervalle (-1,+1) sont donnees
      !	par la relation suivante :
      !                 Xk = cos((2*k+1)*pi/(2*(DEGREE+1))  pour k = 0,DEGREE+1
      !	Les valeurs de la fonction aux abscisses de Tchebychev ne doivent
      !	pas presenter de discontinuite.
      !	Il est conseille de representer la fonction sur un intervalle ne
      !	contenant pas plus d'un extremum.
      !---------------------------------------------------------------------
      integer(kind=4) :: degree         ! degree of representation
      real(kind=8) :: values(0:degree)  ! function at Tchebychev abscissa
      real(kind=8) :: coeff(0:degree)   ! coeffiecients of Tchebychev polynomes
      logical :: error                  ! set if error
    end subroutine ctcheb
  end interface
  !
  interface
    subroutine ftcheb (degree,coeff,x,deriv,values,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Calcul d'une fonction et de ses derivees a partir de sa representation
      !  en developpement de polynomes de Tchebychev
      !  Loosely inspired from EPHAUT / BDL / J003 / 81-1
      !  Modified 5 October 1985 - Michel Perault - POM Version 2.0
      !  Input :
      !	DEGREE	degree of representation
      !	COEFF(0:DEGREE)	coeffiecients of Tchebychev polynomes
      !	X	abscissa (between -1.d0 and +1.d0)
      !	DERIV	number of derivatives to compute
      !  Output :
      !	VALUES(0:DERIV)	function and derivatives
      !	ERROR	logical, if improper value of DEGREE, X or DERIV
      !  Notes :
      !	FTCHEB est exprimee dans la meme unite que les coefficients
      !  	Si dt est l'intervalle de representation exprime dans une unite donnee,
      !  	les derivees doivent etre multipliees par (2/dt)**DERIV
      !---------------------------------------------------------------------
      integer(kind=4) :: degree         ! degree of representation
      real(kind=8) :: coeff(0:degree)   ! coeffiecients of Tchebychev polynomes
      real(kind=8) :: x                 ! abscissa (between -1.d0 and +1.d0)
      integer(kind=4) :: deriv          ! number of derivatives to compute
      real(kind=8) :: values(0:deriv)   ! function and derivatives
      logical :: error                  ! set if error
    end subroutine ftcheb
  end interface
  !
  interface
    subroutine eq_planet(i_planet, s_3, error)
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      !  @ private
      !  Get Apparent RA and DEC of planet I_PLANET
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: i_planet  !
      real(kind=8),    intent(out) :: s_3(3)    !
      logical,         intent(out) :: error     !
    end subroutine eq_planet
  end interface
  !
  interface
    subroutine vsop87(tjj,ico,ider,prec,r,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Compute object position and velocities from VSOP87 ephemeris.
      ! Input:
      ! TJJ (real*8) is the julian date
      ! ICO (integer) 1 - 8 = Mercury to Neptune, relative to Sun
      !                   9 = Earth/Moon barycenter, relative to Sun
      !                  10 = Sun, relative to Solar system barycenter
      ! IDER (integer) number of derivatives to compute (0 to 2)
      ! PREC (real*8) precision 0 to 0.01; 0 is maximal precision
      ! Output:
      ! R(3,0:IDER) (real*8) position (1-3) vector and derivatives
      !                      (in au, au/day, au/day**2)
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: tjj     ! Julian date
      integer(kind=4), intent(in) :: ico  ! 1 - 8 = Mercury to Neptune, relative to Sun
      !                   9 = Earth/Moon barycenter, relative to Sun
      !                  10 = Sun, relative to Solar system barycenter
      integer, intent(in) :: ider     ! number of derivatives to compute (0 to 2)
      real(8), intent(in) :: prec     ! precision 0 to 0.01; 0 is maximal precision
      real(8), intent(out) :: r(3,0:ider) !  position (1-3) vector and derivatives
      !                      (in au, au/day, au/day**2)
      logical, intent(out) :: error   !
    end subroutine vsop87
  end interface
  !
  interface
    subroutine readr8(lun,irecord,nw,w,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Read NW words starting at direct access record IRECORD on file LUN
      !---------------------------------------------------------------------
      integer(kind=4) :: lun            !
      integer(kind=4) :: irecord        !
      integer(kind=4) :: nw             !
      real(kind=8) :: w(nw)             !
      logical :: error                  !
    end subroutine readr8
  end interface
  !
  interface
    subroutine readi4(lun,irecord,nw,w,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Read NW words starting at direct access record IRECORD on file LUN
      !---------------------------------------------------------------------
      integer(kind=4) :: lun            !
      integer(kind=4) :: irecord        !
      integer(kind=4) :: nw             !
      integer(kind=4) :: w(nw)          !
      logical :: error                  !
    end subroutine readi4
  end interface
  !
  interface
    subroutine eph_convert(cfile)
      use gbl_convert
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: cfile          !
    end subroutine eph_convert
  end interface
  !
  interface
    subroutine ephsta (vector,planet,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Define vector to compute
      ! two separate calls EPHSTA and EPHVEC are kept for compatibility.
      !---------------------------------------------------------------------
      integer(kind=4) :: vector         ! vector description code
      integer(kind=4) :: planet         ! object identification
      logical :: error                  !
    end subroutine ephsta
  end interface
  !
  interface
    subroutine ephvec (jdate,deriv,rvec,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Compute position vector and derivatives at given date
      !---------------------------------------------------------------------
      real(kind=8) :: jdate             ! julian date of computation
      integer(kind=4) :: deriv          ! number of derivatives needed (if position only, DERIV = 0)
      real(kind=8) :: rvec(3,0:deriv)   ! resulting vectors
      logical :: error                  ! .true. in case of error
    end subroutine ephvec
  end interface
  !
  interface
    subroutine astro_observatory_byname(arg,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !  Set the Astro observatory given its name
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: arg    ! Telescope name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_observatory_byname
  end interface
  !
end module imager_interfaces_private
