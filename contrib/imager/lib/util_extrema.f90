!
subroutine t_setextrema(h,rmi,jmi,rma,jma)
  use gkernel_interfaces
  use gildas_def
  use image_def
  !------------------------------------------------
  ! @ public
  !
  ! MAPPING
  !   Put extrema into the specified header
  !------------------------------------------------
  type (gildas), intent(inout), target :: h
  real(kind=4), intent(in) :: rmi     ! Value of minimum
  integer(kind=size_length), intent(in) :: jmi  ! Position of minimum
  real(kind=4), intent(in) :: rma     ! Value of maximum
  integer(kind=size_length), intent(in) :: jma  ! Position of maximum
  !
  integer(kind=8) :: ilong 
  !
  h%gil%rmax = rma
  h%gil%rmin = rmi
  ilong = jmi
  call gdf_index_to_where (ilong,h%gil%ndim,h%gil%dim,h%gil%minloc)  
  ilong = jma
  call gdf_index_to_where (ilong,h%gil%ndim,h%gil%dim,h%gil%maxloc)  
  h%gil%extr_words = def_extr_words                ! extrema computed
end subroutine t_setextrema
!
subroutine cube_minmax(comm,hmap,error)
  use image_def
  use gkernel_interfaces
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! @ private
  !   IMAGER
  ! Compute the extrema of a data Cube
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: comm
  type(gildas), intent(inout) :: hmap
  logical, intent(inout) :: error
  !
  integer(kind=size_length) :: good_size
  character(len=128) :: mess
  real :: rmi,rma
  !
  ! Check properly the sizes and allocation status before
  if (.not.associated(hmap%r3d)) then
    call map_message(seve%e,comm,'3D pointer not associated')
    error = .true.
    return
  endif
  error = .false.
  good_size = hmap%gil%dim(1)*hmap%gil%dim(2)*hmap%gil%dim(3)
  if (hmap%loca%size.ne.good_size) then
    write(mess,'(A,I0,A,4(1X,I0))')  '3D size was incorrectly set', &
      & hmap%loca%size,', expected ',good_size, hmap%gil%dim(1:3)
    call map_message(seve%w,comm,mess)
    hmap%loca%size = good_size
  endif
  !
  if (hmap%gil%eval.lt.0) then
    hmap%gil%minloc = 1
    hmap%gil%maxloc = 1
    hmap%gil%minloc(1:3) = minloc(hmap%r3d)
    hmap%gil%maxloc(1:3) = maxloc(hmap%r3d)
    rma = hmap%r3d(hmap%gil%maxloc(1),hmap%gil%maxloc(2),hmap%gil%maxloc(3))
    rmi = hmap%r3d(hmap%gil%minloc(1),hmap%gil%minloc(2),hmap%gil%minloc(3))
    hmap%gil%rmax = rma
    hmap%gil%rmin = rmi
  else
    hmap%loca%addr = locwrd(hmap%r3d)
    call gdf_get_extrema (hmap,error)
  endif
  hmap%gil%extr_words = def_extr_words  ! extrema computed
end subroutine cube_minmax
