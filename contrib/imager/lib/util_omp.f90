subroutine omp_setup
  use omp_control
  !$ use omp_lib
  use gkernel_interfaces
  !
  logical :: error
  real :: rthread
  !
  call sic_defstructure('OMP_MAP',.true.,error)
  omp_compiled = .false.
  !$ omp_compiled = .true.
  call sic_def_logi('OMP_MAP%COMPILED',omp_compiled,.false.,error)
  omp_debug = .false.
  call sic_def_logi('OMP_MAP%DEBUG',omp_debug,.false.,error)
  call sic_def_logi('OMP_MAP%UV_MAP',omp_uvmap,.false.,error)
  call sic_def_inte('OMP_MAP%GRID',omp_grid,0,0,.false.,error)
  call sic_def_logi('OMP_MAP%RESTORE_FAST',omp_restore_fast,.false.,error)
  omp_uvmap_lock = .true.
  call sic_def_logi('OMP_MAP%UV_MAP_LOCK',omp_uvmap_lock,.false.,error)
  call sic_def_inte('OMP_MAP%SINGLE_BEAM_CODE',omp_single_beam,0,0,.false.,error)
  omp_outer_threadgoal = 1
  !$ rthread = omp_get_max_threads()
  !$ omp_outer_threadgoal = nint(sqrt(rthread))
  !$ omp_outer_threadgoal = max(1,omp_outer_threadgoal)
  call sic_def_inte('OMP_MAP%OUTER_GOAL',omp_outer_threadgoal,0,0,.false.,error)
end subroutine omp_setup
!
subroutine ompset_thread_nesting(rname, nplane, max_inner, omp_nested)
  !$ use omp_lib
  use omp_control
  use gbl_message
  use imager_interfaces, only : map_message
  ! @ private
  character(len=*), intent(in) :: rname
  integer, intent(in) :: nplane
  integer, intent(in) :: max_inner
  logical, intent(out) :: omp_nested
  !
  character(len=80) :: chain
  integer :: mthread
  !
  mthread = 1
  !$  mthread = omp_get_max_threads()
  !$  omp_nested = omp_get_nested()
  if (nplane.le.mthread/2) then
    ! There are more available Threads than planes - One can use nested
    ! parallelism with Outer and Inner threads
    omp_outer_thread = nplane 
    omp_inner_thread = mthread/nplane
    !$  call omp_set_nested(.true.)
  else
    omp_outer_thread = mthread
    omp_inner_thread = 1 
    !$  call omp_set_nested(.false.)
  endif
  !
  if (max_inner.ne.0) omp_inner_thread = min(max_inner,omp_inner_thread)
  write(chain,'(A,I0,A,I0)') 'Nested threads Outer ',omp_outer_thread,' Inner ',omp_inner_thread
  call map_message(seve%i,rname,chain)
end subroutine ompset_thread_nesting
!
function ompget_outer_threads()
  use omp_control
  ! @ private
  integer :: ompget_outer_threads ! Intent(out)
  !
  ompget_outer_threads = omp_outer_thread
end function ompget_outer_threads
!
function ompget_inner_threads()
  use omp_control
  ! @ private
  integer :: ompget_inner_threads ! Intent(out)
  !
  ompget_inner_threads = omp_inner_thread
end function ompget_inner_threads
!
function ompget_grid_code()
  use omp_control
  ! @ private
  integer :: ompget_grid_code    ! Intent(out)
  !
  ompget_grid_code = omp_grid    ! From OMP_MAP%GRID variable
end function ompget_grid_code
!
function ompget_debug_code()
  use omp_control
  ! @ private
  logical :: ompget_debug_code    ! Intent(out)
  !
  ompget_debug_code = omp_debug    ! From OMP_MAP%GRID variable
end function ompget_debug_code
!
function ompget_outer_goal()
  use omp_control
  ! @ private
  integer :: ompget_outer_goal    ! Intent(out)
  !
  ompget_outer_goal = omp_outer_threadgoal   ! From OMP_MAP%OUTER_GOAL variable
end function ompget_outer_goal
