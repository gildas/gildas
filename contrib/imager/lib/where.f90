!
subroutine beam_unit_conversion(amethod)
  use clean_def
  use clean_default
  !----------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Major and Minor axes of beam are in seconds but the method structure
  !   assumes that its major and minor fields are in radian. So we need
  !   a conversion...
  !----------------------------------------------------------------------
  type (clean_par) :: amethod
  !
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  real(kind=4), parameter :: sec_to_rad=pi/3600.0/180.0
  !
  amethod%major = beam_size(1)*sec_to_rad
  amethod%minor = beam_size(2)*sec_to_rad
  amethod%angle = beam_size(3)
  !
end subroutine beam_unit_conversion
!
subroutine copy_method(in,out)
  use clean_def
  !----------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Copy method
  !----------------------------------------------------------------------
  type (clean_par), intent(in) ::  in
  type (clean_par), intent(inout) ::  out
  !
  out%method = in%method
  out%verbose = in%verbose
  !
  out%gain = in%gain
  out%fres = in%fres
  out%ares = in%ares
  out%major = in%major
  out%minor = in%minor
  out%angle = in%angle
  out%ratio = in%ratio         ! MRC ratio
  out%spexp = in%spexp         ! Clark speed
  out%phat = in%phat           ! Prussian Hat
  out%smooth = in%smooth       ! Multi smooth
  out%converge = in%converge   ! Convergence
  out%mosaic = in%mosaic       ! Mosaic mode
  out%search = in%search       ! Mosaic search
  out%restor = in%restor       ! Mosaic restore
  out%trunca = in%trunca       ! Mosaic beam truncation
  out%m_iter = in%m_iter
  out%p_iter = in%p_iter
  out%ngoal = in%ngoal
  out%n_major = in%n_major
  out%nker = in%nker           ! Multiscale Kernel
  out%ninflate = in%ninflate   ! and inflation factor
  out%blc = in%blc
  out%trc = in%trc
  out%patch = in%patch
  out%do_mask = in%do_mask
  out%residual = in%residual   ! Add residual or not
  out%worry = in%worry         ! Worry convergence factor
  out%thresh = in%thresh       ! Sidelobe level for FIT
  out%gains = in%gains
  ! 
  ! Pointer issue
  out%atten => in%atten
  ! Consequences
  !     box, beam0, bzone, n_iter, bshift, beam_min, beam_max, bgain, flux
  ! Variables
  !     Major, Minor, Angle
  ! Temporary
  !     Pflux, Pcycle, Qcycle, Pmrc, Pclean,
  ! Return
  !     Do_beam, Do_mask
end subroutine copy_method
!
subroutine copy_param(in,out)
  !--------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Copy only the parameters of the method, not the pointers to the
  !   work arrays
  !   Do not copy the Loop Gain, which is Method specific
  !--------------------------------------------------------------------
  use clean_def
  type (clean_par), intent(in) ::  in
  type (clean_par), intent(inout) ::  out
  !
  if (in%method.ne.out%method) then
     out%method = in%method
  endif
  out%verbose = in%verbose
  !
  ! Not this one...      OUT%GAIN = IN%GAIN
  out%fres = in%fres
  out%ares = in%ares
  out%major = in%major
  out%minor = in%minor
  out%angle = in%angle
  out%ratio = in%ratio         ! MRC ratio
  out%spexp = in%spexp         ! Clark speed
  if (in%phat.ne.out%phat) then
     out%phat = in%phat         ! Prussian Hat
  endif
  out%smooth = in%smooth       ! Multi smooth
  out%converge = in%converge   ! Convergence
  out%mosaic = in%mosaic       ! Mosaic mode
  out%search = in%search       ! Mosaic search
  out%restor = in%restor       ! Mosaic restore
  out%trunca = in%trunca       ! Mosaic beam truncation
  out%m_iter = in%m_iter
  out%p_iter = in%p_iter
  out%ngoal = in%ngoal
  out%n_major = in%n_major
  out%nker = in%nker           ! Multiscale Kernel
  out%blc = in%blc
  out%trc = in%trc
  out%patch = in%patch
  out%do_mask = in%do_mask
  out%residual = in%residual   ! Add residual or not
  out%worry = in%worry         ! Worry convergence factor
  ! Consequences
  !     box, beam0, bzone, n_iter, bshift, beam_min, beam_max, bgain, flux
  ! Variables
  !     Major, Minor, Angle
  ! Temporary
  !     Pflux, Pcycle, Qcycle, Pmrc, Pclean,
  ! Return
  !     Do_beam, Do_mask
end subroutine copy_param
!
subroutine mapping_print_debug(m)
  ! @ private
  use clean_def
  type (clean_par) :: m
  !
  write(6,*) m%gain, m%fres, m%ares,' Loop gain, Fres, Ares'
  write(6,*) m%major, m%minor, m%angle,' Major/Minor/Angle axis'
  write(6,*) m%beam_min, m%beam_max, m%bgain,   &
       &    ' Beam Min/Max Sidelobe'
  write(6,*) m%ratio, m%spexp, m%phat, m%smooth,   &
       &    ' MRC ratio, Speed, Phat, Multi ratio'
  write(6,*) m%flux, m%converge,' Cleaned Flux, Convergence number'
  write(6,*) m%search, m%restor, m%trunca,   &
       &    ' Threshold Search/Restore/Truncate'
  write(6,*) m%blc, m%trc,' BLC, TRC '
  write(6,*) m%box    ,' Cleaning Box'
  write(6,*) m%beam0, m%patch,' Beam center X,Y, Patch size'
  write(6,*) m%bzone  ,' Beam patch Zone'
  write(6,*) m%m_iter, m%p_iter, m%n_iter,   &
       &    ' Iterations Max/Pos/Actual'
  write(6,*) m%ngoal, m%n_major,' Max select, Max cycle'
  write(6,*) m%bshift ,' Beam shift'
  write(6,*) m%nker   ,' Kernel for Multi-Scale clean'
  write(6,*) 'Mos. Flux, Cycle, Query, Mrc, Clean, Beam/Mask'
  write(6,*) m%mosaic, m%pflux,m%pcycle ,m%qcycle,m%pmrc,m%pclean,   &
       &    m%do_mask
  write(6,*) m%first,m%last,m%iplane,m%ibeam,m%nlist,   &
       &    ' First/Last/Current  Beam List'
  write(6,*) m%method    ,' METHOD'
  write(6,*) m%worry     , 'WORRY'
end subroutine mapping_print_debug
