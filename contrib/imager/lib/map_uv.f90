!
! Obsolescent or even already Obsolete routines
!
subroutine one_beam (rname,map,huv,hbeam,hdirty,   &
     &    nx,ny,nu,nv,uvdata,   &
     &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
     &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
  use gkernel_interfaces
  use imager_interfaces, except_this=>one_beam
  use clean_def
  use image_def
  use gbl_message
  use clean_default
  use omp_control
  !------------------------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Compute a map from a CLIC UV Sorted Table
  !   by Gridding and Fast Fourier Transform, with
  !   one single beam for all channels.
  !
  ! Input :
  ! a precessed UV table, sorted in V, ordered in
  ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
  ! Output :
  ! a beam image
  ! a VLM cube
  ! Work space :
  ! a  VLM complex Fourier cube (first V value is for beam)
  !------------------------------------------------------------------------
  character(len=*), intent(in) :: rname   ! Calling Task name
  type (uvmap_par), intent(inout) :: map  ! Mapping parameters
  type (gildas), intent(inout) :: huv     ! UV data set
  type (gildas), intent(inout) :: hbeam   ! Dirty beam data set
  type (gildas), intent(inout) :: hdirty  ! Dirty image data set
  integer, intent(in) :: nx   ! X size
  integer, intent(in) :: ny   ! Y size
  integer, intent(in) :: nu   ! Size of a visibilities
  integer, intent(in) :: nv   ! Number of visibilities
  real, intent(inout) :: uvdata(nu,nv)
  real, intent(inout) :: w_mapu(nx)      ! U grid coordinates
  real, intent(inout) :: w_mapv(ny)      ! V grid coordinates
  real, intent(inout) :: w_grid(nx,ny)   ! Gridding space
  real, intent(inout) :: w_weight(nv)    ! Weight of visibilities
  real, intent(inout) :: w_v(nv)         ! V values
  logical, intent(inout) :: do_weig
  !
  real, intent(inout) :: wfft(*)     ! Work space
  real, intent(inout) :: cpu0        ! CPU
  real, intent(inout) :: uvmax       ! Maximum baseline
  integer, intent(inout) :: sblock   ! Blocking factor
  integer, intent(inout) :: wcol     ! Weight channel
  integer, intent(inout) :: mcol(2)  ! First and last channel
  logical, intent(inout) :: error
  !
  if (omp_single_beam.eq.0) then
    Print *,'Calling SERIAL one_beam'
    call one_beam_serial (rname,map,huv,hbeam,hdirty,   &
     &    nx,ny,nu,nv,uvdata,   &
     &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
     &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
  else
    Print *,'Calling PARALLEL one_beam'
    call one_beam_para (rname,map,huv,hbeam,hdirty,   &
     &    nx,ny,nu,nv,uvdata,   &
     &    w_mapu, w_mapv, w_grid, w_weight, w_v, do_weig,    &
     &    wcol,mcol,wfft,sblock,cpu0,error,uvmax)
  endif
end subroutine one_beam
!
subroutine check_order(visi,np,nv,sorted)
  !----------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Check if visibilites are sorted.
  !   Chksuv does a similar job, but using V values and an index.
  !----------------------------------------------------------
  integer, intent(in) :: np       ! Size of a visibility
  integer, intent(in) :: nv       ! Number of visibilities
  real, intent(in) :: visi(np,nv) ! Visibilities
  logical, intent(out) :: sorted
  !
  real vmax
  integer iv
  !
  vmax = visi(2,1)
  do iv=1,nv
    if (visi(2,iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = visi(2,iv)
  enddo
  sorted = .true.
end subroutine check_order
!
