subroutine howto_comm(line,error)
  use gkernel_interfaces
  use gbl_message
  use imager_interfaces, only : map_message
  !---------------------------------------------------------------------
  ! IMAGER
  !   Support for command HOW_TO
  !   HOW_TO Keywords...
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='HOW_TO'
  integer, parameter :: mkeys=10
  integer :: nkeys, lkeys(mkeys), ikey
  character(len=20) :: ckeys(mkeys),string
  character(len=4) :: ch
  character(len=120) :: ligne
  character(len=256) :: file
  integer, save :: luntem=0
  integer :: ier, i, j, ifound, lt, nline, nfound, fline, lline
  logical :: found, used, lfound
  !
  integer, parameter :: nuse=13
  character(len=8) :: useless(nuse)
  data useless /'?','a','all','and','at','do','i','it','my','some','the','to','we'/
  !
  error = .true.
  nkeys = sic_narg(0)
  if (nkeys.le.0) then
    call map_message(seve%e,rname,'Missing keywords')
    return
  endif
  !
  if (nkeys.gt.mkeys) then
    call map_message(seve%e,rname,'Too many keywords')
    return
  endif
  !
  ikey = 0
  do i=1,nkeys
    call sic_ch(line,0,i,string,lt,.true.,error)
    if (error) return
    if (nkeys.eq.1 .and. string.eq.'?') then
      ikey = 1
      exit
    endif
    !
    call sic_lower(string)
    used = .true.
    do j=1,nuse
      if (string.eq.useless(j)) then
        used = .false.
        exit
      endif
    enddo
    if (used) then
      ikey = ikey+1
      ckeys(ikey) = string
      if (string(lt:lt).eq.'*') then
        lkeys(ikey) = lt-1
        ckeys(ikey)(lt:) = ' '
      else
        lkeys(ikey) = lt+1
      endif
    endif
  enddo
  nkeys = ikey
  !
  ! Open file (in production, will use a rewind(luntem) statement)
  if (luntem.eq.0) then
    ier = sic_getlun(luntem)
    if (ier.ne.1) return
    call sic_parse_file('imager-howto','gag_pro:','.hlp',file)    
    ier = sic_open(luntem,file,'old',.true.)
    if (ier.ne.0) then
      call map_message(seve%e,'HOW_TO','No gag_pro:imager-howto.hlp file')
      error = .true.
      call sic_frelun(luntem)
      luntem = 0
    endif
  else    
    rewind(unit=luntem)
  endif
  !
  ! Special case HOWTO ?
  if (nkeys.eq.1 .and. string.eq.'?') then
    do 
      read(luntem,'(A)',iostat=ier) ligne
      if (ier.ne.0)  exit
      !
      if (ligne(1:1).eq.'1') then
        read(luntem,'(A)',iostat=ier) ligne
        if (ier.eq.0) write(*,'(A)') trim(ligne)
      endif
    enddo
    error = .false.
    return
  endif
  !
  ! Scan for Keywords
  found = .false.
  ifound = 0
  !
  ! First pass, look for ambiguities
  nline = 0
  nfound = 0
  fline = 0
  lline = 0
  do
    lfound = .false.
    ifound = 0
    do 
      read(luntem,'(A)',iostat=ier) ligne
      if (ier.ne.0)  then
        if (ifound.eq.nkeys) nfound = nfound+1
        exit
      endif
      nline = nline+1
      !
      if (ligne(1:1).eq.'1') then
        if (ifound.eq.nkeys) then
          lline = nline
          nfound = nfound+1
          !!Print *,'STARTing ',ifound, nkeys,nline,trim(ligne) 
        endif
        ! We check 1 more character in ligne, which should be blank
        ! for correct match
        ifound = 0  ! Nothing found so far
        lfound = .false.
        do i=1,nkeys
          lt = lkeys(i)
          if (index(ligne,ckeys(i)(1:lt)).ne.0) then
            error = .false.
            ifound = ifound+1
          endif
        enddo
        lfound = ifound.eq.nkeys
        if (lfound) then
          found = .true.
          fline = nline
          read(luntem,'(A)',iostat=ier) ligne
          if (ier.ne.0)  exit
          write(*,'(A)') trim(ligne)
        endif
        !!Print *,ifound,nkeys,nline,trim(ligne)
      endif
    enddo
    !!Print *,'NLINE ',nline
    if (ier.ne.0) exit
  enddo
  if (lline.eq.0) lline = nline
  !
  if (found) then
    !!Print *,'NFOUND ',nfound,' FLINE ',fline,' LLINE ',lline
    !
    ! Second pass to write if no ambiguity
    if (nfound.gt.1) then
      write(*,'(A,I0,A)') 'Sorry, found ',nfound, &
        & ' different results for "how '//trim(line(17:))//'"'
      write(*,'(A)') 'Please, specify additional keywords ' 
    else 
      rewind(luntem)
      do i=1,fline+1
        read(luntem,'(A)',iostat=ier) ch
      enddo
      do i=fline+2,lline
        read(luntem,'(A)',iostat=ier) ligne
        write(*,'(A)') trim(ligne)
      enddo    
    endif
    return
  endif
  !
  ! Third chance: list relevant HOWTOs
  write(*,'(A)') 'Sorry, no result for '//trim(line)
  ifound = 0
  rewind(luntem)
  do 
    read(luntem,'(A)',iostat=ier) ligne
    if (ier.ne.0)  exit
    !
    if (ligne(1:1).eq.'1') then
      if (ifound.eq.nkeys) return ! Every keyword has been found
      ! We check 1 more character in ligne, which should be blank
      ! for correct match
      found = .false.
      do i=1,nkeys
        lt = lkeys(i)
        if (index(ligne,ckeys(i)(1:lt)).ne.0) then
          if (ifound.eq.0) write(*,'(A)') 'Relevant information '
          write(*,'(A)') ligne(3:len_trim(ligne))
          error = .false.
          ifound = ifound+1
        endif
      enddo
    endif
  enddo
end subroutine howto_comm

