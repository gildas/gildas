subroutine uvstat_map(task,freq,map,uvmax,uvmin,print)
  use gkernel_interfaces
  use imager_interfaces, only : map_message
  use clean_def
  use clean_arrays
  use clean_types
  use clean_default
  use gbl_message
  !----------------------------------------------------------------------
  ! @ private
  !
  ! Estimate (and Display) the MAP parameters which will be used for
  ! UV_MAP, MX or UV_RESTORE
  !-----------------------------------------------------------------------
  character(len=*), intent(in)    :: task ! Input task name (UV_MAP or MX)
  type (uvmap_par), intent(inout) :: map  ! Map parameters
  real(8), intent(inout) :: freq          ! Observing frequency
  real(4), intent(in) :: uvmax, uvmin     ! Min & Max UV in m
  logical, optional :: print              ! Printout results
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
  real, parameter :: pixel_per_beam=5.0
  character(len=80) :: chain
  real :: hppb, diam, uvmi, uvma
  logical do_print
  !
  do_print = present(print)
  !
  freq = gdf_uv_frequency(huv,huv%gil%ref(1))
  if (do_print) then
    write(chain,'(A,F12.6,A)') "Observed rest frequency       ",freq*1d-3," GHz"
    call map_message(seve%i,task,chain)
  endif
  !
  ! Primary beam
  if (huv%gil%majo.ne.0) then
    hppb = huv%gil%majo
  else
    diam = 0.
    if (huv%gil%nteles.ne.0) then
      diam = huv%gil%teles(1)%diam
    endif
    if (diam.eq.0) diam = 15.0
    hppb = 63.0*pi/180.0/3600.0*(12.0/diam)*(100.e3/freq) ! [rad] Primary beam size
  endif
  hppb = hppb*180*3600/pi
  if (do_print) then
    write(chain,'(A,F12.1,A)') "Half power primary beam       ",hppb," arcsec"
    call map_message(seve%i,task,chain)
  endif
  !
  ! say "   Phase center RA and Dec       "'&1%int%cent%sex[1]'" "'&1%int%cent%sex[2]'
  !
  uvma = uvmax/(freq*f_to_k)
  uvmi = uvmin/(freq*f_to_k)
  if (do_print) then
    write(chain,100) huv%gil%dim(2),huv%gil%nchan
    call map_message(seve%i,task,chain)
    write(chain,101) uvmi,uvma,' meters'
    call map_message(seve%i,task,chain)
    write(chain,101) uvmin*1e-3/2.0/pi,uvmax*1e-3/2.0/pi,' kiloWavelength'
    call map_message(seve%i,task,chain)
  endif
  !
  ! Now get MAP_CELL and MAP_SIZE
  map%xycell(1) = 0.01*nint(360.0*3600.0*100.0/uvmax/pixel_per_beam)
  if (map%xycell(1).le.0.02) map%xycell(1) =   &
         &        0.002*nint(360.0*3600.0*500.0/uvmax/pixel_per_beam)
  map%xycell(2) = map%xycell(1)  ! In ", rounded to 0.02 or 0.002"
  !
  ! Get a wide enough field of view...
  !
  map%size = 0
  if (map%field(1).ne.0) then
    if (map%field(2).eq.0) map%field(2) = map%field(1)
  else
    ! The factor 2 on the primary beam size is to go down to 6.25 % level by default
    map%field(1) = 2*hppb
    ! The factor 2 on uvmax/uvmin is to allow all scales down to uvmin
    map%field(2) = 2*pixel_per_beam*uvmax/uvmin * map%xycell(1)
    ! The Min is to avoid going to very large images when uvmin is close to 0
    map%field = min(map%field(1),map%field(2))
  endif
  !
  map%size = map%field/map%xycell !
  !
  ! Round to power of two : perhaps not the best option here.
  ! Could become too small or uselessly too large, and be compensated by a different
  ! cell size choice.
  !
  map%size =   2**nint(log(real(map%size))/log(2.0))
  !
  if (do_print) then
    write(chain,'(A,F12.1,a,F12.1,A)') 'Field of view / Largest Scale ',map%field(1),' x ',map%field(2),' "'
    call map_message(seve%i,task,chain)
    write(chain,'(A,F11.1,A,F11.1,A)') 'Image size ',map%size(1)*map%xycell(1),' x ',map%size(2)*map%xycell(2),'" '
    call map_message(seve%i,task,chain)
    !
    write(chain,103) map%size
    call map_message(seve%i,task,chain)
    write(chain,104) map%xycell
    call map_message(seve%i,task,chain)
    !
  endif
  !
100 format('Found ',i12,' Visibilities, ',i4,' channels')
101 format('Baselines ',f9.1,' - ',f9.1,a)
103 format('Recommended map size is   ',i4,' by ',i4)
104 format('Recommended pixel size is ',f8.3,' by ',f8.3,'"')
end subroutine uvstat_map
!
