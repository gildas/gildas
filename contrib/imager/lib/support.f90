subroutine com_support(line,error)
  use gildas_def
  use gkernel_interfaces
  use imager_interfaces, only : map_message, mask_clean
  use clean_default
  use clean_arrays
  use clean_support
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER  
  !   Support routine for command
  !	  SUPPORT [/PLOT] [/CURSOR] [/RESET] [/MASK] [/VARIABLE]
  !           [/THRESHOLD  Raw Smooth [SmoothingLength]
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line  ! Commandand line
  logical,          intent(out) :: error ! Logical error flag
  !
  ! Local variables
  character(len=*), parameter :: rname='SUPPORT'
  integer(kind=4), parameter :: o_name=0
  integer(kind=4), parameter :: o_curs=1
  integer(kind=4), parameter :: o_mask=2
  integer(kind=4), parameter :: o_plot=3
  integer(kind=4), parameter :: o_rese=4
  integer(kind=4), parameter :: o_thre=5
  integer(kind=4), parameter :: o_vari=6
  !
  logical :: fromfile, do_plot
  character(len=filename_length) :: file
  integer(kind=4) :: ier
  real(kind=4), allocatable, target :: imask(:,:)
  !
  do_plot = sic_present(o_plot,0)
  !
  if (sic_present(o_curs,0)) then  ! /CURSOR
    call map_message(seve%i,rname,'Use cursor to define polygon')
    user_method%do_mask = .true.
    file = ' '
    fromfile = .false.
    !
    ! It is actually not obvious that the MASK should be erased here...
    call sic_delvariable('MASK',.false.,error)
    hmask%loca%size = 0
    !
  else if (sic_present(o_rese,0)) then  ! /RESET
    if (allocated(dmask)) then
      deallocate(dmask,stat=ier)
    endif
    call greg_poly_reset(supportpol,supportvar,error)
    if (error)  return
    user_method%do_mask = .true.  ! The mask HAS changed...
    call sic_delvariable('MASK',.false.,error)
    support_type = support_none
    hmask%loca%size = 0
    ! Delete the "supportvar" structure
    call sic_delvariable(supportvar,.false.,error)
    return
    !
  else if (sic_present(o_mask,0)) then  ! /MASK
    if (allocated(dmask)) then
      call sic_delvariable('MASK',.false.,error)
      call sic_mapgildas ('MASK',hmask,error,dmask)
      !
      user_method%do_mask = .true.
      support_type = 1 ! First plane will be used by default
      !
      ! Show it if asked for by /PLOT option
      if (do_plot) then
        allocate (imask(hmask%gil%dim(1),hmask%gil%dim(2)),stat=ier)
        imask = 0.0
        where (dmask(:,:,1).ne.0) imask = 1.0
        call sic_def_real ('BITMAP_MASK',imask,hmask%gil%ndim,   &
         &        hmask%gil%dim,.true.,error)
        call gr_exec2('PLOT BITMAP_MASK /SCALING LIN -0.5 1.5')
        error = gr_error()
        call sic_delvariable('BITMAP_MASK',.false.,error)
        deallocate (imask,stat=ier)
      endif
    else
      call map_message(seve%e,rname,'No mask defined')
      error  = .true.
    endif
    return
  else if (sic_present(o_thre,0)) then
    ! This is obsolescent...
    !
    ! SUPPORT /THRESHOLD Raw Smooth [Length]
    !
    !  Raw      Thresholding (in Sigma) of the Clean image
    !  Smooth   Thresholding (in Sigma) after smoothing
    !  Length   Smoothing length: default is Clean beam major axis
    !
    call map_message(seve%e,rname,'/THRESHOLD option is Obsolete, use MASK THRESHOLD instead')
    error = .true.
    return
  else if (do_plot.and.sic_narg(o_name).eq.0) then  
    ! SUPPORT /PLOT 
    !
    ! Just plot
    if (support_type.eq.support_poly) then
      call greg_poly_plot(supportpol,error)
    else if (support_type.ne.support_none) then
      allocate (imask(hmask%gil%dim(1),hmask%gil%dim(2)),stat=ier)
      imask = 0.0
      where (dmask(:,:,1).ne.0) imask = 1.0
      call sic_def_real ('BITMAP_MASK',imask,hmask%gil%ndim,   &
       &        hmask%gil%dim,.true.,error)
      call gr_exec2('PLOT BITMAP_MASK /SCALING LIN -0.5 1.5')
      error = gr_error()
      call sic_delvariable('BITMAP_MASK',.false.,error)
      deallocate (imask,stat=ier)
      support_type = support_mask
    else
      call map_message(seve%w,rname,'No support defined')
      error  = .true.      
    endif
    return
  else
    !
    ! This also re-instate the Polygon as the Clean support
    user_method%do_mask = .true.
    if (sic_narg(o_name).ge.1) then
      call greg_poly_parsename(line,o_name,o_vari,fromfile,file,error)
      if (error) return
    else
      ! No argument, no /CURSOR option either: re-instate the
      ! current Polygon as a SUPPORT if any.
      if (supportpol%ngon.lt.3) then
        call map_message(seve%e,rname,'No current support defined')
        error = .true.
        return
      endif
      support_type = support_poly
      return
    endif
  endif
  !
  ! File, /VARIABLE or /CURSOR option
  !
  ! Define the polygon from file or cursor:
  call sic_delvariable(supportvar,.false.,error)
  call sic_defstructure(supportvar,.true.,error)
  !
  ! NO Reason to do this here --- ! hmask%loca%size = 0
  call greg_poly_define(rname,file,fromfile,supportpol,supportvar,error)
  if (error)  return
  support_type = support_poly
  !
  ! /PLOT if needed
  if (do_plot) call greg_poly_plot(supportpol,error)
  !
end subroutine com_support
