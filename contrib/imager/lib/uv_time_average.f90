subroutine uv_time_comm(line,error)
  use clean_arrays
  use clean_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uv_time_comm
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  ! Time-Average UV Tables, either on the "current" UV data set
  ! or on files  (usefull for ALMA data, which may be big)
  !
  ! UV_TIME Time /Weight Wcol [/FILE FileIn FileOut]
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_TIME'
  integer, parameter :: opt_file=1
  integer, parameter :: opt_wcol=2
  character(len=filename_length) :: nami,namo
  character(len=32) :: mytime ! Integration time
  real :: myuv   ! Maximum UV distance
  integer :: wcol
  integer :: n
  logical :: required
  !
  if (sic_present(opt_file,0)) then
    !!call map_message(seve%w,rname,'/FILE option not yet fully coded')
    call sic_ch(line,0,1,mytime,n,.true.,error)
    if (error) return
    myuv = 1E10 ! m, bigger than Earth...
    call sic_r4(line,0,2,myuv,.false.,error)
    wcol = 0
    call sic_i4(line,opt_wcol,1,wcol,.false.,error)
    if (error) return
    call sic_ch(line,opt_file,1,nami,n,.true.,error)
    if (error) return
    required = mytime.ne.'?'
    call sic_ch(line,opt_file,2,namo,n,required,error)
    if (error) return
    !
    call uvtime_disk_pre (nami,namo,mytime,myuv,wcol,error)
  else
    call uv_time_mem(line,error)
  endif
end subroutine uv_time_comm
!  
subroutine uv_time_mem(line,error)
  use clean_arrays
  use clean_types
  use gkernel_interfaces
  use imager_interfaces, except_this => uv_time_mem
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  ! UV_TIME Time /Weight Wcol
  !
  ! Time-Average the UV Table.   !
  ! Works on the "current" UV data set: Uses UVS or UVR as needed.
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_TIME_AVERAGE'
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  integer, parameter :: opt_file=1
  integer, parameter :: opt_wcol=2
  !
  real(4), save :: mytime
  !
  integer(4) :: wcol
  character(len=80) :: mess
  real(8), allocatable :: times(:), ttime(:)
  real(4), allocatable ::  btime(:)
  integer, allocatable :: ip(:), ftime(:), ltime(:)
  integer :: ntime, mtime, first_time, last_time, nbtime
  real(8) :: old_time, t0
  integer :: iv, jv, kv, lv, it, ier, mvisi, maxant
  integer :: nu, nv, mv
  real :: rmax
  type (gildas) :: hcuv
  real, pointer :: duv_previous(:,:), duv_next(:,:)
  real, allocatable :: din(:,:)
  character(len=64) :: mychain
  integer :: nc, first, fstart, fend
  integer :: ifield, np, nvout, ixoff, iyoff
  real(4), allocatable :: doff(:,:)
  integer, allocatable :: voff(:)
  logical :: shift, sorted, hasarg
  real :: uvmax, uvmin
  real(8) :: new(3)
  real :: eps, retime
  !
  ! The UV table is available in HUV%
  if (huv%loca%size.eq.0) then
    call map_message(seve%e,rname,'No UV data loaded')
    error = .true.
    return
  endif
  !
  shift = .false.
  new = 0.d0
  eps = 0.01
  call sic_get_real('TIME_LOSS',eps,error)
  error = .false.
  !
  mychain = '?'
  call sic_ch(line,0,1,mychain,nc,.false.,error)
  retime = 86400  ! Some sufficiently large value (1 day here...)
  call get_nyquist_time(rname,huv,duv,retime,eps,error)
  hasarg = sic_present(0,1)
  if ((.not.hasarg).or.mychain.eq.'?') then
    !
    if (error) return
    mytime = retime
    if (hasarg) then
      write(mychain,'(A,I0,A)') 'Can smooth up to UV_TIME = ',nint(mytime),' s'
    else
      write(mychain,'(A,I0,A)') 'Smoothing to ',nint(mytime),' s'
    endif
    call map_message(seve%i,rname,mychain)
    call sic_let_real('UV_TIME',mytime,error)
    if (hasarg) return
  else
    call sic_r4(line,0,1,mytime,.true.,error)
    if (error) return
    if (mytime.le.0) then
      call map_message(seve%e,rname,'Smoothing time must be positive')
      error = .true.
      return
    else if (retime.lt.mytime) then
      write(mychain,'(A,I0,A,I0,A)') 'Smoothing time (',nint(mytime), &
        & ' s) longer than recommended value (', nint(retime),' s)'
      call map_message(seve%w,rname,mychain,3)
    endif    
  endif
  !
  wcol = 0
  call sic_i4(line,opt_wcol,1,wcol,.false.,error)
  if (error) return
  !
  nu = huv%gil%dim(1)
  nv = huv%gil%dim(2)
  !
  if (themap%nfields.eq.0) then
    !
    ! Read the Time Baseline information, and verify it is sorted
    allocate (times(nv), ip(nv), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    !
    t0 = duv(4,1) ! First date
    maxant = 0
    do iv = 1, nv
      times(iv) = (duv(4,iv)-t0)*86400.d0+duv(5,iv)-duv(5,1)
      rmax = max(duv(6,iv),duv(7,iv))
      maxant = max(maxant,nint(rmax))
    enddo
    !
    ! So here is the maximum number of visibilities per time range
    mvisi = (maxant*(maxant-1))/2
    !
    ! Sort the time array
    call gr8_trie(times,ip,nv,error)
    !
    ! Figure out how many output times
    old_time = -1d10
    ntime = 0
    do iv = 1, nv
      if ((times(iv)-old_time).ge.mytime) then
        old_time = times(iv)
        ntime = ntime+1
      endif
    enddo
    mtime = ntime
    !
    allocate (ttime(mtime), ftime(mtime), ltime(mtime), btime(mtime), stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    !
    ! Find out the time boundaries
    jv = 0
    ntime = 0
    old_time = -1d10
    do iv = 1, nv
      if ((times(iv)-old_time).ge.mytime) then
        if (ntime.gt.0) ltime(ntime) = iv-1
        old_time = times(iv)
        ntime = ntime+1
        ftime(ntime) = iv
        ttime(ntime) = times(iv)
      endif
    enddo
    ltime(ntime) = nv   ! The last is a time change
    !
    ! Prepare header and allocate buffers
    call gildas_null(hcuv,type='UVT')
    call gdf_copy_header(huv,hcuv,error)
    mvisi = ntime*mvisi     
    hcuv%gil%dim(2) = mvisi
    hcuv%gil%nvisi = mvisi     ! For the time being...
    nullify (duv_previous, duv_next)
    call uv_find_buffers (rname,nu,mvisi,duv_previous,duv_next,error)
    if (error) return
    !
    ! Setup the Weight column
    if (wcol.eq.0) wcol = max(3,huv%gil%nchan)/3
    !
    ! Now do the job
    old_time = ttime(1)
    jv = 1  ! Initialize here
    nbtime = 0
    do it=1,ntime
      !
      first_time = ftime(it)
      last_time = ltime(it)
      ! Some useless debugging...
      !write(mess,*) 'Time ',old_time,' range ',first_time,last_time, ' / ',nv
      !call map_message(seve%i,rname,mess)
      mv = last_time-first_time+1
      allocate (din(nu,mv),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Memory allocation error')
        error = .true.
        return
      endif
      kv = 0
      do lv = first_time,last_time
        kv = kv+1
        din(:,kv) = duv(:,ip(lv))
      enddo
      if (mv.ne.kv) Print *,'Missing antennas on time ',it,kv,mv
      kv = jv
      call time_compress(din,nu,1,mv, &
          wcol, hcuv, duv_next, jv, mvisi, error)
      if (error) then
        write(mess,*) 'Programming error or strange UV table ',jv,' > ',mvisi
        call map_message(seve%e,rname,mess)
        return
      endif
      if (jv.lt.kv) then
        nbtime = nbtime+1
        btime(nbtime) = old_time
!        write(mess,*) 'No valid averaged visibility for ',old_time
!        call map_message(seve%w,rname,mess)
      endif
      jv = jv+1     ! Point to next one, then...
      old_time = ttime(it)
      deallocate (din)
    enddo
    !
    ! Finalize the UV Table with the right numbers
    hcuv%gil%nvisi = jv-1
    if (nbtime.ne.0) then
      if (nbtime.eq.1) then
        mess = 'No valid averaged visibility for the following time'
      else
        write(mess,'(A,I0,A)') 'No valid averaged visibility for the ',nbtime,' following times'
      endif
      call map_message(seve%w,rname,mess)
      write(*,*) btime(1:nbtime)
    endif
  else
    if (allocated(doff)) deallocate(doff)
    if (allocated(voff)) deallocate(voff)
    np = abs(themap%nfields)
    allocate(doff(2,np),voff(np+1),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    !
    ! Make sure the UV table has been sorted by fields first
    call map_message(seve%w,rname,'Sorting mosaic UV table')
    ixoff = huv%gil%column_pointer(code_uvt_xoff)
    iyoff = huv%gil%column_pointer(code_uvt_yoff)
    if (ixoff.eq.0) then
      ixoff = huvi%gil%column_pointer(code_uvt_loff)
      iyoff = huvi%gil%column_pointer(code_uvt_moff)
    endif
    call mosaic_sort (error,sorted,shift,new,uvmax,uvmin, &
      & ixoff,iyoff,np,doff,voff)
    if (error) return
    !
    ! Prepare header and allocate buffers
    call gildas_null(hcuv,type='UVT')
    call gdf_copy_header(huv,hcuv,error)
    nullify (duv_previous, duv_next)
    call uv_find_buffers (rname,nu,nv,duv_previous,duv_next,error)
    if (error) return
    !
    if (wcol.eq.0) wcol = max(3,huv%gil%nchan)/3
    !
    ! Loop over fields to sort each data
    first = 1
    ! This loop cannot be parallelized ...
    do ifield = 1,np
      !
      fstart = voff(ifield)       ! Starting Visibility of field
      fend   = voff(ifield+1)-1   ! Ending Visibility of field
      !
      call time_integrate(rname,huv,mytime,wcol,nu,fend-fstart+1, &
        & duv(:,fstart:fend),duv_next(:,first:),nvout,error)
        if (error) return
      Print *,'Field ',ifield,nvout, ' Visibilities from range ',fstart,fend
      first = first+nvout
    enddo
    !
    hcuv%gil%dim(2)= first-1 ! Is this allowed ?...
    hcuv%gil%nvisi = first-1
  endif
  !
  if (hcuv%gil%nvisi.eq.0) then
    call map_message(seve%e,rname,'No valid visibility after time compression')
    error = .true.
    hcuv%gil%dim(2)= 1 ! To avoid crashes
    return
  endif
  !
  hcuv%gil%dim(2)= hcuv%gil%nvisi    
  write(mess,'(i12,a,i12,a)') hcuv%gil%nvisi,' Visibilities created (', &
    & huv%gil%nvisi,' before)'
  call map_message(seve%i,rname,mess)
  call uv_clean_buffers (duv_previous, duv_next,error)
  if (error) return
  !
  ! Copy back to UV data set
  call gdf_copy_header(hcuv,huv,error)
  !
  ! Indicate UV data has changed, and weight must be computed
  call uv_new_data(weight=.true.)
end subroutine uv_time_mem
!
subroutine time_compress(invisi,nd,first,last,wcol, out, ovisi, jv, &
  & mvisi, error)
  use gildas_def
  use image_def
  use gkernel_interfaces
  use imager_interfaces, except_this => time_compress
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !   Time compress a UV table
  !   Compute visibilities for a single output time
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: out
  integer, intent(in) :: nd                     ! Visibility size
  real, intent(in) :: invisi(:,:)               ! Input visibilities
  integer, intent(in) :: wcol                   ! Weight column
  real, intent(inout) :: ovisi(:,:)             ! Output visibilities
  integer, intent(in) :: first, last            ! First and last input visibilities
  integer, intent(inout) :: jv                  ! Current output visibility number
  integer, intent(in) :: mvisi
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='TIME_COMPRESS'
  real, allocatable, target :: avisi(:,:), bvisi(:,:)
  real, pointer :: my_visi(:,:)
  integer, allocatable :: tb(:), indx(:)
  integer :: vmax, iant, jant
  integer :: iv, kv, nvisi, ier, icol, ic
  logical :: sorted
  real :: sw
  !
  error = .false.
  !
  ! Extract the relevant - A copy is needed in all cases...
  nvisi = last-first+1
  allocate (avisi(nd,nvisi),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  avisi(1:nd,1:nvisi) = invisi(:,first:last)
  !
  ! Sort it by BASELINE.
  ! We do not care about time here, since we are time averaging
  !
  ! Check the order
  allocate (tb(nvisi),stat=ier)
  do iv=1,nvisi
    ! Use the baseline number code as major driver
    if (avisi(7,iv).lt.avisi(6,iv)) then
      tb(iv) = (256*avisi(6,iv)+avisi(7,iv))
    else if (avisi(7,iv).gt.avisi(6,iv)) then
      tb(iv) = (256*avisi(7,iv)+avisi(6,iv))
    else
      ! No Baseline code for Short spacings which have Iant = Jant
      tb(iv) = 0
    endif
  enddo
  sorted = .true.
  vmax = tb(1)
  do iv = 1,nvisi
    if (tb(iv).lt.vmax) then
      sorted = .false.
      exit
    endif
    vmax = tb(iv)
  enddo
  !
  ! Sort it if needed
  if (.not.sorted) then
    allocate(indx(nvisi),bvisi(nd,nvisi),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    do iv = 1,nvisi
      indx(iv) = iv
    enddo
    call gi4_trie(tb,indx,nvisi,error)
    if (error) then
      call map_message(seve%e,rname,'Sorting error')
      return
    endif
    do iv=1,nvisi
      kv = indx(iv)
      bvisi(:,iv) = avisi(:,kv)
    enddo
    my_visi => bvisi
  else
    my_visi => avisi
  endif
  !
  !!Print *,'Nstokes ',out%gil%nstokes
  !!Print *,'Fcol    ',out%gil%fcol
  !!Print *,'Lcol    ',out%gil%lcol
  !!Print *,'Nchan   ',out%gil%nchan
  ! 
  ! Now do the real work for each baseline
  icol = out%gil%nlead+out%gil%natom*wcol
  !
  kv = jv ! Output visibility pointer
  ovisi(:,jv) = 0      ! Current visibility
  sw = 0.0             ! Current running weight
  iant = my_visi(6,1)  ! First antenna
  jant = my_visi(7,1)  ! Last antenna
  
  do iv=1,nvisi
    if (iant.eq.my_visi(6,iv) .and. jant.eq.my_visi(7,iv)) then
      ! Same antennas... Continue adding 
      ! ... unless it is a Short spacing stuff...
      if (iant.eq.-1 .and. sw.ne.0.0) then
        call mean_visiw(out,ovisi(:,kv),sw)
        kv = kv+1
        if (kv.gt.mvisi) then  ! Test for overflow (normally none...)
          Print *,'MVISI ',mvisi,' First ',first, ' Overflow ...'
          Print *,'KV ',kv, ' -- Visi ',iv+first-1,' changing from ',iant,jant, &
      & ' to ',my_visi(6,iv),my_visi(7,iv)
          error = .true.
          return
        endif
      endif
    else if (iant.eq.my_visi(7,iv) .and. jant.eq.my_visi(6,iv)) then
      ! Same antennas, but reversed order...
      !
      ! We take care of reversed visibilities, which could happen
      ! when V is close to zero, and the UV table has been already
      ! sorted. This can be done by changing the Imaginary part of
      ! my_visi as well as u,v (and w in principle...)
      ! Print *,iv,' Reversed ',my_visi(1:7,iv)
      my_visi(1,iv) = -my_visi(1,iv)
      my_visi(2,iv) = -my_visi(2,iv)
      my_visi(6,iv) = iant
      my_visi(7,iv) = jant
      do ic=out%gil%fcol+1,out%gil%lcol,3
        my_visi(ic,iv) = -my_visi(ic,iv)
      enddo
    else
      ! Different antennas
      !   Stop accumulating the current visibility  
 !!     Print *,'KV ',kv, ' -- Visi ',iv,' changing from ',iant,jant, &
 !!     & ' to ',my_visi(6,iv),my_visi(7,iv)
      if (sw.ne.0.0) then
        ! Normalize it and write it if it is not empty
        call mean_visiw(out,ovisi(:,kv),sw)
        kv = kv+1
        if (kv.gt.mvisi) then
          Print *,'MVISI ',mvisi,' First ',first
          Print *,'KV ',kv, ' -- Visi ',iv+first-1,' changing from ',iant,jant, &
      & ' to ',my_visi(6,iv),my_visi(7,iv)
          error = .true.
          return
        endif
        ovisi(:,kv) = 0.0
      endif
      ! Move to new antenna pair
      iant = my_visi(6,iv)
      jant = my_visi(7,iv)
      sw = 0.0
    endif
    !
    ! Once done, Add to Current visibility - 
    call add_visiw(out,ovisi(:,kv),my_visi(:,iv),sw)
  enddo
  !
  if (sw.ne.0.0) then
    jv = kv
    call mean_visiw(out,ovisi(:,kv),sw)
  else
    jv = kv-1
  endif
  !
end subroutine time_compress
!
subroutine add_visiw(out,ovisi,avisi,sw)
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  ! 
  ! IMAGER
  !   Support for command UV_TIME
  !
  !   Add a visibility to current output one
  !   Pre-channel weights are used, and the mean u,v coordinates
  !   are weighted according to the sum of weights on channels
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: out   ! Input image header
  real, intent(inout) :: ovisi(:)   ! Output visibilities
  real, intent(inout) :: avisi(:)   ! Input visibilities
  real, intent(inout) :: sw         ! Output weight
  !
  integer i
  real :: bw, cw
  real :: uv2
  real :: uv2max=12.0**2 ! Oops, not adequate fo NOEMA
  !
  !  if (aw.le.0) return
  !
  ! This is a patch for old files which have been incorrectly
  ! sorted, without the antennas being swapped...
  if (sw.gt.0) then
    uv2 = (ovisi(1)/sw-avisi(1))**2 + (ovisi(2)/sw-avisi(2))**2
    if (uv2.gt.uv2max) then
      if (ovisi(1)*avisi(1).lt.0) then
        Print *,'Continuity problem',sqrt(uv2), &
        & '(Bad Antenna order ?), Patching data'
        Print *,'Last Mean UV is ',ovisi(1:2)/sw,' while UV to be added ',avisi(1:2)
        Print *,'Last    ',ovisi(5:out%gil%nlead), ovisi(out%gil%nlead+1:out%gil%nlead+2)/sw
        Print *,'Current ',avisi(5:out%gil%nlead+2)
        avisi(1) = -avisi(1)
        avisi(2) = -avisi(2)
        do i = out%gil%nlead+2,out%gil%nstokes*out%gil%natom*out%gil%nchan+out%gil%nlead,out%gil%natom
          avisi(i) = -avisi(i)
        enddo
      endif
    endif
  endif
  !
  cw = 0
  do i = out%gil%nlead+1,out%gil%nstokes*out%gil%natom*out%gil%nchan+out%gil%nlead,out%gil%natom
    bw = avisi(i+2)     ! was   aw before
    if (bw.gt.0) then
      ovisi(i) = ovisi(i) + bw*avisi(i)
      ovisi(i+1) = ovisi(i+1) + bw*avisi(i+1)
      ovisi(i+2) = ovisi(i+2) + avisi(i+2)   ! Sum the weights
      cw = cw+bw
    endif
  enddo
  ovisi(1:3) = ovisi(1:3)+cw*avisi(1:3)
  ovisi(4:out%gil%nlead) = avisi(4:out%gil%nlead)
  do i=out%gil%lcol+1,out%gil%dim(1)
    ovisi(i) = avisi(i)
  enddo
  sw = sw+cw
end subroutine add_visiw
!
subroutine mean_visiw(out,ovisi,sw)
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  !   IMAGER
  !
  !   Support for command UV_TIME
  !   Normalize the final visibility
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: out   ! Input image header
  real, intent(inout) :: ovisi(:)   ! Output visibilities
  real, intent(inout) :: sw         ! Output weight
  !
  integer i
  real :: bw
  !
  if (sw.le.0) return
  !
  ovisi(1:3) = ovisi(1:3)/sw
  do i = out%gil%nlead+1,out%gil%nstokes*out%gil%natom*out%gil%nchan+out%gil%nlead,out%gil%natom
    bw = ovisi(i+2)       ! Was sw before
    if (bw.gt.0) then
      ovisi(i) = ovisi(i)/bw
      ovisi(i+1) = ovisi(i+1)/bw
    endif
  enddo
end subroutine mean_visiw

subroutine time_integrate(rname,huv,mytime,wcol,nu,nv,vin,vout,nvout,error)
  use image_def
  use gbl_message
  use gkernel_interfaces
  use imager_interfaces, only : map_message, time_compress
  !---------------------------------------------------------------------
  ! @ public
  ! Time-Average a UV data set
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname  ! Calling routine
  type(gildas), intent(in) :: huv        ! UV Header
  real(4), intent(in) :: mytime          ! Integration time
  integer, intent(in) :: wcol            ! Weight column
  integer, intent(in) :: nu              ! Size of a visibility
  integer, intent(in) :: nv              ! Number of input visibilities
  real, intent(in) :: vin(:,:)           ! input visibilities
  real, intent(inout) :: vout(:,:)       ! output visibilities
  integer, intent(out) :: nvout          ! Number of output visibilities
  logical, intent(out) :: error          ! Error flag
  !
  character(len=80) :: mess
  real(8), allocatable :: times(:), ttime(:)
  integer, allocatable :: ip(:), ftime(:), ltime(:)
  real, allocatable :: din(:,:)
  integer :: ier, ntime, mtime
  real(8) :: t0, old_time
  integer :: first_time, last_time
  integer :: it,iv,jv,kv,lv,mv
  integer :: mvisi
  !
  ! Read the Time Baseline information, and verify it is sorted
  allocate (times(nv), ip(nv), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  mvisi = ubound(vout,2)
  !
  t0 = vin(4,1) ! First date
  do iv = 1, nv
    times(iv) = (vin(4,iv)-t0)*86400.d0+vin(5,iv)-vin(5,1)
  enddo
  !
  ! Sort the time array
  call gr8_trie(times,ip,nv,error)
  !
  ! Figure out how many output times
  old_time = -1d10
  ntime = 0
  do iv = 1, nv
    if ((times(iv)-old_time).ge.mytime) then
      old_time = times(iv)
      ntime = ntime+1
    endif
  enddo
  mtime = ntime
  !
  allocate (ttime(mtime), ftime(mtime), ltime(mtime), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Find out the time boundaries
  jv = 0
  ntime = 0
  old_time = -1d10
  do iv = 1, nv
    if ((times(iv)-old_time).ge.mytime) then
      if (ntime.gt.0) ltime(ntime) = iv-1
      old_time = times(iv)
      ntime = ntime+1
      ftime(ntime) = iv
      ttime(ntime) = times(iv)
    endif
  enddo
  ltime(ntime) = nv   ! The last is a time change
  !
  ! Now do the job
  old_time = ttime(1)
  jv = 1  ! Initialize here
  do it=1,ntime
    !
    first_time = ftime(it)
    last_time = ltime(it)
    ! Some useless debugging...
    !!Print *,'Time ',old_time,' range ',first_time,last_time, ' / ',nv
    mv = last_time-first_time+1
    allocate (din(nu,mv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    kv = 0
    do lv = first_time,last_time
      kv = kv+1
      din(:,kv) = vin(:,ip(lv))
    enddo
    if (mv.ne.kv) Print *,'Missing antennas on time ',it,kv,mv
    kv = jv
    call time_compress(din,nu,1,mv, wcol, huv, vout, jv, mvisi, error)
    if (error) then
      write(mess,*) 'Programming error or strange UV table ',jv,' > ',mvisi
      call map_message(seve%e,rname,mess)
      return
    endif
    if (jv.lt.kv) then
      write(mess,*) 'No valid averaged visibility for ',old_time
      call map_message(seve%w,rname,mess)
    endif
    jv = jv+1     ! Point to next one, then...
    old_time = ttime(it)
    deallocate (din)
  enddo
  !
  nvout = jv-1
end subroutine time_integrate
!
subroutine uvtime_disk_otf(line,error)
  use clean_arrays
  use clean_types
  use gkernel_interfaces
  use imager_interfaces, only : map_message !! except_this => uvtime_disk_otf
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  ! UV_TIME Time /FILE FileIn FileOut [/WEIGHT Wcol]
  ! 
  ! Time-Average the UV Table found in FileIn and put it
  ! into FileOut. Very usefull for ALMA data which can 
  ! have very small integration times.
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_TIME_AVERAGE'
  !
  real(8), parameter :: pi=3.14159265358979323846d0
!
! Disk time compression
!
! Verify UV table is already time sorted 
! This verification can be made "on the fly", or by reading
! only the date/time information first.
! 
! The difficulty is then, while working with blocks, to make
! sure the integration does not go over the block boundary. 
!
! The simplest way is to re-read the last time sequence, i.e.
! when  time(last_visi)-time(last_treated+1) < integration_time,
! to restart at last_treated+1, where last_treated is the
! latest visibility which has been integrated.  
  call map_message(seve%e,rname,'UVTIME_DISK_OTF not yet coded')
  error = .true.
end subroutine uvtime_disk_otf
!
subroutine uvtime_disk_pre (nami,namo,arg,myuv,wcol,error)
  use image_def
  use gkernel_interfaces
  use imager_interfaces, except_this => uvtime_disk_pre
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !
  ! UV_TIME Time /FILE FileIn FileOut [/WEIGHT Wcol]
  ! 
  ! Time-Average the UV Table found in FileIn and put it
  ! into FileOut. Very usefull for ALMA data which can 
  ! have very small integration times.
  !
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  character(len=*), intent(in) :: arg      ! Desired Integration Time
  real(4), intent(in) :: myuv    ! Maximum UV length change
  integer, intent(inout) :: wcol ! Weight channel
  logical, intent(out) :: error
  !
  ! Disk time compression
  !
  ! Verify UV table is already time sorted - 
  ! This verification is made by reading
  ! only the date/time information first.
  !
  ! Then, with pre-defined time boundaries, the work can be
  ! done by using an adequate buffer, for every time range 
  !
  ! This version does not yet handle the Mosaic case, contrary
  ! to the Memory version
  !
  character(len=*), parameter :: rname='UV_TIME'
  character(len=256) :: mess
  type (gildas) :: hin, hout
  real :: mytime
  real(kind=4), allocatable, target :: din(:,:), dout(:,:)
  integer, allocatable :: the_times(:), ftime(:), ltime(:)
  real(8), allocatable :: times(:), ttime(:)
  integer :: nvis, iv, ntime, first_time, last_time
  real(8) :: old_time, t0
  integer :: mv, nv, jv, it, ier, mvisi, maxant
  integer :: codes(6)
  real :: rmax
  real :: eps
  !
  eps = 0.01
  call sic_get_real('TIME_LOSS',eps,error)
  error = .false.
  !
  ! Read Header of a sorted, UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  !
  if ( ( (hin%gil%column_size(code_uvt_xoff).ne.0) .and. &
    & (hin%gil%column_size(code_uvt_yoff).ne.0) ).or. &
    & ( (hin%gil%column_size(code_uvt_loff).ne.0) .and. &
    & (hin%gil%column_size(code_uvt_moff).ne.0) ) ) then
    call map_message(seve%e,rname,'Mosaic UV tables not yet supported in /FILE mode')
    error = .true.
    return
  endif
  !
  ! Read the Time Baseline information, and verify it is sorted
  nvis = hin%gil%nvisi
  allocate (din(6,nvis),stat=ier)
  codes(1:6) = [code_uvt_u,code_uvt_v,code_uvt_date, code_uvt_time, code_uvt_anti, code_uvt_antj]
  call gdf_read_uvonly_codes(hin, din, codes, 6, error)
  !
  ! Check the Nyquist time if needed
  if (arg.eq.'?' .or. arg.eq.' ') then
    call get_nyquist_time(rname,hin,din,mytime,eps,error)
    if (error) return
    if (arg.eq.'?') then
      write(mess,*) 'Can smooth up to UV_TIME = ',mytime
      call map_message(seve%i,rname,mess)
      return
    else
      write(mess,*) 'Smoothing to ',mytime
      call map_message(seve%i,rname,mess)
    endif
  else
    read(arg,*,iostat=ier) mytime
    if ((ier.ne.0).or.(mytime.lt.0)) then
      call map_message(seve%e,rname,'Invalid time argument '//trim(arg))
      error = .true.
      return
    endif
  endif
  !
  allocate (times(nvis), the_times(nvis), ttime(nvis), ftime(nvis), &
     & ltime(nvis), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Get the chronological order:
  old_time = -1d10
  ntime = 0
  t0 = din(1,1)
  maxant = 0
  do iv = 1, nvis
    times(iv) = (din(3,iv)-t0)*86400.d0+din(4,iv)
    rmax = max(din(5,iv),din(6,iv))
    maxant = max(maxant,nint(rmax))
    if (times(iv).lt.old_time) then
      Print *,'IV ',iv,' T0 ',t0,' Times ',times(iv),din(3,iv),din(4,iv)
      call map_message(seve%e,rname,'UV table is not Time-Baseline sorted')
      error = .true.
      return
    elseif (times(iv).gt.old_time) then
      if (ntime.gt.0) ltime(ntime) = iv-1
      old_time = times(iv)
      ntime = ntime+1
      ftime(ntime) = iv
      ttime(ntime) = times(iv)
    endif
  enddo
  ltime(ntime) = nvis
  deallocate(din)
  ! ftime is the starting number
  ! ltime is the ending number
  !
  ! So here is the maximum number of visibilities per time range
  mvisi = (maxant*(maxant-1))/2
  write(mess,'(A,I0,A,I0)') 'Found ',maxant,' antennas, # baselines ',mvisi
  call map_message(seve%i,rname,mess)
!  do it=1,ntime
!    mvisi = max(ltime(it)-ftime(it)+1,mvisi)  ! Add one...
!  enddo
!  mvisi = mvisi+1
  allocate (dout(hin%gil%dim(1),mvisi), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Prepare output UV Table
  call gildas_null(hout, type='UVT')
  call gdf_copy_header (hin, hout, error)
  !! call gdf_setuv (hout, error)
  !
  ! Find out how many output visibilities there may be
  old_time = ttime(1)
  first_time = ftime(1)
  last_time = ltime(1)
  !
  jv = 0  ! Initialize properly
  do it=1,ntime-1
    if ((ttime(it)-old_time).gt.mytime) then
      jv = jv+mvisi  ! Count all time changes
    endif
  enddo
  jv = jv+mvisi ! Last time is a time change...
  !
  ! Allocate a suitable output UV Table
  hout%gil%dim(2) = jv
  hout%gil%nvisi = jv
  call sic_parsef (namo,hout%file,' ','.uvt')
  call gdf_create_image(hout,error)
  if (error) return
  !
  ! Now do the job
  old_time = ttime(1)
  first_time = ftime(1)
  last_time = ltime(1)
  jv = 1
  !
  if (wcol.eq.0) wcol = max(3,hin%gil%nchan)/3
  hin%blc = 0
  hin%trc = 0
  hout%trc = 0
  hout%blc = 0
  hout%blc(2) = 1
  mv = hin%gil%dim(1)
  nv = hin%gil%nvisi
  !
  do it=1,ntime
    !
    if ((ttime(it)-old_time).gt.mytime .or. it.eq.ntime) then
      !
      ! write(mess,*) it,' Time ',old_time,' range ',first_time,last_time
      ! call map_message(seve%d,rname,mess)
      call progress_report('Processed',first_time,last_time-first_time+1,nv,10)
      !
      hin%blc(2) = first_time
      hin%trc(2) = last_time
      allocate (din(mv,last_time-first_time+1),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Memory allocation error')
        error = .true.
        return
      endif
      call gdf_read_data(hin,din,error)
      if (error) return
      jv = 1 ! Initialize...
      call time_compress(din,mv,1,last_time-first_time+1, &
          wcol, hout, dout, jv, mvisi, error)
      if (error) then
        write(mess,*) it,' Time ',old_time,' range ',first_time,last_time, ' / ',nv
        call map_message(seve%e,rname,mess)
        write(mess,*) 'Programming error or strange UV table ',jv,' > ',mvisi
        call map_message(seve%e,rname,mess)
        return
      endif
      if (jv.gt.0) then
        hout%trc(2) = hout%blc(2)+jv-1
        call gdf_write_data(hout,dout,error)
        if (error) return
        hout%blc(2) = hout%trc(2)+1
      else
        write(mess,*) 'No valid averaged visibility for ',old_time
        call map_message(seve%w,rname,mess)
      endif
      first_time = ftime(it)
      old_time = ttime(it)
      deallocate (din)
    endif
    last_time = ltime(it)
  enddo
  write(*,'(A)') '  ... Done '
  !
  ! Finalize the UV Table with the right numbers
  hout%gil%dim(2)= max(1,hout%trc(2))   ! Play safe
  hout%gil%nvisi = hout%trc(2)
  call gdf_update_header(hout, error)
  call gdf_close_image(hout, error)
  !
end subroutine uvtime_disk_pre
!
subroutine get_nyquist_time(rname,huv,duv,mytime,eps,error)
  use image_def
  use gbl_message
  use clean_default
  use gkernel_interfaces, only : sic_get_real, gdf_uv_frequency
  use imager_interfaces, except_this => get_nyquist_time
  !---------------------------------------------------------------------
  ! @ private
  !
  !  IMAGER
  !     Get Maximum Smoothing time given Field of view,
  !     longest UV baseline and Required Precision
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(gildas), intent(inout) :: huv
  real, intent(in) :: duv(:,:)
  real, intent(inout) :: mytime
  real, intent(in) :: eps
  logical, intent(inout) :: error
  !
  real(8), parameter :: pi=3.14159265358979323846d0
  character(len=256) :: mess
  !
  real :: diam, cdiam    ! Map UV cell size
  real :: cfield         ! Field of view
  real(8) :: freq, wave  ! Frequency, Wevlength
  real :: uvmin, uvmax   ! UV coverage limit
  real :: duvmax         ! Max smoothing length in UV plane
  integer :: time_100, time_10, time_nyquist
  !
  call uvgmax(huv,duv,uvmax,uvmin)
  if (huv%gil%nteles.ne.0) then
    diam = huv%gil%teles(1)%diam/2.0
  else
    diam = 0.
  endif
  !
  freq = gdf_uv_frequency(huv)
  wave = 299792458d0/freq/1d6  ! Wavelength in m
  !
  cfield = 0
  error = .false.
  call sic_get_real('MAP_FIELD[1]',cfield,error)
  if (cfield.ne.0) then
    !
    ! duvmax = sqrt(6*eps)/pi*lambda/cfield
    cfield = cfield*pi/180/3600     ! Field of view in Radians
    diam = 1.22 * wave / 2. /cfield
  endif
  !
  call sic_get_real('MAP_UVCELL',cdiam,error)
  if (cdiam.eq.0) then
    cdiam = diam
  else if (cdiam.lt.0) then
    call map_message(seve%e,rname,'Negative MAP_UVCELL')
    error = .true.
    return
  else if (diam.eq.0) then
    diam = cdiam    ! Set a default DIAM to get a F.o.V.
  else if (cdiam.gt.diam) then
    call map_message(seve%w,rname,'Super-uniform weighting, MAP_UVCELL[1] > Diameter/2')
  endif
  !
  if (diam.eq.0) then
    call map_message(seve%e,rname,'Please specify MAP_UVCELL')
    error = .true.
    return
  else if (cfield.eq.0) then
    cfield = 1.22 * wave / diam / 2 ! Default field of view
    !! Print *,'Default field of view ',cfield*180*3600/pi  
  endif
  !
  ! Derive maximum UV change from Field of View: B.Cotton limit
  duvmax = sqrt(6*eps)*wave/cfield/pi
  time_100 = nint(duvmax/(2*pi*uvmax)*86400)
  !
  ! GILDAS Imiss Rule
  ! Time_10     0.1 Theta_s/Omega_earth/uvmax
  time_10 = default_map%precis*1.22*(wave/uvmax)/cfield*86400/(2*pi)
  !
  !  
  ! This is the Nyquist sampling time, for the specified field size
  time_nyquist = nint(cdiam/(4*pi*uvmax)*86400)
  write(mess,'(A,F6.2,A,I0,A,F4.2,A,I0,A,I0,A)' )  &
    & 'Time for ',100*eps,' % loss: ',time_100, &
    & ' s;  for ',default_map%precis,' precision: ',time_10, &
    & ' s;  Nyquist: ',time_nyquist,' s'
  call map_message(seve%i,rname,mess)
  !
  mytime = min(time_10,time_nyquist)
  !
end subroutine get_nyquist_time
