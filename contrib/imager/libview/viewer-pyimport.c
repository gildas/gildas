
/* Define entry point "initpyviewer" for command "import pyviewer" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(viewer);
