
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the VIEWER package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine viewer_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !
  type(gpack_info_t), intent(out) :: pack
  !
  external :: viewer_pack_init
  external :: viewer_pack_on_exit
  external :: greglib_pack_set
  !
  pack%name='viewer'
  pack%ext = '.ima'
  pack%depend(1:1) = (/ locwrd(greglib_pack_set) /)
  pack%init=locwrd(viewer_pack_init)
  pack%on_exit=locwrd(viewer_pack_on_exit)
  pack%authors="S.Guilloteau"
  !
end subroutine viewer_pack_set
!
subroutine viewer_pack_init(gpack_id,error)
  use gkernel_interfaces
  use sic_def
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  ! Local
  integer(kind=4) :: ier
  !
  ! Local language
  call init_display
  !
  ! One time initialization
  call map_message_set_id(gpack_id)  ! Set library id
  !
  ! Procedure extensions
  call exec_program('SIC'//backslash//'SIC EXTENSION .ima .greg ')
  ! Language priorities
  call exec_program('SIC'//backslash//'SIC PRIORITY 1 DISPLAY')
  !
  ! Specific initializations
  !
  ier = sic_setlog('gag_help_display','gag_doc:hlp/imager-help-display.hlp')
  ier = sic_setlog('gag_html_display:','gag_doc:html/imager-html/')
  !
end subroutine viewer_pack_init
!
subroutine viewer_pack_on_exit(error)
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  logical :: error
  !
end subroutine viewer_pack_on_exit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
