\section{Continuum emission}
\label{sec:cont}

\imager{} naturally handles many spectral channels. However, the
astronomer may be interested in broad-band continuum emission. 
Extracting the properties of such a continuum emission may be 
a difficult issue in some cases. \imager{} offers a number of tools
to do so, including the notion of a \sicvar{CONTINUUM} image.


To first order (over a limited frequency coverage), continuum emission can
be represented by a a flux (at some reference frequency) and a
spectral index.  Both flux and spectral index may vary spatially.
Such a representation is often valid over a factor 2 or 3 in frequency.
In general, the spatial distribution of these properties 
of the continuum emission may be widely different from those
of the spectral lines.  Furthermore, spectral lines can be easily
optically thick at some velocities, and may hide the continuum emitted 
from behind, and thin at others. The continuum itself may sometimes
have sufficient opacity to obscure line emission.

Thus a proper extraction of the continuum
properties can formally only be done in the image plane, after 
imaging the combined continuum and spectral line emission, and
cannot rely on a separation in the \uv{} plane at the level
of the visibilities.

In general, this cannot be done by simply imaging channel per
channel. The reason for that is that continuum emission is in general
fainter than the spectral line one, and often faint enough to have
only limited signal to noise in a single channel. Deconvolution of such
emission is noise limited. Yet, when averaging over many channels
(PolyFix on NOEMA offers 2048 channels per wide band window, ALMA
has up to 4096 per spectral window), the S/N changes by a large
amount, and the undeconvolved sidelobes from the continuum emission
appear. 

\subsection{Continuum imaging}
\label{sub:cont}
To avoid this, it is better to deconvolve the continuum emission
by averaging as many channels as possible. Line emission should be
filtered as much as possible at this level, since otherwise it
contaminates the information which is sought. A further
issue is that the effective resolution of the array goes as $\lambda/B$.
With a wide enough frequency coverage, this is a significant change
over the bandwidth that must be accounted for. It actually helps
to obtain a better \uv{} coverage, a process called \textit{bandwidth synthesis}.


In \imager{}, the whole process can be done in four commands\\
{\color{magenta}
\texttt{uv\_preview}\\
\texttt{uv\_filter}\\
\texttt{uv\_map /cont}\\
\texttt{clean}} (and optionally {\color{magenta} \texttt{uv\_restore}})
\begin{itemize}
\item Command \com{UV\_FILTER}, used after \com{UV\_PREVIEW}, will
remove all regions of the spectrum that are contaminated (above
the noise level) by spectral lines.
\item Command \comm{UV\_MAP}{/CONT} will then image in bandwidth synthesis
the remaining continuum emission. After that, \com{CLEAN} and
\com{UV\_RESTORE} can deconvolve and properly restore the image.
\end{itemize}
In command  \comm{UV\_MAP}{/CONT}, the user can specify the spectral
index of the emission to optimize the signal to noise by giving the
best weight to each spectral channel. 

This part is only a first step in a proper analysis. The next one
is to image the remaining signal in spectral lines, and ultimately
add back the deconvolved continuum image. To help the user in
this, the image obtained as above is automatically saved as
the \sicvar{CONTINUUM} SIC image variable.


\subsection{Split Continuum and Spectral line imaging}
\label{sub:splitcont}

To image the spectral line, we simply remove in the $uv$ plane 
the continuum defined before and image 
the resulting spectral line data:\\
{\color{magenta}
\texttt{uv\_baseline}\\
\texttt{uv\_map}\\
\texttt{clean}} (and optionally \qom{uv\_restore})

The last step is to add back to this image the deconvolved continuum
one:\\
{\color{magenta}
\texttt{define image my\_clean * real /like clean /global} \\
\texttt{map\_combine my\_clean ADD clean continuum}} \\
where the last command is equivalent to the following loop:\\
\texttt{let my\_clean\% clean\%      ! Set its header}\\
\texttt{for i 1 to clean\%dim[3]}\\
\texttt{~~let my\_clean[i] clean[i]+continuum}\\
\texttt{next}\\
Because the Fourier Transform is a linear operation, we end up with a 
combined data set that properly includes all the emission, line and 
continuum, deconvolved in a (well almost...)
%
\footnote{The combination
is only optimal if the spectral index has no spatial variations. A
better handling of spatially variable spectral index is under study.}
%
optimal way.

Further analysis of the spatial variations of the continuum emission
(including variations of its spectral index) can be done on this
combined image that contains all the information, see Sec.\ref{sub:mapcont}

Note that we used in this example commands 
\com{uv\_filter} and \com{uv\_baseline} with their default behaviour
provided by \com{uv\_preview}: the user could specify more adapted
values using the command options and/or control variables if needed.

\subsection{Combined Continuum and Spectral line imaging}
\label{sub:startcont}

An alternative to the method described in Sec.\ref{sub:splitcont} is
to use the Continuum image, or more precisely the Continuum Clean Components, 
as a starting point for the deconvolution of the continuum+line data.
The first step is to save the Continuum Clean Components:\\
{\color{magenta}
\texttt{uv\_preview}\\
\texttt{uv\_filter}\\
\texttt{uv\_map /cont}\\
\texttt{clean} \\
\texttt{write cct Continuum.cct}}\\ 
Re-imaging the data set with the continuum
included can be done by
{\color{magenta}
\texttt{uv\_filter /reset}\\
\texttt{uv\_map}\\
\texttt{read cct Continuum.cct}\\
\texttt{clean /restart}\\
\texttt{uv\_restore}} (as an optional step).

\qomm{UV\_FILTER}{/RESET} reset the weights of the line emission channels to their
proper values.  Reading again the Clean Component list is needed because \qom{UV\_MAP}
destroys this list. The Clean Component list of the continuum data is then used
as a starting value for every spectral channel by the \qomm{CLEAN}{/RESTART}.

\subsection{The \texttt{CONTINUUM} image}
\label{sub:mapcont}

The \sicvar{CONTINUUM} image can also be produced from a (deconvolved)
spectral line data cube by the \com{MAP\_CONTINUUM} command. Compared
to the method presented in Sec.\ref{sub:cont}, this has the advantage of being able
to handle spatially variable spectral index, and also spatially 
variable spectral line contamination. It has the drawback of limited
S/N for the deconvolution, as already mentionned, unless the spectral
line data cube also contains the continuum emission using one of
the two methods presented in Sec.\ref{sub:splitcont}-\ref{sub:startcont}.

Also, the \sicvar{CONTINUUM} image can be read from a GILDAS data file
using command \comm{READ}{CONTINUUM}. And of course, it can be written
to GILDAS data file by command \comm{WRITE}{CONTINUUM}.


