
Notes since Documentation release of Version 3.2, dated  Jan, 30th, 2022

\subsection{Recent changes in repository}
This section gives the changes made 
since the last documentation release (as dated above). The changes 
are available in the working repository, but may not be distributed 
in any standard Gildas release yet.  

\begin{itemize}
  \item{15-Nov-2022} Add the \qom{/MODE} option to the \qom{PIPELINE}
	command, with 4 modes: \texttt{CONTINUUM, SPLIT, ALL} or \texttt{SURVEY}.
  \item{14-Nov-2022} Debug use of \sicvar{MAP\_CENTER} variable in various
	commands (for Mosaics, \qomm{UV\_SELF}, \qom{UV\_SHIFT}).
  \item{08-Nov-2022} Reorganize commands in languages:\qom{EXTRACT}, \qom{SLICE}
	 and \qom{POPUP} are now part of the \lang{Display} language 
  \item{05-Sep-2022} Add the \qom{/ASYMMETRIC} option of \qom{MAP\_SMOOTH}  and \qom{UV\_SMOOTH}
  \item{05-Jul-2022} Add the \com{MAP\_POLAR} command
	This corresponds to revision 3.4-1 of the Documentation.
  \item{29-Jun-2022} Ensure \qom{READ} handles properly Random Frequency Axis in UVFITS format
  \item{29-Jun-2022} Ensure \qom{UV\_EXTRACT} works with Random Frequency Axis
  \item{22-Apr-2022} Improve \qom{UV\_PREVIEW} command
  \item{12-Apr-2022} Support for command \qom{STOKES} on internal buffers.
  \item{23-Mar-2022} Introduce the \lang{IMAGER} language, and the
	\com{PIPELINE} command. This corresponds to revision 3.3 of the Documentation.
\end{itemize}

\subsection{Known Bugs}
This section contains the list of bugs known as of \today.
\begin{itemize}
\item{\discovered 15-Nov-2022} \qom{UV\_RESTORE} in Mosaic mode leads to
subsequent crashes if the channel range is restricted by \qom{UV\_MAP} because
a single beam cannot be applied to all channels.\\
This is often the case for ALMA data, because of the shift implied by the
Doppler correction when creating the UVFITS files. A work around is to 
manually drop the edge channels by reading a coherent range of channels,
and over-writing the UV data.
\end{itemize}

\begin{itemize}
  \item{\corrected 08-Sep-2022} Bug correction in \qom{EXTRACT} command.
  \item{\corrected 29-Jun-2022} \qom{STOKES} command had incorrect derivation from H-V polarization
  \item{\corrected 09-Jun-2022} Debug \qom{UV\_FLAG} command - Allow to use without Display.
  \item{\corrected 13-Apr-2022} \qom{MAP\_COMBINE} and \qom{COMBINE} were not supporting
  big files.  
  \item{\corrected 12-Apr-2022} \qom{UV\_RESIDUAL} was not working on a non-sorted Mosaic data set.
  This could only happen by reading the Clean Component Table after reading the UV data, without
  any imaging step.
  \item{\corrected 09-Mar-2022} \qom{CCT\_MERGE} Concatenation was improper
  \item{\corrected 07-Mar-2022} Beam size derivation was incorrect when an ANGLE was specified
  \item{\corrected 08-Feb-2022} \qom{UV\_RESTORE} was not using fixed beam size.
  \item{\corrected 18-Jan-2022} \texttt{SINGLEDISH} data was incorrectly read, probably since
20-Sep-2021 (v1.53 of read.f90). Improper values were thus used by the \qom{UV\_SHORT} command
(\discovered 17-Jan-2022 only).
  \item{\corrected 12-Jan-2022} Mosaic offsets were improperly computed by command
\qom{UV\_FIELDS} since 29-Jul-2021 (bug reported 10-Jan-2022). The script
\texttt{@ fits\_to\_uvt} was thus leading to wrong pointing offsets.
  \item{\corrected 10-Jan-2022} A improper interaction between scripts was causing
strange behaviour when switching between \qom{EXPLORE} and \qom{INSPECT\_3D} commands. 
  \item{\corrected 06-Jan-2022} The \sic{} command \texttt{DEFINE IMAGE} failed on large
FITS files. This prevented \qom{SHOW} and \qom{VIEW} to work directly from these files.
  \item{\corrected 18-Dec-2021} \qom{UV\_TIME} produced incorrect results on tables
with different weights per channel (e.g. those produced by \qomm{UV\_MERGE}{/MODE CONCATENATE}).
  \item{\corrected 13-Dec-2021} Remove occasional infinite loop in \qom{MAP\_CONTINUUM}
with method GAUSS (discovered 01-Dec-2021)
  \item{\corrected 18-Nov-2021} \qom{UV\_REWEIGHT} syntax bug 
\end{itemize}

\subsection{Functional changes from previous versions}

Changes in Version 3.3
\begin{itemize}
\item{23-Mar-2022}  Introduce the \lang{IMAGER} language. Implement the \com{PIPELINE}
command there, and some miscellaneous (previously user-defined) commands like \com{TIPS}
and \com{POPUP}
\end{itemize}

Changes in Version 3.2
\begin{itemize}
\item{18-Mar-2022} Add the \comm{CLEAN}{/RESTART} capability
\item{10-Mar-2022} Improve dirty beam related commands behaviour: replace \sicvar{MAP\_BEAM\_STEP}
by \sicvar{BEAM\_STEP}, use \sicvar{BEAM\_SIZE} to specify beam shape.
\item{07-Mar-2022} Allow \qomm{WRITE BEAM}{/APPEND} in all cases
\item{08-Feb-2022} Implement the \sicvar{CLEAN\_STOP} syntax in \qom{CLEAN}
\item{10-Jan-2022} Implement the \com{LOAD} command in \viewer{}.
\item{07-Jan-2022} Implement \qom{SHOW}{PV} and improve transposed data display.
\item{06-Jan-2022} Allow direct \qom{READ} of FITS data cubes.
\item{03-Jan-2022} Improve syntax of \qom{COMBINE}
\item{17-Dec-2021} Improve \qomm{UV\_CHECK}{BEAM} and \com{FIT} commands.
\item{16-Dec-2021} Add the \qomm{UV\_MERGE}{/MODE CONCATENATE}
command to stitch adjacent or overlapping spectral windows. 
Imaging is in general only possible using \sicvar{BEAM\_STEP = 1}
after this, so far.
\item{13-Dec-2021} Implement a fast, reliable version of "statcont"
through the \qom{MAP\_CONTINUUM} command (run time $< 40$ secs 
for a 28 Gbyte data set on a 64-core machine)
\item{13-Dec-2021} Globally revised the Robust weighting scheme. Speed
up by huge amount for large (> 10 MegaVis) number of visibilities. 
UV data sorting not required anymore.
\item{08-Dec-2021} Change the CASA - GILDAS interface. Use pure-Python
scripts and put them in the \texttt{\$HOME/.casa} directory to have
a CASA-version independent solution.
\item{07-Dec-2021} A missing initialization was causing \qom{UV\_RESTORE}
to use the slow mode on some compiler versions.
\item{06-Dec-2021} Speed up (by a factor 5-10) the Natural weighting scheme
when there is only 1 channel. 
\item{19-Nov-2021}  Create the \viewer{} program, that only contains
the \lang{DISPLAY} language.
\item{19-Nov-2021} Expand the \qom{COMMAND ? Key} mechanism
to many commands.
\item{18-Nov-2021} Add the \qomm{STATISTIC}{/UPDATE} and\comm{FIT}{/JVM\_FACTOR}
commands
\end{itemize}

