.ec _
.ll 76
.ad b
.in 4
.ti -4
1 Language
.nf
APPLY         : Apply gain solution to current UV Data
COLLECT       : Gather several self-calibration solution together
DERIVE        : Derive Baseline gains from Antenna gains
SCALE__FLUX    : Adjust flux scale on a daily basis
MODEL         : Compute a UV model from the Clean Components Table
SOLVE         : Solve for complex gains using the UV model
TRANSFORM     : Apply transformation on frequency axis
UV__SELECT     : Select UV data to be displayed, imaged or written
UV__SELF       : Build the Self Calibration UV Table and dirty image 
UV__SORT       : Sort and Transpose UV data for plotting
.fi 

.ti -4
1 APPLY
.ti +4
[CALIBRATE_\]APPLY [AMPLI|DELAY|PHASE [Gain]] [/FLAG [Threshold]]

  Apply gain solution computed by MODEL and SOLVE (which
are called implicitely by SELFCAL)  or obtained by READ CGAINS 
to the current UV data. The optional arguments indicate whether this
should be an AMPLITUDE, DELAY or PHASE gain, and what Gain value is used
(in range 0 to 1). A DELAY gain is derived from a Phase self-calibration
assuming the Phase errors represent pathlength changes (i.e. delays):
phase solution is scaled with the Frequency ratio of the self-calibration
and UV data to be calibrated.

  If no argument is given, the current SELF__MODE (see HELP SELFCAL)
is used, and the gain is 1.0.
  
The /FLAG option controls whether data without a valid gain
solution are kept unchanged or flagged. You can actually flag data
on this basis even without applying the self-calibration solution.


.ti -4
2 AMPLI
.ti +4
[CALIBRATE_\]APPLY AMPLI [Gain] [/FLAG [Threshold]]

  Apply an Amplitude gain calibration result. In doing so,
the weights are never up-scaled, even when the Amplitude
correction factor is smaller than 1. They are down-scaled for
visibilities with correction factor larger than 1. The rationale
behind this behaviour is to avoid giving more weights to data
which has been modified in a self-calibration process.  

.ti -4
2 DELAY
.ti +4
[CALIBRATE_\]APPLY DELAY [Gain] [/FLAG [Threshold]]

  Apply an PathLength (geometric delay) gain calibration result. 
The phases are assumed to scale strictly as the frequency. This
mode is in general the most appropriate, since most residual
phase errors are due to the atmospheric pathlength change.

  However, it may produce spurious phase jumps if some instrumental
phase had remained uncalibrated.

.ti -4
2 PHASE
.ti +4
[CALIBRATE_\]APPLY PHASE  [Gain] [/FLAG [Threshold]]

  Apply a Phase gain calibration result. Any possible
frequency dependence of the correction is neglected.

.ti -4
2 /FLAG
.ti +4

[CALIBRATE_\]APPLY [AMPLI|DELAY|PHASE [Gain]] /FLAG [Threshold]

  Apply gain solution (in AMPLITUDE, DELAY or PHASE) and flag data 
without a corresponding valid gain solution. 

  If the Threshold argument is present, all data with a correction
exceeding that threshold are also flagged. This can be useful to
filter out antennas that have unstable gains. A threshold of
90 degrees in phase, or 1.3 in amplitude may be appropriate. 

The optional Gain argument has no effect on the Threshold: it is always
the gain value in the CGAINS array that is compared to the Threshold.

  USEFUL TRICK: If you forgot to flag the data while applying
the self-calibration gain solution, you can still do it a posteriori
without changing the amplitude and/or phase by using command
.ti +8
  APPLY MODE 0 /FLAG Threshold
.ti -8
where MODE is any of AMPLI, DELAY or PHASE. 

.ti -4
1 COLLECT
.ti +4
[CALIBRATE_\]COLLECT  FileFilter MergedFile [ReferenceAntenna] [/FLAG Threshold]

Average self-calibration solutions computed from different frequencies
into a single one,.

  FileFilter is a character string handling a filter to define
all files storing the selected antenna gain solutions (files 
specified by SELF__SNAME in the SELFCAL command). 

  MergedFile is the SNR weighted average of all selected self-calibration
solutions. The averaging of phases is made in DELAY mode, i.e. 
assuming the self-calibration phases are actually Pathlengths (Delays)
at the calibration frequency. 

  The reference antenna can optionally be changed for simpler display 
in this process if argument ReferenceAntenna is present.

.ti -4
2 /FLAG
.ti +4
[CALIBRATE_\]COLLECT  FileFilter MergedFile [ReferenceAntenna] /FLAG Threshold

  If more than 1 self-calibration loop is present in the selected
files, the unstable solutions, defined as those for which the
phase difference between the last 2 iterations exceeds Threshold sigma,
are flagged.

  Note that this option can be used to flag solutions even if only
one frequency was used (i.e. when FileFilter refers to only one file).

.ti -4
1 DERIVE
.ti +4
[CALIBRATE_\]DERIVE AgainsFile [CgainsFile]
  
  Derive Baseline-based (Cgains) solution from an Antenna gain
solution file.  The Baseline-based solution is stored in buffer
CGAINS, unless a file name is given by argument CgainsFile.

.ti -4
2 Example
.ti +4
  Commands COLLECT and DERIVE are typically used to improve
wide-bandwidth self-calibration solutions. At NOEMA, a self-calibration
solution can be computed independently for each of the 4 Wide band 
UV table. Assume these are stored in Wide-'i'-phase-sol.gdf, with i
varying from 1 to 4.
.nf
  COLLECT Wide-phase*.gdf Cont-sol.gdf
  DERIVE  Cont-sol.gdf Cont-cgains.uvt
  READ CGAINS Cont-cgains.uvt
  FOR I 1 to 4
    READ UV Wide-'i'.uvt
    APPLY DELAY /FLAG
    WRITE UV Self-Wide-'i'.uvt
  NEXT
.fi
will average the (antenna-based) Phase calibration solutions (COLLECT)
and compute the corresponding Baseline-based gains (DERIVE).
The loop then applies this better self-calibration solution to 
each UV table, scaling the Phases according to Frequency.


.ti -4
1 SCALE__FLUX
.ti +4
[CALIBRATE_\]SCALE__FLUX Find|Apply|List|Calibrate [Args...]

A set of commands to check flux calibration on a day to day basis.
It gives the ratio between the observed flux (in the current
UV data set) and the model flux for each separate period.  
The Model flux can be derived
from Clean Component Tables using the MODEL command, or read
from an outer file using READ MODEL.

Error bars are approximate. The User-defined command SOLVE__FLUX
performs a more accurate evaluation of the error, but is 
typically 50 times slower.

SCALE__FLUX FIND [DateTolerance [UVmin UVmax]]
.br
determines, by linear regression, the best scaling factor to match date 
by date the UV data set with the MODEL data set. Separate Periods are 
defined when Dates differ more than DateTolerance (default 1 day). Only 
data with baseline lengths in the range UVmin UVmax are considered for 
the regression (default all).

SCALE__FLUX APPLY VarName
.br
apply previously determined flux scale factors to the MODEL data set, 
previously read by READ MODEL. This is in general used only in an 
iterative search way, e.g. by the user-defined command SOLVE__FLUX 
(which calls procedure solve__flux). The resulting UV data set is 
loaded into the specified VarName SIC variable.

SCALE__FLUX LIST
.br
print out dates, baselines and determined flux factors

SCALE__FLUX CALIBRATE
.br
apply previously determined flux scale factors to the current UV data 
set (i.e. divide the visibilities by the scaling factor of each date,
and correct the weight accordingly).
This may then be written using command WRITE UV .

SCALE__FLUX SOLVE [DateTolerance [UVmin UVmax]]
.br
combines FIND and PRINT behaviours.

.ti -4
1 MODEL
.ti +4
[CALIBRATE_\]MODEL [Arg1 [... ArgN] [/MINVAL Value [Unit]] 
  [/MODE CCT|IMAGE|UV__FIT [Frequency]]

  This command creates model visibilities from Clean components, Clean
images or UV__FIT  results from the last UV__FIT command, depending on the /MODE
option. If the option is not present, the default mode is CCT or UV__FIT,
depending whether CLEAN (or its specialized versions HOGBOM, CLARK, 
etc...) or UV__FIT was done last.

  The current UV data is used to define the visibility sampling and data
weights. It should have only one channel, or the same number of channels
as the specified model.

  The resulting UV data is available in SIC structure UV__MODEL. It 
can be written using WRITE UV__MODEL, or selected for further use
using command UV__SELECT.  MODEL and UV__RESIDUAL are 
complementary commands (UV__DATA = UV__MODEL + UV__RESIDUAL).

For the Clean component the syntax is
.ti +4
[CALIBRATE_\]MODEL [MaxIter] [/MINVAL Value [Unit]] [/MODE CCT [Frequency]]
.br
It computes visibilities on the current UV sampling using a source
model made of the MaxIter first Clean Components, or of 
all pixel values above the given Value if /MINVAL is present.

For the Clean image, the syntax is
.ti +4
[CALIBRATE_\]MODEL [ImageName] [/MINVAL Value [Unit]] [/MODE IMAGE]
.br
(ImageName is currently dummy. It will be used to specify any available 
3-D Image variable later.)

For the UV__FIT results, the syntax is
.ti +4
[CALIBRATE_\]MODEL [F1 .. Fn] [/MODE UV__FIT]
.br
which computes a model using the F1, ..., Fn fitted functions 
(default all) for the current UV sampling.

  UV__MODEL can also be selected by using command UV__SELECT
for further imaging by UV__MAP (and thus UV__RESTORE) or display by SHOW UV.

.ti -4
2 /MINVAL
.ti +4
[CALIBRATE_\]MODEL [MaxIter] /MINVAL Value [/MODE CCT [Frequency]] 

For the CCT mode, construct the source model using all Clean Components until
MaxIter (all if MaxIter is 0 or not specified). These components
are stacked on a grid, and then all pixels above the given Value
are taken as source model to derive visibilities.

.ti +4
[CALIBRATE_\]MODEL [ImageName] /MINVAL Value [Unit] /MODE IMAGE [Frequency]

For the IMAGE mode,  all pixels above the given Value
are taken as source model to derive visibilities. An outer band
of 1/8th of the image size is also set to 0 at map edges to avoid
aliasing issues.  Unit can be Jy, mJy, K or sigma. The default value is Jy.

The /MINVAL option is not valid in  UV__FIT mode. 
.ti -4 
2 /MODE 
.ti -4
[CALIBRATE_\]MODEL [Arg] [/MINVAL Value [Unit]] /MODE CCT|IMAGE|UV__FIT [Frequency]

Specify which input data is used to compute the model visibilities: 
Clean Component Table (CCT), Clean image (IMAGE) or UV__FIT results.

By default, the UV__MODEL output UV data takes its Frequency from the input Image or CCT data set. However, this can be 
supersed by the optional argument Frequency, which can take the value IMAGE (the default), UV or any frequency in MHz.

If not specified, the MODE is derived from the last executed operation
(CLEAN or UV__FIT)

.!=======================================================================
.ti -4
1 SOLVE
.ti +4
[CALIBRATE_\]SOLVE Time SNR [Reference] 
      /MODE [Phase|Amplitude] [Antenna|Baseline] [Flag|Keep]

Solve the baseline or antenna based gains using the current UV
data and current MODEL.

Time is the integration time for the solution.
SNR is the minimum Signal to Noise Ratio required to find
a solution. 

.ti -4
2 /MODE
.ti +4
[CALIBRATE_\]SOLVE Time SNR [Reference] 
      /MODE [Phase|Amplitude] [Antenna|Baseline] [Flag|Keep]

Depending on the /MODE arguments, the gains can be antenna-based or 
baseline-based, and include Phase or Amplitude, and data without 
solutions either KEEPed or FLAGged,


.ti -4
1 TRANSFORM
.ti +4
[CALIBRATE_\]TRANSFORM Operation FileIn FileOut [Control]

Apply a transformation along the Frequency axis of a data cube.
Currently recognized Operation values are 
.nf
  FFT     Compute the (complex, hermitian) Fourier Transform
  WAVE    Compute a Wavelet Transform
.fi

FileIn is an Input data cube or the result of a previous
transformation operation.

FileOut is the transformation result. The transformation is reversible:
.br
    TRANSFORM Oper In Out [ControlIn]  followed by 
.br
    TRANSFORM Oper Out In [ControlOut]
.br
re-creates the In file provided the adequate Control[s] are properly
specified.

Input Data Cubes can be in LMV (Position, Position, Velocity)
or VLM format.

Output Files have a format that depends on the Operation. See
HELP TRANSFORM FFT and HELP TRANSFORM WAVE for details.

.ti -4
2 FFT 
.ti +4
[CALIBRATE_\]TRANSFORM FFT  FileIn FileOut [Nchan]

Compute the Fourier Transform of FileIn along the Frequency/Velocity
axis.  The direction of the Fourier Transform depends on the nature
of FileIn.

If FileIn is a standard Data Cube, a direct Fourier transform is
performed, that leads to 4-D cube as FileOut, in order
(position,position,complex,velocity). Its 3rd axis has 2 pixels
that contain the Real and Imaginary parts.
The number of channels is extended to the nearest integer of the
form 2^n 3^p 5^q where p and q are less or equal to 2.

Conversely, if FileIn is such a 4-D cube, an inverse Fourier transform
is performed, leading to a (position,position,velocity) cube as
FileOut. Optionally, Nchan can specify how many channels are actually retained
in this case.


.ti -4
2 WAVE
.ti +4
[CALIBRATE_\]TRANSFORM WAVE  FileIn FileOut Direction

Compute a Wavelet Transform of FileIn along the Frequency/Velocity 
axis. Direction indicates if a direct or inverse Wavelet transformation
is applied.

If Direction = 0, the number of channels is extended to the nearest
power of 2, and a direct wavelet transform is performed

If Direction > 0, an inverse wavelet transform is performed (that
assumes a power of 2 in number of channels in FileIn) and the
first Direction channels only are kept in FileOut.

Thus, for a data cube of Nc channels,  TRANSFORM WAVE In Out 0
followed by TRANSFORM WAVE Out In Nc re-creates the original data.


.ti -4
1 UV__SELF
.ti +4 
[CALIBRATE_\]UV__SELF [CenterX CenterY UNIT [Angle]] 
  [/RANGE [Min Max Type]] [/RESTORE]

Use (and if specified and/or needed create) the "Self Calibrated" 
UV dataset to make a dirty image, instead of using the current
UV table. 

UV__SELF utilizes UV__MAP for imaging. See HELP UV__MAP
for parameters.

.ti -4
2 /RANGE 
.ti +4
[CALIBRATE_\]UV__SELF [CenterX CenterY UNIT [Angle]] /RANGE [Min Max Type]

Create and image the "Self Calibrated" UV data.

The "Self Calibrated" UV dataset is created from the current UV data 
set by extracting the range of channels specified by the /RANGE
arguments Min  Max Type.  Type can be CHANNEL, VELOCITY or FREQUENCY. 
If /RANGE has no argument, all channels are averaged together.

It is then updated by command SOLVE at each self-calibration loop. 
See SOLVE and ADVANCE_\SELFCAL for details.

.ti -4
2 /RESTORE 
.ti +4
[CALIBRATE_\]UV__SELF /RESTORE

As UV__RESTORE but for the self-calibrated UV table.

Restores the Clean image from the Clean Component Table by removing
the components from the Self-calibrated UV data and imaging the
residuals before adding them to the convolved Clean components.

See UV__RESTORE for details.

.ti -4
1 UV__SELECT
.ti +4
[CALIBRATE_\]UV__SELECT [Key]   

  Select which UV data set will be used by commands
UV__MAP, UV__RESTORE, SHOW UV or WRITE UV. Key can be any of following:
.nf

  DATA or UV__DATA           to specify the UV data obtained by READ UV
  MODEL or UV__MODEL         to specify the UV data obtained by READ MODEL,
                            or computed by command MODEL
  RESIDUAL or UV__RESIDUAL   to specify the UV data computed by command
                            UV__FIT

.fi
No other UV related command is affected by UV__SELECT. They all
work on the UV__DATA dataset.

.ti -4
1 UV__SORT
.ti +4
[CALIBRATE_\]UV__SORT Key   [/FILE FileIn FileOut]

Sort and transpose the UV data set. The command has two different
behaviours, depending on the /FILE option.

Without /FILE, the command works on the current UV data, loaded by 
command READ UV File, and creates a transposed, ordered copy of the UV 
data. The Key can be TIME for Time-Baseline ordering, BASE for 
Baseline-Time ordering. The sorted UV data is then available in 
variable UVS for further plotting. This is only done in an internal 
buffer. WRITE UV will **NOT** write this sorted, transposed, buffer.

With the /FILE option, the command creates in FileOut a sorted 
(but not transposed) copy of the UV data file specified in FileIn.

.ti -4
2 /FILE
.ti +4
[CALIBRATE_\]UV__SORT Key /FILE FileIn FileOut

Creates in Fileout a sorted copy of the UV data found in FileIn.
Key can be The Key can be TIME for Time-Baseline ordering, BASE for 
Baseline-Time ordering, or FIELDS for Mosaics. TIME ordering
is required for efficient operation of the time averaging 
command UV__TIME.

The command compares the file size to the available RAM memory
(more specifically the size indicated by the logical name SPACE__GILDAS) 
to decide whether the operation can be done in Memory only or needs 
intermediate files to perform the sorting.



.ti -4
1 ENDOFHELP
