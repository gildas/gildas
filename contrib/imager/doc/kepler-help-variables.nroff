
.ti -4
2 KEPLER__X0
.ti +4
  X offset of disk center (in arcsec)
.ti -4
2 KEPLER__Y0
.ti +4
  Y offset of disk center (in arcsec)
.ti -4
2 KEPLER__ROTA
.ti +4
  Position angle of the projection of the disk rotation axis 
(in degree, East from North)
.ti -4
2 KEPLER__INCLI
.ti +4
  Disk inclination (in degree)
.ti -4
2 KEPLER__DIST
.ti +4
  Disk distance (in pc).
.ti -4
2 KEPLER__VMASS
.ti +4
  Disk rotation velocity at 100 au in km/s. Should be equal
  to 2.98 * sqrt(M*/Msun) from Kepler laws.
.ti -4
2 KEPLER__RMIN
.ti +4
  Inner radius for the Radius-Velocity diagram (in au).
.ti -4
2 KEPLER__RMAX
.ti +4
  Outer radius for the Radius-Velocity diagram (in au).
  KEPLER__RMAX must be greater then KEPLER__ROUT.

  KEPLER__RMAX is an optional variable. By default (i.e. 
  if the variable has not been created by the user), 
  the maximum radius is taken as KEPLER__ROUT + 5*KEPLER__STEP.
.ti -4
2 KEPLER__RINT
.ti +4
  Inner radius (in au) for the integrated spectrum, computed by
  summing up spectra from KEPLER__RINT to KEPLER__ROUT.
.ti -4
2 KEPLER__ROUT
.ti +4
  Outer radius (in au) for the integrated , computed by
  summing up spectra from KEPLER__RINT to KEPLER__ROUT.
.ti -4
2 KEPLER__STEP
.ti +4
  Sampling step in au. Must be compatible with the angular resolution.
  Note that the spectral resolution allows some gain in spatial
  resolution compared to the beam size.
.ti -4
2 KEPLER__THETA
.ti +4
  Maximum angle (in degree) from the disk projected major axis beyond 
which the spectra are not considered in the averaging process.  

This angle is needed because  the projected rotation velocity is degenerate
to zero along the minor axis, which results in a lower effective
angular resolution in this direction. This effect depends on the
disk inclination: a larger angle can be used for less inclined disks.

The default is 60 degree. 
The Azimut coverage can be visualized using command SHOW KEPLER__VELO.

.ti -4
2 KEPLER__AZIMUT
.ti +4
  KEPLER___AZIMUT[1:2] indicates which range of Azimut is retained
in the computation (Azimut Zero corresponds to the oriented minor axis,
i.e. is at KEPLER__ROTA in the sky plane).  

For example 90 270 indicates Az between 90 and 270 degrees, while
270 90 indicates As from 0 to 90 and 270 to 360.

The Azimut coverage can be visualized using command SHOW KEPLER__VELO.

.ti -4
2 KEPLER__VDISK
.ti +4
  KEPLER__VDISK is a variable that indicates the disk systemic velocity (in km/s).
The value is always used to define the velocity field (see KEPLER__VELO
result variable). 

  However, for the radial profile, the usage depends on the /VSYSTEM option. 
When /VSYSTEM is not present, the radial profile is taken as the maximum
brightness of the integrated, velocity aligned, spectrum for
at each radius. When /VSYSTEM option is present, the radial
profile is taken at the velocity KEPLER__VDISK.

.ti -4
2 KEPLER__STRICT
.ti +4
	KEPLER__STRICT is an optional (user-created) logical variable that
specifies whether a spectrum is computed or not when the full velocity
range is not covered. Default is NO, i.e. spectra not covering
the full range are not flagged.

.ti -4
2 Results:
.ti +4
  The KEPLER command produces 2 SIC Tables,  
KEPLER__SPECTRUM, and KEPLER__PROFILE, and two SIC 2-D Images, 
KEPLER__PV and KEPLER__VELO that are available as SIC variables
for further writing or plotting.

  Command SHOW KEPLER Name (where KEPLER__Name is any of the above
variable names) can be used to graphically display these variables.
SHOW KEPLER ALL will display a combined plot. The SHOW KEPLER command
behaviour is controlled by the variables in structure KEPLER__SHOW%.

  SHOW KEPLER SPECTRUM and SHOW KEPLER ALL also perform a Gaussian fit into
the integrated line profile to derive the integrated flux, the disk
systemic velocity (if the option /VSYSTEM was not present in the
KEPLER command) and the line width.  


.ti -4
2 KEPLER__PROFILE
.ti +4
  Radial profile of the peak brightness temperature. The peak brightness
temperature is that appearing at KEPLER__VDISK if /VDISK option was present, or at the
velocity defined by the maximum of the integrated spectrum. This is
a 3 column  table containing the radii (in au), the brightness (in K)
and an estimate of its error.

.ti -4
2 KEPLER__PV
.ti +4
  Velocity-aligned spectra as a function of radius. This is a 2-D image
(equivalent to a Nrad column table) containing the spectra (averaged 
brightness temperature) for each of the Nrad radii defined by the 
sampling KEPLER__RINT, KEPLER__RMAX and KEPLER__STEP.

  The velocities are defined in KEPLER__SPECTRUM[1], but also derived
from the axis description of this 2-D image.

.ti -4
2 KEPLER__SPECTRUM
.ti +4
  Velocity-aligned, disk-integrated spectrum, over the region defined
by KEPLER__RINT, KEPLER__ROUT and KEPLER__THETA. This is a 3 column
table containing the velocities (km/s) in column 1, the flux in
column 2, and an estimate of the flux error in column 3.
  
.ti -4
2 KEPLER__VELO
.ti +4
  Velocity field in the region retained by the KEPLER command. This contains
the line-of-sight projected velocity offset from the (assumed)
disk systemic velocity KEPLER__VDISK.

.ti -4
2 Display:
.ti +4
   The results of the KEPLER command can be displayed in several ways
by command KEPLER SHOW.  The command behaviour is controlled by variables
in the structure KEPLER__SHOW%.
 
.ti -4
2 KEPLER__SHOW
.ti +4
  KEPLER__SHOW% is a Structure variable controlling how the display of the 
KEPLER SHOW (or SHOW KEPLER) command is handled. It contains the following
variables, that can be listed by KEPLER SHOW ?
 
.nf
  Show Mean Spectrum     KEPLER__SHOW%SPEC       [ NO ]
  Show Radial Profile    KEPLER__SHOW%PROF       [ NO ]
  Show PV diagram        KEPLER__SHOW%PV         [ NO ]
  Select compact layout  KEPLER__SHOW%LAYOUT     [ NO ]
 
  Velocity    Range      KEPLER__SHOW%V          [ 0 0 ]
  Radius      Range      KEPLER__SHOW%R          [ 0 0 ]
  Temperature Range      KEPLER__SHOW%T          [ 0 0 ]
  Flux        Range      KEPLER__SHOW%F          [ 0 0 ]
.fi

.ti -4 
2 KEPLER__SHOW%V
.ti +4
	Velocity range for the displays (R-V diagram and Spectrum), in km/s.
.ti -4 
2 KEPLER__SHOW%R
.ti +4
	Radius range for the displays (R-V diagram and Radial Profile),
	in au.
.ti -4 
2 KEPLER__SHOW%T
.ti +4
	Temperature range for the display (R-V diagram), in K 
.ti -4 
2 KEPLER__SHOW%F
.ti +4
	Flux range for the displays (Spectrum), in Jy.
	
.ti -4 
2 KEPLER__SHOW%LAYOUT
.ti +4
	If YES, use a compact layout. If NO use layout with equal panel sizes.
	This only applies to KEPLER SHOW ALL.
	
	
.!======================================================================  
