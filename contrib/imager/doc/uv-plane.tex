\section{UV plane analysis}
\label{sec:uvplane}

\imager{} is not only a tool to produce images. It also allows
direct analysis by model fitting to the UV data. 

A number of commands are directly related to these possibilities:
\begin{itemize}\itemsep 0pt
\item \comm{READ}{UV} 
\item \comm{READ}{MODEL} 
\item \comm{SHOW}{UV\_FIT}
\item \comm{SHOW}{UV}
\item \com{UV\_CIRCLE}
\item \com{UV\_CONTINUUM}
\item \com{UV\_DEPROJECT}
\item \com{UV\_FIT}
\item \com{UV\_RESIDUAL}
\item \com{UV\_RADIAL}
\item \com{UV\_SELECT}
\end{itemize}

The overall intent of these commands is described below. Please
refer to the corresponding HELP for more details about the
command syntax, options and control values.

\begin{itemize}\itemsep 0pt
\item \com{UV\_FIT} is the primary fitting command. It allows to
adjust simple, analytic models of source brightness to the 
current UV data. Up to 8 different source models can be combined
in a single fit \footnote{this is probably too much, and would most likely
lead to instability in the model fitting !..}.

The UV\_FIT results can be displayed using command \comm{SHOW}{UV\_FIT}.
The visibilities modeled by UV\_FIT (or the fit residuals) can be 
displayed using command \comm{SHOW}{UV}, either alone, or superimposed
to the current UV data.

\com{UV\_FIT}  works on spectral line data. To optimally fit continuum
data, it is best to convert the spectrally resolved information into
a \textit{bandwidth synthesis continuum} \uv{} table.

\item \com{UV\_CONTINUUM} converts a spectral line \uv{} table into a 
\textit{bandwidth synthesis continuum} \uv{} table. \com{UV\_CONTINUUM} 
requires some knowledge of the field of view to evaluate how many 
channels should be averaged together. This is done using the same 
parameters (\sicvar{MAP\_FIELD}, or the product of \sicvar{MAP\_SIZE} 
by \sicvar{MAP\_CELL}) and subroutines as for commands 
\comm{UV\_STAT}{SETUP} and \com{UV\_MAP}.

  The \textit{bandwidth synthesis continuum} \uv{} table is peculiar
in the sense that it has several baselines for the same pair of antennas
at the same observing time: one baseline per independent frequency point,
in order to preserve optimally the angular resolution that goes as 
$\lambda/B$. In practice, it is only useful for \com{UV\_FIT}, but
not suitable for self-calibration for example.

\item \comm{SHOW}{UV} will display the best fit visibilities (stored
in the \sicvar{UV\_MODEL} data set) if (and only if)
the \sicvar{UVSHOW\%FIT} variable is set to \texttt{MODEL, POINT} or \texttt{CURVE}.
When \sicvar{UVSHOW\%FIT} variable is set to \texttt{MODEL}, the \sicvar{UV\_MODEL}
data set is assumed to exist, and not re-computed. It is displayed
as data points (not as a curve).

\item \comm{SHOW}{UV\_FIT} is intended to display the fit results
(the best fit parameter values and their errors) as a function of
channels (or velocity, or frequency). This is often a nice way to
show the detection of a signal when the signal to noise is limited,
but of course is useless for single channel data !

\item \comm{READ}{MODEL} can import the visibilities from an outside
model, so that these can be overlaid to the current UV data 
by \comm{SHOW}{UV} with \sicvar{UVSHOW\%FIT} variable set to \texttt{MODEL}.

\item Command \com{MODEL} computes the visibilities (the \sicvar{UV\_MODEL} 
dataset) for the best fit parameters found by \com{UV\_FIT} \textbf{or} 
for the selected Clean components, depending whether \com{UV\_FIT} or 
\com{CLEAN} was used last.

\item Similarly to command \qom{MODEL}, \com{UV\_RESIDUAL} computes 
the residual visibilities (the \sicvar{UV\_RESIDUAL} data set).

\item Commands \com{UV\_CIRCLE} \com{UV\_DEPROJECT} \com{UV\_RADIAL}
can be useful in conjunction to \com{UV\_FIT}, as they can provide
a (deprojected, optionally azimutally averaged) visibility data set
which can the be much more easily compared to simple, circularly
symmetric models.

  Beware that the convention for \textit{angle} are different
in these commands (that use the PA of the axis) and \comm{UV\_FIT}
that use the PA of the major axis: they thus differ by 90 degrees.
  
\item Finally, command \com{UV\_SELECT} allow to select which data set
is displayed by \comm{SHOW}{UV} among the UV data, the \sicvar{UV\_MODEL}
or the \sicvar{UV\_RESIDUAL}.
\end{itemize}

Try \com{@ }\file{gag\_demo:demo-uvfit} for some examples.
