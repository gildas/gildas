\section{Visual Checks and Image Displays}
\label{sec:visual}

The ultimate (and often sole) way to evaluate the data quality
and suitability for scientific analysis is to visualize it.
\imager{} offer simple, yet powerfull, tools to do so.

Most visualization (or related) commands are grouped in
language \lang{Display} (which may be used alone to
provided a simple stand-alone data cube visualisation tool.). 

\subsection{The \viewer{} program}

The \viewer{} program is a subset of \imager{} that only contains the
\lang{Display} language. It can be used to provide visualisation
of any GILDAS data cube. Besides visualisation commands that
are described in the sub-sections, \viewer{} includes the \com{CATALOG}
(and its related \com{FIND} command) that controls the behaviour
of some displays, as well as generic support commands like
\com{STATISTIC}


\subsubsection{The \texttt{SHOW} command}

In general, command \com{SHOW} allows a per-plane display of any SIC 
3-D image variable, with contours overlaid on bitmap for each plane: 
e.g.\\
\texttt{SHOW DIRTY 30 -30} 
will display contour and bitmaps of each channel of the \texttt{DIRTY}
image, starting for channel 30 and ending 30 channels before the last one.
See Fig.\ref{fig:show} for an example. 
\begin{figure}
  \centering
  \includegraphics[width=16cm]{show.png}
  \caption{The \comm{SHOW}{CLEAN}  output.
\label{fig:show}}
\end{figure}
Command \comm{SHOW}{/SIDE} (and also command \qom{SHOW} when the
\sicvar{SHOW\_SIDE} variable is set to \texttt{YES}) will
call the cursor, so that the pixel values
at the cursor position are displayed in the \texttt{<VSIDE} panel. 

A direct use on Gildas 3-D image or UV data files is also possible:
\qom{SHOW}\texttt{ Filename.ext} will directly display the
file if possible. It also works for simple FITS files in which
the data array is in the HDU.

For \uv{}  data, \comm{SHOW}{UV} can plot visibility values
such as amplitude as a function of time, baseline length, etc...,
again on a per channel basis. Fit results can be overlaid, as
shown Fig.\ref{fig:showuv}
\begin{figure}
  \centering
  \includegraphics[width=14cm]{showuv-fit.png}
  \caption{The \comm{SHOW}{UV} output with results from \com{UV\_FIT}
  command overlaid.
\label{fig:showuv}}
\end{figure}

\qom{SHOW} can also display more specific issues:
\begin{itemize}\itemsep 0pt
\item \comm{SHOW}{CCT} will display the cumulative flux as a function 
of number of clean components (Fig.\ref{fig:cct}). 
\begin{figure}
  \centering
  \includegraphics[width=14cm]{show-cct.png}
  \caption{The \comm{SHOW}{CCT} output.
\label{fig:cct}}
\end{figure}
\item \qomm{SHOW}{COVERAGE} will display the \uv{} coverage (it
assumes there is only one, and not one per channel, because the
display time is long, see Fig.\ref{fig:coverage})
\begin{figure}
  \centering
  \includegraphics[width=14cm]{coverage.png}
  \caption{The \comm{SHOW}{COVERAGE} output. Colors indicate
  different dates.
\label{fig:coverage}}
\end{figure}
\item \comm{SHOW}{SELFCAL} behaves as \comm{SELFCAL}{SHOW}
\item \comm{SHOW}{FIELDS} displays the fields of a Mosaic.
\item \comm{SHOW}{NOISE} displays the histogram of the intensity
distribution for each channel, estimating the noise by fitting
a Gaussian in these histograms (see Fig.\ref{fig:noise}).
\begin{figure}
  \centering
  \includegraphics[width=14cm]{show-noise.png}
  \caption{The \comm{SHOW}{NOISE} output.
\label{fig:noise}}
\end{figure}
\item \comm{SHOW}{UV\_FIT} displays the \comm{UV\_FIT} results
for spectral line data. 
\begin{figure}
  \centering
  \includegraphics[width=16cm]{show-uv_fit.png}
  \caption{The \comm{SHOW}{UV\_FIT} output.
\label{fig:uvfits}}
\end{figure}

\end{itemize}

\qom{SHOW} recognizes many other keywords. See \qomm{HELP}{SHOW} for
further details.

\clearpage

\subsubsection{the \texttt{VIEW} command}

The \com{VIEW} command is a powerful alternative to \qom{SHOW}, the
later being inefficient when the number of spectral line channels
is large. 

\qom{VIEW} provides a 4 panel display for 3-D data cubes, with
the current channel bitmap, the integrated area bitmap, the current
spectrum and the integrated intensity spectrum.  The spectra
can be displayed with 2 simultaneous frequency windows, a broad
and a zoomed one, allowing browsing through a large number of
channels. Spectral line identification  (from the line catalog
specified using command \com{CATALOG}) can be added by typing
L when the cursor is on one of the 2 broad-band spectra.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=16cm]{view.png}
  \caption{The \com{VIEW} output.
\label{fig:view}}
\end{figure}

\comm{VIEW}{CCT} will display the cumulative flux of Clean components
for all channels in just one panel, instead of a per-channel panel
for \comm{SHOW}{CCT}
\begin{figure}[!ht]
  \centering
  \includegraphics[width=16cm]{view-cct.png}
  \caption{The \qomm{VIEW}{CCT} output.
\label{fig:viewcct}}
\end{figure}

Similarly, \comm{VIEW}{NOISE} will plot all pixel intensity distributions
from every plane in a single panel, contrarary to \qomm{SHOW}{NOISE}.

\comm{VIEW}{/OVERLAY} allows to overlay (in contours) the plane
of a second data cube. The main use is to overlay the continuum, or
a plane of the current data cube to help revealing velocity structures.

\qom{VIEW} will fallback to \qom{SHOW} whenever it has no specific
support for a given action.

Like \qom{SHOW}, \qomm{VIEW}{/NOPAUSE} will not loop
interactively to explore the data cube, but just display the view
with its current parameters (channel, velocity range, support). The same
behaviour can be obtained by setting \sicvar{DO\_LOOP} to \texttt{NO}.



\clearpage
\subsubsection{the \texttt{INSPECT\_3D} command}

Another alternative to \qom{SHOW} or \qom{VIEW} is the \com{INSPECT\_3D}
command, which presents cuts along the 3 main directions of the data
cube (xy, xv, vy).  Like \qomm{SHOW}{/SIDE} and \qom{VIEW}, \qom{INSPECT\_3D}
has a \texttt{<VSIDE} display panel where data values at current
position are displayed. 

\begin{figure}[!hb]
  \centering
  \includegraphics[width=16cm]{inspect.png}
  \caption{An \com{INSPECT\_3D} output. Cursor controls the coordinates
  of the 3 orthogonal cuts and spectrum.
\label{fig:inspect}}
\end{figure}


\clearpage
\subsubsection{the \texttt{EXPLORE} command}

The latest alternative to \qom{SHOW}, \qom{VIEW} or \qom{INSPECT\_3D}
is the \qom{EXPLORE} command, which presents spectra extracted
from a datacube around an image of one velocity channel or of
the velocity integrated area. 

\begin{figure}[!hb]
  \centering
  \includegraphics[width=16cm]{explore.png}
  \caption{A sample \com{EXPLORE} output. Up to 8 spectra can be displayed
  around the central image.
\label{fig:explore}}
\end{figure}

\clearpage
\subsection{Specific \imager{} visualisation tools}
In addition to the generic tools offered in the \lang{Display} language,
\imager{} contains commands that are more specific to the UV data
handling or imaging process, or to advanced analysis commands. The former
category includes \qom{UV\_PREVIEW}, as well as \qomm{SELFCAL}{SHOW},
while an example of advanced specific display command is \qomm{KEPLER}{SHOW}

Like \qom{SELFCAL}, some other commands (e.g. \comm{KEPLER}{SHOW}) have a \texttt{SHOW}
mode. In general, \com{SHOW} will then recognize the reverse syntax, \comm{SHOW}{KEPLER}
in the above example.

Use \comm{HELP}{SHOW} for more details, as the Help is in general more up-to-date than the
compiled documentation which is more intended to document general features.


\subsubsection{The \texttt{UV\_PREVIEW} command}
With large datasets, imaging can be long. \imager{} offers a
simple, fast pre-imaging viewer through command \com{UV\_PREVIEW}.

This command will display the spectra obtained towards the
phase center at several spatial scales (the default is for 4 tapers).
It will also attempt to detect spectral features, by analyzing for
each spectrum the noise statistics and the outliers. It performs
automatic line identifications, using database(s) in the \lang{LINEDB}
or \astro{} data format selected by command \com{CATALOG}.
Potential spectral lines in the band are displayed in blue, and
detected ones in red.
\qom{UV\_PREVIEW} also warns the user about improper scaling
of the data weights.
The line emission region is indicated in grey.

The result of this automatic signal detection and line identification
is saved in a SIC structure named \sicvar{PREVIEW\%}, that is 
automatically used by commands \comm{UV\_BASELINE}{/CHANNELS} and
\comm{UV\_FILTER}{/CHANNELS} to respectively remove the continuum and
filter the line emission to produce a continuum data set. 
The detected line frequencies are stored in
 \sicvar{PREVIEW\%FOUND\%FREQ} and their names in
\sicvar{PREVIEW\%FOUND\%LINES}. This can be used to reference
the velocity scale of the data to one of the detected lines, by
using command \comm{SPECIFY}{FREQUENCY} \texttt{'PREVIEW\%FOUND\%FREQ[1]'} 
for example before further processing.

An example is shown in Fig\ref{fig:preview}
\begin{figure}[!h]
  \centering
  \includegraphics[width=16cm]{preview.png}
  \caption{The \com{UV\_PREVIEW} output
\label{fig:preview}}
\end{figure}



\clearpage
\subsubsection{the \texttt{SELFCAL SHOW} command}

Verifying the convergence of a self-calibration is important.
Figure \ref{fig:selfphase} shows an example, while Fig.\ref{fig:selftot}
shows the total phase correction between the original data and
the last iteration. 
\begin{figure}[!hb]
  \centering
  \includegraphics[width=12cm]{self-phase.png}
  \caption{The \comm{SELFCAL}{SHOW} output after a phase calibration,
  showing the convergence of the corrections between the last 2
  iterations.
\label{fig:selfphase}}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=12cm]{self-phase-total.png}
  \caption{The \comm{SELFCAL}{SHOW} 4 output after a phase calibration,
  showing the difference between iteration 4 and the original
  data.  
\label{fig:selftot}}
\end{figure}

The displayed range can be controlled
by variables \sicvar{SELF\_PRANGE[2]} for the Phase, 
\sicvar{SELF\_ARANGE[2]} for the Amplitude, and \sicvar{SELF\_TRANGE[2]} 
for the Time. Error bars are displayed if \sicvar{ERROR\_BARS} is \texttt{YES},
as shown in Fig.\ref{fig:selfamp} for amplitude.
\begin{figure}
  \centering
  \includegraphics[width=12cm]{self-ampli-total.png}
  \caption{The \comm{SELFCAL}{SHOW} 4 output after an amplitude self calibration,
  showing the difference between iteration 4 and the original
  data, with the error bars.  
\label{fig:selfamp}}
\end{figure}

