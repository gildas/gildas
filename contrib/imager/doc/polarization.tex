\section{Polarization Handling}
\label{sec:polar}

\imager{} recognizes and handles polarization at different levels.
Although support for polarization is basically complete for continuum data,
it remains \textbf{experimental, and is continuously improving}. 

\textbf{Contact the IMAGER authors if you need to analyze Polarized data, using 
 Mail to: \texttt{imager-hotline@services.cnrs.fr}}

\subsection{Data Handling}

\textbf{Importing data}\\
\indent When importing data, the \script{fits\_to\_uvt} script assumes by default
the data is unpolarized and produces the pseudo-polarisation
state "None" from the UVFITS file, by a properly weighted combination
of the two parallel hand states if more than one state is present. The
weighting is based on the relative noise in both polarization states.

Full polarization information can be preserved by adding the 
\texttt{\color{magenta} /STOKES} option to the \qom{@ fits\_to\_uvt} command. 

The \qomm{READ}{UV} command will read data with any
polarization state(s).

\vspace{0.3cm}\noindent \textbf{Processing data}\\
\indent On the contrary, practically all \imager{} commands cannot handle 
more than 1 polarization state. So far, only two commands fully support polarized
data: \com{UV\_TIME} and \com{STOKES}.

\begin{itemize}
\item \com{STOKES} is the primary command that allows to derive or extract 
a UV data with only one Stokes parameter from a UV data set with 
several polarization states. \imager{} can then process the 
individual Stokes parameters separately.
\item For convenience (because polarized data is obviously in general
bigger), the \com{UV\_TIME} command can be used for time averaging
prior to use of the \com{STOKES} command.
\item Most other commands will flatly refuse to handle data with more than 
1 polarization state (e.g. \com{UV\_FILTER}, \com{UV\_RESAMPLE}, etc\ldots).
\item For debugging purpose, some commands like \com{UV\_PREVIEW}, \com{UV\_MAP}
or \com{UV\_STAT} will operate with more than 1 polarization state,
but will not produce meaningful results (only a subset of the data
may be treated).
\end{itemize}

Some imaging strategies cannot be used directly on polarized data. 
Also, the automatic definition of supports (\comm{MASK}{/THRESHOLD}) 
only makes sense on Stokes I or parallel hand polarization states, not
on Stokes Q,U,V or cross-hand states. The same applies to spectral
line identifications.

\vspace{0.3cm}\noindent\textbf{PIPELINE processing}
The PIPELINE tools does not automatically handle polarization data.
However, through judicious use of the \comm{PIPELINE}{SELECT}
and \comm{PIPELINE}{FIND} commands with ad-hoc filters, imaging,
including self-calibration, is possible through custom (though simple) 
scripts.

The script \script{self-polar.ima} just does that, and follows the naming
conventions used by the \qom{MAP\_POLAR} command. It contains the following commands
\begin{verbatim}
  CATALOG               ! No catalog, no Line emission
  ! "Split the X+Y (XX,YY,XY,YX) UVFITS file into I,Q,U,V Stokes UV tables"
  @ stokes-split &1     
  ! Setup the pipeline
  IMAGER\PIPELINE ?     
  IMAGER\PIPELINE organize        !  " Move UV tables to RAW/"
  sic mkdir UVFITS
  SIC\SYSTEM " mv *.uvfits UVFITS/"
  SIC\SYSTEM " mv RAW/&1.uvt .."  !   "Remove the X+Y UV table from RAW/"
  !
  ! Select the I image for self-calibration
  let all%filter *-I
  IMAGER\PIPELINE select
  IMAGER\PIPELINE compute
  !
  ! Apply self-calibration solution to I,Q,U,V data
  let all%filter *
  IMAGER\PIPELINE find
  IMAGER\PIPELINE select
  IMAGER\PIPELINE collect
  IMAGER\PIPELINE apply
  ! Image every thing consistently
  IMAGER\PIPELINE image
  PIPELINE SAVE
\end{verbatim}

 
\vspace{0.3cm}\noindent\textbf{Analysing data}\\
\indent The \com{MAP\_POLAR} command allows to derive images
of the Polarized intensity and/or fraction and Polarization angle,
as well as display of the polarization vectors onto a background image.
So far, the command only works for a single plane image.

The \com{MAP\_POLAR} command works through data files that follow
conventional naming rules to identify them. The implicit rule is
that Stokes S (where S is any of I,Q,U,V) is stored in a file
of name 'NameBegin'"-N"'NameEnd', e.g. if 'NameBegin' is \texttt{My\_Data}
\begin{verbatim}
    My_Data-I+C.uvt,  My_Data-U+C.lmv-clean, etc..
\end{verbatim}
  
 
\subsection{The STOKES command}

The \com{STOKES} command operates on the current UV buffer, or on files
if the \qom{/FILE} option is present. It allows
to extract a UV data with visibilities for one output (pseudo-)Stokes 
parameter from UV data with visibilities with 1,2 or 4 (pseudo-)Stokes
parameters. Besides the standard Stokes parameters I, Q, U, V, RR, 
LL, RL, LR (Left and Right circular), XX, YY, XY and YX (X and Y
linear) which are defined in the Sky frame\footnote{ultimately, a similar DD, SS, DS and SD pseudo-Stokes polarization set
may be added for circular polarization. D is the first letter for
Dexter, the Latin name for Right, while S is the
first letter of Senester, the Latin name for Left.},
command \com{STOKES} recognizes pseudo-Stokes values NONE, HH, VV,
HV and VH which are the linear polarization states in the frame
of the antennas (Horizontal and Vertical pure states).


Conversion from the H-V pseudo-Stokes polarization states to any standard
Stokes parameter is made by the \com{STOKES} command by applying 
the rotation due to the parallactic angle. For this, the UV data set
must contain the \texttt{PARA\_ANGLE} extra column. If it is not present,
it can be added to the data set by command \comm{UV\_ADD}{/FILE}.
That command can also insert the Doppler correction as an extra
column.


Script \script{stokes-split} can be used to split a full polarization
UV table in 4 UV tables, one for each IQUV Stokes parameter, following
the standard naming convention expected for final image use by  \com{MAP\_POLAR}.
