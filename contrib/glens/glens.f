!
! This program is optimized for quadratic input fields only.
!
      PROGRAM GL
      USE GILDAS_DEF
      USE GKERNEL_INTERFACES
!
      LOGICAL IN,INSIDE,FLA,FILE,CAUYES,DELAYYES
      INTEGER NMAX                       ! Number of segments
      INTEGER DIM,QS,QT,IC,MODEL,MAXPIX,MAXPX,DOCAU,MM,MAXTIME
      INTEGER I,J,K,L,S,A,F,T,G,INT,NDIM,IP,GMI,GMA,FMI,FMA,NSI
      INTEGER MINLIMF,MAXLIMF,MINLIMG,MAXLIMG
      PARAMETER (DIM = 128, MAXPX = 65536, NMAX = 4,MAXTIME = 2048)
      REAL*8 MA(-DIM:DIM,-DIM:DIM),Z(2)
      REAL*8 LE(-DIM:DIM,-DIM:DIM),QE(-DIM:DIM,-DIM:DIM),STEP,AL,RAD
      REAL*8 CSCL,SSCL,WAAG(-DIM:DIM,-DIM:DIM),SENK(-DIM:DIM,-DIM:DIM)
      REAL*8 X(-DIM:DIM,NMAX),GONS(NMAX+1,4),BOUND(4),DF(8),OLDMA
      REAL*8 NEWMA,AS0,SIGMAV,ALPHAE,PHIG,CSG,SSG,YTK,MAXK,KX,KY
      REAL*8 Y(-DIM:DIM,NMAX),DS,DDS,DD,M,DE,XA(-DIM:DIM,NMAX+1)
      REAL*8 YA(-DIM:DIM,NMAX+1),PI,D,D0,D1,D2,SQE,SLE,SEC,FWHM1
      REAL*8 FWHM2,ZS,ZL,FRACTION,VF,VG,XS,YS,Q,O,HUBBLE
      REAL*8 V(MAXPX),W(MAXPX),GC,C,FAC0,FAC1,FAC2,FAC3,F1,F2,INTEN
      REAL*8 GPC,MBH,XSA,YTA,EPS,CORE,COR,PA,CSP,SSP,R,STPSC,ALBH
      REAL*8 ALBH0,ALX,ALY,DX,DY,CLI(2,6),GS,INTENXA,INTENYA
      REAL*8 XSH,YSH,XSAP,YTAP,OM,OR,OV,NS,A1,A2,B1,B2,C1,CAU(2,6)
      REAL*8 S_SIZE,T_SIZE,LA,LB,AP1,AP2,V3,JET1,JET2,INTENK
      REAL*8 ETA,ALCL,BCL,DCL,XCL,YCL,TCL1,TCL2,SIGMACL,GAMMA0,GAMMA
      REAL*8 XSAG,YTAG,XSAQ,YTAQ,CSQ,SSQ,ALXP,ALYP,CSPI,SSPI,XSB,YTB
      REAL*8 INTENXB,INTENYB,MXA,MXB,MYA,MYB,DELTAP2,DELTAPK,LK,XSK
      REAL*8 MAXA,MAXB,DELTAINT,DELTAP,INTENA,INTENB,AX,AY,BX,BY
      REAL*8 TIMED,PSI,TIMEDA,TIMEDB,XSBP,YTBP,PSI2(4),XYM,ALXAB(2)
      REAL*8 ALYAB(2),YI,XI,Y0,XYAB(4),DTIME,PSI0,TIMED2,JETP1,JETP2
      REAL*8 JE(-DIM:DIM,-DIM:DIM),OMEGA(3)
      REAL*8 JE2(-DIM:DIM,-DIM:DIM),JE3(-DIM:DIM,-DIM:DIM)
      REAL*8 V4,V5,XS2,XS3,YS2,YS3,JETP3,JETP4
      REAL*8 JETP5,JETP6,FWHMJ21,FWHMJ22,FWHMJ31,FWHMJ32,FWHMJ41
      REAL*8 FWHMJ42,XS4,YS4,JETP7,JETP8
      REAL*8 JE4(-DIM:DIM,-DIM:DIM),V6,V7,JE5(-DIM:DIM,-DIM:DIM)
      REAL*8 FWHMJ51,FWHMJ52,XS5,YS5,JETP9,JETP10, XYSH(2)
      REAL*8 JE6(-DIM:DIM,-DIM:DIM),JE7(-DIM:DIM,-DIM:DIM),FWHMJ61
      REAL*8 FWHMJ62,XS6,YS6,JETP11,JETP12,FWHMJ1,FWHMJ2, FWHM(2)
      REAL*8 FWHMJ71,FWHMJ72,XS7,YS7,JETP13,JETP14,V8,SS(4),NORM
      REAL*8 SVL1,SVL2,SVL3,SVL4,SVL5,SVL6,SVC7,SVC8,TCL7(2),TCL8(2)
      REAL*8 SVL7,SVL8,ELLIP2,ELLIP5,ELLIP6,ELLIP7,ELLIP8,GAMMA3
      REAL*8 CORE2,CORE3,CORE4,CORE5,CORE6,CORE7,CORE8,PA2,PA3
      REAL*8 PA5,PA6,PA7,PA8,GAMMA4,GAMMA5,GAMMA6,GAMMA8,PHIG3
      REAL*8 PHIG4,PHIG5,PHIG6,PHIG8,MBH6,MBH7,ETA3,ETA5,ETA8,MBH4
      CHARACTER TEXT*40,LENSMOD(9)*10,LENSMODEL*10
      INTEGER SA,SB,TA,TB,NJ,MJ,IZ,NJ1,MJ1,SK,TK,TEST,DELAY
      REAL*8 JET,AJ,BJ,CSJ1,CSJ2,SSJ1,SSJ2,NJ0,MJ0,MJ01,NJ01
      DATA LENSMOD /'POINT','SIS','SIE','SPLS','FGS','SPEMD','FGSE',
     &'FGSE+CL','SPEMD+CL'/
      COMMON /COSMO/ HUBBLE,GPC,C,Q,O,OV,OM,OR,K
!
!
      CALL GILDAS_OPEN
      CALL GILDAS_CHAR ('LENSMODEL$',LENSMODEL)
      MODEL = -1
      DO I=0,8
         IF (LENSMODEL(1:2).EQ.'M'//CHAR(I+48)) MODEL = I
      ENDDO
      IF (MODEL.EQ.-1) THEN
         CALL GAGOUT ('F-GL, model = '//LENSMODEL)
         CALL GAGOUT ('F-GL, not a valid model')
         CALL SYSEXI (FATALE)
      ENDIF
      CALL GILDAS_DBLE ('HUBBLE$',HUBBLE,1)
      CALL GILDAS_DBLE ('OMEGA$',OMEGA,3)
      OM = OMEGA(1)
      OR = OMEGA(2)
      OV = OMEGA(3)
      CALL GILDAS_DBLE ('Z$',Z,2)
      ZS = Z(1)
      ZL = Z(2)
      IF (ZS.LE.ZL.AND.ZS.GT.0.AND.ZL.GE.0) THEN     ! z = positive, Da = negative
         WRITE (TEXT(1:30),'(2f15.6)') ZS,ZL
         CALL GAGOUT ('F-GL, z = '//TEXT(1:30))
         CALL GAGOUT ('F-GL, at least one of the redshifts is wrong')
         CALL SYSEXI (FATALE)
      ENDIF
      CALL GILDAS_DBLE ('FWHM$',FWHM,2)
      FWHM1 = FWHM(1)
      FWHM2 = FWHM(2)
      CALL GILDAS_DBLE ('XYSH$',XYSH,2)
      XSH = XYSH(1)
      YSH = XYSH(2)
!
      CALL GILDAS_INTE ('MAXPIX$',MAXPIX,1)
      CALL GILDAS_DBLE ('STEP$',STEP,1)
      CALL GILDAS_LOGI ('CAU$',CAUYES,1)
      DOCAU = 0
      IF (CAUYES) DOCAU = 1
      CALL GILDAS_LOGI ('DELAY$',DELAYYES,1)
      DELAY = 0
      IF (DELAYYES) DELAY = 1
      CALL GILDAS_INTE ('TEST$',TEST,1)
!
      CALL GILDAS_DBLE ('LENSMASS$',M,1)
!
      CALL GILDAS_DBLE ('SIGMAVL1$',SVL1,1)
      IF (LENSMODEL(1:2).EQ.'M1') SIGMAV = SVL1
!
      CALL GILDAS_DBLE ('SIGMAVL2$',SVL2,1)
      CALL GILDAS_DBLE ('ELLIP2$',ELLIP2,1)
      CALL GILDAS_DBLE ('CORE2$',CORE2,1)
      CALL GILDAS_DBLE ('PA2$',PA2,1)
      IF (LENSMODEL(1:2).EQ.'M2') THEN
         SIGMAV = SVL2
         EPS = ELLIP2
         CORE = CORE2
         PA = PA2
      ENDIF
!
      CALL GILDAS_DBLE ('SIGMAVL3$',SVL3,1)
      CALL GILDAS_DBLE ('ETA3$',ETA3,1)
      CALL GILDAS_DBLE ('CORE3$',CORE3,1)
      CALL GILDAS_DBLE ('GAMMA3$',GAMMA3,1)
      CALL GILDAS_DBLE ('PHIG3$',PHIG3,1)
      IF (LENSMODEL(1:2).EQ.'M3') THEN
         SIGMAV = SVL3
         ETA = ETA3
         CORE = CORE3
         GAMMA0 = GAMMA3
         PHIG = PHIG3
      ENDIF
!
      CALL GILDAS_DBLE ('MBH4$',MBH4,1)
      CALL GILDAS_DBLE ('SIGMAVL4$',SVL4,1)
      CALL GILDAS_DBLE ('CORE4$',CORE4,1)
      CALL GILDAS_DBLE ('GAMMA4$',GAMMA4,1)
      CALL GILDAS_DBLE ('PHIG4$',PHIG4,1)
      IF (LENSMODEL(1:2).EQ.'M4') THEN
         MBH = MBH4
         SIGMAV = SVL4
         CORE = CORE4
         GAMMA0 = GAMMA4
         PHIG = PHIG4
      ENDIF
!
      CALL GILDAS_DBLE ('SIGMAVL5$',SVL5,1)
      CALL GILDAS_DBLE ('ETA5$',ETA5,1)
      CALL GILDAS_DBLE ('ELLIP5$',ELLIP5,1)
      CALL GILDAS_DBLE ('CORE5$',CORE5,1)
      CALL GILDAS_DBLE ('PA5$',PA5,1)
      CALL GILDAS_DBLE ('GAMMA5$',GAMMA5,1)
      CALL GILDAS_DBLE ('PHIG5$',PHIG5,1)
      IF (LENSMODEL(1:2).EQ.'M5') THEN
         SIGMAV = SVL5
         ETA = ETA5
         EPS = ELLIP5
         CORE = CORE5
         PA = PA5
         GAMMA0 = GAMMA5
         PHIG = PHIG5
      ENDIF
!
      CALL GILDAS_DBLE ('MBH6$',MBH6,1)
      CALL GILDAS_DBLE ('SIGMAVL6$',SVL6,1)
      CALL GILDAS_DBLE ('ELLIP6$',ELLIP6,1)
      CALL GILDAS_DBLE ('CORE6$',CORE6,1)
      CALL GILDAS_DBLE ('PA6$',PA6,1)
      CALL GILDAS_DBLE ('GAMMA6$',GAMMA6,1)
      CALL GILDAS_DBLE ('PHIG6$',PHIG6,1)
      IF (LENSMODEL(1:2).EQ.'M6') THEN
         MBH = MBH6
         SIGMAV = SVL6
         EPS = ELLIP6
         CORE = CORE6
         PA = PA6
         GAMMA0 = GAMMA6
         PHIG = PHIG6
      ENDIF
!
      CALL GILDAS_DBLE ('MBH7$',MBH7,1)
      CALL GILDAS_DBLE ('SIGMAVL7$',SVL7,1)
      CALL GILDAS_DBLE ('SIGMAVC7$',SVC7,1)
      CALL GILDAS_DBLE ('TCL7$',TCL7,2)
      CALL GILDAS_DBLE ('ELLIP7$',ELLIP7,1)
      CALL GILDAS_DBLE ('CORE7$',CORE7,1)
      CALL GILDAS_DBLE ('PA7$',PA7,1)
      IF (LENSMODEL(1:2).EQ.'M7') THEN
         MBH = MBH7
         SIGMAV = SVL7
         SIGMACL = SVC7
         TCL2 = TCL7(1)                  ! swapped index
         TCL1 = TCL7(2)
         EPS = ELLIP7
         CORE = CORE7
         PA = PA7
      ENDIF
!
      CALL GILDAS_DBLE ('SIGMAVL8$',SVL8,1)
      CALL GILDAS_DBLE ('SIGMAVC8$',SVC8,1)
      CALL GILDAS_DBLE ('TCL8$',TCL8,2)
      CALL GILDAS_DBLE ('ETA8$',ETA8,1)
      CALL GILDAS_DBLE ('ELLIP8$',ELLIP8,1)
      CALL GILDAS_DBLE ('CORE8$',CORE8,1)
      CALL GILDAS_DBLE ('PA8$',PA8,1)
      CALL GILDAS_DBLE ('GAMMA8$',GAMMA8,1)
      CALL GILDAS_DBLE ('PHIG8$',PHIG8,1)
      IF (LENSMODEL(1:2).EQ.'M8') THEN
         SIGMAV = SVL8
         SIGMACL = SVC8
         TCL2 = TCL8(1)                  ! swapped index
         TCL1 = TCL8(2)
         ETA = ETA8
         EPS = ELLIP8
         CORE = CORE8
         PA = PA8
         GAMMA0 = GAMMA8
         PHIG = PHIG8
      ENDIF
      CALL GILDAS_CLOSE
!
      MINLIMF = 1
      MINLIMG = 1
      MAXLIMF = MAXPIX
      MAXLIMG = MAXPIX
      TCL2    = -TCL2
!
      NDIM = DIM
      SQE  = 0
      SLE  = 0
      PI   = 4*ATAN(1D0)
      RAD  = PI/180D0
      SEC  = PI/180D0/3600D0
      GC   = 6.673D-8                    ! CGS system  (cm^3/gr/s^2)
      GS   = 4.32D6                      ! (km/s)^2 kpc/(10^11Msun)
      AS0  = 3700                        ! (km/s)^2
      C    = 299792.458D5                ! CGS system  (cm/s)
      GPC  = 149.598D11/SEC*1D9          ! Giga parsec (cm)
!
!       Mass
!       Sanders et al., MNRAS 2000
!
      IF (MODEL.NE.0) THEN
         M=SIGMAV**4/(GS*AS0)
      ENDIF
!
      WRITE (6,'(a,f10.3,a)') 'Computations done for a FOV of ',
     &(2*NDIM+1)*STEP,' arcs'
      WRITE (6,'(a,f10.3,a)') 'Pixel size is                  ',
     &STEP,' arcs'
      WRITE (6,'(1x)')
!
      PHIG  = PHIG-90
      STPSC = ABS(STEP*SEC)
      CORE  = ABS(CORE/STEP)+1E-34
      CSP   = COS(PA*RAD)
      SSP   = SIN(PA*RAD)
      CSG   = COS(2*PHIG*RAD)
      SSG   = SIN(2*PHIG*RAD)
!
      MAXA  = 0.0
      FRACTION = 2D0*NDIM/(MAXPIX-1D0)
!
      IF (TEST.EQ.1) THEN
         DO I=-NDIM,NDIM
            DO J=0,29
               DO T=0,3
                  WAAG(I,T+4*2*J-120)=-1.0
                  SENK(T+4*2*J-120,I)=1.0
               ENDDO
            ENDDO
         ENDDO
         DO I=-NDIM,NDIM
            DO J=-NDIM,NDIM
               QE(I,J)=WAAG(I,J)+SENK(I,J)
               IF (QE(I,J).EQ.-1) QE(I,J)=1
               SQE=SQE+QE(I,J)
            ENDDO
         ENDDO
!
         WRITE (6,'(f10.3)') SIGN(1D0,FWHM2)
!
      ELSEIF  (TEST.EQ.0) THEN
         DO I=-NDIM,NDIM
            XS = I*STEP+XSH
            DO J=-NDIM,NDIM
               YS = J*STEP-YSH
               QE(J,I) = EXP(-(XS**2+YS**2)/
     &         (2*(FWHM1/DSQRT(8D0*LOG(2.)))**2))
               IF (FWHM2.NE.0) THEN
                  QE(J,I) = QE(J,I)+SIGN(1D0,FWHM2)*
     &                 EXP(-(XS**2+YS**2)/(2*(FWHM2/DSQRT(8D0*
     &                 LOG(2.)))**2))
               ENDIF
               SQE = SQE+QE(J,I)
               MAXA = MAX(QE(J,I),MAXA)
            ENDDO
         ENDDO
      ENDIF
      SQE = SQE*(STEP*STEP)
!
      DO G=1,MAXPIX
         V(G) = NDIM-(G-1)*FRACTION
         W(G) = NDIM-(G-1)*FRACTION
      ENDDO
!
      FILE = .FALSE.
      IF (TEST.EQ.2) THEN
         OPEN (UNIT=1,FILE='qe.glens',FORM='formatted',STATUS='old',
     &   ERR=200)
         CLOSE (UNIT=1)
         CALL READ_IMAGE ('qe.glens',NDIM,QE,MINLIMG,MAXLIMG,MINLIMF,
     &   MAXLIMF,STPSC,FRACTION,NORM)
         DO G=MINLIMF,MAXLIMF
            W(G) = NDIM-(G-MINLIMF)*2D0*NDIM/(MAXLIMF-MINLIMF)
         ENDDO
         DO G=MINLIMG,MAXLIMG
            V(G) = NDIM-(G-MINLIMG)*2D0*NDIM/(MAXLIMG-MINLIMG)
         ENDDO
         DO I=-NDIM,NDIM
            DO J=-NDIM,NDIM
               SQE=SQE+QE(I,J)
            ENDDO
         ENDDO
         SQE = SQE*NORM
         FILE = .TRUE.
200      CLOSE (UNIT=1)
      ENDIF
!
      O = OM+OR+OV
      IF (ABS(O-1).LT.1E-5) THEN
         K = 0
      ELSEIF  (O.LT.1) THEN
         K = -1
      ELSE
         K = 1
      ENDIF
      CALL ZDA (ZL,DD)
      CALL ZDA (ZS,DS)
!
      DDS = DS-DD*(1+ZL)/(1+ZS)
!
      WRITE (6,'(a,f7.3,a,f4.2,a)') 'Angular-size distance'//
     &'(observer-source):   ',DS,' Gpc (z=',ZS,')'
      WRITE (6,'(a,f7.3,a,f4.2,a)') 'Angular-size distance'//
     &'(observer-lens):     ',DD,' Gpc (z=',ZL,')'
      WRITE (6,'(a,f7.3,a,f4.2,a)') 'Angular-size distance'//
     &'(lens-source):       ',DDS,' Gpc'
      WRITE (6,'(a,f7.3,a)')        'Mass of the lens (Tera Msun)'//
     &':             ',M
!
      IF (K.NE.0) THEN
         R = C/(HUBBLE*SQRT(ABS(O-1)))/1E8
         WRITE (6,'(a,f7.3,a,i2,a)') 'Present scale factor R '//
     &   '(Hubble length):    ',R,' Gpc (k=',K,')'
      ENDIF
      DD   = DD*GPC
      DS   = DS*GPC
      DDS  = DDS*GPC
!
      IF (SIGMAV.LT.0) THEN
         ALPHAE = ABS(SIGMAV)*SEC        ! SPLS,SPEMD
         SIGMAV = (C/1E5)*SQRT(DS/(4*PI*DDS)*ALPHAE)
      ELSE
         ALPHAE = 4*PI*DDS/DS*(SIGMAV/(C/1E5))**2
      ENDIF
      FAC0  = 4*GC*M*1.98E33/(C/1D6)**2  ! Schwarzschild case
      FAC1  = 4*PI*(SIGMAV/(C/1D5))**2   ! Isothermal case
      FAC2  = DS/DDS*ALPHAE**(2-ETA)     ! SPLS,SPEMD
      FAC3  = (SIGMAV/(C/1D5))**2        ! King-Profile (FGS)
      AL    = FAC1
      ALBH0 = SQRT(4*GC*MBH*1.98E33/(C/1D6)**2/DD/STPSC)   ! Black hole
      BCL   = 4*PI*(SIGMACL/(C/1D5))**2  ! Cluster
      COR   = CORE*STPSC
!
      DO I=-NDIM,NDIM
         X(I,1)=(I-5D-1)
         X(I,2)=(I-5D-1)
         X(I,3)=(I+5D-1)
         X(I,4)=(I+5D-1)
         Y(I,1)=(I-5D-1)
         Y(I,2)=(I+5D-1)
         Y(I,3)=(I+5D-1)
         Y(I,4)=(I-5D-1)
      ENDDO
!
      IC = 0
      DO S=-NDIM,NDIM
!	   write (6,'(i5)') s
         DO T=-NDIM,NDIM
            INTEN = 0
            IP = 0
            IF (MODEL.EQ.0) THEN
               DO A=1,NMAX
                  XSA = X(S,A)
                  YTA = Y(T,A)
                  D   = DSQRT(XSA**2+YTA**2)*STPSC
                  AL  = FAC0/(DD*SIN(D))
!		    write (6,*) al
                  DE  = ATAN(TAN(D)-((DDS+DD*SIN(D)**2)*(TAN(D)+
     &            TAN(AL-D)))/DS)
                  XA(S,A) = DE/D*XSA     ! de = beta (general notation)
                  YA(T,A) = DE/D*YTA     ! d = theta (general notation)
               ENDDO
            ELSEIF  (MODEL.EQ.1) THEN
               DO A=1,NMAX
                  XSA = X(S,A)
                  YTA = Y(T,A)
                  D   = DSQRT(XSA**2+YTA**2)*STPSC
                  DE  = ATAN(TAN(D)-((DDS+DD*SIN(D)**2)*(TAN(D)+
     &            TAN(AL-D)))/DS)
                  XA(S,A) = DE/D*XSA
                  YA(T,A) = DE/D*YTA
               ENDDO
            ELSEIF  (MODEL.EQ.2) THEN
               DO A=1,NMAX
                  XSA  = X(S,A)
                  YTA  = Y(T,A)
                  XSAP = XSA*CSP-YTA*SSP
                  YTAP = XSA*SSP+YTA*CSP
                  AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &            (1+EPS)*YTAP**2)
                  ALX  = (1-EPS)*XSAP*AL
                  ALY  = (1+EPS)*YTAP*AL
                  XSAP = XSAP*STPSC
                  YTAP = YTAP*STPSC
                  DX   = ATAN(TAN(XSAP)-((DDS+DD*SIN(XSAP)**2)*
     &            (TAN(XSAP)+TAN(ALX-XSAP)))/DS)
                  DY   = ATAN(TAN(YTAP)-((DDS+DD*SIN(YTAP)**2)*
     &            (TAN(YTAP)+TAN(ALY-YTAP)))/DS)
                  XA(S,A) = (+DX*CSP+DY*SSP)/STPSC
                  YA(T,A) = (-DX*SSP+DY*CSP)/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.3) THEN
               DO A=1,NMAX
                  XSA   = X(S,A)
                  YTA   = Y(T,A)
                  D     = DSQRT(XSA**2+YTA**2)*STPSC
                  XSAG  =  XSA*CSG-YTA*SSG
                  YTAG  = -XSA*SSG-YTA*CSG
                  ALX   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &            XSA+GAMMA0*XSAG)*STPSC
                  ALY   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &            YTA+GAMMA0*YTAG)*STPSC
                  XSA   = XSA*STPSC
                  YTA   = YTA*STPSC
                  DX    = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA)))/DS)
                  DY    = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA)))/DS)
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.4) THEN
               DO A=1,NMAX
                  XSA   = X(S,A)
                  YTA   = Y(T,A)
                  XSAG  =  XSA*CSG-YTA*SSG
                  YTAG  = -XSA*SSG-YTA*CSG
                  D     = DSQRT(XSA**2+YTA**2)
                  D0    = D/CORE
                  D1    = 1.155*D0
                  D2    = 0.579*D0
                  F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                  F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                  AL    = FAC3*(F1-F2)/D
                  ALBH  = ALBH0**2/D**2
                  ALX   = (AL+ALBH)*XSA+GAMMA0*XSAG*STPSC
                  ALY   = (AL+ALBH)*YTA+GAMMA0*YTAG*STPSC
                  D     = D*STPSC
                  XSA   = XSA*STPSC
                  YTA   = YTA*STPSC
                  DX    = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA)))/DS)
                  DY    = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA)))/DS)
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.5) THEN
               DO A=1,NMAX
                  XSA   = X(S,A)
                  YTA   = Y(T,A)
                  XSAP  = (XSA*CSP-YTA*SSP)
                  YTAP  = (XSA*SSP+YTA*CSP)
                  XSAG  =  XSA*CSG-YTA*SSG
                  YTAG  = -XSA*SSG-YTA*CSG
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
                  AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &            STPSC
                  GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                  ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                  XSA   = XSA*STPSC
                  YTA   = YTA*STPSC
                  DX    = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA))/DS))
                  DY    = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA))/DS))
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.6) THEN
               DO A=1,NMAX
                  XSA   = X(S,A)
                  YTA   = Y(T,A)
                  XSAP  = XSA*CSP-YTA*SSP
                  YTAP  = XSA*SSP+YTA*CSP
                  XSAG  =  XSA*CSG-YTA*SSG
                  YTAG  = -XSA*SSG-YTA*CSG
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                  D0    = D/CORE
                  D1    = 1.155*D0
                  D2    = 0.579*D0
                  F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                  F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                  AL    = FAC3*(F1-F2)/D
                  ALBH  = ALBH0**2/(XSA**2+YTA**2)
                  GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSA+GAMMA*XSAG
                  ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTA+GAMMA*YTAG
                  XSA   = XSA*STPSC
                  YTA   = YTA*STPSC
                  DX    = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA))/DS))
                  DY    = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA))/DS))
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.7) THEN
               DO A=1,NMAX
                  XSA  = X(S,A)
                  YTA  = Y(T,A)
                  XSAP = XSA*CSP-YTA*SSP
                  YTAP = XSA*SSP+YTA*CSP
                  D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                  D0   = D/CORE
                  D1   = 1.155*D0
                  D2   = 0.579*D0
                  F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                  F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                  AL   = FAC3*(F1-F2)/D
                  ALBH = ALBH0**2/(XSA**2+YTA**2)
                  XCL  = XSA-TCL1/STEP
                  YCL  = YTA-TCL2/STEP
                  DCL  = SQRT(XCL**2+YCL**2)
                  ALCL = BCL/DCL
                  ALXP = AL*(1-EPS)*XSAP
                  ALYP = AL*(1+EPS)*YTAP
                  ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSA+ALCL*XCL
                  ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTA+ALCL*YCL
                  XSA  = XSA*STPSC
                  YTA  = YTA*STPSC
                  DX   = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA))/DS))
                  DY   = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA))/DS))
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ELSEIF  (MODEL.EQ.8) THEN
               DO A=1,NMAX
                  XSA   = X(S,A)
                  YTA   = Y(T,A)
                  XSAP  = (XSA*CSP-YTA*SSP)
                  YTAP  = (XSA*SSP+YTA*CSP)
                  XCL   = XSA-TCL1/STEP
                  YCL   = YTA-TCL2/STEP
                  DCL   = SQRT(XCL**2+YCL**2)
                  ALCL  = BCL/DCL
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
                  AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)
     &            *STPSC
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                  ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                  XSA   = XSA*STPSC
                  YTA   = YTA*STPSC
                  DX    = ATAN(TAN(XSA)-((DDS+DD*SIN(XSA)**2)*
     &            (TAN(XSA)+TAN(ALX-XSA))/DS))
                  DY    = ATAN(TAN(YTA)-((DDS+DD*SIN(YTA)**2)*
     &            (TAN(YTA)+TAN(ALY-YTA))/DS))
                  XA(S,A) = DX/STPSC
                  YA(T,A) = DY/STPSC
               ENDDO
            ENDIF
!
            XA(S,NMAX+1) = XA(S,1)
            YA(T,NMAX+1) = YA(T,1)
!
            DO I=1,NMAX
               GONS(I,1) = XA(S,I)
               GONS(I,2) = YA(T,I)
               GONS(I,3) = XA(S,I+1)-XA(S,I)
               GONS(I,4) = YA(T,I+1)-YA(T,I)
            ENDDO
            GONS(NMAX+1,1) = XA(S,NMAX+1)
            GONS(NMAX+1,2) = YA(T,NMAX+1)
            BOUND(1)= GONS(1,1)
            BOUND(2)= GONS(1,1)
            BOUND(3)= GONS(1,2)
            BOUND(4)= GONS(1,2)
            DO I=2,NMAX
               BOUND(1) = MIN(BOUND(1),GONS(I,1))    ! xmin
               BOUND(2) = MAX(BOUND(2),GONS(I,1))    ! xmax
               BOUND(3) = MIN(BOUND(3),GONS(I,2))    ! ymin
               BOUND(4) = MAX(BOUND(4),GONS(I,2))    ! ymax
            ENDDO
            GMI = INT((NDIM-BOUND(4))/FRACTION)+2
            GMA = INT((NDIM-BOUND(3))/FRACTION)+1
            FMI = INT((NDIM-BOUND(2))/FRACTION)+2
            FMA = INT((NDIM-BOUND(1))/FRACTION)+1
!
            IF (GMI.GE.MINLIMG.AND.GMA.LE.MAXLIMG.AND.FMI.GE.
     &      MINLIMF.AND.FMA.LE.MAXLIMF) THEN
               DO F=FMI,FMA
                  VF = W(F)
                  DO G=GMI,GMA
                     VG = V(G)
                     IN = INSIDE (VF,VG,NMAX,GONS,BOUND).OR.FILE
                     IF (IN) THEN
                        QS=INT(VF+SIGN(.5D0,VF))
                        QT=INT(VG+SIGN(.5D0,VG))
                        INTEN = INTEN+QE(QS,QT)
                        IP = IP+1
                     ENDIF
                  ENDDO
               ENDDO
               IF (IP.NE.0) THEN
                  LE(S,T) =  INTEN/IP*(STEP*STEP)    !*(ds/dd)**2
                  SLE = SLE+LE(S,T)
               ELSE
                  LE(S,T) = 1.23456E34
                  IC = IC+1
                  WRITE (6,*) 'No valid point for ',S,T
               ENDIF
               A1 = XA(S,1)-XA(S,3)
               A2 = YA(T,1)-YA(T,3)
               B1 = XA(S,2)-XA(S,4)
               B2 = YA(T,2)-YA(T,4)
               C1 = ABS(ATAN(A2/A1)-ATAN(B2/B1))
               MA(S,T) = SQRT((A1*A1+A2*A2)*(B1*B1+B2*B2))*SIN(C1)/2
            ENDIF
         ENDDO
      ENDDO
!
      WRITE (6,'(a)') 'Writing files "qe.gdf" (source plane),'//
     &'"le.gdf" (lens plane), ....'
      CALL WRITE_IMAGE ('qe.gdf',NDIM,QE,STEP)
      CALL WRITE_IMAGE ('le.gdf',NDIM,LE,STEP)
      CALL WRITE_IMAGE ('ma.gdf',NDIM,MA,STEP)
!
      IF (DOCAU.EQ.1) THEN
         WRITE (6,'(a)') 'Writing files "ca.dat" (caustics),'//
     &   '"cl.dat" (critical lines)...'
         OPEN (UNIT=1,FILE='cl.dat',FORM='formatted',STATUS='unknown')
         OPEN (UNIT=2,FILE='ca.dat',FORM='formatted',STATUS='unknown')
         MM  = 0
         CSCL = COS((2*PHIG)*RAD)
         SSCL = SIN((2*PHIG)*RAD)
         DO I=0,359
            IF (MODEL.EQ.0) THEN
               XSAP = SQRT(FAC0*DDS/(DD*DS))*COS(I*RAD)
               YTAP = SQRT(FAC0*DDS/(DD*DS))*SIN(I*RAD)
               WRITE (1,'(2e15.6)')  XSAP, YTAP
            ENDIF
            IF (MODEL.EQ.1) THEN
               XSAP = FAC1*DDS/DS*COS(I*RAD)
               YTAP = FAC1*DDS/DS*SIN(I*RAD)
               WRITE (1,'(2e15.6)')  XSAP, YTAP
            ENDIF
            IF (MODEL.GE.2) THEN
               FLA = .TRUE.
               IF (MODEL.EQ.4) THEN
                  PA  = 0.0
                  EPS = 0.0
                  CSP = 1.0
                  SSP = 0.0
               ENDIF
               CSPI = COS((PA+I)*RAD)
               SSPI = SIN((PA+I)*RAD)
               CSQ  = COS(I*RAD)
               SSQ  = SIN(I*RAD)
               CSG  = COS((2*PHIG+I)*RAD)! cluster shear angle
               SSG  = SIN((2*PHIG+I)*RAD)! cluster shear angle
!
               J   = 0
               DO NSI=-NDIM*10,0,1
                  NS = NSI/10
                  XSA = (NS-.05)
                  YTA = -.05
                  XSAP = XSA*CSPI-YTA*SSPI
                  YTAP = XSA*SSPI+YTA*CSPI
                  XSAQ = XSA*CSQ-YTA*SSQ
                  YTAQ = XSA*SSQ+YTA*CSQ
                  IF (MODEL.EQ.2) THEN
                     AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &               (1+EPS)*YTAP**2)
                     ALXP = (1-EPS)*XSAP*AL
                     ALYP = (1+EPS)*YTAP*AL
                     ALX  =  ALXP*CSP+ALYP*SSP
                     ALY  = -ALXP*SSP+ALYP*CSP
                  ELSEIF  (MODEL.EQ.3) THEN
                     D     = DSQRT(XSAQ**2+YTAQ**2)*STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     ALX   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*XSAQ+GAMMA0*XSAG)*STPSC
                     ALY   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*YTAQ+GAMMA0*YTAG)*STPSC
                  ELSEIF  (MODEL.EQ.5) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ALXP  = (1-EPS)*XSAP*AL
                     ALYP  = (1+EPS)*YTAP*AL
                     ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.6.OR.MODEL.EQ.4) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     IF (MODEL.EQ.4) THEN
                        GAMMA = GAMMA0*STPSC
                     ELSE
                        GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ENDIF
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.7) THEN
                     D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0   = D/CORE
                     D1   = 1.155*D0
                     D2   = 0.579*D0
                     F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL   = FAC3*(F1-F2)/D
                     ALBH = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XCL  = XSAQ-TCL1/STEP
                     YCL  = YTAQ-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     ALXP = AL*(1-EPS)*XSAP
                     ALYP = AL*(1+EPS)*YTAP
                     ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+ALCL*XCL
                     ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+ALCL*YCL
                  ELSEIF  (MODEL.EQ.8) THEN
                     XCL   = XSAQ-TCL1/STEP
                     YCL   = YTAQ-TCL2/STEP
                     DCL   = SQRT(XCL**2+YCL**2)
                     ALCL  = BCL/DCL
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
     &               *STPSC
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                     ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                  ENDIF
                  XSAQ  = XSAQ*STPSC
                  YTAQ  = YTAQ*STPSC
                  DF(1) = ATAN(TAN(XSAQ)-((DDS+DD*SIN(XSAQ)**2)*
     &            (TAN(XSAQ)+TAN(ALX-XSAQ)))/DS)
                  DF(2) = ATAN(TAN(YTAQ)-((DDS+DD*SIN(YTAQ)**2)*
     &            (TAN(YTAQ)+TAN(ALY-YTAQ)))/DS)
!
                  XSA = (NS-.05)
                  YTA = +.05
                  XSAP = XSA*CSPI-YTA*SSPI
                  YTAP = XSA*SSPI+YTA*CSPI
                  XSAQ = XSA*CSQ-YTA*SSQ
                  YTAQ = XSA*SSQ+YTA*CSQ
                  IF (MODEL.EQ.2) THEN
                     AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &               (1+EPS)*YTAP**2)
                     ALXP = (1-EPS)*XSAP*AL
                     ALYP = (1+EPS)*YTAP*AL
                     ALX  =  ALXP*CSP+ALYP*SSP
                     ALY  = -ALXP*SSP+ALYP*CSP
                  ELSEIF  (MODEL.EQ.3) THEN
                     D     = DSQRT(XSAQ**2+YTAQ**2)*STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     ALX  = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*XSAQ+GAMMA0*XSAG)*STPSC
                     ALY  = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*YTAQ+GAMMA0*YTAG)*STPSC
                  ELSEIF  (MODEL.EQ.5) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ALXP  = (1-EPS)*XSAP*AL
                     ALYP  = (1+EPS)*YTAP*AL
                     ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.6.OR.MODEL.EQ.4) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     IF (MODEL.EQ.4) THEN
                        GAMMA = GAMMA0*STPSC
                     ELSE
                        GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ENDIF
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.7) THEN
                     D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0   = D/CORE
                     D1   = 1.155*D0
                     D2   = 0.579*D0
                     F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL   = FAC3*(F1-F2)/D
                     ALBH = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XCL  = XSAQ-TCL1/STEP
                     YCL  = YTAQ-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     ALXP = AL*(1-EPS)*XSAP
                     ALYP = AL*(1+EPS)*YTAP
                     ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+ALCL*XCL
                     ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+ALCL*YCL
                  ELSEIF  (MODEL.EQ.8) THEN
                     XCL   = XSAQ-TCL1/STEP
                     YCL   = YTAQ-TCL2/STEP
                     DCL   = SQRT(XCL**2+YCL**2)
                     ALCL  = BCL/DCL
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                     ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                  ENDIF
                  XSAQ  = XSAQ*STPSC
                  YTAQ  = YTAQ*STPSC
                  DF(3) = ATAN(TAN(XSAQ)-((DDS+DD*SIN(XSAQ)**2)*
     &            (TAN(XSAQ)+TAN(ALX-XSAQ)))/DS)
                  DF(4) = ATAN(TAN(YTAQ)-((DDS+DD*SIN(YTAQ)**2)*
     &            (TAN(YTAQ)+TAN(ALY-YTAQ)))/DS)
!
                  XSA = (NS+.05)
                  YTA = +.05
                  XSAP = XSA*CSPI-YTA*SSPI
                  YTAP = XSA*SSPI+YTA*CSPI
                  XSAQ = XSA*CSQ-YTA*SSQ
                  YTAQ = XSA*SSQ+YTA*CSQ
                  IF (MODEL.EQ.2) THEN
                     AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &               (1+EPS)*YTAP**2)
                     ALXP = (1-EPS)*XSAP*AL
                     ALYP = (1+EPS)*YTAP*AL
                     ALX  =  ALXP*CSP+ALYP*SSP
                     ALY  = -ALXP*SSP+ALYP*CSP
                  ELSEIF  (MODEL.EQ.3) THEN
                     D     = DSQRT(XSAQ**2+YTAQ**2)*STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     ALX   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*XSAQ+GAMMA0*XSAG)*STPSC
                     ALY   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*YTAQ+GAMMA0*YTAG)*STPSC
                  ELSEIF  (MODEL.EQ.5) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ALXP  = (1-EPS)*XSAP*AL
                     ALYP  = (1+EPS)*YTAP*AL
                     ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.6.OR.MODEL.EQ.4) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     IF (MODEL.EQ.4) THEN
                        GAMMA = GAMMA0*STPSC
                     ELSE
                        GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ENDIF
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.7) THEN
                     D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0   = D/CORE
                     D1   = 1.155*D0
                     D2   = 0.579*D0
                     F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL   = FAC3*(F1-F2)/D
                     ALBH = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XCL  = XSAQ-TCL1/STEP
                     YCL  = YTAQ-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     ALXP = AL*(1-EPS)*XSAP
                     ALYP = AL*(1+EPS)*YTAP
                     ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+ALCL*XCL
                     ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+ALCL*YCL
                  ELSEIF  (MODEL.EQ.8) THEN
                     XCL   = XSAQ-TCL1/STEP
                     YCL   = YTAQ-TCL2/STEP
                     DCL   = SQRT(XCL**2+YCL**2)
                     ALCL  = BCL/DCL
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                     ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                  ENDIF
                  XSAQ  = XSAQ*STPSC
                  YTAQ  = YTAQ*STPSC
                  DF(5) = ATAN(TAN(XSAQ)-((DDS+DD*SIN(XSAQ)**2)*
     &            (TAN(XSAQ)+TAN(ALX-XSAQ)))/DS)
                  DF(6) = ATAN(TAN(YTAQ)-((DDS+DD*SIN(YTAQ)**2)*
     &            (TAN(YTAQ)+TAN(ALY-YTAQ)))/DS)
!
                  XSA = (NS+.05)
                  YTA = -.05
                  XSAP = XSA*CSPI-YTA*SSPI
                  YTAP = XSA*SSPI+YTA*CSPI
                  XSAQ = XSA*CSQ-YTA*SSQ
                  YTAQ = XSA*SSQ+YTA*CSQ
                  IF (MODEL.EQ.2) THEN
                     AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &               (1+EPS)*YTAP**2)
                     ALXP = (1-EPS)*XSAP*AL
                     ALYP = (1+EPS)*YTAP*AL
                     ALX  =  ALXP*CSP+ALYP*SSP
                     ALY  = -ALXP*SSP+ALYP*CSP
                  ELSEIF  (MODEL.EQ.3) THEN
                     D     = DSQRT(XSAQ**2+YTAQ**2)*STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     ALX   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*XSAQ+GAMMA0*XSAG)*STPSC
                     ALY   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*YTAQ+GAMMA0*YTAG)*STPSC
                  ELSEIF  (MODEL.EQ.5) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ALXP  = (1-EPS)*XSAP*AL
                     ALYP  = (1+EPS)*YTAP*AL
                     ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.6.OR.MODEL.EQ.4) THEN
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XSAG  =  XSA*CSG-YTA*SSG
                     YTAG  = -XSA*SSG-YTA*CSG
                     IF (MODEL.EQ.4) THEN
                        GAMMA = GAMMA0*STPSC
                     ELSE
                        GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                     ENDIF
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+GAMMA*XSAG
                     ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+GAMMA*YTAG
                  ELSEIF  (MODEL.EQ.7) THEN
                     D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                     D0   = D/CORE
                     D1   = 1.155*D0
                     D2   = 0.579*D0
                     F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL   = FAC3*(F1-F2)/D
                     ALBH = ALBH0**2/(XSAQ**2+YTAQ**2)
                     XCL  = XSAQ-TCL1/STEP
                     YCL  = YTAQ-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     ALXP = AL*(1-EPS)*XSAP
                     ALYP = AL*(1+EPS)*YTAP
                     ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+ALCL*XCL
                     ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+ALCL*YCL
                  ELSEIF  (MODEL.EQ.8) THEN
                     XCL   = XSAQ-TCL1/STEP
                     YCL   = YTAQ-TCL2/STEP
                     DCL   = SQRT(XCL**2+YCL**2)
                     ALCL  = BCL/DCL
                     D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*
     &               STPSC
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**
     &               ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                     ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                  ENDIF
                  XSAQ  = XSAQ*STPSC
                  YTAQ  = YTAQ*STPSC
                  DF(7) = ATAN(TAN(XSAQ)-((DDS+DD*SIN(XSAQ)**2)*
     &            (TAN(XSAQ)+TAN(ALX-XSAQ)))/DS)
                  DF(8) = ATAN(TAN(YTAQ)-((DDS+DD*SIN(YTAQ)**2)*
     &            (TAN(YTAQ)+TAN(ALY-YTAQ)))/DS)
!
                  A1 = DF(1)-DF(5)
                  A2 = DF(2)-DF(6)
                  B1 = DF(3)-DF(7)
                  B2 = DF(4)-DF(8)
                  C1 = ABS(ATAN(A2/A1)-ATAN(B2/B1))
                  NEWMA = SQRT((A1*A1+A2*A2)*(B1*B1+B2*B2))*SIN(C1)/2
!
                  IF (NS.NE.-NDIM) THEN
                     IF (NEWMA.GT.OLDMA.AND.FLA) THEN
                        J  = J+1
                        IF (J.GT.6) THEN
                           WRITE (6,'(a)') 'readapt array dimension...'
                           CALL SYSEXI (FATALE)
                        ENDIF
                        CLI(1,J) = NS*COS((90+I)*RAD)*STPSC
                        CLI(2,J) = NS*SIN((90+I)*RAD)*STPSC
                        XSAP = NS*CSPI
                        YTAP = NS*SSPI
                        XSAQ = NS*CSQ
                        YTAQ = NS*SSQ
                        IF (MODEL.EQ.2) THEN
                           AL   = FAC1/DSQRT(CORE**2+(1-EPS)*XSAP**2+
     &                     (1+EPS)*YTAP**2)
                           ALXP = (1-EPS)*XSAP*AL
                           ALYP = (1+EPS)*YTAP*AL
                           ALX  =  ALXP*CSP+ALYP*SSP
                           ALY  = -ALXP*SSP+ALYP*CSP
                        ELSEIF  (MODEL.EQ.3) THEN
                           D     = DSQRT(XSAQ**2+YTAQ**2)*STPSC
                           XSAG  =  XSAQ*CSCL-YTAQ*SSCL
                           YTAG  = -XSAQ*SSCL-YTAQ*CSCL
                           ALX   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &                     COR**ETA)*XSAQ+GAMMA0*XSAG)*STPSC
                           ALY   = (FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &                     COR**ETA)*YTAQ+GAMMA0*YTAG)*STPSC
                        ELSEIF  (MODEL.EQ.5) THEN
                           D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*
     &                     YTAP**2)*STPSC
                           XSAG  =  XSAQ*CSCL-YTAQ*SSCL
                           YTAG  = -XSAQ*SSCL-YTAQ*CSCL
                           AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &                     COR**ETA)*STPSC
                           GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                           ALXP  = (1-EPS)*XSAP*AL
                           ALYP  = (1+EPS)*YTAP*AL
                           ALX   =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
                           ALY   = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
                        ELSEIF  (MODEL.EQ.6.OR.MODEL.EQ.4) THEN
                           D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*
     &                     YTAP**2)
                           D0    = D/CORE
                           D1    = 1.155*D0
                           D2    = 0.579*D0
                           F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                           F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                           AL    = FAC3*(F1-F2)/D
                           ALBH  = ALBH0**2/(XSAQ**2+YTAQ**2)
                           XSAG  =  XSAQ*CSCL-YTAQ*SSCL
                           YTAG  = -XSAQ*SSCL-YTAQ*CSCL
                           IF (MODEL.EQ.0) THEN
                              GAMMA = GAMMA0*STPSC
                           ELSE
                              GAMMA = GAMMA0/(1+GAMMA0)*STPSC
                           ENDIF
                           ALXP  = AL*(1-EPS)*XSAP
                           ALYP  = AL*(1+EPS)*YTAP
                           ALX   =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+GAMMA*
     &                     XSAG
                           ALY   = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+GAMMA*
     &                     YTAG
                        ELSEIF  (MODEL.EQ.7) THEN
                           D    = DSQRT((1-EPS)*XSAP**2+(1+EPS)*
     &                     YTAP**2)
                           D0   = D/CORE
                           D1   = 1.155*D0
                           D2   = 0.579*D0
                           F1   = 53.2468/D1*(SQRT(1+D1**2)-1)
                           F2   = 44.0415/D2*(SQRT(1+D2**2)-1)
                           AL   = FAC3*(F1-F2)/D
                           ALBH = ALBH0**2/(XSAQ**2+YTAQ**2)
                           XCL  = XSAQ-TCL1/STEP
                           YCL  = YTAQ-TCL2/STEP
                           DCL  = SQRT(XCL**2+YCL**2)
                           ALCL = BCL/DCL
                           ALXP = AL*(1-EPS)*XSAP
                           ALYP = AL*(1+EPS)*YTAP
                           ALX  =  ALXP*CSP+ALYP*SSP+ALBH*XSAQ+ALCL*
     &                     XCL
                           ALY  = -ALXP*SSP+ALYP*CSP+ALBH*YTAQ+ALCL*
     &                     YCL
                        ELSEIF  (MODEL.EQ.8) THEN
                           XCL   = XSAQ-TCL1/STEP
                           YCL   = YTAQ-TCL2/STEP
                           DCL   = SQRT(XCL**2+YCL**2)
                           ALCL  = BCL/DCL
                           D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*
     &                     YTAP**2)*STPSC
                           AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &                     COR**ETA)*STPSC
                           ALXP  = AL*(1-EPS)*XSAP
                           ALYP  = AL*(1+EPS)*YTAP
                           ALX   =  ALXP*CSP+ALYP*SSP+ALCL*XCL
                           ALY   = -ALXP*SSP+ALYP*CSP+ALCL*YCL
                        ENDIF
                        XSAQ = XSAQ*STPSC
                        YTAQ = YTAQ*STPSC
                        DX = ATAN(TAN(XSAQ)-((DDS+DD*SIN(XSAQ)**2)*
     &                  (TAN(XSAQ)+TAN(ALX-XSAQ)))/DS)
                        DY = ATAN(TAN(YTAQ)-((DDS+DD*SIN(YTAQ)**2)*
     &                  (TAN(YTAQ)+TAN(ALY-YTAQ)))/DS)
                        CAU(1,J) = -DY
                        CAU(2,J) = DX
                        FLA = .FALSE.
                     ENDIF
                  ELSE
                     OLDMA = NEWMA
                  ENDIF
                  IF (NEWMA.LT.OLDMA) FLA = .TRUE.
                  OLDMA = NEWMA
               ENDDO
               MM = MAX (MM,J)
               IF (J.NE.0) THEN
                  DO L=J+1,6
                     CLI(1,L) = -1000
                     CLI(2,L) = -1000
                     CAU(1,L) = -1000
                     CAU(2,L) = -1000
                  ENDDO
                  WRITE (1,'(12e13.5)') (CLI(1,L),CLI(2,L),L=1,6)
                  WRITE (2,'(12e13.5)') (CAU(1,L),CAU(2,L),L=1,6)
               ENDIF
            ENDIF
         ENDDO
         IF (MODEL.LE.1) MM = 1
         WRITE (1,'(i4)') MM
         WRITE (2,'(i4)') MM
         CLOSE (UNIT=1)
         CLOSE (UNIT=2)
      ENDIF
!
! Determination of the sources' position for fitting the parameters
!
      MAXA   = 0.0
      MAXB   = 0.0
      INTENA = 0.0
      INTENB = 0.0
      INTENK = 0.0
      DELTAINT = 0.0
!
!
      DO S = 0,NINT(S_SIZE/STEP)
         SA = NINT(-AX/STEP+S)
         SB = NINT(-BX/STEP+S)
         SK = NINT(-KX/STEP+S)
         DO T = 0,NINT(T_SIZE/STEP)
            TA = NINT(AY/STEP+T)
            TB = NINT(BY/STEP+T)
            TK = NINT(KY/STEP+T)
            IF (ABS(TA).LE.NDIM.AND.ABS(SA).LE.NDIM) THEN
               LA = LE(TA,SA)
            ELSE
               LA = 0.0
            ENDIF
            IF (ABS(TB).LE.NDIM.AND.ABS(SB).LE.NDIM) THEN
               LB = LE(TB,SB)
            ELSE
               LB = 0.0
            ENDIF
            IF (ABS(TK).LE.NDIM.AND.ABS(SK).LE.NDIM) THEN
               LK = LE(TK,SK)
            ELSE
               LK = 0.0
            ENDIF
            IF (LA.LT.1000) THEN
               MAXA = MAX(LA,MAXA)
               INTENA  = INTENA+LA
               INTENXA = INTENXA+LA*SA
               INTENYA = INTENYA+LA*TA
               IF (MAXA.EQ.LA) THEN
                  XSA = SA
                  YTA = TA
               ENDIF
            ENDIF
            IF (LK.LT.1000) THEN
               INTENK =INTENK+LK
               MAXK = MAX(LK,MAXK)
               IF (MAXK.EQ.LK) THEN
                  XSK = SK
                  YTK = TK
               ENDIF
            ENDIF
            IF (LB.LT.1000) THEN
               MAXB = MAX(LB,MAXB)
               INTENB = INTENB+LB
               INTENXB = INTENXB+LB*SB
               INTENYB = INTENYB+LB*TB
               IF (MAXB.EQ.LB) THEN
                  XSB = SB
                  YTB = TB
               ENDIF
            ENDIF
         ENDDO
      ENDDO
!
      MXA = INTENXA/INTENA*STEP
      MYA = INTENYA/INTENA*STEP
      MXB = INTENXB/INTENB*STEP
      MYB = INTENYB/INTENB*STEP
      DELTAP2 = SQRT((MXA-MXB+AP1)**2+(MYA-MYB-AP2)**2)
      DELTAP   = SQRT((XSA-XSB+AP1/STEP)**2+(YTA-YTB-AP2/STEP)**2)*
     &STEP
      DELTAINT = INTENA/INTENB
!
      IF (DELTAINT.GE.1E5) DELTAINT=9999
      DELTAPK  = SQRT((XSK-XSB+JET1/STEP)**2+(YTK-YTB-JET2/STEP)**2)*
     &STEP
!
!
!   Time delay
!
!
      IF (DELAY.EQ.1) THEN
         WRITE (6,'(a)') 'Computing time delay ......'
!	dtime = 1d-3/maxtime
         PSI = 0
         TIMED2 = 0
         S = 1
         DO I=1,4
            SS(I) = 1
         ENDDO
         CSP   = COS(PA*RAD)
         SSP   = SIN(PA*RAD)
         CSG   = COS(2*PHIG*RAD)
         SSG   = SIN(2*PHIG*RAD)
!
         IF (MODEL.EQ.0) THEN
            D   = DSQRT(XSA**2+YTA**2)*STPSC
            IF ((XSA**2+YTA**2).NE.0) PSI = FAC0/DD*LOG(TAN(D/2))
            AL  = FAC0/(DD*SIN(D))
            TIMEDA =(1+ZL)/C*DD/2*(DDS/DS*AL**2-2*PSI)
            D   = DSQRT(XSB**2+YTB**2)*STPSC
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*AL**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD*PSI/3600/24
            IF ((XSA**2+YTA**2).NE.0) PSI = FAC0/DD*LOG(TAN(D/2))
            AL  = FAC0/(DD*SIN(D))
            TIMEDB =(1+ZL)/C*DD/2*(DDS/DS*AL**2-2*PSI)
            TIMED = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*AL**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD*PSI/3600/24
         ELSEIF  (MODEL.EQ.1) THEN
            PSI = FAC1*DSQRT(XSA**2+YTA**2)*STPSC
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*FAC1**2-2*PSI)
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*FAC1**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*2*PSI/3600/24
            PSI = FAC1*DSQRT((XSB*STPSC)**2+(YTB*STPSC)**2)
            TIMEDB =(1+ZL)/C*DD/2*(DDS/DS*FAC1**2-2*PSI)
            TIMED = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*FAC1**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*2*PSI/3600/24
!
!
            XYAB(1) = XSA
            XYAB(2) = YTA
            XYAB(3) = XSB
            XYAB(4) = YTB
            DTIME = 1D-1/MAXTIME         ! Other value ???
            DO J=1,4
               XYM   = 0.0
               Y0    = 0
               IZ    = 0
               PSI2(J) = 0
               WRITE (6,*) J
               IF (J.EQ.1.OR.J.EQ.3) THEN
                  YI = 0.0
               ELSEIF  (J.EQ.2.OR.J.EQ.4) THEN
                  L  = J-1
                  XI = XYAB(L)
               ENDIF
               DO WHILE (ABS(XYM).LT.ABS(XYAB(J)))
                  PSI0 = Y0
                  DO I = 1,MAXTIME-1
                     IF (J.EQ.1.OR.J.EQ.3) XI = (I+IZ)*DTIME
                     IF (J.EQ.2.OR.J.EQ.4) YI = (I+IZ)*DTIME
                     D = DSQRT(XI**2+YI**2)*STPSC
                     IF (J.EQ.1.OR.J.EQ.3) Y0 = FAC1/D*XI*STPSC
                     IF (J.EQ.2.OR.J.EQ.4) Y0 = FAC1/D*YI*STPSC
                     PSI0 = PSI0+(MOD(I+1,2)*2+MOD(I,2)*4)*Y0
                  ENDDO
                  XYM   = XYM+MAXTIME*DTIME
                  IF (J.EQ.1.OR.J.EQ.3) XI = XYM
                  IF (J.EQ.2.OR.J.EQ.4) YI = XYM
                  D = DSQRT(XI**2+YI**2)*STPSC
                  IF (J.EQ.1.OR.J.EQ.3) Y0 = FAC1/D*XI*STPSC
                  IF (J.EQ.2.OR.J.EQ.4) Y0 = FAC1/D*YI*STPSC
                  PSI2(J) = STPSC*DTIME/3*(PSI0+Y0)+PSI2(J)
                  IZ = IZ+(MAXTIME+1)
               ENDDO
            ENDDO
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*FAC1**2-2*
     &      (PSI2(1)+PSI2(2)))
            TIMEDB = (1+ZL)/C*DD/2*(DDS/DS*FAC1**2-2*
     &      (PSI2(3)+PSI2(4)))
            TIMED2 = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*FAC1**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(1)+PSI2(2))/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*FAC1**2/3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(3)+PSI2(4))/3600/24
            WRITE (6,*) TIMED2/3600/24
         ELSEIF  (MODEL.EQ.2) THEN
            XSAP = XSA*CSP-YTA*SSP
            YTAP = XSA*SSP+YTA*CSP
            AL   = FAC1/(DSQRT(CORE**2+(1-EPS)*XSAP**2+(1+EPS)*
     &      YTAP**2)*STPSC)
            ALX  = (1-EPS)*XSAP*AL*STPSC
            ALY  = (1+EPS)*YTAP*AL*STPSC
            PSI = FAC1*(DSQRT(CORE**2+(1-EPS)*XSAP**2+(1+EPS)*
     &      YTAP**2)*STPSC-CORE*STPSC)
            TIMEDA =(1+ZL)/C*DD/2*(DDS/DS*(ALX**2+ALY**2)-2*PSI)
            WRITE (6,*) (1+ZL)/C*DD*PSI/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALX**2+ALY**2)/3600/24
            XSBP = XSB*CSP-YTB*SSP
            YTBP = XSB*SSP+YTB*CSP
            AL   = FAC1/(DSQRT(CORE**2+(1-EPS)*XSBP**2+(1+EPS)*
     &      YTBP**2)*STPSC)
            ALX  = (1-EPS)*XSBP*AL*STPSC
            ALY  = (1+EPS)*YTBP*AL*STPSC
            PSI = FAC1*(DSQRT(CORE**2+(1-EPS)*XSBP**2+(1+EPS)*
     &      YTBP**2)*STPSC-CORE*STPSC)
            TIMEDB =(1+ZL)/C*DD/2*(DDS/DS*(ALX**2+ALY**2)-2*PSI)
            TIMED = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD*PSI/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALX**2+ALY**2)/3600/24
         ELSEIF  (MODEL.EQ.3.OR.MODEL.EQ.5) THEN
            TIMED2 = (1+ZL)/C*DD*DS/(2*DDS)*((XSA**2+YTA**2)-
     &      (XSB**2+YTB**2))*STPSC**2    !for eta=1,gamma0=0
            WRITE (6,*) ABS(TIMED2/24/3600)
            XYAB(1) = XSA
            XYAB(2) = YTA
            XYAB(3) = XSB
            XYAB(4) = YTB
            IF (MODEL.EQ.3) THEN
               CSP    = 1
               SSP    = 0
               EPS    = 0
               GAMMA  = GAMMA0*STPSC
            ELSEIF  (MODEL.EQ.5) THEN
               GAMMA = GAMMA0/(1+GAMMA0)*STPSC
            ENDIF
!
            DO A=1,2
               J = 2*(A-1)+1
               XSAP  = XYAB(J)*CSP-XYAB(J+1)*SSP
               YTAP  = XYAB(J)*SSP+XYAB(J+1)*CSP
               D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
               XSAG  =  XYAB(J)*CSG-XYAB(J+1)*SSG
               YTAG  = -XYAB(J)*SSG-XYAB(J+1)*CSG
               AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &         STPSC
               ALXP  = AL*(1-EPS)*XSAP
               ALYP  = AL*(1+EPS)*YTAP
               ALXAB(A) =  ALXP*CSP+ALYP*SSP+GAMMA*XSAG
               ALYAB(A) = -ALXP*SSP+ALYP*CSP+GAMMA*YTAG
            ENDDO
            DTIME = 1D-1/MAXTIME         ! Other value ???
            DO J=1,4
               XYM   = 0.0
               Y0    = 0
               L     = 0
               IZ    = 0
               WRITE (6,*) J
               IF (J.EQ.1.OR.J.EQ.3) THEN
                  YI = 0
               ELSEIF  (J.EQ.2.OR.J.EQ.4)THEN
                  L  = J-1
                  XI = XYAB(L)
               ENDIF
               DO WHILE (ABS(XYM).LT.ABS(XYAB(J)))
                  PSI0 = Y0
                  DO I = 1,MAXTIME-1
                     IF (J.EQ.1.OR.J.EQ.3) XI = (I+IZ)*DTIME
                     IF (J.EQ.2.OR.J.EQ.4) YI = (I+IZ)*DTIME
                     XSAP  = XI*CSP-YI*SSP
                     YTAP  = XI*SSP+YI*CSP
                     D     = DSQRT((1+EPS)*XSAP**2+(1-EPS)*YTAP**2)*
     &               STPSC
                     XSAG  =  XI*CSG-YI*SSG
                     YTAG  = -XI*SSG-YI*CSG
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &               COR**ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     IF (J.EQ.1.OR.J.EQ.3) Y0 =  ALXP*CSP+ALYP*SSP+
     &               GAMMA*XSAG
                     IF (J.EQ.2.OR.J.EQ.4) Y0 = -ALXP*SSP+ALYP*CSP+
     &               GAMMA*YTAG
                     PSI0 = PSI0+(MOD(I+1,2)*2+MOD(I,2)*4)*Y0
                  ENDDO
                  XYM   = (XYM+MAXTIME*DTIME)
                  IF (J.EQ.1.OR.J.EQ.3) XI = XYM
                  IF (J.EQ.2.OR.J.EQ.4) YI = XYM
                  XSAP  = XI*CSP-YI*SSP
                  YTAP  = XI*SSP+YI*CSP
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
                  XSAG  =  XI*CSG-YI*SSG
                  YTAG  = -XI*SSG-YI*CSG
                  AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &            STPSC
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  IF (J.EQ.1.OR.J.EQ.3) Y0  =  ALXP*CSP+ALYP*SSP+
     &            GAMMA*XSAG
                  IF (J.EQ.2.OR.J.EQ.4) Y0  = -ALXP*SSP+ALYP*CSP+
     &            GAMMA*YTAG
                  PSI2(J) = DTIME*STPSC/3*(PSI0+Y0)+PSI2(J)
                  IZ    = IZ+(MAXTIME+1)
               ENDDO
            ENDDO
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(1)**2+ALYAB(1)**2)-
     &      2*(PSI2(1)+PSI2(2)))
            TIMEDB = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(2)**2+ALYAB(2)**2)-
     &      2*(PSI2(3)+PSI2(4)))
            TIMED = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALXAB(1)**2+ALYAB(1)**2)
     &      /3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(1)+PSI2(2))/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALXAB(2)**2+ALYAB(2)**2)
     &      /3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(3)+PSI2(4))/3600/24
!
         ELSEIF  (MODEL.EQ.4.OR.MODEL.EQ.6) THEN
            XYAB(1) = XSA
            XYAB(2) = YTA
            XYAB(3) = XSB
            XYAB(4) = YTB
            IF (MODEL.EQ.4) THEN
               PA     = 0
               EPS    = 0
               GAMMA  = GAMMA0*STPSC
            ELSEIF  (MODEL.EQ.6) THEN
               GAMMA = GAMMA0/(1+GAMMA0)*STPSC
            ENDIF
!
            DO A=1,2
               J = 2*(A-1)+1
               XSAP  = XYAB(J)*CSP-XYAB(J+1)*SSP
               YTAP  = XYAB(J)*SSP+XYAB(J+1)*CSP
               D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
               XSAG  =  XYAB(J)*CSG-XYAB(J+1)*SSG
               YTAG  = -XYAB(J)*SSG-XYAB(J+1)*CSG
               D0    = D/CORE
               D1    = 1.155*D0
               D2    = 0.579*D0
               F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
               F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
               AL    = FAC3*(F1-F2)/D
               ALBH  = ALBH0**2/(XYAB(J)**2+XYAB(J+1)**2)
               ALXP  = AL*(1-EPS)*XSAP
               ALYP  = AL*(1+EPS)*YTAP
               ALXAB(A) =  ALXP*CSP+ALYP*SSP+ALBH*XYAB(J)+GAMMA*XSAG
               ALYAB(A) = -ALXP*SSP+ALYP*CSP+ALBH*XYAB(J+1)+GAMMA*YTAG
            ENDDO
            DTIME = 1D-1/MAXTIME         ! Other value ???
            DO J=1,4
               XYM = 0.0
               Y0  = 0
               L   = 0
               IZ  = 0
               WRITE (6,*) J
               IF (J.EQ.1.OR.J.EQ.3) THEN
                  YI = 0
               ELSEIF  (J.EQ.2.OR.J.EQ.4) THEN
                  L  = J-1
                  XI = XYAB(L)
               ENDIF
               DO WHILE (ABS(XYM).LT.ABS(XYAB(J)))
                  PSI0 = Y0
                  DO I = 1,MAXTIME-1
                     IF (J.EQ.1.OR.J.EQ.3) XI = (I+IZ)*DTIME
                     IF (J.EQ.2.OR.J.EQ.4) YI = (I+IZ)*DTIME
                     XSAP  = XI*CSP-YI*SSP
                     YTAP  = XI*SSP+YI*CSP
                     D     = DSQRT((1+EPS)*XSAP**2+(1-EPS)*YTAP**2)
                     XSAG  =  XI*CSG-YI*SSG
                     YTAG  = -XI*SSG-YI*CSG
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XI**2+YI**2)
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     IF (J.EQ.1.OR.J.EQ.3) Y0 =  ALXP*CSP+ALYP*SSP+
     &               ALBH*XI+GAMMA*XSAG
                     IF (J.EQ.2.OR.J.EQ.4) Y0 = -ALXP*SSP+ALYP*CSP+
     &               ALBH*YI+GAMMA*YTAG
                     PSI0 = PSI0+(MOD(I+1,2)*2+MOD(I,2)*4)*Y0
                  ENDDO
                  XYM   = (XYM+MAXTIME*DTIME)
                  IF (J.EQ.1.OR.J.EQ.3) XI = XYM
                  IF (J.EQ.2.OR.J.EQ.4) YI = XYM
                  XSAP  = XI*CSP-YI*SSP
                  YTAP  = XI*SSP+YI*CSP
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                  XSAG  =  XI*CSG-YI*SSG
                  YTAG  = -XI*SSG-YI*CSG
                  D0    = D/CORE
                  D1    = 1.155*D0
                  D2    = 0.579*D0
                  F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                  F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                  AL    = FAC3*(F1-F2)/D
                  ALBH  = ALBH0**2/(XI**2+YI**2)
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  IF (J.EQ.1.OR.J.EQ.3) Y0  =  ALXP*CSP+ALYP*SSP+
     &            ALBH*XI+GAMMA*XSAG
                  IF (J.EQ.2.OR.J.EQ.4) Y0  = -ALXP*SSP+ALYP*CSP+
     &            ALBH*YI+GAMMA*YTAG
                  PSI2(J) = DTIME*STPSC/3*(PSI0+Y0)+PSI2(J)
                  IZ    = IZ+(MAXTIME+1)
               ENDDO
            ENDDO
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(1)**2+ALYAB(1)**2)-
     &      2*(PSI2(1)+PSI2(2)))
            TIMEDB = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(2)**2+ALYAB(2)**2)-
     &      2*(PSI2(3)+PSI2(4)))
            TIMED = TIMEDA-TIMEDB
!	   write (6,*) (1+zl)/c*Dd/2*Dds/Ds*(alxab(1)**2+
!     1        alyab(1)**2)/3600/24
!	   write (6,*) (1+zl)/c*Dd*(psi2(1)+psi2(2))/3600/24
!	   write (6,*) (1+zl)/c*Dd/2*Dds/Ds*(alxab(2)**2+
!     1        alyab(2)**2)/3600/24
!	   write (6,*) (1+zl)/c*Dd*(psi2(3)+psi2(4))/3600/24
!
         ELSEIF  (MODEL.EQ.7) THEN
            XYAB(1) = XSA
            XYAB(2) = YTA
            XYAB(3) = XSB
            XYAB(4) = YTB
!
            DO A=1,2
               J = 2*(A-1)+1
               XSAP  = XYAB(J)*CSP-XYAB(J+1)*SSP
               YTAP  = XYAB(J)*SSP+XYAB(J+1)*CSP
               D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
               XCL   = XYAB(J)-TCL1/STEP
               YCL   = XYAB(J+1)-TCL2/STEP
               DCL   = SQRT(XCL**2+YCL**2)
               ALCL  = BCL/DCL
               D0    = D/CORE
               D1    = 1.155*D0
               D2    = 0.579*D0
               F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
               F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
               AL    = FAC3*(F1-F2)/D
               ALBH  = ALBH0**2/(XYAB(J)**2+XYAB(J+1)**2)
               ALXP  = AL*(1-EPS)*XSAP
               ALYP  = AL*(1+EPS)*YTAP
               ALXAB(A) =  ALXP*CSP+ALYP*SSP+ALBH*XYAB(J)+ALCL*XCL
               ALYAB(A) = -ALXP*SSP+ALYP*CSP+ALBH*XYAB(J+1)+ALCL*YCL
            ENDDO
            DTIME = 1D-1/MAXTIME         ! Other value ???
            DO J=1,4
               XYM = 0.0
               Y0  = 0
               L   = 0
               IZ  = 0
               WRITE (6,*) J
               IF (J.EQ.1.OR.J.EQ.3) THEN
                  YI = 0
               ELSEIF  (J.EQ.2.OR.J.EQ.4) THEN
                  L  = J-1
                  XI = XYAB(L)
               ENDIF
               DO WHILE (ABS(XYM).LT.ABS(XYAB(J)))
                  PSI0 = Y0
                  DO I = 1,MAXTIME-1
                     IF (J.EQ.1.OR.J.EQ.3) XI = (I+IZ)*DTIME
                     IF (J.EQ.2.OR.J.EQ.4) YI = (I+IZ)*DTIME
                     XSAP  = XI*CSP-YI*SSP
                     YTAP  = XI*SSP+YI*CSP
                     D     = DSQRT((1+EPS)*XSAP**2+(1-EPS)*YTAP**2)
                     XCL   = XI-TCL1/STEP
                     YCL   = YI-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     D0    = D/CORE
                     D1    = 1.155*D0
                     D2    = 0.579*D0
                     F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                     F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                     AL    = FAC3*(F1-F2)/D
                     ALBH  = ALBH0**2/(XI**2+YI**2)
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     IF (J.EQ.1.OR.J.EQ.3) Y0 =  ALXP*CSP+ALYP*SSP+
     &               ALBH*XI+ALCL*XCL
                     IF (J.EQ.2.OR.J.EQ.4) Y0 = -ALXP*SSP+ALYP*CSP+
     &               ALBH*YI+ALCL*YCL
                     PSI0 = PSI0+(MOD(I+1,2)*2+MOD(I,2)*4)*Y0
                  ENDDO
                  XYM   = (XYM+MAXTIME*DTIME)
                  IF (J.EQ.1.OR.J.EQ.3) XI = XYM
                  IF (J.EQ.2.OR.J.EQ.4) YI = XYM
                  XSAP  = XI*CSP-YI*SSP
                  YTAP  = XI*SSP+YI*CSP
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)
                  XCL   = XI-TCL1/STEP
                  YCL   = YI-TCL2/STEP
                  DCL   = SQRT(XCL**2+YCL**2)
                  ALCL  = BCL/DCL
                  D0    = D/CORE
                  D1    = 1.155*D0
                  D2    = 0.579*D0
                  F1    = 53.2468/D1*(SQRT(1+D1**2)-1)
                  F2    = 44.0415/D2*(SQRT(1+D2**2)-1)
                  AL    = FAC3*(F1-F2)/D
                  ALBH  = ALBH0**2/(XI**2+YI**2)
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  IF (J.EQ.1.OR.J.EQ.3) Y0  =  ALXP*CSP+ALYP*SSP+
     &            ALBH*XI+ALCL*XCL
                  IF (J.EQ.2.OR.J.EQ.4) Y0  = -ALXP*SSP+ALYP*CSP+
     &            ALBH*YI+ALCL*YCL
                  PSI2(J) = DTIME*STPSC/3*(PSI0+Y0)+PSI2(J)
                  IZ    = IZ+(MAXTIME+1)
               ENDDO
            ENDDO
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(1)**2+ALYAB(1)**2)-
     &      2*(PSI2(1)+PSI2(2)))
            TIMEDB = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(2)**2+ALYAB(2)**2)-
     &      2*(PSI2(3)+PSI2(4)))
            TIMED = TIMEDA-TIMEDB
!	   write (6,*) (1+zl)/c*Dd/2*Dds/Ds*(alxab(1)**2+alyab(1)**2)
!     1       /3600/24
!	   write (6,*) (1+zl)/c*Dd*(psi2(1)+psi2(2))/3600/24
!	   write (6,*) (1+zl)/c*Dd/2*Dds/Ds*(alxab(2)**2+alyab(2)**2)
!     !       /3600/24
!	   write (6,*) (1+zl)/c*Dd*(psi2(3)+psi2(4))/3600/24
!
         ELSEIF  (MODEL.EQ.8) THEN
            XYAB(1) = XSA
            XYAB(2) = YTA
            XYAB(3) = XSB
            XYAB(4) = YTB
!
            DO A=1,2
               J = 2*(A-1)+1
               XSAP  = XYAB(J)*CSP-XYAB(J+1)*SSP
               YTAP  = XYAB(J)*SSP+XYAB(J+1)*CSP
               D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
               XCL   = XYAB(J)-TCL1/STEP
               YCL   = XYAB(J+1)-TCL2/STEP
               DCL   = SQRT(XCL**2+YCL**2)
               ALCL  = BCL/DCL
               AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &         STPSC
               ALXP  = AL*(1-EPS)*XSAP
               ALYP  = AL*(1+EPS)*YTAP
               ALXAB(A) =  ALXP*CSP+ALYP*SSP+ALCL*XCL
               ALYAB(A) = -ALXP*SSP+ALYP*CSP+ALCL*YCL
            ENDDO
            DTIME = 1D-1/MAXTIME         ! Other value ???
            DO J=1,4
               XYM = 0.0
               Y0  = 0
               IZ  = 0
               L   = 0
               WRITE (6,*) J
               IF (J.EQ.1.OR.J.EQ.3) THEN
                  YI = 0
               ELSEIF  (J.EQ.2.OR.J.EQ.4)THEN
                  L  = J-1
                  XI = XYAB(L)
               ENDIF
               DO WHILE (ABS(XYM).LT.ABS(XYAB(J)))
                  PSI0 = Y0
                  DO I = 1,MAXTIME-1
                     IF (J.EQ.1.OR.J.EQ.3) XI = (I+IZ)*DTIME
                     IF (J.EQ.2.OR.J.EQ.4) YI = (I+IZ)*DTIME
                     XSAP = XI*CSP-YI*SSP
                     YTAP  = XI*SSP+YI*CSP
                     XCL   = XI-TCL1/STEP
                     YCL   = YI-TCL2/STEP
                     DCL  = SQRT(XCL**2+YCL**2)
                     ALCL = BCL/DCL
                     D     = DSQRT((1+EPS)*XSAP**2+(1-EPS)*YTAP**2)*
     &               STPSC
                     AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-
     &               COR**ETA)*STPSC
                     ALXP  = AL*(1-EPS)*XSAP
                     ALYP  = AL*(1+EPS)*YTAP
                     IF (J.EQ.1.OR.J.EQ.3) Y0 =  ALXP*CSP+ALYP*SSP+
     &               ALCL*XCL
                     IF (J.EQ.2.OR.J.EQ.4) Y0 = -ALXP*SSP+ALYP*CSP+
     &               ALCL*YCL
                     PSI0 = PSI0+(MOD(I+1,2)*2+MOD(I,2)*4)*Y0
                  ENDDO
                  XYM   = (XYM+MAXTIME*DTIME)
                  IF (J.EQ.1.OR.J.EQ.3) XI = XYM
                  IF (J.EQ.2.OR.J.EQ.4) YI = XYM
                  XSAP  = XI*CSP-YI*SSP
                  YTAP  = XI*SSP+YI*CSP
                  XCL   = XI-TCL1/STEP
                  YCL   = YI-TCL2/STEP
                  DCL   = SQRT(XCL**2+YCL**2)
                  ALCL  = BCL/DCL
                  D     = DSQRT((1-EPS)*XSAP**2+(1+EPS)*YTAP**2)*STPSC
                  AL    = FAC2/D**2*((D**2+COR**2)**(ETA/2)-COR**ETA)*
     &            STPSC
                  ALXP  = AL*(1-EPS)*XSAP
                  ALYP  = AL*(1+EPS)*YTAP
                  IF (J.EQ.1.OR.J.EQ.3) Y0  =  ALXP*CSP+ALYP*SSP+ALCL*
     &            XCL
                  IF (J.EQ.2.OR.J.EQ.4) Y0  = -ALXP*SSP+ALYP*CSP+ALCL*
     &            YCL
                  PSI2(J) = DTIME*STPSC/3*(PSI0+Y0)+PSI2(J)
                  IZ    = IZ+(MAXTIME+1)
               ENDDO
            ENDDO
            TIMEDA = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(1)**2+ALYAB(1)**2)-
     &      2*(PSI2(1)+PSI2(2)))
            TIMEDB = (1+ZL)/C*DD/2*(DDS/DS*(ALXAB(2)**2+ALYAB(2)**2)-
     &      2*(PSI2(3)+PSI2(4)))
            TIMED = TIMEDA-TIMEDB
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALXAB(1)**2+
     &      ALYAB(1)**2)/3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(1)+PSI2(2))/3600/24
            WRITE (6,*) (1+ZL)/C*DD/2*DDS/DS*(ALXAB(2)**2+
     &      ALYAB(2)**2)/3600/24
            WRITE (6,*) (1+ZL)/C*DD*(PSI2(3)+PSI2(4))/3600/24
         ENDIF
      ENDIF
!
!
      WRITE (6,'(a)') 'Writing file "glens.map"...'
      OPEN (UNIT=1,FILE='glens.map',FORM='formatted',
     &STATUS='unknown')
      WRITE (1,'(a)') 'cle; set mar 0 0 0.1'
      WRITE (1,'(a)') 'set box 4 25 4 20; lim -10 10 0 12'
      WRITE (1,'(a)') 'set exp 1.0; dra tex -12 11 "\\ICosmology:"'//
     &' 6 /user'
      WRITE (1,'(3(a,f5.3),a,f5.1,a)') 'dra tex -8 11 "'//
     &'\\ID\DA\B\UO\US = ',DS/GPC,
     &' Gpc___D\DA\B\UO\UL = ',DD/GPC,' Gpc___D\DA\B\UL\US = ',
     &DDS/GPC,' Gpc_____H\Do = ',HUBBLE,' km/s/Mpc" 6 /user'
      WRITE (1,'(a,f6.4,a)') 'dra tex -5.9 10.4 "\\Iz\UO\US = ',ZS,
     &'" 5 /user'
      WRITE (1,'(a,f6.4,a)') 'dra tex -0.7 10.4 "\\Iz\UO\UL = ',ZL,
     &'" 5 /user'
      WRITE (1,'(3(a,f4.2),a,i2,a)') 'dra tex 8 10.4 "\\I\GW\DM = ',
     &OM,'__\GW\DR = ',OR,'__\GW\DV = ',OV,'__k=',K,'" 5 /user'
      WRITE (1,'(a)') 'dra tex -12 9.5 "\\ILens:" 6 /user'
      IF (MODEL.NE.0) THEN
         WRITE (1,'(a,f6.1,a,f7.2,a,f7.2,a)') 'dra tex -8 9.5 "'//
     &   '\\I\Gs\DV = ',SIGMAV,' km/s___M = ',M,
     &   ' (10\U1\U2M\D\G0)___\GS\Dc = ',C*C/(4*PI*GC)*DDS/
     &   (DS*DD)/1.98E33*(GPC/1E9)**2,' M\D\G0 pc\U-\U2" 6 /user'
         IF (MODEL.EQ.3) THEN
            WRITE (1,'(2(a,f5.2),a,a,f5.3,a,f5.1,a,f6.3,a)')
     &      'dra tex -8 8.9 "\\I\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)','___\Gg = ',GAMMA0,'___\Gf\D\Gg =',
     &      PHIG,'\Uo___\Gh = ',ETA,'" 6 /user'
         ENDIF
         IF (MODEL.EQ.4) THEN
            WRITE (1,'(2(a,f5.2),a,f5.3,a,f5.3,a)')
     &      'dra tex -8 8.9 "\\I\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)___M\Db\Dh = ',MBH,' (10\U1\U2M\D\G0)" 6 /user'
            WRITE (1,'(a,f5.3,a,f7.2,a)') 'dra tex -8 8.3 "\\I\Gg = ',
     &      GAMMA0,'___\Gf\D\Gg = ',PHIG,'\Uo" 6 /user'
         ENDIF
         IF (MODEL.EQ.2) THEN
            WRITE (1,'(2(a,f5.3),a,f5.3,a,f5.1,a)')
     &      'dra tex -8 8.9 "\\I\Ge = ',EPS,'___\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)____PA = ',PA,'\Uo" 6 /user'
         ENDIF
         IF (MODEL.EQ.5) THEN
            WRITE (1,'(2(a,f5.2),a,f6.3,a,f6.1,a)')
     &      'dra tex -8 8.9 "\\I\Ge = ',EPS,'___\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)____PA = ',PA,'\Uo" 6 /user'
            WRITE (1,'(a,f5.3,a,f7.2,a,f6.3,a)')
     &      'dra tex -8 8.3 "\\I\Gg = ',GAMMA0,'___\Gf\D\Gg = ',
     &      PHIG,'\Uo___\Gh = ',ETA,'" 6 /user'
         ENDIF
         IF (MODEL.EQ.6) THEN
            WRITE (1,'(2(a,f5.3),a,f5.3,a,f5.1,a)')
     &      'dra tex -8 8.9 "\\I\Ge = ',EPS,'___\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)____PA = ',PA,'\Uo" 6 /user'
            WRITE (1,'(2(a,f4.2),a,f6.1,a)')
     &      'dra tex -8 8.3 "\\IM\Db\Dh = ', MBH,
     &      '(10\U1\U2M\D\G0)___\Gg = ',
     &      GAMMA0,'___\Gf\D\Gg =',PHIG,'\Uo" 6 /user'
         ENDIF
         IF (MODEL.EQ.7) THEN
            WRITE (1,'(2(a,f5.2),a,f6.3,a,f6.1,a)')
     &      'dra tex -8 8.9 "\\I\Ge = ',EPS,'___\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)____PA = ',PA,'\Uo" 6 /user'
            WRITE (1,'(2(a,f5.1),a,f5.1,a,f5.3,a)')
     &      'dra tex -8 8.3 "\\I\Gs\DC\Dl = ',SIGMACL,
     &      ' km/s___Cl.-Pos. = ',-TCL2,''''',',TCL1,
     &      '''''___M\Db\Dh = ', MBH,'(10\U1\U2M\D\G0)" 6 /user'
         ENDIF
         IF (MODEL.EQ.8) THEN
            WRITE (1,'(2(a,f5.2),a,f6.3,a,f6.1,a,f7.3,a)')
     &      'dra tex -8 8.9 "\\I\Ge = ',EPS,'___\Gx\Dc = ',
     &      CORE*STEP,''''' (= ',DD/GPC*CORE*STEP*SEC*1E6,
     &      ' kpc)____PA = ',PA,'\Uo___\Gh = ',ETA,'" 6 /user'
            WRITE (1,'(a,f5.1,a,f5.2,a,f5.2,a)')
     &      'dra tex -8 8.3 "\\I\Gs\DC\Dl = ',SIGMACL,
     &      ' km/s___Cl.-Pos. = ',-TCL2,''''',',TCL1,
     &      ''''' " 6 /user'
         ENDIF
!
      ELSE
         WRITE (1,'(a,f4.2,a)') 'dra tex -8 9.5 "\\IM = ',M,
     &   ' (10\U1\U2M\D\G0)" 6 /user'
      ENDIF
      WRITE (1,'(a)') 'dra tex -12 7.3 "\\ISource:" 6 /user'
      IF (TEST.EQ.2) THEN
         WRITE (1,'(a)') 'dra tex -8 7.3 "\\IFROM FILE" 6 /user'
      ENDIF
      IF (TEST.EQ.1) THEN
         WRITE (1,'(a)') 'dra tex -8 7.3 "\\IINTERNAL GRID" 6 /user'
      ENDIF
      IF (TEST.EQ.0) THEN
         IF (FWHM2.EQ.0) THEN
            WRITE (1,'(3(a,f6.3),a)')
     &      'dra tex -8 7.3 "\\IGaussian____Pos = ',XSH,''''',',
     &      YSH,'''''___FWHM = ',FWHM1,'''''" 6 /user'
         ELSE
            WRITE (1,'(5(a,f6.3),a)')
     &      'dra tex -8 7.3 "\\IGaussian____Pos = ',XSH,''''',',
     &      YSH,'''''___FWHM1 = ',
     &      FWHM1,'''''__FWHM2 = ',FWHM2,'''''" 6 /user'
         ENDIF
      ENDIF
!	write (1,'(a,f5.1,a)')
!     1  'dra tex -12 6.5 "Time delay:___ \GDt = ',abs(timed/24/3600),
!     2  'days" 6 /user'
      IF (MODEL.EQ.0) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  SCHWARZSCHILD LENS" 5 /user'
      ELSEIF  (MODEL.EQ.1) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  SINGULAR ISOTHERMAL SPHERE" 5 /user'
      ELSEIF  (MODEL.EQ.2) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  ELLIPTICAL POTENTIAL" 5 /user'
      ELSEIF  (MODEL.EQ.3) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  SOFTENED POWER-LAW SPHERE (SPLS)" 5 /user'
      ELSEIF  (MODEL.EQ.4) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  KING PROFILE (FGS)" 5 /user'
      ELSEIF  (MODEL.EQ.5) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  SPEMD" 5 /user'
      ELSEIF  (MODEL.EQ.6) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  FGSE" 5 /user'
      ELSEIF  (MODEL.EQ.7) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  FGSE+CL" 5 /user'
      ELSEIF  (MODEL.EQ.8) THEN
         WRITE (1,'(a)') 'set exp 1.2; dra tex 0 12 "LENS MODEL'//
     &   ':  SPEMD+CL" 5 /user'
      ENDIF
      IF (FILE) THEN
         WRITE (1,'(a)') 'set exp 0.7; set box  4 13 3 12; '//
     &   'ima qe.gdf; lim /rg; ima qe.gl; plot; '//
     &   'box /un sec'
      ELSE
         WRITE (1,'(a)') 'set exp 0.7; set box  4 13 3 12; '//
     &   'ima qe.gdf; lim /rg; plot; box /un sec'
      ENDIF
      WRITE (1,'(a)') 'col x 1 /file ca.dat; let m x[nxy] /new int'
      WRITE (1,'(a)') 'for i 1 to m; pen 0 /col 3^(i-1) ; '//
     &'col x 2*i-1 y 2*i /line 1 360; poin; next'
      WRITE (1,'(a)') 'pen 0 /col 0; lim 6 -6 -6 6'
      WRITE (1,'(a)') 'dra rel 4 5 /use; dra lin 4 4 /use; '//
     &'dra lin 5 4 /use; dra tex 4 5.5 "N" 5 /use'
      WRITE (1,'(a)') 'dra tex 5.5 4 "E" 5 /use; set exp 1.'
      WRITE (1,'(a)') 'label "Source Plane" /x; set box 5 13 3 12'//
     &'; label "FOV (arcs)" /y; set exp 0.7'
      WRITE (1,'(a)') 'set box 16 25 3 12; ima le.gdf; set bla '//
     &'1.23456e34 1e33; lim /rg; plot; box /un sec'
      WRITE (1,'(a)') 'col x 1 /file cl.dat'
      WRITE (1,'(a)') 'for i 1 to m; pen 0 /col 3^(i-1); col x '//
     &'2*i-1 y 2*i /line 1 360; poin; next'
      WRITE (1,'(a)') 'pen 0 /col 0; set exp 1.0; label "Lens '//
     &'Plane" /x'
      WRITE (1,'(a,f6.2,a)') 'dra tex 0 -2 "\\I Amplification = ',
     &SLE/SQE,'" 5 /box 2'
      IF (DELAY.EQ.1) THEN
         WRITE (1,'(a,f10.4,a)') 'dra tex -12 -2 "\\I Time delay = ',
     &   ABS(TIMED/3600/24),' days" 5 /box 2; set exp 0.7'
      ELSE
         WRITE (1,'(a,f10.4,a)') 'dra tex -12 -2 "\\I Time delay '//
     &   '= DISABLED" 5 /box 2; set exp 0.7'
      ENDIF
      WRITE (1,'(a)') 'dra tex 4 0 "\\ISoftware credit: Krips,M. '//
     &'and Neri,R. - IRAM (2013)" 5 90 /box 6'
      CLOSE (UNIT=1)
!
      WRITE (6,'(a,e15.7)') 'QE flux: ',SQE
      WRITE (6,'(a,e15.7,a,f12.5)') 'LE flux: ',SLE,
     &'  Amplification: ',SLE/SQE
      WRITE (6,'(a,i5,a,f0.1,a)') 'Number of invalid points:       ',
     &IC,'  (',100*IC/(2*NDIM+1.)**2,'%)   >  increase MAXPIX'
!
      END PROGRAM GL
!
!
      FUNCTION INSIDE (X,Y,MGON,GONS,BOUND)
!----------------------------------------------------------------------
!	Find if a point is within a n-gon
! Arguments
!     x	r*8	Coordinate of point to test	        Input
!     y	r*8					        Input
!     mgon	i	Dimension of GONS		Input
!     gons	r*8(*)	Description of polygon		Input
!     bound	r*8(4)	Polygon boundary   	        Input
!     inside	l	Point is within ? 		Output
!----------------------------------------------------------------------
      INTEGER INDX,I,MGON
      REAL*8  XOUT,D,XX,X,Y,GONS(MGON+1,4),BOUND(4)
      LOGICAL INSIDE, YIN
!
      IF (X.LT.BOUND(1).OR.X.GT.BOUND(2).OR.Y.LT.BOUND(3).OR.
     &Y.GT.BOUND(4)) THEN
         INSIDE = .FALSE.
         RETURN
      ENDIF
      INSIDE = .TRUE.
      INDX = 0
      XOUT = BOUND(1)-0.01*(BOUND(2)-BOUND(1))
!
      DO I=1,MGON
         IF (X.EQ.GONS(I,1).AND.Y.EQ.GONS(I,2)) THEN
            RETURN
         ENDIF
         IF (Y.LE.GONS(I,2).AND.Y.GE.GONS(I+1,2)) THEN
            YIN = .TRUE.
         ELSEIF (Y.GE.GONS(I,2).AND.Y.LE.GONS(I+1,2)) THEN
            YIN = .TRUE.
         ELSE
            YIN = .FALSE.
         ENDIF
         IF (YIN) THEN
            IF (GONS(I,3).NE.0) THEN
               D = GONS(I,4)/GONS(I,3)
               IF (D.NE.0D0) THEN
                  XX = (Y-GONS(I,2))/D+GONS(I,1)
                  IF (XX.LT.X) THEN
                     INDX = INDX+1
                  ELSEIF (XX.EQ.X) THEN
                     RETURN
                  ENDIF
               ELSE
                  IF (X.GE.GONS(I,1).AND.X.LE.GONS(I+1,1)) THEN
                     RETURN
                  ELSEIF (X.LE.GONS(I,1).AND.X.GE.GONS(I+1,1)) THEN
                     RETURN
                  ENDIF
               ENDIF
            ELSE
               IF (GONS(I,1).LT.X) THEN
                  INDX = INDX+1
               ELSEIF (GONS(I,1).EQ.X) THEN
                  RETURN
               ENDIF
            ENDIF
         ENDIF
      ENDDO
!
      INSIDE = MOD(INDX,2).EQ.1
      END
!
      SUBROUTINE READ_IMAGE (NAME,N,ARRAY,XMI,XMA,YMI,YMA,STPSC,
     &FRAC,NORM)
      USE GILDAS_DEF
      USE IMAGE_DEF
      USE GKERNEL_INTERFACES
      TYPE(GILDAS) X
      INTEGER*4 N
      INTEGER(KIND=INDEX_LENGTH) BLC(4),TRC(4)
      INTEGER(KIND=ADDRESS_LENGTH) IP
      INTEGER*4 XMI,XMA,YMI,YMA
      REAL*4 MEMORY(1)
      REAL*8 ARRAY(-N:N,-N:N),STPSC,FRAC,NORM
      CHARACTER NAME*(*)
      LOGICAL ERROR
!
      call gildas_null (x)
      x%file = name
      x%gil%ndim = 2  
      call gdf_read_gildas(x, name, ' ', error)
!      X%FILE = NAME
!      CALL GDF_GEIS (X%LOCA%ISLO,ERROR)
!      CALL GDF_REIS (X%LOCA%ISLO,X%CHAR%TYPE,X%FILE,X%GIL%FORM,
!     &  X%LOCA%SIZE,ERROR)
!      CALL GDF_READ (X,X%LOCA%ISLO,ERROR)
!      CALL GDF_GET_DATA (X,ERROR)
      IF (X%GIL%DIM(1).NE.2*N+1.OR.X%GIL%DIM(2).NE.2*N+1) THEN
         PRINT *,'Readjust file dimension or program memory...'
         CALL SYSEXI (FATALE)
      ENDIF
!      IP = GAG_POINTER(X%LOCA%ADDR,MEMORY)
!      CALL GL_READ  (MEMORY(IP),ARRAY,N)
!      CALL GDF_FRIS  (X%LOCA%ISLO,ERROR)
      call gl_read (x%r2d, array, n)
      deallocate (x%r2d)
!
      XMA = (N+((1-X%GIL%REF(1))*X%GIL%INC(1)+X%GIL%VAL(1))/STPSC) 
     & /FRAC+1
      XMI = (N+((X%GIL%DIM(1)-X%GIL%REF(1))*X%GIL%INC(1)+X%GIL%VAL(1))
     &  /STPSC)/FRAC+1
      YMA = (N-((1-X%GIL%REF(2))*X%GIL%INC(2)+X%GIL%VAL(2))/STPSC) 
     & /FRAC+1
      YMI = (N-((X%GIL%DIM(2)-X%GIL%REF(2))*X%GIL%INC(2)+X%GIL%VAL(2))
     &  /STPSC)/FRAC+1
      NORM = ABS(X%GIL%INC(2)*X%GIL%INC(1))*(180*3600/(4*ATAN(1D0)))**2
      RETURN
      END SUBROUTINE READ_IMAGE
!
      SUBROUTINE GL_READ (INP,OUT,N)
      INTEGER N,I,J
      REAL*4  INP(-N:N,-N:N)
      REAL*8  OUT(-N:N,-N:N)
!
      DO I=-N,N
         DO J=-N,N
            OUT(I,J) = INP(J,I)
         ENDDO
      ENDDO
      RETURN
      END
!
      SUBROUTINE WRITE_IMAGE (NAME,N,ARRAY,STEP)
      USE GILDAS_DEF
      USE GBL_CONSTANT
      USE IMAGE_DEF
      USE GKERNEL_INTERFACES
      TYPE(GILDAS) X
      INTEGER*4   N,I,J
      INTEGER(KIND=ADDRESS_LENGTH) IP
      INTEGER(KIND=INDEX_LENGTH) BLC(4),TRC(4)
      REAL*8  ARRAY(-N:N,-N:N),STEP
      REAL*4  VALUE, MEMORY(1),rmin,rmax
      CHARACTER NAME*(*)
      LOGICAL ERROR
      DATA BLC /0,0,0,0/, TRC /0,0,0,0/
!
      CALL GILDAS_NULL(X)
      call maxmin (array,n,rmax,rmin)
!
      X%FILE = NAME(1:6)
      X%CHAR%TYPE = 'GILDAS_IMAGE'
      X%CHAR%SYST = 'EQUATORIAL'
      X%GIL%TYPE_GDF = code_gdf_image
      X%GIL%PTYP = P_AZIMUTHAL
      X%CHAR%NAME = 'GL'
      X%CHAR%UNIT = 'NONE'
      X%CHAR%CODE(1) = 'AZIMUTH'
      X%CHAR%CODE(2) = 'ELEVATION'
!
      X%GIL%DIM(1) = 2*N+1
      X%GIL%DIM(2) = 2*N+1
      X%GIL%NDIM = 2
!
      X%LOCA%SIZE = X%GIL%DIM(1)*X%GIL%DIM(2)
      X%GIL%REF(1) = N+1
      X%GIL%VAL(1) = 0
      X%GIL%INC(1) = -STEP*4*ATAN(1D0)/180D0/3600D0
      X%GIL%REF(2) = N+1
      X%GIL%VAL(2) = 0
      X%GIL%INC(2) = +STEP*4*ATAN(1D0)/180D0/3600D0
      X%GIL%FORM = FMT_R4
      X%GIL%BVAL = 1.23456E34
      X%GIL%EVAL = 1
      X%GIL%RMAX = RMAX
      X%GIL%RMIN = RMIN
!
      X%GIL%BLAN_WORDS = 2
      X%GIL%EXTR_WORDS = 2
      X%GIL%DESC_WORDS = 18
      X%GIL%POSI_WORDS = 12
      X%GIL%PROJ_WORDS = 9
      X%GIL%SPEC_WORDS = 12
      X%GIL%RESO_WORDS = 3
!
      call gdf_create_image (x, error)
      call gdf_allocate (x, error)
      call gl_write (x%r2d,array,n)
      call gdf_write_data (x, x%r2d, error)
      RETURN
      END SUBROUTINE WRITE_IMAGE
!
      SUBROUTINE maxmin (INP,N,rmin,rmax)
      INTEGER N,I,J
      REAL*8  INP(-N:N,-N:N)
      REAL*4  rmin,rmax
!
      rmin =  1e10
      rmax = -1e10
      DO I=-N,N
         DO J=-N,N
            rmin = amin1(rmin,real(inp(j,i)))
            rmax = amax1(rmax,real(inp(j,i)))
         ENDDO
      ENDDO
      RETURN
      END

      SUBROUTINE GL_WRITE (OUT,INP,N)
      INTEGER N,I,J
      REAL*8  INP(-N:N,-N:N)
      REAL*4  OUT(-N:N,-N:N)
!
      DO I=-N,N
         DO J=-N,N
            OUT(I,J) = INP(J,I)
         ENDDO
      ENDDO
      RETURN
      END
!
! Buchalter et al. ApJ 494, 503, 1998
!
      SUBROUTINE ZDA (ZS,SK)
      INTEGER I, MAXN,IZ,K
      PARAMETER (MAXN = 2048)
      REAL*8 GPC,C,Q,O,OV,OM,OR,HUBBLE
      REAL*8 DZ,ZM,ZS,ZO,ZR,SC0,SC,SK,ZA
      COMMON /COSMO/ HUBBLE,GPC,C,Q,O,OV,OM,OR,K
!
!	write (6,*) zs
!
      DZ = 1D-3*ZS/MAXN
      IZ = 0
      ZA = 0
      ZM = 0
      ZR = 1
      DO WHILE (ZM.LT.ZS)
         ZO = ZR
         DO I = 1,MAXN-1
            ZR = (I+IZ)*DZ
            ZR = 1/SQRT((1-O)*(1+ZR)**2+OV+OM*(1+ZR)**3+OR*(1+ZR)**4)
            ZO = ZO+(MOD(I+1,2)*2+MOD(I,2)*4)*ZR
         ENDDO
         ZM = ZM+MAXN*DZ
         ZR = 1/SQRT((1-O)*(1+ZM)**2+OV+OM*(1+ZM)**3+OR*(1+ZM)**4)
         ZA = DZ/3*(ZO+ZR)+ZA
         IZ = IZ+(MAXN+1)
      ENDDO
      SC0 = C/HUBBLE/(1+ZM)/1E8
      IF ((O-1).NE.0) SC  = SQRT(K/(O-1))
      IF (K.GT.0) SK = SIN(ZA/SC)*SC0*SC ! in Gpc
      IF (K.EQ.0) SK = ZA*SC0            ! in Gpc
      IF (K.LT.0) SK = SINH(ZA/SC)*SC0*SC! in Gpc
!	zs = zm
!	write (6,*) sk
      RETURN
      END
