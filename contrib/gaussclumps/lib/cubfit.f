	 subroutine cubfit (image,nimage,npts,nterms,a,sigmaa,
     &               flambdin,imagfit,chisqr,eps,itmax,points,s0,
     &               conv,p0,imax,ap,ap1)
        use gildas_def
        use gkernel_interfaces
	integer(kind=4), intent(in) :: nimage
	real*4 image(nimage),imagfit(nimage),points(3,nimage)
	dimension a(11),sigmaa(11),s0(3)
	logical conv
	! Global
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11),cent(3),wid(3)
	! Local
        integer(kind=4), allocatable :: ipt(:)
        real(kind=4), allocatable :: weight(:)
        integer :: ier
        logical :: error
        real*4 x(3)
	real*4 peak
	
c Allocate working buffers
        error = .false.
        allocate(ipt(nimage),weight(nimage),stat=ier)
        if (failed_allocate('CUBFIT','ipt/weight arrays',ier,error))
     &    call sysexi(fatale)

c select positions with at least p0 times peak intensity
c  point to those with ipt(i)
	amax=image(imax)*p0
	npsel=0
	wsum=0.
	do 1 i=1,npts
	if (image(i).ge.amax) then
		do 11 k=1,3
11		x(k)=points(k,i)-a(7+k)
		d2=(x(1)*x(1)+x(2)*x(2))*2.772589/(wid(1)+a(1)**2)
		d2=d2+x(3)*x(3)*2.772589/(wid(3)+a(3)**2)
		if (d2/ap1.le.0.6932) then
		  npsel=npsel+1
		  ipt(npsel)=i
		  weight(i)=expz(-d2/ap)
		  wsum=wsum+weight(i)
		endif
	endif
1	continue
	write (2,*) 
     &    'I-CUBFIT, ',npsel,' points selected with > ',p0,' * peak, ',
     &    ' within ',sqrt(ap1),' * initial fwhm. sum of weights ',wsum

	flambda=flambdin

	call curfit (image,npsel,nterms,a,sigmaa,flambda,
     &               imagfit,chisqr,eps,itmax,points,
     &               conv,weight,ipt,s0,wsum)
	do 2 i=1,npts
2	imagfit(i)=functn(i,a,points,nterms)-a(11)**2

        deallocate(ipt,weight)

	return
	end
