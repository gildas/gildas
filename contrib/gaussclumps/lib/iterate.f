	subroutine iterate(image,nimage,imax,iclump,thresh,itmaxcl,
     &    iter,rms,sum,minsum)

        real*4  minsum
	integer(kind=4), intent(in) :: nimage
	real*4  image(nimage)
	logical iter
        save sum_thresh

	iter=.true.

        if (iclump.eq.1) sum_thresh = minsum*sum
	if (image(imax).lt.thresh/rms) then
		write (2,*) 
     &    'I-ITERATE, threshhold reached after ',iclump,' iterations '
		iter=.false.
	else if (iclump.gt.itmaxcl) then
		write (2,*)
     &          'I-ITERATE, maximum number of clumps; stop iterating '
		iter=.false.
        else if (sum.le.sum_thresh) then
		write (2,*)
     &          'I-ITERATE, integrated intensity lower than ',minsum,
     &          '% of start value after ',iclump,' iterations.' 
		iter=.false.
	endif

	return
	end
