	subroutine listclump (iclump,a,sigmaa,imafile,
     &                        chisqr,imgname,rms)

	common /termpointer/ lista,peak,cent,wid
	dimension lista(11),cent(3),wid(3)
	real*4 peak

	dimension a(11),sigmaa(11),b(11)

	character*(*) imafile
	character*12 sig(11)
	character*12 imgname

	logical first
	data first /.true./
	integer wp(11),bwp(11)
	data wp /8,9,10,7,1,2,4,5,6,3,11/
	data bwp /8,9,10,7,1,2,4,6,5,11,3/

	if (first) then
	  write (2,'(1x,a,a)') ' imagefile: ',imafile
	  write (2,'(1x,a)') imgname
	  write (2,1000)
	  write (3,'(1x,a,a)') ' imagefile: ',imafile
	  write (3,'(1x,a)') imgname
	  write (3,2000)
	  first=.false.
	endif

	do 1 i=1,11
1	sig(i)='            '
	do 2 i=1,11
	li=lista(i)
	if (li.ne.0) then
	  write (sig(li),'(1p,g11.4)') sigmaa(li)
	endif
2	continue

	call convert (a,b,rms)

	write (2,1010) iclump,(b(wp(i)),i=1,10),(a(wp(i)),i=1,11),
     &  chisqr,(sig(wp(i)),i=1,11)

	write (3,2010) iclump,(b(bwp(i)),i=1,11)

1000	format (1x,'   clump   ','      x     ','      y     ',
     &  '      v     ','  amplitude ','   delta x  ','   delta y  ',
     &  '     phi    ','    dv/dr   ','    fwhm    ','   delta v  ',//,
     &  '           ','     x     ','     y     ',
     &  '     v     ',' amplitude ','    a11    ','    a22    ',
     &  '    a12    ','   dv/dx   ','   dv/dy   ','    a33    ',/,
     &          1x,' chisqr   ',/)
1010	format 
     &  (/,1x,i5,6x,1p,10(g12.5),//,11x,11(g11.4),/,1x,g10.4,11a11)
2000	format (1x,' no.','       x  ','       y  ',
     &  '     v  ','    amp.  ','     dx ','     dy ',
     &  '   phi ','  fwhm ','    dv/dr ','  phiv ','   dv ')
2010	format 
c              x,y            v         amp
     &  (1x,i4,2(1x,f9.2),(1x,f7.2),(1x,f9.4),2(1x,f7.2),(1x,f6.1),
     &  (1x,f6.2),(1x,f9.4),(1x,f6.1),(1x,f5.2))
	return
	end
