	subroutine backtrans(image,nimage,points,temp,npts,blanking,
     &         rms,npix1,npix2,npix3,refpix,val,inc)
	real*4 refpix(3),val(3),inc(3)
	integer(kind=4), intent(in) :: nimage
	real*4 image (nimage), points(3,nimage)
	real*4 temp(npix1,npix2,npix3)

	do 1 i=1,npix1
	do 1 j=1,npix2
	do 1 k=1,npix3
1	temp(i,j,k)=blanking

	do 2 l=1,npts
	x=points(1,l)
	y=points(2,l)
	z=points(3,l)
	i=nint((x-val(1))/inc(1)+refpix(1))
	j=nint((y-val(2))/inc(2)+refpix(2))
	k=nint((z-val(3))/inc(3)+refpix(3))
2	temp(i,j,k)=image(l)*rms

	return
	end
