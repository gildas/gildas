	subroutine getpar (imafile,rms,beamfwhm,velres,fbeam,fvel,
     &               fitflag,s0,thresh,p0,ap,ap1,itmaxcl,itmaxfit,
     &               eps,flambda,blanking,tol,savres,savfit,minsum)
	character*30 imafile
	character*1 yn1,yn2,yn3,yn4,yn5
	logical savres
	logical savfit
	real*4  minsum
	integer fitflag(11)
	dimension s0(3)

	open (unit=1,file='clumps_input',status='old')
	read (1,1000,err=999,end=999) 
1000	format (1x)
1001	format (1x,a)
	read (1,*,err=999,end=999) imafile
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) rms,blanking,tol
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) beamfwhm,fbeam,velres,fvel
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) s0(1),thresh,itmaxcl,minsum
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) s0(2),s0(3)
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) p0,ap,ap1
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) yn1,yn2,yn3
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) itmaxfit,eps,flambda
 	read (1,1000,err=999,end=999)
	read (1,*,err=999,end=999) yn4,yn5
 
	ap=ap*ap
	ap1=ap1*ap1

	do i=1,11
	fitflag(i)=1
	enddo

	if (yn1.eq.'N'.or.yn1.eq.'n') then
		fitflag(5)=0
		fitflag(6)=0
	endif
	if (yn2.eq.'N'.or.yn2.eq.'n') then
		fitflag(8)=0
		fitflag(9)=0
		fitflag(10)=0
	endif
	if (yn3.eq.'N'.or.yn3.eq.'n') then
		fitflag(7)=0
	endif
	if (yn4.eq.'N'.or.yn4.eq.'n') savres=.false.
	if (yn5.eq.'N'.or.yn5.eq.'n') savfit=.false.

	open (unit=2,file='clumps_output',status='new')
	open (unit=3,file='clumps_list',status='new')
	write (3,1020) rms,beamfwhm,fbeam,velres,fvel,
     &               thresh,p0,sqrt(ap),sqrt(ap1),s0
	write (2,1020) rms,beamfwhm,fbeam,velres,fvel,
     &               thresh,p0,sqrt(ap),sqrt(ap1),s0
	write (6,1020) rms,beamfwhm,fbeam,velres,fvel,
     &               thresh,p0,sqrt(ap),sqrt(ap1),s0
1020	format (1x,' rms: ',1p,g10.3,', beamfwhm: ',2(g10.3,1x),
     &       ', velres: ',2(g10.3,1x),/,
     &    1x,' threshhold: ',g10.3,', contrast: ',g10.3,
     &       ', weigth-aperture: ',g10.3,
     &       1x,g10.3,/,1x,' stiffness: ',3g10.3)
	close (unit=1)
	return

999	write (6,*) 'I-GETPAR: error reading clumps_inp '
	close (unit=1)
! CKr 15.7.04:
!	call exit
	stop
! CKr.
	end
