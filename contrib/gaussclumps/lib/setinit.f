	subroutine setinit (a,nimage,fitflag,beamfwhm,velres,
     &                      fbeam,fvel,nterms,
     &                      image,imax,points)
*
*   initialize fit 
*
	
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11),cent(3),wid(3)
	real*4 peak
	dimension a(11)
	integer fitflag(11)

	integer(kind=4), intent(in) :: nimage
	real*4  image(nimage),points(3,nimage)

	peak=image(imax)
	ampinit=0.9
	if (fitflag(7).eq.0) ampinit=1.

	a(1)=sqrt(fbeam**2-1.)*beamfwhm
	a(2)=sqrt((1.1*fbeam)**2-1.)*beamfwhm
	a(3)=sqrt( fvel**2-1.)*velres
	a(4)=0.
	a(5)=0.
	a(6)=0.
	a(7)=sqrt(ampinit*peak)
	a(8)=points(1,imax)
	a(9)=points(2,imax)
	a(10)=points(3,imax)
	a(11)=sqrt((1.-ampinit)*peak)

	wid(1)=beamfwhm**2
	wid(2)=(beamfwhm*1.01)**2
	wid(3)=velres**2
	cent(1)=a(8)
	cent(2)=a(9)
	cent(3)=a(10)

	nterms=0
	do 1 i=1,11
	if (fitflag(i).eq.1) then
		nterms=nterms+1
		lista(nterms)=i
	endif
1	continue

	return
	end
