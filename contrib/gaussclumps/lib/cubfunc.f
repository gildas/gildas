	function functn (ip,a,points,nterms)
	dimension a(11),points(3,1)
c ip points to entry in points(k,ip)
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11)
	real*4 peak
	real*4 cent(3),wid(3)
	common /quad/ aq,aa,bb,vv,c,s,c2,s2
	real*4 aq(3)
c	integer*2 list(2,2)
c points (k,ip), k=1,3 is the x,y,v coordinate of point ip
	dimension x(3)
c	data list /1,3,3,2/
c list points to quadratic form coefficient stored in aq(3):
c first principal axis a(1)
c second principal axis a(2)
c rotation angle a(4)
c velocity width is a(3)
c velocity gradent in x is a(5)
c velocity gradient in y is a(6)
c amplitude is a(7)**2
c zero positions are a(8-10)
c offset is a(11)**2
	functn=0.

	x(3)=points(3,ip)-a(10)
	do 1 i=1,2
	x(i)=points(i,ip)-a(7+i)
1	x(3)=x(3)-a(4+i)*x(i)

	functn=aq(1)*x(1)*x(1)+aq(2)*x(2)*x(2)
     &     +2.*aq(3)*x(1)*x(2)
     &     +x(3)**2*vv

	functn=a(7)**2*expz(-functn)+a(11)**2

	return
	end

c	-------------------------------------------------------------------------

	subroutine fderiv (ip,a,deriv,points,nterms,ff)
	dimension a(11),points(3,1),deriv(11)
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11)
	real*4 peak
	real*4 cent(3),wid(3)
	common /quad/ aq,aa,bb,vv,c,s,c2,s2
	real*4 aq(3)
	integer*2 list(2,2)
	dimension x(3)
	data list /1,3,3,2/

	do 3 i=1,10
3	deriv(i)=0.
	deriv(11)=2.*a(11)

	ff=functn (ip,a,points,nterms)
	f=ff-a(11)**2
	if (f.eq.0.) return

	x(3)=points(3,ip)-a(10)
	do 1 i=1,2
	x(i)=points(i,ip)-a(7+i)
1	x(3)=x(3)-a(4+i)*x(i)

	deriv(1)=f*(aa*(s*x(2)-c*x(1)))**2/2.772589*2.*a(1)
	deriv(2)=f*(bb*(s*x(1)+c*x(2)))**2/2.772589*2.*a(2)
	deriv(3)=f*(x(3)*vv)**2/2.772589*2.*a(3)
	deriv(4)=-f*(2.*aq(3)*(x(1)*x(1)-x(2)*x(2)) +
     &               2.*(aq(2)-aq(1))*x(1)*x(2))
	deriv(7)=2.*f/a(7)
	deriv(10)=f*2.*vv*x(3)

	do 2 i=1,2
	      deriv(7+i)=-f*2.*vv*x(3)*a(4+i)
	      deriv(4+i)=f*2.*vv*x(i)*x(3)
	do 2 j=1,2
	      l=list(i,j)
	      deriv(7+i)=deriv(7+i)+f*2.*aq(l)*x(j)
2	continue

	return
	end

c 	-------------------------------------------------------------------

	function expz(x)

	expz=0.
	if (x.gt.40.) then
		expz=exp(40.)
		write (2,*) 'w-expz: called with exponent > 40. '
	else if (x.lt.-20.) then
		expz=0.
	else
		expz=exp(x)
	endif
	return
	end

c	-------------------------------------------------------------------------

	subroutine quadini(a)
	dimension a(11)
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11)
	real*4 peak
	real*4 cent(3),wid(3)
	common /quad/ aq,aa,bb,vv,c,s,c2,s2
	real*4 aq(3)

	c=cos(a(4))
	s=sin(a(4))
	c2=c*c
	s2=s*s
	aa=2.772589/(wid(1)+a(1)**2)
	bb=2.772589/(wid(2)+a(2)**2)
	vv=2.772589/(wid(3)+a(3)**2)

	aq(1)=aa*c2+bb*s2
	aq(2)=aa*s2+bb*c2
	aq(3)=(bb-aa)*s*c

	return
	end
