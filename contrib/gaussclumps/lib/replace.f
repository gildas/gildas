	subroutine replace (image,nimage,imagfit,points,npts,blanking,
     &                rms,npix,refpix,val,inc,imgname,iclump,
     &                savres,savfit)
        use gildas_def
        use gkernel_interfaces
	integer(kind=4), intent(in) :: nimage
	real*4 image(nimage),imagfit(nimage),points(3,nimage)
	integer npix(3)
	real*4 refpix(3),val(3),inc(3)
	character*80 imgname
	logical savres,savfit
        ! Local
        real(kind=4), allocatable :: temp(:)
        real(kind=4), allocatable :: fit(:)
        logical :: error
        character*80 header
	integer i,ier
	
* Allocate working buffers
        error = .false.
        allocate(temp(nimage),fit(nimage),stat=ier)
        if (failed_allocate('REPLACE','temp/fit arrays',ier,error))
     &    call sysexi(fatale)
*
*	initialize Variables
 	do i = 1,nimage
	   fit(i) = 0.
	enddo
*
	npix1=npix(1)
	npix2=npix(2)
	npix3=npix(3)

	do 1 i=1,npts
	image(i)=image(i)-imagfit(i)
1	fit(i)=fit(i)+imagfit(i)
	write (6,*) 'I-REPLACE: fit subtracted '
	
	if (savres) then
	call backtrans (image,nimage,points,temp,npts,blanking,
     &         rms,npix1,npix2,npix3,refpix,val,inc)

	write (header,1000) imgname(1:40),iclump,' clumps subtracted '
1000	format (a40,': ',i6,a)
	call writeimg (temp,nimage,npix,refpix,val,inc,'clumps_tmp',
     &    header)
	write (6,*) 'I-REPLACE: residual image saved on clumps_tmp'
		endif

		if (savfit) then
	call backtrans (fit,nimage,points,temp,npts,blanking,
     &         rms,npix1,npix2,npix3,refpix,val,inc)

	write (header,1000) imgname(1:40),iclump,' fitted clumps '
	call writeimg (temp,nimage,npix,refpix,val,inc,'clumps_img',
     &    header)
	write (6,*) 'I-REPLACE: fitted image saved on clumps_img'
	endif

	deallocate(temp,fit)
	
	return
	end
