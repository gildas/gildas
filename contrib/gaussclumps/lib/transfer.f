	subroutine transfer (image,nimage,points,temp,npts,blanking,
     &         tol,rms,npix1,npix2,npix3,refpix,val,inc)
	integer(kind=4), intent(in) :: nimage
	real*4 refpix(3),val(3),inc(3)
	real*4 image (nimage), points(3,nimage)
	real*4 temp(npix1,npix2,npix3)

	npts=0

	do 1 k=1,npix3
	  z=val(3)+(float(k)-refpix(3))*inc(3)
	do 1 j=1,npix2
	  y=val(2)+(float(j)-refpix(2))*inc(2)
	do 1 i=1,npix1
	  x=val(1)+(float(i)-refpix(1))*inc(1)

	  if (abs(temp(i,j,k)-blanking).le.tol) goto 1
	  npts=npts+1
	  image(npts)=temp(i,j,k)/rms
 	  points(1,npts)=x
	  points(2,npts)=y
	  points(3,npts)=z

1	continue

	return
	end
