	subroutine convert (a,b,rms)
	parameter (pi=3.141592654)
	common /termpointer/ lista,peak,cent,wid
	dimension lista(11),cent(3),wid(3)
	real*4 peak
	real*4 a(11),b(11)

	b(1)=sqrt(a(1)**2+wid(1))
	b(2)=sqrt(a(2)**2+wid(2))
	if (b(1).lt.b(2)) im=2
	b(4)=a(4)/pi*180.
	if (b(2).gt.b(1)) b(4)=b(4)+90.
	b(4)=mod(b(4),180.)
	b(5)=sqrt(a(5)**2+a(6)**2)
	b(6)=sqrt(2./(1./b(1)**2+1./b(2)**2))
	b(3)=sqrt(a(3)**2+wid(3))
	b(7)=a(7)**2*rms
	b(8)=a(8)
	b(9)=a(9)
	b(10)=a(10)
	b(11)=0.
	if (b(5).ne.0.) b(11)=sign(acos(a(6)/b(5)),-a(5))/pi*180.
	
	return
	end
