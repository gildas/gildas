	subroutine findmax (image,nimage,imax,npts,points,badpts,
     &    rms,sum)
	integer(kind=4), intent(in) :: nimage
	real*4    image(nimage),points(3,nimage)
	integer*2 badpts (nimage)
	real*4    max,min

	max=-1.e30
	min=1.e30
	sum=0.

	do 1 i=1,npts
	if (max.lt.image(i) .and. badpts(i).ne.1) then
	 imax=i
	 max=image(i)
	endif
	if (min.gt.image(i)) then
	 imin=i
	 min=image(i)
	endif
	sum=sum+image(i)
1	continue

	write (2,*) 'I-FINDMAX, maximum found at ',
     &    points(1,imax),points(2,imax),points(3,imax),' ',max*rms
	write (2,*) 'I-FINDMAX, minimum found at ',
     &    points(1,imin),points(2,imin),points(3,imin),' ',min*rms
	write (2,*) 'I-FINDMAX, integral ',sum*rms
	write (6,*) 'I-FINDMAX, integral ',sum*rms

	return
	end
