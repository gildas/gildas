      PROGRAM GAUSSCLUMPS
!----------------------------------------------------------------------
! gildas        decomposes a spectral map into gaussian shaped clumps
! written by j.stutzki                                      june-1989
! 11.11.03 revised by C.Kramer: gag_pointer instead of pointer
!----------------------------------------------------------------------
!
! define x y z headers and data types
      USE GILDAS_DEF
      USE IMAGE_DEF
      USE GKERNEL_INTERFACES
!
! local variables and include files
!        external user_action_rtn
      EXTERNAL TRANSFER
      TYPE(GILDAS) X,Y
      INTEGER(KIND=4) :: NIMAGE
      REAL(KIND=4), ALLOCATABLE :: IMAGE(:)
      REAL(KIND=4), ALLOCATABLE :: POINTS(:,:)
      REAL(KIND=4), ALLOCATABLE :: IMAGFIT(:)
      REAL(KIND=4), ALLOCATABLE :: FIT(:)
      REAL*4 A(11),SIGMAA(11)
      REAL*4 MINSUM
      REAL*4 REFPIX(3),VAL(3),INC(3)
      CHARACTER*80 IMAFILE,RESIMAGE,FITIMAGE
!       character*80 imgname,
      CHARACTER*80 BEAMSTRING
      CHARACTER*1  BEAMUNIT
      LOGICAL YN1,YN2,YN3
      LOGICAL CONV,ITER,ERROR,UPIMAGE
      INTEGER FITFLAG(11)
      INTEGER(KIND=2), ALLOCATABLE :: BADPTS(:)
      INTEGER NBAD,IER
      DIMENSION S0(3)
      DATA NBAD /0/
      DATA FITFLAG /1,1,1,1,1,1,1,1,1,1,1/
!
      INTEGER MEMORY(1)
      INTEGER(KIND=ADDRESS_LENGTH) IPX,IPY
!
      CHARACTER*70 VERSION
      PARAMETER
     &(VERSION='GAUSSCLUMPS_3D_3D - asymmetric parabola s0')
!
      NR = 0
      NF = 0
!
!       read parameters from file
      CALL GILDAS_OPEN
      CALL GILDAS_CHAR('INP_IMAGE$',IMAFILE)
      CALL GILDAS_REAL('RMS$',RMS,1)
      CALL GILDAS_CHAR('FWHM_BEAM$',BEAMSTRING)
      CALL GILDAS_REAL('FWHM_START$',FBEAM,1)
      CALL GILDAS_REAL('VELO_RES$',VELRES,1)
      CALL GILDAS_REAL('VELO_START$',FVEL,1)
      CALL GILDAS_REAL('STIFFNESS$',S0,3)
      CALL GILDAS_REAL('THRESHHOLD$',THRESH,1)
      CALL GILDAS_INTE('MAXCLUMP$',ITMAXCL,1)
      CALL GILDAS_REAL('CONTRAST$',P0,1)
      CALL GILDAS_REAL('MININTEGRAL$',MINSUM,1)
      CALL GILDAS_REAL('APERTURE_FWHM$',AP,1)
      CALL GILDAS_REAL('APERTURE_LMTS$',AP1,1)
      CALL GILDAS_LOGI('FIT_DVDR$',YN1,1)
      CALL GILDAS_LOGI('FIT_CENTERPOS$',YN2,1)
      CALL GILDAS_LOGI('FIT_AMPLITUDE$',YN3,1)
      CALL GILDAS_INTE('MAXIT$',ITMAXFIT,1)
      CALL GILDAS_REAL('EPSILON$',EPS,1)
      CALL GILDAS_REAL('FLAMBDA$',FLAMBDA,1)
      CALL GILDAS_LOGI('UPD_IMAGE$',UPIMAGE,1)
      CALL GILDAS_CHAR('RES_IMAGE$',RESIMAGE)
      CALL GILDAS_CHAR('FIT_IMAGE$',FITIMAGE)
      CALL GILDAS_CLOSE
!
!
!  interpret beamstring
!
      N = LENC(BEAMSTRING)
      DO WHILE (BEAMSTRING(N:N).EQ.' '.AND.N.NE.0)
         N = N-1
      ENDDO
      IF (BEAMSTRING(N:N).EQ.'s'.OR.BEAMSTRING(N:N).EQ.'S') THEN
         SPACEFACTOR = 206264.81
         N = N-1
         BEAMUNIT = '"'
      ELSEIF(BEAMSTRING(N:N).EQ.'m'.OR.BEAMSTRING(N:N).EQ.'M') THEN
         SPACEFACTOR = 3437.7468
         N = N-1
         BEAMUNIT = ''''
      ELSEIF(BEAMSTRING(N:N).EQ.'r'.OR.BEAMSTRING(N:N).EQ.'R') THEN
         SPACEFACTOR = 1.0
         N = N-1
         BEAMUNIT = 'r'
      ELSE
         STOP 'F-GAUSSCLUMPS_3D, wrong beam unit'
      ENDIF
      DO WHILE (BEAMSTRING(N:N).EQ.' '.AND.N.NE.0)
         N = N-1
      ENDDO
      IF (N.LE.0) STOP 'F-GAUSSCLUMPS_3D, wrong beamfwhm'
      M = INDEX(BEAMSTRING,'.')
      READ (UNIT=BEAMSTRING(1:N),FMT=*) BEAMFWHM
      WRITE(*,*) 'The beam FWHM is:',BEAMFWHM
      WRITE(*,'(a13,a1)') 'The unit is:',BEAMSTRING(N+1:N+1)
      IF (M.EQ.0) BEAMFWHM = BEAMFWHM*10**N
      OPEN(UNIT=2,FILE='GAUSSCLUMPS_3D.out',STATUS='unknown')
      OPEN(UNIT=3,FILE='GAUSSCLUMPS_3D.list',STATUS='unknown')
!
!       write input parameters to output file
!
      WRITE (3,1010) VERSION
      WRITE (2,1010) VERSION
      WRITE (3,1020) RMS,BEAMFWHM,BEAMUNIT,FBEAM,VELRES,FVEL,
     &THRESH,P0,AP,AP1,S0
      WRITE (2,1020) RMS,BEAMFWHM,BEAMUNIT,FBEAM,VELRES,FVEL,
     &THRESH,P0,AP,AP1,S0
1010  FORMAT (1H ,70A)
1020  FORMAT (1X,' rms: ',1P,G10.3,', beamfwhm: ',G10.3,A1,1X,
     &G10.3,1X,', velres: ',2(G10.3,1X),/,
     &1X,' threshhold: ',G10.3,', contrast: ',G10.3,
     &', weigth-aperture: ',G10.3,
     &1X,G10.3,/,1X,' stiffness: ',3G10.3)
!
!
!  change into program useable variables
!
      IPY = 0
      AP=AP*AP
      AP1=AP1*AP1
      IF (.NOT.YN1) THEN
         FITFLAG(5)=0
         FITFLAG(6)=0
      ENDIF
      IF (.NOT.YN2) THEN
         FITFLAG(8)=0
         FITFLAG(9)=0
         FITFLAG(10)=0
      ENDIF
      AMPINIT=0.9
      IF (.NOT.YN3) THEN
         FITFLAG(7)=0
         AMPINIT=1.
      ENDIF
!
!    read the input image
!
      NI = LENC(IMAFILE)
      WRITE (2,*) 'I-GAUSSCLUMPS_3D, open image ',IMAFILE(1:NI)
      IF (NI.EQ.0) GOTO 100
! Old GILDAS V1 way
!      CALL SIC_PARSEF(IMAFILE(1:NI),X%FILE,' ','.gdf')
!
!       allocate an Image Slot
!
!      CALL GDF_GEIS(X%LOCA%ISLO,ERROR)
!      IF(ERROR) THEN
!         WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to create image slot'
!         GOTO 100
!      ENDIF
!
!       connect image to Slot
!
!      CALL GDF_REIS(X%LOCA%ISLO,X%CHAR%TYPE,X%FILE,X%GIL%FORM,
!     &  X%LOCA%SIZE,ERROR)
!      IF(ERROR) THEN
!         WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to read into image slot'
!         GOTO 100
!      ENDIF
!
!       read the header
!
!      CALL GDF_READ(X,X%LOCA%ISLO,ERROR)
!      IF(ERROR) THEN
!         WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to read header'
!         GOTO 100
!      ENDIF
!
!       connect Image Slot to Memoery Slot
!
!      CALL GDF_GET_DATA(X, ERROR)
!      IF(ERROR) THEN
!         WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to allocate memory slot'
!         GOTO 100
!      ENDIF
!
!       determin the address
!
!      IPX=GAG_POINTER(X%LOCA%ADDR,MEMORY)
!
      call gildas_null(x, type = 'IMAGE')
      call gdf_read_gildas(x, imafile, '.gdf', error, rank=3)
      x%loca%addr = locwrd(x%r3d)
      ipx = gag_pointer(x%loca%addr,memory)
!
! calculate internal axis definition
      REFPIX(1)=SNGL(X%GIL%REF(1))
      VAL(1)   =SNGL(X%GIL%VAL(1))*SPACEFACTOR
      INC(1)   =SNGL(X%GIL%INC(1))*SPACEFACTOR
      REFPIX(2)=SNGL(X%GIL%REF(2))
      VAL(2)   =SNGL(X%GIL%VAL(2))*SPACEFACTOR
      INC(2)   =SNGL(X%GIL%INC(2))*SPACEFACTOR
      REFPIX(3)=SNGL(X%GIL%REF(3))
      VAL(3)   =SNGL(X%GIL%VAL(3))
      INC(3)   =SNGL(X%GIL%INC(3))
!
! Allocate working arrays
      NIMAGE = X%GIL%DIM(1)*X%GIL%DIM(2)*X%GIL%DIM(3)
      ALLOCATE(IMAGE(NIMAGE),STAT=IER)
      IF (IER.NE.0) THEN
        WRITE(2,*) 'F-GAUSSCLUMPS_3D, Error allocating IMAGE'
        GO TO 100
      ENDIF
      ALLOCATE(POINTS(3,NIMAGE),STAT=IER)
      IF (IER.NE.0) THEN
        WRITE(2,*) 'F-GAUSSCLUMPS_3D, Error allocating POINTS'
        GO TO 100
      ENDIF
      ALLOCATE(IMAGFIT(NIMAGE),STAT=IER)
      IF (IER.NE.0) THEN
        WRITE(2,*) 'F-GAUSSCLUMPS_3D, Error allocating IMAGFIT'
        GO TO 100
      ENDIF
      ALLOCATE(FIT(NIMAGE),STAT=IER)
      IF (IER.NE.0) THEN
        WRITE(2,*) 'F-GAUSSCLUMPS_3D, Error allocating FIT'
        GO TO 100
      ENDIF
      ALLOCATE(BADPTS(NIMAGE),STAT=IER)
      IF (IER.NE.0) THEN
        WRITE(2,*) 'F-GAUSSCLUMPS_3D, Error allocating BADPTS'
        GO TO 100
      ENDIF
! Initialize Variables
      DO I = 1,NIMAGE
         FIT(I) = 0.
         BADPTS(I) = 0
      ENDDO
!
!  change input image : drop out blank values, divide temperatures through
!  rms
      CALL TRANSFER (IMAGE,NIMAGE,POINTS,MEMORY(IPX),NPTS,X%GIL%BVAL,
     &X%GIL%EVAL,RMS,X%GIL%DIM(1),X%GIL%DIM(2),X%GIL%DIM(3),
     &REFPIX,VAL,INC)
!
!  frees the input image
!
!      CALL GDF_FRIS(X%LOCA%ISLO,ERROR)
      deallocate(x%r3d, stat=ier)
      call gdf_close_image(x,error)
      IF (ERROR) THEN
         WRITE(2,*) 'F-GAUSSCLUMPS_3D,  cannot free image slot'
         GO TO 100
      ENDIF
!
!  create output images here, if they are updated after each fit
      IF (UPIMAGE) THEN
         NR = LENC(RESIMAGE)
         call gildas_null(y)
         call gdf_copy_header(x,y, error)
         call sic_parsef(resimage(1:nr),y%file,' ','.gdf')
         call gdf_create_image (y, error)
         if (error) then
            write (6,*)'I-GAUSSCLUMPS_3D, unable to write RES header'
            goto 100
         endif
!
!        CALL SIC_PARSEF(RESIMAGE(1:NR),Y%FILE,' ','.gdf')
! Old specially dumb code (reading and writing while doing nothing
! in between)         
!        XI_EXTR = 0
!
!       allocation of an Image slot
!
!         CALL GDF_GEIS(Y%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to get image slot'
!            GOTO 100
!         ENDIF
!
!       copying X-header to Y-header
!
!         CALL GDF_CH(X,Y)
!
!       writing the header
!
!         CALL GDF_WRIT(Y,Y%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to write header'
!            GOTO 100
!         ENDIF
!
!       create the image to the Image Slot
!
!         CALL GDF_CRIS(Y%LOCA%ISLO,Y%CHAR%TYPE,Y%FILE,Y%GIL%FORM,
!     &     Y%LOCA%SIZE,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to create image slot'
!            GOTO 100
!         ENDIF
!
!
!       connect Image Slot to Memory Slot
!
!         CALL GDF_GET_DATA(Y, ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to allocate memory slot
!     &      '
!            GOTO 100
!         ENDIF
!
!       determin the address
!
!         IPY=GAG_POINTER(Y%LOCA%ADDR,MEMORY)
!
!       write into file
!
!         CALL GDF_FRMS(Y%LOCA%MSLO,ERROR)
!         IF(ERROR) THEN
!           WRITE (6,*)
!     &      'I-GAUSSCLUMPS_3D, unable to deallocate memory slot'
!            GOTO 100
!         ENDIF
!
!
         NF = LENC(FITIMAGE)
         call sic_parsef(fitimage(1:nr),x%file,' ','.gdf')
         call gdf_read_header (x, error)
         if (error) then
            write (6,*)'I-GAUSSCLUMPS_3D, unable to read FIT header'
            goto 100
         endif
! Old dumb code
!         CALL SIC_PARSEF(FITIMAGE(1:NF),X%FILE,' ','.gdf')
!
!       allocate an Image Slot
!
!         CALL GDF_GEIS(X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to get image slot'
!            GOTO 100
!         ENDIF
!
!       connect image to Slot
!
!         CALL GDF_REIS(X%LOCA%ISLO,X%CHAR%TYPE,X%FILE,X%GIL%FORM,
!     &     X%LOCA%SIZE,ERROR)
!         IF(ERROR) THEN
!            WRITE(6,*)
!     &        'I-GAUSSCLUMPS_3D, unable to read into image slot'
!            GOTO 100
!         ENDIF
!
!       read the header
!
!         CALL GDF_READ(X,X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to read header'
!            GOTO 100
!         ENDIF
!
!       connect Image Slot to Memoery Slot
!
!         CALL GDF_GET_DATA(X, ERROR)
!         IF(ERROR) THEN
!            WRITE(6,*)
!     &        'I-GAUSSCLUMPS_3D, unable to allocate memory slot'
!            GOTO 100
!         ENDIF
!
!       determin the address
!
!         IPX=GAG_POINTER(X%LOCA%ADDR,MEMORY)
!
!       write into file
!
!         CALL GDF_FRMS(X%LOCA%MSLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)
!     &      'I-GAUSSCLUMPS_3D, unable to deallocate memory slot'
!            GOTO 100
!         ENDIF
      ENDIF
!
      ICLUMP=0
!
1     CONTINUE
      ICLUMP=ICLUMP+1
      CALL FINDMAX (IMAGE,NIMAGE,IMAX,NPTS,POINTS,BADPTS,RMS,SUM)
      CALL ITERATE (IMAGE,NIMAGE,IMAX,ICLUMP,THRESH,ITMAXCL,ITER,
     &RMS,SUM,MINSUM)
      IF (.NOT.ITER) GOTO 999
      WRITE (2,*) 'I-GAUSSCLUMPS_3D, start working on clump ',ICLUMP
      WRITE (6,*) 'I-GAUSSCLUMPS_3D, start working on clump ',ICLUMP
      CALL SETINIT (A,NIMAGE,FITFLAG,BEAMFWHM,VELRES,
     &FBEAM,FVEL,NTERMS,IMAGE,
     &IMAX,POINTS)
      CALL CUBFIT (IMAGE,NIMAGE,NPTS,NTERMS,A,SIGMAA,FLAMBDA,
     &IMAGFIT,CHISQR,EPS,ITMAXFIT,POINTS,S0,
     &CONV,P0,IMAX,AP,AP1)
      IF (.NOT.CONV) THEN
         NBAD=NBAD+1
         BADPTS(IMAX)=1
         WRITE (2,*)
     &   'I-GAUSSCLUMPS_3D, ',(POINTS(I,IMAX),I=1,3),
     &   'marked as unfittable '
         ICLUMP=ICLUMP-1
         GOTO 1
      ENDIF
      CALL LISTCLUMP (ICLUMP,A,SIGMAA,IMAFILE(1:NI),CHISQR,X%CHAR%NAME,
     &  RMS)
!
!
!    calculate residuals and sum fitted image
      DO 2 I=1,NPTS
         IMAGE(I)=IMAGE(I)-IMAGFIT(I)
2     FIT(I)=FIT(I)+IMAGFIT(I)
!
!     now update residual and fitted map,  if wanted
!
      IF (UPIMAGE) THEN
!
!   write residual image to disk
         CALL BACKTRANS (IMAGE,NIMAGE,POINTS,MEMORY(IPY),NPTS,
     &   X%GIL%BVAL,RMS,X%GIL%DIM(1),X%GIL%DIM(2),X%GIL%DIM(3),
     &   REFPIX,VAL,INC)
         WRITE (2,*) 'I-GAUSSCLUMPS_3D, residual image updated in ',
     &   Y%FILE(1:NR)
!
!  update fitted image to disk
         CALL BACKTRANS (FIT,NIMAGE,POINTS,MEMORY(IPX),NPTS,
     &   X%GIL%BVAL,RMS,X%GIL%DIM(1),X%GIL%DIM(2),X%GIL%DIM(3),
     &   REFPIX,VAL,INC)
         WRITE (2,*) 'I-GAUSSCLUMPS_3D, fitted image updated in ',
     &   X%FILE(1:NF)
      ENDIF
!
!
!    next clump
      GO TO 1
!
! continue here after the fit has finished
999   CONTINUE
!
! write last fit results to  disk
      IF (.NOT.UPIMAGE) THEN
         WRITE (2,*) 'I-GAUSSCLUMPS_3D, create output images '
         NR = LENC(RESIMAGE)
         CALL SIC_PARSEF(RESIMAGE(1:NR),X%FILE,' ','.gdf')
         call gdf_create_image (x, error)
         if (error) then
            write(6,*) 'E-GAUSSCLUMPS_3D,  cannot create output image'
            goto 100
         endif
        call gdf_allocate (x, error)
        x%loca%addr = locwrd(x%r3d)
!         XI_EXTR = 0
!
!       allocation of an Image slot
!
!         CALL GDF_GEIS(X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to get image slot'
!            GOTO 100
!         ENDIF
!
!       writing the header
!
!         CALL GDF_WRIT(X,X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to write header'
!            GOTO 100
!         ENDIF
!
!       create the image to the Image Slot
!
!         CALL GDF_CRIS(X%LOCA%ISLO,X%CHAR%TYPE,X%FILE,X%GIL%FORM,
!     &     X%LOCA%SIZE,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to create image slot'
!            GOTO 100
!         ENDIF
!
!
!       connect Image Slot to Memoery Slot
!
!         CALL GDF_GET_DATA(X, ERROR)
!         IF(ERROR) THEN
!            WRITE(6,*)
!     &        'I-GAUSSCLUMPS_3D, unable to allocate memory slot'
!            GOTO 100
!         ENDIF
!
!       determin the address
!
         IPX=GAG_POINTER(X%LOCA%ADDR,MEMORY)
!
!
!   write residual image to disk
         CALL BACKTRANS (IMAGE,NIMAGE,POINTS,MEMORY(IPX),NPTS,
     &   X%GIL%BVAL,RMS,X%GIL%DIM(1),X%GIL%DIM(2),X%GIL%DIM(3),
     &   REFPIX,VAL,INC)
         WRITE (2,*) 'I-GAUSSCLUMPS_3D, residual image updated in ',
     &   X%FILE(1:NR)
          call gdf_write_data (x, x%r3d, error)
!         CALL GDF_FRIS(X%LOCA%ISLO,ERROR)
         IF (ERROR) THEN
            WRITE(2,*) 'F-GAUSSCLUMPS_3D,  cannot free image slot'
            GO TO 100
         ENDIF
!
         NF = LENC(FITIMAGE)
         CALL SIC_PARSEF(FITIMAGE(1:NF),X%FILE,' ','.gdf')
         call gdf_create_image (x, error)
         if (error) then
            write(6,*) 'E-GAUSSCLUMPS_3D,  cannot create output image'
            goto 100
         endif
!        call gdf_allocate (x, error) ! Already allocated in this case
!        x%loca%addr = locwrd(x%r3d)
!
!       allocation of an Image slot
!
!         CALL GDF_GEIS(X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to get image slot'
!            GOTO 100
!         ENDIF
!
!       writing the header
!
!         CALL GDF_WRIT(X,X%LOCA%ISLO,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to write header'
!            GOTO 100
!         ENDIF
!
!       create the image to the Image Slot
!
!         CALL GDF_CRIS(X%LOCA%ISLO,X%CHAR%TYPE,X%FILE,X%GIL%FORM,
!     &     X%LOCA%SIZE,ERROR)
!         IF(ERROR) THEN
!            WRITE (6,*)'I-GAUSSCLUMPS_3D, unable to create image slot'
!            GOTO 100
!         ENDIF
!
!
!       connect Image Slot to Memoery Slot
!
!         CALL GDF_GET_DATA(X, ERROR)
!         IF(ERROR) THEN
!            WRITE(6,*)
!     &        'I-GAUSSCLUMPS_3D, unable to allocate memory slot'
!            GOTO 100
!         ENDIF
!
!       determin the address
!
         IPX=GAG_POINTER(X%LOCA%ADDR,MEMORY)
!
!
!  update fitted image to disk
         CALL BACKTRANS (FIT,NIMAGE,POINTS,MEMORY(IPX),NPTS,
     &   X%GIL%BVAL,RMS,X%GIL%DIM(1),X%GIL%DIM(2),X%GIL%DIM(3),
     &   REFPIX,VAL,INC)
         WRITE (2,*) 'I-GAUSSCLUMPS_3D, fitted image updated in ',
     &   X%FILE(1:NF)
          call gdf_write_data(x, x%r3d, error)
!         CALL GDF_FRIS(X%LOCA%ISLO,ERROR)
         IF (ERROR) THEN
            WRITE(2,*) 'F-GAUSSCLUMPS_3D,  cannot write image slot'
            GO TO 100
         ENDIF
      ELSE
!
!  frees the images
!
        call gdf_close_image (y, error)
!         CALL GDF_FRIS(Y%LOCA%ISLO,ERROR)
         IF (ERROR) THEN
            WRITE(2,*) 'F-GAUSSCLUMPS_3D,  cannot free image slot'
            GO TO 100
         ENDIF
      ENDIF
      call gdf_close_image (x, error)
!
      CALL CLEARUP (NBAD,ICLUMP)
      IF (ALLOCATED(IMAGE))    DEALLOCATE(IMAGE)
      IF (ALLOCATED(POINTS))   DEALLOCATE(POINTS)
      IF (ALLOCATED(IMAGFIT))  DEALLOCATE(IMAGFIT)
      IF (ALLOCATED(FIT))      DEALLOCATE(FIT)
      IF (ALLOCATED(BADPTS))   DEALLOCATE(BADPTS)
!
! Success
      STOP 'S-GAUSSCLUMPS_3D,  successful completion'
!
! Error
100   CALL SYSEXI(FATALE)
      IF (ALLOCATED(IMAGE))    DEALLOCATE(IMAGE)
      IF (ALLOCATED(POINTS))   DEALLOCATE(POINTS)
      IF (ALLOCATED(IMAGFIT))  DEALLOCATE(IMAGFIT)
      IF (ALLOCATED(FIT))      DEALLOCATE(FIT)
      IF (ALLOCATED(BADPTS))   DEALLOCATE(BADPTS)
      END PROGRAM GAUSSCLUMPS

!        subroutine user_action_rtn(out_str,int)
!        integer int
!        character*(*)   out_str
!        write (int,fmt='(15h I-GAUSSCLUMPS_3D,,a)') out_str
!        return
!        end
