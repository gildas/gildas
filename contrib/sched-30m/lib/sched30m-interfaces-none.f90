module sched30m_interfaces_none
  interface
            subroutine horizline(t,nom,ddia,pr,ff)
            real      x, t
            character chain*80, nom*2, ddia*2
            logical   pr,ff
            end subroutine horizline
  end interface
  !
  interface
            subroutine vertline(z,hora,pru,ff)
            real      y, z
            character chain*80, chora*2
            integer   hora
            logical   pru,ff
            end subroutine vertline
  end interface
  !
  interface
            subroutine displ_proj(proje,diap,tim_st,tim_end,s,t,diaa,prueba,ff)
            character  proje*15, chain*80, pr*15
            integer    i, j, s, t, p, u, diaa(16), no_b, no_bl,a1,a2
    !       integer    diap(40,8)
            integer    diap(50,15)
    !       real       tim_st(40,8),tim_end(40,8),corr
            real       tim_st(50,15),tim_end(50,15),corr
    !        real       x(400), y(400), xave, yave, height, width,z
            real       x(100), y(100), xave, yave, height, width,z
            logical    anga,prueba,ff
            end subroutine displ_proj
  end interface
  !
  interface
            subroutine gifcoor(proje,diap,tim_st,tim_end,s,t,diaa,cpolix,cpoliy)
            character  proje*15
            integer    i, j, s, p, u, t, diaa(16),a1,a2
            integer    diap(50,15)
            real       tim_st(50,15),tim_end(50,15)
            real       x(100), y(100), xave, yave, height, width
            real       centrox,centroy,ancho,alto
            real       xoff,yoff,deltax,deltay
            real       cpolix(100), cpoliy(100)
            end subroutine gifcoor
  end interface
  !
  interface
            subroutine makehtml1(s,fichero,fichero2)
            character    fichero*20,fichero2*20
            integer      s
            end subroutine makehtml1
  end interface
  !
  interface
            subroutine makehtml2(s,fichero)
            character    fichero*20
            integer      s
            end subroutine makehtml2
  end interface
  !
  interface
            subroutine  makemap(an,an2,s,fi,proyecto,v,cpolix,cpoliy,yy,mm)
            character(len=*), intent(in) :: fi(50)
            character    fii*20,proyecto(50)*15,bgyear*2
            character    aux*4,poolID*2
            character    href_dir*37
            character*60 text1,text2,text3,text4*50,ans*1
            integer      i,v,s,con_0,con_1,con_2,con_3,con_4,mm,yy,ww
            integer      yydir
            real         cpolix(100), cpoliy(100)
            logical      an, an2
            end subroutine makemap
  end interface
  !
  interface
            subroutine hsidnew(ye,mo,da,sid_ho,sid_min,lon_de,lon_mi,lon_se,lsummer)
            integer*4 ye,mo,da,ho,mi,ho0,mi0,sid_ho,sid_min,k
            real*4 lon_de,lon_mi,lon_se,aa,x1,x2,x4,y1,y2,y3,y4
            real*8 dt,hs0,sec,sidtime,sidtime0,factor,sec1
            character chain*80,csid_ho*4
            logical   lsummer
            end subroutine hsidnew
  end interface
  !
  interface
            subroutine rx_mark(n,m)    ! (dia=n; receptor=m) MR FEB-91
            integer    n,m
            end subroutine rx_mark
  end interface
  !
  interface
            subroutine sunpos (tim1,tim2,tim3,salida,puesta)
    !
            integer         time(7)
            integer         tim1,tim2,tim3
            integer         ig,imi,is,iy,im,id,idt,jj
            real*8          ra,de,frday,alst,dt,gst,lst
            real*8          ajd,al_s,elev,azim,errpos,alat,sila,cola,ha
            real*8          eps,pi,ome,e,err,d,ei,am_a,t_a
            real            salida,puesta
            end subroutine sunpos
  end interface
  !
  interface
            subroutine eecc (elo,ela,ra,de)
    !   subroutine to make the ecliptic to equatorial coordinate conversion
    !   elo = ecliptic longitude (radians)
    !   ela = ecliptic latitude (radians)
    !   ra = riht ascension (radians)
    !   de = declination (radians)
            real*8 elo,ela,ra,de,pi,ep
            end subroutine eecc
  end interface
  !
  interface
            subroutine cgms (angi,gra,min,sec,k)
    !   subroutine to make the conversion of the angle ang with the following
    !   options:
    !   k=1, 0 <= output angle < 360
    !   k=2, -180 < output angle <= 180
    !   k=3, 0 h <= output angle < 24 h
            integer gra,sec,min,k
            real*8  pi,ang,angi,amin
            end subroutine cgms
  end interface
  !
  interface
            subroutine ditra (an,me,di,dt,jd)
    !
    !   subrutina para calcular los dias transcurridos desde el comienzo de nuestra
    !   era. ano 1, mes 1 y dia 1 dara 0 dias transcurridos.
    !
    !       an = ano                        (input integer*4)
    !       me = mes                        (input integer*4)
    !       di = dia                        (input integer*4)
    !       dt = dias transcurridos         (output integer*4)
    !       jd = julian date                (output real*4)
    !
            integer         k,ab,an,me,di,dt
            real*8          jd
            end subroutine ditra
  end interface
  !
  interface
            subroutine getst (dt,gst,lst)
    !
    !   subrutina para obtener gst (greenwich sidereal time) y lst (local sidereal
    !   time) conociendo los dt (dias transcurridos) desde el comienzo de nuestra
    !   era.
    !
    !       dt  = dias transcurridos desde comienzo era     (input real*8)
    !       gst = greenwich sidereal time (fraction of day) (output real*8)
    !       lst = loca sidereal time at 30m radiotelescope  (output real*8)
    !
            real*8  dt,gst,lst,dtb
            end subroutine getst
  end interface
  !
end module sched30m_interfaces_none
