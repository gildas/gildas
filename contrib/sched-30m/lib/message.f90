!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage SCHED30M messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sched30m_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: sched30m_message_id = gpack_global_id  ! Default value for startup message
  !
end module sched30m_message_private
!
subroutine sched30m_message_set_id(id)
  use gbl_message
  use sched30m_message_private
  !---------------------------------------------------------------------
  ! @ private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  sched30m_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',sched30m_message_id
  call sched30m_message(seve%d,'sched30m_message_set_id',mess)
  !
end subroutine sched30m_message_set_id
!
subroutine sched30m_message(mkind,procname,message)
  use sched30m_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(sched30m_message_id,mkind,procname,message)
  !
end subroutine sched30m_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
