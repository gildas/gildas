subroutine sched30m_load
  use sched30m_interfaces, except_this=>sched30m_load
  !---------------------------------------------------------------------
  ! @ private
  ! Define and load languages of the package.
  !---------------------------------------------------------------------
  ! Local
  ! Define commands of a new language
  integer, parameter :: mcom=2
  character(len=12) :: vocab(mcom)
  data vocab / ' SCHEDULE', ' HARDCOPY' /
  !
  ! Load the new language:
  call sic_begin(           &
    'SCHED30M',             &  ! Language name
    'GAG_HELP_SCHED30M',    &  !
    mcom,                   &  ! Number of commands + options in language
    vocab,                  &  ! Array of commands
    '1.0',                  &  ! Some version string
    sched30m_run,           &  ! The routine which handles incoming commands
    sched30m_error)            ! The error status routine of the library
  !
end subroutine sched30m_load
!
subroutine sched30m_run(line,comm,error)
  use gbl_message
  use sched30m_interfaces, except_this=>sched30m_run
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for the language SCHED30M.
  ! This routine is able to call the routines associated to each
  ! command of the language SCHED30M
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Command line to send to support routines
  character(len=12), intent(in)    :: comm   ! Command resolved by the SIC interpreter
  logical,           intent(inout) :: error  ! Logical error flag
  !
  call sched30m_message(seve%c,'SCHED30M',line)
  !
  ! Call appropriate subroutine according to COMM
  select case(comm)
  case ('SCHEDULE')
    call sched30m_schedule_command(line,error)
  case ('HARDCOPY')
    call sched30m_hardcopy_command(line,error)
  case default
    call sched30m_message(seve%e,'SCHED30M_RUN','Unimplemented command '//comm)
    error = .true.
  end select
  !
end subroutine sched30m_run
!
function sched30m_error()
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for the error status of the library. Does nothing
  ! here.
  !---------------------------------------------------------------------
  logical :: sched30m_error
  !
  sched30m_error = .false.
  !
end function sched30m_error
