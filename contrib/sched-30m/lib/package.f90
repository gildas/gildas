!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the SCHED-30M package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sched30m_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use sched30m_interfaces, except_this=>sched30m_pack_set
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='sched30m'
  pack%ext='.sched30m'
  pack%depend(1:2) = (/ locwrd(sic_pack_set), locwrd(greg_pack_set) /) ! List here dependencies
  pack%init=locwrd(sched30m_pack_init)
  pack%clean=locwrd(sched30m_pack_clean)
  !
end subroutine sched30m_pack_set
!
subroutine sched30m_pack_init(gpack_id,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: gpack_id
  logical         :: error
  ! Local
  integer :: ier
  !
  call sched30m_message_set_id(gpack_id)
  !
  ! Specific variables
  ier = sic_setlog('gag_help_sched30m','gag_doc:hlp/sched30m-help-template.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  !
  ! Load local language
  call sched30m_load
  !
  ! Specific initializations
  call gr_exec('set /def')
  call gr_exec('set coor user')
  call gr_exec('set tick 0.0')
  !
end subroutine sched30m_pack_init
!
subroutine sched30m_pack_clean(error)
  !---------------------------------------------------------------------
  ! @ private
  ! Called at end of session. Might clean here for example global
  ! buffers allocated during the session
  !---------------------------------------------------------------------
  logical :: error
  !
end subroutine sched30m_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
