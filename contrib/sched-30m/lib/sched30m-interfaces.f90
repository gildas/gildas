module sched30m_interfaces
  !---------------------------------------------------------------------
  ! SCHED30M interfaces, all kinds (private or public). Do not use
  ! directly out of the library.
  !---------------------------------------------------------------------
  !
  use sched30m_interfaces_public
  use sched30m_interfaces_private
  !
end module sched30m_interfaces
