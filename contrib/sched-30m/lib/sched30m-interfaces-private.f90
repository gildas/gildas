module sched30m_interfaces_private
  interface
    subroutine sched30m_load
      !---------------------------------------------------------------------
      ! @ private
      ! Define and load languages of the package.
      !---------------------------------------------------------------------
      ! Local
      ! Define commands of a new language
    end subroutine sched30m_load
  end interface
  !
  interface
    subroutine sched30m_run(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for the language SCHED30M.
      ! This routine is able to call the routines associated to each
      ! command of the language SCHED30M
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Command line to send to support routines
      character(len=12), intent(in)    :: comm   ! Command resolved by the SIC interpreter
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine sched30m_run
  end interface
  !
  interface
    function sched30m_error()
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for the error status of the library. Does nothing
      ! here.
      !---------------------------------------------------------------------
      logical :: sched30m_error
    end function sched30m_error
  end interface
  !
  interface
    subroutine sched30m_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine sched30m_message_set_id
  end interface
  !
  interface
    subroutine sched30m_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine sched30m_message
  end interface
  !
  interface
    subroutine sched30m_pack_set(pack)
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine sched30m_pack_set
  end interface
  !
  interface
    subroutine sched30m_pack_init(gpack_id,error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: gpack_id
      logical         :: error
    end subroutine sched30m_pack_init
  end interface
  !
  interface
    subroutine sched30m_pack_clean(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Called at end of session. Might clean here for example global
      ! buffers allocated during the session
      !---------------------------------------------------------------------
      logical :: error
    end subroutine sched30m_pack_clean
  end interface
  !
  interface
    subroutine sched30m_schedule_command(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support subroutine for command
      !    SCHEDULE File1 [File2]
      ! Decode the command line
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sched30m_schedule_command
  end interface
  !
  interface
    subroutine sched30m_schedule(infour,file1,file2,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support subroutine for command
      !    SCHEDULE File1 [File2]
      ! Do the actual work
      !---------------------------------------------------------------------
      logical,          intent(in)    :: infour
      character(len=*), intent(in)    :: file1
      character(len=*), intent(in)    :: file2
      logical,          intent(inout) :: error
    end subroutine sched30m_schedule
  end interface
  !
  interface
    subroutine sched30m_hardcopy_command(line,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Support subroutine for command
      !    HARDCOPY
      ! Decode the command line
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sched30m_hardcopy_command
  end interface
  !
  interface
    subroutine sched30m_hardcopy(error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine sched30m_hardcopy
  end interface
  !
  interface
    subroutine parse_file_name(file,basename,wk,vers,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Decode the file version from file name. File name must have the form
      !   wk12v3.dat  (1 or more digits for the version).
      ! Return version as an integer
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: file      !
      character(len=*), intent(out)   :: basename  !
      integer(kind=4),  intent(out)   :: wk        ! Week number
      integer(kind=4),  intent(out)   :: vers      ! Version
      logical,          intent(inout) :: error     !
    end subroutine parse_file_name
  end interface
  !
end module sched30m_interfaces_private
