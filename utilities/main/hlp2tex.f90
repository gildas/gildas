program help
  !------------------------------------------------------------------------
  ! This program produces a Runoff or Tex file from a standard
  ! "GILDAS" help file (similar to VMS help input text file extracted
  ! from a library).
  !------------------------------------------------------------------------
  ! Local variables:
  character in*80, out*100, w1*40, w2*40, accept*1, name*40, new*40
  integer n, l1, l2, level, lw, lenc
  integer iunit, ounit, iback
  logical verb, latex, option
  !------------------------------------------------------------------------
  ! Code:
  option = .false.
  verb = .false.
  name = 'LATEX'
  if (name.eq.'LATEX') then
    latex =.true.
    accept = char(92)          ! This is the Backslash
  else
    latex = .false.
    accept = '_'
  endif
  iunit = 5
  ounit = 6
  level = 0
  l1 = 1
  !
  10    read (iunit,'(a)',end=100) in
  if (.not.verb .and. in.eq.' ') goto 10
  !
  ! n is the Level of information (1 to 3, but 3 is actually
  ! never used)
  n = ichar(in(1:1)) - ichar('0')
  if (n.ge.1 .and. n.le.3) then
    !
    ! End of file
    if (in.eq.'1 ENDOFHELP') goto 100
    !
    ! Help entry line
    call clean(in(2:), out, accept)
    lw = 40
    call sic_blanc(out, lw)
    iback = index(out,'$#$')
    if (iback.ne.0) then
      new = out(1:iback)//char(92)//'backslash'//out(iback+2:)
      out = new
      lw = lenc(out)
    endif
    if (n.eq.1) then
      if (out(1:lw).eq.'LANGUAGE') then
        level = 3
      elseif (latex) then
        level = 1
        w1 = out(1:lw)
        l1 = lw
        if (verb) write(ounit,1000) '\end{verbatim}'
        write(ounit,1000) '\subsection{'//w1(1:l1)//'}'
        write(ounit,1000) '\index{'//w1(1:l1)//'}'
        write(ounit,1000) '\begin{verbatim}'
        verb = .true.
      else
        level = 1
        w1 = out(1:lw)
        l1 = lw
        if (verb) write(ounit,1000) '.END LITERAL'
        write(ounit,1000) '.HL 2 '//w1(1:l1)
        write(ounit,1000) '.INDEX '//w1(1:l1)
        write(ounit,1000) '.LITERAL'
        verb = .true.
      endif
      option = .true.
    elseif (n.eq.2) then
      if (level.eq.3) then
        continue
      elseif (latex) then
        level = 2
        w2 = out(1:lw)
        l2 = lw
        if (verb) write(ounit,1000) '\end{verbatim}'
        if (option) then
          write(ounit,1000) '\subsubsection{'//w1(1:l1)//   &
       &          ' '//w2(1:l2)//'}'
        else
          write(ounit,1000) '\subsubsection{'//w2(1:l2)//'}'
        endif
        write(ounit,1000) '\index{'//w1(1:l1)//   &
     &          '!'//w2(1:l2)//'}'
        write(ounit,1000) '\begin{verbatim}'
        verb = .true.
        if (out(lw:lw).eq.':') option = .false.
      else
        level = 2
        w2 = out(1:lw)
        l2 = lw
        if (verb) write(ounit,1000) '.END LITERAL'
        write(ounit,1000) '.HL 3 '//w2(1:l2)
        write(ounit,1000) '.index '//w1(1:l1)//'>'//w2(1:l2)
        write(ounit,1000) '.LITERAL'
        verb = .true.
      endif
    endif
  elseif (level.ne.3) then
    write(ounit,1000) in(1:lenc(in))
  endif
  goto 10
  !
  ! End of File
  100   continue
  if (verb) then
    if (latex) then
      write(ounit,1000) '\end{verbatim}'
    else
      write(ounit,1000) '.end literal'
    endif
  endif
  stop
  1000  format(a)
end program help
!<FF>
subroutine clean(in,out,accept)
  !------------------------------------------------------------------------
  !------------------------------------------------------------------------
  ! Dummy variables:
  character*(*) in, out, accept
  ! Local variables:
  integer i, k
  !------------------------------------------------------------------------
  ! Code:
  k = 0
  do i=1, len(in)
    if (in(i:i).eq.' ') then
      if (k.ne.0) return
    else
      if (in(i:i).eq.'$'.or.in(i:i).eq.'_'.or.in(i:i).eq.'%') then
        k = k+1
        out(k:) = accept
      endif
      k = k + 1
      if (in(i:i).eq.char(92)) then
        out(k:) = '$#$'
        k = k+2
      else
        out(k:) = in(i:i)
      endif
    endif
  enddo
end
!<FF>
function lenc(c)
  !----------------------------------------------------------------------
  ! SIC	External routine : Integer function
  !
  !	Returns the location of the last SIGNIFICANT character in a
  !       string. Control characters and spaces are non significant...
  ! Arguments :
  !	C	C*(*)	Character string			Input
  !----------------------------------------------------------------------
  character*(*) c
  integer lenc
  integer i,lc
  !
  lc = len(c)
  do i=lc,1,-1
    if (c(i:i).gt.' ') then
      lenc = i
      return
    endif
  enddo
  lenc = 0
end
!<FF>
subroutine sic_blanc(c,n)
  !----------------------------------------------------------------------
  ! SIC	External routine
  !
  ! 	Reduce the character string C to the standard SIC internal syntax.
  !	Suppress all unecessary separators.
  !	Character strings "abcd... " are not modified.
  !	Convert to upper case all characters.
  !	Ignores and destroys comments.
  ! Arguments :
  !	C	C*(*)	Character string to be formatted	Input/Output
  !	N	I	Length of C				Input/Output
  !----------------------------------------------------------------------
  character*(*) c
  integer n
  ! character*1024 tempo
  !
  integer i,nl,nc
  logical lo,li,chaine,some
  integer tab
  parameter (tab=9)
  !
  if (n.eq.0) return
  !
  li = .false.
  lo = .true.
  chaine = .false.
  some = .false.
  nl = n
  nc = len(c)
  n = 0
  i = 1
  !
  do while (i.le.nl)
    !
    ! Skip character strings
    if (c(i:i).eq.'"') then
      ! IF (I.LT.NC) THEN      ! Protect against empty strings
      !    IF (C(I+1:I+1).EQ.'"') THEN
      !       TEMPO = ' '//C(I+1:)
      !       C(I+1:) = TEMPO
      !       NL = MIN(NL+1,NC)
      !    ENDIF
      ! ENDIF
      chaine=.not.chaine
    endif
    if (chaine) goto 30
    !
    ! Suppress redondant separators
    li=.false.
    if (c(i:i).ne.' ' .and. c(i:i).ne.char(tab)) goto 20
    ! Space separator
    li=.true.
    ! Not the first one
    if (lo)  goto 100
    ! First one
    c(i:i)=' '
    goto 30
    !
    ! Skip comment area
    20       continue
    if (c(i:i).eq.'!') then
      some=.true.
      goto 200
    endif
    !
    ! Reset input character string
    30       n=n+1
    if (i.ne.n) c(n:n)=c(i:i)
    if (i.gt.n) c(i:i)=' '
    100      lo=li
    !
    ! End of loop
    i = i+1
  enddo
  !
  ! Remove trailing separators if any
  200   continue
  if (n.ne.0) then
    if (c(n:n).eq.' ') n=n-1
  endif
  ! Clean up end of line
  if( n.lt.nl) c(n+1:) = ' '
  if (some) n=max(n,1)
end
