#!/usr/bin/perl -w

use Getopt::Long;

our($progname) = "gag-parse-messages.pl";

# This utility can be used  to  parse  message  files  produced  by  new
# message facility provided by Gildas since Mar08  release.
#
# Usage:
#    $progname [options/arguments] < file.mes
#
# See subroutine 'usage' for details.

# Retrieve options
GetOptions("pack=s",  \$packages,
           "sever=s", \$sever,
           "n",       \$negate,
           "negate",  \$negate,
           "h",       \$help,
           "help",    \$help,
           "v",       \$version,
           "version", \$version);

# Need help?
if (defined($help)) { &usage }

# Want version?
if (defined($version)) { &show_version }


if (defined($packages)) {
   ($pack_expr = $packages) =~ s/\,/|/g;
} else {
   $pack_expr = ".*";
}

if (defined($sever)) {
   ($seve_expr = $sever) =~ s/\,/|/g;
} else {
   $seve_expr = ".*";
}

while (defined($oneline = <STDIN>)) {

   $c1 = ($oneline =~ /^\s*($pack_expr)\s*:\s+($seve_expr)-/i);

   if (defined($negate)) {
      if (not $c1) { print $oneline; }
   } else {
      if ($c1) { print $oneline; }
   }

} # EndWhile


#========================================================================
sub usage {

    print STDERR "This utility can be used  to  parse  message  files  produced  by  new\n";
    print STDERR "message facility provided by Gildas since Mar08  release.  It  can  be\n";
    print STDERR "used to select kinds and/or proprietary packages messages  come  from.\n";
    print STDERR "This  program  reads  lines  from  standard  input.  Parsing  is  case\n";
    print STDERR "insensitive.\n";
    print STDERR "\n";
    print STDERR "Usage:\n";
    print STDERR "   $progname [options/arguments] < file.mes\n";
    print STDERR "\n";
    print STDERR "Options:\n";
    print STDERR "   -h / --help        : show this help message\n";
    print STDERR "   -v / --version     : show version number\n";
    print STDERR "   --pack=sic,greg,...: take  one  or  more  package  name  as   coma-\n";
    print STDERR "                        separated argument. Default  returns  messages\n";
    print STDERR "                        from all packages.\n";
    print STDERR "   --sever=F,E,...    : take one or  more  severity  letter  as  coma-\n";
    print STDERR "                        separated argument, taken is list F,E,W,R,I,D,\n";
    print STDERR "                        T or U. Default returns messages of all kinds.\n";
    print STDERR "   -n / --negate      : negate the search.\n";
    print STDERR "\n";
    print STDERR "Examples:\n";
    print STDERR "   $progname --pack=mapping < file.mes\n";
    print STDERR "       Show only mapping messages\n";
    print STDERR "   $progname --sever=F,E,W  < file.mes\n";
    print STDERR "       Show only F)atal, E)rrors and W)arnings from all packages\n";
    print STDERR "   $progname --pack=sic --sever=D,T --negate < file.mes\n";
    print STDERR "       Show all messages except D)ebug and T)race coming from sic\n";
    print STDERR "\n";

    exit;

}
#========================================================================
sub show_version {

    my($version) = '$Revision$';

    print STDERR "$progname ".substr($version,1,length($version)-2);
    print STDERR "\n";

    exit;

}
#=======================================================================
