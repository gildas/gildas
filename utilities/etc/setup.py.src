# -*- coding: UTF-8 -*-
# About the above encoding: we do not know in advance what kind of
# character set the users has in his paths (in particular when
# MOD_INC_DIRS and MOD_LIB_DIRS are set). If non-ASCII characters are
# present, Python requires the declaration of an encoding. Using UTF-8
# seems to be the universal solution.

###########################################################################
#
# Compiles pygildas modules
#
###########################################################################

# Debug info
import sys
print("Running Python {0}".format(sys.version))

from setuptools import setup, Extension
import os

mod_sources = '$MOD_SOURCES'
mod_sources = mod_sources.split()

mod_inc_dirs = '$MOD_INC_DIRS'
mod_inc_dirs = mod_inc_dirs.split()

# Extract preprocessing flags (-D) and 32 bits mode (-m32) from ALL_CFLAGS:
all_cflags = '$ALL_CFLAGS'
mod_macros = None
mod_extra_compile_args = None
mod_extra_link_args = None
for flag in all_cflags.split():
   if (flag[0:2] == '-D'):
      flag = flag[2:]
      if (flag.find('=') > 0):
         value = flag[flag.find('=')+1:]
         flag = flag[:flag.find('=')]
      else:
         value = None
      if (mod_macros is None):
         mod_macros = [(flag,value)]
      else:
         mod_macros.append((flag,value))
   elif (flag == '-m32'):
      mod_extra_compile_args=[flag]
      mod_extra_link_args=[flag]
   elif (flag[0:2] == '-I'):
     mod_inc_dirs.append(flag[2:])


mod_lib_dirs = '$MOD_LIB_DIRS'
if (mod_lib_dirs == ''):
  mod_lib_dirs = None
else:
  mod_lib_dirs = mod_lib_dirs.split(':')


# Extract library names from MOD_DEPENDS. There might be some non '-l' flags
mod_depends    = '$MOD_DEPENDS'
mod_lib_names  = None
mod_lib_extras = ""
for flag in mod_depends.split():
   if (flag[0:2] == '-l'):
      flag = flag[2:]
      if (mod_lib_names is None):
         mod_lib_names = [flag]
      else:
         mod_lib_names.append(flag)
   else:
      mod_lib_extras += " "+flag

# More extra arguments required?
mod_extras = '$MOD_EXTRAS'+mod_lib_extras
mod_extras = mod_extras.split()

if (os.environ.get('NUMPY_PRESENT') == "yes"):
   import numpy
   mod_inc_dirs.append(numpy.__path__[0] + '/core/include')
else:
   raise Exception("Numpy python package should be present. Aborting.")


module = Extension('$MOD_IDENTITY',
                   sources            = mod_sources,
                   include_dirs       = mod_inc_dirs,
                   define_macros      = mod_macros,
                   library_dirs       = mod_lib_dirs,
                   libraries          = mod_lib_names,
                   extra_objects      = mod_extras,
                   extra_compile_args = mod_extra_compile_args,
                   extra_link_args    = mod_extra_link_args)

setup(name        = '$MOD_IDENTITY',
      author      = 'GILDAS team (S. BARDEAU)',
      description = 'Communication GILDAS/PYTHON',
      ext_modules = [module])

###########################################################################
