program noise_smooth
  use gildas_def
  use image_def
  use gbl_format
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS	Stand Alone program
  !	Makes a "noise cheating" smoothing, in which data from adjacent
  !	points is averaged until a given total flux is reached. Hence
  !	no smoothing occur for point sources with fluxes higher than
  !	the limit.
  !---------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: pname = 'NOISE_SMOOTH'
  character(len=filename_length) :: namex,namey
  logical :: error
  integer :: ms, i,j, ier
  real :: the
  type(gildas) :: x,y
  !
  call gildas_open
  call gildas_char('Y_NAME$',namey)
  call gildas_char('X_NAME$',namex)
  call gildas_real('THRESHOLD$',the,1)
  call gildas_inte('SMOOTHING$',ms,1)
  call gildas_close
  !
  call gildas_null(y)
  call gdf_read_gildas(y,namey,'.gdf',error,data=.false.)
  if (error) then
    call gagout ('F-'//pname//',  Cannot read input file')
    goto 100
  endif
  !
  call gildas_null(x)
  call gdf_copy_header(y,x,error)
  !
  ! Create output image
  call sic_parsef(namex,x%file,' ','.gdf')
  call gdf_create_image (x,error)
  if (error) then
    call gagout ('F-'//pname//',  Cannot create output image')
    goto 100
  endif
  !
  if (y%gil%eval.ge.0.0) y%gil%eval = max(y%gil%eval,abs(y%gil%bval*1e-7))
  allocate (x%r2d(x%gil%dim(1),x%gil%dim(2)), & 
    & y%r2d(y%gil%dim(1),y%gil%dim(2)),  stat=ier)
  if (ier.ne.0) then
    call gagout ('F-'//pname//',  Memory allocation failure')
    goto 100
  endif
  !    
  do j=1,y%gil%dim(4)
    x%blc(4) = j
    x%trc(4) = j
    do i=1,y%gil%dim(3)
      x%blc(3) = i
      x%trc(3) = i
      y%blc = x%blc
      y%trc = x%trc
      call gdf_read_data(y,y%r2d,error)
      call smo002 (y%r2d,                  &   ! Input image
     &        x%gil%dim(1),x%gil%dim(2),   &   ! Size
     &        x%r2d,                       &   ! Output image
     &        the,                         &   ! Threshold
     &        ms,                          &
     &        y%gil%bval,y%gil%eval)           ! Blanking values
      call gdf_write_data(x,x%r2d,error)
    enddo
  enddo
  call gagout ('I-'//pname//',  Successful completion')
  call sysexi (1)
  !
100 call sysexi (fatale)
end program noise_smooth
!
subroutine smo002(imagein, ncolumns, nlines, imageout, flux,   &
     &    maxpoint, blank, eblank)
  use gildas_def
  integer(kind=index_length) :: ncolumns                  !
  integer(kind=index_length) :: nlines                    !
  real(4) :: imagein(ncolumns,nlines)   !
  real(4) :: imageout(ncolumns,nlines)  !
  real(4) :: flux                       !
  integer :: maxpoint                  !
  real(4) :: blank                      !
  real(4) :: eblank                     !
  ! Local
  integer :: iline,icolumn,k,np,ic,ic1,ic2,il,il1,il2
  real(4) :: x, xx
  do iline=1,nlines
    do icolumn=1,ncolumns
      k=0
      x=imagein(icolumn,iline)
      if (abs(x-blank).le.eblank) then
        x = 0.
        np = 0
      else
        np = 1
      endif
      do while (x.lt.flux .and. k.lt.maxpoint)
        k=k+1
        il1=max(1,iline-k)
        il2=min(nlines,iline+k)
        ic1=max(icolumn-k,1)
        ic2=min(icolumn+k,ncolumns)
        do ic=ic1,ic2
          xx = imagein(ic,il1)
          if (abs(xx-blank).gt.eblank) then
            x = x + xx
            np= np+1
          endif
          xx = imagein(ic,il2)
          if (abs(xx-blank).gt.eblank) then
            x = x + xx
            np= np+1
          endif
        enddo
        il1=max(iline-k+1,1)
        il2=min(iline+k-1,nlines)
        do il=il1,il2
          xx = imagein(ic1,il)
          if (abs(xx-blank).gt.eblank) then
            x = x + xx
            np= np+1
          endif
          xx = imagein(ic2,il)
          if (abs(xx-blank).gt.eblank) then
            x = x + xx
            np= np+1
          endif
        enddo
      enddo
      if (np.ne.0) then
        imageout(icolumn,iline)=x/float(np)
      else
        imageout(icolumn,iline)=blank
      endif
    enddo
  enddo
end subroutine smo002
