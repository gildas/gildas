.ec _
.ll 76
.ad b
.in 4
.ti -4
1 CIMAGE Create a simple geometrical source model
.ti +4

This task create an image which can be read from a file or created from a
function.  The ouput filename is FILEO$.

A- From a FILE:  if the logical variable IFILE$ is set to YES an image is
read from the input formatted file FILEI$ the column must ordered as :  x y
z and expect to have standard gdf image structure.  For convenience, z
should be in Brightness temperature, as for B case.


B- From a FUNCTION: If one of the function is activated a corresponding
image using the parameters PARAMETERS$ will be created.  Intensity are in
Brightness temperature.
 

.nf
E__DISK  : Elliptical Disk
E__RING  : Elliptical Ring
E__GAUS  : Elliptical Gaussian
E__EXPO  : Elliptical Exponential
RECT    : Rectangle
POWER   : Power law
PLAN    : Plan
.fi
 
Convertion to Jansky is possible if variable JANSKY$ is set to YES.
The image can be mutiplied by a gaussian beam if BEAM$ is set to YES
parameters are similar as for function GAUSS.  
       
ONLY ONE function can be computed at the same time,
Nevertheless more complex images can be produced using both  
CIMAGE and COMBINE tasks.

     

.ti -4
2 IFILE$ 
.ti +4
TASK\LOGICAL "use an input FILE" IFILE$ 
Logical variable to use an input file

.ti -4
2 FILEI$ 
.ti +4
TASK\CHARACTER "input FILENAME" FILEI$ 
Name of the input file used to create the gdf image

.ti -4
2 FILEO$ 
.ti +4
TASK\CHARACTER "output FILENAME" FILEO$ 
Name of the output file

.ti -4
2 NXYV$ 
.ti +4
TASK\INTEGER "ARRAY sizes" NXYV$[3]  
Array size : X Y V

.ti -4
2 SIZE$ 
.ti +4
TASK\REAL "Image Size in arcsec" SIZE$ 
Image size lenght (") 

.ti -4
2 SOURCE$
.ti +4
TASK\CHARACTER "Source NAME" SOURCE$ 
Source name

.ti -4
2 LINE$
.ti +4
TASK\CHARACTER "Line NAME" LINE$ 
Line name 

.ti -4
2 FREQUENCY$
.ti +4
TASK\REAL "Frequency (MHz)" FREQUENCY$ 
Line frequency in MHz

.ti -4
2 DF$
.ti +4
TASK\REAL "Frequency Resolution (MHz)" DF$ 
Frequency resolution in MHz

.ti -4
2 E__DISK$
.ti +4
TASK\LOGICAL "E__DISK " E__DISK$ 
Logical variable to compute an elliptical disk function : 
f(x,y)=> (x/a)^2+(y/b)^2<=1 

.ti -4
2 E__RING$
.ti +4
TASK\LOGICAL "E__DISK " E__RING$ 
Logical variable to compute an elliptical ring function : 
f(x,y)=> (x/a)^2+(y/(a*e))^2>=1 and  (x/b)^2+(y/(b*e))^2<=1

.ti -4
2 E__GAUS$
.ti +4
TASK\LOGICAL "E__GAUS " E__GAUS$ 
Logical variable to compute an elliptical gaussian function as
f(x,y)=Tb*exp(-4ln2*[(x/a)^2+(y/b)^2])

.ti -4
2 E__EXPO$
.ti +4
TASK\LOGICAL "EXPONENTIAL " E__EXPO$ 
Logical variable to compute an exponential decreasing function  as
f(x,y)=Tb*exp(-2^C.ln2*[(x/a)^c+(y/b)^c]) ; 
note that for c=2 is equal to an elliptical beam 

.ti -4
2 RECT$
.ti +4
TASK\LOGICAL "RECT " RECT$ 
Logical variable to compute a rectangle function:
f(x,y)=Tb  for  -a/2 < x < a/2
           and  -b/2 < x < b/2 

.ti -4
2 POW$ 
.ti +4
TASK\LOGICAL "POWER " POW$ 
Logical variable to compute a power function as
f(r)=Tb*(r/b)^c   ;  f(r<a)=f(a) ; f(r=b)=Tb

.ti -4
2 PLAN$
.ti +4
TASK\LOGICAL "PLAN  " PLAN$ 
Logical variable to compute a plan  
f(x,y)=ax+by+tb    

.ti -4
2 PARAMETERS$
.ti +4
TASK\REAL "Parameters: " PARAMETERS$[7] 
 the 7 input  parameters are : 

FOR E__DISK,E__GAUS,RECT  
    1,2     X,Y center relative coordinates (") 
    3       Brightness Temperature (K)
    4       Position angle of the source (in DEGREES)
    5,6     Major(a) and Minor(b) axis (")
    7       not used

FOR E__EXPO
    1,2     X,Y center relative coordinates (") 
    3       Brightness Temperature (K)
    4       Position angle of the source (in DEGREES)
    5,6     Major(a) and Minor(b) axis (")
    7       radius's power

FOR E__RING
    1,2     X,Y center relative coordinates (") 
    3       Brightness Temperature (K)
    4       Position angle of the source (in DEGREES)
    5       ring inner equatorial radius     (")
    6       ring outer equatorial radius     (")
    7       flattening factor  (= polar/equatorial radius)

FOR POW
    1,2     X,Y center relative coordinates (") 
    3       Brightness Temperature (K)
    4       A: distance threshold  (")
    5       B: width               (")
    6       C: radius's power
    7       not used

FOR PLAN
    1,2     X,Y center relative coordinates (") 
    3       Brightness Temperature (K)
    4       Position angle of the source (in DEGREES)
    5,6     a,b plan equation coefficient  (1/")
    7       not used


.ti -4
2 JANSKY$ 
.ti +4
TASK\LOGICAL " JANSKY " JANSKY$ 
Intensity scale convertion from Kelvin to Jansky if JANSKY$ is YES

.ti -4
2 BEAM$ 
.ti +4
TASK\LOGICAL " BEAM " BEAM$ 
Mutiplication of the image by a gaussian beam 

.ti -4
2 PARAMETERS2$
.ti +4
TASK\REAL "BEAM Parameters " PARAMETERS2$[7]   
parameters are the same as for function GAUSS

.ti -4
1 ENDOFHELP










