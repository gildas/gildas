program transpose
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS	Transpose a cube
  !     Uses the generic subroutine
  !     GDF_TRANSPOSE
  !     Memory usage can be controlled by logical name SPACE_GILDAS
  !     (on Fortran-90 at least, Fortran-77 version is memory only)
  !---------------------------------------------------------------------
  character(len=filename_length) :: namex,namey
  character(len=4) :: code
  logical :: error
  integer :: space_gildas,ier
  !
  call gildas_open
  call gildas_char('Y_NAME$',namey)
  call gildas_char('X_NAME$',namex)
  call gildas_char('ORDER$',code)
  call gildas_close
  !
  space_gildas = 256
  ier = sic_getlog('SPACE_GILDAS',space_gildas)
  call gdf_transpose(namex,namey,code,space_gildas,error)
end program transpose
