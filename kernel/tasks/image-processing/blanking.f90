program blanking
  use gildas_def
  use image_def
  use gbl_format
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !	Main program to change the blanked pixels
  ! Work in place for efficiency
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: namex,namey
  logical :: error
  integer :: n
  real :: blank(2)
  type(gildas) :: x
  !
  call gildas_open
  call gildas_char('Y_NAME$',namey)
  call gildas_char('X_NAME$',namex)
  call gildas_real('BLANKING$',blank(1),1)
  call gildas_real('TOLERANCE$',blank(2),1)
  call gildas_close
  !
  n = len_trim(namey)
  if (n.eq.0) goto 100
  n = len_trim(namex)
  if (n.eq.0) goto 100
  !
  ! Get a "flat" version of the input data
  call gildas_null(x)
  x%gil%form = fmt_r4
  call gdf_read_gildas(x,namey,'.gdf',error,rank=0)
  if (error) then
    call gagout('F-BLANKING, Cannot read input file')
    goto 100
  endif
  !
  ! Change the blanking
  if (x%gil%blan_words.ne.2 .or. x%gil%eval.lt.0) then
    where (x%r1d.ne.x%r1d) x%r1d = blank(1)
  else
    where (x%r1d.ne.x%r1d) 
      x%r1d = blank(1)
    else where (abs(x%r1d-x%gil%bval).le.x%gil%eval)
      x%r1d = blank(1)
    end where
  endif
  !
  ! Set the header
  x%gil%blan_words = 2
  x%gil%bval = blank(1)
  x%gil%eval = blank(2)
  !
  ! Write the output image
  call sic_parsef(namex,x%file,' ','.gdf') 
  call gdf_write_image(x,x%r1d,error)
  if (error) goto 100
  call gagout('I-BLANKING, Successful completion') 
  call sysexi(1)
  !
100 call sysexi(fatale)
end program blanking
