program header_main
  use gildas_def
  use image_def
  use gkernel_interfaces
  ! Local
  character(len=filename_length) :: file
  type (gildas) :: img
  integer :: n
  logical :: error
  !
  call gildas_open
  call gildas_char('FILE$',file)
  call gildas_close
  n = len_trim(file)
  if (n.eq.0) stop
  !
  call gildas_null(img)
  img%file = file
  call gdf_read_header(img,error)
  if (error) stop
  call gdf_print_header(img)
end program header_main
