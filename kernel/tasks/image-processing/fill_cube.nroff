.ec _
.ll 76
.ad b
.in 4
.ti -4
1 FILL__CUBE Resample a cube by random or regular interpolation
.ti +4
FILL__CUBE

This program resamples an input data cube on a finer grid  for  the
first  two  dimensions (hence the data cube is treated as an ensemble of
images).   The  output  grid  may  be  explicitly  determined  from  the
conversion formulae of the two first axis, or implicitly from the number
of pixels of the two first axis and the input cube conversion  formulae.
Two different methods are available for the resampling :

The SLOW method, which is general and takes properly  into  account
the input blanking value.  This method is based on the same algorithm as
the RANDOM command in  GreG.   It  triangulates  the  input  non-blanked
values  and  uses Lagrange polynomials to interpolate on the finer grid.
For optimisation purposes,  the  same  triangulation  is  used  for  all
planes,  which  assumes  that the same pixels are blanked in all planes.
If this is not the case, the individual planes should be extracted,  and
processed separately.

The FAST method, which ignores  the  input  blanking  value.   This
method  is  based on the same algorithm as the RESAMPLE command in GreG.
It is faster, but should be used only if no input pixel is blanked.

If the data is undersampled, this task is  the  recommended  second
step  in the data analysis of a cube produced by command ANALYSE\CUBE in
CLASS, immediately after  the  TRANSPOSE  program.   Task  MAKE__CUBE  is
preferable for oversampled data.
.ti -4
2 Y__NAME$
.ti +4
TASK\FILE "Input data cube" Y__NAME$ 

This is the name of the input data cube.
.ti -4
2 X__NAME$
.ti +4
TASK\FILE "Output data cube" X__NAME$ 

This is the name of the output resampled data cube.
.ti -4
2 PIXELS$
.ti +4
TASK\INTEGER "Number of pixels" PIXELS$[2] 

These are the dimensions of one plane in  the  resampled  output  image.
They  are usually set to 2 or 3 times the dimensions of the input plane.
Unpredictable results will be obtained  if  the  output  dimensions  are
smaller than the input dimensions.
.ti -4
2 METHOD$
.ti +4
TASK\CHARACTER "Resampling method (FAST or SLOW)" METHOD$ 

This is the resampling method.  FAST method is  faster,  but  only  SLOW
method allows for blanked pixels.
.ti -4
2 MODE$
.ti +4
TASK\LOGICAL "Automatic (YES) or manual (NO) mode" MODE$ 

Determines whether the conversion formulas  for  the  output  plane  are
specified  manually  by  the  user (.FALSE.) or determined automatically
(.TRUE.) from the input plane  conversion  formula  and  the  number  of
pixels.   Automatic  mode  is  usually  sufficient,  unless  you plan to
compare the resampled data with an existing data cube.
.ti -4
2 AXIS__1$
.ti +4
TASK\REAL "First axis conversion formula" AXIS__1$[3] 

This is the conversion formula for the first axis of the  output  plane:
reference  pixel,  value  at  the  reference pixel, and distance between
successive pixels.  This information is only needed if you have selected
the manual mode for the conversion formula.
.ti -4
2 AXIS__2$
.ti +4
TASK\REAL "Second axis conversion formula" AXIS__2$[3]

This is the conversion formula for the second axis of the output  plane:
reference  pixel,  value  at  the  reference pixel, and distance between
successive pixels.  This information is only needed if you have selected
the manual mode for the conversion formula.

.ti -4
2 EXTRAPOLATE$
.ti +4
TASK\LOGICAL "Extrapolate beyond convex hull?" EXTRAPOLATE$

In SLOW method, specify wether extrapolation is allowed or not.
.ti -4
1 ENDOFHELP
