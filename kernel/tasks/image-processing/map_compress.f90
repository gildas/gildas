program map_compress
  use gildas_def
  use gbl_format
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !       COMPRESS a data cube along its TWO first dimensions, by Fourier
  !     Transform
  !---------------------------------------------------------------------
  ! Local
  real, parameter :: eps = 2.e-7
  character(len=filename_length) :: mapin, mapout
  integer :: i,j,ier,n,nx,ny,nexp, ndim, dim(2)
  real :: rexp
  logical :: error
  complex, allocatable :: ipl(:,:), ips(:,:)
  real, allocatable :: ipw(:)
  type(gildas) :: x,y
  !
  call gildas_open
  call gildas_char('INPUT_MAP$',mapin)
  call gildas_char('OUTPUT_MAP$',mapout)
  call gildas_inte('COMPRESS$',nexp,1)
  call gildas_close
  !
  if (nexp.lt.2. .or. nexp.gt.32) then
    write(6,*) 'E-COMPRESS,  Invalid compression ',nexp
    goto 100
  endif
  !
  ! Input file
  n = len_trim(mapin)
  call gildas_null(y)
  y%gil%ndim = 3
  call gdf_read_gildas(y,mapin,'.lmv',error, data=.false.)
  if (error) then
    call gagout('F-MAP_COMPRESS,  Cannot read input cube')
    goto 100
  endif
  !
  call gildas_null(x)
  call gdf_copy_header(y,x,error)
  call sic_parsef(mapout,x%file,' ','.lmv')
  !
  ! Get work space
  rexp = nexp
  nx = nint(float(x%gil%dim(1))/rexp)
  ny = nint(float(x%gil%dim(2))/rexp)
  rexp = max( float(y%gil%dim(1))/float(nx) , float(y%gil%dim(2))/float(ny))
  x%gil%dim(1:2) = nint(y%gil%dim(1:2)/rexp)
  !
  allocate(ipl(y%gil%dim(1),y%gil%dim(2)), ips(x%gil%dim(1),x%gil%dim(2)), &
  & ipw(2*max(y%gil%dim(1),y%gil%dim(2))), stat=ier)
  if (ier.ne.0) goto 100
  allocate(y%r2d(y%gil%dim(1),y%gil%dim(2)), x%r2d(x%gil%dim(1),x%gil%dim(2)), & 
  & stat=ier)
  if (ier.ne.0) goto 100
  !
  ! Transform coordinates (1,1) ---> (1,1)
  x%gil%inc(1:2) = y%gil%inc(1:2)*float(y%gil%dim(1:2))/float(x%gil%dim(1:2))
  x%gil%ref(1:2) = 1.0d0
  x%gil%val(1:2) = (x%gil%ref(1:2)-y%gil%ref(1:2))*y%gil%inc(1:2)+y%gil%val(1:2)
  !
  ! Transform back
  x%gil%ref(1:2) = (y%gil%val(1:2) - x%gil%val(1:2)) / x%gil%inc(1:2) + x%gil%ref(1:2)
  !
  !
  print *,'x%gil%dim ',x%gil%dim
  print *,'y%gil%dim ',y%gil%dim
  print *,'Compress ',nexp,rexp
  call gdf_create_image(x,error)
  if (error) then
    write(6,*) 'F-MAP_COMPRESS,  Cannot create output image'
    goto 100
  endif
  !
  ! Setup FFTW
  ndim = 2
  dim(1) = y%gil%dim(1)
  dim(2) = y%gil%dim(2)
  call fourt_plan(ipl,dim,ndim,-1,0)
  dim(1) = x%gil%dim(1)
  dim(2) = x%gil%dim(2)
  call fourt_plan(ips,dim,ndim,1,1)
  !
  ! Now loop over planes
  do j = 1,x%gil%dim(4)
    y%blc(4) = j
    y%trc(4) = j  
    do i = 1,x%gil%dim(3)
      y%blc(3) = i
      y%trc(3) = i
      x%blc = y%blc
      x%trc = y%trc
      call gdf_read_data(y,y%r2d,error)
      call compress (   &
     &      y%gil%dim(1),y%gil%dim(2),y%r2d,ipl,   &
     &      x%gil%dim(1),x%gil%dim(2),x%r2d,ips,   &
     &      ipw )
      call gdf_write_data(x,x%r2d,error)
    enddo
  enddo
  !
  call gagout('S-MAP_COMPRESS,  Successful completion')
  call sysexi (1)
100 call sysexi (fatale)
end program map_compress
!
subroutine compress(nx,ny,large,wl,mx,my,small,ws,work)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !       Compress an image through FT truncation
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in) :: nx   ! Large X size
  integer(kind=index_length), intent(in) :: ny   ! Large Y size
  real, intent(in) :: large(nx,ny)               ! Input (large) image
  complex, intent(inout) :: wl(nx,ny)            ! Work array
  integer(kind=index_length), intent(in) :: mx   ! Small X size
  integer(kind=index_length), intent(in) :: my   ! Small Y size
  real, intent(out) :: small(mx,my)              ! Ouput (compressed) image
  complex, intent(inout) :: ws(mx,my)            ! Work array
  real, intent(inout) :: work(*)                 ! FFT work space
  ! Local
  integer :: j,ndim,dim(2)
  real :: fact
  !
  wl = large
  !
  ndim = 2
  dim(1) = nx
  dim(2) = ny
  call fourt(wl,dim,ndim,-1,0,work)
  !
  ! Keep inner quarter
  do j=1,my/2
    call r4tor4 (wl(1,j),ws(1,j),mx)
    call r4tor4 (wl(1+nx-mx/2,j),ws(mx/2+1,j),mx)
  enddo
  do j=my/2+1,my
    call r4tor4 (wl(1,j+ny-my),ws(1,j),mx)
    call r4tor4 (wl(1+nx-mx/2,j+ny-my),ws(mx/2+1,j),mx)
  enddo
  dim(1) = mx
  dim(2) = my
  call fourt (ws,dim,ndim,1,1,work)
  fact = 1.0/(nx*ny)
  small = real(ws)*fact
end subroutine compress
