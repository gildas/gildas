program swap
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_format
  !---------------------------------------------------------------------
  !	Mirrors a GDF frame with respect to X or Y axis (2 first dimensions)
  !	and rotate 180 , +90 and -90 .
  !---------------------------------------------------------------------
  ! Local
  character(len=1) :: axis
  character(len=filename_length) :: namey,namex
  logical :: error
  integer :: i, j, iaxis, ier
  type(gildas) :: x,y
  !
  call gildas_open
  call gildas_char('Y_NAME$',namey)
  call gildas_char('X_NAME$',namex)
  call gildas_char('AXIS$',axis)
  call gildas_close
  !
  call gildas_null(y)
  call gdf_read_gildas(y,namey,'.gdf',error,data=.false.)
  if (error) then
    call gagout ('F-SWAP,  Cannot read input file')
    goto 100
  endif
  !
  ! Define the mirroring wanted
  call gildas_null(x)
  call gdf_copy_header(y,x,error)
  call sic_upper(axis)
  if (axis.eq.'X') then
    iaxis = 1
  elseif (axis.eq.'Y') then
    iaxis = 2
  else
    call gagout('F-SWAP,  Unsupported axis '//axis)
    goto 100
  endif
  x%gil%ref(iaxis) =  x%gil%dim(iaxis)-x%gil%ref(iaxis)+1.d0
  x%gil%inc(iaxis) = -x%gil%inc(iaxis)
  !
  ! Create output image
  call sic_parsef(namex,x%file,' ','.gdf')
  call gdf_create_image (x,error)
  if (error) then
    call gagout ('F-SWAP,  Cannot create output file')
    goto 100
  endif
  !
  allocate ( x%r2d(x%gil%dim(1),x%gil%dim(2)), y%r2d(x%gil%dim(1),x%gil%dim(2)), stat=ier) 
  !
  if (iaxis.eq.1) then
    do j=1,x%gil%dim(4)
      x%blc(4) = j 
      x%trc(4) = j
      do i=1,x%gil%dim(3)
        x%blc(3) = i
        x%trc(3) = i
        y%blc = x%blc
        y%trc = x%trc
        call gdf_read_data(y,y%r2d,error)
        if (error) goto 100
        call mirror_1(y%r2d,x%r2d,x%gil%dim(1),x%gil%dim(2))
        call gdf_write_data(x,x%r2d,error)
      enddo
    enddo
  elseif (iaxis.eq.2) then
    do j=1,x%gil%dim(4)
      x%blc(4) = j 
      x%trc(4) = j
      do i=1,x%gil%dim(3)
        x%blc(3) = i
        x%trc(3) = i
        y%blc = x%blc
        y%trc = x%trc
        call gdf_read_data(y,y%r2d,error)
        if (error) goto 100
        call mirror_2(y%r2d,x%r2d,x%gil%dim(1),x%gil%dim(2))
        call gdf_write_data(x,x%r2d,error)
      enddo
    enddo
  endif
  call gagout ('S-SWAP,  Successful completion')
  call sysexi (1)
  !
100 call sysexi (fatale)
end program swap
!
subroutine mirror_1(in,out,nx,ny)
  use gildas_def
  integer(kind=index_length), intent(in) :: nx                     !
  integer(kind=index_length), intent(in) :: ny                     !
  real(4), intent(in) :: in(nx,ny)               !
  real(4), intent(out) :: out(nx,ny)              !
  ! Local
  integer :: j
  !
  do j=1,ny
    out(:,ny-j+1) = in(:,j)
  enddo
end subroutine mirror_1
!
subroutine mirror_2(in,out,nx,ny)
  use gildas_def
  integer(kind=index_length), intent(in) :: nx                     !
  integer(kind=index_length), intent(in) :: ny                     !
  real(4), intent(in) :: in(nx,ny)               !
  real(4), intent(out) :: out(nx,ny)              !
  ! Local
  integer :: i,j
  !
  do j=1,ny
    do i=1,nx
      out(nx-i+1,j) = in(i,j)
    enddo
  enddo
end subroutine mirror_2
