program plane ! En cours
  use gildas_def
  use image_def
  use gkernel_interfaces
  use gbl_format
  !---------------------------------------------------------------------
  ! GDF	Stand alone program
  !	Compute the least square plane through an image, ignoring all
  !       pixels outside a mask, and produce a residual image.
  ! Input
  ! Subroutines
  !	(SIC),(IMAGE)
  !	GDF001,GDF002,PLAN001,PLAN002,PLAN003
  !---------------------------------------------------------------------
  ! Local
  logical :: error
  character(len=filename_length) :: in_name,mask_name,out_name
  integer :: n, ier
  real(8) :: covar(3,3)             ! Inverse of covariance matrix
  real(8), allocatable :: b(:,:)    ! Second member
  real(8), allocatable :: chisq(:)  ! Chi square array
  type(gildas) :: x,y,z
  !
  ! Read parameters
  call gildas_open
  call gildas_char('IN_NAME$',in_name)
  call gildas_char('MASK_NAME$',mask_name)
  call gildas_char('OUT_NAME$',out_name)
  call gildas_close
  !
  ! Read input image
  n = len_trim(in_name)
  if (n.eq.0) call sysexi(fatale)
  call gildas_null(z)
  call gdf_read_gildas(z,in_name,'.gdf',error, data=.false.)
  if (error) then
    write(6,*) 'E-PLANE,  Cannot read input file ',   &
     &      z%file
    goto 100
  endif
  !
  ! Read input mask
  n = len_trim(mask_name)
  if (n.eq.0) call sysexi(fatale)
  call gildas_null(y)
  y%gil%ndim = 2
  call gdf_read_gildas(y,mask_name,'.gdf',error)
  if (error) then
    write(6,*) 'E-PLANE,  Cannot read input mask ',   &
     &      y%file
    goto 100
  endif
  !
  if (z%gil%dim(1).ne.y%gil%dim(1).or.z%gil%dim(2).ne.y%gil%dim(2)) then
    write(6,*) 'E-PLANE,  Data and mask have incompatible',   &
     &      ' dimensions'
    write(6,*) z%gil%dim
    write(6,*) y%gil%dim
    goto 100
  endif
  !
  ! Prepare output file
  call gildas_null(x)
  call gdf_copy_header(z,x,error)
  !
  ! Create residual image
  x%gil%eval = 0                   ! No tolerance on blanking
  n = len_trim(out_name)
  call sic_parsef(out_name(1:n),x%file,' ','.gdf')
  call gdf_create_image(x,error)
  if (error) then
    write(6,*) 'F-PLANE,  Cannot create residual cube'
    goto 100
  endif
  !
  ! Re-shape to Rank-3 arrays
  z%gil%ndim = 3
  z%gil%dim(3) = z%loca%size / (z%gil%dim(1)*z%gil%dim(2))
  x%gil%ndim = 3
  x%gil%dim = z%gil%dim
  !
  ! Get work space
  allocate (b (3,z%gil%dim(3)), chisq(z%gil%dim(3)), & 
  & z%r3d (z%gil%dim(1),z%gil%dim(2),z%gil%dim(3)), & 
  & x%r3d (x%gil%dim(1),x%gil%dim(2),x%gil%dim(3)), stat=ier)
  call gdf_read_data(z,z%r3d,error)
  !
  ! Find plane equation(s)
  call fit_plane(z%gil%dim(1),z%gil%dim(2),z%gil%dim(3),   &
     &    z%r3d, y%r2d, b, covar, chisq, z%gil%bval, z%gil%eval, error)
  if (error) then
    write(6,*) 'E-PLANE,  Error in least square fit'
    goto 100
  endif
  ! Mask is no longer needed
  deallocate (y%r2d)
  !
  ! Fill residual image
  call subtract_plane(z%r3d, x%r3d,   &
     &    x%gil%dim(1),x%gil%dim(2),x%gil%dim(3),b,z%gil%bval,z%gil%eval)
  call gdf_write_data(x,x%r3d,error)
  call gagout('S-PLANE,  Successful completion')
  call sysexi (1)
  !
100 call sysexi (fatale)
end program plane
!
subroutine fit_plane(nx,ny,nplane,values,weight,beta,   &
     &    covar,chisq,bval,eval,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! Given a set of NDATA points X(I),Y(I) with individual weights WEIGHT(I), use
  ! Chi2 minimization to determine the MA coefficients A of a function that
  ! depends linearly on A.
  !---------------------------------------------------------------------
  integer(kind=index_length) :: nx                     !
  integer(kind=index_length) :: ny                     !
  integer(kind=index_length) :: nplane                 !
  real :: values(nx,ny,nplane)      !
  real :: weight(nx,ny)             !
  real(8) :: beta(3,nplane)          !
  real(8) :: covar(3,3)              !
  real(8) :: chisq(nplane)           !
  real :: bval                       !
  real :: eval                       !
  logical :: error                  !
  ! Local
  integer :: i,j,k,l,n
  real :: sig2i,sum,ym,wt,afunc(3)
  integer(kind=index_length), parameter :: one=1
  !
  error = .false.
  !
  ! No blanking: can use a single design matrix for all planes
  write(6,*) 'Blank: ',bval,eval
  if (eval.lt.0) then
    do k=1,3
      do j=1,3
        covar(j,k)=0.
      enddo
      do j=1,nplane
        beta(k,j)=0.
      enddo
    enddo
    afunc(1) = 1
    do j=1,ny
      afunc(3) = j
      do i=1,nx
        afunc(2) = i
        sig2i=weight(i,j)
        do k=1,3
          wt=afunc(k)*sig2i
          do l=1,k
            covar(k,l)=covar(k,l)+wt*afunc(l)
          enddo
          do l=1,nplane
            ym       =values(i,j,l)
            beta(k,l)=beta(k,l)+ym*wt
          enddo
        enddo
      enddo
    enddo
    !     Symetrise
    if (3.gt.1) then
      do j=2,3
        do k=1,j-1
          covar(k,j)=covar(j,k)
        enddo
      enddo
    endif
    ! Solve equations
    call gaussj(covar,3,3,beta,nplane,nplane,error)
    if (error) return
    !
    ! Input cube may contain blanked data: must accumulate a design matrix for
    ! every plane
  else
    do n=1,nplane
      do k=1,3
        do j=1,3
          covar(j,k)=0.
        enddo
        beta(k,n)=0.
      enddo
      afunc(1) = 1
      do j=1,ny
        afunc(3) = j
        do i=1,nx
          if (abs(values(i,j,n)-bval).gt.eval) then
            afunc(2) = i
            sig2i=weight(i,j)
            do k=1,3
              wt=afunc(k)*sig2i
              do l=1,k
                covar(k,l)=covar(k,l)+wt*afunc(l)
              enddo
              ym       =values(i,j,n)
              beta(k,n)=beta(k,n)+ym*wt
            enddo
          endif
        enddo
      enddo
      !     Symetrise
      if (3.gt.1) then
        do j=2,3
          do k=1,j-1
            covar(k,j)=covar(j,k)
          enddo
        enddo
      endif
      ! Solve equations
      call gaussj(covar,3,3,beta(1,n),one,one,error)
      if (error) return
    enddo
  endif
  !
  ! Evaluate Chi square for each plane
  write(6,*) '! A_1 A_I A_J Chi**2'
  do n=1,nplane
    chisq(n)=0.
    do j=1,ny
      do i=1,nx
        if (abs(values(i,j,n)-bval).gt.eval) then
          afunc(1) = 1.
          afunc(2) = i
          afunc(3) = j
          sum=0.
          do k=1,3
            sum=sum+beta(k,n)*afunc(k)
          enddo
          chisq(n)=chisq(n)+   &
     &            ((values(i,j,n)-sum)*weight(i,j))**2
        endif
      enddo
    enddo
    write(6,*) (beta(k,n),k=1,3),chisq(n)
  enddo
  return
end subroutine fit_plane
!
subroutine gaussj(a,n,np,b,m,mp,error)
  use gildas_def
  integer(kind=4)            :: np
  real(kind=8)               :: a(np,np)
  integer(kind=4)            :: n
  integer(kind=index_length) :: mp
  real(kind=8)               :: b(np,mp)
  integer(kind=index_length) :: m
  logical                    :: error
  ! Local
  integer, parameter :: nmax=50
  real(8) big,dum,pivinv
  integer ipiv(nmax),indxr(nmax),indxc(nmax)
  integer i,j,k,l,ll,irow,icol
  !
  irow = 0
  icol = 0
  !
  ipiv(:)=0
  !
  do i=1,n
    big = 0.
    do j=1,n
      if (ipiv(j).ne.1) then
        do k=1,n
          if (ipiv(k).eq.0) then
            if (abs(a(j,k)).ge.big)then
              big=abs(a(j,k))
              irow=j
              icol=k
            endif
          elseif (ipiv(k).gt.1) then
            write(6,*) 'E-PLANE,  Singular matrix'
            error = .true.
            return
          endif
        enddo
      endif
    enddo
    ipiv(icol)=ipiv(icol)+1
    if (irow.ne.icol) then
      do l=1,n
        dum=a(irow,l)
        a(irow,l)=a(icol,l)
        a(icol,l)=dum
      enddo
      do l=1,m
        dum=b(irow,l)
        b(irow,l)=b(icol,l)
        b(icol,l)=dum
      enddo
    endif
    indxr(i)=irow
    indxc(i)=icol
    if (a(icol,icol).eq.0.) then
      write(6,*)  'E-PLANE,  Singular matrix.'
      error = .true.
      return
    endif
    pivinv=1./a(icol,icol)
    a(icol,icol)=1.
    do l=1,n
      a(icol,l)=a(icol,l)*pivinv
    enddo
    do l=1,m
      b(icol,l)=b(icol,l)*pivinv
    enddo
    do ll=1,n
      if (ll.ne.icol) then
        dum=a(ll,icol)
        a(ll,icol)=0.
        do l=1,n
          a(ll,l)=a(ll,l)-a(icol,l)*dum
        enddo
        do l=1,m
          b(ll,l)=b(ll,l)-b(icol,l)*dum
        enddo
      endif
    enddo
  enddo
  do l=n,1,-1
    if (indxr(l).ne.indxc(l)) then
      do  k=1,n
        dum=a(k,indxr(l))
        a(k,indxr(l))=a(k,indxc(l))
        a(k,indxc(l))=dum
      enddo
    endif
  enddo
end subroutine gaussj
!
subroutine subtract_plane(in,out,nx,ny,nplane,plane,bval,eval)
  use gildas_def
  integer(kind=index_length) :: nx                     !
  integer(kind=index_length) :: ny                     !
  integer(kind=index_length) :: nplane                 !
  real :: in(nx,ny,nplane)             !
  real :: out(nx,ny,nplane)            !
  real(8) :: plane(3,nplane)           !
  real :: bval                         !
  real :: eval                     !
  ! Local
  integer :: i,j,k
  !
  do k=1,nplane
    do j=1,ny
      do i=1,nx
        if (abs(in(i,j,k)-bval).gt.eval) then
          out(i,j,k) = in(i,j,k)-plane(1,k)   &
     &            -plane(2,k)*i-plane(3,k)*j
        else
          out(i,j,k) = bval
        endif
      enddo
    enddo
  enddo
end subroutine subtract_plane
