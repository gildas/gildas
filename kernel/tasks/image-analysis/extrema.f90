program extrema_main
  use gildas_def
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS Task EXTREMA
  !
  !  Update the extrema of a GILDAS data set
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: file
  integer :: n
  logical :: error
  type(gildas) :: x
  !
  call gildas_open
  call gildas_char('FILE$',file)
  call gildas_close
  n = len_trim(file)
  if (n.eq.0) stop
  !
  call gildas_null(x)
  ! Ask for a "flat" array description
  call gdf_read_gildas(x, file, '.gdf', error, rank=0, data=.true.)
  if (error) goto 99
  !
  if (x%gil%type_gdf.eq.code_gdf_uvt) then
    call gdf_get_baselines(x,error)
  else if (x%gil%type_gdf.eq.code_gdf_tuv) then
    call gdf_get_baselines(x,error)
  else
    call gdf_get_extrema(x,error)
  endif
  if (error) goto 99
  !
  call gdf_update_header(x,error)
  if (.not.error) then
    stop 'S-EXTREMA,  Header successfully updated'
  endif
99 call sysexi(fatale)
end program extrema_main
