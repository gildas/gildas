.ec _
.ll 76
.ad b
.in 4
.ti -4
1 BACKGROUND Compute background intensity of an image
.ti +4
BACKGROUND

Computes the background image of a given map.  The basic idea is to
make  a  crude  mesh  model of the background by finding the most likely
value of the original map (within some intensity range) for each cell of
the  crude  mesh.  The most likely value is found by making an histogram
of the intensity distribution in the  specified  range  for  all  points
lying  in  a circle around the cell center.  The circle may (and should)
be greater than the cell size.  If  the  number  of  pixels  within  the
intensity range is too small, no value is attributed to the intermediate
mesh.

The intermediate model is then resampled to the original map  using
a   general   triangulation   technique.   This  procedure  has  several
advantages over a simpler method which would compute a "smoothed"  image
as  the background, because it is not biased by any emission outside the
selected range, and it is able to interpolate  over  large  non  sampled
area of the intermediate mesh.  Other tasks with similar names use other
interpolation algorithms.  They will hopefully be merged into  a  single
task one (with a switch for the interpolation algorithm).

CAUTION:  In the present version, blanking in the  input  image  is
not correctly handled.  This can usually be worked around by specifiying
an apropriate range.
.ti -4
2 Y__NAME$
.ti +4
TASK\FILE/OLD "Input file name" Y__NAME$ 

The name of the input image on which the background emission  is  to  be
determined.
.ti -4
2 X__NAME$
.ti +4
TASK\FILE "Output file name" X__NAME$ 

The name of the output file containing the background emission.
.ti -4
2 X__MIN$
.ti +4
TASK\REAL "Lowest value for selection" X__MIN$ 

The minimum value of the background emission.  Values lower than  X__MIN$
are considered as real source structure.
.ti -4
2 X__MAX$
.ti +4
TASK\REAL "Highest value for selection" X__MAX$ 

The maximum value of the background emission.  Values higher than X__MAX$
are considered as real source structure.
.ti -4
2 NBINS$
.ti +4
TASK\INTEGER "Number of histogram bins" NBINS$ 

The number of histogram bins used to derive the value of the  background
emission.   This  number  must  be high enough to derive the most likely
value over the smoothing circle, but small enough compared to the number
of  pixels  in  the smoothing circle to get good statistics on each bin.
It should usually be of the order of the square root of  the  number  of
pixels within the smoothing circle
.ti -4
2 WIDTH$
.ti +4
TASK\INTEGER "Width of smoothing area (pixels)" WIDTH$ 

Radius (in pixels) of the smoothing circle.  The number of pixels in the
smoothing  circle  must  be high enough to get statistically significant
value.
.ti -4
2 SCALE$
.ti +4
TASK\INTEGER "Compression factor (power of 2)" SCALE$ 

The ratio between the sizes of original image and background  mesh.   It
must  be  a  power of 2, and depends on the size scale of the background
variations that you want to remove.  Large values of  SCALE$  will  only
remove  very  large scale background.  Low values will also remove small
scale background (and may be some source structure as well...)
.ti -4
2 N__MIN$
.ti +4
TASK\INTEGER "Minimum number of valid pixels in smoothing area" N__MIN$ 

A minimum number of pixels in the histogram to consider  the  result  as
significant.   This  number  is  used to avoid biasing the background by
areas where few points lie in the range of the background emission.   It
should  be  typically  50  %  of  the  number of pixels in the smoothing
circle.  The background mesh value is blanked when too few valid  pixels
are found, and these points are ignored by the interpolation.
.ti -4
1 ENDOFHELP
