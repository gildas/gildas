.ec _
.ll 76
.ad b
.in 4
.ti -4
1 COMBINE Add, multiply, divide, etc...  two input images
.ti +4
COMBINE

This task makes "combinations" of two input  images  to  produce  a
third  one.   The  two input images may have the same dimensions, or the
first one (Z one) may have less dimensions than the second (Y) one.   In
the latter case, combinations will occur for all the extra planes of the
Y image.  For example you can divide all planes of an input (Y) 3-D cube
by  a  2-D (Z) image, provided each plane of the cube matches the single
image...

Operations are
.nf
        ADD             X = Ay*Y + Az*Z + C
        MULTIPLY        X = AY*Y * AZ*Z + C
        DIVIDE          X = AY*Y / AZ*Z + C
        OPTICAL__DEPTH   X = - LOG (AY*Y / AZ*Z + C)
.fi
provided Y > TY and Z > TZ , TY and TZ being thersholds for the Y and  Z
images.

Image combinations may  also  be  done  using  the  SIC  arithmetic
capabilities,  but  COMBINE  offers  the advantage of handling correctly
blanking information.
.ti -4
2 Z__NAME$
.ti +4
TASK\FILE "First input map" Z__NAME$ 

This is the name of the input map with the smaller number of dimensions.
.ti -4
2 Z__FACTOR$
.ti +4
TASK\REAL "Scaling factor" Z__FACTOR$ 

This is a scaling factor for map Z__NAME$.
.ti -4
2 Z__MIN$
.ti +4
TASK\REAL "Threshold" Z__MIN$ 

This is a threshold on map Z__NAME$. For all values below this threshold,
the output image will be blanked.
.ti -4
2 Y__NAME$
.ti +4
TASK\FILE "Second input map" Y__NAME$ 

This is the name of the input map with the larger number of dimensions.
.ti -4
2 Y__FACTOR$
.ti +4
TASK\REAL "Scaling factor" Y__FACTOR$ 

This is a scaling factor for map Y__NAME$.
.ti -4
2 Y__MIN$
.ti +4
TASK\REAL "Threshold" Y__MIN$ 

This is a threshold on map Y__NAME$. For all values of Y below this
threshold, the output image will be blanked.
.ti -4
2 X__NAME$
.ti +4
TASK\FILE "Output map" X__NAME$ 

This is the name of the output map.
.ti -4
2 BLANKING$
.ti +4
TASK\REAL "New blanking value" BLANKING$ 

This is the blanking value chosen for the output map.
.ti -4
2 OFFSET$
.ti +4
TASK\REAL "Output offset" OFFSET$ 

This is an offset added to the output map.
.ti -4
2 FUNCTION$
.ti +4
TASK\CHARACTER "Function" FUNCTION$ 

Selected operation.  Possible operations are ADD, MULTIPLY, DIVIDE (Y by
Z), and OPTICAL__DEPTH (-Log(DIVIDE)).
.ti -4
1 ENDOFHELP
