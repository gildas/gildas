program example_gildas
  use gildas_def
  use gkernel_interfaces
  ! Local
  integer(kind=4) :: i
  real(kind=8) :: a
  real(kind=4) :: array(4)
  character(len=40) :: chain
  character(len=filename_length) :: file,name
  !
  call gildas_open
  call gildas_inte('I$',i,1)
  call gildas_dble('A$',a,1)
  call gildas_char('CHAIN$',chain)
  call gildas_real('ARRAY$',array,4)
  call gildas_char('FILE$',file)
  call gildas_char('OLD$',name)
  call gildas_close
  !
  if (a.lt.0.0 .or. a.gt.1.0) then
    call gagout('F-EXAMPLE, A is out of bounds')
    goto 100
  endif
  !
  write(6,*) i
  write(6,*) a
  write(6,*) chain
  write(6,*) array
  write(6,*) trim(file)
  write(6,*) trim(name)
  !
  ! Success
  call gagout('I-GDF, Successful completion')
  call sysexi(1)
  !
100 continue
  call sysexi(fatale)
end program example_gildas
