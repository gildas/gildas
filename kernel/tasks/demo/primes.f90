program primes
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !  Compute the primes numbers from 1 to p (defined below). This
  ! program is easily (auto)parallelizable. This is its only purpose.
  !---------------------------------------------------------------------
  integer(kind=4) :: status
  integer(kind=8) :: m,i,j,up
  integer(kind=8), allocatable :: list(:)
  logical :: p
  !
  p = .false.
  m = 2000000  ! Use explicit, non-customizable value. If 'm' not known
               ! at compilation time, ifort won't auto-parallelize the
               ! loops, assuming there are chances that the cost is too
               ! high (because of the -par-threshold100 conservative
               ! option)
  !
  allocate(list(m),stat=status)
  if (status.ne.0) then
    call gagout('Failed to allocate the list array')
    stop
  endif
  !
  list(1) = 0
  do i=2,m  ! Safe for auto-parallelization
    list(i) = i
  enddo
  !
  do i=2,m  ! Safe for auto-parallelization
    up = int(sqrt(real(i,kind=8)),kind=8)
    do j=2,up  ! Not safe for auto-parallelization
      if (list(i).ne.j .and. mod(list(i),j).eq.0) then
        list(i) = 0
      endif
    enddo
  enddo
  !
  if (p) then
    do i=1,m
      if (list(i).ne.0) then
        write(*,*) list(i)
      endif
    enddo
  endif
  !
  deallocate(list)
  !
end program primes
