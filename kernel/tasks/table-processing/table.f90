program table
  use gildas_def
  use image_def
  use gkernel_interfaces
  use gbl_format
  !---------------------------------------------------------------------
  ! GILDAS	Stand alone program
  !	Builds a table from list-directed input file
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: outfile,infile,filein
  integer :: nline,ncol, ier
  logical :: error
  type(gildas) :: x
  real, allocatable :: array(:,:)
  !
  error = .false.
  !
  call gildas_open
  call gildas_char('FILE$',filein)
  call gildas_char('TABLE$',outfile)
  call gildas_inte('LINES$',nline,1)
  call gildas_inte('COLUMNS$',ncol,1)
  call gildas_close
  !
  call sic_parsef(filein,infile,' ','.dat')
  ier = sic_open (1,infile,'OLD',.true.)
  if (ier.ne.0) then
    call gagout('F-TABLE,  Cannot read data file ')
    call putios('E-TABLE,  ',ier)
    goto 100
  endif
  if (nline.le.0) then
    call gagout ('W-TABLE,  Trying to guess how many lines')
	allocate(array(1,ncol),stat=ier)
	if (ier.ne.0) then
		call gagout('E-TABLE,  Memory allocation error')
		goto 100
	endif
    call subread_one(nline,ncol,array,error)
    if (error) goto 100
    deallocate(array)
    rewind(1)
  else
    call gagout ('W-TABLE,  Assuming exact number of lines')
  endif
  !
  allocate(array(nline,ncol),stat=ier)
  if (ier.ne.0) then
    call gagout('E-TABLE, Memory allocation error')
    error = .true.
	  goto 100
  endif
  call subread_all(nline,ncol,array,error)
  if (error) goto 100
  !
  ! Create new file
  call gildas_null (x, TYPE = 'TABLE')
  x%gil%dim(1) = nline
  x%gil%dim(2) = ncol
  call sic_parsef(outfile,x%file,' ','.dat')
  !
  call gdf_write_image(x,array,error)
  if (error) goto 100
  call gagout ('S-TABLE,  Successful completion')
  call sysexi (1)
  !
100 call sysexi (fatale)
end program table
!
subroutine subread_one(nline,ncol,work,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !     Modif GD (27/11/92) permettant de lire des fichiers avec commentaires
  !     Cleaned-up by S.G.
  !---------------------------------------------------------------------
  integer, intent(out)   :: nline                  !
  integer, intent(in)    :: ncol                   !
  real,    intent(out)   :: work(ncol)             !
  logical, intent(inout) :: error                  !
  ! Local
  character(len=80) :: line
  integer :: i,ier
  !
  i = 0
  do while(.true.)
    read(1,*,iostat=ier,end=10) work
    if (ier.eq.0) i = i+1
  enddo
10 continue
  if (i.eq.0) then
    call gagout('E-TABLE,  No Valid Lines Found')
    error = .true.
    return
  endif
  nline = i
  write (line,1000) nline
  call  gagout(line)
1000 format('I-TABLE,  Found ',i8,' valid lines')
end subroutine subread_one
!
subroutine subread_all(nline,ncol,work,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !     Modif GD (27/11/92) permettant de lire des fichiers avec commentaires
  !     Cleaned-up by S.G.
  !---------------------------------------------------------------------
  integer                :: nline
  integer                :: ncol
  real                   :: work(nline,ncol)
  logical, intent(inout) :: error
  ! Local
  character(len=80) :: line
  integer :: igood,iline,j,ier
  !
  igood=0
  iline=0
  do while (.true.)
    iline = iline+1
    igood = igood+1
    read(1,*,iostat=ier,end=98) (work(igood,j),j=1,ncol)
    if (ier.ne.0) then
      write (line,1000) iline
      igood=igood-1
      call gagout(line)
    endif
    if (igood.eq.nline) return
  enddo
98 call gagout('E-TABLE,  End of file during read')
  error = .true.
1000 format('E-TABLE,  List directed I/O syntax error at line #',i8)
end subroutine subread_all
