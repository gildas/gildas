program merge
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS	Stand Alone Program
  !	Merges column by column or line by line
  !     two input tables into a third one
  ! S. Guilloteau
  !---------------------------------------------------------------------
  ! Global
  type (gildas) :: tab1
  type (gildas) :: tab2
  type (gildas) :: out
  !
  logical :: error,doline
  integer :: ier
  character(len=filename_length) :: namex,namey,namez
  !
  ! Open file
  call gildas_open
  call gildas_char('Z_TABLE$',namez)
  call gildas_char('Y_TABLE$',namey)
  call gildas_char('X_TABLE$',namex)
  call gildas_logi('LINE$',doline,1)
  call gildas_close
  !
  call gildas_null(tab1)
  call gildas_null(tab2)
  call gildas_null(out, type = 'TABLE')
  call sic_parsef(namez,tab1%file,' ','.tab')
  call sic_parsef(namey,tab2%file,' ','.tab')
  call sic_parsef(namex,out%file,' ','.tab')
  !
  ! Check files
  call merge_file_check(tab1,tab2,out,doline,error)
  if (error) then
     call sysexi(fatale)
  endif
  !
  ! Allocate work space
  allocate ( &
       tab1%r2d(tab1%gil%dim(1),tab1%gil%dim(2)), &
       tab2%r2d(tab2%gil%dim(1),tab2%gil%dim(2)), & 
       out%r2d(out%gil%dim(1),out%gil%dim(2)), &
       stat=ier)
  if (ier.ne.0) then
     call gagout('E-MERGE, Memory allocation error')
     call sysexi(fatale)
  endif
  call gdf_read_data(tab1,tab1%r2d,error)
  call gdf_read_data(tab2,tab2%r2d,error)
  !
  call merge_file_do(tab1,tab2,out,doline)
  call gdf_write_image(out,out%r2d,error)
  if (error) call sysexi(fatale)
  !
  call gagout('S-MERGE,  Successful completion')
end program merge
!  
subroutine merge_file_check(tab1,tab2,out,doline,error)
  use image_def
  use gkernel_interfaces
  type (gildas), intent(inout) :: tab1
  type (gildas), intent(inout) :: tab2
  type (gildas), intent(inout) :: out
  logical, intent(in) :: doline ! Line or Column mode
  logical, intent(out) :: error
  !
  integer icheck
  character(len=10) verb
  !
  call gdf_read_header(tab1,error)
  if (error) then 
     call gagout('F-MERGE, Cannot read Table 1')
     return 
  endif
  if (tab1%gil%ndim.ne.2) then
     call gagout('F-MERGE, File 1 is not a 2-D Table')
     error = .true.
     return
  endif
  !
  call gdf_read_header(tab2,error)
  if (error) then 
     call gagout('F-MERGE, Cannot read Table 2')
     return 
  endif
  if (tab2%gil%ndim.ne.2) then
     call gagout('F-MERGE, File 2 is not a 2-D Table')
     error = .true.
     return
  endif
  !
  ! Line mode: must have same number of Columns
  if (doline) then
     icheck = 2
     verb = 'Columns'
  else
     icheck = 1
     verb = 'Lines'
  endif
  if (tab1%gil%dim(icheck).ne.tab2%gil%dim(icheck)) then
     call gagout('E-MERGE, Tables do not have the same number of '//verb)
     error = .true.
     return
  endif
  !
  out%gil%ndim = 2
  out%gil%dim(icheck) = tab1%gil%dim(icheck)
  out%gil%dim(3-icheck) = tab1%gil%dim(3-icheck) + tab2%gil%dim(3-icheck)
end subroutine merge_file_check
!
subroutine merge_file_do(tab1,tab2,out,doline)
  use image_def
  type (gildas), intent(inout) :: tab1
  type (gildas), intent(inout) :: tab2
  type (gildas), intent(inout) :: out
  logical, intent(in) :: doline ! Line or Column mode
  !
  integer :: i,nc,nx,ny,nz
  !
  ! Line mode: must have same number of Columns
  if (doline) then
     nc = out%gil%dim(2)
     nx = tab1%gil%dim(1)
     ny = nx+1
     nz = out%gil%dim(1)
     do i=1,nc
        out%r2d(1:nx, i) = tab1%r2d(:,i)
        out%r2d(ny:nz,i) = tab2%r2d(:,i)
     enddo
  else
     nc = tab1%gil%dim(1)
     nx = tab1%gil%dim(2)
     ny = nx+1
     nz = out%gil%dim(2)
     out%r2d(:,1:nx) = tab1%r2d
     out%r2d(:,ny:nz) = tab2%r2d
  endif
end subroutine merge_file_do

