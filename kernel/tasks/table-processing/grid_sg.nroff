.ec _
.ll 76
.ad b
.in 4
.ti -4
1 GRID__SG Optimum gridding of irregularly sampled data from an input table
.ti +4
GRID__SG

Optimum image reconstruction from irregularly sampled data with finite
angular resolution. 

This task makes a cube from a table  containing irregularly sampled  data.
The data is interpolated/extrapolated using a convolution taking into 
account the beam size of the observing telescope. The smoothing effect of
the convolution can be optionally removed by this task, by Fourier
filtering. Fourier filtering is also used to suppress high spatial
frequency noise. The resulting image is thus optimal in terms
of signal to noise and angular resolution.

In mode NATURAL, a convolution kernel is used to interpolate the irregularly
sampled data using the weight of each individual point.

In mode UNIFORM, the same convolution process is used but the individual
points are assumed to have equal weight.

The pixel size is specified by the user, but the map size is determined 
by the task.  The input  table can be created by command GRID in CLASS,
or using task TABLE from a formatted file.

.ti -4
2 TABLE$
.ti +4
TASK\CHARACTER "Input TABLE name" TABLE$

Specify the name of the input table. 

.ti -4
2 MAP$
.ti +4 
TASK\CHARACTER "Output MAP name" MAP$

Specify the name of the output map. The output map will be in VLM
order (Velocity, X coordinate, Y coordinate).

.ti -4
2 XCOL$
.ti +4 
TASK\INTEGER "Column of X coordinates" XCOL$

Specify the column of the table containing the X coordinates

.ti -4
2 YCOL$
.ti +4 
TASK\INTEGER "Column of Y coordinates" YCOL$

Specify the column of the table containing the Y coordinates

.ti -4
2 WCOL$
.ti +4 
TASK\INTEGER "Column for weights" WCOL$

Specify the column containing the weights 

.ti -4
2 MCOL$
.ti +4 
TASK\INTEGER "First and last column to grid" MCOL$[2]

Specify the first and last column to be regridded. Note that the
weight column can be included in order to produce a weight image.
This can allow further processing based on Signal-to-Noise ratio.

.ti -4
2 WEIGHT__MODE$
.ti +4 
TASK\CHARACTER "Gridding Mode (NAtural or UNiform)" WEIGHT__MODE$

Indicate the weight mode. NATURAL indicates a convolution with 
proper weighting, UNIFORM a convolution without weighting.

.ti -4
2 TOLE$
.ti +4 
TASK\REAL "X-Y position tolerance" TOLE$

Positional tolerance. Points separated less than TOLE$ (on each axis)
are added together before resampling. 

.ti -4
2 BEAM$
.ti +4 
TASK\REAL "Primary Beam Size" BEAM$

Size of the beam used to obtain the data (in user units, not in pixels)

.ti -4
2 DIAM$
.ti +4
TASK\REAL "Antenna diameter"  DIAM$ 

Antenna diameter to be use in the  Fourier filtering process. Higher
spatial frequencies are filtered out completely.

.ti -4
2 MIN__WEIGHT$
.ti +4 
TASK\REAL "Minimum weight"  MIN__WEIGHT$  

Minimum relative weight under which pixels in the final image are blanked.

.ti -4
2 GRID$
.ti +4
TASK\LOGICAL "Correct for gridding ?" GRID$ 

Indicate whether the smoothing effect of the convolution kernel should
be corrected or not before producing the final image.

.ti -4
1 ENDOFHELP
