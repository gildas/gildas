program regression
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_format
  !---------------------------------------------------------------------
  ! GDF	Main program for finding best linear combination
  !		X(i,j) = A*Y(i,j) + B
  ! Subroutines
  !	REG001
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: name
  logical :: error
  real :: a,b, r, the
  real :: xm,ym,x2m,y2m,xym, x0,y0
  real :: xmean,ymean,x2mean,y2mean,xymean
  integer(kind=index_length) :: xmin,xmax,ymin,ymax
  integer(kind=size_length) :: np
  type(gildas) :: x
  !
  call gildas_open
  call gildas_char('IN$',name)
  call gildas_real('THRESHOLD$',the,1)
  call gildas_close
  !
  call gildas_null(x)
  x%gil%ndim = 2
  call gdf_read_gildas(x,name,'.gdf',error)
  if (error) call sysexi(fatale)
  !
  xmin = 1
  xmax = x%gil%dim(1)
  ymin = 1
  ymax = x%gil%dim(2)
  call reg001(x%r2d,x%gil%dim(1),x%gil%dim(2),the,  &
              xmin,xmax,ymin,ymax,                  &
              xm,x2m,ym,y2m,xym,np)
  if (np.eq.0) then
    call gagout ('W-REGRESSION,  No data points')
    call sysexi (fatale)
  endif
  !
  y0 = x%gil%val(2) - x%gil%ref(2)*x%gil%inc(2)
  ymean = ym*x%gil%inc(2) + y0
  y2mean = y2m*x%gil%inc(2)**2 + 2.*x%gil%inc(2)*y0*ym + y0**2
  !
  x0 = x%gil%val(1) - x%gil%ref(1)*x%gil%inc(1)
  xmean = xm*x%gil%inc(1) + x0
  x2mean = x2m*x%gil%inc(1)**2 + 2.*x%gil%inc(1)*x0*xm + x0**2
  !
  xymean = xym*x%gil%inc(1)*x%gil%inc(2) + &
           xm*x%gil%inc(1)*y0 + ym*x%gil%inc(2)*x0 + x0*y0
  !
  r = y2mean - ymean**2
  a = (xymean - xmean*ymean) / r
  b = (xmean*y2mean - ymean*xymean) / r
  write(6,*) ' X = ',a,' * Y + ',b
  r = x2mean - xmean**2
  a = (xymean - xmean*ymean) / r
  b = (ymean*x2mean - xmean*xymean) / r
  write(6,*) ' Y = ',a,' * X + ',b
  call gagout ('I-REGRESSION,  Successfull completion')
  call sysexi (1)
  !
end program regression
!
subroutine reg001(x,nx,ny,the,imin,imax,jmin,jmax,xm,x2m,ym,y2m,xym,np)
  use gildas_def
  !---------------------------------------------------------------------
  ! GDF	Internal routine
  !	Given the cross histogram of two images, find the best linear
  !	combination of images.
  !	X = A*Y + B
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)  :: nx        !
  integer(kind=index_length), intent(in)  :: ny        !
  real(kind=4),               intent(in)  :: x(nx,ny)  ! Cross Histogram
  real(kind=4),               intent(in)  :: the       ! Threshold in number of points
  integer(kind=index_length), intent(in)  :: imin      ! X range
  integer(kind=index_length), intent(in)  :: imax      !
  integer(kind=index_length), intent(in)  :: jmin      ! Y range
  integer(kind=index_length), intent(in)  :: jmax      !
  real(kind=4),               intent(out) :: xm        ! Mean of X values (in pixels)
  real(kind=4),               intent(out) :: x2m       ! Mean of X**2 values (in pixels)
  real(kind=4),               intent(out) :: ym        ! Mean of Y values
  real(kind=4),               intent(out) :: y2m       ! Mean of Y**2 values
  real(kind=4),               intent(out) :: xym       ! Mean of X*Y
  integer(kind=size_length),  intent(out) :: np        ! Number of pixels in correlation
  ! Local
  integer(kind=index_length) :: i,j
  !
  np = 0
  xm = 0.
  ym = 0.
  xym = 0.
  y2m = 0.
  x2m = 0.
  do j=jmin,jmax
    do i=imin,imax
      if (x(i,j).gt.the) then
        xm = xm + x(i,j)*i
        ym = ym + x(i,j)*j
        y2m = y2m + x(i,j)*float(j)*float(j)
        xym = xym + x(i,j)*float(i)*float(j)
        x2m = x2m + x(i,j)*float(i)*float(i)
        np = np+nint(x(i,j))
      endif
    enddo
  enddo
  if (np.ne.0) then
    xm = xm / np
    ym = ym / np
    x2m = x2m / np
    y2m = y2m / np
    xym = xym / np
  endif
end subroutine reg001
