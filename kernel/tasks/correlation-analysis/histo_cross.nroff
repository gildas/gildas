.ec _
.ll 76
.ad b
.in 4
.ti -4
1 HISTO__CROSS Cross histogram of two input images/cubes (Image output)
.ti +4
HISTO__CROSS

Computes the cross histogram of two input images/cubes. The
output has the same rank as the input arrays : 2 if images
as input, 3 if cubes. Both inputs must have the same
rank. If the input cubes are 3D data cubes, the third axis
must be the same for both. If n1 and n2 are the numbers of
bins for the first and the second input cube respectively,
the result has dimensions [n1,n2] or [n1,n2,nchan] where
nchan is the third dimension of the input cubes. The value
at a given (I,J) is thus the number of pixels in the input
images that have the value corresponding to slot I in the
first image and to slot J in the second one.  For cubes,
this is done in each plane. You can then sum (see
SIC\COMPUTE) to have the cross histrogram for the whole
cube.

If the tolerance on the blanking value is positive, the task
takes into account the blanking values of the input cubes.

The output image can be used as input to task REGRESSION to
evaluate some statistical parameters of the correlation. See
also HISTO__CLOUD for a slightly different information.

.ti -4
2 Z__NAME$
.ti +4
TASK\FILE "First input image" Z__NAME$

This is the name of the first image used in the correlation.
.ti -4
2 Z__BIN$
.ti +4
TASK\INTEGER "Number of histogram bins" Z__BINS$

This is the number of histogram slots to be used for the first image.
.ti -4
2 Z__MIN$
.ti +4
TASK\REAL "Minimum value" Z__MIN$

This is the value of the lower bin used for the first image.
.ti -4
2 Z__MAX$
.ti +4
TASK\REAL "Maximum value" Z__MAX$

This is the value of the higher bin used for the first
image.  If both Z__MIN and Z__MAX are zero, the extrema are
computed from the input cubes.
.ti -4
2 Y__NAME$
.ti +4
TASK\FILE "Second input image" Y__NAME$

This is the name of the second image used in the correlation.
.ti -4
2 Y__BIN$
.ti +4
TASK\INTEGER "Number of histogram bins" Y__BINS$

This is the number of histogram slots to be used for the second image.
.ti -4
2 Y__MIN$
.ti +4
TASK\REAL "Minimum value" Y__MIN$

This is the value of the lower bin used for the second image.
.ti -4
2 Y__MAX$
.ti +4
TASK\REAL "Maximum value" Y__MAX$

This is the value of the higher bin used for the second
image. Same as Z__ if both thresholds are zero.

.ti -4
2 X__NAME$
.ti +4
TASK\FILE "Output cross correlation image" X__NAME$

This is the name of the  output  cross  correlation image.
.ti -4
1 ENDOFHELP
