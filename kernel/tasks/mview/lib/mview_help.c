
#include "mview_help.h"

#include "mview_view.h"
#include "mview_misc.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <Xm/Form.h>
#include <Xm/PushB.h>
#include <Xm/Text.h>

static Widget helpshell = NULL;

void popdown_help( Widget w, XtPointer tag, XmAnyCallbackStruct * cbs)
{
    Widget temp;

    temp = w;
    while (temp && !XtIsSubclass( temp, shellWidgetClass))
        temp = XtParent( temp);

    XtDestroyWidget( temp);
    helpshell = NULL;

}

void popup_help_file( )
{
    Arg args[20];
    int n;
    char *text;
    struct stat statb;
    FILE *hfile;
    Widget form, button, helptext;

    if (!helpshell) {
        helpshell = XtVaAppCreateShell( "Help", "help_shell", topLevelShellWidgetClass, display, NULL);
        XtAddCallback( helpshell, XtNdestroyCallback, (XtCallbackProc) popdown_help, NULL);
        form = XtVaCreateManagedWidget( "form", xmFormWidgetClass, helpshell, NULL);
        button =
         XtVaCreateManagedWidget( "Dismiss",
                                 xmPushButtonWidgetClass,
                                 form,
                                 XmNleftAttachment, XmATTACH_FORM,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 XmNrightAttachment, XmATTACH_FORM, XmNshowAsDefault, 1, XmNdefaultButtonShadowThickness, 1, NULL);
        XtAddCallback( button, XmNactivateCallback, (XtCallbackProc) popdown_help, NULL);
        n = 0;
        XtSetArg( args[n], XmNrows, 12);
        n++;
        XtSetArg( args[n], XmNcolumns, 80);
        n++;
        XtSetArg( args[n], XmNeditable, False);
        n++;
#ifdef LESSTIF_VERSION
#else
        XtSetArg( args[n], XmNeditMode, XmMULTI_LINE_EDIT);
        n++;
#endif
        XtSetArg( args[n], XmNtopAttachment, XmATTACH_FORM);
        n++;
        XtSetArg( args[n], XmNbottomAttachment, XmATTACH_WIDGET);
        n++;
        XtSetArg( args[n], XmNbottomWidget, button);
        n++;
        XtSetArg( args[n], XmNshadowThickness, 1);
        n++;
        XtSetArg( args[n], XmNleftAttachment, XmATTACH_FORM);
        n++;
        XtSetArg( args[n], XmNrightAttachment, XmATTACH_FORM);
        n++;
        XtSetArg( args[n], XmNcursorPositionVisible, False);
        n++;
        XtSetArg( args[n], XmNtraversalOn, False);
        n++;
        XtSetArg( args[n], XmNscrollBarDisplayPolicy, XmAS_NEEDED);
        n++;

        helptext = (Widget) XmCreateScrolledText( form, "text_w", args, n);
        XtManageChild( helptext);
        XtRealizeWidget( helpshell);

        if (stat( helplocation, &statb) == -1) {
            text = (char *)malloc( 80 * sizeof( char));
            sprintf( text, "%s", "Help Not found ...");
        } else {
            hfile = fopen( helplocation, "r");
            text = (char *)XtMalloc( (unsigned)(statb.st_size + 1));
            fread( text, sizeof( char), statb.st_size + 1, hfile);
            text[statb.st_size] = 0;
            fclose( hfile);
        }
        XmTextSetString( helptext, text);
        XtFree( text);
        XMapRaised( display, XtWindow( helpshell));
    }
}

