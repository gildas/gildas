/*
** Declaration des callbacks
**
*/
extern void create();
extern void expose();
extern void menu();
extern void dismiss();
extern void dismiss_slice();
extern void dismiss_zoom();
extern void change_clips();
extern void dismiss_clips();
extern void re_enter();
extern void extract_image();
extern void scrolled();
extern void quit();
extern void popup_help_file();
extern void make_moments();
extern void average();
extern void smooth();
extern void RedrawWithNewColors();
static char zzz[] =
"<Key>Delete: popup_help_file()\n\
 <Key>Q: quit()\n";
