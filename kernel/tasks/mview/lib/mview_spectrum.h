
#include <Xm/Xm.h>

typedef struct XPoint_item_list* XPoint_list;

struct XPoint_item_list
{
  XPoint xp;
  XPoint_list next;
};

void draw_area();
void get_2D_area(int box[6]);
void redraw_2D_box(Widget w);
void reset_2D_box() /* (For the display only) */;
void redraw_in_integmean();
void Update_IntegMean();
void calc_integspec(float *intspec);
void create_spectrum( Widget w, int w_code, XmAnyCallbackStruct *cbs);
void draw_spec(Widget w, XPoint *ps, float clipmin, float clipmax);
void change_tic(int z);
void expose_spectrum( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void change_pixmap(int z);
void change_Z();
void kill_movie();
void select_next_subset(int dum);
void movie();
void increase_speed( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void decrease_speed( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void dismiss_spectrum( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void movie_CB( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void all_integration( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void calc_integmean(int z1, int z2);
void configure(Widget w, XtPointer data, XEvent *event, char *odd);
void spectrum();
void spec_modify(int x, int y);
void new_spec(Widget w, int x, int y);
void spectrum_input( Widget w, XEvent *event);
void new_tics(int x, int y);
void set_scale(Widget w, XEvent* event, String *params, Cardinal num_params);
void force_tics();
void set_tics(Widget w, XEvent* event, String *params, Cardinal num_params);
int GET_CURZ();
XPoint_list get_pol_coord();
int get_nsum();
void get_relative_sub(int sub[2]);
void get_planes(int sub[3], float velo[2]);

