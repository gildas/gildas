
#define SUBAREA 1
#define SLICE 2
#define SPECTRUM 3

extern void (*phys_coord)(); 

extern int npix[]; /* Number of pixels along each axis of real image */
extern int Nsubs;   /* Number of subsets */
extern int isize;   /* size of one subset */
extern unsigned int sizex, sizey; 
/* Number of pixels along each axis of displayed images in the mosaic */
extern int zsizex, zsizey;
extern int cols, rows;  /* Number of image in X and Y */
extern int zoom; /* zoom factor */
extern int factor;
extern int slice_zoomx, slice_zoomy, localzoom;
extern int *row, *col;  /* row,col coordinates of the subsets in the mosaic */
extern int voffset;
extern int incr;
extern int slicewidth, Np;
extern float *fimage, *sfimage, *Buffer, Blank, Tolerance;
extern float clip[], displayclip[];
extern char *image_name, *helplocation;
extern int show_zposx, show_zposy;
extern int maxspectrumwidth, maxspectrumheight, specwidth, specheight;
extern int standalone;

