
#include <Xm/Xm.h>

void redraws_slice();
void redraws_slice_in_subset();
void redraws_slice_in_integmean();
void dismiss_slice( Widget w, int *code, XmDrawingAreaCallbackStruct *cbs);
void slice();
void draw_integspec_along();
void new_profile();
void dynamic_slice(double x0, double y0, double x1, double y1);
void dynamic_slice_with_blanks(double x0, double y0, double x1, double y1);
void dynamic_slice(double x0, double y0, double x1, double y1);
void draw_angle();
void slice_input( Widget w, XEvent *event);
void spectrum_slice_input( Widget w, XEvent *event);
void reset_slice() /* (For the display only) */;
void coupe_input( Widget w, XEvent *event);
void slice_after_smooth();
void get_slice_extremities(int extremities[4]);

