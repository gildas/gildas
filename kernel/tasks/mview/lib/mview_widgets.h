#include <Xm/Xm.h>
#define k_cliparea      1
#define k_radec         2
#define k_name          3
#define k_pixelvalue    4
#define k_gridcoord     5
#define k_axis1         6
#define k_axis2         7
#define k_axis3         8
#define k_color         9
#define k_spectrum      10
#define k_slice         11
#define k_cursor        12
#define k_zoom          13
#define k_coupe         14
#define k_text          15
#define k_integmean     16
#define k_integspectrum 17
#define k_subset        18
#define k_bwidth        19
#define k_zvalues       20
#define k_bstart        21
#define k_bcontrol      22
#define k_save          23
#define k_clip          24
#define k_highscale     25
#define k_highcut       26
#define k_lowscale      27
#define k_lowcut        28
#define k_ok            29
#define k_coupe_integ   30
#define k_slicelab      31
#define k_scrolled_w    32
#define k_vsb           33
#define k_extract       34
#define k_map_aver      35
#define k_map_sum       36
#define k_spectrum_w    37
#define k_small_w       38
#define k_spectrum_sum  39
#define k_moments       40
#define k_spixelvalue   41
#define k_sgridcoord    42
#define k_saxis1        43
#define k_saxis2        44
#define k_saxis3        45
#define k_sliceticks    46
#define k_average_2     47
#define k_average_3     48
#define k_none          49
#define k_hanning       50
#define k_gauss         51
#define k_box           52
#define k_drawaxes      53
#define k_ldrawaxes     54
#define k_rdrawaxes     55
#define k_newval        56

XmAnyCallbackStruct dumcbs;

Widget average_2;
Widget average_3;
Widget axeinteg;
Widget axis1;
Widget axis2;
Widget axis3;
Widget box;
Widget boxbutton;
Widget cliparea;
Widget clipbutton;
Widget colorb;
Widget colormenu;
Widget comput;
Widget coupe;
Widget coupe_integ;
Widget cursorb;
Widget cursorbutton;
Widget cursormenu;
Widget drawaxes;
Widget exitbutton;
Widget extract_task;
Widget fileb;
Widget filemenu;
Widget form1;
Widget form2;
Widget gauss;
Widget gridcoord;
Widget hanning;
Widget helpbutton;
Widget highcut;
Widget highscale;
Widget hsvbutton;
Widget integmean;
Widget integspectre;
Widget ldrawaxes;
Widget lowcut;
Widget lowscale;
Widget map_aver_task;
Widget map_sum_task;
Widget momentsbutton;
Widget moments_task;
Widget none;
Widget pixelvalue;
Widget radec;
Widget rdrawaxes;
Widget saxis1;
Widget saxis2;
Widget saxis3;
Widget scrolled_w;
Widget sgridcoord;
Widget slicebutton;
Widget slice_dismiss;
Widget slicelab;
Widget slice_task;
Widget sliceticks;
Widget slice_w;
Widget small_w;
Widget smoothb;
Widget smoothmenu;
Widget spectrumbutton;
Widget spectrum_sum_task;
Widget spectrum_task;
Widget spixelvalue;
Widget subset;
Widget tasksb;
Widget tasksmenu;
Widget toplevel;
Widget vsb;
Widget toolsmenu;
Widget toolsb;
Widget zoombutton;
