  filemenu =  XmCreatePulldownMenu(menubar,"filemenu", NULL, 0);
  n = 0;
  XtSetArg(args[n], XmNsubMenuId, filemenu); n++;
  fileb = XmCreateCascadeButton (menubar,"File", args, n);
  XtManageChild(filemenu); 
  XtManageChild(fileb);      

  exitbutton = XtVaCreateManagedWidget("Quit",
				       xmPushButtonWidgetClass,
				       filemenu,
				       NULL);
  XtAddCallback(exitbutton, XmNactivateCallback, (XtCallbackProc) quit, NULL);

  colormenu =  XmCreatePulldownMenu(menubar,"colormenu", NULL, 0);
  n = 0;
  XtSetArg(args[n], XmNsubMenuId, colormenu); n++;

  colorb = XmCreateCascadeButton (menubar,"Color", args, n);
  XtManageChild(colormenu); 
  XtManageChild(colorb);      

  hsvbutton = XtVaCreateManagedWidget("HSV",
				       xmPushButtonWidgetClass,
				       colormenu,
				       NULL);
  XtAddCallback(hsvbutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_color]);
  clipbutton = XtVaCreateManagedWidget("clips",
				       xmPushButtonWidgetClass,
				       colormenu,
				       NULL);
  create(clipbutton, &id[k_clip],dumcbs);
  XtAddCallback(clipbutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_clip]);

  smoothmenu =  XmCreatePulldownMenu(menubar,"smooth", NULL, 0);
  n = 0;
  XtSetArg(args[n], XmNsubMenuId, smoothmenu); n++;

  smoothb = XmCreateCascadeButton (menubar,"Smooth", args, n);
  XtManageChild(smoothmenu); 
  XtManageChild(smoothb);      
 
  average_2 = XtVaCreateManagedWidget("v_ave box2",
				      xmPushButtonWidgetClass,
				      smoothmenu,
				      NULL);
  XtAddCallback(average_2, XmNactivateCallback, (XtCallbackProc) average, &id[k_average_2]);
  average_3 = XtVaCreateManagedWidget("v_ave box3",
				      xmPushButtonWidgetClass,
				      smoothmenu,
				      NULL);
  XtAddCallback(average_3, XmNactivateCallback, (XtCallbackProc) average, &id[k_average_3]);
  none = XtVaCreateManagedWidget("none",
				 xmPushButtonWidgetClass,
				 smoothmenu,
				 NULL);
  XtAddCallback(none, XmNactivateCallback, (XtCallbackProc) average, &id[k_none]);
  hanning = XtVaCreateManagedWidget("lm_ave han",
				 xmPushButtonWidgetClass,
				 smoothmenu,
				 NULL);
  XtAddCallback(hanning, XmNactivateCallback, (XtCallbackProc) smooth, &id[k_hanning]);
  gauss = XtVaCreateManagedWidget("lm_ave gau",
				 xmPushButtonWidgetClass,
				 smoothmenu,
				 NULL);
  XtAddCallback(gauss, XmNactivateCallback, (XtCallbackProc) smooth, &id[k_gauss]);
  box = XtVaCreateManagedWidget("lm_ave box",
				 xmPushButtonWidgetClass,
				 smoothmenu,
				 NULL);
  XtAddCallback(box, XmNactivateCallback, (XtCallbackProc) smooth, &id[k_box]);


  cursormenu =  XmCreatePulldownMenu(menubar,"cursormenu", NULL, 0);
  n = 0;
  XtSetArg(args[n], XmNsubMenuId, cursormenu); n++;

  cursorb = XmCreateCascadeButton (menubar,"Mode", args, n);
  XtManageChild(cursormenu); 
  XtManageChild(cursorb);      

  spectrumbutton = XtVaCreateManagedWidget("Normal",
				      xmPushButtonWidgetClass,
				      cursormenu,
				      NULL);
  create(spectrumbutton, &id[k_spectrum], dumcbs);
  XtAddCallback(spectrumbutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_spectrum]);

  slicebutton = XtVaCreateManagedWidget("Slice",
				      xmPushButtonWidgetClass,
				      cursormenu,
				      NULL);
  create(slicebutton, &id[k_slice], dumcbs);
  XtAddCallback(slicebutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_slice]);
  boxbutton = XtVaCreateManagedWidget("3D box",
				      xmPushButtonWidgetClass,
				      cursormenu,
				      NULL);
  create(boxbutton, &id[k_cursor], dumcbs);
  XtAddCallback(boxbutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_cursor]);

  momentsbutton = XtVaCreateManagedWidget("Moments",
				      xmPushButtonWidgetClass,
				      cursormenu,
				      NULL);
  create(momentsbutton, &id[k_moments], dumcbs);
  XtAddCallback(momentsbutton, XmNactivateCallback, (XtCallbackProc) make_moments, NULL);

if (!standalone) {
  tasksmenu =  XmCreatePulldownMenu(menubar,"tasksmenu", NULL, 0);
  n = 0;
  XtSetArg(args[n], XmNsubMenuId, tasksmenu); n++;

  tasksb = XmCreateCascadeButton (menubar,"Tasks", args, n);
  XtManageChild(tasksmenu); 
  XtManageChild(tasksb);      

  extract_task = XtVaCreateManagedWidget("EXTRACT",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);

  XtAddCallback(extract_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_extract]);
  slice_task = XtVaCreateManagedWidget("SLICE",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);

  XtAddCallback(slice_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_slice]);
  spectrum_task = XtVaCreateManagedWidget("SPECTRUM",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);
  create(spectrum_task, &id[k_spectrum], dumcbs);
  XtAddCallback(spectrum_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_spectrum]);

  map_aver_task = XtVaCreateManagedWidget("MAP_AVER",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);
  create(map_aver_task, &id[k_map_aver], dumcbs);
  XtAddCallback(map_aver_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_map_aver]);

  map_sum_task = XtVaCreateManagedWidget("MAP_SUM",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);
  create(map_sum_task, &id[k_map_sum], dumcbs);
  XtAddCallback(map_sum_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_map_sum]);

  spectrum_sum_task = XtVaCreateManagedWidget("SPECTRUM_SUM",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);
  create(spectrum_sum_task, &id[k_spectrum_sum], dumcbs);
  XtAddCallback(spectrum_sum_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_spectrum_sum]);

  moments_task = XtVaCreateManagedWidget("MOMENTS",
				      xmPushButtonWidgetClass,
				      tasksmenu,
				      NULL);
  create(moments_task, &id[k_moments], dumcbs);
  XtAddCallback(moments_task, XmNactivateCallback, (XtCallbackProc) extract_image, &id[k_moments]);

}
/*   toolsmenu =  XmCreatePulldownMenu(menubar,"toolsmenu", NULL, 0); */
/*   n = 0; */
/*   XtSetArg(args[n], XmNsubMenuId, toolsmenu); n++; */

/*   toolsb = XmCreateCascadeButton (menubar,"Tools", args, n); */
/*   XtManageChild(toolsmenu);  */
/*   XtManageChild(toolsb);       */
/*   zoombutton = XtVaCreateManagedWidget("Zoom", */
/* 				      xmPushButtonWidgetClass, */
/* 				      toolsmenu, */
/* 				      NULL); */
/*   create(zoombutton, &id[k_zoom], dumcbs); */
/*   XtAddCallback(zoombutton, XmNactivateCallback, (XtCallbackProc) menu, &id[k_zoom]); */


  helpbutton = XtVaCreateManagedWidget("Help",
				      xmPushButtonWidgetClass,
				      menubar,
				      NULL);
  XtAddCallback(helpbutton, XmNactivateCallback, (XtCallbackProc) popup_help_file, NULL);


