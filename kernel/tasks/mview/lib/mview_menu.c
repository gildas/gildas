
#include "mview_menu.h"

#include "mview_display.h"
#include "mview_view.h"
#include "mview_clips.h"
#include "mview_widgets.h"
#include "mview_misc.h"
#include "mview_cursor.h"
#include "mview_slice.h"
#include "mview_spectrum.h"
#include "gmotif/hsvcontrol.h"

#include <stdio.h>
#include <stdlib.h>

void save( )
{
    FILE *f_save;
    int box[6];
    int curpix[3];
    int extremities[4];
    XPoint_list xpl;
    int sub[3];
    int x, y, z = 1, compt = 0;
    char chainx[80], chainy[80], chainz[80], chainz2[80], chainv[80];
    float velo[2];

    f_save = fopen( "mview.log", "w");
    printf( chainz2);
    fprintf( f_save, "%s\n", "######################");
    fprintf( f_save, "%s\n", "# Log file for mview #");
    fprintf( f_save, "%s\n", "######################");
    fprintf( f_save, "%s%s%s  %s\n", "'", image_name, "'", "/* Filename */");


    get_3D_area( box);

    phys_coord( &box[0], &box[1], &box[2],
               chainx, chainy, chainz, chainv, strlen( chainx), strlen( chainy), strlen( chainz), strlen( chainv));

    fprintf( f_save, "%d %d %d %s\n", box[0], box[1], box[2], "                    /* Blc */");

    phys_coord( &box[3], &box[4], &box[5],
               chainx, chainy, chainz2, chainv, strlen( chainx), strlen( chainy), strlen( chainz2), strlen( chainv));

    fprintf( f_save, "%d %d %d %s\n", box[3], box[4], box[5], "                    /* Trc */");

    get_curpix( curpix);

    if (curpix[0] != 0)
        fprintf( f_save, "%d %d %d %s\n", curpix[0], curpix[1], curpix[2], "                    /* Current pixel */");

    get_slice_extremities( extremities);
    if (extremities[0] != 0) {
        phys_coord( &extremities[0], &extremities[1], &z,
                   chainx, chainy, chainz, chainv, strlen( chainx), strlen( chainy), strlen( chainz), strlen( chainv));

        fprintf( f_save, "%s %s %s\n", chainx, chainy, " /* Slice begin */");

        phys_coord( &extremities[2], &extremities[3], &z,
                   chainx, chainy, chainz, chainv, strlen( chainx), strlen( chainy), strlen( chainz), strlen( chainv));

        fprintf( f_save, "%s %s %s\n", chainx, chainy, " /* Slice end */");
    }

    get_planes( sub, velo);

    x = 1;
    y = 1;

    phys_coord( &x, &y, &sub[1], chainx, chainy, chainz, chainv, strlen( chainx), strlen( chainy), strlen( chainz), strlen( chainv));

    x = 1;
    y = 1;

    phys_coord( &x, &y, &sub[2], chainx, chainy, chainz2, chainv, strlen( chainx), strlen( chainy), strlen( chainz2), strlen( chainv));

    fprintf( f_save, "%s %s %s\n", chainz, chainz2, "   /* Planes first/end */");

    compt = get_nsum( );
    if (compt != 0) {
        fprintf( f_save, "%d %s\n", compt, "Polygon summits");
        xpl = get_pol_coord( );

        while (xpl != NULL) {
            x = (xpl->xp.x) / localzoom + 1;
            y = (npix[1] - 1 - (xpl->xp.y) / localzoom) + 1;

            phys_coord( &x, &y, &z, chainx, chainy, chainz, chainv, strlen( chainx), strlen( chainy), strlen( chainz), strlen( chainv));
            if (xpl->next != NULL)
                fprintf( f_save, "%s %s\n", chainx, chainy);

            xpl = xpl->next;
        }
    }

    fclose( f_save);
}

void menu( Widget w, XtPointer w_code, XmPushButtonCallbackStruct * cbs)
{
    switch ((long)w_code) {
    case k_color:
        kill_movie( );
        hsv_show( app, on_transfert_function, on_dismiss);
        break;
    case k_clip:
        kill_movie( );
        clips( );
        break;
    case k_slice:
        kill_movie( );
        reset_box( );
        if (Nsubs >= 2)
            reset_2D_box( );
        slice( );
        break;
    case k_cursor:
        kill_movie( );
        reset_slice( );
        cursor( );
        break;
    case k_spectrum:
        reset_box( );
        if (Nsubs >= 2)
            reset_2D_box( );
        reset_slice( );
        spectrum( );
        break;
    }
}

void quit( )
{
    kill_movie( );
    exit( 0);
}

