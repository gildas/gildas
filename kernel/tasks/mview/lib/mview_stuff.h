
void _2DfloatTo2Dbyte(
    float *startf,    /* Addresse de depart des donnees 2Dfloat a extraire */
    int xmin,         /* Borne inferieure sur la 1ere dimension */
    int xmax,         /* Borne superieure sur la 1ere dimension */
    int ymin,         /* Borne inferieure sur la 2eme dimension */
    int ymax,         /* Borne superieure sur la 2eme dimension */
    char *startb,     /* Addresse de depart de la zone memoire */
                      /* contenant les valeurs de pixel */
    int scalex,       /* Facteur d'echelle selon la 1ere dimension */
    int scaley,       /* Facteur d'echelle selon la 2eme dimension */
    float clipdeb,
    float clipfin,
    int incr);
void draw_wedge(float clip[2]);

