
#define F_ADDR(x, y, z) (sfimage + isize*z + npix[0]*y + x)

#define GETCOL(x) (x/sizex)
#define GETROW(y) ((y + voffset)/sizey)

#define GETX(x) (x - GETCOL(x)*sizex)
#define GETY(y) (y + voffset - GETROW(y)*sizey)
#define GETZ(x, y) (GETCOL(x) + GETROW(y)*cols)

#define POSX(x, z) (col[z]*sizex + x)
#define POSY(y, z) (row[z]*sizey + y - voffset)

#define BOX(x, y, w, h) \
          XDrawRectangle(display, XtWindow(radec), gcx, x, y, w, h)

#define LINE(w, x, y, z, t) \
          XDrawLine(display, XtWindow(w), gcx, x, y, z, t)

#define HARDLINE(w, x, y, z, t) \
          XDrawLine(display, XtWindow(w), gc, x, y, z, t)

#define INMOSAIC(x, y) ((x >= 0) && (x < pixmapdim[0]) && \
                        (y >= 0) && (y < pixmapdim[1]))


#define INZOOM(x, y) ((x >= 0) && (x < 512) && \
		      (y >= 0) && (y < 512))

#define ERRASETIC(x, y, w, h) \
  XSetForeground(display, gc, \
		 g_vc.black_pixel); \
  XFillRectangle(display, \
		 XtWindow(integspectre), \
		 gc, x, y, \
		 w, h)

#define DRAWTIC(x, y, w, h) \
  XSetForeground(display, gc, \
		 g_vc.white_pixel); \
  XFillRectangle(display, \
		 XtWindow(integspectre), \
		 gc, x, y, \
		 w, h)

#define DRAWCIRC(w, x, y) \
  XDrawArc(display, XtWindow(w), gcx, x-5, y-5, 10, 10, 360*64, 360*64); \
  XDrawArc(display, XtWindow(w), gcx, x-10, y-10, 20, 20, 360*64, 360*64)


#define DRAWCROSS(w, x, y) \
  LINE(w, x - 10, y, x + 10, y); \
  LINE(w, x, y - 10, x, y + 10)

#define DRAWLINES(z) \
  HARDLINE(radec, POSX(0, z), POSY(0, z), POSX(0, z), POSY(sizey - 1, z)); \
  HARDLINE(radec, POSX(0, z), POSY(sizey - 1, z), \
	   POSX(sizex - 1, z), POSY(sizey - 1, z)); \
  HARDLINE(radec, POSX(sizex - 1, z), POSY(sizey - 1, z), \
	   POSX(sizex - 1, z), POSY(0, z)); \
  HARDLINE(radec, POSX(sizex - 1, z), POSY(0, z), POSX(0, z), POSY(0, z))

#define WHITEAREA(z) \
  XSetForeground(display, gc, \
		 g_vc.white_pixel); \
  DRAWLINES(z)

#define REDAREA(z) \
  XSetForeground(display, gc, \
		 redpix); \
  DRAWLINES(z)

#define min(a,b) (((a)<(b))? (a):(b))
#define max(a,b) (((a)>(b))? (a):(b))

