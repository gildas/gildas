
#include "mview_stuff.h"

#include "gx11/visual_context.h"
#include "mview_view.h"
#include "mview_misc.h"

#include <stdio.h>
#include <math.h>

void _2DfloatTo2Dbyte( float *startf,    /* Addresse de depart des donnees 2Dfloat a extraire */
                      int xmin, /* Borne inferieure sur la 1ere dimension */
                      int xmax, /* Borne superieure sur la 1ere dimension */
                      int ymin, /* Borne inferieure sur la 2eme dimension */
                      int ymax, /* Borne superieure sur la 2eme dimension */
                      char *startb,     /* Addresse de depart de la zone memoire
                                           contenant les valeurs de pixel */
                      int scalex,       /* Facteur d'echelle selon la 1ere dimension */
                      int scaley,       /* Facteur d'echelle selon la 2eme dimension */
                      float clipdeb, float clipfin, int incr)
{
    int i, x, y, l;
    int width, height;
    float sc, tested;
    char *p;
    float *startfsav;

    startfsav = startf;

    sc = (clipfin == clipdeb) ? 0 : ((float)(MAXCOL - 1)) / (clipfin - clipdeb);
    width = (xmax - xmin + 1) * scalex;
    height = (ymax - ymin + 1) * scaley;
    startb = startb + (height - 1) * width;

    for (y = ymin; y <= ymax; y++) {
        p = startb;
        for (x = xmin; x <= xmax; x++) {
            tested = *startf;

            if ((tested == Blank) || (fabs( tested - Blank) <= Tolerance))
                l = MAXCOL;
            else {
                l = (int)(sc * (tested - clipdeb));

                if (l < 0) {
                    l = 0;
                } else {
                    if (l > (MAXCOL - 1))
                        l = MAXCOL - 1;
                }
            }
            memset( startb, g_vc.colmap[l], scalex);
            startb += scalex;
            startf += incr;
        }

        startf -= (xmax - xmin + 1) * incr;

        startb -= 2 * width;
        for (i = 1; i < scaley; i++) {
            (void)memcpy( startb, p, width);
            startb -= width;
        }

        if (startfsav == Buffer)
            startf += xmax * incr;
        else
            startf += npix[0] * incr;
    }
}

static int gesterreurs( Display * canal_aff, XErrorEvent * errev)
{
    static char dumstr[80];

    XGetErrorText( canal_aff, errev->error_code, dumstr, 80);
    fprintf( stderr, "%s\n", dumstr);
    return 0;
}

void draw_wedge( float clip[2])
{
    int k;
    char c[16];
    float f;
    extern Widget cliparea;

    XSetErrorHandler( gesterreurs);

    XSetForeground( display, gc, g_vc.black_pixel);
    XFillRectangle( display, cpixmap, gc, 0, 0, cpixmapdim[0] - 30, cpixmapdim[1]);

    XSetForeground( display, gc, g_vc.white_pixel);

    for (k = 0; k < 15; k++) {
        f = clip[0] + (float)k *(clip[1] - clip[0]) / 14.;

        sprintf( c, "%10.3g -", f);
        XDrawString( display, cpixmap, gc, 0, cpixmapdim[1] - 10 - k * (cpixmapdim[1] / 15), c, 12);
    }
    XCopyArea( display, cpixmap, XtWindow( cliparea), gc, 0, 0, cpixmapdim[0], cpixmapdim[1], 0, 0);
}

