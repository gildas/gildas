
#include <Xm/Xm.h>

void get_crosspos( int pos[2]);
void redraws_box( );
void cursor( );
void reset_box( );
void clear_infos( );
void param( int xcoord, int ycoord, int zcoord, int flag);
void get_curpix( int curpix[3]);
void redraw_cross( );
void redraw_zcross( Widget w);
void draw_cross( Widget w, int x, int y);
void cursor_input( Widget w, XEvent *event);
void get_3D_area( int box[6]);
void extract_image( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void trap_SIGALRM(int lesignal);

