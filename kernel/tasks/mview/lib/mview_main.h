
#include <Xm/Xm.h>

void destroy_zoomshell( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void resize_zoom_w( Widget w, XEvent * event, String * params, Cardinal num_params);
void re_enter( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void get_com_params( int *keyboard, int *idsemap, int *idmemory);
void long_timer( );
void mview( char *field, float *user_image, int *size, void (*user_coord) (), char *helpdir, int *params, float *bl);
void get_image_name( char name[256]);
void PostPopup( Widget w, Widget popup_pane, XButtonEvent * event);
void MenuCB( Widget w, caddr_t client_data, caddr_t call_data);
int make_huge( );
void average_n( float *startf, float *sstartf, int n, int xmin, int xmax, int ymin, int ymax);
void average( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void smooth_w( float *startf, float *sstartf, float *weight, int xmin, int xmax, int ymin, int ymax);
void smooth_with_blanks( float *startf, float *weight, int n);
void smooth( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void UpdateColorChanged( );

