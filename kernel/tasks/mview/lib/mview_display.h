
#include <Xm/Xm.h>

extern unsigned long blackpix, whitepix, graypix, redpix;
extern XtAppContext app;

void dismiss_zoom(Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void redraw(Window window);
void scrolled(Widget w, XtPointer data, XmScrollBarCallbackStruct *cbs);
void draw_sticks(Widget w);
void new_sticks(Widget w, int x, int y);
void set_sticks(Widget w, XEvent* event, String *params, Cardinal num_params);
void draw_axes();
void create( Widget w, int w_code, XmAnyCallbackStruct *cbs);
void on_dismiss( );
void on_transfert_function( );
void expose( Widget w, XtPointer area, XmDrawingAreaCallbackStruct * cbs);
void create_standard_menu( Widget menubar);
void set_colormap( Widget w);
void inidisplay();
void realize_top();
void dismiss( Widget w, int *code, XmDrawingAreaCallbackStruct *cbs);
void change_seuil( Widget w, int *tag, XmScaleCallbackStruct *cbs);
void get_seuil(float *s);
void compute_moments( Widget w, XtPointer w_code, XmAnyCallbackStruct *cbs);
void destroy_momshell( Widget w, Pixmap *area, XmAnyCallbackStruct *cbs);
void make_moments( Widget w, int *code, XmAnyCallbackStruct *cbs);
void view_mean();
void view_velo();
void view_width();
void expose_moment(Widget w, Pixmap *area, XmDrawingAreaCallbackStruct *cbs);
void destroy_moment( Widget w, Pixmap *area, XmAnyCallbackStruct *cbs);
void mom_input( Widget w, XEvent *event);
void create_momwidgets();
void AffImage(float *fimage, Drawable drawable, int sx, int sy, int xmin, int xmax, int ymin, int ymax, int scalex, int scaley, float clipdeb, float clipfin, int incr, int src_x, int src_y, int dest_x, int dest_y, int width, int height);

