/*
** Declaration des actions
**
*/

extern void cursor_input();
extern void slice_input();
extern void spectrum_slice_input();
extern void coupe_input();
extern void set_sticks();
extern void spectrum_input();
extern void mom_input();
extern void quit();

#ifdef REGISTERACTIONS
XtActionsRec actions[] = {
  {"cursor_input", cursor_input},
  {"slice_input", slice_input},
  {"spectrum_slice_input", spectrum_slice_input},
  {"coupe_input", coupe_input},
  {"set_sticks", set_sticks},
  {"spectrum_input", spectrum_input},
  {"quit", quit},
  {"mom_input", mom_input},
};
#endif

