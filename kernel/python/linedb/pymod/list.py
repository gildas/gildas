# list.py -- List the lines form the line index

import math

def llist(lines,full=False,extra=[]):
  """llist(lines,full,extra)
    Display a list of lines

    Arguments:
    ----------
    lines: the list of lines (instance of class "line")
    full:  boolean indicating if the default (False) or all (True)
           columns should be listed
    extra: a list indicating extra columns to be listed, e.g.
           extra = [ ["attr1","Head1","format1"], ["attr2","Head2","format2"]]
           Each element is also a list with the attribute name, the column
           header, and the format to be used for the column content. Note that
           any attribute can be listed this way, in particular third-party
           attributes (unknown by LINEDB) which may have been attached.
  """
  if full:
    llist_full(lines,extra)
  else:
    llist_default(lines,extra)


def llist_default(lines,extra=[]):

   # Find the best width for column Index
   windex = max(3,int(math.ceil(math.log10(len(lines)))))

   # Find the best column width for Species
   wspec = 12  # Minimum width
   for l in lines:
     wspec = max(wspec,len(l.species))

   headformat = "%"+repr(windex)+"s %-"+repr(wspec)+"s   Freq[MHz] Err[MHz] "+ \
                "Eup[K]  Gup  Aij[s-1]    Upper level -- Lower level    Origin"
   headformat = headformat % ("#","Species")

   index = 0
   lineformat = "%"+repr(windex)+"i %-"+repr(wspec)+"s %11.3f %7.3f "+ \
                "%7.1f %4.0f %9.2e %14s -- %-14s %6s"

   # Support for extra columns
   for c in extra:
     headformat += " "+c[1]
     lineformat += " "+c[2]

   # Actual job
   print(headformat)
   for l in lines:

      # Truncate quantum numbers if needed
      if len(l.upper_level.quantum_numbers) >= 14:
         l.upper_level.quantum_numbers = l.upper_level.quantum_numbers[:11] + "..."
      if len(l.lower_level.quantum_numbers) >= 14:
         l.lower_level.quantum_numbers = l.lower_level.quantum_numbers[:11] + "..."

      mycolumns = [index + 1, l.species, l.frequency, l.err_frequency,
        l.upper_level.energy, l.upper_level.statistical_weight, l.einstein_coefficient,
        l.upper_level.quantum_numbers, l.lower_level.quantum_numbers, l.origin]
      for c in extra:
        mycolumns.append(getattr(l,c[0]))

      print(lineformat % tuple(mycolumns))
      index = index + 1


def llist_full(lines,extra=[]):

   # Best width for column Index
   windex = max(3,int(math.ceil(math.log10(len(lines)))))

   # Find the best column width for Species
   wspec = 12  # Minimum width
   for l in lines:
     wspec = max(wspec,len(l.species))

   headformat = "%"+repr(windex)+"s %-"+repr(wspec)+"s   Freq[MHz] Err[MHz] "+ \
                "Aij[s-1]  "+ \
                "Eup[K]  Elow[K]    Gup    Glow    Upper level -- Lower level    "+ \
                "Origin Source        Date"
   headformat = headformat % ("#","Species")

   index = 0
   lineformat = "%"+repr(windex)+"i %-"+repr(wspec)+"s %11.3f %7.3f "+ \
                "%9.2e "+ \
                "%7.1f %7.1f %7.1f %7.1f %14s -- %-14s "+ \
                "%6s %-12s %-32s"

   # Support for extra columns
   for c in extra:
     headformat += " "+c[1]
     lineformat += " "+c[2]

   # Actual job
   print(headformat)
   for l in lines:

      # Truncate quantum numbers if needed
      if len(l.upper_level.quantum_numbers) >= 14:
         l.upper_level.quantum_numbers = l.upper_level.quantum_numbers[:11] + "..."
      if len(l.lower_level.quantum_numbers) >= 14:
         l.lower_level.quantum_numbers = l.lower_level.quantum_numbers[:11] + "..."

      mycolumns = [index + 1, l.species, l.frequency, l.err_frequency,
        l.einstein_coefficient,
        l.upper_level.energy, l.lower_level.energy,
        l.upper_level.statistical_weight, l.lower_level.statistical_weight,
        l.upper_level.quantum_numbers, l.lower_level.quantum_numbers,
        l.origin, l.dbsource, l.date]
      for c in extra:
        mycolumns.append(getattr(l,c[0]))

      print(lineformat % tuple(mycolumns))
      index = index + 1
