# astro.py -- Classes and methods to "save" a line database under Astro
# catalog format

from datetime import datetime

class Astro:

   def __init__(self, catfile):
      self.db_file = catfile

   def create(self, version, fmin=None, fmax=None, overwrite = False):
      if overwrite:
        f = open(self.db_file,'w')  # Write mode
        now = datetime.now()
        f.write('! Created with LineDB '+now.strftime("%Y-%m-%d %H:%M:%S")+'\n')
      else:
        f = open(self.db_file,'a')  # Append mode
      #
      f.close()
      del f

   def add_lines(self, lines, update):
      """
      Add a list of line to the database

      Arguments:
      lines -- line list

      """

      if update:
          raise NotImplementedError("Update mode not implemented for an Astro database")
      else:
          f = open(self.db_file,'a')
          for line in lines:
              row = "{0:14.6f}  {1:<34s}  ! {2:s}\n".format(
                line.frequency*1e-3,"'"+line.species.strip()+"'",line.origin)
              f.write(row)
          f.close()
          del f
