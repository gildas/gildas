# select.py -- Find lines within a given frequency range

from sicparse import OptionParser
import pysic
import linedb
import linedb.consts as consts

f_epsilon = 1e-2
selectUsage = "Command takes no argument. Use options to filter the selection."
freqUsage = "usage: \n\t/FREQ fmin fmax\n\t/FREQ fmin *\n\t/FREQ * fmax\n\t/FREQ freq\n"
sortUsage = "sort order must be one or two from ('frequency', 'energy', 'aij')"

def main():
   """
   Sic interface to command LINEDB\\SELECT
   """

   # Parse arguments and options
   parser = OptionParser()
   parser.add_option("-s", "--species", dest="species", nargs=-1, default=[])  # -1 for variable number of arguments
   parser.add_option("-f", "--frequency", dest="frange", nargs=2)
   parser.add_option("-o", "--origin", dest="origin", nargs=1, default="All")
   parser.add_option("-r", "--sortby", dest="sortby", nargs=3)
   parser.add_option( "-e", "--energy", dest="energy", nargs=1, type="float", default=-1)
   parser.add_option( "-a", "--aij", dest="einstein", nargs=1, type="float", default=-1)
   parser.add_option( "-k", "--epsilon", dest="s_epsilon", nargs=1, type="float", default=-1)
   try:
      (options, args) = parser.parse_args()
   except:
      pysic.message(pysic.seve.e, "SELECT", "Invalid option")
      pysic.sicerror()
      return
   if (len(args)>0):
      pysic.message(pysic.seve.e, "SELECT", selectUsage)
      pysic.sicerror()
      return
   sortby = ['frequency']
   if options.sortby != None:
      if len(options.sortby) not in [1,2]:
         pysic.message(pysic.seve.e, "SELECT", sortUsage)
         pysic.sicerror()
         return
      sortby = []
      try:
         for i in range(len(options.sortby)):
            j = sicparse.ambigs(options.sortby[i], linedb.sortCrit)
            sortby.append(linedb.sortCrit[j])
      except Exception as error:
         pysic.message(pysic.seve.e, "SELECT", "{0}".format(error))
         pysic.sicerror()
         return
   else:
      if options.s_epsilon != -1:
         pysic.message(pysic.seve.e, "SELECT", "epsilon option requires sortby option.")
         pysic.sicerror()
         return
   fmin = -1
   fmax = -1
   if options.frange != None:
      if len(options.frange) > 2:
         pysic.message(pysic.seve.e, "SELECT", freqUsage)
         pysic.sicerror()
         return
      try:
         if len(options.frange) == 1:
            fmin = float(options.frange[0])
            fmax = fmin + f_epsilon
            fmin = fmax - 2 * f_epsilon
         else:
            if options.frange[0] != '*':
               fmin = float(options.frange[0])
            if options.frange[1] != '*':
               fmax = float(options.frange[1])
         if fmin > fmax:
            ftmp = fmin
            fmin = fmax
            fmax = ftmp
            del ftmp
      except:
         pysic.message(pysic.seve.e, "SELECT", freqUsage)
         pysic.sicerror()
         return
   energy = options.energy

   try:
      linedb.select_command("SELECT",fmin,fmax,options.species,options.origin,
        options.energy,options.einstein,sortby,options.s_epsilon)
   except Exception as error:
      pysic.message(pysic.seve.e, "SELECT", "{0}".format(error))
      pysic.sicerror()
      return

main()
