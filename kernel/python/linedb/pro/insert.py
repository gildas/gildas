# insert.py: write previously found lines to the currently selected dbout file

import os
import linedb
from sicparse import OptionParser
import pysic

def main():
   """
   Write lines to the db
   """
   parser = OptionParser()
   # Note that regardless of what options.update is set to, a line will be updated
   # if it's coming from one of the online sources, because we want to keep track
   # of the date change in that case.
   parser.add_option( "-w", "--update", dest="update", action="store_true", default=False)

   try:
      (options, args) = parser.parse_args()
   except:
      pysic.message(pysic.seve.e, "INSERT", "Invalid option")
      pysic.sicerror()
      return

   try:
      if linedb.dbout == None:
         raise Exception("die, stupid snake")
   except:
      pysic.message(pysic.seve.e, "INSERT", "No output database selected")
      pysic.sicerror()
      return

   try:
      # TODO: for the sake of consistency there should be a wrapper around
      # db.py:insertLines in main.py. And it should be the one used here.
      linedb.dbout.insertLines(linedb.dbin, linedb.linesOut, options.update)
   except Exception as error:
      pysic.message(pysic.seve.e, "INSERT", "{0}".format(error))
      pysic.sicerror()
      return

main()
