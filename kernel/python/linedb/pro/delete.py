# delete.py -- remove lines from dbout

import os
import linedb

clight = 299792458.0 # m/s

def main():
   """
   remove lines from the db

   """
   try:
      if linedb.dbout == None:
         raise Exception("die, stupid snake")
   except:
      print("E-REMOVE, No output database selected.")
      return

   try:
      if not linedb.linesOut or len(linedb.linesOut) == 0:
         print("I-REMOVE, No lines selected for deletion.")
         return
      n = len(linedb.linesOut)
      linedb.dbout.removeLines(linedb.linesOut)
      print("I-REMOVE, {0} lines deleted".format(n))
   except Exception as error:
      pysic.message(pysic.seve.e, "REMOVE", "%s" % error)
      pysic.sicerror()
      return

main()
