# list.py -- List the lines form the line index

import sys,math
from sicparse import OptionParser
import numpy
import linedb

def main():
   """
   List the lines from the line index

   """

   # Parse arguments and options
   parser = OptionParser()
   parser.add_option("-f", "--full", dest="full", action="store_true", default=False)
   parser.add_option("-t", "--toc", dest="toc", action="store_true", default=False)
   try:
      (options, args) = parser.parse_args()
   except:
      pysic.message(pysic.seve.e, "LIST", "Invalid option")
      pysic.sicerror()
      return

   # Sanity check:
   if (options.full and options.toc):
      print("E-LIST, Incompatible options /FULL and /TOC")
      return

   # Make sure that we have lines in the index
   try:
      linedb.linesOut
   except NameError:
      print("E-LIST, No lines in the index.")
      return

   if options.toc:
      partition = linedb.equivPartition()
      sortedKeys  = list(partition.keys())
      sortedKeys.sort()
      print(" Species               LinesCount")
      for k in sortedKeys:
         print(" %-21s %i" % (k, len(partition[k])))
      return

   # Print the lines on the screen
   if len(linedb.linesOut) == 0:
      print("I-LIST, Nothing found")
      return

   linedb.llist(linedb.linesOut,full=options.full)

main()
