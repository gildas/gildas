/**
 * @file
 * All-in-one 'pygildas' module include file.
 */

/*****************************************************************************
 *                          Macros & Definitions                             *
 *****************************************************************************/

/* libgsys functions */
#define gmessage_write   CFC_EXPORT_NAME( gmessage_write )

/* libsic functions */
#define enter_program    CFC_EXPORT_NAME( enter_program )
#define exec_program     CFC_EXPORT_NAME( exec_program )
#define exec_command     CFC_EXPORT_NAME( exec_command )
#define sic_defvariable  CFC_EXPORT_NAME( sic_defvariable )
#define sic_crestructure CFC_EXPORT_NAME( sic_crestructure )
#define sic_delvariable  CFC_EXPORT_NAME( sic_delvariable )
#define sic_typecodes    CFC_EXPORT_NAME( sic_typecodes )
#define sic_dictsize     CFC_EXPORT_NAME( sic_dictsize )
#define sic_descptr      CFC_EXPORT_NAME( sic_descptr )
#define sic_getname      CFC_EXPORT_NAME( sic_getname )
#define sic_varlevel     CFC_EXPORT_NAME( sic_varlevel )
#define gmaster_clean    CFC_EXPORT_NAME( gmaster_clean )
#define gmaster_get_id   CFC_EXPORT_NAME( gmaster_get_id )

/* Macro for returning Py_None from a function *
 * Not available before Python 2.4.            */
#ifndef Py_RETURN_NONE
#define Py_RETURN_NONE return Py_INCREF(Py_None), Py_None
#endif

#define SIC_MAXDIMS 7

typedef void *address_t;


/*****************************************************************************
 *                           Function prototypes                             *
 *****************************************************************************/

/* libgsys functions */
void gmessage_write     (int *,int *, char *str1, char *str2
     CFC_DECLARE_STRING_LENGTH(str1) CFC_DECLARE_STRING_LENGTH(str2));

/* libsic functions */
void enter_program      (void);
void exec_program       (char *, int);
void exec_command       (char *, int *, int);
void sic_defvariable    (int *, char *, int *, int *, int);
void sic_crestructure   (char *, int *, int *, int);
void sic_delvariable    (char *, int *, int *, int);
void sic_typecodes      (int *, int *, int *, int *, int *, int *, int *);
int  sic_dictsize       (void);
int  sic_descptr        (char *, int *, address_t **, int);
int  sic_getname        (int *, char *, int *, int*);
int  sic_varlevel       (void);
void gmaster_clean      (int *);
int  gmaster_get_id     (void);
PyObject* gpy_enterprog (void);
PyObject* gpy_exitloop  (void);
void gpy_execfile_error_set (int);

/* pgvar.c functions */
PyObject* gpy_dictsize  (void);
PyObject* gpy_descptr   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_dataptr   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_getname   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_isro      (PyObject*, PyObject*, PyObject*);
PyObject* gpy_varexist  (PyObject*, PyObject*, PyObject*);
PyObject* gpy_vartype   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_vardims   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_defvar    (PyObject*, PyObject*, PyObject*);
PyObject* gpy_defstruct (PyObject*, PyObject*, PyObject*);
PyObject* gpy_delvar    (PyObject*, PyObject*, PyObject*);
PyObject* gpy_varlevel  (void);
PyObject* gpy_isimage   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_isalias   (PyObject*, PyObject*, PyObject*);
PyObject* gpy_maindict  (void);

/* pgcommand.c functions */
PyObject* gpy_sicexec       (PyObject*, PyObject*, PyObject*);
PyObject* gpy_onpythonexit  (void);
PyObject* gpy_get_log_trans (PyObject*, PyObject*);
PyObject* gpy_parse_file    (PyObject*, PyObject*);
PyObject* gpy_sicerror_set  (void);
PyObject* gpy_message       (PyObject*, PyObject*);
