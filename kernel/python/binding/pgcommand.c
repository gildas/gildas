/**
 * @file
 * Provide functions to interact with SIC.
 */

/*****************************************************************************
 *                              Dependencies                                 *
 *****************************************************************************/

#undef _DEBUG // for python52 in release mode
#include <Python.h>
#include <string.h>
#include <stdio.h>
#include "gsys/cfc.h"
#include "gsys/sic_util.h"
#include "gcore/gcomm.h"
#include "pygildas.h"


/*****************************************************************************
 *                        Internal function bodies                           *
 *****************************************************************************/


/*****************************************************************************
 *                             Function bodies                               *
 *****************************************************************************/

/**
 * Send a command line to SIC and execute it.
 * Python input is the command line as a character string.
 * 'exec_command()' is the internal Fortran routine called by SIC to
 * execute command lines.
 * A patch is added for '@' command, which cannot be called by
 * 'exec_command()'. Thus, we call 'exec_program()' instead, but we do
 * not have an error on return.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return the error code as a Python integer.
 */
PyObject* gpy_sicexec(PyObject *self, PyObject *args, PyObject *kwds) {
   char *line;
   char *cline;
   int   error = 0;

   if (! PyArg_ParseTuple(args, "s", &line))
      return NULL;

   if (sic_get_current_task_id() != sic_get_master_task_id()) {
      sic_post_command_text(line);
   } else {
       cline = malloc(1024);  /* Local copy of line, which can be
                                 modified inplace by exec_command */
       CFC_c2f_strcpy(cline,1024,line);
       if (cline[0] == '@') {
          exec_program(cline,1024);
       } else {
          exec_command(cline,&error,1024);
       }
       free(cline);
   }

   return PyLong_FromLong((long) error); /* New reference */
}

/**
 * Called when the exit order comes from Python, whatever the master/
 * slave statuses.
 *
 * @return Py_None.
 */
PyObject* gpy_onpythonexit(void) {
    int error = 0;
    int pygildas_get_loop(void);

    /* Exit directives: see also 'int Py_AtExit(void (*func)) ())' */
    if (pygildas_get_loop() == 1) {
       /* pygildas loop == 1 means that we are at the Python prompt, whatever
          the master/slave status.
          - if Python is master, we have to clean SIC properly
          - if Python is slave, exit order comes probably from Python (e.g.
            sys.exit()). We also clean SIC by hand before dying.
          We may want to had gmaster_on_exit, which calls the optional
          on_exit routines of the packages. */
       gmaster_clean(&error);
    } else {
      /* We are at the SIC prompt, and SIC wants to exit (e.g. command EXIT).
         gpy_onsicexit is executed and then there is nothing to do:
        - if Python is slave, Py_Finalize() is called, sys.at_exit enters here
          but since SIC is master it will exit cleanly by itself.
        - if Python is master, SIC is cleaned, then Py_Exit is called,
          sys.at_exit enters here but we already cleaned SIC */
    }

    Py_RETURN_NONE;
}

/**
 * Return the translation of a SIC logical (case insensitive). Output
 * is a zero-length string if no such SIC logical.
 *
 * @param[in] self
 * @param[in] args is a PyObject with input params values.
 * @return this translation as a PyString (new reference).
 */
PyObject* gpy_get_log_trans(PyObject *self, PyObject *args) {
   char *name;

   if (! PyArg_ParseTuple(args, "s", &name))
      return NULL;

   return PyUnicode_FromString(sic_s_get_translation(name)); /* New reference */
}

/**
 * Translate (if needed) a file name (in particular if it contains Sic
 * logicals)
 *
 * @param[in] self
 * @param[in] args is a PyObject with input params values.
 * @return this translation as a PyString (new reference).
 */
PyObject* gpy_parse_file(PyObject *self, PyObject *args) {
   char *name;

   if (! PyArg_ParseTuple(args, "s", &name))
      return NULL;

   return PyUnicode_FromString(sic_s_get_logical_path(name)); /* New reference */
}

/**
 * Set the Gildas-Python script execution error flag to true, which will be
 * catched by the Sic interpreter when leaving Python and returning to Sic.
 *
 * @return PyNone
 */
PyObject* gpy_sicerror_set(void) {
    gpy_execfile_error_set(1);
    Py_RETURN_NONE;
}

/**
 *
 * @return PyNone
 */
PyObject* gpy_message(PyObject *self, PyObject *args) {
   int sever,packid;

   CFC_DECLARE_LOCAL_STRING(rname);
   CFC_DECLARE_LOCAL_STRING(mess);

   if (! PyArg_ParseTuple(args, "iss", &sever, &rname, &mess))
      return NULL;

   packid = gmaster_get_id();
   CFC_STRING_LENGTH(rname) = strlen(rname);
   CFC_STRING_LENGTH(mess) = strlen(mess);

   gmessage_write(&packid,&sever,rname,mess
     CFC_PASS_STRING_LENGTH( rname) CFC_PASS_STRING_LENGTH( mess)
     );

   Py_RETURN_NONE;
}
