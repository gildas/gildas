
#include "win-main.h"

#include <windows.h>
#include "win-dialog.h"
#include "win-menu.h"
#include "win-graph.h"

void MyErrorHandler( char *str1, char *str2)
{
   fprintf(stderr, "error %d at %s in %s\n", GetLastError(), str1, str2);
}

void init_win_menu_dialog( HANDLE hInstance, 
					  DWORD ul_reason_being_called, LPVOID lpReserved)
{
  switch( ul_reason_being_called ) {
    case DLL_PROCESS_ATTACH:
	//CRT_INIT();
	//printf("Process attachment to dialogs DLL\n");
	init_menu( hInstance);
	init_dialog( hInstance);
	break;
/*
    case DLL_PROCESS_DETACH:
	CRT_INIT();
	break;
	case DLL_THREAD_ATTACH:
    break;
	case DLL_THREAD_DETACH:
    break;*/

  }
}

#ifndef GAG_USE_STATICLINK

BOOL APIENTRY DllMain( HANDLE hInstance, 
					  DWORD ul_reason_being_called, LPVOID lpReserved)
{
    if (!init_win_gtv( hInstance, ul_reason_being_called, lpReserved))
        return FALSE;
    init_win_menu_dialog( hInstance, ul_reason_being_called, lpReserved);
    return TRUE;
}

#endif

#include "gsys/cfc.h"

#define init_gui CFC_EXPORT_NAME( init_gui)

void CFC_API init_gui( )
{
    init_win_menu_dialog( GetModuleHandle( NULL), DLL_PROCESS_ATTACH, NULL);
}

// to force link of gwindows into greg
void gwindows_entry( )
{
}

#define gui_c_message_set_id CFC_EXPORT_NAME( gui_c_message_set_id)
void CFC_API gui_c_message_set_id( int *gpack_id)
{
}

