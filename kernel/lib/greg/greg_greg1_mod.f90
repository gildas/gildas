!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_axes
  !--------------------------------------------------------------------
  ! Variables and parameters used for plotting axes
  !--------------------------------------------------------------------
  logical, parameter :: jclock(8) =  &
    (/ .true.,.false.,.false.,.true.,.false.,.true.,.true.,.false. /)
  integer(4), parameter :: just(8) = (/ 0,0,2,2,2,2,0,0 /)
  real(8) :: smallx,smally              ! Minor tick spaces
  real(8) :: bigx,bigy                  ! Major tick spaces
  !
  ! Axes variables for 3D perspective plots
  real(4) :: xl1,xl2                    ! X-axis limits
  real(4) :: yl1,yl2                    ! Y-axis limits
  real(4) :: zmin,zmax                  ! Z level limits
  real(4) :: xposx,xposy                ! X label position
  real(4) :: yposx,yposy                ! Y label position
  logical :: doaxes                     !
end module greg_axes
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_pen
  !--------------------------------------------------------------------
  ! Virtual pencils attributes
  !--------------------------------------------------------------------
  !
  integer(kind=4), parameter :: maxpen=15     ! Pen numbered from 0 to 15
  ! ZZZ
  integer(kind=4), parameter :: mincolour=-1  !
  integer(kind=4), parameter :: maxcolour=23  !
  integer(kind=4), parameter :: minusercolour=8
  integer(kind=4), parameter :: maxusercolour=maxcolour
  integer(kind=4), parameter :: mindashed=1   !
  integer(kind=4), parameter :: maxdashed=8   ! Same as max_dash in GTV
  integer(kind=4), parameter :: minweight=1   !
  integer(kind=4), parameter :: maxweight=5   !
  !
  ! Default pen number and attributes (PEN /DEF). NB: the last (15th) pen
  ! is used (by default) for negative contouring by RGMAP
  integer(kind=4), parameter :: defpen = 0
  ! Default color for each pen:
  character(len=10), parameter :: defcolname(0:maxpen) =  (/           &
    'FOREGROUND',                                                      &
    'RED       ','GREEN     ','BLUE      ','CYAN      ','YELLOW    ',  &
    'MAGENTA   ','BACKGROUND','FOREGROUND','FOREGROUND','FOREGROUND',  &
    'FOREGROUND','FOREGROUND','FOREGROUND','FOREGROUND','BLUE      '   /)
  ! Placeholder for colors id, resolved from above names once at startup
  integer(kind=4) :: defcolid(0:maxpen)
  !
  integer(kind=4), parameter :: defdashe(0:maxpen) =  &
                    (/ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3 /)
  !
  ! Default weight for each pen (Greg number)
  integer(kind=4), parameter :: defweinum(0:maxpen) =  &
                    (/ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 /)
  ! Placeholder for actual weights, resolved from above numbers once at startup
  real(kind=4) :: defweival(0:maxpen)
  !
  ! Current pen attributes (all pens)
  real(kind=4)    :: lweigh(0:maxpen)  ! [cm] Weights for all pens
  integer(kind=4) :: ldashe(0:maxpen)  ! Dashes for all pens
  integer(kind=4) :: lcolou(0:maxpen)  ! Colours for all pens
  !
  ! Current pen attributes (current pen)
  integer(kind=4) :: cpen = defpen  ! Current pencil number
  real(kind=4)    :: cweigh         ! [cm] Current weight factor
  integer(kind=4) :: cdashe         ! Current dash style
  integer(kind=4) :: ccolou         ! Current colour
  logical         :: penupd         ! Does GTV internal pen attributes
                                    ! need update at next segment creation?
  !
end module greg_pen
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_xyz
  use gildas_def
  !--------------------------------------------------------------------
  ! X,Y,Z data arrays description
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !--------------------------------------------------------------------
  !
  real(kind=8), allocatable, target, save :: column_xyz(:,:)
  real(kind=8), pointer                   :: column_x(:)=>null()
  real(kind=8), pointer                   :: column_y(:)=>null()
  real(kind=8), pointer                   :: column_z(:)=>null()
  !
  integer(kind=4), save :: nxy  ! Number of data points
  integer(kind=4) :: maxxy      ! Current size of X,Y,Z allocation
  !
end module greg_xyz
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
