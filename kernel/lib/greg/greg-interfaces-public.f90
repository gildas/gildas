module greg_interfaces_public
  interface
    subroutine gr4_bars (cloc,nxy,x,y,err,orient,bval,eval)
      use phys_const
      use gbl_message
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ public
      !	Plot an errorbar of length ERR at point (X,Y)
      ! 	LOC = '-X' 'X' '+X' '-Y' 'Y' '+Y' '-O' 'O' '+O'
      ! Arguments :
      !	CLOC	C*(*)	Conventional orientation of errorbar
      !	X	R*4 (*)	Array of X user coordinates
      !	Y	R*4 (*)	Array of Y user coordinates
      !	ERR	R*4 (*) Array of error values
      !	ORIENT	R*4 (*) Array of errorbar orientations (only used for CLOC='O')
      !	NXY	I	Number of points
      ! Subroutines :
      !	INQANG	/ SETANG
      !	DRAW / RELOCATE
      !	SETDAS
      !	POINT
      !---------------------------------------------------------------------
      character(len=*) :: cloc          !
      integer(kind=4) :: nxy                    !
      real(kind=4) :: x(*)                    !
      real(kind=4) :: y(*)                    !
      real(kind=4) :: err(*)                  !
      real(kind=4) :: orient(*)               !
      real(kind=4) :: bval                    !
      real(kind=4) :: eval                    !
    end subroutine gr4_bars
  end interface
  !
  interface
    subroutine cubspl4(n,tau,c0,c1,c2,c3,ibcbeg,ibcend,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Piecewise cubic spline interpolants computation; adapted from
      !  'A practical guide to SPLINES' , CARL DE BOOR, Applied Mathematical
      !  Sciences, SPRINGER-VERLAG, VOL.27, P57-59 (1978).
      !
      !  c1(1), c1(N) are boundary condition information. Specifically:
      !
      !  IBCBEG = 0 means no boundary condition at tau(1) is given. In this
      !             case, the not-a-knot condition is used, i.e. the jump in
      !             the third derivative across TAU(2) is forced to zero,
      !             thus the first and the second cubic polynomial pieces
      !             are made to coincide.
      !  IBCBEG = 1 means that the slope at TAU(1) is made to equal c1(1),
      !             supplied by input.
      !  IBCBEG = 2 means that the second derivative at TAU(1) is made to
      !             equal c1(1), supplied by input.
      !
      !  IBCEND = 0, 1, or 2 has analogous meaning concerning the boundary
      !             condition at TAU(N), with the additional information
      !             taken from c1(N).
      !
      !  CJ(I), J=1,...,4; I=1,...,L (= N-1) are the polynomial coefficients
      !  of the cubic interpolating spline. Precisely, in the interval
      !  [ TAU(I), TAU(I+1) ]  the spline F is given by
      !         F(X) = c0(I)+H*(c1(I)+H*(c2(I)+H*c3(I)/3.)/2.)
      !  where H = X - TAU(I).
      !
      !  In other words, for I=1,...,N, c1(I) and c2(I) are respectively
      !  equal to the values of the first and second derivatives of the
      !  interpolating spline, and c3(I) is equal to the third derivative
      !  of the interpolating spline in the interval [ TAU(I), TAU(I+1) ].
      !  c3(N) is meaningless and is set to 0.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n       ! Number of data points (>1)
      real(kind=4),    intent(in)    :: tau(n)  ! Abscissae of data points, strictly monotonous
      real(kind=4),    intent(inout) :: c0(n)   ! Ordinates of data points (in) / First polynomial coefficient of spline (out)
      real(kind=4),    intent(inout) :: c1(n)   ! Boundary condition information (in) / Second polynomial coefficient of spline (out)
      real(kind=4),    intent(out)   :: c2(n)   ! Third polynomial coefficient of spline
      real(kind=4),    intent(out)   :: c3(n)   ! Fourth polynomial coefficient of spline
      integer(kind=4), intent(in)    :: ibcbeg  ! Boundary condition indicator
      integer(kind=4), intent(in)    :: ibcend  ! Boundary condition indicator
      logical,         intent(inout) :: error   ! Logical error flag
    end subroutine cubspl4
  end interface
  !
  interface
    subroutine cubspl8(n,tau,c0,c1,c2,c3,ibcbeg,ibcend,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Piecewise cubic spline interpolants computation; adapted from
      !  'A practical guide to SPLINES' , CARL DE BOOR, Applied Mathematical
      !  Sciences, SPRINGER-VERLAG, VOL.27, P57-59 (1978).
      !
      !  c1(1), c1(N) are boundary condition information. Specifically:
      !
      !  IBCBEG = 0 means no boundary condition at tau(1) is given. In this
      !             case, the not-a-knot condition is used, i.e. the jump in
      !             the third derivative across TAU(2) is forced to zero,
      !             thus the first and the second cubic polynomial pieces
      !             are made to coincide.
      !  IBCBEG = 1 means that the slope at TAU(1) is made to equal c1(1),
      !             supplied by input.
      !  IBCBEG = 2 means that the second derivative at TAU(1) is made to
      !             equal c1(1), supplied by input.
      !
      !  IBCEND = 0, 1, or 2 has analogous meaning concerning the boundary
      !             condition at TAU(N), with the additional information
      !             taken from c1(N).
      !
      !  CJ(I), J=1,...,4; I=1,...,L (= N-1) are the polynomial coefficients
      !  of the cubic interpolating spline. Precisely, in the interval
      !  [ TAU(I), TAU(I+1) ]  the spline F is given by
      !         F(X) = c0(I)+H*(c1(I)+H*(c2(I)+H*c3(I)/3.)/2.)
      !  where H = X - TAU(I).
      !
      !  In other words, for I=1,...,N, c1(I) and c2(I) are respectively
      !  equal to the values of the first and second derivatives of the
      !  interpolating spline, and c3(I) is equal to the third derivative
      !  of the interpolating spline in the interval [ TAU(I), TAU(I+1) ].
      !  c3(N) is meaningless and is set to 0.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n       ! Number of data points (>1)
      real(kind=8),    intent(in)    :: tau(n)  ! Abscissae of data points, strictly monotonous
      real(kind=8),    intent(inout) :: c0(n)   ! Ordinates of data points (in) / First polynomial coefficient of spline (out)
      real(kind=8),    intent(inout) :: c1(n)   ! Boundary condition information (in) / Second polynomial coefficient of spline (out)
      real(kind=8),    intent(out)   :: c2(n)   ! Third polynomial coefficient of spline
      real(kind=8),    intent(out)   :: c3(n)   ! Fourth polynomial coefficient of spline
      integer(kind=4), intent(in)    :: ibcbeg  ! Boundary condition indicator
      integer(kind=4), intent(in)    :: ibcend  ! Boundary condition indicator
      logical,         intent(inout) :: error   ! Logical error flag
    end subroutine cubspl8
  end interface
  !
  interface
    subroutine gr_curs(xa,ya,x,y,code)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !	Gives the cursor position in both coordinate systems
      !	The cursor is preset at plot position (X,Y).
      !	Code is always one upper case letter.
      ! Arguments :
      !	XA,YA	R*8	User coordinates		Output
      !	X,Y	R*4	Plot coordinates		Input/Output
      !	CODE	C*1	Character stroke		Output
      !---------------------------------------------------------------------
      real(kind=8) :: xa                      !
      real(kind=8) :: ya                      !
      real(kind=4) :: x                       !
      real(kind=4) :: y                       !
      character(len=*) :: code          !
    end subroutine gr_curs
  end interface
  !
  interface
    subroutine gr8_curve(nxy,x,y,z,var,per,bval,eval,error)
      use gildas_def
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !	Connect a polyline with blanking value
      ! Arguments
      !	NXY	I		Number of data points		Input
      !	X	R*8 (*) 	Array of X coordinates		Input
      !	Y	R*8 (*) 	Array of Y coordinates		Input
      !	Z	R*8 (*) 	Array of Z coordinates		Input
      !	VAR	CHAR*(*)	Interpolation variable		Input
      !				'X', 'Y', 'Z',
      !				'NUMBERING',
      !				'POLYGONAL_LENGTH',
      !				'CURVILINEAR_LENGTH'
      !	PER	L*4		Periodic flag			Input
      !	BVAL	R*8 		Blanking value			Input
      !	EVAL	R*8		Tolerance on blanking value	Input
      !---------------------------------------------------------------------
      integer(kind=4)                 :: nxy    !
      real*8                          :: x(*)   !
      real*8                          :: y(*)   !
      real*8                          :: z(*)   !
      character(len=*)                :: var    !
      logical                         :: per    !
      real*8                          :: bval   !
      real*8                          :: eval   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gr8_curve
  end interface
  !
  interface
    subroutine gr4_curve(nxy,x,y,z,var,per,bval,eval,error)
      use gildas_def
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !	Connect a polyline with blanking value
      ! Arguments
      !	NXY	I		Number of data points		Input
      !	X	R*4 (*) 	Array of X coordinates		Input
      !	Y	R*4 (*) 	Array of Y coordinates		Input
      !	Z	R*4 (*) 	Array of Z coordinates		Input
      !	VAR	CHAR*(*)	Interpolation variable		Input
      !				'X', 'Y', 'Z',
      !				'NUMBERING',
      !				'POLYGONAL_LENGTH',
      !				'CURVILINEAR_LENGTH'
      !	PER	L*4		Periodic flag			Input
      !	BVAL	R*4 		Blanking value			Input
      !	EVAL	R*4		Tolerance on blanking value	Input
      !---------------------------------------------------------------------
      integer(kind=4)                 :: nxy    !
      real*4                          :: x(1)   !
      real*4                          :: y(1)   !
      real*4                          :: z(1)   !
      character(len=*)                :: var    !
      logical                         :: per    !
      real*4                          :: bval   !
      real*4                          :: eval   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gr4_curve
  end interface
  !
  interface
    subroutine relocate(x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !   Position pen at user coordinate (X,Y)
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: x  ! User X coordinate
      real(kind=8), intent(in) :: y  ! User Y coordinate
    end subroutine relocate
  end interface
  !
  interface
    subroutine grelocate(ax,ay)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !   Position pen at Plot coordinate (X,Y)
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: ax  ! Plot X coordinate
      real(kind=4), intent(in) :: ay  ! Plot Y coordinate
    end subroutine grelocate
  end interface
  !
  interface
    subroutine run_greg3(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (public but should not) (mandatory because symbol
      ! is used elsewhere)
      ! This entry is for 3-D analysis
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      character(len=*), intent(in)  :: comm   !
      logical,          intent(out) :: error  !
    end subroutine run_greg3
  end interface
  !
  interface
    subroutine gr8_get(name,n,a)
      use gildas_def
      use greg_error
      use greg_xyz
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer(kind=4) :: n                      !
      real(kind=8) :: a(*)                    !
    end subroutine gr8_get
  end interface
  !
  interface
    subroutine gr4_get(name,n,a)
      use gildas_def
      use greg_error
      use greg_xyz
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer(kind=4) :: n                      !
      real(kind=4) :: a(*)                    !
    end subroutine gr4_get
  end interface
  !
  interface
    subroutine greg_projec_get(proj)
      use gwcs_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ public (for Astro)
      !  Return the current projection status in Greg
      !---------------------------------------------------------------------
      type(projection_t), intent(out) :: proj
    end subroutine greg_projec_get
  end interface
  !
  interface
    subroutine gr8_system(icode,error,equinox)
      use gbl_constant
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)           :: icode    !
      logical,         intent(inout)        :: error    !
      real(kind=4),    intent(in), optional :: equinox  !
    end subroutine gr8_system
  end interface
  !
  interface
    subroutine gr8_blanking (xbval,xeval)
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      real(kind=8) :: xbval                   !
      real(kind=8) :: xeval                   !
    end subroutine gr8_blanking
  end interface
  !
  interface
    subroutine gr_draw(name,narg,arg1,arg2)
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer :: narg                   !
      real*4 :: arg1                    !
      real*4 :: arg2                    !
    end subroutine gr_draw
  end interface
  !
  interface
    subroutine gr_draw_text(x,y,text,centering,angle,user,box,character,clip,error)
      use gbl_constant
      use gbl_message
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !
      ! centering: 1-9 for corresponding part of the text. 0 means automatic.
      !            Absent means use current SET CENTERING.
      !
      ! angle [degree]: if absent, use current SET ORIENTATION
      !
      ! user option: u_second, u_minute, u_degree, u_radian (codes from
      !              module gbl_constant). 0 means use current SET ANGLE
      !
      ! box option: 1-9 uses this corner as reference
      !
      ! character option: 1-9 uses this corner as reference
      !
      ! user, box, and character are exclusive. One of them must be present
      !
      ! clip option: 1 means HARD, 2 means SOFT, absent means no clipping
      !
      ! Missing capabilities: support sexagesimal (/USER ABSOLUTE)
      ! coordinates, and '*' '*' inputs (current cursor position)
      !---------------------------------------------------------------------
      real(kind=8),     intent(in)           :: x,y        ! Text coordinates
      character(len=*), intent(in)           :: text       ! Text to draw
      integer(kind=4),  intent(in), optional :: centering  ! Centering parameter
      real(kind=8),     intent(in), optional :: angle      ! [deg] Text angle
      integer(kind=4),  intent(in), optional :: user       ! Coordinates are user
      integer(kind=4),  intent(in), optional :: box        ! Coordinates are from box corner in phys unit
      integer(kind=4),  intent(in), optional :: character  ! Coordinates are from box corner in char unit
      integer(kind=4),  intent(in), optional :: clip       ! Clipping kind
      logical,          intent(inout)        :: error      ! Logical error flag
    end subroutine gr_draw_text
  end interface
  !
  interface
    subroutine gr_labe(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_labe
  end interface
  !
  interface
    subroutine gr_limi(narg,arg1,arg2,arg3,arg4)   ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      integer :: narg                   !
      real*4 :: arg1                    !
      real*4 :: arg2                    !
      real*4 :: arg3                    !
      real*4 :: arg4                    !
    end subroutine gr_limi
  end interface
  !
  interface
    subroutine greg_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      ! Public package definition routine.
      ! It defines package methods and dependencies to other packages.
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack ! Package info structure
    end subroutine greg_pack_set
  end interface
  !
  interface
    subroutine exec_greg(buffer)
      use gildas_def
      use greg_kernel
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (but should not)
      !---------------------------------------------------------------------
      character(len=*) :: buffer        !
    end subroutine exec_greg
  end interface
  !
  interface
    recursive subroutine run_greg1(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (public but should not) (mandatory because symbol
      ! is used elsewhere)
      !	Dispatch the command line
      ! Arguments :
      !	LINE	C*(*)	Command line			Input/Output
      !	COMM	C*(*)	Command				Input
      !	ERROR	L	Logical error flag		Output
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=*) :: comm          !
      logical :: error                  !
    end subroutine run_greg1
  end interface
  !
  interface
    subroutine run_greg2(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (public but should not) (mandatory because symbol
      ! is used elsewhere)
      ! This entry is for 2-D analysis
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=*) :: comm          !
      logical :: error                  !
    end subroutine run_greg2
  end interface
  !
  interface
    function gr_error ()
      use greg_error
      !---------------------------------------------------------------------
      ! @ public
      !	Check GREG library error status
      !---------------------------------------------------------------------
      logical :: gr_error               !
    end function gr_error
  end interface
  !
  interface
    subroutine gr_exec(buffer)
      use greg_error
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      !	Execute a single GREG command
      ! Arguments :
      !	BUFFER	C*(*)	The command line		Input
      ! Subroutines :
      !	(SIC)
      !	RUN_GREG1, RUN_GREG2
      !---------------------------------------------------------------------
      character(len=*) :: buffer        !
    end subroutine gr_exec
  end interface
  !
  interface
    subroutine gr4_histo(nxy,x,y,bval,eval)
      !---------------------------------------------------------------------
      ! @ public
      !  Connect a set of points with blanked values using histogram (top of
      ! histogram is drawn). Support for blank values and NaN.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: nxy     ! Number of points
      real(kind=4),    intent(in) :: x(nxy)  ! Array of X coordinates
      real(kind=4),    intent(in) :: y(nxy)  ! Array of Y coordinates
      real(kind=4),    intent(in) :: bval    ! Blanking value
      real(kind=4),    intent(in) :: eval    ! Tolerance on blanking
    end subroutine gr4_histo
  end interface
  !
  interface
    subroutine gr8_histo(nxy,x,y,bval,eval)
      !---------------------------------------------------------------------
      ! @ public-mandatory
      !  Connect a set of points with blanked values using histogram (top of
      ! histogram is drawn). Support for blank values and NaN.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: nxy     ! Number of points
      real(kind=8),    intent(in) :: x(nxy)  ! Array of X coordinates
      real(kind=8),    intent(in) :: y(nxy)  ! Array of Y coordinates
      real(kind=8),    intent(in) :: bval    ! Blanking value
      real(kind=8),    intent(in) :: eval    ! Tolerance on blanking
    end subroutine gr8_histo
  end interface
  !
  interface
    subroutine gr8_yxhisto(nxy,x,y,bval,eval)
      !---------------------------------------------------------------------
      ! @ public-mandatory
      !  Same as gr8_histo, but binning is along Y, and X provides the
      ! counts per bin. Bval/Eval apply on X.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: nxy     ! Number of points
      real(kind=8),    intent(in) :: x(nxy)  ! Array of X coordinates
      real(kind=8),    intent(in) :: y(nxy)  ! Array of Y coordinates
      real(kind=8),    intent(in) :: bval    ! Blanking value
      real(kind=8),    intent(in) :: eval    ! Tolerance on blanking
    end subroutine gr8_yxhisto
  end interface
  !
  interface
    subroutine gr8_histo_hatchfill(nxy,x,y,nbase,base,bval,eval,drawing,error)
      use gildas_def
      use greg_types
      !---------------------------------------------------------------------
      ! @ public-mandatory
      !  Build the histogram from the input X and Y values, and fill it
      ! with hatches and maybe color. Contour is not drawn.
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)    :: nxy          ! Number of points
      real(kind=8),              intent(in)    :: x(nxy)       ! Array of X coordinates
      real(kind=8),              intent(in)    :: y(nxy)       ! Array of Y coordinates
      integer(kind=size_length), intent(in)    :: nbase        ! Number base points (1 or Nxy)
      real(kind=8),              intent(in)    :: base(nbase)  ! Base of histogram
      real(kind=8),              intent(in)    :: bval         ! Blanking value
      real(kind=8),              intent(in)    :: eval         ! Tolerance on blanking value
      type(polygon_drawing_t),   intent(in)    :: drawing      ! Drawing description
      logical,                   intent(inout) :: error        ! Logical error flag
    end subroutine gr8_histo_hatchfill
  end interface
  !
  interface
    subroutine gr8_yxhisto_hatchfill(nxy,x,y,nbase,base,bval,eval,drawing,error)
      use gildas_def
      use greg_types
      !---------------------------------------------------------------------
      ! @ public-mandatory
      !  Same as gr8_histo_hatchfill, but binning is along Y, and X provides
      ! the counts per bin. Bval/Eval apply on X.
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)    :: nxy          ! Number of points
      real(kind=8),              intent(in)    :: x(nxy)       ! Array of X coordinates
      real(kind=8),              intent(in)    :: y(nxy)       ! Array of Y coordinates
      integer(kind=size_length), intent(in)    :: nbase        ! Number base points (1 or Nxy)
      real(kind=8),              intent(in)    :: base(nbase)  ! Base of histogram
      real(kind=8),              intent(in)    :: bval         ! Blanking value
      real(kind=8),              intent(in)    :: eval         ! Tolerance on blanking value
      type(polygon_drawing_t),   intent(in)    :: drawing      ! Drawing description
      logical,                   intent(inout) :: error        ! Logical error flag
    end subroutine gr8_yxhisto_hatchfill
  end interface
  !
  interface
    subroutine greg_draw_label(string,nc,error)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      ! Public subroutine which draws some text at current location, with
      ! current centering and orientation. This is equivalent to
      !   G\LABEL "Text"
      ! with no options.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: string
      integer(kind=4),  intent(in)    :: nc
      logical,          intent(inout) :: error
    end subroutine greg_draw_label
  end interface
  !
  interface
    subroutine gr4_levels(nl,zlevel)
      use greg_error
      use greg_contours
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      integer,intent(in) :: nl           ! Number of levels
      real(4), intent(in) :: zlevel(*)   ! level values
    end subroutine gr4_levels
  end interface
  !
  interface
    subroutine gr8_levels(nl,zlevel)
      use gbl_message
      use greg_error
      use greg_contours
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      integer, intent(in) :: nl          ! Number of levels
      real(8), intent(in) :: zlevel(*)   ! Level values
    end subroutine gr8_levels
  end interface
  !
  interface
    subroutine load_greg (line)
      use greg_kernel
      use greg_pen
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ public
      ! LOAD_GREG('INTERACTIVE GREGi')
      ! LOAD_GREG('LIBRARY GREGi')
      !---------------------------------------------------------------------
      character(len=*) :: line          !
    end subroutine load_greg
  end interface
  !
  interface
    subroutine gr8_glmsk(poly,msk,nx,ny,xconv,yconv,box)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return a mask from the current polygon (L version)
      !---------------------------------------------------------------------
      type(polygon_t), intent(in)    :: poly        ! Polygon description
      integer(kind=4), intent(in)    :: nx,ny       ! Size of mask
      logical,         intent(out)   :: msk(nx,ny)  ! Mask to be set
      real(kind=8),    intent(in)    :: xconv(3)    ! Conversion along X
      real(kind=8),    intent(in)    :: yconv(3)    ! Conversion along Y
      integer(kind=4), intent(inout) :: box(4)      ! Mask boundary (xblc,yblc,xtrc,ytrc)
    end subroutine gr8_glmsk
  end interface
  !
  interface
    subroutine gr8_gimsk(poly,msk,nx,ny,xconv,yconv,box)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return a mask from the current polygon (I*4 version)
      !---------------------------------------------------------------------
      type(polygon_t), intent(in)    :: poly        ! Polygon description
      integer(kind=4), intent(in)    :: nx,ny       ! Size of mask
      integer(kind=4), intent(out)   :: msk(nx,ny)  ! Mask to be set
      real(kind=8),    intent(in)    :: xconv(3)    ! Conversion along X
      real(kind=8),    intent(in)    :: yconv(3)    ! Conversion along Y
      integer(kind=4), intent(inout) :: box(4)      ! Mask boundary (xblc,yblc,xtrc,ytrc)
    end subroutine gr8_gimsk
  end interface
  !
  interface
    subroutine gr8_inout(poly,x,y,flag,n,where)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return 1 if inside, 0 if not
      !---------------------------------------------------------------------
      type(polygon_t), intent(in)    :: poly     ! Polygon description
      integer(kind=4), intent(in)    :: n        ! Number of points
      real(kind=8),    intent(in)    :: x(n)     ! X coordinates
      real(kind=8),    intent(in)    :: y(n)     ! Y coordinates
      real(kind=8),    intent(inout) :: flag(n)  ! Inside-out
      logical,         intent(in)    :: where    ! Test inside (.TRUE.) or Outside
    end subroutine gr8_inout
  end interface
  !
  interface
    subroutine gr4_inout(poly,x,y,flag,n,where)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return 1 if inside, 0 if not
      !---------------------------------------------------------------------
      type(polygon_t), intent(in)    :: poly     ! Polygon description
      integer(kind=4), intent(in)    :: n        ! Number of points
      real(kind=4),    intent(in)    :: x(n)     ! X coordinates
      real(kind=4),    intent(in)    :: y(n)     ! Y coordinates
      real(kind=4),    intent(inout) :: flag(n)  ! Inside-out
      logical,         intent(in)    :: where    ! Test inside (.TRUE.) or Outside
    end subroutine gr4_inout
  end interface
  !
  interface
    subroutine gr_pen(ipen,icolour,colour,idash,iweight,weight,error)
      use gbl_message
      use greg_pen
      !---------------------------------------------------------------------
      ! @ public
      ! This subroutine:
      !  - sets the pen to be used (if present) or uses the current pen, AND/OR
      !  - sets the new color (if present) of this pen, AND/OR
      !  - sets the new dashing (if present) of this pen, AND/OR
      !  - sets the new weight (if present) of this pen.
      ! All arguments are optional but at least 1 must be present
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in), optional :: ipen
      integer(kind=4),  intent(in), optional :: icolour  ! By Greg colour number OR
      character(len=*), intent(in), optional :: colour   ! By colour name
      integer(kind=4),  intent(in), optional :: idash
      integer(kind=4),  intent(in), optional :: iweight  ! By Greg weight number OR
      real(kind=4),     intent(in), optional :: weight   ! By weight value (cm)
      logical,          intent(inout)        :: error
    end subroutine gr_pen
  end interface
  !
  interface
    subroutine greset
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ public
      ! Reset the current Pen position to "undefined"
      !---------------------------------------------------------------------
      !
    end subroutine greset
  end interface
  !
  interface
    subroutine gr_point(n,is)
      use gildas_def
      use phys_const
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ public
      !  Draw 1 symbol of type N, style IS and current size
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: n   ! Number of summits
      integer(kind=4), intent(in) :: is  ! Style of polygon (0-3)
    end subroutine gr_point
  end interface
  !
  interface
    subroutine greg_poly_reset(poly,varname,error)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      type(polygon_t),  intent(inout) :: poly     !
      character(len=*), intent(in)    :: varname  ! Support variable in Sic
      logical,          intent(inout) :: error    ! Input status preserved if no error here
    end subroutine greg_poly_reset
  end interface
  !
  interface
    subroutine greg_poly_define(rname,srcname,srcisfile,poly,varname,error)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Define or define a polygon from a source (variable or file)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname      !
      character(len=*), intent(in)    :: srcname    ! Source name
      logical,          intent(in)    :: srcisfile  ! Source is a file or variable?
      type(polygon_t),  intent(inout) :: poly       !
      character(len=*), intent(in)    :: varname    ! Support variable in Sic
      logical,          intent(inout) :: error      !
    end subroutine greg_poly_define
  end interface
  !
  interface
    subroutine greg_poly_cursor(rname,poly,error)
      use gbl_message
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      ! Define a polygon with the cursor
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname  ! Calling command name
      type(polygon_t),  intent(out)   :: poly   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_poly_cursor
  end interface
  !
  interface
    subroutine greg_poly_parsename(line,iname,ivar,isfile,name,error)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: iname
      integer(kind=4),  intent(in)    :: ivar
      logical,          intent(out)   :: isfile
      character(len=*), intent(out)   :: name
      logical,          intent(inout) :: error
    end subroutine greg_poly_parsename
  end interface
  !
  interface
    subroutine greg_poly_load(rname,isfile,polyname,poly,error)
      use gbl_message
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      ! Load the polygon data into the poly structure
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      logical,          intent(in)    :: isfile
      character(len=*), intent(in)    :: polyname
      type(polygon_t),  intent(out)   :: poly ! *** JP in or inout?
      logical,          intent(inout) :: error
    end subroutine greg_poly_load
  end interface
  !
  interface
    function greg_poly_isnull()
      use greg_poly
      !---------------------------------------------------------------------
      ! @ public
      ! Return .TRUE. if Greg polygon is not a polygon / not defined
      !---------------------------------------------------------------------
      logical :: greg_poly_isnull ! Function
    end function greg_poly_isnull
  end interface
  !
  interface
    subroutine greg_poly_get(poly)
      use greg_poly
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      ! Return Greg internal polygon values copied input polygon
      !---------------------------------------------------------------------
      type(polygon_t), intent(out) :: poly
    end subroutine greg_poly_get
  end interface
  !
  interface
    function greg_poly_inside(x,y,poly)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      ! Find if a point is within a polygon
      !---------------------------------------------------------------------
      logical :: greg_poly_inside ! Function value on return
      real(kind=8),    intent(in) :: x,y  ! Coordinate of point to test
      type(polygon_t), intent(in) :: poly ! Polygon properties
    end function greg_poly_inside
  end interface
  !
  interface
    subroutine greg_poly2mask(blank,inside,poly,nx,ny,xconv,yconv,r2d)
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      ! Mask part of a 2D array using a given boundary
      !---------------------------------------------------------------------
      real(kind=4),         intent(in)    :: blank ! Blanking value
      logical,              intent(in)    :: inside
      type(polygon_t),      intent(in)    :: poly
      integer(kind=4),      intent(in)    :: nx
      integer(kind=4),      intent(in)    :: ny
      real(kind=8), target, intent(in)    :: xconv(3)
      real(kind=8), target, intent(in)    :: yconv(3)
      real(kind=4),         intent(inout) :: r2d(nx,ny)
    end subroutine greg_poly2mask
  end interface
  !
  interface greg_poly_plot
    subroutine greg_poly_plot1(poly,error)
      use gildas_def
      use gbl_message
      use greg_pen
      use greg_types
      !---------------------------------------------------------------------
      ! @ public-generic greg_poly_plot
      !  Draw the input polygon (contours only) with current pen
      !---------------------------------------------------------------------
      type(polygon_t), intent(in)    :: poly
      logical,         intent(inout) :: error
    end subroutine greg_poly_plot1
    subroutine greg_poly_plot2(poly,drawing,error)
      use gildas_def
      use gbl_message
      use greg_pen
      use greg_types
      !---------------------------------------------------------------------
      ! @ public-generic greg_poly_plot
      !  Draw the input polygon:
      !  - contoured, and/or
      !  - filled, and/or
      !  - hatched
      !---------------------------------------------------------------------
      type(polygon_t),         intent(in)    :: poly     !
      type(polygon_drawing_t), intent(in)    :: drawing  !
      logical,                 intent(inout) :: error    !
    end subroutine greg_poly_plot2
  end interface greg_poly_plot
  !
  interface
    subroutine find_blank4(y,bval,eval,nxy,is,ne,in)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      !  Find the next non-blanked interval in array Y
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)  :: nxy     ! Size of Y
      real(kind=4),              intent(in)  :: y(nxy)  ! Array to be checked against blanking
      real(kind=4),              intent(in)  :: bval    ! Blanking value
      real(kind=4),              intent(in)  :: eval    ! Tolerance
      integer(kind=size_length) :: is                   ! First element
      integer(kind=size_length) :: ne                   ! Number of elements
      integer(kind=size_length) :: in                   ! Start of next interval
    end subroutine find_blank4
  end interface
  !
  interface
    subroutine find_blank8(y,bval,eval,nxy,is,ne,in)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-mandatory
      !  Find the next non-blanked interval in array Y
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nxy     ! Size of Y
      real(kind=8),              intent(in) :: y(nxy)  ! Array to be checked against blanking
      real(kind=8),              intent(in) :: bval    ! Blanking value
      real(kind=8),              intent(in) :: eval    ! Tolerance
      integer(kind=size_length) :: is                  ! First element
      integer(kind=size_length) :: ne                  ! Number of elements
      integer(kind=size_length) :: in                  ! Start of next interval
    end subroutine find_blank8
  end interface
  !
  interface
    subroutine greg_poly_write(rname,poly,fname,error)
      use gildas_def
      use gbl_message
      use greg_kernel  ! For jtmp
      use greg_types
      !---------------------------------------------------------------------
      ! @ public
      !  Write the given polygon into the output file
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      type(polygon_t),  intent(in)    :: poly
      character(len=*), intent(in)    :: fname
      logical,          intent(inout) :: error
    end subroutine greg_poly_write
  end interface
  !
  interface
    subroutine gr_segm(name,error)
      use greg_pen
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*)                :: name   !
      logical,          intent(inout) :: error  !
    end subroutine gr_segm
  end interface
  !
  interface
    function gr_spen(new)
      use greg_pen
      !---------------------------------------------------------------------
      ! @ public
      !  Set the new pen to be used, and return the previous pen number.
      !---------------------------------------------------------------------
      integer(kind=4) :: gr_spen  ! Function value on return
      integer(kind=4), intent(in) :: new  ! New pen to be used
    end function gr_spen
  end interface
  !
  interface
    subroutine gr_out
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
    end subroutine gr_out
  end interface
  !
  interface
    subroutine gr_segm_close(error)
      !---------------------------------------------------------------------
      ! @ public
      ! GreG overlay to GTV closing routine, for symetry with 'gr_segm'.
      ! Close the segment currently opened, and draw it implicitely.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gr_segm_close
  end interface
  !
  interface
    subroutine greg3_spectrum_compute(line,error)
      use image_def
      use sic_types
      use greg_poly
      use gbl_message
      !-----------------------------------------------------------------
      ! @ public  
      !
      ! SPECTRUM OutVar Invar [Mask] /MEAN  [Mean|Sum]
      !     /CORNER Blc Trc /Plane First Last
      !
      !   Compute the mean spectrum over the specified region,
      !   bounded by the GreG polygon.
      !
      ! Invar is a SIC Image Variable
      ! Return this in the specified Outvar SIC variable
      !
      ! The idea is that this routine will be callable by GO VIEW as
      !   SPECTRUM MSPECTRE &1 /CORNER IBLC[1] IBLC[2] ITRC[1] ITRC[2]
      !     /PLANE MIPLANE[1] MIPLANE[2 [/MEAN] [/SUM]
      ! Invar will then be VUE, while for command VIEW in Mapping, Invar
      ! is  W. Outvar is MSPECTRE in the GO VIEW application.
      ! Loops can be parallelized for speed.
      !------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine greg3_spectrum_compute
  end interface
  !
  interface
    subroutine gstrlen(n,string,slength)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !  Finds the actual length (in centimeters) of a character string,
      ! taking into account the current G\SET CHAR and G\SET EXPAND settings
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: n        ! Length of string
      character(len=*), intent(in)  :: string   ! String
      real(kind=4),     intent(out) :: slength  ! Length in plot units
    end subroutine gstrlen
  end interface
  !
  interface
    subroutine gr8_marker(nxy,x,y,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !   Plot markers with blanking value
      !---------------------------------------------------------------------
      integer(kind=4) :: nxy                    ! Number of data points
      real(kind=8)    :: x(nxy)                 ! Array of X coordinates
      real(kind=8)    :: y(nxy)                 ! Array of Y coordinates
      real(kind=8)    :: bval                   ! Blanking value
      real(kind=8)    :: eval                   ! Tolerance on blanking value
    end subroutine gr8_marker
  end interface
  !
  interface
    subroutine gr4_marker(nxy,x,y,bval,eval)
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !   Plot markers with blanking value
      !---------------------------------------------------------------------
      integer(kind=4) :: nxy                    ! Number of data points
      real(kind=4)    :: x(nxy)                 ! Array of X coordinates
      real(kind=4)    :: y(nxy)                 ! Array of Y coordinates
      real(kind=4)    :: bval                   ! Blanking value
      real(kind=4)    :: eval                   ! Tolerance on blanking value
    end subroutine gr4_marker
  end interface
  !
  interface
    subroutine gr8_text(nxy,x,y,text,code,bval,eval)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !   Plot text with blanking value
      ! CODE (1-9) of text placement (ex: 5=center); negative values for
      ! "append" code
      !---------------------------------------------------------------------
      integer(kind=4)  :: nxy           ! Number of data points
      real(kind=8)     :: x(nxy)        ! Array of X coordinates
      real(kind=8)     :: y(nxy)        ! Array of Y coordinates
      character(len=*) :: text          ! TEXT to be plotted
      integer(kind=4)  :: code          ! CODE (1-9) of text placement
      real(kind=8)     :: bval          ! Blanking value
      real(kind=8)     :: eval          ! Tolerance on blanking value
    end subroutine gr8_text
  end interface
  !
  interface
    subroutine gr4_text(nxy,x,y,text,code,bval,eval)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !   Plot text with blanking value
      ! CODE (1-9) of text placement (ex: 5=center); negative values for
      ! "append" code
      !---------------------------------------------------------------------
      integer(kind=4)  :: nxy           ! Number of data points
      real(kind=4)     :: x(nxy)        ! Array of X coordinates
      real(kind=4)     :: y(nxy)        ! Array of Y coordinates
      character(len=*) :: text          ! TEXT to be plotted
      integer(kind=4)  :: code          ! CODE (1-9) of text placement
      real(kind=4)     :: bval          ! Blanking value
      real(kind=4)     :: eval          ! Tolerance on blanking value
    end subroutine gr4_text
  end interface
  !
  interface
    subroutine gr8_hatch(rname,nxy,x,y,angle,separ,phase,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Hatch the input polygon with parallel lines. Polygon must be closed
      ! (last point = first point)
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: rname   ! Calling routine name
      integer(kind=size_length), intent(in)    :: nxy     ! Number of points
      real(kind=8),              intent(in)    :: x(nxy)  ! X coordinates (user units)
      real(kind=8),              intent(in)    :: y(nxy)  ! Y coordinates (user units)
      real(kind=4),              intent(in)    :: angle   ! Hatching angle (radians)
      real(kind=4),              intent(in)    :: separ   ! Hatching separation (physical units)
      real(kind=4),              intent(in)    :: phase   ! Hatching phase (0. to 1.)
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine gr8_hatch
  end interface
  !
  interface
    function gr_clip(clip)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      ! Toggle Clipping
      !---------------------------------------------------------------------
      logical :: gr_clip                !
      logical :: clip                   !
    end function gr_clip
  end interface
  !
  interface
    subroutine gr4_phys_user (xi,yi,xu,yu,nxy)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !  Convert Physical units to User units
      !---------------------------------------------------------------------
      real(4), intent(in) :: xi(*)      ! X plot coordinates
      real(4), intent(in) :: yi(*)      ! Y plot coordinates
      real(4), intent(out) :: xu(*)     ! X User coordinates
      real(4), intent(out) :: yu(*)     ! Y User coordinates
      integer(4), intent(in) :: nxy     ! Number of points
    end subroutine gr4_phys_user
  end interface
  !
  interface
    subroutine gr_where(xa,ya,x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ public
      !	Returns the pen position with same conventions as GR_CURS
      !---------------------------------------------------------------------
      real(8), intent(out) :: xa     ! X User coordinates
      real(8), intent(out) :: ya     ! Y User coordinates
      real(4), intent(out) :: x      ! X Plot coordinates
      real(4), intent(out) :: y      ! Y Plot coordinates
    end subroutine gr_where
  end interface
  !
end module greg_interfaces_public
