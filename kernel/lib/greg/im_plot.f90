subroutine greg_plot (line,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_plot
  !---------------------------------------------------------------------
  ! @ private
  ! GREG
  ! Support for command PLOT
  !  [GREG2\]PLOT [Var_name]
  !     [/BLANKING Bval Eval Colour]
  !     [/SCALING Type Low_cut High_cut]
  !     [/POSITION Posx1 Posx2 Posy1 Posy2]
  !     [/BOUNDARIES Userx1 Userx2 Usery1 Usery2]
  !     [/VISIBLE]
  !
  ! Usage:
  !   Var_name is a 2-D SIC variable name. Subsets like A[4] where A
  ! is a 3-D sic variable are not yet allowed. This command is normally
  ! used in the following sequence:
  !
  !     SET BOX LOCATION Gx1 Gx2 Gy1 Gy2  ! Define box position
  !     RGDATA /VARIABLE Var_name         ! Get characteristics from image.
  !     LIMITS /RGDATA                    ! Get limits
  !     PLOT Var_name                     ! Plot the array in the box
  !     BOX
  !     RGMAP                             ! Draw contour levels (...)
  !
  ! Without argument, the RG variable is plotted
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  !
  call im_plot(line,error)
  if (error)  return
  !
end subroutine greg_plot
!
subroutine im_plot (line,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_types
  use greg_dependencies_interfaces, no_interface1=>gtv_image,  &
                                    no_interface2=>gtv_rgbimage,  &
                                    no_interface3=>gr4_minmax
  use greg_interfaces, except_this=>im_plot
  use greg_kernel
  use greg_rg
  !---------------------------------------------------------------------
  ! @ private
  ! GREG
  ! Support for command PLOT
  !  [GREG2\]PLOT [Var_name]
  !     [/BLANKING Bval Eval Colour]
  !     [/SCALING Type Low_cut High_cut]
  !     [/POSITION Posx1 Posx2 Posy1 Posy2]
  !     [/BOUNDARIES Userx1 Userx2 Usery1 Usery2]
  !     [/VISIBLE]
  !
  ! Usage:
  !   Var_name is a 2-D SIC variable name. Subsets like A[4] where A
  ! is a 3-D sic variable are not yet allowed. This command is normally
  ! used in the following sequence:
  !
  !     SET BOX LOCATION Gx1 Gx2 Gy1 Gy2  ! Define box position
  !     RGDATA /VARIABLE Var_name         ! Get characteristics from image.
  !     LIMITS /RGDATA                    ! Get limits
  !     PLOT Var_name                     ! Plot the array in the box
  !     BOX
  !     RGMAP                             ! Draw contour levels (...)
  !
  ! Without argument, the RG variable is plotted
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=index_length) :: mx,my
  real(kind=4) :: locx1,locx2,locy1,locy2
  real(kind=4) :: wux1,wux2,wuy1,wuy2
  real(kind=4) ::  cuts(2), extrem(2)
  real(kind=4) :: rcuts(2),rextrem(2)
  real(kind=4) :: gcuts(2),gextrem(2)
  real(kind=4) :: bcuts(2),bextrem(2)
  real(kind=4) :: loca(4),limts(4),convrt(6)
  real(kind=4) :: blank(3),scaling_lup_b
  integer(kind=4) :: k,nc
  integer(kind=size_length) :: kmin,kmax,size
  logical :: found,is_image,has_extrema,is_visible,is_rgb
  character(len=32) :: var_name,ima_name
  type(sic_descriptor_t) :: desc,inca
  integer(kind=address_length) :: ipnta,ipntr,ipntg,ipntb,ipnt
  save inca  ! Is this SAVE attribute needed?
  integer(kind=4) :: iper
  integer(kind=4) :: scaling
  integer(kind=4), parameter :: nscale=4
  character(len=12) :: scale(nscale),chain,keyw
  ! Data
  data scale/'LINEAR','LOGARITHMIC','EQUALIZATION','LUPTON'/
  !
  ! Locate the variable
  var_name = 'RG'  ! Use "RG" variable as a default
  call sic_ke (line,0,1,var_name,nc,.false.,error)
  if (error) return
  !
  call plot_getvar(var_name,inca,error)
  if (error)  return
  is_rgb = inca%ndim.eq.3 .and. inca%dims(3).gt.1  ! 3 RGB planes
  if (is_rgb) then
    ipntr = gag_pointer(inca%addr,memory)
    ipntg = ipntr+inca%dims(1)*inca%dims(2)
    ipntb = ipntg+inca%dims(1)*inca%dims(2)
  else
    ipnt = gag_pointer(inca%addr,memory)
  endif
  mx = inca%dims(1)
  my = inca%dims(2)
  !
  k = index(var_name,'[')
  if (k.ne.0) then
    ! call gagout('W-PLOT,  Plotting a Sub-array')
    var_name(k:) = ' '
  endif
  !
  ! Check if it is an IMAGE, and load the associated extrema,
  ! blanking and pixel to user coordinates conversion if so.
  iper = lenc(var_name)+1
  ima_name = var_name
  ima_name(iper:) = '%CONVERT'
  is_image = .false.
  call sic_descriptor(ima_name,desc,is_image)
  !
  if (sic_present(4,0)) then
    !
    ! 4 /BOUNDARIES option indicates the User Limits
    call sic_r4 (line,4,1,wux1,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,4,2,wux2,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,4,3,wuy1,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,4,4,wuy2,.true.,error)
    if (error)  goto 100
  else
    wux1 = gux1
    wux2 = gux2
    wuy1 = guy1
    wuy2 = guy2
  endif
  !
  ! This is an image with everything...
  has_extrema = .false.
  if (is_image) then
    ! Conversion
    ipnta = gag_pointer(desc%addr,memory)
    call r8tor4(memory(ipnta),convrt,6)
    ! Blanking
    ima_name(iper:) = '%BLANK'
    call sic_descriptor(ima_name,desc,found)
    ipnta = gag_pointer(desc%addr,memory)
    call r4tor4(memory(ipnta),blank,2)
    ! Extrema
    ima_name(iper:) = '%EXTREMA'
    has_extrema = sic_varexist(ima_name)
    if (has_extrema) then
      ima_name(iper:) = '%MIN'
      call sic_get_real(ima_name,extrem(1),error)
      ima_name(iper:) = '%MAX'
      call sic_get_real(ima_name,extrem(2),error)
    endif
  elseif (var_name.eq.'RG') then
    ! RG variable also has some conversion formula
    convrt(1) = rg%xref
    convrt(2) = rg%xval
    convrt(3) = rg%xinc
    convrt(4) = rg%yref
    convrt(5) = rg%yval
    convrt(6) = rg%yinc
  else
    !
    ! Compute the user coordinate conversion assuming
    !     1) The image fits in the box
    !     2) It has the corresponding user limits
    !
    convrt(1) = 0.5
    convrt(2) = wux1
    convrt(3) = (wux2-wux1)/mx
    convrt(4) = 0.5
    convrt(5) = wuy1
    convrt(6) = (wuy2-wuy1)/my
  endif
  !
  ! If limits are overridden, change them
  if (sic_present(4,0)) then
    convrt(1) = 0.5
    convrt(2) = wux1
    convrt(3) = (wux2-wux1)/mx
    convrt(4) = 0.5
    convrt(5) = wuy1
    convrt(6) = (wuy2-wuy1)/my
  endif
  !
  limts(1) = gux1
  limts(2) = gux2
  limts(3) = guy1
  limts(4) = guy2             ! It was the WUXi before ...
  !
  ! Option 1 /BLANKING
  if (sic_present(1,0)) then
    call sic_r4 (line,1,1,blank(1),.true.,error)
    if (error)  goto 100
    call sic_r4 (line,1,2,blank(2),.true.,error)
    if (error)  goto 100
    blank(3) = -1.0            ! Use Background Colour
    call sic_r4 (line,1,3,blank(3),.false.,error)
    if (error)  goto 100
  elseif (.not.is_image) then
    ! Use GreG blanking as a default
    blank(1) = cblank
    blank(2) = eblank
  endif
  !
  ! Compute the extrema
  if (is_rgb) then
    ! Ignore RG%MIN/MAX, compute extrema for each plane
    size = mx*my
    call gr4_minmax(size,memory(ipntr),blank(1),blank(2),rextrem(1),rextrem(2),  &
      kmin,kmax)
    call gr4_minmax(size,memory(ipntg),blank(1),blank(2),gextrem(1),gextrem(2),  &
      kmin,kmax)
    call gr4_minmax(size,memory(ipntb),blank(1),blank(2),bextrem(1),bextrem(2),  &
      kmin,kmax)
  else
    ! Get extrema from RG%MIN/MAX, else compute them
    if (.not.has_extrema) then
      if (var_name.eq.'RG') then
        call rgextr(0,extrem(1),extrem(2),rg%data,blank(1),blank(2),.false.)
      else
        call gr4_minmax (inca%size,memory(ipnt),blank(1),blank(2),  &
          extrem(1),extrem(2),kmin,kmax)
      endif
    endif
  endif
  !
  ! 2 /SCALING
  chain = 'LINEAR'  ! Default
  call sic_ke (line,2,1,chain,nc,.false.,error)
  if (error)  goto 100
  call sic_ambigs(rname,chain,keyw,scaling,scale,nscale,error)
  if (error)  goto 100
  !
  if (is_rgb) then
    ! RGB image (3 planes)
    rcuts = rextrem
    gcuts = gextrem
    bcuts = bextrem
    if (scaling.eq.4) then
      ! Lupton mode, retrieve beta factor
      scaling_lup_b = 3.0  ! Default
      call sic_r4(line,2,2,scaling_lup_b,.false.,error)
      if (error)  goto 100
    else
      ! Other modes, can override the default cuts
      call sic_r4(line,2,2,rcuts(1),.false.,error)
      if (error)  goto 100
      call sic_r4(line,2,3,rcuts(2),.false.,error)
      if (error)  goto 100
      !
      call sic_r4(line,2,4,gcuts(1),.false.,error)
      if (error)  goto 100
      call sic_r4(line,2,5,gcuts(2),.false.,error)
      if (error)  goto 100
      !
      call sic_r4(line,2,6,bcuts(1),.false.,error)
      if (error)  goto 100
      call sic_r4(line,2,7,bcuts(2),.false.,error)
      if (error)  goto 100
    endif
  else
    ! RG image (single plane)
    if (scaling.eq.4) then
      call greg_message(seve%e,rname,'LUPTON scaling valid only for RGB images')
      error = .true.
      return
    endif
    cuts = extrem
    call sic_r4 (line,2,2,cuts(1),.false.,error)
    if (error)  goto 100
    call sic_r4 (line,2,3,cuts(2),.false.,error)
    if (error)  goto 100
  endif
  !
  ! Setup the characteristics
  !
  ! Now setup the characteristics of the plotting area
  ! 3 /POSITION
  if (sic_present(3,0)) then
    call sic_r4 (line,3,1,locx1,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,3,2,locx2,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,3,3,locy1,.true.,error)
    if (error)  goto 100
    call sic_r4 (line,3,4,locy2,.true.,error)
    if (error)  goto 100
  else
    ! Use GreG Box location
    locx1 = gx1
    locx2 = gx2
    locy1 = gy1
    locy2 = gy2
  endif
  loca(1) = locx1
  loca(2) = locx2
  loca(3) = locy1
  loca(4) = locy2
  !
  ! /VISIBLE
  is_visible = .not.sic_present(5,0)
  !
  ! Create the image segment
  call gr_segm(var_name,error)
  if (error) then
    call greg_message(seve%e,rname,'Could not create image segment')
    goto 100
  endif
  !
  ! Create the image descriptor in GTVIRT
  if (is_rgb) then
    ! RGB image
    call gtv_rgbimage(                    &
      mx,my,                              &  ! Size of planes
      memory(ipntr),blank,rcuts,rextrem,  &  ! R plane description
      memory(ipntg),blank,gcuts,gextrem,  &  ! G plane description
      memory(ipntb),blank,bcuts,bextrem,  &  ! B plane description
      scaling,scaling_lup_b,              &  ! RGB planes scaling mode
      loca,                               &  ! Position in Paper coordinates
      limts,                              &  ! User limits
      convrt,                             &  ! Image pixel to User conversion formula
      is_visible,                         &  ! Always Visible ?
      error)
  else
    ! RG image
    call gtv_image(        &
      mx,my,memory(ipnt),  &  ! Data
      loca,                &  ! Position in Paper coordinates
      limts,               &  ! User limits
      convrt,              &  ! Image pixel to User conversion formula
      scaling,             &  ! Scaling mode (1=Lin, 2=Log, 3=Equ)
      cuts,                &  ! Low and High cuts
      extrem,              &  ! Low and High extrema
      blank,               &  ! Blanking value
      is_visible,          &  ! Always Visible ?
      .true.,              &  ! Update the SIC structure CURIMA%
      error)
  endif
  ! if (error) do nothing: we have to close this (empty) segment
  !
  ! Close the image segment
  call gtsegm_close(error)
  if (error)  goto 100
  !
100 continue
  call sic_volatile(inca)  ! Free the image incarnation
  !
end subroutine im_plot
!
subroutine plot_getvar(name,inca,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_types
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>plot_getvar
  !---------------------------------------------------------------------
  ! @ private
  ! Check if the variable is something you can plot
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: name   ! Variable name
  type(sic_descriptor_t), intent(out)   :: inca   ! REAL*4 incarnation
  logical,                intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='PLOT'
  type(sic_descriptor_t) :: desc
  logical :: found
  integer(kind=size_length) :: size
  integer(kind=4) :: i
  !
  call sic_descriptor(name,desc,found)
  if (.not.found) then
    call greg_message(seve%e,rname,'Variable '//trim(name)//' does not exist')
    error = .true.
    return
  endif
  !
  if (desc%ndim.eq.1) then
    call greg_message(seve%e,rname,'Variable '//trim(name)//' is 1-D only')
    error = .true.
    return
    !
! elseif (desc%ndim.eq.2) then
!   2-D RG plot: ok
  elseif (desc%ndim.eq.3) then
    ! 3th dimension == 1: ok (2D single plane RG plot)
    ! 3th dimension == 3: ok (2D 3 planes RGB plot)
    if (desc%dims(3).ne.1 .and. desc%dims(3).ne.3) then
      call greg_message(seve%e,rname,  &
        '3rd dimension must be 1 (single plane) or 3 (3 RGB planes)')
      error = .true.
      return
    endif
  elseif (desc%ndim.ge.4) then
    ! Check if the 4th and more dimensions are degenerated
    size = 1
    do i=desc%ndim,4,-1
      size = size*desc%dims(i)
    enddo
    if (size.gt.1) then
      call greg_message(seve%e,rname,'Variable '//trim(name)//' is 4-D or more')
      error = .true.
      return
    endif
  endif
  !
  !
  ! Incarnate it in REAL*4
  call sic_incarnate(fmt_r4,desc,inca,error)
  if (error) return
  !
end subroutine plot_getvar
