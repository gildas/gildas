!
! GREG1 Language
!
!---------------------------------------------------------------------------
!
module greg_lib
  !---------------------------------------------------------------------
  ! Mode Sous-Programme de GREG
  !---------------------------------------------------------------------
  integer :: n
  character(len=256) :: buf,buf2
  character(len=512) :: fbuf
  ! Former BLOCK DATA BLOCK_GREGLIB:
  data n /1/
  data buf/' '/, buf2/' '/
  !
end module greg_lib
!
!---------------------------------------------------------------------------
!
subroutine gr_axis(name,narg,arg1,arg2)    ! 28-Sep-1986
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr_axis
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: narg                   !
  real*4 :: arg1                    !
  real*4 :: arg2                    !
  !
  goto (1,2) narg
  fbuf = trim(buf2)//trim(name)//buf
  goto 10
1 write (buf2,100) trim(name),arg1
  fbuf = trim(buf2)//buf
  goto 10
2 write (buf2,100) trim(name),arg1,arg2
  fbuf = trim(buf2)//buf
10 call gr_exec1(fbuf)
  buf= ' '
  n=1
  return
100 format('AXIS ',a,2(1x,1pg10.3))
entry gr_axis_loca(narg,arg1,arg2)
  goto (11,12) narg
  write (buf(n:),101)
  n=lenc(buf)+1
  return
11 write (buf(n:),101) arg1
  n=lenc(buf)+1
  return
12 write (buf(n:),101) arg1,arg2
  n=lenc(buf)+1
  return
101 format(' /LOCATION',2(1x,1pg10.3))
entry gr_axis_tick(name,narg,arg1,arg2)
  goto (21,22) narg
  buf(n:) = name(1:1)
  n=n+1
  return
21 write (buf(n:),102) name(1:1),arg1
  n=n+13
  return
22 write (buf(n:),102) name(1:1),arg1,arg2
  n=n+24
  return
102 format(' /TICK ',a,2(1x,1pg10.3))
entry gr_axis_labe(name)
  buf(n:)=' /LABEL '//name(1:1)
  n=n+9
  return
entry gr_axis_log
  buf(n:)=' /LOG'
  n=n+5
  return
entry gr_axis_nolo
  buf(n:)=' /NOLOG'
  n=n+7
  return
entry gr_axis_abso
  buf(n:)=' /ABSOLUTE'
  n=n+10
  return
end subroutine gr_axis
!
!---------------------------------------------------------------------------
!
subroutine gr_box(name)        ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_box
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'BOX '//trim(name)//buf
  call gr_exec1(fbuf)
  buf = ' '
  n = 1
  return
  !
entry gr_box_abso
  buf = ' /ABSOLUTE'
  n = n+10
  return
end subroutine gr_box
!
!---------------------------------------------------------------------------
!
subroutine gr_clea(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_clea
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'CLEAR '//name
  call gr_execl(fbuf)
  return
end subroutine gr_clea
!
!---------------------------------------------------------------------------
!
subroutine gr_colu(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_colu
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'COLUMN '//name
  call gr_exec1(fbuf)
  return
end subroutine gr_colu
!
!---------------------------------------------------------------------------
!
subroutine gr_conn             ! Incomplete
  use greg_interfaces, except_this=>gr_conn
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: narg
  real :: arg1,arg2
  !
  fbuf = 'CONNECT '//buf(1:n)
  call gr_exec1(fbuf)
  n = 1
  buf = ' '
  return
  !
entry gr_conn_blan(narg,arg1,arg2)
  if (narg.gt.1) then
    write(buf(n:),101) arg1,arg2
    n = n+40
  elseif (narg.eq.1) then
    write(buf(n:),101) arg1
    n = n+25
  endif
101 format(' /BLANKING ',1pg14.7,1x,1pg14.7)
  return
end subroutine gr_conn
!
!---------------------------------------------------------------------------
!
subroutine gr_devi(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_devi
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'DEVICE '//name
  call gr_execl(fbuf)
  return
end subroutine gr_devi
!
!---------------------------------------------------------------------------
!
subroutine gr_draw(name,narg,arg1,arg2)
  use greg_interfaces, except_this=>gr_draw
  use greg_lib
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: narg                   !
  real*4 :: arg1                    !
  real*4 :: arg2                    !
  ! Local
  integer :: iarg
  !
  goto (1,2) narg
  write(buf2,100) name
  goto 10
1 write(buf2,100) name,arg1
  goto 10
2 write(buf2,100) name,arg1,arg2
  !
10 fbuf = trim(buf2)//buf
  call gr_exec1(fbuf)
  buf = ' '
  n = 1
  return
100 format('DRAW ',a,2(1x,1pg11.4))
  !
entry gr_draw_box(iarg)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  write(buf(n:),101) iarg
  n=n+7
101 format(' /BOX ',i1)
  return
  !
entry gr_draw_char(iarg)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  write(buf(n:),102) iarg
  n=n+13
102 format(' /CHARACTER ',i1)
  return
  !
entry gr_draw_user
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  buf(n:)=' /USER'
  n=n+6
  return
  !
entry gr_draw_clip
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  buf(n:)=' /CLIP'
  n = n+6
  return
  !
end subroutine gr_draw
!
subroutine gr_draw_text(x,y,text,centering,angle,user,box,character,clip,error)
  use gbl_constant
  use gbl_message
  use greg_interfaces, except_this=>gr_draw_text
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  !
  ! centering: 1-9 for corresponding part of the text. 0 means automatic.
  !            Absent means use current SET CENTERING.
  !
  ! angle [degree]: if absent, use current SET ORIENTATION
  !
  ! user option: u_second, u_minute, u_degree, u_radian (codes from
  !              module gbl_constant). 0 means use current SET ANGLE
  !
  ! box option: 1-9 uses this corner as reference
  !
  ! character option: 1-9 uses this corner as reference
  !
  ! user, box, and character are exclusive. One of them must be present
  !
  ! clip option: 1 means HARD, 2 means SOFT, absent means no clipping
  !
  ! Missing capabilities: support sexagesimal (/USER ABSOLUTE)
  ! coordinates, and '*' '*' inputs (current cursor position)
  !---------------------------------------------------------------------
  real(kind=8),     intent(in)           :: x,y        ! Text coordinates
  character(len=*), intent(in)           :: text       ! Text to draw
  integer(kind=4),  intent(in), optional :: centering  ! Centering parameter
  real(kind=8),     intent(in), optional :: angle      ! [deg] Text angle
  integer(kind=4),  intent(in), optional :: user       ! Coordinates are user
  integer(kind=4),  intent(in), optional :: box        ! Coordinates are from box corner in phys unit
  integer(kind=4),  intent(in), optional :: character  ! Coordinates are from box corner in char unit
  integer(kind=4),  intent(in), optional :: clip       ! Clipping kind
  logical,          intent(inout)        :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GR_DRAW_TEXT'
  logical :: douser,dobox,dochar,doclip_hard,doclip_soft
  integer(kind=4) :: kbox,ibox,iclip,icent
  real(kind=8) :: angl
  !
  douser = present(user)
  dobox  = present(box)
  dochar = present(character)
  doclip_hard = present(clip) .and. clip.eq.1
  doclip_soft = present(clip) .and. clip.eq.2
  !
  ! Sanity check
  if ((douser.and.dobox) .or. (douser.and.dochar) .or. (dobox.and.dochar)) then
    call greg_message(seve%e,rname,'user, box, and character options are exclusive')
    error = .true.
    return
  endif
  if (.not.douser .and. .not.dobox .and. .not.dochar) then
    ! We can enable this by looking at what is done in draw_parse_coordmode
    ! when no option is given to the command G\DRAW.
    call greg_message(seve%e,rname,'user, box, or character must be present')
    error = .true.
    return
  endif
  !
  if (douser) then
    if (user.eq.u_degree) then
      kbox = -3
    elseif (user.eq.u_minute) then
      kbox = -4
    elseif (user.eq.u_second) then
      kbox = -5
    else
      kbox = -1
    endif
    ibox = -1  ! Unused
    !
  elseif (dobox) then
    kbox = 0
    ibox = box
    !
  elseif (dochar) then
    kbox = 1
    ibox = character
    !
  endif
  !
  ! Convert X and Y coordinates and set current cursor position
  call draw_setcursor_relative('X',x,kbox,ibox,error)
  if (error)  return
  call draw_setcursor_relative('Y',y,kbox,ibox,error)
  if (error)  return
  ! xcurse and ycurse are now updated
  !
  ! Hard clipping?
  if (doclip_hard) then
    call grclip(xcurse,ycurse,iclip)
  else
    iclip = 0
  endif
  if (iclip.ne.0)  return  ! Add a warning?
  !
  ! Centering?
  if (present(centering)) then
    icent = centering
  else
    icent = icente  ! Current SET CENTERING
  endif
  if (icent.eq.0)  icent = centre(xcurse,ycurse)
  !
  ! Angle?
  if (present(angle)) then
    angl = angle
  else
    angl = tangle
  endif
  !
  call grelocate(xcurse,ycurse)
  call putlabel(len_trim(text),text,icent,angl,doclip_soft)
  !
end subroutine gr_draw_text
!
!---------------------------------------------------------------------------
!
subroutine gr_erro(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_erro
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'ERRORBAR '//name
  call gr_exec1(fbuf)
end subroutine gr_erro
!
!---------------------------------------------------------------------------
!
subroutine gr_hist             ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_hist
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real :: arg,arg1,arg2
  integer :: narg
  !
  call gr_exec1('HISTOGRAM'//buf)
  buf = ' '
  n=1
  return
  !
entry gr_hist_base(arg)
  write(buf(n:),100) arg
100 format(' /BASE ',1pg14.7)
  n = n+21
  return
  !
entry gr_hist_blan(narg,arg1,arg2)
  if (narg.gt.1) then
    write(buf(n:),101) arg1,arg2
    n = n+39
  elseif (narg.eq.1) then
    write(buf(n:),101) arg1
    n = n+25
  endif
101 format(' /BLANKING ',1pg14.7,1x,1pg14.7)
  return
end subroutine gr_hist
!
!---------------------------------------------------------------------------
!
subroutine gr_labe(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_labe
  use greg_lib
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  ! Local
  integer :: iarg
  !
  fbuf = 'LABEL "'//trim(name)//'"'//buf
  call gr_exec1(fbuf)
  buf = ' '
  n=1
  return
  !
entry gr_labe_x
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  buf(n:) = ' /X'
  n=n+3
  return
  !
entry gr_labe_y
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  buf(n:) = ' /Y'
  n=n+3
  return
  !
entry gr_labe_cent(iarg)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  write(buf(n:),100) iarg
  n=n+14
100 format(' /CENTER ',i5)
  return
  !
entry gr_labe_appe
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  buf(n:)=' /APPEND'
  n=n+8
  !
end subroutine gr_labe
!
!---------------------------------------------------------------------------
!
subroutine gr_limi(narg,arg1,arg2,arg3,arg4)   ! 28-Sep-1986
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr_limi
  use greg_lib
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer :: narg                   !
  real*4 :: arg1                    !
  real*4 :: arg2                    !
  real*4 :: arg3                    !
  real*4 :: arg4                    !
  ! Local
  integer :: n2
  character(len=*) :: name
  !
  buf2 = 'LIMITS '
  n2   = 8
  if (narg.eq.0) goto 10
  !
  if (locwrd(arg1).eq.0) then
    buf2(n2:) = ' *'
    n2 = 10
  else
    write(buf2(n2:),100) arg1
    n2 = n2+15
  endif
  if (narg.eq.1) then
    buf2(n2:) = ' * * *'
    n2 = n2+6
    goto 10
  endif
  !
  if (locwrd(arg2).eq.0) then
    buf2(n2:) = ' *'
    n2 = n2+2
  else
    write(buf2(n2:),100) arg2
    n2 = n2+15
  endif
  if (narg.eq.2) then
    buf2(n2:) = ' * *'
    n2 = n2+4
    goto 10
  endif
  !
  if (locwrd(arg3).eq.0) then
    buf2(n2:) = ' *'
    n2 = n2+2
  else
    write(buf2(n2:),100) arg3
    n2 = n2+15
  endif
  if (narg.eq.3) then
    buf2(n2:) = ' *'
    n2 = n2+2
    goto 10
  endif
  !
  if (locwrd(arg4).eq.0) then
    buf2(n2:) = ' *'
    n2 = n2+2
  else
    write(buf2(n2:),100) arg4
    n2 = n2+15
  endif
  !
10 fbuf = buf2(1:n2)//buf
  call gr_exec1(fbuf)
  buf= ' '
  n = 1
100 format(1pg14.7,1x)
  return
  !
entry gr_limi_xlog
  buf(n:) = ' /XLOG'
  n=n+6
  return
  !
entry gr_limi_ylog
  buf(n:) = ' /YLOG'
  n=n+6
  return
  !
entry gr_limi_rgda
  buf(n:) = ' /RGDATA'
  n=n+8
  return
  !
entry gr_limi_reve(name)
  buf(n:) = ' /REVERSE '//name
  n = 10+lenc(name)
  return
  !
entry gr_limi_blan(narg,arg1,arg2)
  if (narg.gt.1) then
    write(buf(n:),101) arg1,arg2
    n = n+40
  elseif (narg.eq.1) then
    write(buf(n:),101) arg1
    n = n+25
  endif
101 format(' /BLANKING ',1pg14.7,1x,1pg14.7)
end subroutine gr_limi
!
!---------------------------------------------------------------------------
!
subroutine gr_poin(narg,arg1)  ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_poin
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: narg                   !
  real*4 :: arg1                    !
  ! Local
  real*4 :: arg2
  !
  if (narg.gt.0) then
    write(buf,100) arg1
    n = n+15
  endif
  call gr_exec1('POINTS'//buf)
  buf = ' '
  n = 1
100 format(1x,1pg14.7)
  return
  !
entry gr_poin_blan(narg,arg1,arg2)
  if (narg.gt.1) then
    write(buf(n:),101) arg1,arg2
    n = n+40
  elseif (narg.eq.1) then
    write(buf(n:),101) arg1
    n = n+25
  endif
101 format(' /BLANKING ',1pg14.7,1x,1pg14.7)
  return
end subroutine gr_poin
!
!---------------------------------------------------------------------------
!
subroutine gr_rule(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_rule
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  !
  fbuf = 'RULE '//trim(name)//buf(1:n)
  call gr_exec1(fbuf)
  buf = ' '
  n = 1
  return
  !
entry gr_rule_majo
  buf(n:) = ' /MAJOR'
  n = n+7
  return
  !
entry gr_rule_mino
  buf(n:) = ' /MINOR'
  n = n+7
  return
end subroutine gr_rule
!
!---------------------------------------------------------------------------
!
subroutine gr_set(name,narg,arg1,arg2,arg3,arg4)   ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_set
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: narg                   !
  real*4 :: arg1                    !
  real*4 :: arg2                    !
  real*4 :: arg3                    !
  real*4 :: arg4                    !
  !
  goto (1,2,3,4) narg
  buf = 'SET '//trim(name)
  goto 10
1 write(buf,100) trim(name),arg1
  goto 10
2 write(buf,100) trim(name),arg1,arg2
  goto 10
3 write(buf,100) trim(name),arg1,arg2,arg3
  goto 10
4 write(buf,100) trim(name),arg1,arg2,arg3,arg4
10 call gr_exec1(buf)
  buf = ' '
  n = 1
  return
100 format('SET ',a,4(1x,1pg11.4))
end subroutine gr_set
!
!---------------------------------------------------------------------------
!
subroutine gr_show(name)
  use greg_interfaces, except_this=>gr_show
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  fbuf = 'SHOW '//name
  call gr_exec1(fbuf)
end subroutine gr_show
!
subroutine gr_tick(narg,arg1,arg2,arg3,arg4)
  use greg_interfaces, except_this=>gr_tick
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: narg                   !
  real*4 :: arg1                    !
  real*4 :: arg2                    !
  real*4 :: arg3                    !
  real*4 :: arg4                    !
  !
  goto (1,2,3,4) narg
  write(buf,100)
  goto 10
1 write(buf,100) arg1
  goto 10
2 write(buf,100) arg1,arg2
  goto 10
3 write(buf,100) arg1,arg2,arg3
  goto 10
4 write(buf,100) arg1,arg2,arg3,arg4
10 call gr_exec1(buf)
  buf = ' '
  n = 1
  return
100 format('TICKSPACE',4(1x,1pg11.4))
end subroutine gr_tick
!
!---------------------------------------------------------------------------
!
subroutine gr_valu             ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_valu
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  call gr_exec1('VALUES')
end subroutine gr_valu
!
subroutine gr_extr             ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_extr
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real :: arg1,arg2
  integer :: narg
  !
  call gr_exec2('EXTREMA'//buf)
  n = 1
  buf = ' '
  return
  !
entry gr_extr_blan(narg,arg1,arg2)
  if (narg.gt.1) then
    write(buf(n:),101) arg1,arg2
    n = n+40
  elseif (narg.eq.1) then
    write(buf(n:),101) arg1
    n = n+25
  endif
  return
101 format(' /BLANKING ',1pg14.7,1x,1pg14.7)
  !
entry gr_extr_plot
  buf(n:) = ' /PLOT'
  n = n+6
  return
end subroutine gr_extr
!
!---------------------------------------------------------------------------
!
subroutine gr_leve(name)       ! Incomplete
  use greg_interfaces, except_this=>gr_leve
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  fbuf = 'LEVELS '//name
  call gr_exec2(fbuf)
end subroutine gr_leve
!
subroutine gr_rgda(name)       ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_rgda
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  ! Local
  integer :: narg,iarg1,iarg2,iarg3,iarg4
  !
  fbuf = 'RGDATA '//trim(name)//buf
  call gr_exec2(fbuf)
  buf = ' '
  n   = 1
  return
entry gr_rgda_subs(narg,iarg1,iarg2,iarg3,iarg4)
  goto (1,2,3,4) narg
  write(buf,100)
  return
1 write(buf,100) iarg1
  return
2 write(buf,100) iarg1,iarg2
  return
3 write(buf,100) iarg1,iarg2,iarg3
  return
4 write(buf,100) iarg1,iarg2,iarg3,iarg4
  return
100 format(' /SUBSET',4(1x,i5))
end subroutine gr_rgda
!
!---------------------------------------------------------------------------
!
subroutine gr_rgma             ! 28-Sep-1986
  use greg_interfaces, except_this=>gr_rgma
  use greg_lib
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: narg
  real*4 :: arg, arg1, arg2
  integer :: iarg1,iarg2
  !
  call gr_exec2('RGMAP'//buf)
  buf = ' '
  n=1
  return
entry gr_rgma_abso(arg)
  write(buf(n:),100) arg
100 format(' /ABSOLUTE ',1pg11.4)
  n=n+22
  return
entry gr_rgma_perc(arg)
  write(buf(n:),101) arg
101 format(' /PERCENT ',1pg11.4)
  n=n+21
  return
entry gr_rgma_blan(narg,arg1,arg2)
  if (narg.ge.2) then
    write(buf(n:),102) arg1,arg2
    n = n+34
  elseif (narg.eq.1) then
    write(buf(n:),102) arg1
    n = n+22
  else
    buf(n:)=' /BLANKING'
    n = n+11
  endif
102 format(' /BLANKING ',1pg11.4,1x,1pg11.4)
entry gr_rgma_keep
  buf(n:) = ' /KEEP'
  n=n+6
  return
entry gr_rgma_pens(narg,iarg1,iarg2)
  if (narg.ge.2) then
    write (buf(n:),103) iarg1,iarg2
    n = n+12
  elseif (narg.eq.1) then
    write (buf(n:),103) iarg1
    n = n+9
  endif
103 format(' /PENS ',i2,1x,i2)
end subroutine gr_rgma
