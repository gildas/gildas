subroutine gconne(line,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gconne
  use gildas_def
  use sic_types
  use greg_kernel
  use greg_xyz
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! GREG Support routine for command
  !   CONNECT/BLANKING Bval Eval
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=size_length) :: ixy
  integer(kind=4) :: form,narg
  type(sic_descriptor_t) :: xinca,yinca
  integer(kind=address_length) :: ipx,ipy
  real(kind=8) :: bval,eval
  real(kind=4) :: r4bv,r4ev
  !
  save xinca,yinca
  !
  ! /FILL option
  if (sic_present(2,0)) then
    call grfill (line,error)
    return
  endif
  !
  eval = eblank
  call sic_r8 (line,1,2,eval,.false.,error)
  if (error) return
  bval = cblank
  call sic_r8 (line,1,1,bval,.false.,error)
  if (error) return
  narg = sic_narg(0)
  if (narg.eq.0 .or. narg.eq.2) then
    form = 0
    call get_incarnation('CONNECT',line,form,ixy,xinca,yinca,error)
    if (error) return
    ipx = gag_pointer(xinca%addr,memory)
    ipy = gag_pointer(yinca%addr,memory)
    if (form.eq.fmt_r8) then
      call gr8_connect(ixy,memory(ipx),memory(ipy),bval,eval)
    else
      r4bv=bval
      r4ev=eval
      call gr4_connect(ixy,memory(ipx),memory(ipy),r4bv,r4ev)
    endif
    call sic_volatile(xinca)
    call sic_volatile(yinca)
  endif
end subroutine gconne
!
subroutine get_incarnation(command,line,form,ixy,xinca,yinca,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>get_incarnation
  use gildas_def
  use sic_types
  use greg_xyz
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! GreG Internal routine
  !   Get incarnation of variables in command line, defaulting to X Y
  ! Warning: this routine may be called with more than 3 arguments on
  ! the command line (see RGDATA command).
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: command  !
  character(len=*),          intent(in)    :: line     !
  integer(kind=4),           intent(inout) :: form     !
  integer(kind=size_length), intent(out)   :: ixy      !
  type(sic_descriptor_t),    intent(out)   :: xinca    !
  type(sic_descriptor_t),    intent(out)   :: yinca    !
  logical,                   intent(inout) :: error    !
  ! Local
  character(len=80) :: var,chain
  logical :: found
  integer(kind=4) :: narg,nc
  type(sic_descriptor_t) :: xdesc,ydesc
  !
  data found/.false./
  !
  narg = sic_narg(0)
  if (narg.ge.2) then
    call sic_ch (line,0,1,var,nc,.true.,error)
    if (error) return
  else
    var = 'X'
  endif
  found = .true.
  call sic_materialize(var,xdesc,found)
  if (.not.found) then
    chain = 'Unknown variable '//var
    call greg_message(seve%e,command,chain)
    error = .true.
    return
  endif
  if (narg.ge.2) then
    call sic_ch (line,0,2,var,nc,.true.,error)
    if (error) return
  else
    var = 'Y'
  endif
  found = .true.
  call sic_materialize(var,ydesc,found)
  if (.not.found) then
    chain = 'Unknown variable '//var
    call greg_message(seve%e,command,chain)
    call sic_volatile(xdesc)   ! It may have been transposed...
    error = .true.
    return
  endif
  if (form.eq.0) then
    if (xdesc%type.eq.fmt_r4 .and. ydesc%type.eq.fmt_r4) then
      form = fmt_r4
    else
      form = fmt_r8
    endif
  endif
  call sic_incarnate(form,xdesc,xinca,error)
  if (error) then
    write(*,*) 'Error! ',var ! xdesc,xinca
    return
  endif
  call sic_incarnate(form,ydesc,yinca,error)
  if (error) then
    ! Must free both XDESC and XINCA if they are Transposed and Incarnated
    ! but they could point to the same virtual memory ...
    if (sic_notsamedesc(xdesc,xinca)) call sic_volatile(xdesc)
    call sic_volatile(xinca)
    return
  endif
  !
  if (yinca%size.ne.xinca%size) then
    call greg_message(seve%w,command,'Arrays have different sizes')
  endif
  ixy = min(xinca%size,yinca%size)
  if (form.eq.fmt_r8 .or. form.eq.fmt_i8) ixy = ixy/2
  ! Must free both XDESC and YDESC if they are Transposed
  ! but they could point to the same virtual memory as their Incarnation...
  if (sic_notsamedesc(xdesc,xinca)) call sic_volatile(xdesc)
  if (sic_notsamedesc(ydesc,yinca)) call sic_volatile(ydesc)
end subroutine get_incarnation
