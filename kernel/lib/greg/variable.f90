subroutine greg_variables
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_variables
  use greg_contours
  use greg_kernel
  use greg_rg
  use greg_wcs
  use greg_xyz
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  ! Local
  logical :: error
  integer :: dim1
  !
  error = .false.
  !
  dim1 = 0  ! if ndim==0, dim1 is ignored
  call sic_def_inte('NXY',           nxy,     0,dim1,.true., error)
  ! Pen position
  call sic_def_real('X_PEN',         xp,      0,dim1,.true., error)
  call sic_def_real('Y_PEN',         yp,      0,dim1,.true., error)
  ! Page size
  call sic_def_real('PAGE_X',        lx2,     0,dim1,.true., error)
  call sic_def_real('PAGE_Y',        ly2,     0,dim1,.true., error)
  ! Box position
  call sic_def_real('BOX_XMIN',      gx1,     0,dim1,.true., error)
  call sic_def_real('BOX_XMAX',      gx2,     0,dim1,.true., error)
  call sic_def_real('BOX_YMIN',      gy1,     0,dim1,.true., error)
  call sic_def_real('BOX_YMAX',      gy2,     0,dim1,.true., error)
  ! User limits
  call sic_def_dble('USER_XMIN',     gux1,    0,dim1,.true., error)
  call sic_def_dble('USER_XMAX',     gux2,    0,dim1,.true., error)
  call sic_def_dble('USER_YMIN',     guy1,    0,dim1,.true., error)
  call sic_def_dble('USER_YMAX',     guy2,    0,dim1,.true., error)
  ! Projection
  call sic_def_dble('LAMBDA_PROJ',   gproj%a0,   0,dim1,.true., error)
  call sic_def_dble('BETA_PROJ',     gproj%d0,   0,dim1,.true., error)
  call sic_def_dble('ANGLE_PROJ',    gproj%angle,0,dim1,.true., error)
  call sic_def_dble('LAMBDA_MIN',    ramin,      0,dim1,.true., error)
  call sic_def_dble('LAMBDA_MAX',    ramax,      0,dim1,.true., error)
  call sic_def_dble('BETA_MIN',      decmin,     0,dim1,.true., error)
  call sic_def_dble('BETA_MAX',      decmax,     0,dim1,.true., error)
  call sic_def_inte('COORD_SYSTEM',  i_system,   0,dim1,.true., error)
  ! Marker
  call sic_def_inte('MARKER_SIDES',   nsides,  0,dim1,.false.,error)
  call sic_def_inte('MARKER_STYLE',   istyle,  0,dim1,.false.,error)
  call sic_def_real('MARKER_SIZE',    csymb,   0,dim1,.false.,error)
  call sic_def_dble('MARKER_ANGLE',   sangle,  0,dim1,.false.,error)
  call sic_def_real('CHARACTER_SIZE', cdef,    0,dim1,.false.,error)
  call sic_def_dble('CHARACTER_ANGLE',tangle,  0,dim1,.false.,error)
  call sic_def_real('TICK_SIZE',      ctick,   0,dim1,.false.,error)
  ! RG array extrema
  call sic_def_real('RGMIN',  rg%zmin, 0,1,.true.,error)
  call sic_def_real('RGMAX',  rg%zmax, 0,1,.true.,error)
  call sic_def_real('RG_XMIN',rg%zxmin,0,1,.true.,error)
  call sic_def_real('RG_XMAX',rg%zxmax,0,1,.true.,error)
  call sic_def_real('RG_YMIN',rg%zymin,0,1,.true.,error)
  call sic_def_real('RG_YMAX',rg%zymax,0,1,.true.,error)
  ! Levels
  call sic_defstructure('GREG%',.true.,error)
  call sic_def_inte('GREG%NLEVEL',   ncl,     0,dim1,.true.,error)
  ! Units
  call sic_def_inte('U_ANGLE',u_angle, 0,1,.true.,error)
  !
  dim1 = 2
  ! Blanking (cblank and eblank are contiguous in memory)
  call sic_def_dble('BLANKING',      cblank,  1,dim1,.false.,error)
  ! Cursor position (xcurse and ycurse are contiguous in memory)
  call sic_def_real('PHY_CURS',      xcurse,  1,dim1,.false.,error)
  call sic_def_dble('USE_CURS',      ucurse,  1,dim1,.false.,error)
  call sic_def_inte('PIX_CURS',      pixcurse,1,dim1,.false.,error)
  call sic_def_char('CURSOR_CODE',   cursor_code,    .false.,error)
  !
  ! Polygon
  call sic_defstructure('POLY%',.true.,error)
  if (error)  return
  !
  ! Set variables
  call sic_defstructure('SET%',.true.,error)
  call greg_set_structure(error)
  if (error)  return
  !
end subroutine greg_variables
!
subroutine greg_set_structure(error)
  use greg_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! Create the structure SET%GREG and fill it.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: struct='SET%GREG'
  !
  call sic_defstructure(struct,.true.,error)
  if (error) return
  !
end subroutine greg_set_structure
