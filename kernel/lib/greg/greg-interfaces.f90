module greg_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! Greg interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use greg_interfaces_private
  use greg_interfaces_public
  !
end module greg_interfaces
!
module greg_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GREG dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
  use gwcs_interfaces_public
  use gio_interfaces_public
  use sic_interfaces_public
  use gchar_interfaces_public
  use gcont_interfaces_public
  use gtv_interfaces_public
end module greg_dependencies_interfaces
