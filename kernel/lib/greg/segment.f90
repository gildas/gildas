subroutine gr_segm(name,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr_segm
  use greg_pen
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*)                :: name   !
  logical,          intent(inout) :: error  !
  !
  call gtsegm(name,error)
  if (penupd) call setpen(cpen)
  gxp = -1e10
  gyp = -1e10
end subroutine gr_segm
!
function gr_spen(new)
  use greg_interfaces, except_this=>gr_spen
  use greg_pen
  !---------------------------------------------------------------------
  ! @ public
  !  Set the new pen to be used, and return the previous pen number.
  !---------------------------------------------------------------------
  integer(kind=4) :: gr_spen  ! Function value on return
  integer(kind=4), intent(in) :: new  ! New pen to be used
  ! Local
  integer(kind=4) :: old
  !
  call inqpen(old)
  if (new.ge.0 .and. new.ne.old) then
    cpen = new
    penupd = .true.
  endif
  gr_spen = old
end function gr_spen
!
subroutine gr_out
  use greg_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  call gtview('Append')
end subroutine gr_out
!
subroutine gr_segm_close(error)
  use greg_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public
  ! GreG overlay to GTV closing routine, for symetry with 'gr_segm'.
  ! Close the segment currently opened, and draw it implicitely.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  !
  call gtsegm_close(error)
  !
end subroutine gr_segm_close
