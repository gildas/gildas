subroutine setup(line,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>setup
  use greg_kernel
  use greg_pen
  use greg_xyz
  use greg_axes
  use greg_image
  use greg_wcs
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  GREG Support routine for command
  !  SET ...
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line   ! Command line
  logical,          intent(out)   :: error  !
  ! Local
  character(len=*), parameter :: rname='SETUP'
  integer, parameter:: h_square=3
  !
  real, parameter :: cheight_d=1.0
  real, parameter :: cwidth_d=0.8
  real, parameter :: cdef_d=0.6
  !
  real, parameter :: csymb_d=0.3
  real, parameter :: ctick_d=0.3
  real, parameter :: cmtick_d=0.15
  !
  integer, parameter :: istyle_d=0
  integer, parameter :: nsides_d=0
  !
  logical, parameter :: axis_xdecim_brief_d=.false.
  logical, parameter :: axis_xsexag_brief_d=.true.
  logical, parameter :: axis_ydecim_brief_d=.false.
  logical, parameter :: axis_ysexag_brief_d=.false.
  logical, parameter :: axis_xexpo_d=.false.
  logical, parameter :: axis_yexpo_d=.false.
  logical, parameter :: axis_zexpo_d=.false.
  !
  real, parameter :: label_xoff_d=2.0
  real, parameter :: label_yoff_d=6.0
  !
  integer(kind=4), parameter :: nwhat=21,ncoord=3,npage=4,nangu=4,nsyst=5
  integer(kind=4), parameter :: naxist=4,naxisn=3,naxisk=4,nexpot=4
  character(len=12) :: what(nwhat),coord(ncoord),page(npage),angu(nangu)
  character(len=12) :: syst(nsyst),axis_type(naxist),axis_name(naxisn)
  character(len=12) :: axis_kind(naxisk)
  character(len=12) :: chain,keyw,cfont1,cfont2,name
  character(len=32) :: argum
  character(len=256) :: local_line
  real(4), save :: lxs,lys
  real(4) :: bxs,bys,size,p1,p2,p3,p4,tangl4
  real(4) :: ratio,aspect
  real(8) :: p81,p82
  logical :: ldef,strict2011
  integer :: xsix,ysix,sever
  integer :: nl,n,i1,i2,i,ip,nc
  character(len=message_length) :: mess
  !
  ! Data
  data what /'ACCURACY','ANGLE_UNIT','AXIS','BLANKING','BOX_LOCATION',         &
             'CENTERING','CHARACTER','COORDINATES','DECIMAL','EXPAND','FONT',  &
             'LABEL','MARKER','ORIENTATION','PLOT_PAGE','SEXAGESIMAL',         &
             'SYSTEM','TICKSIZE','VIEWPORT','DRAW','COMMENT'/
  data cfont1/'SIMPLEX'/, cfont2/'DUPLEX'/
  data coord/'USER','BOX','CHARACTER'/
  data page/'LANDSCAPE','PORTRAIT','SQUARE','MATCH'/
  data angu/'SECOND','MINUTE','DEGREE','RADIAN'/
  data syst/'UNKNOWN','EQUATORIAL','GALACTIC','HORIZONTAL','ICRS'/
  data axis_type/'NOBRIEF','BRIEF','EXPONENT','NOEXPONENT'/
  data axis_name/'X','Y','Z'/
  data axis_kind/'XDECIMAL','YDECIMAL','XSEXAGESIMAL','YSEXAGESIMAL'/
  data lxs/lxs_d/, lys/lys_d/
  !
  ! Default
  error = .false.
  ldef = sic_present(1,0)
  if (.not.sic_present(0,1)) then
    if (ldef) then
      !
      ! Default Values
      accurd  = 0.01
      u_angle = 4
      tangle = 0.0d0
      axis_xdecim_brief = axis_xdecim_brief_d
      axis_xsexag_brief = axis_xsexag_brief_d
      axis_ydecim_brief = axis_ydecim_brief_d
      axis_ysexag_brief = axis_ysexag_brief_d
      axis_xexpo = axis_xexpo_d
      axis_yexpo = axis_yexpo_d
      axis_zexpo = axis_zexpo_d
      cdef  = cdef_d
      cwidth = cwidth_d
      cheight = cheight_d
      csymb = csymb_d
      ctick = ctick_d
      cmtick = cmtick_d
      gx1  = gx1_d
      gy1  = gy1_d
      gx2  = gx2_d
      gy2  = gy2_d
      istyle = istyle_d
      nsides = nsides_d
      expand  = 1.0
      icente = 0
      iboxd  = 0
      xsixty = format_decimal
      ysixty = format_decimal
      ndecimx = 1
      ndecimy = 0
      call setphysical(lxs_d,lys_d)
      call setlim(gux1,gux2,guy1,guy2)
      call setrem              ! Helps if even no projection
      h_mode  = h_landscape
      cblank = 0.d0
      eblank = -1.d0
      i_system = type_un
      i_equinox = equinox_null  ! Not relevant for UNKNOWN system
      smallx = 0               ! Automatic ticking
      bigx = 0
      smally = 0
      bigy = 0
      i_font = 0               ! This is not very consistent indeed
      penupd = .true.
      label_xoff = label_xoff_d
      label_yoff = label_yoff_d
      return
    else
      call greg_message(seve%e,'SET','Missing argument')
      error = .true.
      return
    endif
  endif
  !
  call sic_ke (line,0,1,chain,nc,.true.,error)
  if (error) return
  call sic_ambigs('SET',chain,keyw,n,what,nwhat,error)
  if (error) return
  if (ldef .and. sic_present(0,2)) then
    call greg_message(seve%e,'SET','No parameter value allowed with '//  &
    '/DEFAULT option')
    error = .true.
    return
  elseif (.not.ldef .and. .not.sic_present(0,2)) then
    call greg_message(seve%e,'SET','Missing parameter value(s)')
    error = .true.
    return
  endif
  ip = index(line,' ') + 1
  nl = ip + lenc(chain)
  !
  select case(keyw)
  !
  ! Accuracy of plot
  case('ACCURACY')
    if (ldef) then
      accurd = 0.01
    else
      call sic_r4 (line,0,2,accurd,.true.,error)
      if (error) return
      accurd = max(0.0001,(min(1.,accurd)))
    endif
  !
  ! Angle unit of projection
  case('ANGLE_UNIT')
    if (ldef) then
      u_angle = 4
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('SET ANGLE',name,argum,n,angu,nangu,error)
      if (error) return
      u_angle = n
      if (gproj%type.eq.p_none .and. u_angle.ne.4) then
        call greg_message(seve%w,'SETUP','Angular unit ignored when no '//  &
        'projection defined')
        u_angle = 4
      endif
    endif
  !
  ! Axis
  case('AXIS')
    if (ldef) then
      axis_xdecim_brief = axis_xdecim_brief_d
      axis_xsexag_brief = axis_xsexag_brief_d
      axis_ydecim_brief = axis_ydecim_brief_d
      axis_ysexag_brief = axis_ysexag_brief_d
      axis_xexpo = axis_xexpo_d
      axis_yexpo = axis_yexpo_d
      axis_zexpo = axis_zexpo_d
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('SET AXIS',name,argum,i,axis_type,naxist,error)
      if (error) return
      !
      if (i.le.2) then  ! BRIEF|NOBRIEF
        if (sic_present(0,3)) then
          call sic_ke (line,0,3,name,nc,.true.,error)
          if (error) return
          call sic_ambigs('SET AXIS',name,argum,n,axis_kind,naxisk,error)
          if (error) return
          !
          if (n.eq.1) then
            axis_xdecim_brief = i.eq.2
          elseif (n.eq.2) then
            axis_ydecim_brief = i.eq.2
          elseif (n.eq.3) then
            axis_xsexag_brief = i.eq.2
          elseif (n.eq.4) then
            axis_ysexag_brief = i.eq.2
          endif
          !
        else
          axis_xdecim_brief = i.eq.2
          axis_xsexag_brief = i.eq.2
          axis_ydecim_brief = i.eq.2
          axis_ysexag_brief = i.eq.2
        endif
        !
      else  ! EXPO|NOEXPO
        if (sic_present(0,3)) then
          call sic_ke (line,0,3,name,nc,.true.,error)
          if (error) return
          call sic_ambigs('SET AXIS',name,argum,n,axis_name,naxisn,error)
          if (error) return
          !
          if (n.eq.1) then
            axis_xexpo = i.eq.3
          elseif (n.eq.2) then
            axis_yexpo = i.eq.3
          elseif (n.eq.3) then
            axis_zexpo = i.eq.3
          endif
        else
          axis_xexpo = i.eq.3
          axis_yexpo = i.eq.3
          axis_zexpo = i.eq.3
        endif
      endif
    endif
  !
  ! Blanking value
  case('BLANKING')
    if (ldef) then
      eblank = -1.d0
    else
      call sic_r8 (line,0,2,p81,.true.,error)
      if (error) return
      call sic_r8 (line,0,3,p82,.true.,error)
      if (error) return
      cblank = p81
      eblank = p82
    endif
  !
  ! Box position
  case('BOX_LOCATION')
    p1 = gx1
    p2 = gx2
    p3 = gy1
    p4 = gy2
    if (.not.ldef) then
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      n = lenc(name)
    endif
    if (ldef .or. name(1:n).eq.page(4)(1:n)) then
      if (h_mode.eq.h_landscape) then
        gx1 = gx1_d
        gy1 = gy1_d
        gx2 = gx2_d
        gy2 = gy2_d
      elseif (h_mode.eq.h_portrait) then
        gx1 = gy1_d
        gx2 = gy2_d
        gy1 = gx1_d
        gy2 = gx2_d
      elseif (h_mode.eq.h_square) then
        gx1 = 0.150*lxs
        gx2 = 0.925*lxs
        gy1 = 0.125*lxs
        gy2 = 0.900*lxs
      else
        bxs = lx2-lx1
        bys = ly2-ly1
        gx1 = 0.150*bxs
        gx2 = 0.925*bxs
        gy1 = 0.125*bys
        gy2 = 0.900*bys
      endif
      ! SET BOX_LOCATION MATCH X Y Ratio
      if (ldef) then
        line(ip:) = 'BOX_LOCATION /DEFAULT'
      else
        call sic_r4 (line,0,3,gx1,.false.,error)
        if (error) return
        call sic_r4 (line,0,4,gy1,.false.,error)
        if (error) return
        ratio = 1.0
        call sic_r4 (line,0,5,ratio,.false.,error)
        if (error) return
        aspect = abs((guy2-guy1)/(gux2-gux1)*ratio)
        if (aspect.gt.1e2 .or. aspect.lt.1e-2) then
          call greg_message(seve%e,'SET BOX MATCH','Invalid aspect ratio')
          error = .true.
          return
        endif
        if ((gx2-gx1)*aspect.gt.(gy2-gy1)) then
          gx2 = gx1 + (gy2-gy1)/aspect
        else
          gy2 = gy1 + (gx2-gx1)*aspect
        endif
        local_line = line(nl:)
        line(ip:) = 'BOX_LOCATION'//local_line
      endif
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      n = lenc(name)
      ! Landscape style box
      if (name(1:n).eq.page(1)(1:n)) then
        gx1 = gx1_d
        gx2 = gx2_d
        gy1 = gy1_d
        gy2 = gy2_d
        line(ip:) = 'BOX_LOCATION LANDSCAPE'
        ! Portrait style box
      elseif (name(1:n).eq.page(2)(1:n)) then
        gx1 = gy1_d
        gx2 = gy2_d
        gy1 = gx1_d
        gy2 = gx2_d
        line(ip:) = 'BOX_LOCATION PORTRAIT'
        ! Square box
      elseif (name(1:n).eq.page(3)(1:n)) then
        size = min(lx2-lx1,ly2-ly1)
        if (lx2-lx1.gt.ly2-ly1) then
          gx2 = 0.900*lx2
          gx1 = gx2 - 0.750*size
          gy1 = 0.150*size
          gy2 = 0.900*size
        else
          gy1 = 0.125*lx2
          gy2 = gy1 + 0.750*size
          gx1 = 0.150*size
          gx2 = 0.900*size
        endif
        line(ip:) = 'BOX_LOCATION SQUARE'
        ! Error
      elseif (.not.sic_present(0,5)) then
        call greg_message(seve%e,'SET BOX_LOCATION','Four arguments required')
        error = .true.
        return
      else
        ! Given size box
        call sic_r4 (line,0,2,gx1,.false.,error)
        if (error) return
        call sic_r4 (line,0,3,gx2,.false.,error)
        if (error) return
        call sic_r4 (line,0,4,gy1,.false.,error)
        if (error) return
        call sic_r4 (line,0,5,gy2,.false.,error)
        if (error) return
        local_line = line(nl:)
        line(ip:) = 'BOX_LOCATION'//local_line
      endif
      if (gx1.ge.gx2 .or. gy1.ge.gy2 .or. gx1.lt.lx1 .or. gx2.gt.lx2 .or.  &
          gy1.lt.ly1 .or. gy2.gt.ly2) then
        write(mess,'(A,4(2X,F0.2))') 'Invalid BOX_LOCATION',gx1,gx2,gy1,gy2
        call greg_message(seve%e,rname,mess)
        gx1 = p1
        gx2 = p2
        gy1 = p3
        gy2 = p4
        error = .true.
        return
      endif
    endif
    call setlim(gux1,gux2,guy1,guy2)
    return
  !
  ! Centering Option
  case('CENTERING')
    if (ldef) then
      icente = 0
    else
      call sic_i4 (line,0,2,icente,.true.,error)
      if (error) return
      icente = min( max(icente,0),9)
    endif
  !
  ! Character Size
  case('CHARACTER')
    if (ldef) then
      cdef = cdef_d
    else
      call sic_r4 (line,0,2,cdef,.true.,error)
      if (error) return
    endif
  !
  ! Coordinate System
  case('COORDINATES')
    if (.not.sic_present(0,2)) then  ! /DEFAULT case
      iboxd = 0
      line(ip:) = 'COORDINATE BOX 0'
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('SET COORDINATE',name,argum,n,coord,ncoord,error)
      if (error) return
      if (n.eq.1) then
        iboxd = -1
        line(ip:) = 'COORDINATE USER'
      else
        iboxd = 10*(n-2)
        i = 0
        call sic_i4 (line,0,3,i,.false.,error)
        if (error) return
        if (n.ge.0 .and. n.le.9) then
          name(1:1) = char(i+ichar('0'))
          if (n.eq.2) then
            line(ip:) = 'COORDINATE BOX '//name(1:1)
          else
            line(ip:) = 'COORDINATE CHARACTER '//name(1:1)
          endif
          iboxd = iboxd+i
        else
          call greg_message(seve%e,'SET','Coordinate reference out of range')
          error = .true.
          return
        endif
      endif
    endif
    return
  !
  ! Decimal labels
  case('DECIMAL')
    if (ldef) then
      xsixty = format_decimal  ! SET DECIMAL /DEFAULT
      ysixty = format_decimal
    else
      xsix = xsixty
      ysix = ysixty
      if (sic_present(0,3)) then
        call sic_ke (line,0,3,name,nc,.true.,error)
        if (error) return
        if (name.eq.'X') then
          xsix = format_decimal
        elseif (name.eq.'Y') then
          ysix = format_decimal
        else
          call greg_message(seve%e,'SET','SET DECIMAL [X] [Y]')
          error = .true.
          return
        endif
      endif
      if (sic_present(0,2)) then
        call sic_ke (line,0,2,name,nc,.true.,error)
        if (error) return
        if (name.eq.'X') then
          xsix = format_decimal
        elseif (name.eq.'Y') then
          ysix = format_decimal
        else
          call greg_message(seve%e,'SET','SET DECIMAL [X] [Y]')
          error = .true.
          return
        endif
      endif
      xsixty = xsix
      ysixty = ysix
    endif
  !
  ! Expansion Factor
  case('EXPAND')
    if (ldef) then
      expand = 1.0
    else
      call sic_r4 (line,0,2,expand,.true.,error)
      if (error) return
    endif
  !
  ! Font Type
  case('FONT')
    if (ldef) then
      name = 'SIMPLEX'
      i_font = 0
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      n = lenc(name)
      if (name(1:n).eq.cfont1(1:n)) then
        i_font = 0
      elseif (name(1:n).eq.cfont2(1:n)) then
        i_font = 1
      else
        call greg_message(seve%e,'SET','SET FONT SIMPLEX or DUPLEX')
        error = .true.
        return
      endif
    endif
    penupd = .true.
  !
  ! Labeling
  case('LABEL')
    if (ldef) then
      label_xoff = label_xoff_d
      label_yoff = label_yoff_d
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('SET LABEL',name,argum,i,axis_name,naxisn,error)
      if (error) return
      if (i.eq.1) then
        label_xoff = label_xoff_d
        call sic_r4 (line,0,3,label_xoff,.false.,error)
      else if (i.eq.2) then
        label_yoff = label_yoff_d
        call sic_r4 (line,0,3,label_yoff,.false.,error)
      endif
    endif
  !
  ! Style of Markers
  case('MARKER')
    if (ldef) then
      nsides = nsides_d
      istyle = istyle_d
      csymb = csymb_d
      sangle = 0.d0
    elseif (.not.sic_present(0,4)) then
      call greg_message(seve%e,'SET MARKER','4 arguments (or *) required')
      error = .true.
      return
    else
      i1 = nsides
      i2 = istyle
      p3 = csymb
      p4 = sangle
      argum = '*'
      call sic_ch (line,0,2,argum,nc,.false.,error)
      if (argum.ne.'*') then
        call sic_math_inte(argum,nc,i1,error)
        if (error) return
      endif
      argum = '*'
      call sic_ch (line,0,3,argum,nc,.false.,error)
      if (argum.ne.'*') then
        call sic_math_inte(argum,nc,i2,error)
        if (error) return
      endif
      argum = '*'
      call sic_ch (line,0,4,argum,nc,.false.,error)
      if (argum.ne.'*') then
        call sic_math_real(argum,nc,p3,error)
        if (error) return
      endif
      argum = '*'
      call sic_ch (line,0,5,argum,nc,.false.,error)
      if (argum.ne.'*') then
        call sic_math_real(argum,nc,p4,error)
        if (error) return
      endif
      call setnsides(i1)         ! check and set nsides
      call setsty(i2)            ! check and set istyle
      call setsym(p3)            ! check and set csymb
      tangl4 = tangle            ! setang() requires R*4 arguments:
      call setang(tangl4,p4)     ! set sangle
    endif
  !
  ! Orientation of character strings
  case('ORIENTATION')
    if (ldef) then
      tangle = 0.0d0
      sangle = 0.0d0
    else
      call sic_r8 (line,0,2,tangle,.true.,error)
      sangle = tangle
      call sic_r8 (line,0,3,sangle,.false.,error)
      if (error) return
    endif
  !
  ! Plot_page size
  case('PLOT_PAGE')
    if (ldef) then
      lxs = lxs_d                ! Landscape Style by default
      lys = lys_d
      line(ip:) = 'PLOT_PAGE LANDSCAPE'
      h_mode = h_landscape
    else
      call sic_ch (line,0,2,name,nc,.true.,error)
      if (error) return
      n = lenc(name)
      call sic_upper(name)
      ! Landscape style box
      if (name(1:n).eq.page(1)(1:n)) then
        lxs = lxs_d
        lys = lys_d
        line(ip:) = 'PLOT_PAGE LANDSCAPE'
        h_mode = h_landscape
        ! Portrait style box
      elseif (name(1:n).eq.page(2)(1:n)) then
        lxs = lys_d
        lys = lxs_d
        line(ip:) = 'PLOT_PAGE PORTRAIT'
        h_mode = h_portrait
      elseif (name(1:n).eq.page(3)(1:n)) then
        lxs = lxs_d
        lys = lxs_d
        line(ip:) = 'PLOT_PAGE SQUARE'
        h_mode = h_square
        ! Error
      elseif (.not.sic_present(0,3)) then
        call greg_message(seve%e,'SET PLOT_PAGE','2 arguments required')
        error = .true.
        return
      else
        ! Given size page
        chain = '*'
        call sic_ch (line,0,2,chain,nc,.true.,error)
        if (error) return
        if (chain.eq.'*') then
          lxs = lx2
        else
          call sic_r4 (line,0,2,lxs,.true.,error)
          if (error) return
        endif
        chain = '*'
        call sic_ch (line,0,3,chain,nc,.true.,error)
        if (error) return
        if (chain.eq.'*') then
          lys = ly2
        else
          call sic_r4 (line,0,3,lys,.true.,error)
          if (error) return
        endif
        local_line = line(nl:)
        line(ip:) = 'PLOT_PAGE'//local_line
        h_mode = h_nonstandard
      endif
      !
      if (lxs.lt.0 .or. lys.lt.0) then
        call greg_message(seve%e,'SET','Invalid PLOT_PAGE')
        error = .true.
        return
      endif
    endif
    !
    call greg_message(seve%i,'SET','Clearing the tree and resetting BOX_LOCATION')
    call setphysical(lxs,lys)
    if (h_mode.eq.h_landscape) then
      gx1 = gx1_d
      gy1 = gy1_d
      gx2 = gx2_d
      gy2 = gy2_d
    elseif (h_mode.eq.h_portrait) then
      gx1 = gy1_d
      gx2 = gy2_d
      gy1 = gx1_d
      gy2 = gx2_d
    elseif (h_mode.eq.h_square) then
      gx1 = 0.150*lxs
      gx2 = 0.925*lxs
      gy1 = 0.125*lxs
      gy2 = 0.900*lxs
    else
      bxs = lx2-lx1
      bys = ly2-ly1
      gx1 = 0.150*bxs
      gx2 = 0.925*bxs
      gy1 = 0.125*bys
      gy2 = 0.900*bys
    endif
    call setlim(gux1,gux2,guy1,guy2)
    return
  !
  ! Sexagesimal labels
  case('SEXAGESIMAL')
    if (ldef) then               ! SET SEXAGESIMAL /DEFAULT
      ndecimx = 1
      ndecimy = 0
      xsixty = format_sexage_none
      ysixty = format_sexage_none
    else
      xsix = xsixty
      ysix = ysixty
      if (sic_present(0,4)) then
        call sic_ke (line,0,4,name,nc,.true.,error)
        if (error) return
        if (name.eq.'X') then
          call sic_i4 (line,0,5,ndecimx,.true.,error)
          if (error) return
          xsix = format_sexage_none
        elseif (name.eq.'Y') then
          call sic_i4 (line,0,5,ndecimy,.true.,error)
          if (error) return
          ysix = format_sexage_none
        else
          call greg_message(seve%e,'SET','SET SEXAGESIMAL [X NX] [Y NY]')
          error = .true.
          return
        endif
      endif
      if (sic_present(0,2)) then
        call sic_ke (line,0,2,name,nc,.true.,error)
        if (error) return
        if (name.eq.'X') then
          call sic_i4 (line,0,3,ndecimx,.true.,error)
          if (error) return
          xsix = format_sexage_none
        elseif (name.eq.'Y') then
          call sic_i4 (line,0,3,ndecimy,.true.,error)
          if (error) return
          ysix = format_sexage_none
        else
          call greg_message(seve%e,'SET','SET SEXAGESIMAL [X NX] [Y NY]')
          error = .true.
          return
        endif
      endif
      ndecimx = max(0,ndecimx)
      ndecimy = max(0,ndecimy)
      xsixty = xsix
      ysixty = ysix
    endif
  !
  ! System for projections
  case('SYSTEM')
    if (ldef) then
      i_system = type_un
      i_equinox = equinox_null  ! Not relevant for UNKNOWN system
    else
      call sic_ke (line,0,2,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('SET SYSTEM',name,argum,n,syst,nsyst,error)
      if (error) return
      i_system = n
      if (i_system.eq.type_eq) then
        i_equinox = i_equinox_def
        call sic_r4(line,0,3,i_equinox,.false.,error)
        if (error)  return
      else
        i_equinox = equinox_null  ! Not relevant for UNKNOWN system
      endif
    endif
  !
  ! Size of Ticks
  case('TICKSIZE')
    if (ldef) then
      ctick = ctick_d
      cmtick = cmtick_d
    else
      call sic_r4 (line,0,2,ctick,.true.,error)
      if (error) return
      if (sic_present(0,3)) then
        call sic_r4 (line,0,3,cmtick,.true.,error)
      else
        cmtick = 0.5*ctick
      endif
      if (error) return
    endif
  !
  ! View port : relative size
  case('VIEWPORT')
    bxs = lx2-lx1
    bys = ly2-ly1
    p1 = gx1
    p2 = gx2
    p3 = gy1
    p4 = gy2
    if (ldef) then
      gx1 = 0.150*bxs
      gx2 = 0.925*bxs
      gy1 = 0.125*bys
      gy2 = 0.900*bys
    elseif (.not.sic_present(0,5)) then
      call greg_message(seve%e,'SET VIEW_PORT','Four arguments required')
      error = .true.
      return
    else
      call sic_r4 (line,0,2,gx1,.false.,error)
      if (error) return
      call sic_r4 (line,0,3,gx2,.false.,error)
      if (error) return
      call sic_r4 (line,0,4,gy1,.false.,error)
      if (error) return
      call sic_r4 (line,0,5,gy2,.false.,error)
      if (error) return
      gx1 = gx1*bxs
      gx2 = gx2*bxs
      gy1 = gy1*bys
      gy2 = gy2*bys
    endif
    if (gx1.ge.gx2 .or. gy1.ge.gy2 .or. gx1.lt.lx1 .or. gx2.gt.lx2 .or.  &
        gy1.lt.ly1 .or. gy2.gt.ly2) then
      call greg_message(seve%e,'SET','Invalid VIEWPORT')
      gx1 = p1
      gx2 = p2
      gy1 = p3
      gy2 = p4
      error = .true.
      return
    endif
    call setlim(gux1,gux2,guy1,guy2)
  !
  ! Drawing State : ON or OFF (obsolete)
  case('DRAW')
    call sic_get_logi('GTV%STRICT2011',strict2011,error)
    if (strict2011) then
      sever = seve%e
    else
      sever = seve%w
    endif
    call greg_message(sever,rname,'GREG1\SET DRAW is obsolete. GTVirt is always awake.')
    if (strict2011) then
      error = .true.
      return
    endif
  !
  ! COMMENT indicator (for COLUMN)
  case('COMMENT')
    call sic_ch (line,0,2,argum,nc,.true.,error)
    if (error) return
    if (nc.ne.1) then
      call greg_message(seve%e,'SET','Comment separator must be 1 char')
      error = .true.
    else
      comment = argum
    endif
  !
  case default
    call greg_message(seve%e,'SET','Internal error, unknown keyword '//keyw)
    error=.true.
    return
  end select
  !
  ! O.K. Expand the line
  local_line = line(nl:)
  line(ip:) = trim(keyw)//local_line
end subroutine setup
