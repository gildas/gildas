subroutine sexfor(xa,ya)
  use phys_const
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>sexfor
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  !  Makes a nice format for projection information
  !
  !  Coordinates are displayed with 0.1 milliarcsecond precision
  !  (prec=2.78e-8 degrees), namely:
  !  a) log10(360/(24*3600*prec)) = 5.2 digits fractional part for 24
  !     div/turn sexagesimal notation (Right Ascension)
  !  b) log10(360/(360*3600*prec)) = 4 digits fractional part for 360
  !     div/turn sexagesimal notation (Declination)
  !  c) log10(360/(360*prec)) = 7.6 digits fractional part for 360
  !     div/turn decimal notation (Latitude/Longitude)
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: xa  ! X position
  real(kind=8), intent(in) :: ya  ! Y position
  ! Local
  character(len=*), parameter :: rname='SEXFOR'
  real(kind=8) :: xabs,yabs,xalt,yalt
  character(len=15) :: chra  ! 5 digits fractional part (see rad2sexa)
  character(len=15) :: chde  ! 4 digits fractional part (see rad2sexa)
  character(len=message_length) :: chain
  logical :: error
  !
  error = .false.
  chain = ' '
  call rel_to_abs (gproj,xa,ya,xabs,yabs,1)
  !
  select case (i_system)
  case (type_un)
    write (chain,100) xabs*deg_per_rad,yabs*deg_per_rad
    call greg_message(seve%r,rname,chain)
    !
  case (type_ic)
    ! Display native ICRS coordinates, alternate galactic not implemented
    call rad2sexa(xabs,24,chra)
    call rad2sexa(yabs,360,chde)
    write(chain,105)  chra,chde
    call greg_message(seve%r,rname,chain)
    !
    write(chain,103)
    call greg_message(seve%r,rname,chain)
    !
  case (type_eq)
    ! Display native equatorial coordinates + alternate galactic
    call rad2sexa(xabs,24,chra)
    call rad2sexa(yabs,360,chde)
    if (i_equinox.eq.equinox_null) then
      write(chain,104)  chra,chde
    else
      write(chain,102)  chra,chde,i_equinox
    endif
    call greg_message(seve%r,rname,chain)
    !
    if (i_equinox.eq.equinox_null) then
      write(chain,103)
    else
      call equ_gal(xabs,yabs,i_equinox,xalt,yalt,error)
      if (error)  return
      write(chain,101)  xalt*deg_per_rad,yalt*deg_per_rad
    endif
    call greg_message(seve%r,rname,chain)
    !
  case (type_ga)
    ! Display native galactic coordinates + alternate equatorial (use
    ! an arbitrary equinox)
    call gal_equ(xabs,yabs,xalt,yalt,i_equinox_def,error)
    if (error)  return
    call rad2sexa(xalt,24,chra)
    call rad2sexa(yalt,360,chde)
    write(chain,101)  xabs*deg_per_rad,yabs*deg_per_rad
    call greg_message(seve%r,rname,chain)
    write(chain,102)  chra,chde,i_equinox_def
    call greg_message(seve%r,rname,chain)
    !
  case default
    call greg_message(seve%w,rname,'System not supported')
    !
  end select
  !
100 format('Absolute position ',f13.8,1x,f13.8,' degrees')
101 format('    Lii ',f13.8,t35,'Bii ',f13.8)
102 format('    Ra  ',a    ,t35,'Dec ',a,' (Eq. ',f0.1,')')
103 format('    Lii  N/A'  ,t35,'Bii   N/A')
104 format('    Ra  ',a    ,t35,'Dec ',a,' (Eq. unknown)')
105 format('    Ra  ',a    ,t35,'Dec ',a,' (ICRS)')
end subroutine sexfor
