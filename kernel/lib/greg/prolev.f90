function lever(d,d0)
  !---------------------------------------------------------------------
  ! @ private
  !	Lever d'une etoile de declinaison D a latitude D0
  !
  !	PI/2-D0 < D	Circumpolaire
  !	D0-PI/2 < D < PI/2-D0	H = ACOS (TAN(D)/TAN(D0-PI/2))
  !	D < D0-PI/2	Toujours couchee...
  !
  ! Formule applicable a la partie visible du ciel en projection
  ! sur un plan
  !---------------------------------------------------------------------
  real*8 :: lever                   !
  real*8 :: d                       !
  real*8 :: d0                      !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real*8 :: dd,d1,d2
  real*8, parameter :: precision=1.0d-10
  !
  if (d0.lt.0.d0) then
    d2 = -d
    d1 = -d0
  else
    d2 = d
    d1 = d0
  endif
  dd = d1-pi*0.5d0
  if (d2.gt.-dd+precision) then
    lever = 2.d0*pi
  elseif (d2.lt.dd) then
    lever = -pi
  elseif (abs(d2+dd).le.precision) then
    lever = pi
  else
    lever = acos (tan(d2)/tan(dd))
  endif
end function lever
