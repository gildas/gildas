module greg_leaves
  !
  ! Leaves, contours, and vectors definitions
  integer, parameter :: vect_n=32768  ! Maximum number of vectors
  integer, parameter :: cont_n=16384  ! Maximum number of contours
  integer, parameter :: leaf_n=16384  ! Maximum number of leaves
  integer, parameter :: vect_null=-1000 ! Must be negative
  !
  integer :: vect_i                   !
  integer :: cont_i                   !
  integer :: leaf_i                   !
  ! Leaf
  integer :: leaf_mother(leaf_n)      !
  integer :: leaf_start(leaf_n)       !
  integer :: leaf_end(leaf_n)         !
  integer :: leaf_info(leaf_n)        !
  ! Contours
  integer :: cont_start(cont_n)       !
  integer :: cont_end(cont_n)         !
  integer :: cont_next(cont_n)        !
  ! Pointed chain of vectors
  integer :: vect_x(vect_n)           !
  integer :: vect_y(vect_n)           !
  integer :: vect_next(vect_n)        !
  ! Trigonomical direction
  logical :: trigo                    !
  !
  integer :: feuille_courante         !
  logical :: startofcont              !
  !
  ! Adresse feuille G ou D
  integer :: intern                   ! intern est l'adresse de la feuille deja trouvee
  !
end module greg_leaves
!
subroutine init_tree(icode,clipit,error)
  use greg_interfaces, except_this=>init_tree
  use greg_contours
  use greg_kernel
  use greg_leaves
  use greg_rg
  !---------------------------------------------------------------------
  ! @ private
  ! (re)initialize the tree structure for later filling contour
  ! capability
  !---------------------------------------------------------------------
  integer(kind=4)              :: icode   ! Unused
  logical,         intent(out) :: clipit  !
  logical,         intent(out) :: error   !
  ! Local
  real(kind=4) :: factor
  integer(kind=4) :: zzbox1,zzbox2,colid
  character(len=12) :: segname
  ! Data
  data factor/10000.0/
  !
  ! Beginning of new contour operation. Reset buffer and level counter.
  error = .false.
  !
  ! The cut and fill subroutines inserted by me (Gilles DUVERT) from
  ! my old program 'GRIS' use integer values . I am not sure wether those
  ! routines would still work with Real values. So I am conservative and
  ! convert the REAL cm to INTEGER microns!
  xxbox1=factor
  xxbox2=rg%nx*factor
  yybox1=factor
  yybox2=rg%ny*factor
  !
  ! Take care of clipping
  zzbox1 = factor*((gux1-rg%xval)/rg%xinc+rg%xref)
  zzbox2 = factor*((gux2-rg%xval)/rg%xinc+rg%xref)
  xxbox1 = max(xxbox1,min(zzbox1,zzbox2))
  xxbox2 = min(xxbox2,max(zzbox1,zzbox2))
  zzbox1 = factor*((guy1-rg%yval)/rg%yinc+rg%yref)
  zzbox2 = factor*((guy2-rg%yval)/rg%yinc+rg%yref)
  yybox1 = max(yybox1,min(zzbox1,zzbox2))
  yybox2 = min(yybox2,max(zzbox1,zzbox2))
  !
  clipit = (xxbox1.ne.factor) .or. (xxbox2.ne.rg%nx*factor) .or.  &
           (yybox1.ne.factor) .or. (yybox2.ne.rg%ny*factor)
  !
  call initialise_tree(feuille_courante,error)
  if (error) return
  startofcont=.true.
  ! Set parameters for grey contours
  ilev = ilev+1
  if (.not.chunkpatch) then
    write (segname,777) ilev,ncolo
777 format ('L',i2.2,'P',i2.2)
    call gtv_pencol_num2id('RGMAP',ncolo,colid,error)
    if (error)  return
    call gr_segm(segname,error)
    call setcol(colid)
  endif
end subroutine init_tree
!
subroutine fill_vect(x,y,error)
  use greg_interfaces, except_this=>fill_vect
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routine
  !     For each contour level, puts (X,Y) Coordinates of contour
  !     into tree structure
  !---------------------------------------------------------------------
  integer*4 :: x                    !
  integer*4 :: y                    !
  logical :: error                  !
  !
  ! Since I have inserted the old GRIS code that worked well with integers,
  ! I have converted X and Y to tenthousandts of pixels
  !
  error = .false.
  if (startofcont) then
    !     alloc nouveau contour
    startofcont=.false.
    if (leaf_end(feuille_courante).ne.0) then
      ! on pointe vers le prochain contour(vide)
      leaf_end(feuille_courante) = cont_next(leaf_end(feuille_courante))
    else
      ! C'est le tout debut
      leaf_end(feuille_courante) = leaf_start(feuille_courante)
    endif
    call get_triplet(cont_i,2,error)   !allocation 1 triplet k
    if (error) return
    cont_next(leaf_end(feuille_courante)) = cont_i
    cont_next(cont_i) = 0
    call get_triplet(vect_i,3,error)
    if (error) return
    cont_start(leaf_end(feuille_courante)) = vect_i
    cont_end(leaf_end(feuille_courante)) = 0
    vect_next(vect_i) = 0
    cont_end(leaf_end(feuille_courante)) =  &
      cont_start(leaf_end(feuille_courante))
  else
    cont_end(leaf_end(feuille_courante)) =  &
      vect_next(cont_end(leaf_end(feuille_courante)))
  endif
  vect_x(cont_end(leaf_end(feuille_courante))) = x
  vect_y(cont_end(leaf_end(feuille_courante))) = y
  call get_triplet(vect_i,3,error)
  if (error) return
  vect_next(cont_end(leaf_end(feuille_courante))) = vect_i
  vect_next(vect_i)=0
end subroutine fill_vect
!
subroutine fill_cont(error)
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routine
  !     End of contour line; starts an other contour
  !---------------------------------------------------------------------
  logical :: error                  !
  !
  error = .false.
  startofcont=.true.
end subroutine fill_cont
!
subroutine flush_tree(error)
  use greg_interfaces, except_this=>flush_tree
  use greg_contours
  use greg_leaves
  use greg_rg
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routine
  !     End of contour line; Show it
  !---------------------------------------------------------------------
  logical, intent(out) :: error  !
  ! Local
  logical :: a_ete_coupe,encore
  integer :: nvect_max,nvect,xmin,xmax,ymin,ymax
  real :: factor
  real*4 :: x,y
  integer :: cutx,cuty
  ! designations courantes
  integer :: contour_courant,vecteur_courant
  logical :: wind
  ! Data
  data nvect_max/512/
  data factor/1e-4/            ! from I*4 microns to R*4 cm
  !
  ! Min Max
  !SG!  PRINT *,'Entering FLUSH_TREE,  encore = ',encore
  !
  error = .false.
  trigo = .false.
  call gris_minmax(feuille_courante)
  call info_read(feuille_courante,nvect,xmin,ymin,xmax,ymax)
  !      write (*,*) nvect,highwater,cl(ilev),gzmin
  if (nvect.eq.0.and.highwater) then  
    !SG! PRINT *,'Filling with HIGHWATER '
    ! special handling of some empty contours (mostly used in CHUNK mode)
    ldd=1
    xu(ldd) = (xxbox1*factor-rg%xref)*rg%xinc + rg%xval
    yu(ldd) = (yybox1*factor-rg%yref)*rg%yinc + rg%yval
    ldd=ldd+1
    xu(ldd) = (xxbox1*factor-rg%xref)*rg%xinc + rg%xval
    yu(ldd) = (yybox2*factor-rg%yref)*rg%yinc + rg%yval
    ldd=ldd+1
    xu(ldd) = (xxbox2*factor-rg%xref)*rg%xinc + rg%xval
    yu(ldd) = (yybox2*factor-rg%yref)*rg%yinc + rg%yval
    ldd=ldd+1
    xu(ldd) = (xxbox2*factor-rg%xref)*rg%xinc + rg%xval
    yu(ldd) = (yybox1*factor-rg%yref)*rg%yinc + rg%yval
    ldd=ldd+1
    xu(ldd) = (xxbox1*factor-rg%xref)*rg%xinc + rg%xval
    yu(ldd) = (yybox1*factor-rg%yref)*rg%yinc + rg%yval
    call us4_to_int(xu,yu,xu,yu,ldd)
    call gr_fillpoly(ldd,xu,yu)
  else
    ! add current box to first first leaf, since FIND_BORDER needs it
    call info_write(feuille_courante,nvect,xxbox1,yybox1,xxbox2,yybox2)
    ! fermeture des contours avec caracterisation
    call find_border(feuille_courante,.true.,error)
    if (error) return
    ! virons les contours contenant moins de 2 vecteurs:
    call menage(feuille_courante)
    ! on coupe ensuite la feuille en 2, etc...
    ! cela pour passer moins de nvect_max vecteurs aux routines de base
    ! Polygones
    ! Ici il y a 2 approches: PostScript se debrouille bien avec des
    ! contours disjoints. On pourrait donc lui envoyer une feuille.
    ! X reclame une polyline, donc jointe. On lui envoie donc les contours 1
    ! par 1. Le probleme avec X ne se pose que dans le cas de 2 contours (au moins)
    ! d'une meme feuille qui tournent dans 2 sens differents: il remplira le contour
    ! le plus grand. Il faut donc couper tout contour contenu dans un contour plus
    ! grand. Plus facile a dire qu'a faire.
    !
    a_ete_coupe=.true.
    do while (a_ete_coupe)
      call init_search(1,encore)
      a_ete_coupe=.false.
      do while (encore)
        call explore_tree(feuille_courante,encore)
        call gris_minmax(feuille_courante)
        ! on coupe forcement une feuille contenant 1 contour tournant ds l'autre sens:
        call check_wind(feuille_courante,wind,cutx,cuty)
        if (wind) then
          a_ete_coupe=.true.
          call coupe_feuille(feuille_courante,.true.,cutx,cuty,error)
          if (error) return
        endif
      enddo
    enddo
    ! on va maintenant couper des monocontours a la taille
    ! de nvect_max vecteurs
    a_ete_coupe=.true.
    do while (a_ete_coupe)
      call init_search(1,encore)
      a_ete_coupe=.false.
      do while (encore)
        call explore_tree(feuille_courante,encore)
        call info_read(feuille_courante,nvect,xmin,ymin,xmax,ymax)
        if (nvect.gt.nvect_max) then
          !     on coupe forcement
          a_ete_coupe=.true.
          !     on droppe les feuilles trop petites
          if (((xmax-xmin).gt.2).or.((ymax-ymin).gt.2)) then
            call coupe_feuille(feuille_courante,.false.,xmax,ymax,error)
            if (error) return
          endif
        endif
      enddo
    enddo
    ! Plus de probleme
    call init_search(1,encore)
    do while (encore)
      call explore_tree(feuille_courante,encore)
      !SG! PRINT *,'Feuille_Courante ',feuille_courante,encore,leaf_start(feuille_courante)
      call info_read(feuille_courante,nvect,xmin,ymin,xmax,ymax)
      !     on droppe les feuilles trop petites
      if (((xmax-xmin).gt.2).or.((ymax-ymin).gt.2)) then
        contour_courant = leaf_start(feuille_courante)
        do while (cont_next(contour_courant).ne.0)
          ldd=1
          vecteur_courant = cont_start(contour_courant)
          x = vect_x(vecteur_courant)*factor
          xu(ldd) = (x-rg%xref)*rg%xinc + rg%xval
          y = vect_y(vecteur_courant)*factor
          yu(ldd) = (y-rg%yref)*rg%yinc + rg%yval
          vecteur_courant = vect_next(vecteur_courant)
          do while (vect_next(vecteur_courant).ne.0)
            ldd=ldd+1
            x = vect_x(vecteur_courant)*factor
            xu(ldd) = (x-rg%xref)*rg%xinc + rg%xval
            y = vect_y(vecteur_courant)*factor
            yu(ldd) = (y-rg%yref)*rg%yinc + rg%yval
            vecteur_courant = vect_next(vecteur_courant)
          enddo
          !SG! PRINT *,'Contour_Courant ',contour_courant,cont_next(contour_courant)
          contour_courant = cont_next(contour_courant)
          call us4_to_int(xu,yu,xu,yu,ldd)
          !SG! print *,'FillPoly LDD ',ldd
          !SG! print *,'FillPoly XU ',xu(1:ldd)
          !SG! print *,'FillPoly YU ',yu(1:ldd)
          call gr_fillpoly(ldd,xu,yu)
        enddo
      endif
    enddo
  endif
  !
  ! Grey-scale filling at this level
  if (.not.chunkpatch) then
    ncolo = ncolo+nstep          ! another one
    ncolo = mod((ncolo-8),16)+8  ! pen [8-23]
    ! ZZZ use parameters
  endif
  !
  !SG! PRINT *,'Leaving FLUSH_TREE'
end subroutine flush_tree
!
subroutine initialise_tree(current_leaf,error)
  use greg_interfaces, except_this=>initialise_tree
  use greg_contours
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !     Initialisation structure : 1 noeud, 2 feuilles.
  !     la Droite sera toujours vide
  !     VECT_I 	Occupation buffer
  !     next_cont	Liste chainee sur descripteurs deb_cont et fin_cont
  !     ( La fin de liste est un element next_cont nul, ce n'est pas
  !     un contour valide )
  !     deb_cont	Premier point liste chainee X_vect, Y_vect, next_vect
  !     fin_cont	Dernier point valide !!!       (ou 0 si aucun points)
  !     X_vect	Abscisse point
  !     Y_vect	Ordonnee
  !     next_vect	Pointeur element suivant. on y va Pen Up si
  !     next_vect est <0
  !     ( La fin de liste est un element next_vect nul, ce n'est pas
  !     un point valide )
  ! A NOTER QUE DANS CETTE VERSION IL N'Y A PAS DE NOTION DE PEN_UP
  !---------------------------------------------------------------------
  integer :: current_leaf           !
  logical :: error                  !
  ! Local
  ! designations courantes
  integer :: feuille_precedente
  !
  integer :: nvect,xmin,xmax,ymin,ymax
  !
  error = .false.
  ! Note S.Guilloteau 19-Aug-2014
  ! By initializing these two, I counter-act the strange
  ! cases where the outer box is erroneously added, which
  ! sometimes happened.  I found a case where the
  ! contour was broken in two, each perfectly regular.
  !   Looking at the first contour, the test on adding
  ! the out box turned out .true. because point (0,0) was
  ! among the list of vectors.  
  !   Somehow, the algorithm must have assigned a vect_next
  ! without giving it any value. 
  vect_x(:) = vect_null
  vect_y(:) = vect_null
  !SG! leaf_mother(:) = 0 ! seem useless
  !SG! cont_next(:) = 0   ! seem useless
  !
  vect_i = 0
  cont_i = 0
  leaf_i = 0
  !
  !
  ! allocation 1 triplet noeud
  call get_triplet(leaf_i,1,error)
  if (error) return
  leaf_mother(leaf_i) = 0
  current_leaf=leaf_i
  ! allocation 1 triplet feuille G
  call get_triplet(leaf_i,1,error)
  if (error) return
  leaf_mother(leaf_i) = -current_leaf
  leaf_start(current_leaf)=leaf_i
  ! allocation 1 triplet feuille G
  call get_triplet(cont_i,2,error)
  if (error) return
  leaf_start(leaf_i) = cont_i
  leaf_end(leaf_i) = 0
  cont_next(leaf_start(leaf_i)) = 0
  ! allocation 1 triplet vecteur
  call get_triplet(vect_i,3,error)
  if (error) return
  cont_start(leaf_start(leaf_i)) = vect_i
  cont_end(leaf_start(leaf_i)) = 0
  vect_next(cont_start(leaf_start(leaf_i))) = 0
  feuille_precedente=leaf_i
  ! allocation 1 triplet feuille D
  call get_triplet(leaf_i,1,error)
  if (error) return
  leaf_mother(leaf_i) = -current_leaf
  leaf_end(current_leaf)=leaf_i
  ! allocation 1 triplet contour
  call get_triplet(cont_i,2,error)
  if (error) return
  leaf_start(leaf_i) = cont_i
  leaf_end(leaf_i) = 0
  cont_next(leaf_start(leaf_i)) = 0
  ! allocation 1 triplet vecteur
  call get_triplet(vect_i,3,error)
  if (error) return
  cont_start(leaf_start(leaf_i)) = vect_i
  cont_end(leaf_start(leaf_i)) = 0
  vect_next(cont_start(leaf_start(leaf_i))) = 0
  ! on passe en G
  current_leaf=feuille_precedente
  ! en D
  feuille_precedente=leaf_i
  !
  ! on rentre 0 vecteurs, et max et min par defaut: xxmin , etc
  nvect=0
  xmin=min(xxbox1,xxbox2)
  xmax=max(xxbox1,xxbox2)
  ymin=min(yybox1,yybox2)
  ymax=max(yybox1,yybox2)
  call info_create(current_leaf,nvect,xmin,ymin,xmax,ymax,error)
  if(error) return
  call info_create(feuille_precedente,nvect,xmin,ymin,xmax,ymax,error)
  if(error) return
end subroutine initialise_tree
!
subroutine gris_minmax(feuille)
  use greg_interfaces, except_this=>gris_minmax
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !	MIN MAX sur une feuille
  !---------------------------------------------------------------------
  integer :: feuille                !
  ! Local
  ! designations courantes
  integer :: current_leaf,contour_courant,vecteur_courant
  ! Divers
  logical :: start
  integer :: nvect,xmin,xmax,ymin,ymax
  !
  current_leaf=feuille
  nvect = 0
  start = .true.
  contour_courant = leaf_start(current_leaf)
  do while (cont_next(contour_courant).ne.0)
    vecteur_courant = cont_start(contour_courant)
    do while (vect_next(vecteur_courant).ne.0)
      if (start) then
        xmin = vect_x(vecteur_courant)
        xmax = vect_x(vecteur_courant)
        ymin = vect_y(vecteur_courant)
        ymax = vect_y(vecteur_courant)
        start = .false.
      else
        xmin = min(xmin,vect_x(vecteur_courant))
        xmax = max(xmax,vect_x(vecteur_courant))
        ymin = min(ymin,vect_y(vecteur_courant))
        ymax = max(ymax,vect_y(vecteur_courant))
      endif
      vecteur_courant = vect_next(vecteur_courant)
      nvect = nvect + 1
    enddo
    contour_courant = cont_next(contour_courant)
  enddo
  !
  call info_write(current_leaf,nvect,xmin,ymin,xmax,ymax)
end subroutine gris_minmax
!
subroutine info_create(feuille,nvect,xmin,ymin,xmax,ymax,error)
  use greg_interfaces, except_this=>info_create
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  integer :: feuille                !
  integer :: nvect                  !
  integer :: xmin                   !
  integer :: ymin                   !
  integer :: xmax                   !
  integer :: ymax                   !
  logical :: error                  !
  ! Local
  integer :: current_leaf
  ! designations courantes
  integer :: temp
  !
  error = .false.
  current_leaf=feuille
  call get_triplet(vect_i,3,error)
  if (error) return
  leaf_info(current_leaf)=vect_i
  temp=vect_i
  vect_x(vect_i)=xmin
  vect_y(vect_i)=ymin
  call get_triplet(vect_i,3,error)
  if (error) return
  vect_next(temp)=vect_i
  temp=vect_i
  vect_x(vect_i)=xmax
  vect_y(vect_i)=ymax
  call get_triplet(vect_i,3,error)
  if (error) return
  vect_next(temp)=vect_i
  ! Why do we assign vect_x to a number of vectors ?
  ! and why is vect_y not assigned ?...
  vect_x(vect_i)=nvect 
  ! 
  vect_next(vect_i)=0
end subroutine info_create
!
subroutine info_write(feuille,nvect,xmin,ymin,xmax,ymax)
  use greg_interfaces, except_this=>info_write
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  integer :: feuille                !
  integer :: nvect                  !
  integer :: xmin                   !
  integer :: ymin                   !
  integer :: xmax                   !
  integer :: ymax                   !
  ! Local
  integer :: current_leaf
  integer :: temp
  !
  current_leaf=feuille
  temp=leaf_info(current_leaf)
  vect_x(temp)=xmin
  vect_y(temp)=ymin
  temp=vect_next(temp)
  vect_x(temp)=xmax
  vect_y(temp)=ymax
  temp=vect_next(temp)
  vect_x(temp)=nvect
  !SG! Print *,'Writing MinMax in ',temp,xmin,xmax,ymin,ymax
end subroutine info_write
!
subroutine info_read(feuille,nvect,xmin,ymin,xmax,ymax)
  use greg_interfaces, except_this=>info_read
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  integer :: feuille                !
  integer :: nvect                  !
  integer :: xmin                   !
  integer :: ymin                   !
  integer :: xmax                   !
  integer :: ymax                   !
  ! Local
  integer :: current_leaf
  integer :: temp
  !
  current_leaf=feuille
  temp=leaf_info(current_leaf)
  xmin=vect_x(temp)
  ymin=vect_y(temp)
  temp=vect_next(temp)
  xmax=vect_x(temp)
  ymax=vect_y(temp)
  temp=vect_next(temp)
  nvect=vect_x(temp)
end subroutine info_read
!
subroutine get_triplet(nn,type,error)
  use greg_interfaces, except_this=>get_triplet
  use gbl_message
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: nn                     !
  integer :: type                   !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='GREY'
  integer :: allowed(3)
  character(len=80) :: mess
  ! Data
  data allowed /leaf_n,cont_n,vect_n/
  !
  if (nn.lt.allowed(type)) then
    nn = nn + 1
  else
    write(mess,1) type, allowed(type)
    call greg_message(seve%e,rname,mess)
    call greg_message(seve%e,rname,'Plot too complex, aborting')
    error = .true.
  endif
1 format('Limit on type ',i1.1,' reached (max',i6,')')
end subroutine get_triplet
!
subroutine init_search(nn,encore)
  use greg_interfaces, except_this=>init_search
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! initialise la quete sur une feuille de l'arbre
  !---------------------------------------------------------------------
  integer :: nn                     !
  logical :: encore                 !
  !
  ! trouve la 1ere feuille
  intern=nn
  do while (leaf_mother(intern).ge.0)
    intern=leaf_start(intern)
  enddo
  encore=.true.
end subroutine init_search
!
subroutine explore_tree (nn,encore)
  use greg_interfaces, except_this=>explore_tree
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! explore l'arbre dont les feuilles sont des contours...
  !---------------------------------------------------------------------
  integer :: nn                     !adresse feuille G ou D
  logical :: encore                 !
  ! Local
  integer :: avant
  !
  nn = intern                  !donne la feuille
  avant=intern
  intern=abs(leaf_mother(intern))
  do while (leaf_end(intern).eq.avant)
    avant=intern
    intern=abs(leaf_mother(intern))
  enddo
  if (leaf_mother(intern).eq.0) then
    encore=.false.             !c'etait la derniere
    return
  endif
  intern=leaf_end(intern)
  do while (leaf_mother(intern).ge.0)
    intern=leaf_start(intern)
  enddo
  encore=.true.                !Y en a encore au moins une
end subroutine explore_tree
!
subroutine bord (xa,ya,xb,yb,xmin,xmax,ymin,ymax,trigo,tx,ty,nt)
  use greg_interfaces, except_this=>bord
  !---------------------------------------------------------------------
  ! @ private
  !	Rajoute le bord
  !
  !	Xa, Ya		Fin de l'element de contour courant
  !	Xb, Yb		Debut du suivant
  !	Xmin, Ymin, ...	Valeur des bords
  !	Trigo		Logique, sens rotation trigo ou non.
  !	Tx(12)		Vecteur d'abcisses suivant le bord
  !	Ty(12)		Vecteur d'ordonnees suivant le bord
  !	Nt		Nombre de points suivant le bord
  !			( Tx(1) et Ty(1) sont Xa et Ya,
  !			  Tx(Nt) et Ty(Nt) sont Xb et Yb)
  !
  !	Attention -- Nt est renvoye nul sur erreur. Les valeurs possibles
  !	de Nt en sortie sont 0 (contour deja ferme) ou [2...12].
  !
  !				( PV, sous la surveillance de GD)
  !---------------------------------------------------------------------
  integer :: xa                     !
  integer :: ya                     !
  integer :: xb                     !
  integer :: yb                     !
  integer :: xmin                   !
  integer :: xmax                   !
  integer :: ymin                   !
  integer :: ymax                   !
  logical :: trigo                  !
  integer :: tx(12)                 !
  integer :: ty(12)                 !
  integer :: nt                     !
  ! Local
  integer :: xbord(0:11),ybord(0:11)
  integer :: i1,i2,itri,i1p,i
  !
  nt = 0
  if ((xa.eq.xb).and.(ya.eq.yb)) return    ! deja concatenes....
  ! Les protections ne font pas de mal
  if ((xa.ne.xmin .and. xa.ne.xmax .and. ya.ne.ymin .and. ya.ne. ymax) .or.  &
      (xb.ne.xmin .and. xb.ne.xmax .and. yb.ne.ymin .and. yb.ne. ymax)) then
    ! write (6,*) ' error ',NT , XA,YA,XB,YB
    return
  endif
  xbord(0) = xmin
  ybord(0) = ymin
  xbord(1) = min(xa,xb)
  ybord(1) = ymin
  xbord(2) = max(xa,xb)
  ybord(2) = ymin
  xbord(3) = xmax
  ybord(3) = ymin
  xbord(4) = xmax
  ybord(4) = min(ya,yb)
  xbord(5) = xmax
  ybord(5) = max(ya,yb)
  xbord(6) = xmax
  ybord(6) = ymax
  xbord(7) = max(xa,xb)
  ybord(7) = ymax
  xbord(8) = min(xa,xb)
  ybord(8) = ymax
  xbord(9) = xmin
  ybord(9) = ymax
  xbord(10) = xmin
  ybord(10) = max(ya,yb)
  xbord(11) = xmin
  ybord(11) = min(ya,yb)
  ! Trigo
  if (trigo) then
    itri = 1
  else
    itri = -1
  endif
  ! On recherche (xa, ya)
  i1 = 0
  do while (xbord(i1).ne.xa .or. ybord(i1).ne.ya)
    i1 = modulo_gris(i1+itri,12)
  enddo
  ! I1 pointe sur (xa, ya). On continue a avancer pour sauter les
  ! points redondants eventuels.
  i1p = modulo_gris(i1+itri,12)
  do while (xbord(i1p).eq.xa .and. ybord(i1p).eq.ya)
    i1 = i1p
    i1p = modulo_gris(i1p+itri,12)
  enddo
  ! On recherche le prochain point (xb, yb).
  i2 = i1
  do while (xbord(i2).ne.xb .or. ybord(i2).ne.yb)
    i2 = modulo_gris(i2+itri,12)
  enddo
  ! Si i1 et I2 sont egaux, c'est pas une erreur...
  if (i1.eq.i2) then
    nt=1
    tx(nt) = xbord(i1)
    ty(nt) = ybord(i1)
    nt=2
    tx(nt) = xbord(i2)
    ty(nt) = ybord(i2)
    !         WRITE (6,*) ' egalite ',TX(1),TY(1),TX(2),TY(2)
    return
  endif
  ! On stocke tous les points [I1...I2]
  i = i1
  do while (i.ne.i2)
    nt = nt+1
    tx(nt) = xbord(i)
    ty(nt) = ybord(i)
    if (nt.ge.2) then
      if (tx(nt).eq.tx(nt-1) .and. ty(nt).eq.ty(nt-1)) then
        nt = nt - 1
      endif
    endif
    i = modulo_gris(i+itri,12)
  enddo
  nt = nt+1
  tx(nt) = xbord(i2)
  ty(nt) = ybord(i2)
  if (nt.gt.1) then
    if (tx(nt).eq.tx(nt-1) .and. ty(nt).eq.ty(nt-1)) then
      nt = nt - 1
    endif
  endif
  return
end subroutine bord
!
function modulo_gris (i,n)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer :: modulo_gris            !
  integer :: i                      !
  integer :: n                      !
  !
  if (i.lt.0) then
    modulo_gris = i + n
  elseif (i.ge.n) then
    modulo_gris = i - n
  else
    modulo_gris = i
  endif
end function modulo_gris
!
subroutine find_border(feuille,caracterize,error)
  use greg_interfaces, except_this=>find_border
  use greg_contours
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! Expansion de la feuille pour ajouter des vecteurs de bord.
  ! En d'autres termes, on veut des contours fermes...
  ! On a 4 sortes de contours disponibles: ceux fermes
  ! sur eux-meme, ceux qui partent d'un point interieur et arrivent au
  ! bord, ceux qui partent du bord et arrivent a un point interieur,
  ! et ceux qui vont de bord a bord. les categories 2 et 3, apres tri,
  ! donnent une categorie 4 par chainage; il faut maintenant chainer
  ! la categorie 4 , apres tri, dans le sens "trigo" pour "fermer
  ! le contour gris le plus exterieur".
  ! Par la suite, un algorithme simple de coupure de feuille en 2
  ! donnera toujours des contours caracterises (et donc remplissables
  ! par des traceurs even-odd fill , non-zero-winding fill et
  ! circular-oriented fill )
  ! Bord stocke dans Tx et Ty les valeurs de vecteurs (le premier
  ! etant le dernier vecteur du contour precedent, le dernier
  ! etant le premier du contour actuel, nt le nbre de vecteurs (>= 2)
  !---------------------------------------------------------------------
  integer :: feuille                !
  logical :: caracterize            !dit si on doit caracteriser le bord
  logical :: error                  !
  ! Local
  integer :: current_leaf
  ! caracterisation provisoire des contours en 4 classes:
  ! 1:fermes, 2:commence-sur-bord, 3:finit-sur-bord, 4:de-bord-a-bord
  integer :: contour_courant,contour_precedent,contour_suivant
  integer :: fin_4,fin_3,fin_2,fin_1
  integer :: deb_4,deb_3,deb_2,deb_1
  integer :: i,nsuiv
  integer :: contour_2,contour_3
  integer :: prev_2,prev_3
  ! necessaires pour la relation d'ordre sur le bord oriente de la cat 4
  integer :: ldeb4(cont_n),lfin4(cont_n),lref,ltot
  integer :: ll
  integer :: addr4(cont_n)
  integer :: n4,n3,n2,n1
  ! Chaine pointee de vecteurs
  integer :: nn
  integer :: vecteur_courant,temp
  ! divers...
  integer :: nt,tx(12),ty(12)
  integer :: nvect,xmin,xmax,ymin,ymax
  real :: val,vx1,vy1,vx2,vy2
  logical :: outgrey,tested,found
  !
  ! lecture min max necessaire pour connaitre les bords
  error = .false.
  current_leaf=feuille
  call info_read(current_leaf,nvect,xmin,ymin,xmax,ymax)
  !
  ! caracterisation des types de contour: coupure temporaire en 4 "feuilles"
  fin_1=0                      !temporaire
  fin_2=0                      !temporaire
  fin_3=0                      !temporaire
  fin_4=0                      !temporaire
  deb_1=0
  deb_2=0
  deb_3=0
  deb_4=0
  n1=0
  n2=0
  n3=0
  n4=0
  contour_precedent=0
  contour_courant=leaf_start(current_leaf)
  do while (cont_next(contour_courant).ne.0)
    contour_suivant = cont_next(contour_courant)        ! sauve le pointeur
    if ( (vect_x(cont_start(contour_courant)) .eq.   &  ! cat 1, contours fermes
          vect_x(cont_end(contour_courant)) ) .and.  &
         (vect_y(cont_start(contour_courant)) .eq.   &
          vect_y(cont_end(contour_courant))) ) then     ! il se ferme: type 1
      n1=n1+1
      if (fin_1.ne.0) then     !deb_1 .ne. 0 aussi
        cont_next(contour_courant)=cont_next(fin_1)
        cont_next(fin_1)=contour_courant
        fin_1=contour_courant
      else                     !allocation d'un triplet nul...
        deb_1=contour_courant
        fin_1=contour_courant
        call get_triplet(cont_i,2,error)
        if (error) return
        cont_next(fin_1)=cont_i
        cont_next(cont_i)=0
      endif
      ! Cat 2 or 4
    elseif (vect_x(cont_start(contour_courant)).eq.xmin .or.  &
            vect_x(cont_start(contour_courant)).eq.xmax .or.  &
            vect_y(cont_start(contour_courant)).eq.ymin .or.  &
            vect_y(cont_start(contour_courant)).eq.ymax) then
      ! type 2, fin pas sur le bord
      if (vect_x(cont_end(contour_courant)).ne.xmin .and.  &
          vect_x(cont_end(contour_courant)).ne.xmax .and.  &
          vect_y(cont_end(contour_courant)).ne.ymin .and.  &
          vect_y(cont_end(contour_courant)).ne.ymax) then
        n2=n2+1
        if (fin_2.ne.0) then
          cont_next(contour_courant)=cont_next(fin_2)
          cont_next(fin_2)=contour_courant
          fin_2=contour_courant
        else                   !allocation d'un triplet nul...
          deb_2=contour_courant
          fin_2=contour_courant
          call get_triplet(cont_i,2,error)
          if (error) return
          cont_next(fin_2)=cont_i
          cont_next(cont_i)=0
        endif
      else                     !type 4
        n4=n4+1
        if (fin_4.ne.0) then   !deb_4 .ne. 0 aussi
          cont_next(contour_courant)=cont_next(fin_4)
          cont_next(fin_4)=contour_courant
          fin_4=contour_courant
        else                   !allocation d'un triplet nul...
          deb_4=contour_courant
          fin_4=contour_courant
          call get_triplet(cont_i,2,error)
          if (error) return
          cont_next(fin_4)=cont_i
          cont_next(cont_i)=0
        endif
      endif
    else                       !cat 3
      ! type 5, fin pas sur le bord pour un type 3 a priori !!!
      if (vect_x(cont_end(contour_courant)).ne.xmin .and.  &
          vect_x(cont_end(contour_courant)).ne.xmax .and.  &
          vect_y(cont_end(contour_courant)).ne.ymin .and.  &
          vect_y(cont_end(contour_courant)).ne.ymax) then
        ! on le vire et un peu vite!!!
        if (contour_precedent.eq.0) then
          leaf_start(current_leaf)=cont_next(contour_courant)
        else
          cont_next(contour_precedent)=contour_suivant
        endif
      else
        n3=n3+1
        if (fin_3.ne.0) then   !deb_3 .ne. 0 aussi
          cont_next(contour_courant)=cont_next(fin_3)
          cont_next(fin_3)=contour_courant
          fin_3=contour_courant
        else                   !allocation d'un triplet nul...
          deb_3=contour_courant
          fin_3=contour_courant
          call get_triplet(cont_i,2,error)
          if (error) return
          cont_next(fin_3)=cont_i
          cont_next(cont_i)=0
        endif
      endif
    endif
    contour_precedent=contour_courant
    contour_courant = contour_suivant  !restitue le pointeur
  enddo
  ! conversion des categories 2 + 3 en une categorie 4:
  if (deb_2.ne.0.and.deb_3.ne.0) then  ! 2 et 3 existent...
    contour_2 = deb_2
    prev_2 = 0
    do while(cont_next(contour_2).ne.0)
      prev_3    = 0
      contour_3 = deb_3
      found=.false.
      do while(cont_next(contour_3).ne.0)
        ! on devrait essayer de sortir avant...
        if (vect_x(cont_start(contour_3)).eq.vect_x(cont_end(contour_2)) .and. &
            vect_y(cont_start(contour_3)).eq.vect_y(cont_end(contour_2)) ) then
          found=.true.
          vect_next(cont_end(contour_2)) = vect_next(cont_start(contour_3))
          cont_end(contour_2) = cont_end(contour_3)
          ! chainage de contour_3 pour "sauter" le trou
          if (prev_3.ne.0) then
            ! et prev_3 inchange...
            cont_next(prev_3) = cont_next(contour_3)
          else
            deb_3=cont_next(contour_3)
            prev_3 = 0         ! pour sur...
          endif
        else
          prev_3 = contour_3   !incremente prev_3
        endif
        contour_3=cont_next(contour_3)
      enddo
      if (.not.found) then
        ! chainage de contour_2 pour "sauter" un contour 2 n'ayant pas de
        ! petit frere (!)
        if (prev_2.ne.0) then
          ! et prev_2 inchange...
          cont_next(prev_2) = cont_next(contour_2)
        else
          deb_2=cont_next(contour_2)
          prev_2 = 0           ! pour sur...
        endif
      else
        prev_2=contour_2
      endif
      contour_2=cont_next(contour_2)
    enddo
    ! ici les 2 sont devenus des 4 : on les chaine en groupe a 4
    ! et les 3 ont disparu..
    deb_3 = 0
    fin_3 = 0
    if (deb_4.eq.0) then       !pas de 4 : 2=4 par jeu de pointeurs
      deb_4 = deb_2
      fin_4 = fin_2
      deb_2 = 0
      fin_2 = 0
    else                       !insere 2 derriere 4:
      cont_next(fin_4)=deb_2
      fin_4 = fin_2
      fin_2 = 0
      deb_2 = 0
    endif
  endif
  ! conversion cat 4 en une cat 1. On appelle une subroutine speciale
  ! qui donne la longueur sur le contour oriente: distance.
  ! Addr4(n4) contient les adresses des debuts de contours de 4
  ! Ldeb4(n4) contient la distance (orientee => modulo!)
  !           des debuts de contour correspondants
  ! Lfin4(n4) contient la distance (orientee => modulo!)
  !           des fins de contour correspondants
  ! On concatene la fin d'un contour avec le contour suivant le plus
  !           proche au sens de la relation d'ordre sur le bord
  !
  if (deb_4.ne.0) then         !remplissage addr4,Ldeb4,Lfin4
    n4=0
    contour_courant=deb_4
    do while(cont_next(contour_courant).ne.0)
      n4=n4+1
      addr4(n4) = contour_courant
      ldeb4(n4) = distance(vect_x(cont_start(contour_courant)),  &
        vect_y(cont_start(contour_courant)),xmin,xmax,ymin,ymax,trigo)
      lfin4(n4) = distance(vect_x(cont_end(contour_courant)),  &
        vect_y(cont_end(contour_courant)),xmin,xmax,ymin,ymax,trigo)
      contour_courant=cont_next(contour_courant)
    enddo
    !
    ! n4 est le nombre total de contours de 4; on boucle
    ! betement pour trouver le "plus proche"
    ltot = 2*(xmax-xmin+ymax-ymin)
    do while (n4.gt.0)
      ! contour_courant = addr4(1). il a une fin en Lfin(1). on cherche
      ! le suivant des Ldeb4(1:n4). c'est le point qui est le + proche
      ! de Lfin4 entre Lfin4(1) et Ldeb4(1) si Lfin4 est < Ldeb4, sinon c'est
      ! le pt le plus proche de Lfin4 ds l'intervalle Lfin4-ltot(longueur
      ! totale du contour) et 0-Ldeb4 (passage par 0)
      nn=2
      if (lfin4(1).le.ldeb4(1)) then
        nsuiv=1
        do while(nn.le.n4)
          if (ldeb4(nn).le.ldeb4(1) .and. ldeb4(nn).ge.lfin4(1)) then
            if (ldeb4(nn).lt.ldeb4(nsuiv)) then
              nsuiv=nn
            endif
          endif
          nn=nn+1
        enddo
      else
        nsuiv=1
        lref=ldeb4(1)+ltot-lfin4(1)    !changement de repere...
        do while(nn.le.n4)
          if (ldeb4(nn).ge.lfin4(1) .or. ldeb4(nn).le.ldeb4(1)) then
            ! il est candidat au titre
            if (ldeb4(nn).ge.lfin4(1))  ll=ldeb4(nn)-lfin4(1)
            if (ldeb4(nn).le.ldeb4(1))  ll=ldeb4(nn)+ltot-lfin4(1)
            if (ll.lt.lref) then
              nsuiv=nn
              lref=ll
            endif
          endif
          nn=nn+1
        enddo
      endif
      !
      ! donc nsuiv pointe sur le suivant: on concatene par Bord, et
      ! on fait le menage dans le tableau
      ! a-t'on ferme le contour ? (cad nsuiv=1)
      contour_courant=addr4(1)
      if (nsuiv.eq.1) then     !on a un type 1
        call bord(vect_x(cont_end(addr4(1))),  vect_y(cont_end(addr4(1))),  &
                  vect_x(cont_start(addr4(1))),vect_y(cont_start(addr4(1))),  &
                  xmin,xmax,ymin,ymax,trigo,tx,ty,nt)
        if (nt.ne.0) then
          do i=2,nt
            nvect = nvect + 1  !mise a jour nvect
            vecteur_courant=vect_next(cont_end(contour_courant))
            vect_x(vecteur_courant)=tx(i)
            vect_y(vecteur_courant)=ty(i)
            call get_triplet(vect_i,3,error)
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            cont_end(contour_courant)=vecteur_courant
          enddo
        endif
        ! passage en fin de 1
        if (deb_1.ne.0) then
          cont_next(contour_courant)=cont_next(fin_1)
          cont_next(fin_1)=contour_courant
          fin_1=contour_courant
        else
          deb_1=contour_courant
          fin_1=contour_courant
          call get_triplet(cont_i,2,error)
          if (error) return
          cont_next(fin_1)=cont_i
          cont_next(cont_i)=0
        endif
      else                     !il faut fermer sur le bord.
        ! on met a jour Lfin4(1):
        lfin4(1)=lfin4(nsuiv)
        ! on appelle Bord depuis 1 vers nsuiv
        call bord(vect_x(cont_end(addr4(1))),vect_y(cont_end(addr4(1))),    &
        vect_x(cont_start(addr4(nsuiv))),vect_y(cont_start(addr4(nsuiv))),  &
        xmin,xmax,ymin,ymax,trigo,tx,ty,nt)
        vect_next(cont_end(addr4(1)))=vect_next(cont_end(addr4(1)))
        if (nt.ne.0) then
          do i=2,nt-1
            nvect = nvect + 1  !mise a jour nvect
            vecteur_courant=vect_next(cont_end(contour_courant))
            vect_x(vecteur_courant)=tx(i)
            vect_y(vecteur_courant)=ty(i)
            call get_triplet(vect_i,3,error)
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            cont_end(contour_courant)=vecteur_courant
          enddo
          vect_next(cont_end(contour_courant))=cont_start(addr4(nsuiv))
          ! concatenation complete:
          cont_end(contour_courant)=cont_end(addr4(nsuiv))
        else
          ! enlever les points redondants, puisque la fin de l'un est le debut
          !  de l'autre
          vect_next(cont_end(contour_courant)) =  &
            vect_next(cont_start(addr4(nsuiv)))
          ! concatenation complete...
          cont_end(contour_courant)=cont_end(addr4(nsuiv))
        endif
      endif
      do i=nsuiv,n4-1          !on vire nsuiv de la liste:
        addr4(i)=addr4(i+1)
        ldeb4(i)=ldeb4(i+1)
        lfin4(i)=lfin4(i+1)
      enddo
      n4=n4-1                  !on diminue la liste de 1...
    enddo
  endif
  !
  ! il faut caracteriser la cat 1, seule en lice. cad: la boite
  ! est la boite totale (xx1,xx2,yy1,yy2) et on doit examiner le sens
  ! de rotation d'un contour touchant le bord.
  ! Test de la couleur (signe) de l'exterieur de la carte
  ! hypothese : la couleur de l'exterieur d'un contour est donnee par le
  ! signe de xx1yy2-xx2yy1, ou xx1,yy1 sont les coordonnees du 1er point
  ! de l'ensemble des contours (strictement interieur a la boite
  ! dans notre cas,
  ! puisque cat 4 = 0) qui TOUCHE la "boite" min-max
  !
  if (caracterize.and.deb_4.eq.0) then
    ! assure qu'il n'y avait que des categories 1
    call gris_minmax(current_leaf)
    call info_read(current_leaf,nvect,xmin,ymin,xmax,ymax)
    outgrey = .false.          !most probable for an inner contour
    tested  = .false.
    !SG! Print *,'TRIGO ',trigo
    contour_courant = deb_1    !debut_feuille(current_leaf)
    do while (cont_next(contour_courant).ne.0.and..not.tested)
      vecteur_courant = cont_start(contour_courant)
      !SG! print *,vect_x(1:15)
      do while (vect_next(vect_next(vecteur_courant)).ne.0 .and. .not.tested)
        temp= vect_next(vecteur_courant)
        !SG! print *,'X,Y ',temp,vect_x(temp),vect_y(temp)
        !SG! print *,'next X,Y ',vect_next(temp),vect_x(vect_next(temp)),vect_y(vect_next(temp))
        !SG! Print *,'Vect_next ',temp,vect_next(temp),vect_next(vect_next(temp)), vect_i
        if (vect_x(temp).eq.xmin .or.  &
            vect_x(temp).eq.xmax .or.  &
            vect_y(temp).eq.ymin .or.  &
            vect_y(temp).eq.ymax) then
          temp= vect_next(vecteur_courant)
          !
          ! The last vector may be uninitialized...
          if (vect_x(vect_next(temp)).eq.vect_null .and. &
            & vect_y(vect_next(temp)).eq.vect_null) then
            ! Note S.Guilloteau 19-Aug-2014
            ! Strange case where the next vector is actually not existent
            continue
          else
            vx1 = vect_x(temp)-vect_x(vecteur_courant)
            vy1 = vect_y(temp)-vect_y(vecteur_courant)
            vx2 = vect_x(vect_next(temp))-vect_x(temp)
            vy2 = vect_y(vect_next(temp))-vect_y(temp)
            ! ca peut ne pas marcher si il y a 2 pts confondus sur
            ! TOUS les bords...
            val = vx1*vy2-vy1*vx2
            if (val.ne.0.0) then            
              !SG! Print *,'Contour courant ',contour_courant
              outgrey = ((val.lt.0.0.and.trigo).or.(val.gt.0.0.and..not.trigo))
              tested = .true.
              !SG! Print *,'Min Max ',xmin,xmax,ymin,ymax
              !SG! Print *,'VAL ',val,vx1,vx2,vy1,vy2
              !SG! Print *,'Vect_next ',temp,vect_next(temp), vect_i
              !SG! Print *,'Vect X ',vect_x(temp),vect_x(vecteur_courant),vect_x(vect_next(temp))
              !SG! Print *,'Vect Y ',vect_y(temp),vect_y(vecteur_courant),vect_y(vect_next(temp))
            endif
          endif
        endif
        vecteur_courant = vect_next(vecteur_courant)
      enddo
      contour_courant = cont_next(contour_courant)
    enddo
    ! dans ce cas, on rajoute le "bord" en tant que contour
    ! si l'exterieur est gris...
    if (outgrey.and.tested) then
      !SG! Print *,'Adding the outside ',outgrey,tested
      xmin=min(xxbox1,xxbox2)
      xmax=max(xxbox1,xxbox2)
      ymin=min(yybox1,yybox2)
      ymax=max(yybox1,yybox2)
      contour_courant=cont_next(fin_1) !fin_feuille(current_leaf)
      call get_triplet(cont_i,2,error)
      if (error) return
      cont_next(contour_courant)=cont_i
      cont_next(cont_i)=0
      fin_1=contour_courant    !fin_feuille(current_leaf)
      call get_triplet(vect_i,3,error)
      if (error) return
      cont_start(contour_courant)=vect_i
      vect_x(vect_i)=xmin
      vect_y(vect_i)=ymin
      nn=vect_i
      call get_triplet(vect_i,3,error)
      if (error) return
      vect_next(nn)=vect_i
      if (trigo) vect_x(vect_i)=xmax
      if (trigo) vect_y(vect_i)=ymin
      if (.not.trigo) vect_x(vect_i)=xmin
      if (.not.trigo) vect_y(vect_i)=ymax
      nn=vect_i
      call get_triplet(vect_i,3,error)
      if (error) return
      vect_next(nn)=vect_i
      vect_x(vect_i)=xmax
      vect_y(vect_i)=ymax
      nn=vect_i
      call get_triplet(vect_i,3,error)
      if (error) return
      vect_next(nn)=vect_i
      if (trigo) vect_x(vect_i)=xmin
      if (trigo) vect_y(vect_i)=ymax
      if (.not.trigo) vect_x(vect_i)=xmax
      if (.not.trigo) vect_y(vect_i)=ymin
      nn=vect_i
      call get_triplet(vect_i,3,error)
      if (error) return
      vect_next(nn)=vect_i
      vect_x(vect_i)=xmin
      vect_y(vect_i)=ymin
      nvect = nvect + 5        !mise a jour nvect
      nn=vect_i
      cont_end(contour_courant)=vect_i
      call get_triplet(vect_i,3,error)
      if (error) return
      vect_next(nn)=vect_i
      vect_next(vect_i)=0
    endif
  endif
  leaf_start(current_leaf)=deb_1   !remet pendule a l'heure
  leaf_end(current_leaf)=fin_1
  !
  ! ici 1 feuille de Nvect vecteurs dans boite xmin,ymin,xmax,ymax.
  ! On ecrit ces valeurs dans info_feuille
  call info_write(current_leaf,nvect,xmin,ymin,xmax,ymax)
end subroutine find_border
!
function distance(xb,yb,xmin,xmax,ymin,ymax,trigo)
  !---------------------------------------------------------------------
  ! @ private
  ! Calcule une distance sur le bord oriente entre xmin,ymin et xb,yb
  !
  !	Xb, Yb		Debut du contour
  !	Xmin, Ymin, ...	Valeur des bords
  !	Trigo		Logique, sens rotation trigo ou non.
  !---------------------------------------------------------------------
  integer :: distance               !
  integer :: xb                     !
  integer :: yb                     !
  integer :: xmin                   !
  integer :: xmax                   !
  integer :: ymin                   !
  integer :: ymax                   !
  logical :: trigo                  !
  ! Local
  integer :: l,xl,yl
  !
  ! calcul explicite distance suivant 4 cas :
  l = 0
  xl = xmax-xmin
  yl = ymax-ymin
  if (trigo) then
    if (yb.eq.ymin) then
      l = xb-xmin
    elseif (xb.eq.xmax) then
      l = xl + yb-ymin
    elseif (yb.eq.ymax) then
      l = xl + yl + xmax-xb
    else
      l = 2*xl + yl + ymax-yb
    endif
  else
    if (xb.eq.xmin) then
      l = yb-ymin
    elseif (yb.eq.ymax) then
      l = yl + xb-xmin
    elseif (xb.eq.xmax) then
      l = yl + xl + ymax-yb
    else
      l = 2*yl +xl +xmax-xb
    endif
  endif
  distance = l
end function distance
!
subroutine coupe_feuille(feuille,force,x,y,error)
  use greg_interfaces, except_this=>coupe_feuille
  use gbl_message
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  !	coupe une feuille en 2 feuilles + 1 noeud
  !---------------------------------------------------------------------
  integer :: feuille                !
  logical :: force                  !
  integer :: x                      !
  integer :: y                      !
  logical :: error                  !
  ! Local
  ! cense representer la feuille de l'arbre des pages de contours
  integer :: nvectd,nvectg
  integer :: current_leaf
  integer :: feuille_precedente
  integer :: sauvegarde_fin_feuille
  ! chaine pointee de contours (debut et fin des susdits)
  ! au debut: ils ne se ferment pas...
  integer :: contour_courant,contour_precedent
  integer :: prochain_contour,sauve_cont
  ! Chaine pointee de vecteurs
  integer :: temp
  integer :: vecteur_courant,prochain_vecteur
  ! max et min apres coupure
  integer ::  xming,yming,xmaxg,ymaxg,xmind,ymind,xmaxd,ymaxd
  integer ::  xsect,ysect,xt1,yt1,xt2,yt2
  integer :: nvect,xmin,xmax,ymin,ymax
  ! divers
  logical :: g,cutx
  !
  error = .false.
  current_leaf=feuille
  call info_read(current_leaf,nvect,xmin,ymin,xmax,ymax)
  ! Par def, G est la partie haute (valeurs elevees)
  ! et D (.not.G) l'autre partie .
  ! A ces deux sous-feuilles correspondent des
  ! bornes xminG,xmaxG,yminG,ymaxG etc...
  if (((xmax-xmin).le.2).and.((ymax-ymin).le.2) ) then
    call greg_message(seve%e,'GREY','Internal error number 1')
    error = .true.
    return
  endif
  if (xmax-xmin.ge.ymax-ymin) then
    cutx=.true.
    xmaxg=xmax
    xmind=xmin
    xming=(xmax+xmin)/2
    if (force) xming=x
    xmaxd=xming
    ymaxg=ymax
    yming=ymin
    ymaxd=ymax
    ymind=ymin
  else
    cutx=.false.
    ymaxg=ymax
    ymind=ymin
    yming=(ymax+ymin)/2
    if (force) yming=y
    ymaxd=yming
    xmaxg=xmax
    xming=xmin
    xmaxd=xmax
    xmind=xmin
  endif
  ! coupure feuille en 2: on initialise 1 noeud + 2 feuilles
  ! a partir de la feuille courante:
  call get_triplet(leaf_i,1,error)
  if (error) return
  leaf_start(leaf_i)=leaf_start(current_leaf)
  ! information transmise en f (feuille Gauche)
  leaf_end(leaf_i)=leaf_end(current_leaf)
  leaf_info(leaf_i)=leaf_info(current_leaf)
  ! - car feuille et non noeud
  leaf_mother(leaf_i)= -current_leaf
  ! devient 1  noeud
  leaf_mother(current_leaf)= -leaf_mother(current_leaf)
  leaf_start(current_leaf)=leaf_i  !G est reperee
  ! G est definie et les contours de Feuille_mere lui sont appliques
  current_leaf = leaf_i    !on peut retrouver la feuille mere...
  ! on est sur la feuille G (pour l'instant...)
  g = .true.
  nvectg=0
  call get_triplet(leaf_i,1,error) !pour la feuille D
  if (error) return
  leaf_end(-(leaf_mother(current_leaf)))=leaf_i    ! eh oui...
  leaf_mother(leaf_i)=leaf_mother(current_leaf)
  ! permettra de passer de G a D et versa vice
  feuille_precedente=leaf_i
  ! on alloue un triplet k vide pour D, et un vecteur vide n:
  call get_triplet(cont_i,2,error)
  if (error) return
  leaf_start(feuille_precedente)=cont_i
  leaf_end(feuille_precedente)=0   !vide au debut...
  cont_next(leaf_start(feuille_precedente))=0
  ! n
  nvectd=0
  call info_create(feuille_precedente,0,0,0,0,0,error)
  if (error) return
  call get_triplet(vect_i,3,error)
  if (error) return
  cont_start(leaf_start(feuille_precedente))=vect_i
  cont_end(leaf_start(feuille_precedente))=0
  vect_next(cont_start(leaf_start(feuille_precedente)))=0
  ! en place pour le quadrille
  ! a chaque debut de contour, on fait en sorte d'etre dans la "bonne" feuille
  contour_courant = leaf_start(current_leaf)   !G au depart
  contour_precedent = 0
  do while(cont_next(contour_courant).ne.0)
    !
    ! examinons si le 1er contour est dans la "bonne" feuille
    vecteur_courant=cont_start(contour_courant)
    ! il l'est :
    !    1) si G et vecteur > limite ou si (.not.G) et vecteur< limite
    !    2) dans le cas ou le vecteur de debut de contour est exactement a
    !       la limite: appliquer 1) et 2) au vecteur suivant, etc..
    do while ((cutx .and. ((g.and.vect_x(vecteur_courant).eq.xming) .or.  &
                      (.not.g.and.vect_x(vecteur_courant).eq.xmaxd)))     &
                                   .or.                                   &
         (.not.cutx .and. ((g.and.vect_y(vecteur_courant).eq.yming) .or.  &
                      (.not.g.and.vect_y(vecteur_courant).eq.ymaxd)) ) )
      ! peut crasher sur la fin de contour dans des cas vicieux...
      vecteur_courant = vect_next(vecteur_courant)
      if (vecteur_courant.eq.0) then
        contour_courant=cont_next(contour_courant)
        if (contour_precedent.ne.0) then
          cont_next(contour_precedent)=contour_courant
        else
          leaf_start(current_leaf) = contour_courant
        endif
        vecteur_courant=cont_start(contour_courant)
      endif
    enddo
    if (    (cutx .and. ((g.and.vect_x(vecteur_courant).lt.xming) .or.  &
                    (.not.g.and.vect_x(vecteur_courant).gt.xmaxd)))     &
                                 .or.                                   &
       (.not.cutx .and. ((g.and.vect_y(vecteur_courant).lt.yming) .or.  &
                    (.not.g.and.vect_y(vecteur_courant).gt.ymaxd))) ) then
      ! on change: on passe tout sur l'autre feuille
      if (contour_precedent .ne. 0) then
        if (leaf_end(feuille_precedente).ne.0) then
          cont_next(contour_precedent)=cont_next(leaf_end(feuille_precedente))
          cont_next(leaf_end(feuille_precedente))=contour_courant
        else
          cont_next(contour_precedent) = leaf_start(feuille_precedente)
          leaf_start(feuille_precedente) = contour_courant
        endif
        temp=leaf_end(feuille_precedente)  !temporary storage
        leaf_end(feuille_precedente) = leaf_end(current_leaf)
        leaf_end(current_leaf) = contour_precedent
        contour_precedent = temp   !peut etre 0
        temp=feuille_precedente
        feuille_precedente=current_leaf
        current_leaf=temp
      else
        ! G <-> D au niveau des feuilles...
        temp=leaf_end(-(leaf_mother(current_leaf)))
        leaf_end(-(leaf_mother(current_leaf))) =  &
          leaf_start(-(leaf_mother(current_leaf)))
        leaf_start(-(leaf_mother(current_leaf))) = temp
      endif
      g = .not.g
    endif
    ! on est dans la bonne feuille, le bon contours, etc...
    sauvegarde_fin_feuille=leaf_end(current_leaf)
    leaf_end(current_leaf)=contour_courant
    sauve_cont=cont_end(contour_courant)
    prochain_contour=cont_next(contour_courant)    !servira a la fin
    vecteur_courant=cont_start(contour_courant)    !bis repetita placent
    do while (vect_next(vecteur_courant).ne.0)
      ! on debobine jusqu'a ce qu'un vecteur coupe la frontiere
      if (cutx) then
        if (g) then
          ! par construction, le vecteur_courant est toujours du bon cote
          nvectg=nvectg+1
          if (vect_next(vect_next(vecteur_courant)).ne.0 .and.  &
              vect_x(vect_next(vecteur_courant)).lt.xming) then
            ! on calcule le point d'intersection
            xt1=vect_x(vecteur_courant)
            yt1=vect_y(vecteur_courant)
            xt2=vect_x(vect_next(vecteur_courant))
            yt2=vect_y(vect_next(vecteur_courant))
            xsect=xming
            if (xt2.ne.xt1) then
              ysect=yt1+(yt2-yt1)*(xsect-xt1)/(xt2-xt1)
            else
              ysect=yt1
            endif
            ! on finit le contour courant sur l'intersection
            call get_triplet(vect_i,3,error)
            if (error) return
            ! sauve l'ancien successeur
            prochain_vecteur=vect_next(vecteur_courant)
            vect_next(vecteur_courant)=vect_i
            vecteur_courant=vect_next(vecteur_courant)
            vect_x(vecteur_courant)=xsect
            vect_y(vecteur_courant)=ysect
            nvectg=nvectg+1
            call get_triplet(vect_i,3,error)   !dernier vecteur du contour
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0    !fin du contour
            cont_end(contour_courant) = vecteur_courant
            leaf_end(current_leaf) = contour_courant
            ! on a fini pour ce contour; on passe sur l'autre feuille
            temp=current_leaf
            current_leaf=feuille_precedente
            feuille_precedente=temp
            if (leaf_end(current_leaf).ne.0) then  !etat normal
              leaf_end(current_leaf) = cont_next(leaf_end(current_leaf))
            else
              leaf_end(current_leaf) = leaf_start(current_leaf)
            endif
            contour_courant=leaf_end(current_leaf)
            g = .not.g
            ! il faut creer un nouveau contour sur l'autre feuille
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0
            ! allocation 1er vecteur
            call get_triplet(vect_i,3,error)
            if (error) return
            cont_start(contour_courant)=vect_i
            cont_end(contour_courant)=vect_i
            nvectd=nvectd+1
            vect_x(cont_end(contour_courant)) = xsect
            vect_y(cont_end(contour_courant)) = ysect
            vecteur_courant=prochain_vecteur
            vect_next(cont_end(contour_courant)) = vecteur_courant
            cont_end(contour_courant)=sauve_cont
            vecteur_courant=cont_start(contour_courant)
          endif
        else
          nvectd=nvectd+1
          if (vect_next(vect_next(vecteur_courant)).ne.0 .and.  &
              vect_x(vect_next(vecteur_courant)).gt.xmaxd) then
            xt1=vect_x(vecteur_courant)
            yt1=vect_y(vecteur_courant)
            xt2=vect_x(vect_next(vecteur_courant))
            yt2=vect_y(vect_next(vecteur_courant))
            xsect=xmaxd
            if (xt2.ne.xt1) then
              ysect=yt1+(yt2-yt1)*(xsect-xt1)/(xt2-xt1)
            else
              ysect=yt1
            endif
            ! on finit le contour courant sur l'intersection
            call get_triplet(vect_i,3,error)
            if (error) return
            ! sauve l'ancien successeur
            prochain_vecteur=vect_next(vecteur_courant)
            vect_next(vecteur_courant)=vect_i
            vecteur_courant=vect_next(vecteur_courant)
            vect_x(vecteur_courant)=xsect
            vect_y(vecteur_courant)=ysect
            nvectd=nvectd+1
            call get_triplet(vect_i,3,error)   !dernier vecteur du contour
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0    !fin du contour
            cont_end(contour_courant) = vecteur_courant
            leaf_end(current_leaf) = contour_courant
            ! on a fini pour ce contour; on passe sur l'autre feuille
            temp=current_leaf
            current_leaf=feuille_precedente
            feuille_precedente=temp
            if (leaf_end(current_leaf).ne.0) then  !etat normal
              leaf_end(current_leaf) = cont_next(leaf_end(current_leaf))
            else
              leaf_end(current_leaf) = leaf_start(current_leaf)
            endif
            contour_courant=leaf_end(current_leaf)
            g = .not.g
            ! il faut creer un nouveau contour sur l'autre feuille
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0
            ! allocation 1er vecteur
            call get_triplet(vect_i,3,error)
            if (error) return
            cont_start(contour_courant)=vect_i
            cont_end(contour_courant)=vect_i
            nvectg=nvectg+1
            vect_x(cont_end(contour_courant)) = xsect
            vect_y(cont_end(contour_courant)) = ysect
            vecteur_courant=prochain_vecteur
            vect_next(cont_end(contour_courant)) = vecteur_courant
            cont_end(contour_courant)=sauve_cont
            vecteur_courant=cont_start(contour_courant)
          endif
        endif
      else
        if (g) then
          ! par construction, le vecteur_courant est toujours du bon cote
          nvectg=nvectg+1
          if (vect_next(vect_next(vecteur_courant)).ne.0 .and.  &
              vect_y(vect_next(vecteur_courant)).lt.yming) then
            xt1=vect_x(vecteur_courant)
            yt1=vect_y(vecteur_courant)
            xt2=vect_x(vect_next(vecteur_courant))
            yt2=vect_y(vect_next(vecteur_courant))
            ysect=yming
            if (yt2.ne.yt1) then
              xsect=xt1+(xt2-xt1)*(ysect-yt1)/(yt2-yt1)
            else
              xsect=xt1
            endif
            ! on finit le contour courant sur l'intersection
            call get_triplet(vect_i,3,error)
            if (error) return
            ! sauve l'ancien successeur
            prochain_vecteur=vect_next(vecteur_courant)
            vect_next(vecteur_courant)=vect_i
            vecteur_courant=vect_next(vecteur_courant)
            vect_x(vecteur_courant)=xsect
            vect_y(vecteur_courant)=ysect
            nvectg=nvectg+1
            call get_triplet(vect_i,3,error)   !dernier vecteur du contour
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0    !fin du contour
            cont_end(contour_courant) = vecteur_courant
            leaf_end(current_leaf) = contour_courant
            ! on a fini pour ce contour; on passe sur l'autre feuille
            temp=current_leaf
            current_leaf=feuille_precedente
            feuille_precedente=temp
            if (leaf_end(current_leaf).ne.0) then  !etat normal
              leaf_end(current_leaf) = cont_next(leaf_end(current_leaf))
            else
              leaf_end(current_leaf) = leaf_start(current_leaf)
            endif
            contour_courant=leaf_end(current_leaf)
            g = .not.g
            ! il faut creer un nouveau contour sur l'autre feuille
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0
            ! allocation 1er vecteur
            call get_triplet(vect_i,3,error)
            if (error) return
            cont_start(contour_courant)=vect_i
            cont_end(contour_courant)=vect_i
            nvectd=nvectd+1
            vect_x(cont_end(contour_courant)) = xsect
            vect_y(cont_end(contour_courant)) = ysect
            vecteur_courant=prochain_vecteur
            vect_next(cont_end(contour_courant)) = vecteur_courant
            cont_end(contour_courant)=sauve_cont
            vecteur_courant=cont_start(contour_courant)
          endif
        else
          nvectd=nvectd+1
          if (vect_next(vect_next(vecteur_courant)).ne.0 .and.  &
              vect_y(vect_next(vecteur_courant)).gt.ymaxd) then
            xt1=vect_x(vecteur_courant)
            yt1=vect_y(vecteur_courant)
            xt2=vect_x(vect_next(vecteur_courant))
            yt2=vect_y(vect_next(vecteur_courant))
            ysect=ymaxd
            if (yt2.ne.yt1) then
              xsect=xt1+(xt2-xt1)*(ysect-yt1)/(yt2-yt1)
            else
              xsect=xt1
            endif
            ! on finit le contour courant sur l'intersection
            call get_triplet(vect_i,3,error)
            if (error) return
            ! sauve l'ancien successeur
            prochain_vecteur=vect_next(vecteur_courant)
            vect_next(vecteur_courant)=vect_i
            vecteur_courant=vect_next(vecteur_courant)
            vect_x(vecteur_courant)=xsect
            vect_y(vecteur_courant)=ysect
            nvectd=nvectd+1
            call get_triplet(vect_i,3,error)   !dernier vecteur du contour
            if (error) return
            vect_next(vecteur_courant)=vect_i
            vect_next(vect_i)=0
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0    !fin du contour
            cont_end(contour_courant) = vecteur_courant
            leaf_end(current_leaf) = contour_courant
            ! on a fini pour ce contour; on passe sur l'autre feuille
            temp=current_leaf
            current_leaf=feuille_precedente
            feuille_precedente=temp
            if (leaf_end(current_leaf).ne.0) then  !etat normal
              leaf_end(current_leaf) = cont_next(leaf_end(current_leaf))
            else
              leaf_end(current_leaf) = leaf_start(current_leaf)
            endif
            contour_courant=leaf_end(current_leaf)
            g = .not.g
            ! il faut creer un nouveau contour sur l'autre feuille
            call get_triplet(cont_i,2,error)
            if (error) return
            cont_next(contour_courant)=cont_i
            cont_next(cont_i)=0
            ! allocation 1er vecteur
            call get_triplet(vect_i,3,error)
            if (error) return
            cont_start(contour_courant)=vect_i
            cont_end(contour_courant)=vect_i
            nvectg=nvectg+1
            vect_x(cont_end(contour_courant)) = xsect
            vect_y(cont_end(contour_courant)) = ysect
            vecteur_courant=prochain_vecteur
            vect_next(cont_end(contour_courant)) = vecteur_courant
            cont_end(contour_courant)=sauve_cont
            vecteur_courant=cont_start(contour_courant)
          endif
        endif
      endif
      vecteur_courant=vect_next(vecteur_courant)
    enddo
    ! remettre les pendules a l'heure
    contour_precedent=contour_courant
    cont_next(contour_courant) = prochain_contour
    contour_courant=cont_next(contour_courant)
    leaf_end(current_leaf)=sauvegarde_fin_feuille
  enddo
  ! current_leaf est G ou .not.G
  !      CALL GRIS_MINMAX(current_leaf)
  !      CALL GRIS_MINMAX(FEUILLE_PRECEDENTE)
  if (g) then
    xmax=xmaxg
    xmin=xming
    ymax=ymaxg
    ymin=yming
    call info_write(current_leaf,nvectg,xmin,ymin,xmax,ymax)
    xmax=xmaxd
    xmin=xmind
    ymax=ymaxd
    ymin=ymind
    call info_write(feuille_precedente,nvectd,xmin,ymin,xmax,ymax)
  else
    xmax=xmaxg
    xmin=xming
    ymax=ymaxg
    ymin=yming
    call info_write(feuille_precedente,nvectg,xmin,ymin,xmax,ymax)
    xmax=xmaxd
    xmin=xmind
    ymax=ymaxd
    ymin=ymind
    call info_write(current_leaf,nvectd,xmin,ymin,xmax,ymax)
  endif
  ! si par hasard une feuille est vide (c'est "feuille_precedente"),
  !   on la vire
  call info_read(feuille_precedente,nvect,xmin,ymin,xmax,ymax)
  if (nvect.eq.0) then
    ! write(*,*) 'feuille vide @ ', feuille_precedente
    feuille_precedente=abs(leaf_mother(current_leaf))
    leaf_start(feuille_precedente)  =  leaf_start(current_leaf)
    leaf_end(feuille_precedente)    =  leaf_end(current_leaf)
    leaf_info(feuille_precedente)   =  leaf_info(current_leaf)
    leaf_mother(feuille_precedente) = -leaf_mother(feuille_precedente)
  endif
  ! fermeture des contours,pas de caracterisation
  call find_border(current_leaf,.false.,error)
  if (error) return
  call find_border(feuille_precedente,.false.,error)
  if (error) return
end subroutine coupe_feuille
!
subroutine menage(feuille)
  use greg_interfaces, except_this=>menage
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! This routine cleans the contours, first by removing consecutive,
  ! identical points, then (parts of) contour that do not close any
  ! surface (i.e, line segments), then contours of .le. two points.
  !---------------------------------------------------------------------
  integer :: feuille                !
  ! Local
  integer :: current_leaf
  integer :: contour_courant,contour_precedent
  logical :: start,repli
  integer :: x,y,ox,oy,tmp,vecteur_courant,vecteur_precedent
  !
  current_leaf=feuille
  ! suppresses copies of same point(s)
  contour_courant = leaf_start(current_leaf)
  do while (cont_next(contour_courant).ne.0)
    vecteur_courant = cont_start(contour_courant)
    ox=vect_x(vecteur_courant)
    oy=vect_y(vecteur_courant)
    vecteur_precedent=vecteur_courant
    vecteur_courant=vect_next(vecteur_courant)
    do while (vect_next(vecteur_courant).ne.0)
      x = vect_x(vecteur_courant)
      y = vect_y(vecteur_courant)
      if (x.eq.ox.and.y.eq.oy) then
        vect_next(vecteur_precedent)=vect_next(vecteur_courant)
      else
        ox=x
        oy=y
        vecteur_precedent=vecteur_courant
      endif
      vecteur_courant = vect_next(vecteur_courant)
    enddo
    cont_end(contour_courant)=vecteur_precedent
    contour_courant = cont_next(contour_courant)
  enddo
  ! drops parts of contour of null surface.
  contour_courant = leaf_start(current_leaf)
  do while (cont_next(contour_courant).ne.0)
    repli=.true.
    do while(repli)
      vecteur_courant = cont_start(contour_courant)
      vecteur_precedent=vecteur_courant
      ox=vect_x(vecteur_courant)
      oy=vect_y(vecteur_courant)
      vecteur_courant=vect_next(vecteur_courant)
      repli=.false.
      do while (vect_next(vecteur_courant).ne.0)
        tmp=vect_next(vecteur_courant)
        if(vect_next(tmp).ne.0)then
          x = vect_x(tmp)
          y = vect_y(tmp)
          if (x.eq.ox.and.y.eq.oy) then
            repli=.true.
            vect_next(vecteur_precedent)=vect_next(tmp)
            vecteur_courant = vect_next(vecteur_precedent)
          else
            ox=vect_x(vecteur_courant)
            oy=vect_y(vecteur_courant)
            vecteur_precedent=vecteur_courant
            vecteur_courant = vect_next(vecteur_courant)
          endif
        else
          vecteur_courant = vect_next(vecteur_courant)
        endif
      enddo
    enddo
    contour_courant = cont_next(contour_courant)
  enddo
  ! 0 or 1 or 2 point contours
  start = .true.
  contour_courant = leaf_start(current_leaf)
  do while (cont_next(contour_courant).ne.0)
    if (cont_end(contour_courant).ne.0                           .and.  &
        cont_end(contour_courant).ne.cont_start(contour_courant) .and.  &
        vect_next(cont_start(contour_courant)).ne.                      &
        cont_end(contour_courant)) then
      contour_precedent = contour_courant
      contour_courant = cont_next(contour_courant)
      start=.false.
    else
      if (.not.start) then
        cont_next(contour_precedent)=cont_next(contour_courant)
      else
        leaf_start(current_leaf)=cont_next(contour_courant)
      endif
      contour_courant = cont_next(contour_courant)
    endif
  enddo
  ! updates fin_feuille
  !      LEAF_END(current_leaf)=CONTOUR_PRECEDENT
  !      WRITE (*,*) 'CONT:',CONTOUR_PRECEDENT,
  !     $FIN_FEUILLE(current_leaf)
  !
end subroutine menage
!
subroutine check_wind(feuille,wind,x,y)
  use greg_interfaces, except_this=>check_wind
  use greg_leaves
  !---------------------------------------------------------------------
  ! @ private
  ! This routine looks for imbricated contours which do not turn in the same
  ! direction, (WIND=.TRUE.) suggesting a point (X,Y) where to cut this gordian
  ! knot.
  !---------------------------------------------------------------------
  integer :: feuille                !
  logical :: wind                   !
  integer :: x                      !
  integer :: y                      !
  ! Local
  logical :: start,tested
  real :: vx1,vy1,vx2,vy2
  real :: val,fx,fy
  ! designations courantes
  integer :: current_leaf,contour_courant,vecteur_courant
  integer :: temp
  !
  integer :: xmin,xmax,ymin,ymax
  !
  current_leaf=feuille
  contour_courant = leaf_start(current_leaf)
  wind = .false.
  ! wind is used only for more than 1 contour per leaf
  if (cont_next(cont_next(contour_courant)).eq.0) return
  do while (cont_next(contour_courant).ne.0.and..not.wind)
    ! compute minmax of contour:
    start = .true.
    vecteur_courant = cont_start(contour_courant)
    do while (vect_next(vecteur_courant).ne.0)
      if (start) then
        xmin = vect_x(vecteur_courant)
        xmax = vect_x(vecteur_courant)
        ymin = vect_y(vecteur_courant)
        ymax = vect_y(vecteur_courant)
        start = .false.
      else
        xmin = min(xmin,vect_x(vecteur_courant))
        xmax = max(xmax,vect_x(vecteur_courant))
        ymin = min(ymin,vect_y(vecteur_courant))
        ymax = max(ymax,vect_y(vecteur_courant))
      endif
      vecteur_courant = vect_next(vecteur_courant)
    enddo
    !     check winding on this (closed) contour:
    vecteur_courant = cont_start(contour_courant)
    tested=.false.
    do while (vect_next(vect_next(vecteur_courant)).ne.0 .and. .not.tested)
      if (vect_x(vect_next(vecteur_courant)).eq.xmin .or.  &
          vect_x(vect_next(vecteur_courant)).eq.xmax .or.  &
          vect_y(vect_next(vecteur_courant)).eq.ymin .or.  &
          vect_y(vect_next(vecteur_courant)).eq.ymax) then
        temp= vect_next(vecteur_courant)
        vx1 = vect_x(temp)-vect_x(vecteur_courant)
        vy1 = vect_y(temp)-vect_y(vecteur_courant)
        temp= vect_next(temp)
        vx2 = vect_x(temp)-vect_x(vect_next(vecteur_courant))
        vy2 = vect_y(temp)-vect_y(vect_next(vecteur_courant))
        !     ca peut ne pas marcher si il y a 2 pts confondus sur
        !     TOUS les bords...
        val = (vx1*vy2)-(vy1*vx2)
        if (val.ne.0.0) then
          tested=.true.
          wind = ((val.lt.0.0.and.trigo) .or. (val.gt.0.0.and..not.trigo))
          if (wind) then
            fx=(float(xmax)+float(xmin))/2.0
            fy=(float(ymax)+float(ymin))/2.0
            x=fx
            y=fy
            goto 1
          endif
        endif
      endif
      vecteur_courant = vect_next(vecteur_courant)
    enddo
    contour_courant = cont_next(contour_courant)
  enddo
  !
1 return
end subroutine check_wind
!
!      SUBROUTINE INFO_STAT(FEUILLE,NCONT)
!*--------------------------------------------------------------------
!      USE GILDAS_DEF
!      INCLUDE 'gris.inc'
!      INTEGER NCONT
!      REAL SURF
!*
!* designations courantes
!      INTEGER FEUILLE,FEUILLE_COURANTE,CONTOUR_COURANT
!      INTEGER NVECT,XMIN,XMAX,YMIN,YMAX
!*
!      FEUILLE_COURANTE=FEUILLE
!      NCONT = 0
!      CALL INFO_READ(FEUILLE_COURANTE,NVECT,XMIN,YMIN,XMAX,YMAX)
!      SURF=FLOAT(XMAX-XMIN)*FLOAT(YMAX-YMIN)
!      IF (SURF.LE.1.0E4) THEN
!         NCONT=1
!         RETURN
!      ENDIF
!      CONTOUR_COURANT = LEAF_START(FEUILLE_COURANTE)
!      DO WHILE (CONT_NEXT(CONTOUR_COURANT).NE.0)
!         NCONT = NCONT + 1
!         CONTOUR_COURANT = CONT_NEXT(CONTOUR_COURANT)
!      ENDDO
!      END
!
!      SUBROUTINE LIST_CONT(CONTOUR_COURANT)
!*--------------------------------------------------------------------
!      USE GILDAS_DEF
!      INCLUDE 'gris.inc'
!* designations courantes
!      INTEGER CONTOUR_COURANT,VECTEUR_COURANT
!      INTEGER X,Y
!*
!      VECTEUR_COURANT = CONT_START(CONTOUR_COURANT)
!      DO WHILE (VECT_NEXT(VECTEUR_COURANT).NE.0)
!         X = VECT_X(VECTEUR_COURANT)
!         Y = VECT_Y(VECTEUR_COURANT)
!         WRITE (*,*) X,Y
!         VECTEUR_COURANT = VECT_NEXT(VECTEUR_COURANT)
!      ENDDO
!      WRITE (*,'(A)') '-1e4 -1e4'
!      END
!
