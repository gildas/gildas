module greg_curve
  !---------------------------------------------------------------------
  ! MODULE FOR CURVE (NLINK >> 4*KLINK)
  !---------------------------------------------------------------------
  integer(kind=4), parameter :: nlink=300,klink=30,ninimx=100   ! GREG values
  integer(kind=4) :: n,k
  real(kind=4) :: psto(nlink)
  real(kind=4) :: xsto(nlink),x1(nlink),x2(nlink),x3(nlink)
  real(kind=4) :: ysto(nlink),y1(nlink),y2(nlink),y3(nlink)
  real(kind=4) :: accurd
end module greg_curve
!
subroutine plcurv (nxy,x,y,z,accur,algorithm,variable,periodic,grelo,gvect,  &
    error)
  use gildas_def
  use greg_interfaces
  use greg_curve
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! GREG Support routine
  !       This routine connects a series of points in the subject space
  !	with a solid line or a dashed line. Cubic spline interpolation
  !	is available either for functions of X or functions of Y or for
  !	curves (X and Y both interpolated, see below). If the number of
  !	points exceeds spline-buffer capacity, the total curve is cut
  !	into several arcs and care is taken to ensure a good fitting
  !	between two adjacent splines.
  !
  !	Adapted from PIGRA.  P. Valiron  16-OCT-84
  !
  !     /VARIABLE X : the variable is X (abscissa)
  !                  	*** Y = F(X)
  !     /VARIABLE Y : the variable is Y (ordinate)
  !                  	*** X = F(Y)
  !     /VARIABLE Z : the variable is the Z array
  !                  	*** explicit parametric form.
  !     /VARIABLE NUMBERING : the variable is the numbering of the points
  !                  *** The sequence of the points is related to a fixed
  !                      increment in an explicit or implicit parametrisa-
  !                      tion of the curve (plotting the solution of a dif
  !                      -ferential equation for equally spaced steps...)
  !     /VARIABLE POLYGONAL_LENGTH OR CURVILINEAR_LENGTH :
  !		   *** the variable is an approximation of the
  !			curvilinear abscissa
  !                  *** The curvilinear abscissa is estimated iteratively.
  !                  The starting approximation is the sum of the distan-
  !                  ces of the points. For each iteration, the spline
  !                  coefficients are computed and the curvilinear abscis-
  !                  sa is integrated along the spline curve.
  !		   The number of iterations is ITERMX.
  !			POLYGONAL_LENGTH	ITERMX=0
  !			CURVILINEAR_LENGTH	ITERMX=2
  ! Arguments :
  !	X	R*8 (*)	Array of X coordinates		Input
  !	Y	R*8 (*)	Array of Y coordinates		Input
  !	Z	R*8 (*)	Array of parameter values	Input
  !	ACCUR	R*4	Accuracy of interpolation	Input
  !	ALGORITHM C*(*)	Algorithm used by interpolation	Input
  !	VARIABLE  C*(*)	Variable used for interpolation	Input
  !	PERIODIC  L	Is it a periodic function	Input
  !	GRELO	Ext	Pen up plotting function	Input
  !	GVECT	Ext	Pen down plotting function	Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in) :: nxy  ! Number of data points
  real*8 :: x(1)                                !
  real*8 :: y(1)                                !
  real*8 :: z(1)                                !
  real :: accur                                 !
  character(len=*) :: algorithm                 !
  character(len=*) :: variable                  !
  logical :: periodic                           !
  external :: grelo                             !
  external :: gvect                             !
  logical :: error                              !
  !----------------------------------------------------------------------
  !   VARX / logical; set if Y=F(X)
  !   VARY / logical; set if X=F(Y)
  !  NLINK / dimension of spline buffers
  !  KLINK / *** when number of points exceeds spline-buffer capacity,
  !          *** the curve is cut into several arcs.
  !          To ensure a good matching of consecutive splines, the KLINK
  !          end points of the current spline are not plotted; in the next
  !          spline, the first 2*KLINK points are copied from the previous
  !          one and the KLINK beginning points are not plotted.
  !          (see below, points not plotted are listed as stars)
  !
  !       I-th spline array IIIIIIIIIIIII*****
  !                                 *****JJJJJJJJJJJJJJ next spline array
  !              matching points....[2*klink ]
  !
  ! NINIMX / Maximum number of interpolated points between two adjacent
  !          data points.
  ! ITERMX / Maximum number of iterations allowed to obtain the approxi-
  !          mation of the curvilinear abscissa on the curve
  !
  !      N / Number of points stored in the buffer
  !      K / Index of the following point
  !  NSTOP / Index of last point to process in the buffer.
  !          If LINK is set, NSTOP is less than N to allow to the follo-
  !          wing spline to have some points in common with the current
  !          one.
  !----------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: rname='CURVE'
  logical :: varx,vary,varnum,varpol,varcur,varz,llcub,llsmo,link,init
  integer :: iter,itermx,km1,nini,nadd,i,nperiod
  integer(kind=size_length) :: n1,n2
  integer :: iu,iu1,iu2,is1,is2,kmax,js1,js2,ju1,ju2,ku
  real :: pkm1,pk,xkm1,xk,ykm1,yk,fifkm1,fifk,padd,dadd,h,arc,para
  real :: xp,yp,period,denom
  character(len=80) :: chain
  dimension para(nlink)
  !
  ! Selection of Options And Initialisation
  init=.true.
  accurd=accur/10.
  psto(1)=0
  llcub=.false.
  llsmo=.false.
  if (algorithm.eq.'CUBIC_SPLINE') then
    llcub=.true.
    !/	ELSEIF (ALGORITHM.EQ.'SMOOTHING_SPLINE') THEN
    !/	    LLSMO=.TRUE.
  else
    chain = 'Algorithm '//algorithm//' not implemented'
    call greg_message(seve%e,rname,chain)
    error=.true.
    return
  endif
  varx=.false.
  vary=.false.
  varz=.false.
  varnum=.false.
  varpol=.false.
  varcur=.false.
  if (variable.eq.'X') then
    varx=.true.
  elseif (variable.eq.'Y') then
    vary=.true.
  elseif (variable.eq.'NUMBERING') then
    varnum=.true.
  elseif (variable.eq.'POLYGONAL_LENGTH') then
    varpol=.true.
  elseif (variable.eq.'CURVILINEAR_LENGTH') then
    varcur=.true.
    itermx=2
  elseif (variable.eq.'Z') then
    varz=.true.
  endif
  !
  if (periodic) then
    n1=2-klink
    n2=nxy+klink
    if (varx) then
      if (y(1).ne.y(nxy)) goto 600
    elseif (vary) then
      if (x(1).ne.x(nxy)) goto 600
    else
      if (x(1).ne.x(nxy) .or. y(1).ne.y(nxy)) goto 600
    endif
    if (varx) then
      call us8_to_int (x,y,xsto,ysto,1)
      call us8_to_int (x(nxy),y(nxy),xsto(2),ysto,1)
      period=xsto(2)-xsto(1)
    elseif (vary) then
      call us8_to_int (x,y,xsto,ysto,1)
      call us8_to_int (x(nxy),y(nxy),xsto,ysto(2),1)
      period=ysto(2)-ysto(1)
    elseif (varz) then
      period=z(nxy)-z(1)
    endif
  else
    n1=1
    n2=nxy
  endif
  !
  ! Initialise for first point
  link=.false.
  ! if (nxy.le.4 .and. .not.periodic) then
  !   call gagout ('E-CURVE,  2, 3 or 4 Points available only')
  ! endif
  !
  ! Multi-buffer Loop
  !  N1,N2	Full range addresses of user data to be copied
  !  IU1,IU2	Current addresses of user coordinates to be copied
  !  IS1,IS2	Corresponding addresses in buffer
  !  KM1	Previous plotted address in buffer
  !  KMAX	Last address in buffer to be plotted
1000 continue
  if (link) then
    !
    ! LINK .TRUE.
    k=0
    do i=nlink-2*klink+1,nlink
      k=k+1
      psto(k)=psto(i)
      xsto(k)=xsto(i)
      ysto(k)=ysto(i)
    enddo
    if (n2-iu2.gt.nlink-2*klink) then
      ! LINK remains .TRUE.
      is1=2*klink+1
      is2=nlink
      iu1=iu2+1
      iu2=iu2+nlink-2*klink
      km1=klink
      !		N,KMAX unchanged. LINK remains .TRUE.
    else
      ! The end! LINK is reset to .FALSE.
      is1=2*klink+1
      is2=2*klink+n2-iu2
      iu1=iu2+1
      iu2=n2
      km1=klink
      if (periodic) then
        kmax=is2-klink
      else
        kmax=is2
      endif
      n=is2
      link=.false.
    endif
    !
  else
    !
    ! LINK is .FALSE.
    !
    if (n2-n1+1.gt.nlink) then
      ! Buffer overflow. Set LINK to .TRUE.
      is1=1
      is2=nlink
      iu1=n1
      iu2=n1+nlink-1
      if (periodic) then
        km1=klink
      else
        km1=1
      endif
      kmax=nlink-klink
      n=nlink
      link=.true.
    else
      ! Buffer is large enough. All points will be plotted within one pass
      is1=1
      is2=n2-n1+1
      iu1=n1
      iu2=n2
      if (periodic) then
        km1=klink
        kmax=is2-klink
      else
        km1=1
        kmax=is2
      endif
      n=is2
      link=.false.
    endif
  endif
  !
  ! For periodic interpolation, IU1 and/or IU2 may lie outside the range
  ! [1...NXY].
  ! J-indices JS1, JS2, JU1 and JU2 are the restriction of associated
  ! I-indices to the range [1...NXY] in user addresses
  !
  if (periodic) then
    ju1=max(iu1,1)
    ju2=min(iu2,nxy)
    js1=is1+(ju1-iu1)
    js2=is2+(ju2-iu2)
  else
    ju1=iu1
    ju2=iu2
    js1=is1
    js2=is2
  endif
  !
  ! Convert X and Y user coordinates to paper coordinates
  call us8_to_int (x(ju1),y(ju1),xsto(js1),ysto(js1),ju2-ju1+1)
  if (varz) then
    iu=ju1
    do k=js1,js2
      psto(k)=z(iu)
      iu=iu+1
    enddo
  endif
  !
  if (periodic) then
    ! Before...
    iu=iu1
    do k=is1,js1-1
      nperiod=-(iu-1)/(nxy-1)
      ku=iu+nperiod*(nxy-1)
      if (ku.le.0) then
        nperiod=nperiod+1
        ku=ku+(nxy-1)
      endif
      call us8_to_int (x(ku),y(ku),xsto(k),ysto(k),1)
      if (varx) then
        xsto(k)=xsto(k)-nperiod*period
      elseif (vary) then
        ysto(k)=ysto(k)-nperiod*period
      elseif (varz) then
        psto(k)=z(ku)-nperiod*period
      endif
      iu=iu+1
    enddo
    ! After...
    iu=ju2+1
    do k=js2+1,is2
      nperiod=-(iu-1)/(nxy-1)
      ku=iu+nperiod*(nxy-1)
      if (ku.le.0) then
        nperiod=nperiod+1
        ku=ku+(nxy-1)
      endif
      call us8_to_int (x(ku),y(ku),xsto(k),ysto(k),1)
      if (varx) then
        xsto(k)=xsto(k)-nperiod*period
      elseif (vary) then
        ysto(k)=ysto(k)-nperiod*period
      elseif (varz) then
        psto(k)=z(ku)-nperiod*period
      endif
      iu=iu+1
    enddo
  endif
  !
  ! Compute the parameter array according to /VARIABLE
  if (varx) then
    do k=is1,is2
      psto(k)=xsto(k)
    enddo
  elseif (vary) then
    do k=is1,is2
      psto(k)=ysto(k)
    enddo
  elseif (varnum) then
    do k=max(is1,2),is2
      psto(k)=psto(k-1)+1
    enddo
  elseif (varpol.or.varcur) then
    do k=max(is1,2),is2
      psto(k)=psto(k-1) + sqrt((xsto(k)-xsto(k-1))**2+(ysto(k)-ysto(k-1))**2)
    enddo
  endif
  !
  ! Compute spline coefficients
  if (varx) then
    call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
  elseif (vary) then
    call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
  else
    call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
    call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
  endif
  if (error) return
  !
  ! Iterate for Curvilinear Abscissa
  if (varcur) then
    do iter=1,itermx
      para(1)=0.
      do k=2,n
        call curvil (arc,error)
        if (error) goto 400
        para(k)=para(k-1)+arc
      enddo
      psto(1) = 0.
      do k=2,n
        psto(k)=para(k)
      enddo
      pkm1=psto(km1)
      call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
      if (error) return
      call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
      if (error) return
    enddo
  endif
300 continue
  !
  ! Loop on K to interpolate the intervals [PKM1...PK]
  if (init) then
    ! Initialisation for point KM1
    init=.false.
    pkm1=psto(km1)
    xkm1=xsto(km1)
    ykm1=ysto(km1)
    if (varx) then
      fifkm1=sqrt(y2(km1)**2/(1.+y1(km1)**2))
    elseif (vary) then
      fifkm1=sqrt(x2(km1)**2/(1.+x1(km1)**2))
    else
      denom = (x1(km1)**2+y1(km1)**2)
      if (denom.gt.1.e-10) then
        fifkm1=sqrt((x1(km1)*y2(km1)-x2(km1)*y1(km1))**2 / denom)
      else
        fifkm1 = 0
      endif
    endif
    call grelo (xkm1,ykm1)
  endif
  !
  ! Point KM1 has been already plotted
  do k=km1+1,kmax
    pk=psto(k)
    xk=xsto(k)
    yk=ysto(k)
    !
    ! Compute the number NINI of points to interpolate.
    if (varx) then
      fifk=sqrt(y2(k)**2/(1.+y1(k)**2))
    elseif (vary) then
      fifk=sqrt(x2(k)**2/(1.+x1(k)**2))
    else
      denom = (x1(k)**2+y1(k)**2)
      if (denom.gt.1.e-10) then
        fifk=sqrt((x1(k)*y2(k)-x2(k)*y1(k))**2 / denom)
      else
        fifk = 0
      endif
    endif
    nini=sqrt((pk-pkm1)**2*max(fifkm1,fifk)/accur)/2.
    !
    ! Interpolation Fork
    if (nini.lt.1) then
      !
      ! Plot point (XK,YK) with no interpolation
      call gvect (xk,yk)
      !
    else
      !
      ! Initialise for interpolation process
      if (nini.gt.ninimx) nini=ninimx
      padd   = pkm1
      dadd   = (pk-pkm1) / float (nini+1)
      nadd   = 0
      !
      ! Add NINI Interpolated Points Between (XKM1,YKM1) and (XK,YK)
      do i=1,nini
        padd=padd+dadd
        h=padd-psto(km1)
        if (varx) then
          xp=padd
          yp=ysto(km1) + h*(y1(km1)+h*(y2(km1)+h*y3(km1)/3.)/2.)
        elseif (vary) then
          xp=xsto(km1) + h*(x1(km1)+h*(x2(km1)+h*x3(km1)/3.)/2.)
          yp=padd
        else
          xp=xsto(km1) + h*(x1(km1)+h*(x2(km1)+h*x3(km1)/3.)/2.)
          yp=ysto(km1) + h*(y1(km1)+h*(y2(km1)+h*y3(km1)/3.)/2.)
        endif
        call gvect (xp,yp)
      enddo
      !
      ! Before Plotting (XK,YK)
      call gvect (xk,yk)
      !
      ! End of Interpolation Fork
    endif
    !
    ! End of Loop on Points to Interpolate
    km1=k
    pkm1=pk
    xkm1=xk
    ykm1=yk
    fifkm1=fifk
  enddo
  !
  if (link) goto 1000
  return
  !
  ! Error Recovery From Integration Failure in DIFSYS4
400 write(chain,*) 'Integration failed during iteration',iter
  call greg_message(seve%e,rname,chain)
  if (iter.eq.1) then
    chain = 'Parametrisation POLYGONAL_LENGTH is used instead'
  else
    write(chain,*) 'Parametrisation of iteration',iter-1,' is used instead'
  endif
  call greg_message(seve%i,rname,chain)
  goto 300
  !
  ! Non Periodic Data With /PERIODIC Option
600 call greg_message(seve%e,rname,'Option /PERIODIC needs periodic data')
  error=.true.
  return
end subroutine plcurv
!
subroutine plcurv4 (nxy,x,y,z,accur,algorithm,variable,periodic,grelo,gvect,  &
    error)
  use gildas_def
  use greg_interfaces, except_this=>plcurv4
  use greg_curve
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Support routine
  !       This routine connects a series of points in the subject space
  !	with a solid line or a dashed line. Cubic spline interpolation
  !	is available either for functions of X or functions of Y or for
  !	curves (X and Y both interpolated, see below). If the number of
  !	points exceeds spline-buffer capacity, the total curve is cut
  !	into several arcs and care is taken to ensure a good fitting
  !	between two adjacent splines.
  !
  !	Adapted from PIGRA.  P. Valiron  16-OCT-84
  !
  !     /VARIABLE X : the variable is X (abscissa)
  !                  	*** Y = F(X)
  !     /VARIABLE Y : the variable is Y (ordinate)
  !                  	*** X = F(Y)
  !     /VARIABLE Z : the variable is the Z array
  !                  	*** explicit parametric form.
  !     /VARIABLE NUMBERING : the variable is the numbering of the points
  !                  *** The sequence of the points is related to a fixed
  !                      increment in an explicit or implicit parametrisa-
  !                      tion of the curve (plotting the solution of a dif
  !                      -ferential equation for equally spaced steps...)
  !     /VARIABLE POLYGONAL_LENGTH OR CURVILINEAR_LENGTH :
  !		   *** the variable is an approximation of the
  !			curvilinear abscissa
  !                  *** The curvilinear abscissa is estimated iteratively.
  !                  The starting approximation is the sum of the distan-
  !                  ces of the points. For each iteration, the spline
  !                  coefficients are computed and the curvilinear abscis-
  !                  sa is integrated along the spline curve.
  !		   The number of iterations is ITERMX.
  !			POLYGONAL_LENGTH	ITERMX=0
  !			CURVILINEAR_LENGTH	ITERMX=2
  ! Arguments :
  !	X	R*4 (*)	Array of X coordinates		Input
  !	Y	R*4 (*)	Array of Y coordinates		Input
  !	Z	R*4 (*)	Array of parameter values	Input
  !	ACCUR	R*4	Accuracy of interpolation	Input
  !	ALGORITHM C*(*)	Algorithm used by interpolation	Input
  !	VARIABLE  C*(*)	Variable used for interpolation	Input
  !	PERIODIC  L	Is it a periodic function	Input
  !	GRELO	Ext	Pen up plotting function	Input
  !	GVECT	Ext	Pen down plotting function	Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in) :: nxy  ! Number of data points
  real*4 :: x(1)                    !
  real*4 :: y(1)                    !
  real*4 :: z(1)                    !
  real :: accur                     !
  character(len=*) :: algorithm     !
  character(len=*) :: variable      !
  logical :: periodic               !
  external :: grelo                 !
  external :: gvect                 !
  logical :: error                  !
  !----------------------------------------------------------------------
  !   VARX / logical; set if Y=F(X)
  !   VARY / logical; set if X=F(Y)
  !  NLINK / dimension of spline buffers
  !  KLINK / *** when number of points exceeds spline-buffer capacity,
  !          *** the curve is cut into several arcs.
  !          To ensure a good matching of consecutive splines, the KLINK
  !          end points of the current spline are not plotted; in the next
  !          spline, the first 2*KLINK points are copied from the previous
  !          one and the KLINK beginning points are not plotted.
  !          (see below, points not plotted are listed as stars)
  !
  !       I-th spline array IIIIIIIIIIIII*****
  !                                 *****JJJJJJJJJJJJJJ next spline array
  !              matching points....[2*klink ]
  !
  ! NINIMX / Maximum number of interpolated points between two adjacent
  !          data points.
  ! ITERMX / Maximum number of iterations allowed to obtain the approxi-
  !          mation of the curvilinear abscissa on the curve
  !
  !      N / Number of points stored in the buffer
  !      K / Index of the following point
  !  NSTOP / Index of last point to process in the buffer.
  !          If LINK is set, NSTOP is less than N to allow to the follo-
  !          wing spline to have some points in common with the current
  !          one.
  !----------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: rname='CURVE'
  logical :: varx,vary,varnum,varpol,varcur,varz,llcub,llsmo,link,init
  integer :: iter,itermx,km1,nini,nadd,i,nperiod
  integer(kind=size_length) :: n1,n2
  integer :: iu,iu1,iu2,is1,is2,kmax,js1,js2,ju1,ju2,ku
  real :: pkm1,pk,xkm1,xk,ykm1,yk,fifkm1,fifk,padd,dadd,h,arc,para
  real :: xp,yp,period,denom
  character(len=80) :: chain
  dimension para(nlink)
  !
  ! Selection of Options And Initialisation
  init=.true.
  accurd=accur/10.
  psto(1)=0
  llcub=.false.
  llsmo=.false.
  if (algorithm.eq.'CUBIC_SPLINE') then
    llcub=.true.
    !/	ELSEIF (ALGORITHM.EQ.'SMOOTHING_SPLINE') THEN
    !/	    LLSMO=.TRUE.
  else
    chain = 'Algorithm '//algorithm//' not implemented'
    call greg_message(seve%e,rname,chain)
    error=.true.
    return
  endif
  varx=.false.
  vary=.false.
  varz=.false.
  varnum=.false.
  varpol=.false.
  varcur=.false.
  if (variable.eq.'X') then
    varx=.true.
  elseif (variable.eq.'Y') then
    vary=.true.
  elseif (variable.eq.'NUMBERING') then
    varnum=.true.
  elseif (variable.eq.'POLYGONAL_LENGTH') then
    varpol=.true.
  elseif (variable.eq.'CURVILINEAR_LENGTH') then
    varcur=.true.
    itermx=2
  elseif (variable.eq.'Z') then
    varz=.true.
  endif
  !
  if (periodic) then
    n1=2-klink
    n2=nxy+klink
    if (varx) then
      if (y(1).ne.y(nxy)) goto 600
    elseif (vary) then
      if (x(1).ne.x(nxy)) goto 600
    else
      if (x(1).ne.x(nxy) .or. y(1).ne.y(nxy)) goto 600
    endif
    if (varx) then
      call us4_to_int (x,y,xsto,ysto,1)
      call us4_to_int (x(nxy),y(nxy),xsto(2),ysto,1)
      period=xsto(2)-xsto(1)
    elseif (vary) then
      call us4_to_int (x,y,xsto,ysto,1)
      call us4_to_int (x(nxy),y(nxy),xsto,ysto(2),1)
      period=ysto(2)-ysto(1)
    elseif (varz) then
      period=z(nxy)-z(1)
    endif
  else
    n1=1
    n2=nxy
  endif
  !
  ! Initialise for first point
  link=.false.
  ! if (nxy.le.4 .and. .not.periodic) then
  !   call gagout ('W-CURVE,  2, 3 or 4 Points available only')
  ! endif
  !
  ! Multi-buffer Loop
  !  N1,N2	Full range addresses of user data to be copied
  !  IU1,IU2	Current addresses of user coordinates to be copied
  !  IS1,IS2	Corresponding addresses in buffer
  !  KM1	Previous plotted address in buffer
  !  KMAX	Last address in buffer to be plotted
1000 continue
  if (link) then
    !
    ! LINK .TRUE.
    k=0
    do i=nlink-2*klink+1,nlink
      k=k+1
      psto(k)=psto(i)
      xsto(k)=xsto(i)
      ysto(k)=ysto(i)
    enddo
    if (n2-iu2.gt.nlink-2*klink) then
      ! LINK remains .TRUE.
      is1=2*klink+1
      is2=nlink
      iu1=iu2+1
      iu2=iu2+nlink-2*klink
      km1=klink
      !		N,KMAX unchanged. LINK remains .TRUE.
    else
      ! The end! LINK is reset to .FALSE.
      is1=2*klink+1
      is2=2*klink+n2-iu2
      iu1=iu2+1
      iu2=n2
      km1=klink
      if (periodic) then
        kmax=is2-klink
      else
        kmax=is2
      endif
      n=is2
      link=.false.
    endif
    !
  else
    !
    ! LINK is .FALSE.
    !
    if (n2-n1+1.gt.nlink) then
      ! Buffer overflow. Set LINK to .TRUE.
      is1=1
      is2=nlink
      iu1=n1
      iu2=n1+nlink-1
      if (periodic) then
        km1=klink
      else
        km1=1
      endif
      kmax=nlink-klink
      n=nlink
      link=.true.
    else
      ! Buffer is large enough. All points will be plotted within one pass
      is1=1
      is2=n2-n1+1
      iu1=n1
      iu2=n2
      if (periodic) then
        km1=klink
        kmax=is2-klink
      else
        km1=1
        kmax=is2
      endif
      n=is2
      link=.false.
    endif
  endif
  !
  ! For periodic interpolation, IU1 and/or IU2 may lie outside the range
  ! [1...NXY].
  ! J-indices JS1, JS2, JU1 and JU2 are the restriction of associated
  ! I-indices to the range [1...NXY] in user addresses
  !
  if (periodic) then
    ju1=max(iu1,1)
    ju2=min(iu2,nxy)
    js1=is1+(ju1-iu1)
    js2=is2+(ju2-iu2)
  else
    ju1=iu1
    ju2=iu2
    js1=is1
    js2=is2
  endif
  !
  ! Convert X and Y user coordinates to paper coordinates
  call us4_to_int (x(ju1),y(ju1),xsto(js1),ysto(js1),ju2-ju1+1)
  if (varz) then
    iu=ju1
    do k=js1,js2
      psto(k)=z(iu)
      iu=iu+1
    enddo
  endif
  !
  if (periodic) then
    ! Before...
    iu=iu1
    do k=is1,js1-1
      nperiod=-(iu-1)/(nxy-1)
      ku=iu+nperiod*(nxy-1)
      if (ku.le.0) then
        nperiod=nperiod+1
        ku=ku+(nxy-1)
      endif
      call us4_to_int (x(ku),y(ku),xsto(k),ysto(k),1)
      if (varx) then
        xsto(k)=xsto(k)-nperiod*period
      elseif (vary) then
        ysto(k)=ysto(k)-nperiod*period
      elseif (varz) then
        psto(k)=z(ku)-nperiod*period
      endif
      iu=iu+1
    enddo
    ! After...
    iu=ju2+1
    do k=js2+1,is2
      nperiod=-(iu-1)/(nxy-1)
      ku=iu+nperiod*(nxy-1)
      if (ku.le.0) then
        nperiod=nperiod+1
        ku=ku+(nxy-1)
      endif
      call us4_to_int (x(ku),y(ku),xsto(k),ysto(k),1)
      if (varx) then
        xsto(k)=xsto(k)-nperiod*period
      elseif (vary) then
        ysto(k)=ysto(k)-nperiod*period
      elseif (varz) then
        psto(k)=z(ku)-nperiod*period
      endif
      iu=iu+1
    enddo
  endif
  !
  ! Compute the parameter array according to /VARIABLE
  if (varx) then
    do k=is1,is2
      psto(k)=xsto(k)
    enddo
  elseif (vary) then
    do k=is1,is2
      psto(k)=ysto(k)
    enddo
  elseif (varnum) then
    do k=max(is1,2),is2
      psto(k)=psto(k-1)+1
    enddo
  elseif (varpol.or.varcur) then
    do k=max(is1,2),is2
      psto(k)=psto(k-1) + sqrt((xsto(k)-xsto(k-1))**2+(ysto(k)-ysto(k-1))**2)
    enddo
  endif
  !
  ! Compute spline coefficients
  if (varx) then
    call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
  elseif (vary) then
    call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
  else
    call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
    if (error) return
    call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
  endif
  if (error) return
  !
  ! Iterate for Curvilinear Abscissa
  if (varcur) then
    do iter=1,itermx
      para(1)=0.
      do k=2,n
        call curvil (arc,error)
        if (error) goto 400
        para(k)=para(k-1)+arc
      enddo
      psto(1) = 0.
      do k=2,n
        psto(k)=para(k)
      enddo
      pkm1=psto(km1)
      call cubspl4 (n,psto,xsto,x1,x2,x3,0,0,error)
      if (error) return
      call cubspl4 (n,psto,ysto,y1,y2,y3,0,0,error)
      if (error) return
    enddo
  endif
300 continue
  !
  ! Loop on K to interpolate the intervals [PKM1...PK]
  if (init) then
    ! Initialisation for point KM1
    init=.false.
    pkm1=psto(km1)
    xkm1=xsto(km1)
    ykm1=ysto(km1)
    if (varx) then
      fifkm1=sqrt(y2(km1)**2/(1.+y1(km1)**2))
    elseif (vary) then
      fifkm1=sqrt(x2(km1)**2/(1.+x1(km1)**2))
    else
      denom = (x1(km1)**2+y1(km1)**2)
      if (denom.gt.1.e-10) then
        fifkm1=sqrt((x1(km1)*y2(km1)-x2(km1)*y1(km1))**2 / denom)
      else
        fifkm1 = 0
      endif
    endif
    call grelo (xkm1,ykm1)
  endif
  !
  ! Point KM1 has been already plotted
  do k=km1+1,kmax
    pk=psto(k)
    xk=xsto(k)
    yk=ysto(k)
    !
    ! Compute the number NINI of points to interpolate.
    if (varx) then
      fifk=sqrt(y2(k)**2/(1.+y1(k)**2))
    elseif (vary) then
      fifk=sqrt(x2(k)**2/(1.+x1(k)**2))
    else
      denom = (x1(k)**2+y1(k)**2)
      if (denom.gt.1.e-10) then
        fifk=sqrt((x1(k)*y2(k)-x2(k)*y1(k))**2 / denom)
      else
        fifk = 0
      endif
    endif
    nini=sqrt((pk-pkm1)**2*max(fifkm1,fifk)/accur)/2.
    !
    ! Interpolation Fork
    if (nini.lt.1) then
      !
      ! Plot point (XK,YK) with no interpolation
      call gvect (xk,yk)
      !
    else
      !
      ! Initialise for interpolation process
      if (nini.gt.ninimx) nini=ninimx
      padd   = pkm1
      dadd   = (pk-pkm1) / float (nini+1)
      nadd   = 0
      !
      ! Add NINI Interpolated Points Between (XKM1,YKM1) and (XK,YK)
      do i=1,nini
        padd=padd+dadd
        h=padd-psto(km1)
        if (varx) then
          xp=padd
          yp=ysto(km1) + h*(y1(km1)+h*(y2(km1)+h*y3(km1)/3.)/2.)
        elseif (vary) then
          xp=xsto(km1) + h*(x1(km1)+h*(x2(km1)+h*x3(km1)/3.)/2.)
          yp=padd
        else
          xp=xsto(km1) + h*(x1(km1)+h*(x2(km1)+h*x3(km1)/3.)/2.)
          yp=ysto(km1) + h*(y1(km1)+h*(y2(km1)+h*y3(km1)/3.)/2.)
        endif
        call gvect (xp,yp)
      enddo
      !
      ! Before Plotting (XK,YK)
      call gvect (xk,yk)
      !
      ! End of Interpolation Fork
    endif
    !
    ! End of Loop on Points to Interpolate
    km1=k
    pkm1=pk
    xkm1=xk
    ykm1=yk
    fifkm1=fifk
  enddo
  !
  if (link) goto 1000
  return
  !
  ! Error Recovery From Integration Failure in DIFSYS4
400 write(chain,*) 'Integration failed during iteration',iter
  call greg_message(seve%e,rname,chain)
  if (iter.eq.1) then
    chain = 'Parametrisation POLYGONAL_LENGTH is used instead'
  else
    write(chain,*) 'Parametrisation of iteration',iter-1,' is used instead'
  endif
  call greg_message(seve%i,rname,chain)
  goto 300
  !
  ! Non Periodic Data With /PERIODIC Option
600 call greg_message(seve%e,rname,'Option /PERIODIC needs periodic data')
  error=.true.
  return
end subroutine plcurv4
!
subroutine curvil (s,error)
  use greg_interfaces, except_this=>curvil
  use greg_curve
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routine
  !    	This routine is called by CURV to compute the curvilinear length
  !     	S of the spline arc [PSTO(K-1),PSTO(K)].
  !	RETURN 1 occurs if DIFSYS4 fails to integrate
  ! Arguments :
  !	S	R*4	Curvilinear length		Output
  !     ERROR	L	Error return		        Output
  ! Subroutines :
  !	CURFUN, DIFSYS4
  !---------------------------------------------------------------------
  real :: s                         !
  logical :: error                  !
  ! Local
  real :: d,h,p,p1,p2,q
  !
  p1 = psto(k-1)
  p2 = psto(k)
  p  = p1
  d  = p2-p1
  s  = 0.
  h  = d/10.
  !
2 continue
  q  = (p2-p)/h
  if (q.lt.1.e-6) then
    return
  elseif (q.lt.1.1) then
    h  = p2-p
  endif
  call difsys4 (curfun,accurd,h,p,s)
  if (h.eq.0.) then
    call greg_message(seve%e,'CURVE','Integration failure in DIFSYS4')
    error = .true.
    return
  endif
  goto 2
end subroutine curvil
!
subroutine curfun (p,s,dsdp)
  use greg_interfaces, except_this=>curfun
  use greg_curve
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! GREG	Internal routine
  !	This routine states the differential equation to be solved by
  !	DIFSYS4 in order to obtain the curvilinear length of an arc of
  !	spline.
  ! Arguments :
  !	P	R*4	Value of parameter			Input
  !	S	R*4	Curvilinear length			Input
  !	DSDP	R*4	Derivative of S relative to P		Output
  !---------------------------------------------------------------------
  real :: p                         !
  real :: s                         !
  real :: dsdp                      !
  ! Local
  real :: h
  !
  h=p-psto(k-1)
  dsdp = sqrt((x1(k-1)+h*(x2(k-1)+x3(k-1)*h/2.))**2 +  &
              (y1(k-1)+h*(y2(k-1)+y3(k-1)*h/2.))**2)
end subroutine curfun
