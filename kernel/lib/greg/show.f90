subroutine greg_show(line,error)
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_show
  use greg_kernel
  use greg_pen
  use greg_axes
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  !  GREG  Support routine for command
  !  SHOW
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Command line
  logical,          intent(out) :: error  ! Logical error flag
  ! Global
  include 'gbl_pi.inc'
  ! Local
  character(len=*), parameter :: rname='SHOW'
  integer :: i,j,nc
  logical :: all
  character(len=16) :: chain
  character(len=2) :: what
  character(len=14) :: angu(4),syst(5)
  character(len=message_length) :: mess
  ! Data
  data syst/'UNKNOWN','EQUATORIAL','GALACTIC','HORIZONTAL','ICRS'/
  data angu/'SECOND','MINUTE','DEGREE','RADIAN'/
  !
  error = .true.               ! Will be resetted by any SHOW
  chain = 'WHere'              ! Default
  call sic_ke (line,0,1,chain,nc,.false.,error)
  if (error) return
  all = (chain(1:2).eq.'AL' .or. chain.eq.'*')
  what = chain(1:2)
  !
  ! Accuracy
  if (all.or.what.eq.'AC') then
    write(mess,100) 'Accuracy ',accurd
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! ANgle
  if (all.or.what.eq.'AN') then
    if (gproj%type.ne.p_none) then
      write(mess,102) 'Angle Unit ',angu(u_angle)
    else
      write(mess,102) 'Angle Unit ','NOT CHECKED'
    endif
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! AXis
  if (all.or.what.eq.'AX') then
    if (axis_xdecim_brief) then
      write(mess,111) 'Axis ','XDECIMAL','BRIEF'
    else
      write(mess,111) 'Axis ','XDECIMAL','NOBRIEF'
    endif
    call greg_message(seve%r,rname,mess)
    !
    if (axis_ydecim_brief) then
      write(mess,111) 'Axis ','YDECIMAL','BRIEF'
    else
      write(mess,111) 'Axis ','YDECIMAL','NOBRIEF'
    endif
    call greg_message(seve%r,rname,mess)
    !
    if (axis_xsexag_brief) then
      write(mess,111) 'Axis ','XSEXAGESIMAL','BRIEF'
    else
      write(mess,111) 'Axis ','XSEXAGESIMAL','NOBRIEF'
    endif
    call greg_message(seve%r,rname,mess)
    !
    if (axis_ysexag_brief) then
      write(mess,111) 'Axis ','YSEXAGESIMAL','BRIEF'
    else
      write(mess,111) 'Axis ','YSEXAGESIMAL','NOBRIEF'
    endif
    call greg_message(seve%r,rname,mess)
    !
    if (.not.all) return
  endif
  !
  ! BLanking
  if (all.or.what.eq.'BL') then
    if (eblank.lt.0) then
      write(mess,102) 'Blanking ','TURNED OFF'
      call greg_message(seve%r,rname,mess)
    else
      write(mess,106) 'Blanking value    ',cblank
      call greg_message(seve%r,rname,mess)
      write(mess,106) 'Blanking tolerance',eblank
      call greg_message(seve%r,rname,mess)
    endif
    if (.not.all) return
  endif
  !
  ! CEntering
  if (all.or.what.eq.'CE') then
    write(mess,101) 'Centering option ',icente
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! CHaracter size
  if (all.or.what.eq.'CH') then
    write(mess,100) 'Character size ',cdef
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! COordinate system
  if (all.or.what.eq.'CO') then
    if (iboxd.eq.-1) then
      write(mess,102) 'Coordinate system','USER'
      call greg_message(seve%r,rname,mess)
    else
      i = mod(iboxd,10)
      j = iboxd/10
      if (j.eq.0) then
        write(mess,102) 'Coordinate system','BOX',i
      else
        write(mess,102) 'Coordinate system','CHARACTER',i
      endif
      call greg_message(seve%r,rname,mess)
    endif
    if (.not.all) return
  endif
  !
  ! DEcimal or SExagesimal
  if (all.or.what.eq.'SE'.or.what.eq.'DE') then
    if (xsixty.ne.format_decimal) then
      write(mess,102) 'X labelling is ','SEXAGESIMAL with ',ndecimx,' decimals'
    else
      write(mess,102) 'X labelling is ','DECIMAL'
    endif
    call greg_message(seve%r,rname,mess)
    if (ysixty.ne.format_decimal) then
      write(mess,102) 'Y labelling is ','SEXAGESIMAL with ',ndecimy,' decimals'
    else
      write(mess,102) 'Y labelling is ','DECIMAL'
    endif
    call greg_message(seve%r,rname,mess)
  endif
  !
  ! EXpansion factor for characters and symbols
  if (all.or.what.eq.'EX') then
    write(mess,100) 'Expansion factor ',expand
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! FOnt of characters
  if (all.or.what.eq.'FO') then
    if (i_font.eq.0) then
      write(mess,102) 'Character font ','SIMPLEX'
    else
      write(mess,102) 'Character font ','DUPLEX'
    endif
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! BOx location
  if (all.or.what.eq.'BO'.or.what.eq.'VI') then
    write(mess,100) 'Box Location ',gx1,gx2,gy1,gy2
    call greg_message(seve%r,rname,mess)
    write(mess,100) 'View Port    ',gx1/(lx2-lx1),gx2/(lx2-lx1), &
      gy1/(ly2-ly1),gy2/(ly2-ly1)
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! PLot_PAge size
  if (all .or. what.eq.'PA' .or. what.eq.'PL') then
    write(mess,100) 'Plot page size ',lx2-lx1,ly2-ly1
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! MArker style and size
  if (all.or.what.eq.'MA') then
    write(mess,101) 'Marker type  ',nsides,istyle
    call greg_message(seve%r,rname,mess)
    write(mess,100) 'Marker size  ',csymb
    call greg_message(seve%r,rname,mess)
    write(mess,100) 'Marker angle ',sangle
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! Orientation of characters
  if (all.or.what.eq.'OR') then
    write(mess,100) 'Orientation of Text    ',tangle
    call greg_message(seve%r,rname,mess)
    write(mess,100) 'Orientation of Markers ',sangle
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! Projection
  if (all.or.what.eq.'PR') then
    if (gproj%type.ne.p_none) then
      write (mess,110) projnam(gproj%type),gproj%angle*180.d0/pi
      call sexfor(0.d0,0.d0)
    else
      write (mess,102) 'Projection','DISABLED'
    endif
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! SYstem
  if (all.or.what.eq.'SY') then
    write(mess,102) 'System is ',syst(i_system)
    if (i_system.eq.type_eq) then
      if (i_equinox.eq.equinox_null) then
        write(mess,'(A,1X,A)')  trim(mess),'(equinox unset)'
      else
        write(mess,'(A,1X,F0.1)')  trim(mess),i_equinox
      endif
    endif
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! TIck size
  if (all.or.what.eq.'TI') then
    write(mess,100) 'Tick size ',ctick
    call greg_message(seve%r,rname,mess)
    write(mess,107) 'Tick space',smallx,bigx
    call greg_message(seve%r,rname,mess)
    write(mess,108) 'Tick space',smally,bigy
    call greg_message(seve%r,rname,mess)
    if (.not.all) return
  endif
  !
  ! PEn attributes
  if (all.or.what.eq.'PE') then
    write(mess,*) ' PEN    Dashed    Weight    Colour'
    call greg_message(seve%r,rname,mess)
    do i=0,maxpen
      if (i.eq.cpen) then
        write(mess,103) i,ldashe(i),gtv_penwei_tostr(lweigh(i)),  &
          gtv_pencol_id2name(lcolou(i)),'(Current pen)'
      else
        write(mess,103) i,ldashe(i),gtv_penwei_tostr(lweigh(i)),  &
          gtv_pencol_id2name(lcolou(i))
      endif
      call greg_message(seve%r,rname,mess)
    enddo
    if (.not.all) return
  endif
  !
  ! WHere is the pen. Synonym for SHOW LImits.
  if (all .or. what.eq.'WH' .or. what.eq.'LI') then
    call show_limits(line,error)
    if (error)  return
    if (.not.all) return
  endif
  !
  if (.not.all) then
    call greg_message(seve%e,rname,'Argument not understood: '//chain)
    error = .true.
    return
  endif
  !
  return
100 format(a,t25,4(f8.3,2x))
101 format(a,t24,4(i6,4x))
102 format(a,t29,a,i2,a)
103 format(1x,2(i3,6x),a,2x,a,2x,a)
106 format(a,t25,1pg11.4)
107 format(a,t29,'X  Small ',1pg16.8,'    Big ',1pg16.8)
108 format(a,t29,'Y  Small ',1pg16.8,'    Big ',1pg16.8)
110 format('Projection ',t29,a,' at angle ',f12.6,' from center')
111 format(a,t24,a12,1x,a)
end subroutine greg_show
!
subroutine show_limits(line,error)
  use phys_const
  use gbl_constant
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>show_limits
  use greg_kernel
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='SHOW'
  integer(kind=4) :: w_angle,n,nc
  real(kind=8) :: uxp,uyp,uxp2,uyp2,x1,x2,y1,y2,abstmp,reltmp
  character(len=12) :: cuxp,cuyp,cx1,cx2,cy1,cy2
  character(len=message_length) :: mess
  integer(kind=4), parameter :: nangu=5
  character(len=8) :: angu(nangu),dummy,uname
  ! Data
  data angu/'SECOND','MINUTE','DEGREE','RADIAN','ABSOLUTE'/
  !
  if (gproj%type.eq.p_none .and. sic_present(0,2)) then
    call greg_message(seve%w,rname,  &
      'No projection defined, default to user units')
  endif
  !
  w_angle = u_angle
  if (axis_xlog) then
    uxp = exp((xp-gx1)/gux + lux)
  else
    uxp = (xp-gx1)/gux + gux1
  endif
  if (axis_ylog) then
    uyp = exp((yp-gy1)/guy + luy)
  else
    uyp = (yp-gy1)/guy + guy1
  endif
  !
  write(mess,104)
  call greg_message(seve%r,rname,mess)
  !
  write(cuxp,99) xp
  write(cuyp,99) yp
  write(cx1,99)  gx1
  write(cx2,99)  gx2
  write(cy1,99)  gy1
  write(cy2,99)  gy2
  write(mess,105) 'Plot',cuxp,cuyp,cx1,cx2,cy1,cy2
  call greg_message(seve%r,rname,mess)
  !
  if (gproj%type.ne.p_none) then
    if (sic_present(0,2))then
      call sic_ke (line,0,2,dummy,nc,.true.,error)
      if (error) return
      call sic_ambigs(rname,dummy,uname,n,angu,nangu,error)
      if (error) then
        call greg_message(seve%e,rname,'Wrong expected angle type')
        return
      endif
      w_angle=n
    endif
    if (w_angle.eq.u_degree) then
      uname = 'Degrees'
      write(cuxp,99) uxp*180.0d0/pi
      write(cuyp,99) uyp*180.0d0/pi
      write(cx1,99)  gux1*180.0d0/pi
      write(cx2,99)  gux2*180.0d0/pi
      write(cy1,99)  guy1*180.0d0/pi
      write(cy2,99)  guy2*180.0d0/pi
    elseif (w_angle.eq.u_minute) then
      uname = 'Minutes'
      write(cuxp,99) uxp*1.08d4/pi
      write(cuyp,99) uyp*1.08d4/pi
      write(cx1,99)  gux1*1.08d4/pi
      write(cx2,99)  gux2*1.08d4/pi
      write(cy1,99)  guy1*1.08d4/pi
      write(cy2,99)  guy2*1.08d4/pi
    elseif (w_angle.eq.u_second) then
      uname = 'Seconds'
      write(cuxp,99) uxp*6.48d5/pi
      write(cuyp,99) uyp*6.48d5/pi
      write(cx1,99)  gux1*6.48d5/pi
      write(cx2,99)  gux2*6.48d5/pi
      write(cy1,99)  guy1*6.48d5/pi
      write(cy2,99)  guy2*6.48d5/pi
    elseif (w_angle.eq.u_radian) then
      uname = 'Radians'
      write(cuxp,99) uxp
      write(cuyp,99) uyp
      write(cx1,99) gux1
      write(cx2,99) gux2
      write(cy1,99) guy1
      write(cy2,99) guy2
    else
      uname = 'Absolute'
      call rel_to_abs(gproj,uxp,uyp,uxp2,uyp2,1)
      ! In order to be symetric to LIMITS ... ABSOLUTE, we must display the
      ! coordinates of the middle of the box sides.
      reltmp = (gux1+gux2)/2.d0
      call rel_to_abs(gproj,reltmp,guy1,abstmp,y1,1)
      call rel_to_abs(gproj,reltmp,guy2,abstmp,y2,1)
      reltmp = (guy1+guy2)/2.d0
      call rel_to_abs(gproj,gux1,reltmp,x1,abstmp,1)
      call rel_to_abs(gproj,gux2,reltmp,x2,abstmp,1)
      if (i_system.eq.type_eq .or. i_system.eq.type_ic) then
        call sexag(cuxp,uxp2,24)
        call sexag(cx1,x1,24)
        call sexag(cx2,x2,24)
      else
        call sexag(cuxp,uxp2,360)
        call sexag(cx1,x1,360)
        call sexag(cx2,x2,360)
      endif
      call sexag(cuyp,uyp2,360)
      call sexag(cy1,y1,360)
      call sexag(cy2,y2,360)
    endif
  else
    uname = 'User'
    write(cuxp,99) uxp
    write(cuyp,99) uyp
    write(cx1,99) gux1
    write(cx2,99) gux2
    write(cy1,99) guy1
    write(cy2,99) guy2
  endif
  write(mess,105)  uname,cuxp,cuyp,cx1,cx2,cy1,cy2
  call greg_message(seve%r,rname,mess)
  !
  if (axis_xlog) then
    write(mess,102) 'X axis scale is','LOGARITHMIC'
    call greg_message(seve%r,rname,mess)
  endif
  if (axis_ylog) then
    write(mess,102) 'Y axis scale is','LOGARITHMIC'
    call greg_message(seve%r,rname,mess)
  endif
  !
102 format(a,t29,a,i2,a)
104 format(  t9,'|---- Current X and Y ----|----------------- X and Y ranges ------------------')
105 format(a,t9,'|',a,1x,a,               '|',4(a,1x))
99  format(1pg11.4)
end subroutine show_limits
