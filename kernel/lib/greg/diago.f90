subroutine rstrip(line,error)
  use gildas_def
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>rstrip
  use greg_kernel
  use greg_rg
  use greg_xyz
  !---------------------------------------------------------------------
  ! @ private
  !  Make a strip across the regular grid array
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line   ! Command line, updated in return
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='STRIP'
  character(len=1) :: code
  real(kind=8) :: x1,x2,y1,y2
  !
  ! Check if map is here
  if (rg%status.eq.code_pointer_null) then
    call greg_message(seve%e,rname,'No map loaded')
    error = .true.
    return
    !
    ! Fill in arguments from command line
  elseif (sic_present(0,4)) then
    call sic_r8 (line,0,1,x1,.true.,error)
    if (error) return
    call sic_r8 (line,0,2,y1,.true.,error)
    if (error) return
    call sic_r8 (line,0,3,x2,.true.,error)
    if (error) return
    call sic_r8 (line,0,4,y2,.true.,error)
    if (error) return
  elseif (gtg_curs()) then
    !
    ! Insufficient arguments, call the cursor
    call greg_message(seve%i,rname,'Using the cursor.')
    call greg_message(seve%i,rname,'Type Q to abort, any other key to set '//  &
    'strip points')
    error = .true.
    call gr_curs(x1,y1,xcurse,ycurse,code)
    if (code.eq.'Q') return
    call gr_curs(x2,y2,xcurse,ycurse,code)
    if (code.eq.'Q') return
    error = .false.
  else
    !
    ! No cursor either
    call greg_message(seve%e,rname,'No cursor available')
    error = .true.
    return
  endif
  !
  call delete_xyz (.true.,error)
  if (error) return
  call more_xyz (max(rg%nx,rg%ny),error)
  if (error)  return
  call sloppy(x1,x2,y1,y2,rg%data,column_x,column_y,nxy,error)
  if (error) return
  call create_xyz(error)
  if (error)  return
  !
  write(line,100) x1,y1,x2,y2
  return
  !
100 format('STRIP',4(1x,1pg13.6))
end subroutine rstrip
!
subroutine sloppy(x1,x2,y1,y2,r,sample,value,nxy,error)
  use gbl_message
  use greg_interfaces, except_this=>sloppy
  use greg_rg
  !---------------------------------------------------------------------
  ! @ private
  !  Make a diagonal strip across the regular grid array
  !  The number of pixels is NX or NY, according to wether the slope is
  ! greater than NX/NY and interpolation in the non-sampled direction is
  ! made using a parabola fitting the 3 nearby points.
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)  :: x1,y1      ! Coordinates of one point on line
  real(kind=8),    intent(in)  :: x2,y2      ! Coordinates of another point on line
  real(kind=4),    intent(in)  :: r(*)       ! Array of map value
  real(kind=8),    intent(out) :: sample(*)  ! Array of coordinates to be filled
  real(kind=8),    intent(out) :: value(*)   ! Array of values to be filled
  integer(kind=4), intent(out) :: nxy        ! Number of samples filled
  logical,         intent(out) :: error      ! Logical error flag
  ! Local
  real(kind=8) :: dx,xx,tmp
  integer(kind=4) :: i,j,k,n,is
  real(kind=4) :: vx,vy1,vy2,vy3,a,b,x,y,xi1,xi2,yi1,yi2
  !
  ! Take care of strip along X axis
  error = .false.
  if (y2.eq.y1) then
    tmp = (y1-rg%yval)/rg%yinc + rg%yref
    n = idnint(tmp)
    if (n.gt.rg%ny .or. n.lt.1) goto 99
    dx = rg%xinc
    sample(1) = rg%xval + (1.d0-rg%xref)*rg%xinc
    k = (n-1)*rg%nx+1
    value(1) = r(k)
    do i=2,rg%nx
      k = k+1
      value(i) = r(k)
      sample(i) = sample(i-1) + dx
    enddo
    nxy = rg%nx
    return
    !
    ! Take care of strip along Y axis
  elseif (x2.eq.x1) then
    tmp = (x1-rg%xval)/rg%xinc + rg%xref
    n = idnint(tmp)
    if (n.gt.rg%nx .or. n.lt.1) goto 99
    dx = rg%yinc
    sample(1) = rg%yval + (1.d0-rg%yref)*rg%yinc
    k = n
    value(1) = r(k)
    do i=2,rg%ny
      k = k+rg%nx
      value(i) = r(k)
      sample(i) = sample(i-1) + dx
    enddo
    nxy = rg%ny
    return
  endif
  !
  ! Get the line equation in map pixels
  yi1 = (y1-rg%yval)/rg%yinc+rg%yref
  yi2 = (y2-rg%yval)/rg%yinc+rg%yref
  xi1 = (x1-rg%xval)/rg%xinc+rg%xref
  xi2 = (x2-rg%xval)/rg%xinc+rg%xref
  a = (yi2-yi1)/(xi2-xi1)
  !
  if (abs(a).lt.1.0) then
    ! If slope is less than (NY-1)/(NX-1), use I pixels from 1 to NX
    ! with line equation Y = A*X+B
    b = yi1 - a*xi1
    dx = rg%xinc
    xx = -rg%xref*rg%xinc+rg%xval
    is = 0
    !
    do i=1,rg%nx
      y = a*i+b
      xx = xx+dx
      if (y.lt.1 .or. y.gt.rg%ny) cycle   ! I
      !
      ! Set the user coordinate
      is = is+1
      sample(is) = xx
      !
      ! Find nearest pixel J and interpolate between J-1, J and J+1
      j = nint(y)
      if (j.eq.1) then
        value(i) = r(i) + (r(rg%nx+i)-r(i))*(y-1)
      elseif (j.eq.rg%ny) then
        value(i) = r((rg%ny-1)*rg%nx+i) + (r((rg%ny-1)*rg%nx+i)-r((rg%ny-2)*rg%nx+i)) * (rg%ny-y)
      else
        k = (j-2)*rg%nx+i
        vy1 = r(k)
        k = k+rg%nx
        vy2 = r(k)
        k = k+rg%nx
        vy3 = r(k)
        vx = y-j
        value(is) = ( (vy1+vy3-2*vy2)*vx + vy3-vy1 )/2.*vx + vy2
      endif
    enddo
    !
  else
    ! If slope greater than (NY-1)/(NX-1), use J pixels from 1 to NY
    ! with line equation X = A*Y + B
    !
    a = 1./a
    b = xi1 - a*yi1
    dx = rg%yinc
    xx = -rg%yref*rg%yinc+rg%yval
    is = 0
    !
    do j=1,rg%ny
      xx = xx+dx
      x = a*j+b
      if (x.lt.1 .or. x.gt.rg%nx) cycle   ! J
      !
      ! Set the user coordinate
      is = is+1
      sample(is) = xx
      !
      ! Find nearest pixel I and interpolate between I-1, I and I+1
      i = nint(x)
      k = (j-1)*rg%nx+i
      if (i.eq.1) then
        value(j) = r(k) + (r(k+1)-r(k))*(x-1.)
      elseif (i.eq.rg%nx) then
        value(j) = r(k) + (r(k)-r(k-1)) * (float(rg%nx)-x)
      else
        vy1 = r(k-1)
        vy2 = r(k)
        vy3 = r(k+1)
        vx = x-i
        value(is) = ( (vy1+vy3-2*vy2)*vx + vy3-vy1 )/2.*vx + vy2
      endif
    enddo  ! J
  endif
  !
  if (is.ne.0) then
    nxy = is
    return
  endif
  !
99 call greg_message(seve%e,'STRIP','Outside map boundaries')
  error = .true.
end subroutine sloppy
