
/* Define entry point "initpygreg" for command "import pygreg" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(greg);
