module greg_interfaces_private
  interface
    subroutine greg_arrow(line,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support file for command
      !   G\ARROW Xref Yref Length Angle [Reference]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine greg_arrow
  end interface
  !
  interface
    subroutine greg_arrow_desc(xdesc,ydesc,ldesc,adesc,reference,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Incarnate the arguments as R*4
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: xdesc
      type(sic_descriptor_t), intent(in)    :: ydesc
      type(sic_descriptor_t), intent(in)    :: ldesc
      type(sic_descriptor_t), intent(in)    :: adesc
      real(kind=4),           intent(in)    :: reference
      logical,                intent(inout) :: error
    end subroutine greg_arrow_desc
  end interface
  !
  interface
    subroutine greg_arrow_do(xdesc,ydesc,ldesc,adesc,reference,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Incarnate the arguments as R*4
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: xdesc
      type(sic_descriptor_t), intent(in)    :: ydesc
      type(sic_descriptor_t), intent(in)    :: ldesc
      type(sic_descriptor_t), intent(in)    :: adesc
      real(kind=4),           intent(in)    :: reference
      logical,                intent(inout) :: error
    end subroutine greg_arrow_do
  end interface
  !
  interface
    subroutine greg_axis(line,error)
      use gbl_message
      use greg_axes
      use greg_kernel
      use greg_pen
      use greg_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routines for commands
      !   AXIS  XLow|XUp|YLeft|YRight [A1 A2]
      ! 1       /TICK ICLOCK SMALL BIG
      ! 2       /LABEL POSITION [FORMAT]
      ! 3       /LOCATION   X Y LENGTH
      ! 4-5     /[NO]LOGARITHM
      ! 6       /ABSOLUTE
      ! 7       /UNIT Angle
      ! 8-9     /[NO]BRIEF
      ! --- to be implemented ---
      !                       /WINDOW AMIN AMAX
      !   TICKSPACE  [X SMALL BIG]  [Y SMALL BIG]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_axis
  end interface
  !
  interface
    subroutine greg_rule(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support for command RULE
      !  RULE [X] [Y] [/MAJOR] [/MINOR]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine greg_rule
  end interface
  !
  interface
    subroutine rulexy(major,ixy,error)
      use phys_const
      use greg_axes
      use greg_kernel
      use greg_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !  RULE [X] [Y] [/MAJOR] [/MINOR]
      ! Arguments :
      !  IXY    For ruling X or Y ticks
      !    1 Rule X ticks
      !    2 Rule Y ticks
      !    3 Rule both X and Y ticks
      !---------------------------------------------------------------------
      logical,         intent(in)    :: major  ! T for lines at labelled ticks, F for all ticks
      integer(kind=4), intent(in)    :: ixy    ! For ruling X or Y ticks (1:X, 2:Y, 3:both)
      logical,         intent(inout) :: error  ! Logical flag
    end subroutine rulexy
  end interface
  !
  interface
    subroutine plot_axis(a1,a2,small,big,ax,ay,alen,axis,error)
      use greg_kernel
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      !  Plot an axis: line+ticks and/or tick labels
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: a1     ! User coordinate value at start point of axis
      real(kind=8), intent(in)    :: a2     ! User coordinate value at end point of axis
      real(kind=8), intent(in)    :: small  ! Minor tick space
      real(kind=8), intent(in)    :: big    ! Major (labelled) tick space
      real(kind=4), intent(in)    :: ax     ! Page coordinates of start point of axis
      real(kind=4), intent(in)    :: ay     ! Page coordinates of start point of axis
      real(kind=4), intent(in)    :: alen   ! Length of axis
      type(axis_t), intent(inout) :: axis   ! Parameters for labels and ticks
      logical,      intent(inout) :: error  ! Error flag
    end subroutine plot_axis
  end interface
  !
  interface
    subroutine plot_at_tick(a1,a2,small,big,ax,ay,alen,axis,func,error)
      use gbl_message
      use greg_kernel
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      !  Execute the input function at each tick position
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: a1     ! User coordinate value at start point of axis
      real(kind=8), intent(in)    :: a2     ! User coordinate value at end point of axis
      real(kind=8), intent(in)    :: small  ! Minor tick space
      real(kind=8), intent(in)    :: big    ! Major (labelled) tick space
      real(kind=4), intent(in)    :: ax     ! Page X coordinates of start point of axis
      real(kind=4), intent(in)    :: ay     ! Page Y coordinates of start point of axis
      real(kind=4), intent(in)    :: alen   ! Length of axis
      type(axis_t), intent(inout) :: axis   ! Parameters for labels and ticks
      external                    :: func   ! Tick drawing function
      logical,      intent(inout) :: error  ! Logical flag
    end subroutine plot_at_tick
  end interface
  !
  interface
    subroutine plot_tickmark(tick,axis)
      use greg_kernel
      use greg_types
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Plot one tick mark
      !---------------------------------------------------------------------
      type(tick_t), intent(in) :: tick    ! Tick description
      type(axis_t), intent(in) :: axis     ! Axis description
    end subroutine plot_tickmark
  end interface
  !
  interface
    subroutine plot_ticklabel(tick,axis)
      use phys_const
      use greg_kernel
      use greg_types
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Plot one tick label
      !---------------------------------------------------------------------
      type(tick_t), intent(in) :: tick  ! Tick description
      type(axis_t), intent(in) :: axis  ! Axis description
    end subroutine plot_ticklabel
  end interface
  !
  interface
    subroutine plot_rulexy(tick,axis)
      use greg_types
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Used by command RULE for gridding the plot page
      !---------------------------------------------------------------------
      type(tick_t), intent(in) :: tick  ! Tick description
      type(axis_t), intent(in) :: axis  ! Axis description
    end subroutine plot_rulexy
  end interface
  !
  interface
    subroutine greg_tickspace(line,error)
      use greg_axes
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  Support routine for command
      !  TICKSPACE SMALLX BIGX SMALLY BIGY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_tickspace
  end interface
  !
  interface
    subroutine tick_offset(sexage,v1,v2,brief,o)
      use gbl_message
      use greg_kernel
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      ! Custom rounding function. Takes 2 floats as input and return
      ! the most common value.
      ! - Example 1: d1=111259. and d2=111335.
      !    We want to return 111000. There is trap here: dx<100 but we have
      !    to round to the 1000s.
      ! - Example 2: d1=123456. and d2=3.
      !    We want to return 0.
      ! - Example 3: d1=123456. and d2=-120000.
      !    We want to return 0.
      ! - Example 4: d1=123456.789 and d2=d1
      !    We want to return d1
      !---------------------------------------------------------------------
      logical,      intent(in)  :: sexage  ! Sexagesimal notation?
      real(kind=8), intent(in)  :: v1,v2   !
      logical,      intent(out) :: brief   !
      real(kind=8), intent(out) :: o       !
    end subroutine tick_offset
  end interface
  !
  interface
    subroutine bars(line,error)
      use gildas_def
      use sic_types
      use greg_kernel
      use greg_xyz
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for command
      !	ERRORBAR [+X] [X] [-X] [+Y] [Y] [-Y] [[+/-]O] [X_var Y_var Z_var]
      !	Draw error bars at data point location. The error bar size is
      !	defined by array Z value.
      !---------------------------------------------------------------------
      character(len=*)               :: line   ! Input command line
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine bars
  end interface
  !
  interface
    subroutine greg_box(line,error)
      use gildas_def
      use phys_const
      use gbl_message
      use greg_axes
      use greg_kernel
      use greg_pen
      use greg_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !   BOX [Parallel [Orthogonal [In]]]  [/ABSOLUTE]
      !       [Orthogonal [Parallel [Out]]] [/UNIT Angle]
      !       [None [None [None]]]
      !                                     [/LABEL Pen]
      !                                     [/FILL Color]
      !                                     [/BRIEF [X|Y]]
      !                                     [/NOBRIEF [X|Y]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_box
  end interface
  !
  interface
    subroutine greg_box_brief(line,axis,error)
      use gbl_message
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   G\BOX /BRIEF|NOBRIEF [X|Y]
      ! Parse the command line options /BRIEF and /NOBRIEF. They are
      ! compatible as long as the same axis is not used twice, like e.g.
      ! in /BRIEF X /NOBRIEF Y
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      type(axis_t),     intent(inout) :: axis(4)  ! Axes parameters
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine greg_box_brief
  end interface
  !
  interface
    subroutine defdel_xyz(action,i,error)
      use gbl_message
      use sic_types
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      !  Define or delete one of the X, Y, or Z Greg variables. An error is
      ! raised if the variable already exists and was not created by Greg.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: action  ! 0=check, 1=define, 2=delete
      integer(kind=4), intent(in)    :: i       ! 1=X, 2=Y, 3=Z
      logical,         intent(inout) :: error   ! Logical error flag
    end subroutine defdel_xyz
  end interface
  !
  interface
    subroutine delete_xyz(delv,error)
      use gildas_def
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! Delete the SIC variables named X, Y and Z
      !---------------------------------------------------------------------
      logical, intent(in)    :: delv
      logical, intent(inout) :: error
    end subroutine delete_xyz
  end interface
  !
  interface
    subroutine create_xyz(error)
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      !  Create the SIC variables named X, Y and Z, if they are not yet
      ! defined. If yes, they are assumed to point to the correct memory
      ! object (checked in delete_xyz).
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine create_xyz
  end interface
  !
  interface
    subroutine more_xyz(n,error)
      use gildas_def
      use gbl_message
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! Re-allocate more buffer space for the internal X, Y and Z buffers
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n      !
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine more_xyz
  end interface
  !
  interface
    subroutine column(line,error)
      use gildas_def
      use image_def
      use greg_xyz
      use greg_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  Support routine for command
      !       COLUMN [X nx] [Y ny] [Z nz]
      !  1           [/FILE Filename]
      !  2           [/LINE L1 L2]
      !  3           [/TABLE Tablename]
      !  4           [/COMMENT "Separator"]
      !  5           [/CLOSE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine column
  end interface
  !
  interface
    subroutine readcol(ncx,arrx,ncy,arry,ncz,arrz,nxy,mxy,error,icode,com)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Read up to 3 columns in the input file. Uses list-directed format
      ! but avoid comment lines beginning with a comment character (default
      ! "!", but user selectable).
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: ncx      ! Column number of X values
      real(kind=8),     intent(out)   :: arrx(*)  ! X array of values
      integer(kind=4),  intent(in)    :: ncy      ! Column number of Y values
      real(kind=8),     intent(out)   :: arry(*)  ! Y array of values
      integer(kind=4),  intent(in)    :: ncz      ! Column number of Z values
      real(kind=8),     intent(out)   :: arrz(*)  ! Z array of values
      integer(kind=4),  intent(out)   :: nxy      ! Number of values read
      integer(kind=4),  intent(in)    :: mxy      ! Maximum number of values
      logical,          intent(inout) :: error    ! Logical error flag
      integer(kind=4),  intent(out)   :: icode    ! Return code
      character(len=1), intent(in)    :: com      ! Comment character
    end subroutine readcol
  end interface
  !
  interface
    subroutine gwrite(val,x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! GREG	Internal routine
      !	This subroutine writes the value VAL at location X,Y in User
      ! 	Coordinates. VAL is encoded in shortest format.
      ! Arguments :
      !	VAL	R*4	Value to be encoded
      !	X	R*4	X coordinate
      !	Y	R*4	Y coordinate
      !---------------------------------------------------------------------
      real*4 :: val                     !
      real*4 :: x                       !
      real*4 :: y                       !
    end subroutine gwrite
  end interface
  !
  interface
    subroutine conecd (arg,string,ns)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	Encode the ARG value in the string STRING in the shortest
      !	possible format
      ! Arguments :
      !	ARG	R*4	Value to be encoded
      !	STRING	C*(*)	String to receive the coded value
      !	NS	I	Length of string
      ! Subroutines :
      !	(SIC)
      !---------------------------------------------------------------------
      real :: arg                       !
      character(len=*) :: string        !
      integer :: ns                     !
    end subroutine conecd
  end interface
  !
  interface
    subroutine gconne(line,error)
      use gildas_def
      use sic_types
      use greg_kernel
      use greg_xyz
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !   CONNECT/BLANKING Bval Eval
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gconne
  end interface
  !
  interface
    subroutine get_incarnation(command,line,form,ixy,xinca,yinca,error)
      use gildas_def
      use sic_types
      use greg_xyz
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GreG Internal routine
      !   Get incarnation of variables in command line, defaulting to X Y
      ! Warning: this routine may be called with more than 3 arguments on
      ! the command line (see RGDATA command).
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: command  !
      character(len=*),          intent(in)    :: line     !
      integer(kind=4),           intent(inout) :: form     !
      integer(kind=size_length), intent(out)   :: ixy      !
      type(sic_descriptor_t),    intent(out)   :: xinca    !
      type(sic_descriptor_t),    intent(out)   :: yinca    !
      logical,                   intent(inout) :: error    !
    end subroutine get_incarnation
  end interface
  !
  interface
    subroutine conmap(line,error)
      use gbl_message
      use gildas_def
      use greg_contours
      use greg_kernel
      use greg_pen
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !     RGMAP
      !   1     /ABSOLUTE aval
      !   2     /PERCENT  pval
      !   3     /KEEP filename [blank]
      !   4     /BLANKING bval Eval
      !   5     /GREY First nstep
      !   6     /PENS pos neg
      !   7     /ONLY Positive|Negative 
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine conmap
  end interface
  !
  interface
    subroutine ext001(a,nx,ny,b,kx,ky,minx,maxx,miny,maxy)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: nx        !
      integer(kind=4), intent(in) :: ny        !
      real(kind=4),    intent(in) :: a(nx,ny)  !
      integer(kind=4), intent(in) :: kx        !
      integer(kind=4), intent(in) :: ky        !
      real(kind=4)                :: b(kx,ky)  ! inout?
      integer(kind=4), intent(in) :: minx      !
      integer(kind=4), intent(in) :: maxx      !
      integer(kind=4), intent(in) :: miny      !
      integer(kind=4), intent(in) :: maxy      !
    end subroutine ext001
  end interface
  !
  interface
    subroutine frstd (icode,error)
      use gbl_message
      use greg_contours
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  For each contour level, puts (X,Y) Coordinates of contour into
      ! arrays XU,YU. Then plots the contour line using CONNECT
      !  Take care of grey scale contouring on SECAPA.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: icode  !
      logical,         intent(inout) :: error  !
    end subroutine frstd
  end interface
  !
  interface
    subroutine vectd(x,y,error)
      use greg_contours
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  For each contour level, puts (X,Y) Coordinates of contour into
      ! arrays XU,YU. Then plots the contour line using CONNECT
      !  Middle of contour line; Convert to user coordinates
      !  Return if still some place left in buffer
      !---------------------------------------------------------------------
      real(kind=4), intent(in)    :: x      !
      real(kind=4), intent(in)    :: y      !
      logical,      intent(inout) :: error  !
    end subroutine vectd
  end interface
  !
  interface
    subroutine lastd(error)
      use gildas_def
      use greg_contours
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  End of contour line; Plots the contour line using CONNECT.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine lastd
  end interface
  !
  interface
    subroutine contd(error)
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  End of contour line; Show it
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine contd
  end interface
  !
  interface
    subroutine frstg(icode,error)
      use gildas_def
      use gbl_message
      use greg_contours
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !   For each contour level, puts (X,Y) Coordinates of contour into
      ! arrays XU,YU. For Grey-scale.
      !   Call INIT_TREE to reset the filling capability
      !---------------------------------------------------------------------
      integer(kind=4)                :: icode  ! Unused
      logical,         intent(inout) :: error  !
    end subroutine frstg
  end interface
  !
  interface
    subroutine vectg(x,y,error)
      use gildas_def
      use gbl_message
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Middle of contour line; Convert to user coordinates.
      !  We should also clip here... If clipping applies, then a pseudo-end
      ! of contour appears
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: x      !
      real(kind=4), intent(in) :: y      !
      logical                  :: error  !
    end subroutine vectg
  end interface
  !
  interface
    subroutine lastg(error)
      use gildas_def
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  End of contour line; Plots the contour line using FILL_VECT (to
      ! put a vector) and FILL_CONT (to end a contour)
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine lastg
  end interface
  !
  interface
    subroutine contg(error)
      use gildas_def
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! End of contour line; Show it using FLUSH_TREE
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine contg
  end interface
  !
  interface
    subroutine press_ctrlc(error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine press_ctrlc
  end interface
  !
  interface
    subroutine grisclip(x,y,c)
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for the clipping algorithm ; called from DRAW and
      ! GRPOLY only. C is a 4 bit code indicating the relationship between
      ! point (X,Y) and the window boundaries ; 0 implies the point is
      ! within the window.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: x  ! Position of point
      integer(kind=4), intent(in)  :: y  ! Position of point
      integer(kind=4), intent(out) :: c  ! Code
    end subroutine grisclip
  end interface
  !
  interface
    subroutine greg_corners(line,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   CORNERS
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_corners
  end interface
  !
  interface
    subroutine curse(line,error)
      use gildas_def
      use phys_const
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_pen
      use greg_rg
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for command
      !	DRAW	Code
      !		LINE	[X Y]	[/USER]	[/BOX]	[/CHARACTER] [/CLIP]
      !		MARKER
      !		ARROW
      !		TEXT	[X Y "texte a ecrire" [Center [Orientation]]]
      !		VALUE
      ! Uses the interactive cursor to perform various functions.
      !	The cursor routine of GREG is GR_CURS
      ! Arguments :
      !	LINE	C*(*)	Command line		Input
      !	ERROR	L	Logical error flag	Output
      !
      ! Force the line style to solid for the end of an arrow	PV, 17-jul-87
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine curse
  end interface
  !
  interface
    subroutine draw_parse_coordmode(line,kbox,ibox,error)
      use gbl_message
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Parse command line to retrieve the coordinates conversion mode, i.e.
      ! how the input coordinates have to be understood
      !
      !  kbox < 0: user coordinates
      !  kbox = 0: physical coordinates from box "corner"
      !  kbox = 1: character coordinates from box "corner"
      !
      !  ibox = -1   irrelevant
      !  ibox = 1-9  the box "corner" to be used if relevant
      !  ibox = 0    automatic corner choice if cursor mode, corner 1 otherwise
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(out)   :: kbox
      integer(kind=4),  intent(out)   :: ibox
      logical,          intent(inout) :: error
    end subroutine draw_parse_coordmode
  end interface
  !
  interface
    subroutine draw_setcursor_relative(ckind,v,kbox,ibox,error)
      use phys_const
      use gbl_message
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current cursor position (user and physical) according to
      ! relative coordinate 'v' and system of coordinates (kbox,ibox)
      !
      !  kbox < 0: user coordinates
      !  kbox = 0: physical coordinates from box "corner"
      !  kbox = 1: character coordinates from box "corner"
      !  kbox = 2: absolute physical coordinates on page
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: ckind  ! Coordinate kind ('X' or 'Y')
      real(kind=8),     intent(in)    :: v
      integer(kind=4),  intent(in)    :: kbox,ibox
      logical,          intent(inout) :: error
    end subroutine draw_setcursor_relative
  end interface
  !
  interface
    subroutine draw_setcursor_absolute(xc,yc,kbox,error)
      use phys_const
      use gbl_message
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current cursor position (user and physical) according to
      ! absolute coordinates (xc,yc) and system of coordinates (kbox)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: xc,yc  ! Sexagesimal absolute coordinates
      integer(kind=4),  intent(in)    :: kbox   !
      logical,          intent(inout) :: error  !
    end subroutine draw_setcursor_absolute
  end interface
  !
  interface
    subroutine arrow(xa,ya)
      use gildas_def
      use phys_const
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	Draws an arrow from current pen position to (XA,YA) (User)
      !	Entry GARROW does the same thing for paper coordinates (XZ,YZ)
      ! Arguments :
      !	XA,YA	R*8	User coordinates		Input
      !	XZ,YZ	R*4	Plot coordinates		Input
      ! Subroutines :
      !	GDRAW
      !
      ! Force the line style to solid for the end of an arrow	PV, 17-jul-87
      !---------------------------------------------------------------------
      real(kind=8) :: xa                      !
      real(kind=8) :: ya                      !
    end subroutine arrow
  end interface
  !
  interface
    subroutine regval(z,xa,ya,value,lecri)
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG 	Internal routine
      !	Computes the Regular Grid array value at user coordinates XA,YA
      ! Arguments :
      !	Z	R*4 (*)	Regular grid array		Input
      !	XA	R*8	X user coordinate 		Input
      !	YA	R*8	Y user coordinate		Input
      !	VALUE	R*4	Value at this position		Output
      !	LECRI	L	Logical flag for writing	Input
      !---------------------------------------------------------------------
      real(kind=4) :: z(*)                    !
      real(kind=8) :: xa                      !
      real(kind=8) :: ya                      !
      real(kind=4) :: value                   !
      logical :: lecri                  !
    end subroutine regval
  end interface
  !
  interface
    function voisin(x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !  Find the nearest of the 9 significant points in BOX
      !---------------------------------------------------------------------
      integer(kind=4) :: voisin        !
      real(kind=4), intent(in) :: x,y  ! Absolute position of point
    end function voisin
  end interface
  !
  interface
    subroutine corner(k,x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !  Returns the coordinate of the significant point number K
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: k    ! Point number
      real(kind=4),    intent(out) :: x,y  ! Coordinates
    end subroutine corner
  end interface
  !
  interface
    function centre(x,y)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      ! Find the appropriate centering option
      !---------------------------------------------------------------------
      integer(kind=4) :: centre        !
      real(kind=4), intent(in) :: x,y  ! Absolute position of point
    end function centre
  end interface
  !
  interface
    subroutine arraydraw2(code,cra,la,cdec,ld,error)
      use phys_const
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !     Draw what CODE indicates, at absolute position defined by
      !     CRA and CDEC.
      !
      !     CODE    C*(*)    Drawing action (Relocate, Line, Marker, Arrow)
      !     CRA     C*(*)    String for X position
      !     LA      I        Length of CRA
      !     CDEC    C*(*)    String for Y position
      !     LD      I        Length of CDEC
      !---------------------------------------------------------------------
      character(len=1)                :: code   !
      character(len=*)                :: cra    !
      integer(kind=4)                 :: la     !
      character(len=*)                :: cdec   !
      integer(kind=4)                 :: ld     !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine arraydraw2
  end interface
  !
  interface
    subroutine draw_text_relative(x,y,string,nchar,kbox,ic,doclip_soft)
      use phys_const
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a TEXT, at position X,Y in coordinate system KBOX
      !---------------------------------------------------------------------
      real(kind=8),     intent(in) :: x,y          ! Coordinates
      character(len=*), intent(in) :: string       ! Text to be drawn
      integer(kind=4),  intent(in) :: nchar        ! Length of STRING
      integer(kind=4),  intent(in) :: kbox         ! Coordinate system
      integer(kind=4),  intent(in) :: ic           ! Centering code
      logical,          intent(in) :: doclip_soft  ! Soft clipping enabled?
    end subroutine draw_text_relative
  end interface
  !
  interface
    subroutine garrow2(xz,yz)
      use phys_const
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	Draws an arrow from current pen position to (XZ,YZ)
      !	in paper coordinates (XZ,YZ)
      ! Arguments :
      !	XZ,YZ	R*4	Plot coordinates		Input
      !
      ! Does not change the line style to solid for the end of an arrow
      ! since it would create spurious segments
      !---------------------------------------------------------------------
      real(kind=4) :: xz                      !
      real(kind=4) :: yz                      !
    end subroutine garrow2
  end interface
  !
  interface
    subroutine print_position (w_angle)
      use phys_const
      use greg_kernel
      use greg_wcs
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !     Print current cursor position
      !
      !     W_ANGLE  I        Coordinate system                         Input
      !---------------------------------------------------------------------
      integer(kind=4) :: w_angle                !
    end subroutine print_position
  end interface
  !
  interface
    subroutine print_line(line,kbox,ibox,chain,fact)
      use phys_const
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Re-write the command line for a DRAW Code action
      !
      !  kbox < 0: user coordinates
      !  kbox = 0: physical coordinates from box "corner"
      !  kbox = 1: character coordinates from box "corner"
      !  kbox = 2: absolute physical coordinates on page
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: line   ! Command line
      integer(kind=4),  intent(in)  :: kbox   ! Coordinate system
      integer(kind=4),  intent(in)  :: ibox   ! Box corner (if relevant according to KBOX)
      character(len=*), intent(in)  :: chain  ! Code of action (all but TEXT)
      real(kind=4),     intent(in)  :: fact   ! Scaling factor
    end subroutine print_line
  end interface
  !
  interface
    subroutine print_text(line,kbox,ibox,chain,fact,string,ic,ang)
      use phys_const
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Re-write the command line for a DRAW TEXT action
      !
      !  kbox < 0: user coordinates
      !  kbox = 0: physical coordinates from box "corner"
      !  kbox = 1: character coordinates from box "corner"
      !  kbox = 2: absolute physical coordinates on page
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: line    ! Command line
      integer(kind=4),  intent(in)  :: kbox    ! Coordinate system
      integer(kind=4),  intent(in)  :: ibox    ! Box corner (if relevant according to KBOX)
      character(len=*), intent(in)  :: chain   ! Code of action (all but TEXT)
      real(kind=4),     intent(in)  :: fact    ! Scaling factor
      character(len=*), intent(in)  :: string  ! Text to be drawn
      integer(kind=4),  intent(in)  :: ic      ! Centering code
      real(kind=8),     intent(in)  :: ang     ! Orientation of text
    end subroutine print_text
  end interface
  !
  interface
    subroutine glook(line,error)
      use greg_kernel
      use greg_pen
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG
      !     Support for command
      !     LOOK "command"
      !
      ! Calls the cursor, and execute the specified command each time it is
      ! pressed. The command can be a procedure (e.g. @my_look) and can test
      ! the pressed character which is added at the end as first argument.
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine glook
  end interface
  !
  interface
    subroutine curve (line,error)
      use gildas_def
      use sic_types
      use greg_kernel
      use greg_xyz
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for command
      ! 	CURVE [Array_X Array_Y]
      !	/ACCURACY [en cm]
      !		Fonctions:
      !	/VARIABLE X			Y=F(X)
      !	/VARIABLE Y			X=F(Y)
      !		Courbes parametriques:
      !	/VARIABLE Z [Array_Z]		Parametre: Z
      !	/VARIABLE NUMBERING		Parametre: Numero du point
      !	/VARIABLE POLYGONAL_LENGTH	(Analytique)
      !	/VARIABLE CURVILINEAR_LENGTH	(Iteratif, non analytique)
      !
      !	/PERIODIC
      !		Suppose que les points 1 et NXY sont separes par
      !		un nombre entier de periodes:
      !		/VARIABLE X	Suppose Y(1) = Y(NXY)
      !		/VARIABLE Y	Suppose X(1) = X(NXY)
      !		Courbe parametrique, suppose
      !				X(1) = X(NXY) et Y(1)=Y(NXY)
      !	/BLANKING [Bval [Eval]]
      !
      ! Arguments :
      !	LINE	C*(*)	Command line			Input/Output
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine curve
  end interface
  !
  interface
    subroutine get_same_inca (command,line,iopt,iarg,form,ixy,xinca,error)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GreG  Internal routine
      !  Get incarnation of variables in command line
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: command  !
      character(len=*),          intent(in)    :: line     !
      integer(kind=4),           intent(in)    :: iopt     !
      integer(kind=4),           intent(in)    :: iarg     !
      integer(kind=4),           intent(in)    :: form     !
      integer(kind=size_length), intent(inout) :: ixy      !
      type(sic_descriptor_t),    intent(out)   :: xinca    !
      logical,                   intent(inout) :: error    !
    end subroutine get_same_inca
  end interface
  !
  interface
    subroutine get_greg_inca (command,name,form,ixy,xinca,error)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Get incarnation of GREG X Y or Z variable. Note that the code does
      ! not distinguish the program-defined variables (by e.g. COLUMN) from
      ! user-defined ones.
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: command  ! Command name (feedback)
      character(len=*),          intent(in)    :: name     ! Variable name
      integer(kind=4),           intent(in)    :: form     ! Desired incarnation type
      integer(kind=size_length), intent(inout) :: ixy      ! Number of elements
      type(sic_descriptor_t),    intent(out)   :: xinca    ! Incarnation descriptor
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine get_greg_inca
  end interface
  !
  interface
    subroutine rstrip(line,error)
      use gildas_def
      use gbl_message
      use greg_kernel
      use greg_rg
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      !  Make a strip across the regular grid array
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Command line, updated in return
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine rstrip
  end interface
  !
  interface
    subroutine sloppy(x1,x2,y1,y2,r,sample,value,nxy,error)
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Make a diagonal strip across the regular grid array
      !  The number of pixels is NX or NY, according to wether the slope is
      ! greater than NX/NY and interpolation in the non-sampled direction is
      ! made using a parabola fitting the 3 nearby points.
      !---------------------------------------------------------------------
      real(kind=8),    intent(in)  :: x1,y1      ! Coordinates of one point on line
      real(kind=8),    intent(in)  :: x2,y2      ! Coordinates of another point on line
      real(kind=4),    intent(in)  :: r(*)       ! Array of map value
      real(kind=8),    intent(out) :: sample(*)  ! Array of coordinates to be filled
      real(kind=8),    intent(out) :: value(*)   ! Array of values to be filled
      integer(kind=4), intent(out) :: nxy        ! Number of samples filled
      logical,         intent(out) :: error      ! Logical error flag
    end subroutine sloppy
  end interface
  !
  interface
    subroutine difsys4 (f,eps,h,x,y)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !       adapted from Prof. Dr. R. BULIRSCH, 1972-73
      !	P. VALIRON				21-AUG-80
      !	adapted for single precision		27-SEP-84
      !	Integration of first order differential equations.
      !	The equation is supposed to be of the form:
      !             DY/DX = F(X,Y)
      ! Arguments :
      !	F	Ext	User supplied subroutine. Computes
      !			the right end of (1) for a given X and Y
      !			F must contain the instructions:
      !				SUBROUTINE F(X,Y,DYDX)
      !				REAL*4 X,Y,DYDX
      !	EPS	R*4	Accuracy, kept internally > 1.E-11
      !	H	R*4	User estimate of the best step of
      !			integration, updated.
      !	X	R*4	initial abcissa.
      !	Y	R*4	Initial value of Y.
      !	After the calculation, X is replaced by X+H2 and Y by its
      !	value for the abscissa X+H2. H2 is less or equal to H, its value
      !	is chosen internally to satisfy the required accuracy EPS.
      !	The subroutine returns H equal to the best value for the next
      !	step. If the system seems to be ill conditioned, or if the initial
      !	value for H is much too high, H is returned equal to 0.
      !	( See BULIRSCH,R. and STOER,J.; NUM.MATH. /8/,1-13 (1966) )
      ! Subroutines :
      !	F	External
      !---------------------------------------------------------------------
      external :: f                     !
      real*4 :: eps                     !
      real*4 :: h                       !
      real*4 :: x                       !
      real*4 :: y                       !
    end subroutine difsys4
  end interface
  !
  interface
    subroutine grline(cxa,cya,cxb,cyb)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !   Plot a line from CXA,CYA to CXB,CYB with current pen attributes.
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: cxa  ! Plot X coordinate of start point
      real(kind=4), intent(in) :: cya  ! Plot Y coordinate of start point
      real(kind=4), intent(in) :: cxb  ! Plot X coordinate of end point
      real(kind=4), intent(in) :: cyb  ! Plot Y coordinate of end point
    end subroutine grline
  end interface
  !
  interface
    subroutine grpoly(nq,xq,yq)
      use gildas_def
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Plots a poly-line in paper coordinate, with clipping in the BOX
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nq     ! Number of points
      real(kind=4),              intent(in) :: xq(*)  ! Plot X coordinates
      real(kind=4),              intent(in) :: yq(*)  ! Plot Y coordinates
    end subroutine grpoly
  end interface
  !
  interface
    subroutine grclip (x,y,c)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !   Support routine for the clipping algorithm ; called from DRAW
      ! and GRPOLY only.  C is a 4 bit code indicating the relationship
      ! between point (X,Y) and the window boundaries ; 0 implies the
      ! point is within the window.
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: x  ! Plot X coordinate
      real(kind=4),    intent(in)  :: y  ! Plot Y coordinate
      integer(kind=4), intent(out) :: c  ! Code for clipping
    end subroutine grclip
  end interface
  !
  interface
    subroutine cross(xp,yp,xq,yq,x1,x2,y1,y2,xc,yc,xd,yd,ok)
      !---------------------------------------------------------------------
      ! @ private
      ! Clipping subroutine. Not used, kept for reference.
      !
      ! Return segment XC,YC,XD,YD where the segment XP,YP,XQ,YQ crosses the
      ! box X1,Y1,X2,Y2. Set OK to FALSE if never in the box.
      ! It is assumed that X1 < X2 and Y1 < Y2.
      !---------------------------------------------------------------------
      real(kind=4), intent(in)  :: xp,yp  ! Line to be clipped (start point)
      real(kind=4), intent(in)  :: xq,yq  ! Line to be clipped (end point)
      real(kind=4), intent(in)  :: x1,y1  ! Box BLC
      real(kind=4), intent(in)  :: x2,y2  ! Box TRC
      real(kind=4), intent(out) :: xc,yc  ! Clipped line (start point)
      real(kind=4), intent(out) :: xd,yd  ! Clipped line (end point)
      logical,      intent(out) :: ok     ! Is the clipped line visible?
    end subroutine cross
  end interface
  !
  interface
    subroutine ellipse(line,error)
      use phys_const
      use gbl_constant
      use gbl_message
      use greg_kernel
      use greg_pen
      use greg_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command:
      !   ELLIPSE Major Minor PA
      ! 1  /BOX  X Y
      ! 2  /USER X Y
      ! 3  /ARC THETA1 THETA2
      ! 4  /FILL [Icolor]
      ! 5  /HATCH [Ipen] [Angle] [Separ] [Phase]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine ellipse
  end interface
  !
  interface
    subroutine ellipse_scalar(line,box,user,abso,usunit,uscale,arc,drawing,error)
      use phys_const
      use greg_kernel
      use greg_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Draw a single ellipse, command line arguments must be scalar
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: line     ! Input command line
      logical,                 intent(in)    :: box      !
      logical,                 intent(in)    :: user     ! /USER
      logical,                 intent(in)    :: abso     ! /USER ABSO
      integer(kind=4),         intent(in)    :: usunit   !
      real(kind=8),            intent(in)    :: uscale   !
      logical,                 intent(in)    :: arc      ! /ARC
      type(polygon_drawing_t), intent(in)    :: drawing  ! Drawing description
      logical,                 intent(inout) :: error    ! Logical error flag
    end subroutine ellipse_scalar
  end interface
  !
  interface
    subroutine ellipse_array(line,uscale,arc,drawing,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      !  Draw one or several ellipses, command line arguments can be scalar
      ! or array
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: line     ! Input command line
      real(kind=8),            intent(in)    :: uscale   !
      logical,                 intent(in)    :: arc      ! /ARC
      type(polygon_drawing_t), intent(in)    :: drawing  ! Drawing description
      logical,                 intent(inout) :: error    ! Logical error flag
    end subroutine ellipse_array
  end interface
  !
  interface
    subroutine greg_extrema(line,error)
      use gildas_def
      use gbl_message
      use greg_kernel
      use greg_pen
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  [GREG2\]EXTREMA
      !  1       [/BLANKING Bval Eval]
      !  2       [/PLOT]
      !  3       [/COMPUTE]
      !  4       [/LOCAL PLOT|TERMINAL|FileName]
      ! Find the extrema of regular grid array loaded
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_extrema
  end interface
  !
  interface
    subroutine rgextr(itype,zmin,zmax,z,bval,eval,forced)
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Finds the maxima and minima for Regular Grid array and writes on
      ! screen the values and corresponding position if ITYPE = 1
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: itype   ! Type values on screen
      real(kind=4)                :: zmin    ! Minimum map value (output)
      real(kind=4)                :: zmax    ! Maximum map value (output)
      real(kind=4),    intent(in) :: z(*)    ! RG map array
      real(kind=4),    intent(in) :: bval    ! Blanking value
      real(kind=4),    intent(in) :: eval    ! Tolerance on blanking
      logical,         intent(in) :: forced  ! Force computation
    end subroutine rgextr
  end interface
  !
  interface
    subroutine rgextr_local(z,toplot,toterm,tolun,error)
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Finds local extrema of the Regular Grid array
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)    :: z(rg%nx,rg%ny)  ! Regular grid array
      logical,         intent(in)    :: toplot          ! Display on plot
      logical,         intent(in)    :: toterm          ! Display on terminal
      integer(kind=4), intent(in)    :: tolun           ! Output lun, if defined
      logical,         intent(inout) :: error           ! Logical error flag
    end subroutine rgextr_local
  end interface
  !
  interface
    subroutine grfill(line,error)
      use gildas_def
      use sic_types
      use greg_kernel
      use greg_xyz
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command FILL X Y
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine grfill
  end interface
  !
  interface
    subroutine gr_fillpoly(n,x,y)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: n     !
      real(kind=4),              intent(in) :: x(*)  !
      real(kind=4),              intent(in) :: y(*)  !
    end subroutine gr_fillpoly
  end interface
  !
  interface
    subroutine parse_polygon_drawing(rname,line,optfill,opthatch,drawing,error)
      use phys_const
      use gbl_message
      use greg_types
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      ! Parse the following options in the command line:
      !   /FILL [Icolor]
      !   /HATCH [Ipen] [Angle] [Separ] [Phase]
      ! Defaults for the arguments are defined here.
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: rname     !
      character(len=*),        intent(in)    :: line      !
      integer(kind=4),         intent(in)    :: optfill   ! Number of option /FILL
      integer(kind=4),         intent(in)    :: opthatch  ! Number of option /HATCH
      type(polygon_drawing_t), intent(out)   :: drawing   !
      logical,                 intent(inout) :: error     ! Logical error flag
    end subroutine parse_polygon_drawing
  end interface
  !
  interface
    subroutine reallocate_poly_buffers(npts,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! 'npts' is the number of points of the input polygon
      ! This subroutine will allocate:
      ! - npts+1 values (so that the caller can close the polygon) for the
      !   unclipped buffer.
      ! - clipping buffer: npts*3/2+1, because clipping may add more
      !   summits, and this is the worst case.
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)    :: npts
      logical,                   intent(inout) :: error
    end subroutine reallocate_poly_buffers
  end interface
  !
  interface
    subroutine flimit
      use gbl_message
      use greg_image
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !
      !	This subroutine computes the limits of the Regular Grid array.
      !	It tries to handle "Astronomical Maps" as best as possible (!).
      !---------------------------------------------------------------------
    end subroutine flimit
  end interface
  !
  interface
    subroutine greg_point(line,error)
      use gildas_def
      use sic_types
      use greg_kernel
      use greg_xyz
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    POINTS [Array__X Array__Y]
      ! 1    [/BLANKING [Bval [Eval]]]
      ! 2    [/SIZE MaxVal [Array__Z [Expo]]]
      ! 3    [/MARKER]
      !
      !  Draw markers of current style and size. If MaxVal is given, draws
      ! symbols of current style but of size proportional to Z value at
      ! positions (X,Y). The current marker size corresponds to points with
      ! value MaxVal.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_point
  end interface
  !
  interface
    subroutine threed (line,error)
      use gildas_def
      use gbl_message
      use greg_axes
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !
      ! PERSPECTIVE [Altitude Azimuth]
      ! 1           [/FACTOR factor offset]
      ! 2           [/LOWER]
      ! 3           [/LINES]
      ! 4           [/STEP nx ny]
      ! 5           [/AXES Xmin Xmax Ymin Ymax]
      ! 6           [/CENTER]
      ! 7           [/VERTICAL Vmin Vmax]
      !
      ! 3D perspective plot, define the projective transformation
      !
      ! ALTitude      : degrees
      ! AZImuth       : degrees
      ! XLEN and YLEN : Length of X and Y axes (cf SET LOCATION)
      ! XOFF and YOFF : Origin position
      ! ZFACtor       : Conversion factor from Z to physical units
      ! ZOFF          : Origine offset along Z (physical units)
      !---------------------------------------------------------------------
      character(len=*) :: line          ! Command line               Input
      logical :: error                  ! Logical error flag         Output
    end subroutine threed
  end interface
  !
  interface
    subroutine pswap (xin,nx,ny,xout,mx,my,ix1,ix2,ix3,iy1,iy2,iy3)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: nx                     !
      integer :: ny                     !
      real :: xin(nx,ny)                !
      integer :: mx                     !
      integer :: my                     !
      real :: xout(mx,my)               !
      integer :: ix1                    !
      integer :: ix2                    !
      integer :: ix3                    !
      integer :: iy1                    !
      integer :: iy2                    !
      integer :: iy3                    !
    end subroutine pswap
  end interface
  !
  interface
    subroutine plt3d(a,m,n,x,y,l,alt,az,xlen,xoff,ylen,yoff,zfac,zoff,blnk3d,  &
      rgblank,eblank,ier)
      use greg_axes
      !---------------------------------------------------------------------
      ! @ private
      ! 3D plot routine, adapted from SAS-3 routine of same name
      !---------------------------------------------------------------------
      integer :: m     ! Dimensions of data array A
      integer :: n     ! Dimensions of data array A
      real :: a(m,n)   ! Array, represents height of surface as function of location in plane
      integer :: l     ! Length of work arrays, .GE. 2*MIN(M,N)
      real :: x(l)     ! Real work array
      real :: y(l)     ! Real work array
      real :: alt      ! Altitude viewing angle in degrees
      real :: az       ! Azimuth viewing angle in degrees
      real :: xlen     ! Length of unprojected X axis in plot coordinates
      real :: xoff     ! Offset of origin in plot coordinates
      real :: ylen     ! Length of unprojected Y axis in plot coordinates
      real :: yoff     ! Offset of origin in plot coordinates
      real :: zfac     ! Scaling of Z-axis from data units to unprojected plot coordinates
      real :: zoff     ! Offset of Z-origin in plot coordinates
      real :: blnk3d   !
      real :: rgblank  !
      real :: eblank   !
      integer :: ier   ! Returns 0 for no error
    end subroutine plt3d
  end interface
  !
  interface
    subroutine nxtvu(ic,x,y,n,ier)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Internal routine
      ! Find the next vector visible and plot it.
      !---------------------------------------------------------------------
      integer :: ic           ! Integer code                         Input
      integer :: n            ! Length of X and Y arrays             Input
      real :: x(n)            ! Work array
      real :: y(n)            ! Work array
      integer :: ier          ! Error code                           Output
    end subroutine nxtvu
  end interface
  !
  interface
    subroutine outp(x,y,ier)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Internal routine
      ! Buffers the vector to keep track of vectors plotted
      !---------------------------------------------------------------------
      real :: x                         !              Input
      real :: y                         !              Input
      integer :: ier                    ! Error code   Output
    end subroutine outp
  end interface
  !
  interface
    function alin(x0,x1,y0,y1,x)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Internal routine
      ! Find the Y coordinate of absissae X along line (X0,Y0)-->(X1,Y1)
      !---------------------------------------------------------------------
      real :: alin          ! Y coordinate of point                 Output
      real :: x0            ! X coordinate of start point           Input
      real :: x1            ! X coordinate of end point             Input
      real :: y0            ! Y coordinate of start point           Input
      real :: y1            ! Y coordinate of end point             Input
      real :: x             ! X coordinate of point                 Input
    end function alin
  end interface
  !
  interface
    subroutine sub_axes(a,m,n,a3,a2,a1,b1,b2,b3,b4,saz,caz,blnk3d)
      use greg_pen
      use greg_axes
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Internal routine
      ! Cette routine trace les axes sur la vue en perspective lorsque
      ! l'option /AXES est presente dans la commande PERSPECTIVE.
      !---------------------------------------------------------------------
      integer :: m                      !
      integer :: n                      !
      real :: a(m,n)                    !
      real :: a3                        !
      real :: a2                        !
      real :: a1                        !
      real :: b1                        !
      real :: b2                        !
      real :: b3                        !
      real :: b4                        !
      real :: saz                       !
      real :: caz                       !
      real :: blnk3d                    !
    end subroutine sub_axes
  end interface
  !
  interface
    subroutine traxes (alpha,beta,ex,exx,zlxx,zlyy,ey,eyy,ez,ezz,vz,zlzz)
      use greg_axes
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Internal routine
      ! Cette routine est utilisee par la routine SUB_AXES pour afficher ces
      ! axes
      !---------------------------------------------------------------------
      real :: alpha              ! Orientation de l'axe des abscisses
      real :: beta               ! Orientation de l'axe des ordonnees
      real :: ex                 ! abscisse du coin apparaissant au centre
      real :: exx                ! ordonnee du coin apparaissant au centre
      real :: zlxx               ! longueur de l'axe des abscisses
      real :: zlyy               ! longueur de l'axe des ordonnees
      real :: ey                 ! abscisse du coin apparaissant a gauche
      real :: eyy                ! ordonnee du coin apparaissant a gauche
      real :: ez                 ! abscisse du coin apparaissant a droite
      real :: ezz                ! ordonnee du coin apparaissant a droite
      real :: vz                 ! ordonnee du bas de l'axe vertical
      real :: zlzz               ! longueur de l'axe des donnees
    end subroutine traxes
  end interface
  !
  interface
    subroutine echange (xl1,xl2,yl1,yl2)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real*4 :: xl1                     !
      real*4 :: xl2                     !
      real*4 :: yl1                     !
      real*4 :: yl2                     !
    end subroutine echange
  end interface
  !
  interface
    subroutine permute (val1,val2)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real*4 :: val1                    !
      real*4 :: val2                    !
    end subroutine permute
  end interface
  !
  interface
    subroutine greg3_close
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  -- Support routine for command GREG3\IMAGE [Name]
      !   Close the current image
      !---------------------------------------------------------------------
      ! Global
    end subroutine greg3_close
  end interface
  !
  interface
    subroutine greg3_image(line,error)
      use gildas_def
      use gbl_message
      use gbl_format
      use gbl_constant
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  -- Support routine for command
      !  GREG3\IMAGE [Name]
      !  1     [/PLANE I1 I2]
      !  2     [/SUBSET Imin Imax Jmin Jmax]
      !  3     [/WRITE]
      !  4     [/CLOSE]
      !---------------------------------------------------------------------
      character(len=*)                :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg3_image
  end interface
  !
  interface
    subroutine greg3_spectrum(line,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for the 2 alternate syntaxes of command
      !     GREG3\SPECTRUM
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg3_spectrum
  end interface
  !
  interface
    subroutine greg3_spectrum_extract(line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS      Support routine for command
      !     GREG3\SPECTRUM I,J,K
      !---------------------------------------------------------------------
      character(len=*) :: line          ! Input command line
      logical :: error                  ! Logical error flag
    end subroutine greg3_spectrum_extract
  end interface
  !
  interface
    subroutine greg3_kill(line,error)
      use gildas_def
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !   GRGE3\KILL
      !  Call the cursor, and display the map value or kill the pixel
      ! depending on the cursor code
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine greg3_kill
  end interface
  !
  interface
    subroutine killr(xarray,pixel,code)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    GREG3\KILL
      !  Display the map value or kill the pixel depending on the cursor
      ! code.
      !---------------------------------------------------------------------
      real(kind=4),     intent(inout) :: xarray(:,:)  ! Current map
      integer(kind=4),  intent(in)    :: pixel(2)     ! Pixel coordinate
      character(len=*), intent(in)    :: code         ! Code for operation
    end subroutine killr
  end interface
  !
  interface
    subroutine greg3_variables(error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine greg3_variables
  end interface
  !
  interface
    subroutine gr4_sub_extrema(z,n,m,bval,eval,zmin,zmax,i1,i2,j1,j2)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: n                      !
      integer(kind=4) :: m                      !
      real(kind=4) :: z(n,m)                  !
      real(kind=4) :: bval                    !
      real(kind=4) :: eval                    !
      real(kind=4) :: zmin                    !
      real(kind=4) :: zmax                    !
      integer(kind=4) :: i1                     !
      integer(kind=4) :: i2                     !
      integer(kind=4) :: j1                     !
      integer(kind=4) :: j2                     !
    end subroutine gr4_sub_extrema
  end interface
  !
  interface
    subroutine subset4 (x,nx,ny,y,mx,my,i1,i2,j1,j2)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS Internal routine
      ! Extract a two dimensional subset from a two dimensional image
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in)  :: nx,ny     ! Dimensions of X
      real(kind=4),               intent(in)  :: x(nx,ny)  ! Input array
      integer(kind=4),            intent(in)  :: mx,my     ! Dimensions of Y
      real(kind=4),               intent(out) :: y(mx,my)  ! Output subset
      integer(kind=index_length), intent(in)  :: i1,i2     ! Min and Max pixels along first axis
      integer(kind=index_length), intent(in)  :: j1,j2     ! Min and Max pixels along second axis
    end subroutine subset4
  end interface
  !
  interface
    subroutine subset8(x,nx,ny,y,mx,my,i1,i2,j1,j2)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS Internal routine
      ! Extract a two dimensional subset from a two dimensional image
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in)  :: nx,ny     ! Dimensions of X
      real(kind=8),               intent(in)  :: x(nx,ny)  ! Input array
      integer(kind=4),            intent(in)  :: mx,my     ! Dimensions of Y
      real(kind=4),               intent(out) :: y(mx,my)  ! Output subset
      integer(kind=index_length), intent(in)  :: i1,i2     ! Min and Max pixels along first axis
      integer(kind=index_length), intent(in)  :: j1,j2     ! Min and Max pixels along second axis
    end subroutine subset8
  end interface
  !
  interface
    subroutine greg_kernel_init
      !--------------------------------------------------------------------
      ! @ private
      ! Initialization routine for greg_kernel module.
      ! It must be called once and only once, otherwise an error will
      ! raise and program will exit.
      !--------------------------------------------------------------------
      !
    end subroutine greg_kernel_init
  end interface
  !
  interface
    subroutine greg_mod_init
      !--------------------------------------------------------------------
      ! @ private
      ! Call to initialization routines for GREG modules.
      ! It must be called once and only once, otherwise an error will
      ! raise and program will exit.
      !--------------------------------------------------------------------
    end subroutine greg_mod_init
  end interface
  !
  interface
    subroutine gr_axis(name,narg,arg1,arg2)    ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer :: narg                   !
      real*4 :: arg1                    !
      real*4 :: arg2                    !
    end subroutine gr_axis
  end interface
  !
  interface
    subroutine gr_box(name)        ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_box
  end interface
  !
  interface
    subroutine gr_clea(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_clea
  end interface
  !
  interface
    subroutine gr_colu(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_colu
  end interface
  !
  interface
    subroutine gr_conn             ! Incomplete
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine gr_conn
  end interface
  !
  interface
    subroutine gr_devi(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_devi
  end interface
  !
  interface
    subroutine gr_erro(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_erro
  end interface
  !
  interface
    subroutine gr_hist             ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine gr_hist
  end interface
  !
  interface
    subroutine gr_poin(narg,arg1)  ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: narg                   !
      real*4 :: arg1                    !
    end subroutine gr_poin
  end interface
  !
  interface
    subroutine gr_rule(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_rule
  end interface
  !
  interface
    subroutine gr_set(name,narg,arg1,arg2,arg3,arg4)   ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer :: narg                   !
      real*4 :: arg1                    !
      real*4 :: arg2                    !
      real*4 :: arg3                    !
      real*4 :: arg4                    !
    end subroutine gr_set
  end interface
  !
  interface
    subroutine gr_show(name)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_show
  end interface
  !
  interface
    subroutine gr_tick(narg,arg1,arg2,arg3,arg4)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: narg                   !
      real*4 :: arg1                    !
      real*4 :: arg2                    !
      real*4 :: arg3                    !
      real*4 :: arg4                    !
    end subroutine gr_tick
  end interface
  !
  interface
    subroutine gr_valu             ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine gr_valu
  end interface
  !
  interface
    subroutine gr_extr             ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine gr_extr
  end interface
  !
  interface
    subroutine gr_leve(name)       ! Incomplete
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_leve
  end interface
  !
  interface
    subroutine gr_rgda(name)       ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: name          !
    end subroutine gr_rgda
  end interface
  !
  interface
    subroutine gr_rgma             ! 28-Sep-1986
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine gr_rgma
  end interface
  !
  interface
    subroutine greg_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine greg_message_set_id
  end interface
  !
  interface
    subroutine greg_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine greg_message
  end interface
  !
  interface
    subroutine greg_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Private routine to set the GREG environment.
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine greg_pack_init
  end interface
  !
  interface
    subroutine greg_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Private routine to clean the GREG environment
      ! (e.g. Remove temporary buffers, unregister languages...)
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine greg_pack_clean
  end interface
  !
  interface
    subroutine gridder (line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_rg
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  RANDOM_MAP Grid_descriptor[S]
      !  1    [/BLANKING Bval]
      !  2    [/NEIGHBOURS N]
      !  3    [/TRIANGLES]
      !  4    [/EXTRAPOLATE]
      !  5    [/VARIABLE]
      !  6    [/SKIP [TRIANGULATION] [INTERPOLATION]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gridder
  end interface
  !
  interface
    subroutine drawg(x,y)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      real :: x                         !
      real :: y                         !
    end subroutine drawg
  end interface
  !
  interface
    subroutine relog(x,y)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      real :: x                         !
      real :: y                         !
    end subroutine relog
  end interface
  !
  interface
    subroutine init_tree(icode,clipit,error)
      use greg_contours
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! (re)initialize the tree structure for later filling contour
      ! capability
      !---------------------------------------------------------------------
      integer(kind=4)              :: icode   ! Unused
      logical,         intent(out) :: clipit  !
      logical,         intent(out) :: error   !
    end subroutine init_tree
  end interface
  !
  interface
    subroutine fill_vect(x,y,error)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !     For each contour level, puts (X,Y) Coordinates of contour
      !     into tree structure
      !---------------------------------------------------------------------
      integer*4 :: x                    !
      integer*4 :: y                    !
      logical :: error                  !
    end subroutine fill_vect
  end interface
  !
  interface
    subroutine fill_cont(error)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !     End of contour line; starts an other contour
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine fill_cont
  end interface
  !
  interface
    subroutine flush_tree(error)
      use greg_contours
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !     End of contour line; Show it
      !---------------------------------------------------------------------
      logical, intent(out) :: error  !
    end subroutine flush_tree
  end interface
  !
  interface
    subroutine initialise_tree(current_leaf,error)
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private
      !     Initialisation structure : 1 noeud, 2 feuilles.
      !     la Droite sera toujours vide
      !     VECT_I 	Occupation buffer
      !     next_cont	Liste chainee sur descripteurs deb_cont et fin_cont
      !     ( La fin de liste est un element next_cont nul, ce n'est pas
      !     un contour valide )
      !     deb_cont	Premier point liste chainee X_vect, Y_vect, next_vect
      !     fin_cont	Dernier point valide !!!       (ou 0 si aucun points)
      !     X_vect	Abscisse point
      !     Y_vect	Ordonnee
      !     next_vect	Pointeur element suivant. on y va Pen Up si
      !     next_vect est <0
      !     ( La fin de liste est un element next_vect nul, ce n'est pas
      !     un point valide )
      ! A NOTER QUE DANS CETTE VERSION IL N'Y A PAS DE NOTION DE PEN_UP
      !---------------------------------------------------------------------
      integer :: current_leaf           !
      logical :: error                  !
    end subroutine initialise_tree
  end interface
  !
  interface
    subroutine gris_minmax(feuille)
      !---------------------------------------------------------------------
      ! @ private
      !	MIN MAX sur une feuille
      !---------------------------------------------------------------------
      integer :: feuille                !
    end subroutine gris_minmax
  end interface
  !
  interface
    subroutine info_create(feuille,nvect,xmin,ymin,xmax,ymax,error)
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      integer :: feuille                !
      integer :: nvect                  !
      integer :: xmin                   !
      integer :: ymin                   !
      integer :: xmax                   !
      integer :: ymax                   !
      logical :: error                  !
    end subroutine info_create
  end interface
  !
  interface
    subroutine info_write(feuille,nvect,xmin,ymin,xmax,ymax)
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      integer :: feuille                !
      integer :: nvect                  !
      integer :: xmin                   !
      integer :: ymin                   !
      integer :: xmax                   !
      integer :: ymax                   !
    end subroutine info_write
  end interface
  !
  interface
    subroutine info_read(feuille,nvect,xmin,ymin,xmax,ymax)
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      integer :: feuille                !
      integer :: nvect                  !
      integer :: xmin                   !
      integer :: ymin                   !
      integer :: xmax                   !
      integer :: ymax                   !
    end subroutine info_read
  end interface
  !
  interface
    subroutine get_triplet(nn,type,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: nn                     !
      integer :: type                   !
      logical :: error                  !
    end subroutine get_triplet
  end interface
  !
  interface
    subroutine init_search(nn,encore)
      !---------------------------------------------------------------------
      ! @ private
      ! initialise la quete sur une feuille de l'arbre
      !---------------------------------------------------------------------
      integer :: nn                     !
      logical :: encore                 !
    end subroutine init_search
  end interface
  !
  interface
    subroutine explore_tree (nn,encore)
      !---------------------------------------------------------------------
      ! @ private
      ! explore l'arbre dont les feuilles sont des contours...
      !---------------------------------------------------------------------
      integer :: nn                     !adresse feuille G ou D
      logical :: encore                 !
    end subroutine explore_tree
  end interface
  !
  interface
    subroutine bord (xa,ya,xb,yb,xmin,xmax,ymin,ymax,trigo,tx,ty,nt)
      !---------------------------------------------------------------------
      ! @ private
      !	Rajoute le bord
      !
      !	Xa, Ya		Fin de l'element de contour courant
      !	Xb, Yb		Debut du suivant
      !	Xmin, Ymin, ...	Valeur des bords
      !	Trigo		Logique, sens rotation trigo ou non.
      !	Tx(12)		Vecteur d'abcisses suivant le bord
      !	Ty(12)		Vecteur d'ordonnees suivant le bord
      !	Nt		Nombre de points suivant le bord
      !			( Tx(1) et Ty(1) sont Xa et Ya,
      !			  Tx(Nt) et Ty(Nt) sont Xb et Yb)
      !
      !	Attention -- Nt est renvoye nul sur erreur. Les valeurs possibles
      !	de Nt en sortie sont 0 (contour deja ferme) ou [2...12].
      !
      !				( PV, sous la surveillance de GD)
      !---------------------------------------------------------------------
      integer :: xa                     !
      integer :: ya                     !
      integer :: xb                     !
      integer :: yb                     !
      integer :: xmin                   !
      integer :: xmax                   !
      integer :: ymin                   !
      integer :: ymax                   !
      logical :: trigo                  !
      integer :: tx(12)                 !
      integer :: ty(12)                 !
      integer :: nt                     !
    end subroutine bord
  end interface
  !
  interface
    function modulo_gris (i,n)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: modulo_gris            !
      integer :: i                      !
      integer :: n                      !
    end function modulo_gris
  end interface
  !
  interface
    subroutine find_border(feuille,caracterize,error)
      use greg_contours
      !---------------------------------------------------------------------
      ! @ private
      ! Expansion de la feuille pour ajouter des vecteurs de bord.
      ! En d'autres termes, on veut des contours fermes...
      ! On a 4 sortes de contours disponibles: ceux fermes
      ! sur eux-meme, ceux qui partent d'un point interieur et arrivent au
      ! bord, ceux qui partent du bord et arrivent a un point interieur,
      ! et ceux qui vont de bord a bord. les categories 2 et 3, apres tri,
      ! donnent une categorie 4 par chainage; il faut maintenant chainer
      ! la categorie 4 , apres tri, dans le sens "trigo" pour "fermer
      ! le contour gris le plus exterieur".
      ! Par la suite, un algorithme simple de coupure de feuille en 2
      ! donnera toujours des contours caracterises (et donc remplissables
      ! par des traceurs even-odd fill , non-zero-winding fill et
      ! circular-oriented fill )
      ! Bord stocke dans Tx et Ty les valeurs de vecteurs (le premier
      ! etant le dernier vecteur du contour precedent, le dernier
      ! etant le premier du contour actuel, nt le nbre de vecteurs (>= 2)
      !---------------------------------------------------------------------
      integer :: feuille                !
      logical :: caracterize            !dit si on doit caracteriser le bord
      logical :: error                  !
    end subroutine find_border
  end interface
  !
  interface
    function distance(xb,yb,xmin,xmax,ymin,ymax,trigo)
      !---------------------------------------------------------------------
      ! @ private
      ! Calcule une distance sur le bord oriente entre xmin,ymin et xb,yb
      !
      !	Xb, Yb		Debut du contour
      !	Xmin, Ymin, ...	Valeur des bords
      !	Trigo		Logique, sens rotation trigo ou non.
      !---------------------------------------------------------------------
      integer :: distance               !
      integer :: xb                     !
      integer :: yb                     !
      integer :: xmin                   !
      integer :: xmax                   !
      integer :: ymin                   !
      integer :: ymax                   !
      logical :: trigo                  !
    end function distance
  end interface
  !
  interface
    subroutine coupe_feuille(feuille,force,x,y,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !	coupe une feuille en 2 feuilles + 1 noeud
      !---------------------------------------------------------------------
      integer :: feuille                !
      logical :: force                  !
      integer :: x                      !
      integer :: y                      !
      logical :: error                  !
    end subroutine coupe_feuille
  end interface
  !
  interface
    subroutine menage(feuille)
      !---------------------------------------------------------------------
      ! @ private
      ! This routine cleans the contours, first by removing consecutive,
      ! identical points, then (parts of) contour that do not close any
      ! surface (i.e, line segments), then contours of .le. two points.
      !---------------------------------------------------------------------
      integer :: feuille                !
    end subroutine menage
  end interface
  !
  interface
    subroutine check_wind(feuille,wind,x,y)
      !---------------------------------------------------------------------
      ! @ private
      ! This routine looks for imbricated contours which do not turn in the same
      ! direction, (WIND=.TRUE.) suggesting a point (X,Y) where to cut this gordian
      ! knot.
      !---------------------------------------------------------------------
      integer :: feuille                !
      logical :: wind                   !
      integer :: x                      !
      integer :: y                      !
    end subroutine check_wind
  end interface
  !
  interface
    subroutine ghisto(line,error)
      use gildas_def
      use gbl_format
      use phys_const
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_pen
      use greg_types
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   HISTOGRAM [Array_X Array_Y]
      ! 1   [/BLANKING  Bval Eval]
      ! 2   [/FILL [Icolor]]
      ! 3   [/BASE [Base]]
      ! 4   [/HATCH [Ipen] [Angle] [Separ] [Phase]]
      ! 5   [/Y]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine ghisto
  end interface
  !
  interface
    subroutine gr8_histo_base(nxy,x,y,nbase,base,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Connect a set of points with blanked values using histogram
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nxy          ! Number of points
      real(kind=8),              intent(in) :: x(nxy)       ! Array of X coordinates
      real(kind=8),              intent(in) :: y(nxy)       ! Array of Y coordinates
      integer(kind=size_length), intent(in) :: nbase        ! Number base points (1 or Nxy)
      real(kind=8),              intent(in) :: base(nbase)  ! Base value(s)
      real(kind=8),              intent(in) :: bval         ! Blanking value
      real(kind=8),              intent(in) :: eval         ! Tolerance on blanking
    end subroutine gr8_histo_base
  end interface
  !
  interface
    subroutine gr8_yxhisto_base(nxy,x,y,nbase,base,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !  Same as gr8_histo_base, but binning is along Y, and X provides the
      ! counts per bin. Bval/Eval apply on X.
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nxy          ! Number of points
      real(kind=8),              intent(in) :: x(nxy)       ! Array of X coordinates
      real(kind=8),              intent(in) :: y(nxy)       ! Array of Y coordinates
      integer(kind=size_length), intent(in) :: nbase        ! Number base points (1 or Nxy)
      real(kind=8),              intent(in) :: base(nbase)  ! Base value(s)
      real(kind=8),              intent(in) :: bval         ! Blanking value
      real(kind=8),              intent(in) :: eval         ! Tolerance on blanking
    end subroutine gr8_yxhisto_base
  end interface
  !
  interface
    subroutine greg_plot (line,error)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG
      ! Support for command PLOT
      !  [GREG2\]PLOT [Var_name]
      !     [/BLANKING Bval Eval Colour]
      !     [/SCALING Type Low_cut High_cut]
      !     [/POSITION Posx1 Posx2 Posy1 Posy2]
      !     [/BOUNDARIES Userx1 Userx2 Usery1 Usery2]
      !     [/VISIBLE]
      !
      ! Usage:
      !   Var_name is a 2-D SIC variable name. Subsets like A[4] where A
      ! is a 3-D sic variable are not yet allowed. This command is normally
      ! used in the following sequence:
      !
      !     SET BOX LOCATION Gx1 Gx2 Gy1 Gy2  ! Define box position
      !     RGDATA /VARIABLE Var_name         ! Get characteristics from image.
      !     LIMITS /RGDATA                    ! Get limits
      !     PLOT Var_name                     ! Plot the array in the box
      !     BOX
      !     RGMAP                             ! Draw contour levels (...)
      !
      ! Without argument, the RG variable is plotted
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_plot
  end interface
  !
  interface
    subroutine im_plot (line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG
      ! Support for command PLOT
      !  [GREG2\]PLOT [Var_name]
      !     [/BLANKING Bval Eval Colour]
      !     [/SCALING Type Low_cut High_cut]
      !     [/POSITION Posx1 Posx2 Posy1 Posy2]
      !     [/BOUNDARIES Userx1 Userx2 Usery1 Usery2]
      !     [/VISIBLE]
      !
      ! Usage:
      !   Var_name is a 2-D SIC variable name. Subsets like A[4] where A
      ! is a 3-D sic variable are not yet allowed. This command is normally
      ! used in the following sequence:
      !
      !     SET BOX LOCATION Gx1 Gx2 Gy1 Gy2  ! Define box position
      !     RGDATA /VARIABLE Var_name         ! Get characteristics from image.
      !     LIMITS /RGDATA                    ! Get limits
      !     PLOT Var_name                     ! Plot the array in the box
      !     BOX
      !     RGMAP                             ! Draw contour levels (...)
      !
      ! Without argument, the RG variable is plotted
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine im_plot
  end interface
  !
  interface
    subroutine plot_getvar(name,inca,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Check if the variable is something you can plot
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: name   ! Variable name
      type(sic_descriptor_t), intent(out)   :: inca   ! REAL*4 incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine plot_getvar
  end interface
  !
  interface
    subroutine setdas(n)
      use greg_pen
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routines
      !	This is a set of routines and entry points used to communicate
      !	with GREG internal commons without addressing the common
      !	themselves.
      !
      !	SETDAS	Calls GTEDIT with the dashed pattern N and other pen
      !		attributes of the current PENCIL. The modification is
      !		only valid until the next call to SETPEN. Sets PENUPD.
      !		HINT! SETDAS is inactive if the dashed pattern N has
      !		already been selected by a previous call to SETPEN or
      !		SETDAS.
      !	SETWEI	Idem for the weight attribute
      !	SETCOL	Idem for the colour attribute
      !
      !	INQDAS	Returns the current dashed pattern in use
      !		(Current pen, possibly altered by SETDAS)
      !	INQWEI	Idem for the weight attribute
      !	INQCOL	Idem for the colour attribute
      !
      !	SETPEN	Calls GTEDIT with the attributes of the current PENCIL.
      !		Resets any alterations by SETDAS, SETWEI, SETCOL.
      !		Resets the PENUPD flag which is set by PENCIL and by
      !		calls to SETDAS, SETWEI, SETCOL and SETPHYSICAL.
      !		To open a new segment with pen N :
      !			CALL GTSEGM('INQUIRE',ERROR)
      !			CALL SETPEN (N)
      !		To open a new segment with the current pen IPEN :
      !			CALL GTSEGM('INQUIRE',ERROR)
      !			IF (PENUPD) CALL SETPEN (IPEN)
      !---------------------------------------------------------------------
      integer :: n                      !
    end subroutine setdas
  end interface
  !
  interface
    subroutine setsys(system,equinox)
      use gbl_constant
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Set the internal coordinate system (and equinox if relevant)
      !  Do not use out of Greg. Use gr8_system instead.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)           :: system
      real(kind=4),    intent(in), optional :: equinox
    end subroutine setsys
  end interface
  !
  interface
    subroutine labels(line,error)
      use greg_kernel
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for command
      !	LABEL "Un texte quelconque" [Orien] /APPEND /CENTERING n 
      !       /X [Yoffset] /Y [Xoffset]
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line          ! Command line
      logical, intent(inout) :: error               ! Error flag
    end subroutine labels
  end interface
  !
  interface
    subroutine xlabel(nchar,string,off)
      use greg_kernel
      use greg_pen
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	  Write an X label
      !---------------------------------------------------------------------
      integer(4), intent(in) :: nchar                ! Number of characters
      character(len=*), intent(in) :: string         ! String
      real(4), intent(in) :: off                     ! Offset in Y from axis
    end subroutine xlabel
  end interface
  !
  interface
    subroutine ylabel(nchar,string,off)
      use phys_const
      use gbl_message
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	Write an Y label at proper position
      !---------------------------------------------------------------------
      integer(4), intent(in) :: nchar                ! Number of characters
      character(len=*), intent(in) :: string         ! String
      real(4), intent(in) :: off                     ! Offset in X from axis
    end subroutine ylabel
  end interface
  !
  interface
    subroutine putlabel(nchar,string,loc,angle,doclip)
      use phys_const
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      !  Puts a label according to location (VT100 numeric Keypad...)
      !     7 8 9
      !     4 5 6
      !     1 2 3
      ! Left, center or right in X and Y
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in) :: nchar   ! Number of characters
      character(len=*), intent(in) :: string  ! String
      integer(kind=4),  intent(in) :: loc     ! Centering option
      real(kind=8),     intent(in) :: angle   ! [deg] Text angle
      logical,          intent(in) :: doclip  ! Clip the text to the current box?
    end subroutine putlabel
  end interface
  !
  interface
    subroutine label(nchar,string,angle,doclip)
      use phys_const
      !---------------------------------------------------------------------
      ! @ private
      ! Puts a label at current location, with current angle
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in) :: nchar   ! Number of characters
      character(len=*), intent(in) :: string  ! String
      real(kind=8),     intent(in) :: angle   ! [deg] Text angle
      logical,          intent(in) :: doclip  ! Soft clipping?
    end subroutine label
  end interface
  !
  interface
    subroutine level(line,error)
      use gildas_def
      use greg_contours
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !   LEVELS n1 TO n2 BY n3 ...
      ! or
      !   LEVELS EXPO [Step [Number [Factor]]
      !
      ! Contour definition for RGMAP or RANDOM_MAP
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine level
  end interface
  !
  interface
    subroutine limits(line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_rg
      use greg_wcs
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !   LIMITS [X1 X2 Y1 Y2 [TYPE]]
      ! 1        [/XLOG]
      ! 2        [/YLOG]
      ! 3        [/RGDATA [Var_name]]
      ! 4        [/BLANKING]
      ! 5        [/REVERSE [X] [Y]]
      ! 6        [/VARIABLES Xv Yv]
      !
      ! With:
      !   Automatic computation if Xi     *
      !   Last value if                   =
      !   Union with last values          >
      !   Intersection with last values   <
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine limits
  end interface
  !
  interface
    subroutine setlim(ax1,ax2,ay1,ay2)
      use greg_kernel
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  Internal routine
      !   Setup new limits and conversion formulae
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: ax1  ! Left limit of X axis
      real(kind=8), intent(in) :: ax2  ! Right limit of X axis
      real(kind=8), intent(in) :: ay1  ! Low limit of Y axis
      real(kind=8), intent(in) :: ay2  ! High limit of Y axis
    end subroutine setlim
  end interface
  !
  interface
    subroutine limits_margin(name,logf,xl,xr,lmarg,rmarg,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Apply margins on the provided range
      !  NB: margin can be negative, i.e. truncate the [xl-xr] range
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Axis name
      logical,          intent(in)    :: logf         ! Log or linear axis?
      real(kind=8),     intent(inout) :: xl,xr        ! Current (in) and new (out) values
      real(kind=8),     intent(in)    :: lmarg,rmarg  ! Left and right margins (fraction of [xl-max])
      logical,          intent(inout) :: error        !
    end subroutine limits_margin
  end interface
  !
  interface
    subroutine exit_greg(error)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      ! Clean the GREG-related components on exit
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine exit_greg
  end interface
  !
  interface
    subroutine greg_user(flagin,array)
      use greg_kernel
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !   Give or retrieve coordinate conversion formula to/from GTVIRT.
      ! The buffer is of size 26, same as the type 'greg_values' in the
      ! GTVIRT.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: flagin     !
      integer(kind=4)             :: array(26)  !
    end subroutine greg_user
  end interface
  !
  interface
    subroutine maskon(line,error)
      use gildas_def
      use gbl_message
      use greg_kernel
      use greg_poly
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GreG Support routine for command
      !  GREG2\MASK [IN|OUT] [/BLANKING Bval]
      ! Mask the inside or the outside of a polygon
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine maskon
  end interface
  !
  interface
    subroutine pencil(line,error)
      use greg_kernel
      use greg_pen
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  PEN [I|*]
      !      [/COLOUR Name]
      !      [/DASHED ND]
      !      [/WEIGHT NW|Valmm]
      !      [/DEFAULT]
      ! Define the status of the pen(s) or change the default pen
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine pencil
  end interface
  !
  interface
    subroutine setpendef(kpen)
      use greg_pen
      !---------------------------------------------------------------------
      ! @ private
      !  Update all or single the pen attributes to their default, and
      ! reset the pen number
      !---------------------------------------------------------------------
      integer(kind=4), intent(in), optional :: kpen
    end subroutine setpendef
  end interface
  !
  interface
    subroutine plcurv4 (nxy,x,y,z,accur,algorithm,variable,periodic,grelo,gvect,  &
        error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine
      !       This routine connects a series of points in the subject space
      !	with a solid line or a dashed line. Cubic spline interpolation
      !	is available either for functions of X or functions of Y or for
      !	curves (X and Y both interpolated, see below). If the number of
      !	points exceeds spline-buffer capacity, the total curve is cut
      !	into several arcs and care is taken to ensure a good fitting
      !	between two adjacent splines.
      !
      !	Adapted from PIGRA.  P. Valiron  16-OCT-84
      !
      !     /VARIABLE X : the variable is X (abscissa)
      !                  	*** Y = F(X)
      !     /VARIABLE Y : the variable is Y (ordinate)
      !                  	*** X = F(Y)
      !     /VARIABLE Z : the variable is the Z array
      !                  	*** explicit parametric form.
      !     /VARIABLE NUMBERING : the variable is the numbering of the points
      !                  *** The sequence of the points is related to a fixed
      !                      increment in an explicit or implicit parametrisa-
      !                      tion of the curve (plotting the solution of a dif
      !                      -ferential equation for equally spaced steps...)
      !     /VARIABLE POLYGONAL_LENGTH OR CURVILINEAR_LENGTH :
      !		   *** the variable is an approximation of the
      !			curvilinear abscissa
      !                  *** The curvilinear abscissa is estimated iteratively.
      !                  The starting approximation is the sum of the distan-
      !                  ces of the points. For each iteration, the spline
      !                  coefficients are computed and the curvilinear abscis-
      !                  sa is integrated along the spline curve.
      !		   The number of iterations is ITERMX.
      !			POLYGONAL_LENGTH	ITERMX=0
      !			CURVILINEAR_LENGTH	ITERMX=2
      ! Arguments :
      !	X	R*4 (*)	Array of X coordinates		Input
      !	Y	R*4 (*)	Array of Y coordinates		Input
      !	Z	R*4 (*)	Array of parameter values	Input
      !	ACCUR	R*4	Accuracy of interpolation	Input
      !	ALGORITHM C*(*)	Algorithm used by interpolation	Input
      !	VARIABLE  C*(*)	Variable used for interpolation	Input
      !	PERIODIC  L	Is it a periodic function	Input
      !	GRELO	Ext	Pen up plotting function	Input
      !	GVECT	Ext	Pen down plotting function	Input
      !	ERROR	L	Logical error flag		Output
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nxy  ! Number of data points
      real*4 :: x(1)                    !
      real*4 :: y(1)                    !
      real*4 :: z(1)                    !
      real :: accur                     !
      character(len=*) :: algorithm     !
      character(len=*) :: variable      !
      logical :: periodic               !
      external :: grelo                 !
      external :: gvect                 !
      logical :: error                  !
    end subroutine plcurv4
  end interface
  !
  interface
    subroutine curvil (s,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !    	This routine is called by CURV to compute the curvilinear length
      !     	S of the spline arc [PSTO(K-1),PSTO(K)].
      !	RETURN 1 occurs if DIFSYS4 fails to integrate
      ! Arguments :
      !	S	R*4	Curvilinear length		Output
      !     ERROR	L	Error return		        Output
      ! Subroutines :
      !	CURFUN, DIFSYS4
      !---------------------------------------------------------------------
      real :: s                         !
      logical :: error                  !
    end subroutine curvil
  end interface
  !
  interface
    subroutine curfun (p,s,dsdp)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! GREG	Internal routine
      !	This routine states the differential equation to be solved by
      !	DIFSYS4 in order to obtain the curvilinear length of an arc of
      !	spline.
      ! Arguments :
      !	P	R*4	Value of parameter			Input
      !	S	R*4	Curvilinear length			Input
      !	DSDP	R*4	Derivative of S relative to P		Output
      !---------------------------------------------------------------------
      real :: p                         !
      real :: s                         !
      real :: dsdp                      !
    end subroutine curfun
  end interface
  !
  interface
    subroutine clip_poly(n,x,y,xc,yc)
      use gildas_def
      use gbl_message
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(inout) :: n      ! Number of summits (updated, may be 0 on return, may be more)
      real(kind=4),              intent(in)    :: x(:)   !
      real(kind=4),              intent(in)    :: y(:)   !
      real(kind=4),              intent(out)   :: xc(:)  !
      real(kind=4),              intent(out)   :: yc(:)  !
    end subroutine clip_poly
  end interface
  !
  interface
    subroutine clip_one (x0,y0,x1,y1,ok)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: x0                        !
      real :: y0                        !
      real :: x1                        !
      real :: y1                        !
      logical :: ok                     !
    end subroutine clip_one
  end interface
  !
  interface
    subroutine setgon(line,error)
      use gildas_def
      use phys_const
      use gbl_message
      use greg_kernel
      use greg_poly
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   POLYGON [(File)Name]
      ! 1  [/PLOT]
      ! 2  [/VARIABLE]
      ! 3  [/FILL [Icolor]]
      ! 4  [/RESET]
      ! 5  [/HATCH [Ipen] [Angle] [Separ] [Phase]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line
      logical,          intent(out) :: error
    end subroutine setgon
  end interface
  !
  interface
    subroutine greg_poly_delvar(varname,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Delete the content of the polygon variable (NOT the structure
      ! itself: this is a program defined reserved name, always present)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: varname  ! Support variable in Sic
      logical,          intent(inout) :: error    ! Input status preserved if no error here
    end subroutine greg_poly_delvar
  end interface
  !
  interface
    subroutine meanva(line,error)
      use gildas_def
      use gbl_message
      use greg_kernel
      use greg_poly
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line
      logical,          intent(out) :: error
    end subroutine meanva
  end interface
  !
  interface
    subroutine gr8_moments(z,poly,b,eb,clip,center,range,sum,aire,npix,mean,  &
      sigma,minmax)
      use phys_const
      use greg_types
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! Compute statistics about the regular grid array within a N-gon
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: z(*)       ! Regular grid array
      type(polygon_t), intent(in)  :: poly       ! Current polygon
      real(kind=4),    intent(in)  :: b          ! Blanking value
      real(kind=4),    intent(in)  :: eb         ! Tolerance on blanking
      logical,         intent(in)  :: clip       ! Clip out of range values
      real(kind=4),    intent(in)  :: center     ! Center of accepted interval
      real(kind=4),    intent(in)  :: range      ! Half width of accepted interval
      real(kind=4),    intent(out) :: sum        ! Integrated map value
      real(kind=4),    intent(out) :: aire       ! Area of polygon
      integer(kind=4), intent(out) :: npix       ! Number of interior pixels
      real(kind=4),    intent(out) :: mean       ! Mean map value
      real(kind=4),    intent(out) :: sigma      ! RMS value
      real(kind=4),    intent(out) :: minmax(2)  ! Min and max values
    end subroutine gr8_moments
  end interface
  !
  interface
    subroutine print_mean(sum,aire,npix,mean,sigma,minmax)
      use phys_const
      use gbl_message
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Print statistics about the regular grid array within a N-gon
      !---------------------------------------------------------------------
      real(kind=4),    intent(in) :: sum        ! Integrated map value
      real(kind=4),    intent(in) :: aire       ! Area of polygon
      integer(kind=4), intent(in) :: npix       ! Number of pixels within polygon
      real(kind=4),    intent(in) :: mean       ! Mean map value
      real(kind=4),    intent(in) :: sigma      ! RMS value
      real(kind=4),    intent(in) :: minmax(2)  ! Min and max values
    end subroutine print_mean
  end interface
  !
  interface
    subroutine greg_poly_loadsub(rname,poly,lun,chain,error)
      use gildas_def
      use gbl_message
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      !  Defines a polygon, either from file, from variable, or with the
      ! cursor.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname  ! Calling command name
      type(polygon_t),  intent(out)   :: poly   ! Defined polygon
      integer(kind=4),  intent(in)    :: lun    ! Logical unit number (0=>Cursor)
      character(len=*), intent(in)    :: chain  ! Variable name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_poly_loadsub
  end interface
  !
  interface
    subroutine greg_poly_variable(rname,varname,poly,error)
      use gbl_format
      use gbl_message
      use sic_types
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      ! Define the polygon from a Sic variable name. The variables can be:
      !  - a real or double array of the form:
      !       VAR[NXY,2]  Real or Double
      !  - a structure of the form:
      !       VAR%NXY         Integer or Long
      !       VAR%X[VAR%NXY]  Real or Double
      !       VAR%Y[VAR%NXY]  Real or Double
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname    ! Calling routine name
      character(len=*), intent(in)    :: varname  ! Sic variable name
      type(polygon_t),  intent(out)   :: poly     ! Defined polygon
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine greg_poly_variable
  end interface
  !
  interface
    subroutine greg_convert(line,error)
      use gildas_def
      use phys_const
      use gbl_format
      use gbl_message
      use gwcs_types
      use sic_types
      use greg_xyz
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    CONVERT RA0 DEC0 [ANGLE]
      !    1  [/TYPE P_TYPE]
      !    2  [/UNIT]
      !    3  [/SYSTEM EQUATORIAL|GALACTIC [Equinox]]
      !    4  [/VARIABLE A B]
      ! Converts from one projection system or absolute coordinates to the
      ! current projection system.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_convert
  end interface
  !
  interface
    subroutine defpro(line,error)
      use phys_const
      use gbl_message
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! GREG Support routine for command
      !    PROJECTION [Ra Dec] [Angle] [/TYPE Type]
      ! Defines the projection type and center
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine defpro
  end interface
  !
  interface
    subroutine greg_grid(line,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  GRID Step [/LABEL] [/ALTERNATE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_grid
  end interface
  !
  interface
    subroutine gridmp(line,error)
      use phys_const
      use gbl_message
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  GRID Step [/LABEL]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gridmp
  end interface
  !
  interface
    subroutine galac(line,error)
      use gbl_message
      use phys_const
      use gwcs_types
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command GRID
      !  Plot a Lii/Bii grid onto a RA-DEC plot or Vice-Versa
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine galac
  end interface
  !
  interface
    subroutine meridien(ra,myrelocate,mydraw,error)
      use gildas_def
      use phys_const
      use gbl_message
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Plot the meridian at a given RA
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: ra          ! Right ascension of meridian
      external                    :: myrelocate  !
      external                    :: mydraw      !
      logical,      intent(inout) :: error       ! Logical error flag
    end subroutine meridien
  end interface
  !
  interface
    subroutine parallele(dec,myrelocate,mydraw,error)
      use gildas_def
      use phys_const
      use gbl_message
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a parallel
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: dec         ! Declination
      external                    :: myrelocate  !
      external                    :: mydraw      !
      logical,      intent(inout) :: error       ! Logical error flag
    end subroutine parallele
  end interface
  !
  interface
    subroutine grid_label(proj,ra,dec,dora,error)
      use gildas_def
      use phys_const
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(projection_t), intent(in)    :: proj    ! Projection description
      real(kind=8),       intent(in)    :: ra,dec  ! [rad]
      logical,            intent(in)    :: dora    ! Do RA or DEC?
      logical,            intent(inout) :: error   !
    end subroutine grid_label
  end interface
  !
  interface
    subroutine setrem
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      ! MAP	Computes the coordinates in the absolute system
      !	of the remarkable points of the projection
      !
      ! There are 6 privileged points
      !
      !	2------------4-------------6
      !	|                          |
      !	|                          |	RX,RY	Relative coordinates
      !	|                          |
      !	|                          |	AX,AY	Absolute coordinates
      !	|                          |
      !	1------------3-------------5
      !
      !
      ! En fait en cas d'angle non nul, la methode ci-dessus ne marche plus.
      ! Une astuce COLOSSALE consiste a considerer le plus petit cercle contenant
      ! le rectangle et centre sur le centre de projection. Ce cercle est la
      ! projection de points angulairement equidistants du centre de projection.
      ! Or, en projection GNOMONIC, les points d'elongations extremes
      ! correspondent aux tangentes au cercle passant par le pole.
      ! i.e.	Xgnom	= 	SQRT(Yg(POLE)**2 - D**2) * D / Yg(POLE)
      !	Ygnom	=	D**2 / Yg(POLE)
      ! Les coordonnees absolues s'en deduise trivialement
      !
      ! Pour la declinaison, le probleme est evident.
      !
      ! Bien sur tout ca ne marche pas avec les projections AITOFF et RADIO .
      !---------------------------------------------------------------------
      ! Global
    end subroutine setrem
  end interface
  !
  interface
    function lever(d,d0)
      !---------------------------------------------------------------------
      ! @ private
      !	Lever d'une etoile de declinaison D a latitude D0
      !
      !	PI/2-D0 < D	Circumpolaire
      !	D0-PI/2 < D < PI/2-D0	H = ACOS (TAN(D)/TAN(D0-PI/2))
      !	D < D0-PI/2	Toujours couchee...
      !
      ! Formule applicable a la partie visible du ciel en projection
      ! sur un plan
      !---------------------------------------------------------------------
      real*8 :: lever                   !
      real*8 :: d                       !
      real*8 :: d0                      !
    end function lever
  end interface
  !
  interface
    subroutine sexfor(xa,ya)
      use phys_const
      use gbl_message
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  Makes a nice format for projection information
      !
      !  Coordinates are displayed with 0.1 milliarcsecond precision
      !  (prec=2.78e-8 degrees), namely:
      !  a) log10(360/(24*3600*prec)) = 5.2 digits fractional part for 24
      !     div/turn sexagesimal notation (Right Ascension)
      !  b) log10(360/(360*3600*prec)) = 4 digits fractional part for 360
      !     div/turn sexagesimal notation (Declination)
      !  c) log10(360/(360*prec)) = 7.6 digits fractional part for 360
      !     div/turn decimal notation (Latitude/Longitude)
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: xa  ! X position
      real(kind=8), intent(in) :: ya  ! Y position
    end subroutine sexfor
  end interface
  !
  interface
    subroutine rgimag(line,optrgdata,dosubset,optsubset,error)
      use gildas_def
      use gbl_format
      use gbl_constant
      use gbl_message
      use sic_types
      use greg_image
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for subset of commands
      !   RGDATA  Name
      !     /VARIABLE
      !     [/SUBSET]
      ! and
      !   LIMITS /RGDATA  Name
      ! Load RG data from variable name
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Command line
      integer(kind=4),  intent(in)    :: optrgdata  ! RG data option
      logical,          intent(in)    :: dosubset   ! Subset option present?
      integer(kind=4),  intent(in)    :: optsubset  ! Subset option number, if relevant
      logical,          intent(inout) :: error      ! Error flag
    end subroutine rgimag
  end interface
  !
  interface
    subroutine rgrsd(line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! Subroutine for Regularly Spaced Data, possibly uncompletely sampled,
      ! in column (XYZ or other variables) "format".
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  !
    end subroutine rgrsd
  end interface
  !
  interface
    subroutine greg_rgdata(line,error)
      use gildas_def
      use gbl_constant
      use gbl_format
      use gbl_message
      use greg_image
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  RGDATA Name
      !   [/SUBSET Imin Jmin Imax Jmax]
      !   [/VARIABLE]
      !   [/FORMAT fmt_code]
      !  Read a RGDATA file into the Regular grid array
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine greg_rgdata
  end interface
  !
  interface
    subroutine rghead (error)
      use greg_kernel
      use greg_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Internal routine
      !	This subroutine reads a map header and retrieves the following
      ! 	information for later use by GREG
      !		XREF, NX, XVAL, XINC, XUNIT
      !		YREF, NY, YVAL, YINC, YUNIT
      ! Arguments :
      !	NX	I	Number of X pixels		Output
      !	NY	I	Number of Y pixels		Output
      !	*	*	Alternate error return		Input
      ! No subroutine referenced
      ! Commons	GRUNIT, FITS
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine rghead
  end interface
  !
  interface
    subroutine rgread(regmap,all,nfx,nfy,fmt,error)
      use gbl_message
      use greg_kernel
      use greg_image
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG 	Internal routine
      !	  Read the data in a RGDATA file
      !   An optional format can be specified. Default is 10Z8.8
      !---------------------------------------------------------------------
      integer, intent(in) :: nfx                    ! X grid size
      integer, intent(in) :: nfy                    ! Y grid size
      real(4), intent(out):: regmap(nfx*nfy)        ! Grid values
      logical, intent(in) :: all                    ! Read all map ?
      character(len=*), intent(in) :: fmt           ! Format for reading
      logical, intent(out) :: error                 !
    end subroutine rgread
  end interface
  !
  interface
    subroutine rgread_all(regmap,nfx,nfy,fmt,code,error)
      use gbl_format
      use gbl_message
      use greg_kernel
      use greg_image
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG 	Internal routine
      !	Read the data in a RGDATA file
      ! Arguments :
      !     REGMAP	R*4 (*)	Regular grid array to be filled		Output
      !     NFX	I*4	First map dimension			Output
      !     NFY	I*4	Second map dimension			Output
      !     FMT	C*(*)	Data file format			Input
      !     CODE      I*4     Input data type                         Input
      !     ERROR	L	Error return			        Output
      !---------------------------------------------------------------------
      integer, intent(in) :: nfx                    ! X grid size
      integer, intent(in) :: nfy                    ! Y grid size
      real(4), intent(out):: regmap(nfx*nfy)        ! Grid values
      character(len=*), intent(in) :: fmt           ! Format for reading
      integer, intent(in) :: code                   ! Type of data
      logical, intent(out) :: error                 !
    end subroutine rgread_all
  end interface
  !
  interface
    subroutine find_mapchar8(x,y,n,xinc,yinc,xmin,ymin,xmax,ymax,eb,cb)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: n                      !
      real(8) :: x(n)                    !
      real(8) :: y(n)                    !
      real(8) :: xinc                    !
      real(8) :: yinc                    !
      real(8) :: xmin                    !
      real(8) :: ymin                    !
      real(8) :: xmax                    !
      real(8) :: ymax                    !
      real(8) :: eb                      !
      real(8) :: cb                      !
    end subroutine find_mapchar8
  end interface
  !
  interface
    subroutine find_siz8(x,y,n,xmin,xmax,ymin,ymax,eb,cb)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: n                      !
      real(8) :: x(n)                    !
      real(8) :: y(n)                    !
      real(8) :: xmin                    !
      real(8) :: xmax                    !
      real(8) :: ymin                    !
      real(8) :: ymax                    !
      real(8) :: eb                      !
      real(8) :: cb                      !
    end subroutine find_siz8
  end interface
  !
  interface
    subroutine reallocate_rgdata(mx,my,error)
      use gildas_def
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  (Re)allocate the RG buffer, taking care of its previous allocation
      ! status.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: mx     !
      integer(kind=4), intent(in)    :: my     !
      logical,         intent(inout) :: error  !
    end subroutine reallocate_rgdata
  end interface
  !
  interface
    subroutine reassociate_rgdata(rgbuf,mx,my,error)
      use gildas_def
      use gbl_message
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  (Re)associate the RG buffer, taking care of its previous allocation
      ! status.
      ! ---
      !  Bug fix. Using an explicit-shape dummy argument
      !     real(kind=4), intent(in), target :: rgbuf(mx,my)
      ! and pointing to it:
      !     rg%data => rgbuf
      ! is processor-dependent according to the Fortran. See Fortran 2003
      ! Section 12.4.1.2:
      !  "If the dummy argument has the TARGET attribute and is an
      ! explicit-shape array or is an assumed-size array, and the corresponding
      ! actual argument has the TARGET attribute but is not an array section
      ! with a vector subscript then:
      !   1) [...]
      !   2) when execution of the procedure completes, the pointer association
      !      status of any pointer that is pointer associated with the dummy
      !      argument is processor dependent."
      ! In other words, when sending the data array to the subroutine, the
      ! caller may send a copy of the actual argument, so that the dummy
      ! argument is contiguous and satisfies the explicit-shape declaration.
      !
      !   Ifort 9.0 to 14.0.1 (at least): seems indeed affected by this. The
      ! association status "seems" correct in return, but according to the
      ! standard the pointer association status is undefined (i.e. not
      ! reliable). In practice, a subsequent deallocation of the pointer
      ! fails if the allocation has been stealed to the target in the
      ! context:
      !  pointer :: a,b
      !  allocate(a)
      !  b => a
      !  deallocate(b)
      ! This is done in some cases.
      !
      !  On the other hand, one can use an assumed-shape dummy argument:
      !    real(kind=4), intent(in), target :: rgbuf(:,:)
      ! In this case, the caller knows the interface and can send the actual
      ! array (maybe with strides and boundaries specific to the call), so
      ! that it is not a problem to associate a pointer.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)         :: mx          !
      integer(kind=4), intent(in)         :: my          !
      real(kind=4),    intent(in), target :: rgbuf(:,:)  !
      logical,         intent(inout)      :: error       !
    end subroutine reassociate_rgdata
  end interface
  !
  interface
    subroutine deallocate_rgdata(error)
      use gildas_def
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Deallocate the RG buffer, taking care of its previous allocation
      ! status.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine deallocate_rgdata
  end interface
  !
  interface
    subroutine greg_write(line,error)
      use gbl_format
      use gbl_message
      use greg_kernel
      use greg_poly
      use greg_rg
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for command
      !
      !   WRITE
      !     COLUMN Filename [NEW [Format]] /TABLE X Nx Y Ny z Nz
      !     IMAGE
      !     RGDATA Filename "Comment X" "Comment Y"
      !     POLYGON
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine greg_write
  end interface
  !
  interface
    subroutine greg_write_column(line,fname,error)
      use image_def
      use gbl_message
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   WRITE COLUMN Filename [NEW [Format]] /TABLE X Nx Y Ny z Nz
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: fname
      logical,          intent(inout) :: error
    end subroutine greg_write_column
  end interface
  !
  interface
    subroutine greg_write_rgdata(line,jtmp,regmap,mreg,error)
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !  Writes a RGDATA like file from the Regular_Grid array
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line          ! Input command line
      integer(kind=4),  intent(in)    :: jtmp          ! Logical unit number
      integer(kind=4),  intent(in)    :: mreg          ! Size of array
      real(kind=4),     intent(in)    :: regmap(mreg)  ! Array to write
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine greg_write_rgdata
  end interface
  !
  interface
    subroutine wrcol2(jun,n,x,y)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: jun   !
      integer(kind=4), intent(in) :: n     !
      real(kind=8),    intent(in) :: x(*)  !
      real(kind=8),    intent(in) :: y(*)  !
    end subroutine wrcol2
  end interface
  !
  interface
    subroutine wrcol3(jun,n,x,y,z)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: jun   !
      integer(kind=4), intent(in) :: n     !
      real(kind=8),    intent(in) :: x(*)  !
      real(kind=8),    intent(in) :: y(*)  !
      real(kind=8),    intent(in) :: z(*)  !
    end subroutine wrcol3
  end interface
  !
  interface
    subroutine greg_write_image(name,array,error)
      use gildas_def
      use gbl_message
      use gbl_format
      use phys_const
      use greg_wcs
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name      ! File name
      real(kind=4),     intent(in)    :: array(*)  ! Data array
      logical,          intent(inout) :: error     ! Error flag
    end subroutine greg_write_image
  end interface
  !
  interface
    subroutine sampler(line,error)
      use gildas_def
      use gbl_message
      use greg_kernel
      use greg_rg
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Support routine for Command
      !	RESAMPLE NX NY/ [/X Xref Xval Xinc] [/Y Yref Yval Yinc]
      !		[/BLANKING Blank]
      ! 	Resample the current Regular Grid onto a different one.
      ! Arguments :
      !	LINE	C*(*)	Command line		Input
      !	ERROR	L	Logical error flag	Output
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine sampler
  end interface
  !
  interface
    subroutine resamp(a,mx,my,aconv,ablank,b,nx,ny,bconv)
      !---------------------------------------------------------------------
      ! @ private
      ! GREG	Stand alone subroutine
      !	Resample an input map to a different grid. bilinear interpolation.
      ! Arguments
      !	A	R*4(*)	Input map of dimensions MX MY
      !	MX,MY	I
      !	ACONV	R*8(6)	Pixel conversion formulae
      !			CONV(1) = Xref, CONV(2)=Xval, CONV(3)=Xinc
      !	B	R*4(*)	Output map of dimensions NX,NY
      !	NX,NY	I
      !	BCONV	R*8(6)
      !	BLANK	R*4	Blanking value
      !---------------------------------------------------------------------
      integer :: mx                     !
      integer :: my                     !
      real*4 :: a(mx,my)                !
      real*8 :: aconv(6)                !
      real*4 :: ablank(2)               !
      integer :: nx                     !
      integer :: ny                     !
      real*4 :: b(nx,ny)                !
      real*8 :: bconv(6)                !
    end subroutine resamp
  end interface
  !
  interface
    subroutine setup(line,error)
      use greg_kernel
      use greg_pen
      use greg_xyz
      use greg_axes
      use greg_image
      use greg_wcs
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  GREG Support routine for command
      !  SET ...
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Command line
      logical,          intent(out)   :: error  !
    end subroutine setup
  end interface
  !
  interface
    subroutine greg_show(line,error)
      use gbl_message
      use greg_kernel
      use greg_pen
      use greg_axes
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !  GREG  Support routine for command
      !  SHOW
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine greg_show
  end interface
  !
  interface
    subroutine show_limits(line,error)
      use phys_const
      use gbl_constant
      use gbl_message
      use greg_kernel
      use greg_wcs
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine show_limits
  end interface
  !
  interface
    subroutine spanum_to_greg(string,nchar,doexpo)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   This subroutine converts a floating point number produced by
      ! SPANUM such as 1.234E+07 or 1.E+12 in a string of characters
      ! allowing usual exponential notation with GREG e.g.
      !                              7
      !     1.2345E+07 -->  1.2345 10
      !
      !                       12
      !     1.E+12  ----->  10
      !
      !                         -8
      !     2.E-08  ----->  2 10
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: string  ! Character string to convert
      integer(kind=4),  intent(inout) :: nchar   ! Length of string
      logical,          intent(in)    :: doexpo  ! Prefer exponential notation when possible?
    end subroutine spanum_to_greg
  end interface
  !
  interface
    subroutine gag_cflabh(chrstr,x,w,d,type,short)
      !---------------------------------------------------------------------
      ! @ private
      !  Convert floating point value to a display of the form of the
      ! "Babylonian" ddd mm ss.s, where each field represents a sexagesimal
      ! increment over its successor. The value X must be in the units of
      ! the rightmost field. For instance, X must be in seconds of time for
      ! hours, minutes, seconds
      !
      ! This routine insert the proper labelling in GreG style display.
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: chrstr  ! Formatted string
      real(kind=8),     intent(in)  :: x       ! Input number
      integer(kind=4),  intent(out) :: w       ! Length of string
      integer(kind=4),  intent(in)  :: d       ! Number of decimals
      integer(kind=4),  intent(in)  :: type    ! Type of label (None/Hour/Degree)
      logical,          intent(in)  :: short   ! Drop off zero-leading fields?
    end subroutine gag_cflabh
  end interface
  !
  interface
    subroutine gstring(n,string,angle,doclip)
      use greg_kernel
      !---------------------------------------------------------------------
      ! @ private
      !   Interprets and writes a character string
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in) :: n       ! Length of string
      character(len=*), intent(in) :: string  !
      real(kind=8),     intent(in) :: angle   ! Text angle, in radians
      logical,          intent(in) :: doclip  ! Clip the text in the box?
    end subroutine gstring
  end interface
  !
  interface
    subroutine gvaleur (line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      use greg_kernel
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine gvaleur
  end interface
  !
  interface
    subroutine greg_variables
      use greg_contours
      use greg_kernel
      use greg_rg
      use greg_wcs
      use greg_xyz
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine greg_variables
  end interface
  !
  interface
    subroutine greg_set_structure(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Create the structure SET%GREG and fill it.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine greg_set_structure
  end interface
  !
  interface
    subroutine grwedge(line,error)
      use gbl_message
      use gbl_format
      use gildas_def
      use phys_const
      use sic_types
      use greg_kernel
      use greg_axes
      use greg_contours
      use greg_pen
      use greg_types
      !---------------------------------------------------------------------
      ! @ private
      ! GreG
      !  Support routine for command
      !  WEDGE [Top|Bottom|Left|Right [Size]]
      !  1     [/SCALING Lin|Log Min Max]
      !  2     [/LEVELS [YES] ]
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine grwedge
  end interface
  !
  interface
    subroutine zmont(z,id1,nxi,nxf,nyi,nyf,x0,y0,dx,dy,az,el,fe)
      !---------------------------------------------------------------------
      ! @ private
      !  This subroutine plots a perspective of profiles of a bidimensional
      ! functions. (L. DELGADO Yebes 1983)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: id1       ! First dimension of Z
      real(kind=4),    intent(inout) :: z(id1,*)  ! Function values array, destroyed in the process
      integer(kind=4), intent(in)    :: nxi       ! First index to be shown in X
      integer(kind=4), intent(in)    :: nxf       ! Last index to be shown in X
      integer(kind=4), intent(in)    :: nyi       ! First index to be shown in Y
      integer(kind=4), intent(in)    :: nyf       ! Last index to be shown in Y
      real(kind=4),    intent(in)    :: x0        ! X paper coordinate of lower left point for Z=0.
      real(kind=4),    intent(in)    :: y0        ! Y paper coordinate of this point
      real(kind=4),    intent(in)    :: dx        ! X Size of projection area
      real(kind=4),    intent(in)    :: dy        ! Y Size of projection area
      real(kind=4),    intent(in)    :: az        ! Azimuth of projection (-90<AZ<90)
      real(kind=4),    intent(in)    :: el        ! Elevation of projection (0<EL<90)
      real(kind=4),    intent(in)    :: fe        ! Scaling factor in Z
    end subroutine zmont
  end interface
  !
  interface
    subroutine zsom(z,id1,x0,axx,axy,nxi,nxf,nyi,i,j)
      !---------------------------------------------------------------------
      ! @ private
      !  This subroutine is used by ZMONT for determining visible areas
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: id1       ! First dimension of Z
      real(kind=4),    intent(in) :: z(id1,*)  ! Function values array
      real(kind=4),    intent(in) :: x0        ! X paper coordinate of lower left point for Z=0
      real(kind=4),    intent(in) :: axx       ! X paper coordinate increment between consecutive lines
      real(kind=4),    intent(in) :: axy       ! Y paper coordinate increment
      integer(kind=4), intent(in) :: nxi       ! First index to be shown in X
      integer(kind=4), intent(in) :: nxf       ! Last index to be shown in X
      integer(kind=4), intent(in) :: nyi       ! First index to be shown in Y
      integer(kind=4), intent(in) :: i         ! X Index of the starting point of current line
      integer(kind=4), intent(in) :: j         ! Y Index of this point
    end subroutine zsom
  end interface
  !
  interface
    subroutine zmx(z,id1,ili,ilf,l,xili,axx,xi,yi,xf,yf)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: id1       !
      real(kind=4),    intent(in)    :: z(id1,*)  !
      integer(kind=4), intent(in)    :: ili,ilf   !
      integer(kind=4), intent(in)    :: l         !
      real(kind=4),    intent(in)    :: xili      !
      real(kind=4),    intent(in)    :: axx       !
      real(kind=4),    intent(inout) :: xi,yi     !
      real(kind=4),    intent(inout) :: xf,yf     !
    end subroutine zmx
  end interface
  !
end module greg_interfaces_private
