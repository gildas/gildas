subroutine grwedge(line,error)
  use gbl_message
  use gbl_format
  use gildas_def
  use phys_const
  use sic_types
  use greg_dependencies_interfaces, no_interface=>gr4_dicho
  use greg_interfaces, except_this=>grwedge
  use greg_kernel
  use greg_axes
  use greg_contours
  use greg_pen
  use greg_types
  !---------------------------------------------------------------------
  ! @ private
  ! GreG
  !  Support routine for command
  !  WEDGE [Top|Bottom|Left|Right [Size]]
  !  1     [/SCALING Lin|Log Min Max]
  !  2     [/LEVELS [YES] ]
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='WEDGE'
  logical :: found
  integer(kind=index_length) :: mx,my
  integer :: i,nc
  integer(kind=size_length) :: nelem,j
  real*4 :: cuts(2),extrem(2),loca(4),limts(4),convrt(6)
  real*4 :: blank(3)
  real*8 :: a1,a2,small,big
  type(axis_t) :: axis
  real*4 :: ax,ay,alen,size,xx,dx,value
  real*8 :: val,dble
  character(len=8) :: chain
  character(len=32) :: nom_image
  character(len=32) :: charval
  type(sic_descriptor_t) :: desc
  integer(kind=address_length) :: ipt
  integer :: lut_size,nwedge,iscale,nkey,eqnlev
  character(len=12) :: place(4),keyw,scale(3)
  real, allocatable :: wedge(:)
  real :: lcut,hcut,lhcut,llcut
  logical :: levl,labl
  type(tick_t) :: tick
  ! Data
  data convrt/6*1.0/
  data place/'BOTTOM','LEFT','RIGHT','TOP'/
  data scale/'LINEAR','LOGARITHMIC','EQUALIZATION'/
  !
  chain = 'RIGHT'
  call sic_ke (line,0,1,chain,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,chain,keyw,nkey,place,4,error)
  if (error) return
  size = 0.6
  call sic_r4 (line,0,2,size,.false.,error)
  if (error) return
  !
  iscale = 1
  lcut = 0.0
  hcut = 1.0
  call sic_get_inte('LUT%SIZE',lut_size,error)
  if (lut_size.le.0) then
    call greg_message(seve%e,rname,'LUT%SIZE is not positive!')
    error = .true.
    return
    !
  elseif (lut_size.gt.2048) then
    ! Limit the wedge to 2048 values. This seems unreasonable to go further
    ! because:
    ! - the LUT have (mostly) always smooth color variations,
    ! - we have not more then ~2048 pixels on our screens,
    ! - PostScript hardcopy size would explode for each 65356-colors wedge
    !   we would store.
    nwedge = 2048
  else
    nwedge = lut_size
  endif
  call sic_get_inte('CURIMA%SCALING',iscale,error)
  call sic_get_real('CURIMA%SCALE[1]',lcut,error)
  call sic_get_real('CURIMA%SCALE[2]',hcut,error)
  ! if (error): no problem, scaling can come from the command line options
  !
  levl = sic_present(2,0)
  labl = sic_present(2,1)
  if (sic_present(1,0)) then
    call sic_ke (line,1,1,chain,nc,.true.,error)
    if (error) return
    if (chain.ne.'*') then
      call sic_ambigs(rname,chain,keyw,iscale,scale,3,error)
      if (error) return
    endif
    call sic_r4 (line,1,2,lcut,.false.,error)
    if (error) return
    call sic_r4 (line,1,3,hcut,.false.,error)
    if (error) return
  endif
  !
  if (iscale.eq.3) then
    call sic_get_inte('CURIMA%EQUAL%NLEV',eqnlev,error)
    if (error) then
      call greg_message(seve%e,rname,'No equalisation performed yet')
      return
    elseif (eqnlev.eq.0) then
      call greg_message(seve%e,rname,'No valid equalisation present in '//  &
      'memory')
      error=.true.
      return
    elseif (ncl.eq.0) then
      call greg_message(seve%e,rname,'I need LEVELS in equalisation mode!')
      error=.true.
      return
    else
      found = .true.
      call sic_descriptor ('CURIMA%EQUAL%LEV',desc,found)
      if (.not.found) then
        call greg_message(seve%e,rname,'Internal error: Unknown '//  &
          'variable CURIMA%EQUAL%LEV')
        error=.true.
        return
      endif
      ipt= gag_pointer(desc%addr,memory)
    endif
    levl=.true.
    labl=.true.
  endif
  if (hcut.eq.lcut) hcut = lcut+1.0
  axis%loga = .false.
  axis%label%format = format_decimal
  axis%label%brief = .false.
  axis%label%expo = axis_zexpo
  !
  allocate(wedge(nwedge))
  do i=1,nwedge
    wedge(i) = float(i)
  enddo
  convrt(2)=0.0
  convrt(5)=0.0
  !
  ! Top
  if (nkey.eq.4) then
    mx = nwedge
    my = 1
    limts(1) = -0.5
    limts(2) = nwedge-0.5
    limts(3) = -0.5
    limts(4) = 0.5
    loca(1) = gx1
    loca(2) = gx2
    loca(3) = gy2
    loca(4) = gy2+size
    !
    axis%doline = .true.
    axis%dolabel = .true.
    axis%angle = 0.d0
    axis%tdirect = jclock(3)  ! Inum = 3
    axis%label%justif = 1          ! or JUST(INUM)?
    axis%label%offset = 1.
    ax = loca(1)
    ay = loca(4)
    alen = loca(2)-loca(1)
    a1 = lcut
    a2 = hcut
    axis%loga = iscale.eq.2
    small = 0
    big = 0
    nom_image = 'TWEDGE'
    ! Right
  elseif (nkey.eq.3) then
    ! Inum = 7
    mx = 1
    my = nwedge
    limts(3) = -0.5
    limts(4) = nwedge-0.5
    limts(1) = -0.5
    limts(2) = 0.5
    loca(1) = gx2
    loca(2) = gx2+size
    loca(3) = gy1
    loca(4) = gy2
    !
    axis%doline = .true.
    axis%dolabel = .true.
    axis%angle = pi/2.d0
    axis%tdirect = jclock(7) ! Inum = 7
    axis%label%justif = 0         ! or JUST(INUM)?
    axis%label%offset = 1.
    ax = loca(2)
    ay = loca(3)
    alen = loca(4)-loca(3)
    a1 = lcut
    a2 = hcut
    axis%loga = iscale.eq.2
    small = 0
    big = 0
    nom_image = 'RWEDGE'
    !
    ! Left
  elseif (nkey.eq.2) then
    ! Inum = 7
    mx = 1
    my = nwedge
    limts(3) = -0.5
    limts(4) = nwedge-0.5
    limts(1) = -0.5
    limts(2) =  0.5
    loca(1) = gx1-size
    loca(2) = gx1
    loca(3) = gy1
    loca(4) = gy2
    !
    axis%doline = .true.
    axis%dolabel = .true.
    axis%angle = pi/2.d0
    axis%tdirect = jclock(5) ! Inum = 5
    axis%label%justif = 2         ! or JUST(INUM)?
    axis%label%offset = 1.
    ax = loca(1)
    ay = loca(3)
    alen = loca(4)-loca(3)
    a1 = lcut
    a2 = hcut
    axis%loga = iscale.eq.2
    small = 0
    big = 0
    nom_image = 'LWEDGE'
    !
    ! Bottom
  else
    mx = nwedge
    my = 1
    limts(1) = -0.5
    limts(2) = nwedge-0.5
    limts(3) = -0.5
    limts(4) =  0.5
    loca(1) = gx1
    loca(2) = gx2
    loca(3) = gy1-size
    loca(4) = gy1
    !
    axis%doline = .true.
    axis%dolabel = .true.
    axis%angle = 0.d0
    axis%tdirect = jclock(1) ! Inum = 1
    axis%label%justif = 1         !  or JUST(INUM) ?
    axis%label%offset = 1.
    ax = loca(1)
    ay = loca(3)
    alen = loca(2)-loca(1)
    a1 = lcut
    a2 = hcut
    axis%loga = iscale.eq.2
    small = 0
    big = 0
    nom_image = 'BWEDGE'
  endif
  !
  blank(1) = 0.
  blank(2) = -1.
  cuts(1) = 1.
  cuts(2) = nwedge
  extrem(1) = 1.
  extrem(2) = nwedge
  !
  ! Create the image segment
  call gr_segm(nom_image,error)
  if (error) then
    call greg_message(seve%e,rname,'Could not create image segment')
    deallocate(wedge)
    return
  endif
  !
  ! Create the image descriptor in GTVIRT
  call gtv_image(  &
    mx,my,wedge,   &  ! Data
    loca,          &  ! Position in Paper
    limts,         &  ! User limits
    convrt,        &  ! Image pixel to User formula
    1,             &  ! Scaling mode (1=Lin)
    cuts,          &  ! Low and High cuts
    extrem,        &  ! Low and High extrema
    blank,         &  ! Blanking value
    .true.,        &  ! Always Visible ?
    .false.,       &  ! Do not update the SIC structure CURIMA%
    error)
  deallocate(wedge)
  ! if (error) do nothing: we have to close this (empty) segment
  !
  call gtsegm_close(error)
  if (error)  return
  !
  ! Make the box around
  call gr_segm('WBOX',error)
  call setdas(1)
  call grline(loca(1),loca(3),loca(2),loca(3))
  call grline(loca(2),loca(3),loca(2),loca(4))
  call grline(loca(2),loca(4),loca(1),loca(4))
  call grline(loca(1),loca(4),loca(1),loca(3))
  !
  ! Some caution on LOG:
  if (lcut.le.0)  axis%loga = .false.
  if (axis%loga) then
    llcut=log10(lcut)
    lhcut=log10(hcut)
  endif
  !
  if (.not.levl.or..not.labl)  &
    call plot_axis(a1,a2,small,big,ax,ay,alen,axis,error)
  !
  if (levl) then
    if (nkey.eq.1 .or. nkey.eq.4) then
      if (axis%loga) then
        dx = (loca(2)-loca(1))/(lhcut-llcut)
      elseif (iscale.eq.3) then
        dx = (loca(2)-loca(1))/eqnlev
      else
        dx = (loca(2)-loca(1))/(hcut-lcut)
      endif
      do i=1,ncl
        if (cl(i).gt.lcut .and. cl(i).lt.hcut) then
          if (axis%loga) then
            value = log10(cl(i))-llcut
          elseif (iscale.eq.3) then
            nelem = desc%dims(1)
            call gr4_dicho(nelem,memory(ipt),cl(i),j)
            value=float(j)
          else
            value = cl(i)-lcut
          endif
          xx = loca(1)+value*dx
          call grline(xx,loca(3),xx,loca(4))
          if (labl) then
            tick%major = .true.
            tick%x = xx
            tick%y = ay
            write(charval,1) cl(i)
            read(charval,1)  val
            tick%value = val
            tick%lbrief = .false.
            call plot_ticklabel(tick,axis)
          endif
        endif
      enddo
    else
      if (axis%loga) then
        dx = (loca(4)-loca(3))/(lhcut-llcut)
      elseif (iscale.eq.3) then
        dx = (loca(4)-loca(3))/eqnlev
      else
        dx = (loca(4)-loca(3))/(hcut-lcut)
      endif
      do i=1,ncl
        if (cl(i).gt.lcut .and. cl(i).lt.hcut) then
          if (axis%loga) then
            value = log10(cl(i))-llcut
          elseif (iscale.eq.3) then
            nelem = desc%dims(1)
            call gr4_dicho(nelem,memory(ipt),cl(i),j)
            value = float(j)
          else
            value = cl(i)-lcut
          endif
          xx = loca(3)+value*dx
          call grline(loca(1),xx,loca(2),xx)
          val = dble(cl(i))
          if (labl) then
            tick%major = .true.
            tick%x = ax
            tick%y = xx
            write(charval,1) cl(i)
            read(charval,1)  val
            tick%value = val
            tick%lbrief = .false.
            call plot_ticklabel(tick,axis)
          endif
        endif
      enddo
    endif
  endif
  !
  call gtsegm_close(error)
  if (error)  return
  !
1 format(g12.3)
end subroutine grwedge
