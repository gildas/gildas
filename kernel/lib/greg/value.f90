subroutine gvaleur (line,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_types
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gvaleur
  use greg_kernel
  use greg_xyz
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='VALUES'
  real(kind=8) :: bval,eval
  integer(kind=4) :: narg,form,argz
  type(sic_descriptor_t) :: xinca,yinca,zinca
  integer(kind=address_length) :: xaddr,yaddr,zaddr
  integer(kind=size_length) :: ixy
  save xinca,yinca,zinca
  !
  ! Blanking ?
  eval = eblank
  call sic_r8 (line,1,2,eval,.false.,error)
  if (error) return
  bval = cblank
  call sic_r8 (line,1,1,bval,.false.,error)
  if (error) return
  !
  ! Arguments
  narg = sic_narg(0)
  ixy = 0
  form = fmt_r8
  if (narg.eq.2 .or.  &  ! Explicit X Y arrays (plot Y values)
      narg.eq.3) then    ! Explicit X Y Z arrays (plot Z values)
    if (narg.eq.2) then
      argz = 2
    else
      argz = 3
    endif
    call get_same_inca(rname,line,0,1,form,ixy,xinca,error)
    if (error) return
    call get_same_inca(rname,line,0,2,form,ixy,yinca,error)
    if (error) then
      call sic_volatile(xinca)
      return
    endif
    call get_same_inca(rname,line,0,argz,form,ixy,zinca,error)
    if (error) then
      call sic_volatile(xinca)
      call sic_volatile(yinca)
      return
    endif
    !
  elseif (narg.eq.0) then  ! Implicit X Y Z arrays
    ixy = 0
    form = fmt_r8
    call get_greg_inca(rname,'X',form,ixy,xinca,error)
    if (error) return
    call get_greg_inca(rname,'Y',form,ixy,yinca,error)
    if (error) then
      call sic_volatile(xinca)
      return
    endif
    call get_greg_inca(rname,'Z',form,ixy,zinca,error)
    if (error) then
      call sic_volatile(xinca)
      call sic_volatile(yinca)
      return
    endif
    !
  else
    call greg_message(seve%e,rname,'0, 2, or 3 arguments required')
    error = .true.
    return
  endif
  !
  xaddr = gag_pointer(xinca%addr,memory)
  yaddr = gag_pointer(yinca%addr,memory)
  zaddr = gag_pointer(zinca%addr,memory)
  call values (memory(xaddr),memory(yaddr),memory(zaddr),ixy,bval,eval)
  call sic_volatile(xinca)
  call sic_volatile(yinca)
  call sic_volatile(zinca)
end subroutine gvaleur
!
subroutine values(xd,yd,zd,ndp,bval,eval)
  use gildas_def
  use greg_interfaces
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   Plot the data values on the contour map currently up to 10
  ! characters for each value are displayed
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in) :: ndp      ! Number of values
  real(kind=8),              intent(in) :: xd(ndp)  ! Array of X coordinates
  real(kind=8),              intent(in) :: yd(ndp)  ! Array of Y coordinates
  real(kind=8),              intent(in) :: zd(ndp)  ! Array of values
  real(kind=8),              intent(in) :: bval     ! Blanking value
  real(kind=8),              intent(in) :: eval     ! Tolerance on blanking value
  ! Local
  integer(kind=size_length) :: k
  integer(kind=4) :: nchar
  real(kind=8) :: xmin,xmax,ymin,ymax
  character(len=20) :: string
  real(kind=4) :: x
  !
  ! Loop and plot all values
  xmin = min(gux1,gux2)
  xmax = max(gux1,gux2)
  ymin = min(guy1,guy2)
  ymax = max(guy1,guy2)
  do  k=1,ndp
    if (xd(k).ge.xmin .and. xd(k).le.xmax .and.  &
        yd(k).ge.ymin .and. yd(k).le.ymax) then
      if (eval.ge.0.d0 .and. abs(zd(k)-bval).le.eval)  &
        cycle  ! Value is blanked, skip
      !
      x = zd(k)
      call conecd   (x,string,nchar)
      call relocate (xd(k),yd(k))
      call putlabel (nchar,string,5,tangle,.false.)
    endif
  enddo
end subroutine values
