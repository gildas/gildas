subroutine greg_point(line,error)
  use greg_dependencies_interfaces, no_interface1=>gr8_minmax
  use greg_interfaces, except_this=>greg_point,  &
                       no_interface2=>find_blank8
  use gildas_def
  use sic_types
  use greg_kernel
  use greg_xyz
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    POINTS [Array__X Array__Y]
  ! 1    [/BLANKING [Bval [Eval]]]
  ! 2    [/SIZE MaxVal [Array__Z [Expo]]]
  ! 3    [/MARKER]
  !
  !  Draw markers of current style and size. If MaxVal is given, draws
  ! symbols of current style but of size proportional to Z value at
  ! positions (X,Y). The current marker size corresponds to points with
  ! value MaxVal.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='POINTS'
  real(kind=8) :: zmax,zmin,bval,eval
  real(kind=4) :: zfact,expo
  integer(kind=address_length) :: xaddr,yaddr,zaddr,nsi_addr,ist_addr
  type(sic_descriptor_t) :: xinca,yinca,zinca,nsi_inca,ist_inca
  integer(kind=4) :: form,narg
  integer(kind=size_length) :: ixy,is,ne,in,nmin,nmax
  logical :: same_marker,optsize
  !
  save xinca,yinca,zinca,nsi_inca,ist_inca
  !
  ! Initialise
  ixy = 0
  !
  ! Blanking ?
  eval = eblank
  call sic_r8 (line,1,2,eval,.false.,error)
  if (error) return
  bval = cblank
  call sic_r8 (line,1,1,bval,.false.,error)
  if (error) return
  !
  ! Z scaling ?
  optsize = sic_present(2,0)
  if (optsize) then
    call sic_r4 (line,2,1,zfact,.true.,error)
    if (error) return
  else
    zfact = -1.0
  endif
  expo = 0.5
  call sic_r4 (line,2,3,expo,.false.,error)
  if (error) return
  !
  ! Check on which variable
  narg = sic_narg(0)
  if (narg.eq.2 .or. narg.eq.0) then
    form = fmt_r8
    call get_incarnation('POINTS',line,form,ixy,xinca,yinca,error)
    if (error) return
    xaddr = gag_pointer(xinca%addr,memory)
    yaddr = gag_pointer(yinca%addr,memory)
    if (optsize) then
      if (sic_present(2,2)) then
        call get_same_inca ('POINTS',line,2,2,form,ixy,zinca,error)
      else
        call greg_message(seve%w,rname,'Using Z array')
        call get_greg_inca ('POINTS','Z',form,ixy,zinca,error)
      endif
      if (error) then
        call sic_volatile(xinca)
        call sic_volatile(yinca)
        return
      endif
      zaddr = gag_pointer(zinca%addr,memory)
    endif
  else
    call greg_message(seve%e,rname,'Two arguments or none')
    error = .true.
    return
  endif
  !
  ! If no points, exit
  if (ixy.lt.1) goto 10
  !
  ! Variable marker style?
  if (sic_present(3,0)) then
    narg = sic_narg(3)
    if (narg.eq.2) then
      form = fmt_i4
      call get_same_inca ('POINTS',line,3,1,form,ixy,nsi_inca,error)
      call get_same_inca ('POINTS',line,3,2,form,ixy,ist_inca,error)
      if (error) then
        call sic_volatile(xinca)
        call sic_volatile(yinca)
        call sic_volatile(zinca)
        call sic_volatile(nsi_inca)
        return
      endif
      nsi_addr = gag_pointer(nsi_inca%addr,memory)
      ist_addr = gag_pointer(ist_inca%addr,memory)
    else
      call greg_message(seve%e,rname,'Two arguments required for option '//  &
      '/MARKER')
      error = .true.
      return
    endif
    same_marker = .false.
  else
    nsi_addr = gag_pointer(locwrd(nsides),memory)
    ist_addr = gag_pointer(locwrd(istyle),memory)
    same_marker = .true.
  endif
  !
  ! Scale marker size
  if (optsize) then
    if (zfact.eq.0.) then
      call gr8_minmax (ixy,memory(zaddr),0.d0,-1.d0,zmin,zmax,nmin,nmax)
      zfact = max(abs(zmax),abs(zmin))
    endif
    zfact = abs (csymb / zfact**expo)
  endif
  !
  if (eval.lt.0.0d0) then
    ! Blanking is off if eval is negative (i.e. blanking is disabled)
    call points(memory(nsi_addr),memory(ist_addr),zfact,expo,memory(xaddr),  &
      memory(yaddr),memory(zaddr),ixy,same_marker)
    !
  else
    ! With blanking, applied to Y or Z array
    in = 1
    do while (in.ne.0)
      if (optsize) then
        call find_blank8(memory(zaddr),bval,eval,ixy,is,ne,in)
      else
        call find_blank8(memory(yaddr),bval,eval,ixy,is,ne,in)
      endif
      if (ne.gt.0)  &
        call points(memory(nsi_addr),memory(ist_addr),zfact,expo,  &
          memory(xaddr+2*(is-1)),memory(yaddr+2*(is-1)),memory(zaddr+2*(is-1)),  &
          ne,same_marker)
    enddo
  endif
  !
10 continue
  call sic_volatile(xinca)
  call sic_volatile(yinca)
  if (zfact.ge.0) call sic_volatile(zinca)
end subroutine greg_point
