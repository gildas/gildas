subroutine level(line,error)
  use gildas_def
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>level
  use greg_contours
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GREG Support routine for command
  !   LEVELS n1 TO n2 BY n3 ...
  ! or
  !   LEVELS EXPO [Step [Number [Factor]]
  !
  ! Contour definition for RGMAP or RANDOM_MAP
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LEVELS'
  character(len=varname_length) :: argum
  integer(kind=4) :: nc,istart,i
  !
  ! Exponentially spaced level definition
  real(8), save :: step=1.0d0
  integer(4), save :: nlin=3
  real(8), save :: fact=sqrt(2.0d0)
  !
  if (sic_present(0,1)) then
    ncl = 0  ! i.e. GREG%NLEVEL=0
    call sic_delvariable('GREG%LEVELS',.false.,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    !
    argum = ' '
    if (sic_narg(0).ge.1) then
      call sic_ch (line,0,1,argum,nc,.true.,error)
      if (error)  return
      call sic_upper(argum)
      if (argum.eq.'NONE') return
    endif
    !
    if (argum.eq.'EXPO') then
      !   LEVELS EXPO [Step [Number_of_Linear [Factor]]
      call sic_r8 (line,0,2,step,.false.,error)
      call sic_i4 (line,0,3,nlin,.false.,error)
      call sic_r8 (line,0,4,fact,.false.,error)
      !
      do i=1,nlin
        cl(i) = -(nlin-i+1)*step
      enddo
      do i=nlin+1,2*nlin
        cl(i) = (i-nlin)*step
      enddo
      do i=2*nlin+1,maxl
        cl(i) = fact*cl(i-1)
      enddo
      ncl = maxl
    else
      istart = sic_start(0,1)
      call sic_build_listr4(cl,ncl,maxl,line(istart:),rname,error)
      if (error) return
    endif
  endif
  !
  if (ncl.gt.0) then
    call greg_message(seve%r,rname,'Contour levels are :')
    call levels_type(ncl,cl)
    ! Create GREG%LEVELS only if it does not exist any more (otherwise,
    ! command LEVELS without argument gives an error message here)
    if (sic_present(0,1)) then
      call sic_def_real('GREG%LEVELS',cl,1,ncl,.false.,error)
    endif
  else
    call greg_message(seve%w,rname,'No contour levels')
  endif
  !
contains
  subroutine levels_type(n,arr)
    !-------------------------------------------------------------------
    ! Same as Sic r4_type, but messages can be filtered with
    !   SIC MESSAGE GREG S-R
    !-------------------------------------------------------------------
    integer(kind=4), intent(in) :: n
    real(kind=4),    intent(in) :: arr(n)
    ! Local
    character(len=75) :: mess
    integer(kind=4) :: i,j,m
    !
    m = n/5
    do i=1,5*m,5
      write(mess,100) (arr(j),j=i,i+4)
      call greg_message(seve%r,rname,mess)
    enddo
    if (5*m.ne.n) then
      write(mess,100) (arr(j),j=5*m+1,n)
      call greg_message(seve%r,rname,mess)
    endif
  100 format (5(1x,1pg14.7))
  end subroutine levels_type
end subroutine level
!
subroutine gr4_levels(nl,zlevel)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr4_levels
  use greg_error
  use greg_contours
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer,intent(in) :: nl           ! Number of levels
  real(4), intent(in) :: zlevel(*)   ! level values
  ! Local
  character(len=*), parameter :: rname='GR4_LEVELS'
  integer :: i
  character(len=60) :: chain
  logical :: error
  !
  if (nl.lt.0) then
    call greg_message(seve%e,rname,'Number of levels negative')
    errorg = .true.
    return
  elseif (nl.gt.maxl) then
    write(chain,'(A,I2,A,I6,A)') 'Levels ',maxl+1,' to ',nl,' have been lost'
    call greg_message(seve%w,rname,chain)
  endif
  ncl = min (nl,maxl)
  do i=1,ncl
    cl(i) = zlevel(i)
  enddo
  call sic_delvariable('GREG%LEVELS',.false.,error)
  if (ncl.eq.0) return
  error = .false.  ! Attempt to delete non-existent variables is not an error
  call sic_def_real('GREG%LEVELS',cl,1,ncl,.false.,error)
end subroutine gr4_levels
!
subroutine gr8_levels(nl,zlevel)
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr8_levels
  use greg_error
  use greg_contours
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer, intent(in) :: nl          ! Number of levels
  real(8), intent(in) :: zlevel(*)   ! Level values
  ! Local
  character(len=*), parameter :: rname='GR8_LEVELS'
  character(len=60) :: chain
  integer :: i
  logical :: error
  !
  if (nl.lt.0) then
    call greg_message(seve%e,rname,'Number of levels negative')
    errorg = .true.
    return
  elseif (nl.gt.maxl) then
    write(chain,'(A,I2,A,I6,A)') 'Levels ',maxl+1,' to ',nl,' have been lost'
    call greg_message(seve%w,rname,chain)
  endif
  ncl = min (nl,maxl)
  do i=1,ncl
    cl(i) = zlevel(i)
  enddo
  call sic_delvariable('GREG%LEVELS',.false.,error)
  if (ncl.eq.0) return
  error = .false.  ! Attempt to delete non-existent variables is not an error
  call sic_def_real('GREG%LEVELS',cl,1,ncl,.false.,error)
end subroutine gr8_levels
