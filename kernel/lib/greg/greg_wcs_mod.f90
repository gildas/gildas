module greg_wcs
  use gbl_constant
  use gwcs_types
  !--------------------------------------------------------------------
  ! Projection definitions
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !--------------------------------------------------------------------
  !
  ! Current projection in Greg
  type(projection_t), save :: gproj
  !
  ! Internal to GreG only
  integer(kind=4)         :: u_angle    ! Angle unit
  integer(kind=4)         :: ipole      ! Which pole visible ? 1 none, 2 North, 3 South, 4 Both
  integer(kind=4), save   :: i_system   ! Coordinate system
  real(kind=4)            :: i_equinox  ! Equinox if coordinate system is Equatorial
  real(kind=4), parameter :: i_equinox_def=2000.0  ! Default equinox if Equatorial system
  logical                 :: vpole      ! Pole at infinity
  !
  ! Remarkable points
  real(kind=8) :: axrem1,axrem2,axrem3,axrem4,axrem5,axrem6
  real(kind=8) :: ayrem1,ayrem2,ayrem3,ayrem4,ayrem5,ayrem6
  real(kind=8), save :: decmin,decmax   ! Dec minima and maxima
  real(kind=8), save :: ramin,ramax     ! RA minima and maxima
end module greg_wcs
