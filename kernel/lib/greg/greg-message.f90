!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GREG messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: greg_message_id = gpack_global_id  ! Default value for startup message
  !
end module greg_message_private
!
subroutine greg_message_set_id(id)
  use greg_interfaces, except_this=>greg_message_set_id
  use greg_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  greg_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',greg_message_id
  call greg_message(seve%d,'greg_message_set_id',mess)
  !
end subroutine greg_message_set_id
!
subroutine greg_message(mkind,procname,message)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_message
  use greg_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(greg_message_id,mkind,procname,message)
  !
end subroutine greg_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
