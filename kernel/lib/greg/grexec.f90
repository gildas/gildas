function gr_error ()
  use greg_error
  !---------------------------------------------------------------------
  ! @ public
  !	Check GREG library error status
  !---------------------------------------------------------------------
  logical :: gr_error               !
  !
  gr_error = errorg
  errorg = .false.
end function gr_error
!
subroutine gr_exec(buffer)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr_exec
  use greg_error
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public
  !	Execute a single GREG command
  ! Arguments :
  !	BUFFER	C*(*)	The command line		Input
  ! Subroutines :
  !	(SIC)
  !	RUN_GREG1, RUN_GREG2
  !---------------------------------------------------------------------
  character(len=*) :: buffer        !
  ! Local
  integer :: nl, nh
  character(len=255) :: line
  character(len=12) :: lang
  character(len=12) :: comm
  character(len=12) :: header
  character(len=1), parameter :: backslash=char(92)
  !
  save nh, nl, header, line
  !
  ! Analyse line
  header = 'G'//backslash
  nh = 2
  goto 10
  !
entry gr_exec1(buffer)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  header = 'GREG1'//backslash
  nh = 6
  goto 10
  !
entry gr_exec2(buffer)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  header = 'GREG2'//backslash
  nh = 6
  goto 10
  !
entry gr_execl(buffer)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  header = 'GTVL'//backslash
  nh = 5
  goto 10
  !
  ! Check error status
10 continue
  if (errorg) then
    call gagout (message)
    call gagout (line(1:nl))
    call sysexi (fatale)
  endif
  line  = header(1:nh)//buffer
  nl = 255
  !
  ! Analyse line
  call sic_blanc(line,nl)
  call sic_analyse(comm,line,nl,errorg)
  if (errorg) then
    message = 'F-GREG,  Error interpreting line'
    return
  endif
  call sic_lang(lang)
  !
  ! Execute line
  if (lang.eq.'GREG1') then
    call run_greg1(line,comm,errorg)
  elseif (lang.eq.'GREG2') then
    call run_greg2(line,comm,errorg)
  elseif (lang.eq.'GTVL') then
    call run_gtvl(line,comm,errorg)
    if (comm.eq.'CLEAR') call greset
  else
    message = 'F-GREG,  Unexpected language '//lang
    errorg = .true.
    return
  endif
  if (errorg) then
    message = 'F-GREG,  Error executing line'
    return
  endif
end subroutine gr_exec
