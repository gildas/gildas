subroutine gstring(n,string,angle,doclip)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gstring
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ private
  !   Interprets and writes a character string
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in) :: n       ! Length of string
  character(len=*), intent(in) :: string  !
  real(kind=8),     intent(in) :: angle   ! Text angle, in radians
  logical,          intent(in) :: doclip  ! Clip the text in the box?
  ! Local
  real*4 :: co,si,slength
  !
  co = cos(angle)      ! COS and SINE
  si = sin(angle)
  !
  ! Call the font generator
  call gtchar(n,string,cdef*expand,slength,xp,yp,co,si,6,i_font,  &
    doclip,grpoly)
  !
  ! Update the pen location to the right reference point.
  ! This will allow to use LABEL/APPEND.
  xp = xp + co*slength
  yp = yp + si*slength
end subroutine gstring
!
subroutine gstrlen(n,string,slength)
  use greg_dependencies_interfaces
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  !  Finds the actual length (in centimeters) of a character string,
  ! taking into account the current G\SET CHAR and G\SET EXPAND settings
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)  :: n        ! Length of string
  character(len=*), intent(in)  :: string   ! String
  real(kind=4),     intent(out) :: slength  ! Length in plot units
  call gtg_charlen (n,string,cdef*expand,slength,i_font)
end subroutine gstrlen
