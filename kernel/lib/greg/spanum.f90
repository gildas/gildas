subroutine spanum_to_greg(string,nchar,doexpo)
  use greg_interfaces, except_this=>spanum_to_greg
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !   This subroutine converts a floating point number produced by
  ! SPANUM such as 1.234E+07 or 1.E+12 in a string of characters
  ! allowing usual exponential notation with GREG e.g.
  !                              7
  !     1.2345E+07 -->  1.2345 10
  !
  !                       12
  !     1.E+12  ----->  10
  !
  !                         -8
  !     2.E-08  ----->  2 10
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: string  ! Character string to convert
  integer(kind=4),  intent(inout) :: nchar   ! Length of string
  logical,          intent(in)    :: doexpo  ! Prefer exponential notation when possible?
  ! Local
  character(len=80) :: chain
  logical :: first
  integer :: i,j,n,expo,ival
  real(kind=8) :: value
  character(len=1), parameter :: backslash=char(92)
  !
  i = index(string,'E')
  if (i.eq.0) return
  !
  if (.not.doexpo) then
    ! Analyze if an integer format would do it
    read(string(i+1:nchar),'(I6)') expo
    if (expo.ge.0 .and. expo.lt.6) then ! An integer may fit...
      if (i.le.expo+3) then
        read(string(1:nchar),*) value
        ival = nint(value)
        ! print *,'String ',string(i+1:nchar),'Expo ',expo,' Value ',value,ival
        write(chain,'(I6)') ival
        string = chain
        nchar = 6
        return
      endif
    endif
  endif
  !
  ! mantissa analysis
  if (i.eq.3) then
    if (string(1:2).eq.'1.') then
      chain = '10'//backslash//backslash//'U'
      n = 5
    else
      chain = string(1:1)//' 10'//backslash//backslash//'U'
      n = 7
    endif
  else
    chain = string(1:i-1)//' 10'//backslash//backslash//'U'
    n = 5+i
  endif
  !
  ! Exponant analysis
  first = .true.
  do j=i+1,nchar
    if (string(j:j).eq.'+') cycle
    if (string(j:j).eq.'0' .and. first) cycle
    n = n+1
    chain(n:n) = string(j:j)
    if (string(j:j).ne.'-') first=.false.
  enddo
  !
  ! FIRST indicates no significant exponant (E+00)
  if (first) then
    if (string(i-1:i-1).ne.'.') then
      chain = string(1:i-1)
      n=i-1
    else
      chain = string(1:i-2)
      n=i-2
    endif
  else
    chain(n+1:) = backslash//backslash//'D'
    n = n+3
  endif
  !
  string = chain
  nchar  = n
  if (nchar.gt.len(string)) then
    call greg_message(seve%w,'CONVER','Label too long, truncated')
    nchar=len(string)
  endif
end subroutine spanum_to_greg
!
subroutine gag_cflabh(chrstr,x,w,d,type,short)
  !---------------------------------------------------------------------
  ! @ private
  !  Convert floating point value to a display of the form of the
  ! "Babylonian" ddd mm ss.s, where each field represents a sexagesimal
  ! increment over its successor. The value X must be in the units of
  ! the rightmost field. For instance, X must be in seconds of time for
  ! hours, minutes, seconds
  !
  ! This routine insert the proper labelling in GreG style display.
  !---------------------------------------------------------------------
  character(len=*), intent(out) :: chrstr  ! Formatted string
  real(kind=8),     intent(in)  :: x       ! Input number
  integer(kind=4),  intent(out) :: w       ! Length of string
  integer(kind=4),  intent(in)  :: d       ! Number of decimals
  integer(kind=4),  intent(in)  :: type    ! Type of label (None/Hour/Degree)
  logical,          intent(in)  :: short   ! Drop off zero-leading fields?
  ! Local
  real(kind=8) :: y
  integer(kind=4) :: h, m, s, f
  integer(kind=4) :: lh, lm, ls, dd, fd, lt(3), l
  character(len=20) :: th,ts  ! These may be large
  character(len=2) :: tm  ! This one is always 2 digits
  character(len=4) :: hour(3),minu(3),seco(3)
  character(len=1), parameter :: backslash=char(92)
  !
  data hour/' ',' uh','^'/
  data minu/' ',' um',''''/
  data seco/' ',' us','`'/
  data lt/1,3,1/
  !
  if (type.eq.2) then  ! format_sexage_hour
    hour(2)(1:1)=backslash
    minu(2)(1:1)=backslash
    seco(2)(1:1)=backslash
  endif
  !
  y = abs (x)       ! Positive computations
  h = y
  h = h / 3600      ! Hours
  y = y - (3600*h)
  m = y
  m = m / 60        ! Minutes
  y = y - (60*m)
  s = y             ! Seconds
  y = y - s
  dd = max(0,d)
  fd = 10**dd
  f = fd*y + 0.5d0  ! Fraction of seconds
  if (f.ge.fd) then
    f = f - fd
    s = s + 1
  endif
  if (s.gt.59) then
    s = 0
    m = m + 1
  endif
  if (m.gt.59) then
    m = 0
    h = h + 1
  endif
  !
  ! Initialize the output string, format the sign field
  if (x.lt.0.d0) then
    chrstr = '-'
    w = 1
  else
    chrstr = ''
    w = 0
  endif
  !
  ! Format the hours field
  if (h.eq.0) then
    if (short) then
      lh = 0
    else
      th = '00'
      lh = 2
    endif
  else
    lh = ceiling(log10(h+.1))  ! 1<=h<=9 => lh=1
    if (lh.eq.1) then
      write(th,121) h
    elseif (lh.eq.2) then
      write(th,122) h
    elseif (lh.eq.3) then
      write(th,123) h
    elseif (lh.eq.4) then
      write(th,124) h
    else
      write(th,120) h
    endif
  endif
  ! Concatenate
  l = lt(type)
  if (lh.gt.0) then
    if (w.eq.0) then
      chrstr = th(1:lh)//hour(type)(1:l)
      w = lh+l
    else
      chrstr = chrstr(1:w)//th(1:lh)//hour(type)(1:l)
      w = w+lh+l
    endif
  endif
  !
  ! Format the minutes field
  if (short .and. h.eq.0 .and. m.eq.0) then
    lm = 0
  else
    write(tm,112) m
    lm = 2
  endif
  ! Concatenate
  if (lm.gt.0) then
    if (w.eq.0) then
      chrstr = tm(1:lm)//minu(type)(1:l)
      w = lm+l
    else
      chrstr = chrstr(1:w)//tm(1:lm)//minu(type)(1:l)
      w = w+lm+l
    endif
  endif
  !
  ! Format the seconds field
  write(ts,112) s
  ls = 2
  ! Second units
  ts(ls+1:) = seco(type)(1:l)
  ls=ls+l
  ! Add the fraction of seconds
  if (dd.gt.0) then
    ! Add a dot
    ts(ls+1:) = backslash
    ts(ls+2:) = 'b.'
    ls=ls+3
    ! Add the fraction value
    if (dd.eq.1) then
      write(ts(ls+1:),111) f
    elseif (dd.eq.2) then
      write(ts(ls+1:),112) f
    elseif (dd.eq.3) then
      write(ts(ls+1:),113) f
    elseif (dd.eq.4) then
      write(ts(ls+1:),114) f
    else
      write(ts(ls+1:),115) f
      dd = 5
    endif
    ls=ls+dd
  endif
  ! Concatenate
  if (w.eq.0) then
    chrstr = ts(1:ls)
    w = ls
  else
    chrstr = chrstr(1:w)//ts(1:ls)
    w = w+ls
  endif
  !
111 format(i1.1)
112 format(i2.2)
113 format(i3.3)
114 format(i4.4)
115 format(i5.5)
  !
120 format (i10)
121 format (i1)
122 format (i2)
123 format (i3)
124 format (i4)
end subroutine gag_cflabh
