subroutine zmont(z,id1,nxi,nxf,nyi,nyf,x0,y0,dx,dy,az,el,fe)
  use greg_interfaces, except_this=>zmont
  !---------------------------------------------------------------------
  ! @ private
  !  This subroutine plots a perspective of profiles of a bidimensional
  ! functions. (L. DELGADO Yebes 1983)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: id1       ! First dimension of Z
  real(kind=4),    intent(inout) :: z(id1,*)  ! Function values array, destroyed in the process
  integer(kind=4), intent(in)    :: nxi       ! First index to be shown in X
  integer(kind=4), intent(in)    :: nxf       ! Last index to be shown in X
  integer(kind=4), intent(in)    :: nyi       ! First index to be shown in Y
  integer(kind=4), intent(in)    :: nyf       ! Last index to be shown in Y
  real(kind=4),    intent(in)    :: x0        ! X paper coordinate of lower left point for Z=0.
  real(kind=4),    intent(in)    :: y0        ! Y paper coordinate of this point
  real(kind=4),    intent(in)    :: dx        ! X Size of projection area
  real(kind=4),    intent(in)    :: dy        ! Y Size of projection area
  real(kind=4),    intent(in)    :: az        ! Azimuth of projection (-90<AZ<90)
  real(kind=4),    intent(in)    :: el        ! Elevation of projection (0<EL<90)
  real(kind=4),    intent(in)    :: fe        ! Scaling factor in Z
  ! Local
  integer(kind=4) :: nx,ny,i,j
  real(kind=4) :: gr,sa,ca,se,cefe,ayx,ayy,axx,axy,a1
  logical :: error
  ! Data
  data gr/57.29578/
  !
  nx=nxf-nxi
  ny=nyf-nyi
  sa=sin(az/gr)
  ca=cos(az/gr)
  se=sin(el/gr)
  cefe=cos(el/gr)*fe
  ayx=dx/nx*sa*se
  ayy=dy/ny*ca*se
  axx=dx/nx*ca
  axy=-dy/ny*sa
  do j=nyi,nyf
    a1=ayy*(j-nyi)
    do i=nxi,nxf
      z(i,j)=y0+ayx*(i-nxi)+a1+z(i,j)*cefe
    enddo
  enddo
  !
  ! Create a segment for the plot
  error = .false.
  call gr_segm('PERSP',error)
  !
  do j=nyi,nyf
    !          IF (MOD(J,2).EQ.0) THEN
    !             DO I=NXF-1,NXI,-1
    !                CALL ZSOM(Z,ID1,X0,AXX,AXY,NXI,NXF,NYI,I,J)
    !             ENDDO
    !          ELSE
    do i=nxi,nxf-1
      call zsom(z,id1,x0,axx,axy,nxi,nxf,nyi,i,j)
    enddo
    !          ENDIF
  enddo
  !
  ! Can be avoided if Z is destroyed
  !       DO J=NYI,NYF
  !          A1=AYY*(J-NYF)
  !          DO I=NXI,NXF
  !             Z(I,J)=(Z(I,J)-Y0-AYX*(I-NXI)-A1)/CEFE
  !          ENDDO
  !       ENDDO
end subroutine zmont
!
subroutine zsom(z,id1,x0,axx,axy,nxi,nxf,nyi,i,j)
  use greg_interfaces, except_this=>zsom
  !---------------------------------------------------------------------
  ! @ private
  !  This subroutine is used by ZMONT for determining visible areas
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: id1       ! First dimension of Z
  real(kind=4),    intent(in) :: z(id1,*)  ! Function values array
  real(kind=4),    intent(in) :: x0        ! X paper coordinate of lower left point for Z=0
  real(kind=4),    intent(in) :: axx       ! X paper coordinate increment between consecutive lines
  real(kind=4),    intent(in) :: axy       ! Y paper coordinate increment
  integer(kind=4), intent(in) :: nxi       ! First index to be shown in X
  integer(kind=4), intent(in) :: nxf       ! Last index to be shown in X
  integer(kind=4), intent(in) :: nyi       ! First index to be shown in Y
  integer(kind=4), intent(in) :: i         ! X Index of the starting point of current line
  integer(kind=4), intent(in) :: j         ! Y Index of this point
  ! Local
  integer(kind=4) :: l,ili,ilf
  real(kind=4) :: xi,xf,xj,xult,yult
  real(kind=4) :: yi,yf,xili
  ! Data
  data xult,yult/2*-1./
  !
  xj=x0+(j-nyi)*axy
  xi=xj+(i-nxi)*axx
  xf=xi+axx                    !xj+(i-nxi+1)*axx
  yi=z(i,j)
  yf=z(i+1,j)
  if (j.eq.nyi) then
    if (mod(j,2).eq.0) then
      if (i.eq.nxf-1) call grelocate (xf,yf)
      call gdraw (xi,yi)
    else
      if (i.eq.nxi) call grelocate (xi,yi)
      call gdraw (xf,yf)
    endif
    return
  endif
  do l=j-1,nyi,-1
    ili=nxi+(xi-x0-(l-nyi)*axy)/axx
    ilf=ili+2
    if (ili.lt.nxi) ili=nxi
    if (ilf.gt.nxf) ilf=nxf
    if (ili.lt.ilf) then
      xili=x0+(ili-nxi)*axx+(l-nyi)*axy
      call zmx(z,id1,ili,ilf,l,xili,axx,xi,yi,xf,yf)
      if (xi.ge.xf) return
    endif
  enddo
  if (mod(j,2).eq.0) then
    if (xult.ne.xf.or.yult.ne.yf) call grelocate (xf,yf)
    call gdraw (xi,yi)
    xult=xi
    yult=yi
  else
    if (xult.ne.xi.or.yult.ne.yi) call grelocate (xi,yi)
    call gdraw (xf,yf)
    xult=xf
    yult=yf
  endif
end subroutine zsom
!
subroutine zmx(z,id1,ili,ilf,l,xili,axx,xi,yi,xf,yf)
  use greg_interfaces, except_this=>zmx
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: id1       !
  real(kind=4),    intent(in)    :: z(id1,*)  !
  integer(kind=4), intent(in)    :: ili,ilf   !
  integer(kind=4), intent(in)    :: l         !
  real(kind=4),    intent(in)    :: xili      !
  real(kind=4),    intent(in)    :: axx       !
  real(kind=4),    intent(inout) :: xi,yi     !
  real(kind=4),    intent(inout) :: xf,yf     !
  ! Local
  real(kind=4) :: ryx1,ryx2,x1,x,y
  integer(kind=4) :: k
  logical :: tap
  !
  tap=.false.
  ryx1=(yf-yi)/(xf-xi)
  do k=0,ilf-ili-1
    ryx2 = (z(ili+k+1,l)-z(ili+k,l))/axx
    if (ryx1.eq.ryx2) cycle
    x1 = xili+axx*k
    x = (xi*ryx1-x1*ryx2-yi+z(ili+k,l))/(ryx1-ryx2)
    if (x.ge.max(xi,x1).and.x.le.min(xf,x1+axx)) then
      y=(x-xi)*ryx1+yi
      if (ryx1.gt.ryx2) then
        xi = max(xi,x)
        if (ryx1.ge.0) then
          yi=max(yi,y)
        else
          yi=min(yi,y)
        endif
      else
        xf=min(xf,x)
        if (ryx1.ge.0) then
          yf=min(yf,y)
        else
          yf=max(yf,y)
        endif
      endif
      if (xi.ge.xf) return
    elseif (x.lt.max(xi,x1)     .and. ryx2.gt.ryx1 .or.  &
            x.gt.min(xf,x1+axx) .and. ryx2.lt.ryx1) then
      if (k.eq.ilf-ili-2) then
        tap=.true.
      elseif (tap.or.k.eq.0) then
        xf=xi
        return
      endif
    endif
  enddo
  return
end subroutine zmx
