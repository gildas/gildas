!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the GREG package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine greg_pack_set(pack)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_pack_set
  use gpack_def
  !----------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  ! Public package definition routine.
  ! It defines package methods and dependencies to other packages.
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack ! Package info structure
  !
  pack%name='greg'
  pack%ext='.greg'
  pack%depend(1:1) = (/ locwrd(sic_pack_set) /)
  pack%init=locwrd(greg_pack_init)
  pack%clean=locwrd(greg_pack_clean)
  pack%authors="J.Pety, S.Bardeau, S.Guilloteau, E.Reynier"
  !
end subroutine greg_pack_set
!
subroutine greg_pack_init(gpack_id,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_pack_init
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Private routine to set the GREG environment.
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Set libraries id's
  call gchar_message_set_id(gpack_id)
  call gcont_message_set_id(gpack_id)
  call gtv_message_set_id(gpack_id)
  call gtv_c_message_set_id(gpack_id)
  call greg_message_set_id(gpack_id)
  call gcore_c_message_set_id(gpack_id)
#ifdef GAG_USE_GTK
  call gui_c_message_set_id(gpack_id)
#endif
  !
  ! Local language
  call load_greg('INTERACTIVE')
  call load_greg('INTERACTIVE GREG3')
  !
  if (.not.gmaster_hide_gui()) then
     call gr_exec('DEVICE IMAGE WHITE')
     error = gr_error()
     if (error) return
  endif
  !
end subroutine greg_pack_init
!
subroutine greg_pack_clean(error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_pack_clean
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Private routine to clean the GREG environment
  ! (e.g. Remove temporary buffers, unregister languages...)
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call exit_greg(error)
  ! if (error)  continue
  !
  ! Clear the graphic screen
  call exit_clear
  !
end subroutine greg_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
