subroutine gr_where(xa,ya,x,y)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  !	Returns the pen position with same conventions as GR_CURS
  !---------------------------------------------------------------------
  real(8), intent(out) :: xa     ! X User coordinates
  real(8), intent(out) :: ya     ! Y User coordinates
  real(4), intent(out) :: x      ! X Plot coordinates
  real(4), intent(out) :: y      ! Y Plot coordinates
  !
  x = xp
  y = yp
  if (axis_xlog) then
    xa = exp((x-gx1)/gux + lux)
  else
    xa = (x-gx1)/gux + gux1
  endif
  if (axis_ylog) then
    ya = exp((y-gy1)/guy + luy)
  else
    ya = (y-gy1)/guy + guy1
  endif
end subroutine gr_where
