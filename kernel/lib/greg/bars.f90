subroutine bars(line,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>bars
  use gildas_def
  use sic_types
  use greg_kernel
  use greg_xyz
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Support routine for command
  !	ERRORBAR [+X] [X] [-X] [+Y] [Y] [-Y] [[+/-]O] [X_var Y_var Z_var]
  !	Draw error bars at data point location. The error bar size is
  !	defined by array Z value.
  !---------------------------------------------------------------------
  character(len=*)               :: line   ! Input command line
  logical,         intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='ERRORBAR'
  type(sic_descriptor_t) :: xinca,yinca,zinca
  integer(kind=address_length) :: xaddr,yaddr,zaddr
  integer(kind=size_length) :: ixy
  integer(kind=4) :: form,narg,nc,ier
  character(len=4) :: cloc
  character(len=60) :: chain
  real(kind=8) :: bval,eval
  logical :: orient
  real(kind=8), allocatable :: ovalues(:)
  character(len=varname_length) :: var
  !
  save xinca,yinca,zinca
  !
  cloc = ' '
  call sic_ke (line,0,1,cloc,nc,.true.,error)
  if (error) return
  if (cloc.ne.'+X' .and. cloc.ne.'-X' .and. cloc.ne.'X' .and.  &
      cloc.ne.'+Y' .and. cloc.ne.'-Y' .and. cloc.ne.'Y' .and.  &
      cloc.ne.'+O' .and. cloc.ne.'-O' .and. cloc.ne.'O') then
    chain = 'Invalid error bar type '//cloc
    error = .true.
    call greg_message(seve%e,rname,chain)
    return
  endif
  error = .false.
  orient = cloc.eq.'+O' .or. cloc.eq.'-O' .or. cloc.eq.'O'
  !
  narg = sic_narg(0)
  if (narg.ne.1.and.narg.ne.4.and..not.(narg.eq.5.and.orient)) then
    call greg_message(seve%e,rname,'1 or 4 arguments required')
    error = .true.
    return
  endif
  !
  ixy = 0
  form = fmt_r8
  if (narg.ge.4) then
    call get_same_inca ('ERRORBAR',line,0,2,form,ixy,xinca,error)
    if (error) return
    call get_same_inca ('ERRORBAR',line,0,3,form,ixy,yinca,error)
    if (error) then
      call sic_volatile(xinca)
      return
    endif
    call get_same_inca ('ERRORBAR',line,0,4,form,ixy,zinca,error)
    if (error) then
      call sic_volatile(xinca)
      call sic_volatile(yinca)
      return
    endif
  else
    call get_greg_inca ('ERRORBAR','X',form,ixy,xinca,error)
    if (error) return
    call get_greg_inca ('ERRORBAR','Y',form,ixy,yinca,error)
    if (error) then
      call sic_volatile(xinca)
      return
    endif
    call get_greg_inca ('ERRORBAR','Z',form,ixy,zinca,error)
    if (error) then
      call sic_volatile(xinca)
      call sic_volatile(yinca)
      return
    endif
  endif
  !
  ! Non trivial errorbar orientation: get the specifier array
  if (orient) then
    allocate(ovalues(ixy),stat=ier)
    if (failed_allocate(rname,'O values',ier,error))  goto 100
    if (narg.eq.5) then
      ! Orientation specified by an array
      call sic_ke (line,0,5,var,nc,.true.,error)
      if (error)  goto 100
      call sic_variable_getr8_1d(rname,var,ovalues,ixy,error)
      if (error)  goto 100
    else
      ! Fill a scratch array with current marker orientation
      ovalues(:) = dble(sangle)
    endif
  else
    allocate(ovalues(0))  ! Unused (this is valid Fortran array)
  endif
  !
  xaddr = gag_pointer(xinca%addr,memory)
  yaddr = gag_pointer(yinca%addr,memory)
  zaddr = gag_pointer(zinca%addr,memory)
  !
  call gtsegm(rname,error)
  bval = cblank
  eval = eblank
  call gr8_bars(cloc,ixy,memory(xaddr),memory(yaddr),memory(zaddr),  &
   ovalues,bval,eval)
  call gtsegm_close(error)
  !
100 continue
  call sic_volatile(xinca)
  call sic_volatile(yinca)
  call sic_volatile(zinca)
  if (allocated(ovalues))  deallocate(ovalues)
end subroutine bars
!
subroutine gr8_bars (cloc,nxy,x,y,err,orient,bval,eval)
  use phys_const
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces
  use greg_kernel
  use greg_pen
  !---------------------------------------------------------------------
  ! @ no-interface (because of type mismatch)
  ! 	Plot an errorbar of length ERR at point (X,Y)
  ! 	LOC = '-X' 'X' '+X' '-Y' 'Y' '+Y' '-O' 'O' '+O'
  ! Arguments :
  !	CLOC	C*(*)	Conventional orientation of errorbar
  !	X	R*8 (*)	Array of X user coordinates
  !	Y	R*8 (*)	Array of Y user coordinates
  !	ERR	R*8 (*) Array of error values
  !	ORIENT	R*8 (*) Array of errorbar orientations (only used for CLOC='O')
  !	NXY	I	Number of points
  ! Subroutines :
  !	INQANG	/ SETANG
  !	DRAW / RELOCATE
  !	SETDAS
  !	POINT
  !---------------------------------------------------------------------
  character(len=*) :: cloc          !
  integer(kind=4) :: nxy                    !
  real(kind=8) :: x(*)                    !
  real(kind=8) :: y(*)                    !
  real(kind=8) :: err(*)                  !
  real(kind=8) :: orient(*)               !
  real(kind=8) :: bval                    !
  real(kind=8) :: eval                    !
  ! Local
  character(len=*), parameter :: rname='BARS'
  real(kind=8) :: xx,yy,co,so,ss
  integer(kind=4) :: j,idas
  real(kind=4) :: o_angl,o_sangl,symbsize
  logical :: error
  real(kind=4), parameter :: epsr4=1e-7
  !
  if (nxy.lt.1) return
  call inqang(o_angl,o_sangl)
  if (cloc.eq.'+X' .or. cloc.eq.'-X' .or. cloc.eq.'X') then
    call setang(o_angl,90.)
  elseif (cloc.eq.'+Y' .or. cloc.eq.'-Y' .or. cloc.eq.'Y') then
    call setang(o_angl,0.)
  elseif (cloc.eq.'+O' .or. cloc.eq.'-O' .or. cloc.eq.'O') then
    call inqsym(symbsize)
    ss = 0.5*symbsize/sqrt(abs(gux*guy))
    if (abs(guy).lt.abs(gux)*(1-10*epsr4) .or.  &
        abs(guy).gt.abs(gux)*(1+10*epsr4))  &
      call greg_message(seve%w,rname,'O code with mismatched axes')
  else
    call greg_message(seve%e,rname,'Unknown orientation code')
    return
  endif
  call inqdas(idas)
  !
  if (penupd) call setpen(cpen)
  !
  error = .false.
  !
  if (cloc.eq.'+X') then
    if (idas.eq.1) then
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j) + err(j)
          yy = y(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'+Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy + err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy + err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'-X') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j) - err(j)
          yy = y(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'-Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'X') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          xx = x(j) - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          xx = x(j) - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          xx = x(j) - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          yy = y(j) - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          yy = y(j) - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          yy = y(j) - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'+O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          call relocate(xx,yy)
          xx = xx + err(j)*co
          yy = yy + err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)*co
          yy = yy + err(j)*so
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  elseif (cloc.eq.'-O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)*co
          yy = yy - err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)*cos(rad_per_deg*orient(j))
          yy = yy - err(j)*sin(rad_per_deg*orient(j))
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  elseif (cloc.eq.'O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
          call relocate(xx,yy)
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx,yy)
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  else
    call greg_message(seve%e,rname,'Unknown orientation code')
    error = .true.
  endif
  !
  if (idas.ne.1) then
    ! If idas.ne.1, this means that we have forced the current dash
    ! style to 1 for the ticks (setdas(1)). This style is applied to
    ! the currently opened segment (ERRORTICK). We can not reset it
    ! to its original value (setdas(idas)), because this would affect
    ! the segment ERRORTICK: it must be closed before. Since it is
    ! closed out of this subroutine, just turn the Greg global flag to
    ! the correct value, and set "pen update" to true for a later udpate.
    cdashe = idas
    penupd = .true.
  endif
  call setang(o_angl,o_sangl)
  !
end subroutine gr8_bars
!
subroutine gr4_bars (cloc,nxy,x,y,err,orient,bval,eval)
  use phys_const
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>gr4_bars
  use greg_kernel
  use greg_pen
  !---------------------------------------------------------------------
  ! @ public
  !	Plot an errorbar of length ERR at point (X,Y)
  ! 	LOC = '-X' 'X' '+X' '-Y' 'Y' '+Y' '-O' 'O' '+O'
  ! Arguments :
  !	CLOC	C*(*)	Conventional orientation of errorbar
  !	X	R*4 (*)	Array of X user coordinates
  !	Y	R*4 (*)	Array of Y user coordinates
  !	ERR	R*4 (*) Array of error values
  !	ORIENT	R*4 (*) Array of errorbar orientations (only used for CLOC='O')
  !	NXY	I	Number of points
  ! Subroutines :
  !	INQANG	/ SETANG
  !	DRAW / RELOCATE
  !	SETDAS
  !	POINT
  !---------------------------------------------------------------------
  character(len=*) :: cloc          !
  integer(kind=4) :: nxy                    !
  real(kind=4) :: x(*)                    !
  real(kind=4) :: y(*)                    !
  real(kind=4) :: err(*)                  !
  real(kind=4) :: orient(*)               !
  real(kind=4) :: bval                    !
  real(kind=4) :: eval                    !
  ! Local
  character(len=*), parameter :: rname='BARS'
  real(kind=8) :: xx,yy,co,so,ss
  integer(kind=4) :: j,idas
  real(kind=4) :: o_angl,o_sangl,symbsize
  logical :: error
  real(kind=4), parameter :: epsr4=1e-7
  !
  if (nxy.lt.1) return
  call inqang(o_angl,o_sangl)
  if (cloc.eq.'+X' .or. cloc.eq.'-X' .or. cloc.eq.'X') then
    call setang(o_angl,90.)
  elseif (cloc.eq.'+Y' .or. cloc.eq.'-Y' .or. cloc.eq.'Y') then
    call setang(o_angl,0.)
  elseif (cloc.eq.'+O' .or. cloc.eq.'-O' .or. cloc.eq.'O') then
    call inqsym(symbsize)
    ss = 0.5*symbsize/sqrt(abs(gux*guy))
    if (abs(guy).lt.abs(gux)*(1-10*epsr4) .or.  &
        abs(guy).gt.abs(gux)*(1+10*epsr4))  &
      call greg_message(seve%w,rname,'O code with mismatched axes')
  else
    call greg_message(seve%e,rname,'Unknown orientation code')
    return
  endif
  call inqdas(idas)
  !
  if (penupd) call setpen(cpen)
  !
  error = .false.
  !
  if (cloc.eq.'+X') then
    if (idas.eq.1) then
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j = 1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j) + err(j)
          yy = y(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'+Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy + err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy + err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'-X') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j) - err(j)
          yy = y(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'-Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          yy = yy - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'X') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          xx = x(j) - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          xx = x(j) - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j)
          xx = x(j) + err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          xx = x(j) - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'Y') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          yy = y(j) - err(j)
          call draw(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          yy = y(j) - err(j)
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          yy = y(j) + err(j)
          xx = x(j)
          call relocate(xx,yy)
          call gr_point(2,0)
          yy = y(j) - err(j)
          call relocate(xx,yy)
          call gr_point(2,0)
        endif
      enddo
    endif
  elseif (cloc.eq.'+O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)*co
          yy = yy + err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx + err(j)*co
          yy = yy + err(j)*so
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  elseif (cloc.eq.'-O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)*co
          yy = yy - err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j)
          yy = y(j)
          call relocate(xx,yy)
          xx = xx - err(j)*co
          yy = yy - err(j)*so
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  elseif (cloc.eq.'O') then
    if (idas.eq.1) then
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
          call relocate(xx,yy)
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call draw(xx,yy)
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    else
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx,yy)
          xx = x(j) + err(j)*co
          yy = y(j) + err(j)*so
          call draw(xx,yy)
        endif
      enddo
      call gtsegm_close(error)
      call gtsegm('ERRORTICK',error)
      call setdas(1)  ! Ticks always use solid lines
      do j=1,nxy
        if (y(j).eq.y(j).and.abs(y(j)-bval).gt.eval) then
          co = cos(rad_per_deg*orient(j))
          so = sin(rad_per_deg*orient(j))
          xx = x(j) - err(j)*co
          yy = y(j) - err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
          xx = x(j) + err(j)*so
          yy = y(j) + err(j)*so
          call relocate(xx+so*ss,yy-co*ss)
          call draw(xx-so*ss,yy+co*ss)
        endif
      enddo
    endif
  else
    call greg_message(seve%e,rname,'Unknown orientation code')
    error = .true.
  endif
  !
  if (idas.ne.1) then
    ! If idas.ne.1, this means that we have forced the current dash
    ! style to 1 for the ticks (setdas(1)). This style is applied to
    ! the currently opened segment (ERRORTICK). We can not reset it
    ! to its original value (setdas(idas)), because this would affect
    ! the segment ERRORTICK: it must be closed before. Since it is
    ! closed out of this subroutine, just turn the Greg global flag to
    ! the correct value, and set "pen update" to true for a later udpate.
    cdashe = idas
    penupd = .true.
  endif
  call setang(o_angl,o_sangl)
  !
end subroutine gr4_bars
