subroutine greg_box(line,error)
  use gildas_def
  use phys_const
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_box
  use greg_axes
  use greg_kernel
  use greg_pen
  use greg_types
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  ! GREG Support routine for command
  !   BOX [Parallel [Orthogonal [In]]]  [/ABSOLUTE]
  !       [Orthogonal [Parallel [Out]]] [/UNIT Angle]
  !       [None [None [None]]]
  !                                     [/LABEL Pen]
  !                                     [/FILL Color]
  !                                     [/BRIEF [X|Y]]
  !                                     [/NOBRIEF [X|Y]]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='BOX'
  character(len=1) :: c1,c2,c3
  real(kind=8) :: ax1,ax2,ay1,ay2,a
  real(kind=8), parameter :: precision=1.0d-10
  integer(kind=size_length), parameter :: nsummits=5
  real(kind=4) :: boxx(nsummits),boxy(nsummits)
  type(axis_t) :: axis(4)
  real(kind=4) :: loffset
  logical :: dolabel,fancypen
  integer(kind=4) :: lpen,open,ncolo,nc
  character(len=12) :: argum,name,angu(4)
  integer(kind=4) :: c_angle,n,ioff
  integer(kind=4) :: xsix,ysix
  ! Data
  data angu /'SECOND','MINUTE','DEGREE','RADIAN'/
  !
  fancypen=sic_present(3,1)
  if (fancypen) then
    call sic_i4 (line,3,1,lpen,.true.,error)
    if (error) return
  endif
  !
  ! Decode arguments
  c1 = 'P'
  c2 = 'O'
  c3 = 'I'
  call sic_ke (line,0,1,c1,nc,.false.,error)
  if (error) goto 99
  call sic_ke (line,0,2,c2,nc,.false.,error)
  if (error) goto 99
  call sic_ke (line,0,3,c3,nc,.false.,error)
  if (error) goto 99
  if (index('POIN',c1).eq.0) goto 99
  if (index('POIN',c2).eq.0) goto 99
  if (index('POIN',c3).eq.0) goto 99
  !
  ! Force dashed to none for the box outline. Have ever seen a dashed box?
  if (sic_present(4,0)) then
    call gtv_pencol_arg2id(rname,line,4,1,ncolo,error)
    if (error) return
    call gtsegm('BACK',error)
    call setcol(ncolo)
    boxx(1) = gx1
    boxy(1) = gy1
    boxx(2) = gx1
    boxy(2) = gy2
    boxx(3) = gx2
    boxy(3) = gy2
    boxx(4) = gx2
    boxy(4) = gy1
    boxx(5) = gx1
    boxy(5) = gy1
    call gr_fillpoly(nsummits,boxx,boxy)
    call gtsegm_close(error)
    if (error) return
  endif
  !
  ! Simple outer box
  if (c3.eq.'N') then
    call gtsegm('BOX',error)
    if (penupd) call setpen(cpen)
    call setdas(1)
    call grelocate(gx1,gy1)
    call gdraw(gx2,gy1)
    call gdraw(gx2,gy2)
    call gdraw(gx1,gy2)
    call gdraw(gx1,gy1)
    if (penupd) call setpen(cpen)
    call gtsegm_close(error)
    return
  endif
  !
  ! No projection, No system, Nothing
  if (gproj%type.eq.p_none .and. i_system.eq.type_un .and. sic_present(1,0)) then
    call greg_message(seve%w,rname,'/ABSOLUTE coordinates ignored when '//  &
    'no projection')
    ax1 = gux1
    ax2 = gux2
    ay1 = guy1
    ay2 = guy2
    xsix = format_decimal
    ysix = format_decimal
    !
    ! Absolute labelling
  elseif (sic_present(1,0).and.(abs(gproj%angle).le.precision)) then
    if (max(abs(gux1),abs(gux2),abs(guy1),abs(guy2)).gt.0.2 .and.  &
        gproj%type.ne.p_none) then
      call greg_message(seve%e,rname,  &
        'Field of view is too large for absolute labelling')
      call greg_message(seve%e,rname,'Try GRID /LABEL')
      error = .true.
      return
    endif
    if (i_system.eq.type_eq .or. i_system.eq.type_ic) then
      xsix = format_sexage_hour
      ysix = format_sexage_degree
      ax1 = (axrem1+axrem2)*0.5d0/pi*3.6d3*1.2d1
      ax2 = (axrem5+axrem6)*0.5d0/pi*3.6d3*1.2d1
      ay1 = (ayrem1+ayrem5)*0.5d0/pi*3.6d3*1.8d2
      ay2 = (ayrem2+ayrem6)*0.5d0/pi*3.6d3*1.8d2
    elseif (i_system.eq.type_ga) then
      if (xsixty.eq.format_sexage_none) then
        ax1 = (axrem1+axrem2)*0.5d0/pi*1.8d2*3.6d3
        ax2 = (axrem5+axrem6)*0.5d0/pi*1.8d2*3.6d3
        xsix = format_sexage_degree
      else
        ax1 = (axrem1+axrem2)*0.5d0/pi*1.8d2
        ax2 = (axrem5+axrem6)*0.5d0/pi*1.8d2
        xsix = format_decimal
      endif
      if (ysixty.eq.format_sexage_none) then
        ay1 = (ayrem1+ayrem5)*0.5d0/pi*1.8d2*3.6d3
        ay2 = (ayrem2+ayrem6)*0.5d0/pi*1.8d2*3.6d3
        ysix = format_sexage_degree
      else
        ay1 = (ayrem1+ayrem5)*0.5d0/pi*1.8d2
        ay2 = (ayrem2+ayrem6)*0.5d0/pi*1.8d2
        ysix = format_decimal
      endif
    else
      if (xsixty.eq.format_sexage_none) then
        ax1 = (axrem1+axrem2)*0.5d0/pi*1.8d2*3.6d3
        ax2 = (axrem5+axrem6)*0.5d0/pi*1.8d2*3.6d3
        xsix = format_sexage_none
      else
        ax1 = (axrem1+axrem2)*0.5d0/pi*1.8d2
        ax2 = (axrem5+axrem6)*0.5d0/pi*1.8d2
        xsix = format_decimal
      endif
      if (ysixty.eq.format_sexage_none) then
        ay1 = (ayrem1+ayrem5)*0.5d0/pi*1.8d2*3.6d3
        ay2 = (ayrem2+ayrem6)*0.5d0/pi*1.8d2*3.6d3
        ysix = format_sexage_none
      else
        ay1 = (ayrem1+ayrem5)*0.5d0/pi*1.8d2
        ay2 = (ayrem2+ayrem6)*0.5d0/pi*1.8d2
        ysix = format_decimal
      endif
    endif
    if (gux1.lt.gux2) then
      a = ax1
      ax1 = ax2
      ax2 = a
    endif
    if (guy1.gt.guy2) then
      a = ay1
      ay1 = ay2
      ay2 = a
    endif
  else
    if (sic_present(1,0)) then
      call greg_message(seve%w,rname,'/ABSOLUTE unsupported for rotated axis')
      call greg_message(seve%w,rname,'Try GRID /LABEL')
    endif
    !
    ! Relative labelling in current angle unit
    c_angle = u_angle
    if (sic_present(2,0)) then
      call sic_ke (line,2,1,name,nc,.true.,error)
      if (error) return
      call sic_ambigs('BOX',name,argum,n,angu,4,error)
      if (error) return
      c_angle = n
    endif
    xsix = format_decimal
    ysix = format_decimal
    if (c_angle.eq.u_second) then
      ax1 = gux1/pi*3.6d3*1.8d2
      ax2 = gux2/pi*3.6d3*1.8d2
      ay1 = guy1/pi*3.6d3*1.8d2
      ay2 = guy2/pi*3.6d3*1.8d2
    elseif (c_angle.eq.u_minute) then
      ax1 = gux1/pi*6.0d1*1.8d2
      ax2 = gux2/pi*6.0d1*1.8d2
      ay1 = guy1/pi*6.0d1*1.8d2
      ay2 = guy2/pi*6.0d1*1.8d2
    elseif (c_angle.eq.u_degree) then
      ax1 = gux1/pi*1.8d2
      ax2 = gux2/pi*1.8d2
      ay1 = guy1/pi*1.8d2
      ay2 = guy2/pi*1.8d2
    else
      ax1 = gux1
      ax2 = gux2
      ay1 = guy1
      ay2 = guy2
    endif
  endif
  !
  if (c3.ne.'O') then
    loffset = 1.
    ioff = 0
  else
    loffset = -2.
    ioff = 1
  endif
  !
  ! XLow
  axis(1)%doline = .true.
  axis(1)%angle = 0.d0
  axis(1)%loga = axis_xlog
  axis(1)%label%format = xsix
  if (xsix.eq.format_decimal) then
    axis(1)%label%brief = axis_xdecim_brief
  else
    axis(1)%label%brief = axis_xsexag_brief
    axis(1)%label%format_nd = ndecimx
  endif
  if (c1.eq.'N') then
    axis(1)%dolabel = .false.
    axis(1)%label%justif = 1
  elseif (c1.eq.'O') then
    axis(1)%dolabel = .true.
    axis(1)%label%justif = just(1+ioff)
  else
    axis(1)%dolabel = .true.
    axis(1)%label%justif = 1
  endif
  axis(1)%tdirect = jclock(1+ioff)
  axis(1)%label%offset = loffset
  axis(1)%label%expo = axis_xexpo
  !
  ! YRight
  axis(2)%doline = .true.
  axis(2)%dolabel = .false.
  axis(2)%angle = pi/2.d0
  axis(2)%loga = axis_ylog
  axis(2)%label%format = ysix
  if (ysix.eq.format_decimal) then
    axis(2)%label%brief = axis_ydecim_brief
  else
    axis(2)%label%brief = axis_ysexag_brief
    axis(2)%label%format_nd = ndecimy
  endif
  axis(2)%tdirect = jclock(7+ioff)
  axis(2)%label%justif = 0
  axis(2)%label%offset = loffset
  axis(2)%label%expo = axis_yexpo
  !
  ! YLeft
  axis(3)%doline = .true.
  axis(3)%angle = pi/2.d0
  axis(3)%loga = axis_ylog
  axis(3)%label%format = ysix
  if (ysix.eq.format_decimal) then
    axis(3)%label%brief = axis_ydecim_brief
  else
    axis(3)%label%brief = axis_ysexag_brief
    axis(3)%label%format_nd = ndecimy
  endif
  if (c2.eq.'N') then
    axis(3)%dolabel = .false.
  elseif (c2.eq.'P') then
    axis(3)%dolabel = .true.
    axis(3)%label%justif = 1
  else
    axis(3)%dolabel = .true.
    axis(3)%label%justif = just(5+ioff)
  endif
  axis(3)%tdirect = jclock(5+ioff)
  axis(3)%label%offset = loffset
  axis(3)%label%expo = axis_yexpo
  !
  ! XUp
  axis(4)%doline = .true.
  axis(4)%dolabel = .false.
  axis(4)%angle = 0.d0
  axis(4)%loga = axis_xlog
  axis(4)%label%format = xsix
  if (xsix.eq.format_decimal) then
    axis(4)%label%brief = axis_xdecim_brief
  else
    axis(4)%label%brief = axis_xsexag_brief
    axis(4)%label%format_nd = ndecimx
  endif
  axis(4)%tdirect = jclock(3+ioff)
  axis(4)%label%justif = 1
  axis(4)%label%offset = loffset
  axis(4)%label%expo = axis_xexpo
  !
  ! /BRIEF|NOBRIEF
  call greg_box_brief(line,axis,error)
  if (error)  return
  !
  ! Do the graphic work. Open the box segment
  call gtsegm('BOX',error)
  if (penupd) call setpen(cpen)
  open=cpen
  call setdas(1)
  !
  ! Draw the axis first WITHOUT labels, whatever the labelling
  ! status. They will be drawn after if needed.
  dolabel = axis(1)%dolabel
  axis(1)%dolabel = .false.
  call plot_axis(ax1,ax2,smallx,bigx,gx1,gy1,gx2-gx1,axis(1),error)
  axis(1)%dolabel = dolabel
  call plot_axis(ax1,ax2,smallx,bigx,gx1,gy2,gx2-gx1,axis(4),error)
  !
  call plot_axis(ay1,ay2,smally,bigy,gx2,gy1,gy2-gy1,axis(2),error)
  dolabel = axis(3)%dolabel
  axis(3)%dolabel = .false.
  call plot_axis(ay1,ay2,smally,bigy,gx1,gy1,gy2-gy1,axis(3),error)
  axis(3)%dolabel = dolabel
  !
  ! Label is a different segment only when /LABEL pen is explicitely
  ! used. Thus, for regular uses, a CLEAR SEGMENT will kill the complete
  ! box, which is the normal use.
  if (c1.ne.'N' .or. c2.ne.'N') then
    if (fancypen) then
      ! Close the BOX segment
      call gtsegm_close(error)
      if (error) return
      !
      call gr_segm('LABEL',error)
      call setpen(lpen)
      call setdas(1)
    endif
    if (axis(3)%dolabel) then
      axis(3)%doline = .false.
      call plot_axis(ay1,ay2,smally,bigy,gx1,gy1,gy2-gy1,axis(3),error)
    endif
    if (axis(1)%dolabel) then
      axis(1)%doline = .false.
      call plot_axis(ax1,ax2,smallx,bigx,gx1,gy1,gx2-gx1,axis(1),error)
    endif
    if (fancypen) then
      cpen = open
      penupd = .true.
    endif
  endif
  !
  ! And close/draw the segment
  call gtsegm_close(error)
  if (error) return
  !
  return
  !
99 call greg_message(seve%e,rname,'Invalid arguments. Should be P/O/N '//  &
     'O/P/N I/O/N')
  error = .true.
end subroutine greg_box
!
subroutine greg_box_brief(line,axis,error)
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_box_brief
  use greg_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   G\BOX /BRIEF|NOBRIEF [X|Y]
  ! Parse the command line options /BRIEF and /NOBRIEF. They are
  ! compatible as long as the same axis is not used twice, like e.g.
  ! in /BRIEF X /NOBRIEF Y
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line     ! Command line
  type(axis_t),     intent(inout) :: axis(4)  ! Axes parameters
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='BOX'
  logical :: briefx,briefy,nobriefx,nobriefy,donex,doney
  character(len=12) :: argum
  integer(kind=4) :: nc
  !
  donex = .false.
  doney = .false.
  !
  ! /BRIEF [X|Y]
  if (sic_present(5,0)) then
    if (sic_present(5,1)) then
      briefx = .false.
      briefy = .false.
      call sic_ke(line,5,1,argum,nc,.true.,error)
      if (error)  return
      if (argum.eq.'X')  then
        briefx = .true.
      elseif (argum.eq.'Y')  then
        briefy = .true.
      elseif (argum.ne.'') then
        call greg_message(seve%e,rname,  &
          '/BRIEF argument must be X, Y, or nothing')
        error = .true.
        return
      endif
    else
      briefx = .true.
      briefy = .true.
    endif
    !
    if (briefx) then
      donex = .true.
      axis(1)%label%brief = .true.  ! XLow
      axis(4)%label%brief = .true.  ! XUp
    endif
    if (briefy) then
      doney = .true.
      axis(2)%label%brief = .true.  ! YLeft
      axis(3)%label%brief = .true.  ! YRight
    endif
  endif
  !
  ! /NOBRIEF [X|Y]
  if (sic_present(6,0)) then
    if (sic_present(6,1)) then
      nobriefx = .false.
      nobriefy = .false.
      call sic_ke(line,6,1,argum,nc,.true.,error)
      if (error)  return
      if (argum.eq.'X')  then
        nobriefx = .true.
      elseif (argum.eq.'Y')  then
        nobriefy = .true.
      elseif (argum.ne.'') then
        call greg_message(seve%e,rname,  &
          '/NOBRIEF argument must be X, Y, or nothing')
        error = .true.
        return
      endif
    else
      nobriefx = .true.
      nobriefy = .true.
    endif
    !
    if (nobriefx .and. donex) then
      call greg_message(seve%e,rname,'X used twice with /BRIEF and /NOBRIEF')
      error = .true.
      return
    endif
    if (nobriefy .and. doney) then
      call greg_message(seve%e,rname,'Y used twice with /BRIEF and /NOBRIEF')
      error = .true.
      return
    endif
    !
    if (nobriefx) then
      axis(1)%label%brief = .false.  ! XLow
      axis(4)%label%brief = .false.  ! XUp
    endif
    if (nobriefy) then
      axis(2)%label%brief = .false.  ! YLeft
      axis(3)%label%brief = .false.  ! YRight
    endif
  endif
  !
end subroutine greg_box_brief
