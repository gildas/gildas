subroutine setdas(n)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>setdas
  use greg_pen
  use greg_kernel
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routines
  !	This is a set of routines and entry points used to communicate
  !	with GREG internal commons without addressing the common
  !	themselves.
  !
  !	SETDAS	Calls GTEDIT with the dashed pattern N and other pen
  !		attributes of the current PENCIL. The modification is
  !		only valid until the next call to SETPEN. Sets PENUPD.
  !		HINT! SETDAS is inactive if the dashed pattern N has
  !		already been selected by a previous call to SETPEN or
  !		SETDAS.
  !	SETWEI	Idem for the weight attribute
  !	SETCOL	Idem for the colour attribute
  !
  !	INQDAS	Returns the current dashed pattern in use
  !		(Current pen, possibly altered by SETDAS)
  !	INQWEI	Idem for the weight attribute
  !	INQCOL	Idem for the colour attribute
  !
  !	SETPEN	Calls GTEDIT with the attributes of the current PENCIL.
  !		Resets any alterations by SETDAS, SETWEI, SETCOL.
  !		Resets the PENUPD flag which is set by PENCIL and by
  !		calls to SETDAS, SETWEI, SETCOL and SETPHYSICAL.
  !		To open a new segment with pen N :
  !			CALL GTSEGM('INQUIRE',ERROR)
  !			CALL SETPEN (N)
  !		To open a new segment with the current pen IPEN :
  !			CALL GTSEGM('INQUIRE',ERROR)
  !			IF (PENUPD) CALL SETPEN (IPEN)
  !---------------------------------------------------------------------
  integer :: n                      !
  ! Local
  integer :: ioldpt
  real*4 :: arg, arg2, sizex, sizey, oldwei, newwei, curwei
  real*8 :: darg1, darg2
  integer :: ns,is
  real :: size
  logical :: error
  !
  ! Dashed pattern
  ioldpt = cdashe
  cdashe = max(mindashed,min(n,maxdashed))
  if (cdashe.ne.ioldpt) then
    penupd = .true.
    goto 1000
  endif
  return
  !
entry inqdas(n)
  n = cdashe
  return
  !
entry setwei(newwei)  ! Thickening factor
  ! Beware: NEWWEI and CWEIGH must be the actual line width (cm),
  ! not the Greg weight number
  oldwei = cweigh
  cweigh = newwei
  if (cweigh.ne.oldwei) then
    penupd = .true.
    goto 1000
  endif
  return
  !
entry inqwei(curwei)
  curwei = cweigh
  return
  !
  ! Colour
entry setcol(n)
  ! Beware: N and CCOLOU must be GTV internal id for the
  ! color, not the Greg color number
  ioldpt = ccolou
  ccolou = n
  if (ccolou.ne.ioldpt) then
    penupd = .true.
    goto 1000
  endif
  return
  !
entry inqcol(n)
  n = ccolou
  return
  !
  ! Set Pencil
entry setpen(n)
  cpen = n
  cdashe = ldashe(cpen)
  cweigh = lweigh(cpen)
  ccolou = lcolou(cpen)
  penupd = .false.
1000 call gtedit(cdashe,cweigh,ccolou,0,.true.)
  return
  !
  ! Segment depth
entry setdepth(n)
  ! Note: no global for current depth: specific call to gtedit
  call gtedit(cdashe,cweigh,ccolou,n,.true.)
  return
  !
entry inqpen(n)
  n = cpen
  return
  !
  ! Angle
entry setang(arg,arg2)
  tangle = arg
  sangle = arg2
  return
entry inqang(arg,arg2)
  arg = tangle
  arg2= sangle
  return
  !
  ! Expand
entry setexp(arg)
  expand = arg
  return
entry inqexp(arg)
  arg = expand
  return
  !
  ! Symbol Size
entry setsym(arg)
  csymb = arg
  return
entry inqsym(arg)
  arg = csymb
  return
  !
  ! Symbol style
entry setsty(n)
  istyle = max(0,min(n,4))
  return
entry inqsty(n)
  n = istyle
  return
  !
  ! Symbol type
entry setnsides(n)
  nsides = min(max(0,n),10000)
  return
entry inqnsides(n)
  n = nsides
  return
  !
  ! Physical limits
entry setphysical(sizex,sizey)
  lx1 = 0.0
  ly1 = 0.0
  lx2 = min(sizex,160.)
  ly2 = min(sizey,160.)
  error = .false.
  call gt_setphysical(lx2,ly2,error)
  if (error)  continue
  gxp = -1e10                  ! Reset pen position to
  gyp = -1e10                  ! undefined...
  penupd = .true.              ! As GTINIT resets the virtual pen
  xcurse = lx2*0.5
  ycurse = ly2*0.5
  return
  !
  ! Blanking
entry setbla(darg1,darg2)
  cblank = darg1
  eblank = darg2
  return
  !
  ! Public entry points
entry gr_get_physical(sizex,sizey)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  sizex = lx2
  sizey = ly2
  return
  !
entry gr_set_marker(ns,is,size)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  nsides = min(max(0,ns),10000)
  istyle = max(0,min(is,3))
  csymb = size
  return
  !
entry gr_get_marker(ns,is,size)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  ns = nsides
  is = istyle
  size = csymb
end subroutine setdas
!
subroutine setsys(system,equinox)
  use gbl_constant
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  !  Set the internal coordinate system (and equinox if relevant)
  !  Do not use out of Greg. Use gr8_system instead.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)           :: system
  real(kind=4),    intent(in), optional :: equinox
  !
  select case (system)
  case (type_eq,type_ga,type_ic)
    i_system = system
  case default
    i_system = type_un
  end select
  !
  if (i_system.eq.type_eq) then
    if (present(equinox)) then
      i_equinox = equinox
    else
      i_equinox = i_equinox_def
    endif
  endif
  !
end subroutine setsys
