subroutine flimit
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>flimit
  use greg_image
  use greg_rg
  !---------------------------------------------------------------------
  ! @ private
  ! GREG	Internal routine
  !
  !	This subroutine computes the limits of the Regular Grid array.
  !	It tries to handle "Astronomical Maps" as best as possible (!).
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='RGDATA'
  real*8 :: tmp,xmin,xmax,ymin,ymax
  character(len=80) :: chain
  !
  if (rg%xval.ne.0.d0 .and. xunit.ne.'UNKNOWN') then
    if (dabs(rg%xinc/rg%xval).lt.1.d-3) then
      tmp = rg%xval/15.d0
      if (xunit.eq.'(DEGREES)') tmp = 3600.d0*tmp
      if (xunit.eq.'(ARC MIN.)') tmp = 60.d0*tmp
      call deg2sexa(tmp/3600.d0,360,coffx,2,left=.true.)
      call greg_message(seve%i,rname,'Offset in X '//coffx)
      rg%xval = 0.d0
    endif
  endif
  !
  if (rg%yval.ne.0.d0 .and. yunit.ne.'UNKNOWN') then
    if (dabs(rg%yinc/rg%yval).lt.1.d-3) then
      tmp = rg%yval
      if (yunit.eq.'(DEGREES)') tmp = 3600.*tmp
      if (yunit.eq.'(ARC MIN.)') tmp = 60.*tmp
      call deg2sexa(tmp/3600.d0,360,coffy,1,left=.true.)
      call greg_message(seve%i,rname,'Offset in Y '//coffy)
      rg%yval = 0.d0
    endif
  endif
  !
  xmin = (1.d0-rg%xref)*rg%xinc + rg%xval
  xmax = (dble(rg%nx)-rg%xref)*rg%xinc + rg%xval
  ymin = (1.d0-rg%yref)*rg%yinc + rg%yval
  ymax = (dble(rg%ny)-rg%yref)*rg%yinc + rg%yval
  !
  ! Signal the limits to the user
  write(chain,'(A,4(1X,1PG12.5))') 'Limits ',xmin,xmax,ymin,ymax
  call greg_message(seve%i,rname,chain)
end subroutine flimit
