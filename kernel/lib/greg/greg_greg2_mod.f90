module greg_image
  !--------------------------------------------------------------------
  ! GILDAS and FITS images support in GREG
  !--------------------------------------------------------------------
  !
  ! FITS image: RGDATA input
  real(8) :: fxref, fyref               !
  real(8) :: fxval, fyval               !
  real(8) :: fxinc, fyinc               !
  real(8) :: bscal                      !
  real(8) :: bzero                      !
  real(8) :: datamin,datamax            !
  integer(4) :: ninfx, nsupx            !
  integer(4) :: ninfy, nsupy            !
  integer(4) :: nbit                    !
  integer(4) :: fnx, fny                !
  character(len=20) :: coffx, coffy     !
  character(len=80) :: xunit, yunit     !
  character(len=1) :: comment='!'       !
  !
end module greg_image
!
module greg_rg
  use gildas_def
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  !  Support module for the RG data buffer
  !---------------------------------------------------------------------
  !
  ! THIS TYPE SHOULD GO IN MODULE GREG_TYPES!!!
  ! Type describing the Greg Regular Grid
  type :: greg_rg_t
    real(kind=8)          :: xref,xval,xinc            ! X axis description (user coord.)
    real(kind=8)          :: yref,yval,yinc            ! Y axis description (user coord.)
    integer(kind=4)       :: nx=0                      ! Number of X pixels
    integer(kind=4)       :: ny=0                      ! Number of Y pixels
    real(kind=4), pointer :: data(:,:)=>null()         ! RG data buffer
    integer(kind=4)       :: status=code_pointer_null  ! Pointer allocation status
    logical               :: minmax=.false.            ! Extremas computed ?
    real(kind=4)          :: zmin,zmax                 ! Mini and maximum map value
    real(kind=4)          :: zxmin,zymin               ! Position of mini map value
    real(kind=4)          :: zxmax,zymax               ! Position of maxi map value
    type(gildas)          :: imag                      ! Associated image, if relevant
  end type greg_rg_t
  !
  type(greg_rg_t), save :: rg
end module greg_rg
!
module greg_contours
  use gildas_def
  !--------------------------------------------------------------------
  ! Contours support in GREG
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !--------------------------------------------------------------------
  !
  ! RG array description and extremas, and contour levels description
  integer, parameter :: maxl=40     !
  integer, save :: ncl=0            ! Current number of contours
  !
  real(4), save :: cl(maxl)         ! Values of contours
  real(4) :: qlev                   ! Multiplicative factor for contours
  !
  integer, parameter :: maxu=512    ! Maximum number of pixels for which
                                    ! we attempt contour-filling
  integer, parameter :: chunksize=65536 !
  !
  real(4) :: xu(maxu),yu(maxu)      ! Buffers for contour coordinates
  real(4) :: gzmin,gzmax            ! Max-min of subset of map (for Grey-fill).
  real(4) :: blankfk                ! Blanking value for keeping
  integer :: icolo                  ! Color index used for filling
  integer :: ncolo                  ! Color index used of contours
  integer :: nstep                  ! Trying parameter for filling
  integer :: ilev                   ! Contour level counter
  logical :: lgrey                  ! Grey filling ?
  logical :: link                   ! Connect buffers ?
  logical :: lout                   ! Keeping copy on formatted file ?
  ! Used by Contour Filling:
  logical :: highwater
  logical :: chunkpatch
  integer(kind=4), allocatable :: ixu(:)  ! Buffer for contour X coordinates
  integer(kind=4), allocatable :: iyu(:)  ! Buffer for contour Y coordinates
  integer(kind=address_length) :: ipxu,ipyu
  logical :: noclip_gris
  !
  ! Positive/negative contours
  integer :: ipen_pos, ipen_neg     !
  integer(kind=size_length) :: ldd  !
  integer :: mxu                    !
  logical :: quietmode              !
  !
  ! Physical box for the drawing output
  real :: xbox1, ybox1, xbox2, ybox2         ! In REAL centimeters
  integer :: xxbox1, xxbox2, yybox1, yybox2  ! In INTEGER microns
  !
end module greg_contours
!
module greg_poly
  use greg_types
  !--------------------------------------------------------------------
  ! Variables for polygon support in GREG
  ! For portability, SAVE Fortran variables (or their common) which are
  ! target of SIC variables.
  !--------------------------------------------------------------------
  !
  type(polygon_t), save :: gpoly
end module greg_poly
