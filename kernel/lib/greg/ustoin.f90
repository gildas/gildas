function gr_clip(clip)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  ! Toggle Clipping
  !---------------------------------------------------------------------
  logical :: gr_clip                !
  logical :: clip                   !
  !
  gr_clip = .not.noclip
  noclip = .not.clip
end function gr_clip
!
subroutine gr4_phys_user (xi,yi,xu,yu,nxy)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ public
  !  Convert Physical units to User units
  !---------------------------------------------------------------------
  real(4), intent(in) :: xi(*)      ! X plot coordinates
  real(4), intent(in) :: yi(*)      ! Y plot coordinates
  real(4), intent(out) :: xu(*)     ! X User coordinates
  real(4), intent(out) :: yu(*)     ! Y User coordinates
  integer(4), intent(in) :: nxy     ! Number of points
  ! Local
  integer(4) :: i
  !
  if (axis_xlog) then
    do i=1,nxy
      xu(i) = exp((xi(i)-gx1)/gux + lux)
    enddo
  else
    do i=1,nxy
      xu(i) = (xi(i)-gx1)/gux + gux1
    enddo
  endif
  if (axis_ylog) then
    do i=1,nxy
      yu(i) = exp((yi(i)-gy1)/guy + luy)
    enddo
  else
    do i=1,nxy
      yu(i) = (yi(i)-gy1)/guy + guy1
    enddo
  endif
end subroutine gr4_phys_user
!
subroutine gr8_phys_user (xi,yi,xu,yu,nxy)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ no-interface (because of rank mismatch)
  !  Convert Physical units to User units
  !---------------------------------------------------------------------
  real(4), intent(in) :: xi(*)      ! X plot coordinates
  real(4), intent(in) :: yi(*)      ! Y plot coordinates
  real(8), intent(out) :: xu(*)     ! X User coordinates
  real(8), intent(out) :: yu(*)     ! Y User coordinates
  integer(4), intent(in) :: nxy     ! Number of points
  ! Local
  integer(4) :: i
  !
  if (axis_xlog) then
    do i=1,nxy
      xu(i) = exp((xi(i)-gx1)/gux + lux)
    enddo
  else
    do i=1,nxy
      xu(i) = (xi(i)-gx1)/gux + gux1
    enddo
  endif
  if (axis_ylog) then
    do i=1,nxy
      yu(i) = exp((yi(i)-gy1)/guy + luy)
    enddo
  else
    do i=1,nxy
      yu(i) = (yi(i)-gy1)/guy + guy1
    enddo
  endif
end subroutine gr8_phys_user
!
subroutine us8_to_int(xu,yu,xi,yi,nxy)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument rank mismatch)
  ! GREG	Internal routine
  !	Converts XU array in user coordinates into XI array in plot
  !	coordinates
  ! No subroutine referenced
  !---------------------------------------------------------------------
  real(8), intent(in) :: xu(*)       ! X User coordinates
  real(8), intent(in) :: yu(*)       ! Y User coordinates
  real(4), intent(out) :: xi(*)      ! X Plot coordinates
  real(4), intent(out) :: yi(*)      ! Y plot coordinates
  integer(4), intent(in) :: nxy      ! Number of points
  ! Local
  integer(4) :: i
  !
entry gr8_user_phys (xu,yu,xi,yi,nxy)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  if (axis_xlog) then
    do i=1,nxy
      if (xu(i).gt.0) then
        xi(i) = gx1 + gux * (log(xu(i))-lux)
      else if (gux.gt.0) then
        xi(i) = gx1
     else
        xi(i) = gx2
      endif
    enddo
  else
    do i=1,nxy
      xi(i) = gx1 + gux * (xu(i)-gux1)
    enddo
  endif
  !
  if (axis_ylog) then
    do i=1,nxy
      if (yu(i).gt.0) then
        yi(i) = gy1 + guy * (log(yu(i))-luy)
      else if (guy.gt.0) then
        yi(i) = gy1
     else
        yi(i) = gy2
      endif
    enddo
  else
    do i=1,nxy
      yi(i) = gy1 + guy * (yu(i)-guy1)
    enddo
  endif
end subroutine us8_to_int
!
subroutine us4_to_int(xu,yu,xi,yi,nxy)
  use greg_kernel
  !---------------------------------------------------------------------
  ! @ no-interface
  ! GREG	Internal routine
  !	Converts XU array in user coordinates into XI array in plot
  !	coordinates
  !---------------------------------------------------------------------
  real(4), intent(in) :: xu(*)       ! X User coordinates
  real(4), intent(in) :: yu(*)       ! Y User coordinates
  real(4), intent(out) :: xi(*)      ! X Plot coordinates
  real(4), intent(out) :: yi(*)      ! Y Plot coordinates
  integer(4), intent(in) :: nxy      ! Number of points
  ! Local
  integer(4) :: i
  !
entry gr4_user_phys (xu,yu,xi,yi,nxy)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  if (axis_xlog) then
    do i=1,nxy
      if (xu(i).gt.0) then
        xi(i) = gx1 + gux * (log(xu(i))-lux)
      else if (gux.gt.0) then
        xi(i) = gx1
     else
        xi(i) = gx2
      endif
    enddo
  else
    do i=1,nxy
      xi(i) = gx1 + gux * (xu(i)-gux1)
    enddo
  endif
  !
  if (axis_ylog) then
    do i=1,nxy
      if (yu(i).gt.0) then
        yi(i) = gy1 + guy * (log(yu(i))-luy)
      else if (guy.gt.0) then
        yi(i) = gy1
     else
        yi(i) = gy2
      endif
    enddo
  else
    do i=1,nxy
      yi(i) = gy1 + guy * (yu(i)-guy1)
    enddo
  endif
end subroutine us4_to_int
