subroutine exec_greg(buffer)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>exec_greg
  use gildas_def
  use greg_kernel
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public (but should not)
  !---------------------------------------------------------------------
  character(len=*) :: buffer        !
  ! Local
  character(len=*), parameter :: rname='GREG'
  character(len=2048) :: line
  character(len=12) :: lang
  character(len=12) :: comm
  logical :: error
  integer :: icode,ocode
  !
  if (library) goto 99
  line = buffer
  icode = -1
  !
  error = .false.
  do while (.true.)
    call sic_run (line,lang,comm,error,icode,ocode)
    if (ocode.eq.-1) return    ! From subroutine mode
    if (ocode.eq.1) return     ! Final return
    select case (lang)
    case('GREG1')
      call run_greg1(line,comm,error)
    case('GREG2')
      call run_greg2(line,comm,error)
    case('GREG3')
      call run_greg3(line,comm,error)
    case('GTVL')
      call run_gtvl(line,comm,error)
      if (comm.eq.'CLEAR') call greset
    case('VECTOR')
      call run_vector(line,comm,error)
    case default
      call greg_message(seve%e,rname,'Unknown Language '//lang)
      error = .true.
    end select
    error = error .or. gterrtst()
    icode = 0
  enddo
  !
99 call greg_message(seve%f,rname,'Call to *_GREG with GreG loaded in '//  &
   'Library Mode')
  call sysexi (fatale)
end subroutine exec_greg
!
recursive subroutine run_greg1(line,comm,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>run_greg1
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public-mandatory (public but should not) (mandatory because symbol
  ! is used elsewhere)
  !	Dispatch the command line
  ! Arguments :
  !	LINE	C*(*)	Command line			Input/Output
  !	COMM	C*(*)	Command				Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='GREG1'
  integer, save :: icall=0
  !
  if (icall.ne.0)  &
    call greg_message(seve%d,rname,'Reentrant call to RUN_GREG1 '//comm)
  !
  icall = icall+1
  !
  call greg_message(seve%c,rname,line)
  !
  error = .false.
  select case (comm)
  case('AXIS')
    call gr_segm(comm,error)
    call greg_axis(line,error)  ! Calls SETDAS
    call gtsegm_close(error)
  case('BOX')
    call greg_box(line,error)   ! Calls SETDAS
  case('CONNECT')
    call gr_segm(comm,error)
    call gconne(line,error)
    call gtsegm_close(error)
  case('COLUMN')
    call column(line,error)
  case('CORNERS')
    call greg_corners(line,error)
  case('CURVE')
    call gr_segm(comm,error)
    call curve(line,error)
    call gtsegm_close(error)
  case('DRAW')
    ! Calls GR_SEGM(COMM,ERROR), and SETPEN [SETDAS]
    call curse(line,error)
  case('ERRORBAR')
    call bars(line,error)
  case('HISTOGRAM')
    call ghisto(line,error)
  case('LABEL')
    call gr_segm(comm,error)
    call labels(line,error)    ! Calls SETDAS
    call gtsegm_close(error)
  case('LIMITS')
    call limits(line,error)
  case('PENCIL')
    call pencil(line,error)
  case('POINTS')
    call gr_segm(comm,error)
    call greg_point(line,error)    ! Calls SETDAS
    call gtsegm_close(error)
  case('SHOW')
    call greg_show(line,error)
  case('SET')
    call setup(line,error)
  case('RULE')
    call gr_segm(comm,error)
    call greg_rule(line,error)
    call gtsegm_close(error)
  case('TICKSPACE')
    call greg_tickspace(line,error)
  case('VALUES')
    call gr_segm(comm,error)
    call gvaleur(line,error)
    call gtsegm_close(error)
  case('LOOK')
    call glook(line,error)
  case default
    call greg_message(seve%w,rname,'No code to execute for '//comm)
    error = .true.
  end select
  !
  icall = icall-1
end subroutine run_greg1
!
subroutine run_greg2(line,comm,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>run_greg2
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public-mandatory (public but should not) (mandatory because symbol
  ! is used elsewhere)
  ! This entry is for 2-D analysis
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='GREG2'
  integer, save :: icall=0
  !
  if (icall.ne.0)  &
    call greg_message(seve%d,rname,'Reentrant call to RUN_GREG2 '//comm)
  !
  icall = icall+1
  !
  call greg_message(seve%c,rname,line)
  !
  error = .false.
  select case (comm)
  case('ARROW')
    call greg_arrow(line,error)
  case('RGMAP')
    call conmap(line,error)
  case('LEVELS')
    call level(line,error)
  case('EXTREMA')
    call greg_extrema(line,error)
! case('FITS')
!   call fits(line,error)
  case('PLOT')
    call greg_plot(line,error)
  case('RGDATA')
    call greg_rgdata(line,error)
  case('PERSPECTIVE')
    call threed(line,error)
  case('RANDOM_MAP')
    call gr_segm(comm,error)
    call gridder(line,error)
    call gtsegm_close(error)
  case('WRITE')
    call greg_write(line,error)
  case('ELLIPSE')
    call ellipse(line,error)
  case('MEAN')
    call meanva(line,error)
  case('MASK')
    call maskon(line,error)
  case('POLYGON')
    call setgon(line,error)
  case('STRIP')
    call rstrip(line,error)
  case('CONVERT')
    call greg_convert(line,error)
  case('PROJECTION')
    call defpro(line,error)
  case('GRID')
    call greg_grid(line,error)
  case('RESAMPLE')
    call sampler(line,error)
  case('WEDGE')
    call grwedge(line,error)
  case default
    call greg_message(seve%w,rname,'No code to execute for '//comm)
    error = .true.
  end select
  !
  icall = icall-1
end subroutine run_greg2
