subroutine defpro(line,error)
  use phys_const
  use gbl_message
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>defpro
  use greg_wcs
  !---------------------------------------------------------------------
  ! @ private
  ! GREG Support routine for command
  !    PROJECTION [Ra Dec] [Angle] [/TYPE Type]
  ! Defines the projection type and center
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='PROJECTION'
  character(len=13) :: argum,keywor,projnam_array(0:mproj)
  character(len=80) :: chain
  character(len=24) :: prochain
  real(kind=8) :: ra,dec,ang
  integer(kind=4) :: nkey,type,nc
  character(len=6), parameter :: angu(4) = (/ 'SECOND','MINUTE','DEGREE','RADIAN' /)
  !
  ! Decode projection type
  type = gproj%type
  if (sic_present(1,0)) then
    if (.not.sic_present(1,1)) then
      type = p_none
    else
      call sic_ke (line,1,1,argum,nc,.true.,error)
      if (error) return
      !
      call projnam_list(projnam_array)
      call sic_ambigs(rname,argum,keywor,nkey,projnam_array,mproj+1,error)
      if (error) return
      type = nkey-1
    endif
  endif
  !
  ! Decode center
  if (sic_present(0,1)) then
    if (type.ne.p_aitoff) then
      call sic_ke (line,0,2,prochain,nc,.true.,error)
      call sic_sexa(prochain,nc,dec,error)
      if (error) return
      dec = pi*dec/180.0d0
    else
      call greg_message(seve%w,rname,'Declination ignored in AITOFF')
    endif
    call sic_ke (line,0,1,prochain,nc,.true.,error)
    call sic_sexa(prochain,nc,ra,error)
    if (error) return
    if (i_system.eq.type_eq .or. i_system.eq.type_ic) then
      ra = pi*ra/12.0d0
    else
      ra = pi*ra/180.0d0
    endif
    ang = 0.d0
    if (sic_present(0,3)) then
      if (type.eq.p_aitoff) then
        call greg_message(seve%w,rname,'Angle ignored in AITOFF')
      elseif (type.eq.p_radio) then
        call greg_message(seve%w,rname,'Angle ignored in RADIO')
      else
        call sic_ke(line,0,3,prochain,nc,.true.,error)
        call sic_sexa(prochain,nc,ang,error)
        if (error) return
        ang = pi*ang/180.0d0
      endif
    endif
    !
    ! Set it only if specified
    call gwcs_projec(ra,dec,ang,type,gproj,error)
    if (error)  return
    call setrem
    !
  else
    gproj%type = type  ! Simpler than calling gwcs_projec
  endif
  !
  if (type.eq.p_none) then
    call greg_message(seve%i,rname,'No projection defined')
    if (type.eq.p_none .and. u_angle.ne.u_radian) then
      chain = 'Angular unit is '//angu(u_angle)
      call greg_message(seve%w,rname,chain)
    endif
  else
    write (chain,110) projnam(gproj%type),180.d0*gproj%angle/pi
    call greg_message(seve%r,rname,chain)
    call sexfor(0.d0,0.d0)
  endif
  !
110 format('Projection ',a,' at angle ',f12.6,' from center')
end subroutine defpro
