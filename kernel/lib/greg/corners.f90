subroutine greg_corners(line,error)
  use greg_dependencies_interfaces
  use greg_interfaces, except_this=>greg_corners
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   CORNERS
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  !
  call gr_segm('CORNERS',error)
  if (error)  return
  !
  call gtl_corner(error)
  ! if (error)  continue
  !
  call gtsegm_close(error)
  if (error)  return
  !
end subroutine greg_corners
