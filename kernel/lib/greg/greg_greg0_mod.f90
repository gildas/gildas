!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_types
  !---------------------------------------------------------------------
  ! Greg types
  !---------------------------------------------------------------------
  !
  integer, parameter :: mgon=1000     ! Maximum number of summits
  type :: polygon_t
    real(8) :: xgon(mgon)            ! X Coordinates of summits
    real(8) :: ygon(mgon)            ! Y Coordinates of summits
    real(8) :: dxgon(mgon)           ! XGON(I+1)-XGON(I)
    real(8) :: dygon(mgon)           ! YGON(I+1)-YGON(I)
    real(8) :: xout                  ! X coordinate of a point outside
    real(8) :: xgon1                 ! Lower X value
    real(8) :: xgon2                 ! Upper X value
    real(8) :: ygon1                 ! Lower Y value
    real(8) :: ygon2                 ! Upper Y value
    integer :: ngon                  ! Number of summits
  end type polygon_t
  !
  ! Type describing the labeling rules of an axis (axis_t)
  type :: axis_label_t
    integer(kind=4) :: justif     ! Label justification (left, center, right). Contains
                                  ! implicitly the label orientation (1 => Parallel to axis)
    real(kind=4)    :: offset     ! Label offset (?)
    integer(kind=4) :: format     ! Decimal (0) or sexagesimal (>0) labels?
    integer(kind=4) :: format_nd  ! Number of decimals if sexagesimal label
    logical         :: brief      ! All but one label abbreviated?
    logical         :: expo       ! Prefer exponential notation (when possible)?
  end type axis_label_t
  !
  ! Type describing an "axis" in GreG, in a large sense. Grid cans also
  ! be described with this type, with grid lines as special ticks.
  type :: axis_t
    ! General rules
    logical      :: doline       ! Draw the axis line or not
    logical      :: dolabel      ! Draw the labels or not
    real(kind=8) :: angle        ! Axis orientation (radians)
    logical      :: loga         ! Is it a log axis?
    ! Ticks rules
    logical      :: tdirect      ! Tick direction: direct: up/right, indirect: down,left
    ! Label rules
    type(axis_label_t) :: label  !
    ! Grid rules
    real(kind=4) :: glength      ! Length of grid line
    logical      :: gmajor       ! Draw grid on major ticks only or minor ticks only?
    logical      :: ghoriz       ! Horizontal or vertical grid
  end type axis_t
  !
  ! Type describing a single tick in GreG, in a large sense.
  type :: tick_t
    logical      :: major    ! Is the tick major or minor?
    real(kind=4) :: x        ! Tick X reference position (in world coordinates)
    real(kind=4) :: y        ! Tick Y reference position (in world coordinates)
    real(kind=8) :: value    ! Associated value at tick position (in user coordinates)
    ! Label rules
    logical      :: lbrief   ! Abbreviate the label?
    real(kind=8) :: loffval  ! Abbreviation offset
  end type tick_t
  !
  type :: polygon_drawing_t
    logical         :: contoured  ! Should the polygon be contoured?
    integer(kind=4) :: cpen       ! Pen for contouring
    !
    logical         :: filled     ! Should the polygon be color-filled?
    integer(kind=4) :: fcolor     ! Color to be used
    !
    logical         :: hatched    ! Should the polygon be hatch-filled?
    integer(kind=4) :: hpen       ! Pen for hatching
    real(kind=4)    :: hangle     ! Line orientation (radians)
    real(kind=4)    :: hsepar     ! Line separation (physical units)
    real(kind=4)    :: hphase     ! Line start (0. to 1.)
  end type polygon_drawing_t
  !
end module greg_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_error
  !--------------------------------------------------------------------
  ! Error support
  !--------------------------------------------------------------------
  character(len=80) :: message  ! Error message
  logical :: errorg             !
end module greg_error
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module greg_kernel
  !--------------------------------------------------------------------
  ! GREG general variables
  ! For portability, SAVE Fortran variables (or their common) which are
  ! target of SIC variables.
  !--------------------------------------------------------------------
  real(8) :: gux1, gux2             ! Users X-limit
  real(8) :: guy1, guy2             ! Users Y-limit
  real(8) :: gux, guy               ! Conversion factor
  real(8) :: lux, luy               ! Logarithmique conversion offset
  real(8), save :: ucurse(2)        ! Cursor in user coordinates
  real(8) :: tangle                 ! Position angle for texts
  real(8) :: sangle                 ! Position angle for symbols
  real(8) :: cblank                 ! Blanking value
  real(8) :: eblank                 !
  !
  ! Default values
  real(4), parameter :: lxs_d=30.0  ! X physical size (cm)
  real(4), parameter :: lys_d=21.0  ! Y physical size (cm)
  real(4), parameter :: gx1_d=4.0   ! Left X coordinate of the box (cm)
  real(4), parameter :: gx2_d=28.0  ! Right X coordinate of the box (cm)
  real(4), parameter :: gy1_d=2.5   ! Lower Y coordinate of the box (cm)
  real(4), parameter :: gy2_d=19.5  ! Upper Y coordinate of the box (cm)
  !
  ! Current values
  real(4) :: lx1, lx2               ! Pen X physical limits
  real(4) :: ly1, ly2               ! Pen Y physical limits
  real(4) :: gx1, gx2               ! X coordinates of the box
  real(4) :: gy1, gy2               ! Y coordinates of the box
  real(4), save :: xp,yp            ! Position of the pen
  real(4) :: gxp,gyp                ! Position after clipping
  real(4) :: xcurse, ycurse         ! Position of the cursor
  real(4) :: expand=1.0             ! Expansion factor
  real(4) :: accurd                 ! Plot accuracy (cm)
  real(4) :: cheight, cwidth        ! Height and width of the characters
  real(4), save :: cdef=0.6         ! Character size
  real(4), save :: csymb=0.3        ! Marker size
  real(4), save :: ctick=0.4        ! Major tick size
  real(4), save :: cmtick           ! Minor tick size
  !
  integer :: i_font                 ! Pointeur des caracteres (0/1)
  integer :: icente                 ! Option de centrage
  integer, save :: nsides, istyle   ! Type de symbole
  integer :: iboxd                  ! Unites BOX (0) ou USER (1)
  integer :: h_mode                 ! Type de Hardcopy
  integer, save :: pixcurse(2)      !
  character(len=1), save :: cursor_code
  !
  logical :: noclip=.false.         ! Logique de clipping
  logical :: axis_xlog=.false.      ! X axis scale is log or linear?
  logical :: axis_ylog=.false.      ! Y axis scale is log or linear?
  !
  logical :: axis_xdecim_brief
  logical :: axis_ydecim_brief
  logical :: axis_xsexag_brief
  logical :: axis_ysexag_brief
  logical :: axis_xexpo
  logical :: axis_yexpo
  logical :: axis_zexpo
  integer(kind=4), parameter :: format_decimal=0
  integer(kind=4), parameter :: format_sexage_none=1
  integer(kind=4), parameter :: format_sexage_hour=2
  integer(kind=4), parameter :: format_sexage_degree=3
  integer(kind=4) :: xsixty,ysixty     !
  integer(kind=4) :: ndecimx, ndecimy  ! Idem pour X et Y en sexagesimal
  real(kind=4) :: label_xoff=2.0       ! Offset in characters for X Label
  real(kind=4) :: label_yoff=6.0       ! Offset in characters for Y Label
  !
  integer, parameter :: h_nonstandard=0
  integer, parameter :: h_landscape=1
  integer, parameter :: h_portrait=2
  !
  common /greg01/                         &
    gux1,gux2,guy1,guy2,gux,guy,lux,luy,  &  ! These 8 ones must be contiguous
    cblank,eblank,                        &  ! These 2 ones must be contiguous
    lx1,lx2,ly1,ly2,                      &  ! These 4 ones must be contiguous
    gx1,gx2,gy1,gy2,                      &  ! These 4 ones must be contiguous
    xcurse,ycurse                            ! These 2 ones must be contiguous
  save /greg01/  ! because it contains variables which are target of SIC variables
  !
  ! IO units
  integer(kind=4) :: jtmp=0     ! Logical Unit Number for Closed operation
  integer(kind=4) :: lung=0     ! Logical Unit Number for GTV terminal
  integer(kind=4) :: lunh=0     ! Logical Unit Number for GTV hardcopy
  logical         :: library    ! Is language loaded as library only?
end module greg_kernel
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine greg_kernel_init
  use greg_kernel
  !--------------------------------------------------------------------
  ! @ private
  ! Initialization routine for greg_kernel module.
  ! It must be called once and only once, otherwise an error will
  ! raise and program will exit.
  !--------------------------------------------------------------------
  !
  logical, save :: done=.false.
  ! call sic_c_assert(.not.done, 'E-GREG_KERNEL,  Multiple call of module initialization')
  if (done) return
  !
  gux1 = 0.
  gux2 = 1.
  guy1 = 0.
  guy2 = 1.
  lx1  = 0.
  lx2  = 30.
  ly1  = 0.
  ly2  = 21.
  gx1  = gx1_d
  gx2  = gx2_d
  gy1  = gy1_d
  gy2  = gy2_d
  !
  done = .true.
end subroutine greg_kernel_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine greg_mod_init
  !--------------------------------------------------------------------
  ! @ private
  ! Call to initialization routines for GREG modules.
  ! It must be called once and only once, otherwise an error will
  ! raise and program will exit.
  !--------------------------------------------------------------------
  call greg_kernel_init
end subroutine greg_mod_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
