function gag_erfinv(x0)
  use phys_const
  use gmath_interfaces, except_this=>gag_erfinv
  !-----------------------------------------------------------------------
  ! @ public
  ! This subroutine calculates the inverse error function.
  !-----------------------------------------------------------------------
  real(kind=8) :: gag_erfinv
  real(kind=8), intent(in) :: x0
  ! Local
  real(kind=8) :: x,x2,r,y
  integer(kind=4) :: sign_x
  ! Parameters
  real(kind=8), parameter :: erfinv_a3 = -0.140543331d0
  real(kind=8), parameter :: erfinv_a2 = 0.914624893d0
  real(kind=8), parameter :: erfinv_a1 = -1.645349621d0
  real(kind=8), parameter :: erfinv_a0 = 0.886226899d0
  real(kind=8), parameter :: erfinv_b4 = 0.012229801d0
  real(kind=8), parameter :: erfinv_b3 = -0.329097515d0
  real(kind=8), parameter :: erfinv_b2 = 1.442710462d0
  real(kind=8), parameter :: erfinv_b1 = -2.118377725d0
  real(kind=8), parameter :: erfinv_b0 = 1.d0
  real(kind=8), parameter :: erfinv_c3 = 1.641345311d0
  real(kind=8), parameter :: erfinv_c2 = 3.429567803d0
  real(kind=8), parameter :: erfinv_c1 = -1.62490649d0
  real(kind=8), parameter :: erfinv_c0 = -1.970840454d0
  real(kind=8), parameter :: erfinv_d2 = 1.637067800d0
  real(kind=8), parameter :: erfinv_d1 = 3.543889200d0
  real(kind=8), parameter :: erfinv_d0 = 1.d0
  !
  if (x0.le.-1.d0 .or. x0.ge.1.d0) then
    ! erfinv(+1)     = +Inf
    ! erfinv(-1)     = -Inf
    ! erfinv(others) = Nan
    gag_erfinv = 0.d0
    return
  endif
  !
  if (x0.eq.0.d0) then
    gag_erfinv = 0.d0
    return
  endif
  !
  if (x0.gt.0.d0) then
    sign_x = 1
    x = x0
  else
    sign_x = -1
    x = -x0;
  endif
  !
  if (x.le.0.7) then
    x2 = x * x
    r = x * (((erfinv_a3 * x2 + erfinv_a2) * x2 + erfinv_a1) * x2 + erfinv_a0)
    r = r / ((((erfinv_b4 * x2 + erfinv_b3) * x2 + erfinv_b2) * x2 +  &
                erfinv_b1) * x2 + erfinv_b0)
  else
    y = sqrt (-log ((1.d0-x) / 2.d0))
    r = (((erfinv_c3 * y + erfinv_c2) * y + erfinv_c1) * y + erfinv_c0)
    r = r / (((erfinv_d2 * y + erfinv_d1) * y + erfinv_d0))
  endif
  !
  r = r * sign_x
  x = x * sign_x
  !
  r = r - ((erf (r) - x) / (2 / sqrt (pi) * exp (-r * r)))
  r = r - ((erf (r) - x) / (2 / sqrt (pi) * exp (-r * r)))
  !
  gag_erfinv = r
  !
end function gag_erfinv
!
function gag_erfcinv(x)
  use gmath_interfaces, except_this=>gag_erfcinv
  !-----------------------------------------------------------------------
  ! @ public
  ! This subroutine calculates the inverse complementary error function.
  !-----------------------------------------------------------------------
  real(kind=8) :: gag_erfcinv
  real(kind=8), intent(in) :: x
  gag_erfcinv = gag_erfinv(1.d0-x)
end function gag_erfcinv
