function gag_bessel_in(n,x)
  use gmath_interfaces, except_this=>gag_bessel_in
  !-----------------------------------------------------------------------
  ! @ public
  ! This subroutine calculates the first kind modified Bessel function
  ! of integer order N, for any REAL X. We use here the classical
  ! recursion formula, when X > N. For X < N, the Miller's algorithm
  ! is used to avoid overflows.
  !
  !     REFERENCE:
  !     C.W.CLENSHAW, CHEBYSHEV SERIES FOR MATHEMATICAL FUNCTIONS,
  !     MATHEMATICAL TABLES, VOL.5, 1962.
  !-----------------------------------------------------------------------
  real(kind=8) :: gag_bessel_in
  integer(kind=4), intent(in) :: n
  real(kind=8),    intent(in) :: x
  ! Local
  integer(kind=4), parameter :: iacc=40
  real(kind=8), parameter :: bigno=1.d10, bigni=1.d-10
  integer(kind=4) :: m, j
  real(kind=8) :: tox,bim,bi,bip
  !
  if (n.eq.0) then
    gag_bessel_in = gag_bessel_i0(x)
    return
  endif
  if (n.eq.1) then
    gag_bessel_in = gag_bessel_i1(x)
    return
  endif
  if (x.eq.0.d0) then
    gag_bessel_in=0.d0
    return
  endif
  !
  tox = 2.d0/x
  bip = 0.d0
  bi  = 1.d0
  gag_bessel_in = 0.d0
  m = 2*((n+int(sqrt(float(iacc*n)))))
  do j=m,1,-1
    bim = bip+dfloat(j)*tox*bi
    bip = bi
    bi  = bim
    if (abs(bi).gt.bigno) then
      bi  = bi*bigni
      bip = bip*bigni
      gag_bessel_in = gag_bessel_in*bigni
    endif
    if (j.eq.n) gag_bessel_in = bip
  enddo
  gag_bessel_in = gag_bessel_in*gag_bessel_i0(x)/bi
  !
end function gag_bessel_in
!
function gag_bessel_i0(x)
  !---------------------------------------------------------------------
  ! @ public
  ! Bessel functions for N=0
  !---------------------------------------------------------------------
  real(kind=8) :: gag_bessel_i0
  real(kind=8), intent(in) :: x
  ! Local
  real(kind=8) :: y,ax,bx
  real(kind=8), parameter :: p1=1.d0
  real(kind=8), parameter :: p2=3.5156229d0
  real(kind=8), parameter :: p3=3.0899424d0
  real(kind=8), parameter :: p4=1.2067492d0
  real(kind=8), parameter :: p5=0.2659732d0
  real(kind=8), parameter :: p6=0.360768d-1
  real(kind=8), parameter :: p7=0.45813d-2
  real(kind=8), parameter :: q1=0.39894228d0
  real(kind=8), parameter :: q2=0.1328592d-1
  real(kind=8), parameter :: q3=0.225319d-2
  real(kind=8), parameter :: q4=-0.157565d-2
  real(kind=8), parameter :: q5=0.916281d-2
  real(kind=8), parameter :: q6=-0.2057706d-1
  real(kind=8), parameter :: q7=0.2635537d-1
  real(kind=8), parameter :: q8=-0.1647633d-1
  real(kind=8), parameter :: q9=0.392377d-2
  !
  if (abs(x).lt.3.75d0) then
    y = (x/3.75d0)**2
    gag_bessel_i0 = p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))
  else
    ax = abs(x)
    y = 3.75d0/ax
    bx = exp(ax)/sqrt(ax)
    ax = q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*(q7+y*(q8+y*q9)))))))
    gag_bessel_i0 = ax*bx
  endif
  !
end function gag_bessel_i0
!
function gag_bessel_i1(x)
  !---------------------------------------------------------------------
  ! @ public
  ! Bessel functions for N=1
  !---------------------------------------------------------------------
  real(kind=8) :: gag_bessel_i1
  real(kind=8), intent(in) :: x
  ! Local
  real(kind=8) :: y,ax,bx
  real(kind=8), parameter :: p1=0.5d0
  real(kind=8), parameter :: p2=0.87890594d0
  real(kind=8), parameter :: p3=0.51498869d0
  real(kind=8), parameter :: p4=0.15084934d0
  real(kind=8), parameter :: p5=0.2658733d-1
  real(kind=8), parameter :: p6=0.301532d-2
  real(kind=8), parameter :: p7=0.32411d-3
  real(kind=8), parameter :: q1=0.39894228d0
  real(kind=8), parameter :: q2=-0.3988024d-1
  real(kind=8), parameter :: q3=-0.362018d-2
  real(kind=8), parameter :: q4=0.163801d-2
  real(kind=8), parameter :: q5=-0.1031555d-1
  real(kind=8), parameter :: q6=0.2282967d-1
  real(kind=8), parameter :: q7=-0.2895312d-1
  real(kind=8), parameter :: q8=0.1787654d-1
  real(kind=8), parameter :: q9=-0.420059d-2
  !
  if (abs(x).lt.3.75d0) then
    y = (x/3.75d0)**2
    gag_bessel_i1 = x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
  else
    ax = abs(x)
    y = 3.75d0/ax
    bx = exp(ax)/sqrt(ax)
    ax = q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*(q7+y*(q8+y*q9)))))))
    gag_bessel_i1=ax*bx
  endif
  !
end function gag_bessel_i1
