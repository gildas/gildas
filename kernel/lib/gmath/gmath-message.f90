!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GMATH messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gmath_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gmath_message_id = gpack_global_id  ! Default value for startup message
  !
end module gmath_message_private
!
subroutine gmath_message_set_id(id)
  use gmath_interfaces, except_this=>gmath_message_set_id
  use gmath_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gmath_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gmath_message_id
  call gmath_message(seve%d,'gmath_message_set_id',mess)
  !
end subroutine gmath_message_set_id
!
subroutine gmath_message(mkind,procname,message)
  use gmath_dependencies_interfaces
  use gmath_interfaces, except_this=>gmath_message
  use gmath_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gmath_message_id,mkind,procname,message)
  !
end subroutine gmath_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
