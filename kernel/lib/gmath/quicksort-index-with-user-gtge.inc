  !---------------------------------------------------------------------
  ! Code for g*_quicksort_index_with_user_gtge subroutines. The code is
  ! always the same, only the variables type and kind (declared out of
  ! this include file) differ.
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='QUICKSORT'
  logical :: mgtl,lgtr,rgtm
  character(len=message_length) :: mess
  !
  error = .false.
  !
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! Set KEY = median of X(L), X(M), X(R)
  ! No! This is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! To fix this problem, I found (but I cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. P.V.
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  !
  mgtl = ugt(x(m),x(l))
  rgtm = ugt(x(r),x(m))
  !
  ! Algorithm to select the median key:
  !
  !                       MGTL    RGTM    LGTR    MGTL.EQV.LGTR   MEDIAN_KEY
  !
  !       KL < KM < KR    T       T       *       *               KM
  !       KL > KM > KR    F       F       *       *               KM
  !
  !       KL < KM > KR    T       F       F       F               KR
  !       KL < KM > KR    T       F       T       T               KL
  !
  !       KL > KM < KR    F       T       F       T               KL
  !       KL > KM < KR    F       T       T       F               KR
  !
  if (mgtl .eqv. rgtm) then
    key = x(m)
  else
    lgtr = ugt(x(l),x(r))
    if (mgtl .eqv. lgtr) then
      key = x(l)
    else
      key = x(r)
    endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
10 if (uge(x(i),key)) goto 11
  i = i + 1
  goto 10
11 continue
  ! Find a small record on the right
20 if (uge(key,x(j))) goto 21
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  temp = x(i)
  x(i) = x(j)
  x(j) = temp
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! Push the two halves on the stack
2 continue
  if (j-l+1 .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(mess,*) 'Stack overflow ',sp
      call gmath_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    lstack(sp) = l
    rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
    sp = sp + 1
    if (sp.gt.maxstack) then
      write(mess,*) 'Stack overflow ',sp
      call gmath_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    lstack(sp) = j+1
    rstack(sp) = r
  endif
  !
  ! Anything left to process?
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do j=n-1,1,-1
    k = j
    do i=j+1,n
      if (uge(x(i),x(j))) exit ! i
      k = i
    enddo ! i
    if (k.eq.j) cycle ! j
    temp = x(j)
    do i = j+1,k
      x(i-1) = x(i)
    enddo ! i
    x(k) = temp
  enddo ! j
