!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module fit_minuit_parameters
  !---------------------------------------------------------------------
  ! @ private
  ! Support module for MINUIT fitting routines
  !---------------------------------------------------------------------
  !
  real(kind=8), parameter :: theprec=1.d-6  !
  integer(kind=4), parameter :: ntot=48     ! Size of problem
  integer(kind=4), parameter :: nvar=40     ! Size of problem
  !
end module fit_minuit_parameters
!
module fit_minuit_types
  use gildas_def
  use fit_minuit_parameters
  !---------------------------------------------------------------------
  ! @ private
  ! Support module for MINUIT fitting routines
  !---------------------------------------------------------------------
  !
  type :: fit_minuit_t
    real(kind=8) :: x(nvar), xt(nvar), dirin(nvar)
    real(kind=8) :: u(ntot), werr(ntot), alim(ntot), blim(ntot)
    real(kind=8) :: v(nvar,nvar)
    real(kind=8) :: xs(nvar), xts(nvar), wts(nvar)
    real(kind=8) :: y(nvar+1)
    real(kind=8) :: g(ntot), g2(ntot)
    real(kind=8) :: pstar(nvar), pstst(nvar), pbar(nvar), prho(nvar)
    real(kind=8) :: p(nvar,nvar), vt(nvar,nvar)
    real(kind=8) :: epsi, apsi, vtest
    real(kind=8) :: amin, up, sigma
    !
    integer(kind=4) :: maxint, npar
    integer(kind=4) :: maxext, nu, lcode(ntot), lcorsp(ntot), limset
    integer(kind=4) :: ipfix(nvar),npfix
    integer(kind=4) :: jh, jl
    integer(kind=4) :: isw(7), nblock
    integer(kind=4) :: nstepq, nfcn, nfcnmx
    integer(kind=4) :: newmin, itaur
    !
    ! Address of untyped 'blob' of data to be minimized. Not used by
    ! minuit but given back to callers thanks to callbacks. Keep this
    ! address to 0 will ensure this data will not be passed.
    integer(kind=address_length) :: data=0
    !
    ! Messages
    logical :: verbose
    integer(kind=4) :: owner=0   ! Owner id (default is the "global" package)
    integer(kind=4) :: isyswr=6  ! Output logical unit (obsolescent)
    !
  end type fit_minuit_t
  !
end module fit_minuit_types
!
module fit_minuit
  !---------------------------------------------------------------------
  ! @ public
  ! Support module for MINUIT fitting routines
  !---------------------------------------------------------------------
  use fit_minuit_parameters
  use fit_minuit_types
end module fit_minuit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module eclass_types
  !---------------------------------------------------------------------
  ! Support module for equivalence classes
  !---------------------------------------------------------------------
  !
  ! Common part shared by all equivalence class types. Not to be used
  ! directly
  type eclass_common_t
    integer(kind=4) :: nval  ! In: Number of values
    integer(kind=4) :: nequ  ! Out: Number of equivalence classes
    integer(kind=4), pointer :: cnt(:) => null()  ! In/out: counts for each value
    integer(kind=4), pointer :: bak(:) => null()  ! Out: Back pointer to the associated class
  end type eclass_common_t
  !
  type, extends(eclass_common_t) :: eclass_inte_t
    integer(kind=4), pointer :: val(:) => null()  ! In/out: values
  end type eclass_inte_t
  !
  type, extends(eclass_common_t) :: eclass_2inte_t
    integer(kind=4), pointer :: val1(:) => null()  ! In/out: values
    integer(kind=4), pointer :: val2(:) => null()  ! In/out: values
  end type eclass_2inte_t
  !
  type, extends(eclass_common_t) :: eclass_2inte1char_t
    integer(kind=4),   pointer :: val1(:) => null()  ! In/out: values
    integer(kind=4),   pointer :: val2(:) => null()  ! In/out: values
    character(len=16), pointer :: val3(:) => null()  ! In/out: values
  end type eclass_2inte1char_t
  !
  ! Not extended from eclass_common_t because of an ifort 14 bug which
  ! fails compiling mrtcal/lib-imbfits/read-header.f90
  type :: eclass_2inte2char_t
    integer(kind=4) :: nval  ! In: Number of values
    integer(kind=4) :: nequ  ! Out: Number of equivalence classes
    integer(kind=4),   pointer :: val1(:) => null()  ! In/out: values
    integer(kind=4),   pointer :: val2(:) => null()  ! In/out: values
    character(len=16), pointer :: val3(:) => null()  ! In/out: values
    character(len=16), pointer :: val4(:) => null()  ! In/out: values
    integer(kind=4), pointer :: cnt(:) => null()  ! In/out: counts for each value
    integer(kind=4), pointer :: bak(:) => null()  ! Out: Back pointer to the associated class
  end type eclass_2inte2char_t
  !
  type, extends(eclass_common_t) :: eclass_dble_t
    real(kind=8),    pointer :: val(:) => null()  ! In/out: values
  end type eclass_dble_t
  !
  type, extends(eclass_common_t) :: eclass_2dble_t
    real(kind=8),    pointer :: val1(:) => null()  ! In/out: values
    real(kind=8),    pointer :: val2(:) => null()  ! In/out: values
  end type eclass_2dble_t
  !
  type, extends(eclass_common_t) :: eclass_2dble1char_t
    real(kind=8),      pointer :: val1(:) => null()  ! In/out: values
    real(kind=8),      pointer :: val2(:) => null()  ! In/out: values
    character(len=24), pointer :: val3(:) => null()  ! In/out: values
  end type eclass_2dble1char_t
  !
  type, extends(eclass_common_t) :: eclass_2dble3inte_t
    real(kind=8),      pointer :: val1(:) => null()  ! In/out: values
    real(kind=8),      pointer :: val2(:) => null()  ! In/out: values
    integer(kind=4),   pointer :: val3(:) => null()  ! In/out: values
    integer(kind=4),   pointer :: val4(:) => null()  ! In/out: values
    integer(kind=4),   pointer :: val5(:) => null()  ! In/out: values
  end type eclass_2dble3inte_t
  !
  type, extends(eclass_common_t) :: eclass_char_t
    character(len=80), pointer :: val(:) => null()  ! In/out: values
  end type eclass_char_t
  !
end module eclass_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
