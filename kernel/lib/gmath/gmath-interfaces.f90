module gmath_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GMATH interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gmath_interfaces_public
  use gmath_interfaces_private
  !
end module gmath_interfaces
!
module gmath_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GMATH dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  !
end module gmath_dependencies_interfaces
