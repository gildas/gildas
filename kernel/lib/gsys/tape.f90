subroutine mtskip(chan,nskip,ncoun,end_tape,err)
  use gsys_interfaces, except_this=>mtskip
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! FITS  Internal routine
  !       Skip tape-marks and return count and status
  !       This entry should only be called when END_OF_FILE is
  !       already .TRUE.
  ! Arguments :
  !       CHAN     I      I/O Channel
  !       NSKIP    I      Number of files to skip         Input
  !       NCOUN    I      Number of tape mark skipped     Output
  !       END_TAPE L      End-of-Tape reached             Output
  !       ERR      L      Logical error flag              Output
  !---------------------------------------------------------------------
  integer(kind=4) :: chan           !
  integer(kind=2) :: nskip          !
  integer(kind=4) :: ncoun          !
  logical         :: end_tape       !
  logical         :: err            !
  integer(kind=4) :: nbuf           !
  integer(kind=1) :: buf(*)         ! NBUF normally
  logical         :: end_file       !
  integer(kind=4) :: block          !
  !*----------------------------------------------------------------------
  ! Skip a record
  !
entry mtskbl (chan,nskip,ncoun,end_file,end_tape,err)
  !
entry mtend(chan,end_tape,err)
  !----------------------------------------------------------------------
  ! UTIL  Internal routine
  !       Move to end of current file on tape
  ! Arguments
  !       CHAN    I       I/O channel                     Input
  !       END_TAPE L      End of tape reached
  !       ERR     L       logical error flag              Output
  !----------------------------------------------------------------------
  !
entry mtinit (chan,err)
  !----------------------------------------------------------------------
  ! UTIL  Internal routine
  !       Init a magnetic tape
  ! Arguments
  !       CHAN    I       I/O channel                     Input
  !       ERR     L       logical error flag              Output
  !----------------------------------------------------------------------
  !
entry mtrewi (chan,err)
  !----------------------------------------------------------------------
  ! UTIL  Internal routine
  !       Rewind a magtape
  ! Arguments
  !       CHAN    I       i/o channel                     Input
  !       ERR     L       logical error flag              Output
  !----------------------------------------------------------------------
  !
entry mtmark(chan,err)
  !----------------------------------------------------------------------
  ! SAS   Utility routine
  !       Writes an end-of-tape marker at current tape position
  !       and leave tape at end of tape.
  ! Arguments :
  !       CHAN    I       I/O Channel assigned to tape    Input
  !       ERR     L       Logical error flag              Output
  !----------------------------------------------------------------------
entry mteof(chan,err)
  !----------------------------------------------------------------------
  ! SAS   Utility routine
  !       Writes an end-of-file marker at current tape position
  !       and leave tape at end of tape.
  ! Arguments :
  !       CHAN    I       I/O Channel assigned to tape    Input
  !       ERR     L       Logical error flag              Output
  !----------------------------------------------------------------------
entry mtwrit (chan,buf,nbuf,err)
  !----------------------------------------------------------------------
  ! UTIL  Internal routine
  !       Writes buffer to I/O channel
  ! Arguments :
  !       CHAN    I       I/O channel                     Input
  !       BUF     L*1(*)  Buffer to be written            Input
  !       NBUF    I       Number of bytes in BUFFER       Input
  !       ERR     L       Logical error flag              Output
  !----------------------------------------------------------------------
entry mtread (chan,buf,nbuf,end_file,end_tape,err)
  !----------------------------------------------------------------------
  ! UTIL  Internal routine
  !       Read buffer from I/O channel
  ! Arguments :
  !       CHAN    I       I/O channel                     Input
  !       BUF     L*1(*)  Buffer to be written            Input
  !       NBUF    I       Number of bytes in BUF          Input
  !       END_FILE L      End of file flag                Output
  !       END_TAPE L      End of tape flag                Output
  !       ERR     L       Logical error flag              Output
  !----------------------------------------------------------------------
entry mtflus (chan,err)
  !----------------------------------------------------------------------
  ! UTIL  : Internal routine
  !       Flush output buffer for blocked-I/O
  !----------------------------------------------------------------------
entry mtblock (block)
  !----------------------------------------------------------------------
  ! UTIL  : Internal routine
  !       Specify blocking factor for tape write
  !----------------------------------------------------------------------
  call gsys_message(seve%e,'TAPE','Tape routines have not yet been implemented')
  err = .true.
end subroutine mtskip
!
subroutine mtmoun (tape,dens,chan,err)
  use gsys_interfaces, except_this=>mtmoun
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! UTIL  Internal routine
  !       Tries to mount a tape on device "TAPE", which
  !       must be assigned to a generic name of tape devices.
  ! Arguments :
  !       TAPE    C*(*)   Tape device logical name        Input
  !       CHAN    I       I/O channel assigned            Output
  !       ERR     L       Logical error flag              Input
  !---------------------------------------------------------------------
  character(len=*) :: tape          !
  integer(kind=4)  :: dens          !
  integer(kind=4)  :: chan          !
  logical          :: err           !
  !
entry mtdism (tape,chan,err)
  !----------------------------------------------------------------------
  ! SAS   Internal routine
  !       Dismount a tape on given channel, and frees the corresponding
  !       I/O channel
  ! Arguments :
  !       TAPE    C*(*)   Logical name of tape drive              Input
  !       CHAN    I       I/O Channel assigned to tape            Input
  !       ERR     L       Logical error flag                      Output
  !----------------------------------------------------------------------
  call gsys_message(seve%e,'TAPE','Tape routines have not yet been implemented')
  err = .true.
end subroutine mtmoun
