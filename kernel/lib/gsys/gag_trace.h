
#ifndef _GAG_TRACE_H_
#define _GAG_TRACE_H_

/**
 * @file
 */

/*****************************************************************************
 *                            Dependencies                                   *
 *****************************************************************************/

#include <stdio.h>
#include "cfc.h"

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

int gag_trace_open( const char *filename, const char *prefix);
int gag_trace_activate( const char *prog_name);
int gag_trace_activate_args( int argc, char **argv);
void gag_trace_close( );
void gag_trace( const char *args, ...);
FILE *gag_trace_get_file_pointer( );
void gag_trace_set_file_pointer( FILE *fp);

#endif /* _GAG_TRACE_H_ */

