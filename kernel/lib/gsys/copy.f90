subroutine w4tow4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of 'untyped' 4-bytes words. In practice, use integer*4
  ! type because any bit pattern is valid in this representation
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine w4tow4
!
subroutine r4tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of real*4. In practice, use integer*4 type because any
  ! bit pattern is valid in this representation. Some compilers and/or
  ! architectures may change the real*4 bit pattern during the copy,
  ! especially if value is a NaN
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r4tor4
!
subroutine r8tor8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of real*8. In practice, use integer*8 type because any
  ! bit pattern is valid in this representation. Some compilers and/or
  ! architectures may change the real*8 bit pattern during the copy,
  ! especially if value is a NaN.
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)  :: in(*)   !
  integer(kind=8), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r8tor8
!
subroutine r8tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r8tor4
!
subroutine r4tor8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: in(*)   !
  real(kind=8),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r4tor8
!
subroutine i4tor8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  real(kind=8),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4tor8
!
subroutine i4tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4tor4
!
subroutine r4toi4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r4toi4_fini instead.
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -2147483648
    elseif  (in(i).le.iplus) then
      out(i) = nint(in(i),kind=4)
    else
      out(i) = 2147483647
    endif
  enddo
end subroutine r4toi4
!
subroutine r8toi4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r8toi4_fini instead.
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -2147483648
    elseif  (in(i).le.iplus) then
      out(i) = nint(in(i),kind=4)
    else
      out(i) = 2147483647
    endif
  enddo
end subroutine r8toi4
!
subroutine i8tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i8tor4
!
subroutine r4toi8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r4toi8_fini instead.
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: in(*)   !
  integer(kind=8), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -9223372036854775808_8
    elseif (in(i).le.iplus) then
      out(i) = nint(in(i),kind=8)
    else
      out(i) = 9223372036854775807_8
    endif
  enddo
end subroutine r4toi8
!
subroutine l4tol4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of logical*4. Be conservative: use integer*4 type
  ! because any bit pattern is valid in this representation. Not sure of
  ! what happens in case of invalid logical value.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine l4tol4
!
subroutine i4toi4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4toi4
!
subroutine i8toi8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)  :: in(*)   !
  integer(kind=8), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i8toi8
!
subroutine i4toi8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: in(*)   !
  integer(kind=8), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4toi8
!
subroutine i8toi4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  integer(kind=8), parameter :: iplus = 2147483647_8 ! 2**31-1
  integer(kind=8), parameter :: imoins = -2147483648_8 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = imoins
    elseif (in(i).gt.iplus) then
      out(i) = iplus
    else
      out(i) = in(i)
    endif
  enddo
end subroutine i8toi4
!
subroutine i8tor8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)  :: in(*)   !
  real(kind=8),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i8tor8
!
subroutine r8toi8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r8toi8_fini instead.
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)  :: in(*)   !
  integer(kind=8), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -9223372036854775808_8
    elseif (in(i).le.iplus) then
      out(i) = nint(in(i),kind=8)
    else
      out(i) =  9223372036854775807_8
    endif
  enddo
end subroutine r8toi8
!
subroutine c4toc4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  complex(kind=4), intent(in)  :: in(*)   !
  complex(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine c4toc4
!
subroutine c4tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  complex,         intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine c4tor4
!
subroutine r4toc4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: in(*)   !
  complex(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i) = cmplx(in(i),kind=4)
  enddo
end subroutine r4toc4
!
subroutine c8tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  double complex,  intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine c8tor4
!
subroutine r4toc8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: in(*)   !
  double complex,  intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r4toc8
!
subroutine i2toi4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=2), intent(in)  :: in(*)   !
  integer(kind=4), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i) = in(i)
  enddo
end subroutine i2toi4
!
subroutine adtoad(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=address_length), intent(in)  :: in(*)   !
  integer(kind=address_length), intent(out) :: out(*)  !
  integer(kind=4),              intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i) = in(i)
  enddo
end subroutine adtoad
!
subroutine i2tor4(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=2), intent(in)  :: in(*)   !
  real(kind=4),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i) = in(i)
  enddo
end subroutine i2tor4
!
subroutine i2tor8(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=2), intent(in)  :: in(*)   !
  real(kind=8),    intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i) = in(i)
  enddo
end subroutine i2tor8
!
subroutine bytoby(in,out,n)
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=1), intent(in)  :: in(*)   !
  integer(kind=1), intent(out) :: out(*)  !
  integer(kind=4), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine bytoby
!
subroutine bytoch(in,ch,n)
  use gildas_def
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Copy a Byte array into a Fortran character String
  !---------------------------------------------------------------------
  integer(kind=1),  intent(in)    :: in(*)  !
  character(len=*), intent(inout) :: ch     ! Ifort complains if intent(out)
  integer(kind=4),  intent(in)    :: n      ! [bytes]
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ich
  integer(kind=4) :: i, m
  !
  ich = bytpnt(locstr(ch),membyt)
  m = min(n,len(ch))
  do i=1,m
    membyt(ich+i-1) = in(i)
  enddo
end subroutine bytoch
!
subroutine chtoby(ch,out,n)
  use gildas_def
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Copy a Fortran character String into a Byte array
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: ch      !
  integer(kind=1),  intent(out) :: out(*)  !
  integer(kind=4),  intent(in)  :: n       ! [bytes]
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ich
  integer(kind=4) :: i, m
  !
  ich = bytpnt(locstr(ch),membyt)
  m = min(n,len(ch))
  do i=1,m
    out(i) = membyt(ich+i-1)
  enddo
end subroutine chtoby
!
subroutine w4toch(in,ch,n)
  use gildas_def
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Same as bytoch except number of elements is in words (not bytes).
  ! Suited for arrays multiple of 4-bytes.
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)    :: in(*)  !
  character(len=*), intent(inout) :: ch     !
  integer(kind=4),  intent(in)    :: n      ! [words]
  !
  call bytoch(in,ch,n*4)
  !
end subroutine w4toch
!
subroutine chtow4(ch,out,n)
  use gildas_def
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Same as chtoby except number of elements is in words (not bytes).
  ! Suited for arrays multiple of 4-bytes.
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: ch      !
  integer(kind=4),  intent(out) :: out(*)  !
  integer(kind=4),  intent(in)  :: n       ! [words]
  !
  call chtoby(ch,out,n*4)
  !
end subroutine chtow4
!
! Below duplicate of the subroutines, but which accept number of elements
! to be copied as integers of kind=size_length.
!
subroutine w4tow4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of 'untyped' 4-bytes words. In practice, use integer*4
  ! type because any bit pattern is valid in this representation
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine w4tow4_sl
!
subroutine w8tow8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of 'untyped' 8-bytes words. In practice, use integer*8
  ! type because any bit pattern is valid in this representation
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  integer(kind=8),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine w8tow8_sl
!
subroutine r4tor4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of real*4. In practice, use integer*4 type because any
  ! bit pattern is valid in this representation. Some compilers and/or
  ! architectures may change the real*4 bit pattern during the copy,
  ! especially if value is a NaN
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r4tor4_sl
!
subroutine r8tor8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of real*8. In practice, use integer*8 type because any
  ! bit pattern is valid in this representation. Some compilers and/or
  ! architectures may change the real*8 bit pattern during the copy,
  ! especially if value is a NaN.
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  integer(kind=8),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r8tor8_sl
!
subroutine r8tor4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)  :: in(*)   !
  real(kind=4),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r8tor4_sl
!
subroutine r4tor8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)  :: in(*)   !
  real(kind=8),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine r4tor8_sl
!
subroutine l4tol4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Copy a list of logical*4. Be conservative: use integer*4 type
  ! because any bit pattern is valid in this representation. Not sure of
  ! what happens in case of invalid logical value.
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine l4tol4_sl
!
subroutine i4tor8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  real(kind=8),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4tor8_sl
!
subroutine i4tor4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  real(kind=4),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4tor4_sl
!
subroutine r4toi4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r4toi4_fini instead.
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -2147483648
    elseif  (in(i).le.iplus) then
      out(i) = nint(in(i),kind=4)
    else
      out(i) = 2147483647
    endif
  enddo
end subroutine r4toi4_sl
!
subroutine r8toi4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r8toi4_fini instead.
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -2147483648
    elseif  (in(i).le.iplus) then
      out(i) = nint(in(i),kind=4)
    else
      out(i) = 2147483647
    endif
  enddo
end subroutine r8toi4_sl
!
subroutine i8tor4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  real(kind=4),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i8tor4_sl
!
subroutine r4toi8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r8toi4_fini instead.
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)  :: in(*)   !
  integer(kind=8),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -9223372036854775808_8
    elseif (in(i).le.iplus) then
      out(i) = nint(in(i),kind=8)
    else
      out(i) = 9223372036854775807_8
    endif
  enddo
end subroutine r4toi8_sl
!
subroutine i4toi8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  integer(kind=8),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i4toi8_sl
!
subroutine i8toi4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  integer(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  integer(kind=8), parameter :: iplus = 2147483647_8 ! 2**31-1
  integer(kind=8), parameter :: imoins = -2147483648_8 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = imoins
    elseif (in(i).gt.iplus) then
      out(i) = iplus
    else
      out(i) = in(i)
    endif
  enddo
end subroutine i8toi4_sl
!
subroutine i8tor8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  real(kind=8),              intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine i8tor8_sl
!
subroutine r8toi8_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Do not check for non-finite values (NaN, +Inf, -Inf) which might be
  ! a serious problem. Use r8toi8_fini instead.
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)  :: in(*)   !
  integer(kind=8),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (in(i).lt.imoins) then
      out(i) = -9223372036854775808_8
    elseif (in(i).le.iplus) then
      out(i) = nint(in(i),kind=8)
    else
      out(i) =  9223372036854775807_8
    endif
  enddo
end subroutine r8toi8_sl
!
subroutine c4toc4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  complex(kind=4),           intent(in)  :: in(*)   !
  complex(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine c4toc4_sl
!
subroutine r4toc4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)  :: in(*)   !
  complex(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i) = cmplx(in(i),kind=4)
  enddo
end subroutine r4toc4_sl
!
subroutine r8toc4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)  :: in(*)   !
  complex(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i) = cmplx(in(i),kind=4)
  enddo
end subroutine r8toc4_sl
!
subroutine i4toc4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: in(*)   !
  complex(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i) = cmplx(in(i),kind=4)
  enddo
end subroutine i4toc4_sl
!
subroutine i8toc4_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)  :: in(*)   !
  complex(kind=4),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=size_length) :: i
  do i=1,n
    out(i) = cmplx(in(i),kind=4)
  enddo
end subroutine i8toc4_sl
!
subroutine r4toi4_fini(in,out,n,error)
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: in(*)   !
  integer(kind=4), intent(out)   :: out(*)  !
  integer(kind=4), intent(in)    :: n       !
  logical,         intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=4) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (.not.sic_fini4(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*4')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=4)
    endif
  enddo
end subroutine r4toi4_fini
!
subroutine r4toi8_fini(in,out,n,error)
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: in(*)   !
  integer(kind=8), intent(out)   :: out(*)  !
  integer(kind=4), intent(in)    :: n       !
  logical,         intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=4) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (.not.sic_fini4(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*8')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=8)
    endif
  enddo
end subroutine r4toi8_fini
!
subroutine r8toi4_fini(in,out,n,error)
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)    :: in(*)   !
  integer(kind=4), intent(out)   :: out(*)  !
  integer(kind=4), intent(in)    :: n       !
  logical,         intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=4) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (.not.sic_fini8(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*4')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=4)
    endif
  enddo
end subroutine r8toi4_fini
!
subroutine r8toi8_fini(in,out,n,error)
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)    :: in(*)   !
  integer(kind=8), intent(out)   :: out(*)  !
  integer(kind=4), intent(in)    :: n       !
  logical,         intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=4) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (.not.sic_fini8(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*8')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=8)
    endif
  enddo
end subroutine r8toi8_fini
!
subroutine i8toi4_fini(in,out,n,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8), intent(in)    :: in(*)   !
  integer(kind=4), intent(out)   :: out(*)  !
  integer(kind=4), intent(in)    :: n       !
  logical,         intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=4) :: i
  integer(kind=8), parameter :: iplus = 2147483647_8 ! 2**31-1
  integer(kind=8), parameter :: imoins = -2147483648_8 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'INTEGER*8 value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'INTEGER*8 value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = in(i)
    endif
  enddo
end subroutine i8toi4_fini
!
subroutine r4toi4_fini_sl(in,out,n,error)
  use gildas_def
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)    :: in(*)   !
  integer(kind=4),           intent(out)   :: out(*)  !
  integer(kind=size_length), intent(in)    :: n       !
  logical,                   intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=size_length) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (.not.sic_fini4(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*4')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=4)
    endif
  enddo
end subroutine r4toi4_fini_sl
!
subroutine r4toi8_fini_sl(in,out,n,error)
  use gildas_def
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=4),              intent(in)    :: in(*)   !
  integer(kind=8),           intent(out)   :: out(*)  !
  integer(kind=size_length), intent(in)    :: n       !
  logical,                   intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=size_length) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (.not.sic_fini4(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*8')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=8)
    endif
  enddo
end subroutine r4toi8_fini_sl
!
subroutine r8toi4_fini_sl(in,out,n,error)
  use gildas_def
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)    :: in(*)   !
  integer(kind=4),           intent(out)   :: out(*)  !
  integer(kind=size_length), intent(in)    :: n       !
  logical,                   intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=size_length) :: i
  real(kind=8), parameter :: iplus = 2147483647d0 ! 2**31-1
  real(kind=8), parameter :: imoins = -2147483648d0 ! -2**31
  !
  do i=1,n
    if (.not.sic_fini8(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*4')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=4)
    endif
  enddo
end subroutine r8toi4_fini_sl
!
subroutine r8toi8_fini_sl(in,out,n,error)
  use gildas_def
  use gbl_message
  use gsys_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Check for non-finite values (NaN, +Inf, -Inf)
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)    :: in(*)   !
  integer(kind=8),           intent(out)   :: out(*)  !
  integer(kind=size_length), intent(in)    :: n       !
  logical,                   intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=size_length) :: i
  real(kind=8), parameter :: imoins = -2.0d0**63
  real(kind=8), parameter :: iplus = 2.0d0**63-1
  !
  do i=1,n
    if (.not.sic_fini8(in(i))) then
      call gsys_message(seve%e,rname,  &
        'Cannot convert non-finite value (NaN, +Inf or -Inf) to INTEGER*8')
      error = .true.
      return
    elseif (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'Float value is too large to accomodate in an INTEGER*8')
      error = .true.
      return
    else
      out(i) = nint(in(i),kind=8)
    endif
  enddo
end subroutine r8toi8_fini_sl
!
subroutine i8toi4_fini_sl(in,out,n,error)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           intent(in)    :: in(*)   !
  integer(kind=4),           intent(out)   :: out(*)  !
  integer(kind=size_length), intent(in)    :: n       !
  logical,                   intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SIC'
  integer(kind=size_length) :: i
  integer(kind=8), parameter :: iplus = 2147483647_8 ! 2**31-1
  integer(kind=8), parameter :: imoins = -2147483648_8 ! -2**31
  !
  do i=1,n
    if (in(i).lt.imoins) then
      call gsys_message(seve%e,rname,  &
        'INTEGER*8 value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    elseif (in(i).gt.iplus) then
      call gsys_message(seve%e,rname,  &
        'INTEGER*8 value is too large to accomodate in an INTEGER*4')
      error = .true.
      return
    else
      out(i) = in(i)
    endif
  enddo
end subroutine i8toi4_fini_sl
!
subroutine bytoby_sl(in,out,n)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=1),           intent(in)  :: in(*)   !
  integer(kind=1),           intent(out) :: out(*)  !
  integer(kind=size_length), intent(in)  :: n       !
  ! Local
  integer(kind=4) :: i
  do i=1,n
    out(i)=in(i)
  enddo
end subroutine bytoby_sl
