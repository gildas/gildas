subroutine sic_decode(chain,value,ndiv,error)
  use gsys_interfaces, except_this=>sic_decode
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !   Decode a sexagesimal character string into angle value in radians.
  ! Formats can be:
  !       61.5000000      or      161
  !       61:30.0000      or      161:40
  !       61:45:50.9      or      161:40:50
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: chain  ! Character string
  real(kind=8),     intent(out)   :: value  ! Value of angle
  integer(kind=4),  intent(in)    :: ndiv   ! 360/24
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ANGLE'
  real(kind=8) :: conv,degrees,minutes,seconds
  integer(kind=4) :: ndot,nlen,nmin,nsec
  logical :: negative
  real(kind=8), parameter :: pi=3.141592653589793d0
  character(len=16) :: string
  !
  error = .true.
  !
  ! First find if any dot in Chain
  nlen = lenc(chain)
  ndot = index(chain,'.')
  if (ndot.eq.0) then
    ndot=nlen+1
  endif
  !
  ! Set CONV factor
  if (ndiv.eq.24) then
    conv = pi/12.d0
  elseif (ndiv.eq.360) then
    conv = pi/180.d0
  else
    write(string,*) ndiv
    call gsys_message(seve%e,rname,'Invalid division number '//string)
    return
  endif
  !
  ! Find sign
  negative = index(chain,'-') .ne. 0
  !
  ! Find if any Minutes Field
  nmin = index(chain,':')
  if (nmin.eq.0) then
    ! No Minutes Field
    if (nlen.eq.0) goto 99
    read(chain(1:nlen),*,err=99) degrees
    value = conv*degrees
    error = .false.
    return
  endif
  !
  ! Find if any Second field
  nsec = index(chain(nmin+1:nlen),':')
  if (nsec.eq.0) then
    ! No Seconds Field
    !         IF (NDOT.NE.NMIN+3) THEN
    if (ndot.lt.nmin+1) then
      ! Minutes Field not two characters
      goto 99
    endif
    read(chain(nmin+1:nlen),*,err=99) minutes
    if (minutes.ge.60.d0) then
      call gsys_message(seve%e,rname,'more than 60 minutes...')
      return
    endif
    read(chain(1:nmin-1),*,err=99) degrees
    if (negative) then
      value = conv*(degrees-minutes/60.d0)
    else
      value = conv*(degrees+minutes/60.d0)
    endif
  elseif (nsec.le.1) then
    ! Minutes Field not two characters
    goto 99
  else
    ! Seconds Field is present
    if (ndot.le.nmin+nsec) then
      ! Seconds Field not two characters
      goto 99
    endif
    read(chain(nmin+nsec+1:nlen),*,err=99) seconds
    if (seconds.ge.60.d0) then
      call gsys_message(seve%e,rname,'more than 60 seconds...')
      return
    endif
    read(chain(nmin+1:nmin+nsec-1),*,err=99) minutes
    if (minutes.ge.60.d0) then
      call gsys_message(seve%e,rname,'more than 60 minutes...')
      return
    endif
    read(chain(1:nmin-1),*,err=99) degrees
    if (negative) then
      value = conv*(degrees-minutes/60.d0-seconds/3600.d0)
    else
      value = conv*(degrees+minutes/60.d0+seconds/3600.d0)
    endif
  endif
  error = .false.
  return
  !
99 call gsys_message(seve%e,rname,'Invalid angle format '//chain)
end subroutine sic_decode
