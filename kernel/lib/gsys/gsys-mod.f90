!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gbl_message
  !---------------------------------------------------------------------
  ! @public
  ! Support for message printing to screen and message file.
  !---------------------------------------------------------------------
  integer(kind=4), parameter :: message_length=512
  integer(kind=4), parameter :: filter_length=16    ! Filter string, e.g. "s=fewridtcu"
                                            ! There should be a link with nseve
  !
  ! Define severity type with codes in variables with same name
  type :: message_severity_t  ! Kind of message
    integer(kind=4) :: f      ! Fatal error
    integer(kind=4) :: e      ! Error
    integer(kind=4) :: w      ! Warning
    integer(kind=4) :: r      ! Result
    integer(kind=4) :: i      ! Information
    integer(kind=4) :: d      ! Debug
    integer(kind=4) :: t      ! Trace
    integer(kind=4) :: c      ! Command
    integer(kind=4) :: u      ! Unknown (reserved for error handling, should not be used)
  end type message_severity_t
  !
  ! Incarnate a severity structure:
  type(message_severity_t), parameter :: seve = message_severity_t(1,2,3,4,5,6,7,8,9)
  !
#ifdef WIN32
!MS$ATTRIBUTES DLLEXPORT :: seve
#endif /* WIN32 */
end module gbl_message
!
module gbl_ansicodes
  !---------------------------------------------------------------------
  ! @ public
  ! ANSI terminal control codes
  ! Contains only the most supported ones to colorize and emphasize
  !---------------------------------------------------------------------
  character(len=1), parameter :: c_esc = achar(27)
  character(len=2), parameter :: c_start = c_esc // '['
  character(len=1), parameter :: c_end = 'm'
  !
  character(len=5), parameter :: c_black = c_start//'30'//c_end
  character(len=5), parameter :: c_red = c_start//'31'//c_end
  character(len=5), parameter :: c_green = c_start//'32'//c_end
  character(len=5), parameter :: c_yellow = c_start//'33'//c_end
  character(len=5), parameter :: c_blue = c_start//'34'//c_end
  character(len=5), parameter :: c_magenta = c_start//'35'//c_end
  character(len=5), parameter :: c_cyan = c_start//'36'//c_end
  character(len=5), parameter :: c_white = c_start//'37'//c_end
  character(len=4), parameter :: c_clear = c_start // '0' // c_end
  character(len=4), parameter :: c_bold = c_start // '1' // c_end
  character(len=4), parameter :: c_under = c_start // '4' // c_end
  character(len=4), parameter :: c_reverse = c_start // '7' // c_end
  character(len=4), parameter :: c_blink = c_start // '5' // c_end
  !
  ! Experimental 8-bits colors (to be tested on several terminals)
  character(len=11), parameter :: c_orange = c_start//'38;5;208'//c_end
  !
end module gbl_ansicodes
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_def
  use gildas_def
  !---------------------------------------------------------------------
  ! @public
  ! Useful constants that should have been in module for long
  ! We probably need to do something to merge this with gildas_def.f90...
  !---------------------------------------------------------------------
  character(len=1), parameter :: backslash = char(92)
  integer(kind=4),  parameter :: screen_line_length = 256
  !
end module sic_def
!
module gpack_def
  use sic_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @public
  ! Parameters and types used to define a package
  !---------------------------------------------------------------------
  integer(kind=4), parameter :: gpack_max_count = 32    ! Max number of packages
  integer(kind=4), parameter :: gpack_name_length = 10  ! Max length of package name
  integer(kind=4), parameter :: gpack_ext_length = 16   ! Max length of file extensions
  integer(kind=4), parameter :: gpack_null_id = -1      ! Id of undefined packages
  integer(kind=4), parameter :: gpack_global_id = 0     ! Id of the 'global' pseudo-package
  character(len=gpack_name_length), parameter :: gpack_global_name = 'global'
  !
  ! Type providing all infos which define a package
  type gpack_info_t
     character(len=gpack_name_length)  :: name=''          ! Package name
     character(len=gpack_ext_length)   :: ext=''           ! Package extension
     character(len=gpack_ext_length)   :: pro_suffix=''    ! Suffix to add on standard procedure names (welcome, define)
     character(len=screen_line_length) :: authors=''
     character(len=filter_length)      :: toscreen=''
     character(len=filter_length)      :: tofile=''
     integer(kind=address_length)      :: depend(gpack_max_count) = 0
     integer(kind=address_length)      :: init=0            ! Build subroutine
     integer(kind=address_length)      :: exec_on_child=0   ! Hook to execute for each parent
     integer(kind=address_length)      :: on_exit=0         ! Test for exit subroutine
     integer(kind=address_length)      :: clean=0           ! Cleanup subroutine
  end type gpack_info_t
  !
  ! Package properties (with some internals) are summarized in this type:
  type gpack_prop_t
     type(gpack_info_t) :: info                            ! Package static info
     integer(kind=4)    :: id = 0                          ! Package id
     integer(kind=4)    :: depend_id(gpack_max_count) = 0  ! Package dependencies
     logical            :: init_done = .false.             ! Init done flag
  end type gpack_prop_t
  !
end module gpack_def
!
module gsys_types
  use gildas_def
  !
  ! Elemental type which describe the 3 kinds of times (private)
  type :: cputime_elem_t
    real(kind=8) :: elapsed = 0.d0
    real(kind=8) :: user = 0.d0
    real(kind=8) :: system = 0.d0
  end type cputime_elem_t
  !
  ! Type associated to the public subroutines (public)
  type :: cputime_t
    type(cputime_elem_t) :: refe  ! Reference times
    type(cputime_elem_t) :: curr  ! Current times since reference
    type(cputime_elem_t) :: diff  ! Difference since last measurement
  end type cputime_t
  !
  ! Timing type (public)
  type time_t
    type(cputime_t) :: cputime
    logical         :: cleverstart = .true.  ! Start timing with a few loops
    integer(kind=8) :: iloop       = 0       ! Current loop number
    integer(kind=8) :: nloop       = 0       ! Total number of loops
    integer(kind=4) :: nbin        = 10      ! Number of divisions in standard mode
    ! Interval between each message, in number of loops. Use a float value to
    ! avoid a remainder at the end. Printing routines perform its own nint().
    real(kind=8)    :: interval    = 0.d0    ! Actual interval in use
    real(kind=8)    :: interval0   = 0.d0    ! Natural interval in standard mode
    real(kind=8)    :: maxtime     = 10.d0   ! Threshold for total time
  end type time_t
  !
  ! Modified-file type (public)
  type mfile_t
    character(len=filename_length) :: file=''  ! File name
    ! Last modification time (to the file system granularity)
    integer(kind=8) :: mtime
    ! Last time a modification was proved (to the OS granularity)
    integer(kind=8) :: ptime
    ! Was the file modified before last evaluation?
    logical         :: modif
  end type mfile_t
  !
end module gsys_types
!
module gsys_variables
  !---------------------------------------------------------------------
  ! Global variables private to libgsys
  !---------------------------------------------------------------------
  logical :: controlc=.false.  ! ^C has been pressed
end module gsys_variables
