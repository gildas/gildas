
#ifndef _SIC_UTIL_H_
#define _SIC_UTIL_H_

/**
 * @file
 * @see sic_util.c
 */

/*****************************************************************************
 *                          Macros & Definitions                             *
 *****************************************************************************/

#define SIC_MAX_PATH_LENGTH 1024
#define SIC_MAX_FILE_LENGTH 256
#define SIC_MAX_TRANSLATION_LENGTH 256

/*****************************************************************************
 *                           Function prototypes                             *
 *****************************************************************************/

char *sic_s_get_translation( const char *name);
char *sic_s_get_logical_path( const char *path);

#endif /* _SIC_UTIL_H_ */

