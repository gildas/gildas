module gsys_interfaces_none
  interface
    subroutine cdclose(ierror,lun)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
    end subroutine cdclose
  end interface
  !
  interface
    subroutine cddir(ierror,lun,s_dir,lines)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      character(len=*) :: s_dir                 !
      integer(kind=4) :: lines                  !
    end subroutine cddir
  end interface
  !
  interface
    subroutine cdfclose(ierror,fid)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: fid                    !
    end subroutine cdfclose
  end interface
  !
  interface
    subroutine cdfopen(ierror,lun,s_file,fid,recl,bytes)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      character(len=*) :: s_file                !
      integer(kind=4) :: fid                    !
      integer(kind=4) :: recl                   !
      integer(kind=4) :: bytes                  !
    end subroutine cdfopen
  end interface
  !
  interface
    subroutine cdfread(ierror,fid,startbyte,numbytes,array)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: fid                    !
      integer(kind=4) :: startbyte              !
      integer(kind=4) :: numbytes               !
      integer(kind=1) :: array(0:*)             !
    end subroutine cdfread
  end interface
  !
  interface
    subroutine cdgetblock(ierror,lun,block,array)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      integer(kind=4) :: block                  !
      integer(kind=1) :: array(0:511)           !
    end subroutine cdgetblock
  end interface
  !
  interface
    subroutine cdgetdir(ierror,lun,block,att,lines)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      integer(kind=4) :: block                  !
      integer(kind=4) :: att                    !
      integer(kind=4) :: lines                  !
    end subroutine cdgetdir
  end interface
  !
  interface
    subroutine cdgetdirline(ierror,lun,lineno,linebytes,linelen)
      integer(kind=4) :: ierror                  !
      integer(kind=4) :: lun                     !
      integer(kind=4) :: lineno                  !
      integer(kind=1) :: linebytes(0:*)          !
      integer(kind=4) :: linelen                 !
    end subroutine cdgetdirline
  end interface
  !
  interface
    subroutine cdgetlbs(ierror,lun,lbs,rootbeg,rootatt)
      integer(kind=4) :: ierror                 !Error flag
      integer(kind=4) :: lun                    !Logical unit number
      integer(kind=4) :: lbs                    !Logical block size
      integer(kind=4) :: rootbeg                !Starting block of root
      integer(kind=4) :: rootatt                !Attribute blocks in root
    end subroutine cdgetlbs
  end interface
  !
  interface
    subroutine cdgetlun(ierror,lun,lindex)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      integer(kind=4) :: lindex                 !
    end subroutine cdgetlun
  end interface
  !
  interface
    subroutine cdopen(ierror,lun,s_name)
      integer(kind=4) :: ierror                 !
      integer(kind=4) :: lun                    !
      character(len=*) :: s_name                !
    end subroutine cdopen
  end interface
  !
  interface
    subroutine smvb(from,length,to)
      integer(kind=1) :: from(*)                !
      integer(kind=4) :: length                 !
      integer(kind=1) :: to(*)                  !
    end subroutine smvb
  end interface
  !
#if defined(CYGWIN) || defined(MINGW)
  interface
    subroutine gbl_message_mp_seve
    end subroutine gbl_message_mp_seve
  end interface
  !
#endif
end module gsys_interfaces_none
