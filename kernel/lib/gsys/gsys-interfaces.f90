module gsys_interfaces_public_c
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public
  ! Public interfaces to C routines (hand made)
  !---------------------------------------------------------------------
  !
  integer(kind=4), external :: gag_isreal  ! Actually a Fortran function, not C
  integer(kind=4), external :: gag_isdble  ! Actually a Fortran function, not C
  integer(kind=address_length), external :: locwrd  ! no-interface
  !
  interface sic_getvm
    ! Generic interface to sic_getvm4/sic_getvm8, depending on the kind
    ! of the integer storing Nwords.
    function sic_getvm4(nwords,address)
    use gildas_def
    integer(kind=4) :: sic_getvm4  ! Function value on return
    integer(kind=4),              intent(in)  :: nwords   ! As a 32-bit integer
    integer(kind=address_length), intent(out) :: address
    end function sic_getvm4
    !
    function sic_getvm8(nwords,address)
    use gildas_def
    integer(kind=4) :: sic_getvm8  ! Function value on return
    integer(kind=8),              intent(in)  :: nwords   ! As a 64-bit integer
    integer(kind=address_length), intent(out) :: address
    end function sic_getvm8
    !
#if defined(FORTRAN2003)
    function sic_getvm4c(nwords,address)
    use iso_c_binding
    integer(kind=4) :: sic_getvm4c  ! Function value on return
    integer(kind=4), intent(in)  :: nwords   ! As a 32-bit integer
    type(c_ptr), intent(out) :: address
    end function sic_getvm4c
    !
    function sic_getvm8c(nwords,address)
    use iso_c_binding
    integer(kind=4) :: sic_getvm8c  ! Function value on return
    integer(kind=8), intent(in)  :: nwords   ! As a 64-bit integer
    type(c_ptr), intent(out) :: address
    end function sic_getvm8c
#endif
  end interface
  !
  interface free_vm
    subroutine free_vm4(nwords,addr)
    use gildas_def
    integer(kind=4),              intent(in) :: nwords
    integer(kind=address_length), intent(in) :: addr
    end subroutine free_vm4
    !
    subroutine free_vm8(nwords,addr)
    use gildas_def
    integer(kind=8),              intent(in) :: nwords
    integer(kind=address_length), intent(in) :: addr
    end subroutine free_vm8
  end interface
  !
  interface
    function gag_getpid()
    integer(kind=4) :: gag_getpid
    end function gag_getpid
    !
    function locstr(string)
    use gildas_def
    integer(kind=address_length) :: locstr
    character(len=*), intent(in) :: string
    end function locstr
    !
    function gag_pointer(addr,mem)
    use gildas_def
    integer(kind=address_length) :: gag_pointer
    integer(kind=address_length), intent(in) :: addr
    integer(kind=4),              intent(in) :: mem(*)
    end function gag_pointer
    !
    function bytpnt(addr,mem)
    use gildas_def
    integer(kind=address_length) :: bytpnt
    integer(kind=address_length), intent(in) :: addr
    integer(kind=1),              intent(in) :: mem(*)
    end function bytpnt
    !
    subroutine gag_fillook(fd,file)
    integer(kind=4),  intent(out) :: fd
    character(len=*), intent(in)  :: file
    end subroutine gag_fillook
    !
    subroutine gag_filclose(fd)
    integer(kind=4), intent(in) :: fd
    end subroutine gag_filclose
    !
    function gag_filseek(fd,offset)
    integer(kind=4) :: gag_filseek
    integer(kind=4), intent(in) :: fd
    integer(kind=4), intent(in) :: offset
    end function gag_filseek
    !
    function gag_system(string)
    integer(kind=4) :: gag_system
    character(len=*), intent(in) :: string
    end function gag_system
    !
    subroutine gag_debug_system(sw)
    integer(kind=4), intent(in) :: sw
    end subroutine gag_debug_system
    !
    subroutine gag_filrm(file)
    character(len=*), intent(in) :: file
    end subroutine gag_filrm
    !
    function gag_filcopy(fromfile,tofile)
    integer(kind=4) :: gag_filcopy
    character(len=*), intent(in) :: fromfile
    character(len=*), intent(in) :: tofile
    end function gag_filcopy
    !
    function gag_filmove(fromfile,tofile)
    integer(kind=4) :: gag_filmove
    character(len=*), intent(in) :: fromfile
    character(len=*), intent(in) :: tofile
    end function gag_filmove
    !
    function gag_filrename(fromfile,tofile)
    integer(kind=4) :: gag_filrename
    character(len=*), intent(in) :: fromfile
    character(len=*), intent(in) :: tofile
    end function gag_filrename
    !
    function gag_filsize(file,fsize)
    integer(kind=4) :: gag_filsize
    character(len=*), intent(in)  :: file
    integer(kind=8),  intent(out) :: fsize
    end function gag_filsize
    !
    function gag_filappend(fromfile,tofile)
    integer(kind=4) :: gag_filappend
    character(len=*), intent(in) :: fromfile
    character(len=*), intent(in) :: tofile
    end function gag_filappend
    !
    function gag_rmdir(dirname)
    integer(kind=4) :: gag_rmdir
    character(len=*), intent(in) :: dirname
    end function gag_rmdir
    !
    function gag_isdir(dirname)
    integer(kind=4) :: gag_isdir
    character(len=*), intent(in) :: dirname
    end function gag_isdir
    !
    subroutine sic_getenv(name,trans)
    character(len=*), intent(in) :: name
    character(len=*), intent(out) :: trans
    end subroutine sic_getenv
    !
    function sic_setenv(name,trans)
    integer(kind=4) :: sic_setenv
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: trans
    end function sic_setenv
    !
    function gag_inquire(infile,length)
    integer(kind=4) :: gag_inquire
    character(len=*), intent(in) :: infile
    integer(kind=4),  intent(in) :: length
    end function gag_inquire
    !
    function gag_mdate(path,date)
    integer(kind=4) :: gag_mdate
    character(len=*), intent(in)  :: path
    integer(kind=4),  intent(out) :: date
    end function gag_mdate
    !
    subroutine sic_c_date(returned_date)
    character(len=*), intent(out) :: returned_date
    end subroutine sic_c_date
    !
    subroutine sic_wait(time)
    real(kind=4), intent(in) :: time
    end subroutine sic_wait
    !
    subroutine executable_extension(ext)
    character(len=*), intent(out) :: ext
    end subroutine executable_extension
    !
    subroutine sic_c_assert(condition,message)
    logical,          intent(in) :: condition
    character(len=*), intent(in) :: message
    end subroutine sic_c_assert
    !
    subroutine gag_mkpath(path,error)
    character(len=*), intent(in)  :: path
    logical,          intent(out) :: error
    end subroutine gag_mkpath
    !
    subroutine gag_show_locked_files()
    end subroutine gag_show_locked_files
    !
    function gag_lock_file(filename)
    integer(kind=4) :: gag_lock_file
    character(len=*), intent(in) :: filename
    end function gag_lock_file
    !
    subroutine gag_resources()
    end subroutine gag_resources
    !
    function gag_setlocale(locname)
    integer(kind=4) :: gag_setlocale
    character(len=*), intent(in) :: locname
    end function gag_setlocale
    !
    subroutine gag_printlocale()
    end subroutine gag_printlocale
    !
    subroutine gag_ramsize(ramsize)
    integer(kind=8), intent(out) :: ramsize
    end subroutine gag_ramsize
    !
    subroutine gag_cachesize(l1size,l2size,l3size,l4size)
    integer(kind=8), intent(out) :: l1size
    integer(kind=8), intent(out) :: l2size
    integer(kind=8), intent(out) :: l3size
    integer(kind=8), intent(out) :: l4size
    end subroutine gag_cachesize
    !
    function sic_isatty()
    integer(kind=4) :: sic_isatty
    end function sic_isatty
    !
    function sic_ttyncol()
    integer(kind=4) :: sic_ttyncol
    end function sic_ttyncol
    !
    function sic_ttynlin()
    integer(kind=4) :: sic_ttynlin
    end function sic_ttynlin
    !
    function sic_fini4(x)
    logical :: sic_fini4
    real(kind=4), intent(in) :: x
    end function sic_fini4
    !
    function sic_fini8(x)
    logical :: sic_fini8
    real(kind=8), intent(in) :: x
    end function sic_fini8
    !
  end interface
  !
end module gsys_interfaces_public_c
!
module gsys_interfaces_private_c
  !---------------------------------------------------------------------
  ! @ private
  ! Private interfaces to C routines (hand made)
  !---------------------------------------------------------------------
  !
  interface
    subroutine gag_crash()
    end subroutine gag_crash
    !
    subroutine gag_errno(error,string)
    integer(kind=4), intent(in) :: error
    character(len=*), intent(out) :: string
    end subroutine gag_errno
    !
    subroutine sic_resolve_env(src,dst)
    character(len=*), intent(in) :: src
    character(len=*), intent(out) :: dst
    end subroutine sic_resolve_env
    !
    subroutine sic_c_datetime_from_raw(rawtime,returned_asciitime)
    integer(kind=4),  intent(in)  :: rawtime
    character(len=*), intent(out) :: returned_asciitime
    end subroutine sic_c_datetime_from_raw
    !
    subroutine sic_c_idatetime(vals,frac)
    integer(kind=4), intent(out) :: vals(6)
    real(kind=4),    intent(out) :: frac
    end subroutine sic_c_idatetime
    !
    subroutine sic_c_datetime(returned_asciitime)
    character(len=*), intent(out) :: returned_asciitime
    end subroutine sic_c_datetime
    !
    subroutine sic_c_isodatetime(returned_asciitime)
    character(len=*), intent(out) :: returned_asciitime
    end subroutine sic_c_isodatetime
    !
    subroutine gag_c_cputime(t)
    real(kind=8), intent(out) :: t(5)
    end subroutine gag_c_cputime
    !
    function set_dir(dir)
    integer(kind=4) :: set_dir
    character(len=*), intent(in) :: dir
    end function set_dir
    !
    function gag_directory_num_C(dirname,count)
    integer(kind=4) :: gag_directory_num_C
    character(len=*), intent(in) :: dirname
    integer(kind=4), intent(out) :: count
    end function gag_directory_num_C
    !
    function gag_mtime_c(path,sec,nsec)
    integer(kind=4) :: gag_mtime_c
    character(len=*), intent(in)  :: path
    integer(kind=4),  intent(out) :: sec
    integer(kind=4),  intent(out) :: nsec
    end function gag_mtime_c
    !
    function gag_time_c(sec,nsec)
    integer(kind=4) :: gag_time_c
    integer(kind=4), intent(out) :: sec
    integer(kind=4), intent(out) :: nsec
    end function gag_time_c
    !
    function gag_filsame_c(file1,file2)
    integer(kind=4) :: gag_filsame_c
    character(len=*), intent(in) :: file1
    character(len=*), intent(in) :: file2
    end function gag_filsame_c
  end interface
  !
end module gsys_interfaces_private_c
!
module gsys_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GSYS interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gsys_interfaces_private
  use gsys_interfaces_private_c
  !
end module gsys_interfaces
