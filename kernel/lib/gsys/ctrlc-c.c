
#include <signal.h>

#include "ctrlc.h"

/**
 * SIC Handler for Ctrl+C
 */
static void ctrlc_handler( int signum)
{
    set_ctrlc( );
    return;
}

/**
 * SIC Internal routine
 * Initiate Ctrl+C trapping in an interactive session
 */
void *CFC_API trap_ctrlc( void)
{
    signal( SIGINT, ctrlc_handler);
    return ((void *)0);
}

/**
 * SIC Internal routine
 * Suppress Ctrl+C trapping in an interactive session
 */
void *CFC_API no_ctrlc( void)
{
    signal( SIGINT, SIG_IGN);
    return ((void *)0);
}
