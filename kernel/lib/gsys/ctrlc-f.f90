function sic_ctrlc()
  use gsys_variables
  !---------------------------------------------------------------------
  ! @ public
  ! Return .TRUE. if ^C has been pressed, and restore CONTROLC flag to
  ! .FALSE.
  !---------------------------------------------------------------------
  logical :: sic_ctrlc  ! Function value on return
  !
  sic_ctrlc = controlc
  controlc = .false.
end function sic_ctrlc
!
function sic_ctrlc_status()
  use gsys_variables
  !---------------------------------------------------------------------
  ! @ public
  ! Return .TRUE. if ^C has been pressed but do *not* restore CONTROLC
  ! flag to .FALSE.
  !---------------------------------------------------------------------
  logical :: sic_ctrlc_status  ! Function value on return
  !
  sic_ctrlc_status = controlc
end function sic_ctrlc_status
!
subroutine set_ctrlc
  use gsys_variables
  !---------------------------------------------------------------------
  ! @ private
  ! Set CONTROLC flag to .TRUE.
  ! Re-initiate the ^C trapping by calling TRAP_CTRLC
  !---------------------------------------------------------------------
  controlc = .true.
  call trap_ctrlc
end subroutine set_ctrlc
