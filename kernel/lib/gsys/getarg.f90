function sic_get_arg_count()
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer(kind=4) :: sic_get_arg_count  !  Function value on return
  ! Local
  integer(kind=4) :: iargc
  !
  sic_get_arg_count = iargc()  ! This is a GNU F77 extension...
end function sic_get_arg_count
!
subroutine sic_get_arg(n,arg)
  use gsys_interfaces, except_this=>sic_get_arg
  !---------------------------------------------------------------------
  ! @ public
  !   This machine dependent function retrieves the Nth argument of
  ! command line.
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)  :: n    ! Argument index
  character(len=*), intent(out) :: arg  ! Argument value
  ! Local
  integer(kind=4) :: narg
  !
  narg = sic_get_arg_count()
  !
  if (n.lt.1.or.n.gt.narg) then
    arg = ''
    return
  endif
  !
  call getarg(n,arg)  ! This is a GNU F77 extension...
  !
end subroutine sic_get_arg
