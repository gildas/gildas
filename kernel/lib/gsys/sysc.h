
#ifndef _SYSC_H_
#define _SYSC_H_

/**
 * @file
 * @see sysc.c
 */

/*****************************************************************************
 *                           Dependencies                                    *
 *****************************************************************************/

#include <inttypes.h>
typedef void *fortran_pointer;
#include "cfc.h"


/*****************************************************************************
 *                        Definitions & Macros                               *
 *****************************************************************************/

#define gag_errno CFC_EXPORT_NAME( gag_errno)
#define gag_getpid CFC_EXPORT_NAME( gag_getpid)
#define gag_filnew CFC_EXPORT_NAME( gag_filnew)
#define gag_filopen CFC_EXPORT_NAME( gag_filopen)
#define gag_fillook CFC_EXPORT_NAME( gag_fillook)
#define gag_filtest CFC_EXPORT_NAME( gag_filtest)
#define gag_filclose CFC_EXPORT_NAME( gag_filclose)
#define gag_filread CFC_EXPORT_NAME( gag_filread)
#define gag_filwrite CFC_EXPORT_NAME( gag_filwrite)
#define gag_filseek CFC_EXPORT_NAME( gag_filseek)
#define gag_filsize CFC_EXPORT_NAME( gag_filsize)
#define gag_filsame_c CFC_EXPORT_NAME( gag_filsame_c)
#define gag_ramsize CFC_EXPORT_NAME( gag_ramsize)
#define gag_cachesize CFC_EXPORT_NAME( gag_cachesize)
#define gag_crash CFC_EXPORT_NAME( gag_crash)
#define gag_system CFC_EXPORT_NAME( gag_system)
#define gag_debug_system CFC_EXPORT_NAME( gag_debug_system)
#define gag_rmdir CFC_EXPORT_NAME( gag_rmdir)
#define gag_isdir CFC_EXPORT_NAME( gag_isdir)
#define gag_filrm CFC_EXPORT_NAME( gag_filrm)
#define gag_filcopy CFC_EXPORT_NAME( gag_filcopy)
#define gag_filmove CFC_EXPORT_NAME( gag_filmove)
#define gag_filrename CFC_EXPORT_NAME( gag_filrename)
#define gag_filappend CFC_EXPORT_NAME( gag_filappend)
#define gag_directory_num_c CFC_EXPORT_NAME( gag_directory_num_c)
#define sic_getenv CFC_EXPORT_NAME( sic_getenv)
#define sic_setenv CFC_EXPORT_NAME( sic_setenv)
#define sic_resolve_env CFC_EXPORT_NAME( sic_resolve_env)
#define gag_inquire CFC_EXPORT_NAME( gag_inquire)
#define gag_mdate CFC_EXPORT_NAME( gag_mdate)
#define gag_mtime_c CFC_EXPORT_NAME( gag_mtime_c)
#define gag_time_c CFC_EXPORT_NAME( gag_time_c)
#define sic_c_idatetime CFC_EXPORT_NAME( sic_c_idatetime)
#define sic_c_datetime CFC_EXPORT_NAME( sic_c_datetime)
#define sic_c_datetime_from_raw CFC_EXPORT_NAME( sic_c_datetime_from_raw)
#define sic_c_isodatetime CFC_EXPORT_NAME( sic_c_isodatetime)
#define sic_c_time CFC_EXPORT_NAME( sic_c_time)
#define sic_c_date CFC_EXPORT_NAME( sic_c_date)
#define gag_c_cputime CFC_EXPORT_NAME( gag_c_cputime)
#define sic_wait CFC_EXPORT_NAME( sic_wait)
#define sic_isatty CFC_EXPORT_NAME( sic_isatty)
#define sic_ttyncol CFC_EXPORT_NAME( sic_ttyncol)
#define sic_ttynlin CFC_EXPORT_NAME( sic_ttynlin)
#define sic_fini4 CFC_EXPORT_NAME( sic_fini4)
#define sic_fini8 CFC_EXPORT_NAME( sic_fini8)
#define sic_c_infini CFC_EXPORT_NAME( sic_c_infini)
#define sic_getvm4 CFC_EXPORT_NAME( sic_getvm4)
#define sic_getvm8 CFC_EXPORT_NAME( sic_getvm8)
#define sic_getvm4c CFC_EXPORT_NAME( sic_getvm4c)
#define sic_getvm8c CFC_EXPORT_NAME( sic_getvm8c)
#define free_vm4 CFC_EXPORT_NAME( free_vm4)
#define free_vm8 CFC_EXPORT_NAME( free_vm8)
#define gag_pointer CFC_EXPORT_NAME( gag_pointer)
#define bytpnt CFC_EXPORT_NAME( bytpnt)
#define set_dir CFC_EXPORT_NAME( set_dir)
#define locstr CFC_EXPORT_NAME( locstr)
#define locwrd CFC_EXPORT_NAME( locwrd)
#define executable_extension CFC_EXPORT_NAME( executable_extension)
#define sic_c_assert CFC_EXPORT_NAME( sic_c_assert)
#define gag_mkpath CFC_EXPORT_NAME( gag_mkpath)
#define gag_resources CFC_EXPORT_NAME( gag_resources)
#define gag_setlocale CFC_EXPORT_NAME( gag_setlocale)
#define gag_printlocale CFC_EXPORT_NAME( gag_printlocale)


/*****************************************************************************
 *                           Function prototypes                             *
 *****************************************************************************/

void CFC_API gag_filnew( int *fd, CFC_FString file CFC_DECLARE_STRING_LENGTH( file));
void CFC_API gag_filopen( int *fd, CFC_FString file CFC_DECLARE_STRING_LENGTH( file));
void CFC_API gag_fillook( int *fd, CFC_FString file CFC_DECLARE_STRING_LENGTH( file));
int CFC_API gag_filtest( CFC_FString file CFC_DECLARE_STRING_LENGTH( file));
void CFC_API gag_filclose( int *fd);
int CFC_API gag_filread( int *fd, CFC_FString buf, int *len);
int CFC_API gag_filwrite( int *fd, CFC_FString buf, int *len);
int CFC_API gag_filseek( int *fd, int *offset);
void CFC_API gag_crash( );
int CFC_API gag_system( CFC_FString string CFC_DECLARE_STRING_LENGTH( string));
void CFC_API gag_debug_system( int *sw);
void CFC_API gag_filrm( CFC_FString file CFC_DECLARE_STRING_LENGTH( file));
int CFC_API gag_rmdir( CFC_FString dirname CFC_DECLARE_STRING_LENGTH( dirname));
int CFC_API gag_isdir( CFC_FString dirname CFC_DECLARE_STRING_LENGTH( dirname));
int gag_directory_remove(char *dirname);
int CFC_API gag_filcopy( CFC_FString fromfile, CFC_FString tofile
                        CFC_DECLARE_STRING_LENGTH( fromfile)
                        CFC_DECLARE_STRING_LENGTH( tofile));
int filcopy_or_filappend( char *tmp1, char *tmp2, const char *myflag);
int CFC_API gag_filmove( CFC_FString fromfile, CFC_FString tofile
                        CFC_DECLARE_STRING_LENGTH( fromfile)
                        CFC_DECLARE_STRING_LENGTH( tofile));
int CFC_API gag_filappend( CFC_FString fromfile, CFC_FString tofile
                          CFC_DECLARE_STRING_LENGTH( fromfile)
                          CFC_DECLARE_STRING_LENGTH( tofile));
int CFC_API gag_directory_num_c( CFC_FString dirname, int *count
                                 CFC_DECLARE_STRING_LENGTH( dirname));
void CFC_API sic_getenv( CFC_FString name, CFC_FString trans
                        CFC_DECLARE_STRING_LENGTH( name)
                        CFC_DECLARE_STRING_LENGTH( trans));
int CFC_API sic_setenv( CFC_FString name, CFC_FString trans
                        CFC_DECLARE_STRING_LENGTH( name)
                        CFC_DECLARE_STRING_LENGTH( trans));
void CFC_API sic_resolve_env( CFC_FString src, CFC_FString dst
                        CFC_DECLARE_STRING_LENGTH( src)
                        CFC_DECLARE_STRING_LENGTH( dst));
int CFC_API gag_inquire( const CFC_FString infile, const int *length);
void CFC_API gag_ramsize( int64_t *size);
void CFC_API gag_cachesize( int64_t *l1size,
                            int64_t *l2size,
                            int64_t *l3size,
                            int64_t *l4size);
int CFC_API gag_filsize( const CFC_FString path, int64_t *size
                         CFC_DECLARE_STRING_LENGTH( path));
int CFC_API gag_mdate( const CFC_FString path, int *date
                        CFC_DECLARE_STRING_LENGTH( path));
int CFC_API gag_mtime_c( const CFC_FString path, int *sec, int *nsec
                         CFC_DECLARE_STRING_LENGTH( path));
int CFC_API gag_time_c( int *sec, int *nsec);
void CFC_API sic_c_datetime( CFC_FString returned_asciitime
                             CFC_DECLARE_STRING_LENGTH( returned_asciitime));
void CFC_API sic_c_datetime_from_raw( int *rawtime, CFC_FString returned_asciitime
                                      CFC_DECLARE_STRING_LENGTH( returned_asciitime) );
void CFC_API sic_c_isodatetime( CFC_FString returned_asciitime
                                CFC_DECLARE_STRING_LENGTH( returned_asciitime));
void CFC_API sic_c_time( CFC_FString returned_time
                         CFC_DECLARE_STRING_LENGTH( returned_time));
void CFC_API sic_c_date( CFC_FString returned_date
                         CFC_DECLARE_STRING_LENGTH( returned_date));
void CFC_API gag_c_cputime( double time[5]);
void CFC_API sic_wait( float *time);
int CFC_API sic_isatty();
int CFC_API sic_ttyncol();
int CFC_API sic_ttynlin();
int CFC_API finite( double x);
int CFC_API sic_fini4( float *x);
int CFC_API sic_fini8( double *x);
void CFC_API sic_c_infini( double *anum, CFC_FString string, int *stringLength
                           CFC_DECLARE_STRING_LENGTH( string));
int CFC_API sic_getvm4( int32_t *nwords, fortran_pointer *addr);
int CFC_API sic_getvm8( int64_t *nwords, fortran_pointer *addr);
int CFC_API sic_getvm4c( int32_t *nwords, fortran_pointer *addr);
int CFC_API sic_getvm8c( int64_t *nwords, fortran_pointer *addr);
void CFC_API free_vm4( int32_t *nwords32, fortran_pointer *addr);
void CFC_API free_vm8( int64_t *nwords64, fortran_pointer *addr);
fortran_pointer CFC_API gag_pointer( fortran_pointer *adr, fortran_pointer *mem);
fortran_pointer CFC_API bytpnt( fortran_pointer *adr, fortran_pointer *mem);
int CFC_API set_dir( CFC_FString dir
                     CFC_DECLARE_STRING_LENGTH( dir));
char *CFC_API locstr( CFC_FString string);
int *CFC_API locwrd( int *word);
void CFC_API executable_extension( CFC_FString ext
                                   CFC_DECLARE_STRING_LENGTH( ext));
void CFC_API sic_c_assert( long *condition, const CFC_FString message
                        CFC_DECLARE_STRING_LENGTH( message));
void CFC_API gag_mkpath( const CFC_FString path, int *error
                           CFC_DECLARE_STRING_LENGTH( path));
void CFC_API gag_resources( );
int CFC_API gag_setlocale(CFC_FString fchain CFC_DECLARE_STRING_LENGTH(fchain));
void CFC_API gag_printlocale( );

#endif /* _SYSC_H_ */
