
/**
 * @file
 * General messaging support file, C version.
 *
 * These routines call the GAG_MESSAGE Fortran routine and implies links to
 * Fortran compiler libraries. This is a problem for main programs written in
 * C and compiled/linked by the C compiler (e.g. libexec programs).
 *
 */

#include "gmessage-c.h"

/*****************************************************************************
 *                           Dependencies                                    *
 *****************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include "cfc.h"


#define gmessage_write CFC_EXPORT_NAME(gmessage_write)
void gmessage_write(int *,int *, char *str1, char *str2
     CFC_DECLARE_STRING_LENGTH(str1) CFC_DECLARE_STRING_LENGTH(str2));

/**
 * Send a message for printing to Fortran GAG_MESSAGE.
 *
 * @param[in] seve is the severity kind (integer).
 * @param[in] c_rname is the calling routine name which appears in prefix.
 * @param[in] c_message in the message.
 * @return nothing.
 */
void gmessage_c_write( int id, enum severities seve, const char *c_rname, const char *c_format, va_list l)
{
    char c_message[GAG_MAX_MESSAGE_LENGTH];

    CFC_DECLARE_LOCAL_STRING(f_rname);
    CFC_DECLARE_LOCAL_STRING(f_message);

    /* Write in c_message using input c_format and extra args. */
    vsprintf( c_message, c_format, l);

    /* Make Fortran strings pointing to C ones. */
    CFC_STRING_VALUE(f_rname) = (char *)c_rname;
    CFC_STRING_LENGTH(f_rname) = strlen(c_rname);

    CFC_STRING_VALUE(f_message) = c_message;
    CFC_STRING_LENGTH(f_message) = strlen(c_message);

    gmessage_write( &id, (int*)&seve, f_rname, f_message
        CFC_PASS_STRING_LENGTH( f_rname) CFC_PASS_STRING_LENGTH( f_message)
        );

}
