subroutine gag_cputime_init(time)
  use gsys_interfaces, except_this=>gag_cputime_init
  use gsys_types
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(cputime_t), intent(inout) :: time  !
  ! Local
  real(kind=8) :: t0(5)
  !
  call gag_c_cputime(t0)
  !
  time%refe%elapsed = t0(1)
  time%refe%user    = t0(2)+t0(4)
  time%refe%system  = t0(3)+t0(5)
  !
  time%curr%elapsed = 0.d0
  time%curr%user    = 0.d0
  time%curr%system  = 0.d0
  !
  time%diff%elapsed = 0.d0
  time%diff%user    = 0.d0
  time%diff%system  = 0.d0
  !
end subroutine gag_cputime_init
!
subroutine gag_cputime_get(time)
  use gsys_interfaces, except_this=>gag_cputime_get
  use gsys_types
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(cputime_t), intent(inout) :: time  !
  ! Local
  real(kind=8) :: t1(5)
  !
  call gag_c_cputime(t1)
  !
  time%diff%elapsed = (t1(1)       - time%refe%elapsed) - time%curr%elapsed
  time%diff%user    = (t1(2)+t1(4) - time%refe%user)    - time%curr%user
  time%diff%system  = (t1(3)+t1(5) - time%refe%system)  - time%curr%system
  !
  time%curr%elapsed = t1(1)       - time%refe%elapsed
  time%curr%user    = t1(2)+t1(4) - time%refe%user
  time%curr%system  = t1(3)+t1(5) - time%refe%system
  !
end subroutine gag_cputime_get
!
subroutine gtime_init4(time,nloop,error)
  use gsys_types
  use gsys_interfaces, except_this=>gtime_init4
  !---------------------------------------------------------------------
  ! @ public-generic gtime_init
  ! Init the timing structure (INTEGER*4 nloop)
  !---------------------------------------------------------------------
  type(time_t),    intent(out)   :: time
  integer(kind=4), intent(in)    :: nloop
  logical,         intent(inout) :: error
  !
  call gtime_init8(time,int(nloop,kind=8),error)
  !
end subroutine gtime_init4
!
subroutine gtime_init8(time,nloop,error)
  use gbl_message
  use gsys_types
  use gsys_interfaces, except_this=>gtime_init8
  !---------------------------------------------------------------------
  ! @ public-generic gtime_init
  ! Init the timing structure (INTEGER*8 nloop)
  !---------------------------------------------------------------------
  type(time_t),    intent(out)   :: time
  integer(kind=8), intent(in)    :: nloop
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TIME/INIT'
  !
  call gsys_message(seve%t,rname,'Welcome')
  !
  ! Sanity check. NB: it is ok to have 0 loops.
  if (nloop.lt.0) then
    call gsys_message(seve%e,rname,'Less than 0 element in loop')
    error = .true.
    return
  endif
  !
  call gag_cputime_init(time%cputime)
  !
  time%iloop = 0
  if (nloop.gt.10) then     ! nloop > 10
    time%nloop = nloop
    time%cleverstart = .true.
    time%interval = 2.d0
    time%nbin = 10
  elseif (nloop.gt.0) then  ! 0 < nloop < 10
    time%nloop = nloop
    time%cleverstart = .false.
    time%interval = 1.d0
    time%nbin = nloop
  else                      ! nloop == 0
    time%nloop = -1  ! Avoid 0-divisions
    time%cleverstart = .false.
    time%interval = 1.d0
    time%nbin = -1  ! Avoid 0-divisions
  endif
  time%interval0 = max(1d0,real(time%nloop,kind=8)/time%nbin)
  time%maxtime = 10.d0
  !
end subroutine gtime_init8
!
subroutine gtime_current(time)
  use gbl_message
  use gsys_types
  use gsys_interfaces, except_this=>gtime_current
  !---------------------------------------------------------------------
  ! @ public
  ! Display the timing statistics and projections
  !---------------------------------------------------------------------
  type(time_t), intent(inout) :: time
  ! Local
  character(len=*), parameter :: rname='TIME/CURRENT'
  real(kind=8) :: elapsed,frac,rate
  character(len=20) :: crate
  character(len=message_length) :: mess
  !
  time%iloop = time%iloop+1
  !
  ! Warning: time%interval must be different from 0 or there will be an error
  !          at least under ifort => the max(1,int(time%nloop/time%nbin)) below)
  ! if (mod(time%iloop,time%interval).ne.0)                     return  ! Integer interval
  if (mod(real(time%iloop,kind=8)+.5d0,time%interval).ge.1.d0)  return  ! Float interval
  !
  call gag_cputime_get(time%cputime)
  elapsed = time%cputime%curr%elapsed  ! Since the previous gtime_init
  !
  if (time%cleverstart) then
    if (elapsed.lt.time%maxtime) then
      ! Quite fast for the first interval, try many more loops
      time%interval = time%interval*6d0
      if (time%interval.gt.time%interval0) then
        time%interval = time%interval0
        time%cleverstart = .false.
      endif
      return
    else
      if ((elapsed*real(time%nloop,kind=8)/real(time%iloop,kind=8)).ge.  &
          time%maxtime) then
        ! Projection gives a time larger than the desired limit => Verbose
        time%interval = 2d0*time%interval
        if (time%interval.gt.time%interval0) then
          time%interval = time%interval0
          time%cleverstart = .false.
        endif
      else
        ! Projection gives a time lower than the desired limit => Switch to
        ! standard mode, not verbose here
        time%interval = time%interval0
        time%cleverstart = .false.
        return
      endif
    endif
    !
  elseif (elapsed.lt.((time%maxtime/time%nbin)*time%iloop/time%interval)) then
    ! Standard mode: elapsed time is lower than the desired threshold for this
    ! bin => computation will be fast, nothing to print
    return
  endif
  !
  if (elapsed.lt.time%maxtime)  return  ! Not verbose before this threshold,
                                        ! assuming the information is useless
                                        ! and approximative
  !
  rate = real(time%iloop,kind=8)/elapsed  ! number/second
  if (rate.lt.1.d0/60.d0) then  ! Less than 1/minute
    ! Return float rate in hours
    write(crate,'(F0.1,A)')  rate*3.6d3,'/hr'
  elseif (rate.lt.1.d0) then    ! Less than 1/second
    ! Return float rate in minutes
    write(crate,'(F0.1,A)')  rate*60d0,'/min'
  elseif (rate.lt.10.d0) then   ! Less than 10/second
    ! Return float rate in seconds
    write(crate,'(F0.1,A)')  rate,'/sec'
  else                          ! 10/second or better
    ! Return integer rate in seconds
    write(crate,'(A,A)')  trim(format_num_r8(rate)),'/sec'
  endif
  !
  frac = real(time%iloop,kind=8)/real(time%nloop,kind=8)
  if (frac.lt.1.0d-2) then  ! Less than 1%
    write(mess,100)  &
      1.d2*frac,                               &  ! Percentage
      trim(format_num_i8(time%iloop)),         &  ! Loop number
      trim(format_time(elapsed)),              &  ! Time elapsed
      trim(crate),                             &  ! Rate
      trim(format_time(elapsed/frac)),         &  ! Projection
      trim(format_time(elapsed/frac-elapsed))     ! Time remaining
   else
    write(mess,101)  &
      nint(1.d2*frac),                         &  ! Percentage
      trim(format_num_i8(time%iloop)),         &  ! Loop number
      trim(format_time(elapsed)),              &  ! Time elapsed
      trim(crate),                             &  ! Rate
      trim(format_time(elapsed/frac)),         &  ! Projection
      trim(format_time(elapsed/frac-elapsed))     ! Time remaining
   endif
  call gsys_message(seve%r,rname,mess)
  !
100 format('Done ',F3.1,'% (',A,') in ',A,' (rate: ',A,'). Total: ',A,', ',A,' to go')
101 format('Done ',I3,  '% (',A,') in ',A,' (rate: ',A,'). Total: ',A,', ',A,' to go')
  !
contains
  !
  function format_num_i8(num)
    !-------------------------------------------------------------------
    ! Format a number to a string in k, M, G
    !-------------------------------------------------------------------
    character(len=6) :: format_num_i8
    integer(kind=8), intent(in) :: num
    format_num_i8 = format_num_r8(real(num,kind=8))
  end function format_num_i8
  !
  function format_num_r8(num)
    !-------------------------------------------------------------------
    ! Format a number to a string in k, M, G
    !-------------------------------------------------------------------
    character(len=6) :: format_num_r8
    real(kind=8), intent(in) :: num
    !
    real(kind=8) :: val
    integer(kind=4) :: it
    integer(kind=4), parameter :: nt=5
    character(len=1), parameter :: what(nt) = (/ ' ','k','M','G','T'  /)
    real(kind=8), parameter :: factor(nt)   = (/ 1d0,1d3,1d6,1d9,1d12 /)
    !
    it = 1
    do while (it.le.nt)
      val = num/factor(it)
      if (nint(val,kind=8).lt.10000) then
        write(format_num_r8,'(I0,A)')  nint(val,kind=8),what(it)
        return
      else
        it = it+1
      endif
    enddo
    write(format_num_r8,'(A,A)')  '>9999',what(nt)
  end function format_num_r8
  !
  function format_time(sec)
    !-------------------------------------------------------------------
    ! Format a time [sec] to a string in sec, min, or hours...
    !-------------------------------------------------------------------
    character(len=12) :: format_time
    real(kind=8), intent(in) :: sec
    !
    real(kind=8) :: val
    integer(kind=4) :: it
    integer(kind=4), parameter :: nt=5
    character(len=4), parameter :: what(nt) = (/ ' sec',' min',' hr ',' day',' wk '  /)
    real(kind=8), parameter :: factor(nt)   = (/    1d0,   6d1, 3.6d3,8.64d4,6.048d5 /)
    !
    it = 1
    do while (it.le.nt)
      val = sec/factor(it)
      if (nint(val*10.d0).lt.100) then
        write(format_time,'(F3.1,A)')  val,what(it)  ! Floating point X.Y
        return
      elseif (nint(val).lt.100) then
        write(format_time,'(I0,A)')  nint(val),what(it)  ! Integer XY
        return
      else
        it = it+1
      endif
    enddo
    write(format_time,'(A,A)')  '>100',what(nt)
  end function format_time
end subroutine gtime_current
