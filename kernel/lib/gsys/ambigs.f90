subroutine sic_ambigs(prog,line,comm,ncom,vocab,nv,error)
  use gsys_interfaces, except_this=>sic_ambigs
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !  Resolves command abbreviations and check for ambiguities
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: prog       ! Message prefix
  character(len=*), intent(in)    :: line       ! Abbreviation to be analysed
  character(len=*), intent(out)   :: comm       ! Command found
  integer(kind=4),  intent(out)   :: ncom       ! Command number
  integer(kind=4),  intent(in)    :: nv         ! List of commands
  character(len=*), intent(in)    :: vocab(nv)  ! Number of command
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=4) :: lpar,mpar
  character(len=80) :: uline
  character(len=message_length) :: mess
  logical :: verbose
  !
  error = .false.
  verbose = len_trim(prog).gt.0
  lpar = lenc(line)
  mpar = len (vocab(1))
  ncom = 0
  if (lpar.gt.mpar) then
    if (verbose) then
      write(mess,103) line(1:lpar)
      call gsys_message(seve%w,prog,mess)
    endif
    error = .true.
    return
  endif
  uline = line(1:lpar)
  ! call sic_upper(uline(1:lpar))
  !
  ! List vocabulary if argument is "?" (always verbose)
  if (uline(1:lpar).eq.'?') then
    call sic_ambigs_list(prog,seve%i,'Choices are:',vocab)
    error = .true.  ! So that the calling subroutine does not continue
    return
  endif
  !
  ! Find the command
  call sic_ambigs_sub(prog,uline,comm,ncom,vocab,nv,error)
  if (error) return            ! There was an ambiguity
  !
  ! Command not found
  if (ncom.eq.0) then
    if (verbose) then
      write(mess,103) uline(1:lpar)
      call gsys_message(seve%e,prog,mess)
    endif
    error = .true.
    return
  endif
  !
  ! Return the command name
  comm = vocab(ncom)
  !
103 format('Unknown keyword ',a)
end subroutine sic_ambigs
!
subroutine sic_ambigs_sub(prog,line,comm,ncom,vocab,nv,error)
  use gbl_message
  use gsys_interfaces, except_this=>sic_ambigs_sub
  !---------------------------------------------------------------------
  ! @ public
  ! Still to be renamed
  ! Same as 'sic_ambigs' but:
  !  - do not recognize "?" which can be used to list all the
  !    possible values,
  !  - do not raise an error if no known keyword is found.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: prog       ! Message prefix
  character(len=*), intent(in)    :: line       ! Abbreviation to be analysed
  character(len=*), intent(out)   :: comm       ! Command found
  integer(kind=4),  intent(out)   :: ncom       ! Command number
  integer(kind=4),  intent(in)    :: nv         ! List of commands
  character(len=*), intent(in)    :: vocab(nv)  ! Number of command
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=4) :: lpar,mpar,j,nmatch,iambigs(nv)
  logical :: match
  !
  error = .false.
  match = .false.
  lpar = lenc(line)
  mpar = len(vocab(1))
  comm = ''
  ncom = 0
  nmatch = 0
  !
  ! If trimmed argument is longer than non-trimmed keywords, there can be
  ! no match.
  if (lpar.gt.mpar)  return
  !
  ! Find the command
  do j = 1,nv
    if (line(1:lpar).ne.vocab(j)(1:lpar))  cycle  ! Not a match
    !
    if (.not.match) then
      ! First match
      comm = vocab(j)
      match = .true.
      ncom = j
      nmatch = 1
    else
      ! More than 1: ambiguous
      error = .true.
      nmatch = nmatch + 1
    endif
    iambigs(nmatch) = j
    !
  enddo
  !
  if (error) then
    ! Ambiguity. Display ambiguity list
    call sic_ambigs_list(prog,seve%e,'Ambiguous keyword, choices are:',vocab,iambigs(1:nmatch))
    return
  endif
  !
end subroutine sic_ambigs_sub
!
subroutine sic_ambigs_list(caller,sever,message,vocab,ikeys,first,last)
  use gbl_message
  use gsys_interfaces, except_this=>sic_ambigs_list
  !---------------------------------------------------------------------
  ! @ public
  !  Display an ambiguity list
  !---------------------------------------------------------------------
  character(len=*), intent(in)                   :: caller    ! Calling routine name
  integer(kind=4),  intent(in)                   :: sever     ! Severity of header message
  character(len=*), intent(in)                   :: message   ! Header message before list
  character(len=*), intent(in)                   :: vocab(:)  ! List of keywords
  integer(kind=4),  intent(in), optional, target :: ikeys(:)  ! Restrict to these keys?
  integer(kind=4),  intent(in), optional         :: first     ! Truncate start of string?
  integer(kind=4),  intent(in), optional         :: last      ! Truncate or force to this length?
  ! Local
  character(len=message_length) :: chain
  integer(kind=4) :: ic,j,mpar,number,nv,fi,la
  integer(kind=4), pointer :: jkeys(:)
  !
  if (present(ikeys)) then
    nv = size(ikeys)
    jkeys => ikeys
  else
    nv = size(vocab)
    allocate(jkeys(nv))
    do j=1,nv
      jkeys(j) = j
    enddo
  endif
  !
  if (present(first)) then
    fi = first
  else
    fi = 1
  endif
  !
  if (present(last)) then
    la = min(last,len(vocab(1)))
    mpar = last-fi+3
  else
    la = len(vocab(1))
    ! Compute 'mpar', the size of each column. Set to longest trimmed chain + 2
    ! spacing characters.
    mpar = 3
    do j=1,nv
      mpar = max(mpar,len_trim(vocab(jkeys(j)))-fi+3)
    enddo
  endif
  !
  call gsys_message(sever,caller,message)
  !
  ! Number of columns per line. Do not forget the 5 leading spaces
  number = (sic_ttyncol()-5)/mpar
  chain(1:5) = ' '
  ic = 6
  do j=1,nv
    chain(ic:ic+mpar-1) = vocab(jkeys(j))(fi:la)
    ic = ic+mpar
    if (mod(j,number).eq.0) then
      call gsys_message(seve%r,caller,chain(1:ic-1))
      ic = 6
    endif
  enddo
  if (ic.gt.6)  &
    call gsys_message(seve%r,caller,chain(1:ic-1))
  !
  if (.not.present(ikeys))  deallocate(jkeys)
  !
end subroutine sic_ambigs_list
