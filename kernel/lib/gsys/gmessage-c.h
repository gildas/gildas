
#ifndef _GMESSAGE_C_H_
#define _GMESSAGE_C_H_

/**
 * @file
 * @see gmessage-c.c
 */

#include "cfc.h"
#include <stdarg.h>

/*****************************************************************************
 *                          Macros & Definitions                             *
 *****************************************************************************/

#define GAG_MAX_MESSAGE_LENGTH 4096

enum severities { seve_f=1, seve_e, seve_w,
                  seve_r,   seve_i, seve_d,
                  seve_t,   seve_c, seve_u };

/*****************************************************************************
 *                           Function prototypes                             *
 *****************************************************************************/

#define GMESSAGE_C_DECLARE(package)                                           \
void name2(package,_c_message)( enum severities kind, const char *procname,   \
                                const char *message, ...)

#define GMESSAGE_C_WRITE gmessage_c_write

void GMESSAGE_C_WRITE( int id, enum severities seve, const char *c_rname, const char *c_format, va_list l);

#define GMESSAGE_C_DEFINE(package)                                            \
/*                                                                            \
 * Routines to manage C messages                                              \
 */                                                                           \
                                                                              \
static int name2(package,_c_message_id) = 0; /* 0 for gpack_global_id */      \
                                                                              \
/*                                                                            \
 * @private                                                                   \
 * Alter library id into input id. Should be called by the library            \
 * which wants to share its id with the current one.                          \
 */                                                                           \
void CFC_API CFC_EXPORT_NAME(name2(package,_c_message_set_id))( int* id)      \
{                                                                             \
    name2(package,_c_message_id) = *id;                                       \
}                                                                             \
                                                                              \
/*                                                                            \
 * @private                                                                   \
 * Messaging facility for the current library. Calls the low-level            \
 * messaging routine with its own identifier.                                 \
 */                                                                           \
void name2(package,_c_message)( enum severities kind, const char *procname,   \
                                const char *message, ...)                     \
{                                                                             \
    va_list l;                                                                \
                                                                              \
    va_start( l, message);                                                    \
    GMESSAGE_C_WRITE( name2(package,_c_message_id), kind, procname, message,  \
                      l);                                                     \
    va_end( l);                                                               \
}

#endif /* _GMESSAGE_C_H_ */

