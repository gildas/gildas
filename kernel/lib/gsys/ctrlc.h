
#ifndef _SIC_CTRLC_H_
#define _SIC_CTRLC_H_

/**
 * @file
 * @see ctrlc-c.c and ctrlc-f.f90
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "cfc.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

#define trap_ctrlc CFC_EXPORT_NAME( trap_ctrlc)
#define no_ctrlc   CFC_EXPORT_NAME( no_ctrlc)
#define set_ctrlc  CFC_EXPORT_NAME( set_ctrlc)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

/* C functions callable from Fortran */
void *CFC_API trap_ctrlc( void);
void *CFC_API no_ctrlc( void);

/* Fortran functions callable from C */
void CFC_API set_ctrlc( void);

#endif /* _SIC_CTRLC_H_ */

