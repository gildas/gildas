
#include <windows.h>

#include <stdio.h>
#ifndef MINGW
#define fileno _fileno
#define fdopen _fdopen
#define unlink _unlink
#define lseek _lseek
#define STDIN_FILENO (_fileno( stdin))
#endif /* MINGW */

#include <io.h>
#define open _open
#define close _close
#define read _read
#define write _write
#define isatty _isatty

#include <direct.h>
#define getcwd _getcwd
#define chdir _chdir

#include <string.h>
#define stricmp _stricmp
#define strcasecmp _stricmp
#define strncasecmp _strnicmp
