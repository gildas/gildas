!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GSYS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gsys_message_private
  use gpack_def
  !---------------------------------------------------------------------
  ! @private
  ! Identifier used for message identification
  !---------------------------------------------------------------------
  integer(kind=4) :: gsys_message_id = gpack_global_id  ! Default@startup
  !
end module gsys_message_private
!
subroutine gsys_message_set_id(id)
  use gsys_interfaces, except_this=>gsys_message_set_id
  use gsys_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gsys_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gsys_message_id
  call gsys_message(seve%d,'gsys_message_set_id',mess)
  !
end subroutine gsys_message_set_id
!
subroutine gsys_message(mkind,procname,message)
  use gsys_interfaces, except_this=>gsys_message
  use gsys_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gsys_message_id,mkind,procname,message)
  !
end subroutine gsys_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
