
#include "sic_util.h"

#include "gtlgtr.h"

char *sic_s_get_translation( const char *name)
{
    static char s_translation[SIC_MAX_TRANSLATION_LENGTH];
    int name_length;
    int max_length = SIC_MAX_TRANSLATION_LENGTH;

    name_length = (int)strlen( name);
    sic_ctrans( name, &name_length, s_translation, &max_length);

    return s_translation;
}

#define sic_parsef CFC_EXPORT_NAME(sic_parsef)
void sic_parsef(char *str1, char *str2, char *str3, char *str4
     CFC_DECLARE_STRING_LENGTH(str1) CFC_DECLARE_STRING_LENGTH(str2)
     CFC_DECLARE_STRING_LENGTH(str3) CFC_DECLARE_STRING_LENGTH(str4));

char *sic_s_get_logical_path( const char *path)
{
    static char s_path[SIC_MAX_TRANSLATION_LENGTH];
    static char s_name[SIC_MAX_TRANSLATION_LENGTH];
    static char s_file[SIC_MAX_TRANSLATION_LENGTH];
    CFC_DECLARE_LOCAL_STRING(name);
    CFC_DECLARE_LOCAL_STRING(file);
    CFC_DECLARE_LOCAL_STRING(empty);

    CFC_STRING_VALUE(name) = s_name;
    CFC_STRING_LENGTH(name) = SIC_MAX_TRANSLATION_LENGTH;
    CFC_STRING_VALUE(file) = s_file;
    CFC_STRING_LENGTH(file) = SIC_MAX_TRANSLATION_LENGTH;
    CFC_STRING_VALUE(empty) = "";
    CFC_STRING_LENGTH(empty) = 0;
    CFC_c2f_strcpy( name, CFC_STRING_LENGTH( name), (char *)path);
    sic_parsef( name, file, empty, empty
        CFC_PASS_STRING_LENGTH( name)
        CFC_PASS_STRING_LENGTH( file)
        CFC_PASS_STRING_LENGTH( empty)
        CFC_PASS_STRING_LENGTH( empty));
    CFC_f2c_strcpy( s_path, file, CFC_STRING_LENGTH_MIN( file, SIC_MAX_TRANSLATION_LENGTH));

    return s_path;
}
