subroutine gio_hd32_to_gildas_v2(hd32,v2)
  use gio_headers
  use gio_uv
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GIO/GDF
  !     Transfer a 32-bit "old" Data File Header to
  !     the "new" type(gildas)
  !---------------------------------------------------------------------
  type(gildas_header_v1), intent(in) :: hd32
  type(gildas), intent(inout) :: v2   ! Must be (inout) as initiliazed before
  !
  integer :: uaxi
  integer, allocatable :: column_codes(:)
  integer :: i, ier
  !
  call bytoch (hd32%ijtyp, v2%char%type, 12)
  v2%gil%form = hd32%form
  v2%gil%ndb = hd32%ndb
  v2%gil%version_gdf = code_version_gdf_v10 ! Data is from "old" Gildas file..
  if (v2%char%type.eq.'GILDAS_IMAGE') then
    v2%gil%type_gdf = code_gdf_image
  elseif  (v2%char%type.eq.'GILDAS_UVDAT'.or.v2%char%type.eq.'GILDAS_UVSOR') then
    v2%gil%type_gdf = code_gdf_uvold
      ! Now that the Version is defined, reset GTYPE
    v2%char%type = 'GILDAS_UVFIL'
      ! Locate axis order
  elseif  (v2%char%type.eq.'GILDAS_UVFIL') then
    v2%gil%type_gdf = code_gdf_uvt
  else
    !!call gio_message(seve%w,'GIO_RIH','Unsupported image type '//v2%char%type)
    print *,'GIO_RIH','Unsupported image type '//v2%char%type
    v2%gil%type_gdf = code_gdf_image
  endif
  !
  ! GENERAL SECTION
  v2%gil%dim_words = def_dim_words !! 2 + 2*gdf_maxdims
  v2%gil%ndim = hd32%ndim
  v2%gil%dim = 1   ! Make trailing dimensions = 1
  v2%gil%dim(1:4) = hd32%dim(1:4)
  call r8tor8(hd32%ref1, v2%gil%convert, 12)
     ! BLANKING
  v2%gil%blan_words = hd32%blan
  v2%gil%bval = hd32%bval
  v2%gil%eval = hd32%eval
     ! EXTREMA
  v2%gil%extr_words = hd32%extr
  if (v2%gil%extr_words.ne.0) then
    v2%gil%rmin = hd32%rmin
    v2%gil%rmax = hd32%rmax
    v2%gil%minloc = 0
    v2%gil%maxloc = 0
    v2%gil%minloc(1:4) = (/hd32%min1,hd32%min2,hd32%min3,hd32%min4/)
    v2%gil%maxloc(1:4) =(/hd32%max1,hd32%max2,hd32%max3,hd32%max4/)
    v2%gil%extr_words = def_extr_words
    !!Print *,'to v2%gil extr '
  endif
    ! DESCRIPTION
  v2%gil%desc_words = hd32%desc
  call bytoch (hd32%ijuni, v2%char%unit, 12)
  do i=1,4 !! v2%gil%ndim
    if (any(hd32%ijcod(:,i).ne.0)) then
      call bytoch (hd32%ijcod(1,i), v2%char%code(i), 12)
    endif
  enddo
  call bytoch (hd32%ijsys, v2%char%syst, 12)
     ! POSITION
  v2%gil%posi_words = hd32%posi
  call bytoch (hd32%ijsou, v2%char%name, 12)
  v2%gil%ra = hd32%ra
  v2%gil%dec = hd32%dec
  v2%gil%lii = hd32%lii
  v2%gil%bii = hd32%bii
  v2%gil%epoc = hd32%epoc
     ! PROJECTION
  v2%gil%proj_words = hd32%proj
  v2%gil%ptyp = hd32%ptyp
  v2%gil%a0 = hd32%a0
  v2%gil%d0 = hd32%d0
  v2%gil%pang = hd32%pang
  v2%gil%xaxi = hd32%xaxi
  v2%gil%yaxi = hd32%yaxi
     ! SPECTROSCOPY
  v2%gil%spec_words = hd32%spec
  call bytoch (hd32%ijlin, v2%char%line, 12)
  v2%gil%fres = hd32%fres
  v2%gil%fima = hd32%fima
  v2%gil%freq = hd32%freq
  v2%gil%vres = hd32%vres
  v2%gil%voff = hd32%voff
  v2%gil%faxi = hd32%faxi
  v2%gil%dopp = 0
  v2%gil%vtyp = vel_lsr
  call gio_message(seve%w,'GDF','UNKNOWN Velocity type defaulted to LSR')
     ! RESOLUTION
  v2%gil%reso_words = hd32%reso
  v2%gil%majo = hd32%majo
  v2%gil%mino = hd32%mino
  v2%gil%posa = hd32%posa
     ! NOISE
  v2%gil%nois_words = hd32%sigm
  v2%gil%noise = hd32%noise
  v2%gil%rms = hd32%rms
     ! ASTROMETRY
  v2%gil%astr_words = hd32%prop
  v2%gil%mura = hd32%mura
  v2%gil%mudec = hd32%mudec
  v2%gil%parallax = hd32%parallax
  !
  !!Print *,'gio_hd32_to_gildas_v2 ',v2%char%type
  if  (v2%char%type.eq.'GILDAS_UVFIL') then
    !!print *,'HD32 --> V2, doing UVDATA '
    if (v2%gil%proj_words.eq.0) then
      v2%gil%ptyp = p_azimuthal             ! Azimuthal (Sin)
      v2%gil%a0 = v2%gil%ra
      v2%gil%d0 = v2%gil%dec
      call gio_message(seve%w,'GDF', &
           & 'No projection information, Assumed Azimuthal around RA and DEC')
    endif
    !
    if (v2%char%code(1).eq.'RANDOM') then
      uaxi = 2
      v2%gil%type_gdf = code_gdf_tuv
    else
      uaxi = 1
      v2%gil%type_gdf = code_gdf_uvt
    endif
    !
    v2%gil%nchan = (v2%gil%dim(uaxi)-7)/3
    v2%gil%nvisi = v2%gil%dim(3-uaxi)
    v2%gil%nstokes = 1
    v2%gil%natom = 3
    !
    ! Not in data...
    v2%gil%basemin = 0.
    v2%gil%basemax = 0.
    !
    v2%gil%fcol = 8
    v2%gil%lcol = 7+3*v2%gil%nchan
    !
    v2%gil%nlead = 7
    v2%gil%ntrail = v2%gil%dim(uaxi)-7-3*v2%gil%nchan
    !
    ! Set the column codes
    allocate (column_codes(v2%gil%nlead+v2%gil%ntrail),stat=ier)
    column_codes(1:7) = (/code_uvt_u,code_uvt_v,code_uvt_scan, &
      & code_uvt_date,code_uvt_time,code_uvt_anti,code_uvt_antj/)
    if (v2%gil%ntrail.ne.0) then
      column_codes(v2%gil%nlead+1:v2%gil%nlead+v2%gil%ntrail) = (/code_uvt_xoff,code_uvt_yoff/)
    endif
    !
    ! Set the back pointers
    v2%gil%column_pointer = code_null
    do i = 1, v2%gil%nlead
      v2%gil%column_pointer(column_codes(i)) = i
      v2%gil%column_size(column_codes(i)) = 1
    enddo
    do i = 1, v2%gil%ntrail
      v2%gil%column_pointer(column_codes(v2%gil%nlead+i)) = i+v2%gil%nlead+3*v2%gil%nchan*v2%gil%nstokes
      v2%gil%column_size(column_codes(v2%gil%nlead+i)) = 1
    enddo
    !
    v2%gil%uvda_words = 18+2*(v2%gil%nlead+v2%gil%ntrail) !
    v2%gil%atoms = [1,2,3,0]
    !
    ! Compute the Doppler factor
    v2%gil%version_uv = code_version_uvt_freq
    call gdf_uv_doppler(v2,code_version_uvt_current)
  else
    v2%gil%uvda_words = 0 ! No UV Data
  endif
  !
  v2%gil%tele_words = 0
end subroutine gio_hd32_to_gildas_v2
!
!
subroutine gio_gildas_v2_to_hd32(v2,hd32)
  use gio_headers
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  !     Transfer the "old" type(gildas)
  !     to a new 64-bit Data File Header
  !---------------------------------------------------------------------
  type(gildas_header_v1), intent(inout) :: hd32
  type(gildas), intent(inout) :: v2
  integer(kind=4) :: iwhere(2,4)
  integer :: i
  !
  call chtoby (v2%char%type, hd32%ijtyp, 12)
  hd32%form = v2%gil%form
  hd32%ndb = v2%gil%ndb
     ! GENERAL SECTION
  hd32%gene = 29 ! v2%gil%gene
  hd32%ndim = v2%gil%ndim
  hd32%dim(1:4) = v2%gil%dim(1:4)
  call r8tor8(v2%gil%convert, hd32%ref1, 12)
     ! BLANKING
  hd32%blan = v2%gil%blan_words
  hd32%bval = v2%gil%bval
  hd32%eval = v2%gil%eval
     ! EXTREMA
  hd32%extr = v2%gil%extr_words
  if (hd32%extr.ne.0) then
    hd32%rmin = v2%gil%rmin
    hd32%rmax = v2%gil%rmax
    iwhere = 0
    iwhere(1,1:hd32%ndim) = v2%gil%minloc(1:hd32%ndim)
    iwhere(2,1:hd32%ndim) = v2%gil%maxloc(1:hd32%ndim)
    call i4toi4(iwhere,hd32%min1,8)
    hd32%extr = 10
  endif
     ! DESCRIPTION
  hd32%desc = v2%gil%desc_words
  call chtoby (v2%char%unit, hd32%ijuni,  12)
  do i=1,4
    call chtoby (v2%char%code(i), hd32%ijcod(1,i), 12)
  enddo
  call chtoby (v2%char%syst, hd32%ijsys, 12)
     ! POSITION
  hd32%posi = v2%gil%posi_words
  call chtoby (v2%char%name, hd32%ijsou, 12)
  hd32%ra = v2%gil%ra
  hd32%dec = v2%gil%dec
  hd32%lii = v2%gil%lii
  hd32%bii = v2%gil%bii
  hd32%epoc = v2%gil%epoc
     ! PROJECTION
  hd32%proj = v2%gil%proj_words
  hd32%ptyp = v2%gil%ptyp
  hd32%a0 = v2%gil%a0
  hd32%d0 = v2%gil%d0
  hd32%pang = v2%gil%pang
  hd32%xaxi = v2%gil%xaxi
  hd32%yaxi = v2%gil%yaxi
     ! SPECTROSCOPY
  hd32%spec = v2%gil%spec_words
  call chtoby (v2%char%line, hd32%ijlin, 12)
  hd32%fres = v2%gil%fres
  hd32%fima = v2%gil%fima
  hd32%freq = v2%gil%freq
  hd32%vres = v2%gil%vres
  hd32%voff = v2%gil%voff
  hd32%faxi = v2%gil%faxi
  ! vdop & vtyp are lost
     ! RESOLUTION
  hd32%reso = v2%gil%reso_words
  hd32%majo = v2%gil%majo
  hd32%mino = v2%gil%mino
  hd32%posa = v2%gil%posa
     ! NOISE
  hd32%sigm = v2%gil%nois_words
  hd32%noise = v2%gil%noise
  hd32%rms = v2%gil%rms
     ! ASTROMETRY
  hd32%prop = v2%gil%astr_words
  hd32%mura = v2%gil%mura
  hd32%mudec = v2%gil%mudec
  hd32%parallax = v2%gil%parallax
  !
  ! There is nothing specific about UV Data in old format...
  !
  ! Except the Doppler factor !..
  if (v2%gil%version_uv .ne. code_version_uvt_freq) then
    Print *,'No support of Doppler factor in Gildas V1'
  endif
  if (v2%gil%tele_words .ne. 0) then
    Print *,'No support for Telescope coordinates in Gildas V1'
  endif
end subroutine gio_gildas_v2_to_hd32
!
!
subroutine gio_hd64_to_gildas_v2 (hd64,v2,error)
  use gio_interfaces, except_this=>gio_hd64_to_gildas_v2
  use gio_uv
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GIO/GDF
  !     Transfer a new 64-bit Data File Header to
  !     the "new" type(gildas)
  !---------------------------------------------------------------------
  type (gildas_header_v2), intent(in) :: hd64
  type (gildas), intent(inout) :: v2   ! Must be (inout) because initialized before
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='GDF_HD64_TO_GILDAS_V2'
  !
  character(len=12) :: chain
  character(len=80) :: mess
  integer :: i, ier
  !
  call gio_message(seve%t,rname,'Entering...')
  !
  call bytoch(hd64%ijtyp,v2%char%type,12)
  v2%gil%form = hd64%form
  v2%gil%ndb = hd64%ndb
  v2%gil%type_gdf =  hd64%type_gdf
  !
  if (hd64%ndim.gt.gdf_maxdims) then
    call gio_message(seve%e,rname,'Too many dimensions for current GILDAS format')
    error = .true.
    return
  endif
  v2%gil%dim = 1
  v2%gil%ndim = hd64%ndim
  v2%gil%dim(1:v2%gil%ndim) = hd64%dim(1:hd64%ndim)
  v2%gil%dim_words = def_dim_words
  !
  v2%gil%blan_words = hd64%blan_words
  v2%gil%bval = hd64%bval
  v2%gil%eval = hd64%eval
  !
  if (hd64%extr_words.ne.0) then
    v2%gil%rmin = hd64%rmin
    v2%gil%rmax = hd64%rmax
!    v2%gil%mini = hd64%mini
!    v2%gil%maxi = hd64%maxi
    v2%gil%minloc = hd64%minloc
    v2%gil%maxloc = hd64%maxloc
    v2%gil%extr_words = def_extr_words
    !!Print *,'to v2%gil extr '
  else
    v2%gil%extr_words = 0
  endif
  !
  ! This section is not available for TABLEs
  if (hd64%coor_words.ne.0) then
    v2%gil%convert = hd64%convert
    v2%gil%coor_words = def_coor_words
    !!Print *,'to v2%gil gene '
  else
    v2%gil%coor_words = 0
  endif
  !
  ! Description
  if (v2%gil%desc_words.ne.0) then
    call bytoch (hd64%ijuni, v2%char%unit, 12)
    do i=1,max(4,v2%gil%ndim)
      if (any(hd64%ijcod(:,i).ne.0)) then
        call bytoch (hd64%ijcod(1,i), v2%char%code(i), 12)
      endif
    enddo
    v2%gil%desc_words = def_desc_words
    !!Print *,'to v2%gil desc '
  else
    v2%gil%desc_words = 0
  endif
  !  ! POSITION
  if (hd64%posi_words.ne.0) then
    call bytoch (hd64%ijsys, v2%char%syst, 12)
    call bytoch (hd64%ijsou, v2%char%name, 12)
    v2%gil%ra = hd64%ra
    v2%gil%dec = hd64%dec
    v2%gil%lii = hd64%lii
    v2%gil%bii = hd64%bii
    v2%gil%epoc = hd64%epoc
    v2%gil%posi_words = def_posi_words
    !!Print *,'to v2%gil posi '
  else
    v2%gil%posi_words = 0
  endif
  ! !
  !  ! PROJECTION
  if (hd64%proj_words.ne.0) then
    v2%gil%a0 = hd64%a0
    v2%gil%d0 = hd64%d0
    v2%gil%pang = hd64%pang
    v2%gil%ptyp = hd64%ptyp
    v2%gil%xaxi = hd64%xaxi
    v2%gil%yaxi = hd64%yaxi
    v2%gil%proj_words = def_proj_words
    !!Print *,'to v2%gil proj '
  else
    v2%gil%proj_words = 0
  endif
  !
  !  ! SPECTROSCOPY
  if (hd64%spec_words.ne.0) then
    v2%gil%fres = hd64%fres
    v2%gil%fima = hd64%fima
    v2%gil%freq = hd64%freq
    v2%gil%vres = hd64%vres
    v2%gil%voff = hd64%voff
    v2%gil%faxi = hd64%faxi
    v2%gil%dopp = hd64%dopp
    call bytoch (hd64%ijlin, v2%char%line, 12)
    v2%gil%vtyp = hd64%vtyp
    v2%gil%spec_words = def_spec_words
    !!Print *,'to v2%gil spec ',hd64%vtyp, def_spec_words
  else
    v2%gil%spec_words = 0
  endif
  !  !
  !  ! RESOLUTION
  if (hd64%reso_words.ne.0) then
    v2%gil%majo = hd64%majo
    v2%gil%mino = hd64%mino
    v2%gil%posa = hd64%posa
    v2%gil%reso_words = def_reso_words
  else
    v2%gil%reso_words = 0
  endif
  !  !
  !  ! NOISE
  if (hd64%nois_words.ne.0) then
    v2%gil%noise = hd64%noise
    v2%gil%rms = hd64%rms
    v2%gil%nois_words = def_nois_words
    !!Print *,'to v2%gil sigm '
  else
    v2%gil%nois_words = 0
  endif
  !  !
  !  ! ASTROMETRY
  if (hd64%astr_words.ne.0) then
    v2%gil%mura = hd64%mura
    v2%gil%mudec = hd64%mudec
    v2%gil%parallax = hd64%parallax
    v2%gil%astr_words = def_astr_words
    !!Print *,'to v2%gil prop '
  else
    v2%gil%astr_words = 0
  endif
  !
  !  ! UV_DATA information
  !
  if (hd64%uvda_words.ne.0) then
    chain = v2%char%type
    if (chain(8:9).ne.'UV') then
      call gio_message(seve%e,rname,'UV Data Section for non UV dataset')
      write(mess,*) 'uvda_words ',hd64%uvda_words, v2%gil%uvda_words, chain
      call gio_message(seve%w,rname,mess)
      error = .true.
      return
    endif
    !
    if (v2%gil%proj_words.eq.0) then
      v2%gil%ptyp = p_azimuthal             ! Azimuthal (Sin)
      v2%gil%a0 = v2%gil%ra
      v2%gil%d0 = v2%gil%dec
      call gio_message(seve%w,'GDF', &
           & 'No projection information, Assumed Azimuthal around RA and DEC')
    endif
    !
    ! Here, one may compute %fcol and %ncol from %nlead and %ntrail or vice-versa...
    v2%gil%nchan = hd64%nchan
    v2%gil%nvisi = hd64%nvisi
    v2%gil%nstokes = hd64%nstokes
    v2%gil%natom = 3 ! Will be a more complex operation for V2
    v2%gil%order = hd64%order
    !
    v2%gil%basemin = hd64%basemin
    v2%gil%basemax = hd64%basemax
    !
    v2%gil%fcol = hd64%fcol
    v2%gil%lcol = hd64%lcol
    !
    v2%gil%nlead = hd64%nlead
    v2%gil%ntrail = hd64%ntrail
    !
    ! Set the back pointers... They must have been defined when the data has been read.
    !
    v2%gil%column_pointer = hd64%column_pointer !
    v2%gil%column_size = hd64%column_size !
    v2%gil%atoms = hd64%atoms
    !
    v2%gil%uvda_words = 18+2*(v2%gil%nlead+v2%gil%ntrail) !
    !!Print *,'to v2%gil UVDATA ',hd64%nfreq
    !
    ! Code for Frequencies & Stokes is missing here
    if (hd64%nfreq.ne.0) then
      if (associated(v2%gil%freqs)) deallocate(v2%gil%freqs)
      if (associated(v2%gil%stokes)) deallocate(v2%gil%stokes)
      v2%gil%nfreq = hd64%nfreq
      allocate(v2%gil%freqs(hd64%nfreq), v2%gil%stokes(hd64%nfreq), stat=ier)
      if (ier.ne.0) Print *,'Freqs & Stokes IER ',ier
      v2%gil%freqs = hd64%freqs
      v2%gil%stokes = hd64%stokes
    else
      v2%gil%nfreq = 0
      if (abs(hd64%order).eq.abs(code_stok_chan)) then
        allocate(v2%gil%stokes(hd64%nstokes), stat=ier)
        v2%gil%stokes = hd64%stokes
      endif
    endif
    !
    ! Compute the Doppler factor, frequency and velocity resolutions...
    v2%gil%version_uv = hd64%version_uv
    call gdf_uv_doppler(v2,code_version_uvt_current)
  else
    v2%gil%uvda_words = 0
  endif
  !
  !
  if (hd64%tele_words.ne.0) then
    v2%gil%tele_words = hd64%tele_words
!    if (code_version_uvt_current.lt.code_version_uvt_syst) then
!      call gio_message(seve%w,'GDF', &
!           & 'Telescope information ignored in this version (see SIC UV)')
!      v2%gil%tele_words = 0
!      v2%gil%nteles = 0
!    else
      !!Print *,'HD64 --> V2 tele is experimental, NTELES = ',hd64%nteles
      allocate(v2%gil%teles(hd64%nteles), stat=ier)
      v2%gil%nteles = hd64%nteles
      v2%gil%magic  = hd64%magic
      v2%gil%teles(:) = hd64%teles(:)  ! Is that a copy of pointer or of data ?
!    endif
  else
    v2%gil%tele_words = 0
  endif
  error = .false.
  call gio_message(seve%t,rname,'Leaving..')
  !
end subroutine gio_hd64_to_gildas_v2
!
!
subroutine gio_gildas_v2_to_hd64 (v2,hd64,error)
  use gio_interfaces, except_this=>gio_gildas_v2_to_hd64
  use image_def
  use gio_params
  use gio_headers
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !     Transfer the "new" type(gildas)
  !     to a new 64-bit Data File Header
  !---------------------------------------------------------------------
  type (gildas), intent(in) :: v2
  type (gildas_header_v2), intent(out) :: hd64
  logical, intent(out) :: error
  ! Local
  integer :: i, ier, nhb
  character(len=12) :: chain
  character(len=80) :: mess
  character(len=*), parameter :: rname='GDF_GILDAS_V2_TO_HD64'
  !
  call gio_message(seve%t,rname,'Entering...')
  !
  call chtoby (v2%char%type, hd64%ijtyp, 12)
  hd64%form = v2%gil%form
  !! Print *,'V2 to HD64 -- Format ',v2%gil%form, v2%char%type, v2%gil%type_gdf
  hd64%ndb = v2%gil%ndb
  hd64%type_gdf = v2%gil%type_gdf ! Yes, man...
  ! Already set hd64%nhb = nhb
  ! Already set hd64%ntb = 0
  !
  hd64%dim = 1
  hd64%ndim = v2%gil%ndim
  hd64%dim(1:gdf_maxdims) = v2%gil%dim
  !
  hd64%blan_words = v2%gil%blan_words
  hd64%bval = v2%gil%bval
  hd64%eval = v2%gil%eval
  !
  hd64%extr_words = v2%gil%extr_words
  if (hd64%extr_words.ne.0) then
    hd64%rmin = v2%gil%rmin
    hd64%rmax = v2%gil%rmax
!    hd64%mini = v2%gil%mini
!    hd64%maxi = v2%gil%maxi
    hd64%minloc = v2%gil%minloc
    hd64%maxloc = v2%gil%maxloc
  endif
  !
  ! This section is not available for TABLEs
  if (v2%gil%coor_words.ne.0) then
    !!print *,'V2 --> HD64, doing coor '
    hd64%coor_words = 6*gdf_maxdims
    hd64%convert = v2%gil%convert
  else
    hd64%coor_words = 0
    hd64%convert = 1.d0
  endif
  !
  ! Description
  hd64%desc_words = v2%gil%desc_words
  if (hd64%desc_words.ne.0) then
    !!print *,'V2 --> HD64, doing desc '
    call chtoby (v2%char%unit, hd64%ijuni, 12)
    do i=1,gdf_maxdims
      call sic_upper(v2%char%code(i))
      call chtoby (v2%char%code(i), hd64%ijcod(1,i), 12)
    enddo
  endif
  !  ! POSITION
  hd64%posi_words = v2%gil%posi_words
  if (hd64%posi_words.ne.0) then
    !!print *,'V2 --> HD64, doing posi '
    call chtoby (v2%char%name, hd64%ijsou, 12)
    call sic_upper(v2%char%syst)
    call chtoby (v2%char%syst, hd64%ijsys, 12)
    hd64%ra = v2%gil%ra
    hd64%dec = v2%gil%dec
    hd64%lii = v2%gil%lii
    hd64%bii = v2%gil%bii
    hd64%epoc = v2%gil%epoc
  endif
  ! !
  !  ! PROJECTION
  hd64%proj_words = v2%gil%proj_words
  if (hd64%proj_words.ne.0) then
    !!print *,'V2 --> HD64, doing proj '
    hd64%a0 = v2%gil%a0
    hd64%d0 = v2%gil%d0
    hd64%pang = v2%gil%pang
    hd64%ptyp = v2%gil%ptyp
    hd64%xaxi = v2%gil%xaxi
    hd64%yaxi = v2%gil%yaxi
  endif
  !
  !  ! SPECTROSCOPY
  hd64%spec_words = v2%gil%spec_words
  if (hd64%spec_words.ne.0) then
    !!print *,'V2 --> HD64, doing spec ',hd64%spec_words,v2%gil%vtyp
    hd64%fres = v2%gil%fres
    hd64%fima = v2%gil%fima
    hd64%freq = v2%gil%freq
    hd64%vres = v2%gil%vres
    hd64%voff = v2%gil%voff
    hd64%dopp = v2%gil%dopp
    hd64%faxi = v2%gil%faxi
    call chtoby (v2%char%line, hd64%ijlin, 12)
    hd64%vtyp = v2%gil%vtyp
  endif
  !  !
  !  ! RESOLUTION
  hd64%reso_words = v2%gil%reso_words
  if (hd64%reso_words.ne.0) then
    !!print *,'V2 --> HD64, doing reso '
    hd64%majo = v2%gil%majo
    hd64%mino = v2%gil%mino
    hd64%posa = v2%gil%posa
  endif
  !  !
  !  ! NOISE
  hd64%nois_words = v2%gil%nois_words
  if (hd64%nois_words.ne.0) then
    !!print *,'V2 --> HD64, doing noise '
    hd64%noise = v2%gil%noise
    hd64%rms = v2%gil%rms
  endif
  !  !
  !  ! ASTROMETRY
  !  !
  hd64%astr_words = v2%gil%astr_words
  if (hd64%astr_words.ne.0) then
    !!print *,'V2 --> HD64, doing astro '
    hd64%mura = v2%gil%mura
    hd64%mudec = v2%gil%mudec
    hd64%parallax = v2%gil%parallax
  endif
  !
  !  ! TELESCOPE, before UV_DATA
  !
  hd64%tele_words = v2%gil%tele_words
  if (hd64%tele_words.ne.0) then
    hd64%nteles = v2%gil%nteles
    if (hd64%nteles.le.0) then
      call gio_message(seve%w,rname,'Removed empty telescope description')
      hd64%tele_words = 0
    else
      !! write(mess,*) '%tele is experimental NTELES ',hd64%nteles
      !! call gio_message(seve%d,rname,mess)
      allocate(hd64%teles(hd64%nteles), stat=ier)
      hd64%magic = v2%gil%magic
      hd64%teles(:) = v2%gil%teles(:)
    endif
  endif
  ! !
  !  ! UV_DATA information
  !
  hd64%uvda_words = v2%gil%uvda_words
  write(mess,*) 'UV info UVDA_WORDS ',v2%gil%uvda_words,v2%char%type
  call gio_message(seve%d,rname,mess)
  !
  if (hd64%uvda_words.ne.0) then
    !
    ! UV Data section -- check type
    chain = v2%char%type
    if (chain(8:9).ne.'UV') then
      call gio_message(seve%e,rname,'UV Data Section for non UV dataset')
      error = .true.
      return
    endif
    !
    ! Here, one may compute %fcol and %ncol from %nlead and %ntrail or vice-versa...
    hd64%nchan = v2%gil%nchan
    hd64%nvisi = v2%gil%nvisi
    hd64%nstokes = v2%gil%nstokes
    hd64%natom = v2%gil%natom
    hd64%order = v2%gil%order
    write(mess,*) 'UV info ',hd64%nchan,hd64%nvisi,hd64%nstokes,hd64%natom,hd64%order
    call gio_message(seve%d,rname,mess)
!!    read(5,*) hd64%nchan
    !
    hd64%basemin = v2%gil%basemin
    hd64%basemax = v2%gil%basemax
    !
    ! Set the back pointers... They must have been defined when
    ! the UV data set has been created (in the calling programs)
    !
    hd64%column_pointer  = v2%gil%column_pointer
    hd64%column_size  = v2%gil%column_size
    !
    ! Use the appropriate subroutine to derive fcol/lcol nlead/ntrail consistently
    hd64%version_uv = v2%gil%version_uv
    !!Print *,'Before GIO_SETUV Version UV ',hd64%version_uv,' Teles ',hd64%nteles
    call gio_setuv (hd64,error)
    !!Print *,'After  GIO_SETUV Version UV ',hd64%version_uv
    if (error) then
      call gio_message(seve%e,rname,'Inconsistent UV pointers')
      error = .true.
      return
    endif
    !
    hd64%uvda_words = 18+hd64%nlead+hd64%ntrail !
    hd64%uvda_words = hd64%uvda_words + 3*v2%gil%nfreq
    !!Print *,'to hd64 UVDATA ',v2%gil%freq
    !
    ! What happens with the variable length array %freqs and %stokes
    ! which are of length   %nfreq  ?
    !
    ! We should
    ! 1)  copy them here too...
    ! 2)  reset %ndb to a proper length, accounting for the space
    !     required to store %freqs and %stokes
    !
    if (v2%gil%nfreq.ne.0) then
      hd64%nfreq = v2%gil%nfreq
      ! The initial 2 here also depends on the number of extra columns (< 29), but we play safe
      ! There are 3 32-bit words per entry (one frequency and one stokes)
      nhb = hd64%nhb
      hd64%nhb = 2 + (3*hd64%nfreq + 2+10*hd64%nteles + 127)/128   ! Total number of HEADER blocks required...
      !! Print *,'Setting the number of Header Blocks from ',nhb,' to ',hd64%nhb
      if (hd64%nhb.gt.gio_block1) then      ! This may be removed at some stage
        write(mess,'(A,I0,A,I0,A)') 'UV Table header is long (',hd64%nhb,' > ',gio_block1,')'
        call gio_message(seve%w,rname,mess)
!!        error = .true.
!!        return
      endif
      !
      allocate(hd64%freqs(hd64%nfreq), hd64%stokes(hd64%nfreq), stat=ier)
      hd64%freqs = v2%gil%freqs
      hd64%stokes = v2%gil%stokes
    else
      hd64%nfreq = 0
      if (abs(hd64%order).eq.abs(code_stok_chan)) then
        if ((hd64%nlead + hd64%ntrail + hd64%nstokes).gt.29) hd64%nhb = hd64%nhb + 1
        allocate(hd64%stokes(hd64%nstokes), stat=ier)
        if (.not.associated(v2%gil%stokes)) then
          call gio_message(seve%e,rname,'Programming error -- Stokes parameters not allocated ')
          hd64%stokes = 0 ! Just in case
        else
          hd64%stokes = v2%gil%stokes
        endif
      endif
    endif
  else
    !
    ! Image or Tables -- check type
    write(mess,*) 'TYPE_GDF ',hd64%type_gdf, v2%gil%type_gdf, code_gdf_uvt
    call gio_message(seve%d,rname,mess)
    !
    if (abs(hd64%type_gdf).eq.abs(code_gdf_uvt) ) then
      call gio_message(seve%e,rname,'No UV Data Section for UV dataset')
      error = .true.
      return
    endif
    !
    ! Adjust Header length if needed...
    if (hd64%nteles.ne.0) then
      ! Total number of HEADER blocks required...
      ! Here, we remove the unexisting UV data section length, but not its pointers.
      hd64%nhb = 1 + (second_words + 2 + 2+10*hd64%nteles + 127) / 128
      write(mess,'(A,I0,A,I0)') 'Set hd64%nhb = ',hd64%nhb,' for Nteles = ',hd64%nteles
      call gio_message(seve%d,rname,mess)
    endif
  endif
  !
  !!Print *,'Done GIO_GILDAS_V2_TO_HD64'
  error = .false.
  call gio_message(seve%t,rname,'Leaving!..')
end subroutine gio_gildas_v2_to_hd64
!
subroutine gio_setuv (hd64,error)
  use gio_interfaces, except_this=>gio_setuv
  use image_def
  use gbl_message
  !
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Define the UV section consistently.
  ! It could also define the column_types and column_codes if needed
  !
  type(gildas_header_v2), intent(inout) :: hd64
  logical, intent(out) :: error
  !
  integer, allocatable :: cases(:)
  integer :: i, j, icase, visisize, ipoint
  character(len=80) :: mess
  character(len=*), parameter :: rname= 'GIO_SETUV'
  !
  call gio_message(seve%t,rname,'Entering...')
  !
  ! Define visibility size
  if (hd64%type_gdf.eq.code_gdf_tuv) then
    visisize = hd64%dim(2)
  else
    visisize = hd64%dim(1)
  endif
  !
  ! Define number of columns
  error = .false.
  icase = 0
  do i=1,code_uvt_last
    ipoint = hd64%column_pointer(i)
    if (ipoint.eq.0) then
      ! Nothing: check it is of Size 0
      if (hd64%column_size(i).ne.0) then
        write(mess,*) 'Inconsistent size ',i, ipoint, hd64%column_size(i)
        call gio_message(seve%e,rname,mess)
        error = .true.
        return
      endif
    else if (ipoint.le.visisize) then
      ! In current range: check it as a size of at least 1
      if (hd64%column_size(i).eq.0) then
        hd64%column_size(i) = 1  ! By default
      endif
      icase = max(icase,hd64%column_pointer(i)+hd64%column_size(i)-1)
    else 
      ! Out of range: check it is of Size 0 and set to 0
      hd64%column_pointer(i) = 0
      if (hd64%column_size(i).ne.0) then
        hd64%column_size(i) = 0
        write(mess,*) 'Erased column pointer ',i,' beyond visibility size'
        call gio_message(seve%i,rname,mess)
      endif
    endif
    write(mess,*) 'checked ',i, hd64%column_pointer(i), hd64%column_size(i), icase
    call gio_message(seve%d,rname,mess)
  enddo
  !
  ! Fill the columns ID
  allocate(cases(icase))
  cases = 0
  do i=1,code_uvt_last
    if (hd64%column_pointer(i).ne.0) then
      do j=1,hd64%column_size(i)
        cases(j+hd64%column_pointer(i)-1) = 1
      enddo
    endif
  enddo
  !
  ! Locate the first zero: this means start of data Columns
  hd64%fcol = icase+1  ! In case no trailing
  do i=1,icase
    if (cases(i).eq.0) then
      hd64%fcol = i
      exit
    endif
  enddo
  write(mess,*) 'Found hd64%fcol = ',hd64%fcol
  call gio_message(seve%d,rname,mess)
  !
  ! Locate the last zero: this means end of data Columns
  hd64%lcol = visisize  ! If no trailing column at all
  do i=hd64%fcol,icase
    if (cases(i).eq.1) then
      hd64%lcol = i-1
      exit
    endif
  enddo
  write(mess,*) 'Found hd64%lcol = ',hd64%lcol
  call gio_message(seve%d,rname,mess)
  !
  ! Consistency check: Figure out Trailing Empty columns 
  hd64%ntrail = 0     ! No trailing column at start
  do i=hd64%lcol+1,icase
    if (cases(i).eq.0) then
      write(mess,*) 'Empty trailing column at ',i
      call gio_message(seve%w,rname,mess)
      hd64%ntrail = hd64%ntrail + 1 !! Count it as a Trailing column
!      error = .true.  ! But do not set any error
    endif
  enddo
  if (icase.gt.hd64%lcol.and.icase.lt.visisize) then
    write(mess,*) 'Empty trailing column(s) beyond ',icase,' until ',visisize
    hd64%ntrail = hd64%ntrail + visisize-icase !! Count them as a Trailing column
    call gio_message(seve%w,rname,mess)
  endif
  if (error) return  
  !
  ! Now lead & trail
  hd64%nlead = hd64%fcol-1
!!  hd64%ntrail = 0 !! Now already defined above
  do i=1,code_uvt_last
    if (hd64%column_pointer(i).gt.hd64%lcol) then
      hd64%ntrail = hd64%ntrail+hd64%column_size(i)
    endif
  enddo
  write(mess,*) 'Found hd64%nlead = ',hd64%nlead,' %trail = ',hd64%ntrail
  call gio_message(seve%d,rname,mess)
  !
  if (hd64%nfreq.ne.0) then
    call gio_setuv_freq(hd64,error)
    ! The 2 here also depends on the number of extra columns (< 29), but we play safe
    ! There are 3 32-bit words per entry (one frequency and one stokes)
    hd64%nhb = 2 + (3*hd64%nfreq + 2+10*hd64%nteles + 127)/128   ! Total number of HEADER blocks required...
    write(mess,*) 'Set hd64%nhb = ',hd64%nhb,' because of Nfreq ',hd64%nfreq
    call gio_message(seve%i,rname,mess)
  else if (hd64%nteles.ne.0) then
    ! Here, we play safe too, as the stuff actually may fit in 2 header blocks only
    hd64%nhb = 2 + (2+10*hd64%nteles + 127)/128   ! Total number of HEADER blocks required...
    !write(mess,*) 'Set hd64%nhb = ',hd64%nhb,' because of Nteles ',hd64%nteles
    !call gio_message(seve%i,rname,mess)
  endif
  call gio_message(seve%t,rname,'Leaving !..')
end subroutine gio_setuv
!
subroutine gio_setuv_freq(hd64,error)
  use gio_interfaces, except_this=>gio_setuv_freq
  use image_def
  use gbl_message
  !
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Define the UV section consistently.
  ! It could also define the column_types and column_codes if needed
  !
  type(gildas_header_v2), intent(inout) :: hd64
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname= 'GIO_SETUV_FREQ'
  real(8), parameter :: clight=299792.458d0
  !
  character(len=120) :: mess
  integer :: i, lt
  real(8) :: fmin, fmax, df
  real(8), allocatable :: dfreq(:)
  !
  if (hd64%nfreq.eq.0) return
  !
  call gio_message(seve%t,rname,'Entering...')
  write(mess,'(A,I0,A,I0,A,L1)') 'Frequencies ',hd64%nfreq,' Stokes ',hd64%nstokes,' allocated ',associated(hd64%stokes)
  !
  ! Set the Frequency and Stokes array, to determine the number of Header Blocks
  !
  ! Set a representative frequency - Unclear whether hd64%fres is already defined.
  ! We assume so...
  hd64%convert(1,hd64%faxi) = (hd64%nfreq+1.d0)/2.d0
  hd64%freq = sum(hd64%freqs(1:hd64%nfreq))/hd64%nfreq
  hd64%convert(2,hd64%faxi) = hd64%freq
  hd64%vres = -hd64%fres/hd64%freq*clight    
  !
  ! Attempt to linearize the axis
  if (hd64%nfreq.eq.1) then
    call gio_message(seve%d,rname,mess)
    call gio_message(seve%i,rname,'Only one Frequency') 
    hd64%freq = hd64%freqs(1)
    hd64%convert(1,hd64%faxi) = 1.d0
    hd64%convert(2,hd64%faxi) = hd64%freq
    hd64%vres = -hd64%fres/hd64%freq*clight
    if (associated(hd64%stokes)) deallocate(hd64%stokes)
    deallocate(hd64%freqs)
    hd64%nfreq = 0
  else 
    if (.not.associated(hd64%stokes)) then
      allocate(hd64%stokes(hd64%nfreq))
    else
      lt = len_trim(mess)
      write(mess(lt+1:),'(A,I0)') 'Stokes array size ',ubound(hd64%stokes)
    endif
    call gio_message(seve%d,rname,mess)
    ! 
    if (hd64%nstokes.le.1) then
      ! May linearize the Frequency axis if no Stokes parameter !!Under TEST
      allocate(dfreq(hd64%nfreq-1))
      do i=1,hd64%nfreq-1
        dfreq(i) = hd64%freqs(i+1) - hd64%freqs(i)
      enddo
      fmin = minval(dfreq)
      fmax = maxval(dfreq)
      df = (fmax-fmin)/max(abs(fmax),abs(fmin))
      !!do i=1,hd64%nfreq-1
      !!  Print *,'Freq ',i,hd64%freqs(i),dfreq(i)
      !!enddo
      !Print *,'Freq ',hd64%nfreq,hd64%freqs(hd64%nfreq)
      !Print *,'Df ',df,'  min ',fmin,' max ',fmax
      !! Read(5,*) DF
      !
      if (df.lt.1e-3) then
        hd64%convert(3,hd64%faxi)  = dfreq(hd64%faxi)
        ! val  = hd64%freqs(1)
        ! ref  = 1.d0
        !
        ! val = hd64%val(1) + hd64%inc(1)*(ref-hd64%ref(1))
!!        hd64%ref(hd64%faxi) = 1.d0 - (hd64%freqs(1)-hd64%val(hd64%faxi)) / hd64%inc(hd64%faxi)
        !
        ! Set a representative frequency
        hd64%convert(1,hd64%faxi) = 1.0d0
        hd64%freq = hd64%freqs(1)
        hd64%convert(2,hd64%faxi) = hd64%freq
        hd64%vres = -hd64%fres/hd64%freq*clight
        !
        hd64%nfreq = 0
        deallocate(hd64%freqs,hd64%stokes)
      else
        call gio_message(seve%w,rname,'Keeping non-contiguous Frequency axis')
        write(mess,'(A,F15.3,A,F15.3)') '   Steps:  Min ',fmin,',  Max ',fmax
        call gio_message(seve%i,rname,mess) 
        !
        ! Set a representative frequency
        hd64%convert(1,hd64%faxi) = (hd64%nfreq+1.d0)/2.d0
        hd64%freq = sum(hd64%freqs(1:hd64%nfreq))/hd64%nfreq
        hd64%convert(2,hd64%faxi) = hd64%freq
        hd64%vres = -hd64%fres/hd64%freq*clight
      endif
    else
      call gio_message(seve%d,rname,'Keeping Stokes and Frequency axis')
    endif
  endif
  !
  if (hd64%nfreq.eq.0) call gio_message(seve%i,rname,'Frequency axis was linearized')
  !
  call gio_message(seve%t,rname,'Leaving !..')
end subroutine gio_setuv_freq
!
subroutine gdf_setuv(h,error)
  use gio_interfaces, only : gio_setuv
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Define the UV section consistently.
  ! It could also define the column_types and column_codes if needed
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: h
  logical,      intent(inout) :: error
  call gio_setuv(h%gil, error)
end subroutine gdf_setuv
!
subroutine gdf_uv_shift_columns(huvin,huvou)
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  !   Shift the trailing columns when copying and modifying
  !   a UV table (channel extraction, Stokes reduction, etc...)
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: huvin
  type(gildas), intent(inout) :: huvou
  !
  integer dtrail, dsize, doff, osize, nsize, l
  !
  ! Trail may have changed
  dtrail = huvou%gil%ntrail - huvin%gil%ntrail
  !
  ! Visibility size may have changed
  osize = huvin%gil%natom * max(huvin%gil%nstokes,1) * huvin%gil%nchan
  nsize = huvou%gil%natom * max(huvou%gil%nstokes,1) * huvou%gil%nchan
  dsize = nsize-osize
  doff = dsize+dtrail
  !
  ! Shift the trailing columns of the output table by the right amount
  do l=1,code_uvt_last
    if (doff.lt.0) then
      if (huvou%gil%column_pointer(l).gt.huvin%gil%lcol+doff) then
        huvou%gil%column_pointer(l) = &
        huvou%gil%column_pointer(l)+doff
      endif
    else if (huvou%gil%column_pointer(l).gt.huvin%gil%lcol) then 
      if (huvou%gil%column_pointer(l).le.huvin%gil%lcol+doff) then
        huvou%gil%column_pointer(l) = &
        huvou%gil%column_pointer(l)+doff
      endif
    endif
  enddo
end subroutine gdf_uv_shift_columns
