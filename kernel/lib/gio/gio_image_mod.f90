module gio_convert
  integer(kind=4), parameter :: unknown=-20
  !
  ! 32 bit codes
  integer(kind=4), parameter :: vax_to_ieee=+1
  integer(kind=4), parameter :: ieee_fr_vax=-1
  integer(kind=4), parameter :: ieee_to_vax=+2
  integer(kind=4), parameter :: vax_fr_ieee=-2
  integer(kind=4), parameter :: vax_to_eeei=+3
  integer(kind=4), parameter :: eeei_fr_vax=-3
  integer(kind=4), parameter :: eeei_to_vax=+4
  integer(kind=4), parameter :: vax_fr_eeei=-4
  integer(kind=4), parameter :: ieee_to_eeei=+5
  integer(kind=4), parameter :: ieee_fr_eeei=-5
  integer(kind=4), parameter :: eeei_fr_ieee=-6
  integer(kind=4), parameter :: eeei_to_ieee=+6
  !
  ! 64 bits codes: as above, but with offset off_64bit, which must be 10
  integer(kind=4), parameter :: off_64bit=+10
  integer(kind=4), parameter :: ndb_64bit=2**(31-7)  ! Number of virtual 512-byte blocks for a 2 Gbytes file
  !
  ! Only 6 effective conversions are known 
  integer(kind=4), parameter :: mconve=6
end module gio_convert
!
module gio_image
  use gio_params
  use image_def
  !--------------------------------------------------------------------
  ! GIO global variables and parameters for image I/O support
  !--------------------------------------------------------------------
  integer(kind=4), parameter :: mis=30           ! Number of image slot
  integer(kind=4), parameter :: mms=60           ! Number of memory slots
  !
  ! Image slot status
  integer(kind=4) :: islot(mis) = code_gio_empty 
  integer(kind=4) :: ichan(mis) = 0              ! Channel numbers
  integer(kind=4) :: istbl(mis)                  ! Starting block for each Slot (kind=4 is enough)
  integer(kind=4) :: iconv(mis)                  ! Image header conversion type
  integer(kind=4) :: iform(mis)                  ! Image data type
  integer(kind=4) :: indim(mis)                  ! Image number of dimensions
  integer(kind=4) :: ivers(mis)                  ! Image Format Version number
  integer(kind=index_length) :: idims(gdf_maxdims,mis)! Image dimensions
  integer(kind=index_length) :: irblock(mis)     ! Last read block
  integer(kind=index_length) :: iwblock(mis)     ! Last written block
  integer(kind=index_length) :: imblock(mis)     ! End of file block
  integer(kind=4) :: iunit(mis)                  ! Logical units for images
  logical :: mapped(mis) = .false.               ! Mapping QIO indicator
  integer(kind=4) :: isbig(mis)                  ! Big block size indicator
  !
  integer(kind=4), parameter :: lenbuf=128       ! Length of small blocks
  integer(kind=4), parameter :: lenbig=gio_block2*gio_block1*lenbuf  ! Length of big blocks
  ! 
  integer(kind=4) :: mstbl = 0                   ! Maximum number of starting blocks
  integer(kind=4) :: startbl = 2                 ! Default number of starting blocks
  integer(kind=4), allocatable :: gdfbig(:,:)    ! (lenbuf,mstbl) 
  integer(kind=4) :: gdfbuf(lenbuf)              ! First header block 
  !
  character(len=filename_length) :: cname(mis)   ! Filenames
  type(gildas), target, save :: gheads(mis)      ! New storage of Headers
  !  
  integer(kind=address_length) :: maddr(2,mms)   ! Address of memory slot
  integer(kind=4) :: mslot(mms) = 0              ! Memory image slot number
  integer(kind=size_length) :: msize(mms) = 0    ! Memory image slot size
  integer(kind=size_length) :: moffs(mms)        ! Memory address offset
  integer(kind=size_length) :: mleng(mms)        ! Memory current slot size
  integer(kind=index_length) :: mblc(gdf_maxdims,mms) ! Memory image BLC
  integer(kind=index_length) :: mtrc(gdf_maxdims,mms) ! Memory image TRC
  integer(kind=4) :: mform(mms)                  ! Memory data type
  logical :: mcont(mms)                          ! Contiguous slot indicator
  !
  logical :: pre_allocate=.false.                ! Pre-allocate the file ?
end module gio_image
!
module gio_xy
  use image_def
  type(gildas), save :: fits
end module gio_xy
!
module gio_uv
  use gio_headers
  integer(kind=4), save :: code_version_uvt_current = code_version_uvt_default
end module gio_uv
