subroutine fits_gildas_sub(iname,oname,style,blc,trc,khdu,check,overwrite,error,  &
  getsymbol,wpr)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>fits_gildas_sub
  use gio_params
  use gio_xy
  use gio_fitsdef
  use gbl_message
  !--------------------------------------------------------------------
  ! @ public
  ! GIO FITS interface
  ! Convert a FITS file to a GDF file
  !--------------------------------------------------------------------
  character(len=*),           intent(in)    :: iname      ! Input FITS file
  character(len=*),           intent(in)    :: oname      ! Output GILDAS file
  character(len=*),           intent(in)    :: style      ! FITS style
  integer(kind=index_length), intent(in)    :: blc(:)     ! Bottom Left Corner
  integer(kind=index_length), intent(in)    :: trc(:)     ! Top Right Corner
  integer(kind=4),            intent(in)    :: khdu       ! HDU number (1=Primary)
  logical,                    intent(in)    :: check      ! Echo keywords ?
  logical,                    intent(in)    :: overwrite  ! Overwrite existing output file?
  logical,                    intent(inout) :: error      ! Returned error code
  external                                  :: getsymbol  ! Translate FITS keyword
  external                                  :: wpr        ! Ask a question if unknown
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=filename_length) :: name
  logical :: end_of_tape,err
  integer, parameter :: mstyle=5
  character(len=16) :: styles(mstyle),comm
  integer(kind=4) :: ncom,pcount,a_stoke,nc
  ! Data
  data styles /'STANDARD','UVFITS','AIPSFITS','SORTED_AIPSFITS','CASA_UVFITS'/
  !
  call gag_notanum(fd%bval0)
  question = .false.
  gdfname = oname
  name = iname
  !
  ! Open FITS file, including basic sanity checks
  call gfits_open(name,'IN',error)
  if (error)  return
  call gfits_goto_hdu(fd,khdu,error)
  if (error)  goto 10
  !
  ! Try to read a cube or a UV FITS?
  if (style.eq.' ') then
    call fits2gdf_guess_style(a_style,error)
    if (error)  goto 10
  else
    call sic_ambigs('STYLE',style,comm,ncom,styles,mstyle,error)
    if (error)  goto 10
    a_style = ncom-1
  endif
  !
  if (a_style.lt.code_fits_uvfits) then  ! "Normal" FITS
    call gildas_null(fits)
    !
    call fitscube2gdf_header(name,khdu,fd,fits,getsymbol,error)
    if (error) then
      call gio_message(seve%e,rname,'Error reading header in file '//name)
      goto 10
    endif
    call sic_parse_file(oname,' ','.gdf',fits%file)
    nc = len_trim(fits%file)
    !
    if (overwrite .and. gag_inquire(fits%file,nc).eq.0)  &
      call gag_filrm(fits%file(1:nc))
    !
    if (any(blc.ne.0) .or. any(trc.ne.0)) then
      ! Subset only
      call read_sub(fits,error,end_of_tape,blc,trc)
    else
      ! Full data
      call read_all(fits,error,end_of_tape)
    endif
    !
  else  ! UV FITS family
    call touvt(fits,a_style,question,check,error,pcount,getsymbol)
    if (error) then
      call gio_message(seve%e,rname,'Error reading UVFITS header')
      call gfits_close(err)
      return
    endif
    if (a_style.eq.code_fits_casa) then
      a_stoke = 0   ! Get all Stokes parameters
    else
      a_stoke = 1   ! Only get unpolarized signal
    endif
    call read_uvfits(fits,a_stoke,error,end_of_tape,pcount)
    if (error) then
      call gio_message(seve%e,rname,'Error reading UVFITS data')
      goto 10
    endif
    !
  endif
  !
10 continue
  call gfits_close(error)
  !
end subroutine fits_gildas_sub
