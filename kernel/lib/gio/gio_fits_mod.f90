module gio_fitsdef
  use gfits_types
  !--------------------------------------------------------------------
  ! GIO global variable used to define FITS or UV FITS files
  !--------------------------------------------------------------------
  !
  ! Constants
  integer(kind=4), parameter :: code_fits_standard=0
  integer(kind=4), parameter :: code_fits_uvfits=1
  integer(kind=4), parameter :: code_fits_aips=2
  integer(kind=4), parameter :: code_fits_sorted=3
  integer(kind=4), parameter :: code_fits_casa=4
  !
  ! Variables
  character(len=256) :: gdfname  ! Image name
  integer(kind=4) :: a_style     ! Style of processed FITS-tape
  logical :: question            ! Prompt when unrecognized axis type ?
  logical :: sort                ! Sort aips++ fits file ?
  !
  type(gfits_hdesc_t) :: fd
  !
  ! Variables for UV fits file
  real(kind=4) ::  cscal,czero                  ! Complex visibilities scale
  real(kind=4) ::  uscal,uzero                  ! U       scale
  real(kind=4) ::  vscal,vzero                  ! V        and
  real(kind=4) ::  wscal,wzero                  ! W       zero
  real(kind=4) ::  tscal,tzero                  ! Time scale and zero
  real(kind=4) ::  dscal,dzero                  ! Date scale and zero
  real(kind=4) ::  wps                          ! Weight scaling factor
  integer(kind=4) :: luu,lvv,lww,lbase,ldate,ltime,lsour,linte,lfreq
  integer(kind=4), parameter :: mcount=9    ! Must handle all cases of 1 digit
  real(kind=4) ::  pscal(mcount),pzero(mcount)  ! Scale and Zero for Pgroup
  real(kind=8) :: crval(mcount),cdelt(mcount),crpix(mcount)  ! Axis conversion
  character(len=8) :: ptype(mcount), ctype(mcount)
  !
end module gio_fitsdef
