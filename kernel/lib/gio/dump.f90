subroutine gdf_dump_array1(file,array)
  use image_def
  use gio_interfaces, except_this => gdf_dump_array1
  !
  ! @ public-generic gdf_dump_array 
  ! 
  character(len=*), intent(in) :: file
  real, intent(in) :: array(:)
  !
  type(gildas) :: h
  logical :: error
  !
  call gildas_null(h, type='IMAGE')
  h%gil%ndim = 1
  h%gil%dim(1:1) = ubound(array)-lbound(array)+1
  h%gil%convert(:,1) = 1.d0
  !
  call sic_parse_file(file,' ','.gdf',h%file)
  call gdf_write_image(h,array,error)
  call gdf_close_image(h,error)
end subroutine gdf_dump_array1
!
subroutine gdf_dump_array2(file,array)
  use image_def
  use gio_interfaces, except_this => gdf_dump_array2
  !
  ! @ public-generic gdf_dump_array 
  ! 
  character(len=*), intent(in) :: file
  real, intent(in) :: array(:,:)
  !
  type(gildas) :: h
  logical :: error
  !
  call gildas_null(h, type='IMAGE')
  h%gil%ndim = 2
  h%gil%dim(1:2) = ubound(array)-lbound(array)+1
  h%gil%convert(:,1:2) = 1.d0
  !
  call sic_parse_file(file,' ','.gdf',h%file)
  call gdf_write_image(h,array,error)
  call gdf_close_image(h,error)
end subroutine gdf_dump_array2
!
subroutine gdf_dump_array3(file,array)
  use image_def
  use gio_interfaces, except_this => gdf_dump_array3
  !
  ! @ public-generic gdf_dump_array 
  ! 
  character(len=*), intent(in) :: file
  real, intent(in) :: array(:,:,:)
  !
  type(gildas) :: h
  logical :: error
  !
  call gildas_null(h, type='IMAGE')
  h%gil%ndim = 3
  h%gil%dim(1:3) = ubound(array)-lbound(array)+1
  h%gil%convert(:,1:3) = 1.d0
  !
  call sic_parse_file(file,' ','.gdf',h%file)
  call gdf_write_image(h,array,error)
  call gdf_close_image(h,error)
end subroutine gdf_dump_array3
!
subroutine gdf_dump_array4(file,array)
  use image_def
  use gio_interfaces, except_this => gdf_dump_array4
  !
  ! @ public-generic gdf_dump_array 
  ! 
  character(len=*), intent(in) :: file
  real, intent(in) :: array(:,:,:,:)
  !
  type(gildas) :: h
  logical :: error
  !
  call gildas_null(h, type='IMAGE')
  h%gil%ndim = 4
  h%gil%dim(1:4) = ubound(array)-lbound(array)+1
  h%gil%convert(:,1:4) = 1.d0
  !
  call sic_parse_file(file,' ','.gdf',h%file)
  call gdf_write_image(h,array,error)
  call gdf_close_image(h,error)
end subroutine gdf_dump_array4
!
