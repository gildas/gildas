subroutine gildas_fits_sub(iname,oname,style,nbits,overwrite,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gildas_fits_sub
  use gio_xy
  use gio_fitsdef
  use gbl_message
  !--------------------------------------------------------
  ! @ public
  ! FITS
  !     Convert a GDF file to a FITS file
  !--------------------------------------------------------
  character(len=*), intent(in)    :: iname      ! GILDAS file name
  character(len=*), intent(in)    :: oname      ! FITS file name
  character(len=*), intent(in)    :: style      ! FITS style
  integer(kind=4),  intent(in)    :: nbits      ! Number of bits
  logical,          intent(in)    :: overwrite  ! Overwrite existing file?
  logical,          intent(inout) :: error      ! Error flag
  ! Local
  character(len=filename_length) :: name
  logical :: err
  !
  call gfits_set_nbit(nbits,error)
  if (error) return
  !
  name = oname                 ! Output FITS file name
  gdfname = iname              ! Input GDF file name
  call gfits_set_style(style)  ! Desired style
  !
  call gfits_open (name,'OUT',error,overwrite=overwrite)
  if (error) return
  call tofits(fits,.false.,error)
  if (error) then
    err = .false.
    call gfits_close(err)
    return
  endif
  call gfits_close (error)
end subroutine gildas_fits_sub
!
subroutine gildas_to_fits(hfits,oname,style,nbits,overwrite,error)
  use image_def
  use gio_dependencies_interfaces
  use gio_fitsdef
  use gio_interfaces, except_this=>gildas_to_fits
  !--------------------------------------------------------
  ! @ public
  ! FITS
  !     Write a GDF data set in memory onto a FITS file
  !--------------------------------------------------------
  type(gildas), intent(inout) :: hfits          ! GILDAS Header & Data
  character(len=*), intent(in)    :: oname      ! FITS file name
  character(len=*), intent(in)    :: style      ! FITS style
  integer(kind=4),  intent(in)    :: nbits      ! Number of bits
  logical,          intent(in)    :: overwrite  ! Overwrite existing file?
  logical,          intent(inout) :: error      ! Error flag
  ! Local
  character(len=filename_length) :: name
  logical :: err
  !
  call gfits_set_nbit(nbits,error)
  if (error) return
  !
  gdfname = ' '                ! Void name: do not read any file...
  name = oname                 ! Output FITS file name
  call gfits_set_style(style)  ! Desired style
  call gfits_open (name,'OUT',error,overwrite=overwrite)
  if (error) return
  call tofits(hfits,.false.,error)
  if (error) then
    err = .false.
    call gfits_close(err)
    return
  endif
  call gfits_close (error)
end subroutine gildas_to_fits
!
subroutine gfits_set_nbit(nbits,error)
  use gio_interfaces, only : gio_message
  use gio_fitsdef
  use gbl_message
  !--------------------------------------------------------
  ! @ public
  ! FITS
  !     Set number NBIT in FITS Library
  !--------------------------------------------------------
  integer(kind=4),  intent(in)    :: nbits      ! Number of bits
  logical,          intent(inout) :: error      ! Error flag
  !
  fd%snbit = nbits
  if (fd%snbit.ne.16 .and. fd%snbit.ne.32 .and. fd%snbit.ne.-32) then
    call gio_message(seve%e,'GILDAS_FITS','Invalid number of bits')
    error = .true.
    return
  endif
end subroutine gfits_set_nbit
!
subroutine gfits_set_style(style)
  use gio_dependencies_interfaces
  use gio_fitsdef
  !--------------------------------------------------------
  ! @ public
  ! FITS
  !     Define the FITS style in library
  !--------------------------------------------------------
  character(len=*), intent(in) :: style  ! Style name
  !
  logical :: error
  integer, parameter :: mstyle=5
  character(len=20) :: styles(mstyle),argum,comm
  integer :: ncom
  data styles/'STANDARD','UVFITS','AIPSFITS','SORTED_AIPSFITS','CASA_UVFITS'/
  !
  argum = style
  if (argum.eq.' ') then
    a_style = 0
  else
    call sic_upper(argum)
    call sic_ambigs('STYLE',argum,comm,ncom,styles,mstyle,error)
    if (error) then
      a_style = 0
    else
      a_style = ncom-1
      sort = .false.
      if (a_style.eq.code_fits_sorted) then
        a_style = code_fits_aips
        sort = .true.
      endif
      if (a_style.eq.code_fits_casa) then
        a_style = code_fits_aips
      endif
    endif
  endif
end subroutine gfits_set_style
