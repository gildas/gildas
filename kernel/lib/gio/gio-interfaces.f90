module gio_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GIO interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gio_interfaces_public
  use gio_interfaces_private
  !
end module gio_interfaces
!
module gio_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GIO dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use ginc_interfaces_public
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
  use gwcs_interfaces_public
  use gfits_interfaces_public
end module gio_dependencies_interfaces
