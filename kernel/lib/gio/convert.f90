subroutine gio_syscod (sycode)
  !---------------------------------------------------------------------
  ! @ private
  !   Return the GILDAS "System" code, '-', '_', or '.'
  !---------------------------------------------------------------------
  character(len=1), intent(out) :: sycode   ! Returned code
#if defined(VAX)
  sycode = '_'
#endif
#if defined(IEEE)
  sycode = '-'
#endif
#if defined(EEEI)
  sycode = '.'
#endif
end subroutine gio_syscod
!
subroutine gdf_getcod (csyst)
  !---------------------------------------------------------------------
  ! @ public
  !   Return the "System" code in a comprehensible string. 
  !---------------------------------------------------------------------
  character(len=4), intent(out) :: csyst  ! Code
#if defined(VAX)
  csyst = 'VAX_'
#endif
#if defined(EEEI)
  csyst = 'EEEI'
#endif
#if defined(IEEE)
  csyst = 'IEEE'
#endif
end subroutine gdf_getcod
!
subroutine gdf_convcod (cfile,csyst,conver)
  use gio_interfaces, only : gio_message
  use gio_convert
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  !   Determine the conversion code to be applied
  !   between cfile (the data file) and csyst (the current OS code)
  !   Return 0 if no conversion
  !---------------------------------------------------------------------
  character(len=4), intent(in) :: cfile         ! File Gildas system code
  character(len=4), intent(in) :: csyst         ! Operating system Gildas code
  integer, intent(out) :: conver                ! Conversion code
  !
  conver = unknown
  if (cfile.eq.csyst) then
    conver = 0
  else
    if (cfile.eq.'VAX_') then
      if (csyst.eq.'IEEE') then
        conver = vax_to_ieee
      elseif (csyst.eq.'EEEI') then
        conver = vax_to_eeei
      endif
    elseif (cfile.eq.'IEEE') then
      if (csyst.eq.'VAX_') then
        conver = ieee_to_vax
      elseif (csyst.eq.'EEEI') then
        conver = ieee_to_eeei
      endif
    elseif (cfile.eq.'EEEI') then
      if (csyst.eq.'VAX_') then
        conver = eeei_to_vax
      elseif (csyst.eq.'IEEE') then
        conver = eeei_to_ieee
      endif
    endif
    call gio_message(seve%i,'GDF','Converting from '//cfile//' to '//csyst)
  endif
end subroutine gdf_convcod
!
subroutine gdf_conversion(code,conver)
  use gio_convert
  !---------------------------------------------------------------------
  ! @ public
  !   Translate a Gildas conversion code into a human readable string. 
  !---------------------------------------------------------------------
  integer, intent(in) :: code                   ! Conversion code
  character(len=*), intent(out) :: conver       ! Name of conversion
  ! Local
  character(len=20) :: conversion (0:mconve)
  data conversion /' [Native]',          &
    ' [VAX to IEEE]', ' [IEEE to VAX]',  &
    ' [VAX to EEEI]', ' [EEEI to VAX]',  &
    ' [IEEE to EEEI]',' [EEEI to IEEE]'/
  !
  if (code.ge.0 .and. code.le.mconve) then
    conver = conversion(code)
  else
    conver = '[Unknown]'
  endif
end subroutine gdf_conversion
!
function gdf_conv (in,ou,n,oufmt,infmt)
  use gbl_format
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Perform automatic format conversion for a Buffer
  !---------------------------------------------------------------------
  integer(kind=4) :: gdf_conv       ! Return code (1 Success, 0 Error)
  integer, intent(in) :: n          ! Number of 32-bit Words
  integer, intent(in) :: in(n)      ! Input buffer
  integer, intent(out) :: ou(n)     ! Output buffer
  integer, intent(in) :: oufmt      ! Output format
  integer, intent(in) :: infmt      ! Input format
  gdf_conv = 1
  !
  if (oufmt.eq.iee_r4) then
    if (infmt.eq.vax_r4) then
      call var4ie (in,ou,n)
    elseif (infmt.eq.eei_r4) then
      call eir4ie (in,ou,n)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.iee_r8) then
    if (infmt.eq.vax_r8) then
      call var8ie (in,ou,n/2)
    elseif (infmt.eq.eei_r8) then
      call eir8ie (in,ou,n/2)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.iee_c4) then
    if (infmt.eq.eei_c4) then
      call eir4ie (in,ou,n)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.eei_r4) then
    if (infmt.eq.iee_r4) then
      call ier4ei (in,ou,n)
    elseif (infmt.eq.vax_r4) then
      call var4ei (in,ou,n)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.eei_r8) then
    if (infmt.eq.iee_r8) then
      call ier8ei(in,ou,n/2)
    elseif (infmt.eq.vax_r8) then
      call var8ei (in,ou,n/2)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.eei_c4) then
    if (infmt.eq.iee_c4) then
      call ier4ei (in,ou,n)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.vax_r4) then
    if (infmt.eq.iee_r4) then
      call ier4va (in,ou,n)
    elseif (infmt.eq.eei_r4) then
      call eir4va (in,ou,n)
    elseif (infmt.eq.fmt_i4) then
      call i4tor4 (in,ou,n)
      !         elseif (infmt.eq.vax_r8) then
      !            call r8tor4 (in,ou,n)
    else
      gdf_conv = 0
    endif
  elseif (oufmt.eq.vax_r8) then
    if (infmt.eq.iee_r8) then
      call ier8va (in,ou,n/2)
    elseif (infmt.eq.eei_r8) then
      call eir8va (in,ou,n/2)
      !         elseif (infmt.eq.fmt_i4) then
      !            call i4tor8 (in,ou,n)
      !         elseif (infmt.eq.vax_r4) then
      !            call r4tor8 (in,ou,n)
    else
      gdf_conv = 0
    endif
  else
    gdf_conv = 0
  endif
end function gdf_conv
