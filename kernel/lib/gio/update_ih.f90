subroutine gdf_geih(is,p,error)
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, only : gio_message
  use gio_image
  !---------------------------------------------------------------------
  ! @ public
  ! GDF   GEt a pointer to Image Header
  !       Return the image header for further use
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: is     ! Image Slot
  type(gildas),    pointer       :: p      ! Pointer to header
  logical,         intent(inout) :: error  ! Error flag
  !
  if (islot(is).eq.code_gio_empty .or. islot(is).eq.code_gio_full) then
    call gio_message(seve%e,'GEIH','Image slot is not mapped')
    p => null()
    error = .true.
    return
  endif
  p => gheads(is)
end subroutine gdf_geih
!
subroutine gdf_flih (is,check,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_flih
  use gio_image
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public
  ! GDF   FLush Image Header
  !       FLush an image header
  !---------------------------------------------------------------------
  integer, intent(in) :: is                        ! Image Slot
  logical, intent(in) :: check                     ! Check if slot is writeable
  logical, intent(out) :: error                    ! Error flag
  ! Local
  integer(kind=record_length) :: nvb
  character(len=*), parameter :: rname='FLIH'
  integer(kind=4) :: form, ier
  integer(kind=record_length) :: js
  character(len=12) :: gtype
  !
  ! Check frame is OK
  error = .false.
  if (ichan(is).eq.0) return   ! This is Memory Frame
  !
  if (islot(is).ne.code_gio_write .and.islot(is).ne.code_gio_dumpe ) then
    if (check) then
      call gio_message(seve%e,rname,'Image slot is not Writeable')
      error = .true.
      return
    endif
  endif
  open(unit=iunit(is),file=cname(is),status='OLD',access='DIRECT',  &
       form='UNFORMATTED',action='READWRITE',recl=lenbuf*facunf,iostat=ier)
  if (ier.ne.0) then
    call putios(seve%e,'FLIH(Open)',ier)
    call gio_message(seve%e,rname,'Error opening '//trim(cname(is)))
    goto 100
  endif
  read (unit=iunit(is),rec=1,iostat=ier) gdfbuf
  if (ier.ne.0) then
    call putios(seve%e,'FLIH(Read)',ier)
    goto 99
  endif
  ier = gio_eih (is,gtype,form,nvb)
  if (ier.ne.1) then
    call gio_message(seve%e,rname,'File is not a GILDAS Data Frame')
    goto 99
  endif
  !
  ! Update Header
  !! Print *,'Calling GIO_WIH from gdf_flih ',nvb,istbl(is)
  ier = gio_wih (is,gtype,iform(is),nvb)
  if (ier.eq.0) goto 99
  do js=1,istbl(is)
    write (unit=iunit(is),rec=js,iostat=ier) gdfbig(:,js)
    if (ier.ne.0) then
      call putios(seve%e,'FLIH(Write)',ier)
      goto 99
    endif
  enddo
  !
  ! On LINUX with a Readonly file, the error may happen at the CLOSE
  ! rather than during the WRITE. This is redundant with the OPEN
  ! statement check with ACTION='READWRITE', but does not harm.
  close (unit=iunit(is),iostat=ier)
  if (ier.ne.0) then
    call putios(seve%e,'FLIH(Close)',ier)
    error = .true.
  endif
  !!write(99,*) 'Tested GDF_FLIH'
  return
  !
99 close (unit=iunit(is),iostat=ier)
100 error = .true.
end subroutine gdf_flih
