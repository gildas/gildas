subroutine gio_exis (is,gtype,name,form,size,error)
  use gbl_format
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gio_exis
  use gio_convert
  use gio_image
  !---------------------------------------------------------------------
  ! @ public
  ! GDF EXtend Image Slot
  !     Extend a Greg Data Frame, and return size and address to caller
  !     Used by SIC only
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)    :: is     ! Image slot
  character(len=*),          intent(out)   :: gtype  ! Image type (GILDAS_vwxyz)
  character(len=*),          intent(in)    :: name   ! File name
  integer(kind=4),           intent(out)   :: form   ! Data format
  integer(kind=size_length), intent(inout) :: size   ! Data size
  logical,                   intent(out)   :: error  ! Error flag
  ! Local
  integer(kind=record_length) :: old_nvb,new_nvb,js
  character(len=*), parameter :: rname='EXIS'
  integer(kind=4) :: ier,old_isbig
  integer :: zero(128) = 0
  logical :: allocate_last
  character(len=message_length) :: mess
  !
  ! Check frame is OK
  if (islot(is).eq.code_gio_empty) then
    call gio_message(seve%e,rname,'Image slot is empty')
    error = .true.
    return
  elseif (islot(is).ne.code_gio_full) then
    call gio_message(seve%e,rname,'Image slot is already mapped')
    error = .true.
    return
  endif
  error = .false.
  !
  ! Check header
  ier = sic_getlun (iunit(is))
  if (ier.ne.1) then
    error = .true.
    return
  endif
  open(unit=iunit(is),file=name,status='OLD',form='UNFORMATTED',  &
       action='READWRITE',recl=lenbuf*facunf,access='DIRECT',iostat=ier)
  if (ier.ne.0) then
    call putios(seve%e,'EXIS(Open)',ier)
    goto 99
  endif
  !
  ! Read old header
  read (unit=iunit(is),rec=1,iostat=ier) gdfbuf
  if (ier.ne.0) then
    call putios(seve%e,'EXIS(Read)',ier)
    goto 99
  endif
  ier = gio_eih (is,gtype,form,old_nvb)
  if (ier.ne.1) then
    call gio_message(seve%e,rname,'File is not a GILDAS Data Frame')
    goto 99
  endif
  !
  ! Compute allocation quantity, and extend if required
  new_nvb = gio_block(form,size)   ! New size
  !
  if (istbl(is).eq.1) then
    ! Output file is GDF version 1
    if (new_nvb.gt.ndb_64bit) then
      write (mess,'(A,I0,A,I0,A)')  &
        'Can not extend GDF V1 file: limit reached (',new_nvb,' > ',ndb_64bit,' blocks)'
      call gio_message(seve%e,rname,mess)
      goto 99
    endif
  endif
  !
  ! Perform pre-allocation if needed
  allocate_last = pre_allocate
  !
  old_isbig = isbig(is)
  ! Compute new blocking for efficiency reasons (gio_blocking with argument
  ! .true. to ensure rounding of the Blocking Factor). The logic of
  !       imblock(is) (End of file block)
  !       irblock(is) (last block read)
  !  and  iwblock(is) (last block written)
  ! ensures the end of file is handled properly in all cases
  ! (reading the last big block before completing it if needed).
  isbig(is) = gio_blocking(new_nvb,istbl(is),.true.)
  !
  ! Perform pre-allocation if blocking factor changed
  ! This is no longer needed, given the comment above
  !! if (old_isbig.ne.isbig(is)) allocate_last = .true.
  !
  if (allocate_last) then
    ! Write the last block to make sure all pointers are set
    ! If the extension is not too big, the possible file high-water
    ! marking will not harm too much
    if (new_nvb.gt.old_nvb) then
      js = new_nvb+istbl(is)
      write (unit=iunit(is),rec=js,iostat=ier) zero
      if (ier.ne.0) then
        call putios(seve%e,'EXIS(Extend)',ier)
        goto 99
      endif
      imblock(is) = js 
    endif
  endif
  !
  ! Update Header
  ier = gio_wih (is,gtype,iform(is),new_nvb)
  if (ier.eq.0) goto 99
  !
  do js=1,istbl(is)
    write(unit=iunit(is),rec=js) gdfbig(:,js)
  enddo
  !
  call gio_idim (is,size)
  ichan(is) = iunit(is)
  cname(is) = name             ! Remember name
  close(unit=iunit(is))        ! Close the file
  !
  islot(is) = code_gio_write
  return
  !
99 call gdf_deis(is,error)
  error = .true.
end subroutine gio_exis
!
subroutine gdf_extend_image (x,newdim,error)
  use image_def
  use gbl_format
  use gio_interfaces, except_this=>gdf_extend_image
  !---------------------------------------------------------------------
  ! @ public
  ! GDF API routine
  !     Extend an existing image by increasing its LAST dimension
  !---------------------------------------------------------------------
  type(gildas),               intent(inout) :: x       ! Gildas Image
  integer(kind=index_length)                :: newdim  ! New value of last dimension
  logical,                    intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=12) :: gtype
  integer :: is,form,idime
  integer(kind=size_length) :: osize,nsize
  !
  call gio_geis (is,error)
  if (error) return
  call gio_wris (is,gtype,x%file,form,osize,error)
  if (error) return
  call gio_read_header(x,is,error)
  if (error) return
  !
  ! Now extend it if needed
  if (x%gil%ndim.lt.1) then
    x%gil%ndim = 1
    x%gil%dim(1) = 1
    x%gil%convert = 1.d0
  endif
  !
  if (newdim.gt.x%gil%dim(x%gil%ndim)) then
    x%gil%dim(x%gil%ndim) = newdim
    call gio_write_header(x,is,error)
    if (error) return
    call gio_clis (is,error)     ! Close image slot
    !
    nsize = 1
    do idime=1,x%gil%ndim
      nsize = nsize*x%gil%dim(idime)
    enddo
    call gio_exis (is,gtype,x%file,form,nsize,error)
    if (error) return
  endif
  !
  ! Set the image slot
  x%status = 0
  x%loca%islo = is
end subroutine gdf_extend_image
