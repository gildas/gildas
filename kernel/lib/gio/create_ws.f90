subroutine gio_crws (is,gtype,form,size,error)
  use gio_interfaces, except_this=>gio_crws
  use gio_image
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! GDF CReate Work Slot
  !     This is a pure virtual memory slot
  !     Used by SIC only
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: is     ! Image slot
  character(len=*),          intent(in)  :: gtype  ! Image type (GILDAS_vwxyz)
  integer(kind=4),           intent(in)  :: form   ! Data format
  integer(kind=size_length), intent(in)  :: size   ! Data size
  logical,                   intent(out) :: error  ! Error flag
  ! Local
  integer(kind=size_length) :: jsize
  integer(kind=record_length) :: leng
  character(len=*), parameter :: rname='GIO_CRWS'
  integer :: ier
  !
  jsize = size ! Test maximum size here
  !
  error = .true.
  if (islot(is).eq.code_gio_empty) then
    call gio_message(seve%e,rname,'Image slot is empty')
    return
  elseif (islot(is).ne.code_gio_full) then
    call gio_message(seve%e,rname,'Image slot is already mapped')
    return
  endif
  leng = gio_block(form,jsize)
  !
  if (leng.eq.0) then
    ! This is actually acceptable for DEFINE HEADER A *
    ! but the Header must be initialized here, while
    ! it has already been initialized otherwise.
    if (gtype(1:9).eq.'GILDAS_UV') then
      call gildas_null(gheads(is),type='UVT')
    else
      call gildas_null(gheads(is))
    endif
  endif
  !
  ! Write header
  iconv(is) = 0
  ier = gio_wih (is,gtype,form,leng)
  if (ier.eq.0) return
  !
  ! Now set the proper dimensions
  call gio_idim (is,jsize)
  iform(is) = form
  ichan(is) = 0
  islot(is) = code_gio_write
  error = .false.
end subroutine gio_crws
