module gio_interfaces_none
  interface
    subroutine gdf_prealloc(state)
      use gio_image
      use gbl_message
      !
      logical, intent(in) :: state
    end subroutine gdf_prealloc
  end interface
  !
end module gio_interfaces_none
