subroutine gio_cris (is,gtype,name,form,size,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gio_cris
  use gio_image
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public
  ! GDF CReate Image Slot  - Used by SIC only
  !---------------------------------------------------------------------
  integer(4), intent(in)  :: is                  ! Image slot
  character(len=*), intent(in)  :: gtype         ! Image type (GILDAS_vwxyz)
  character(len=*), intent(in)  :: name          ! File name
  integer(4), intent(in)  :: form                ! Data format
  integer(kind=size_length), intent(in)  :: size ! Data size
  logical, intent(out) :: error                  ! Error flag
  ! Local
  integer(kind=size_length) :: jsize
  integer(kind=record_length):: leng, js
  character(len=*), parameter :: rname='CRIS'
  character(len=24) :: chain
  logical :: exist
  integer(kind=4) :: ier
  integer :: zero(128)=0
  !
  if (islot(is).eq.code_gio_empty) then
    call gio_message(seve%e,rname,'Image slot is empty')
    error = .true.
    return
  elseif (islot(is).ne.code_gio_full) then
    call gio_message(seve%e,rname,'Image slot is already mapped')
    error = .true.
    return
  endif
  !
  jsize = size ! Test maximum size here
  leng = gio_block(form,jsize)
  if (leng.le.0) then
    write(chain,*) leng
    call gio_message(seve%e,rname,'Image size is null or negative ' //chain)
    call gio_dump_header(gheads(is))
    error = .true.
    return
  endif
  !
  if (gtype.eq.'GILDAS_UVDAT'.or.gtype.eq.'GILDAS_UVSOR') then
    call gio_message(seve%e,rname,'UVDAT format is obsolete, use GILDAS_UVFIL')
    error = .true.
    return
  endif
  !
  ! Round to LENBIG word multiples i.e. BLOCKING blocks for efficiency reason
  isbig(is) = gio_blocking(leng,istbl(is),.true.)
  error = .false.
  !
  ier = sic_getlun(iunit(is))
  if (ier.ne.1) then
    error = .true.
    return
  endif
  !
  ! Create image
  inquire (file=name,exist=exist)
  if (exist)  call gag_filrm(name)
  open(unit=iunit(is),file=name,status='UNKNOWN',form='UNFORMATTED',  &
       action='READWRITE',access='DIRECT',recl=lenbuf*facunf,iostat=ier)
  if (ier.ne.0) then
    call putios(seve%e,'CIS(Open)',ier)
    goto 99
  endif
  !
  ! Write the last block to ensure file integrity
  if (pre_allocate) then
    ! However, on some systems, this implies writing (or "high-water marking")
    ! the whole file, so it is inefficient. Do not to it.
    js = leng+istbl(is)
    write (unit=iunit(is),rec=js,iostat=ier) zero
    if (ier.ne.0) then
      call putios(seve%e,'E-CIS(Create)',ier)
      goto 99
    endif
    imblock(is) = js
  else
    !
    ! Instead write only the first blocked set of records
    ! which includes the Header blocks
    js = istbl(is)/isbig(is)                      ! Number of blocks
    if (mod(istbl(is),isbig(is)).ne.0) js = js+1  ! Make sure it fits
    js = isbig(is) * js                           ! Number of records
    write (unit=iunit(is),rec=js,iostat=ier) zero
    imblock(is) = js
  endif
  !
  ! Set "No Conversion" for output images
  iconv(is) = 0
  ier = gio_wih (is,gtype,form,leng)
  if (ier.eq.0) goto 99
  !
  do js=1,istbl(is)
    write(unit=iunit(is),rec=js,iostat=ier) gdfbig(:,js)
    if (ier.ne.0) then
      call putios(seve%e,'CIS(Create)',ier)
      goto 99
    endif
  enddo
  imblock(is) = max(istbl(is),imblock(is))
  !
  close(unit=iunit(is))
  cname(is) = name
  iform(is) = form
  !
  call gio_idim (is,jsize) ! Useless ?
  !
  ichan(is) = iunit(is)
  islot(is) = code_gio_write
  error = .false.
  return
  !
99 close (unit=iunit(is))
  call sic_frelun (iunit(is))
  iunit(is) = 0
  error = .true.
end subroutine gio_cris
!
function gio_blocking(leng,startbl,new)
  use gildas_def
  use gbl_message
  use gio_params
  !------------------------------------------------------------------------
  ! @ public
  !
  !   Return best blocking factor for new file
  !------------------------------------------------------------------------
  integer(kind=4) :: gio_blocking  ! intent(out)   ! Blocking factor
  integer(kind=size_length), intent(inout) :: leng ! Number of 512 Byte blocks
  integer(kind=4) :: startbl                       ! Number of starting blocks
  logical(kind=4) :: new                           ! New or old file ?
  !
  character(len=*), parameter :: rname='GIO_BLOCKING'
  character(len=80) :: mess
  !
  if (new) then
    ! Select the blocking factor according to size, to avoid losing
    ! space for small files, but be efficient for large ones
    if (leng.gt.gio_block1*gio_block2*8) then
      gio_blocking = gio_block1*gio_block2  ! Newly allowed for V2.1
    else
      gio_blocking = gio_block1    ! This is the default since V1 and V2.0
    endif
    !
    if (mod(leng+startbl,gio_blocking).ne.0) then
      leng = gio_blocking * ((leng+startbl)/gio_blocking + 1) - startbl
    endif
    write(mess,*) 'Blocking factor ',gio_blocking,' selected'
  else
    !
    ! Check the blocking factor
    if (mod(leng+startbl,gio_block1*gio_block2).eq.0) then
      gio_blocking = gio_block1*gio_block2
    else if (mod(leng+startbl,gio_block1).eq.0) then
      gio_blocking = gio_block1
    else
      gio_blocking = 1
    endif
    write(mess,*) 'Blocking factor ',gio_blocking,' found in file'
  endif
  call gio_message(seve%d,rname,mess)
end function gio_blocking

subroutine gdf_prealloc(state)
  use gio_image
  use gbl_message
  !
  logical, intent(in) :: state
  character(len=*), parameter :: rname='GDF_PREALLOC'
  !
  pre_allocate = state
  if (pre_allocate) then
    call gio_message(seve%i,rname,'Use pre-allocation')
  else
    call gio_message(seve%i,rname,'No pre-allocation')
  endif
end subroutine gdf_prealloc
