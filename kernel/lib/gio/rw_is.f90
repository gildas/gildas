subroutine gio_reis (is,gtype,name,form,size,error)
  use gildas_def
  use gio_interfaces, only : gio_rewris
  !---------------------------------------------------------------------
  ! @ public
  ! GDF   REad Image Slot
  !   Only used by SIC
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)    :: is     ! Image SLot
  character(len=*),          intent(out)   :: gtype  ! Image type
  character(len=*),          intent(in)    :: name   ! File name
  integer(kind=4),           intent(out)   :: form   ! Data type
  integer(kind=size_length), intent(out)   :: size   ! Data size
  logical,                   intent(inout) :: error  ! Error flag
  !
  call gio_rewris(.true.,is,gtype,name,form,size,error)
end subroutine gio_reis
!
subroutine gio_wris (is,gtype,name,form,size,error)
  use gildas_def
  use gio_interfaces, only : gio_rewris
  !----------------------------------------------------------------------
  ! @ public
  ! GDF   WRite Image Slot
  !   Only used by SIC
  !----------------------------------------------------------------------
  integer(kind=4),           intent(in)    :: is     ! Image SLot
  character(len=*),          intent(out)   :: gtype  ! Image type
  character(len=*),          intent(in)    :: name   ! File name
  integer(kind=4),           intent(out)   :: form   ! Data type
  integer(kind=size_length), intent(out)   :: size   ! Data size
  logical,                   intent(inout) :: error  ! Error flag
  !
  call gio_rewris(.false.,is,gtype,name,form,size,error)
end subroutine gio_wris
!
subroutine gio_rewris(rdonly,is,gtype,name,form,size,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gio_rewris
  use gildas_def
  use gio_image
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! GDF   REad or WRite Image Slot. Common part for gio_reis and
  !       gio_wris, do not use directly.
  !---------------------------------------------------------------------
  logical,                   intent(in)  :: rdonly  ! Read or Write mode?
  integer(kind=4),           intent(in)  :: is      ! Image Slot
  character(len=*),          intent(out) :: gtype   ! Image type
  character(len=*),          intent(in)  :: name    ! File name
  integer(kind=4),           intent(out) :: form    ! Data type
  integer(kind=size_length), intent(out) :: size    ! Data size
  logical,                   intent(out) :: error   ! Error flag
  ! Local
  integer(kind=size_length) :: jsize
  integer(kind=record_length) :: leng
  integer :: ier
  character(len=3) :: rname
  character(len=30) :: cfits
  character(len=10) :: action
  character(len=filename_length) :: file,curdir
  integer :: curlen
  integer(4) :: fitbuf(8)
  equivalence (fitbuf,cfits)
  logical :: is_fits, probably_fits
  save curlen,curdir  ! Needed to delete temporary file in case of FITS file.
  !
  if (rdonly) then
    rname = 'RIS'
    action = 'READ'
  else
    rname = 'WIS'
    action = 'READWRITE'
  endif
  is_fits = .false.
  file = name
  !
10 continue
  if (islot(is).eq.code_gio_empty) then
    call gio_message(seve%e,rname,'Image slot is empty')
    error = .true.
    return
  elseif (islot(is).ne.code_gio_full) then
    call gio_message(seve%e,rname,'Image slot is already mapped')
    error = .true.
    return
  endif
  error = .false.
  !
  ! Check header
  ier = sic_getlun(iunit(is))
  if (ier.ne.1) then
    error = .true.
    return
  endif
  open(unit=iunit(is),file=file,status='OLD',form='UNFORMATTED',  &
       access='DIRECT',action=action,recl=lenbuf*facunf,iostat=ier)
  if (ier.ne.0) then
    call putios(seve%e,rname//'(Open)',ier)
    call gio_message(seve%e,rname,'Error opening '//file)
    goto 99
  endif
  read (unit=iunit(is),rec=1,iostat=ier) gdfbuf
  ichan(is) = iunit(is)
  if (ier.ne.0) then
    call putios(seve%e,rname//'(Read)',ier)
    call gio_message(seve%e,rname,'Error reading '//file)
    goto 99
  endif
  !
  ier = gio_rih (is,gtype,form,leng)
  !! print *,'After gdf_rih : DIMENSIONS ',gheads(is)%gil%dim ,' Header ', gheads(is)%header
  !
  if (ier.ne.1) then
    if (rdonly .and. .not.is_fits) then
      call bytoby (gdfbuf,fitbuf,30)
      if (cfits.eq.'SIMPLE  =                    T') then
        probably_fits = .true.
      elseif(cfits(1:10).eq.'SIMPLE  = ') then
        call gio_message(seve%e,rname,'Non standard FITS file found')
        call gio_message(seve%e,rname,'Will nonetheless try to proceed')
        probably_fits = .true.
      else
        probably_fits = .false.
      endif
      if (probably_fits) then
        ! where are we?
        curlen = 0
        call sic_setdir(curdir,curlen,error)
        ! Close file as it needs to be reopened for conversion inside fits2gdf.
        close(unit=iunit(is))
        call sic_frelun(iunit(is))
     		iunit(is) = 0
        ! Tries SIMPLE fits to gdf translation.
        call fits2gdf(file,error)
        if (.not.error) then
          is_fits = .true.
          goto 10
        endif
      endif
    endif
    call gio_message(seve%e,rname,'File '//trim(file)//' is neither a '//  &
    'GILDAS data frame nor a SIMPLE FITS file')
    goto 99
  endif
  call gio_idim (is,jsize)
  size = jsize ! Test maximum size here
  !
  if (rdonly) then
    islot(is) = code_gio_reado
  else
    islot(is) = code_gio_write
    ! Forbid using 'GILDAS_UVDAT' as output
    if (abs(ivers(is)).eq.code_gdf_uvold) then
      call gio_message(seve%e,'GDF_RWIS','UVDAT format obsolete for '//  &
      'output, use UVT_CONVERT first')
      close(unit=iunit(is))
      call gdf_deis(is,error)
      error = .true.
      return
    endif
  endif
  cname(is) = file             ! #1
  close (unit=iunit(is))       ! #1
  return
  !
99 call gdf_deis (is,error)
  error = .true.
  return
  !
end subroutine gio_rewris
!
subroutine gag_file_guess_fromname(caller,filename,filekind,error)
  use gbl_format
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gag_file_guess_fromname
  !---------------------------------------------------------------------
  ! @ public-generic gag_file_guess
  ! Guess if the input file is a GDF file (code 1), a FITS file (code
  ! 2), or something else (code 0)
  ! ---
  ! This version with the file name as argument, to be Fortran-opened-
  ! and-closed during the guess
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller
  character(len=*), intent(in)    :: filename
  integer(kind=4),  intent(out)   :: filekind
  logical,          intent(inout) :: error
  ! Local
  integer(kind=4) :: ier,lun,reclen
  !
  filekind = 0  ! Code 0 in case of early exit
  !
  ier = sic_getlun(lun)
  if (mod(ier,2).eq.0)  return  ! Do not to be cleverer!
  !
  reclen = 3  ! Use 3-words record length as this is only what we need
              ! in gag_file_guess_fromlun
  open(unit=lun,            &
       file=filename,       &
       access='DIRECT',     &
       form='UNFORMATTED',  &
       recl=facunf*reclen,  &
       status='OLD',        &
       action='READ',       &
       iostat=ier)
  !
  if (ier.eq.0) then
    call gag_file_guess_fromlun(caller,lun,filekind,error)
    if (error)  continue
    close(unit=lun)
  else
    call gio_message(seve%e,caller,'Error opening file '//filename)
    call putios('E-'//caller//',  ',ier)
    error = .true.
    continue
  endif
  !
  call sic_frelun(lun)
  !
end subroutine gag_file_guess_fromname
!
subroutine gag_file_guess_fromlun(caller,lun,filekind,error)
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gag_file_guess_fromlun
  !---------------------------------------------------------------------
  ! @ public-generic gag_file_guess
  ! Guess if the input file is a GDF file (code 1), a FITS file (code
  ! 2), or something else (code 0)
  ! ---
  ! This version with the file name already opened in input logical unit
  ! (must be opened as OLD, DIRECT, UNFORMATTED).
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller
  integer(kind=4),  intent(in)    :: lun
  integer(kind=4),  intent(out)   :: filekind
  logical,          intent(inout) :: error
  ! Local
  integer(kind=4) :: ier,buf(3)  ! 3 words are enough for 10 characters
  character(len=10) :: start
  character(len=filename_length) :: filename
  !
  filekind = 0
  !
  read(lun,rec=1,iostat=ier) buf
  if (ier.ne.0) then
    inquire(unit=lun,name=filename)
    call gio_message(seve%e,caller,'Error reading file '//filename)
    call putios('E-'//caller//',  ',ier)
    error = .true.
    return
  endif
  !
  call bytoch(buf,start,10)
  if (start(1:6).eq.'GILDAS') then  ! GDF
    filekind = 1
    return
  elseif (start(1:10).eq.'SIMPLE  = ') then  ! FITS
    filekind = 2
    return
  endif
  !
  ! There is no point checking more: the corresponding reader will (or
  ! should) perform all the necessary tests to check the file format
  ! consistency.
  !
end subroutine gag_file_guess_fromlun
