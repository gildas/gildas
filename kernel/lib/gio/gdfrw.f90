!-----------------------------------------------------------------------
! Basic GDF_READ / WRIT routines
! ---
! OBSOLESCENT: DO NOT USE FOR ANY NEW CODE!
!-----------------------------------------------------------------------
subroutine gdf_read(x,islo,error)
  use gio_interfaces, only : gio_read_header 
  use gildas_def
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  !  Can not use 'gdf_read_header' since it allocates an image slot
  ! while the caller to the present routine have done this by itself
  ! (and from time to time X image is mixed with a Y slot...)
  ! Use GIO_READ_HEADER to do this now...
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: x      !
  integer,      intent(in)    :: islo   !
  logical,      intent(inout) :: error  !
  !
  call gio_read_header(x,islo,error)
end subroutine gdf_read
!
subroutine gdf_writ(x,islo,error)
  use gio_interfaces, only : gio_write_header
  use gildas_def
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: x      !
  integer,      intent(in)    :: islo   !
  logical,      intent(inout) :: error  !
  !
  call gio_write_header(x,islo,error)
end subroutine gdf_writ
!
subroutine gdf_ch(input,output)
  use gio_interfaces, only : gio_init_gildas, gdf_copy_gil
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  ! Copy Header IN into OUT
  !---------------------------------------------------------------------
  type(gildas), target, intent(inout) :: input 
  type(gildas), target, intent(inout) :: output 
  !
  logical :: error
  ! Check %ref Pointers
  call gio_init_gildas(input, 'GDF_CH') 
  call gio_init_gildas(output, 'GDF_CH') 
  output%file = input%file
  output%char = input%char
  output%loca = input%loca
  error = .false.
  call gdf_copy_gil (input,output, error)
end subroutine gdf_ch
