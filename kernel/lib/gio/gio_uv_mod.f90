!
module gio_uv
  use gio_params
  use gio_headers
  !
  type uv_head_type_t
     integer(kind=4) :: sort_order = code_null ! Sort order if any...
     integer(kind=8) :: ndata = 0     ! Number of read visibilities
     integer(kind=4) :: nchan = 0     ! Number of read channels
     integer(kind=4) :: range(2) = 0  ! First and last read channels
     real(kind=4) :: xmin  = +1e38 ! [rad]
     real(kind=4) :: xmax  = -1e38 ! [rad]
     real(kind=4) :: ymin  = +1e38 ! [rad]
     real(kind=4) :: ymax  = -1e38 ! [rad]
     real(kind=4) :: umin  = +1e38 ! [  m]
     real(kind=4) :: umax  = -1e38 ! [  m]
     real(kind=4) :: vmin  = +1e38 ! [  m]
     real(kind=4) :: vmax  = -1e38 ! [  m]
     real(kind=4) :: uvmax =   0.0 ! [  m]
     real(kind=4) :: uvmin = +1e38 ! [  m]
  end type uv_head_type_t
  !
  type uv_head_t
     type(gildas_v2)       :: gil  ! File header
     type(uv_head_type_t)  :: uvt  ! Type header
     real(kind=4), pointer :: u(:)    => null() ! [  m] U                    
     real(kind=4), pointer :: v(:)    => null() ! [  m] V                    
     real(kind=4), pointer :: w(:)    => null() ! [  m] W                    
     real(kind=4), pointer :: x(:)    => null() ! [rad] X pointing offset
     real(kind=4), pointer :: y(:)    => null() ! [rad] Y pointing offset
     real(kind=4), pointer :: l(:)    => null() ! [rad] L phase offset
     real(kind=4), pointer :: m(:)    => null() ! [rad] M phase offset
     real(kind=4), pointer :: scan(:) => null() ! [---] Scan                 
     real(kind=4), pointer :: date(:) => null() ! [gag] Observing date       
     real(kind=4), pointer :: time(:) => null() ! [sec] Time                 
     real(kind=4), pointer :: anti(:) => null() ! [---] First antenna number 
     real(kind=4), pointer :: antj(:) => null() ! [---] Second antenna number
     ! etc...
  end type uv_head_t
  !
  type uv_buffer_t
     real(kind=4), pointer :: uvp1(:,:)   => null() ! Columns 1 to nlead   => dimensions: nvisi x nlead
     real(kind=4), pointer :: uvp2(:,:,:) => null() ! Columns nlead+1 to ... 
     !             => dimensions: nvisi x 3 x (nchan x nstokes) for tuv order                                           
     !             => dimensions: 3 x (nchan x nstokes) x nvisi for uvt order
     real(kind=4), pointer :: uvp3(:,:)   => null() ! Columns n-ntrail+1 to n    => dimensions: nvisi x ntrail 
  end type uv_buffer_t
  !
  type uv_t
     type(uv_buffer_t) :: buff
     type(uv_head_t)   :: head
     real(kind=4), pointer :: data(:,:,:)     => null() ! ndata x (Real [Jy], Imaginary [Jy], Weight [???]) x nchan
     real(kind=4), pointer :: stokes(:,:,:,:) => null() ! ndata x (Real [Jy], Imaginary [Jy], Weight [???]) x nchan x nstokes
  end type uv_t

end module gio_uv
!

