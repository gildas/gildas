subroutine gdf_set_uvt_version(name)
  use image_def
  use gio_uv
  use gbl_message
  use gio_dependencies_interfaces
  !----------------------------------------------------
  ! @ public
  !   Select GDF data format version beyond 2.0
  !----------------------------------------------------
  character(len=*), intent(in) :: name
  !
  character(len=16) :: uname
  character(len=60) :: chain
  !
  uname = name
  call sic_upper(uname)
  select case(uname)
  case('2.0')
    code_version_uvt_current = code_version_uvt_v20
  case('2.1')
    code_version_uvt_current = code_version_uvt_v21
  case('2.2')
    code_version_uvt_current = code_version_uvt_v22
  case('*')
    code_version_uvt_current = code_version_uvt_default
  case('FREQUENCY')
    code_version_uvt_current = code_version_uvt_freq
  case('DOPPLER')
    code_version_uvt_current = code_version_uvt_dopp
  case('SYSTEM')
    code_version_uvt_current = code_version_uvt_syst
  case(' ')
    continue
  case default
    call gio_message(seve%w,'GDF','Unsupported UVT version '//uname)
  end select
  !
  select case(code_version_uvt_current)
  case (code_version_uvt_freq)
    write(chain,'(A,F3.1)') 'UV version Frequency V',0.1*code_version_uvt_current
  case (code_version_uvt_dopp)
    write(chain,'(A,F3.1)') 'UV version Doppler V',0.1*code_version_uvt_current
  case (code_version_uvt_syst)
    write(chain,'(A,F3.1)') 'UV version System V',0.1*code_version_uvt_current
  case default
    write(chain,'(A,F3.1,A)') 'UV version V',0.1*code_version_uvt_current, &
    & ' (yet undocumented)'
  end select
  call gio_message(seve%i,'GDF',chain)
end subroutine gdf_set_uvt_version
!
subroutine gdf_get_uvt_version(code)
  use gio_uv
  !----------------------------------------------------
  ! @ public
  !   Get UVT data format version beyond 2.0
  !----------------------------------------------------
  integer(kind=4), intent(out) :: code
  code = code_version_uvt_current
end subroutine gdf_get_uvt_version
