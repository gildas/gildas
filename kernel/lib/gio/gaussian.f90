subroutine gdf_gauss2d_convolution(h,a2,b2,t2,error)
  use image_def
  use phys_const
  use gio_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public
  !   Convolve the resolution section of the input header by a 2D
  ! gaussian. Results are saved in the header itself.
  !   All input and output angles use GDF convention i.e. from North to
  ! East. The convolution position angle should be defined in the same
  ! frame as the input header is defined.
  !---------------------------------------------------------------------
  type(gildaS), intent(inout) :: h      !
  real(kind=4), intent(in)    :: a2     ! [same as h%gil%major] Convolution major axis
  real(kind=4), intent(in)    :: b2     ! [same as h%gil%major] Convolution minor axis
  real(kind=4), intent(in)    :: t2     ! [rad]                 Convolution position angle
  logical,      intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: a3,b3,u1,u2,u3,shift,sig
  !
  ! If no resolution section, nothing to be done
  if (h%gil%reso_words.le.0)  return
  !
  ! Need to convert GDF angles to Fortran/math convention
  if (h%gil%inc(h%gil%yaxi).gt.0.d0) then
    shift = +pi/2.
  else
    shift = -pi/2.
  endif
  if (h%gil%inc(h%gil%xaxi)*h%gil%inc(h%gil%yaxi).lt.0.d0) then
    sig = +1.
  else
    sig = -1.
  endif
  !
  u1 = sig*h%gil%posa + shift
  u2 = sig*t2 + shift
  !
  call gauss2d_convolution(h%gil%majo,h%gil%mino,u1,  &
                           a2,b2,u2,  &
                           a3,b3,u3,error)
  if (error)  return
  !
  h%gil%majo = a3
  h%gil%mino = b3
  h%gil%posa = sig*(u3 - shift)  ! Back to GDF convention
  !
  ! Angle modulated in [-pi/2,pi/2], because the gaussian is kept identical
  ! with a pi rotation
  h%gil%posa = modulo(h%gil%posa+pi/2.,pi)-pi/2.
  !
end subroutine gdf_gauss2d_convolution
!
subroutine gdf_gauss2d_deconvolution(h,a3,b3,t3,a2,b2,t2,error)
  use image_def
  use phys_const
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_gauss2d_deconvolution
  !---------------------------------------------------------------------
  ! @ public
  !   Compute the convolution kernel needed to reach the desired
  ! resolution starting from the input header resolution section.
  !   All input and output angles use GDF convention i.e. from North to
  ! East. The goal position angle should be defined in the same
  ! frame as the input header is defined.
  !---------------------------------------------------------------------
  type(gildaS), intent(in)    :: h      !
  real(kind=4), intent(in)    :: a3     ! [same as h%gil%major] Goal major axis
  real(kind=4), intent(in)    :: b3     ! [same as h%gil%major] Goal minor axis
  real(kind=4), intent(in)    :: t3     ! [rad]                 Goal position angle
  real(kind=4), intent(out)   :: a2     ! [same as h%gil%major] Convolution major axis
  real(kind=4), intent(out)   :: b2     ! [same as h%gil%major] Convolution minor axis
  real(kind=4), intent(out)   :: t2     ! [rad]                 Convolution position angle
  logical,      intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GAUSS2D'
  real(kind=4) :: u1,u2,u3,shift,sig
  !
  if (h%gil%reso_words.le.0) then
    call gio_message(seve%e,rname,'No resolution section')
    error = .true.
    return
  endif
  !
  ! Need to convert GDF angles to Fortran/math convention
  if (h%gil%inc(h%gil%yaxi).gt.0.d0) then
    shift = +pi/2.
  else
    shift = -pi/2.
  endif
  if (h%gil%inc(h%gil%xaxi)*h%gil%inc(h%gil%yaxi).lt.0.d0) then
    sig = +1.
  else
    sig = -1.
  endif
  !
  u1 = sig*h%gil%posa + shift
  u3 = sig*t3 + shift
  !
  call gauss2d_deconvolution(h%gil%majo,h%gil%mino,u1,  &
                             a3,b3,u3,  &
                             a2,b2,u2,error)
  if (error)  return
  !
  ! Back to GDF convention
  t2 = sig*(u2-shift)
  !
  ! Angle modulated in [-pi,pi]
  t2 = modulo(t2+pi,twopi)-pi
  !
end subroutine gdf_gauss2d_deconvolution
