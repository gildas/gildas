module gio_interfaces_public
  interface
    subroutine gio_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine gio_message_set_id
  end interface
  !
  interface
    subroutine gdf_setuv(h,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Define the UV section consistently.
      ! It could also define the column_types and column_codes if needed
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: h
      logical,      intent(inout) :: error
    end subroutine gdf_setuv
  end interface
  !
  interface
    subroutine gdf_uv_shift_columns(huvin,huvou)
      use image_def
      !---------------------------------------------------------------------
      ! @ public
      !   Shift the trailing columns when copying and modifying
      !   a UV table (channel extraction, Stokes reduction, etc...)
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: huvin
      type(gildas), intent(inout) :: huvou
    end subroutine gdf_uv_shift_columns
  end interface
  !
  interface
    subroutine gdf_index_to_where(inone,ndim,dims,iwhere)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      ! GDF Convert a (rank-1) position in a pixel number for arbitrary dimensions
      ! Will need an INTERFACE from outside
      !---------------------------------------------------------------------
      integer(kind=8), intent(in)  :: inone         ! Relative position in rank-1 array
      integer(kind=4), intent(in)  :: ndim          ! Number of dimensions
      integer(kind=index_length), intent(in)  :: dims(ndim)    ! Number of dimensions
      integer(kind=index_length), intent(out) :: iwhere(ndim)  ! Pixel position
    end subroutine gdf_index_to_where
  end interface
  !
  interface
    subroutine gdf_where_to_index(inone,ndim,dims,iwhere)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      ! GDF Convert a pixel number in a (rank-1) position for arbitrary dimensions
      ! Will need an INTERFACE from outside
      !---------------------------------------------------------------------
      integer(kind=8), intent(out) :: inone         ! Relative position in rank-1 array
      integer(kind=4), intent(in)  :: ndim          ! Number of dimensions
      integer(kind=index_length), intent(in)  :: dims(ndim)    ! Number of dimensions
      integer(kind=index_length), intent(in)  :: iwhere(ndim)  ! Pixel position
    end subroutine gdf_where_to_index
  end interface
  !
  interface
    subroutine gdf_dims(is,ndim,dims)
      use gildas_def
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! GDF  API routine
      !             Return image dimensions (for SIC, in fact)
      ! The output dimensions are kind=index_length
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: is                  ! Image slot
      integer(kind=4), intent(out) :: ndim               ! Number of dimensions
      integer(kind=index_length), intent(out) :: dims(:) ! Dimensions
    end subroutine gdf_dims
  end interface
  !
  interface
    function gdf_check_nvisi(is,mess)
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! Return value:
      !  = 0 if visibility axis exists and Nvisi is equal to this axis, or
      !      if visibility axis does not exist
      !  < 0 if visibility axis exists and Nvisi is valid (lower than this
      !      axis: 0<=Nvisi<axis. Note 0 is still valid).
      !  > 0 if visibility axis exists but Nvisi is invalid (greater than
      !      this axis, or negative)
      ! A message is also set describing this status. The caller can use it
      ! or ignore it.
      !---------------------------------------------------------------------
      integer(kind=4) :: gdf_check_nvisi
      integer(kind=4),  intent(in)  :: is    ! Image slot
      character(len=*), intent(out) :: mess  ! Message in return
    end function gdf_check_nvisi
  end interface
  !
  interface
    subroutine gdf_getcod (csyst)
      !---------------------------------------------------------------------
      ! @ public
      !   Return the "System" code in a comprehensible string. 
      !---------------------------------------------------------------------
      character(len=4), intent(out) :: csyst  ! Code
    end subroutine gdf_getcod
  end interface
  !
  interface
    subroutine gdf_convcod (cfile,csyst,conver)
      use gio_convert
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Determine the conversion code to be applied
      !   between cfile (the data file) and csyst (the current OS code)
      !   Return 0 if no conversion
      !---------------------------------------------------------------------
      character(len=4), intent(in) :: cfile         ! File Gildas system code
      character(len=4), intent(in) :: csyst         ! Operating system Gildas code
      integer, intent(out) :: conver                ! Conversion code
    end subroutine gdf_convcod
  end interface
  !
  interface
    subroutine gdf_conversion(code,conver)
      use gio_convert
      !---------------------------------------------------------------------
      ! @ public
      !   Translate a Gildas conversion code into a human readable string. 
      !---------------------------------------------------------------------
      integer, intent(in) :: code                   ! Conversion code
      character(len=*), intent(out) :: conver       ! Name of conversion
    end subroutine gdf_conversion
  end interface
  !
  interface
    subroutine gio_cris (is,gtype,name,form,size,error)
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! GDF CReate Image Slot  - Used by SIC only
      !---------------------------------------------------------------------
      integer(4), intent(in)  :: is                  ! Image slot
      character(len=*), intent(in)  :: gtype         ! Image type (GILDAS_vwxyz)
      character(len=*), intent(in)  :: name          ! File name
      integer(4), intent(in)  :: form                ! Data format
      integer(kind=size_length), intent(in)  :: size ! Data size
      logical, intent(out) :: error                  ! Error flag
    end subroutine gio_cris
  end interface
  !
  interface
    function gio_blocking(leng,startbl,new)
      use gildas_def
      use gbl_message
      use gio_params
      !------------------------------------------------------------------------
      ! @ public
      !
      !   Return best blocking factor for new file
      !------------------------------------------------------------------------
      integer(kind=4) :: gio_blocking  ! intent(out)   ! Blocking factor
      integer(kind=size_length), intent(inout) :: leng ! Number of 512 Byte blocks
      integer(kind=4) :: startbl                       ! Number of starting blocks
      logical(kind=4) :: new                           ! New or old file ?
    end function gio_blocking
  end interface
  !
  interface
    subroutine gio_crws (is,gtype,form,size,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF CReate Work Slot
      !     This is a pure virtual memory slot
      !     Used by SIC only
      !---------------------------------------------------------------------
      integer(kind=4),           intent(in)  :: is     ! Image slot
      character(len=*),          intent(in)  :: gtype  ! Image type (GILDAS_vwxyz)
      integer(kind=4),           intent(in)  :: form   ! Data format
      integer(kind=size_length), intent(in)  :: size   ! Data size
      logical,                   intent(out) :: error  ! Error flag
    end subroutine gio_crws
  end interface
  !
  interface
    subroutine gio_exis (is,gtype,name,form,size,error)
      use gbl_format
      use gbl_message
      use gio_convert
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! GDF EXtend Image Slot
      !     Extend a Greg Data Frame, and return size and address to caller
      !     Used by SIC only
      !---------------------------------------------------------------------
      integer(kind=4),           intent(in)    :: is     ! Image slot
      character(len=*),          intent(out)   :: gtype  ! Image type (GILDAS_vwxyz)
      character(len=*),          intent(in)    :: name   ! File name
      integer(kind=4),           intent(out)   :: form   ! Data format
      integer(kind=size_length), intent(inout) :: size   ! Data size
      logical,                   intent(out)   :: error  ! Error flag
    end subroutine gio_exis
  end interface
  !
  interface
    subroutine gdf_extend_image (x,newdim,error)
      use image_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API routine
      !     Extend an existing image by increasing its LAST dimension
      !---------------------------------------------------------------------
      type(gildas),               intent(inout) :: x       ! Gildas Image
      integer(kind=index_length)                :: newdim  ! New value of last dimension
      logical,                    intent(inout) :: error   ! Logical error flag
    end subroutine gdf_extend_image
  end interface
  !
  interface
    subroutine gdf_stbl(stbl,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF set STarting BLOcks default
      !     temporary routine to change the number of header blocks
      !---------------------------------------------------------------------
      integer, intent(in)  :: stbl      ! Number of starting blocks
      logical, intent(out) :: error     ! Error flag
    end subroutine gdf_stbl
  end interface
  !
  interface
    function gdf_stbl_get()
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! Return the STarting BLOcks default
      !     temporary routine to change the number of header blocks
      !---------------------------------------------------------------------
      integer(kind=4) :: gdf_stbl_get
    end function gdf_stbl_get
  end interface
  !
  interface
    subroutine gio_geis (is,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF GEt Image Slot
      !     Only used by SIC
      !---------------------------------------------------------------------
      integer, intent(out) :: is        ! Image slot
      logical, intent(out) :: error     ! Error flag
    end subroutine gio_geis
  end interface
  !
  interface
    subroutine gio_fris (is,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF FRee Image Slot
      !   Only used by SIC
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !---------------------------------------------------------------------
      integer, intent(in)    :: is     ! Image slot
      logical, intent(inout) :: error  ! Error flag
    end subroutine gio_fris
  end interface
  !
  interface
    subroutine gio_clis (is,error)
      use gio_image
      use gbl_message
      !----------------------------------------------------------------------
      ! @ public
      ! GDF CLose Image Slot
      !   Only used by SIC
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !----------------------------------------------------------------------
      integer, intent(in)    :: is     ! Image slot
      logical, intent(inout) :: error  ! Error flag
    end subroutine gio_clis
  end interface
  !
  interface
    function gdf_stis (is)
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! GDF STatus of Image Slot
      !             return slot status (-1 if not available)
      !---------------------------------------------------------------------
      integer :: gdf_stis               ! intent(out)
      integer, intent(in) :: is         ! Image slot
    end function gdf_stis
  end interface
  !
  interface
    function gio_chis (is,rdonly)
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! Change READ Status to allow or avoid rewriting buffers...
      !     Only used by SIC
      !---------------------------------------------------------------------
      integer :: gio_chis               ! intent(out)
      integer, intent(in) :: is         ! Image slot
      logical, intent(in) :: rdonly     ! ReadOnly Status
    end function gio_chis
  end interface
  !
  interface
    subroutine gio_lsis (chain,error)
      use gildas_def
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF LiSt Image Slot
      !             A debugging routine only used by SIC
      ! It will prompt for user input if CHAIN is not empty.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: chain
      logical, intent(out) :: error     ! Error flag
    end subroutine gio_lsis
  end interface
  !
  interface
    subroutine gio_gems (ms, is, blc, trc, addr, form, error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! GIO
      !     GEt Memory Slot
      !     Generalized version to also obtain non-contiguous subsets
      !---------------------------------------------------------------------
      integer, intent(out) :: ms                         ! Memory slot
      integer, intent(in)  :: is                         ! Image slot
      integer(kind=index_length), intent(in)  :: blc(:)  ! Bottom left corner
      integer(kind=index_length), intent(in)  :: trc(:)  ! Top right corner
      integer(kind=address_length), intent(out) :: addr  ! Returned address
      integer, intent(in)  :: form                       ! Image format
      logical, intent(out) :: error                      ! Flag
    end subroutine gio_gems
  end interface
  !
  interface
    subroutine gio_frms (ms,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF   FRee Memory Slot
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !---------------------------------------------------------------------
      integer, intent(in)    :: ms     ! Memory slot
      logical, intent(inout) :: error  ! error flag
    end subroutine gio_frms
  end interface
  !
  interface
    subroutine gildas_open
      use gio_headers
      !---------------------------------------------------------------------
      ! @ public
      ! GILDAS
      !       I/O routines for GILDAS tasks.
      !---------------------------------------------------------------------
      ! Local
    end subroutine gildas_open
  end interface
  !
  interface
    subroutine gildas_char (name,chain)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      character(len=*), intent(out) :: chain
    end subroutine gildas_char
  end interface
  !
  interface
    subroutine gildas_charn (name,string,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      character(len=*), intent(out) :: string(n)
    end subroutine gildas_charn
  end interface
  !
  interface
    subroutine gildas_close
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
    end subroutine gildas_close
  end interface
  !
  interface gildas_dble
    subroutine gildas_dble_0d (name,adble,n)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gildas_dble
      ! ---
      ! This version for scalar argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      real(kind=8),     intent(out) :: adble
    end subroutine gildas_dble_0d
    subroutine gildas_dble_1d (name,adble,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic gildas_dble
      ! ---
      ! This version for 1D array as argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      real(kind=8),     intent(out) :: adble(n)
    end subroutine gildas_dble_1d
  end interface gildas_dble
  !
  interface gildas_inte
    subroutine gildas_inte4_0d(name,ainte,n)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gildas_inte
      ! ---
      ! This version for scalar I*4 argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      integer(kind=4),  intent(out) :: ainte
    end subroutine gildas_inte4_0d
    subroutine gildas_inte4_1d(name,ainte,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic gildas_inte
      ! ---
      ! This version for a 1D I*4 variable
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      integer(kind=4),  intent(out) :: ainte(n)
    end subroutine gildas_inte4_1d
    subroutine gildas_inte8_1d(name,along,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic gildas_inte
      ! ---
      ! This version for a 1D I*8 variable
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      integer(kind=8),  intent(out) :: along(n)
    end subroutine gildas_inte8_1d
  end interface gildas_inte
  !
  interface gildas_logi
    subroutine gildas_logi_0d (name,alogi,n)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gildas_logi
      ! ---
      ! This version for scalar argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      logical,          intent(out) :: alogi
    end subroutine gildas_logi_0d
    subroutine gildas_logi_1d (name,alogi,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic gildas_logi
      ! ---
      ! This version for 1D array as argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      logical,          intent(out) :: alogi(n)
    end subroutine gildas_logi_1d
  end interface gildas_logi
  !
  interface gildas_real
    subroutine gildas_real_0d (name,areal,n)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gildas_real
      ! ---
      ! This version for scalar argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      real(kind=4),     intent(out) :: areal
    end subroutine gildas_real_0d
    subroutine gildas_real_1d (name,areal,n)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic gildas_real
      ! ---
      ! This version for 1D array as argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name
      integer(kind=4),  intent(in)  :: n
      real(kind=4),     intent(out) :: areal(n)
    end subroutine gildas_real_1d
  end interface gildas_real
  !
  interface
    subroutine gio_reis (is,gtype,name,form,size,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public
      ! GDF   REad Image Slot
      !   Only used by SIC
      !---------------------------------------------------------------------
      integer(kind=4),           intent(in)    :: is     ! Image SLot
      character(len=*),          intent(out)   :: gtype  ! Image type
      character(len=*),          intent(in)    :: name   ! File name
      integer(kind=4),           intent(out)   :: form   ! Data type
      integer(kind=size_length), intent(out)   :: size   ! Data size
      logical,                   intent(inout) :: error  ! Error flag
    end subroutine gio_reis
  end interface
  !
  interface
    subroutine gio_wris (is,gtype,name,form,size,error)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ public
      ! GDF   WRite Image Slot
      !   Only used by SIC
      !----------------------------------------------------------------------
      integer(kind=4),           intent(in)    :: is     ! Image SLot
      character(len=*),          intent(out)   :: gtype  ! Image type
      character(len=*),          intent(in)    :: name   ! File name
      integer(kind=4),           intent(out)   :: form   ! Data type
      integer(kind=size_length), intent(out)   :: size   ! Data size
      logical,                   intent(inout) :: error  ! Error flag
    end subroutine gio_wris
  end interface
  !
  interface gag_file_guess
    subroutine gag_file_guess_fromname(caller,filename,filekind,error)
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gag_file_guess
      ! Guess if the input file is a GDF file (code 1), a FITS file (code
      ! 2), or something else (code 0)
      ! ---
      ! This version with the file name as argument, to be Fortran-opened-
      ! and-closed during the guess
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller
      character(len=*), intent(in)    :: filename
      integer(kind=4),  intent(out)   :: filekind
      logical,          intent(inout) :: error
    end subroutine gag_file_guess_fromname
    subroutine gag_file_guess_fromlun(caller,lun,filekind,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic gag_file_guess
      ! Guess if the input file is a GDF file (code 1), a FITS file (code
      ! 2), or something else (code 0)
      ! ---
      ! This version with the file name already opened in input logical unit
      ! (must be opened as OLD, DIRECT, UNFORMATTED).
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller
      integer(kind=4),  intent(in)    :: lun
      integer(kind=4),  intent(out)   :: filekind
      logical,          intent(inout) :: error
    end subroutine gag_file_guess_fromlun
  end interface gag_file_guess
  !
  interface
    subroutine gdf_geih(is,p,error)
      use gbl_message
      use gio_image
      !---------------------------------------------------------------------
      ! @ public
      ! GDF   GEt a pointer to Image Header
      !       Return the image header for further use
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: is     ! Image Slot
      type(gildas),    pointer       :: p      ! Pointer to header
      logical,         intent(inout) :: error  ! Error flag
    end subroutine gdf_geih
  end interface
  !
  interface
    subroutine gdf_flih (is,check,error)
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! GDF   FLush Image Header
      !       FLush an image header
      !---------------------------------------------------------------------
      integer, intent(in) :: is                        ! Image Slot
      logical, intent(in) :: check                     ! Check if slot is writeable
      logical, intent(out) :: error                    ! Error flag
    end subroutine gdf_flih
  end interface
  !
  interface
    function gdf_uv_frequency(huv, dchan)
      use image_def
      !-----------------------------------------------------------------------
      ! @ public
      !
      ! GIO Library
      !   Get observing frequency at given channel
      !-----------------------------------------------------------------------
      type(gildas), intent(in) :: huv   ! UV Header
      real(8), intent(in), optional :: dchan      ! Channel number (possibly fractional)
      real(8) :: gdf_uv_frequency       ! Intent(out)
    end function gdf_uv_frequency
  end interface
  !
  interface
    subroutine gdf_modify(h,velocity,frequency,unit,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Modify the reference frequency and reference velocity of an input
      ! GDF header, and optionaly the axis unit (FREQUENCY or VELOCITY)
      !---------------------------------------------------------------------
      type(gildas),     intent(inout), target :: h          ! Header to be shifted
      real(kind=4),     intent(in)            :: velocity   ! New velocity
      real(kind=8),     intent(in)            :: frequency  ! New frequency
      character(len=*), intent(in), optional  :: unit       ! New axis unit
      logical,          intent(out)           :: error      ! Error flag
    end subroutine gdf_modify
  end interface
  !
  interface
    subroutine gdf_uv_doppler(huv,version_uv)
      use image_def
      use gbl_message
      use gio_uv
      !------------------------------------------------------------------
      ! @ public
      !   GIO
      !     Convert a UV table Frequency information to the appropriate
      !     shape. The Frequency information model of the data is
      !     in huv%gil%version_uv, and it is converted towards model
      !     described by version_uv, which can be
      !   code_version_uvt_freq
      !     Observing frequency is in the first axis - This is the
      !     historical behaviour.
      !   code_version_uvt_dopp
      !     Observing frequency is derived from a mean Doppler correction
      !     and the rest frequency in the Spectroscopy section.
      !   code_version_uvt_syst
      !     Observing frequency is derived from a time-dependent Doppler
      !     correction stored in the code_uvt_topo column,
      !     and the rest frequency in the Spectroscopy section
      !     It falls back to the code_version_uvt_dopp behaviour if
      !     no such column is found.
      !------------------------------------------------------------------
      type(gildas), intent(inout) :: huv
      integer, intent(in) :: version_uv
    end subroutine gdf_uv_doppler
  end interface
  !
  interface
    subroutine gdf_set_uvt_version(name)
      use image_def
      use gio_uv
      use gbl_message
      !----------------------------------------------------
      ! @ public
      !   Select GDF data format version beyond 2.0
      !----------------------------------------------------
      character(len=*), intent(in) :: name
    end subroutine gdf_set_uvt_version
  end interface
  !
  interface
    subroutine gdf_get_uvt_version(code)
      use gio_uv
      !----------------------------------------------------
      ! @ public
      !   Get UVT data format version beyond 2.0
      !----------------------------------------------------
      integer(kind=4), intent(out) :: code
    end subroutine gdf_get_uvt_version
  end interface
  !
  interface
    subroutine gdf_gauss2d_convolution(h,a2,b2,t2,error)
      use image_def
      use phys_const
      !---------------------------------------------------------------------
      ! @ public
      !   Convolve the resolution section of the input header by a 2D
      ! gaussian. Results are saved in the header itself.
      !   All input and output angles use GDF convention i.e. from North to
      ! East. The convolution position angle should be defined in the same
      ! frame as the input header is defined.
      !---------------------------------------------------------------------
      type(gildaS), intent(inout) :: h      !
      real(kind=4), intent(in)    :: a2     ! [same as h%gil%major] Convolution major axis
      real(kind=4), intent(in)    :: b2     ! [same as h%gil%major] Convolution minor axis
      real(kind=4), intent(in)    :: t2     ! [rad]                 Convolution position angle
      logical,      intent(inout) :: error  ! Logical error flag
    end subroutine gdf_gauss2d_convolution
  end interface
  !
  interface
    subroutine gdf_gauss2d_deconvolution(h,a3,b3,t3,a2,b2,t2,error)
      use image_def
      use phys_const
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Compute the convolution kernel needed to reach the desired
      ! resolution starting from the input header resolution section.
      !   All input and output angles use GDF convention i.e. from North to
      ! East. The goal position angle should be defined in the same
      ! frame as the input header is defined.
      !---------------------------------------------------------------------
      type(gildaS), intent(in)    :: h      !
      real(kind=4), intent(in)    :: a3     ! [same as h%gil%major] Goal major axis
      real(kind=4), intent(in)    :: b3     ! [same as h%gil%major] Goal minor axis
      real(kind=4), intent(in)    :: t3     ! [rad]                 Goal position angle
      real(kind=4), intent(out)   :: a2     ! [same as h%gil%major] Convolution major axis
      real(kind=4), intent(out)   :: b2     ! [same as h%gil%major] Convolution minor axis
      real(kind=4), intent(out)   :: t2     ! [rad]                 Convolution position angle
      logical,      intent(inout) :: error  ! Logical error flag
    end subroutine gdf_gauss2d_deconvolution
  end interface
  !
  interface
    subroutine gdf_read_uvonly(huv,uv,error)
      use image_def
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Read the UV data of a GILDAS UV structure
      !       UVT and TUV order are allowed...
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: huv               ! UV table descriptor
      real(kind=4), intent(out) :: uv(huv%gil%nvisi,2)  ! Return U,V coordinates
      logical, intent(out) :: error                     ! Flag
    end subroutine gdf_read_uvonly
  end interface
  !
  interface
    subroutine gdf_read_uvonly_codes(huv,uv,codes,ncode,error)
      use image_def
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Read the Time-Baseline data of a GILDAS UV structure
      !       UVT and TUV order are allowed...
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: huv               ! UV table descriptor
      integer(4), intent(in) :: ncode                   ! Number of codes
      integer(4), intent(in) :: codes(ncode)            ! Desired codes
      real(kind=4), intent(out) :: uv(ncode,huv%gil%nvisi)  ! Return Values
      logical, intent(out) :: error                     ! Flag
    end subroutine gdf_read_uvonly_codes
  end interface
  !
  interface
    subroutine gdf_read_uvdataset(huvin,huvou,nc,duvou,error)
      use image_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !       Read UV data and Associated parameters from a GILDAS UV
      !       structure and place it in the specified array
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: huvin     ! Input UV header (data file)
      type(gildas), intent(inout) :: huvou     ! Output UV header (program used)
      integer(kind=4), intent(in) :: nc(2)     ! Selected channels
      real(kind=4), intent(inout) :: duvou(huvou%gil%dim(1),huvou%gil%dim(2))  ! Data
      logical,      intent(out)   :: error     ! Flag
    end subroutine gdf_read_uvdataset
  end interface
  !
  interface
    subroutine gdf_compare_shape(first,second,equal)
      use image_def
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Compare shapes of two GILDAS images
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: first       ! First GILDAS data set
      type (gildas), intent(in) :: second      ! Second GILDAS data set
      logical, intent(out) :: equal         ! are they equal ?
    end subroutine gdf_compare_shape
  end interface
  !
  interface
    subroutine gdf_read_gdforfits_header(name,isfits,hgdf,hfits,error)
      use gbl_message
      use image_def
      use gfits_types
      !------------------------------------------------------------------
      ! @ public
      ! Read the named file and store its header under GDF format
      ! in 'hgdf' structure. The input file can be either a standard GDF
      ! or a supported FITS.
      ! In return, the internal buffers are set to be ready to read data.
      !------------------------------------------------------------------
      character(len=*),    intent(in)    :: name    ! File name
      logical,             intent(out)   :: isfits  ! Found a GDF or a FITS?
      type(gildas),        intent(inout) :: hgdf    ! GDF image structure
      type(gfits_hdesc_t), intent(inout) :: hfits   ! FITS internal description, if relevant
      logical,             intent(inout) :: error   ! Error flag
    end subroutine gdf_read_gdforfits_header
  end interface
  !
  interface
    subroutine gdf_read_header(imag,error,rank)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Read an image header from the requested file
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag   ! Image structure
      logical,      intent(out)   :: error  ! Error flag
      integer,      intent(in), optional :: rank  ! Desired rank
    end subroutine gdf_read_header
  end interface
  !
  interface
    subroutine gdf_trim_header(imag,rank,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Read an image header from the requested file
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag    ! Image structure
      integer(4),   intent(in)    :: rank    ! Requested Rank
      logical,      intent(out)   :: error   ! Error flag
    end subroutine gdf_trim_header
  end interface
  !
  interface
    subroutine sub_trim_header(caller,imag,therank,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !       Read an image header from the requested file
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: caller  ! Name of caller
      type(gildas), intent(inout)  :: imag    ! Image structure
      integer(4),   intent(in)     :: therank ! Requested Rank
      logical,      intent(inout)  :: error   ! Error flag
    end subroutine sub_trim_header
  end interface
  !
  interface
    subroutine gdf_update_header(imag,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API routine
      !     Update an image header
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: imag      ! Image structure
      logical, intent(out) :: error             ! Flag
    end subroutine gdf_update_header
  end interface
  !
  interface
    subroutine gdf_copy_header(input,output, error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Copy (a part of) the input header into the output header.
      ! Since the arguments are the full gildas type, 'output' must be
      ! inout to avoid resetting the data part of the type. This implies that
      ! the other header components must also have been set/initialized
      ! before.
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: input   ! Input header
      type(gildas), intent(inout) :: output  ! Output header
      logical,      intent(inout) :: error   !
    end subroutine gdf_copy_header
  end interface
  !
  interface
    subroutine gdf_transpose_header(input,output,order,error,drop)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !       Transpose a header information
      !---------------------------------------------------------------------
      type (gildas),     intent(in)    :: input   ! Initial Header
      type (gildas),     intent(inout) :: output  ! Transposed Header
      character(len=*),  intent(in)    :: order   ! Transposition code
      logical,           intent(inout) :: error   ! Flag
      logical, optional, intent(in)    :: drop    ! Drop trailing dimensions?
    end subroutine gdf_transpose_header
  end interface
  !
  interface
    function gildas_error(header,name,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API routine
      !     Translate any possible error code, and return an error if needed
      !---------------------------------------------------------------------
      logical gildas_error                  ! intent(out)
      type (gildas), intent(in) :: header   ! Image header
      character(len=*), intent(in) :: name  ! Calling facility name
      logical, intent(out) :: error         ! Error flag
    end function gildas_error
  end interface
  !
  interface
    subroutine gio_read_header(imag,islo,error)
      use image_def
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF / GIO Internal routine
      !       Read an image header on a specified image slot
      !           The slot must have been opened before
      !---------------------------------------------------------------------
      type(gildas),    intent(inout) :: imag   !
      logical,         intent(inout) :: error  !
      integer(kind=4), intent(in)    :: islo   !
    end subroutine gio_read_header
  end interface
  !
  interface
    subroutine gio_write_header(imag,islo,error)
      use image_def
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF / GIO Internal routine
      !       Update an image header on a specified image slot
      !           The slot must have been opened before
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: imag  !
      logical, intent(out) :: error      !
      integer(kind=4), intent(in) :: islo
    end subroutine gio_write_header
  end interface
  !
  interface
    subroutine gdf_get_extrema (mine,error)
      use gbl_format
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !   Compute the extrema from a Virtual Memory image (one with the
      !   appropriate address field). Note that the address field
      !   can have been set independently of the %getvm status
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: mine   !
      logical,       intent(out)   :: error  !
    end subroutine gdf_get_extrema
  end interface
  !
  interface
    subroutine gdf_get_baselines (mine,error)
      use image_def
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !   Compute the baseline range from a Virtual Memory UV data (one with the
      !   appropriate address field). Note that the address field
      !   can have been set independently of the %getvm status
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: mine   !
      logical,       intent(out)   :: error  !
    end subroutine gdf_get_baselines
  end interface
  !
  interface
    subroutine gildas_null(hx, type)
      use image_def
      use gio_uv
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !   Initialize a Gildas structure and reset its content
      !---------------------------------------------------------------------
      type(gildas), intent(out), target :: hx ! Gildas structure
      character(len=*), intent(in), optional :: type
    end subroutine gildas_null
  end interface
  !
  interface
    subroutine gdf_copy_gil(input,output,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Copy the %gil%  part of the input header into the output header.
      !   Only used by SIC outside of GIO...
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: input   ! Input header
      type (gildas), intent(inout) :: output  ! Output header
      logical, intent(out) :: error
    end subroutine gdf_copy_gil
  end interface
  !
  interface
    subroutine gdf_update_image(imag,array,error)
      use gildas_def
      use image_def
      use gbl_message
      use gio_params
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API Routine
      !     Save an image structure and the specified data on the image file
      !     This version is for updating an existing file
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag      ! Image descriptor
      real,         intent(in)    :: array(*)  ! Data to be written
      logical,      intent(inout) :: error     ! Flag
    end subroutine gdf_update_image
  end interface
  !
  interface
    subroutine gdf_write_image(imag,array,error)
      use gildas_def
      use image_def
      use gbl_message
      use gio_params
      !---------------------------------------------------------------------
      ! @ public 
      ! GDF API routine
      !     Save an image structure and the specified data on the image file
      !     This version is to create a new file
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag      ! Image descriptor
      real,         intent(in)    :: array(*)  ! Data array
      logical,      intent(inout) :: error     ! Flag
    end subroutine gdf_write_image
  end interface
  !
  interface
    subroutine gdf_open_image(imag,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API
      !     Open an image structure on the requested file for writing
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag   ! Image descriptor
      logical,      intent(inout) :: error  ! Flag
    end subroutine gdf_open_image
  end interface
  !
  interface
    subroutine gdf_create_image(imag,error)
      use gio_params
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API Routine
      !     Create an image structure on the requested file
      !     Nothing is written yet to the file.
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag   ! Image descriptor
      logical,      intent(inout) :: error  ! Flag
    end subroutine gdf_create_image
  end interface
  !
  interface
    subroutine gdf_close_image(imag,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Close an image structure. No further READ allowed on this image
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: imag   ! Image descriptor
      logical,      intent(inout) :: error  ! Flag
    end subroutine gdf_close_image
  end interface
  !
  interface
    subroutine gdf_allocate (hx,error)
      use image_def
      use gbl_message
      !-----------------------------------------------------------------
      ! @ public
      !   Allocate the appropriate work space
      !   for a Gildas data type.
      ! CAUTION
      !   blc, trc not yet tested ...
      !
      !-----------------------------------------------------------------
      type (gildas), intent(inout) :: hx    ! Gildas data type
      logical, intent(out) :: error         ! Error flag
    end subroutine gdf_allocate
  end interface
  !
  interface
    subroutine gdf_read_gildas(hx,name,ext,error, rank, data)
      use image_def
      use gbl_message
      !-----------------------------------------------------------------
      ! @ public
      ! Read a Gildas Data File of name "name" and extention "ext"
      ! and return it into the "hx" Gildas data type.
      !
      ! Desired Form & Rank can be specified: an error will happen
      ! in case of mismatch.
      !-----------------------------------------------------------------
      type(gildas),     intent(inout)        :: hx      ! Gildas data type
      character(len=*), intent(in)           :: name    ! Filename
      character(len=*), intent(in)           :: ext     ! Extension
      logical,          intent(inout)        :: error   ! Error flag
      integer,          intent(in), optional :: rank    !
      logical,          intent(in), optional :: data    !
    end subroutine gdf_read_gildas
  end interface
  !
  interface gdf_range
    function gdf_range_48(nc,nchan)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic   gdf_range
      ! GDF API
      !       Return a range of (pixel / channel / items)
      !---------------------------------------------------------------------
      integer(4) :: gdf_range_48  ! Intent(out)  0 if no error
      integer(8), intent(in) :: nchan     ! The actual number of channels
      integer(4), intent(inout) :: nc(2)  ! The interpreted range of selected channels
    end function gdf_range_48
    function gdf_range_88(nc,nchan)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic  gdf_range
      ! GDF API
      !       Return a range of (pixel / channel / items)
      !---------------------------------------------------------------------
      integer(4) :: gdf_range_88          ! Intent(out)
      integer(8), intent(in) :: nchan     ! The actual number of channels
      integer(8), intent(inout) :: nc(2)  ! The interpreted range of selected channels
    end function gdf_range_88
    function gdf_range_44(nc,nchan) ! result (res)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic  gdf_range
      ! GDF API
      !       Return a range of (pixel / channel / items)
      !---------------------------------------------------------------------
      integer(4) :: gdf_range_44          ! Intent(out)  0 if no error
      integer(4), intent(in) :: nchan     ! The actual number of channels
      integer(4), intent(inout) :: nc(2)  ! The interpreted range of selected channels
    end function gdf_range_44
    function gdf_range_84(nc,nchan)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic  gdf_range
      ! GDF API
      !       Return a range of (pixel / channel / items)
      !---------------------------------------------------------------------
      integer(4) :: gdf_range_84          ! Intent(out)  0 if no error
      integer(4), intent(in) :: nchan     ! The actual number of channels
      integer(8), intent(inout) :: nc(2)  ! The interpreted range of selected channels
    end function gdf_range_84
  end interface gdf_range
  !
  interface
    subroutine gdf_print_header(img)
      use image_def
      use gbl_format
      use gbl_constant
      use phys_const
      use gio_uv
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! GIO Library
      !       Support routine for command HEADER
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: img  ! Header to be printed
    end subroutine gdf_print_header
  end interface
  !
  interface
    subroutine gdf_addteles(fits,key,name,value,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------
      ! @ public
      ! GDF
      !   Add a new telescope
      !---------------------------------------------------------------
      type(gildas),     intent(inout) :: fits      ! Output GILDAS structure
      character(len=*), intent(in)    :: key       ! Operation Key
      character(len=*), intent(in)    :: name      ! Telescope name
      real(kind=8),     intent(in)    :: value(3)  ! Input values
      logical,          intent(inout) :: error     !
    end subroutine gdf_addteles
  end interface
  !
  interface
    subroutine gdf_rmteles(fits,name,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------
      ! @ public
      !  Remove a telescope from section, identified by its name
      !---------------------------------------------------------------
      type(gildas),     intent(inout) :: fits      ! Output GILDAS structure
      character(len=*), intent(in)    :: name      ! Telescope name
      logical,          intent(inout) :: error     !
    end subroutine gdf_rmteles
  end interface
  !
  interface
    subroutine gdf_transpose (nameou,namein,code,space_gildas,error)
      use gsys_types
      use image_def
      use gbl_message
      use gio_params
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ public
      ! GDF API routine
      !  Transpose a GILDAS image
      !  Disk version when needed
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: nameou        ! Output transposed file name
      character(len=*), intent(in)    :: namein        ! Input file name
      character(len=*), intent(inout) :: code          ! Transposition code
      integer(kind=4),  intent(in)    :: space_gildas  ! Useable memory size
      logical,          intent(inout) :: error         ! Error flag
    end subroutine gdf_transpose
  end interface
  !
  interface gdf_nitems
    subroutine gdf_nitems_4(logname,nitems,asize)
      !---------------------------------------------------------------------
      ! @ public-generic gdf_nitems
      !   Return the number of items of size ASIZE in a block size specified
      !   by LOGNAME (typically SPACE_GILDAS)
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: logname
      integer(kind=4),  intent(out) :: nitems
      integer(kind=4),  intent(in)  :: asize
    end subroutine gdf_nitems_4
    subroutine gdf_nitems_8(logname,nitems,asize)
      !---------------------------------------------------------------------
      ! @ public-generic gdf_nitems
      !   Return the number of items of size ASIZE in a block size specified
      !   by LOGNAME (typically SPACE_GILDAS)
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: logname
      integer(kind=4),  intent(out) :: nitems
      integer(kind=8),  intent(in)  :: asize
    end subroutine gdf_nitems_8
  end interface gdf_nitems
  !
  interface gdf_dump_array
    subroutine gdf_dump_array1(file,array)
      use image_def
      !
      ! @ public-generic gdf_dump_array 
      ! 
      character(len=*), intent(in) :: file
      real, intent(in) :: array(:)
    end subroutine gdf_dump_array1
    subroutine gdf_dump_array2(file,array)
      use image_def
      !
      ! @ public-generic gdf_dump_array 
      ! 
      character(len=*), intent(in) :: file
      real, intent(in) :: array(:,:)
    end subroutine gdf_dump_array2
    subroutine gdf_dump_array3(file,array)
      use image_def
      !
      ! @ public-generic gdf_dump_array 
      ! 
      character(len=*), intent(in) :: file
      real, intent(in) :: array(:,:,:)
    end subroutine gdf_dump_array3
    subroutine gdf_dump_array4(file,array)
      use image_def
      !
      ! @ public-generic gdf_dump_array 
      ! 
      character(len=*), intent(in) :: file
      real, intent(in) :: array(:,:,:,:)
    end subroutine gdf_dump_array4
  end interface gdf_dump_array
  !
  interface
    function gag_sizeof(form)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! Return data word length in Bytes
      ! This function is generic for GDF, SIC variables, etc as they share
      ! the same codes. Hence the prefix gag_
      !---------------------------------------------------------------------
      integer(kind=4) :: gag_sizeof     ! Function value on return
      integer(kind=4), intent(in) :: form ! Data format
    end function gag_sizeof
  end interface
  !
  interface
    function gdf_nelem(h)
      use image_def
      !---------------------------------------------------------------------
      ! @ public
      ! Return the number of elements from the input GDF header
      !---------------------------------------------------------------------
      integer(kind=size_length) :: gdf_nelem
      type(gildas), intent(in) :: h
    end function gdf_nelem
  end interface
  !
  interface
    function gdf_size(h)
      use image_def
      !---------------------------------------------------------------------
      ! @ public
      ! Return the whole data size in bytes
      !---------------------------------------------------------------------
      integer(kind=size_length) :: gdf_size
      type(gildas), intent(in) :: h
    end function gdf_size
  end interface
  !
  interface
    subroutine gdf_toobig(h,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Return an error if the image is too large to be opened on the
      ! current system
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h
      logical,      intent(inout) :: error       !
    end subroutine gdf_toobig
  end interface
  !
  interface
    subroutine fitscube2gdf_header(file,ihdu,fd,ghead,getsymbol,error)
      use image_def
      use gbl_message
      use gfits_types
      !---------------------------------------------------------------------
      ! @ public
      ! Load the header of the extension currently opened in gfits and
      ! translate it as a GDF header. Suited for N-dimensional cubes only.
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: file       ! FITS file name
      integer(kind=4),     intent(in)    :: ihdu       ! HDU we are reading (1=Primary)
      type(gfits_hdesc_t), intent(inout) :: fd         !
      type(gildas),        intent(inout) :: ghead      ! Gildas header
      external                           :: getsymbol  ! Symbol translation routine
      logical,             intent(inout) :: error      !
    end subroutine fitscube2gdf_header
  end interface
  !
  interface
    subroutine fitscube2gdf_patch_bval(fd,ghead,data,ndata,nblank,error)
      use gildas_def
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ public
      ! Patch blanking values to a better value in input data array
      !---------------------------------------------------------------------
      type(gfits_hdesc_t),       intent(in)    :: fd           !
      type(gildas),              intent(in)    :: ghead        !
      integer(kind=size_length), intent(in)    :: ndata        !
      real(kind=4),              intent(inout) :: data(ndata)  !
      integer(kind=size_length), intent(inout) :: nblank       ! Updated in return
      logical,                   intent(inout) :: error        !
    end subroutine fitscube2gdf_patch_bval
  end interface
  !
  interface
    subroutine unit_prefix_scale(prefix,unit,scale,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Find the scaling factor to go from 'unit' (e.g. 'Hz') to prefix
      ! (e.g. 'MHz').
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: prefix  ! Prefixed unit
      character(len=*), intent(in)    :: unit    ! Natural unit
      real(kind=8),     intent(out)   :: scale   ! Scaling factor
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine unit_prefix_scale
  end interface
  !
  interface
    subroutine fits_gildas_sub(iname,oname,style,blc,trc,khdu,check,overwrite,error,  &
      getsymbol,wpr)
      use gio_params
      use gio_xy
      use gio_fitsdef
      use gbl_message
      !--------------------------------------------------------------------
      ! @ public
      ! GIO FITS interface
      ! Convert a FITS file to a GDF file
      !--------------------------------------------------------------------
      character(len=*),           intent(in)    :: iname      ! Input FITS file
      character(len=*),           intent(in)    :: oname      ! Output GILDAS file
      character(len=*),           intent(in)    :: style      ! FITS style
      integer(kind=index_length), intent(in)    :: blc(:)     ! Bottom Left Corner
      integer(kind=index_length), intent(in)    :: trc(:)     ! Top Right Corner
      integer(kind=4),            intent(in)    :: khdu       ! HDU number (1=Primary)
      logical,                    intent(in)    :: check      ! Echo keywords ?
      logical,                    intent(in)    :: overwrite  ! Overwrite existing output file?
      logical,                    intent(inout) :: error      ! Returned error code
      external                                  :: getsymbol  ! Translate FITS keyword
      external                                  :: wpr        ! Ask a question if unknown
    end subroutine fits_gildas_sub
  end interface
  !
  interface
    subroutine gildas_fits_sub(iname,oname,style,nbits,overwrite,error)
      use gio_xy
      use gio_fitsdef
      use gbl_message
      !--------------------------------------------------------
      ! @ public
      ! FITS
      !     Convert a GDF file to a FITS file
      !--------------------------------------------------------
      character(len=*), intent(in)    :: iname      ! GILDAS file name
      character(len=*), intent(in)    :: oname      ! FITS file name
      character(len=*), intent(in)    :: style      ! FITS style
      integer(kind=4),  intent(in)    :: nbits      ! Number of bits
      logical,          intent(in)    :: overwrite  ! Overwrite existing file?
      logical,          intent(inout) :: error      ! Error flag
    end subroutine gildas_fits_sub
  end interface
  !
  interface
    subroutine gildas_to_fits(hfits,oname,style,nbits,overwrite,error)
      use image_def
      use gio_fitsdef
      !--------------------------------------------------------
      ! @ public
      ! FITS
      !     Write a GDF data set in memory onto a FITS file
      !--------------------------------------------------------
      type(gildas), intent(inout) :: hfits          ! GILDAS Header & Data
      character(len=*), intent(in)    :: oname      ! FITS file name
      character(len=*), intent(in)    :: style      ! FITS style
      integer(kind=4),  intent(in)    :: nbits      ! Number of bits
      logical,          intent(in)    :: overwrite  ! Overwrite existing file?
      logical,          intent(inout) :: error      ! Error flag
    end subroutine gildas_to_fits
  end interface
  !
  interface
    subroutine gfits_set_nbit(nbits,error)
      use gio_fitsdef
      use gbl_message
      !--------------------------------------------------------
      ! @ public
      ! FITS
      !     Set number NBIT in FITS Library
      !--------------------------------------------------------
      integer(kind=4),  intent(in)    :: nbits      ! Number of bits
      logical,          intent(inout) :: error      ! Error flag
    end subroutine gfits_set_nbit
  end interface
  !
  interface
    subroutine gfits_set_style(style)
      use gio_fitsdef
      !--------------------------------------------------------
      ! @ public
      ! FITS
      !     Define the FITS style in library
      !--------------------------------------------------------
      character(len=*), intent(in) :: style  ! Style name
    end subroutine gfits_set_style
  end interface
  !
  interface
    subroutine gio_uvwps(awps)
      use gio_fitsdef
      ! @ public
      !
      real, intent(out) :: awps
    end subroutine gio_uvwps
  end interface
  !
  interface
    function stokes_from_fits(rfits)
      use image_def
      !--------------------------------------------------------
      ! @ public
      !
      ! Here we decode the Stokes convention from the FITS Header
      ! -8  -7  -6  -5  -4  -3  -2  -1   0   1   2   3   4
      ! YX  XY  YY  XX  LR  RL  LL  RR  None I   Q   U   V
      !--------------------------------------------------------
      !
      integer :: stokes_from_fits
      real, intent(in) :: rfits
    end function stokes_from_fits
  end interface
  !
  interface
    function fits_from_stokes(istoke)
      use image_def
      !--------------------------------------------------------
      ! @ public
      !
      ! Here we decode the Stokes convention from the FITS Header
      ! -8  -7  -6  -5  -4  -3  -2  -1  0    1   2   3   4
      ! YX  XY  YY  XX  LR  RL  LL  RR  None I   Q   U   V
      !--------------------------------------------------------
      !
      integer :: fits_from_stokes
      integer, intent(in) :: istoke
    end function fits_from_stokes
  end interface
  !
end module gio_interfaces_public
