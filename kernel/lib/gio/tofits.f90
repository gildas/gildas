subroutine tofits(fits,check,error)
  use gbl_message
  use image_def
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>tofits
  use gio_fitsdef
  !---------------------------------------------------------------------
  ! @ private
  ! GFITS
  !     Write GILDAS data set to FITS tape
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: fits   !
  logical,      intent(in)    :: check  ! Verbose flag
  logical,      intent(out)   :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='UVFITS'
  character(len=message_length) :: mess
  !
  if (fd%snbit.ne.16 .and. fd%snbit.ne.32 .and. fd%snbit.ne.-32 .and. fd%snbit.ne.-64) then
    call gio_message(seve%e,rname,'Unsupported number of bits')
    error = .true.
    return
  endif
  !
  if (gdfname.ne.' ') then
    !
    ! If a file name is specified in GDFNAME common variable,  
    ! read the corresponding file into the "fits" Gildas header given as argument
    call gildas_null(fits)
    call sic_parsef(gdfname,fits%file,' ','.gdf')
    call gio_geis (fits%loca%islo,error)
    if (.not.error) call gio_reis (fits%loca%islo,fits%char%type,fits%file,fits%gil%form,fits%loca%size,error)
    if (error) return
    !
    if (a_style.ne.code_fits_uvfits.and.a_style.ne.code_fits_aips) then
      if (a_style.eq.0 .and. fits%char%type(1:9).eq.'GILDAS_UV') then
        call gio_message(seve%i,'UVFITS','Gildas file is a UV data set')
        call gio_read_header(fits,fits%loca%islo,error)
        if (fits%gil%type_gdf .ne. code_gdf_uvt) then
          write(mess,100) 'Wrong axis type or order',fits%char%code
          call gio_message(seve%e,rname,mess)
          error = .true.
          return
        endif
        call gio_gems (fits%loca%mslo,fits%loca%islo,fits%blc,fits%trc,fits%loca%addr,fits%gil%form,error)
        if (error) return
        call to_uvfits(fits,check,error)
      else if (fits%char%type.ne.'GILDAS_IMAGE') then
        call gio_message(seve%e,'IMFITS','Gildas file is not an image')
        error = .true.
        return
      else
        call gio_read_header(fits,fits%loca%islo,error)
        if (error) return
        call gio_gems(fits%loca%mslo,fits%loca%islo,fits%blc,fits%trc,fits%loca%addr,fits%gil%form,error)
        call to_imfits(fits,check,error)
      endif
    else
      if (fits%char%type(1:9).ne.'GILDAS_UV') then
        call gio_message(seve%e,rname,'Gildas file is not a raw UV data set')
        error = .true.
        return
      endif
      call gio_read_header(fits,fits%loca%islo,error)
      if (fits%char%code(1)(1:2).ne.'UV' .or. fits%char%code(2).ne.'RANDOM') then
        write(mess,100) 'Wrong axis type or order',fits%char%code
        call gio_message(seve%e,rname,mess)
        error = .true.
        return
      endif
      call gio_gems (fits%loca%mslo,fits%loca%islo,fits%blc,fits%trc,fits%loca%addr,fits%gil%form,error)
      if (error) return
      call to_uvfits(fits,check,error)
    endif
    call gio_fris (fits%loca%islo,error)
    ! 
  else
    !
    ! If no file name is present, the "fits" Gildas header already contains the data
    if (a_style.ne.code_fits_uvfits.and.a_style.ne.code_fits_aips) then
      if (a_style.eq.0 .and. fits%char%type(1:9).eq.'GILDAS_UV') then
        call gio_message(seve%i,'UVFITS','Gildas file is a UV data set')
        if (fits%gil%type_gdf .ne. code_gdf_uvt) then
          write(mess,100) 'Wrong axis type or order',fits%char%code
          call gio_message(seve%e,rname,mess)
          error = .true.
          return
        endif
        call to_uvfits(fits,check,error)
      else if (fits%char%type.ne.'GILDAS_IMAGE') then
        call gio_message(seve%e,'IMFITS','Gildas file is not an image')
        error = .true.
        return
      else
        call to_imfits(fits,check,error)
      endif
    else
      if (fits%char%type(1:9).ne.'GILDAS_UV') then
        call gio_message(seve%e,rname,'Gildas file is not a raw UV data set')
        error = .true.
        return
      endif
      call to_uvfits(fits,check,error)
    endif
  endif
  !
100 format(a,a)
end subroutine tofits
