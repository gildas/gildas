module gio_interfaces_private
  interface
    subroutine gio_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gio_message
  end interface
  !
  interface
    subroutine gio_hd32_to_gildas_v2(hd32,v2)
      use gio_headers
      use gio_uv
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GIO/GDF
      !     Transfer a 32-bit "old" Data File Header to
      !     the "new" type(gildas)
      !---------------------------------------------------------------------
      type(gildas_header_v1), intent(in) :: hd32
      type(gildas), intent(inout) :: v2   ! Must be (inout) as initiliazed before
    end subroutine gio_hd32_to_gildas_v2
  end interface
  !
  interface
    subroutine gio_gildas_v2_to_hd32(v2,hd32)
      use gio_headers
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !     Transfer the "old" type(gildas)
      !     to a new 64-bit Data File Header
      !---------------------------------------------------------------------
      type(gildas_header_v1), intent(inout) :: hd32
      type(gildas), intent(inout) :: v2
    end subroutine gio_gildas_v2_to_hd32
  end interface
  !
  interface
    subroutine gio_hd64_to_gildas_v2 (hd64,v2,error)
      use gio_uv
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GIO/GDF
      !     Transfer a new 64-bit Data File Header to
      !     the "new" type(gildas)
      !---------------------------------------------------------------------
      type (gildas_header_v2), intent(in) :: hd64
      type (gildas), intent(inout) :: v2   ! Must be (inout) because initialized before
      logical, intent(out) :: error
    end subroutine gio_hd64_to_gildas_v2
  end interface
  !
  interface
    subroutine gio_gildas_v2_to_hd64 (v2,hd64,error)
      use image_def
      use gio_params
      use gio_headers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !     Transfer the "new" type(gildas)
      !     to a new 64-bit Data File Header
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: v2
      type (gildas_header_v2), intent(out) :: hd64
      logical, intent(out) :: error
    end subroutine gio_gildas_v2_to_hd64
  end interface
  !
  interface
    subroutine gio_setuv (hd64,error)
      use image_def
      use gbl_message
      !
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Define the UV section consistently.
      ! It could also define the column_types and column_codes if needed
      !
      type(gildas_header_v2), intent(inout) :: hd64
      logical, intent(out) :: error
    end subroutine gio_setuv
  end interface
  !
  interface
    subroutine gio_setuv_freq(hd64,error)
      use image_def
      use gbl_message
      !
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Define the UV section consistently.
      ! It could also define the column_types and column_codes if needed
      !
      type(gildas_header_v2), intent(inout) :: hd64
      logical, intent(out) :: error
    end subroutine gio_setuv_freq
  end interface
  !
  interface
    subroutine gio_hd32_to_gildas_v1(hd32,v1)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: hd32
      integer, intent(in) :: v1
    end subroutine gio_hd32_to_gildas_v1
  end interface
  !
  interface
    subroutine gio_gildas_v1_to_hd32(v1,hd32)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: hd32
      integer, intent(in) :: v1
    end subroutine gio_gildas_v1_to_hd32
  end interface
  !
  interface
    subroutine gio_gildas_v1_to_hd64(v1,hd64)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: hd64
      integer, intent(in) :: v1
    end subroutine gio_gildas_v1_to_hd64
  end interface
  !
  interface
    subroutine gio_hd64_to_gildas_v1(v1,hd64)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: hd64
      integer, intent(in) :: v1
    end subroutine gio_hd64_to_gildas_v1
  end interface
  !
  interface
    function gio_riox (lun,ibl,buf,size)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !       Read one direct acess block
      !       The file must have been opened with the appropriate block size
      !---------------------------------------------------------------------
      integer(kind=4) :: gio_riox  !
      integer(kind=4),             intent(in)  :: lun        ! Logical unit number
      integer(kind=record_length), intent(in)  :: ibl        ! Block number
      integer(kind=4),             intent(in)  :: size       ! Block size
      integer(kind=4),             intent(out) :: buf(size)  ! Buffer to be read
    end function gio_riox
  end interface
  !
  interface
    function gio_wiox (lun,ibl,buf,size)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !       Write one direct acess block
      !       The file must have been opened with the appropriate block size
      !---------------------------------------------------------------------
      integer(kind=4) :: gio_wiox  !
      integer(kind=4),             intent(in) :: lun        ! Logical unit number
      integer(kind=record_length), intent(in) :: ibl        ! Block number
      integer(kind=4),             intent(in) :: size       ! Block size
      integer(kind=4),             intent(in) :: buf(size)  ! Buffer to be read
    end function gio_wiox
  end interface
  !
  interface
    subroutine gio_form (inform,ouform)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !       Determine machine dependent data format from image
      !       data format
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: inform                 ! Input Format
      integer(kind=4), intent(out) :: ouform                ! Output Format
    end subroutine gio_form
  end interface
  !
  interface
    function gio_block (form,size)
      use gildas_def
      use gio_params
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !       Compute number of blocks from image size
      !---------------------------------------------------------------------
      integer(kind=record_length) :: gio_block  !
      integer(kind=4),           intent(in) :: form  ! Input format
      integer(kind=size_length), intent(in) :: size  ! Array  size
    end function gio_block
  end interface
  !
  interface
    subroutine gdf_style (is,gtype)
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      !       Returns image type
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: is             ! Image Slot
      character(len=*), intent(out) :: gtype        ! Image type
    end subroutine gdf_style
  end interface
  !
  interface
    subroutine gio_idim (is,size)
      use gio_params
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      !  Define image dimensions and compute image size
      !---------------------------------------------------------------------
      integer(kind=4),           intent(in)  :: is    ! Image Slot
      integer(kind=size_length), intent(out) :: size  ! Array  size
    end subroutine gio_idim
  end interface
  !
  interface
    subroutine convgcod (sycode,imcode,conver,mess,isev)
      use gio_convert
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !       SYCODE system code
      !       '-'    IEEE
      !       '.'    EEEI (IBM like)
      !       '_'    VAX
      !       IMCODE file code
      !       '<'    IEEE  64 bits    (Little Endian, 99.9 % of recent computers)
      !       '>'    EEEI  64 bits    (Big Endian, HPUX, IBM-RISC, and SPARC ...)
      !---------------------------------------------------------------------
      character(len=1), intent(in) :: sycode        !  System code
      character(len=1), intent(inout) :: imcode     !  File code
      integer, intent(out)  :: conver               !  Conversion code number
      character(len=*), intent(out) :: mess         !  Message trailer
      integer, intent(out)  :: isev                 !  Severity of message (D or I)
    end subroutine convgcod
  end interface
  !
  interface
    function gio_rih (is,gtype,form,lndb)
      use gildas_def
      use gio_image
      use gio_convert
      use gio_headers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO  Internal routine
      !       Read full header from internal buffer, with format conversion
      !     GIO_RIH / GIO_WIH / GIO_EIH are the only routine addressing
      !             the data format of the header
      !---------------------------------------------------------------------
      integer(kind=4) :: gio_rih  !
      integer(kind=4),             intent(in)  :: is     ! Image Slot
      character(len=*),            intent(out) :: gtype  ! Image type
      integer(kind=4),             intent(out) :: form   ! Data format
      integer(kind=record_length), intent(out) :: lndb   ! Number of 512 byte blocks
    end function gio_rih
  end interface
  !
  interface
    function gio_wih (is,gtype,form,lndb)
      use gildas_def
      use gbl_message
      use gio_image
      use gio_convert
      use gio_headers
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO  Internal routine
      !       Write full header to internal buffer, with format conversion
      !     GIO_RIH / GIO_WIH / GIO_EIH are the only routine addressing
      !             the data format of the header
      !---------------------------------------------------------------------
      integer(kind=4) :: gio_wih  !
      integer(kind=4),             intent(in) :: is     ! Image Slot
      character(len=*),            intent(in) :: gtype  ! Image type
      integer(kind=4),             intent(in) :: form   ! Data format
      integer(kind=record_length), intent(in) :: lndb   ! Number of 512 byte blocks
    end function gio_wih
  end interface
  !
  interface
    function gio_eih (is,gtype,form,lndb)
      use gildas_def
      use gbl_message
      use gio_image
      use gio_convert
      !---------------------------------------------------------------------
      ! @ private
      !  GDF        API routine
      !             Read partial header to prepare an update
      !             Return the Type, the data format and the number of blocks
      !     GIO_RIH / GIO_WIH / GIO_EIH are the only routine addressing
      !             the data format of the header
      !---------------------------------------------------------------------
      integer(kind=4) :: gio_eih
      integer(kind=4),             intent(in)    :: is     ! Image Slot
      character(len=*),            intent(inout) :: gtype  ! Image type
      integer(kind=4),             intent(inout) :: form   ! Data format
      integer(kind=record_length), intent(inout) :: lndb   ! Number of 512 byte blocks
    end function gio_eih
  end interface
  !
  interface
    subroutine gio_buffer_to_hd64 (len,inbuf,outhd64,r8,r4,i4,i8,error)
      use gbl_message
      use gio_headers
      use gio_uv
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: len       ! Length of buffer
      integer(kind=4),        intent(in)    :: inbuf(*)  ! Input values in file format
      type(gildas_header_v2), intent(out)   :: outhd64   ! Converted values in machine format
      external                              :: r8        ! Real*8 conversion routine
      external                              :: r4        ! Real*4 conversion routine
      external                              :: i4        ! Integer*4 conversion routine
      external                              :: i8        ! Integer*8 conversion routine
      logical,                intent(inout) :: error     !
    end subroutine gio_buffer_to_hd64
  end interface
  !
  interface
    subroutine gio_hd64_to_buffer (len,inhd64,outbuf,r8,r4,i4,i8)
      use gio_headers
      use gio_uv
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(inout)  :: len    ! Length of buffer
      integer(kind=4), intent(out) :: outbuf(*) ! Input values in file format
      type (gildas_header_v2), intent(inout) :: inhd64  ! Converted values in machine format
      external :: r8                    ! Real*8 conversion routine
      external :: r4                    ! Real*4 conversion routine
      external :: i4                    ! Integer*4 conversion routine
      external :: i8                    ! Integer*8 conversion routine
    end subroutine gio_hd64_to_buffer
  end interface
  !
  interface
    function gio_gdfbuf(rname,iblock)
      use gio_image
      use gbl_message
      ! @ private
      !
      integer :: gio_gdfbuf
      character(len=*), intent(in) :: rname
      integer, intent(in) :: iblock
    end function gio_gdfbuf
  end interface
  !
  interface
    subroutine gio_syscod (sycode)
      !---------------------------------------------------------------------
      ! @ private
      !   Return the GILDAS "System" code, '-', '_', or '.'
      !---------------------------------------------------------------------
      character(len=1), intent(out) :: sycode   ! Returned code
    end subroutine gio_syscod
  end interface
  !
  interface
    subroutine gio_cdim (is,ndims,dims)
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      ! GDF
      !       Change DIM of an Image Slot
      !       Change the dimensions of a pre-defined image slot
      !       This can be useful to reshape an image slot, and
      !               allow reading by blocks regardless of the actual
      !       underlying data structure. Typical applications include
      !       computing extrema, transposition, FITS interface...
      !
      !             It is used by GDF_READ_DATA and GDF_WRITE_DATA which can
      !         thus specify whatever shape they want for their data.
      !---------------------------------------------------------------------
      integer, intent(in) :: is                         ! Image Slot
      integer(4), intent(in) :: ndims                   ! Number of dimensions
      integer(kind=size_length), intent(in) :: dims(:)  ! Dimensions
    end subroutine gio_cdim
  end interface
  !
  interface
    subroutine gio_dams (ms,is,blc,trc,array,form,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF
      !       Data Associated Memory Slot
      !       Associate pre-defined array ARRAY to (sub)-set BLC,TRC of
      !       image IS, at slot MS.
      !---------------------------------------------------------------------
      integer(kind=4),            intent(out) :: ms        ! Memory slot
      integer(kind=4),            intent(in)  :: is        ! Image slot
      integer(kind=index_length), intent(in)  :: blc(:)    ! Bottom left corner
      integer(kind=index_length), intent(in)  :: trc(:)    ! Top right corner
      real(kind=4),               intent(in)  :: array(*)  ! Data array
      integer(kind=4),            intent(in)  :: form      ! Image format
      logical,                    intent(out) :: error     ! Flag
    end subroutine gio_dams
  end interface
  !
  interface
    subroutine gio_rems (ms,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   REad Memory Slot
      !---------------------------------------------------------------------
      integer, intent(in)  :: ms         ! Memory slot
      logical, intent(out) :: error      ! Flag
    end subroutine gio_rems
  end interface
  !
  interface
    subroutine gio_rmslot (ms,lun,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Read Memory SLOT
      !       Direct access solution
      !---------------------------------------------------------------------
      integer, intent(in)  :: ms         ! Memory slot
      integer, intent(in)  :: lun        ! Logical Unit Number
      logical, intent(out) :: error      ! Flag
    end subroutine gio_rmslot
  end interface
  !
  interface
    subroutine gio_pums (ms,is,blc,trc,addr,form,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF
      !       Put Memory Slot
      !       Associate pre-defined area ADDR to (sub)-set BLC,TRC of
      !       image IS, at slot MS.
      !---------------------------------------------------------------------
      integer, intent(out) :: ms         ! Memory slot
      integer, intent(in)  :: is         ! Image slot
      integer(kind=index_length), intent(in)  :: blc(:) ! Bottom left corner
      integer(kind=index_length), intent(in)  :: trc(:) ! Top right corner
      integer(kind=address_length), intent(in) :: addr  ! Input address
      integer, intent(in)  :: form       ! Image format
      logical, intent(out) :: error      ! Flag
    end subroutine gio_pums
  end interface
  !
  interface
    subroutine gio_lnslot (ms,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   Get a free memory slot
      !---------------------------------------------------------------------
      integer, intent(out) :: ms                     ! Memory slot number
      logical, intent(out) :: error                  !
    end subroutine gio_lnslot
  end interface
  !
  interface
    subroutine gdf_deis (is,error)
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      ! GDF DElete Image Slot
      !             whatever the consequences...
      !             Close the unit, and free it
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !---------------------------------------------------------------------
      integer, intent(in)    :: is     ! Image slot
      logical, intent(inout) :: error  ! Error flag
    end subroutine gdf_deis
  end interface
  !
  interface
    subroutine gio_gemsco (ms, is, blc, trc, addr, form, error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF
      !     GEt Memory Slot COntiguous
      !     Restricted version for contiguous subsets
      !---------------------------------------------------------------------
      integer, intent(out) :: ms                         ! Memory slot
      integer, intent(in)  :: is                         ! Image slot
      integer(kind=index_length), intent(in)  :: blc(gdf_maxdims)    ! Bottom left corner
      integer(kind=index_length), intent(in)  :: trc(gdf_maxdims)    ! Top right corner
      integer(kind=address_length), intent(out) :: addr  ! Returned address
      integer, intent(in)  :: form                       ! Image format
      logical, intent(out) :: error                      ! Flag
    end subroutine gio_gemsco
  end interface
  !
  interface
    subroutine gdf_sub4 (blc, n1,n2,n3,n4,in, m1,m2,m3,m4,out)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=index_length) :: blc(4)                 !
      integer(kind=index_length) :: n1                     !
      integer(kind=index_length) :: n2                     !
      integer(kind=index_length) :: n3                     !
      integer(kind=index_length) :: n4                     !
      integer(4) :: in(n1,n2,n3,n4)     !
      integer(kind=index_length) :: m1                     !
      integer(kind=index_length) :: m2                     !
      integer(kind=index_length) :: m3                     !
      integer(kind=index_length) :: m4                     !
      integer(4) :: out(m1,m2,m3,m4)    !
    end subroutine gdf_sub4
  end interface
  !
  interface
    subroutine gio_cont (is,ndim,blc,trc,iblc,itrc,isize,icont)
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO
      !     Determine if a slot is contiguous in memory
      !---------------------------------------------------------------------
      integer, intent(in)  :: is                         ! Image slot
      integer, intent(out) :: ndim                       ! Number of dimensions
      integer(kind=index_length), intent(in)  :: blc(:)  ! Bottom left corner
      integer(kind=index_length), intent(in)  :: trc(:)  ! Top right corner
      integer(kind=index_length), intent(out) :: iblc(:) ! Bottom left corner
      integer(kind=index_length), intent(out) :: itrc(:) ! Top right corner
      integer(kind=size_length), intent(out) :: isize    ! Size of memory region
      logical, intent(out) :: icont                      ! Contiguous flag
    end subroutine gio_cont
  end interface
  !
  interface
    subroutine gio_gmslot (offs, size, form, ms, big, stablo, error)
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private 
      ! GDF   Get a free memory slot.
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)  :: offs          ! Offset from start of image, in "pixel"
      integer(kind=size_length), intent(inout)  :: size       ! Required size (in pixels), and allocated size (in WOrds)
      integer, intent(in)  :: form          ! Type of image
      integer, intent(out) :: ms            ! Memory slot number
      integer, intent(in)  :: big           ! Are big blocks being used ?
      integer, intent(in)  :: stablo        ! Number of Starting Blocks
      logical, intent(out) :: error         ! Logical error flag
    end subroutine gio_gmslot
  end interface
  !
  interface
    subroutine gio_mmslot (offs,leng,form,ms,addr,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO
      !             Map Memory SLOT
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)  :: offs     ! Offset from start of image, in "pixel"
      integer(kind=size_length), intent(in)  :: leng     ! Required size (in Words)
      integer, intent(in)  :: form                       ! Type of image
      integer, intent(in)  :: ms                         ! Memory slot number
      integer(kind=address_length), intent(out) :: addr  ! Returned address
      logical, intent(out) :: error                      ! Logical error flag
    end subroutine gio_mmslot
  end interface
  !
  interface
    subroutine gio_wmslot (ms,lun,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Write Memory SLOT.
      ! Direct access solution, contiguous data only.
      !
      ! In case of success, error flag (T/F) is preserved on return. Code is
      ! insensitive to error=T on input.
      !---------------------------------------------------------------------
      integer, intent(in)    :: ms     ! Memory slot number
      integer, intent(in)    :: lun    ! Logical Unit Number
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gio_wmslot
  end interface
  !
  interface
    subroutine gio_buffs_vm(big,ioffs,size,ikbfi,ikbla,sizbuf,stablo)
      use gildas_def
      use gio_image
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: big ! Blocking factor
      integer(kind=size_length),   intent(inout) :: ioffs   ! Memory offset
      integer(kind=size_length),   intent(inout) :: size    ! Effective size in words
      integer(kind=record_length), intent(out)   :: ikbfi   ! First block
      integer(kind=record_length), intent(out)   :: ikbla   ! Last block
      integer(kind=4),             intent(out)   :: sizbuf  ! Buffer Size in words (output)
      integer(kind=4),             intent(in)    :: stablo  ! Number of Starting Blocks
    end subroutine gio_buffs_vm
  end interface
  !
  interface
    subroutine gio_umps (ms,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   UPdate Memory Slot
      !             Write it to file if needed
      !
      ! THIS ROUTINE IS NOW OBSOLETE (Apr 2014)
      ! and the code code_gdf_dumpi  also
      !---------------------------------------------------------------------
      integer, intent(in)  :: ms                     ! Memory slot
      logical, intent(out) :: error                  ! Error flag
    end subroutine gio_umps
  end interface
  !
  interface
    subroutine gio_zams (ms,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   ZAp Memory Slot
      !                     Just delete it, don't do anything else
      !---------------------------------------------------------------------
      integer, intent(in)  :: ms                     ! Memory slot
      logical, intent(out) :: error                  ! Error flag
    end subroutine gio_zams
  end interface
  !
  interface
    subroutine gildas_fatale(name)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name
    end subroutine gildas_fatale
  end interface
  !
  interface
    subroutine gio_whsec32(ibuf32,type,array,size,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   Stand-Alone routine
      !       Write a descriptor in Header of file, in "old" 32 bit
      !           format.
      !----------------------------------------------------------------------
      !
      !       Greg Data Frame structure
      !       Fixed length 512-bytes Records
      !
      ! First Record  : Header
      ! Others        : Data
      ! After data (if needed) : Comments
      !
      ! Basic Header for data checks
      !       'GILDAS_IMAGE  '                        ! C*12
      !       Type (Real, Real*8, Integer)
      !       Data Length (Blocks)                    !
      !       Additional Information (blocks)         !
      !                                               ! 10  Words reserved
      !
      ! KEY = GENERAL                 (1)
      !       LENGTH
      !       NDIM (<4)               1       2
      !       DIM(4)                  4       6
      !       XREF,XVAL,XINC (4 fois) 3*8     30
      !       Length = 30     Words                   ! 40
      !
      ! KEY = TABLE                   (8)
      !       LENGTH
      !       NDIM                    1       2
      !       DIM(4)                  1       6
      !       Length = 6      Words
      !
      ! KEY = BLANKING                (2)
      !       LENGTH
      !       BVAL,EVAL               2       3
      !       Length = 3      Words                   ! 43
      !
      ! KEY = EXTREMA                 (3)
      !       LENGTH
      !       RMIN,RMAX, MIN1,MAX1, MIN2,MAX2, MIN3,MAX3, MIN4,MAX4
      !       Length = 11     Words                   ! 54
      !
      ! KEY = DESCRIPTION             (4)
      !       LENGTH
      !       UNIT C*12       Map unit  3     4
      !       CODE C*12(4)    Axis type 4*3   16
      !       SYST C*12       Coord. Sy.3     19
      !       Length = 16     Words                   ! 73
      !
      ! KEY = POSITION                (5)
      !       LENGTH
      !       RSOURC C*12             3       4       ! Source name
      !       REPOCH R*4              1       5       ! Epoch of observation
      !       RRA,   R*8              2       7       ! R.A. of center position
      !       RDEC,  R*8              2       9       ! Declination ----
      !       RL,    R*8              2       11      ! Galactic longitude
      !       RB     R*8              2       13      ! Galactic latitude
      !       Length = 13     Words                   ! 86
      !
      ! KEY = PROJECTION              (6)
      !       LENGTH
      !       TYPE,A0,D0,ANGLE        1+3*2   8
      !       AXIS1,AXIS2             2*1     10      ! Projected axis
      !       Length = 10     Words                   ! 96
      !
      ! KEY = SPECTROSCOPY            (7)
      !       LENGTH
      !       RLINE  C*12             ! Line name
      !       RFRES, R*8              ! Frequency resolution
      !       RFIMA, R*8              ! Image Frequency
      !       RVRES, R*4              ! Velocity resolution
      !       RVOFF  R*4              ! Velocity offset
      !       RRESTF R*8              ! Rest frequency of transition
      !       RFAXI  I*4              ! Frequency axis
      !       Length = 13             ! 109
      !
      ! KEY = RESOLUTION              (8)
      !       LENGTH
      !       MAJOR                   ! Major axis
      !       MINOR                   ! Minor axis
      !       POSITION_ANGLE          !
      !       Length = 4              ! 113
      !---------------------------------------------------------------------
      integer, intent(inout) :: ibuf32(*)         ! Buffer where sections must be stored
      character(len=*), intent(in) :: type        ! Section name
      integer, intent(in) :: array(*)             ! Section array values
      integer, intent(in) :: size                 ! Section size
      logical :: error                  !
    end subroutine gio_whsec32
  end interface
  !
  interface
    subroutine gio_rhsec32(ibuf32,type,array,size,error)
      use gio_image
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GREG  Stand-alone routine
      !       Find information Greg Data Frame header
      !---------------------------------------------------------------------
      integer, intent(inout) :: ibuf32(*)         ! Buffer from which section must be fetched
      character(len=*), intent(in) :: type        ! Section name
      integer, intent(inout) :: array(*)          ! Section data
      integer, intent(inout) :: size              ! Section size
      logical :: error                  !
    end subroutine gio_rhsec32
  end interface
  !
  interface
    subroutine gio_cvhd32 (len,inhead,ouhead,r8,r4,i4)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in)  :: len       ! Length of buffer
      integer, intent(in)  :: inhead(*) ! Input values in file format
      integer, intent(out) :: ouhead(*) ! Converted values in machine format
      external :: r8                    ! Real*8 conversion routine
      external :: r4                    ! Real*4 conversion routine
      external :: i4                    ! Integer*4 conversion routine
    end subroutine gio_cvhd32
  end interface
  !
  interface
    subroutine gio_read32header(hd32,ibuf32,error)
      use gio_headers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GIO / GDF Internal routine
      !       Read an image header from the requested slot ISLO
      !       The slot must have been properly opened before
      !---------------------------------------------------------------------
      type (gildas_header_v1), intent(out) :: hd32   ! GILDAS image structure
      integer(kind=4), intent(inout) :: ibuf32(*)    ! Buffer of data
      logical, intent(inout) :: error                ! Error flag
    end subroutine gio_read32header
  end interface
  !
  interface
    subroutine gio_write32header(hd32,ibuf32,error)
      use gio_headers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO Internal routine
      !       Update an image header on a specified image slot
      !           The slot must have been opened before
      !---------------------------------------------------------------------
      type (gildas_header_v1), intent(inout) :: hd32   ! GILDAS type output structure
      integer(kind=4), intent(out) :: ibuf32(*)  ! Working buffer
      logical, intent(out) :: error             ! Logical error flag
    end subroutine gio_write32header
  end interface
  !
  interface
    subroutine gio_rewris(rdonly,is,gtype,name,form,size,error)
      use gildas_def
      use gio_image
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF   REad or WRite Image Slot. Common part for gio_reis and
      !       gio_wris, do not use directly.
      !---------------------------------------------------------------------
      logical,                   intent(in)  :: rdonly  ! Read or Write mode?
      integer(kind=4),           intent(in)  :: is      ! Image Slot
      character(len=*),          intent(out) :: gtype   ! Image type
      character(len=*),          intent(in)  :: name    ! File name
      integer(kind=4),           intent(out) :: form    ! Data type
      integer(kind=size_length), intent(out) :: size    ! Data size
      logical,                   intent(out) :: error   ! Error flag
    end subroutine gio_rewris
  end interface
  !
  interface
    subroutine gdf_modify_resolution(old_freq,new_freq,majo,mino,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Modify the resolution according to old and new frequencies
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: old_freq  !
      real(kind=8), intent(in)    :: new_freq  !
      real(kind=4), intent(inout) :: majo      !
      real(kind=4), intent(inout) :: mino      !
      logical,      intent(inout) :: error     !
    end subroutine gdf_modify_resolution
  end interface
  !
  interface
    integer function gio_word_length(header)
      use image_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF / GIO Internal routine
      !       Return the word length (in Bytes) of a GILDAS image
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: header           !
    end function gio_word_length
  end interface
  !
  interface
    subroutine gio_init_gildas(hx, caller)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GDF API
      !   Initialize a Gildas structure: set the required pointers
      !---------------------------------------------------------------------
      type(gildas), intent(inout), target :: hx ! Gildas structure
      character(len=*), intent(in) :: caller    ! Caller's name
    end subroutine gio_init_gildas
  end interface
  !
  interface
    subroutine gio_zero_gildas(header)
      use image_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! GDF API routine
      !    Initialize a Gildas header content to default values
      !---------------------------------------------------------------------
      type (gildas), intent(inout) :: header           !
    end subroutine gio_zero_gildas
  end interface
  !
  interface
    subroutine gio_dump_header (imag)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type (gildas), intent(in) :: imag                !
    end subroutine gio_dump_header
  end interface
  !
  interface
    subroutine gdf_transpose4(in,ou,code,iblock,space_gildas,isfits,error)
      use image_def
      use gbl_message
      use gsys_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gildas),               intent(inout) :: in            !
      type(gildas),               intent(inout) :: ou            !
      character(len=*),           intent(in)    :: code          !
      integer(kind=index_length), intent(in)    :: iblock(5)     !
      integer(kind=4),            intent(in)    :: space_gildas  ! Useable memory size (MB)
      logical,                    intent(in)    :: isfits        !
      logical,                    intent(inout) :: error         !
    end subroutine gdf_transpose4
  end interface
  !
  interface
    subroutine gdf_transpose4_disk(in,ou,iblock,space_gildas,isfits,error)
      use image_def
      use gbl_message
      use gsys_types
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! Perform the transposition on disk
      !---------------------------------------------------------------------
      type(gildas),               intent(inout) :: in            !
      type(gildas),               intent(inout) :: ou            !
      integer(kind=index_length), intent(in)    :: iblock(5)     !
      integer(kind=4),            intent(in)    :: space_gildas  ! Useable memory (MB)
      logical,                    intent(in)    :: isfits        !
      logical,                    intent(inout) :: error         !
    end subroutine gdf_transpose4_disk
  end interface
  !
  interface
    subroutine gdf_transpose4_memory(in,ou,iblock,isfits,error)
      use image_def
      use gbl_message
      use gsys_types
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! Problem does fit in memory. Loop on the last dimension
      !---------------------------------------------------------------------
      type(gildas),               intent(inout) :: in         !
      type(gildas),               intent(inout) :: ou         !
      integer(kind=index_length), intent(in)    :: iblock(5)  !
      logical,                    intent(in)    :: isfits     !
      logical,                    intent(inout) :: error      !
    end subroutine gdf_transpose4_memory
  end interface
  !
  interface
    subroutine gdf_transpose_uvcolumn8(ou,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Transpose the REAL*8 (2 words) columns which have been misordered
      ! by a previous transposition.
      !  GDF UV tables specific, because only GDF UV tables have data
      ! columns spread over several columns.
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: ou     !
      logical,      intent(inout) :: error  !
    end subroutine gdf_transpose_uvcolumn8
  end interface
  !
  interface
    function gdf_2gelems(h)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! Return .true. if the data has more than 2 giga-elements
      !---------------------------------------------------------------------
      logical :: gdf_2gelems  ! Function value on return
      type(gildas), intent(in) :: h
    end function gdf_2gelems
  end interface
  !
  interface
    function gdf_2gbytes(h)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! Return .true. if the data is larger than 2 GB (bytes, not 4-bytes
      ! words)
      !---------------------------------------------------------------------
      logical :: gdf_2gbytes  ! Function value on return
      type(gildas), intent(in) :: h
    end function gdf_2gbytes
  end interface
  !
  interface
    subroutine fits2gdf(name,error)
      use gbl_message
      use gio_params
      !---------------------------------------------------------------------
      ! @ private
      ! Automatic FITS to GDF converion
      !---------------------------------------------------------------------
      character(len=*), intent(inout)  :: name       ! File name (modified)
      logical, intent(out) :: error                  ! Error flag
    end subroutine fits2gdf
  end interface
  !
  interface
    subroutine fits2gdf_wpr(chain,answer)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      character(len=*) :: answer        !
    end subroutine fits2gdf_wpr
  end interface
  !
  interface
    subroutine fitscube2gdf_check_naxis(fhdict,ihdu,ndim,dims,error)
      use gildas_def
      use gbl_message
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read and check the NAXIS* values
      !---------------------------------------------------------------------
      type(gfits_hdict_t),        intent(in)    :: fhdict   !
      integer(kind=4),            intent(in)    :: ihdu     !
      integer(kind=4),            intent(out)   :: ndim     !
      integer(kind=index_length), intent(out)   :: dims(:)  !
      logical,                    intent(inout) :: error    !
    end subroutine fitscube2gdf_check_naxis
  end interface
  !
  interface
    subroutine fitscube2gdf_check_convert(fhdict,convert,rota,unit,error)
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Translate all the CRVALi, CRPIXi, CDELTi, CTYPEi, CROTAi
      !---------------------------------------------------------------------
      type(gfits_hdict_t),  intent(in)    :: fhdict        !
      real(kind=8),         intent(out)   :: convert(:,:)  !
      real(kind=8),         intent(out)   :: rota(:)       !
      character(len=*),     intent(out)   :: unit(:)       !
      logical,              intent(inout) :: error         !
    end subroutine fitscube2gdf_check_convert
  end interface
  !
  interface
    subroutine fitscube2gdf_check_cdmatrix(fhdict,convert,rota,error)
      use phys_const
      use gbl_message
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Apply the CD or PC matrix to the convert/rota arrays
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict        !
      real(kind=8),        intent(inout) :: convert(:,:)  !
      real(kind=8),        intent(inout) :: rota(:)       !
      logical,             intent(inout) :: error         !
    end subroutine fitscube2gdf_check_cdmatrix
  end interface
  !
  interface
    subroutine fitscube2gdf_check_array(fhdict,fd,ghead,error)
      use gbl_message
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get the binary array description
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict   !
      type(gfits_hdesc_t), intent(inout) :: fd       !
      type(gildas),        intent(inout) :: ghead    !
      logical,             intent(inout) :: error    !
    end subroutine fitscube2gdf_check_array
  end interface
  !
  interface
    subroutine fitscube2gdf_check_minmax(fhdict,nwords,datamin,datamax,error)
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get the binary array description
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict    !
      integer(kind=4),     intent(out)   :: nwords    !
      real(kind=4),        intent(out)   :: datamin   !
      real(kind=4),        intent(out)   :: datamax   !
      logical,             intent(inout) :: error     !
    end subroutine fitscube2gdf_check_minmax
  end interface
  !
  interface
    subroutine fitscube2gdf_check_system(fhdict,rota,ghead,error)
      use phys_const
      use gbl_message
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict    !
      real(kind=8),        intent(in)    :: rota(:)   !
      type(gildas),        intent(inout) :: ghead     !
      logical,             intent(inout) :: error     !
    end subroutine fitscube2gdf_check_system
  end interface
  !
  interface
    subroutine fitscube2gdf_check_spec(fhdict,unit,ghead,error)
      use phys_const
      use gbl_message
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict    !
      character(len=*),    intent(in)    :: unit(:)   !
      type(gildas),        intent(inout) :: ghead     !
      logical,             intent(inout) :: error     !
    end subroutine fitscube2gdf_check_spec
  end interface
  !
  interface
    subroutine fitscube2gdf_check_resolution(fhdict,ghead,error)
      use phys_const
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict  !
      type(gildas),        intent(inout) :: ghead   !
      logical,             intent(inout) :: error   !
    end subroutine fitscube2gdf_check_resolution
  end interface
  !
  interface
    subroutine fitscube2gdf_check_telescope(fhdict,ghead,error)
      use phys_const
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict  !
      type(gildas),        intent(inout) :: ghead   !
      logical,             intent(inout) :: error   !
    end subroutine fitscube2gdf_check_telescope
  end interface
  !
  interface
    subroutine fits2gdf_guess_style(style,error)
      use gbl_message
      use gio_params
      use gfits_types
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      !  Guess the style of the extension currently opened in GFITS
      !---------------------------------------------------------------------
      integer(kind=4), intent(out)   :: style
      logical,         intent(inout) :: error
    end subroutine fits2gdf_guess_style
  end interface
  !
  interface
    subroutine fitscube2gdf_import_sfl_as_radio(fhdict,ghead,error)
      use phys_const
      use gbl_constant
      use gbl_message
      use image_def
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! If possible, convert back the SFL projection to the usual radio
      ! projection. This patch is symetric to what is done at export time,
      ! but the export patch puts the reference on the Equator, which is a
      ! bit annoying.
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fhdict
      type(gildas),        intent(inout) :: ghead
      logical,             intent(inout) :: error
    end subroutine fitscube2gdf_import_sfl_as_radio
  end interface
  !
  interface
    subroutine read_all(fits,error,eot)
      use gildas_def
      use gbl_format
      use gbl_message
      use image_def
      use gsys_types
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! Read all the input FITS image
      !
      ! Should be re-written by using a block mode operation, with say a
      ! few Mbyte buffer, or even less...
      ! Slot re-shaping can be used for this
      ! Extrema should be carried on through, not in a separate pass...
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: fits   !
      logical,      intent(out)   :: error  ! Error flag
      logical,      intent(inout) :: eot    ! End of Tape flag
    end subroutine read_all
  end interface
  !
  interface
    subroutine read_sub(fits,error,eot,blc,trc)
      use gildas_def
      use gbl_format
      use gbl_message
      use image_def
      use gio_params
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! FITS  Support routine for command
      !
      !       READ [/BLC Nx1 Nx2 Nx3 Nx4] [/TRC Ny1 Ny2 Ny3 Ny4]
      ! Arguments :
      !       ERROR   L       Logical error flag      Output
      !---------------------------------------------------------------------
      type(gildas),          intent(inout) :: fits    !
      logical,               intent(out)   :: error   ! Error flag
      logical,               intent(inout) :: eot     ! End of Tape flag
      integer(index_length), intent(in)    :: blc(:)  !
      integer(index_length), intent(in)    :: trc(:)  !
    end subroutine read_sub
  end interface
  !
  interface
    subroutine tofits(fits,check,error)
      use gbl_message
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! GFITS
      !     Write GILDAS data set to FITS tape
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: fits   !
      logical,      intent(in)    :: check  ! Verbose flag
      logical,      intent(out)   :: error  ! Error flag
    end subroutine tofits
  end interface
  !
  interface
    subroutine to_uvfits(fits,check,error)
      use gildas_def
      use gbl_message
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! FITS        Internal routine.
      !     Write current UV data on tape.
      !---------------------------------------------------------------------
      type(gildas), intent(in)  :: fits   !
      logical,      intent(in)  :: check  ! Verbose flag
      logical,      intent(out) :: error  ! Error flag
    end subroutine to_uvfits
  end interface
  !
  interface
    subroutine wr_fitshead(fits,rmin,rmax,umin,umax,vmin,vmax,wmin,wmax,jmin,  &
      ps,telescop,check,error)
      use phys_const
      use gbl_message
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! UVFITS
      !     Write the FITS header for a visibility set.
      !---------------------------------------------------------------------
      type(gildas),     intent(in)  :: fits       !
      real(kind=4),     intent(in)  :: rmin,rmax  !
      real(kind=4),     intent(in)  :: umin,umax  !
      real(kind=4),     intent(in)  :: vmin,vmax  !
      real(kind=4),     intent(in)  :: wmin,wmax  !
      real(kind=4),     intent(in)  :: jmin       !
      real(kind=4),     intent(in)  :: ps         ! Weight scaling factor
      character(len=*), intent(in)  :: telescop   !
      logical,          intent(in)  :: check      !
      logical,          intent(out) :: error      !
    end subroutine wr_fitshead
  end interface
  !
  interface
    subroutine write_antenna_extension(telescop,diam,nant,check,error)
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: telescop  ! Array name
      real(kind=4),     intent(in)    :: diam      ! Antenna diameters (same for all)
      integer(kind=4),  intent(in)    :: nant      ! Number of antennas
      logical,          intent(in)    :: check     ! Verbose flag
      logical,          intent(inout) :: error     ! Error flag
    end subroutine write_antenna_extension
  end interface
  !
  interface
    subroutine to_imfits(fits,check,error)
      use gildas_def
      use phys_const
      use gbl_message
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! FITS
      !       Write a GDF-image onto FITS tape
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: fits   !
      logical,      intent(in)    :: check  ! Verbose flag
      logical,      intent(inout) :: error  ! Error flag
    end subroutine to_imfits
  end interface
  !
  interface
    subroutine write_axis(fits,axis_number,check,error)
      use phys_const
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! FITS
      ! Try to write a sensible output axis name and units, according to
      ! standard FITS (stupid) conventions which requires degrees instead
      ! of Radians...
      !---------------------------------------------------------------------
      type(gildas),    intent(in)    :: fits         !
      integer(kind=4), intent(in)    :: axis_number  ! Axis number
      logical,         intent(in)    :: check        ! Verbose flag
      logical,         intent(inout) :: error        ! Error flag
    end subroutine write_axis
  end interface
  !
  interface
    subroutine touvt(fits,istyle,what,check,error,pcount,getsymbol)
      use gbl_message
      use phys_const
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      ! Read a UVFITS file from tape and convert it to GDF format in virtual
      ! memory.
      !---------------------------------------------------------------------
      type(gildas),    intent(inout) :: fits       !
      integer(kind=4), intent(in)    :: istyle     ! A cryptic style code, 1,2 or something else
      logical,         intent(in)    :: what       ! Prompt on unknown axis types ?
      logical,         intent(in)    :: check      ! Verbose flag
      logical,         intent(out)   :: error      ! Error flag
      integer(kind=4), intent(out)   :: pcount     ! Parameter count
      external                       :: getsymbol  ! FITS Keyword translation routine
    end subroutine touvt
  end interface
  !
  interface
    subroutine read_uvfits(fits,astoke,error,eot,pcount)
      use image_def
      use gbl_format
      use gbl_message
      use image_def
      use gio_fitsdef
      !---------------------------------------------------------------------
      ! @ private
      !   Read all the input FITS UV table
      !---------------------------------------------------------------------
      type(gildas),    intent(inout) :: fits    !
      integer(kind=4), intent(in)    :: astoke  ! Stokes parameter
      logical,         intent(out)   :: error   ! Error flag
      logical,         intent(out)   :: eot     ! End ot Tape flag
      integer(kind=4), intent(in)    :: pcount  ! Daps count
    end subroutine read_uvfits
  end interface
  !
  interface
    subroutine key_order(pcount,syst)
      use gio_fitsdef
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !    Define the scaling factors for each parameter
      !    and find out the ordering of the required Daps
      !---------------------------------------------------------------------
      integer, intent(in) :: pcount         ! Parameter count
      character(len=*), intent(in) :: syst  ! Coordinate system
    end subroutine key_order
  end interface
  !
  interface
    subroutine gio_setistbl(is,stbl)
      use gio_image
      !--------------------------------------------------------
      ! @ private
      ! Temporary routine to set the Number of Header blocks
      ! Use by FITS interface to support Ntsokes > 1.
      !--------------------------------------------------------
      integer, intent(in) :: is     ! Image slot number
      integer, intent(in) :: stbl   ! Number of header blocks
    end subroutine gio_setistbl
  end interface
  !
  interface
    subroutine uvfits_stokes_select(inarray,inchan,outarray,onchan, &
      & nstokes,in_stokes,out_stokes,stokes_order)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !   GIO   perform the appropriate Stokes conversion
      !---------------------------------------------------------------------
      integer, intent(in) :: inchan             ! Input number of channels
      real, intent(in) :: inarray(3,inchan)     ! Input visibilitiees
      integer, intent(in) :: onchan             ! Output number of channels
      real, intent(out) :: outarray(3,onchan)   ! Output visibilities
      integer, intent(in) :: nstokes            ! Number of input Stokes
      integer, intent(in) :: in_stokes(nstokes) ! Input Stokes codes
      integer, intent(in) :: out_stokes         ! Output Stokes
      integer, intent(in) :: stokes_order       ! Stokes ordering
    end subroutine uvfits_stokes_select
  end interface
  !
end module gio_interfaces_private
