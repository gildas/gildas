!-----------------------------------------------------------------------
! Set of utilities to access some GDF properties, most often derived
! from header itself.
!-----------------------------------------------------------------------
function gag_sizeof(form)
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public
  ! Return data word length in Bytes
  ! This function is generic for GDF, SIC variables, etc as they share
  ! the same codes. Hence the prefix gag_
  !---------------------------------------------------------------------
  integer(kind=4) :: gag_sizeof     ! Function value on return
  integer(kind=4), intent(in) :: form ! Data format
  !
  gag_sizeof=4
  if (form.eq.fmt_r8)then
    gag_sizeof=8
  elseif (form.eq.fmt_i8)then
    gag_sizeof=8
  elseif (form.eq.fmt_c4)then
    gag_sizeof=8
  elseif (form.eq.fmt_c8)then
    gag_sizeof=16
  elseif (form.eq.fmt_by)then
    gag_sizeof=1
  elseif (form.gt.0)then
    gag_sizeof=form
  endif
end function gag_sizeof
!
function gdf_nelem(h)
  use image_def
  !---------------------------------------------------------------------
  ! @ public
  ! Return the number of elements from the input GDF header
  !---------------------------------------------------------------------
  integer(kind=size_length) :: gdf_nelem
  type(gildas), intent(in) :: h
  ! Local
  integer(kind=4) :: jdim
  !
  gdf_nelem = 1
  do jdim=1,h%gil%ndim
    gdf_nelem = gdf_nelem * max(1,h%gil%dim(jdim))
  enddo
  !
end function gdf_nelem
!
function gdf_size(h)
  use image_def
  use gio_interfaces, except_this=>gdf_size
  !---------------------------------------------------------------------
  ! @ public
  ! Return the whole data size in bytes
  !---------------------------------------------------------------------
  integer(kind=size_length) :: gdf_size
  type(gildas), intent(in) :: h
  !
  gdf_size = gdf_nelem(h) * gag_sizeof(h%gil%form)
  !
end function gdf_size
!
function gdf_2gelems(h)
  use image_def
  use gio_interfaces, except_this=>gdf_2gelems
  !---------------------------------------------------------------------
  ! @ private
  ! Return .true. if the data has more than 2 giga-elements
  !---------------------------------------------------------------------
  logical :: gdf_2gelems  ! Function value on return
  type(gildas), intent(in) :: h
  ! Local
  integer(kind=8), parameter :: maxi4=2147483647_8 ! 2**31-1
  !
  gdf_2gelems = gdf_nelem(h).gt.maxi4
end function gdf_2gelems
!
function gdf_2gbytes(h)
  use image_def
  use gio_interfaces, except_this=>gdf_2gbytes
  !---------------------------------------------------------------------
  ! @ private
  ! Return .true. if the data is larger than 2 GB (bytes, not 4-bytes
  ! words)
  !---------------------------------------------------------------------
  logical :: gdf_2gbytes  ! Function value on return
  type(gildas), intent(in) :: h
  ! Local
  integer(kind=8), parameter :: size4max=2147483647  ! 2**31-1
  !
  gdf_2gbytes = gdf_size(h).gt.size4max
end function gdf_2gbytes
!
subroutine gdf_toobig(h,error)
  use image_def
  use gbl_message
  use gio_interfaces, except_this=>gdf_toobig
  !---------------------------------------------------------------------
  ! @ public
  ! Return an error if the image is too large to be opened on the
  ! current system
  !---------------------------------------------------------------------
  type(gildas), intent(in)    :: h
  logical,      intent(inout) :: error       !
  ! Local
  character(len=*), parameter :: rname='GDF'
  !
#if defined(BITS64)
  ! There maybe limits on the number of elements
  if (gdf_2gelems(h)) then
    if (index_length.eq.8) then
      ! Ok
      continue
    else
      call gio_message(seve%e,rname,  &
      'Images or UV tables of more than 2**31-1 elements not supported')
      error = .true.
      return
    endif
  endif
#else
  if (gdf_2gbytes(h)) then
    ! File is larger than 2 GB. We can think to use the blc and trc,
    ! but there are offsets in bytes which are computed by the
    ! file access layer (even if the underlying operating system is
    ! purely 64 bits) that overflow.
    !
    ! So even having moffs being of kind=size_length set to 64 bits
    ! does not help
    error = .true.
    call gio_message(seve%e,rname,  &
      'Files larger than 2 GB are not supported under 32 bits architecture')
  endif
#endif
  !
end subroutine gdf_toobig
