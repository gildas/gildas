subroutine gdf_update_image(imag,array,error)
  use gildas_def
  use image_def
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_update_image
  use gio_params
  !---------------------------------------------------------------------
  ! @ public
  ! GDF API Routine
  !     Save an image structure and the specified data on the image file
  !     This version is for updating an existing file
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: imag      ! Image descriptor
  real,         intent(in)    :: array(*)  ! Data to be written
  logical,      intent(inout) :: error     ! Flag
  ! Local
  integer :: islo
  !
  call gio_geis (islo,error)
  if (error) return
  imag%loca%islo = islo
  !
  ! Open image
  imag%status = code_write_data
  call gio_wris (islo,imag%char%type,imag%file,imag%gil%form, &
       & imag%loca%size,error)
  if (error) then
    call gio_message(seve%e,'GDF_UPDATE_IMAGE','Cannot update output file')
    call gio_fris(islo,error)
    return
  endif
  !
  ! Associate pre-defined memory area to image
  imag%loca%addr = locwrd(array)
  call gio_pums (imag%loca%mslo,islo,imag%blc,imag%trc,imag%loca%addr,  &
  imag%gil%form,error)
  if (error) then
    call gio_fris(islo,error)
    return
  endif
  !
  ! Dump to disk
  call gio_fris(islo,error)
  if (.not.error) imag%status = 0
end subroutine gdf_update_image
!
subroutine gdf_write_image(imag,array,error)
  use gildas_def
  use image_def
  use gbl_message
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_write_image
  use gio_params
  !---------------------------------------------------------------------
  ! @ public 
  ! GDF API routine
  !     Save an image structure and the specified data on the image file
  !     This version is to create a new file
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: imag      ! Image descriptor
  real,         intent(in)    :: array(*)  ! Data array
  logical,      intent(inout) :: error     ! Flag
  ! Local
  character(len=*), parameter :: rname = 'GDF_WRITE_IMAGE'
  character(len=filename_length) :: fich
  integer :: i, islo
  !
  imag%status = code_write_header
  fich = imag%file
  call sic_parsef(fich,imag%file,' ','.gdf')
  call gio_geis (islo,error)
  if (error) return
  imag%loca%islo = islo
  !
  ! Use the proper routine here
  call gio_write_header(imag,islo,error)
  if (error) return
  !
  imag%loca%size = 1
  do i=1,imag%gil%ndim
    imag%loca%size = imag%loca%size*imag%gil%dim(i)
  enddo
  !
  ! Create image
  imag%status = code_write_data
  call gio_cris (islo,imag%char%type,imag%file,imag%gil%form, &
       & imag%loca%size,error)
  if (error) then
    call gio_message(seve%e,'GDF_WRITE_IMAGE','Cannot create output file')
    call gio_fris(islo,error)
    return
  endif
  !
  ! For UV Data, use specialized routine
  if (abs(imag%gil%type_gdf).eq.abs(code_gdf_uvt)) then
    if (any(imag%blc.ne.0) .or. any(imag%trc.ne.0)) then
      call gio_message(seve%w,rname,'Writing subset of UV data, not tested')
      !
      ! Associate pre-defined memory area to image
      imag%loca%addr = locwrd(array)
      call gio_pums (imag%loca%mslo,islo,imag%blc,imag%trc,imag%loca%addr,  &
      imag%gil%form,error)
    else
       call gdf_write_uvall(imag,array,error)
   endif
  else
    !
    ! Associate pre-defined memory area to image
    imag%loca%addr = locwrd(array)
    call gio_pums (imag%loca%mslo,islo,imag%blc,imag%trc,imag%loca%addr,  &
    imag%gil%form,error)
  endif
  !
  if (error) then
    call gio_fris(islo,error)
    return
  endif
  !
  ! Free image slot & Dump to disk
  call gio_fris(islo,error)
  if (.not.error) imag%status = 0
end subroutine gdf_write_image
!
subroutine gdf_open_image(imag,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_open_image
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! GDF API
  !     Open an image structure on the requested file for writing
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: imag   ! Image descriptor
  logical,      intent(inout) :: error  ! Flag
  ! Local
  character(len=filename_length) :: fich
  integer :: islo
  !
  imag%status = code_write_header
  fich = imag%file
  call sic_parsef(fich,imag%file,' ','.gdf')
  call gio_geis (islo,error)
  if (error) return
  call gio_wris (islo,imag%char%type,imag%file,imag%gil%form,imag%loca%size,  &
  error)
  if (error) then
    call gio_message(seve%e,'GDF_OPEN_IMAGE','Cannot open input file')
    call gio_fris(islo,error)
    return
  endif
  imag%status = 0
  imag%loca%islo = islo
end subroutine gdf_open_image
!
subroutine gdf_create_image(imag,error)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gdf_create_image
  use gio_params
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! GDF API Routine
  !     Create an image structure on the requested file
  !     Nothing is written yet to the file.
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: imag   ! Image descriptor
  logical,      intent(inout) :: error  ! Flag
  ! Local
  character(len=*), parameter :: rname='GDF_CREATE_IMAGE'
  character(len=filename_length) :: fich
  integer :: i, islo
  !
  imag%status = code_write_header
  call gio_geis (islo,error)
  if (error) return
  imag%loca%islo = islo
  !
  if (imag%file.eq.'') then
    call gio_message(seve%e,rname,'Empty file name')
    error = .true.
    return
  endif
  fich = imag%file
  call sic_parsef(fich,imag%file,' ','.gdf')
  !
  ! Use the proper routine here
  call gio_write_header(imag,islo,error)
  if (error) return
  !
  imag%loca%size = 1
  do i=1,imag%gil%ndim
    imag%loca%size = imag%loca%size*imag%gil%dim(i)
  enddo
  !
  ! Create image
  imag%status = code_write_data
  call gio_cris (islo,imag%char%type,imag%file,imag%gil%form, &
       & imag%loca%size,error)
  if (error) then
    call gio_message(seve%e,rname,'Cannot create output file')
    call gio_fris(islo,error)
    return
  endif
  !
  imag%loca%getvm = .false.
  imag%status = 0
end subroutine gdf_create_image
!
subroutine gdf_close_image(imag,error)
  use gio_interfaces, except_this=>gdf_close_image
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Close an image structure. No further READ allowed on this image
  !
  ! In case of success, error flag (T/F) is preserved on return. Code is
  ! insensitive to error=T on input.
  !---------------------------------------------------------------------
  type(gildas), intent(inout) :: imag   ! Image descriptor
  logical,      intent(inout) :: error  ! Flag
  ! Local
  integer :: islo
  !
  islo = imag%loca%islo
  imag%status = code_free_image
  if (gdf_stis(islo).eq.-1) then
    call gio_message(seve%e,'GDF_CLOSE_IMAGE','No such image '//trim(imag%file))
    error = .true.
    return
  endif
  call gio_fris(islo,error)
  !
  ! Set the image slot to Undefined
  imag%loca%islo = 0
  imag%status = 0
  !
  ! Leave the error status as it was.
  !! error = .false.
end subroutine gdf_close_image
!
