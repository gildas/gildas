subroutine gio_hd32_to_gildas_v1(hd32,v1)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer, intent(in) :: hd32
  integer, intent(in) :: v1
  print *,'gio_hd32_to_gildas_v1 not called in GILDAS_V2 Version'
end subroutine gio_hd32_to_gildas_v1
!
subroutine gio_gildas_v1_to_hd32(v1,hd32)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer, intent(in) :: hd32
  integer, intent(in) :: v1
  print *,'gio_hd32_to_gildas_v1 not called in GILDAS_V2 Version'
end subroutine gio_gildas_v1_to_hd32
!
subroutine gio_gildas_v1_to_hd64(v1,hd64)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer, intent(in) :: hd64
  integer, intent(in) :: v1
  print *,'gio_gildas_v1_to_hd64 not called in GILDAS_V2 Version'
end subroutine gio_gildas_v1_to_hd64
!
subroutine gio_hd64_to_gildas_v1(v1,hd64)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer, intent(in) :: hd64
  integer, intent(in) :: v1
  print *,'gio_hd64_to_gildas_v1 not called in GILDAS_V2 Version'
end subroutine gio_hd64_to_gildas_v1
