!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GIO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gio_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gio_message_id = gpack_global_id  ! Default value for startup message
  !
end module gio_message_private
!
subroutine gio_message_set_id(id)
  use gio_interfaces, except_this=>gio_message_set_id
  use gio_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gio_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gio_message_id
  call gio_message(seve%d,'gio_message_set_id',mess)
  !
end subroutine gio_message_set_id
!
subroutine gio_message(mkind,procname,message)
  use gio_dependencies_interfaces
  use gio_interfaces, except_this=>gio_message
  use gio_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gio_message_id,mkind,procname,message)
  !
end subroutine gio_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
