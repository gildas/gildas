module gfits_blanking
  !
  ! Blanking value definitions
  !
#if defined(VAX)
  real(4) :: r4bval=1.23456e+38
  real(8) :: r8bval=1.23456d+38
#endif
#if defined(IEEE)
  integer(2) :: r4bval(2)=0
  integer(2) :: r8bval(4)=0
#endif
#if defined(EEEI)
  integer(4) :: r4bval=0
  integer(4) :: r8bval(2)=0
#endif
end module gfits_blanking
!
subroutine iei4ei (inpu,oupu,ni)
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 4-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=4) :: ni             ! Number of items
  integer(kind=1) :: inpu(4,ni)     ! Input array
  integer(kind=1) :: oupu(4,ni)     ! Output array
  ! Local
  integer(kind=4) :: i
  integer(kind=1) :: tmp(4), abyt(4)
  !
entry eii4ie (inpu,oupu,ni)
entry vai4ei (inpu,oupu,ni)
entry eii4va (inpu,oupu,ni)
entry eir4ie (inpu,oupu,ni)
entry ier4ei (inpu,oupu,ni)
  !
  do i=1,ni
    tmp = inpu(1:4,i)
    abyt(1) = tmp(4)
    abyt(2) = tmp(3)
    abyt(3) = tmp(2)
    abyt(4) = tmp(1)
    oupu(1:4,i) = abyt
  enddo
end subroutine iei4ei
!
subroutine vai4ie (inpu,oupu,ni)
  !---------------------------------------------------------------------
  ! @ private
  ! Simple copy: vax and swapped IEEE I4 are identical. Provided for
  ! generality
  !---------------------------------------------------------------------
  integer(kind=4) :: ni        ! Number of items
  integer(kind=4) :: inpu(ni)  ! Input array
  integer(kind=4) :: oupu(ni)  ! Output array
  ! Local
  integer(kind=4) :: i
  !
entry iei4va (inpu,oupu,ni)
  do i=1,ni
    oupu(i) = inpu(i)
  enddo
end subroutine vai4ie
!
subroutine iei2ei (inpu,oupu,ni)
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 2-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=4) :: ni          ! Number of items
  integer(kind=1) :: inpu(2,ni)  ! Output array
  integer(kind=1) :: oupu(2,ni)  ! Input array
  ! Local
  integer(kind=4) :: i
  integer(kind=1) :: tmp(2),abyt(2)
  !
entry eii2ie (inpu,oupu,ni)
entry vai2ei (inpu,oupu,ni)
entry eii2va (inpu,oupu,ni)
  !
  do i=1,ni
    tmp = inpu(1:2,i)
    abyt(1) = tmp(2)
    abyt(2) = tmp(1)
    oupu(1:2,i) = abyt
  enddo
end subroutine iei2ei
!
subroutine eir8ie (in,out,ni)
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 8-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=4) :: ni         ! Number of items
  integer(kind=1) :: in(8,ni)   ! Input array
  integer(kind=1) :: out(8,ni)  ! Output array
  ! Local
  integer(kind=4) :: i
  integer(kind=1) :: tmp(8), abyt(8)
  !
entry iei8ei (in,out,ni)
entry eii8ie (in,out,ni)
entry ier8ei (in,out,ni)
  !
  do i=1,ni
    tmp = in(1:8,i)
    abyt(1) = tmp(8)
    abyt(2) = tmp(7)
    abyt(3) = tmp(6)
    abyt(4) = tmp(5)
    abyt(5) = tmp(4)
    abyt(6) = tmp(3)
    abyt(7) = tmp(2)
    abyt(8) = tmp(1)
    out(1:8,i) = abyt
  enddo
end subroutine eir8ie
!
#if defined(VAX)
!
subroutine ier4va (ieee,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 32-bit items
  integer(2) :: ieee(2,ni)           ! Input array
  real(4) :: real(ni)                ! Output array
  ! Local
  integer(4) :: entier,expo
  integer(4), parameter :: ieee_expm=Z'00007F80'
  integer(2) :: int2(2)
  integer(kind=1) :: octet(4)
  integer :: i
  real(4) :: reel
  equivalence (entier,int2)
  equivalence (entier,octet)
  equivalence (entier,reel)
  ! Code
  do i=1,ni
    int2 (2) = ieee(1,i)
    int2 (1) = ieee(2,i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(i) = r4bval
    elseif (expo.eq.0) then
      real(i) = 0.0
    elseif (octet(2).ge.127) then
      real(i) = r4bval
    else
      real(i) = 4.0*reel
    endif
  enddo
end subroutine ier4va
!
subroutine eir4va (eeei,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(4) :: ni                  ! Number of 32-bit items
  integer(kind=1) :: eeei (4,ni)    ! Input array
  real(4) :: real(ni)               ! Output array
  ! Local
  integer(4) :: entier,expo
  integer(kind=1) :: octet(4)
  integer :: i
  real(4) :: reel
  equivalence (entier,octet)
  equivalence (entier,reel)
  integer(4), parameter :: ieee_expm=Z'00007F80'
  !
  do i=1,ni
    octet(2) = eeei(1,i)
    octet(1) = eeei(2,i)
    octet(4) = eeei(3,i)
    octet(3) = eeei(4,i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(i) = r4bval
    elseif (expo.eq.0) then
      real(i) = 0.0
    elseif (octet(2).ge.127) then
      real(i) = r4bval
    else
      real(i) = 4.0*reel
    endif
  enddo
end subroutine eir4va
!
subroutine var4ie (real,ieee,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  real(4) :: real(ni)               ! Input array
  integer(2) :: ieee(2,ni)          ! Output array
  ! Local
  integer :: i
  real(4) :: reel
  integer(2) :: int2(2)
  equivalence (int2,reel)
  !
  do i=1,ni
    reel = 0.25*real(i)
    ieee(1,i) = int2(2)
    ieee(2,i) = int2(1)
  enddo
end subroutine var4ie
!
subroutine var4ei (real,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  real(4) :: real(ni)               ! Input array
  integer(kind=1) :: eeei(4,ni)     ! Output array
  ! Local
  integer :: i
  real(4) :: reel
  integer(kind=1) :: octet(4)
  equivalence (octet,reel)
  !
  do i=1,ni
    reel = 0.25*real(i)
    eeei(1,i) = octet(2)
    eeei(2,i) = octet(1)
    eeei(3,i) = octet(4)
    eeei(4,i) = octet(3)
  enddo
end subroutine var4ei
!
subroutine eir8va (eeei,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 64-bit items
  integer(kind=1) :: eeei(8,ni)      ! Input array
  real(8) :: real(ni)                ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(kind=1) :: octet(8)
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (octet,tmp4)
  equivalence (tmp2,tmp4)
  integer(2) :: ou(4),mant
  equivalence (ou,reel)
  !
  do i=1,ni
    octet(8) = eeei(1,i)
    octet(7) = eeei(2,i)
    octet(6) = eeei(3,i)
    octet(5) = eeei(4,i)
    octet(4) = eeei(5,i)
    octet(3) = eeei(6,i)
    octet(2) = eeei(7,i)
    octet(1) = eeei(8,i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm)   ! Exponent
      sign = iand(tmp2(4),ieee_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)    ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(i) = 0.0d0
    elseif (expo.gt.125) then  ! Check exponent overflow
      real(i) = r8bval
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
      ou(1) = tmp2(4)          ! Reverse 16 bit words
      ou(2) = tmp2(3)
      ou(3) = tmp2(2)
      ou(4) = tmp2(1)
      real(i) = 4.d0*reel
    endif
  enddo
end subroutine eir8va
!
subroutine ier8va (ieee,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 64-bit items
  integer(4) :: ieee(2,ni)           ! Input array
  real(8) :: real(ni)                ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  integer(2) :: ou(4),mant
  equivalence (ou,reel)
  !
  do i=1,ni
    tmp4(1) = ieee(1,i)
    tmp4(2) = ieee(2,i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm)   ! Exponent
      sign = iand(tmp2(4),ieee_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)    ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(i) = 0.0d0
    elseif (expo.gt.125) then  ! Check exponent overflow
      real(i) = r8bval
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
      ou(1) = tmp2(4)          ! Reverse words
      ou(2) = tmp2(3)
      ou(3) = tmp2(2)
      ou(4) = tmp2(1)
      real(i) = 4.d0*reel
    endif
  enddo
end subroutine ier8va
!
subroutine var8ie (real,ieee,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  real(8) :: real(ni)                ! Input array
  integer(4) :: ieee(2,ni)           ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'x
  integer(2), parameter :: vaxd_mant=Z'007F'x
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  integer(2) :: in(4),mant
  equivalence (in,reel)
  !
  do i=1,ni
    ! Divide by 4.0
    if (real(i).eq.0.0d0) then
      ieee(1,i) = 0
      ieee(2,i) = 0
      cycle                    ! I
    endif
    reel = 0.25d0*real(i)
    tmp2(1) = in(4)            ! Swap 16-bit words
    tmp2(2) = in(3)
    tmp2(3) = in(2)
    tmp2(4) = in(1)
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm)   ! Exponent
      sign = iand(tmp2(4),vaxd_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)    ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    ieee(1,i) = tmp4(1)        ! Move 32-bit words
    ieee(2,i) = tmp4(2)
  enddo
end subroutine var8ie
!
subroutine var8ei (real,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  real(8) :: real(ni)                ! Input array
  integer(kind=1) :: eeei(8,ni)     ! Output array
  ! Local
  integer :: i,j
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'
  integer(2), parameter :: vaxd_mant=Z'007F'
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(kind=1) :: octet(8)
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (octet,tmp4)
  equivalence (tmp2,tmp4)
  integer(2) :: in(4),mant
  equivalence (in,reel)
  !
  do i=1,ni
    ! Divide by 4.0
    if (real(i).eq.0.0d0) then
      do j=1,8
        eeei(j,i) = 0
      enddo
      cycle                    ! I
    endif
    reel = 0.25d0*real(i)
    tmp2(1) = in(4)            ! Swap 16-bit words
    tmp2(2) = in(3)
    tmp2(3) = in(2)
    tmp2(4) = in(1)
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm)   ! Exponent
      sign = iand(tmp2(4),vaxd_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)    ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    eeei(1,i) = octet(8)       ! Swap Bytes
    eeei(2,i) = octet(7)
    eeei(3,i) = octet(6)
    eeei(4,i) = octet(5)
    eeei(5,i) = octet(4)
    eeei(6,i) = octet(3)
    eeei(7,i) = octet(2)
    eeei(8,i) = octet(1)
  enddo
end subroutine var8ei
!
#endif
!
#if defined(IEEE)
subroutine ier4va (ieee32,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines, and most likely correct...
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 32-bit items
  real(4) :: ieee32(ni)              ! Input array
  integer(2) :: real(2,ni)           ! Output array
  ! Local
  integer(4), parameter :: ieee_expm=Z'7F800000'
  integer(4) :: entier,expo
  integer(2) :: int2(2)
  integer(kind=1) :: octet(4)
  integer :: i
  real(4) :: reel
  equivalence (entier,int2)
  equivalence (entier,octet)
  equivalence (entier,reel)
  !
  do i=1,ni
    reel = ieee32(i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(1,i) = r4bval(1)
      real(2,i) = r4bval(2)
    elseif (expo.eq.0) then
      real(1,i) = 0
      real(2,i) = 0
    elseif (int2(1).eq.32256) then
      real(1,i) = r4bval(1)
      real(2,i) = r4bval(2)
    else
      reel = 4.0*reel
      real(1,i) = int2 (2)
      real(2,i) = int2 (1)
    endif
  enddo
end subroutine ier4va
!
subroutine var4ie (real,ieee32,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  integer(2) :: real(2,ni)           ! Input array
  real(4) :: ieee32(ni)              ! Output array
  ! Local
  integer :: i
  real(4) :: reel
  integer(2) :: int2(2)
  equivalence (int2,reel)
  !
  do i=1,ni
    int2(2) = real(1,i)
    int2(1) = real(2,i)
    ieee32(i) = 0.25*reel
  enddo
end subroutine var4ie
!
subroutine ier8va (ieee64,real,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 64-bit items
  real(8) :: ieee64(ni)              ! Input array
  integer(2) :: real(4,ni)           ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    reel = 4.0d0*ieee64(i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm) ! Exponent
      sign = iand(tmp2(4),ieee_sign) ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)  ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(1,i) = 0
      real(2,i) = 0
      real(3,i) = 0
      real(4,i) = 0
    elseif (expo.ge.127) then
      real(1,i) = r8bval(1)
      real(2,i) = r8bval(2)
      real(3,i) = r8bval(3)
      real(4,i) = r8bval(4)
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))    ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))    ! Set sign
      real(1,i) = tmp2(4)      ! Reverse words
      real(2,i) = tmp2(3)
      real(3,i) = tmp2(2)
      real(4,i) = tmp2(1)
    endif
  enddo
end subroutine ier8va
!
subroutine var8ie (real,ieee64,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  integer(2) :: real(4,ni)           ! Input array
  real(8) :: ieee64(ni)              ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'
  integer(2), parameter :: vaxd_mant=Z'007F'
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    tmp2(4) = real(1,i)        ! Swap 16-bit words
    tmp2(3) = real(2,i)
    tmp2(2) = real(3,i)
    tmp2(1) = real(4,i)
    if (tmp4(1).eq.0 .and. tmp4(2).eq.0) then
      ieee64(i) = 0.0d0
      cycle                    ! I
    endif
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm) ! Exponent
      sign = iand(tmp2(4),vaxd_sign) ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)  ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))    ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))    ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    ieee64(i) = 0.25d0*reel
  enddo
end subroutine var8ie
!
subroutine eir4va (eeei,real,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(4) :: ni                 ! Number of 32-bit items
  integer(4) :: eeei(ni)           ! Input array
  integer(4) :: real(ni)           ! Output array
  call eir4ie(eeei,eeei,ni)
  call ier4va(eeei,real,ni)
end subroutine eir4va
!
subroutine var4ei (real,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 32-bit items
  integer(4) :: real(ni)             ! Input array
  integer(4) :: eeei(ni)             ! Output array
  call var4ie(real,eeei,ni)
  call ier4ei(eeei,eeei,ni)
end subroutine var4ei
!
subroutine eir8va (eeei,real,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(4) :: ni                 ! Number of 64-bit items
  real(8) :: eeei(ni)           ! Input array
  real(8) :: real(ni)           ! Output array
  call eir8ie(eeei,eeei,ni)
  call ier8va(eeei,real,ni)
end subroutine eir8va
!
subroutine var8ei (real,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(4) :: ni                 ! Number of 64-bit items
  real(8) :: real(ni)           ! Input array
  real(8) :: eeei(ni)           ! Output array
  call var8ie(real,eeei,ni)
  call ier8ei(eeei,eeei,ni)
end subroutine var8ei
#endif
!
#if defined(EEEI)
subroutine eir4va (eeei,vaxr,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for EEEI machines.
  !---------------------------------------------------------------------
  integer(4) :: ni                   ! Number of 32-bit items
  integer(4) :: eeei(ni)             ! Input array
  integer(4) :: vaxr(ni)             ! Output array
  ! Local
  integer(4), parameter :: eeei_expm=Z'7F800000'
  integer(4) :: reelin,reelou,expo
  integer(2) :: int2(2)
  integer(kind=1) :: tmpin(4),tmpou(4)
  integer :: i
  real(4) :: reel
  equivalence (reelin,reel)
  equivalence (reelin,tmpin)
  equivalence (reelou,tmpou)
  equivalence (int2,expo)
  !
  do i=1,ni
    ! Convert to swapped VAX reals
    reelin = eeei(i)
    ! Check special cases.
    expo = iand(reelin,eeei_expm)
    if (expo.eq.eeei_expm) then
      vaxr(i) = r4bval
    elseif (expo.eq.0) then
      vaxr(i) = 0
    elseif (int2(1).eq.32256) then
      vaxr(i) = r4bval
    else
      ! Swap to VAX reals (1234 --> 2143).
      ! Equivalent to
      !      unless (tmpin(1)==(126,127,-2,-1,0))
      !      tmpou(2) = tmpin(1)+1     ! Exponent field
      !      tmpou(1) = tmpin(2)
      !      tmpou(3) = tmpin(4)
      !      tmpou(4) = tmpin(3)
      !
      reel = 4.0*reel
      tmpou(2) = tmpin(1)
      tmpou(1) = tmpin(2)
      tmpou(4) = tmpin(3)
      tmpou(3) = tmpin(4)
      vaxr(i) = reelou
    endif
  enddo
end subroutine eir4va
!
subroutine var8ei (vaxr,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !     Convert NI VAX reals to NI EEEI reals
  !     This version is for EEEI machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 64-bit items
  integer(kind=1) :: vaxr(8,ni)     ! Input array
  real(8) :: eeei(ni)               ! Output array
  ! Local
  integer :: i
  integer(2), parameter :: vxdsig=Z'8000'
  integer(2), parameter :: vxdexp=Z'7F80'
  integer(2), parameter :: vxdman=Z'007F'
  integer, parameter :: vxgbt3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  integer(kind=1) :: tmp1(8)
  equivalence (tmp1,tmp2)
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    tmp1 (2) = vaxr (1,i)
    tmp1 (1) = vaxr (2,i)
    tmp1 (4) = vaxr (3,i)
    tmp1 (3) = vaxr (4,i)
    tmp1 (6) = vaxr (5,i)
    tmp1 (5) = vaxr (6,i)
    tmp1 (8) = vaxr (7,i)
    tmp1 (7) = vaxr (8,i)
    if (tmp4(1).eq.0 .and. tmp4(2).eq.0) then
      eeei(i) = 0.0d0
      cycle                    ! I
    endif
    !
    if (tmp4(1).ne.0) then
      expo = iand( tmp2(1),vxdexp )
      sign = iand( tmp2(1),vxdsig )
      tmp2(1) = iand( tmp2(1),vxdman )
      tmp4(1) = ishftc(tmp4(1),29,32)
      hbit3 = iand ( tmp4(1),vxgbt3 )
      tmp4(1) = tmp4(1)-hbit3
      expo = expo/128
      expo = expo-128
      expo = expo+1024
      expo = expo*16
      tmp2(1) = ior( expo,tmp2(1) )
      tmp2(1) = ior( sign,tmp2(1) )
    else
      tmp2(1) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(2).ne.0) then
      tmp4(2) = ishftc(tmp4(2),29,32)
      lbit3 = iand ( tmp4(2),vxgbt3 )
      tmp4(2) = tmp4(2)-lbit3
    endif
    tmp4(2) = ior ( tmp4(2),hbit3 )
    eeei(i) = 0.25d0*reel
  enddo
end subroutine var8ei
!
subroutine eir8va (eeei,vaxr,ni)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !     Convert NI EEEI reals to NI VAX reals
  !     This version is for EEEI machines
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 64-bit items
  real(8) :: eeei(ni)               ! Input array
  integer :: vaxr(2,ni)             ! Output array
  ! Local
  integer :: i
  integer(2) :: tmp2(4),expo,sign
  integer(2), parameter :: eeei_sign=Z'8000'
  integer(2), parameter :: eeei_mant=Z'000F'
  integer(2), parameter :: eeei_expm=Z'7FF0'
  integer :: tmp4(2),lbit3,reelou(2)
  real(8) :: reel
  integer(kind=1) :: tmp1(8),tmpou(8)
  equivalence (tmp1,tmp2)
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  equivalence (reelou,tmpou)
  !
  ! Order is Sign, Exponent, Mantissa
  do i=1,ni
    reel = 4.0d0*eeei(i)
    if (tmp4(1).ne.0) then
      expo = iand(tmp2(1),eeei_expm)
      sign = iand(tmp2(1),eeei_sign)
      tmp2(1) = iand(tmp2(1),eeei_mant)
      expo = expo/16
      expo = expo-1024
    else
      expo = -1024
    endif
    if (tmp4(2).ne.0) then
      tmp4(2) = ishftc(tmp4(2),3,32)
      lbit3 = iand(tmp4(2),7)
      tmp4(2) = tmp4(2)-lbit3
      tmp4(1) = 8*tmp4(1)+lbit3
    elseif (tmp4(1).ne.0) then
      tmp4(1) = 8*tmp4(1)
    endif
    ! Special cases
    if (expo.lt.-128) then
      vaxr(1,i) = 0
      vaxr(2,i) = 0
    elseif (expo.ge.127) then
      vaxr(1,i) = r8bval(1)
      vaxr(2,i) = r8bval(2)
    else
      expo = expo+128
      expo = expo*128
      tmp2(1) = ior(expo,tmp2(1))
      tmp2(1) = ior(sign,tmp2(1))
      tmpou(2) = tmp1(1)
      tmpou(1) = tmp1(2)
      tmpou(4) = tmp1(3)
      tmpou(3) = tmp1(4)
      tmpou(6) = tmp1(5)
      tmpou(5) = tmp1(6)
      tmpou(8) = tmp1(7)
      tmpou(7) = tmp1(8)
      vaxr(1,i) = reelou(1)
      vaxr(2,i) = reelou(2)
    endif
  enddo
end subroutine eir8va
!
subroutine var8ie (in,ou,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer :: ni                     ! Number of 64-bit items
  call var8ei (in,ou,ni)
  call eir8ie (ou,ou,ni)
end subroutine var8ie
!
subroutine ier8va (in,ou,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer :: ni                     ! Number of 64-bit items
  call ier8ei (in,ou,ni)
  call eir8va (ou,ou,ni)
end subroutine ier8va
!
subroutine var4ie (in,ou,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer :: ni                     ! Number of 32-bit items
  call var4ei (in,ou,ni)
  call eir4ie (ou,ou,ni)
end subroutine var4ie
!
subroutine ier4va (in,ou,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer :: ni                     ! Number of 32-bit items
  call ier4ei (in,ou,ni)
  call eir4va (ou,ou,ni)
end subroutine ier4va
!
subroutine var4ei (vaxr,eeei,ni)
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for EEEI machines.
  !---------------------------------------------------------------------
  integer :: ni                     ! Number of 32-bit items
  integer(kind=1) :: vaxr(4,ni)     ! Input array
  real(4) :: eeei(ni)               ! Output array
  ! Local
  integer :: i
  real(4) :: reel
  integer(kind=1) :: int1(4)
  equivalence (int1,reel)
  !
  do i=1,ni
    int1(2) = vaxr(1,i)
    int1(1) = vaxr(2,i)
    int1(4) = vaxr(3,i)
    int1(3) = vaxr(4,i)
    eeei(i)  = 0.25*reel
  enddo
end subroutine var4ei
#endif
!
!-----------------------------------------------------------------------
! Duplicate of previous subroutines, but using an
! integer(kind=size_length) for the number of values to be converted
!-----------------------------------------------------------------------
subroutine iei4ei_sl(inpu,oupu,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 4-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni             ! Number of items
  integer(kind=1) :: inpu(4,ni)     ! Input array
  integer(kind=1) :: oupu(4,ni)     ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(kind=1) :: tmp(4), abyt(4)
  !
entry eii4ie_sl (inpu,oupu,ni)
entry vai4ei_sl (inpu,oupu,ni)
entry eii4va_sl (inpu,oupu,ni)
entry eir4ie_sl (inpu,oupu,ni)
entry ier4ei_sl (inpu,oupu,ni)
  !
  do i=1,ni
    tmp = inpu(1:4,i)
    abyt(1) = tmp(4)
    abyt(2) = tmp(3)
    abyt(3) = tmp(2)
    abyt(4) = tmp(1)
    oupu(1:4,i) = abyt
  enddo
end subroutine iei4ei_sl
!
subroutine vai4ie_sl (inpu,oupu,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ private
  ! Simple copy: vax and swapped IEEE I4 are identical. Provided for
  ! generality
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni        ! Number of items
  integer(kind=4) :: inpu(ni)  ! Input array
  integer(kind=4) :: oupu(ni)  ! Output array
  ! Local
  integer(kind=size_length) :: i
  !
entry iei4va_sl (inpu,oupu,ni)
  do i=1,ni
    oupu(i) = inpu(i)
  enddo
end subroutine vai4ie_sl
!
subroutine iei2ei_sl (inpu,oupu,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 2-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni          ! Number of items
  integer(kind=1) :: inpu(2,ni)  ! Output array
  integer(kind=1) :: oupu(2,ni)  ! Input array
  ! Local
  integer(kind=size_length) :: i
  integer(kind=1) :: tmp(2),abyt(2)
  !
entry eii2ie_sl (inpu,oupu,ni)
entry vai2ei_sl (inpu,oupu,ni)
entry eii2va_sl (inpu,oupu,ni)
  !
  do i=1,ni
    tmp = inpu(1:2,i)
    abyt(1) = tmp(2)
    abyt(2) = tmp(1)
    oupu(1:2,i) = abyt
  enddo
end subroutine iei2ei_sl
!
subroutine eir8ie_sl (in,out,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface
  ! Byte swapping function for 8-bytes words
  ! Aliased input and output arrays are supported
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni         ! Number of items
  integer(kind=1) :: in(8,ni)   ! Input array
  integer(kind=1) :: out(8,ni)  ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(kind=1) :: tmp(8), abyt(8)
  !
entry iei8ei_sl (in,out,ni)
entry eii8ie_sl (in,out,ni)
entry ier8ei_sl (in,out,ni)
  !
  do i=1,ni
    tmp = in(1:8,i)
    abyt(1) = tmp(8)
    abyt(2) = tmp(7)
    abyt(3) = tmp(6)
    abyt(4) = tmp(5)
    abyt(5) = tmp(4)
    abyt(6) = tmp(3)
    abyt(7) = tmp(2)
    abyt(8) = tmp(1)
    out(1:8,i) = abyt
  enddo
end subroutine eir8ie_sl
!
#if defined(VAX)
!
subroutine ier4va_sl (ieee,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 32-bit items
  integer(2) :: ieee(2,ni)           ! Input array
  real(4) :: real(ni)                ! Output array
  ! Local
  integer(4) :: entier,expo
  integer(4), parameter :: ieee_expm=Z'00007F80'
  integer(2) :: int2(2)
  integer(kind=1) :: octet(4)
  integer(kind=size_length) :: i
  real(4) :: reel
  equivalence (entier,int2)
  equivalence (entier,octet)
  equivalence (entier,reel)
  ! Code
  do i=1,ni
    int2 (2) = ieee(1,i)
    int2 (1) = ieee(2,i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(i) = r4bval
    elseif (expo.eq.0) then
      real(i) = 0.0
    elseif (octet(2).ge.127) then
      real(i) = r4bval
    else
      real(i) = 4.0*reel
    endif
  enddo
end subroutine ier4va_sl
!
subroutine eir4va_sl (eeei,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                  ! Number of 32-bit items
  integer(kind=1) :: eeei (4,ni)    ! Input array
  real(4) :: real(ni)               ! Output array
  ! Local
  integer(4) :: entier,expo
  integer(kind=1) :: octet(4)
  integer(kind=size_length) :: i
  real(4) :: reel
  equivalence (entier,octet)
  equivalence (entier,reel)
  integer(4), parameter :: ieee_expm=Z'00007F80'
  !
  do i=1,ni
    octet(2) = eeei(1,i)
    octet(1) = eeei(2,i)
    octet(4) = eeei(3,i)
    octet(3) = eeei(4,i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(i) = r4bval
    elseif (expo.eq.0) then
      real(i) = 0.0
    elseif (octet(2).ge.127) then
      real(i) = r4bval
    else
      real(i) = 4.0*reel
    endif
  enddo
end subroutine eir4va_sl
!
subroutine var4ie_sl (real,ieee,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  real(4) :: real(ni)               ! Input array
  integer(2) :: ieee(2,ni)          ! Output array
  ! Local
  integer(kind=size_length) :: i
  real(4) :: reel
  integer(2) :: int2(2)
  equivalence (int2,reel)
  !
  do i=1,ni
    reel = 0.25*real(i)
    ieee(1,i) = int2(2)
    ieee(2,i) = int2(1)
  enddo
end subroutine var4ie_sl
!
subroutine var4ei_sl (real,eeei,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  real(4) :: real(ni)               ! Input array
  integer(kind=1) :: eeei(4,ni)     ! Output array
  ! Local
  integer(kind=size_length) :: i
  real(4) :: reel
  integer(kind=1) :: octet(4)
  equivalence (octet,reel)
  !
  do i=1,ni
    reel = 0.25*real(i)
    eeei(1,i) = octet(2)
    eeei(2,i) = octet(1)
    eeei(3,i) = octet(4)
    eeei(4,i) = octet(3)
  enddo
end subroutine var4ei_sl
!
subroutine eir8va_sl (eeei,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 64-bit items
  integer(kind=1) :: eeei(8,ni)      ! Input array
  real(8) :: real(ni)                ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(kind=1) :: octet(8)
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (octet,tmp4)
  equivalence (tmp2,tmp4)
  integer(2) :: ou(4),mant
  equivalence (ou,reel)
  !
  do i=1,ni
    octet(8) = eeei(1,i)
    octet(7) = eeei(2,i)
    octet(6) = eeei(3,i)
    octet(5) = eeei(4,i)
    octet(4) = eeei(5,i)
    octet(3) = eeei(6,i)
    octet(2) = eeei(7,i)
    octet(1) = eeei(8,i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm)   ! Exponent
      sign = iand(tmp2(4),ieee_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)    ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(i) = 0.0d0
    elseif (expo.gt.125) then  ! Check exponent overflow
      real(i) = r8bval
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
      ou(1) = tmp2(4)          ! Reverse 16 bit words
      ou(2) = tmp2(3)
      ou(3) = tmp2(2)
      ou(4) = tmp2(1)
      real(i) = 4.d0*reel
    endif
  enddo
end subroutine eir8va_sl
!
subroutine ier8va_sl (ieee,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 64-bit items
  integer(4) :: ieee(2,ni)           ! Input array
  real(8) :: real(ni)                ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  integer(2) :: ou(4),mant
  equivalence (ou,reel)
  !
  do i=1,ni
    tmp4(1) = ieee(1,i)
    tmp4(2) = ieee(2,i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm)   ! Exponent
      sign = iand(tmp2(4),ieee_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)    ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(i) = 0.0d0
    elseif (expo.gt.125) then  ! Check exponent overflow
      real(i) = r8bval
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
      ou(1) = tmp2(4)          ! Reverse words
      ou(2) = tmp2(3)
      ou(3) = tmp2(2)
      ou(4) = tmp2(1)
      real(i) = 4.d0*reel
    endif
  enddo
end subroutine ier8va_sl
!
subroutine var8ie_sl (real,ieee,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  real(8) :: real(ni)                ! Input array
  integer(4) :: ieee(2,ni)           ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'x
  integer(2), parameter :: vaxd_mant=Z'007F'x
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  integer(2) :: in(4),mant
  equivalence (in,reel)
  !
  do i=1,ni
    ! Divide by 4.0
    if (real(i).eq.0.0d0) then
      ieee(1,i) = 0
      ieee(2,i) = 0
      cycle                    ! I
    endif
    reel = 0.25d0*real(i)
    tmp2(1) = in(4)            ! Swap 16-bit words
    tmp2(2) = in(3)
    tmp2(3) = in(2)
    tmp2(4) = in(1)
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm)   ! Exponent
      sign = iand(tmp2(4),vaxd_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)    ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    ieee(1,i) = tmp4(1)        ! Move 32-bit words
    ieee(2,i) = tmp4(2)
  enddo
end subroutine var8ie_sl
!
subroutine var8ei_sl (real,eeei,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for VAX machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  real(8) :: real(ni)                ! Input array
  integer(kind=1) :: eeei(8,ni)     ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(kind=4) :: j
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'
  integer(2), parameter :: vaxd_mant=Z'007F'
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(kind=1) :: octet(8)
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (octet,tmp4)
  equivalence (tmp2,tmp4)
  integer(2) :: in(4),mant
  equivalence (in,reel)
  !
  do i=1,ni
    ! Divide by 4.0
    if (real(i).eq.0.0d0) then
      do j=1,8
        eeei(j,i) = 0
      enddo
      cycle                    ! I
    endif
    reel = 0.25d0*real(i)
    tmp2(1) = in(4)            ! Swap 16-bit words
    tmp2(2) = in(3)
    tmp2(3) = in(2)
    tmp2(4) = in(1)
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm)   ! Exponent
      sign = iand(tmp2(4),vaxd_sign)   ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)    ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))  ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))  ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    eeei(1,i) = octet(8)       ! Swap Bytes
    eeei(2,i) = octet(7)
    eeei(3,i) = octet(6)
    eeei(4,i) = octet(5)
    eeei(5,i) = octet(4)
    eeei(6,i) = octet(3)
    eeei(7,i) = octet(2)
    eeei(8,i) = octet(1)
  enddo
end subroutine var8ei_sl
!
#endif
!
#if defined(IEEE)
subroutine ier4va_sl (ieee32,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines, and most likely correct...
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 32-bit items
  real(4) :: ieee32(ni)              ! Input array
  integer(2) :: real(2,ni)           ! Output array
  ! Local
  integer(4), parameter :: ieee_expm=Z'7F800000'
  integer(4) :: entier,expo
  integer(2) :: int2(2)
  integer(kind=1) :: octet(4)
  integer(kind=size_length) :: i
  real(4) :: reel
  equivalence (entier,int2)
  equivalence (entier,octet)
  equivalence (entier,reel)
  !
  do i=1,ni
    reel = ieee32(i)
    expo = iand(entier,ieee_expm)
    if (expo.eq.ieee_expm) then
      real(1,i) = r4bval(1)
      real(2,i) = r4bval(2)
    elseif (expo.eq.0) then
      real(1,i) = 0
      real(2,i) = 0
    elseif (int2(1).eq.32256) then
      real(1,i) = r4bval(1)
      real(2,i) = r4bval(2)
    else
      reel = 4.0*reel
      real(1,i) = int2 (2)
      real(2,i) = int2 (1)
    endif
  enddo
end subroutine ier4va_sl
!
subroutine var4ie_sl (real,ieee32,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  integer(2) :: real(2,ni)           ! Input array
  real(4) :: ieee32(ni)              ! Output array
  ! Local
  integer(kind=size_length) :: i
  real(4) :: reel
  integer(2) :: int2(2)
  equivalence (int2,reel)
  !
  do i=1,ni
    int2(2) = real(1,i)
    int2(1) = real(2,i)
    ieee32(i) = 0.25*reel
  enddo
end subroutine var4ie_sl
!
subroutine ier8va_sl (ieee64,real,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 64-bit items
  real(8) :: ieee64(ni)              ! Input array
  integer(2) :: real(4,ni)           ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: ieee_sign=Z'8000'
  integer(2), parameter :: ieee_expm=Z'7FF0'
  integer(2), parameter :: ieee_mant=Z'000F'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),bit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    reel = 4.0d0*ieee64(i)
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),ieee_expm) ! Exponent
      sign = iand(tmp2(4),ieee_sign) ! Sign
      tmp2(4) = iand(tmp2(4),ieee_mant)  ! Mantissa, first part
      expo = expo/16           ! Shift by 4 bits
      expo = expo-1024         ! Subtract G_Float exponent bias
    else
      expo = -1024
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),3,32)   ! Shift right 3 position, circul
      bit3 = iand(tmp4(1),7)   ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-bit3   ! Mask lowest 3 bits in mantissa
      tmp4(2) = 8*tmp4(2)+bit3 ! Push bits as appropriate
    elseif (tmp4(2).ne.0) then
      tmp4(2) = 8*tmp4(2)
    endif
    !
    ! Check for special cases
    if (expo.lt.-128) then
      real(1,i) = 0
      real(2,i) = 0
      real(3,i) = 0
      real(4,i) = 0
    elseif (expo.ge.127) then
      real(1,i) = r8bval(1)
      real(2,i) = r8bval(2)
      real(3,i) = r8bval(3)
      real(4,i) = r8bval(4)
    else
      expo = expo+128          ! Add D_float exponent bias
      expo = expo*128          ! Shift by 7 bits
      tmp2(4) = ior(expo,tmp2(4))    ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))    ! Set sign
      real(1,i) = tmp2(4)      ! Reverse words
      real(2,i) = tmp2(3)
      real(3,i) = tmp2(2)
      real(4,i) = tmp2(1)
    endif
  enddo
end subroutine ier8va_sl
!
subroutine var8ie_sl (real,ieee64,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for IEEE machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  integer(2) :: real(4,ni)           ! Input array
  real(8) :: ieee64(ni)              ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: vaxd_sign=Z'8000'
  integer(2), parameter :: vaxd_expm=Z'7F80'
  integer(2), parameter :: vaxd_mant=Z'007F'
  integer(4), parameter :: vaxg_bit3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer(4) :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    tmp2(4) = real(1,i)        ! Swap 16-bit words
    tmp2(3) = real(2,i)
    tmp2(2) = real(3,i)
    tmp2(1) = real(4,i)
    if (tmp4(1).eq.0 .and. tmp4(2).eq.0) then
      ieee64(i) = 0.0d0
      cycle                    ! I
    endif
    !
    ! Now bytes and bits are in the right order ...
    if (tmp4(2).ne.0) then
      expo = iand(tmp2(4),vaxd_expm) ! Exponent
      sign = iand(tmp2(4),vaxd_sign) ! Sign
      tmp2(4) = iand(tmp2(4),vaxd_mant)  ! Mantissa, first part
      tmp4(2) = ishftc(tmp4(2),29,32)  ! Shift left 3 position, circula
      hbit3 = iand(tmp4(2),vaxg_bit3)  ! Keep highest 3 bits
      tmp4(2) = tmp4(2)-hbit3  ! Mask highest 3 bits
      expo = expo/128          ! Shift by 7 bits
      expo = expo-128          ! Subtract D_Float exponent bias
      expo = expo+1024         ! Add G_float exponent bias
      expo = expo*16           ! Shift by 3 bits
      tmp2(4) = ior(expo,tmp2(4))    ! Set exponent
      tmp2(4) = ior(sign,tmp2(4))    ! Set sign
    else                       ! Avoid special case for 0
      tmp2(4) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(1).ne.0) then
      tmp4(1) = ishftc(tmp4(1),29,32)  ! Shift left 3 position, circula
      lbit3 = iand(tmp4(1),vaxg_bit3)  ! Extract highest 3 bits
      tmp4(1) = tmp4(1)-lbit3  ! Mask highest 3 bits in mantissa
    endif
    tmp4(1) = ior(tmp4(1),hbit3)   ! Transfer 3 bits from 1st mantissa
    ieee64(i) = 0.25d0*reel
  enddo
end subroutine var8ie_sl
!
subroutine eir4va_sl (eeei,real,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                 ! Number of 32-bit items
  integer(4) :: eeei(ni)           ! Input array
  integer(4) :: real(ni)           ! Output array
  call eir4ie_sl(eeei,eeei,ni)
  call ier4va_sl(eeei,real,ni)
end subroutine eir4va_sl
!
subroutine var4ei_sl (real,eeei,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 32-bit items
  integer(4) :: real(ni)             ! Input array
  integer(4) :: eeei(ni)             ! Output array
  call var4ie_sl(real,eeei,ni)
  call ier4ei_sl(eeei,eeei,ni)
end subroutine var4ei_sl
!
subroutine eir8va_sl (eeei,real,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                 ! Number of 64-bit items
  real(8) :: eeei(ni)           ! Input array
  real(8) :: real(ni)           ! Output array
  call eir8ie_sl(eeei,eeei,ni)
  call ier8va_sl(eeei,real,ni)
end subroutine eir8va_sl
!
subroutine var8ei_sl (real,eeei,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                 ! Number of 64-bit items
  real(8) :: real(ni)           ! Input array
  real(8) :: eeei(ni)           ! Output array
  call var8ie_sl(real,eeei,ni)
  call ier8ei_sl(eeei,eeei,ni)
end subroutine var8ei_sl
#endif
!
#if defined(EEEI)
subroutine eir4va_sl (eeei,vaxr,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for EEEI machines.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                   ! Number of 32-bit items
  integer(4) :: eeei(ni)             ! Input array
  integer(4) :: vaxr(ni)             ! Output array
  ! Local
  integer(4), parameter :: eeei_expm=Z'7F800000'
  integer(4) :: reelin,reelou,expo
  integer(2) :: int2(2)
  integer(kind=1) :: tmpin(4),tmpou(4)
  integer(kind=size_length) :: i
  real(4) :: reel
  equivalence (reelin,reel)
  equivalence (reelin,tmpin)
  equivalence (reelou,tmpou)
  equivalence (int2,expo)
  !
  do i=1,ni
    ! Convert to swapped VAX reals
    reelin = eeei(i)
    ! Check special cases.
    expo = iand(reelin,eeei_expm)
    if (expo.eq.eeei_expm) then
      vaxr(i) = r4bval
    elseif (expo.eq.0) then
      vaxr(i) = 0
    elseif (int2(1).eq.32256) then
      vaxr(i) = r4bval
    else
      ! Swap to VAX reals (1234 --> 2143).
      ! Equivalent to
      !      unless (tmpin(1)==(126,127,-2,-1,0))
      !      tmpou(2) = tmpin(1)+1     ! Exponent field
      !      tmpou(1) = tmpin(2)
      !      tmpou(3) = tmpin(4)
      !      tmpou(4) = tmpin(3)
      !
      reel = 4.0*reel
      tmpou(2) = tmpin(1)
      tmpou(1) = tmpin(2)
      tmpou(4) = tmpin(3)
      tmpou(3) = tmpin(4)
      vaxr(i) = reelou
    endif
  enddo
end subroutine eir4va_sl
!
subroutine var8ei_sl (vaxr,eeei,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !     Convert NI VAX reals to NI EEEI reals
  !     This version is for EEEI machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 64-bit items
  integer(kind=1) :: vaxr(8,ni)     ! Input array
  real(8) :: eeei(ni)               ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2), parameter :: vxdsig=Z'8000'
  integer(2), parameter :: vxdexp=Z'7F80'
  integer(2), parameter :: vxdman=Z'007F'
  integer, parameter :: vxgbt3=Z'E0000000'
  integer(2) :: tmp2(4),expo,sign
  integer :: tmp4(2),lbit3,hbit3
  real(8) :: reel
  integer(kind=1) :: tmp1(8)
  equivalence (tmp1,tmp2)
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  !
  do i=1,ni
    tmp1 (2) = vaxr (1,i)
    tmp1 (1) = vaxr (2,i)
    tmp1 (4) = vaxr (3,i)
    tmp1 (3) = vaxr (4,i)
    tmp1 (6) = vaxr (5,i)
    tmp1 (5) = vaxr (6,i)
    tmp1 (8) = vaxr (7,i)
    tmp1 (7) = vaxr (8,i)
    if (tmp4(1).eq.0 .and. tmp4(2).eq.0) then
      eeei(i) = 0.0d0
      cycle                    ! I
    endif
    !
    if (tmp4(1).ne.0) then
      expo = iand( tmp2(1),vxdexp )
      sign = iand( tmp2(1),vxdsig )
      tmp2(1) = iand( tmp2(1),vxdman )
      tmp4(1) = ishftc(tmp4(1),29,32)
      hbit3 = iand ( tmp4(1),vxgbt3 )
      tmp4(1) = tmp4(1)-hbit3
      expo = expo/128
      expo = expo-128
      expo = expo+1024
      expo = expo*16
      tmp2(1) = ior( expo,tmp2(1) )
      tmp2(1) = ior( sign,tmp2(1) )
    else
      tmp2(1) = 16*(1024-128)
      hbit3 = 0
    endif
    if (tmp4(2).ne.0) then
      tmp4(2) = ishftc(tmp4(2),29,32)
      lbit3 = iand ( tmp4(2),vxgbt3 )
      tmp4(2) = tmp4(2)-lbit3
    endif
    tmp4(2) = ior ( tmp4(2),hbit3 )
    eeei(i) = 0.25d0*reel
  enddo
end subroutine var8ei_sl
!
subroutine eir8va_sl (eeei,vaxr,ni)
  use gildas_def
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !     Convert NI EEEI reals to NI VAX reals
  !     This version is for EEEI machines
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 64-bit items
  real(8) :: eeei(ni)               ! Input array
  integer :: vaxr(2,ni)             ! Output array
  ! Local
  integer(kind=size_length) :: i
  integer(2) :: tmp2(4),expo,sign
  integer(2), parameter :: eeei_sign=Z'8000'
  integer(2), parameter :: eeei_mant=Z'000F'
  integer(2), parameter :: eeei_expm=Z'7FF0'
  integer :: tmp4(2),lbit3,reelou(2)
  real(8) :: reel
  integer(kind=1) :: tmp1(8),tmpou(8)
  equivalence (tmp1,tmp2)
  equivalence (tmp2,tmp4)
  equivalence (tmp2,reel)
  equivalence (reelou,tmpou)
  !
  ! Order is Sign, Exponent, Mantissa
  do i=1,ni
    reel = 4.0d0*eeei(i)
    if (tmp4(1).ne.0) then
      expo = iand(tmp2(1),eeei_expm)
      sign = iand(tmp2(1),eeei_sign)
      tmp2(1) = iand(tmp2(1),eeei_mant)
      expo = expo/16
      expo = expo-1024
    else
      expo = -1024
    endif
    if (tmp4(2).ne.0) then
      tmp4(2) = ishftc(tmp4(2),3,32)
      lbit3 = iand(tmp4(2),7)
      tmp4(2) = tmp4(2)-lbit3
      tmp4(1) = 8*tmp4(1)+lbit3
    elseif (tmp4(1).ne.0) then
      tmp4(1) = 8*tmp4(1)
    endif
    ! Special cases
    if (expo.lt.-128) then
      vaxr(1,i) = 0
      vaxr(2,i) = 0
    elseif (expo.ge.127) then
      vaxr(1,i) = r8bval(1)
      vaxr(2,i) = r8bval(2)
    else
      expo = expo+128
      expo = expo*128
      tmp2(1) = ior(expo,tmp2(1))
      tmp2(1) = ior(sign,tmp2(1))
      tmpou(2) = tmp1(1)
      tmpou(1) = tmp1(2)
      tmpou(4) = tmp1(3)
      tmpou(3) = tmp1(4)
      tmpou(6) = tmp1(5)
      tmpou(5) = tmp1(6)
      tmpou(8) = tmp1(7)
      tmpou(7) = tmp1(8)
      vaxr(1,i) = reelou(1)
      vaxr(2,i) = reelou(2)
    endif
  enddo
end subroutine eir8va_sl
!
subroutine var8ie_sl (in,ou,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer(kind=size_length) :: ni                     ! Number of 64-bit items
  call var8ei_sl (in,ou,ni)
  call eir8ie_sl (ou,ou,ni)
end subroutine var8ie_sl
!
subroutine ier8va_sl (in,ou,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer(kind=size_length) :: ni                     ! Number of 64-bit items
  call ier8ei_sl (in,ou,ni)
  call eir8va_sl (ou,ou,ni)
end subroutine ier8va_sl
!
subroutine var4ie_sl (in,ou,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  call var4ei_sl (in,ou,ni)
  call eir4ie_sl (ou,ou,ni)
end subroutine var4ie_sl
!
subroutine ier4va_sl (in,ou,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  !---------------------------------------------------------------------
  integer :: in(*)                  ! Input array
  integer :: ou(*)                  ! Output array
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  call ier4ei_sl (in,ou,ni)
  call eir4va_sl (ou,ou,ni)
end subroutine ier4va_sl
!
subroutine var4ei_sl (vaxr,eeei,ni)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (multiple definition because of preprocessing)
  ! This version is for EEEI machines.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: ni                     ! Number of 32-bit items
  integer(kind=1) :: vaxr(4,ni)     ! Input array
  real(4) :: eeei(ni)               ! Output array
  ! Local
  integer(kind=size_length) :: i
  real(4) :: reel
  integer(kind=1) :: int1(4)
  equivalence (int1,reel)
  !
  do i=1,ni
    int1(2) = vaxr(1,i)
    int1(1) = vaxr(2,i)
    int1(4) = vaxr(3,i)
    int1(3) = vaxr(4,i)
    eeei(i)  = 0.25*reel
  enddo
end subroutine var4ei_sl
#endif
!
#if defined(VAX)
subroutine gdf_setblnk4(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ public
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  real(4), intent(in) :: bval
  r4bval = bval
end subroutine gdf_setblnk4
!
subroutine gdf_setblnk8(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ public
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  real(8), intent(in) :: bval
  ! Code
  r8bval = bval
end subroutine gdf_setblnk8
#endif
!
#if defined(IEEE)
subroutine gdf_setblnk4(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch under IEEE)
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  integer(2), intent(in) :: bval(2)
  r4bval(1) = bval(1)
  r4bval(2) = bval(2)
end subroutine gdf_setblnk4
!
subroutine gdf_setblnk8(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch under IEEE)
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  integer(2), intent(in) :: bval(4)
  ! Code
  r8bval(1) = bval(1)
  r8bval(2) = bval(2)
  r8bval(3) = bval(3)
  r8bval(4) = bval(4)
end subroutine gdf_setblnk8
#endif
!
#if defined(EEEI)
subroutine gdf_setblnk4(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ public
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  real(4), intent(in) :: bval
  r4bval = bval
end subroutine gdf_setblnk4
!
subroutine gdf_setblnk8(bval)
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch under EEEI)
  !   Set the blanking for automatic conversions
  !---------------------------------------------------------------------
  integer(4), intent(in) :: bval(2)
  ! Code
  r8bval(1) = bval(1)
  r8bval(2) = bval(2)
end subroutine gdf_setblnk8
#endif
!
subroutine gdf_rsetblnk
  use gfits_blanking
  !---------------------------------------------------------------------
  ! @ no-interface
  !---------------------------------------------------------------------
  integer :: i
#if defined(VAX)
  r4bval = 1.23456e38
  r8bval = 1.23456d38
#endif
#if defined(IEEE)
  !
  do i=1,2
    r4bval(i) = 0
  enddo
  do i=1,4
    r8bval(i) = 0
  enddo
#endif
#if defined(EEEI)
  !
  r4bval = 0
  do i=1,2
    r8bval(i) = 0
  enddo
#endif
end subroutine gdf_rsetblnk
