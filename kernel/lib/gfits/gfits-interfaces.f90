module gfits_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GFITS interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gfits_interfaces_public
  use gfits_interfaces_private
  !
end module gfits_interfaces
!
module gfits_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GFITS dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
end module gfits_dependencies_interfaces
