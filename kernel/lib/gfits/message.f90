!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GFITS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gfits_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gfits_message_id = gpack_global_id  ! Default value for startup message
  !
end module gfits_message_private
!
subroutine gfits_message_set_id(id)
  use gfits_interfaces, except_this=>gfits_message_set_id
  use gfits_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gfits_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gfits_message_id
  call gfits_message(seve%d,'gfits_message_set_id',mess)
  !
end subroutine gfits_message_set_id
!
subroutine gfits_message(mkind,procname,message)
  use gfits_dependencies_interfaces
  use gfits_interfaces, except_this=>gfits_message
  use gfits_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gfits_message_id,mkind,procname,message)
  !
end subroutine gfits_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
