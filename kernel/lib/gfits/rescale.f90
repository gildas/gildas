subroutine int2_to_real(int2,ni,array,ndata,nfill,bscal,bzero)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert an integer(2) array (a la FITS, i.e., little-endian) into a
  ! real array using an offset and a scale factor.
  !---------------------------------------------------------------------
  integer(kind=2),           intent(in)    :: int2(*)       ! Input FITS buffer
  integer(kind=size_length), intent(in)    :: ni            ! Size of buffer
  integer(kind=size_length), intent(in)    :: ndata         ! Size of output array
  real(kind=4),              intent(out)   :: array(ndata)  ! Output array
  integer(kind=size_length), intent(inout) :: nfill         ! EOB pointer in output array
  real(kind=4),              intent(in)    :: bscal         ! Scaling
  real(kind=4),              intent(in)    :: bzero         ! Offset
  ! Local
  integer(kind=size_length) :: i
  !
#if defined(IEEE) || defined(VAX)
  call iei2ei_sl(int2,int2,ni)
#endif
  do i=1,ni
    nfill=nfill+1
    if (nfill.gt.ndata) return
    array(nfill) = bscal*int2(i) + bzero
  enddo
end subroutine int2_to_real
!
subroutine int4_to_real(int4,ni,array,ndata,nfill,bscal,bzero)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert an integer(4) array (a la FITS, i.e., little-endian) into a
  ! real array using an offset and a scale factor.
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)    :: int4(*)       ! Input FITS buffer
  integer(kind=size_length), intent(in)    :: ni            ! Size of buffer
  integer(kind=size_length), intent(in)    :: ndata         ! Size of output array
  real(kind=4),              intent(out)   :: array(ndata)  ! Output array
  integer(kind=size_length), intent(inout) :: nfill         ! EOB pointer in output array
  real(kind=4),              intent(in)    :: bscal         ! Scaling
  real(kind=4),              intent(in)    :: bzero         ! Offset
  ! Local
  integer(kind=size_length) :: i
  !
#if defined(IEEE) || defined(VAX)
  call iei4ei_sl(int4,int4,ni)
#endif
  do i=1,ni
    nfill=nfill+1
    if(nfill.gt.ndata) return
    array(nfill) = bscal*int4(i) + bzero
  enddo
end subroutine int4_to_real
!
subroutine byte_to_real(int1,ni,array,ndata,nfill,bscal,bzero)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !  Convert a BYTE array into a real array using an offset and a scale
  ! factor.
  !---------------------------------------------------------------------
  integer(kind=1),           intent(in)    :: int1(*)       ! Input FITS buffer
  integer(kind=size_length), intent(in)    :: ni            ! Size of buffer
  integer(kind=size_length), intent(in)    :: ndata         ! Size of output array
  real(kind=4),              intent(out)   :: array(ndata)  ! Output array
  integer(kind=size_length), intent(inout) :: nfill         ! EOB pointer in output array
  real(kind=4),              intent(in)    :: bscal         ! Scaling
  real(kind=4),              intent(in)    :: bzero         ! Offset
  ! Local
  integer(kind=4) :: entier
  integer(kind=size_length) :: i
  !
  do i=1,ni
    nfill=nfill+1
    if(nfill.gt.ndata) return
    entier = int1(i)
    if (entier.lt.0) entier=entier+256
    array(nfill) = bscal*entier + bzero
  enddo
end subroutine byte_to_real
!
subroutine real_to_int2(int2,ni,array,ndata,nfill,bscal,bzero,bval,eval)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert a real array into an integer(2) array (a la FITS, i.e.
  ! little-endian) using an offset and a scale factor.
  !---------------------------------------------------------------------
  integer(kind=2),      intent(out)   :: int2(*)       ! Output FITS buffer
  integer(kind=4),      intent(in)    :: ni            ! Size of buffer
  integer(size_length), intent(in)    :: ndata         ! Size of input array
  real(kind=4),         intent(in)    :: array(ndata)  ! Input array
  integer(size_length), intent(inout) :: nfill         ! EOB pointer in input array
  real(kind=4),         intent(in)    :: bscal         ! Scaling
  real(kind=4),         intent(in)    :: bzero         ! Offset
  real(kind=4),         intent(in)    :: bval          ! Blanking value
  real(kind=4),         intent(in)    :: eval          ! Blanking tolerance
  ! Local
  integer(kind=4) :: i
  !
  if (eval.ge.0.0) then
    do i=1,ni
      nfill=nfill+1
      if (nfill.gt.ndata) then
        int2(i) = 0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      elseif (abs(array(nfill)-bval).gt.eval) then
        int2(i) = nint ((array(nfill)-bzero)/bscal)
      else
        int2(i) = 32767
      endif
    enddo
  else
    do i=1,ni
      nfill=nfill+1
      if (nfill.gt.ndata) then
        int2(i) = 0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      else
        int2(i) = nint ((array(nfill)-bzero)/bscal)
      endif
    enddo
  endif
#if defined(IEEE) || defined(VAX)
  call iei2ei(int2,int2,ni)
#endif
end subroutine real_to_int2
!
subroutine real_to_int4(int4,ni,array,ndata,nfill,bscal,bzero,bval,eval)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert a real array into an integer(4) array (a la FITS, i.e.,
  ! little-endian) using an offset and a scale factor.
  !---------------------------------------------------------------------
  integer(kind=4),      intent(out)   :: int4(*)       ! Output FITS buffer
  integer(kind=4),      intent(in)    :: ni            ! Size of buffer
  integer(size_length), intent(in)    :: ndata         ! Size of input array
  real(kind=4),         intent(in)    :: array(ndata)  ! Input array
  integer(size_length), intent(inout) :: nfill         ! EOB pointer in input array
  real(kind=4),         intent(in)    :: bscal         ! Scaling
  real(kind=4),         intent(in)    :: bzero         ! Offset
  real(kind=4),         intent(in)    :: bval          ! Blanking value
  real(kind=4),         intent(in)    :: eval          ! Blanking tolerance
  ! Local
  integer(kind=4) :: i
  !
  if (eval.ge.0.0) then
    do i=1,ni
      nfill=nfill+1
      if (nfill.gt.ndata) then
        int4(i) = 0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      elseif (abs(array(nfill)-bval).gt.eval) then
        int4(i) = nint ((array(nfill)-bzero)/bscal)
      else
        int4(i) = 2147483647
      endif
    enddo
  else
    do i=1,ni
      nfill=nfill+1
      if (nfill.gt.ndata) then
        int4(i) = 0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      else
        int4(i) = nint ((array(nfill)-bzero)/bscal)
      endif
    enddo
  endif
#if defined(IEEE) || defined(VAX)
  call iei4ei(int4,int4,ni)
#endif
end subroutine real_to_int4
!
subroutine real_to_real4(real4,nr,array,ndata,nfill,bval,eval)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert a real array into an real(4) array (a la FITS, i.e.,
  ! little-endian).
  !---------------------------------------------------------------------
  real(kind=4),         intent(out)   :: real4(*)      ! Output FITS buffer
  integer(kind=4),      intent(in)    :: nr            ! Size of buffer
  integer(size_length), intent(in)    :: ndata         ! Size of input array
  real(kind=4),         intent(in)    :: array(ndata)  ! Input array
  integer(size_length), intent(inout) :: nfill         ! EOB pointer in input array
  real(kind=4),         intent(in)    :: bval          ! Blanking value
  real(kind=4),         intent(in)    :: eval          ! Blanking tolerance
  ! Global
  include 'gbl_nan.inc'
  ! Local
  integer(kind=4) :: i
  !
  if (eval.ge.0.0) then
    do i=1,nr
      nfill=nfill+1
      if (nfill.gt.ndata) then
        real4(i) = 0.0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      elseif (abs(array(nfill)-bval).gt.eval) then
        real4(i) = array(nfill)
      else
        real4(i) = s_nan
      endif
    enddo
  else
    do i=1,nr
      nfill=nfill+1
      if (nfill.gt.ndata) then
        real4(i) = 0.0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      else
        real4(i) = array(nfill)
      endif
    enddo
  endif
#if defined(IEEE) || defined(VAX)
  call ier4ei(real4,real4,nr)
#endif
end subroutine real_to_real4
!
subroutine real_to_real8(real8,nr,array,ndata,nfill,bval,eval)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface
  !  Convert a real array into an real(8) array (a la FITS, i.e.,
  ! little-endian).
  !---------------------------------------------------------------------
  real(kind=8),         intent(out)   :: real8(*)      ! Output FITS buffer
  integer(kind=4),      intent(in)    :: nr            ! Size of buffer
  integer(size_length), intent(in)    :: ndata         ! Size of input array
  real(kind=4),         intent(in)    :: array(ndata)  ! Input array
  integer(size_length), intent(inout) :: nfill         ! EOB pointer in input array
  real(kind=4),         intent(in)    :: bval          ! Blanking value
  real(kind=4),         intent(in)    :: eval          ! Blanking tolerance
  ! Global
  include 'gbl_nan.inc'
  ! Local
  integer(kind=4) :: i
  !
  if (eval.ge.0.0) then
    do i=1,nr
      nfill=nfill+1
      if (nfill.gt.ndata) then
        real8(i) = 0.d0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      elseif (abs(array(nfill)-bval).gt.eval) then
        real8(i) = array(nfill)
      else
        real8(i) = d_nan
      endif
    enddo
  else
    do i=1,nr
      nfill=nfill+1
      if (nfill.gt.ndata) then
        real8(i) = 0.d0  ! Zero padding (Wells et al, A&A Sup. Ser 44, P.364, line 47)
      else
        real8(i) = array(nfill)
      endif
    enddo
  endif
#if defined(IEEE) || defined(VAX)
  call ier8ei(real8,real8,nr)
#endif
end subroutine real_to_real8
