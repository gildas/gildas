module gfits_interfaces_private
  interface
    subroutine vai4ie (inpu,oupu,ni)
      !---------------------------------------------------------------------
      ! @ private
      ! Simple copy: vax and swapped IEEE I4 are identical. Provided for
      ! generality
      !---------------------------------------------------------------------
      integer(kind=4) :: ni        ! Number of items
      integer(kind=4) :: inpu(ni)  ! Input array
      integer(kind=4) :: oupu(ni)  ! Output array
    end subroutine vai4ie
  end interface
  !
  interface
    subroutine vai4ie_sl (inpu,oupu,ni)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Simple copy: vax and swapped IEEE I4 are identical. Provided for
      ! generality
      !---------------------------------------------------------------------
      integer(kind=size_length) :: ni        ! Number of items
      integer(kind=4) :: inpu(ni)  ! Input array
      integer(kind=4) :: oupu(ni)  ! Output array
    end subroutine vai4ie_sl
  end interface
  !
  interface
    subroutine gfits_skip_hdu(fd,ihdu,list,error)
      use gildas_def
      use gbl_message
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Skip entirely the current HDU. Optionaly list its contents.
      !---------------------------------------------------------------------
      type(gfits_hdesc_t), intent(inout) :: fd     !
      integer(kind=4),     intent(in)    :: ihdu   ! HDU we are skipping (1=Primary)
      logical,             intent(in)    :: list   !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine gfits_skip_hdu
  end interface
  !
  interface
    subroutine gfits_setsort(fh,error)
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the sorting list of the input fits header (fh%sort)
      !  Remark: fh%sort() must contain the unsorted elements before
      ! entering gi0_quicksort_index_with_user_gtge. It can be partially
      ! sorted, or not at all, this will work.
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(inout) :: fh
      logical,             intent(inout) :: error
    end subroutine gfits_setsort
  end interface
  !
  interface
    subroutine gfits_find_value(fh,key,found,value,error)
      use gfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get the value in the dictionary from the given key. Output is the
      ! raw character string (not evaluated).
      !---------------------------------------------------------------------
      type(gfits_hdict_t), intent(in)    :: fh
      character(len=*),    intent(in)    :: key
      logical,             intent(out)   :: found
      character(len=*),    intent(out)   :: value
      logical,             intent(inout) :: error
    end subroutine gfits_find_value
  end interface
  !
  interface
    subroutine gfits_putrec(buff,error)
      use gfits_buf
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Write one logical FITS record
      !---------------------------------------------------------------------
      integer(kind=1), intent(in)    :: buff(2880)  ! I/O buffer
      logical,         intent(inout) :: error       ! Error flag
    end subroutine gfits_putrec
  end interface
  !
  interface
    subroutine gfits_getrec(buff,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Read one logical FITS record (verbose mode)
      !---------------------------------------------------------------------
      integer(kind=1), intent(out)   :: buff(2880)  ! I/O buffer
      logical,         intent(inout) :: error       ! Error flag
    end subroutine gfits_getrec
  end interface
  !
  interface
    subroutine fgetrecquiet(buff,error,eof)
      !---------------------------------------------------------------------
      ! @ private
      ! Read one logical FITS record (quiet mode)
      !---------------------------------------------------------------------
      integer(kind=1), intent(out)   :: buff(2880)  ! I/O buffer
      logical,         intent(inout) :: error       ! Error flag
      logical,         intent(out)   :: eof         ! End of file flag
    end subroutine fgetrecquiet
  end interface
  !
  interface
    subroutine fgetrec_sub(buff,error,eof,quiet)
      use gfits_buf
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! FITS  Internal routine
      !       Read one logical FITS record
      !---------------------------------------------------------------------
      integer(kind=1), intent(out)   :: buff(2880)  ! I/O buffer
      logical,         intent(inout) :: error       ! Error flag
      logical,         intent(out)   :: eof         ! End of file flag
      logical,         intent(in)    :: quiet       ! Verbosity flag
    end subroutine fgetrec_sub
  end interface
  !
  interface
    subroutine fseefilepos()
      use gildas_def
      use gfits_buf
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Used while debugging.
      !---------------------------------------------------------------------
    end subroutine fseefilepos
  end interface
  !
  interface
    subroutine gfits_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gfits_message
  end interface
  !
  interface
    subroutine ieee64_to_dble (ieee64,ni,map,ndata,nfill,bscal,bzero,bval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Convert 8-bytes floating point numbers to native format (double
      ! precision).
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)    :: ieee64(*)   ! Input buffer in IEEE format
      integer(kind=size_length), intent(in)    :: ni          ! Buffer size
      integer(kind=size_length), intent(in)    :: ndata       ! Output array size
      real(kind=8),              intent(out)   :: map(ndata)  ! Output array
      integer(kind=size_length), intent(inout) :: nfill       ! Counter in output array
      real(kind=8),              intent(in)    :: bscal       ! Scaling factor
      real(kind=8),              intent(in)    :: bzero       ! Offset
      real(kind=8),              intent(in)    :: bval        ! Blanking value
    end subroutine ieee64_to_dble
  end interface
  !
  interface
    subroutine dble_to_ieee64 (ieee64,ni,map,ndata,nfill,bval,eval)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Blanking values are NOT correctly translated to a NaN in present
      ! version. This must be fixed before floating FITS can be routinely
      ! written!!!
      !---------------------------------------------------------------------
      real(kind=8),              intent(out)   :: ieee64(*)   ! Output buffer in IEEE format
      integer(kind=size_length), intent(in)    :: ni          ! Buffer size
      integer(kind=size_length), intent(in)    :: ndata       ! Input array size
      real(kind=8),              intent(in)    :: map(ndata)  ! Input array
      integer(kind=size_length), intent(inout) :: nfill       ! Counter in output array
      real(kind=8),              intent(in)    :: bval        ! Blanking value
      real(kind=8),              intent(in)    :: eval        ! Blanking tolerance
    end subroutine dble_to_ieee64
  end interface
  !
end module gfits_interfaces_private
