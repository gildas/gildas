subroutine write_all(fd,map,nsize,nfill,bval,eval,error)
  use gildas_def
  use gfits_interfaces
  use gfits_buf
  use gfits_types
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  type(gfits_hdesc_t),       intent(in)    :: fd          !
  integer(kind=size_length), intent(in)    :: nsize       ! Data size
  real(kind=4),              intent(in)    :: map(nsize)  ! Data array
  integer(kind=size_length), intent(out)   :: nfill       ! Number of written items
  real(kind=4),              intent(in)    :: bval        ! Blanking value
  real(kind=4),              intent(in)    :: eval        ! Blanking tolerance
  logical,                   intent(inout) :: error       ! Error flag
  !
  nfill = 0
  do while (nfill.lt.nsize)
    if (fd%snbit.eq.16) then
      call real_to_int2(buffer,1440,map,nsize,nfill,fd%bscal,fd%bzero,bval,eval)
    elseif (fd%snbit.eq.32) then
      call real_to_int4(buffer,720,map,nsize,nfill,fd%bscal,fd%bzero,bval,eval)
    elseif (fd%snbit.eq.-32) then
      call real_to_real4(buffer,720,map,nsize,nfill,bval,eval)
    endif
    call gfits_putbuf(buffer,2880,error)
    if (error) return
  enddo
end subroutine write_all
