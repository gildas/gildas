subroutine fitreal(fd,ndat,array,scale,zero,error)
  use gbl_message
  use gfits_interfaces, except_this=>fitreal
  use gfits_buf
  use gfits_types
  !---------------------------------------------------------------------
  ! @ public
  ! Write NDAT data values in FITS data area,
  ! with scaling SCALE and offset ZERO
  !---------------------------------------------------------------------
  type(gfits_hdesc_t), intent(inout) :: fd           !
  integer(kind=4),     intent(in)    :: ndat         ! Number of data elements
  real(kind=4),        intent(in)    :: array(ndat)  ! Data array
  real(kind=4),        intent(in)    :: scale        ! Scaling factor
  real(kind=4),        intent(in)    :: zero         ! Zero offset
  logical,             intent(inout) :: error        ! Error flag
  ! Local
  character(len=*), parameter :: rname='FITREAL'
  integer(kind=4) :: i
  real(kind=4) :: vtest
  character(len=message_length) :: mess
  !
  ! I*2
  if (fd%snbit.eq.16) then
    do i = 1, ndat
      vtest = (array(i)-zero)/scale
      if (vtest.lt.-32768.) then
        write(mess,*) 'Value ',array(i),' below range'
        call gfits_message(seve%e,rname,mess)
        vtest = -32768.
      elseif (vtest.gt.32767.) then
        write(mess,*) 'Value ',array(i),' above range'
        call gfits_message(seve%e,rname,mess)
        vtest = 32767.
      endif
      fd%nb = fd%nb+1
      i2buf(fd%nb) = vtest
      if (fd%nb.ge.1440) then
#if defined(IEEE) || defined(VAX)
        call iei2ei (buffer,buffer,1440)
#endif
        call gfits_putbuf(buffer,2880,error)
        fd%nb = 0
        if (error) return
      endif
    enddo
  elseif (fd%snbit.eq.32) then
    do i = 1, ndat
      vtest = (array(i)-zero)/scale
      if (vtest.lt.-2147483648.) then
        write(mess,*) 'Value ',array(i),' below range'
        call gfits_message(seve%e,rname,mess)
        vtest = -2147483648.
      elseif (vtest.gt.2147483647.) then
        write(mess,*) 'Value ',array(i),' above range'
        call gfits_message(seve%e,rname,mess)
        vtest = 2147483647.
      endif
      fd%nb = fd%nb+1
      i4buf(fd%nb) = vtest
      if (fd%nb.ge.720) then
#if defined(IEEE) || defined(VAX)
        call iei4ei(buffer,buffer,720)
#endif
        call gfits_putbuf(buffer,2880,error)
        fd%nb = 0
        if (error) return
      endif
    enddo
  elseif  (fd%snbit.eq.-32) then
#if defined(VAX)
    call setblnk4(bval)
#endif
    do i = 1, ndat
      vtest = (array(i)-zero)/scale
      fd%nb= fd%nb+1
#if defined(VAX)
      call var4ei(vtest,i4buf(fd%nb),1)
#endif
#if defined(IEEE)
      call ier4ei(vtest,i4buf(fd%nb),1)
#endif
#if defined(EEEI)
      i4buf(fd%nb) = vtest
#endif
      if (fd%nb.ge.720) then
        call gfits_putbuf(buffer,2880,error)
        fd%nb = 0
        if (error) return
      endif
    enddo
  else
    error = .true.
  endif
end subroutine fitreal
!
subroutine fitreal_end(fd,error)
  use gfits_interfaces, except_this=>fitreal_end
  use gfits_buf
  use gfits_types
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(gfits_hdesc_t), intent(inout) :: fd     !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  integer(kind=4) :: i
  !
  if (fd%nb.eq.0) return
  if (fd%snbit.eq.16) then
    do i = fd%nb+1,1440
      i2buf (i) = 0
    enddo
#if defined(IEEE) || defined(VAX)
    call iei2ei(buffer,buffer,1440)
#endif
  elseif (fd%snbit.eq.32) then
    do i = fd%nb+1,720
      i4buf (i) = 0
    enddo
#if defined(IEEE) || defined(VAX)
    call iei4ei(buffer,buffer,720)
#endif
  elseif (fd%snbit.eq.-32) then
    do i = fd%nb+1,720
      i4buf (i) = 0
    enddo
  else
    error = .true.
    return
  endif
  call gfits_putbuf(buffer,2880,error)
  fd%nb = 0
end subroutine fitreal_end
