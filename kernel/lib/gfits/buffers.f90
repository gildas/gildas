module gfits_buf
  use gildas_def
  !--------------------------------------------------------------------
  ! GIO global variables for FITS I/O support
  !--------------------------------------------------------------------
  !
  ! Device types
  integer(kind=4), parameter :: disk=0   !
  integer(kind=4), parameter :: cdrom=1  !
  integer(kind=4), parameter :: tape=2   !
  !
  ! Variables for current file
  integer(kind=4) :: ifile               ! Current location on sequential device
  integer(kind=4) :: unit                ! Fortran LUN, C file pointer, or RMS channel
  integer(kind=4) :: dev_type            ! Type of physical device (tape, CDrom,...)
  integer(kind=4) :: ib                  ! Pointer within FITS I/O buffer
  integer(kind=record_length) :: irec    ! Next record for direct access (block mode +CD)
  logical :: read                        ! File is opened in read mode?
  logical :: stdio                       ! ??
  integer(kind=record_length) :: hdurec  ! Starting record of current HDU
  !
  ! I/O buffer
  integer(kind=1) :: buffer(2880)
  integer(kind=2) :: i2buf(1440)
  integer(kind=4) :: i4buf(720)
  equivalence (buffer,i2buf,i4buf)
  !
end module gfits_buf
