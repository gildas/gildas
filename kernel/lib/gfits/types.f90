module gfits_types
  use gildas_def
  !
  !---------------------------------------------------------------------
  !
  integer(kind=4), parameter :: key_length=15
  integer(kind=4), parameter :: char0d_length=80   ! FITS character scalar values
  integer(kind=4), parameter :: char1d_length=16   ! FITS character array values
  integer(kind=4), parameter :: comment_length=72
  !
  ! Scalar types
  type fits_unkn_0d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     character(len=char0d_length) :: val = ''
  end type fits_unkn_0d_t
  type fits_dble_0d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     real(kind=8) :: val = 0.d0
  end type fits_dble_0d_t
  type fits_inte_0d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=4) :: val = 0
  end type fits_inte_0d_t
  type fits_logi_0d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     logical :: val = .false.
  end type fits_logi_0d_t
  type fits_char_0d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     character(len=char0d_length) :: val = ''
  end type fits_char_0d_t
  !
  ! 1D array types
  type fits_inte_1d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=index_length) :: n = 0
     integer(kind=4), pointer :: val(:) => null()
  end type fits_inte_1d_t
  type fits_real_1d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=index_length) :: n = 0
     real(kind=4), pointer :: val(:) => null()
  end type fits_real_1d_t
  type fits_dble_1d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=index_length) :: n = 0
     real(kind=8), pointer :: val(:) => null()
  end type fits_dble_1d_t
  type fits_logi_1d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=index_length) :: n = 0
     logical, pointer :: val(:) => null()
  end type fits_logi_1d_t
  type fits_char_1d_t
     character(len=comment_length) :: comment = ''
     character(len=key_length) :: key = ''
     integer(kind=index_length) :: n = 0
     character(len=char1d_length), pointer :: val(:) => null()
  end type fits_char_1d_t
  !
  !---------------------------------------------------------------------
  !
  ! FITS header descriptor
  type :: gfits_hdesc_t
    real(kind=4)    :: bscal   ! Tape scaling factor
    real(kind=4)    :: bzero   ! Map offset
    integer(kind=4) :: nbit    ! Number of bits per value in current file
    integer(kind=4) :: snbit   ! Number of bits per value for FITS writing
    integer(kind=4) :: nb      ! Pointer within I/O buffer
    real(kind=4)    :: bval0   ! Default blanking value
  end type gfits_hdesc_t
  !
  ! Dictionary of cards in the header
  type :: gfits_hdict_t
    integer(kind=4) :: ncard
    type(fits_unkn_0d_t), allocatable :: card(:)
    integer(kind=4), allocatable :: sort(:)  ! Sorting array
  end type gfits_hdict_t
  !
contains
  !
  subroutine gfits_reallocate_dict(hdict,error)
    use gsys_interfaces_public
    !---------------------------------------------------------------------
    ! Double the size of the input dictionary if needed, keeping its
    ! current contents.
    !---------------------------------------------------------------------
    type(gfits_hdict_t), intent(inout) :: hdict
    logical,             intent(inout) :: error
    ! Local
    type(fits_unkn_0d_t), allocatable :: newcard(:)
    integer(kind=4),      allocatable :: newsort(:)
    integer(kind=4) :: icard,oldsize,newsize,ier
    integer(kind=4), parameter :: mcard=500  ! Default allocation size
    !
    if (allocated(hdict%card)) then
      oldsize = size(hdict%card)
    else
      oldsize = 0
    endif
    if (oldsize.gt.hdict%ncard)  return  ! There is room for one more card => nothing to be done
    newsize = max(mcard,oldsize*2)
    !
    allocate(newcard(newsize),newsort(newsize),stat=ier)
    if (failed_allocate('FITS','card and sort buffers',ier,error))  return
    !
    do icard=1,oldsize
      newcard(icard)%key     = hdict%card(icard)%key
      newcard(icard)%val     = hdict%card(icard)%val
      newcard(icard)%comment = hdict%card(icard)%comment
      newsort(icard)         = hdict%sort(icard)
    enddo
    !
    if (oldsize.gt.0)  deallocate(hdict%card,hdict%sort)
    call move_alloc(from=newcard,to=hdict%card)
    call move_alloc(from=newsort,to=hdict%sort)
    !
  end subroutine gfits_reallocate_dict
  !
  subroutine gfits_list_dict(hdict,error)
    use gbl_message
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(gfits_hdict_t), intent(in)    :: hdict
    logical,             intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='LIST>DICT'
    integer(kind=4) :: icard,mval
    character(len=message_length) :: mess
    !
    mval = 1
    do icard=1,hdict%ncard
      mval = max(mval,len_trim(hdict%card(icard)%val))
    enddo
    !
    do icard=1,hdict%ncard
      ! ZZZ Should we display them sorted?
      write(mess,'(5A)')  &
        hdict%card(icard)%key(1:8),  &
        '= ',  &
        hdict%card(icard)%val(1:mval),  &
        ' / ',  &
        trim(hdict%card(icard)%comment)
      call gfits_message(seve%r,rname,mess)
    enddo
    !
  end subroutine gfits_list_dict
  !
end module gfits_types
