module gwcs_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GWCS interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gwcs_interfaces_public
  use gwcs_interfaces_private
  !
end module gwcs_interfaces
!
module gwcs_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GWCS dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  !
end module gwcs_dependencies_interfaces
