!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GWCS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gwcs_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gwcs_message_id = gpack_global_id  ! Default value for startup message
  !
end module gwcs_message_private
!
subroutine gwcs_message_set_id(id)
  use gwcs_message_private
  use gbl_message
  use gwcs_interfaces, except_this=>gwcs_message_set_id
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gwcs_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gwcs_message_id
  call gwcs_message(seve%d,'gwcs_message_set_id',mess)
  !
end subroutine gwcs_message_set_id
!
subroutine gwcs_message(mkind,procname,message)
  use gwcs_dependencies_interfaces
  use gwcs_message_private
  use gbl_message
  use gwcs_interfaces, except_this=>gwcs_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gwcs_message_id,mkind,procname,message)
  !
end subroutine gwcs_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
