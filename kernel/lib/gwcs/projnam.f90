module gwcs_projnam
  use gbl_constant
  !---------------------------------------------------------------------
  ! DO NOT USE out of this file! The following array can not be made
  ! public because of problems of data sharing under various
  ! platforms.
  !---------------------------------------------------------------------
  !
  character(len=13), parameter :: projections(0:mproj) = (/  &
      'NONE         ',  &
      'GNOMONIC     ',  &
      'ORTHOGRAPHIC ',  &
      'AZIMUTHAL    ',  &
      'STEREOGRAPHIC',  &
      'LAMBERT      ',  &
      'AITOFF       ',  &
      'RADIO        ',  &
      'SFL          ',  &
      'MOLLWEIDE    ',  &
      'NCP          ',  &
      'CARTESIAN    '   /)
  !
end module gwcs_projnam
!
function projnam(iproj)
  use gwcs_projnam
  use gbl_message
  use gwcs_interfaces, except_this=>projnam
  !---------------------------------------------------------------------
  ! @ public
  ! Return the projection name as a character string
  ! ---
  !  The purpose of this function is to avoid declaring a static
  ! character string array containing the projection names: there are
  ! difficulties to share such kind of data between libraries under
  ! several systems (MSWIN, MINGW, ...)
  !---------------------------------------------------------------------
  character(len=13) :: projnam  ! Function value on return
  integer(kind=4), intent(in) :: iproj  ! The projection number
  ! Local
  character(len=*), parameter :: rname='PROJNAM'
  character(len=message_length) :: mess
  !
  if (iproj.lt.0 .or. iproj.gt.mproj) then
    write(mess,*) 'Unrecognized projection number ',iproj
    call gwcs_message(seve%e,rname,mess)
    projnam = 'UNKNOWN'
  else
    projnam = projections(iproj)
  endif
  !
end function projnam
!
subroutine projnam_list(list)
  use gbl_constant
  use gwcs_projnam
  use gwcs_interfaces, except_this=>projnam_list
  !---------------------------------------------------------------------
  ! @ public
  ! Return the list of projections in a string array
  !---------------------------------------------------------------------
  character(len=*), intent(out) :: list(0:mproj)  !
  !
  list(0:mproj) = projections(0:mproj)
  !
end subroutine projnam_list
