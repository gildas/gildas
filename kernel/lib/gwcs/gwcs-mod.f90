module gwcs_types
  !---------------------------------------------------------------------
  ! Projection definitions
  !---------------------------------------------------------------------
  !
  ! The following type is not specific to SFL but only implemented for
  ! SFL for now. See Calabretta & Greisen 2002, table 1.
  type :: projection_sfl_t
    real(kind=8) :: phi_p    ! Native longitude of the celestial pole
    real(kind=8) :: alpha_p  ! Celestial longitude of the native pole
    real(kind=8) :: delta_p  ! Celestial latitude of the native pole
    real(kind=8) :: cosdp    ! Cosinus of delta_p
    real(kind=8) :: sindp    ! Sinus of delta_p
  end type projection_sfl_t
  !
  type :: projection_t
    ! Projection definition
    real(kind=8)    :: a0     ! X Coordinate of projection point
    real(kind=8)    :: d0     ! Y Coordinate of projection point
    real(kind=8)    :: angle  ! Angle of projection line
    integer(kind=4) :: type   ! Projection type
    ! Derived parameters
    real(kind=8)    :: sina0  ! Sinus and
    real(kind=8)    :: cosa0  ! Cosinus of A0
    real(kind=8)    :: sind0  ! Sinus and
    real(kind=8)    :: cosd0  ! Cosinus of D0
    real(kind=8)    :: npole  ! Position of North pole
    real(kind=8)    :: spole  ! Position of South pole
    ! SFL support
    type(projection_sfl_t) :: sfl
  end type projection_t
end module gwcs_types
