subroutine fits_gildas(line,error)
  use gildas_def
  use image_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>fits_gildas
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !   FITS File.fits FROM File.gdf /STYLE [/HDU Number] /CHECK /OVERWRITE
  !   FITS File.fits TO File.gdf   /STYLE /BITS /BLC /TRC /OVERWRITE
  !
  ! Internal FITS_GILDAS or GILDAS_FITS conversion.
  ! Allow unrecognized FITS keyword handling, either by Symbol translation
  ! or by user input.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=filename_length) :: gildas_file,fits_file
  character(len=12) :: key,style
  character(len=80) :: chain
  integer(kind=index_length) :: blc(gdf_maxdims),trc(gdf_maxdims)
  integer(kind=4) :: ng,nh,nc,nbit,ncom,ikey,hdunum
  logical :: check,overwrite
  character(len=16) :: argum,comm
  integer(kind=4), parameter :: nbit_def=-32  ! Default BITPIX
  integer(kind=4), parameter :: mway=2,mstyle=5
  character(len=4), parameter :: ways(mway) = (/ 'TO  ','FROM' /)
  character(len=15), parameter :: styles(mstyle) =  (/ 'STANDARD       ',  &
    'UVFITS         ','AIPSFITS       ', 'SORTED_AIPSFITS','CASA_UVFITS    ' /)
  ! Options to command:
  integer(kind=4), parameter :: optbits=1
  integer(kind=4), parameter :: optstyl=2
  integer(kind=4), parameter :: opthdu =3
  integer(kind=4), parameter :: optchec=4
  integer(kind=4), parameter :: optblc =5
  integer(kind=4), parameter :: opttrc =6
  integer(kind=4), parameter :: optover=7
  !
  ! Arguments
  call sic_ch(line,0,1,fits_file,nh,.true.,error)
  if (error) return
  if (sic_narg(0).eq.1) then
    ! Only a FITS file name as argument: list its contents and leave
    call gfits_list_hdus(fits_file,error)
    return  ! Always
  endif
  call sic_ke(line,0,2,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,argum,key,ikey,ways,mway,error)
  if (error) return
  call sic_ch(line,0,3,gildas_file,ng,.true.,error)
  if (error) return
  !
  ! Options
  check = sic_present(optchec,0)
  overwrite = sic_present(optover,0)
  !
  style = ' '  ! Empty means guess style
  if (sic_present(optstyl,1)) then
    call sic_ke(line,optstyl,1,argum,nc,.true.,error)
    if (error)  return
    call sic_ambigs(rname,argum,comm,ncom,styles,mstyle,error)
    if (error) return
    style = comm
  endif
  !
  select case (key)
  case ('TO')  ! FITS --> GILDAS
    ! BLC/TRC
    call parse_blctrc('BLC',optblc,blc,error)
    if (error)  return
    call parse_blctrc('TRC',opttrc,trc,error)
    if (error)  return
    !
    ! /HDU
    hdunum = 1  ! Primary by default
    call sic_i4(line,opthdu,1,hdunum,.false.,error)
    if (error)  return
    !
    call fits_gildas_sub(fits_file,gildas_file,style,blc,trc,hdunum,check,  &
      overwrite,error,sic_getsymbol,sic_wpr)
    !
  case ('FROM')  ! GILDAS --> FITS
    if (sic_present(optblc,0) .or. sic_present(opttrc,0)) then
      chain = 'Options /BLC or /TRC not supported in this direction'
      call sic_message(seve%e,rname,chain)
      error = .true.
      return
    endif
    if (sic_present(opthdu,0)) then
      chain = 'Option /HDU not supported in this direction'
      call sic_message(seve%e,rname,chain)
      error = .true.
      return
    endif
    nbit = nbit_def
    call sic_i4(line,optbits,1,nbit,.false.,error)
    if (error)  return
    if (nbit.ne.16 .and. nbit.ne.32 .and. nbit.ne.-32 .and. nbit.ne.-64) then
      write(chain,'(A,I0)') 'Invalid number of bits ',nbit
      call sic_message(seve%e,rname,chain)
      error = .true.
      return
    endif
    !
    call gildas_fits_sub(gildas_file,fits_file,style,nbit,overwrite,error)
  end select
  !
contains
  subroutine parse_blctrc(what,optnum,corn,error)
    character(len=*),           intent(in)    :: what
    integer(kind=4),            intent(in)    :: optnum
    integer(kind=index_length), intent(out)   :: corn(:)
    logical,                    intent(inout) :: error
    ! Local
    integer(kind=4) :: i,n
    !
    corn(:) = 0
    if (.not.sic_present(optnum,0))  return
    !
    n = sic_narg(optnum)
    if (n.gt.gdf_maxdims) then
      write(chain,'(A,A,2X,I0,A,I0)')  &
        'Too many values for ',what,n,' > ',gdf_maxdims
      call sic_message(seve%e,rname,chain)
      error = .true.
      return
    endif
    do i=1,n
      call sic_i0(line,optnum,i,corn(i),.true.,error)
      if (error) return
    enddo
  end subroutine parse_blctrc
  !
end subroutine fits_gildas
