module sic_interfaces_private_c
  !---------------------------------------------------------------------
  ! @ private
  ! Private interfaces to C routines (hand made)
  !---------------------------------------------------------------------
  !
  interface
    function gpy_callfuncs(nargs,indx,strides,nwords,memory,a)
    use gildas_def
    integer(kind=4) :: gpy_callfuncs
    integer(kind=4),              intent(in)  :: nargs       !
    integer(kind=address_length), intent(in)  :: indx        ! a C size_t integer
    integer(kind=4),              intent(in)  :: strides(*)  !
    integer(kind=address_length), intent(in)  :: nwords(*)   ! a C size_t integer
    integer(kind=4),              intent(in)  :: memory(*)   ! The usual 'memory' array
    real(kind=4),                 intent(out) :: a           !
    end function gpy_callfuncs
  end interface
  !
  interface
    function gpy_callfuncd(nargs,indx,strides,nwords,memory,a)
    use gildas_def
    integer(kind=4) :: gpy_callfuncd
    integer(kind=4),              intent(in)  :: nargs       !
    integer(kind=address_length), intent(in)  :: indx        ! a C size_t integer
    integer(kind=4),              intent(in)  :: strides(*)  !
    integer(kind=address_length), intent(in)  :: nwords(*)   ! a C size_t integer
    integer(kind=4),              intent(in)  :: memory(*)   ! The usual 'memory' array
    real(kind=8),                 intent(out) :: a           !
    end function gpy_callfuncd
  end interface
  !
end module sic_interfaces_private_c
!
module sic_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! SIC interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use sic_interfaces_public
  use sic_interfaces_private
  use sic_interfaces_private_c
  !
end module sic_interfaces
!
module sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! SIC dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use ginc_interfaces_public
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
  use gwcs_interfaces_public
  use gfits_interfaces_public
  use gio_interfaces_public
  use ggui_interfaces_public_c
  !
end module sic_dependencies_interfaces
