function err_vector (dummy)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  logical :: err_vector             !
  logical :: dummy                  !
  err_vector = .false.
end function err_vector
!
subroutine load_vector
  use sic_dependencies_interfaces
  use sic_interactions
  use sic_interfaces, except_this=>load_vector
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: rname='VECTOR'
  integer(kind=4) :: ier
  integer(kind=4), parameter :: mcom=21
  character(len=12) :: vocab(mcom)
  character(len=32) :: name
  logical :: error
  ! Data
  data vocab /                                                               &
    ' FITS',   '/BITS','/STYLE','/HDU','/CHECK','/BLC','/TRC','/OVERWRITE',  &
    ' HEADER', '/EXTREMA','/TELESCOPE',                                      &
    '$RUN',    '/EDIT','/WINDOW','/NOWINDOW',                                &
    '$SUBMIT', '/EDIT','/WINDOW','/NOWINDOW',                                &
    ' SPY',                                                                  &
    ' TRANSPOSE' /
  !
  ! Set Up Language and Prompt
  call sic_begin ('VECTOR','GAG_HELP_VECTOR',mcom,vocab,'4.0    20-Jul-2011',  &
  run_vector,err_vector)
  !
  ! Define TASK executor
  task_node = 'LOCAL'  ! Default
  ier = sic_getlog('GILDAS_NODE',task_node)
  !
  error = .false.
  call sic_def_char('GILDAS_NODE',task_node,.false.,error)
  if (error) call sic_message(seve%e,rname,'Error defining GILDAS_NODE')
  !
  ! Window mode
  call sic_get_logi('SIC%WINDOW',run_window,error)
  call sic_def_logi('RUN_WINDOW',run_window,.false.,error)
  tee = .false.  ! WARNING: if 'tee' is .true., any runtime error when
                 ! the task is executed. is not trapped (because of the
                 ! pipe construct)
  call sic_def_logi('SIC%TEE',tee,.false.,error)
  ier = sic_getlog('GILDAS_PIPE_ERROR',name)
  if (ier.eq.0) then
    piperr = 1
  else
    piperr = 0
  endif
  !
  ! Load the tasks
  call load_task
  !
end subroutine load_vector
!
subroutine run_vector (line,comm,error)
  use sic_interfaces, except_this=>run_vector
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public-mandatory (but should be private in the context of new
  ! language initialization) (mandatory because symbol is used elsewhere)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  !
  call sic_message(seve%c,'VECTOR',line)
  !
  error = .false.
  select case (comm)
  case('TRANSPOSE')
    call sic_transpose (line,error)
  case('HEADER')
    call vector_header(line,error)
  case('FITS')
    call fits_gildas(line,error)
  !
  ! Task related commands
  case('RUN')
    call parameter(comm,line,error)
  case('SUBMIT')
    call parameter(comm,line,error)
  case('SPY')
    call spy (line,error)
  case default
    call sic_message(seve%e,'VECTOR','Unsupported command '//comm)
    error = .true.
  end select
end subroutine run_vector
!
subroutine detach(area,name,file,nowait,error)
  use gildas_def
  use gbl_message
  use gsys_types
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>detach
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  !  GILDAS Support routine for command
  !     RUN
  !  Creates a detached job to run the command
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: area    !
  character(len=*), intent(in)  :: name    ! Program name
  character(len=*), intent(in)  :: file    ! Parameter file name
  logical,          intent(in)  :: nowait  !
  logical,          intent(out) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DETACH'
  character(len=filename_length) :: input,output,tempo
  integer :: ni,no,ier,jer
  logical :: exist
  character(len=message_length) :: mess
  type(cputime_t) :: time
  !
  ! Remove old .gildas file if necessary
  tempo = name
  call sic_parsef(tempo,output,'GAG_LOG:','.gildas')
  no = len_trim(output)
  inquire(file=output,exist=exist)
  if (exist) then
    tempo = trim(output)//'~'
    ier = gag_filrename(output,tempo)
  endif
  !
  ! Check that output file can be created
  ier = sic_open(tlun,output,'NEW',.false.)
  if (ier.ne.0) then
    call putios('E-DETACH,  ',ier)
    call sic_message(seve%e,rname,'Error opening '//output)
    error = .true.
    return
  endif
  close(tlun)
  !
  ! Prepare command line
  !    AREA:taskname.exe < 'input'  >output 2>&1
  ! OR
  !    AREA:taskname.exe < 'input' 2>&1 | tee output
  !
  tempo = name
  !    CALL SIC_PARSEF(TEMPO,INPUT,AREA,TASKEXT)
  exist = sic_findfile(tempo,input,area,taskext)
  ! The following line does not work with GAG_SYSTEM
  !      INPUT = '"'//trim(INPUT)//'"'
  input = trim(input)//' < "'//trim(file)//'"'
  ni = len_trim(input)
  !
  ! This code is to obtain a copy on the screen through a "tee" pipe
  if (tee) then
    if (piperr.eq.0) then
      input(ni+1:) = ' 2>&1'
      ni = ni+5
    endif
    input(ni+1:) = ' | tee '
    ni = ni+7
    input(ni+1:) = '"'//trim(output)//'"'
    ni = ni+no+2
    !
    ! Simple log file, but no copy on screen
  else
    input(ni+1:) = ' > '
    ni = ni+3
    input(ni+1:) = '"'//trim(output)//'"'
    ni = ni+no+2
    if (piperr.eq.0) then
      input(ni+1:) = ' 2>&1'
      ni = ni+5
    endif
  endif
  !
  if (nowait) then
    input(ni+1:) = '&'
    ni = ni+1
  endif
  !
  call sic_message(seve%i,'RUN','Task '//trim(name)//' running, logfile is')
  call sic_message(seve%i,'RUN',output)
  tempo = input(1:ni)
  !
  call gag_cputime_init(time)
  call sic_message(seve%d,'RUN','Running command: '//tempo)
  ier = gag_system(tempo)
  ! Display messages (hereafter) in any case (error or not) and then exit
  !
  if ((.not.tee).and.(.not.nowait)) then
#if defined(WIN32)
    tempo = 'type "'//trim(output)//'"'
#else
    tempo = 'cat "'//trim(output)//'"'
#endif
    write (*,'(1X)')
    jer = gag_system(tempo)
    if (jer.ne.0) then
      call sic_message(seve%e,rname,'Error showing '//output)
      error = .true.
    endif
    write (*,'(1X)')
  endif
  call gag_cputime_get(time)
  write(mess,'(3(A,F0.1))') 'Elapsed ',time%diff%elapsed,  &
                            ', User ',time%diff%user,      &
                            ', System ',time%diff%system
  call sic_message(seve%i,'RUN',mess)
  !
  last_task = name
  if (ier.ne.0) then
    write (mess,102) 'Task ',trim(name),' crashed with status ',ier
    call sic_message(seve%i,'RUN',mess)
    error = .true.
  else
    if (nowait) then
      call sic_wait(3.0)
      call sic_message(seve%i,'RUN','Task '//trim(name)//' activated ...')
    else
      call sic_message(seve%i,'RUN','Task '//trim(name)//' completed '//  &
      'successfully')
    endif
    error = .false.
  endif
  !
102 format(a,a,a,i0)
end subroutine detach
!
subroutine spy (line,error)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interactions
  use sic_interfaces, except_this=>spy
  !---------------------------------------------------------------------
  ! @ private
  !	Support routine for command SPY
  !	List all active Tasks of current session.
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  integer :: i,ier
  character(len=filename_length) :: name,file,para
  !
  if (sic_present(0,1)) then
    call sic_ch (line,0,1,name,i,.true.,error)
    if (error) return
  else
    name = last_task
  endif
  !
  call sic_parsef (name,para,'GAG_SCRATCH:','.par')
  call sic_parsef (name,file,'GAG_LOG:','.gildas')
#if defined(WIN32)
  name = 'type '//trim(file)//'|more'
#else
  name = 'more '//trim(para)//' '//file
#endif
  ier = gag_system(name)
  if (ier.ne.0) then
    call sic_message(seve%e,'SPY','Error spying file '//file)
    error = .true.
    return
  endif
end subroutine spy
!
! Remote.f ---
subroutine remote (name,lun,file,error,inter,node)
  use sic_interfaces, except_this=>remote
  use sic_interactions
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GILDAS	Internal routine
  !	Submit a program to run in batch queue GILDAS_BATCH
  ! Arguments:
  !	NAME	C*(*)	Program Name
  !	LUN	I	Logical unit where file is opened
  !       FILE    C*(*)   Input file name
  !	ERROR	L	Logical error flag
  !       INTER   L       RUN or SUBMIT command ?
  !       NODE    C*(*)   Remote Node
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  integer :: lun                    !
  character(len=*) :: file          !
  logical :: error                  !
  logical :: inter                  !
  character(len=*) :: node          !
  ! Local
  character(len=255) :: input,output,chain
  character(len=128) :: pwd
  character(len=10) :: word
  integer :: lpa
  integer :: nl,ni,no,nf,ier
  character(len=message_length) :: mess
  !
  close (unit=lun)
  !
  ! Remote Node Name
  call sic_lower(node)
  !
  ! Log File: should it go in current directory ?
  nl = len_trim(name)
  call local_to_remote('gag_log:',output,node)
  no = len_trim(output)
  output(no+1:) = trim(name)//'.gildas'
  no = len_trim(output)
  !
  ! Prepare command line
  !	GILDAS_RUN:taskname.exe < input  > output 2>&1
  !
  ! Command line should actually depend on remote system type. The above syntax
  ! is for instance invalid if the remote system is a SUN.
  call local_to_remote('GILDAS_RUN:',input,node)
  ni = len_trim(input)
  input(ni+1:) = name
  ni  = ni+nl
  nf = len_trim(file)
  input(ni+1:) = ".exe < '"//trim(file)//"' >"
  ni = ni+nf+11
  input(ni+1:ni+no) = output(1:no)
  ni = ni+no
  !
  if (piperr.eq.0) then
    input(ni+1:) = ' 2>&1'
    ni = ni+5
  endif
  !
  ier = sic_open (lun,'task_submit.tmp','NEW',.false.)
  if (ier.ne.0) then
    call sic_message(seve%i,'SUBMIT','Open error file "task_submit.tmp"')
    call putios('I-SUBMIT, ',ier)
    error = .true.
    return
  endif
  !
  ! Add equivalent working directory in remote tasks
  pwd = ' '
  lpa = 0
  call sic_setdir(pwd,lpa,error)
  !
  write (lun,'(A)') 'cd '//pwd(1:lpa)
  write (lun,'(A)') input(1:ni)
  write (lun,'(A,A)') 'rm ',trim(file)
  write (lun,'(A)') 'rm task_submit.tmp'
  close (unit=lun)
  !
  ! OK, send the remote command
  if (inter) then
    chain = 'ssh '//trim(node)//' < task_submit.tmp'
    word  = 'executed,'
  else
    chain = 'ssh '//trim(node)//'  "batch < task_submit.tmp'
    word  = 'submitted,'
  endif
  ier = gag_system (chain)
  error = ier.ne.0
  !
  if (error) then
    write (mess,102) 'Task ',trim(name),' not '//word//' status ',ier
    call sic_message(seve%e,'REMOTE',mess)
  else
    call sic_message(seve%i,'REMOTE','Task '//trim(name)//' remotely '//  &
    word//' logfile is')
    call sic_message(seve%i,'REMOTE',output(1:no))
  endif
  !
102 format(a,a,a,i13)
end subroutine remote
!
subroutine local_to_remote (name,file,node)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>local_to_remote
  !---------------------------------------------------------------------
  ! @ private
  !  VECTOR
  !     Find the equivalent remote path of a local path
  ! Arguments:
  ! 	NAME	C*(*)	File name			Input
  !	FILE	C*(*)	Fully extended file name	Output
  !	NODE    C*(*)	Remote NODE                     Input
  !---------------------------------------------------------------------
  character(len=*) :: name          !
  character(len=*) :: file          !
  character(len=*) :: node          !
  call sic_parse_file(name,"","",file)
end subroutine local_to_remote
!
! Submit.f ---
subroutine submit(area,name,lun,file,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>submit
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GILDAS	Internal routine
  !	Submit a program to run in batch queue GILDAS_BATCH
  ! Arguments:
  !	NAME	C*(*)	Program Name
  !	LUN	I	Logical unit where file is opened
  !	ERROR	L	Logical error flag
  !---------------------------------------------------------------------
  character(len=*) :: area          !
  character(len=*) :: name          !
  integer :: lun                    !
  character(len=*) :: file          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='SUBMIT'
  character(len=256) :: input,output,tempo,fich
  character(len=24) :: nom,time
  character(len=256) :: chain
  integer :: ni,no,ier,nff
  character(len=message_length) :: mess
  !
  close (unit=lun)
  tempo = name
  call sic_parsef(tempo,output,'GAG_LOG:','.gildas')
  no = len_trim(output)
  !
  ! Prepare command line
  !	AREA:taskname.exe < input  >output 2>&1
  !
  ! Get unambigous file name based on time
  call sic_date(fich)
  time = fich(13:14)//fich(16:17)//fich(19:20)//fich(22:23)
  nom = 'par-'//time
  call sic_parsef(nom,fich,'GAG_TMP:','.par')
  nff = len_trim(fich)
  ! Copy parameter file
  tempo = 'cp '//trim(file)//' '//fich
  ier = gag_system(tempo)
  call sic_wait(1.0)
  !
  tempo = name
  call sic_parsef(tempo,input,area,taskext)
  ni = len_trim(input)
  input(ni+1:) = " < '"//trim(fich)//"' >"
  ni = ni+nff+7
  input(ni+1:ni+no) = output(1:no)
  ni = ni+no
  input(ni+1:) = ' 2>&1'
  ni = ni+5
  !
  nom = 'par-'//time
  call sic_parsef(nom,fich,'GAG_TMP:','.submit')
  open (unit=lun,file=fich,status='UNKNOWN',iostat=ier)
  if (ier.ne.0) then
    call sic_message(seve%i,rname,'Open error file "task.submit"')
    call putios('I-SUBMIT,  ',ier)
    error = .true.
    call sic_frelun(lun)
    return
  endif
  !
  write (lun,'(A)') input(1:ni)
  close (unit=lun)
  !
  chain = 'batch < '//fich
  ier = gag_system (chain)
  if (ier.ne.0) then
    write (mess,102) 'Task ',trim(name),' not submitted, status ',ier
    call sic_message(seve%i,rname,mess)
    error = .true.
  else
    call sic_message(seve%i,rname,'Task '//trim(name)//' submitted, '//  &
    'logfile is')
    call sic_message(seve%i,rname,output(1:no))
    error = .false.
  endif
102 format(a,a,a,i13)
end subroutine submit
!
! Parameter.f
subroutine parameter(comm,line,error)
  use sic_dependencies_interfaces
  use sic_interactions
  use sic_interfaces, except_this=>parameter
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GILDAS	Support routine for commands
  !	RUN 	Program [Parameter_File] [/EDIT] [/NOWINDOW] [/WINDOW]
  !	SUBMIT	Program [Parameter_File] [/EDIT] [/NOWINDOW] [/WINDOW]
  ! Arguments
  !	COMM	C*(*)	Command (RUN or SUBMIT)
  !	LINE	C*(*)	Command line
  !	ERROR	L	Logical error flag
  !---------------------------------------------------------------------
  character(len=*) :: comm          !
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  character(len=1), parameter :: backslash=char(92)
  integer :: nc,lun,iopt,ntas,nini,narea,lno,nf
  character(len=128) :: taskname
  character(len=filename_length) :: program,initfile,checkfile,helpfile
  character(len=20) :: area,node,help
  character(len=256) :: chain,command,sline
  integer :: lire,status
  logical :: edit,exist,run,remo,xwnd,nowait,ok
  !
  sline = line
  error = .false.
  !
  ! Get GILDAS Node Name
  call sic_get_char('GILDAS_NODE',node,lno,error)
  remo = node(1:lno) .ne. 'LOCAL'
  !
  ! Get procedure name and verify that it exists
  edit = sic_present(1,0)
  xwnd = sic_present(2,0)
  if (sic_present(3,0)) then
    if (xwnd) then
      call sic_message(seve%e,'RUN','Conflicting options')
      error = .true.
      return
    endif
    x_window = .false.
  else
    x_window = run_window .or. xwnd
  endif
  !
  ! Slightly tricky code to support RUN /EDIT /WINDOW and RUN /WINDOW /EDIT
  ! and "RUN /EDIT toto tutu /WINDOW" and "RUN /WINDOW toto tutu /EDIT",
  ! etc...
  iopt = sic_narg(0)
  if (iopt.eq.0) then
    if (edit.and.xwnd) then
      if (sic_narg(1).eq.1) then
        iopt = 1
      elseif (sic_narg(2).eq.1) then
        iopt = 2
      endif
    elseif (edit) then
      iopt = 1
    elseif (xwnd) then
      iopt = 2
    endif
  else
    iopt = 0
  endif
  !
  call sic_ch (line,iopt,1,taskname,nc,.true.,error)
  if (error) return
  call sic_parsef(taskname,program,'GILDAS_LOCAL:',taskext)
  ntas = len_trim(taskname)
  inquire (file=program,exist=exist)
  if (.not.exist) then
    exist = sic_findfile(taskname,program,'TASK#DIR:',taskext)
    if (.not.exist) then
      call sic_message(seve%e,comm,'Program '//trim(taskname)//' does not '//  &
      'exist')
      call sic_parse_file('gag_help_run_deleted',' ','.hlp',helpfile)
      ok = sichelp(puthelp,taskname,"",helpfile,.true.,.true.)
      error = .true.
      return
    endif
    area = 'TASK#DIR:'
    help = area
  else
    area = 'GILDAS_LOCAL:'
    help = 'GILDAS_LOCAL:'
  endif
  status = parse_file(program,-1)
  narea = len_trim(area)
  !
  ! No /EDIT option :
  if (.not.edit) then
    if (sic_present(iopt,2)) then
      call sic_ch (line,iopt,2,checkfile,nc,.false.,error)
      if (error) return
      if (checkfile.eq.'*') checkfile = trim(taskname)//'.init'
      call sic_parsef(checkfile,initfile,' ','.init')
    else
      exist = sic_findfile(taskname,initfile,area,'.init')
    endif
    !
    ! Check if parameter file already exist
    nini = len_trim(initfile)
    inquire (file=initfile,exist=exist)
    if (.not.exist) then
      call sic_message(seve%e,comm,'Parameter file does not exist')
      error = .true.
      return
    endif
    !
    ! /EDIT option : take parameter file in current directory if it exits,
    ! else take default parameter file
    !
  else
    call edtask (line,comm,iopt,taskname,ntas,initfile,nini,area,narea,error)
    if (error) return
  endif
  !
  ! Checker file
  error = .not.sic_findfile(taskname,checkfile,area,'.check')
  if (error) then
    call sic_parsef(taskname,checkfile,'gag_scratch:','.check')
    error = .false.
  endif
  call sic_ch(line,iopt,3,checkfile,nc,.false.,error)
  if (error) return
  !
  ! Window mode
  if (x_window) then
    call copy_parfile (line,comm,iopt,taskname,ntas,initfile,nini,area,narea,  &
    error)
    if (error) return
    error = .not.sic_findfile(taskname,helpfile,help,'.hlp')
    if (error) then
      call sic_message(seve%e,comm,trim(taskname)//'.hlp file not found')
      return
    endif
    !
    ! Transmit Help File name for further use
    help_text_file = trim(helpfile)//char(0)
    !
    chain = 'GUI'//backslash//'PANEL '//trim(taskname)//' "'//trim(helpfile)//  &
    '" /LOG task'
    nf = len_trim(chain)
    call sic_blanc(chain,nf)
    call sic_analyse(command,chain,nf,error)
    call xgag_input(chain,error)
  endif
  !
  ! Prepare data file
  call prepare(comm,taskname,program,initfile,checkfile,lun,remo,error)
  if (error) then
    close(unit=lun,status='DELETE')
    return
  endif
  !
  ! Submit (or run) the job
  if (.not.remo) then
    if (comm.eq.'SUBMIT') then
      call submit(area,taskname,lun,initfile,error)
    elseif (comm.eq.'RUN') then
      close (unit=lun)
      ! If GO has an argument, assume we should not wait...
      nowait = sic_present(0,1)  ! Invalid: command line pointers have been
                                 ! overwritten when parsing the init file!
      call detach(area,taskname,initfile,nowait,error)
    endif
  else
    run = comm.eq.'RUN'
    call remote(taskname,lun,initfile,error,run,node)
  endif
  lire = sic_lire()
  if (lire.eq.0) call sic_insert(sline)
end subroutine parameter
!
! edtask.f
subroutine edtask (line,comm,iopt,task,ntas,init,nini,area,narea,error)
  use sic_interfaces, except_this=>edtask
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  character(len=*)                :: comm   !
  integer                         :: iopt   !
  character(len=*)                :: task   !
  integer                         :: ntas   !
  character(len=*)                :: init   !
  integer                         :: nini   !
  character(len=*)                :: area   !
  integer                         :: narea  !
  logical,          intent(inout) :: error  !
  ! Local
  integer :: nc
  !
  character(len=132) :: fich
  !
  ! Prepare parameter file
  call copy_parfile (line,comm,iopt,task,ntas,init,nini,area,narea,error)
  !
  ! Edit the file
  call editor(init(1:nini),error)
  if (error) return
  !
  fich = ' '
  call sic_wprn('I-EDIT,  Press <CR> to run task, Q to QUIT ',fich,nc)
  if (len_trim(fich).ne.0) then
    call sic_message(seve%e,comm,'Aborted by QUIT command')
    error = .true.
  endif
end subroutine edtask
!
subroutine copy_parfile (line,comm,iopt,task,ntas,init,nini,area,narea,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>copy_parfile
  !---------------------------------------------------------------------
  ! @ private
  ! Copy Parameter File
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  character(len=*)                :: comm   !
  integer                         :: iopt   !
  character(len=*)                :: task   !
  integer                         :: ntas   !
  character(len=*)                :: init   !
  integer                         :: nini   !
  character(len=*)                :: area   !
  integer                         :: narea  !
  logical,          intent(inout) :: error  !
  ! Local
  logical :: exist
  character(len=256) :: comlin,comlin2
  character(len=132) :: fich
  integer(kind=4) :: ier
  !
  if (sic_present(iopt,2)) then
    call sic_ch (line,iopt,2,fich,nini,.false.,error)
    if (error) return
    if (fich.eq.'*') fich = task(1:ntas)
    call sic_parsef(fich,init,' ','.init')
    nini = len_trim(init)
  else
    fich = task(1:ntas)
    call sic_parsef(fich,init,' ','.init')
    nini = len_trim(init)
    inquire (file=init,exist=exist)
    if (.not.exist) then
      fich = task(1:ntas)
      exist = sic_findfile(fich,comlin,area,'.init')
      comlin2= init(1:nini)
      ier = gag_filcopy(comlin,comlin2)
    endif
  endif
end subroutine copy_parfile
!
! Prepare.f
subroutine prepare(comm,task,exec,file,check_file,lun,rem,error)
  use sic_interfaces, except_this=>prepare
  use gildas_def
  use sic_interactions
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GILDAS	Internal routine
  !	Execute the two procedures necessary to setup
  !	task parameters before running or submitting the task
  !
  ! Arguments :
  !	TASK		Task name
  !	EXEC		Program to be executed
  !	FILE		User edited procedure (input)
  !			    command file to execute (output)
  !	CHECK_FILE	Checking procedure
  !	LUN		  Logical unit number where procedure is editted
  !       REM     Remote Flag
  !     ERROR		Logical error flag
  !---------------------------------------------------------------------
  character(len=*) :: comm          !
  character(len=*) :: task          !
  character(len=*) :: exec          !
  character(len=*) :: file          !
  character(len=*) :: check_file    !
  integer :: lun                    !
  logical :: rem                    !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='TASK'
  integer :: nt,ier,i,status
  character(len=filename_length) :: fich,proced
  logical :: found
  !
  tprog = task
  nt = len_trim(task)
  !
  ! Open parameter / command file
  if (rem) then
    task(nt+1:)='_PAR'
    nt=nt+4
    call sic_parsef (task,fich,' ','.tmp')
    nt = nt-4
    task(nt+1:) = ' '
  else
    call sic_parsef (task,fich,'GAG_SCRATCH:','.par')
  endif
  ier = sic_open(tlun,fich,'NEW',.false.)
  if (ier.ne.0) then
    call putios('E-TASK, ',ier)
    call sic_message(seve%e,rname,'Error opening '//fich)
    error = .true.
    return
  endif
  !
  tabort = .true.
  nreal = 0
  ninte = 0
  nlogi = 0
  nchar = 0
  npar = 0
  !
  ! Make file name non-translatable.
  proced = '@ "!'//trim(file)//'"'
  !
  ! Duplicate init file if X_Window mode.
  if (x_window) then
    call sic_if (.true.)
    call exec_task(proced)
    call xgag_finish
    call sic_if (.false.)
    x_window = .false.
    call task_out(file,task,error)
  else
    call exec_task(proced)
    if (tabort) call sic_message(seve%e,rname,'Missing GO command in INIT file')
  endif
  !
  ! Now execute the file
  if (tabort) then
    continue
    !         CALL GAGOUT('E-TASK,  Missing GO command in INIT file')
  else
    tabort = .true.
    file = fich
    status = parse_file(file,-1)
    !
    ! Create .check file if it does not exist
    inquire(file=check_file,exist=found)
    if (.not.found) then
      call write_check (check_file,error)
      if (error) return
    endif
    ! Make file name non-translatable.
    proced = '@ "!'//trim(check_file)//'"'
    call exec_task(proced)
    !
    ! Remove .check file if it was created here
    if (.not.found) then
      call gag_filrm(check_file)
    endif
  endif
  do i=1,npar
    call sic_delvariable(tname(i),.false.,error)
  enddo
  lun = tlun
  error = tabort
end subroutine prepare
!
function parse_file(chain,stat)
  use sic_dependencies_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ private
  ! VECTOR
  !	Support routine for commands FILE [/OLD] [/NEW]
  !	CHAIN	C*(*)	File name
  !	STAT	I	Status of file (0 Unknown, -1 Old, 1 New)
  !---------------------------------------------------------------------
  integer :: parse_file             !
  character(len=*) :: chain         !
  integer :: stat                   !
  ! Local
  character(len=filename_length) :: name,tempo
  integer :: i,lnode,ldisk,lname,ier
  !
  parse_file = 0
  !
  ! One logical name translation, just in case of logname
  call sic_parse_file(chain,"","",chain)
  !
  ! Check for node
  lnode = index(chain,'::')
  if (lnode.ne.0) return
  !
  ! Find "OLD" files
  parse_file = 1
  !
  ! Check for disk for "UNKNOWN" or "NEW" files
  ldisk = index(chain,':')
  if (ldisk.eq.0) return
  lname = ldisk-1
  name = chain(1:lname)
  tempo = chain(ldisk+1:)
  !
  ! Translate all possible logical names
  do i=1,10
    ier = sic_getlog(name)
    lname = len_trim(name)
    ldisk = index(name(1:lname),':')
    if (ldisk.ne.0) then
      if (name(ldisk:ldisk+1).eq.'::') ldisk = 0
    endif
    if (ldisk.ne.lname) then
      if (name(lname-1:lname).eq.'.]') then
        chain = name(ldisk+1:lname-1)//tempo(2:)
      elseif (name(lname:lname).eq.']') then
        chain = name(ldisk+1:lname)//tempo
      elseif (tempo(1:1).ne.':') then
        chain = name(ldisk+1:lname)//':'//tempo
      else
        chain = name(ldisk+1:lname)//tempo
      endif
      tempo=chain
    endif
    if (ldisk.eq.0) return
    name(ldisk:) = ' '
  enddo
end function parse_file
!
subroutine task_out(file,task,error)
  use sic_interfaces, except_this=>task_out
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! TASK
  !     Copy the .INIT file after X-Window input
  !---------------------------------------------------------------------
  character(len=*) :: file          !
  character(len=*) :: task          !
  logical :: error                  !
  ! Local
  character(len=1), parameter :: backslash=char(92)
  character(len=256) :: line
  character(len=filename_length) :: argu  ! Any kind of argument (numeric, file name,...)
  character(len=256) :: fich1,fich2
  character(len=5) :: tache
  integer :: ilin,iopt,idol,ibra,iquo,icom
  integer :: ilun,slun,dlun,nf,ier
  !
  integer :: icont
  !
  ier = sic_getlun(ilun)
  ier = sic_getlun(dlun)
  ier = sic_getlun(slun)
  !
  ! Scan .init file & .save file as input
  ! Open .last file as output
  tache = 'TASK'//backslash
  nf = len_trim(file)
  ier=sic_open (ilun,file,'OLD',.true.)
  ier=sic_open (slun,'task.save','OLD',.true.)
  call sic_parsef (task,fich2,' ','.last')
  ier=sic_open (dlun,fich2(1:nf),'UNKNOWN',.false.)
  do while (.true.)
    read(ilun,'(A)',end=10) line
    ilin = len_trim(line)
    icom = index(line,'!')
    icont = index(line(ilin:),'-')
    do while (icont.gt.0)
      line(ilin:ilin) = ' '
      read (ilun,'(A)',end=11) line(ilin+1:)
      ilin = len_trim(line)
      icont = index(line(ilin:),'-')
    enddo
11  continue
    if (index(line,tache).eq.0) then
      write(dlun,'(A)') line(1:ilin)
    elseif (icom.gt.0.and.index(line,tache).gt.icom) then
      write(dlun,'(A)') line(1:ilin)
    elseif (index(line,'MORE').ne.0) then
      write(dlun,'(A)') line(1:ilin)
    else
      read(slun,'(A)',iostat=ier) argu
      if (ier.ne.0) then
        write(dlun,'(A)') line(1:ilin)
      else
        idol = index(line,'$')
        if (line(idol+1:idol+1).eq.'[') then
          ibra = index(line(idol:),']')
          idol = idol+ibra
        endif
        !
        ! Check if an option precedes the argument.
        iquo = index(line(idol:),'"')
        !
        ! Quoted text
        if (iquo.ne.0) then
          iquo = iquo+idol
          iopt = index(line(iquo:),'"')
          iquo = iopt+iquo
          ! followed by options or not
          iopt = index(line(iquo:),'/')
          if (iopt.eq.0) then
            write(dlun,'(A,1X,A)') line(1:idol),trim(argu)
          else
            iopt = iopt+iquo-1
            write(dlun,'(A,1X,A,1X,A)') line(1:idol),trim(argu),line(iopt:ilin)
          endif
        else
          !
          ! No quoted text
          iopt = index(line(idol:),'/')
          if (iopt.eq.0) then
            write(dlun,'(A,1X,A)') line(1:idol),trim(argu)
          else
            iopt = iopt+idol-1
            write(dlun,'(A,1X,A,1X,A)') line(1:idol),trim(argu),line(iopt:ilin)
          endif
        endif
      endif
    endif
  enddo
  !
10 close(unit=ilun)
  close(unit=slun)
  close(unit=dlun)
  call sic_frelun(ilun)
  call sic_frelun(slun)
  call sic_frelun(dlun)
  !
  fich1 = fich2(1:nf)
  fich2 = file(1:nf)
  ier = gag_filrename(fich1,fich2)
  call gag_filrm('task.save')
end subroutine task_out
!
subroutine write_check(chkfil,error)
  use sic_interfaces, except_this=>write_check
  use sic_interactions
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! Automatically creat the .check file if it is not already
  ! existing.
  !---------------------------------------------------------------------
  character(len=*) :: chkfil        !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='TASK'
  integer :: i,ier,ilun
  !
  ier = sic_getlun(ilun)
  if (ier.ne.1) then
    error = .true.
    return
  endif
  ier = sic_open(ilun,chkfil,'NEW',.false.)
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Cannot create .check file')
    call putios('E-TASK, ',ier)
    error = .true.
    return
  endif
  !
  do i=1,npar
    write(ilun,100) tname(i)
  enddo
  write(ilun,101)
  close (unit=ilun)
  call sic_frelun(ilun)
  !
  call sic_message(seve%i,rname,'Created .check File '//chkfil)
  !
100 format ('TASK\WRITE ',a)
101 format ('TASK\GO')
end subroutine write_check
