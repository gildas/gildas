###########################################################################
#
# Makefile system for GILDAS softwares (2003-2024).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################

include $(gagadmdir)/Makefile.def

###########################################################################

LIB_IDENTITY = sic

# Creates pysic.so (implicit compilation of sic-pyimport.c)
IMPORT_FROM_PYTHON = yes

LIB_EXPORTS = gpackage-sicimport.h gpackage-pyimport.h	\
sic_interfaces_public.mod sic_types.mod

LIB_C_OBJECTS = getcar.o import-c.o sic-message-c.o sic-sicimport.o	\
gmaster-c.o

LIB_F_OBJECTS = aliasvar.o argauto.o abbrev.o analyse.o argumexp.o	\
codefunc.o compute.o controlc.o datetime.o defvar.o delete.o delvar.o	\
desc.o diff.o dofunc.o dofuncl.o dofuncs.o dofuncd.o dofunci.o		\
editor.o error.o evaluate.o examine.o execsic.o execute.o		\
expand_macro.o expr.o fill.o fits.o forme.o get.o getcom.o getlin.o	\
grep.o hashing.o header.o help.o import.o inicli.o interstate.o		\
intrinsic.o let.o liste.o loop.o lun.o macro.o math.o mcmc.o		\
minimize.o modify.o operand.o powell.o procedure.o prompt.o read.o	\
reduce.o reduce-math.o sicfits.o gmessage.o sicopt.o sic_inter_mod.o	\
sic-interfaces.o sic-interfaces-private.o sic-interfaces-public.o	\
sic-package.o sic_parse_mod.o sic-python-f.o sicsay.o sicset.o		\
sicsub.o sicvms.o siman.o sort.o stack.o symbol.o task.o timer.o	\
transpose.o varexp.o varexp-generic.o vector.o wrpro.o xvariable.o	\
gmaster.o sic-message.o

ifeq ($(GAG_USE_PYTHON),yes)
  LIB_C_OBJECTS += sic_python.o
endif

LOCAL_CPPFLAGS = -DARCH="'$(GAG_MACHINE)'" \
		 -DREV_NAME="'$(GAG_VERS)'" \
		 -DREV_COMMIT="'$(shell git log -1 --format=%h 2> /dev/null || echo)'" \
		 -DREV_DATE="'$(shell git log -1 --format=%aI 2> /dev/null || echo)'" \
		 -DSYSTEM="'$(GAG_EXEC_SYSTEM)'" \
		 -DOS_KIND="'$(GAG_ENV_KIND)'" \
		 -DOS_NAME="'$(GAG_ENV_NAME)'" \
		 -DOS_VERSION="'$(GAG_ENV_VERS)'" \
		 -DPYTHON_VERSION="'$(GAG_PYTHON_VERSION)'" \
		 -DCOMP_NAME="'$(GAG_COMPILER_FEXE)'"

LOCAL_CFLAGS = -I../

LOCAL_FFLAGS = -I../gsys/$(moddir)

LIB_DEPENDS  = $(SIC_LIB_DEPENDS)

ADD_LIBS += $(SIC_SYS_LIBS)

ifeq ($(GAG_COMPILER_FKIND),gfortran)
  ifeq ($(GAG_COMPILER_FVERSION_GE_10),yes)
    LOCAL_FFLAGS += -fallow-argument-mismatch
  endif
endif

###########################################################################

include $(gagadmdir)/Makefile.lib

###########################################################################

include $(builddir)/Makefile.deps

###########################################################################
