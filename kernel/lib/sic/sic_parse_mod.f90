!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_types
  use gildas_def
  use image_def
  !--------------------------------------------------------------------
  !
  !--------------------------------------------------------------------
  !
  type :: sic_identifier_t
    ! Unique identifier of a Sic variable
    character(len=varname_length) :: name
    integer(kind=4)               :: lname
    integer(kind=4)               :: level
  end type sic_identifier_t
  !
  ! Code for variable descriptor status. A positive status indicates a
  ! variable mapped to an image file, and stores its image slot. These
  ! codes should therefore be negative to avoid conflicts.
  integer, parameter :: program_defined=0    ! Code for program defined (static) named variables
  integer, parameter :: scratch_operand=-1   ! Code for scratch (allocated, used) un-named operands
  integer, parameter :: free_operand=-2      ! Code for free (allocated, unused) un-named operands
  integer, parameter :: readonly_operand=-3  ! Code for READONLY un-named operands (static strings, variable parts...)
  integer, parameter :: interm_operand=-4    ! Code for intermediate results (un-named operands pointing to final result)
  integer, parameter :: user_defined=-5      ! Code for interactively defined variables, i.e. SIC\DEFINE REAL A
  integer, parameter :: alias_defined=-6     ! Variable is an alias
  integer, parameter :: empty_operand=-999   ! Code for unallocated operands
  !
  type :: sic_descriptor_t
    integer(kind=4)              :: type              ! Variable type
    integer(kind=4)              :: ndim              ! Variable number of dimensions
    integer(kind=index_length)   :: dims(sic_maxdims) ! Variable dimensions
    integer(kind=address_length) :: addr = ptr_null   ! Data address (if relevant)
    type(gildas), pointer        :: head => null()    ! Pointer to header (if relevant)
    integer(kind=size_length)    :: size              ! Size of allocation (words)
    integer(kind=4)        :: status = empty_operand  ! Origin of variable (codes above)
                                                      !    0     program defined
                                                      !  < 0     various codes
                                                      !  > 0     image (slot in gio)
    logical                      :: readonly          ! Variable is read only for user?
  end type sic_descriptor_t
  !
  type :: sic_variable_t
    ! A Sic variable has an identifier and an internal descriptor
    type(sic_identifier_t) :: id
    type(sic_descriptor_t) :: desc
  end type sic_variable_t
  !
  ! Description dimensions in Sic, for parsing e.g. A[1,2][3:4]
  type :: sic_dimensions_do_t
    logical :: strict    ! Must be a valid specifier, as opposed to an expression like "A[1].eq.B[2]"
    logical :: range     ! e.g. A[3:5] allowed
    logical :: subset    ! e.g. A[2,] allowed
    logical :: implicit  ! e.g. A[i] allowed
    logical :: twospec   ! e.g. A[i][j:k] allowed
  end type sic_dimensions_do_t
  type :: sic_dimensions_done_t
    logical :: range     ! e.g. A[3:5] was found
  ! logical :: subset    ! e.g. A[2,] was found
    logical :: implicit  ! e.g. A[i] was found
    integer(kind=4) :: ndim
    integer(kind=index_length) :: dims(sic_maxdims,2)
    type(sic_identifier_t)     :: vars(sic_maxdims)  ! Name of implicit dimensions
  end type sic_dimensions_done_t
  type :: sic_dimensions_t
    type(sic_dimensions_do_t)   :: do
    type(sic_dimensions_done_t) :: done(2)
  end type sic_dimensions_t
  !
  integer(kind=4), parameter :: symbol_length=12
  integer(kind=4), parameter :: translation_length=512
  type :: sic_symdict_t
    ! Symbols
    integer(kind=4)                                :: msym=0     ! Maximum number of symbols
    character(len=symbol_length),      allocatable :: name(:)    ! Symbol name
    character(len=translation_length), allocatable :: trans(:)   ! Symbol translation
    integer(kind=4),                   allocatable :: ltrans(:)  ! Length of translation
    ! Dictionary hashing
    integer(kind=4)              :: pf(0:27)  ! Alphabetic pointers PF(27)=number of symbols
    integer(kind=4), allocatable :: pn(:)     ! Chained pointers
  end type sic_symdict_t
  !
  ! Type describing a program-defined function (sic_def_func)
  type :: sic_function_t
    integer(kind=4)                :: code  ! Identifier
    integer(kind=4)                :: narg  ! Number of arguments
    character(len=filename_length) :: help  ! Associated HLP file / SIC logical
  end type sic_function_t
  !
  integer, parameter :: language_length=12 ! Internal limit on languages name
  type :: sic_language_t
    character(len=language_length) :: name          ! Language name
    integer(kind=4)                :: lname         ! Name length
    character(len=132)             :: mess          ! Short language description
    character(len=filename_length) :: help          ! Logical name of help file
    integer(kind=4)                :: prio          ! Language priority
    logical                        :: asleep        ! Language not in scope
    logical                        :: libmod        ! Library only mode
    logical                        :: user=.false.  ! Is the language a user language?
    type(sic_symdict_t), pointer   :: usym=>null()  ! Dictionary of symbols is user language
  end type sic_language_t
  !
  ! Type for parsed command line
  integer(kind=4), parameter :: command_length=16  ! Internal limit on commands and options name
  integer(kind=4), parameter :: mword=512          ! Maximum number of items on a command line
  integer(kind=4), parameter :: maxopt=128         ! Maximum number of options to a command
  type :: sic_command_t
    ! Command line
    integer(kind=address_length)   :: aline        ! Memory address of command line
    ! Language
    integer(kind=4)                :: ilang        ! Language id
    character(len=language_length) :: lang         ! Language name
    ! Command
    integer(kind=4)                :: icom         ! Command number id
    character(len=command_length)  :: command      ! Command name
    integer(kind=4)                :: mopt         ! Max number of options for this command
    ! Arguments
    integer(kind=4) :: nword           ! Total number of word in command line (command included)
    integer(kind=4) :: ibeg(mword)     ! Pointer to beginning of word in line
    integer(kind=4) :: iend(mword)     ! Pointer to end of word in line
    integer(kind=4) :: narg(0:maxopt)  ! Number of arguments of command/options
    integer(kind=4) :: popt(0:maxopt)  ! Position of option in command line [words]
    ! Initialization done?
    logical :: initialized=.false.
  end type sic_command_t
  !
  ! Support types for Sic lists:
  ! List1 ... ListN
  !  where
  ! ListI = a [TO b [BY c]]
  type :: sic_listi4_t
    integer(kind=4)              :: nlist=0  ! Actual number of lists
    integer(kind=4)              :: mlist=0  ! Maximum number of lists (size of allocation)
    integer(kind=4), allocatable :: i1(:)    ! Lower bounds
    integer(kind=4), allocatable :: i2(:)    ! Upper bounds
    integer(kind=4), allocatable :: i3(:)    ! Steps
  end type sic_listi4_t
  type :: sic_listi8_t
    integer(kind=4)              :: nlist=0  ! Actual number of lists
    integer(kind=4)              :: mlist=0  ! Maximum number of lists (size of allocation)
    integer(kind=8), allocatable :: i1(:)    ! Lower bounds
    integer(kind=8), allocatable :: i2(:)    ! Upper bounds
    integer(kind=8), allocatable :: i3(:)    ! Steps
  end type sic_listi8_t
  type :: sic_listr8_t
    integer(kind=4)           :: nlist=0  ! Actual number of lists
    integer(kind=4)           :: mlist=0  ! Maximum number of lists (size of allocation)
    real(kind=8), allocatable :: r1(:)    ! Lower bounds
    real(kind=8), allocatable :: r2(:)    ! Upper bounds
    real(kind=8), allocatable :: r3(:)    ! Steps
  end type sic_listr8_t
  !
  type datetime_spec_t
    logical                   :: isvar  ! Termimal or variable?
    type(sic_descriptor_t)    :: desc   !
    integer(kind=4)           :: form   !
    integer(kind=size_length) :: nelem  !
  end type datetime_spec_t
  !
end module sic_types
!
module sic_structures
  use gildas_def
  use sic_types
  !--------------------------------------------------------------------
  ! Basic support of SIC interpreter.
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !--------------------------------------------------------------------
  !
  ! Limits for SIC interpreter execution
  integer, parameter :: mcif=255     ! Maximum nesting of IF blocks
  integer, parameter :: maxlev=127   ! Maximum nesting of levels
  integer, parameter :: mcloop=127   ! Maximum nesting of FOR loops
  integer, parameter :: maxmacarg=32 ! Maximum number of macro arguments
  ! CAUTION: look at code in getcom.f if the above number no longer fits in one digit
  integer, parameter :: maxext=32    ! Maximum number of default extensions,
                                     ! should be related to gpack_max_count
  !
  ! Macros support
  character(len=12) :: sicext(maxext)             ! Macro default extension
  integer :: lext(maxext)                         ! Macro default extension length
  ! Commands support
  type(sic_command_t), save :: ccomm           ! Current command line
  !
  ! Program status
  logical, save :: lverif          ! Verify ON
  integer :: mlire(maxlev)         ! Read status for each level
  !       -2 = Read in loop buffers
  !       -1 = Read from program
  !        0 = Read on unit 5
  !       +N = Read in Nth active macro
  integer :: nlire                 ! Level depth
  integer :: nmacro                ! Number of active macros
  integer :: jmac(maxlev)          ! Current line in each macro
  logical :: library_mode          ! Mode is library only
  logical :: sic_quiet             ! SIC is not active (in IF blocks)
  integer, save :: sic_stepin=0    ! Macro Step by Step (for Debug)
  !
  ! Error handling
  character(len=128) :: errcom(0:maxlev) ! Buffer for error recovery command
                                         ! Length of ERRCOM if > 0, or error code:
  integer :: nerr(0:maxlev)              !
  integer, parameter :: e_pause=0        !  0 PAUSE
  integer, parameter :: e_cont=-1        ! -1 CONTINUE
  integer, parameter :: e_quit=-2        ! -2 QUIT
  integer, parameter :: e_retu=-3        ! -3 RETURN
  logical, save :: sicvar_error=.false.  ! Support for variable SIC%ERROR
  !
  ! IF blocks support
  integer :: if_depth(maxlev)            !
  integer :: if_loop_level(mcloop)       !
  integer :: if_current                  !
  integer :: if_last                     !
  logical :: if_active(mcif)             !
  logical :: if_elsefound(mcif)          ! Standalone ELSE already found once?
  logical :: if_finished(mcif)           !
  !
  ! FOR loops support
  integer, parameter :: mloop=10         ! Number of sub-loops in a FOR command
  integer, parameter :: looend=1024*20   ! 80 kBytes max size for loop contents
  integer(kind=address_length) :: loobuf(looend) ! Loop buffer
  integer :: loolen                      ! Current length of buffer
  integer :: bulend                      ! Number of commands in buffer
  integer :: mlen                        !
  integer :: firllo(mcloop)              ! Pointer to first line of loop
  integer :: firblo(mcloop)              ! Pointer to first byte of loop
  integer :: lasllo(mcloop)              ! Pointer to last line of loop
  integer :: lasblo(mcloop)              ! Pointer to last byte of loop
  integer :: ploop(mcloop)               ! Previous loop number
  integer :: ifloop(maxlev)              ! Number of loops at this level
  integer :: curbyt(maxlev)              ! Current byte at this level
  integer :: curlin(maxlev)              ! Current line at this level
  integer :: iloo                        ! First command to be considered
  integer :: jloo                        ! Current command played
  integer :: kloo(mcloop)                ! Current index list used
  integer :: nloo                        ! Byte pointer for next command
  logical :: compil                      ! Compilation flag
  !
  integer :: cloop                       ! Current loop number
  integer :: aloop                       ! Current number of loops
  integer :: loop_length(mcloop)         ! Length of logical test
  type(sic_listr8_t), save :: loop_index(mcloop)  ! Loop lists
  real(8) :: indice(mcloop)              ! Value of loop index
  !
  integer(kind=4), parameter :: loop_string_length=240  ! 240 = 3*MLOOP*8, but this is by chance...
  character(len=loop_string_length) :: loop_string(mcloop)
  integer(kind=4) :: loop_list(mcloop)    ! Variable number in dictionary for /IN List
  integer(kind=4) :: loop_size(mcloop)    ! Dimension of /IN List
  integer(kind=4) :: loop_curarg(mcloop)  ! Current list counter for /IN List
  integer(kind=4) :: loop_var(mcloop)     ! Identifier of the loop variable
  !
  ! Procedures
  integer(kind=4), parameter :: maxpro=512
  integer(kind=4), parameter :: procname_length=64
  !
  character(len=procname_length) :: proc_name(maxpro)  ! Procedure name
  character(len=filename_length) :: proc_file(maxpro)  ! Associated file name
  character(len=filename_length) :: gag_proc           ! GAG_PROC: value
  integer :: iproc                                     ! Current procedure
  integer :: nproc                                     ! Number of procedures
  logical :: proced                                    !
  !
  ! Logical units used by SIC
  integer, parameter :: mlun=40   ! MLUN   Max number of logical units allocatable by SIC
  integer, parameter :: mlunpa=8  ! MLUNPA Number of pre-allocated logical units for macro by SIC_LUNMAC_INIT (Range 0 ... MLUN)
  integer :: lunlog=0             ! LUNLOG Logical unit for the log_file (permanent connection)
  integer :: luntem               ! LUNTEM Logical unit for temporary connection to files
  integer :: lunmac(mlun)         ! LUNMAC Array of logical units for macros.
end module sic_structures
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_expressions
  !--------------------------------------------------------------------
  ! Support for interpretation of functions or formulas
  !--------------------------------------------------------------------
  !
  ! User-defined functions (DEFINE FUNCTION)
  integer, parameter :: mfun=128    ! Maximum number of functions
  integer, parameter :: mfunarg=8   ! Maximum number of arguments
  integer, parameter :: mpoi=20     ! Maximum number of pointers
  integer :: nfun                   ! Number of functions
  integer :: nfunarg(mfun)          ! Number of arguments per fonction
  integer :: lonc(mfun)             ! Length of function name
  integer :: lptr(mfun)             ! Number of pointers for function
  integer :: ifi(mpoi,mfun)         ! Pointer to beg. in definition
  integer :: ila(mpoi,mfun)         ! Pointer to end  in definition
  integer :: iva(mpoi,mfun)         ! Argument number
  character(len=24) :: fonc(mfun)   ! Function name
  character(len=80) :: defi(mfun)   ! Function definition
  !
  ! Formulas
  integer, parameter :: maxterm=10          ! Maximum number of terms in block
  integer, parameter :: formula_length=512  ! Maximum length of formula
  integer, parameter :: maxtree=512         ! Length of buffer space
  integer, parameter :: maxpar=32           ! Maximum parenthesis level
  integer, parameter :: maxoper=32          ! Maximum number of operands
  real(8), parameter :: eps=1.d-15          ! Relative accuracy on real(8)
  !
  ! Arguments
  integer, parameter :: margu=100        !
  integer :: next_arg,next_st            !
  real(kind=8)        :: argu_r8(margu)  !
  real(kind=4)        :: argu_r4(margu)  !
  integer(kind=4)     :: argu_i4(margu)  !
  integer(kind=8)     :: argu_i8(margu)  !
  logical             :: argu_l4(margu)  !
  character(len=1024) :: argu_st(margu)  !
end module sic_expressions
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_dictionaries
  use gildas_def
  use sic_structures
  use sic_types
  !--------------------------------------------------------------------
  ! Dictionaries used to store SIC objects or features
  !--------------------------------------------------------------------
  !
  ! Languages
  integer, parameter :: mvocab=2048         ! Limit of 400 is exhausted by CLIC,
                                            ! ... and 500 by CLASS90,
                                            ! ... and 1000 by MOPSIC
  integer, parameter :: mlang=32            ! Limit of 15 is exhausted by MOPSIC
  type(sic_language_t), save :: languages(mlang)  ! The languages
  integer :: nopt(0:mvocab)                 ! Total number of commands and options
  integer :: hasopt(0:mvocab)               ! Does the command accepts options or not?
  integer :: mbuilt(0:mlang) = 0            ! Last command in a given language
  integer :: nlang                          ! Number of languages
  integer, parameter :: mbuilt_sic_inter=23  ! Number of commands in the SIC\
                                             ! language which can alter the normal
                                             ! flow of commands (e.g. @, IF, FOR, ...)
  integer, parameter :: mucom=100           ! Maximum number of commands per user language.
  !
  ! Language priorities
  integer(kind=4), parameter :: mprio=mlang        ! Maximum number of priority levels
  integer(kind=4) :: klang(mprio) = 0              ! Number of languages at each priority level
  integer(kind=4) :: olang(mlang,mprio)            ! Language at each priority level
  integer(kind=4) :: nprio                         ! Current number of priority levels
  !
  character(len=command_length) :: vocab(mvocab) = ' ' ! Array of commands and options
  !
  ! Help (general)
  integer :: lsynt                                         ! SIC syntax
  character(len=filename_length) :: help_text(mvocab)=' '  ! Help files for USER\ commands
  integer :: help_mode                                     ! Help mode
  integer, parameter :: help_scroll=1                      !
  integer, parameter :: help_page=2                        !
  integer, parameter :: help_html=3                        !
  !
  ! Variables
  integer, parameter :: maxvardef=10000   ! Default value for maxvar
  integer :: maxvar                       ! Number of different variable names
  integer :: pfvar(0:27)                  ! Pointers PF(27)=number of symbols
  integer, allocatable :: pnvar(:)        ! Chained pointers
  integer :: var_n                        ! Number of local user defined variables
  integer :: var_g                        ! Number of global user defined variables
  integer :: var_level                    ! Current level for local variables
  integer :: var_macro(maxlev+1)          ! First local variable for level i
  integer, allocatable :: var_pointer(:)  ! Pointer to user defined variables
  integer, allocatable :: alias(:)        ! List of variables which are aliases
  integer, allocatable :: pointee(:)      ! List of variables which are aliased
  integer :: nalias                       ! Number of aliases
  integer :: lsavcom                      ! Length of SAVCOM command
  integer :: lunsav                       ! LUN for saving variables...
  type(sic_variable_t), allocatable :: dicvar(:)
  character(len=varname_length) :: savcom
  type(sic_identifier_t), allocatable :: dicali(:)
  !
  ! Symbols
  integer(kind=4), parameter :: maxsym=400  ! Max number of symbols
  type(sic_symdict_t), save :: symbols      ! The dictionary of SIC symbols
  !
  ! --- Program-defined functions (i.e. added by the programs) ---------
  integer(kind=4), parameter :: maxfun=53  ! Maximum number of defined functions
  integer(kind=4) :: pffun(0:27)           ! Alphabetic pointers PF(27)=number of symbols
  integer(kind=4) :: pnfun(maxfun)         ! Chained pointers
  character(len=16) :: namfun(maxfun)      ! Function names
  type(sic_function_t) :: descfun(maxfun)  ! Function descriptions
  integer(kind=4) :: code_user             ! Next available function code
  !
  ! --- Intrinsic functions (i.e. native support in SIC) ---------------
  integer(kind=address_length) :: addr_function(300)  ! Function address
  !
  ! Special cases
  integer, parameter :: code_nop      = 0    ! Null operator
  integer, parameter :: code_coma     = 1    ! Coma
  !
  ! Logical operators
  integer, parameter :: code_or       = 2    ! OR operator
  integer, parameter :: code_and      = 3    ! AND operator
  integer, parameter :: code_not      = 4    ! NOT operator
  !
  ! Relational operators
  integer, parameter :: code_ne       = 5    ! Not equal
  integer, parameter :: code_eq       = 6    ! Equal
  integer, parameter :: code_le       = 7    ! Less or equal than
  integer, parameter :: code_lt       = 8    ! Less than
  integer, parameter :: code_ge       = 9    ! Greater or equal than
  integer, parameter :: code_gt       = 10   ! Greater than
  !
  ! Codes for arithmetic operators
  integer, parameter :: code_bplus    = 11   ! Binary plus
  integer, parameter :: code_bminus   = 12   ! Binary minus
  integer, parameter :: code_mul      = 13   ! Multiply
  integer, parameter :: code_div      = 14   ! Divide
  integer, parameter :: code_uplus    = 15   ! Unary plus
  integer, parameter :: code_uminus   = 16   ! Unary minus
  integer, parameter :: code_power    = 17   ! Exponentiation
  !
  ! Codes for intrinsic one operator functions
  integer, parameter :: min_code_func  = 21   ! Minimum code for a function
  integer, parameter :: code_abs       = 21   ! Absolute value
  integer, parameter :: code_acos      = 22   ! Arc cosine
  integer, parameter :: code_asin      = 23   ! Arc sine
  integer, parameter :: code_atan      = 24   ! Arc tangent
  integer, parameter :: code_atanh     = 25   ! Hyperbolic arc tangent
  integer, parameter :: code_bessel_i0 = 26   ! Modified Bessel function of the first kind of order zero
  integer, parameter :: code_bessel_i1 = 27   ! Modified Bessel function of the first kind of order one
  integer, parameter :: code_bessel_j0 = 28   ! Bessel function of the first kind of order zero
  integer, parameter :: code_bessel_j1 = 29   ! Bessel function of the first kind of order one
  integer, parameter :: code_bessel_y0 = 30   ! Bessel function of the second kind of order zero
  integer, parameter :: code_bessel_y1 = 31   ! Bessel function of the second kind of order one
  integer, parameter :: code_cos       = 32   ! Cosine
  integer, parameter :: code_cosh      = 33   ! Hyperbolic cosine
  integer, parameter :: code_erf       = 34   ! Error function
  integer, parameter :: code_erfc      = 35   ! Complementary error function
  integer, parameter :: code_erfinv    = 36   ! Inverse error function
  integer, parameter :: code_erfcinv   = 37   ! Inverse complementary error function
  integer, parameter :: code_exp       = 38   ! Exponential
  integer, parameter :: code_int       = 39   ! Truncation
  integer, parameter :: code_log       = 40   ! Natural logarithm
  integer, parameter :: code_log10     = 41   ! Decimal logarithm
  integer, parameter :: code_nint      = 42   ! Rounding
  integer, parameter :: code_sin       = 43   ! Sine
  integer, parameter :: code_sinh      = 44   ! Hyperbolic sine
  integer, parameter :: code_sqrt      = 45   ! Square root
  integer, parameter :: code_tan       = 46   ! Tangent
  integer, parameter :: code_tanh      = 47   ! Hyperbolic tangent
  integer, parameter :: code_floor     = 48   ! Round to floor
  integer, parameter :: code_ceiling   = 49   ! Round to ceiling
  !
  ! Intrinsic function with 2 arguments or more
  integer, parameter :: code_max       = 50   ! 2 argument maximum
  integer, parameter :: code_min       = 51   ! 2 argument minimum
  integer, parameter :: code_atan2     = 52   ! 2 argument arc tangent
  integer, parameter :: code_bessel_in = 53   ! Modified Bessel function of the first kind of order N
  integer, parameter :: code_bessel_jn = 54   ! Bessel function of the first kind of order N
  integer, parameter :: code_bessel_yn = 55   ! Bessel function of the second kind of order N
  integer, parameter :: code_dim       = 56   ! Positive difference (a-min(a,b))
  integer, parameter :: code_mod       = 57   ! Remainder
  integer, parameter :: code_sign      = 58   ! Transfer of sign
! integer, parameter :: code_conc      =      ! Concatenation (not yet implemented!)
  !
  ! Intrinsic function with a name as argument
  integer, parameter :: code_exist    = 59   ! Exist(NAME)
  integer, parameter :: code_file     = 60   ! File(NAME)
  integer, parameter :: code_typeof   = 61   ! Typeof(NAME)
  integer, parameter :: code_rank     = 62   ! Rank(NAME)
  integer, parameter :: code_size     = 63   ! Size(NAME,DIM)
  integer, parameter :: code_index    = 64   ! Index(STRING,SUBSTRING)
  integer, parameter :: code_function = 65   ! Function(NAME) (is NAME a function ?)
  integer, parameter :: code_len      = 66   ! Length of character string
  integer, parameter :: code_len_trim = 67   ! Length of trimmed character string
  integer, parameter :: code_all      = 68   ! All(LogicalArray)
  integer, parameter :: code_any      = 69   ! Any(LogicalArray)
  integer, parameter :: code_symbol   = 70   ! Symbol(NAME)
  integer, parameter :: code_isnan    = 71   ! IsNaN(NAME)
  integer, parameter :: code_isnum    = 72   ! IsNUM(String)
  integer, parameter :: max_code_func = 72   ! Maximum code for a function
  !
  ! Python Generic function
  integer, parameter :: code_pyfunc   = 73   ! Function in python namespace
  !
  ! Start Scalar functions. Is this useful?
  integer, parameter :: code_scalar   = 73   ! Vector defined function end here
  !
  ! Keep list of intrinsic functions, useful only for HELP FUNCTION (for now)
  integer(kind=4), parameter :: mintfunc=max_code_func-min_code_func+1
  character(len=16), parameter :: name_function(mintfunc) = (/  &
    'ABS             ','ACOS            ','ALL             ','ANY             ',  &
    'ASIN            ','ATAN            ','ATANH           ','ATAN2           ',  &
    'BESSEL_I0       ','BESSEL_I1       ','BESSEL_IN       ','BESSEL_J0       ',  &
    'BESSEL_J1       ','BESSEL_JN       ','BESSEL_Y0       ','BESSEL_Y1       ',  &
    'BESSEL_YN       ','CEILING         ','COS             ','COSH            ',  &
    'DIM             ','EXIST           ','ERF             ','ERFC            ',  &
    'ERFINV          ','ERFCINV         ','EXP             ','FILE            ',  &
    'FLOOR           ','FUNCTION        ','INDEX           ','INT             ',  &
    'ISNAN           ','ISNUM           ','LEN             ','LEN_TRIM        ',  &
    'LOG             ','LOG10           ','MAX             ','MIN             ',  &
    'MOD             ','NINT            ','RANK            ','SIGN            ',  &
    'SIN             ','SINH            ','SIZE            ','SQRT            ',  &
    'SYMBOL          ','TAN             ','TANH            ','TYPEOF          ' /)
  !
end module sic_dictionaries
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
