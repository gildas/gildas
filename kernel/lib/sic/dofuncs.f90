!*********************************************************************
! REAL*4 Generic External three argument function on vectors
!
subroutine do_real_loop3 (func,n,result,m1,oper1,m2,oper2,m3,oper3,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),              external      :: func       !
  integer(kind=size_length)                :: n          !
  real(kind=4)                             :: result(n)  !
  integer(kind=size_length)                :: m1         !
  real(kind=4)                             :: oper1(m1)  !
  integer(kind=size_length)                :: m2         !
  real(kind=4)                             :: oper2(m2)  !
  integer(kind=size_length)                :: m3         !
  real(kind=4)                             :: oper3(m3)  !
  logical,                   intent(inout) :: error      !
  ! Local
  real(kind=4) :: r,a,b,c
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1 .and. m2.eq.1 .and. m3.eq.1) then
    r = func(oper1(1),oper2(1),oper3(1))
    do i=1,n
      result(i) = r
    enddo
  elseif (m1.eq.n .and. m2.eq.1 .and. m3.eq.1) then
    b = oper2(1)
    c = oper3(1)
    do i=1,n
      result(i) = func(oper1(i),b,c)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n .and. m3.eq.1) then
    a = oper1(1)
    c = oper3(1)
    do i=1,n
      result(i) = func(a,oper2(i),c)
    enddo
  elseif (m1.eq.n .and. m2.eq.n .and. m3.eq.1) then
    c = oper3(1)
    do i=1,n
      result(i) = func(oper1(i),oper2(i),c)
    enddo
  elseif (m1.eq.1 .and. m2.eq.1 .and. m3.eq.n) then
    a = oper1(1)
    b = oper2(1)
    do i=1,n
      result(i) = func(a,b,oper3(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.1 .and. m3.eq.n) then
    b = oper2(1)
    do i=1,n
      result(i) = func(oper1(i),b,oper3(i))
    enddo
  elseif (m1.eq.1 .and. m2.eq.n .and. m3.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = func(a,oper2(i),oper3(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n .and. m3.eq.n) then
    do i=1,n
      result(i) = func(oper1(i),oper2(i),oper3(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2,m3
    call sic_message(seve%e,'S_FUNC',mess)
    error = .true.
  endif
end subroutine do_real_loop3
!
!*********************************************************************
! REAL*4 Generic External two argument function on vectors
!
subroutine do_real_loop2 (func,n,result,m1,oper1,m2,oper2,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4),              external      :: func       !
  integer(kind=size_length)                :: n          !
  real(kind=4)                             :: result(n)  !
  integer(kind=size_length)                :: m1         !
  real(kind=4)                             :: oper1(m1)  !
  integer(kind=size_length)                :: m2         !
  real(kind=4)                             :: oper2(m2)  !
  logical,                   intent(inout) :: error      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1 .and. m2.eq.1) then
    a = func(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = func(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = func(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = func(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_FUNC',mess)
    error = .true.
  endif
end subroutine do_real_loop2
!
!----------------------------------------------------------------------
! REAL*4 Two operands arithmetic operations on vectors
!
function lsic_s_bplus (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bplus         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_bplus = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)+oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)+a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a+oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)+oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_BPLUS',mess)
    lsic_s_bplus = 1
  endif
end function lsic_s_bplus
!
function lsic_s_bminus (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bminus        !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_bminus = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)-oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)-a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a-oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)-oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_BMINUS',mess)
    lsic_s_bminus = 1
  endif
end function lsic_s_bminus
!
function lsic_s_mul (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_mul           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_mul = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)*oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)*a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a*oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)*oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_MUL',mess)
    lsic_s_mul = 1
  endif
end function lsic_s_mul
!
function lsic_s_div (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_div           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Global
  include 'gbl_nan.inc'
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_div = 0
  if (m1.eq.1 .and. m2.eq.1) then
    if (oper2(1).ne.0) then
      a =  oper1(1)/oper2(1)
    elseif  (oper1(1).gt.0) then
      a = s_pinf
    elseif  (oper1(1).lt.0) then
      a = s_minf
    else
      a = s_nan
    endif
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      if (a.ne.0) then
        result(i) = oper1(i)/a
      elseif  (oper1(i).gt.0) then
        result(i) = s_pinf
      elseif  (oper1(i).lt.0) then
        result(i) = s_minf
      else
        result(i) = s_nan
      endif
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      if (oper2(i).ne.0) then
        result(i) = a/oper2(i)
      elseif  (a.gt.0) then
        result(i) = s_pinf
      elseif  (a.lt.0) then
        result(i) = s_minf
      else
        result(i) = s_nan
      endif
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      if (oper2(i).ne.0) then
        result(i) = oper1(i)/oper2(i)
      elseif  (oper1(i).gt.0) then
        result(i) = s_pinf
      elseif  (oper1(i).lt.0) then
        result(i) = s_minf
      else
        result(i) = s_nan
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_DIV',mess)
    lsic_s_div = 1
  endif
end function lsic_s_div
!
function lsic_s_power (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_power         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  integer :: p
  character(len=message_length) :: mess
  !
  lsic_s_power = 0
  if (m1.eq.1 .and. m2.eq.1) then
    p = nint(oper2(1))
    if (abs(float(p)-oper2(1)).le.1.0e-7*abs(float(p))) then
      a = oper1(1)**p
    else
      a = oper1(1)**oper2(1)
    endif
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    p = nint(a)
    if (abs(float(p)-a).le.1.0e-7*abs(float(p))) then
      if (p.eq.2) then
        do i=1,n
          result(i) = oper1(i)*oper1(i)
        enddo
      else
        do i=1,n
          result(i) = oper1(i)**p
        enddo
      endif
    else
      do i=1,n
        result(i) = oper1(i)**a
      enddo
    endif
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      p = nint(oper2(i))
      if (abs(float(p)-oper2(i)).le.1.0e-7*abs(float(p))) then
        result(i) = a**p
      else
        result(i) = a**oper2(i)
      endif
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      p = nint(oper2(i))
      if (abs(float(p)-oper2(i)).le.1.0e-7*abs(float(p))) then
        result(i) = oper1(i)**p
      else
        result(i) = oper1(i)**oper2(i)
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_POWER',mess)
    lsic_s_power = 1
  endif
end function lsic_s_power
!
!----------------------------------------------------------------------
! REAL*4 Two argument functions on vectors
!
function lsic_s_min (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_min           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_min = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = min(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = min(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = min(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = min(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_MIN',mess)
    lsic_s_min = 1
  endif
end function lsic_s_min
!
function lsic_s_max (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_max           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_max = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = max(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = max(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = max(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = max(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_MAX',mess)
    lsic_s_max = 1
  endif
end function lsic_s_max
!
function lsic_s_atan2 (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_atan2         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_atan2 = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = atan2(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = atan2(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = atan2(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = atan2(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_ATAN2',mess)
    lsic_s_atan2 = 1
  endif
end function lsic_s_atan2
!
function lsic_s_dim (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_dim           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_dim = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = dim(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = dim(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = dim(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = dim(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_DIM',mess)
    lsic_s_dim = 1
  endif
end function lsic_s_dim
!
function lsic_s_sign (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_sign          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_sign = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = sign(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = sign(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = sign(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = sign(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_SIGN',mess)
    lsic_s_sign = 1
  endif
end function lsic_s_sign
!
function lsic_s_mod (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_mod           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_mod = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = mod(oper1(1),oper2(1))
    if (a.lt.0.0) a = a+oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = mod(oper1(i),a)
      if (result(i).lt.0.0) result(i) = result(i)+a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = mod(a,oper2(i))
      if (result(i).lt.0.0) result(i) = result(i)+oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = mod(oper1(i),oper2(i))
      if (result(i).lt.0.0) result(i) = result(i)+oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_MOD',mess)
    lsic_s_mod = 1
  endif
end function lsic_s_mod
!
!----------------------------------------------------------------------
! REAL*4 Generic External single argument function on vector
!
subroutine do_real_loop1(func,n,result,m1,oper1,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  real(kind=4) ,             external      :: func       !
  integer(kind=size_length)                :: n          !
  real(kind=4)                             :: result(n)  !
  integer(kind=size_length)                :: m1         !
  real(kind=4)                             :: oper1(m1)  !
  logical,                   intent(inout) :: error      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1) then
    a = func(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = func(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_FUNC',mess)
    error = .true.
  endif
end subroutine do_real_loop1
!
!----------------------------------------------------------------------
! REAL*4 Single argument functions on vectors
!
function lsic_s_abs (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_abs           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_abs = 0
  if (m1.eq.1) then
    a = abs(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = abs(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ABS',mess)
    lsic_s_abs = 1
  endif
end function lsic_s_abs
!
function lsic_s_acos (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_acos          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Global
  include 'gbl_nan.inc'
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_acos = 0
  if (m1.eq.1) then
    a = acos(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      if (oper1(i).ge.-1.0 .and. oper1(i).le.1.0) then
        result(i) = acos(oper1(i))
      else
        result(i) = s_nan
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ACOS',mess)
    lsic_s_acos = 1
  endif
end function lsic_s_acos
!
function lsic_s_asin (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_asin          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Global
  include 'gbl_nan.inc'
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_asin = 0
  if (m1.eq.1) then
    a = asin(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      if (oper1(i).ge.-1.0 .and. oper1(i).le.1.0) then
        result(i) = asin(oper1(i))
      else
        result(i) = s_nan
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ASIN',mess)
    lsic_s_asin = 1
  endif
end function lsic_s_asin
!
function lsic_s_atan (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_atan          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_atan = 0
  if (m1.eq.1) then
    a = atan(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = atan(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ATAN',mess)
    lsic_s_atan = 1
  endif
end function lsic_s_atan
!
function lsic_s_cos (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_cos           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_cos = 0
  if (m1.eq.1) then
    a = cos(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = cos(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_COS',mess)
    lsic_s_cos = 1
  endif
end function lsic_s_cos
!
function lsic_s_atanh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_atanh         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_atanh = 0
  if (m1.eq.1) then
    a = atanh(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = atanh(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ATANH',mess)
    lsic_s_atanh = 1
  endif
end function lsic_s_atanh
!
function lsic_s_cosh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_cosh          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_cosh = 0
  if (m1.eq.1) then
    a = cosh(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = cosh(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_COSH',mess)
    lsic_s_cosh = 1
  endif
end function lsic_s_cosh
!
function lsic_s_exp (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_exp           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_exp = 0
  if (m1.eq.1) then
    a = exp(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = exp(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_EXP',mess)
    lsic_s_exp = 1
  endif
end function lsic_s_exp
!
function lsic_s_int (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_int           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_int = 0
  if (m1.eq.1) then
    a = aint(oper1(1))
    if (a.gt.oper1(1)) a = a-1.0
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = aint(oper1(i))
      if (result(i).gt.oper1(i)) result(i) = result(i)-1.0
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_INT',mess)
    lsic_s_int = 1
  endif
end function lsic_s_int
!
function lsic_s_log (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_log           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_log = 0
  if (m1.eq.1) then
    a = log(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = log(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_LOG',mess)
    lsic_s_log = 1
  endif
end function lsic_s_log
!
function lsic_s_log10 (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_log10         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_log10 = 0
  if (m1.eq.1) then
    a = log10(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = log10(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_LOG10',mess)
    lsic_s_log10 = 1
  endif
end function lsic_s_log10
!
function lsic_s_nint (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_nint          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_nint = 0
  if (m1.eq.1) then
    a = anint(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = anint(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_NINT',mess)
    lsic_s_nint = 1
  endif
end function lsic_s_nint
!
function lsic_s_sin (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_sin           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4)  :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_sin = 0
  if (m1.eq.1) then
    a = sin(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = sin(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_SIN',mess)
    lsic_s_sin = 1
  endif
end function lsic_s_sin
!
function lsic_s_sinh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_sinh          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_sinh = 0
  if (m1.eq.1) then
    a = sinh(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = sinh(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_SINH',mess)
    lsic_s_sinh = 1
  endif
end function lsic_s_sinh
!
function lsic_s_sqrt (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_sqrt          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Global
  include 'gbl_nan.inc'
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_sqrt = 0
  if (m1.eq.1) then
    a = sqrt(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      if (oper1(i).ge.0.0) then
        result(i) = sqrt(oper1(i))
      else
        result(i) = s_nan
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_SQRT',mess)
    lsic_s_sqrt = 1
  endif
end function lsic_s_sqrt
!
function lsic_s_tan (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_tan           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_tan = 0
  if (m1.eq.1) then
    a = tan(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = tan(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_TAN',mess)
    lsic_s_tan = 1
  endif
end function lsic_s_tan
!
function lsic_s_tanh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_tanh          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4)  :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_tanh = 0
  if (m1.eq.1) then
    a = tanh(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = tanh(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_TANH',mess)
    lsic_s_tanh = 1
  endif
end function lsic_s_tanh
!
function lsic_s_floor (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_floor         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4)  :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_floor = 0
  if (m1.eq.1) then
    a = real(floor(oper1(1)),kind=4)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = real(floor(oper1(i)),kind=4)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_FLOOR',mess)
    lsic_s_floor = 1
  endif
end function lsic_s_floor
!
function lsic_s_ceiling (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_ceiling       !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4)  :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_ceiling = 0
  if (m1.eq.1) then
    a = real(ceiling(oper1(1)),kind=4)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = real(ceiling(oper1(i)),kind=4)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_CEILING',mess)
    lsic_s_ceiling = 1
  endif
end function lsic_s_ceiling
!
function lsic_s_uminus (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_uminus        !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_uminus = 0
  if (m1.eq.1) then
    a = -oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = -oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_UMINUS',mess)
    lsic_s_uminus = 1
  endif
end function lsic_s_uminus
!
function lsic_s_uplus (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_uplus         !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_uplus = 0
  if (m1.eq.1) then
    a = oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_PLUS',mess)
    lsic_s_uplus = 1
  endif
end function lsic_s_uplus
!
function lsic_s_pyfunc (noper,nelem,nwords,n,result)
  use gildas_def
  use gbl_message
  use sic_interfaces, except_this=>lsic_s_pyfunc
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Generic PYTHON function with variable number of arguments
  !        Called by DO_VECTOR
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_pyfunc  ! Function value on return
  integer(kind=4),              intent(in)  :: noper          ! Number of operands
  integer(kind=size_length),    intent(in)  :: nelem(noper)   ! # of elements for each operand
  integer(kind=address_length), intent(in)  :: nwords(noper)  ! Memory location for each operand
  integer(kind=size_length),    intent(in)  :: n              ! Size of result
  real(kind=4),                 intent(out) :: result(n)      ! Result
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=4) :: j
  integer(kind=size_length) :: m
  ! Note on 'ielem': it should be 'size_length', but C functions have no such
  ! equivalent: use 'address_length' (Fortran) == 'size_t' (C), with the limit
  ! that under 32 bits architectures, 'size_length' can be 8 but
  ! 'address_length' 4.
  integer(kind=address_length) :: ielem
  integer(kind=4) :: strides(noper)
  real(kind=4) :: value
  logical :: hasarray
  !
  ! Check dimensions and construct strides
  lsic_s_pyfunc = 1
#if defined(GAG_USE_PYTHON)
  hasarray = .false.
  do j=1,noper
    m = nelem(j)
    if (m.eq.1) then
      strides(j) = 0
    elseif (m.eq.n) then
      strides(j) = 1
      hasarray=.true.
    else
      call sic_message(seve%e,'S_PYFUNC','Inconsistent dimensions')
      lsic_s_pyfunc = 1
      return
    endif
  enddo
  !
  if (.not.hasarray) then
    ielem = 1
    lsic_s_pyfunc = gpy_callfuncs(noper,ielem,strides,nwords,memory,value)
    if (lsic_s_pyfunc.ne.0) return
    do ielem=1,n
      result(ielem) = value
    enddo
  else
    do ielem=1,n
      lsic_s_pyfunc = gpy_callfuncs(noper,ielem,strides,nwords,memory,value)
      if (lsic_s_pyfunc.ne.0) return
      result(ielem) = value
    enddo
  endif
#else
  call sic_message(seve%e,'PYFUNC','Python not available')
#endif
end function lsic_s_pyfunc
!
function lsic_s_erf(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_erf           !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_erf = 0
  if (m1.eq.1) then
    a = erf(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = erf(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ERF',mess)
    lsic_s_erf = 1
  endif
end function lsic_s_erf
!
function lsic_s_erfc(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_erfc          !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_erfc = 0
  if (m1.eq.1) then
    a = erfc(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = erfc(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ERFC',mess)
    lsic_s_erfc = 1
  endif
end function lsic_s_erfc
!
function lsic_s_erfinv(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_erfinv        !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=8):: x
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_erfinv = 0
  if (m1.eq.1) then
    x = real(oper1(1),kind=8)
    a = gag_erfinv(x)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      x = real(oper1(i),kind=8)
      result(i) = gag_erfinv(x)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ERFINV',mess)
    lsic_s_erfinv = 1
  endif
end function lsic_s_erfinv
!
function lsic_s_erfcinv(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_erfcinv       !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=8) :: x
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_erfcinv = 0
  if (m1.eq.1) then
    x = real(oper1(1),kind=8)
    a = gag_erfcinv(x)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      x = real(oper1(i),kind=8)
      result(i) = gag_erfcinv(x)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_ERFCINV',mess)
    lsic_s_erfcinv = 1
  endif
end function lsic_s_erfcinv
!
function lsic_s_bessel_i0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_i0     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  real(kind=8) :: x
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_bessel_i0 = 0
  if (m1.eq.1) then
    x = oper1(1)
    a = gag_bessel_i0(x)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      x = oper1(i)
      result(i) = gag_bessel_i0(x)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_I0',mess)
    lsic_s_bessel_i0 = 1
  endif
end function lsic_s_bessel_i0
!
function lsic_s_bessel_i1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_i1     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  real(kind=8) :: x
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_bessel_i1 = 0
  if (m1.eq.1) then
    x = oper1(1)
    a = gag_bessel_i1(x)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      x = oper1(i)
      result(i) = gag_bessel_i1(x)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_I1',mess)
    lsic_s_bessel_i1 = 1
  endif
end function lsic_s_bessel_i1
!
function lsic_s_bessel_in(n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_in     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  real(kind=8) :: x
  integer(kind=size_length) :: i
  integer(kind=4) :: o
  character(len=message_length) :: mess
  !
  lsic_s_bessel_in = 0
  if (m1.eq.1 .and. m2.eq.1) then
    o = nint(oper1(1))
    x = dble(oper2(1))
    a = gag_bessel_in(o,x)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    x = oper2(1)
    do i=1,n
      o = nint(oper1(i))
      result(i) = gag_bessel_in(o,x)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    o = nint(oper1(1))
    do i=1,n
      x = oper2(i)
      result(i) = gag_bessel_in(o,x)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      o = nint(oper1(i))
      x = oper2(i)
      result(i) = gag_bessel_in(o,x)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_BESSEL_IN',mess)
    lsic_s_bessel_in = 1
  endif
end function lsic_s_bessel_in
!
function lsic_s_bessel_j0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_j0     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_j0 = 0
  if (m1.eq.1) then
    a = bessel_j0(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = bessel_j0(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_J0',mess)
    lsic_s_bessel_j0 = 1
  endif
#endif
end function lsic_s_bessel_j0
!
function lsic_s_bessel_j1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_j1     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_j1 = 0
  if (m1.eq.1) then
    a = bessel_j1(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = bessel_j1(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_J1',mess)
    lsic_s_bessel_j1 = 1
  endif
#endif
end function lsic_s_bessel_j1
!
function lsic_s_bessel_jn(n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_jn     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  integer(kind=4) :: o
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_jn = 0
  if (m1.eq.1 .and. m2.eq.1) then
    o = nint(oper1(1))
    a = bessel_jn(o,oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      o = nint(oper1(i))
      result(i) = bessel_jn(o,a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    o = nint(oper1(1))
    do i=1,n
      result(i) = bessel_jn(o,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      o = nint(oper1(i))
      result(i) = bessel_jn(o,oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_BESSEL_JN',mess)
    lsic_s_bessel_jn = 1
  endif
#endif
end function lsic_s_bessel_jn
!
function lsic_s_bessel_y0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_y0     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_y0 = 0
  if (m1.eq.1) then
    a = bessel_y0(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = bessel_y0(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_Y0',mess)
    lsic_s_bessel_y0 = 1
  endif
#endif
end function lsic_s_bessel_y0
!
function lsic_s_bessel_y1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_y1     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: k          !
  integer                   :: dummy      !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_y1 = 0
  if (m1.eq.1) then
    a = bessel_y1(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = bessel_y1(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'S_BESSEL_Y1',mess)
    lsic_s_bessel_y1 = 1
  endif
#endif
end function lsic_s_bessel_y1
!
function lsic_s_bessel_yn(n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_bessel_yn     !
  integer(kind=size_length) :: n          !
  real(kind=4)              :: result(n)  !
  integer(kind=size_length) :: m1         !
  real(kind=4)              :: oper1(m1)  !
  integer(kind=size_length) :: m2         !
  real(kind=4)              :: oper2(m2)  !
  ! Local
  real(kind=4) :: a
  integer(kind=size_length) :: i
  integer(kind=4) :: o
  character(len=message_length) :: mess
  !
#if defined(IFORT_VERSION) && IFORT_VERSION < 140
  call sic_message(seve%e,'S_BESSEL_J0','Not available with ifort version older than 14.0')
  lsic_s_bessel_j0 = 1
#else
  lsic_s_bessel_yn = 0
  if (m1.eq.1 .and. m2.eq.1) then
    o = nint(oper1(1))
    a = bessel_yn(o,oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      o = nint(oper1(i))
      result(i) = bessel_yn(o,a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    o = nint(oper1(1))
    do i=1,n
      result(i) = bessel_yn(o,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      o = nint(oper1(i))
      result(i) = bessel_yn(o,oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_BESSEL_YN',mess)
    lsic_s_bessel_yn = 1
  endif
#endif
end function lsic_s_bessel_yn
