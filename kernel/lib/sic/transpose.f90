subroutine sic_transpose(line,error)
  use sic_dependencies_interfaces
  use gildas_def
  use sic_interfaces, except_this=>sic_transpose
  use sic_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GILDAS Transpose a cube
  !  The algorithm is stupid, but it should not matter on relatively
  ! small cubes
  !---------------------------------------------------------------------
  character(len=*)               :: line   !
  logical,         intent(inout) :: error  !
  ! Local
  character(len=256) :: namex,namey
  character(len=sic_maxdims) :: code
  integer :: space_gildas,n,ier
  !
  logical :: found
  type(sic_descriptor_t) :: descin,descout
  !
  call sic_ke (line,0,3,code,n,.true.,error)
  if (error) return
  call sic_ch (line,0,2,namex,n,.true.,error)
  if (error) return
  call sic_ch (line,0,1,namey,n,.true.,error)
  if (error) return
  !
  ! Check if output (X) is a Variable or a File
  found = .true.
  call sic_descriptor(namey,descin,found)  ! Checked
  if (found) then
    call sic_descriptor(namex,descout,found)   ! Checked
    if (.not.found) then
      call sic_message(seve%e,'TRANSPOSE','Input and Output are not variables')
      error = .true.
      return
    endif
    !
    ! Transpose Variables
    call var_transpose(descin,descout,code,error,namey,namex)
  else
    !
    ! Transpose Files
    space_gildas = 256
    ier = sic_getlog('SPACE_GILDAS',space_gildas)
    call gdf_transpose(namex,namey,code,space_gildas,error)
  endif
end subroutine sic_transpose
!
subroutine get_dims(desc,dims)
  use sic_interfaces, except_this=>get_dims
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(sic_descriptor_t)     :: desc  !
  integer(kind=index_length) :: dims(sic_maxdims)  !
  ! Local
  integer :: i
  !
  do i=1,sic_maxdims
    dims(i) = desc%dims(i)
    dims(i) = max(1,dims(i))
  enddo
  !
end subroutine get_dims
!
subroutine var_transpose(descin,descout,code,error,namei,nameo)
  use gildas_def
  use image_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces, no_interface1=>trans4all,  &
                                   no_interface2=>trans8all
  use sic_interfaces, except_this=>var_transpose
  use sic_types
  use sic_dictionaries
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(sic_descriptor_t)               :: descin   !
  type(sic_descriptor_t)               :: descout  !
  character(len=*),      intent(in)    :: code     !
  logical,               intent(inout) :: error    !
  character(len=*),      intent(in)    :: namei     !
  character(len=*),      intent(in)    :: nameo     !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='TRANSPOSE'
  integer(kind=address_length) :: ipx,ipy
  integer(kind=index_length) :: idim(sic_maxdims),odim(sic_maxdims),iblock(sic_maxdims)
  integer(kind=4) :: itr(sic_maxdims),i,form
  integer(kind=index_length) :: nfirst,nsecon,nmiddl,nlast,nelems
  integer(kind=size_length) :: ntot
  !
  form = descout%type
  if (form.ne.descin%type) then
    call sic_message(seve%e,rname,'Variable type mismatch')
    error = .true.
    return
  endif
  call transpose_getorder(code,itr,sic_maxdims,error)
  if (error) return
  do i=1,descin%ndim
    if (itr(i).eq.0) itr(i) = i
  enddo
  !
  ! Check output variable dimensions
  if (descout%ndim.ne.descin%ndim) then
    error = .true.
  else
    call get_dims(descout,odim)
    call get_dims(descin,idim)
    do i=1,descout%ndim
      if (idim(itr(i)).ne.odim(i)) then
        error = .true.
      endif
    enddo
  endif
  if (error) then
    call sic_message(seve%e,rname,'Dimension mismatch')
    return
  endif
  !
  ! Get the block sizes
  call transpose_getblock(idim,sic_maxdims,code,iblock,error)
  if (error) return
  !
  nelems = iblock(1)
  nfirst = iblock(2)
  nmiddl = iblock(3)
  nsecon = iblock(4)
  nlast  = iblock(5)
  !
  ipy = gag_pointer(descin%addr,memory)
  ipx = gag_pointer(descout%addr,memory)
  if (form.eq.fmt_r4 .or. form.eq.fmt_i4 .or. form.eq.fmt_l) then
    if (nfirst*nmiddl.eq.1 .or. nsecon*nmiddl.eq.1) then
      ntot = 1_8*nelems*nfirst*nmiddl*nsecon*nlast
      call w4tow4_sl(memory(ipy),memory(ipx),ntot)
    else
      call trans4all(memory(ipx),memory(ipy),nelems,nfirst,nmiddl,nsecon,nlast)
    endif
    !
  elseif (form.eq.fmt_r8 .or. form.eq.fmt_i8 .or. form.eq.fmt_c4) then
    if (nfirst*nmiddl.eq.1 .or. nsecon*nmiddl.eq.1) then
      ntot = 1_8*nelems*nfirst*nmiddl*nsecon*nlast
      call w8tow8_sl(memory(ipy),memory(ipx),ntot)
    else
      call trans8all(memory(ipx),memory(ipy),nelems,nfirst,nmiddl,nsecon,nlast)
    endif
    !
  else
    call sic_message(seve%e,rname,'Unsupported data kind')
    error = .true.
    return
  endif
  !
  ! Transpose the Header of Image Variables
  !! Print *,'Desc%Status IN ',descin%status,' OUT ',descout%status
  if (.not.associated(descin%head))  return
  if (.not.associated(descout%head))  return
  !
  call gdf_transpose_header(descin%head,descout%head,code,error)
  if (error)  return
  !
end subroutine var_transpose
!
subroutine tmp_print(iarray,n)
  integer(kind=8) n
  integer iarray(n)
  integer i
  do i =1,n
    print *,'I ',i,' Value ',iarray(i)
  enddo
end subroutine tmp_print
