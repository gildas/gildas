
#ifndef _SIC_GETCAR_H_
#define _SIC_GETCAR_H_

/**
 * @file
 * @see getcar.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/cfc.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

#define sic_c_do_exit CFC_EXPORT_NAME( sic_c_do_exit)
#define sic_c_on_exit CFC_EXPORT_NAME( sic_c_on_exit)
#define get_com_params CFC_EXPORT_NAME( get_com_params)
#define init_gui CFC_EXPORT_NAME( init_gui)
#define prompt_loop CFC_EXPORT_NAME( prompt_loop)
#define search_clients CFC_EXPORT_NAME( search_clients)
#define trap_sigus CFC_EXPORT_NAME( trap_sigus)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void CFC_API sic_c_do_exit( int *status);
void CFC_API sic_c_on_exit( );
void CFC_API get_com_params( int *keyboard, int *idsemap, int *idmemory);
void CFC_API init_gui( );
void CFC_API prompt_loop( int *code);
void CFC_API search_clients( int *code);
void CFC_API trap_sigus( );

#endif /* _SIC_GETCAR_H_ */

