subroutine sic_lunmac_init(error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_lunmac_init
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private
  !  Initialise private logical units used by SIC
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='MACRO'
  integer(kind=4) :: icode,i
  character(len=message_length) :: mess
  !
  ! Pre-allocate logical units for TEMporary files
  icode = sic_getlun(luntem)
  if (icode.ne.1) then
    call sic_message(seve%e,rname,  &
      'Failed to pre-allocate lun for temporary files')
    error = .true.
    return
  endif
  write(mess,'(A,I0,A)')  &
    'Preallocated 1 lun (#',luntem,') for temporary files'
  call sic_message(seve%d,rname,mess)
  !
  ! Pre-allocate MLUNPA logical units for macros
  do i=1,mlunpa
    icode = sic_getlun(lunmac(i))
    if (icode.ne.1)  error = .true.
  enddo
  if (error) then
    call sic_message(seve%e,rname,'Failed to pre-allocate luns for macros')
    return
  endif
  write(mess,'(A,I0,A,I0,A,I0,A)')  &
    'Preallocated ',mlunpa,' luns (between #',lunmac(1),' and #',  &
    lunmac(mlunpa),') for macros'
  call sic_message(seve%d,rname,mess)
  !
end subroutine sic_lunmac_init
!
subroutine sic_lunmac_exit(error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_lunmac_exit
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private
  ! Free all the pre-allocated LUNs
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: i
  !
  call sic_frelun(luntem)
  !
  do i=1,mlunpa
    call sic_frelun(lunmac(i))
  enddo
  !
end subroutine sic_lunmac_exit
!
subroutine sic_lunmac_get(imac,error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_lunmac_get
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private
  ! Entry to allocate one LUN for a macro call
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: imac
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MACRO'
  integer(kind=4) :: icode
  !
  ! Can we use one of the preallocate LUNs?
  if (imac.le.mlunpa) return
  !
  if (imac.gt.mlun) then
    call sic_message(seve%e,rname,'Parameter MLUN is dimensioned too small')
    error=.true.
    return
  endif
  !
  icode = sic_getlun(lunmac(imac))
  if (mod(icode,2).eq.0)  error=.true.
  !
end subroutine sic_lunmac_get
!
subroutine sic_lunmac_free(imac)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_lunmac_free
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private
  ! Entry to free one LUN for a macro call
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: imac
  !
  ! Never deallocate preallocated LUNs
  if (imac.le.mlunpa) return
  !
  call sic_frelun(lunmac(imac))
  !
end subroutine sic_lunmac_free
