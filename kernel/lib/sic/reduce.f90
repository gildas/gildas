subroutine sic_compute (line,nline,error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_compute
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !    COMPUTE OutVar OPERATION InVar [Parameters]
  !  1         /BLANKING Bval [Eval]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  integer(kind=4),  intent(in)    :: nline  ! Command line length
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=7), parameter :: rname = 'COMPUTE'
  integer(kind=4) :: nkey,nc
  real(kind=8) :: vblank8,eblank8
  integer(kind=4), parameter :: mvoc=33
  character(len=12) :: argum,vocab(mvoc),keywor
  data vocab /  &
    'EXTRACT','FFT+','FFT-','DATE','GAG_DATE','FOURT-','FOURT+',  &  !  1- 7
    'COMPLEX','CMPMUL','CMPDIV','CMPADD','CMPSUB','REAL','IMAG',  &  !  8-14
    'ABS','PHASE','DIMOF','LOCATION','INTEGRAL','DERIVATIVE',     &  ! 15-20
    'RANKORDER','GATHER','BTEST','IS_A_SIC_VAR',                  &  ! 21-24
    ! Operations that accept /BLANKING option:
    'LINES','HISTOGRAM',                                          &  ! 25-26
    'MAX','MIN','MEAN','RMS','SUM','PRODUCT','MEDIAN'/               ! 27-33
  !
  call sic_ke (line,0,2,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,argum,keywor,nkey,vocab,mvoc,error)
  if (error) return
  !
  ! Blanks
  vblank8 = 0.0d0
  eblank8 = -1.0d0
  if (sic_present(1,0)) then
    if (nkey.lt.25) then
      call sic_message(seve%e,rname,  &
        'Option /BLANKING is not allowed for operation '//keywor)
      error=.true.
      return
    elseif (nkey.ge.26) then  ! LINES excluded
      call sic_r8(line,1,1,vblank8,.true.,error)
      if (error)  return
      eblank8 = 0.0d0
      call sic_r8(line,1,2,eblank8,.false.,error)
      if (error)  return
    endif
  endif
  !
  select case (keywor)
  case('EXTRACT')
    call extract_string(line,error)
  case('FFT+')
    call fftsub(line,1,error)
  case('FFT-')
    call fftsub(line,-1,error)
  case('DATE','LINES')
    call compute_file(line,keywor,error)
  case('GAG_DATE')
    call compute_gagdate(line,error)
  case('FOURT-')
    call fourts(line,-1,error)
  case('FOURT+')
    call fourts(line,+1,error)
  case('COMPLEX')
    call cmp_complex(line,error)
  case('CMPADD')
    call cmp_operation(line,keywor,error)
  case('CMPSUB')
    call cmp_operation(line,keywor,error)
  case('CMPMUL')
    call cmp_operation(line,keywor,error)
  case('CMPDIV')
    call cmp_operation(line,keywor,error)
  case('REAL')
    call cmp_real(line,keywor,error)
  case('IMAG')
    call cmp_real(line,keywor,error)
  case('ABS')
    call cmp_real(line,keywor,error)
  case('PHASE')
    call cmp_real(line,keywor,error)
  case('DIMOF')
    call compute_dimof (line,error)
  case('LOCATION')
    call compute_location(line,error)
  case('INTEGRAL')
    call compute_primitive(line,keywor,error)
  case('DERIVATIVE')
    call compute_primitive(line,keywor,error)
  case('HISTOGRAM')
    call compute_histo(line,vblank8,eblank8,error)
  case('RANKORDER')
    call compute_rankorder(line,error)
  case('GATHER')
    call compute_gather(line,error)
  case('BTEST')
    call compute_btest(line,error)
  case('IS_A_SIC_VAR')
    call compute_isasicvar(line,error)
  case default ! MAX, MIN, MEAN, RMS, SUM, PRODUCT, MEDIAN
    if (sic_narg(0).gt.3) then
      ! This to reject the old syntax (before 2008-oct-16) for blanking
      ! values (instead of ignoring them).
      call sic_message(seve%e,rname,  &
        'Operation '//trim(keywor)//' has too many arguments')
      error = .true.
      return
    endif
    call reduce (line,keywor,vblank8,eblank8,error)
    if (error)  return
  end select
  !
end subroutine sic_compute
!
subroutine fftsub(line,is,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces, no_interface1=>fourt,  &
                                   no_interface2=>fourt_plan
  use sic_interfaces, except_this=>fftsub
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !	COMPUTE Varout FFT+ VarIn [REAL]
  ! or	COMPUTE Varout FFT- VarIn [REAL]
  !
  ! Where
  !	VarIn is the input variable, of dimension
  !		[N,1] if REAL, [N,2] if COMPLEX
  !	VarOut is the output variable, of dimension
  !		[N,2]
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  integer                         :: is     !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout,incain
  integer(kind=address_length) :: ipnta
  integer(kind=4) :: ier,form,nc
  integer(kind=size_length) :: nx
  logical :: found
  complex(kind=4), allocatable :: cdata(:),cwork(:)
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Output Variable Non Existent')
    error = .true.
    call sic_volatile(descin)
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,'COMPUTE','Non contiguous sub-array not '//  &
    'supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  form = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,'FFT','Variable is Read Only')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (form.ne.fmt_r4) then
    call sic_message(seve%e,'FFT','Variable must be Real')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  call sic_incarnate(form,descin,incain,error)
  if (error) then
    call sic_volatile(descin)
    return
  endif
  if (sic_present(0,4)) then   ! real -> complex
    if (incain%ndim .ne.1 .or. incain%dims(1) .ne.descout%dims(1) .or.  &
        descout%ndim.ne.2 .or. descout%dims(2).ne.2) then
      call sic_message(seve%e,'FFT','Dimension mismatch')
      call sic_volatile(incain)
      call sic_volatile(descin)
      error = .true.
      return
    endif
  else                         ! complex -> complex
    if (incain%ndim.ne.2 .or. incain%dims(1).ne.descout%dims(1) .or.  &
        incain%dims(2).ne.2 .or. descout%ndim.ne.2 .or. descout%dims(2).ne.2) then
      call sic_message(seve%e,'FFT','Dimension mismatch')
      call sic_volatile(incain)
      call sic_volatile(descin)
      error = .true.
      return
    endif
  endif
  nx = incain%dims(1)
  !
  allocate(cdata(nx),cwork(nx),stat=ier)
  if (failed_allocate('FFT','complex buffers',ier,error)) then
    call sic_volatile(incain)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  ipnta = gag_pointer(incain%addr,memory)
  call fourt_plan(cdata,nx,1,is,1)
  if (sic_present(0,4)) then   ! real -> complex
    call real1_to_complex(memory(ipnta),cdata,nx)
    call fourt(cdata,nx,1,is,0,cwork)
  else                         ! complex -> complex
    call real2_to_complex(memory(ipnta),cdata,nx)
    call fourt(cdata,nx,1,is,1,cwork)
  endif
  ipnta = gag_pointer(descout%addr,memory)
  call complex_to_real(cdata,memory(ipnta),nx)
  deallocate(cdata,cwork)
  call sic_volatile(incain)
  call sic_volatile(descin)
end subroutine fftsub
!
subroutine real1_to_complex(rel,cmpl,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: rel(n)                  !
  complex :: cmpl(n)                !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    cmpl(i) = rel(i)
  enddo
end subroutine real1_to_complex
!
subroutine real2_to_complex(rel,cmpl,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: rel(n,2)                !
  complex :: cmpl(n)                !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    cmpl(i) = cmplx( rel(i,1),rel(i,2) )
  enddo
end subroutine real2_to_complex
!
subroutine real3_to_complex(rel,ima,cmpl,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: rel(n)                  !
  real*4 :: ima(n)                  !
  complex :: cmpl(n)                !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    cmpl(i) = cmplx( rel(i),ima(i) )
  enddo
end subroutine real3_to_complex
!
subroutine complex_to_real(cmpl,rel,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: cmpl(2,n)               !
  real*4 :: rel(n,2)                !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    rel(i,1) = cmpl(1,i)
    rel(i,2) = cmpl(2,i)
  enddo
end subroutine complex_to_real
!
subroutine fourts(line,is,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces, no_interface1=>fourt,  &
                                   no_interface2=>fourt_plan
  use sic_interfaces, except_this=>fourts
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !       COMPUTE VarOut FOURT+ VarIn
  !       COMPUTE VarOut FOURT- VarIn
  !
  ! Where
  !       VarIn is the input variable, of type Complex
  !               [NX,NY,Nz,Nw]
  !       VarOut is the output variable, of type Complex
  !               [NX,NY,Nz,Nw]
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  integer                         :: is     !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer :: ier,form,vtype
  integer :: ndim,nn(2),nc
  integer(kind=size_length) :: nw,nt,np,i
  logical :: found
  complex(kind=4), allocatable :: cwork(:),ctmp(:)
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Output Variable Non Existent')
    error = .true.
    call sic_volatile(descin)
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,'COMPUTE','Non contiguous sub-array not '//  &
    'supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,'FOURT','Variable is Read Only')
    call sic_volatile(descin)
    error=.true.
    return
  elseif (vtype.ne.fmt_c4) then
    call sic_message(seve%e,'FOURT','Output must be Complex')
    call sic_volatile(descin)
    error=.true.
    return
  endif
  form = descin%type
  if (form.ne.fmt_c4) then
    call sic_message(seve%e,'FOURT','Input must be Complex')
    call sic_volatile(descin)
    error=.true.
    return
  endif
  !
  if (descin%ndim.ne.descout%ndim   .or.  &
      descin%dims(1).ne.descout%dims(1) .or.  &
      descin%dims(2).ne.descout%dims(2)) then
    call sic_message(seve%e,'FFT','Dimension mismatch')
    call sic_volatile(descin)
    error=.true.
    return
  elseif (descin%ndim.gt.4) then
    call sic_message(seve%e,'FOURT','Supported up to 4D arrays')
    call sic_volatile(descin)
    error=.true.
    return
  endif
  !
  ndim = min(descin%ndim,2)
  nn(1) = descin%dims(1)
  nn(2) = descin%dims(2)
  np = max(1,descin%dims(3))*max(1,descin%dims(4))  ! What if sic_maxdims!=4 ?
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  !
  nw = max(nn(1),nn(2))
  allocate(cwork(nw),stat=ier)
  if (failed_allocate('FOURT','cwork buffer',ier,error)) then
    call sic_volatile(descin)
    error=.true.
    return
  endif
  !
  if (np.eq.1) then
    call w4tow4_sl(memory(ipnta),memory(ipntb),descin%size)
    call fourt_plan(memory(ipntb),nn,ndim,is,1)
    call fourt(memory(ipntb),nn,ndim,is,1,cwork)
  else
    nt = nn(1)*nn(2)
    allocate(ctmp(nt))
    call fourt_plan(ctmp,nn,ndim,is,1)
    do i=1,np
      call c4toc4_sl(memory(ipnta),ctmp,nt)
      call fourt(ctmp,nn,ndim,is,1,cwork)
      call c4toc4_sl(ctmp,memory(ipntb),nt)
      ipnta = ipnta + 2*nt
      ipntb = ipntb + 2*nt
    enddo
    deallocate(ctmp)
  endif
  deallocate(cwork)
  call sic_volatile(descin)
  error = .false.
  return
  !
end subroutine fourts
!
subroutine cmp_complex (line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>cmp_complex
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: varin,varout,varim
  type(sic_descriptor_t) :: descin,descout,descim
  integer(kind=address_length) :: ipnta,ipntb,ipntc
  logical :: found
  integer :: form,vtype,nc
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Output Variable Non Existent')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,'COMPUTE','Non contiguous sub-array not '//  &
    'supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  form = descin%type
  if (form.ne.fmt_r4) then
    call sic_message(seve%e,'COMPLEX','Input must be Real')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,'COMPLEX','Variable is Read Only')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (vtype.ne.fmt_c4) then
    call sic_message(seve%e,'COMPLEX','Output must be Complex')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (descin%size.ne.(descout%size/2)) then
    call sic_message(seve%e,'COMPLEX','Size do not match')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  !
  ! Check if imaginary part also supplied
  if (sic_present(0,4)) then
    call sic_ke (line,0,4,varim,nc,.true.,error)
    if (error) then
      call sic_volatile(descin)
      return
    endif
    found = .true.
    call sic_materialize(varim,descim,found)
    if (.not.found) then
      call sic_volatile(descin)
      error=.true.
      return
    endif
    form = descim%type
    if (form.ne.fmt_r4) then
      call sic_message(seve%e,'COMPLEX','Input must be Real')
      call sic_volatile(descim)
      call sic_volatile(descin)
      error=.true.
      return
    elseif (descim%size.ne.descin%size) then
      call sic_message(seve%e,'COMPLEX','Sizes do not match')
      call sic_volatile(descim)
      call sic_volatile(descin)
      error=.true.
      return
    endif
    ipntc = gag_pointer(descim%addr,memory)
    call real3_to_complex (memory(ipnta),memory(ipntc),memory(ipntb),descin%size)
  else
    !
    ! Real part only
    call real1_to_complex(memory(ipnta),memory(ipntb),descin%size)
  endif
  call sic_volatile(descim)
  call sic_volatile(descin)
  error = .false.
end subroutine cmp_complex
!
subroutine cmp_real (line,code,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>cmp_real
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  character(len=*)                :: code   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='COMPUTE'
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer(kind=size_length) :: nd
  integer :: form,vtype,nc
  logical :: found
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Variable is Read Only')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (vtype.ne.fmt_r4) then
    call sic_message(seve%e,rname,'Output must be Real*4')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  form = descin%type
  if (form.ne.fmt_c4) then
    call sic_message(seve%e,rname,'Input must be Complex')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  if (descin%size/2.ne.descout%size) then
    call sic_message(seve%e,rname,'Sizes do not match')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  nd = descin%size/2
  if (code.eq.'REAL') then
    call complex_real (memory(ipnta),memory(ipntb),nd)
  elseif (code.eq.'IMAG') then
    call complex_imag (memory(ipnta),memory(ipntb),nd)
  elseif (code.eq.'ABS') then
    call complex_abs (memory(ipnta),memory(ipntb),nd)
  elseif (code.eq.'PHASE') then
    call complex_phi (memory(ipnta),memory(ipntb),nd)
  endif
  call sic_volatile(descin)
  error = .false.
end subroutine cmp_real
!
subroutine complex_real(cmpl,rel,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: cmpl(2,n)               !
  real*4 :: rel(n)                  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    rel(i) = cmpl(1,i)
  enddo
end subroutine complex_real
!
subroutine complex_imag(cmpl,rel,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: cmpl(2,n)               !
  real*4 :: rel(n)                  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    rel(i) = cmpl(2,i)
  enddo
end subroutine complex_imag
!
subroutine complex_abs(cmpl,rel,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: cmpl(2,n)               !
  real*4 :: rel(n)                  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    rel(i) = sqrt(cmpl(1,i)**2+cmpl(2,i)**2)
  enddo
end subroutine complex_abs
!
subroutine complex_phi(cmpl,rel,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length) :: n    !
  real*4 :: cmpl(2,n)               !
  real*4 :: rel(n)                  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    if (cmpl(1,i).ne.0 .or. cmpl(2,i).ne.0) then
      rel(i) = atan2(cmpl(2,i),cmpl(1,i))
    else
      rel(i) = 0.
    endif
  enddo
end subroutine complex_phi
!
subroutine cmp_operation(line,key,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>cmp_operation
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  character(len=*), intent(in)    :: key    ! Kind of operation ( + - / *)
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='COMPLEX'
  character(len=80) :: varin,varout,varim
  type(sic_descriptor_t) :: descin,descout,descim
  integer(kind=address_length) :: ipnta,ipntb,ipntc
  integer(kind=4) :: form,vtype,nc
  logical :: found
  !
  call sic_ke (line,0,4,varim,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varim,descim,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Imaginary Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Real Input Variable Non Existent')
    call sic_volatile(descim)
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Variable is Read Only')
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  elseif (vtype.ne.fmt_c4) then
    call sic_message(seve%e,rname,'Output must be Complex')
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  endif
  form = descin%type
  if (form.ne.fmt_c4) then
    call sic_message(seve%e,rname,'Input must be Complex')
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  endif
  form = descin%type
  if (form.ne.fmt_c4) then
    call sic_message(seve%e,rname,'Input must be Complex')
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  endif
  if (descin%size.ne.descout%size .or. descin%size.ne.descim%size) then
    call sic_message(seve%e,rname,'Size do not match')
    call sic_volatile(descin)
    call sic_volatile(descim)
    error = .true.
    return
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descim%addr,memory)
  ipntc = gag_pointer(descout%addr,memory)
  !
  select case (key)
  case ('CMPADD')
    call complex_add(memory(ipnta),memory(ipntb),memory(ipntc),descin%size/2)
  case ('CMPSUB')
    call complex_sub(memory(ipnta),memory(ipntb),memory(ipntc),descin%size/2)
  case ('CMPMUL')
    call complex_mul(memory(ipnta),memory(ipntb),memory(ipntc),descin%size/2)
  case ('CMPDIV')
    call complex_div(memory(ipnta),memory(ipntb),memory(ipntc),descin%size/2)
  case default
    call sic_message(seve%e,rname,'Operation '//trim(key)//' is not implemented')
    error = .true.
    ! continue
  end select
  !
  call sic_volatile(descin)
  call sic_volatile(descim)
end subroutine cmp_operation
!
subroutine complex_add(a,b,c,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n     !
  complex(kind=4),           intent(in)  :: a(n)  !
  complex(kind=4),           intent(in)  :: b(n)  !
  complex(kind=4),           intent(out) :: c(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    c(i) = a(i)+b(i)
  enddo
end subroutine complex_add
!
subroutine complex_sub(a,b,c,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n     !
  complex(kind=4),           intent(in)  :: a(n)  !
  complex(kind=4),           intent(in)  :: b(n)  !
  complex(kind=4),           intent(out) :: c(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    c(i) = a(i)-b(i)
  enddo
end subroutine complex_sub
!
subroutine complex_mul(a,b,c,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n     !
  complex(kind=4),           intent(in)  :: a(n)  !
  complex(kind=4),           intent(in)  :: b(n)  !
  complex(kind=4),           intent(out) :: c(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    c(i) = a(i)*b(i)
  enddo
end subroutine complex_mul
!
subroutine complex_div(a,b,c,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n     !
  complex(kind=4),           intent(in)  :: a(n)  !
  complex(kind=4),           intent(in)  :: b(n)  !
  complex(kind=4),           intent(out) :: c(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n
    c(i) = a(i)/b(i)
  enddo
end subroutine complex_div
!
subroutine compute_gagdate(line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_gagdate
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !     Convert an ASCII date in format DD-MM-YYYY in
  !     internal "GAG" code, as used by CLASS and CLIC.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='GAG_DATE'
  character(len=256) :: varout,argum
  character(len=11) :: date  ! "15-DEC-2035"
  type(sic_descriptor_t) :: descout
  integer(kind=address_length) :: ipntb
  logical :: found
  integer(kind=4) :: vtype,nc,ivalue
  !
  ! Get output variable
  call sic_ke(line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  ! Check output variable
  found = .true.
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,'Non contiguous sub-array not '//  &
    'supported for the output variable')
    error = .true.
    return
  elseif (descout%ndim.ne.0 .and. descout%ndim.ne.1 .and.  &
          descout%dims(1).ne.1) then
    call sic_message(seve%e,rname,'Output variable must be scalar')
    error = .true.
    return
  endif
  !
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Variable cannot be written')
    error = .true.
    return
  endif
  !
  vtype = descout%type
  if (vtype.eq.fmt_i4 .or. vtype.eq.fmt_i8) then
    ! Get the input date string
    call sic_ch(line,0,3,argum,nc,.true.,error)
    if (error) return
    !
    ! Convert to the integer code
    call gag_fromdate(argum(1:nc),ivalue,error)
    if (error) return
    !
    ! Copy to output variable
    ipntb = gag_pointer(descout%addr,memory)
    if (vtype.eq.fmt_i4) then
      call i4toi4(ivalue,memory(ipntb),1)
    elseif (vtype.eq.fmt_i8) then
      call i4toi8(ivalue,memory(ipntb),1)
    endif
    !
  elseif (vtype.gt.0) then
    ! Get the input integer code
    call sic_i4(line,0,3,ivalue,.true.,error)
    if (error) return
    !
    ! Convert to date string
    call gag_todate(ivalue,date,error)
    if (error) return
    !
    ipntb = bytpnt(descout%addr,membyt)
    nc=min(11,vtype)
    call bytoby(date,membyt(ipntb),nc)
    !
  else
    call sic_message(seve%e,rname,  &
      'Output variable must be Integer or Character')
    error = .true.
    return
  endif
  !
end subroutine compute_gagdate
!
subroutine compute_histo(line,vblank8,eblank8,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_histo
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  ! [SIC\]COMPUTE OutVar HISTOGRAM InVar [Hmin] [Hmax]
  ! 1             /BLANKING Bval [Eval]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line     !
  real(kind=8),     intent(in)    :: vblank8  !
  real(kind=8),     intent(in)    :: eblank8  !
  logical,          intent(inout) :: error    !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='HISTO'
  character(len=80) :: varin,varout
  character(len=20) :: cc
  type(sic_descriptor_t) :: descin,descout,desccopy
  integer(kind=address_length) :: ipnta,ipntb
  real(8) :: hismin8,hismax8
  real(4) :: hismin4,hismax4,vblank4,eblank4
  real(8) :: hwidth
  logical :: found
  integer :: typein,typeout,nc
  character(len=message_length) :: mess
  !
  ! Check number of arguments for old blanking syntax
  if (sic_narg(0).gt.5) then
    call sic_message(seve%e,'COMPUTE','Operation HISTO has too many arguments')
    call sic_message(seve%e,'COMPUTE','To set blanking values, use '//  &
      '/BLANKING option')
    error = .true.
    return
  endif
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Output Variable Non Existent')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,'COMPUTE','Non contiguous sub-array not '//  &
    'supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  typeout = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,'COMPUTE','Output variable cannot be written')
    call sic_volatile(descin)
    error =.true.
    return
  elseif (typeout.ne.fmt_r4 .and. typeout.ne.fmt_r8) then
    call sic_message(seve%e,'COMPUTE','Output variable must be Real')
    call sic_volatile(descin)
    error =.true.
    return
  endif
  !
  ! Check dimensions
  if (descout%ndim.ne.2 .or. descout%dims(1).le.1 .or.  &
      descout%dims(2).le.1) then
    call sic_message(seve%e,'COMPUTE','Invalid OUTPUT variable dimensions')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  !
  typein = descin%type
  if (typein.ne.fmt_r4 .and. typein.ne.fmt_r8 .and. typein.ne.fmt_i4) then
    call sic_message(seve%e,rname,'Input variable must be Real or Integer')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  !
  if (typein.eq.fmt_i4) then
    ! Cast the integer array in a real copy. Precision is controled by the
    ! output variable.
    call sic_incarnate(typeout,descin,desccopy,error)
    call sic_volatile(descin)  ! Suppress the original (i4) materialization
    descin = desccopy          ! New descriptor takes place of the original one
    typein = typeout           ! in the subsequent computations
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  !
  hwidth = 0.05d0              ! Default half-width is 5% when all data are equal
  !
  if (typein.eq.fmt_r8) then
    ! Real(8) case
    if (sic_present(0,4)) then
      cc = ' '
      call sic_ch(line,0,4,cc,nc,.true.,error)
      if (cc.eq.'*') then
        call comp_r8_min (memory(ipnta),descin%size/2,vblank8,eblank8,hismin8)
      else
        call sic_r8(line,0,4,hismin8,.false.,error)
      endif
      if (error) then
        call sic_message(seve%e,rname,'Impossible to read histogram min')
        call sic_volatile(descin)
        return
      endif
    else
      call comp_r8_min (memory(ipnta),descin%size/2,vblank8,eblank8,hismin8)
    endif
    if (sic_present(0,5)) then
      cc = ' '
      call sic_ch(line,0,5,cc,nc,.true.,error)
      if (cc.eq.'*') then
        call comp_r8_max (memory(ipnta),descin%size/2,vblank8,eblank8,hismax8)
      else
        call sic_r8(line,0,5,hismax8,.false.,error)
      endif
      if (error) then
        call sic_message(seve%e,rname,'Impossible to read histogram max')
        call sic_volatile(descin)
        return
      endif
    else
      call comp_r8_max (memory(ipnta),descin%size/2,vblank8,eblank8,hismax8)
    endif
    if (hismin8.eq.hismax8) then
      write(mess,'(A,ES14.6)') 'All data values are ',hismax8
      call sic_message(seve%w,rname,mess)
      if (hismin8.eq.0.0d0) then
        hismin8 = -hwidth
        hismax8 =  hwidth
      else
        hismin8 = hismin8*(1.0d0-hwidth)
        hismax8 = hismax8*(1.0d0+hwidth)
      endif
    endif
    if (typeout.eq.fmt_r8) then
      call histo88(memory(ipnta),descin%size/2,memory(ipntb),descout%dims(1),  &
        descout%dims(2),hismin8,hismax8,vblank8,eblank8)
    else
      call histo84(memory(ipnta),descin%size/2,memory(ipntb),descout%dims(1),  &
        descout%dims(2),hismin8,hismax8,vblank8,eblank8)
    endif
    !
  else
    ! Real(4) case
    vblank4 = vblank8
    eblank4 = eblank8
    !
    if (sic_present(0,4)) then
      cc = ' '
      call sic_ch(line,0,4,cc,nc,.true.,error)
      if (cc.eq.'*') then
        call comp_r4_min (memory(ipnta),descin%size,vblank4,eblank4,hismin4)
      else
        call sic_r4(line,0,4,hismin4,.false.,error)
      endif
      if (error) then
        call sic_message(seve%e,rname,'Impossible to read histogram max')
        call sic_volatile(descin)
        return
      endif
    else
      call comp_r4_min (memory(ipnta),descin%size,vblank4,eblank4,hismin4)
    endif
    if (sic_present(0,5)) then
      cc = ' '
      call sic_ch(line,0,5,cc,nc,.true.,error)
      if (cc.eq.'*') then
        call comp_r4_max (memory(ipnta),descin%size,vblank4,eblank4,hismax4)
      else
        call sic_r4(line,0,5,hismax4,.false.,error)
      endif
      if (error) then
        call sic_message(seve%e,rname,'Impossible to read histogram max')
        call sic_volatile(descin)
        return
      endif
    else
      call comp_r4_max (memory(ipnta),descin%size,vblank4,eblank4,hismax4)
    endif
    if (hismin4.eq.hismax4) then
      write(mess,'(A,ES12.4)') 'All data values are ',hismax4
      call sic_message(seve%w,rname,mess)
      if (hismin4.eq.0.0) then
        hismin4 = -hwidth
        hismax4 =  hwidth
      else
        hismin4 = hismin4*(1.0-hwidth)
        hismax4 = hismax4*(1.0+hwidth)
      endif
    endif
    if (typeout.eq.fmt_r8) then
      call histo48(memory(ipnta),descin%size,memory(ipntb),descout%dims(1),  &
        descout%dims(2),hismin4,hismax4,vblank4,eblank4)
    else
      call histo44(memory(ipnta),descin%size,memory(ipntb),descout%dims(1),  &
        descout%dims(2),hismin4,hismax4,vblank4,eblank4)
    endif
  endif
  error = .false.
  call sic_volatile(descin)
end subroutine compute_histo
!
subroutine histo88(a,na,b,nb,mb,min,max,bval,eval)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   Perform computations in a temporary integer array since float
  ! values can not be incremented by 1 beyond a given limit (16777216.
  ! for REAL*4). Use INTEGER*8 because number of values per bin can be
  ! larger than 2**31-1.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: na   !
  real*8 :: a(na)                   !
  integer(kind=index_length) :: nb  !
  integer(kind=index_length) :: mb  !
  real*8 :: b(nb,mb)                !
  real*8 :: min                     !
  real*8 :: max                     !
  real*8 :: bval                    !
  real*8 :: eval                    !
  ! Local
  real*8 :: step,v,hmin,hmax
  integer(kind=size_length) :: l,n
  integer(kind=8) :: ib(nb)  ! Automatic array
  !
  ! Initialization
  ib(:) = 0
  !
  step=(max-min)/(nb-1)
  hmin=min-step/2.0
  hmax=max+step/2.0
  if (eval.lt.0.0) then
    do l = 1,na
      if (a(l).eq.a(l)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  else
    do l = 1,na
      if (a(l).eq.a(l) .and. (abs(a(l)-bval).gt.eval)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  endif
  !
  ! Now fill the histogram: counts and bin definitions
  do l=1,nb
    b(l,1) = ib(l)
    b(l,2) = min+(l-1)*step
  enddo
end subroutine histo88
!
subroutine histo84(a,na,b,nb,mb,min,max,bval,eval)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   Perform computations in a temporary integer array since float
  ! values can not be incremented by 1 beyond a given limit (16777216.
  ! for REAL*4). Use INTEGER*8 because number of values per bin can be
  ! larger than 2**31-1.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: na   !
  real*8 :: a(na)                   !
  integer(kind=index_length) :: nb  !
  integer(kind=index_length) :: mb  !
  real*4 :: b(nb,mb)                !
  real*8 :: min                     !
  real*8 :: max                     !
  real*8 :: bval                    !
  real*8 :: eval                    !
  ! Local
  real*8 :: step,v,hmin,hmax
  integer(kind=size_length) :: l,n
  integer(kind=8) :: ib(nb)  ! Automatic array
  !
  ! Initialization
  ib(:) = 0
  !
  step=(max-min)/(nb-1)
  hmin=min-step/2.0
  hmax=max+step/2.0
  if (eval.lt.0.0) then
    do l = 1,na
      if (a(l).eq.a(l)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  else
    do l = 1,na
      if (a(l).eq.a(l) .and. (abs(a(l)-bval).gt.eval)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  endif
  !
  ! Now fill the histogram: counts and bin definitions
  do l=1,nb
    b(l,1) = ib(l)
    b(l,2) = min+(l-1)*step
  enddo
end subroutine histo84
!
subroutine histo48(a,na,b,nb,mb,min,max,bval,eval)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   Perform computations in a temporary integer array since float
  ! values can not be incremented by 1 beyond a given limit (16777216.
  ! for REAL*4). Use INTEGER*8 because number of values per bin can be
  ! larger than 2**31-1.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: na   !
  real*4 :: a(na)                   !
  integer(kind=index_length) :: nb  !
  integer(kind=index_length) :: mb  !
  real*8 :: b(nb,mb)                !
  real*4 :: min                     !
  real*4 :: max                     !
  real*4 :: bval                    !
  real*4 :: eval                    !
  ! Local
  real*4 :: step,v,hmin,hmax
  integer(kind=size_length) :: l,n
  integer(kind=8) :: ib(nb)  ! Automatic array
  !
  ! Initialization
  ib(:) = 0
  !
  step=(max-min)/(nb-1)
  hmin=min-step/2.0
  hmax=max+step/2.0
  if (eval.lt.0.0) then
    do l = 1,na
      if (a(l).eq.a(l)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  else
    do l = 1,na
      if (a(l).eq.a(l) .and. (abs(a(l)-bval).gt.eval)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  endif
  !
  ! Now fill the histogram: counts and bin definitions
  do l=1,nb
    b(l,1) = ib(l)
    b(l,2) = min+(l-1)*step
  enddo
end subroutine histo48
!
subroutine histo44(a,na,b,nb,mb,min,max,bval,eval)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   Perform computations in a temporary integer array since float
  ! values can not be incremented by 1 beyond a given limit (16777216.
  ! for REAL*4). Use INTEGER*8 because number of values per bin can be
  ! larger than 2**31-1.
  !---------------------------------------------------------------------
  integer(kind=size_length) :: na   !
  real*4 :: a(na)                   !
  integer(kind=index_length) :: nb  !
  integer(kind=index_length) :: mb  !
  real*4 :: b(nb,mb)                !
  real*4 :: min                     !
  real*4 :: max                     !
  real*4 :: bval                    !
  real*4 :: eval                    !
  ! Local
  real*4 :: step,v,hmin,hmax
  integer(kind=size_length) :: l,n
  integer(kind=8) :: ib(nb)  ! Automatic array
  !
  ! Initialization
  ib(:) = 0
  !
  step=(max-min)/(nb-1)
  hmin=min-step/2.0
  hmax=max+step/2.0
  if (eval.lt.0.0) then
    do l = 1,na
      if (a(l).eq.a(l)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  else
    do l = 1,na
      if (a(l).eq.a(l) .and. (abs(a(l)-bval).gt.eval)) then
        if (a(l).ge.min .and. a(l).le.max) then
          v = (a(l)-hmin)/step
          n=int(v)+1
          ib(n) = ib(n)+1
        endif
      endif
    enddo
  endif
  !
  ! Now fill the histogram: counts and bin definitions
  do l=1,nb
    b(l,1) = ib(l)
    b(l,2) = min+(l-1)*step
  enddo
end subroutine histo44
!
subroutine compute_file(line,key,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>compute_file
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  character(len=*), intent(in)    :: key    !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='COMPUTE'
  character(len=varname_length) :: varout
  character(len=filename_length) :: name,file
  type(sic_descriptor_t) :: descout
  integer(kind=address_length) :: ipntb
  logical :: found,doblank
  integer(kind=4) :: nc,value4
  integer(kind=8) :: value8
  !
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Variable does not exist')
    error = .true.
    return
  endif
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Variable cannot be written')
    error = .true.
    return
  elseif (descout%type.ne.fmt_i4 .and. descout%type.ne.fmt_i8) then
    call sic_message(seve%e,rname,'Variable must be Integer')
    error = .true.
    return
  endif
  !
  ! Check dimensions
  if (descout%ndim.ne.0 .and. descout%ndim.ne.1 .and.  &
      descout%dims(1).ne.1) then
    call sic_message(seve%e,rname,'Invalid variable dimensions')
    error = .true.
    return
  endif
  !
  ! Now the file
  call sic_ch (line,0,3,name,nc,.true.,error)
  if (error) return
  call sic_parse_file(name,' ','.dat',file)
  inquire(file=file,exist=found)
  if (.not.found) then
    call sic_message(seve%e,rname,'File '//trim(file)//' does not exist')
    error = .true.
    return
  endif
  !
  ! Compute the value
  select case(key)
  case ('DATE')
    call compute_file_date(file,descout%type,value8,error)
  case ('LINES')
    doblank = sic_present(1,0)  ! COMPUTE LINES /BLANK
    call gag_fillines(file,doblank,value4,error)
    value8 = value4
  case default
    call sic_message(seve%e,rname,trim(key)//' not implemented')
    error = .true.
  end select
  if (error)  return
  !
  ipntb = gag_pointer(descout%addr,memory)
  if (descout%type.eq.fmt_i4) then
    call i8toi4(value8,memory(ipntb),1)
  else
    call i8toi8(value8,memory(ipntb),1)
  endif
  !
end subroutine compute_file
!
subroutine compute_file_date(file,type,value,error)
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>compute_file_date
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the file date from 01-jan-1970. If 'type' is I*4, returned
  ! value are seconds. If 'type' is I*8, returned value are nanoseconds.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: file   !
  integer(kind=4),  intent(in)    :: type   !
  integer(kind=8),  intent(out)   :: value  !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='DATE'
  integer(kind=4) :: ier,sec
  integer(kind=8) :: nsec
  !
  if (type.eq.fmt_i4) then
    ! Type is I*4: return value in seconds
    ier = gag_mdate(file,sec)
    value = sec
  else  ! fmt_i8
    ! Type is I*8: return value in nanoseconds
    ier = gag_mtime(file,nsec)
    value = nsec
  endif
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Cannot stat the file')
    error = .true.
    return
  endif
  !
end subroutine compute_file_date
!
subroutine compute_dimof(line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_dimof
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   COMPUTE VarOut DIMOF VarIn
  ! where
  !   VarIn is the input variable, of dimension
  !     [Nx,Ny,Nz,Nv,Na,Nb,Nc]
  !   VarOut is the output variable, of dimension
  !     [8]
  !   containing Nx,Ny,Nz,Nv,Na,Nb,Nc,Ndim
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='DIMOF'
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnt
  integer :: oform,nc
  integer(kind=index_length) :: dims(sic_maxdims+1)
  logical :: found
  character(len=message_length) :: mess
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_descriptor(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Input Variable Non Existent')
    error = .true.
    return
  elseif (descin%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the input variable')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the output variable')
    call sic_volatile(descout)
    error = .true.
    return
  endif
  !
  oform = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Output variable is Read Only')
    error = .true.
    return
  elseif (.not.(oform.eq.fmt_i4 .or. oform.eq.fmt_i8)) then
    call sic_message(seve%e,rname,'Output variable must be Integer or Long')
    error = .true.
    return
  elseif (descout%ndim.ne.1 .or.  &
         (descout%dims(1).ne.5 .and. descout%dims(1).ne.(sic_maxdims+1)) ) then
    ! Expect sic_maxdims+1 sized vector, but tolerate 4+1 for backward
    ! compatibility (01-sep-2011). No publicity about this latter point.
    write(mess,'(A,I0,A)')  &
      'Output variable must be a rank-1 array of Dim [',sic_maxdims+1,']'
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  if (descout%dims(1).eq.5 .and. descin%ndim.gt.4) then
    ! Ensure backward compatibility when possible
    write(mess,'(A,I0,A)')  &
      'Output variable is too small to store the ',descin%ndim,' dimensions'
    call sic_message(seve%e,rname,mess)
    write(mess,'(A,I0,A)')  &
      'Output variable must be a rank-1 array of Dim [',sic_maxdims+1,']'
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  dims(1:descout%dims(1)-1) = descin%dims(1:descout%dims(1)-1)
  dims(descout%dims(1)) = descin%ndim
  !
  ! Transfer to output variable
  ipnt = gag_pointer(descout%addr,memory)
  if (index_length.eq.4) then
    if (oform.eq.fmt_i4) then
      call i4toi4 (dims,memory(ipnt),descout%dims(1))
    else  ! oform.eq.fmt_i8
      call i4toi8 (dims,memory(ipnt),descout%dims(1))
    endif
  else  ! index_length.eq.8
    if (oform.eq.fmt_i4) then
      call i8toi4_fini (dims,memory(ipnt),descout%dims(1),error)
      if (error)  return
    else  ! oform.eq.fmt_i8
      call i8toi8 (dims,memory(ipnt),descout%dims(1))
    endif
  endif
  !
end subroutine compute_dimof
!
subroutine compute_location(line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_location
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! COMPUTE OutVar LOCATION Invar Value
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='COMPUTE'
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  real*8 :: dvalue
  real*4 :: svalue
  logical :: found
  integer :: vtype,nc
  integer(kind=size_length) :: loc(2)
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_descriptor(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Input Variable Non Existent')
    error = .true.
    return
  endif
  !
  ! Output variable
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,'Non contiguous sub-array not supported '//  &
    'for the output variable')
    call sic_volatile(descout)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Output Variable cannot be written')
    error =.true.
    return
  elseif (vtype.ne.fmt_i4 .and. vtype.ne.fmt_i8) then
    call sic_message(seve%e,rname,'Output Variable must be Integer')
    error =.true.
    return
  endif
  !
  ! Check dimensions
  if (descout%ndim.ne.1.or.descout%dims(1).ne.2) then
    call sic_message(seve%e,rname,'Invalid OUTPUT variable dimensions')
    error = .true.
    return
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  if (descin%type.eq.fmt_r8) then
    call sic_r8(line,0,4,dvalue,.true.,error)
    if (error)  goto 100
    call gr8_locate (memory(ipnta),descin%size/2,dvalue,loc(1),loc(2))
  else
    call sic_r4(line,0,4,svalue,.true.,error)
    if (error)  goto 100
    call gr4_locate (memory(ipnta),descin%size,svalue,loc(1),loc(2))
  endif
  !
  ipntb = gag_pointer(descout%addr,memory)
  if (descout%type.eq.fmt_i4) then
    if (size_length.eq.4) then
      call i4toi4(loc,memory(ipntb),2)
    else
      call i8toi4_fini(loc,memory(ipntb),2,error)
      ! if (error) continue
    endif
  else  ! fmt_i8
    if (size_length.eq.4) then
      call i4toi8(loc,memory(ipntb),2)
    else
      call i8toi8(loc,memory(ipntb),2)
    endif
  endif
  !
100 continue
  call sic_volatile(descin)
end subroutine compute_location
!
subroutine compute_primitive (line,code,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_primitive
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  character(len=*)                :: code   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=80) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer(kind=size_length) :: nd
  integer :: form,vtype,nc,scalin,scalout
  logical :: found
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,'PRIMITIVE','Output Variable Non Existent')
    call sic_volatile(descin)
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,'PRIMITIVE','Non contiguous sub-array not '//  &
    'supported for the output variable')
    call sic_volatile(descout)
    call sic_volatile(descin)
    error = .true.
    return
  endif
  vtype = descout%type
  if (descout%readonly) then
    call sic_message(seve%e,'PRIMITIVE','Variable is Read Only')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  form = descin%type
  if (form.eq.fmt_r8) then
    scalin = 2
  else
    scalin = 1
  endif
  if (vtype.eq.fmt_r8) then
    scalout = 2
  else
    scalout = 1
  endif
  if (descin%size/scalin.ne.descout%size/scalout) then
    call sic_message(seve%e,'PRIMITIVE','Size do not match')
    error = .true.
    return
  endif
  !
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  nd = descout%size/scalout
  !
  if (form.eq.fmt_r4 .and. vtype.eq.fmt_r4) then
    if (code.eq.'INTEGRAL') then
      call cmp44_integral(memory(ipnta),memory(ipntb),nd)
    elseif (code.eq.'DERIVATIVE') then
      call cmp44_derivative(memory(ipnta),memory(ipntb),nd)
    endif
  elseif  (form.eq.fmt_r4 .and. vtype.eq.fmt_r8) then
    if (code.eq.'INTEGRAL') then
      call cmp48_integral(memory(ipnta),memory(ipntb),nd)
    elseif (code.eq.'DERIVATIVE') then
      call cmp48_derivative(memory(ipnta),memory(ipntb),nd)
    endif
  elseif  (form.eq.fmt_r8 .and. vtype.eq.fmt_r4) then
    if (code.eq.'INTEGRAL') then
      call cmp84_integral(memory(ipnta),memory(ipntb),nd)
    elseif (code.eq.'DERIVATIVE') then
      call cmp84_derivative(memory(ipnta),memory(ipntb),nd)
    endif
  elseif  (form.eq.fmt_r8 .and. vtype.eq.fmt_r8) then
    if (code.eq.'INTEGRAL') then
      call cmp88_integral(memory(ipnta),memory(ipntb),nd)
    elseif (code.eq.'DERIVATIVE') then
      call cmp88_derivative(memory(ipnta),memory(ipntb),nd)
    endif
  endif
  call sic_volatile(descin)
  error = .false.
end subroutine compute_primitive
!
subroutine cmp44_integral(fun,int,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(4),                   intent(in)  :: fun(n)  !
  real(4),                   intent(out) :: int(n)  !
  ! Local
  integer(kind=size_length) :: i
  real(4) :: val
  !
  val = 0.
  do i=1,n
    val = val+fun(i)
    int(i) = val
  enddo
end subroutine cmp44_integral
!
subroutine cmp44_derivative(fun,der,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(4),                   intent(in)  :: fun(n)  !
  real(4),                   intent(out) :: der(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n-1
    der(i) = fun(i+1)-fun(i)
  enddo
  if (n.gt.2) then
    der(n) = 2*der(n-1)-der(n-2)
  else
    der(n) = der(n-1)
  endif
end subroutine cmp44_derivative
!
subroutine cmp48_integral(fun,int,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(4),                   intent(in)  :: fun(n)  !
  real(8),                   intent(out) :: int(n)  !
  ! Local
  integer(kind=size_length) :: i
  real(8) :: val
  !
  val = 0.
  do i=1,n
    val = val+fun(i)
    int(i) = val
  enddo
end subroutine cmp48_integral
!
subroutine cmp48_derivative(fun,der,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(4),                   intent(in)  :: fun(n)  !
  real(8),                   intent(out) :: der(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n-1
    der(i) = fun(i+1)-fun(i)
  enddo
  if (n.gt.2) then
    der(n) = 2*der(n-1)-der(n-2)
  else
    der(n) = der(n-1)
  endif
end subroutine cmp48_derivative
!
subroutine cmp84_integral(fun,int,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(8),                   intent(in)  :: fun(n)  !
  real(4),                   intent(out) :: int(n)  !
  ! Local
  integer(kind=size_length) :: i
  real(8) :: val
  !
  val = 0.
  do i=1,n
    val = val+fun(i)
    int(i) = val
  enddo
end subroutine cmp84_integral
!
subroutine cmp84_derivative(fun,der,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(8),                   intent(in)  :: fun(n)  !
  real(4),                   intent(out) :: der(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n-1
    der(i) = fun(i+1)-fun(i)
  enddo
  if (n.gt.2) then
    der(n) = 2*der(n-1)-der(n-2)
  else
    der(n) = der(n-1)
  endif
end subroutine cmp84_derivative
!
subroutine cmp88_integral(fun,int,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(8),                   intent(in)  :: fun(n)  !
  real(8),                   intent(out) :: int(n)  !
  ! Local
  integer(kind=size_length) :: i
  real(8) :: val
  !
  val = 0.
  do i=1,n
    val = val+fun(i)
    int(i) = val
  enddo
end subroutine cmp88_integral
!
subroutine cmp88_derivative(fun,der,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n       !
  real(8),                   intent(in)  :: fun(n)  !
  real(8),                   intent(out) :: der(n)  !
  ! Local
  integer(kind=size_length) :: i
  !
  do i=1,n-1
    der(i) = fun(i+1)-fun(i)
  enddo
  if (n.gt.2) then
    der(n) = 2*der(n-1)-der(n-2)
  else
    der(n) = der(n-1)
  endif
end subroutine cmp88_derivative
!
subroutine extract_string (line,error)
  use gildas_def
  use gbl_message
  use sic_interfaces
  !---------------------------------------------------------------------
  ! @ obsolete since 05-jan-2015
  !     COMPUTE OutStr EXTRACT InStr Start End
  ! Obsolete now that SIC supports direct subtring access, e.g.
  !     LET OutStr InStr[Start:End]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='COMPUTE'
  character(len=varname_length) :: varout,varin
  integer(kind=4) :: nc,istart,iend
  character(len=message_length) :: mess
  !
  call sic_ke(line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  call sic_ke(line,0,3,varin,nc,.true.,error)
  if (error) return
  !
  istart = 1
  call sic_i4(line,0,4,istart,.false.,error)
  !
  iend = istart
  call sic_i4(line,0,5,iend,.false.,error)
  !
  call sic_message(seve%e,rname,'COMPUTE EXTRACT is obsolete. Use direct substring access e.g.')
  write(mess,'(5A,I0,A,I0,A)') '  LET ',trim(varout),' ''',trim(varin),'[',istart,':',iend,']'''
  call sic_message(seve%e,rname,mess)
  error = .true.
  !
end subroutine extract_string
!
subroutine compute_rankorder (line,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>compute_rankorder
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='RANKORDER'
  character(len=varname_length) :: varin,varout
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer(kind=size_length) :: ndin,ndou
  integer(kind=4) :: nc,ier
  logical :: found
  integer(kind=8), allocatable :: iwork(:)
  real(kind=8), allocatable :: rwork(:)
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  ! Input variable
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Input variable non existent')
    error = .true.
    return
  endif
  if (descin%type.ne.fmt_r4 .and.  &
      descin%type.ne.fmt_r8) then
    call sic_message(seve%e,rname,'Input variable must be R*4 or R*8')
    error = .true.
  endif
  if (error)  goto 10
  ndin = desc_nelem(descin)
  ipnta = gag_pointer(descin%addr,memory)
  !
  ! Output variable
  found = .true.
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    goto 10
  endif
  if (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,'Non contiguous sub-array not '//  &
      'supported for the output variable')
    error = .true.
  endif
  if (descout%readonly) then
    call sic_message(seve%e,rname,'Variable is Read Only')
    error = .true.
  endif
  if (descout%type.ne.fmt_r4 .and.  &
      descout%type.ne.fmt_r8) then
    call sic_message(seve%e,rname,'Output variable must be R*4 or R*8')
    error = .true.
  endif
  if (error)  goto 10
  ndou = desc_nelem(descout)
  ipntb = gag_pointer(descout%addr,memory)
  !
  ! Sanity
  if (ndin.ne.ndou) then
    call sic_message(seve%e,rname,'Size do not match')
    error = .true.
    return
  endif
  !
  allocate (iwork(ndin),rwork(ndin),stat=ier)
  if (failed_allocate(rname,'rwork/iwork',ier,error))  goto 10
  if (descin%type.eq.fmt_r4) then
    call r4tor8_sl (memory(ipnta),rwork,ndin)
  elseif (descin%type.eq.fmt_r8) then
    call r8tor8_sl (memory(ipnta),rwork,ndin)
  endif
  !
  call gr8_trie(rwork,iwork,ndin,error)
  if (error)  goto 10
  !
  if (descout%type.eq.fmt_r4) then
    call i8tor4_sl(iwork,memory(ipntb),ndin)
  elseif (descout%type.eq.fmt_r8) then
    call i8tor8_sl(iwork,memory(ipntb),ndin)
  endif
  !
10 continue
  call sic_volatile(descin)
  ! Implicit deallocation of iwork/rwork
end subroutine compute_rankorder
!
subroutine compute_gather (line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_gather
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! COMPUTE Out GATHER In
  ! Builds (in Out) the list of values found in In
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  logical, parameter :: global=.true.
  character(len=80) :: varin,varout
  character(len=32) :: chain,string
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer(kind=size_length) :: nd,nw
  integer :: form,nc,ier
  logical :: found
  real(kind=4), allocatable :: rwork(:)
  real(kind=8), allocatable :: dwork(:)
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_materialize(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,'COMPUTE','Input Variable Non Existent')
    error = .true.
    return
  endif
  call sic_descriptor(varout,descout,found)
  if (found) then
    call sic_message(seve%e,'GATHER','Output Variable Already Exist')
    call sic_volatile(descin)
    error = .true.
    return
!   call sic_delvariable(varout,.false.,error)
!   error = .false.
  endif
  !
  ! Scan the input values
  form = descin%type
  ipnta = gag_pointer(descin%addr,memory)
  if (form.eq.fmt_r8) then
    nd = descin%size/2
    allocate (dwork(nd),stat=ier)
    call gather_r8(dwork,nw,memory(ipnta),nd,0.d0,-1.d0)
  else
    nd = descin%size
    allocate (rwork(nd),stat=ier)
    call gather_r4(rwork,nw,memory(ipnta),nd,0.,-1.)
  endif
  !
  if (nw.ne.0) then
    write (chain,'(i12)') nw
    string='['//trim(adjustl(chain))//']'
    nc = len_trim(varout)+1
    varout(nc:) = string
    call sic_defvariable(form,varout,global,error)
    if (error)  goto 100
    !
    varout(nc:) = ' '
    call sic_descriptor(varout,descout,found)
    ipntb = gag_pointer(descout%addr,memory)
    if (form.eq.fmt_r8) then
      call r8tor8_sl(dwork,memory(ipntb),nw)
    else
      call r4tor4_sl(rwork,memory(ipntb),nw)
    endif
  endif
  !
100 continue
  if (form.eq.fmt_r8) then
    deallocate (dwork)
  else
    deallocate (rwork)
  endif
  !
  call sic_volatile(descin)
end subroutine compute_gather
!
subroutine compute_btest(line,error)
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>compute_btest
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   COMPUTE VarOut BTEST VarIn
  ! Bit Test (same as Fortran BTEST)
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='BTEST'
  character(len=80) :: varin,varout
  integer(kind=4) :: nc,ibit,nbit,nbyte
  type(sic_descriptor_t) :: descin,descout
  integer(kind=address_length) :: ipnta,ipntb
  integer(kind=size_length) :: ielem
  logical :: found,one
  character(len=message_length) :: mess
  !
  call sic_ke (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  call sic_descriptor(varin,descin,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Input Variable Non Existent')
    error = .true.
    return
  elseif (descin%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the input variable')
    call sic_volatile(descin)
    error = .true.
    return
  endif
  !
  nbyte = gag_sizeof(descin%type)
  if (nbyte.ne.4 .and. nbyte.ne.8) then
    ! Do not support types not aligned on 4 byte words, because of the
    ! ipnta shift in the computation loop.
    call sic_message(seve%e,rname,'Input Variable type is not supported')
    error = .true.
    return
  endif
  nbit = 8*nbyte
  if (sic_present(0,4)) then
    one = .true.
    call sic_i4(line,0,4,ibit,.true.,error)
    if (error)  return
    if (ibit.le.0 .or. ibit.gt.nbit) then
      call sic_message(seve%e,rname,'Bit number out of range')
      error = .true.
      return
    endif
  else
    one = .false.
  endif
  !
  call sic_descriptor(varout,descout,found)
  if (.not.found) then
    call sic_message(seve%e,rname,'Output Variable Non Existent')
    error = .true.
    return
  elseif (descout%status.eq.scratch_operand) then
    call sic_message(seve%e,rname,  &
      'Non contiguous sub-array not supported for the output variable')
    call sic_volatile(descout)
    error = .true.
    return
  elseif (descout%readonly) then
    call sic_message(seve%e,rname,'Output variable is Read Only')
    error = .true.
    return
  elseif (descout%type.ne.fmt_l) then
    call sic_message(seve%e,rname,'Output Variable must be logical')
    error = .true.
    return
  endif
  !
  ! Check dimensions
  if (one) then
    ! Only 1 bit to test (per element of the input array)
    if (descout%ndim.ne.descin%ndim) then
      write(mess,'(A,I0)')  'Output variable must be of rank ',descin%ndim
      call sic_message(seve%e,rname,mess)
      error = .true.
    elseif (any(descout%dims(1:descout%ndim).ne.descin%dims(1:descin%ndim))) then
      call sic_message(seve%e,rname,'Dimensions mismatch')
      error = .true.
    endif
  else
    ! All bits to test (for each element of the input array)
    if (descout%ndim.ne.descin%ndim+1) then
      write(mess,'(A,I0)')  'Output variable must be of rank ',descin%ndim+1
      call sic_message(seve%e,rname,mess)
      error = .true.
    elseif (descout%dims(1).ne.nbit) then
      write(mess,'(A,I0)')  'First dimension of output variable must be ',nbit
      call sic_message(seve%e,rname,mess)
      error = .true.
    elseif (any(descout%dims(2:descout%ndim).ne.descin%dims(1:descin%ndim))) then
      call sic_message(seve%e,rname,'Dimensions mismatch')
      error = .true.
    endif
  endif
  if (error)  return
  !
  ! Transfer to output variable
  ipnta = gag_pointer(descin%addr,memory)
  ipntb = gag_pointer(descout%addr,memory)
  do ielem=1,desc_nelem(descin)
    if (one) then
      call l4tol4(btest(memory(ipnta),ibit-1),memory(ipntb),1)
      ipntb = ipntb+1
    else
      do ibit=1,nbit
        ! NB: 'POS' argument in BTEST is numbered C-like
        call l4tol4(btest(memory(ipnta),ibit-1),memory(ipntb),1)
        ipntb = ipntb+1
      enddo
    endif
    ipnta = ipnta+nbyte/4
  enddo
  !
end subroutine compute_btest
!
subroutine compute_isasicvar (line,error)
  use gildas_def
  use sic_dependencies_interfaces
  use sic_dictionaries
  use sic_interfaces, except_this=>compute_isasicvar
  use sic_types
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! COMPUTE Out IS_A_SIC_VAR In
  !   Test whether In is a known SIC variable
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  logical, parameter :: global=.true.
  character(len=filename_length) :: varin
  character(len=varname_length) :: varout
  integer(kind=address_length) :: ipntb
  type(sic_descriptor_t) :: descin,descout
  integer(kind=4) :: form, nc
  logical :: found, exist
  !
  call sic_ch (line,0,3,varin,nc,.true.,error)
  if (error) return
  call sic_upper(varin)
  call sic_ke (line,0,1,varout,nc,.true.,error)
  if (error) return
  !
  found = .true.
  exist = .true.
  call sic_materialize(varin,descin,exist)
  !
  call sic_descriptor(varout,descout,found)
  if (found) then
    form = descout%type
    if (form.ne.fmt_l) then
      call sic_message(seve%e,'IS_A_SIC_VAR','Existing Output Variable is not Logical')
      call sic_volatile(descin)
      error = .true.
      return
    endif
  else
    form = fmt_l
    call sic_defvariable(form,varout,global,error)
    if (error) goto 100
    call sic_descriptor(varout,descout,found)
  endif
  !
  ipntb = gag_pointer(descout%addr,memory)
  call i4toi4(exist,memory(ipntb),1)
  !
100 continue
  !
  call sic_volatile(descin)
end subroutine compute_isasicvar
!
subroutine gather_r4(out,n,in,m,vblank4,eblank4)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)    :: m        !
  real(kind=4),              intent(inout) :: out(m)   !
  integer(kind=size_length), intent(out)   :: n        !
  real(kind=4),              intent(in)    :: in(m)    !
  real(kind=4),              intent(in)    :: vblank4  !
  real(kind=4),              intent(in)    :: eblank4  !
  ! Local
  integer(kind=size_length) :: i,j,k,l
  !
  n = 0
  j = 0
  do i=1,m
    if (in(i).eq.in(i)) then
      if (eblank4.lt.0.) then
        n = 1
        out(1) = in(i)
        j = i
        exit
      else
        if (abs(in(i)-vblank4).gt.eblank4) then
          n = 1
          out(1) = in(i)
          j = i
          exit
        endif
      endif
    endif
  enddo
  if (n.eq.0) return
  !
  do i=j+1,m
    if (in(i).eq.in(i)) then
      l = 0
      do k=1,n
         if (in(i).eq.out(k)) then
            l = k
            exit
         endif
      enddo
      !
      if (l.eq.0) then
        n = n+1
        out(n) = in(i)
      endif
    endif
  enddo
  !
end subroutine gather_r4
!
subroutine gather_r8(out,n,in,m,vblank8,eblank8)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)    :: m        !
  real(kind=8),              intent(inout) :: out(m)   !
  integer(kind=size_length), intent(out)   :: n        !
  real(kind=8),              intent(in)    :: in(m)    !
  real(kind=8),              intent(in)    :: vblank8  !
  real(kind=8),              intent(in)    :: eblank8  !
  ! Local
  integer(kind=size_length) :: i,j,k,l
  !
  n = 0
  j = 0
  do i=1,m
    if (in(i).eq.in(i)) then
      if (eblank8.lt.0.d0) then
        n = 1
        out(1) = in(i)
        j = i
        exit
      else
        if (abs(in(i)-vblank8).gt.eblank8) then
          n = 1
          out(1) = in(i)
          j = i
          exit
        endif
      endif
    endif
  enddo
  if (n.eq.0) return
  !
  do i=j+1,m
    if (in(i).eq.in(i)) then
      l = 0
      do k=1,n
         if (in(i).eq.out(k)) then
            l = k
            exit
         endif
      enddo
      if (l.eq.0) then
        n = n+1
        out(n) = in(i)
      endif
    endif
  enddo
  !
end subroutine gather_r8
