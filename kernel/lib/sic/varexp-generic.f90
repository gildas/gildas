!-----------------------------------------------------------------------
! Entry points for SIC_DEF_REAL
!
subroutine sic_def_real_0d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel       ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel,ndim,jdim,readonly,error)
end subroutine sic_def_real_0d_1i4
!
subroutine sic_def_real_0d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel       ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel,ndim,jdim,readonly,error)
end subroutine sic_def_real_0d_1i8
!
subroutine sic_def_real_0d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=4),                intent(in)    :: rel        ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_real_addr(symb,rel,ndim,dim,readonly,error)
end subroutine sic_def_real_0d_nil
!
subroutine sic_def_real_1d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel(:)    ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1),ndim,jdim,readonly,error)
end subroutine sic_def_real_1d_1i4
!
subroutine sic_def_real_1d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel(:)    ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1),ndim,jdim,readonly,error)
end subroutine sic_def_real_1d_1i8
!
subroutine sic_def_real_1d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=4),                intent(in)    :: rel(:)     ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_real_addr(symb,rel(1),ndim,dim,readonly,error)
end subroutine sic_def_real_1d_nil
!
subroutine sic_def_real_2d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_2d_1i4
!
subroutine sic_def_real_2d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_2d_1i8
!
subroutine sic_def_real_2d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=4),                intent(in)    :: rel(:,:)   ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_real_addr(symb,rel(1,1),ndim,dim,readonly,error)
end subroutine sic_def_real_2d_nil
!
subroutine sic_def_real_3d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb        ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim         ! Dimensions
  logical,          intent(in)    :: readonly    ! Read only?
  logical,          intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_3d_1i4
!
subroutine sic_def_real_3d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb        ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim         ! Dimensions
  logical,          intent(in)    :: readonly    ! Read only?
  logical,          intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_3d_1i8
!
subroutine sic_def_real_3d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb        ! Variable name
  real(kind=4),                intent(in)    :: rel(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim        ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)   ! Dimensions
  logical,                     intent(in)    :: readonly    ! Read only?
  logical,                     intent(inout) :: error       ! Logical error flag
  !
  call sic_def_real_addr(symb,rel(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_real_3d_nil
!
subroutine sic_def_real_4d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb          ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim           ! Dimensions
  logical,          intent(in)    :: readonly      ! Read only?
  logical,          intent(inout) :: error         ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_4d_1i4
!
subroutine sic_def_real_4d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb          ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim           ! Dimensions
  logical,          intent(in)    :: readonly      ! Read only?
  logical,          intent(inout) :: error         ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_4d_1i8
!
subroutine sic_def_real_4d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb          ! Variable name
  real(kind=4),                intent(in)    :: rel(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim          ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)     ! Dimensions
  logical,                     intent(in)    :: readonly      ! Read only?
  logical,                     intent(inout) :: error         ! Logical error flag
  !
  call sic_def_real_addr(symb,rel(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_real_4d_nil
!
subroutine sic_def_real_5d_1i4(symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb            ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim             ! Dimensions
  logical,          intent(in)    :: readonly        ! Read only?
  logical,          intent(inout) :: error           ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_5d_1i4
!
subroutine sic_def_real_5d_1i8 (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb            ! Variable name
  real(kind=4),     intent(in)    :: rel(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim             ! Dimensions
  logical,          intent(in)    :: readonly        ! Read only?
  logical,          intent(inout) :: error           ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_real_addr(symb,rel(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_real_5d_1i8
!
subroutine sic_def_real_5d_nil (symb,rel,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_real
  !  Define a REAL Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb            ! Variable name
  real(kind=4),                intent(in)    :: rel(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim            ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)       ! Dimensions
  logical,                     intent(in)    :: readonly        ! Read only?
  logical,                     intent(inout) :: error           ! Logical error flag
  !
  call sic_def_real_addr(symb,rel(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_real_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_DBLE
!
subroutine sic_def_dble_0d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=8),     intent(in)    :: dble      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble,ndim,jdim,readonly,error)
end subroutine sic_def_dble_0d_1i4
!
subroutine sic_def_dble_0d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=8),     intent(in)    :: dble      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble,ndim,jdim,readonly,error)
end subroutine sic_def_dble_0d_1i8
!
subroutine sic_def_dble_0d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=8),                intent(in)    :: dble       ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble,ndim,dim,readonly,error)
end subroutine sic_def_dble_0d_nil
!
subroutine sic_def_dble_1d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=8),     intent(in)    :: dble(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_1d_1i4
!
subroutine sic_def_dble_1d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  real(kind=8),     intent(in)    :: dble(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_1d_1i8
!
subroutine sic_def_dble_1d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=8),                intent(in)    :: dble(:)    ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble(1),ndim,dim,readonly,error)
end subroutine sic_def_dble_1d_nil
!
subroutine sic_def_dble_2d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_2d_1i4
!
subroutine sic_def_dble_2d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_2d_1i8
!
subroutine sic_def_dble_2d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  real(kind=8),                intent(in)    :: dble(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble(1,1),ndim,dim,readonly,error)
end subroutine sic_def_dble_2d_nil
!
subroutine sic_def_dble_3d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_3d_1i4
!
subroutine sic_def_dble_3d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_3d_1i8
!
subroutine sic_def_dble_3d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb         ! Variable name
  real(kind=8),                intent(in)    :: dble(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
  logical,                     intent(in)    :: readonly     ! Read only?
  logical,                     intent(inout) :: error        ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_dble_3d_nil
!
subroutine sic_def_dble_4d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_4d_1i4
!
subroutine sic_def_dble_4d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_4d_1i8
!
subroutine sic_def_dble_4d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb           ! Variable name
  real(kind=8),                intent(in)    :: dble(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
  logical,                     intent(in)    :: readonly       ! Read only?
  logical,                     intent(inout) :: error          ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_dble_4d_nil
!
subroutine sic_def_dble_5d_1i4(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_5d_1i4
!
subroutine sic_def_dble_5d_1i8(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  real(kind=8),     intent(in)    :: dble(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_dble_addr(symb,dble(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_dble_5d_1i8
!
subroutine sic_def_dble_5d_nil(symb,dble,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_dble
  !  Define a DOUBLE Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb             ! Variable name
  real(kind=8),                intent(in)    :: dble(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
  logical,                     intent(in)    :: readonly         ! Read only?
  logical,                     intent(inout) :: error            ! Logical error flag
  !
  call sic_def_dble_addr(symb,dble(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_dble_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_INTE
!
subroutine sic_def_inte_0d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=4),  intent(in)    :: inte      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte,ndim,jdim,readonly,error)
end subroutine sic_def_inte_0d_1i4
!
subroutine sic_def_inte_0d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=4),  intent(in)    :: inte      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte,ndim,jdim,readonly,error)
end subroutine sic_def_inte_0d_1i8
!
subroutine sic_def_inte_0d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=4),             intent(in)    :: inte       ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte,ndim,dim,readonly,error)
end subroutine sic_def_inte_0d_nil
!
subroutine sic_def_inte_1d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=4),  intent(in)    :: inte(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_1d_1i4
!
subroutine sic_def_inte_1d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=4),  intent(in)    :: inte(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_1d_1i8
!
subroutine sic_def_inte_1d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=4),             intent(in)    :: inte(:)    ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte(1),ndim,dim,readonly,error)
end subroutine sic_def_inte_1d_nil
!
subroutine sic_def_inte_2d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_2d_1i4
!
subroutine sic_def_inte_2d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_2d_1i8
!
subroutine sic_def_inte_2d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=4),             intent(in)    :: inte(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte(1,1),ndim,dim,readonly,error)
end subroutine sic_def_inte_2d_nil
!
subroutine sic_def_inte_3d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_3d_1i4
!
subroutine sic_def_inte_3d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_3d_1i8
!
subroutine sic_def_inte_3d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb         ! Variable name
  integer(kind=4),             intent(in)    :: inte(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
  logical,                     intent(in)    :: readonly     ! Read only?
  logical,                     intent(inout) :: error        ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_inte_3d_nil
!
subroutine sic_def_inte_4d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_4d_1i4
!
subroutine sic_def_inte_4d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_4d_1i8
!
subroutine sic_def_inte_4d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb           ! Variable name
  integer(kind=4),             intent(in)    :: inte(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
  logical,                     intent(in)    :: readonly       ! Read only?
  logical,                     intent(inout) :: error          ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_inte_4d_nil
!
subroutine sic_def_inte_5d_1i4(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_5d_1i4
!
subroutine sic_def_inte_5d_1i8(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  integer(kind=4),  intent(in)    :: inte(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_inte_addr(symb,inte(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_inte_5d_1i8
!
subroutine sic_def_inte_5d_nil(symb,inte,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_inte
  !  Define an INTEGER Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb             ! Variable name
  integer(kind=4),             intent(in)    :: inte(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
  logical,                     intent(in)    :: readonly         ! Read only?
  logical,                     intent(inout) :: error            ! Logical error flag
  !
  call sic_def_inte_addr(symb,inte(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_inte_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_LONG
!
subroutine sic_def_long_0d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=8),  intent(in)    :: long      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long,ndim,jdim,readonly,error)
end subroutine sic_def_long_0d_1i4
!
subroutine sic_def_long_0d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=8),  intent(in)    :: long      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long,ndim,jdim,readonly,error)
end subroutine sic_def_long_0d_1i8
!
subroutine sic_def_long_0d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=8),             intent(in)    :: long       ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_long_addr(symb,long,ndim,dim,readonly,error)
end subroutine sic_def_long_0d_nil
!
subroutine sic_def_long_1d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=8),  intent(in)    :: long(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1),ndim,jdim,readonly,error)
end subroutine sic_def_long_1d_1i4
!
subroutine sic_def_long_1d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  integer(kind=8),  intent(in)    :: long(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1),ndim,jdim,readonly,error)
end subroutine sic_def_long_1d_1i8
!
subroutine sic_def_long_1d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=8),             intent(in)    :: long(:)    ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_long_addr(symb,long(1),ndim,dim,readonly,error)
end subroutine sic_def_long_1d_nil
!
subroutine sic_def_long_2d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_2d_1i4
!
subroutine sic_def_long_2d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_2d_1i8
!
subroutine sic_def_long_2d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  integer(kind=8),             intent(in)    :: long(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_long_addr(symb,long(1,1),ndim,dim,readonly,error)
end subroutine sic_def_long_2d_nil
!
subroutine sic_def_long_3d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_3d_1i4
!
subroutine sic_def_long_3d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_3d_1i8
!
subroutine sic_def_long_3d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb         ! Variable name
  integer(kind=8),             intent(in)    :: long(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
  logical,                     intent(in)    :: readonly     ! Read only?
  logical,                     intent(inout) :: error        ! Logical error flag
  !
  call sic_def_long_addr(symb,long(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_long_3d_nil
!
subroutine sic_def_long_4d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_4d_1i4
!
subroutine sic_def_long_4d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_4d_1i8
!
subroutine sic_def_long_4d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb           ! Variable name
  integer(kind=8),             intent(in)    :: long(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
  logical,                     intent(in)    :: readonly       ! Read only?
  logical,                     intent(inout) :: error          ! Logical error flag
  !
  call sic_def_long_addr(symb,long(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_long_4d_nil
!
subroutine sic_def_long_5d_1i4(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_5d_1i4
!
subroutine sic_def_long_5d_1i8(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  integer(kind=8),  intent(in)    :: long(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_long_addr(symb,long(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_long_5d_1i8
!
subroutine sic_def_long_5d_nil(symb,long,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_long
  !  Define a LONG Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb             ! Variable name
  integer(kind=8),             intent(in)    :: long(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
  logical,                     intent(in)    :: readonly         ! Read only?
  logical,                     intent(inout) :: error            ! Logical error flag
  !
  call sic_def_long_addr(symb,long(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_long_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_CPLX
!
subroutine sic_def_cplx_0d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  complex(kind=4),  intent(in)    :: cplx      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx,ndim,jdim,readonly,error)
end subroutine sic_def_cplx_0d_1i4
!
subroutine sic_def_cplx_0d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  complex(kind=4),  intent(in)    :: cplx      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx,ndim,jdim,readonly,error)
end subroutine sic_def_cplx_0d_1i8
!
subroutine sic_def_cplx_0d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  complex(kind=4),             intent(in)    :: cplx       ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx,ndim,dim,readonly,error)
end subroutine sic_def_cplx_0d_nil
!
subroutine sic_def_cplx_1d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_1d_1i4
!
subroutine sic_def_cplx_1d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_1d_1i8
!
subroutine sic_def_cplx_1d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  complex(kind=4),             intent(in)    :: cplx(:)    ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx(1),ndim,dim,readonly,error)
end subroutine sic_def_cplx_1d_nil
!
subroutine sic_def_cplx_2d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_2d_1i4
!
subroutine sic_def_cplx_2d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_2d_1i8
!
subroutine sic_def_cplx_2d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  complex(kind=4),             intent(in)    :: cplx(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx(1,1),ndim,dim,readonly,error)
end subroutine sic_def_cplx_2d_nil
!
subroutine sic_def_cplx_3d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_3d_1i4
!
subroutine sic_def_cplx_3d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_3d_1i8
!
subroutine sic_def_cplx_3d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb         ! Variable name
  complex(kind=4),             intent(in)    :: cplx(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
  logical,                     intent(in)    :: readonly     ! Read only?
  logical,                     intent(inout) :: error        ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_cplx_3d_nil
!
subroutine sic_def_cplx_4d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_4d_1i4
!
subroutine sic_def_cplx_4d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_4d_1i8
!
subroutine sic_def_cplx_4d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb           ! Variable name
  complex(kind=4),             intent(in)    :: cplx(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
  logical,                     intent(in)    :: readonly       ! Read only?
  logical,                     intent(inout) :: error          ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_cplx_4d_nil
!
subroutine sic_def_cplx_5d_1i4(symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_5d_1i4
!
subroutine sic_def_cplx_5d_1i8 (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  complex(kind=4),  intent(in)    :: cplx(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_cplx_addr(symb,cplx(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_cplx_5d_1i8
!
subroutine sic_def_cplx_5d_nil (symb,cplx,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_cplx
  !  Define a COMPLEX Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb             ! Variable name
  complex(kind=4),             intent(in)    :: cplx(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
  logical,                     intent(in)    :: readonly         ! Read only?
  logical,                     intent(inout) :: error            ! Logical error flag
  !
  call sic_def_cplx_addr(symb,cplx(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_cplx_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_LOGIN
!
subroutine sic_def_login_0d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  logical,          intent(in)    :: logi      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi,ndim,jdim,readonly,error)
end subroutine sic_def_login_0d_1i4
!
subroutine sic_def_login_0d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  logical,          intent(in)    :: logi      ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi,ndim,jdim,readonly,error)
end subroutine sic_def_login_0d_1i8
!
subroutine sic_def_login_0d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  logical,                     intent(in)    :: logi       ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_login_addr(symb,logi,ndim,dim,readonly,error)
end subroutine sic_def_login_0d_nil
!
subroutine sic_def_login_1d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  logical,          intent(in)    :: logi(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1),ndim,jdim,readonly,error)
end subroutine sic_def_login_1d_1i4
!
subroutine sic_def_login_1d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  logical,          intent(in)    :: logi(:)   ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1),ndim,jdim,readonly,error)
end subroutine sic_def_login_1d_1i8
!
subroutine sic_def_login_1d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  logical,                     intent(in)    :: logi(:)    ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_login_addr(symb,logi(1),ndim,dim,readonly,error)
end subroutine sic_def_login_1d_nil
!
subroutine sic_def_login_2d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  logical,          intent(in)    :: logi(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_2d_1i4
!
subroutine sic_def_login_2d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb       ! Variable name
  logical,          intent(in)    :: logi(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim        ! Dimensions
  logical,          intent(in)    :: readonly   ! Read only?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_2d_1i8
!
subroutine sic_def_login_2d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  logical,                     intent(in)    :: logi(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_login_addr(symb,logi(1,1),ndim,dim,readonly,error)
end subroutine sic_def_login_2d_nil
!
subroutine sic_def_login_3d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  logical,          intent(in)    :: logi(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_3d_1i4
!
subroutine sic_def_login_3d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb         ! Variable name
  logical,          intent(in)    :: logi(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim          ! Dimensions
  logical,          intent(in)    :: readonly     ! Read only?
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_3d_1i8
!
subroutine sic_def_login_3d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb         ! Variable name
  logical,                     intent(in)    :: logi(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
  logical,                     intent(in)    :: readonly     ! Read only?
  logical,                     intent(inout) :: error        ! Logical error flag
  !
  call sic_def_login_addr(symb,logi(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_login_3d_nil
!
subroutine sic_def_login_4d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  logical,          intent(in)    :: logi(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_4d_1i4
!
subroutine sic_def_login_4d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb           ! Variable name
  logical,          intent(in)    :: logi(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim            ! Dimensions
  logical,          intent(in)    :: readonly       ! Read only?
  logical,          intent(inout) :: error          ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_4d_1i8
!
subroutine sic_def_login_4d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb           ! Variable name
  logical,                     intent(in)    :: logi(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
  logical,                     intent(in)    :: readonly       ! Read only?
  logical,                     intent(inout) :: error          ! Logical error flag
  !
  call sic_def_login_addr(symb,logi(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_login_4d_nil
!
subroutine sic_def_login_5d_1i4(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  logical,          intent(in)    :: logi(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_5d_1i4
!
subroutine sic_def_login_5d_1i8(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb             ! Variable name
  logical,          intent(in)    :: logi(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim              ! Dimensions
  logical,          intent(in)    :: readonly         ! Read only?
  logical,          intent(inout) :: error            ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_login_addr(symb,logi(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_login_5d_1i8
!
subroutine sic_def_login_5d_nil(symb,logi,ndim,dim,readonly,error)
  use gildas_def
  use gbl_format
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_login
  !  Define a LOGICAL array Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb             ! Variable name
  logical,                     intent(in)    :: logi(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
  logical,                     intent(in)    :: readonly         ! Read only?
  logical,                     intent(inout) :: error            ! Logical error flag
  !
  call sic_def_login_addr(symb,logi(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_login_5d_nil
!
!-----------------------------------------------------------------------
! Entry points for SIC_DEF_CHARN
!
subroutine sic_def_charn_0d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  character(len=*), intent(in)    :: chain     ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain,ndim,jdim,readonly,error)
end subroutine sic_def_charn_0d_1i4
!
subroutine sic_def_charn_0d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  Scalar target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  character(len=*), intent(in)    :: chain     ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain,ndim,jdim,readonly,error)
end subroutine sic_def_charn_0d_1i8
!
subroutine sic_def_charn_0d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  Scalar target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  character(len=*),            intent(in)    :: chain      ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain,ndim,dim,readonly,error)
end subroutine sic_def_charn_0d_nil
!
subroutine sic_def_charn_1d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  character(len=*), intent(in)    :: chain(:)  ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_1d_1i4
!
subroutine sic_def_charn_1d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  1D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb      ! Variable name
  character(len=*), intent(in)    :: chain(:)  ! Target
  integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim       ! Dimensions
  logical,          intent(in)    :: readonly  ! Read only?
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_1d_1i8
!
subroutine sic_def_charn_1d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  1D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb       ! Variable name
  character(len=*),            intent(in)    :: chain(:)   ! Target
  integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
  logical,                     intent(in)    :: readonly   ! Read only?
  logical,                     intent(inout) :: error      ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain(1),ndim,dim,readonly,error)
end subroutine sic_def_charn_1d_nil
!
subroutine sic_def_charn_2d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb        ! Variable name
  character(len=*), intent(in)    :: chain(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim         ! Dimensions
  logical,          intent(in)    :: readonly    ! Read only?
  logical,          intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_2d_1i4
!
subroutine sic_def_charn_2d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  2D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb        ! Variable name
  character(len=*), intent(in)    :: chain(:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim         ! Dimensions
  logical,          intent(in)    :: readonly    ! Read only?
  logical,          intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_2d_1i8
!
subroutine sic_def_charn_2d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  2D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb        ! Variable name
  character(len=*),            intent(in)    :: chain(:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim        ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)   ! Dimensions
  logical,                     intent(in)    :: readonly    ! Read only?
  logical,                     intent(inout) :: error       ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain(1,1),ndim,dim,readonly,error)
end subroutine sic_def_charn_2d_nil
!
subroutine sic_def_charn_3d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb          ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim           ! Dimensions
  logical,          intent(in)    :: readonly      ! Read only?
  logical,          intent(inout) :: error         ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_3d_1i4
!
subroutine sic_def_charn_3d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  3D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb          ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim           ! Dimensions
  logical,          intent(in)    :: readonly      ! Read only?
  logical,          intent(inout) :: error         ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_3d_1i8
!
subroutine sic_def_charn_3d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  3D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb          ! Variable name
  character(len=*),            intent(in)    :: chain(:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim          ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)     ! Dimensions
  logical,                     intent(in)    :: readonly      ! Read only?
  logical,                     intent(inout) :: error         ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain(1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_charn_3d_nil
!
subroutine sic_def_charn_4d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb            ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim             ! Dimensions
  logical,          intent(in)    :: readonly        ! Read only?
  logical,          intent(inout) :: error           ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_4d_1i4
!
subroutine sic_def_charn_4d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  4D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb            ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim             ! Dimensions
  logical,          intent(in)    :: readonly        ! Read only?
  logical,          intent(inout) :: error           ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_4d_1i8
!
subroutine sic_def_charn_4d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  4D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb            ! Variable name
  character(len=*),            intent(in)    :: chain(:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim            ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)       ! Dimensions
  logical,                     intent(in)    :: readonly        ! Read only?
  logical,                     intent(inout) :: error           ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain(1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_charn_4d_nil
!
subroutine sic_def_charn_5d_1i4(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 4
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb              ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim              ! Number of dimensions
  integer(kind=4),  intent(in)    :: dim               ! Dimensions
  logical,          intent(in)    :: readonly          ! Read only?
  logical,          intent(inout) :: error             ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_5d_1i4
!
subroutine sic_def_charn_5d_1i8(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  5D target
  !  Dimension as a scalar integer of kind 8
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: symb              ! Variable name
  character(len=*), intent(in)    :: chain(:,:,:,:,:)  ! Target
  integer(kind=4),  intent(in)    :: ndim              ! Number of dimensions
  integer(kind=8),  intent(in)    :: dim               ! Dimensions
  logical,          intent(in)    :: readonly          ! Read only?
  logical,          intent(inout) :: error             ! Logical error flag
  ! Local
  integer(kind=index_length) :: jdim(sic_maxdims)
  !
  jdim(1) = dim
  call sic_def_charn_addr(symb,chain(1,1,1,1,1),ndim,jdim,readonly,error)
end subroutine sic_def_charn_5d_1i8
!
subroutine sic_def_charn_5d_nil(symb,chain,ndim,dim,readonly,error)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public-generic sic_def_charn
  !  Define a CHARACTER array Sic variable
  !---
  !  5D target
  !  Dimensions as an integer array of kind index_length
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: symb              ! Variable name
  character(len=*),            intent(in)    :: chain(:,:,:,:,:)  ! Target
  integer(kind=4),             intent(in)    :: ndim              ! Number of dimensions
  integer(kind=index_length),  intent(in)    :: dim(ndim)         ! Dimensions
  logical,                     intent(in)    :: readonly          ! Read only?
  logical,                     intent(inout) :: error             ! Logical error flag
  !
  call sic_def_charn_addr(symb,chain(1,1,1,1,1),ndim,dim,readonly,error)
end subroutine sic_def_charn_5d_nil
