subroutine init_stack
  use sic_interfaces, except_this=>init_stack
  use sic_dependencies_interfaces
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !       Initialize the stack
  ! To be revised for general use
  !---------------------------------------------------------------------
  ! Local
  integer :: maxwrd,ier
  !
  maxwrd = 2560
  maxbyt = 4*maxwrd
  ier = sic_getvm(maxwrd,stack_addr)
  if (ier.ne.1) then
    call sic_message(seve%f,'STACK','Error allocating stack')
    call sysexi (ier)
  endif
  iloc = stack_addr
  iend = 0
  istart = 1
  jstart = stack_addr
end subroutine init_stack
!
subroutine edit_stack(error,edit)
  use sic_interfaces, except_this=>edit_stack
  use gildas_def
  use sic_structures
  use sic_interactions
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine : edit the stack using the default text
  !       editor specified by the user
  !---------------------------------------------------------------------
  logical :: error                  !
  logical :: edit                   !
  ! Local
  character(len=*), parameter :: rname='EDIT'
  integer(kind=4) :: nc
  integer(kind=address_length) :: addr
  integer :: i,ier,j,ll
  character(len=20) :: fich,file
  character(len=1024) :: local_line
  !
  ! Dump the stack on a file with a unique name.
  file = 'stack'
  call sic_parsef(file,fich,' ',sicext(1))
  call sic_message(seve%i,rname,'Writing stack content on '//fich)
  ier = sic_open(luntem,fich,'NEW',.false.)
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Error writing stack content on '//fich)
    call putios('E-EDIT, ',ier)
    close (luntem)
    error = .true.
    return
  endif
  do i=istart,iend
    j = mod(i-1,maxlin)+1
    nc = stack_desc(1,j) ! Length
    addr = stack_desc(2,j) ! Address
    call destoc(nc,addr,local_line)
    ll = stack_desc(1,j)
    call wstack(luntem,local_line,ll,' ',0)
  enddo
  !
  ! Edit the file
  close (luntem)
  if (edit) call editor (fich,error)
end subroutine edit_stack
!
subroutine sic_recall (line,nline,iline,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_recall
  use gildas_def
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Recall a command line from the stack
  !---------------------------------------------------------------------
  character(len=*), intent(out)   :: line   ! Recalled command
  integer(kind=4),  intent(out)   :: nline  ! Length of line
  integer(kind=4),  intent(inout) :: iline  ! Line number
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=address_length) ::  addr
  integer(kind=4) :: jline
  character(len=message_length) :: chain
  !
  if (iend.eq.0) return
  if (iline.eq.0) iline = iend
  if (iline.lt.istart) then
    write (chain,'(A,I0)') 'Non existent line in buffer #',iline
    call sic_message(seve%w,'RECALL',chain)
    error = .true.
    return
  endif
  !
  iline = min(iline,iend)
  jline = mod(iline-1,maxlin)+1
  nline = stack_desc(1,jline)
  addr  = stack_desc(2,jline)
  call destoc (nline,addr,line)
  !
end subroutine sic_recall
!
subroutine sic_type(line,tofile,goto_code,error)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_type
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !  TYPE [FileIn] [/OUTPUT FileOut]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line       ! Input command line
  logical,          intent(in)    :: tofile     ! Is /OUTPUT allowed?
  integer,          intent(out)   :: goto_code  !
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='TYPE'
  character(len=filename_length) :: file,filein,fileout
  integer :: ier,nc,lun
  logical :: input,output,found
  !
  goto_code = 10  ! Do not put in stack, handle error directly
  !
  output = sic_present(1,0)
  if (output) then
    call sic_ch(line,1,1,file,nc,.true.,error)
    if (error)  return
    call sic_parsef(file,fileout,' ','.log')
  endif
  !
  if (output .and. .not.tofile) then
    call sic_message(seve%w,rname,'/OUTPUT invalid in this context, ignored')
    ! Not an error, goto 10
    return
  endif
  !
  input = sic_present(0,1)
  if (input) then
    call sic_ch(line,0,1,filein,nc,.true.,error)
    if (error) return
  endif
  !
  if (input .and. output) then
    ! TYPE FileIn /OUTPUT FileOut: this is a copy!
    call sic_message(seve%e,rname,  &
      'Forbidden: use SIC COPY if you want to copy files')
    error = .true.
    return
  endif
  !
  if (output) then  ! TYPE [FileIn] /OUTPUT FileOut
    ! Get a logical unit
    ier = sic_getlun(lun)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    ! Open
    ier = sic_open(lun,fileout,'NEW',.false.)
    if (ier.ne.0) then
      call putios('E-SIC, ',ier)
      error = .true.
      call sic_frelun(lun)
      return
    endif
  else
    lun = 6  ! STDOUT
  endif
  !
  if (input) then
    call find_procedure(filein,file,found)
    if (.not.found) then
      call sic_message(seve%e,rname,'No such procedure '//filein)
      error = .true.
      return
    endif
    !
    nc = len_trim(file)
    call type_file(file,nc,lun,error)
    !
    goto_code = 50  ! Put command in stack + error handling
  else
    call type_stack(lun)
    if (lun.ne.6)  goto_code = 50  ! Put command in stack + error handling
  endif
  !
  if (lun.ne.6) then
    close (unit=lun)
    call sic_frelun(lun)
  endif
  !
end subroutine sic_type
!
subroutine wstack(lun,line,nl,chain,nc)
  use sic_interfaces, except_this=>wstack
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Write a line of the stack to a file
  !       Avoid line overflows by using the continuation flag '-'
  ! Arguments :
  !       LUN     I       Logical unit number		Input
  !       LINE    C*(*)   Command line			Input
  !       NL      I       Length of line			Input
  !	CHAIN	C*(*)	String inserted after command	Input
  !	NC	I	Length of CHAIN			Input
  !---------------------------------------------------------------------
  integer :: lun                    !
  character(len=*) :: line          !
  integer :: nl                     !
  character(len=*) :: chain         !
  integer :: nc                     !
  ! Local
  integer :: n1,nr,nwu
  integer, parameter :: nw=78
  !
  nwu = nw-nc                  ! Usable width, reserve right margin for CHAIN, if defined
  n1 = 1                       ! First char to be printed on next line
  nr = nl                      ! Remaining line width to be printed
  !
  do while (nr.gt.nwu)
    write(lun,'(A,''-'')') line(n1:n1+nwu-2)   ! Keep a place for '-' !
    n1 = n1+nwu-1
    nr = nl-n1+1
  enddo
  !
  if (nc.eq.0) then
    write(lun,'(A)') line(n1:n1+nr-1)
  elseif (nr.le.nw/2) then
    write(lun,'(A,T40,A)') line(n1:n1+nr-1),chain(1:nc)    ! NW/2=39
  else
    write(lun,'(A,A)') line(n1:n1+nr-1),chain(1:nc)
  endif
  !
end subroutine wstack
!
subroutine sic_insert (line)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_insert
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  !       Insert a command line in the stack if possible.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line  ! Command line
  ! Local
  integer(kind=4) :: nline
  integer(kind=4) :: knlen,inlen,kstart,maclen,tnlen
  character(len=commandline_length) :: tline
  !
  ! Entry for automatic insertion to the end of the Stack
  if (.not.logbuf) return
  inlen = len_trim(line)
  goto 10
  !
  ! Entry for insertion regardless of the LOGBUF value.
entry insert (line,nline)
  !----------------------------------------------------------------------
  ! SIC   Internal entry point
  !       Insert a command line in the stack, regardless of stack
  !       status.
  ! Argument
  !       LINE    C*(*)   Command line
  !       NLINE   I       Length of line
  !----------------------------------------------------------------------
  inlen = nline
  !
10 continue
  !
  call sic_translate_dollar(line,inlen,tline,tnlen)
  !
  knlen = (tnlen+3)/4    ! In words
  knlen = knlen*4
  iend = iend+1
  maclen = mod(iend-1,maxlin)+1
  if (iend.eq.maxlin+istart) then
    istart = istart+1
    kstart = mod(istart-1,maxlin)+1
    jstart = stack_desc(2,kstart)
  endif
  !
  if (iloc.lt.jstart) then
    do while (iloc+knlen.gt.jstart)
      istart = istart+1
      if (istart.eq.iend) goto 20
      kstart = mod(istart-1,maxlin)+1
      jstart = stack_desc(2,kstart)
    enddo
  endif
  if (iloc+knlen .gt. maxbyt+stack_addr-1) then
    iloc = stack_addr
    do while (iloc+knlen.gt.jstart)
      istart = istart+1
      if (istart.eq.iend) goto 20
      kstart = mod(istart-1,maxlin)+1
      jstart = stack_desc(2,kstart)
    enddo
  endif
  !
  ! Build a Descriptor for current line
20 continue
  stack_desc (1,maclen) = tnlen
  stack_desc (2,maclen) = iloc
  if (istart.eq.iend) jstart=iloc
  !
  ! Insert new command at location ILOC
  call ctodes (tline,tnlen,iloc)
  !
  ! Update stack pointers
  iloc = iloc+knlen
  !
contains
  !
  subroutine sic_translate_dollar(iline,niline,oline,noline)
    !---------------------------------------------------------------------
    ! Translate a $ command to SIC\SYSTEM, taking double-quotes properly
    ! into account.
    !---------------------------------------------------------------------
    character(len=*), intent(in)  :: iline
    integer(kind=4),  intent(in)  :: niline
    character(len=*), intent(out) :: oline
    integer(kind=4),  intent(out) :: noline
    ! Local
    integer(kind=4) :: nl
    !
    if (iline(1:1).ne.'$') then
      oline = iline
      noline = niline
      return
    endif
    !
    oline = 'SIC\SYSTEM "'  ! Important: this also adds trailing blanks
    noline = 12
    !
    do nl=2,niline
      noline = noline+1
      if (iline(nl:nl).eq.'"') then
        oline(noline:noline+1) = '""'
        noline = noline+1
      else
        oline(noline:noline) = iline(nl:nl)
      endif
    enddo
    !
    noline = noline+1
    oline(noline:noline) = '"'
    !
  end subroutine sic_translate_dollar
  !
end subroutine sic_insert
!
subroutine type_stack(lun)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>type_stack
  use gildas_def
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !  List the stack onto the given logical unit (6 = STDOUT)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: lun  ! Output logical unit
  ! Local
  integer(kind=address_length) :: addr
  integer(kind=4) :: i,j,ll,nc
  character(len=8) :: chain
  character(len=1024) :: local_line
  !
  do i=istart,iend
    j = mod(i-1,maxlin)+1
    nc = stack_desc(1,j) ! Length
    addr = stack_desc(2,j) ! Address
    write(chain,'(A3,I5)') ' ! ',i
    call destoc(nc,addr,local_line)
    ll = stack_desc(1,j)
    call wstack(lun,local_line,ll,chain,8)
  enddo
  !
end subroutine type_stack
!
subroutine recfin(line,nline,iline,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>recfin
  use gildas_def
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !       Retrieve a command line from its first characters
  ! Arguments
  !       LINE    C*(*)   Abbreviation of the command     Input
  !       NLINE   I       Length of LINE                  Input
  !       ILINE   I       Command number                  Output
  !       ERROR   L       Logical error flag              Output
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  integer :: nline                  !
  integer :: iline                  !
  logical :: error                  !
  ! Local
  integer(kind=address_length) :: addr
  integer(kind=4) :: jline,nc
  character(len=1024) :: local_line
  !
  do iline=iend,istart,-1
    jline = mod(iline-1,maxlin)+1
    nc = stack_desc(1,jline)
    addr = stack_desc(2,jline)
    call destoc(nc,addr,local_line)
    if (cindex(line,nline,local_line)) return
  enddo
  call sic_message(seve%e,'RECALL','Command line not found')
  error = .true.
end subroutine recfin
!
function cindex(line,nline,chain)
  use sic_interfaces, except_this=>cindex
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !       Logical function called by RECFIN to determine whether
  !       the input line is an abbreviation of the command line.
  !       Skips language field if required.
  ! Arguments
  !       LINE    C*(*)   Input line
  !       NLINE   I       length of line
  !       CHAIN   C*(*)   Command line
  !---------------------------------------------------------------------
  logical :: cindex                 !
  character(len=*) :: line          !
  integer :: nline                  !
  character(len=*) :: chain         !
  ! Local
  integer :: i,k
  character(len=1), parameter :: backslash=char(92)
  !
  ! Skip language name, and compare
  k = index(line(1:nline),backslash)
  if (k.eq.0) then
    i = index(chain,backslash)
    if (index(chain,' ').lt.i) i = 0
  else
    i = 0
  endif
  cindex = sic_eqchain(chain(i+1:i+nline),line(1:nline))
end function cindex
!
subroutine type_file(file,nfile,lunout,error)
  use sic_interfaces, except_this=>type_file
  use sic_structures
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! SIC Internal routine
  ! Support routine for command:
  !       TYPE File_name
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: file    ! File name
  integer,          intent(in)    :: nfile   ! Length of file name
  integer,          intent(in)    :: lunout  ! Where to display the file
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='TYPE'
  character(len=256) :: line
  integer :: ier,n,j
  !
#if defined(WIN32)
  line = 'TYPE '//file(1:nfile)//'|more '
  ier = gag_system(line)
  return
#else
  !
  ier = sic_open (luntem,file,'OLD',.true.)
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Cannot open '//file(1:nfile))
    call putios('E-TYPE,  ',ier)
    error = .true.
    return
  endif
  j=1
  do while(.true.)
    read(luntem,'(A)',end=40,err=50) line
    n = len_trim(line)
    if (n.gt.0) then
      write(lunout,'(A)') line(1:n)
      if (line(n:n).ne.'-') j=j+1
    else
      write(lunout,'(A)')   ! No string, only carriage return
    endif
    !
    ! Every 20 lines, check for ^C
    if (mod(j,20).eq.0) then
      if (sic_ctrlc()) goto 40
    endif
  enddo
40 close(unit=luntem)
  return
  !
50 call sic_message(seve%e,rname,'Error reading '//file(1:nfile))
  close(unit=luntem)
  error=.true.
  return
  !
#endif
end subroutine type_file
