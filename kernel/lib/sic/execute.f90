subroutine sic_execute(line,error)
  use gildas_def
  use gbl_message
  use sic_interfaces, except_this=>sic_execute
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    SIC\EXECUTE "Command arg /option arg"
  !    SIC\EXECUTE 'stringvar'
  !  Execute a command line from a string or from a string variable.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='EXECUTE'
  character(len=commandline_length) :: command
  integer(kind=4) :: nc
  !
  if (sic_narg(0).gt.1) then
    call sic_message(seve%e,rname,  &
      'Trailing arguments. Command expects a single string.')
    error = .true.
    return
  endif
  !
  call sic_ch(line,0,1,command,nc,.true.,error)
  if (error)  return
  !
  if (command(1:1).eq.'@') then
    call exec_program(command)
  else
    call exec_command(command,error)
    if (error)  return
  endif
  !
end subroutine sic_execute
