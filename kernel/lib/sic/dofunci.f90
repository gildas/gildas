!*********************************************************************
! INTEGER*8 Generic External three argument function on vectors
!
subroutine do_long_loop3 (func,n,result,m1,oper1,m2,oper2,m3,oper3,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           external      :: func      !
  integer(kind=size_length)                :: n         !
  integer(kind=8)                          :: result(n) !
  integer(kind=size_length)                :: m1        !
  integer(kind=8)                          :: oper1(m1) !
  integer(kind=size_length)                :: m2        !
  integer(kind=8)                          :: oper2(m2) !
  integer(kind=size_length)                :: m3        !
  integer(kind=8)                          :: oper3(m3) !
  logical,                   intent(inout) :: error     !
  ! Local
  integer(kind=8) :: r,a,b,c
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1 .and. m2.eq.1 .and. m3.eq.1) then
    r = func(oper1(1),oper2(1),oper3(1))
    do i=1,n
      result(i) = r
    enddo
  elseif (m1.eq.n .and. m2.eq.1 .and. m3.eq.1) then
    b = oper2(1)
    c = oper3(1)
    do i=1,n
      result(i) = func(oper1(i),b,c)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n .and. m3.eq.1) then
    a = oper1(1)
    c = oper3(1)
    do i=1,n
      result(i) = func(a,oper2(i),c)
    enddo
  elseif (m1.eq.n .and. m2.eq.n .and. m3.eq.1) then
    c = oper3(1)
    do i=1,n
      result(i) = func(oper1(i),oper2(i),c)
    enddo
  elseif (m1.eq.1 .and. m2.eq.1 .and. m3.eq.n) then
    a = oper1(1)
    b = oper2(1)
    do i=1,n
      result(i) = func(a,b,oper3(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.1 .and. m3.eq.n) then
    b = oper2(1)
    do i=1,n
      result(i) = func(oper1(i),b,oper3(i))
    enddo
  elseif (m1.eq.1 .and. m2.eq.n .and. m3.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = func(a,oper2(i),oper3(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n .and. m3.eq.n) then
    do i=1,n
      result(i) = func(oper1(i),oper2(i),oper3(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2,m3
    call sic_message(seve%e,'I_FUNC',mess)
    error = .true.
  endif
end subroutine do_long_loop3
!
!*********************************************************************
! INTEGER*8 Generic External two argument function on vectors
!
subroutine do_long_loop2 (func,n,result,m1,oper1,m2,oper2,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           external      :: func      !
  integer(kind=size_length)                :: n         !
  integer(kind=8)                          :: result(n) !
  integer(kind=size_length)                :: m1        !
  integer(kind=8)                          :: oper1(m1) !
  integer(kind=size_length)                :: m2        !
  integer(kind=8)                          :: oper2(m2) !
  logical,                   intent(inout) :: error     !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1 .and. m2.eq.1) then
    a = func(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = func(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = func(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = func(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_FUNC',mess)
    error = .true.
  endif
end subroutine do_long_loop2
!
!----------------------------------------------------------------------
! INTEGER*8 Two operands arithmetic operations on vectors
!
function lsic_i_bplus (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bplus   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_bplus = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)+oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)+a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a+oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)+oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_BPLUS',mess)
    lsic_i_bplus = 1
  endif
end function lsic_i_bplus
!
function lsic_i_bminus (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bminus  !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_bminus = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)-oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)-a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a-oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)-oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_BMINUS',mess)
    lsic_i_bminus = 1
  endif
end function lsic_i_bminus
!
function lsic_i_mul (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_mul     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_mul = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)*oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)*a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a*oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)*oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_MUL',mess)
    lsic_i_mul = 1
  endif
end function lsic_i_mul
!
function lsic_i_div (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Integer division: what should be the rule with negative operands?
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_div     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  integer(kind=8), parameter :: i_pinf=9223372036854775807_8  ! 2**63-1
  integer(kind=8), parameter :: i_minf=-i_pinf-1              ! -2**63
  integer(kind=8), parameter :: i_nan=0
  !
  lsic_i_div = 0
  if (m1.eq.1 .and. m2.eq.1) then
    if (oper2(1).ne.0) then
      a = int(oper1(1)/oper2(1),8)  ! Wrong (precision loss)?
    elseif (oper1(1).gt.0) then
      a = i_pinf
    elseif  (oper1(1).lt.0) then
      a = i_minf
    else
      a = i_nan
    endif
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      if (a.ne.0) then
        result(i) = int(oper1(i)/a,8)  ! Wrong (precision loss)?
      elseif  (oper1(i).gt.0) then
        result(i) = i_pinf
      elseif  (oper1(i).lt.0) then
        result(i) = i_minf
      else
        result(i) = i_nan
      endif
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      if (oper2(i).ne.0) then
        result(i) = int(a/oper2(i),8)  ! Wrong (precision loss)?
      elseif  (a.gt.0) then
        result(i) = i_pinf
      elseif  (a.lt.0) then
        result(i) = i_minf
      else
        result(i) = i_nan
      endif
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      if (oper2(i).ne.0) then
        result(i) = int(oper1(i)/oper2(i),8)  ! Wrong (precision loss)?
      elseif  (oper1(i).gt.0) then
        result(i) = i_pinf
      elseif  (oper1(i).lt.0) then
        result(i) = i_minf
      else
        result(i) = i_nan
      endif
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_DIV',mess)
    lsic_i_div = 1
  endif
end function lsic_i_div
!
function lsic_i_power (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_power   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_power = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1)**oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i)**a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a**oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i)**oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_POWER',mess)
    lsic_i_power = 1
  endif
end function lsic_i_power
!
!----------------------------------------------------------------------
! INTEGER*8 Two argument functions on vectors
!
function lsic_i_min (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_min     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_min = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = min(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = min(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = min(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = min(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_MIN',mess)
    lsic_i_min = 1
  endif
end function lsic_i_min
!
function lsic_i_max (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_max     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_max = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = max(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = max(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = max(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = max(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_MAX',mess)
    lsic_i_max = 1
  endif
end function lsic_i_max
!
function lsic_i_atan2 (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_atan2   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ATAN2','Not implemented')
  lsic_i_atan2 = 1
  !
end function lsic_i_atan2
!
function lsic_i_dim (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_dim     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_dim = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = dim(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = dim(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = dim(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = dim(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_DIM',mess)
    lsic_i_dim = 1
  endif
end function lsic_i_dim
!
function lsic_i_sign (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_sign    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_sign = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = sign(oper1(1),oper2(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = sign(oper1(i),a)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = sign(a,oper2(i))
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = sign(oper1(i),oper2(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_SIGN',mess)
    lsic_i_sign = 1
  endif
end function lsic_i_sign
!
function lsic_i_mod (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_mod     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_mod = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = mod(oper1(1),oper2(1))
    if (a.lt.0.0) a = a+oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = mod(oper1(i),a)
      if (result(i).lt.0.0) result(i) = result(i)+a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = mod(a,oper2(i))
      if (result(i).lt.0.0) result(i) = result(i)+oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = mod(oper1(i),oper2(i))
      if (result(i).lt.0.0) result(i) = result(i)+oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_MOD',mess)
    lsic_i_mod = 1
  endif
end function lsic_i_mod
!
!----------------------------------------------------------------------
! INTEGER*8 Generic External single argument function on vector
!
subroutine do_long_loop1(func,n,result,m1,oper1,error)
  use sic_interfaces
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=8),           external      :: func       !
  integer(kind=size_length)                :: n          !
  integer(kind=8)                          :: result(n)  !
  integer(kind=size_length)                :: m1         !
  integer(kind=8)                          :: oper1(m1)  !
  logical,                   intent(inout) :: error      !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  if (m1.eq.1) then
    a = func(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = func(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_FUNC',mess)
    error = .true.
  endif
end subroutine do_long_loop1
!
!----------------------------------------------------------------------
! INTEGER*8 Single argument functions on vectors
!
function lsic_i_abs (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_abs     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_abs = 0
  if (m1.eq.1) then
    a = abs(oper1(1))
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = abs(oper1(i))
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_ABS',mess)
    lsic_i_abs = 1
  endif
end function lsic_i_abs
!
function lsic_i_acos (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_acos    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ACOS','Not implemented')
  lsic_i_acos = 1
  !
end function lsic_i_acos
!
function lsic_i_asin (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_asin    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ASIN','Not implemented')
  lsic_i_asin = 1
  !
end function lsic_i_asin
!
function lsic_i_atan (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_atan    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ATAN','Not implemented')
  lsic_i_atan = 1
  !
end function lsic_i_atan
!
function lsic_i_atanh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_atanh   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ATANH','Not implemented')
  lsic_i_atanh = 1
  !
end function lsic_i_atanh
!
function lsic_i_cos (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_cos     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_COS','Not implemented')
  lsic_i_cos = 1
  !
end function lsic_i_cos
!
function lsic_i_cosh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_cosh    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_COSH','Not implemented')
  lsic_i_cosh = 1
  !
end function lsic_i_cosh
!
function lsic_i_exp (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_exp     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_EXP','Not implemented')
  lsic_i_exp = 1
  !
end function lsic_i_exp
!
function lsic_i_int (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_int     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_int = 0
  if (m1.eq.1) then
    a = oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_INT',mess)
    lsic_i_int = 1
  endif
end function lsic_i_int
!
function lsic_i_log (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_log     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_LOG','Not implemented')
  lsic_i_log = 1
  !
end function lsic_i_log
!
function lsic_i_log10 (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_log10   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_LOG10','Not implemented')
  lsic_i_log10 = 1
  !
end function lsic_i_log10
!
function lsic_i_nint (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_nint    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_nint = 0
  if (m1.eq.1) then
    a = oper1(1)  ! NINT on int value => simple copy
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_NINT',mess)
    lsic_i_nint = 1
  endif
end function lsic_i_nint
!
function lsic_i_sin (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_sin     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_SIN','Not implemented')
  lsic_i_sin = 1
  !
end function lsic_i_sin
!
function lsic_i_sinh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_sinh    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_SINH','Not implemented')
  lsic_i_sinh = 1
  !
end function lsic_i_sinh
!
function lsic_i_sqrt (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_sqrt    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_SQRT','Not implemented')
  lsic_i_sqrt = 1
  !
end function lsic_i_sqrt
!
function lsic_i_tan (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_tan     !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_TAN','Not implemented')
  lsic_i_tan = 1
  !
end function lsic_i_tan
!
function lsic_i_tanh (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_tanh    !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_TANH','Not implemented')
  lsic_i_tanh = 1
  !
end function lsic_i_tanh
!
function lsic_i_floor (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_floor   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_floor = 0
  if (m1.eq.1) then
    a = oper1(1)  ! FLOOR on int value => simple copy
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_FLOOR',mess)
    lsic_i_floor = 1
  endif
end function lsic_i_floor
!
function lsic_i_ceiling (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_ceiling  !
  integer(kind=size_length) :: n     !
  integer(kind=8) :: result(n)       !
  integer(kind=size_length) :: m1    !
  integer(kind=8) :: oper1(m1)       !
  integer(kind=size_length) :: k     !
  integer :: dummy                   !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_ceiling = 0
  if (m1.eq.1) then
    a = oper1(1)  ! CEILING on int value => simple copy
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_CEILING',mess)
    lsic_i_ceiling = 1
  endif
end function lsic_i_ceiling
!
function lsic_i_uminus (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_uminus  !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_uminus = 0
  if (m1.eq.1) then
    a = -oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = -oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_UMINUS',mess)
    lsic_i_uminus = 1
  endif
end function lsic_i_uminus
!
function lsic_i_uplus (n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_uplus   !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  ! Local
  integer(kind=8) :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_uplus = 0
  if (m1.eq.1) then
    a = oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'I_PLUS',mess)
    lsic_i_uplus = 1
  endif
end function lsic_i_uplus
!
function lsic_i_pyfunc (noper,nelem,nwords,n,result)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Generic PYTHON function with variable number of arguments
  !        Called by DO_VECTOR
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_pyfunc  ! Function value on return
  integer(kind=4),              intent(in)  :: noper          ! Number of operands
  integer(kind=size_length),    intent(in)  :: nelem(noper)   ! # of elements for each operand
  integer(kind=address_length), intent(in)  :: nwords(noper)  ! Memory location for each operand
  integer(kind=size_length),    intent(in)  :: n              ! Size of result
  integer(kind=8),              intent(out) :: result(n)      ! Result
  !
  ! Check dimensions and construct strides
#if defined(GAG_USE_PYTHON)
  ! C does not have an equivalent for integer*8 on all platforms...
  result(:) = 0
  call sic_message(seve%e,'I_PYFUNC','Not implemented')
  lsic_i_pyfunc = 1
#else
  call sic_message(seve%e,'I_PYFUNC','Python not available')
  lsic_i_pyfunc = 1
#endif
end function lsic_i_pyfunc
!
function lsic_i_erf(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_erf    !
  integer(kind=size_length) :: n   !
  integer(kind=8) :: result(n)     !
  integer(kind=size_length) :: m1  !
  integer(kind=8) :: oper1(m1)     !
  integer(kind=size_length) :: k   !
  integer :: dummy                 !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ERF','Not implemented')
  lsic_i_erf = 1
  !
end function lsic_i_erf
!
function lsic_i_erfc(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_erfc   !
  integer(kind=size_length) :: n   !
  integer(kind=8) :: result(n)     !
  integer(kind=size_length) :: m1  !
  integer(kind=8) :: oper1(m1)     !
  integer(kind=size_length) :: k   !
  integer :: dummy                 !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ERFC','Not implemented')
  lsic_i_erfc = 1
  !
end function lsic_i_erfc
!
function lsic_i_erfinv(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_erfinv  !
  integer(kind=size_length) :: n    !
  integer(kind=8) :: result(n)      !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: k    !
  integer :: dummy                  !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ERFINV','Not implemented')
  lsic_i_erfinv = 1
  !
end function lsic_i_erfinv
!
function lsic_i_erfcinv(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_erfcinv  !
  integer(kind=size_length) :: n     !
  integer(kind=8) :: result(n)       !
  integer(kind=size_length) :: m1    !
  integer(kind=8) :: oper1(m1)       !
  integer(kind=size_length) :: k     !
  integer :: dummy                   !
  !
  result(:) = 0
  call sic_message(seve%e,'I_ERFCINV','Not implemented')
  lsic_i_erfcinv = 1
  !
end function lsic_i_erfcinv
!
function lsic_i_bessel_i0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_i0  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_I0','Not implemented')
  lsic_i_bessel_i0 = 1
  !
end function lsic_i_bessel_i0
!
function lsic_i_bessel_i1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_i1  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_I1','Not implemented')
  lsic_i_bessel_i1 = 1
  !
end function lsic_i_bessel_i1
!
function lsic_i_bessel_in(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_in  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_IN','Not implemented')
  lsic_i_bessel_in = 1
  !
end function lsic_i_bessel_in
!
function lsic_i_bessel_j0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_j0  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_J0','Not implemented')
  lsic_i_bessel_j0 = 1
  !
end function lsic_i_bessel_j0
!
function lsic_i_bessel_j1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_j1  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_J1','Not implemented')
  lsic_i_bessel_j1 = 1
  !
end function lsic_i_bessel_j1
!
function lsic_i_bessel_jn(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_jn  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_JN','Not implemented')
  lsic_i_bessel_jn = 1
  !
end function lsic_i_bessel_jn
!
function lsic_i_bessel_y0(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_y0  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_Y0','Not implemented')
  lsic_i_bessel_y0 = 1
  !
end function lsic_i_bessel_y0
!
function lsic_i_bessel_y1(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_y1  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_Y1','Not implemented')
  lsic_i_bessel_y1 = 1
  !
end function lsic_i_bessel_y1
!
function lsic_i_bessel_yn(n,result,m1,oper1,k,dummy)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_bessel_yn  !
  integer(kind=size_length) :: n       !
  integer(kind=8) :: result(n)         !
  integer(kind=size_length) :: m1      !
  integer(kind=8) :: oper1(m1)         !
  integer(kind=size_length) :: k       !
  integer :: dummy                     !
  !
  result(:) = 0
  call sic_message(seve%e,'I_BESSEL_YN','Not implemented')
  lsic_i_bessel_yn = 1
  !
end function lsic_i_bessel_yn
