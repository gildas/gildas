subroutine sic_delete(line,error)
  use gildas_def
  use sic_structures
  use sic_dictionaries
  use sic_interfaces, except_this=>sic_delete
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !       Support for commands
  !	DELETE/VARIABLE and DELETE/SYMBOL and DELETE/FUNCTION
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='DELETE'
  integer :: i,nl,ndelarg
  character(len=varname_length) :: name
  !
  ! Protection against order mismatch
  if (sic_present(0,1)) then
    call sic_message(seve%e,rname,'Delete arguments must come after the '//  &
    'option')
  endif
  !
  if (sic_present(1,0)) then
    ! DELETE /SYMBOL
    if (sic_present(2,0).or.sic_present(3,0)) then
      call sic_message(seve%e,rname,'Incompatible options')
      error = .true.
      return
    endif
    ndelarg = sic_narg(1)
    do i=1,ndelarg
      call sic_ke (line,1,i,name,nl,.true.,error)
      if (error) return
      call sic_delsymbol(name(1:nl),error)
      if (error) then
        call sic_message(seve%e,rname,'Symbol '//name(1:nl)//' not deleted')
        return
      endif
    enddo
    !
  elseif (sic_present(2,0)) then
    ! DELETE /VARIABLE
    if (sic_present(3,0)) then
      call sic_message(seve%e,rname,'Incompatible options')
      error = .true.
      return
    endif
    ndelarg = sic_narg(2)
    do i=1,ndelarg
      call sic_ch (line,2,i,name,nl,.true.,error)
      if (error) return
      call sic_delvariable(name(1:nl),.true.,error)
      if (error) then
        call sic_message(seve%e,rname,'Variable '//name(1:nl)//' not deleted')
        return
      endif
    enddo
    !
  elseif (sic_present(3,0)) then
    ! DELETE /FUNCTION
    ndelarg = sic_narg(3)
    do i=1,ndelarg
      call sic_ke(line,3,i,name,nl,.true.,error)
      if (error) return
      call sic_del_expr(name,nl,error)
      if (error)  return
    enddo
  else
    call sic_message(seve%e,rname,'Missing option')
    error = .true.
  endif
  !
end subroutine sic_delete
