module sic_interfaces_private
  interface
    subroutine sic_alias(line,rglobal,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   DEFINE ALIAS AliasName Variable [/GLOBAL]
      !
      !   Create an alias, that is another name for the same variable.
      !---------------------------------------------------------------------
      character(len=*) , intent(in)    :: line     !
      logical,           intent(in)    :: rglobal  !
      logical,           intent(inout) :: error    !
    end subroutine sic_alias
  end interface
  !
  interface
    subroutine zap_alias (in)
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Delete an Alias from the list of aliases
      !---------------------------------------------------------------------
      integer,intent(in) :: in          ! Alias number
    end subroutine zap_alias
  end interface
  !
  interface
    subroutine del_alias (in,local)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Delete the aliases pointing (even partially) towards variable IN
      !---------------------------------------------------------------------
      integer, intent(in) :: in         ! Pointee number
      logical, intent(in) :: local      ! Local or Global status
    end subroutine del_alias
  end interface
  !
  interface
    subroutine sic_desc (line,iopt,iarg,desc,default,present,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Retrieves the IARG-th argument of the IOPT-th option
      !       and returns a SIC descriptor pointing to the appropriate
      !       variable.
      !
      ! Arguments:
      !       LINE    C*(*)   Command line                    Input
      !       IOPT    I       Option number (0=command)       Input
      !       IARG    I       Argument number (0=command)     Input
      !       PRESENT L       Must the argument be present ?  Input
      !---------------------------------------------------------------------
      character(len=*)                      :: line     !
      integer                               :: iopt     !
      integer                               :: iarg     !
      type(sic_descriptor_t), intent(out)   :: desc     ! Descriptor
      type(sic_descriptor_t), intent(in)    :: default  ! Default descriptor
      logical                               :: present  !
      logical,                intent(inout) :: error    ! Error return
    end subroutine sic_desc
  end interface
  !
  interface
    subroutine sic_desc_inca (line,iopt,iarg,desc,default,present,error,code)
      use gildas_def
      use gbl_message
      use gbl_format
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use sic_expressions
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Retrieves the IARG-th argument of the IOPT-th option
      !       and returns a SIC descriptor pointing to the appropriate
      !       variable.
      !
      ! Arguments:
      !       LINE    C*(*)   Command line                    Input
      !       IOPT    I       Option number (0=command)       Input
      !       IARG    I       Argument number (0=command)     Input
      !       PRESENT L       Must the argument be present ?  Input
      !       ERROR   L       Error return
      !       CODE    I       1 <=> Descriptor,  2 <=> Incarnation Input
      ! (5-mar-1985)
      !---------------------------------------------------------------------
      character(len=*) :: line                    !
      integer :: iopt                             !
      integer :: iarg                             !
      type(sic_descriptor_t), intent(out) :: desc     ! Descriptor
      type(sic_descriptor_t), intent(in)  :: default  ! Default descriptor
      logical :: present                          !
      logical :: error                            !
      integer :: code                             !
    end subroutine sic_desc_inca
  end interface
  !
  interface
    subroutine define_symbol (line,nline,error)
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Support routine for command
      !       SYMBOL [ABBREV] ["ANYTHING you want"] [/INQUIRE ["Question"]]
      !       Defines, list or delete an abbreviation
      ! Arguments :
      !       LINE    C*(*)   Command line defining the abbreviation
      !---------------------------------------------------------------------
      character(len=*)       :: line   !
      integer                :: nline  !
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine define_symbol
  end interface
  !
  interface
    subroutine replace_usercom(user,command,line,nline,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !   Identifies and translate the user command from the input command
      ! line
      !---------------------------------------------------------------------
      type(sic_language_t), intent(in)    :: user     ! The user language
      character(len=*),     intent(inout) :: command  ! The user command to translate
      character(len=*),     intent(inout) :: line     ! Line to be translated (updated)
      integer(kind=4),      intent(inout) :: nline    ! Line length (updated)
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine replace_usercom
  end interface
  !
  interface
    subroutine replace_symbol(line,nline,error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Identifies and translate all the SIC symbols from the input
      ! command line
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Line to be translated (updated)
      integer(kind=4),  intent(inout) :: nline  ! Line length (updated)
      logical,          intent(out)   :: error  ! Logical error flag
    end subroutine replace_symbol
  end interface
  !
  interface
    subroutine replace_logical(line,nline,error)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal Routine
      !     Replace logical names between Back Quotes by their translation
      !---------------------------------------------------------------------
      character(len=*)       :: line   !
      integer                :: nline  !
      logical, intent(inout) :: error  !
    end subroutine replace_logical
  end interface
  !
  interface
    subroutine replace_symlog(line,lsymb,trans,ltrans,pos,nline,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Replace a Symbol or Logical name by its translation.
      ! Note that the surrounding quotes (for Symbols) or backquotes (for
      ! logical names) are striped off.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line    ! Command line
      integer(kind=4),  intent(in)    :: lsymb   ! Length of symbol
      character(len=*), intent(in)    :: trans   ! Translation
      integer(kind=4),  intent(in)    :: ltrans  ! Length of translation
      integer(kind=4),  intent(inout) :: pos     ! Curent position in line
      integer(kind=4),  intent(inout) :: nline   ! Length of line
      logical,          intent(inout) :: error   !
    end subroutine replace_symlog
  end interface
  !
  interface
    subroutine sic_parse_line(line,nline,library,quiet,command,ordre,error)
      use gildas_def
      use gbl_message
      use sic_types
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  SIC command line analyser
      !  IBEG    pointer in LINE
      !  M       pointer in ORDRE
      !  JOPT    Option number   (0=command)
      !  JARG    Argument number (0=option or command)
      !  COMM%POPT(Jopt)               Pointer of option in argument list
      !  COMM%IBEG(POPT(Jopt)+Jarg)    Start pointer of argument
      !  COMM%IEND(POPT(Jopt)+Jarg)    End pointer of argument
      !
      !  The character string describing an argument is thus
      !   LINE(COMM%IBEG(COMM%POPT(Jopt)+Jarg):COMM%IEND(COMM%POPT(Jopt)+Jarg))
      ! provided COMM%JOPT < 10 and Ipoint(Jopt)+Jarg < 100 . More arguments
      ! may exist, but a special procedure is required to find them, as no
      ! pointers are defined for these. The language is included in the
      ! command line on return.
      !  Argument 'COMMAND' is inout so that it is not fully reset (for
      ! efficiency purpose) since it has been done on first call. From your
      ! side you may consider SAVing the input variable in order to enable
      ! this optimisation.
      !---------------------------------------------------------------------
      character(len=*),    intent(inout) :: line     ! The line to be analysed (updated)
      integer(kind=4),     intent(inout) :: nline    ! Length of line (updated)
      logical,             intent(in)    :: library  ! Library mode
      logical,             intent(in)    :: quiet    !
      type(sic_command_t), intent(inout) :: command  ! The parsed command line
      character(len=*)                   :: ordre    ! Work buffer
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine sic_parse_line
  end interface
  !
  interface
    subroutine sic_parse_command(line,nline,iquiet,library,ilang,icom,ocode,error)
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Parse the input string of the form:
      !    LANG\COMMAND
      ! and decode the language and command.
      ! No global variable modified here
      !
      ! - IQUIET behaviour:
      !    IQUIET = 0: Verbose
      !    IQUIET = 1: Quiet
      !    IQUIET = 2: Verbose on ambiguity
      !
      ! - OCODE is used to return a thiner information on error than the
      !   on/off ERROR flag:
      !    OCODE = 0: no error
      !    OCODE = 1: Unknown command
      !    OCODE = 2: No options allowed for command
      !    OCODE = 3: Name is fully ambiguous
      !    OCODE = 4: Name is partially ambiguous
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: line     ! String to parse
      integer(kind=4),     intent(in)    :: nline    ! Length of string
      integer(kind=4),     intent(in)    :: iquiet   ! Verbosity
      logical,             intent(in)    :: library  ! Library mode?
      integer(kind=4),     intent(out)   :: ilang    ! Language found
      integer(kind=4),     intent(out)   :: icom     ! Command found
      integer(kind=4),     intent(out)   :: ocode    ! Error code
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine sic_parse_command
  end interface
  !
  interface
    subroutine sic_parse_option(line,nline,iquiet,jcom,iopt,error)
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Analyse an option of the /OPTION.
      ! No global variable modified here.
      !
      ! - IQUIET behaviour:
      !    IQUIET = 0: Verbose
      !    IQUIET = 1: Quiet
      !    IQUIET = 2: Verbose on ambiguity
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! String to parse
      integer(kind=4),  intent(in)    :: nline    ! Length of string
      integer(kind=4),  intent(in)    :: iquiet   ! Verbosity
      integer(kind=4),  intent(in)    :: jcom     ! Parent command
      integer(kind=4),  intent(out)   :: iopt     ! Option found
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_parse_option
  end interface
  !
  interface
    subroutine sic_argument(line,iopt,iarg,presen,error,  &
        oreal,odble,olong,ointe,oshort,ologi,chtyp,ostring,olen,odesc)
      use gildas_def
      use gbl_message
      use sic_interactions
      use sic_dictionaries
      use sic_structures
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Retrieves the IARG-th argument of the IOPT-th option
      !  The type and kind of the output argument is ruled by the present
      ! argument.
      !---------------------------------------------------------------------
      character(len=*),       intent(in)              :: line    ! Command line
      integer(kind=4),        intent(in)              :: iopt    ! Option number (0=command)
      integer(kind=4),        intent(in)              :: iarg    ! Argument number (0=command)
      logical,                intent(in)              :: presen  ! Must the argument be present ?
      logical,                intent(out)             :: error   ! Error flag
      ! One of these output arguments will be filled. Unchanged if absent.
      ! Numerics or logical
      real(kind=4),           intent(inout), optional :: oreal   !
      real(kind=8),           intent(inout), optional :: odble   !
      integer(kind=8),        intent(inout), optional :: olong   !
      integer(kind=4),        intent(inout), optional :: ointe   !
      integer(kind=2),        intent(inout), optional :: oshort  !
      logical,                intent(inout), optional :: ologi   !
      ! Characters
      integer(kind=4),        intent(in),    optional :: chtyp   ! Which kind of character parsing?
      character(len=*),       intent(inout), optional :: ostring !
      integer(kind=4),        intent(inout), optional :: olen    !
      ! Descriptor
      type(sic_descriptor_t), intent(inout), optional :: odesc   !
    end subroutine sic_argument
  end interface
  !
  interface
    subroutine sic_argument_desc(argum,larg,odesc,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Produce a valid descriptor describing the argument
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: argum  !
      integer(kind=4),        intent(in)    :: larg   ! Length of argum
      type(sic_descriptor_t), intent(out)   :: odesc
      logical,                intent(inout) :: error
    end subroutine sic_argument_desc
  end interface
  !
  interface
    subroutine sic_expand(argum,line,ifirst,ilast,length,error)
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Format an argument by expanding the variables, etc...
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: argum   ! Returned argument
      character(len=*), intent(in)    :: line    ! Command line
      integer(kind=4),  intent(in)    :: ifirst  ! Pointer to start
      integer(kind=4),  intent(in)    :: ilast   ! Pointer to end
      integer(kind=4),  intent(out)   :: length  ! Argument length
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_expand
  end interface
  !
  interface
    subroutine sic_dble_to_string(num,string)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Format a double-precision float according to current SIC PRECISION
      !---------------------------------------------------------------------
      real(kind=8),     intent(in)  :: num     !
      character(len=*), intent(out) :: string  !
    end subroutine sic_dble_to_string
  end interface
  !
  interface
    subroutine sic_float_to_string(num,itype,string)
      use gbl_format
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Format a float (single or double) according to current SIC
      ! PRECISION. This subroutine always receives a double-precision
      ! float as input, but its input type is taken into account.
      !---------------------------------------------------------------------
      real(kind=8),     intent(in)  :: num     !
      integer(kind=4),  intent(in)  :: itype   !
      character(len=*), intent(out) :: string  !
    end subroutine sic_float_to_string
  end interface
  !
  interface
    subroutine sic_var_to_string(var,string)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(sic_descriptor_t) :: var     !
      character(len=*)       :: string  !
    end subroutine sic_var_to_string
  end interface
  !
  interface
    subroutine sic_expand_variable(line,argum,found,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !   Replace a variable name by its value
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      character(len=*), intent(out)   :: argum  ! Returned argument
      logical,          intent(out)   :: found  ! Variable has been resolved
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_expand_variable
  end interface
  !
  interface
    subroutine sic_shape(argum,line,ifirst,ilast,length,error)
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Format an argument
      !  Similar to SIC_EXPAND, but does not suppress double quotes
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: argum   ! Returned argument
      character(len=*), intent(in)    :: line    ! Command line
      integer(kind=4),  intent(in)    :: ifirst  ! Pointer to start
      integer(kind=4),  intent(in)    :: ilast   ! Pointer to end
      integer(kind=4),  intent(out)   :: length  ! Argument length
      logical,          intent(inout) :: error   ! Error flag
    end subroutine sic_shape
  end interface
  !
  interface
    subroutine sic_keyw(argum,line,ifirst,ilast,length,error)
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Format an argument
      !  Similar to SIC_EXPAND and SIC_SHAPE but check on syntax
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: argum   ! Returned argument
      character(len=*), intent(in)    :: line    ! Command line
      integer(kind=4),  intent(in)    :: ifirst  ! Pointer to start
      integer(kind=4),  intent(in)    :: ilast   ! Pointer to end
      integer(kind=4),  intent(out)   :: length  ! Argument length
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_keyw
  end interface
  !
  interface
    function codes (func)
      use gildas_def
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: codes  ! Function value on return
      integer(kind=address_length) :: func(300)  !
    end function codes
  end interface
  !
  interface
    subroutine sic_get_func(name_in,code,nfunarg,error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Returns number of arguments and code
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name_in     !
      integer(kind=4),  intent(out)   :: code        !
      integer(kind=4),  intent(out)   :: nfunarg(2)  ! Min-max number of arguments
      logical,          intent(inout) :: error       !
    end subroutine sic_get_func
  end interface
  !
  interface
    subroutine sic_inifunction
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine sic_inifunction
  end interface
  !
  interface
    subroutine sic_list_func
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_list_func
  end interface
  !
  interface
    recursive subroutine build_tree(formula,n,operand,tree,last_node,  &
      max_level,min_level,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Formula interpretation.
      ! Transforms formula into its tree equivalent. Each node of the tree
      ! contains:
      !       - level of the node
      !       - code of operator
      !       - pointer to previous node
      !       - pointer to next node
      !       - number of associated operands
      !       - for each operand, a pointer to operand
      !  Recursive for e.g. evaluating ANY(A.EQ.1). The function ANY invokes
      ! its own build_tree to compute the array 'A.EQ.1'.
      !---------------------------------------------------------------------
      character(len=*),   intent(in)  :: formula                 ! The line to be analysed
      integer(kind=4),    intent(in)  :: n                       ! Length of line
      type(sic_descriptor_t)          :: operand(0:maxoper)      ! List of operands (output)
      integer(kind=4)                 :: tree(formula_length*2)  ! Tree structure (output)
      integer(kind=4),    intent(out) :: last_node               ! Pointer to last node in tree
      integer(kind=4),    intent(out) :: max_level               ! Maximum parenthesis level
      integer(kind=4),    intent(out) :: min_level               !
      logical,            intent(out) :: error                   ! Logical error flag
    end subroutine build_tree
  end interface
  !
  interface
    subroutine read_operand(chain,nch,descr,error)
      use gildas_def
      use sic_interactions
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !  Decode operands
      !---------------------------------------------------------------------
      character(len=*),       intent(in)  :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)  :: nch    ! Length of chain
      type(sic_descriptor_t)              :: descr  ! Operand descriptor  Output
      logical,                intent(out) :: error  ! Logical error flag
    end subroutine read_operand
  end interface
  !
  interface
    subroutine find_operator(line,nline,i_deb,i_fin,i_oper,code,preced,nfunarg,  &
      next,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Search for first operator in line
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line        ! The line to be analysed
      integer(kind=4),  intent(in)  :: nline       ! Length of line
      integer(kind=4),  intent(out) :: i_deb       ! Start of operand if any
      integer(kind=4),  intent(out) :: i_fin       ! End of operand if any
      integer(kind=4),  intent(out) :: i_oper      ! End of operator
      integer(kind=4),  intent(out) :: code        ! Code of operator
      integer(kind=4),  intent(out) :: preced      ! Precedence of operator
      integer(kind=4),  intent(out) :: nfunarg(2)  ! Min-max number of arguments
      integer(kind=4),  intent(out) :: next        ! Start of non analysed part
      logical,          intent(out) :: error       ! Logical error flag
    end subroutine find_operator
  end interface
  !
  interface
    subroutine get_logcode(chain,i_code,nfunarg,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Sends back a code for the logical or relational operator contained
      ! in CHAIN
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: chain       ! The line to be analysed
      integer(kind=4),  intent(out) :: i_code      ! Code for operator
      integer(kind=4),  intent(out) :: nfunarg(2)  ! Min-max number of arguments
      logical,          intent(out) :: error       ! Logical error flag
    end subroutine get_logcode
  end interface
  !
  interface
    subroutine get_level(line,n,level,coma,error)
      use sic_expressions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Could be done while removing blanks and tabs...
      ! Determines the parenthesis level for each character of string LINE.
      !    LEVEL(I) is the level of LINE(I:I)
      ! Parenthesis themselves are stored at the level below their contents. 
      ! Comas are also stored one level below their surroundings.
      !    COMA(I) is defined for opening parenthesis as the number of comas
      ! at their level before drop to lower levels. For a function call, it
      ! should correspond to the number of arguments minus 1.
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line      ! The line to be analysed
      integer(kind=4),  intent(in)  :: n         ! Length of LINE
      integer(kind=4)               :: level(n)  ! Parenthesis level array
      integer(kind=4)               :: coma(n)   !
      logical,          intent(out) :: error     !
    end subroutine get_level
  end interface
  !
  interface
    subroutine ctrlc_check (error)
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Pause if ^C has been pressed and reset CONTROLC flag to false.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine ctrlc_check
  end interface
  !
  interface
    subroutine sic_datetime(line,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    DATETIME InValue1 [Invalue2] OutFormat Outvar1 [Outvar2]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_datetime
  end interface
  !
  interface
    subroutine sic_datetime_parse(line,in,ninput,out,noutput,nelem,error)
      use gbl_format
      use phys_const
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line     !
      type(datetime_spec_t),     intent(out)   :: in(:)    !
      integer(kind=4),           intent(inout) :: ninput   ! Useful size of in(:)
      type(datetime_spec_t),     intent(out)   :: out(:)   !
      integer(kind=4),           intent(inout) :: noutput  ! Useful size of out(:)
      integer(kind=size_length), intent(out)   :: nelem    ! Size of problem
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_datetime_parse
  end interface
  !
  interface
    subroutine sic_datetime_do(in,ninput,out,noutput,nelem,error)
      use gbl_format
      use phys_const
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(datetime_spec_t),     intent(in)    :: in(:)
      integer(kind=4),           intent(in)    :: ninput
      type(datetime_spec_t),     intent(in)    :: out(:)
      integer(kind=4),           intent(in)    :: noutput
      integer(kind=size_length), intent(in)    :: nelem
      logical,                   intent(inout) :: error
    end subroutine sic_datetime_do
  end interface
  !
  interface
    subroutine sic_datetime_clean(in,ninput,out,noutput,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Clean the temporaries created for command execution
      !---------------------------------------------------------------------
      type(datetime_spec_t), intent(inout) :: in(:)    !
      integer(kind=4),       intent(in)    :: ninput   ! Useful size of in(:)
      type(datetime_spec_t), intent(inout) :: out(:)   !
      integer(kind=4),       intent(in)    :: noutput  ! Useful size of out(:)
      logical,               intent(inout) :: error    !
    end subroutine sic_datetime_clean
  end interface
  !
  interface sic_datetime_setvalue
    subroutine sic_datetime_setvalue_i4(out,ielem,i4,obuf,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private-generic sic_datetime_setvalue
      !---------------------------------------------------------------------
      type(datetime_spec_t),     intent(in)    :: out
      integer(kind=size_length), intent(in)    :: ielem
      integer(kind=4),           intent(in)    :: i4
      character(len=*),          intent(inout) :: obuf
      logical,                   intent(inout) :: error
    end subroutine sic_datetime_setvalue_i4
    subroutine sic_datetime_setvalue_r8(out,ielem,r8,obuf,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private-generic sic_datetime_setvalue
      !---------------------------------------------------------------------
      type(datetime_spec_t),     intent(in)    :: out
      integer(kind=size_length), intent(in)    :: ielem
      real(kind=8),              intent(in)    :: r8
      character(len=*),          intent(inout) :: obuf
      logical,                   intent(inout) :: error
    end subroutine sic_datetime_setvalue_r8
    subroutine sic_datetime_setvalue_ch(out,ielem,ch,obuf,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private-generic sic_datetime_setvalue
      !---------------------------------------------------------------------
      type(datetime_spec_t),     intent(in)    :: out
      integer(kind=size_length), intent(in)    :: ielem
      character(len=*),          intent(in)    :: ch
      character(len=*),          intent(inout) :: obuf
      logical,                   intent(inout) :: error
    end subroutine sic_datetime_setvalue_ch
  end interface sic_datetime_setvalue
  !
  interface
    subroutine sic_define (line,nline,error)
      use gildas_def
      use gbl_message
      use gbl_format
      use sic_dictionaries
      use sic_interactions
      use sic_structures
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Support routine for command
      !       DEFINE Type Variable1 [Variable2 [...]]] 
      !         [/GLOBAL] [/LIKE Other] [/TRIM]
      !       Defines new variables, local or global
      !       DEFINE CHARACTER*Length  is now supported
      !---------------------------------------------------------------------
      character(len=*)                :: line   ! Command line          Input
      integer(kind=4)                 :: nline  ! Length of line        Input
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_define
  end interface
  !
  interface
    subroutine sic_define_likevar(desc,cast)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @private
      ! Return the dimensions (if any) of variable described by the input
      ! descriptor. Return an empty string if this variable is a scalar.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)  :: desc  ! Variable descriptor
      character(len=*),       intent(out) :: cast  ! Dimensions as string
    end subroutine sic_define_likevar
  end interface
  !
  interface
    recursive subroutine sic_define_likestruct(newvar,like,inlist,nelem,global,error)
      use gildas_def
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    DEFINE STRUCTURE MYNEW /LIKE MYOLD
      ! Fill a (just defined) structure with the same tree as the other
      ! (already existing) structure given with option /LIKE.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: newvar         ! Name of structure to be filled
      character(len=*), intent(in)    :: like           ! Name of structure used like
      integer,          intent(in)    :: nelem          ! Number of variables to check
      integer,          intent(in)    :: inlist(nelem)  ! List of variable to check
      logical,          intent(in)    :: global         ! Will new variable be global?
      logical,          intent(inout) :: error          ! Error flag
    end subroutine sic_define_likestruct
  end interface
  !
  interface
    function sic_checkstruct(namein,global)
      use gildas_def
      use gbl_message
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Check wether the name NAMEIN is a structure member. It is normally
      ! forbidden to attach a new element under a header/image, except for
      ! aliases.
      !
      ! Return values
      !     1        The parent structure exists
      !     0        There is no parent structure
      !     -1       This is not a structure member
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_checkstruct      ! Function value on return
      character(len=*), intent(in) :: namein  ! Variable name
      logical,          intent(in) :: global  ! Input variable status
    end function sic_checkstruct
  end interface
  !
  interface
    subroutine sic_inivariable
      use sic_dictionaries
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC
      !     Initialize the variable structure list
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_inivariable
  end interface
  !
  interface
    subroutine sic_define_image(namein,file,status,global,therank,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !     Define a new variable associated to an image file from command
      !     DEFINE IMAGE Variable File Read|Write|Integer|Real|Double|Extend
      !     Variable may have ReadOnly protection.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: namein   ! Variable name
      character(len=*), intent(in)    :: file     ! Associated file name
      character(len=*), intent(in)    :: status   ! Variable type / status
      logical,          intent(in)    :: global   ! Global or Local variable
      integer(kind=4),  intent(in)    :: therank  ! Trim degenerate dimensions
      logical,          intent(inout) :: error    ! Return error flag
    end subroutine sic_define_image
  end interface
  !
  interface
    subroutine sic_define_old_file(filename,code,rdonly,rank,desc,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support subroutine for
      !   DEFINE HEADER
      !   DEFINE IMAGE
      !   DEFINE TABLE
      !   DEFINE UVTABLE
      ! Open an old (already existing) file and fill the corresponding Sic
      ! structure, mapped on a GIO slot
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: filename  !
      integer(kind=4),        intent(in)    :: code      ! Kind of object (header, image, uv...)
      logical,                intent(in)    :: rdonly    !
      integer(kind=4),        intent(inout) :: rank      ! Desired rank (in) / actual rank (out)
      type(sic_descriptor_t), intent(out)   :: desc      !
      logical,                intent(inout) :: error     !
    end subroutine sic_define_old_file
  end interface
  !
  interface
    subroutine sic_define_map_file(file,desc,rdonly,code,therank,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Map an existing image, or a virtual buffer, in a gio image slot, in
      ! ReadOnly or ReadWrite.
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: file     ! Image name
      type(sic_descriptor_t), intent(out)   :: desc     ! Descriptor
      logical,                intent(in)    :: rdonly   ! Open in Read Only
      integer(kind=4),        intent(in)    :: code     ! Kind of object (header, image, uv...)
      integer(kind=4),        intent(inout) :: therank  ! Trim trailing dimensions
      logical,                intent(inout) :: error    ! Error flag
    end subroutine sic_define_map_file
  end interface
  !
  interface
    subroutine sic_define_map_fits(file,code,desc,rank,error)
      use gildas_def
      use gbl_message
      use image_def
      use gfits_types
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  DEFINE IMAGE|HEADER Var FitsFile READ
      ! i.e. fill (memory-only) a FITS file to a GDF image structure and map
      ! it on a GIO slot
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: file   !
      integer(kind=4),        intent(in)    :: code   ! Kind of object (header, image, uv...)
      type(sic_descriptor_t), intent(out)   :: desc   !
      integer(kind=4),        intent(inout) :: rank   ! Desired rank (in) / actual rank (out)
      logical,                intent(inout) :: error  !
    end subroutine sic_define_map_fits
  end interface
  !
  interface
    subroutine sic_define_new_file (file,ndim,dim,desc,istat,code,error)
      use image_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      !     Creates a new image of specified dimensions. The
      !     increments along each axis are initialized to 1.
      !---------------------------------------------------------------------
      character(len=*) :: file              ! Image file name       Input
      integer :: ndim                       ! Number of dimensions  Input
      integer(kind=index_length), intent(in) :: dim(ndim)  ! Dimensions
      type(sic_descriptor_t) :: desc        ! Returned descriptor   Output
      integer(kind=4), intent(in) :: istat  ! Code for format
      integer(kind=4), intent(in) :: code   ! Code for type of image
      logical :: error                      ! Logical error flag    Output
    end subroutine sic_define_new_file
  end interface
  !
  interface
    subroutine sic_define_new_uvtable(file,ndim,dim,desc,istat,error)
      use image_def
      use gbl_message
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Creates a new image of specified dimensions. The increments along
      ! each axis are initialized to 1.
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: file       ! Image file name
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      type(sic_descriptor_t)                    :: desc       ! Returned decriptor
      integer(kind=4),            intent(in)    :: istat      ! Code for format
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sic_define_new_uvtable
  end interface
  !
  interface
    subroutine sic_define_extend_file (file,newdim,desc,error)
      use image_def
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      !  Extend an existing image by increasing its LAST dimension
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: file    ! Image file name
      integer(kind=index_length), intent(in)    :: newdim  ! New value of last dimension
      type(sic_descriptor_t),     intent(out)   :: desc    ! Returned descriptor
      logical,                    intent(inout) :: error   ! Logical error flag
    end subroutine sic_define_extend_file
  end interface
  !
  interface
    subroutine sic_define_header(namein,file,status,global,therank,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      !     Define a new variable associated to an image header, from command
      !     DEFINE HEADER Variable File Read|Write  /TRIM [Rank]
      ! and
      !     DEFINE HEADER Variable * Image|UVData [/LIKE Other]
      !     Variable may have ReadOnly protection.
      !---------------------------------------------------------------------
      character(len=*)                :: namein  ! Variable name
      character(len=*)                :: file    ! Associated file name
      character(len=*)                :: status  ! Variable type / status
      logical, intent(in)             :: global  ! Global or Local variable
      integer(4), intent(in)          :: therank ! Desired Output Rank
      logical,          intent(inout) :: error   ! Return error flag
    end subroutine sic_define_header
  end interface
  !
  interface
    subroutine sub_dim_header(head,subdims)
      use image_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the dimensions in case of new header
      !---------------------------------------------------------------------
      type(gildas),                intent(inout) :: head     ! Target header
      type(sic_dimensions_done_t), intent(in)    :: subdims  !
    end subroutine sub_dim_header
  end interface
  !
  interface
    subroutine sub_def_header(varin,sep,h,rd,therank,error)
      use image_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Define all variables associated to an image header. Input object
      ! must be the whole 'gildas' type because we map head%gil% and
      ! head%char% elements.
      !   NB: if missing, the blanking section is enabled to default values
      ! (with eval<0). Because of this the input target header is
      ! intent(inout).
      !---------------------------------------------------------------------
      type(sic_identifier_t), intent(in)    :: varin  ! Parent structure
      character(len=*),       intent(in)    :: sep    ! Separator between parent structure and its components (1 char)
      type(gildas),           intent(inout) :: h      ! Target header
      logical,                intent(in)    :: rd     ! Readonly status
      integer(4),             intent(in)    :: therank ! Trim degenerate trailing dims
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sub_def_header
  end interface
  !
  interface
    subroutine rename_variable(namein,nameout,error)
      use gbl_message
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !   Rename a Sic variable
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: namein   ! Original name
      character(len=*), intent(in)    :: nameout  ! New name
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine rename_variable
  end interface
  !
  interface
    subroutine sub_def_real(symb,rel,ndim,dim,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb       ! Variable name
      real(kind=4),               intent(in)    :: rel        ! Real array
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      logical,                    intent(in)    :: readonly   ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev        ! Variable level
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sub_def_real
  end interface
  !
  interface
    subroutine sub_def_dble(symb,dble,ndim,dim,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb       ! Variable name
      real(kind=8),               intent(in)    :: dble       ! REAL*8 array
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      logical,                    intent(in)    :: readonly   ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev        ! Variable level
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sub_def_dble
  end interface
  !
  interface
    subroutine sub_def_long(symb,long,ndim,dim,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb       ! Variable name
      integer(kind=8),            intent(in)    :: long       ! Long integer array
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      logical,                    intent(in)    :: readonly   ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev        ! Variable level
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sub_def_long
  end interface
  !
  interface
    subroutine sub_def_logi (symb,logi,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb      ! Variable name
      logical,                    intent(in)    :: logi      ! Logical scalar
      logical,                    intent(in)    :: readonly  ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev       ! Variable level
      logical,                    intent(inout) :: error     ! Logical error flag
    end subroutine sub_def_logi
  end interface
  !
  interface
    subroutine sub_def_login(symb,logi,ndim,dim,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb       ! Variable name
      logical,                    intent(in)    :: logi       ! Logical array
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      logical,                    intent(in)    :: readonly   ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev        ! Variable level
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sub_def_login
  end interface
  !
  interface
    subroutine sub_def_char(symb,chain,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb      ! Variable name
      character(len=*),           intent(in)    :: chain     ! Character string
      logical,                    intent(in)    :: readonly  ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev       ! Variable level
      logical,                    intent(inout) :: error     ! Logical error flag
    end subroutine sub_def_char
  end interface
  !
  interface
    subroutine sub_def_charn(symb,chain,ndim,dim,readonly,lev,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines global variables by program
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: symb       ! Variable name
      character(len=*),           intent(in)    :: chain      ! Character string
      integer(kind=4),            intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length), intent(in)    :: dim(ndim)  ! Dimensions
      logical,                    intent(in)    :: readonly   ! ReadOnly flag
      integer(kind=4),            intent(in)    :: lev        ! Variable level
      logical,                    intent(inout) :: error      ! Logical error flag
    end subroutine sub_def_charn
  end interface
  !
  interface
    subroutine sic_changevariable(namin,rdonly,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Change the status of an IMAGE variable (HEADER or TABLE) to
      ! Read or Write
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: namin  ! Variable name
      logical, intent(in) :: rdonly          !
      logical, intent(out) :: error          !
    end subroutine sic_changevariable
  end interface
  !
  interface
    subroutine sic_changeheader(headvar,jn,rdonly,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Change the status of an HEADER variable to Read or Write
      !---------------------------------------------------------------------
      type(sic_identifier_t), intent(in)    :: headvar  ! SIC variable
      integer,                intent(in)    :: jn       ! Variable number DUMMY ...
      logical,                intent(in)    :: rdonly   ! Read-only status
      logical,                intent(inout) :: error    ! Error flag
    end subroutine sic_changeheader
  end interface
  !
  interface
    subroutine trim_rank(rname,ndim,dims,therank,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Trim the rank of an Image or Header
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: rname    !
      integer(kind=4),            intent(inout) :: ndim     !
      integer(kind=index_length), intent(inout) :: dims(:)  !
      integer(kind=4),            intent(inout) :: therank  ! Desired rank
      logical,                    intent(inout) :: error    !
    end subroutine trim_rank
  end interface
  !
  interface
    subroutine sic_delete(line,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Support for commands
      !	DELETE/VARIABLE and DELETE/SYMBOL and DELETE/FUNCTION
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine sic_delete
  end interface
  !
  interface
    subroutine sic_zapvariable(in,user,local,error)
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Delete Variable number IN from variable list
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: in     ! Variable number
      logical,         intent(in)    :: user   ! Is variable User defined?
      logical,         intent(in)    :: local  ! Is variable Local or Global
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine sic_zapvariable
  end interface
  !
  interface
    subroutine sic_delstructure(structvar,user,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Delete a Structure and all its members. It assumes the variable is a
      ! structure, not a header.
      ! This routine is called when input variable is already checked as a TRUE
      ! structure.
      !---------------------------------------------------------------------
      type(sic_identifier_t), intent(in)    :: structvar  ! Structure name to delete
      logical,                intent(in)    :: user       ! User or Program request
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_delstructure
  end interface
  !
  interface
    subroutine erase_variables
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Delete and reset to zero all local variables at level
      !       VAR_LEVEL, and decrement VAR_LEVEL
      !---------------------------------------------------------------------
      ! Local
    end subroutine erase_variables
  end interface
  !
  interface
    subroutine del_ima_var
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Delete all image variables. This is called upon Program exit to
      !       ensure updating (when the mapping mechanism cannot be used).
      !---------------------------------------------------------------------
      ! Local
    end subroutine del_ima_var
  end interface
  !
  interface
    subroutine sic_free_variable
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Release the variable structure list at exit time
      !---------------------------------------------------------------------
    end subroutine sic_free_variable
  end interface
  !
  interface
    function desc_2gelems(desc)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return .true. if the data has more than 2 giga-elements
      !---------------------------------------------------------------------
      logical :: desc_2gelems  ! Function value on return
      type(sic_descriptor_t), intent(in) :: desc
    end function desc_2gelems
  end interface
  !
  interface
    function desc_2gbytes(desc)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return .true. if the data is larger than 2 GB (bytes, not 4-bytes
      ! words)
      !---------------------------------------------------------------------
      logical :: desc_2gbytes  ! Function value on return
      type(sic_descriptor_t), intent(in) :: desc
    end function desc_2gbytes
  end interface
  !
  interface
    subroutine desc_toobig(desc,error)
      use gildas_def
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return an error if the image is too large to be opened on the
      ! current system
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc
      logical,                intent(inout) :: error
    end subroutine desc_toobig
  end interface
  !
  interface
    subroutine copy_descr(in,out,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Produce an exact copy of a descriptor.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: in
      type(sic_descriptor_t), intent(out)   :: out
      logical,                intent(inout) :: error
    end subroutine copy_descr
  end interface
  !
  interface
    subroutine extract_descr(out,in,spec,name,inca,form,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! From the original full variable descriptor, extract the partial
      ! variable descriptor according to specified dimensions or substring
      !---------------------------------------------------------------------
      type(sic_descriptor_t)                     :: out      ! Resulting descriptor
      type(sic_descriptor_t)                     :: in       ! Descriptor of variable
      type(sic_dimensions_done_t), intent(inout) :: spec(2)  ! Dimension specifications
      character(len=*),            intent(in)    :: name     ! Name of variable
      logical,                     intent(in)    :: inca     ! Allow Incarnation
      integer(kind=4),             intent(in)    :: form     ! Incarnation type
      logical,                     intent(inout) :: error    ! Logical error flag
    end subroutine extract_descr
  end interface
  !
  interface
    subroutine extract_descr_subarray(out,in,spec,name,inca,form,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Get sub-array descriptor according to specified dimensions
      !---------------------------------------------------------------------
      type(sic_descriptor_t)                     :: out   ! Resulting descriptor
      type(sic_descriptor_t)                     :: in    ! Descriptor of variable
      type(sic_dimensions_done_t), intent(inout) :: spec  ! Subarray specification
      character(len=*),            intent(in)    :: name  ! Name of variable
      logical,                     intent(in)    :: inca  ! Allow Incarnation
      integer(kind=4),             intent(in)    :: form  ! Incarnation type
      logical,                     intent(inout) :: error ! Logical error flag
    end subroutine extract_descr_subarray
  end interface
  !
  interface
    subroutine extract_descr_substring(out,in,spec,name,error)
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sic_descriptor_t),      intent(inout) :: out    ! Resulting descriptor
      type(sic_descriptor_t),      intent(in)    :: in     ! Descriptor of variable
      type(sic_dimensions_done_t), intent(inout) :: spec   ! Substring specification
      character(len=*),            intent(in)    :: name   ! Name of variable
      logical,                     intent(inout) :: error  ! Logical error flag
    end subroutine extract_descr_substring
  end interface
  !
  interface
    subroutine sic_parse_dim(string,var,spec,verbose,error)
      use gildas_def
      use gbl_message
      use sic_types
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Parse the dimension field of an input variable e.g. NAME[i,j][k:l]
      ! On return fill:
      !   var%name
      !   var%lname
      !   specifications description (subarray and/or substring)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: string    ! String to parse
      type(sic_identifier_t), intent(out)   :: var       ! Parsed variable
      type(sic_dimensions_t), intent(inout) :: spec      ! Dimensions specifications
      logical,                intent(in)    :: verbose   ! Verbose work?
      logical,                intent(inout) :: error     ! Error flag
    end subroutine sic_parse_dim
  end interface
  !
  interface
    subroutine sic_decode_dims(chain,lc,dim,ndim,range,implicit,has_range,  &
      has_implicit,dimvars,verbose,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Parse one dimension of an input variable i:j
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: chain     ! String to parse
      integer,                    intent(in)    :: lc        ! Length of input string
      integer(kind=index_length), intent(inout) :: dim(sic_maxdims,2)  ! The dimensions parsed
      integer,                    intent(inout) :: ndim      ! The Nth dimension being parsed
      logical,                    intent(in)    :: range     ! Accept a dimension range
      logical,                    intent(in)    :: implicit  ! Accept implicit dimension
      logical,                    intent(inout) :: has_range             ! Some range found?
      logical,                    intent(inout) :: has_implicit          ! Implicit dimension found?
      type(sic_identifier_t),     intent(inout) :: dimvars(sic_maxdims)  ! Name of implicit dimension
      logical,                    intent(in)    :: verbose   ! Print messages
      logical,                    intent(inout) :: error     ! Logical error flag
    end subroutine sic_decode_dims
  end interface
  !
  interface
    subroutine sic_parse_var(namein,var,spec,impvars,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC
      !   Parse the dimension field of a variable for implicit dimensions
      ! NAME[i,j].
      !   Define the support variables for the implicit (named) dimensions
      ! in the Sic variables dictionary.
      ! On return fill:
      !   var%name
      !   var%lname
      !   specifications description (subarray and/or substring)
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: namein    ! String to parse
      type(sic_identifier_t),     intent(out)   :: var       ! Variable name
      type(sic_dimensions_t),     intent(inout) :: spec      ! Dimensions specifications
      integer(kind=4),            intent(out)   :: impvars(sic_maxdims)  ! Id. of implicit variables
      logical,                    intent(inout) :: error     ! Logical error flag
    end subroutine sic_parse_var
  end interface
  !
  interface
    subroutine sic_parse_char(string,var,nchar,spec,verbose,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Parse the size field of a character variable NAME*i
      ! On return fill:
      !   var%name
      !   var%lname
      !   nchar      (string length)
      !   specifications description (subarray and/or substring)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: string   ! String to parse
      type(sic_identifier_t), intent(out)   :: var      ! Variable
      integer(kind=4),        intent(out)   :: nchar    ! String length
      type(sic_dimensions_t), intent(inout) :: spec     ! Dimensions specifications
      logical,                intent(in)    :: verbose  ! Verbose work?
      logical,                intent(inout) :: error    ! Error flag
    end subroutine sic_parse_char
  end interface
  !
  interface
    subroutine intege(var,inte,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Translate the input variable to an integer value. The routine makes
      ! sure that it is really a scalar numeric variable, and cast it to an
      ! integer if needed.
      !---------------------------------------------------------------------
      type(sic_identifier_t),     intent(inout) :: var    ! Variable to translate
      integer(kind=index_length), intent(out)   :: inte   ! Integer value on return
      logical,                    intent(out)   :: error  ! Logical error flag
    end subroutine intege
  end interface
  !
  interface
    subroutine copy_back(in,out,ndim,dim,name,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !  Copy back from scratch contiguous incarnation of a subarray to the
      ! relevant fraction of the original variable.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),     intent(in)    :: in     ! Descriptor of scratch incarnation of subarray
      type(sic_descriptor_t),     intent(in)    :: out    ! Descriptor of the permanent variable
      integer,                    intent(in)    :: ndim   ! Number of fixed indexes
      integer(kind=index_length), intent(in)    :: dim(sic_maxdims,2)  ! Fixed indexes
      character(len=*),           intent(in)    :: name   ! Name of variable
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine copy_back
  end interface
  !
  interface
    subroutine sic_diff(line,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC\DIFF Header1 Header2
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine sic_diff
  end interface
  !
  interface
    subroutine sic_diff_descriptors(desc1,desc2,datatol,differ,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between two SIC variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(out)   :: differ
      logical,                intent(inout) :: error
    end subroutine sic_diff_descriptors
  end interface
  !
  interface
    subroutine sic_diff_headers(h1,h2,differ,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between two GDF headers
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_headers
  end interface
  !
  interface
    subroutine sic_diff_general(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section GENERAL)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_general
  end interface
  !
  interface
    subroutine sic_diff_dimension(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section DIMENSION)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_dimension
  end interface
  !
  interface
    subroutine sic_diff_blanking(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section BLANKING)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_blanking
  end interface
  !
  interface
    subroutine sic_diff_extrema(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section EXTREMA)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_extrema
  end interface
  !
  interface
    subroutine sic_diff_coordinate(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section COORDINATE)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_coordinate
  end interface
  !
  interface
    subroutine sic_diff_description(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section DESCRIPTION)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_description
  end interface
  !
  interface
    subroutine sic_diff_position(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section POSITION)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_position
  end interface
  !
  interface
    subroutine sic_diff_projection(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section PROJECTION)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_projection
  end interface
  !
  interface
    subroutine sic_diff_spectroscopy(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section SPECTROSCOPY)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_spectroscopy
  end interface
  !
  interface
    subroutine sic_diff_resolution(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section RESOLUTION)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_resolution
  end interface
  !
  interface
    subroutine sic_diff_noise(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section NOISE)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_noise
  end interface
  !
  interface
    subroutine sic_diff_astrometry(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section ASTROMETRY)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_astrometry
  end interface
  !
  interface
    subroutine sic_diff_telescope(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section TELESCOPE)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_telescope
  end interface
  !
  interface
    subroutine sic_diff_uvdescr(h1,h2,differ,error)
      use gbl_message
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !  Print the differences between 2 headers (section UV_DATA
      ! description)
      !---------------------------------------------------------------------
      type(gildas), intent(in)    :: h1,h2
      logical,      intent(inout) :: differ
      logical,      intent(inout) :: error
    end subroutine sic_diff_uvdescr
  end interface
  !
  interface
    function sic_diff_presec(secname,len1,len2,differ)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Return .true. if the section is absent from 1 or from the 2 headers
      !---------------------------------------------------------------------
      logical :: sic_diff_presec
      character(len=*), intent(in)    :: secname    ! Section name
      integer(kind=4),  intent(in)    :: len1,len2  ! Section lengths
      logical,          intent(inout) :: differ     !
    end function sic_diff_presec
  end interface
  !
  interface
    subroutine sic_diff_datadescr(desc1,desc2,datatol,differ,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between the data of two SIC variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(inout) :: differ
      logical,                intent(inout) :: error
    end subroutine sic_diff_datadescr
  end interface
  !
  interface
    subroutine sic_diff_uvt(desc1,desc2,datatol,differ,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between the data of two UVT variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(inout) :: differ
      logical,                intent(inout) :: error
    end subroutine sic_diff_uvt
  end interface
  !
  interface
    subroutine sic_diff_uvtcolumn(desc1,desc2,colid,datatol,secwarned,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between the data of two UV columns variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      integer(kind=4),        intent(in)    :: colid
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(inout) :: secwarned
      logical,                intent(inout) :: error
    end subroutine sic_diff_uvtcolumn
  end interface
  !
  interface
    subroutine sic_diff_uvtdata(desc1,desc2,datatol,differ,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between the data of two UV columns variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(inout) :: differ
      logical,                intent(inout) :: error
    end subroutine sic_diff_uvtdata
  end interface
  !
  interface
    subroutine sic_diff_image(desc1,desc2,datatol,differ,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Show the differences between the data of two SIC variables
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc1,desc2
      real(kind=4),           intent(in)    :: datatol
      logical,                intent(inout) :: differ
      logical,                intent(inout) :: error
    end subroutine sic_diff_image
  end interface
  !
  interface
    function lsic_s_ne (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Non-Equality of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_ne      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*4 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*4 :: oper2(m2)               !
    end function lsic_s_ne
  end interface
  !
  interface
    function lsic_s_ge (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_ge      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*4 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*4 :: oper2(m2)               !
    end function lsic_s_ge
  end interface
  !
  interface
    function lsic_s_gt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_gt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*4 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*4 :: oper2(m2)               !
    end function lsic_s_gt
  end interface
  !
  interface
    function lsic_s_le (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_le      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*4 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*4 :: oper2(m2)               !
    end function lsic_s_le
  end interface
  !
  interface
    function lsic_s_lt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_lt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*4 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*4 :: oper2(m2)               !
    end function lsic_s_lt
  end interface
  !
  interface
    function lsic_d_ne (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Non-Equality of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_ne      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*8 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*8 :: oper2(m2)               !
    end function lsic_d_ne
  end interface
  !
  interface
    function lsic_d_ge (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_ge      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*8 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*8 :: oper2(m2)               !
    end function lsic_d_ge
  end interface
  !
  interface
    function lsic_d_gt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_gt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*8 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*8 :: oper2(m2)               !
    end function lsic_d_gt
  end interface
  !
  interface
    function lsic_d_le (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_le      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*8 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*8 :: oper2(m2)               !
    end function lsic_d_le
  end interface
  !
  interface
    function lsic_d_lt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_lt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      real*8 :: oper1(m1)               !
      integer(kind=size_length) :: m2   !
      real*8 :: oper2(m2)               !
    end function lsic_d_lt
  end interface
  !
  interface
    function lsic_i_ne (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Non-Equality of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_ne      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_ne
  end interface
  !
  interface
    function lsic_i_ge (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_ge      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_ge
  end interface
  !
  interface
    function lsic_i_gt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_gt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_gt
  end interface
  !
  interface
    function lsic_i_le (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_le      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_le
  end interface
  !
  interface
    function lsic_i_lt (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Comparison of two vectors
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_lt      !
      integer(kind=size_length) :: n    !
      logical :: result(n)              !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_lt
  end interface
  !
  interface
    function lsic_s_bplus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bplus         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_bplus
  end interface
  !
  interface
    function lsic_s_bminus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bminus        !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_bminus
  end interface
  !
  interface
    function lsic_s_mul (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_mul           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_mul
  end interface
  !
  interface
    function lsic_s_div (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_div           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_div
  end interface
  !
  interface
    function lsic_s_power (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_power         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_power
  end interface
  !
  interface
    function lsic_s_min (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_min           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_min
  end interface
  !
  interface
    function lsic_s_max (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_max           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_max
  end interface
  !
  interface
    function lsic_s_atan2 (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_atan2         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_atan2
  end interface
  !
  interface
    function lsic_s_dim (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_dim           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_dim
  end interface
  !
  interface
    function lsic_s_sign (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_sign          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_sign
  end interface
  !
  interface
    function lsic_s_mod (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_mod           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_mod
  end interface
  !
  interface
    function lsic_s_abs (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_abs           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_abs
  end interface
  !
  interface
    function lsic_s_acos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_acos          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_acos
  end interface
  !
  interface
    function lsic_s_asin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_asin          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_asin
  end interface
  !
  interface
    function lsic_s_atan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_atan          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_atan
  end interface
  !
  interface
    function lsic_s_cos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_cos           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_cos
  end interface
  !
  interface
    function lsic_s_atanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_atanh         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_atanh
  end interface
  !
  interface
    function lsic_s_cosh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_cosh          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_cosh
  end interface
  !
  interface
    function lsic_s_exp (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_exp           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_exp
  end interface
  !
  interface
    function lsic_s_int (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_int           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_int
  end interface
  !
  interface
    function lsic_s_log (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_log           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_log
  end interface
  !
  interface
    function lsic_s_log10 (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_log10         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_log10
  end interface
  !
  interface
    function lsic_s_nint (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_nint          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_nint
  end interface
  !
  interface
    function lsic_s_sin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_sin           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_sin
  end interface
  !
  interface
    function lsic_s_sinh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_sinh          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_sinh
  end interface
  !
  interface
    function lsic_s_sqrt (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_sqrt          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_sqrt
  end interface
  !
  interface
    function lsic_s_tan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_tan           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_tan
  end interface
  !
  interface
    function lsic_s_tanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_tanh          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_tanh
  end interface
  !
  interface
    function lsic_s_floor (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_floor         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_floor
  end interface
  !
  interface
    function lsic_s_ceiling (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_ceiling       !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_ceiling
  end interface
  !
  interface
    function lsic_s_uminus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_uminus        !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_uminus
  end interface
  !
  interface
    function lsic_s_uplus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_uplus         !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_uplus
  end interface
  !
  interface
    function lsic_s_pyfunc (noper,nelem,nwords,n,result)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*4 Generic PYTHON function with variable number of arguments
      !        Called by DO_VECTOR
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_pyfunc  ! Function value on return
      integer(kind=4),              intent(in)  :: noper          ! Number of operands
      integer(kind=size_length),    intent(in)  :: nelem(noper)   ! # of elements for each operand
      integer(kind=address_length), intent(in)  :: nwords(noper)  ! Memory location for each operand
      integer(kind=size_length),    intent(in)  :: n              ! Size of result
      real(kind=4),                 intent(out) :: result(n)      ! Result
    end function lsic_s_pyfunc
  end interface
  !
  interface
    function lsic_s_erf(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_erf           !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_erf
  end interface
  !
  interface
    function lsic_s_erfc(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_erfc          !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_erfc
  end interface
  !
  interface
    function lsic_s_erfinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_erfinv        !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_erfinv
  end interface
  !
  interface
    function lsic_s_erfcinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_erfcinv       !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_erfcinv
  end interface
  !
  interface
    function lsic_s_bessel_i0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_i0     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_i0
  end interface
  !
  interface
    function lsic_s_bessel_i1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_i1     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_i1
  end interface
  !
  interface
    function lsic_s_bessel_in(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_in     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_bessel_in
  end interface
  !
  interface
    function lsic_s_bessel_j0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_j0     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_j0
  end interface
  !
  interface
    function lsic_s_bessel_j1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_j1     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_j1
  end interface
  !
  interface
    function lsic_s_bessel_jn(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_jn     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_bessel_jn
  end interface
  !
  interface
    function lsic_s_bessel_y0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_y0     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_y0
  end interface
  !
  interface
    function lsic_s_bessel_y1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_y1     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_s_bessel_y1
  end interface
  !
  interface
    function lsic_s_bessel_yn(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_s_bessel_yn     !
      integer(kind=size_length) :: n          !
      real(kind=4)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=4)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=4)              :: oper2(m2)  !
    end function lsic_s_bessel_yn
  end interface
  !
  interface
    function lsic_d_bplus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bplus         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_bplus
  end interface
  !
  interface
    function lsic_d_bminus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bminus        !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_bminus
  end interface
  !
  interface
    function lsic_d_mul (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_mul           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_mul
  end interface
  !
  interface
    function lsic_d_div (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_div           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_div
  end interface
  !
  interface
    function lsic_d_power (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_power         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_power
  end interface
  !
  interface
    function lsic_d_min (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_min           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_min
  end interface
  !
  interface
    function lsic_d_max (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_max           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_max
  end interface
  !
  interface
    function lsic_d_atan2 (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_atan2         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_atan2
  end interface
  !
  interface
    function lsic_d_dim (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_dim           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_dim
  end interface
  !
  interface
    function lsic_d_sign (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_sign          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_sign
  end interface
  !
  interface
    function lsic_d_mod (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_mod           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_mod
  end interface
  !
  interface
    function lsic_d_abs (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_abs           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_abs
  end interface
  !
  interface
    function lsic_d_acos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_acos          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_acos
  end interface
  !
  interface
    function lsic_d_asin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_asin          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_asin
  end interface
  !
  interface
    function lsic_d_atan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_atan          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_atan
  end interface
  !
  interface
    function lsic_d_cos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_cos           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_cos
  end interface
  !
  interface
    function lsic_d_atanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_atanh         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_atanh
  end interface
  !
  interface
    function lsic_d_cosh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_cosh          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_cosh
  end interface
  !
  interface
    function lsic_d_exp (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_exp           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_exp
  end interface
  !
  interface
    function lsic_d_int (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_int           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_int
  end interface
  !
  interface
    function lsic_d_log (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_log           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_log
  end interface
  !
  interface
    function lsic_d_log10 (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_log10         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_log10
  end interface
  !
  interface
    function lsic_d_nint (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_nint          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_nint
  end interface
  !
  interface
    function lsic_d_sin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_sin           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_sin
  end interface
  !
  interface
    function lsic_d_sinh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_sinh          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_sinh
  end interface
  !
  interface
    function lsic_d_sqrt (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_sqrt          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_sqrt
  end interface
  !
  interface
    function lsic_d_tan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_tan           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_tan
  end interface
  !
  interface
    function lsic_d_tanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_tanh          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_tanh
  end interface
  !
  interface
    function lsic_d_floor (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_floor         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_floor
  end interface
  !
  interface
    function lsic_d_ceiling (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_ceiling       !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_ceiling
  end interface
  !
  interface
    function lsic_d_uminus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_uminus        !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_uminus
  end interface
  !
  interface
    function lsic_d_uplus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_uplus         !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_uplus
  end interface
  !
  interface
    function lsic_d_pyfunc (noper,nelem,nwords,n,result)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! REAL*8 Generic PYTHON function with variable number of arguments
      !        Called by DO_VECTOR
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_pyfunc  ! Function value on return
      integer(kind=4),              intent(in)  :: noper          ! Number of operands
      integer(kind=size_length),    intent(in)  :: nelem(noper)   ! # of elements for each operand
      integer(kind=address_length), intent(in)  :: nwords(noper)  ! Memory location for each operand
      integer(kind=size_length),    intent(in)  :: n              ! Size of result
      real(kind=8),                 intent(out) :: result(n)      ! Result
    end function lsic_d_pyfunc
  end interface
  !
  interface
    function lsic_d_erf(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_erf           !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_erf
  end interface
  !
  interface
    function lsic_d_erfc(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_erfc          !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_erfc
  end interface
  !
  interface
    function lsic_d_erfinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_erfinv        !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_erfinv
  end interface
  !
  interface
    function lsic_d_erfcinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_erfcinv       !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_erfcinv
  end interface
  !
  interface
    function lsic_d_bessel_i0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_i0     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_i0
  end interface
  !
  interface
    function lsic_d_bessel_i1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_i1     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_i1
  end interface
  !
  interface
    function lsic_d_bessel_in(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_in     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_bessel_in
  end interface
  !
  interface
    function lsic_d_bessel_j0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_j0     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_j0
  end interface
  !
  interface
    function lsic_d_bessel_j1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_j1     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_j1
  end interface
  !
  interface
    function lsic_d_bessel_jn(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_jn     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_bessel_jn
  end interface
  !
  interface
    function lsic_d_bessel_y0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_y0     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_y0
  end interface
  !
  interface
    function lsic_d_bessel_y1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_y1     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: k          !
      integer                   :: dummy      !
    end function lsic_d_bessel_y1
  end interface
  !
  interface
    function lsic_d_bessel_yn(n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_d_bessel_yn     !
      integer(kind=size_length) :: n          !
      real(kind=8)              :: result(n)  !
      integer(kind=size_length) :: m1         !
      real(kind=8)              :: oper1(m1)  !
      integer(kind=size_length) :: m2         !
      real(kind=8)              :: oper2(m2)  !
    end function lsic_d_bessel_yn
  end interface
  !
  interface
    function lsic_i_bplus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bplus   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_bplus
  end interface
  !
  interface
    function lsic_i_bminus (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bminus  !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_bminus
  end interface
  !
  interface
    function lsic_i_mul (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_mul     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_mul
  end interface
  !
  interface
    function lsic_i_div (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Integer division: what should be the rule with negative operands?
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_div     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_div
  end interface
  !
  interface
    function lsic_i_power (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_power   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_power
  end interface
  !
  interface
    function lsic_i_min (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_min     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_min
  end interface
  !
  interface
    function lsic_i_max (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_max     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_max
  end interface
  !
  interface
    function lsic_i_atan2 (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_atan2   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_atan2
  end interface
  !
  interface
    function lsic_i_dim (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_dim     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_dim
  end interface
  !
  interface
    function lsic_i_sign (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_sign    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_sign
  end interface
  !
  interface
    function lsic_i_mod (n,result,m1,oper1,m2,oper2)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_mod     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: m2   !
      integer(kind=8) :: oper2(m2)      !
    end function lsic_i_mod
  end interface
  !
  interface
    function lsic_i_abs (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_abs     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_abs
  end interface
  !
  interface
    function lsic_i_acos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_acos    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_acos
  end interface
  !
  interface
    function lsic_i_asin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_asin    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_asin
  end interface
  !
  interface
    function lsic_i_atan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_atan    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_atan
  end interface
  !
  interface
    function lsic_i_atanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_atanh   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_atanh
  end interface
  !
  interface
    function lsic_i_cos (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_cos     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_cos
  end interface
  !
  interface
    function lsic_i_cosh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_cosh    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_cosh
  end interface
  !
  interface
    function lsic_i_exp (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_exp     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_exp
  end interface
  !
  interface
    function lsic_i_int (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_int     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_int
  end interface
  !
  interface
    function lsic_i_log (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_log     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_log
  end interface
  !
  interface
    function lsic_i_log10 (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_log10   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_log10
  end interface
  !
  interface
    function lsic_i_nint (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_nint    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_nint
  end interface
  !
  interface
    function lsic_i_sin (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_sin     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_sin
  end interface
  !
  interface
    function lsic_i_sinh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_sinh    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_sinh
  end interface
  !
  interface
    function lsic_i_sqrt (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_sqrt    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_sqrt
  end interface
  !
  interface
    function lsic_i_tan (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_tan     !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_tan
  end interface
  !
  interface
    function lsic_i_tanh (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_tanh    !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_tanh
  end interface
  !
  interface
    function lsic_i_floor (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_floor   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_floor
  end interface
  !
  interface
    function lsic_i_ceiling (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_ceiling  !
      integer(kind=size_length) :: n     !
      integer(kind=8) :: result(n)       !
      integer(kind=size_length) :: m1    !
      integer(kind=8) :: oper1(m1)       !
      integer(kind=size_length) :: k     !
      integer :: dummy                   !
    end function lsic_i_ceiling
  end interface
  !
  interface
    function lsic_i_uminus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_uminus  !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_uminus
  end interface
  !
  interface
    function lsic_i_uplus (n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_uplus   !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_uplus
  end interface
  !
  interface
    function lsic_i_pyfunc (noper,nelem,nwords,n,result)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! INTEGER*8 Generic PYTHON function with variable number of arguments
      !        Called by DO_VECTOR
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_pyfunc  ! Function value on return
      integer(kind=4),              intent(in)  :: noper          ! Number of operands
      integer(kind=size_length),    intent(in)  :: nelem(noper)   ! # of elements for each operand
      integer(kind=address_length), intent(in)  :: nwords(noper)  ! Memory location for each operand
      integer(kind=size_length),    intent(in)  :: n              ! Size of result
      integer(kind=8),              intent(out) :: result(n)      ! Result
    end function lsic_i_pyfunc
  end interface
  !
  interface
    function lsic_i_erf(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_erf    !
      integer(kind=size_length) :: n   !
      integer(kind=8) :: result(n)     !
      integer(kind=size_length) :: m1  !
      integer(kind=8) :: oper1(m1)     !
      integer(kind=size_length) :: k   !
      integer :: dummy                 !
    end function lsic_i_erf
  end interface
  !
  interface
    function lsic_i_erfc(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_erfc   !
      integer(kind=size_length) :: n   !
      integer(kind=8) :: result(n)     !
      integer(kind=size_length) :: m1  !
      integer(kind=8) :: oper1(m1)     !
      integer(kind=size_length) :: k   !
      integer :: dummy                 !
    end function lsic_i_erfc
  end interface
  !
  interface
    function lsic_i_erfinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_erfinv  !
      integer(kind=size_length) :: n    !
      integer(kind=8) :: result(n)      !
      integer(kind=size_length) :: m1   !
      integer(kind=8) :: oper1(m1)      !
      integer(kind=size_length) :: k    !
      integer :: dummy                  !
    end function lsic_i_erfinv
  end interface
  !
  interface
    function lsic_i_erfcinv(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_erfcinv  !
      integer(kind=size_length) :: n     !
      integer(kind=8) :: result(n)       !
      integer(kind=size_length) :: m1    !
      integer(kind=8) :: oper1(m1)       !
      integer(kind=size_length) :: k     !
      integer :: dummy                   !
    end function lsic_i_erfcinv
  end interface
  !
  interface
    function lsic_i_bessel_i0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_i0  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_i0
  end interface
  !
  interface
    function lsic_i_bessel_i1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_i1  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_i1
  end interface
  !
  interface
    function lsic_i_bessel_in(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_in  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_in
  end interface
  !
  interface
    function lsic_i_bessel_j0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_j0  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_j0
  end interface
  !
  interface
    function lsic_i_bessel_j1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_j1  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_j1
  end interface
  !
  interface
    function lsic_i_bessel_jn(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_jn  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_jn
  end interface
  !
  interface
    function lsic_i_bessel_y0(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_y0  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_y0
  end interface
  !
  interface
    function lsic_i_bessel_y1(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_y1  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_y1
  end interface
  !
  interface
    function lsic_i_bessel_yn(n,result,m1,oper1,k,dummy)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: lsic_i_bessel_yn  !
      integer(kind=size_length) :: n       !
      integer(kind=8) :: result(n)         !
      integer(kind=size_length) :: m1      !
      integer(kind=8) :: oper1(m1)         !
      integer(kind=size_length) :: k       !
      integer :: dummy                     !
    end function lsic_i_bessel_yn
  end interface
  !
  interface
    subroutine editor (file,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Edit a file using one of the following editors
      !       - VI
      !       - EMACS
      !  according to SIC EDITOR TT_EDIT variable, or GAG logical name
      !  GAG_EDIT.
      !---------------------------------------------------------------------
      character(len=*) :: file          ! File to be edited
      logical :: error                  ! Logical error flag
    end subroutine editor
  end interface
  !
  interface
    subroutine sic_trap
      use sic_interactions
      use gbl_message
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !
      !       Abort execution in non-interactive session after any error
      !---------------------------------------------------------------------
    end subroutine sic_trap
  end interface
  !
  interface
    subroutine break
      use gildas_def
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !
      !       Make a PAUSE
      !----------------------------------------------------------------------
      !
      ! Generate a traceback if in interactive mode
    end subroutine break
  end interface
  !
  interface
    subroutine traceback
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Generate traceback information
      !---------------------------------------------------------------------
      ! Local
    end subroutine traceback
  end interface
  !
  interface
    subroutine erreur(error,line,nline,lect,err)
      use sic_interactions
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   (Internal routine) Error handler
      !
      ! This routine traps all errors and take appropriate action:
      !  - Generate a PAUSE with traceback (Default)
      !  - or activate an error recovery command defined by the user in
      !    command "ON ERROR Command"
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical flag indicating if an error occured
      character(len=*) :: line         ! Command line to be executed     Output
      integer :: nline                 ! Length of line                  Output
      integer :: lect                  ! Level of execution              Input
      logical, intent(out)   :: err    !
    end subroutine erreur
  end interface
  !
  interface
    subroutine sic_on(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    ON Arg
      !  Disambiguise the first argument.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_on
  end interface
  !
  interface
    subroutine seterr(line,error)
      use gildas_def
      use gbl_message
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !   Defines the error recovery action by command ON ERROR Command
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine seterr
  end interface
  !
  interface
    subroutine evaluate_tree(operand,tree,last_node,max_level,min_level,result,  &
      type,err)
      use gildas_def
      use sic_dictionaries
      use sic_expressions
      use sic_interactions
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Transforms tree into result. Each node of the tree
      !       contains:
      !       - level of the node
      !       - code of operator
      !       - pointer to previous node
      !       - pointer to next node
      !       - number of associated operands
      !       - for each operand a pointer to operand descriptor
      !
      ! Arguments:
      !       TREE            I       Tree structure                  Input
      !       LAST_NODE       I       Pointer to last node in tree    Input
      !       MAX_LEVEL       I       Maximum parenthesis level       Input
      !       RESULT          I       Descriptor of Result            Input
      !       TYPE            I       Type of RESULT                  Output
      !       ERR             L       Logical error flag              Output
      ! Written       11-Feb-1987     T.Forveille
      !---------------------------------------------------------------------
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input)
      integer :: tree(formula_length*2)             !
      integer :: last_node                      !
      integer :: max_level                      !
      integer :: min_level                      !
      type(sic_descriptor_t) :: result          !
      integer :: type                           !
      logical :: err                            !
    end subroutine evaluate_tree
  end interface
  !
  interface
    subroutine do_tree(operand,tree,last_node,max_level,min_level,type,err)
      use gildas_def
      use sic_dictionaries
      use sic_expressions
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Transforms tree into result. Each node of the tree
      !       contains:
      !       - level of the node
      !       - code of operator
      !       - pointer to previous node
      !       - pointer to next node
      !       - number of associated operands
      !       - for each operand, a pointer to operand descriptor
      !
      ! Arguments:
      !       TREE            I       Tree structure                  Input
      !       LAST_NODE       I       Pointer to last node in tree    Input
      !       MAX_LEVEL       I       Maximum parenthesis level       Input
      !       TYPE            I       Type of RESULT                  Output
      !       ERR             L       Logical error flag              Output
      ! Written       11-Feb-1987     T.Forveille
      !---------------------------------------------------------------------
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input)
      integer :: tree(formula_length*2)             !
      integer :: last_node                      !
      integer :: max_level                      !
      integer :: min_level                      !
      integer :: type                           !
      logical :: err                            !
    end subroutine do_tree
  end interface
  !
  interface
    subroutine execute(operand,tree,first_node,last_node,level,min_level,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Executes all operations at level LEVEL
      !
      ! Arguments:
      !       TREE            I       Tree structure                  Input-Output
      !       FIRST_NODE      I       Pointer to first node in tree   Input-Output
      !       LAST_NODE       I       Pointer to last node in tree    Input-Output
      !       LEVEL           I       Parenthesis level               Input-Output
      !---------------------------------------------------------------------
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input-output)
      integer                           :: tree(formula_length*2)  !
      integer                           :: first_node      !
      integer                           :: last_node       !
      integer                           :: level           !
      integer                           :: min_level       !
      logical,            intent(inout) :: error           ! Logical error flag
    end subroutine execute
  end interface
  !
  interface
    subroutine do_call(code,noper,ioper,operand,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_interactions
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Evaluate one elementary numerical operation
      !
      ! Arguments:
      !       CODE            I       Code for operation              Input
      !       NOPER           I       Number of operands              Input
      !       IOPER           I       Pointers to operands            Input
      !       ERROR           L       Logical error flag              Output
      ! Written       11-Oct-1987     S.Guilloteau, T.Forveille
      !---------------------------------------------------------------------
      integer :: code                           !
      integer :: noper                          !
      integer :: ioper(noper)                   !
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input-output)
      logical :: error                          !
    end subroutine do_call
  end interface
  !
  interface
    subroutine numeric_call(code,noper,ioper,operand,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Evaluate one elementary numerical operation
      !
      ! Arguments:
      !       CODE            I       Code for operation    Input
      !       NOPER           I       Number of operands    Input
      !       IOPER           I       Pointers to operands  Input
      !       ERROR           L       Logical error flag    Output
      ! Written       11-Feb-1987     T.Forveille
      !---------------------------------------------------------------------
      integer :: code                           !
      integer :: noper                          !
      integer :: ioper(noper)                   !
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input-output)
      logical :: error                          !
    end subroutine numeric_call
  end interface
  !
  interface
    subroutine logic_call(code,noper,ioper,operand,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Evaluate one elementary logical operation
      !
      ! Arguments:
      !       CODE            I       Code for operation              Input
      !       NOPER           I       Number of operands              Input
      !       IOPER           I       Pointers to operands            Input
      !       ERROR           L       Logical error flag              Output
      ! Written       11-Feb-1987     T.Forveille
      !---------------------------------------------------------------------
      integer :: code                           !
      integer :: noper                          !
      integer :: ioper(noper)                   !
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input-output)
      logical :: error                          !
    end subroutine logic_call
  end interface
  !
  interface
    subroutine char_call(code,noper,ioper,operand,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Evaluate one elementary character operation
      !
      ! Arguments:
      !       CODE            I       Code for operation              Input
      !       NOPER           I       Number of operands              Input
      !       IOPER           I       Pointers to operands            Input
      !       ERROR           L       Logical error flag              Output
      ! Written       11-Feb-1987     T.Forveille
      !---------------------------------------------------------------------
      integer :: code                           !
      integer :: noper                          !
      integer :: ioper(noper)                   !
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands (input-output)
      logical :: error                          !
    end subroutine char_call
  end interface
  !
  interface
    subroutine size_call(noper,ioper,operand,error)
      use gbl_format
      use gbl_message
      use sic_expressions
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for function SIZE(Var,Dim)
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: noper               ! Number of operators involved
      integer(kind=4),        intent(in)    :: ioper(noper)        ! Pointers to operands
      type(sic_descriptor_t), intent(inout) :: operand(0:maxoper)  !
      logical,                intent(inout) :: error               ! Logical error flag
    end subroutine size_call
  end interface
  !
  interface
    subroutine index_call(noper,ioper,operand,error)
      use gbl_format
      use gbl_message
      use sic_expressions
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for function SIZE(Var,Dim)
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: noper               ! Number of operators involved
      integer(kind=4),        intent(in)    :: ioper(noper)        ! Pointers to operands
      type(sic_descriptor_t), intent(inout) :: operand(0:maxoper)  !
      logical,                intent(inout) :: error               ! Logical error flag
    end subroutine index_call
  end interface
  !
  interface
    subroutine get_precis(kind,noper,precis,error)
      use sic_expressions
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !  Determine automatically the precision (format) of elementary
      ! operation result from format of input operands.
      !
      !  fmt_i8 NOT returned in automatic precision (on purpose) because
      ! of the roundings it provides (not suited for scientific
      ! computations)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: noper        ! Number of operands
      integer(kind=4), intent(in)    :: kind(noper)  ! Format of input operands
      integer(kind=4), intent(out)   :: precis       ! Format of operation results
      logical,         intent(inout) :: error        ! Logical error flag
    end subroutine get_precis
  end interface
  !
  interface
    subroutine examine_variable(line,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC  Support routine for command
      ! SIC\EXAMINE [Variable]
      ! 1           [/GLOBAL]
      ! 2           [/FUNCTION]
      ! 3           [/HEADER]
      ! 4           [/ADDRESS]
      ! 5           [/ALIAS]
      ! 6           [/PAGE]
      ! 7           [/SAVE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine examine_variable
  end interface
  !
  interface
    subroutine sic_examine(name,error)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Support subroutine for command SIC\EXAMINE
      ! Given an input name or pattern, list one or all associated
      ! variables. Verbosity relies on the kind of input.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Name or pattern to find
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_examine
  end interface
  !
  interface
    subroutine sic_examine_brief(ik,error)
      use sic_dictionaries
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command SIC\EXAMINE
      ! Print out in short format e.g.
      ! PI              DOUBLE                                       GBL,RD
      !---------------------------------------------------------------------
      integer, intent(in)    :: ik     ! Variable index in dicvar
      logical, intent(inout) :: error  ! Error flag
    end subroutine sic_examine_brief
  end interface
  !
  interface
    subroutine sic_examine_strtype_byname(varname,global,verbose,chain,error)
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !   Return a type description suited e.g. for EXAMINE. See
      ! sic_examine_strtype_byid for examples
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: varname  ! Variable name
      logical,          intent(in)    :: global   ! Search for global or local?
      logical,          intent(in)    :: verbose  ! Verbose in case of error?
      character(len=*), intent(out)   :: chain    !
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_examine_strtype_byname
  end interface
  !
  interface
    subroutine sic_examine_strtype_byid(ik,msg)
      use gbl_format
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Return a type description suited e.g. for EXAMINE. Examples:
      !    LOGICAL
      !    CHARACTER*64
      !    INTEGER[3,4]
      !    (STRUCTURE)
      !    (IMAGE)REAL[512,512]
      !    etc
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: ik   ! Position in global dictionary
      character(len=*), intent(out) :: msg  ! Returned string
    end subroutine sic_examine_strtype_byid
  end interface
  !
  interface
    subroutine sic_examine_summary(name,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Examine the input variable. Output does not display the value and
      ! must fit in one line.
      ! PI                    ! Double  GLOBAL RO
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Variable to examine
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_examine_summary
  end interface
  !
  interface
    subroutine sic_examine_short(name,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Examine the input variable. Output (tries to) display the value but
      ! must fit in one line, e.g.
      ! SIC%EDITOR      = emacs            ...  ! Character*  256 GLOBAL RO
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Variable to examine
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_examine_short
  end interface
  !
  interface
    subroutine sic_examine_full(name,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Examine the input variable. Output can fit in several lines,
      ! e.g. arrays or long strings. Example:
      ! PI              =     3.141592653589793      ! Double  GLOBAL RO
      !---------------------------------------------------------------------
      character(len=*)                :: name   ! Variable to examine
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_examine_full
  end interface
  !
  interface
    subroutine sic_examine_save(name,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Examine the input variable. Output is suited for re-execution by Sic
      ! itself. Example:
      ! PI              =     3.141592653589793
      !---------------------------------------------------------------------
      character(len=*)                :: name   ! Variable to examine
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_examine_save
  end interface
  !
  interface
    subroutine sic_examine_print(name,mode,incarnable,olun,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! List the variable of specified name. Output displays the value or
      ! not, on 1 or several lines, depending on the tuned mode.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name        ! Variable to examine
      integer,          intent(in)    :: mode        ! Verbosity mode
      logical,          intent(in)    :: incarnable  ! Incarnations are allowed?
      integer(kind=4),  intent(in)    :: olun        ! Output LUN
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_examine_print
  end interface
  !
  interface
    subroutine sic_examine_type(name,utype,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! List the variable of specified name, Local and Globals
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name    !
      integer,          intent(out)   :: utype   !
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_examine_type
  end interface
  !
  interface
    subroutine sic_exambigs(name,error)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! List all matches of specified name, Local and Globals
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_exambigs
  end interface
  !
  interface
    subroutine sic_list_alias
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! List all the known aliases
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_list_alias
  end interface
  !
  interface
    subroutine sic_debug_variables
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !   SIC DEBUG VARIABLES
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_debug_variables
  end interface
  !
  interface
    function sic_error()
      !---------------------------------------------------------------------
      ! @ private
      ! Default error function
      !---------------------------------------------------------------------
      logical :: sic_error              ! Value on return
    end function sic_error
  end interface
  !
  interface
    subroutine run_gui(line,command,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Execute an elemental GUI command
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     !
      character(len=*), intent(in)    :: command  !
      logical,          intent(inout) :: error    !
    end subroutine run_gui
  end interface
  !
  interface
    function err_gui()
      !-----------------------------------------------------------------------
      ! @ private
      ! Default error function
      !-----------------------------------------------------------------------
      logical :: err_gui  ! Value en return
    end function err_gui
  end interface
  !
  interface
    subroutine sic_execute(line,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC\EXECUTE "Command arg /option arg"
      !    SIC\EXECUTE 'stringvar'
      !  Execute a command line from a string or from a string variable.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_execute
  end interface
  !
  interface
    subroutine expand_macro(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine expand_macro
  end interface
  !
  interface
    subroutine sic_add_expr (line,nl,output,no,error)
      use sic_expressions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Replace a function by its translation in command line
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      integer :: nl                     !
      character(len=*) :: output        !
      integer :: no                     !
      logical :: error                  !
    end subroutine sic_add_expr
  end interface
  !
  interface
    function sic_get_expr(func,nf)
      use sic_expressions
      !---------------------------------------------------------------------
      ! @ private
      !  Translate a function name to its identifier. Return 0 if no such
      ! function (not an error, up to the caller to decide)
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_get_expr  ! Function value on return
      character(len=*), intent(in) :: func  !
      integer(kind=4),  intent(in) :: nf    !
    end function sic_get_expr
  end interface
  !
  interface
    subroutine sic_list_expr (func,nf,error)
      use sic_expressions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  List 1 or all user-defined functions
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: func   !
      integer(kind=4),  intent(in)    :: nf     !
      logical,          intent(inout) :: error  !
    end subroutine sic_list_expr
  end interface
  !
  interface
    subroutine sic_del_expr(func,nf,error)
      use sic_expressions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Delete a user function
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: func   ! Function name
      integer(kind=4),  intent(in)    :: nf     ! Length of func
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_del_expr
  end interface
  !
  interface
    subroutine sic_def_expr (func,nf,line,nl,error)
      use sic_expressions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Define a user function (DEFINE FUNCTION)
      !---------------------------------------------------------------------
      character(len=*) :: func          !
      integer :: nf                     !
      character(len=*) :: line          !
      integer :: nl                     !
      logical :: error                  !
    end subroutine sic_def_expr
  end interface
  !
  interface
    function expr_oper1 (line,ip,in,nl)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !       Returns index of first character in line which could be part of
      !       an operator field.
      ! Arguments :
      !       LINE    B(N)    Character string to be formatted        Input
      !       IP      I       Index of character before operator	Output
      !	IN	I	Index of character after operator	Input/Output
      !       NL      I       Length of C                             Input
      !---------------------------------------------------------------------
      integer :: expr_oper1             !
      character(len=*) :: line          !
      integer :: ip                     !
      integer :: in                     !
      integer :: nl                     !
    end function expr_oper1
  end interface
  !
  interface
    function expr_oper2 (line,ip,in,nl)
      !---------------------------------------------------------------------
      ! @ private
      !       Returns index of first character in line which is a
      !	matching comma or closing parentheses
      ! Arguments :
      !       LINE    B(N)    Character string to be formatted        Input
      !       IP      I       Index of character before operator	Output
      !	IN	I	Index of character after operator	Input/Output
      !       NL      I       Length of C                             Input
      !---------------------------------------------------------------------
      integer :: expr_oper2             !
      character(len=*) :: line          !
      integer :: ip                     !
      integer :: in                     !
      integer :: nl                     !
    end function expr_oper2
  end interface
  !
  interface
    function expr_anoper (line)
      !---------------------------------------------------------------------
      ! @ private
      !       Returns index of first character in line which could be part of
      !       an operator field.
      ! Arguments :
      !       LINE    C*1     Character string to be formatted        Input
      !---------------------------------------------------------------------
      logical :: expr_anoper            !
      character(len=1) :: line          !
    end function expr_anoper
  end interface
  !
  interface
    subroutine mask_fill (idesc,odesc,ldesc)
      use gildas_def
      use gbl_format
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sic_descriptor_t) :: idesc  !
      type(sic_descriptor_t) :: odesc  !
      type(sic_descriptor_t) :: ldesc  !
    end subroutine mask_fill
  end interface
  !
  interface
    subroutine sic_variable_getdesc(caller,varname,readwrite,desc,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get the descriptor of a variable and perform the appropriate checks
      ! for use by the sic_variable_fill* subroutines
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: caller     ! Calling routine name
      character(len=*),       intent(in)    :: varname    !
      logical,                intent(in)    :: readwrite  ! Need read-write access?
      type(sic_descriptor_t), intent(out)   :: desc       !
      logical,                intent(inout) :: error      !
    end subroutine sic_variable_getdesc
  end interface
  !
  interface
    subroutine sic_descriptor_getnelem(desc,neleml,istarget,nelem0,ipnt0,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute and return the number of elements and the 'ipnt' of the Sic
      ! variable described by its descriptor, and check if it is consistent
      ! with 'neleml'.
      !  Technical subroutine mostly to be used by the sic_descriptor_fill*
      ! and sic_descriptor_getval* subroutines below.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),       intent(in)    :: desc      !
      integer(kind=size_length),    intent(in)    :: neleml    !
      logical,                      intent(in)    :: istarget  ! 'desc' is target or source of values?
      integer(kind=size_length),    intent(out)   :: nelem0    !
      integer(kind=address_length), intent(out)   :: ipnt0     !
      logical,                      intent(inout) :: error     !
    end subroutine sic_descriptor_getnelem
  end interface
  !
  interface
    subroutine sic_descriptor_get1elem(desc,istarget,ielem,ipnt0,error)
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get the address of the ielem-th element in array
      !---------------------------------------------------------------------
      type(sic_descriptor_t),       intent(in)    :: desc
      logical,                      intent(in)    :: istarget  ! 'desc' is target or source of values?
      integer(kind=size_length),    intent(in)    :: ielem
      integer(kind=address_length), intent(out)   :: ipnt0
      logical,                      intent(inout) :: error
    end subroutine sic_descriptor_get1elem
  end interface
  !
  interface
    subroutine sblanc(c,n,suite,ier)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Reduce the character string C to the standard SIC internal syntax:
      !  - Suppress all unecessary separators.
      !  - Character strings "abcd... " are not modified.
      !  - Reject blanks in single quote expressions e.g. '1 + 2 3' (same
      !    behaviour as LET A 1 + 2 3)
      !  - Ignores and destroys comments.
      !  - Take care of continuations lines
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: c      ! Character string to be formatted
      integer(kind=4),  intent(inout) :: n      ! Length of C
      logical,          intent(inout) :: suite  ! Indicates continuation
      integer(kind=4),  intent(out)   :: ier    ! Error return code
    end subroutine sblanc
  end interface
  !
  interface
    function sic_eqchain(a,b)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical :: sic_eqchain            !
      character(len=*) :: a             !
      character(len=*) :: b             !
    end function sic_eqchain
  end interface
  !
  interface
    subroutine sic_descriptor_get_getnelem(desc,neleml,nelem0,ipnt0,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute and return the number of elements and the 'ipnt' of the Sic
      ! variable described by its descriptor, and check if it is consistent
      ! with 'neleml'.
      !  Technical subroutine mostly to be used by the sic_descriptor_get*
      ! subroutines below.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),       intent(in)    :: desc    !
      integer(kind=size_length),    intent(in)    :: neleml  !
      integer(kind=size_length),    intent(out)   :: nelem0  !
      integer(kind=address_length), intent(out)   :: ipnt0   !
      logical,                      intent(inout) :: error   !
    end subroutine sic_descriptor_get_getnelem
  end interface
  !
  interface
    subroutine sic_descriptor_getr4(desc,r4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the r4(*) variable with the descriptor target values. A
      ! consistency check of the number of values is performed, and value is
      ! casted if needed to thee SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=4),              intent(out)   :: r4(*)   ! Flat array to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getr4
  end interface
  !
  interface
    subroutine sic_descriptor_getr8(desc,r8,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the r8(*) variable with the descriptor target values. A
      ! consistency check of the number of values is performed, and value is
      ! casted if needed to thee SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=8),              intent(out)   :: r8(*)   ! Flat array to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getr8
  end interface
  !
  interface
    subroutine getcom(ligne,nl,lire,invite,np,error)
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Gets a Command Line for the Interpretor
      ! Arguments
      !       LIGNE   Command line read               Output
      !       NL      Length of line                  Output
      !       LIRE    Input pointer                   Input
      !       INVITE  Prompt                          Input
      !       NP      Length of prompt                Input
      !       ERROR   Logical error flag              Output
      !---------------------------------------------------------------------
      character(len=*) :: ligne         !
      integer :: nl                     !
      integer :: lire                   !
      character(len=*) :: invite        !
      integer :: np                     !
      logical :: error                  !
    end subroutine getcom
  end interface
  !
  interface
    subroutine getlin(ligne,nc,j,i,next,m,buf)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  Retrieves a command line from an internal buffer
      !---------------------------------------------------------------------
      character(len=*),             intent(out)   :: ligne   ! Command line retrieved
      integer(kind=4),              intent(out)   :: nc      ! Length of line
      integer(kind=4),              intent(inout) :: j       ! Current buffer command number
      integer(kind=4),              intent(in)    :: i       ! First buffer command number
      integer(kind=4),              intent(inout) :: next    ! Pointer in buffer
      integer(kind=4),              intent(in)    :: m       ! Number of commands in buffer
      integer(kind=address_length), intent(in)    :: buf(*)  ! Internal buffer
    end subroutine getlin
  end interface
  !
  interface
    subroutine aroba (line,nl,error)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !
      !       Adds a blank space after an @ if the @ is the first char in LINE
      !       and is not already followed by a blank.
      !
      !       LINE    Command line to be modified     Updated
      !       NL      Length of line                  Updated
      !       ERROR   Logical error flag              Output
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      integer :: nl                     !
      logical :: error                  !
    end subroutine aroba
  end interface
  !
  interface
    subroutine sic_echo (invite,np,line,nl)
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Echo the command to the terminal in VERIFY mode
      ! Arguments
      !       INVITE  C*(*)   Prompt text
      !       NP      I       Length of prompt
      !       LINE    C*(*)   Command line including prompt
      !       NL      I       Length of LINE
      !---------------------------------------------------------------------
      character(len=*) :: invite        !
      integer :: np                     !
      character(len=*) :: line          !
      integer :: nl                     !
    end subroutine sic_echo
  end interface
  !
  interface
    subroutine find_edit
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Determines if editing is possible on this device
      !---------------------------------------------------------------------
    end subroutine find_edit
  end interface
  !
  interface
    function history_old_behaviour(dummy)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical :: history_old_behaviour  !
      integer :: dummy                  !
    end function history_old_behaviour
  end interface
  !
  interface
    function sub_kbdline(pr,chain,code,command)
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Read one command line from terminal. Lines from stack buffer can
      ! be recovered if ^P or ^N or UP and DOWN arrow are pressed. This
      ! routine is only called when line editing is enabled. COMMAND is
      ! true if waiting for a command.
      !---------------------------------------------------------------------
      integer(kind=4) :: sub_kbdline  ! Function value on return
      character(len=*) :: pr            !
      character(len=*) :: chain         !
      integer :: code                   !
      integer :: command                !
    end function sub_kbdline
  end interface
  !
  interface
    subroutine read_line(chain,nchain,invite,ninv)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !     Read one command line from terminal. Lines from stack buffer
      !     can be recovered if ^P or ^N or UP and DOWN arrow are pressed.
      !     This routine is only called when line editing is enabled.
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      integer :: nchain                 !
      character(len=*) :: invite        !
      integer :: ninv                   !
    end subroutine read_line
  end interface
  !
  interface
    subroutine get_line(chain,nchain,invite,ninv)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC
      !     Get line with prompt and editing.
      !     Lines from the stack cannot be recovered, but the original
      !     line can be restored when UP and DOWN arrows are pressed
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      integer :: nchain                 !
      character(len=*) :: invite        !
      integer :: ninv                   !
    end subroutine get_line
  end interface
  !
  interface
    subroutine edit_line(chain,nchain,invite,ninv)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC
      !     Get line with prompt and editing.
      !     Lines from the stack cannot be recovered, but the original
      !     line can be restored when UP and DOWN arrows are pressed
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      integer :: nchain                 !
      character(len=*) :: invite        !
      integer :: ninv                   !
    end subroutine edit_line
  end interface
  !
  interface
    subroutine sic_grep(line,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    SIC GREP Pattern File
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_grep
  end interface
  !
  interface
    subroutine sic_grep_do(filename,pattern,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename
      character(len=*), intent(in)    :: pattern
      logical,          intent(inout) :: error
    end subroutine sic_grep_do
  end interface
  !
  interface
    subroutine sic_grep_engine(lun,pattern,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: lun
      character(len=*), intent(in)    :: pattern
      logical,          intent(inout) :: error
    end subroutine sic_grep_engine
  end interface
  !
  interface
    function sic_hasins(rname,mdim,pf,pn,dict,var,in)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as type(sic_variable_t) arrays ***
      !       Insert a new name or replace an existing name
      !       in a dictionnary using a hashing structure
      !       and a list of pointers.
      !
      !       SIC_HASINS      0       E  Invalid name
      !                       1       S  Successfully inserted
      !                       2       E  Too many names
      !                       3       S  Successfully replaced
      !
      !       PF      I(0:27) PF(J) Address of first name beginning
      !                       with CHAR(J-ICHAR('A')) in dictionnary.
      !                       PF(26) is the address of the first
      !                       available place.                        Input/Output
      !                       PF(27) is the number of symbols
      !       PN      I(MDIM) Address of next name with same first
      !                       letter. If PN(J)=0, no more such name.  Input/Output
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_hasins  ! Function value on return
      character(len=*),       intent(in)    :: rname       ! Calling routine name
      integer(kind=4),        intent(in)    :: mdim        ! Size of dictionnary
      integer(kind=4),        intent(inout) :: pf(0:27)    !
      integer(kind=4),        intent(inout) :: pn(mdim)    !
      type(sic_variable_t),   intent(inout) :: dict(mdim)  ! Dictionnary array of size MDIM
      type(sic_identifier_t), intent(inout) :: var         ! Variable to be inserted
      integer(kind=4),        intent(out)   :: in          ! Address of this variable
    end function sic_hasins
  end interface
  !
  interface
    function sic_hasdel(mdim,pf,pn,dict,var)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as type(sic_variable_t) arrays ***
      !       Delete a name in a dictionnary using a hashing structure
      !       and a list of pointers.
      !
      !       SIC_HASDEL      1       Name deleted
      !                       3       Name does not exist
      !
      !       PF      I(0:27) PF(J) Address of first name beginning
      !                       with CHAR(J-ICHAR('A')) in dictionnary.
      !                       PF(26) is the address of the first
      !                       available place.                        Input/Output
      !                       PF(27) is the number of symbols
      !       PN      I(MDIM) Address of next name with same first
      !                       letter. If PN(J)=0, no more such name.  Input/Output
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_hasdel  ! Function value on return
      integer(kind=4),        intent(in)    :: mdim        ! Size of dictionnary
      integer(kind=4),        intent(inout) :: pf(0:27)    !
      integer(kind=4),        intent(inout) :: pn(mdim)    !
      type(sic_variable_t),   intent(in)    :: dict(mdim)  ! Dictionnary array of size MDIM
      type(sic_identifier_t), intent(inout) :: var         ! Variable to be deleted
    end function sic_hasdel
  end interface
  !
  interface
    function sic_hasfin(mdim,pf,pn,dict,var,in)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as type(sic_variable_t) arrays ***
      !       Find a name in a dictionnary using a hashing structure
      !       and a list of pointers.
      !
      !       SIC_HASFIN      0       No such name
      !                       1       Name found
      !       PF      I(0:27) PF(J) Address of first name beginning
      !                       with CHAR(J-ICHAR('A')) in dictionnary.
      !                       PF(26) is the address of the first
      !                       available place.                        Input
      !       PN      I(MDIM) Address of next name with same first
      !                       letter. If PN(J)=0, no more such name.  Input
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_hasfin                          ! Function value on return
      integer(kind=4),        intent(in)    :: mdim        ! Size of dictionnary
      integer(kind=4),        intent(in)    :: pf(0:27)    !
      integer(kind=4),        intent(in)    :: pn(mdim)    !
      type(sic_variable_t),   intent(in)    :: dict(mdim)  ! Dictionnary array of size MDIM
      type(sic_identifier_t), intent(inout) :: var         ! Variable to be found
      integer(kind=4),        intent(out)   :: in          ! Address of this variable
    end function sic_hasfin
  end interface
  !
  interface
    function sic_hasambigs(mdim,pf,pn,dict,name,if,il)
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as type(sic_variable_t) arrays ***
      !       Find all names beginning with the letters of NAME
      !       in  a dictionnary using a hashing structure
      !       and a list of pointers.
      !
      !       SIC_HASAMBIGS   0       No such name
      !                       n       N Names found
      !       PF      I(0:27) PF(J) Address of first name beginning
      !                       with CHAR(J-ICHAR('A')) in dictionnary.
      !                       PF(26) is the address of the first
      !                       available place.                        Input
      !       PN      I(MDIM) Address of next name with same first
      !                       letter. If PN(J)=0, no more such name.  Input
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_hasambigs                     ! Value on return
      integer(kind=4),      intent(in)  :: mdim        ! Size of dictionary
      integer(kind=4),      intent(in)  :: pf(0:27)    !
      integer(kind=4),      intent(in)  :: pn(mdim)    !
      type(sic_variable_t), intent(in)  :: dict(mdim)  ! Dictionary array
      character(len=*),     intent(in)  :: name        ! Name against whom search
      integer(kind=4),      intent(out) :: if          ! Adress of first Match
      integer(kind=4),      intent(out) :: il          ! Adress of  last Match
    end function sic_hasambigs
  end interface
  !
  interface
    subroutine sic_hassort(mdim,pf,pn,dict,list,leng)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as type(sic_variable_t) arrays ***
      !
      !  List alphabetically a dictionnary using a hashing structure and a
      ! list of pointers. This routine just builds the list of pointers to
      ! current entries.
      !
      !   PF(0:27)   PF(0:25) Address of first name beginning with
      !                       CHAR(J-ICHAR('A')) in dictionnary.
      !              PF(26)   is the address of the first available place.
      !              PF(27)   the number of symbols
      !   PN(MDIM)            Address of next name with same first letter.
      !                       If PN(J)=0, no more such name.
      !---------------------------------------------------------------------
      integer(kind=4),      intent(in)  :: mdim        ! Size of dictionnary
      integer(kind=4),      intent(in)  :: pf(0:27)    !
      integer(kind=4),      intent(in)  :: pn(mdim)    !
      type(sic_variable_t), intent(in)  :: dict(mdim)  ! Dictionary array
      integer(kind=4),      intent(out) :: list(mdim)  ! List of entries in dictionnary
      integer(kind=4),      intent(out) :: leng        ! Current number of entries
    end subroutine sic_hassort
  end interface
  !
  interface
    subroutine gag_hassort(mdim,pf,pn,dict,list,leng)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** This version for dictionaries as character string arrays ***
      !  (This subroutine should go in kernel/lib/gsys/hashing.f90 but
      !   the subroutine gi0_quicksort_index_with_user_gtge is needed.)
      !
      !  List alphabetically a dictionnary using a hashing structure and a
      ! list of pointers. This routine just builds the list of pointers to
      ! current entries.
      !
      !   PF(0:27)   PF(0:25) Address of first name beginning with
      !                       CHAR(J-ICHAR('A')) in dictionnary.
      !              PF(26)   is the address of the first available place.
      !              PF(27)   the number of symbols
      !   PN(MDIM)            Address of next name with same first letter.
      !                       If PN(J)=0, no more such name.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: mdim        ! Size of dictionnary
      integer(kind=4),  intent(in)  :: pf(0:27)    !
      integer(kind=4),  intent(in)  :: pn(mdim)    !
      character(len=*), intent(in)  :: dict(mdim)  ! Dictionnary array of size MDIM
      integer(kind=4),  intent(out) :: list(mdim)  ! List of entries in dictionnary
      integer(kind=4),  intent(out) :: leng        ! Current number of entries
    end subroutine gag_hassort
  end interface
  !
  interface
    subroutine vector_header(line,error)
      use image_def
      use gbl_message
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !  VECTOR\HEADER GDFFile|FITSFile|SicVariable
      ! 1 /EXTREMA         Add/update the extrema section
      ! 2 /TELESCOPE Name  Add/update the telescope section
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine vector_header
  end interface
  !
  interface
    subroutine vector_header_telescope(line,img,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   V\HEADER File /TELESCOPE [+-]Name1 ... [+-]NameN
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      type(gildas),     intent(inout) :: img
      logical,          intent(inout) :: error
    end subroutine vector_header_telescope
  end interface
  !
  interface
    subroutine gdf_fits_header(file,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command VECTOR\HEADER File
      !    for FITS file format
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: file
      logical, intent(inout) :: error
    end subroutine gdf_fits_header
  end interface
  !
  interface
    subroutine sic_help(line,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !   Read the help file from a sequential text file where command texts
      ! start with the command name and additional text start with a TAB or
      ! a SPACE.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_help
  end interface
  !
  interface
    subroutine help_general(error)
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! General help
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine help_general
  end interface
  !
  interface
    subroutine help_syntax
      use gildas_def
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Special case: syntax
      !---------------------------------------------------------------------
      ! Local
    end subroutine help_syntax
  end interface
  !
  interface
    subroutine help_language(lname,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Topic is a language
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: lname  ! Language name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine help_language
  end interface
  !
  interface
    subroutine help_language_list(llist,error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Build the list of languages
      !---------------------------------------------------------------------
      character(len=:), allocatable   :: llist(:)
      logical,          intent(inout) :: error
    end subroutine help_language_list
  end interface
  !
  interface
    subroutine help_command(cname,cnamesub,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !--------------------------------------------------------------------
      ! @ private
      ! Topic is a command. Try to find associated language
      !--------------------------------------------------------------------
      character(len=*), intent(in)    :: cname     ! Command name
      character(len=*), intent(in)    :: cnamesub  ! Subtopic name
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine help_command
  end interface
  !
  interface
    function sichelp(phelp,topic,subtopic,file,reset,quiet)
      use gbl_message
      use sic_structures
      use sic_interactions
      use gbl_ansicodes
      !---------------------------------------------------------------------
      ! @ private
      !  Output help from a SIC Standard Help File
      !  Returns an error status
      !---------------------------------------------------------------------
      logical :: sichelp                        ! Function value on return
      external                     :: phelp     ! Subroutine to output text
      character(len=*), intent(in) :: topic     ! Help topic (case sensitive)
      character(len=*), intent(in) :: subtopic  ! Help subtopic (case sensitive)
      character(len=*), intent(in) :: file      ! Help file name
      logical,          intent(in) :: reset     ! Reset the line counter?
      logical,          intent(in) :: quiet     ! No error message if (sub)topic not found?
    end function sichelp
  end interface
  !
  interface
    subroutine puthelp(line)
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Output a line on the current terminal taking into account the
      ! output mode (SCROLL or PAGE)
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  !
    end subroutine puthelp
  end interface
  !
  interface
    function html_help(topic,subtopic,jlang)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Read the help from html files starting with:
      !       GAG_HTML_program:node1.html
      !---------------------------------------------------------------------
      logical :: html_help                      ! Function value on return
      character(len=*), intent(in) :: topic     ! Help topic
      character(len=*), intent(in) :: subtopic  ! Help subtopic
      integer,          intent(in) :: jlang     ! Language number
    end function html_help
  end interface
  !
  interface
    subroutine help_run(tname,vname,icode,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Display task help.
      !  Meaning of icode:
      !   1: normal mode i.e. search for the task and display its help
      !   2: Just check for tasks with the same name and warn about them
      !      (help not displayed)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: tname  ! Task name
      character(len=*), intent(in)    :: vname  ! Variable name
      integer(kind=4),  intent(in)    :: icode  ! Printout code (HELP, or HELP TASK)
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine help_run
  end interface
  !
  interface
    subroutine help_task(group,error)
      use gildas_def
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Display a summary of all the available tasks
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: group  ! Group name (blank for all)
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine help_task
  end interface
  !
  interface
    subroutine help_function(fname,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Display a summary of all the available functions
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: fname  ! Function name (blank for all)
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine help_function
  end interface
  !
  interface
    subroutine help_procedure(procname,subname,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Display procedure help (HELP GO ProcName)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: procname  ! Procedure name
      character(len=*), intent(in)    :: subname   ! Subtopic name
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine help_procedure
  end interface
  !
  interface
    subroutine tkeys(task,key)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: task             !
      character(len=*), intent(in) :: key  !
    end subroutine tkeys
  end interface
  !
  interface
    subroutine help_run_html(task,argu,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Read the help file with the following structures :
      !   - HTML files in GAG_HTML: or GAG_HTML_language:
      !   - plus taskname.html in same area
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: task   ! Task name
      character(len=*), intent(in)    :: argu   ! Subtopic name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine help_run_html
  end interface
  !
  interface
    subroutine import_package(line,error)
      use gpack_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      !    Support routine for command SIC\IMPORT. Syntax is:
      !    IMPORT foo [/DEBUG]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine import_package
  end interface
  !
  interface
    subroutine fatal(ierr,lang)
      use gildas_def
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: ierr                   !
      character(len=*) :: lang          !
    end subroutine fatal
  end interface
  !
  interface
    subroutine sic_define_language(name,help,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Define a new user language
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Language name
      character(len=*), intent(in)    :: help   ! Help file
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_define_language
  end interface
  !
  interface
    subroutine sic_load_options
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Setup or setup again the options pointer for faster analysis
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_load_options
  end interface
  !
  interface
    subroutine sic_free_languages(error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Free the Sic languages
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine sic_free_languages
  end interface
  !
  interface
    subroutine sense_inter_state
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Sense automatically whether SIC has an interactive access or not.
      ! Action is different from sic_isatty() since there is also a special
      ! patch for XML widgets.
      ! + Compute some other global variables.
      !---------------------------------------------------------------------
      ! Local
    end subroutine sense_inter_state
  end interface
  !
  interface
    subroutine set_inter_state(flag)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Force the inter_state status.
      !---------------------------------------------------------------------
      logical, intent(in) :: flag
    end subroutine set_inter_state
  end interface
  !
  interface
    subroutine get_funcode(chain,i_code,nfunarg,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Sends back a code for the function contained in CHAIN
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain       ! The line to be analysed
      integer(kind=4),  intent(out)   :: i_code      ! Code for function
      integer(kind=4),  intent(out)   :: nfunarg(2)  ! Min-max number of arguments
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine get_funcode
  end interface
  !
  interface
    recursive subroutine read_operand_byfunc(funcode,chain,nch,descr,error)
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Specific operand parsing for functions which do not accept
      ! numerical operands as arguments.
      !  Recursive for e.g. evaluating ANY(A.EQ.1). The function ANY invokes
      ! its own build_tree+read_operand_byfunc to compute the array 'A.EQ.1'
      !---------------------------------------------------------------------
      integer(kind=4),        intent(inout) :: funcode  ! Function code, emptied in return
      character(len=*),       intent(in)    :: chain    ! The operand
      integer(kind=4),        intent(in)    :: nch      ! Length of chain
      type(sic_descriptor_t)                :: descr    ! Resulting descriptor of operand
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine read_operand_byfunc
  end interface
  !
  interface
    subroutine read_operand_exist(chain,nch,descr,error)
      use gildas_def
      use sic_interactions
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Find if a variable exist...
      !---------------------------------------------------------------------
      character(len=*),       intent(in)  :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)  :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out) :: descr  ! Operand descriptor
      logical,                intent(out) :: error  ! Logical error flag
    end subroutine read_operand_exist
  end interface
  !
  interface
    subroutine read_operand_descr(chain,nch,funcode,descr,error)
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Get one of the variable descriptor properties
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: chain    ! Chain containing operand
      integer(kind=4),        intent(in)    :: nch      ! Length of chain
      integer(kind=4),        intent(in)    :: funcode  ! Function code
      type(sic_descriptor_t), intent(out)   :: descr    ! Operand descriptor
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine read_operand_descr
  end interface
  !
  interface
    subroutine read_operand_len(chain,nch,trimmed,descr,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Return the length (trimmed or not) of a Sic variable
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: chain    ! Chain containing operand
      integer(kind=4),        intent(in)    :: nch      ! Length of chain
      logical,                intent(in)    :: trimmed  ! Trimmed value?
      type(sic_descriptor_t), intent(out)   :: descr    ! Operand descriptor
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine read_operand_len
  end interface
  !
  interface
    subroutine read_operand_symb(chain,nch,descr,error)
      use gildas_def
      use sic_structures
      use sic_interactions
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Find if a SYMBOL exist...
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)    :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out)   :: descr  ! Operand descriptor
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine read_operand_symb
  end interface
  !
  interface
    subroutine read_operand_func(chain,nch,descr,error)
      use gildas_def
      use sic_structures
      use sic_interactions
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Find if a function exist...
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)    :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out)   :: descr  ! Operand descriptor
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine read_operand_func
  end interface
  !
  interface
    subroutine read_operand_file(chain,nch,descr,error)
      use gildas_def
      use sic_structures
      use sic_interactions
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Find if a file exist...
      !  The input file name (CHAIN) can be any "implicitely formatted" SIC
      ! expression.
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)    :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out)   :: descr  ! Operand descriptor
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine read_operand_file
  end interface
  !
  interface
    subroutine read_operand_allorany(chain,nch,doall,odesc,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Return true if all or any elements of a logical array are true
      !---------------------------------------------------------------------
      character(len=*),       intent(in)  :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)  :: nch    ! Length of chain
      logical,                intent(in)  :: doall  ! Do ALL or ANY?
      type(sic_descriptor_t), intent(out) :: odesc  ! Output descriptor
      logical,                intent(out) :: error  ! Logical error flag
    end subroutine read_operand_allorany
  end interface
  !
  interface
    subroutine read_operand_isnan(chain,nch,odesc,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Return a logical array which is true for each value evaluating as
      ! NaN
      !---------------------------------------------------------------------
      character(len=*),       intent(in)  :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)  :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out) :: odesc  ! Output descriptor
      logical,                intent(out) :: error  ! Logical error flag
    end subroutine read_operand_isnan
  end interface
  !
  interface
    subroutine read_operand_isnum(chain,nch,descr,error)
      use gildas_def
      use sic_interactions
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Find if a string is a number or scalar number variable...
      !---------------------------------------------------------------------
      character(len=*),       intent(in)  :: chain  ! Chain containing operand
      integer(kind=4),        intent(in)  :: nch    ! Length of chain
      type(sic_descriptor_t), intent(out) :: descr  ! Operand descriptor
      logical,                intent(out) :: error  ! Logical error flag
    end subroutine read_operand_isnum
  end interface
  !
  interface
    subroutine let_variable (line,nline,error)
      use gbl_message
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !       Support routine for command
      !       LET Variable [=] Arithmetic expression
      !     [/NEW Type [Attr]]
      !     [/PROMPT "Prompt text"]
      !     [/WHERE Logical_Expression]
      !     [/RANGE Min Max]
      !     [/CHOICE C1 .. CN [*]]
      !     [/FILE Filter ]
      !     [/INDEX C1 .. CN]
      !     [/SEXAGESIMAL]
      !     [/LOWER]
      !     [/UPPER]
      !     [/FORMAT String]
      !     [/FORMULA]
      !     [/REPLACE]
      !     [/STATUS Read|Write]
      !     [/NOQUOTE]
      !     [/RESIZE]
      !
      ! Upper level to allow HEADER and STRUCTURE assignment
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      integer(kind=4),  intent(in)    :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine let_variable
  end interface
  !
  interface
    subroutine let_avar(line,nameout,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_structures
      use sic_dictionaries
      use sic_expressions
      use sic_interactions
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  LET Variable [=] Arithmetic expression [/OPTIONS]
      !
      ! Lower level for individual variable assignment
      !---------------------------------------------------------------------
      character(len=*)                :: line     !
      character(len=*), intent(inout) :: nameout  ! Variable name
      logical,          intent(inout) :: error    !
    end subroutine let_avar
  end interface
  !
  interface
    subroutine let_avar_numeric(line,var,spec,impvars,argfirst,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_expressions
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for LET (numeric or logical output variable)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: line                  ! Input command line
      type(sic_variable_t),   intent(in)    :: var                   ! Output variable
      type(sic_dimensions_t), intent(inout) :: spec                  !
      integer(kind=4),        intent(in)    :: impvars(sic_maxdims)  !
      integer(kind=4),        intent(in)    :: argfirst              !
      logical,                intent(inout) :: error                 !
    end subroutine let_avar_numeric
  end interface
  !
  interface
    subroutine let_avar_character(line,var,spec,argfirst,error)
      use gbl_message
      use sic_dictionaries
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for LET (numeric or logical output variable)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: line      ! Input command line
      type(sic_variable_t),   intent(in)    :: var       ! Output variable
      type(sic_dimensions_t), intent(inout) :: spec      !
      integer(kind=4),        intent(in)    :: argfirst  !
      logical,                intent(inout) :: error     !
    end subroutine let_avar_character
  end interface
  !
  interface
    subroutine let_new(line,varname,new_type,error)
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  LET Var Arg1 ... ArgN /NEW Type [GLOBAL|LOCAL]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line      !
      character(len=*), intent(inout) :: varname   ! Variable name
      character(len=*), intent(out)   :: new_type  !
      logical,          intent(inout) :: error     !
    end subroutine let_new
  end interface
  !
  interface
    subroutine let_replace(line,varout,error)
      use gildas_def
      use gbl_message
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   LET NewName [=] OldName /REPLACE
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: varout
      logical,          intent(inout) :: error
    end subroutine let_replace
  end interface
  !
  interface
    subroutine let_status(line,error)
      use gildas_def
      use gbl_message
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   LET Variable /STATUS READ|WRITE
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine let_status
  end interface
  !
  interface
    subroutine let_resize(in,var,firstarg,error)
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Support subroutine for command
      !  LET Var [=] Arg1 ... ArgN /RESIZE
      !---------------------------------------------------------------------
      integer(kind=4),        intent(inout) :: in        ! Variable index (updated in return)
      type(sic_identifier_t), intent(inout) :: var       ! Variable identifier
      integer(kind=4),        intent(in)    :: firstarg  ! Position of Arg1 in command line
      logical,                intent(inout) :: error     ! Logical error flag
    end subroutine let_resize
  end interface
  !
  interface
    subroutine let_xwindow(line,nameout,iout,i_command,error)
      use gbl_format
      use sic_dictionaries
      use sic_interactions
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !  Update the variable value seen in the widget, when this value is
      ! changed from the command line, e.g.
      !   @ gag_demo:demo-widget
      !   exa demo%widget%main%winteger      ! returns 123
      !   let demo%widget%main%winteger 456  ! value in the widget turns to 456
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Input command line
      character(len=*), intent(in)    :: nameout    ! Output variable (name)
      integer(kind=4),  intent(in)    :: iout       ! Output variable (index in dicvar)
      integer(kind=4),  intent(out)   :: i_command  !
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine let_xwindow
  end interface
  !
  interface
    subroutine let_header(line,nameout,error)
      use gildas_def
      use gbl_message
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=*) :: nameout       !
      logical :: error                  !
    end subroutine let_header
  end interface
  !
  interface
    subroutine let_copyheader(in,out)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: in
      type(gildas), intent(inout) :: out
    end subroutine let_copyheader
  end interface
  !
  interface
    subroutine assign_structure(input,output,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(sic_identifier_t) :: input         !
      type(sic_identifier_t) :: output        !
      logical                :: error         !
    end subroutine assign_structure
  end interface
  !
  interface
    subroutine sic_letformat (line,iarg,argum,error)
      use gildas_def
      use gbl_message
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      integer(kind=4)                 :: iarg   !
      character(len=*)                :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_letformat
  end interface
  !
  interface
    subroutine let_avar_sexag(line,var,spec,error)
      use gildas_def
      use gbl_format
      use phys_const
      use gbl_message
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command LET Out [=] In /SEXA [D|H|R] [D|H|R]
      ! In and Out may be Numeric and String or vice-versa.
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: line   ! Input command line
      type(sic_variable_t),   intent(in)    :: var    ! Output variable
      type(sic_dimensions_t), intent(inout) :: spec   !
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine let_avar_sexag
  end interface
  !
  interface
    subroutine list2(l,k,r,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Execute a set of loops defined by sic_parse_listr8
      !       R               Current value
      !       K               Current loop number
      !---------------------------------------------------------------------
      type(sic_listr8_t), intent(in)    :: l      !
      integer(kind=4),    intent(inout) :: k      !
      real(kind=8),       intent(inout) :: r      !
      logical,            intent(out)   :: error  !
    end subroutine list2
  end interface
  !
  interface
    subroutine iniloo (line,nline,error)
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Support for command
      !               FOR Variable n1 TO n2 ....
      !       or      FOR Variable /IN List_Variable_1D
      !       or      FOR /WHILE Logical_Expression
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      integer(kind=4),  intent(in)  :: nline  ! Line length
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine iniloo
  end interface
  !
  interface
    subroutine insloo (line,nline,error)
      use gildas_def
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC
      !   Support for FOR command 
      !   Insert a command line into the current loop buffer
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      integer(kind=4),  intent(in)    :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine insloo
  end interface
  !
  interface
    subroutine desinsloo
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! This if an error occurs, to be used immediately to forget erroneous
      ! line.
      !---------------------------------------------------------------------
    end subroutine desinsloo
  end interface
  !
  interface
    subroutine exeloo (error)
      use gbl_format
      use gbl_message
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Execute a FOR - NEXT loop : activated by a NEXT command
      !---------------------------------------------------------------------
      logical, intent(out) :: error  !
    end subroutine exeloo
  end interface
  !
  interface
    subroutine finloo
      use gbl_message
      use sic_structures
      use sic_dictionaries
      !----------------------------------------------------------------------
      ! @ private
      ! End of a loop execution
      !----------------------------------------------------------------------
      ! Local
    end subroutine finloo
  end interface
  !
  interface
    subroutine aboloo
      use gbl_message
      use sic_structures
      !----------------------------------------------------------------------
      ! @ private
      ! Abort a loop compilation : decrease loop pointers and exit
      ! compilation mode if needed
      !----------------------------------------------------------------------
      ! Local
    end subroutine aboloo
  end interface
  !
  interface
    subroutine check_loop_variable(loop_number,line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !   Support for FOR command
      !   Check the non-existence of the loop variable specified in command
      ! FOR, and insert it in dictionary.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: loop_number  ! Loop number
      character(len=*), intent(in)    :: line         ! Command line
      logical,          intent(inout) :: error        ! Error flag
    end subroutine check_loop_variable
  end interface
  !
  interface
    subroutine delete_loop_variable(loop_number)
      use sic_dictionaries
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Delete loop variable
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: loop_number
    end subroutine delete_loop_variable
  end interface
  !
  interface
    subroutine verify_loop_variable(loop_number,line,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !   Support for NEXT Variable
      !   Verify variable name match with the FOR command
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: loop_number  !
      character(len=*), intent(in)    :: line         !
      logical,          intent(inout) :: error        !
    end subroutine verify_loop_variable
  end interface
  !
  interface
    subroutine check_loop_logical(loop_number,line,error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Check that the expression specified in /WHILE option of FOR command
      ! is a valid logical expression.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: loop_number  !
      character(len=*), intent(in)    :: line         !
      logical,          intent(inout) :: error        !
    end subroutine check_loop_logical
  end interface
  !
  interface
    subroutine begin_forin(loop_number,error)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !     FOR Var /IN List
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: loop_number  ! Current loop number
      logical,          intent(inout) :: error        ! Error flag
    end subroutine begin_forin
  end interface
  !
  interface
    subroutine sic_lunmac_init(error)
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Initialise private logical units used by SIC
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_lunmac_init
  end interface
  !
  interface
    subroutine sic_lunmac_exit(error)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Free all the pre-allocated LUNs
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine sic_lunmac_exit
  end interface
  !
  interface
    subroutine sic_lunmac_get(imac,error)
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Entry to allocate one LUN for a macro call
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: imac
      logical,         intent(inout) :: error
    end subroutine sic_lunmac_get
  end interface
  !
  interface
    subroutine sic_lunmac_free(imac)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Entry to free one LUN for a macro call
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: imac
    end subroutine sic_lunmac_free
  end interface
  !
  interface
    subroutine exemac(line,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Execute a macro
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine exemac
  end interface
  !
  interface
    subroutine endmac
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! End of macro execution
      !-----------------------------------------------------------------------
      !
      ! Close the unit and reset the active macro number
    end subroutine endmac
  end interface
  !
  interface
    subroutine sic_math_auto (chain,nch,argum,ocode,error)
      use gildas_def
      use sic_expressions
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Decode a string as a double precision OR LOGICAL argument :
      !   Attempt to read it
      !   Find if it is a variable
      !   Compute arithmetic expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      real(kind=8),     intent(out)   :: argum  ! 2 words buffer for output result
      integer(kind=4),  intent(out)   :: ocode  ! Format of output result
      logical,          intent(inout) :: error  !
    end subroutine sic_math_auto
  end interface
  !
  interface
    subroutine sic_math_desc(chain,nch,ofmt,osize,desc,error)
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_expressions
      !---------------------------------------------------------------------
      ! @ private
      !  Evaluate an expression and return the descriptor of the result. The
      ! result can be an array.
      ! The result can be a scratch variable, it is the responsibility of
      ! the caller to free it afterwards.
      ! ---
      !  This subroutine is a factorization of LET /WHERE XXX (parsing of
      ! the 'XXX' vectorized condition). It may need modification to make
      ! it fully generic.
      !---------------------------------------------------------------------
      character(len=*),          intent(in)           :: chain  !
      integer(kind=4),           intent(in)           :: nch    !
      integer(kind=4),           intent(in)           :: ofmt   ! Output result format
      integer(kind=size_length), intent(in), optional :: osize  ! Output size
      type(sic_descriptor_t),    intent(out)          :: desc   !
      logical,                   intent(inout)        :: error  !
    end subroutine sic_math_desc
  end interface
  !
  interface
    subroutine sic_vtype(namein,vtype,reado,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: namein        !
      integer :: vtype                  !
      logical :: reado                  !
      logical :: error                  !
    end subroutine sic_vtype
  end interface
  !
  interface
    subroutine adjust_mcmc (comm,line,error)
      use gildas_def
      use gbl_format
      use sic_adjust
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !     Support for commands
      !
      !     EMCEE [Data Command] [/BEGIN] [/START A1 B1 C1 ...] [/STEP A2 B2 C2 ...]
      !     [/Bounds Par Low Up [...]] [/Parameters Va Vb Vc ...]
      !     [/ROOT  Name] [/WALKERS NWalk] [/LENGTH Length]
      !
      ! Allows fitting arbitrary data with arbitrary number of parameters
      ! using a MCMC routine
      !---------------------------------------------------------------------
      character(len=*)                :: comm   !
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine adjust_mcmc
  end interface
  !
  interface
    subroutine adjust_display(comm,line,error)
      use gildas_def
      use sic_adjust
      use sic_types
      use gbl_message
      !------------------------------------------------------------------
      ! @ private
      !   ADJUST support for ESHOW
      !   ESHOW AUTOCORR|CHAINS|ERRORS|RESULTS|TRIANGLES [Args]
      !     [/BURN] [/SPLIT]
      !------------------------------------------------------------------
      character(len=*), intent(in) :: line
      character(len=*), intent(in) :: comm
      logical, intent(out) :: error
    end subroutine adjust_display
  end interface
  !
  interface
    subroutine all_from_internal
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine all_from_internal
  end interface
  !
  interface
    subroutine from_internal(ivar,xint,xext)
      use sic_adjust
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: ivar       ! Variable number
      real(8), intent(in) :: xint       ! Internal value
      real(8), intent(out) :: xext      ! Outside value
    end subroutine from_internal
  end interface
  !
  interface
    subroutine all_to_adjust
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine all_to_adjust
  end interface
  !
  interface
    subroutine to_adjust(ivar,xint,xext)
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(in) :: ivar       ! Variable number
      real(8), intent(out) :: xint      ! Internal value
      real(8), intent(inout) :: xext    ! Outside value
    end subroutine to_adjust
  end interface
  !
  interface
    subroutine run_adjust(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support the ADJUST Language
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*), intent(in)    :: comm   !
      logical,          intent(inout) :: error  !
    end subroutine run_adjust
  end interface
  !
  interface
    subroutine init_mfit
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      ! Initialize the MFIT driver routine address
      !---------------------------------------------------------------------
    end subroutine init_mfit
  end interface
  !
  interface
    subroutine fit_expression (comm,line,error)
      use gildas_def
      use gbl_format
      use sic_adjust
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !     Support for commands
      !
      !     ADJUST Data Command [/START A1 B1 C1 ...] [/STEP A2 B2 C2 ...]
      !     [/EPSILON e] [/WEIGHTS w] [/METHOD Powell|Robust|Simplex|Slatec|Anneal]
      !     [/ITER Niter] [/QUIET]  /Bounds Par Low Up [...] /Parameters Va Vb Vc ...
      !     [/ROOT]
      !
      ! and
      !     MFIT Y=f(X) [/START A1,B1,C1,...] [/STEP A2,B2,C2,...]
      !     [/EPSILON e] [/WEIGHTS w] [/METHOD Powell|Robust|Simplex|Slatec|Anneal]
      !     [/ITER Niter] [/QUIET] /Bounds &Par Low Up
      !
      ! Allows fitting arbitrary data with arbitrary number of parameters
      !
      ! POWELL and SIMPLEX methods use the library routines from Press et al.,
      ! Numerical Recipes modified to allow easy error recovery.
      !
      ! SLATEC method uses the Slatec library, modified Levenberg-Marquardt
      ! routine DNLS1E, and also computes the error bars on the parameters.
      ! Much faster, but sensitive to starting values...
      !
      ! ROBUST uses a combination of SIMPLEX to get started and SLATEC to finish with
      !
      ! ANNEAL uses a simulated annealing. Very robust, but slow... It finishes
      ! with a SLATEC method to compute the errors.
      !---------------------------------------------------------------------
      character(len=*)                :: comm   !
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine fit_expression
  end interface
  !
  interface
    subroutine minimize_all (np,darray,xstart,xstep,weight,epsilon,do_anneal,  &
      do_simplex,do_powell,do_slatec,do_errorbars,error,iter,quiet)
      use gildas_def
      use sic_adjust
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Adjust parameters in the least-square sense for a general problem.
      !
      ! Uses a set of minimization routines
      ! - Simplex
      ! - Gradient (Powell method)
      ! - Simulated annealing
      ! - Levenberg Marquardt (from SLATEC)
      ! and error bars through the Jacobian
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: np            ! Number of parameters to be adjusted
      type(sic_descriptor_t), intent(in)    :: darray        ! Descriptor of Data array
    ! character(len=*)                      :: cstart        ! Starting values
    ! character(len=*)                      :: cstep         ! Increments to compute derivatives
      real(kind=4),           intent(in)    :: xstart(np)    ! Starting values
      real(kind=4),           intent(in)    :: xstep(np)     ! Increments to compute derivatives
      type(sic_descriptor_t), intent(in)    :: weight        ! Descriptor of Weight array
      real(kind=8),           intent(inout) :: epsilon       ! Precision to decided convergence
      logical,                intent(in)    :: do_anneal     !
      logical,                intent(in)    :: do_simplex    !
      logical,                intent(in)    :: do_powell     !
      logical,                intent(in)    :: do_slatec     !
      logical,                intent(inout) :: do_errorbars  !
      logical,                intent(inout) :: error         ! Error return flag
      integer(kind=4),        intent(in)    :: iter          !
      logical,                intent(in)    :: quiet         !
    end subroutine minimize_all
  end interface
  !
  interface
    subroutine mini_dnls(iflag,mdat,npar,x,fvec,fjac,ldfjac)
      use sic_adjust
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Driver routine for the Levenberq-Marquardt
      !---------------------------------------------------------------------
      integer, intent(inout)  :: iflag        ! Operation Flag
      integer, intent(in)  :: mdat            ! Number of data points
      integer, intent(in)  :: npar            ! Number of variable fitted parameters
      real(8), intent(in)  :: x(npar)         ! Variable fitted parameters
      real(8), intent(inout) :: fvec(mdat)    ! Difference between value and data
      integer, intent(in)  :: ldfjac          ! First dimension of fjac
      real(8), intent(out) :: fjac(ldfjac,*)  ! Jacobian
    end subroutine mini_dnls
  end interface
  !
  interface
    subroutine mini_siman(npar,x,value,ier)
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Driver routine for the Simulated Annealing
      !---------------------------------------------------------------------
      integer, intent(in) :: npar          ! Number of parameters
      real(kind=8), intent(in) :: x(npar)  ! Parameter values
      real(kind=8), intent(out) :: value   ! Resulting Chi2
      integer, intent(inout) :: ier        ! Error code
    end subroutine mini_siman
  end interface
  !
  interface
    subroutine get_formula(formula,fitexpr,darray,pdim,quiet,error)
      use gildas_def
      use sic_adjust
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Fit a given formula to a set of data points using the
      ! DNLS1E method form SLATEC (Modified Levenberg-Marquardt routine)
      !---------------------------------------------------------------------
      character(len=*) :: formula                ! Formula used for minimisation
      character(len=*) :: fitexpr                ! Formula used for minimisation
      type(sic_descriptor_t) :: darray           !
      integer :: pdim                            !
      logical :: quiet                           !
      logical :: error                           !
    end subroutine get_formula
  end interface
  !
  interface
    subroutine txt_array(chain,ndim,values,default)
      !---------------------------------------------------------------------
      ! @ private
      ! Read a real array from a comma separated text string.
      ! Return the default in case of error.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: chain  ! Input chain (modified)
      integer, intent(in) :: ndim               ! Number of values
      real(4), intent(out) :: values(ndim)      ! Output values
      real(4), intent(in) :: default(ndim)      ! Default values
    end subroutine txt_array
  end interface
  !
  interface
    function txt_real (text, default)
      !---------------------------------------------------------------------
      ! @ private
      ! Read a real variable from a text string. Return the default
      ! in case of error.
      !---------------------------------------------------------------------
      real :: txt_real                      !  Output value
      character(len=*), intent(in) :: text  !  Input string
      real(4), intent(in) :: default        !  Default value
    end function txt_real
  end interface
  !
  interface
    subroutine create_fitvar (in,out,y_array,err)
      use gildas_def
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Copy a SIC variable into R(8) array
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: in                 ! Input SIC variable name
      character(len=*), intent(in) :: out                ! Output DOUBLE variable
      type(sic_descriptor_t), intent(out) :: y_array     ! Output variable descriptor
      logical, intent(out) :: err                        ! Error flag
    end subroutine create_fitvar
  end interface
  !
  interface
    subroutine m_varcop (in,out,out_array,global,err)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Create a copy of a SIC variable
      !---------------------------------------------------------------------
      character(len=*) :: in                        ! Input variable name
      character(len=*) :: out                       ! Output variable name
      type(sic_descriptor_t) :: out_array           ! Output variable descriptor
      logical :: global                             ! Global status
      logical :: err                                ! Error flag
    end subroutine m_varcop
  end interface
  !
  interface
    subroutine replace_string (in,old,new,out,l1,l2,l3,nr)
      !---------------------------------------------------------------------
      ! @ private
      ! Replace occurrence of string OLD in IN by NEW and store the new string in
      ! OUT. String lengths for the input strings have to be provided.
      ! Number of replacements is returned in integer NR.
      !---------------------------------------------------------------------
      character(len=*) :: in            !
      character(len=*) :: old           !
      character(len=*) :: new           !
      character(len=*) :: out           !
      integer :: l1                     !
      integer :: l2                     !
      integer :: l3                     !
      integer :: nr                     !
    end subroutine replace_string
  end interface
  !
  interface
    subroutine sic_modify(line,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC\MODIFY  VarName|GDFName
      ! 1  /FREQUENCY Linename RestFreq
      ! 2  /VELOCITY Velocity
      ! 3  /SPECUNIT Unit
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine sic_modify
  end interface
  !
  interface
    subroutine sic_modify_variable(cline,desc,error)
      use gildas_def
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC\MODIFY VarName
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: cline  !
      type(sic_descriptor_t), intent(inout) :: desc   !
      logical,                intent(inout) :: error  !
    end subroutine sic_modify_variable
  end interface
  !
  interface
    subroutine sic_modify_file(cline,filename,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    SIC\MODIFY GDFName
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: cline     !
      character(len=*), intent(in)    :: filename  !
      logical,          intent(inout) :: error     !
    end subroutine sic_modify_file
  end interface
  !
  interface
    subroutine sic_modify_header(cline,x,error)
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Modify consistently:
      !   - the line name
      !   - the rest frequency
      !   - the velocity
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: cline  !
      type(gildas),     intent(inout) :: x      !
      logical,          intent(inout) :: error  !
    end subroutine sic_modify_header
  end interface
  !
  interface
    subroutine get_resu (form,dimr,noper,ioper,operand,iresul,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !  Get a new pointer to descriptor of result.
      !---------------------------------------------------------------------
      integer :: form                           ! Format of result (R4, R8, L)
      integer(kind=size_length) :: dimr         ! Dimension of result
      integer :: noper                          ! Number of operands
      integer :: ioper(noper)                   ! Pointer to operand descriptors
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands
      integer :: iresul                         ! Pointer to result descriptor
      logical :: error                          ! Logical error flag
    end subroutine get_resu
  end interface
  !
  interface
    subroutine free_oper (form,dimr,noper,ioper,operand,iresul,error)
      use gildas_def
      use sic_expressions
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Free useless operands and move pointers to current result.
      !---------------------------------------------------------------------
      integer :: form                           ! Format of result (R4, R8, L)
      integer(kind=size_length) :: dimr         ! Dimension of result
      integer :: noper                          ! Number of operands
      integer :: ioper(noper)                   ! Pointer to operand descriptors
      type(sic_descriptor_t) :: operand(0:maxoper)  ! List of operands
      integer :: iresul                         ! Pointer to result descriptor
      logical :: error                          ! Logical error flag
    end subroutine free_oper
  end interface
  !
  interface
    function press_func (para)
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: press_func                !
      real :: para(*)                   !
    end function press_func
  end interface
  !
  interface
    subroutine press_amoeba(p,y,mp,np,ndim,ftol,my_func,iter)
      use sic_adjust
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: mp                     !
      integer :: np                     !
      real :: p(mp,np)                  !
      real :: y(mp)                     !
      integer :: ndim                   !
      real :: ftol                      !
      real, external :: my_func         !
      integer :: iter                   !
    end subroutine press_amoeba
  end interface
  !
  interface
    subroutine press_powell(p,xi,n,np,ftol,iter,fret,my_func,my_f1dim)
      use sic_adjust
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: np                     !
      real :: p(np)                     !
      real :: xi(np,np)                 !
      integer :: n                      !
      real :: ftol                      !
      integer :: iter                   !
      real :: fret                      !
      real, external :: my_func         !
      real, external :: my_f1dim        !
    end subroutine press_powell
  end interface
  !
  interface
    subroutine press_linmin(p,xi,n,fret,ftol,my_f1dim)
      use sic_adjust ! Is it compatible ?
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: n                      !
      real :: p(n)                      !
      real :: xi(n)                     !
      real :: fret                      !
      real :: ftol                      !
      real, external :: my_f1dim        !
    end subroutine press_linmin
  end interface
  !
  interface
    function press_brent(ax,bx,cx,my_f1dim,tol,xmin)
      use sic_adjust
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: press_brent               !
      real :: ax                        !
      real :: bx                        !
      real :: cx                        !
      real, external :: my_f1dim        !
      real :: tol                       !
      real :: xmin                      !
    end function press_brent
  end interface
  !
  interface
    subroutine press_mnbrak(ax,bx,cx,fa,fb,fc,my_func)
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: ax                        !
      real :: bx                        !
      real :: cx                        !
      real :: fa                        !
      real :: fb                        !
      real :: fc                        !
      real, external :: my_func         !
    end subroutine press_mnbrak
  end interface
  !
  interface
    function press_f1dim(x)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: press_f1dim               !
      real :: x                         !
    end function press_f1dim
  end interface
  !
  interface
    subroutine open_procedure(line,error)
      use gildas_def
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine open_procedure
  end interface
  !
  interface
    subroutine write_procedure(com,line,nline,error)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: com    !
      character(len=*)                :: line   !
      integer                         :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine write_procedure
  end interface
  !
  interface
    subroutine close_procedure(com,line,nline,error)
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: com    !
      character(len=*)                :: line   !
      integer                         :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine close_procedure
  end interface
  !
  interface
    subroutine end_procedure
      use gildas_def
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine end_procedure
  end interface
  !
  interface
    subroutine find_procedure(name,file,found)
      use gildas_def
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the procedure file name (path+name+extension) given the
      ! input name ([path+]name[+extension])
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Name to be resolved
      character(len=*), intent(out)   :: file   ! Path to the file
      logical,          intent(inout) :: found  ! Found a procedure or not?
    end subroutine find_procedure
  end interface
  !
  interface
    subroutine begin_area(line,error)
      use gildas_def
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine begin_area
  end interface
  !
  interface
    subroutine bldprt (klire)
      use sic_structures
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Modify the suffix of the prompt, suited for the current execution
      ! level and execution mode
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: klire  ! Execution level
    end subroutine bldprt
  end interface
  !
  interface
    subroutine gprompt_get(pr)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Get current prompt
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: pr
    end subroutine gprompt_get
  end interface
  !
  interface
    subroutine sic_accept(line,error)
      use gildas_def
      use gbl_message
      use gbl_format
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Support routine for command
      !   ACCEPT Var1 [Var2 [...]]
      !    3     [/COLUMN Filename [Separ]]
      !    4     [/FORMAT Form]
      !    5     [/LINE l1 l2]
      ! or
      !   ACCEPT Var1
      !    1 [/ARRAY  Filename]
      !    4 [/FORMAT Form]
      !    5 [/LINE l1 l2]
      ! or
      !   ACCEPT Var1
      !    2 [/BINARY Filename Offset]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  !
    end subroutine sic_accept
  end interface
  !
  interface
    subroutine sic_accept_array(line,lun,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ACCEPT /ARRAY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line      ! Input command line
      integer(kind=4),  intent(in)    :: lun       !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_accept_array
  end interface
  !
  interface
    subroutine sic_accept_binary(line,lun,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ACCEPT /BINARY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line      ! Input command line
      integer(kind=4),  intent(in)    :: lun       !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_accept_binary
  end interface
  !
  interface
    subroutine sic_accept_column(line,lun,line1,line2,error)
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ACCEPT /COLUMN
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line    ! Input command line
      integer(kind=4),  intent(in)    :: lun     ! Logical unit
      integer(kind=4),  intent(in)    :: line1   !
      integer(kind=4),  intent(in)    :: line2   !
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_accept_column
  end interface
  !
  interface
    subroutine sic_accept_column_noformat(line,lun,line1,line2,narg,fmt,ip,is,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ACCEPT /COLUMN (no /FORMAT)
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: line       ! Input command line
      integer(kind=4),              intent(in)    :: lun        !
      integer(kind=4),              intent(in)    :: line1      !
      integer(kind=4),              intent(in)    :: line2      !
      integer(kind=4),              intent(in)    :: narg       !
      integer(kind=4),              intent(in)    :: fmt(narg)  !
      integer(kind=address_length), intent(in)    :: ip(narg)   !
      integer(kind=size_length),    intent(in)    :: is(narg)   !
      logical,                      intent(inout) :: error      ! Logical error flag
    end subroutine sic_accept_column_noformat
  end interface
  !
  interface
    subroutine sic_accept_column_format(line,lun,line1,narg,fmt,ip,is,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ACCEPT /COLUMN /FORMAT
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: line       ! Input command line
      integer(kind=4),              intent(in)    :: lun        !
      integer(kind=4),              intent(in)    :: line1      !
      integer(kind=4),              intent(in)    :: narg       !
      integer(kind=4),              intent(in)    :: fmt(narg)  !
      integer(kind=address_length), intent(inout) :: ip(narg)   !
      integer(kind=size_length),    intent(in)    :: is(narg)   !
      logical,                      intent(inout) :: error      ! Logical error flag
    end subroutine sic_accept_column_format
  end interface
  !
  interface
    subroutine sic_accept_getvar(line,iarg,maxdims,type,ip,siz,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Return the some elements describing the variable, and make some
      ! checks
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: line     ! Input command line
      integer(kind=4),              intent(in)    :: iarg     ! Argument position
      integer(kind=4),              intent(in)    :: maxdims  ! Maximum dimension allowed
      integer(kind=4),              intent(out)   :: type     ! Variable type
      integer(kind=address_length), intent(out)   :: ip       ! Data address
      integer(kind=size_length),    intent(out)   :: siz      ! Number of elements
      logical,                      intent(inout) :: error    ! Logical error flag
    end subroutine sic_accept_getvar
  end interface
  !
  interface
    subroutine sic_accept_getformat(line,opt_form,format,nf,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Parse the command line for the /FORMAT arguments
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Input command line
      logical,          intent(out)   :: opt_form   ! /FORMAT is present?
      character(len=*), intent(out)   :: format     ! Argument
      integer(kind=4),  intent(out)   :: nf         ! Length of format
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_accept_getformat
  end interface
  !
  interface
    subroutine read_one_ch(lun,form,format,ier,array,size)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4),           intent(in)  :: lun     !
      logical,                   intent(in)  :: form    !
      character(len=*),          intent(in)  :: format  !
      integer(kind=4),           intent(out) :: ier     !
      character(len=*),          intent(out) :: array   !
      integer(kind=size_length), intent(in)  :: size    !
    end subroutine read_one_ch
  end interface
  !
  interface
    subroutine read_all_var(lun,form,format,ier,ilin,ip,is,fmt,size,narg,memory,  &
      work,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! This assumes that any kind of values can be stored in double
      ! precision floats, which is false for long integers.
      !---------------------------------------------------------------------
      integer(kind=4),              intent(in)    :: lun         !
      logical,                      intent(in)    :: form        !
      character(len=*),             intent(in)    :: format      !
      integer(kind=4),              intent(out)   :: ier         !
      integer(kind=4),              intent(out)   :: ilin        !
      integer(kind=4),              intent(in)    :: narg        !
      integer(kind=address_length), intent(inout) :: ip(narg)    !
      integer(kind=size_length),    intent(in)    :: is(narg)    !
      integer(kind=4),              intent(in)    :: fmt(narg)   !
      integer(kind=size_length),    intent(in)    :: size        !
      integer(kind=4)                             :: memory(*)   !
      real(kind=8)                                :: work(narg)  ! Working array
      logical,                      intent(inout) :: error       !
    end subroutine read_all_var
  end interface
  !
  interface
    subroutine sic_accept_oneline(separ,line,nvar,vtype,val,error)
      use gildas_def
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Read the input line and fill the variables thanks to their type
      ! and pointer to their value.
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: separ     ! Separator
      character(len=*),             intent(in)    :: line      ! Valid input line to read
      integer(kind=4),              intent(in)    :: nvar      ! Number of variables to fill
      integer(kind=4)                             :: vtype(*)  ! Variable types
      integer(kind=address_length), intent(in)    :: val(*)    ! Pointers to variables
      logical,                      intent(inout) :: error     ! Logical error flag
    end subroutine sic_accept_oneline
  end interface
  !
  interface
    subroutine sic_compute (line,nline,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !    COMPUTE OutVar OPERATION InVar [Parameters]
      !  1         /BLANKING Bval [Eval]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      integer(kind=4),  intent(in)    :: nline  ! Command line length
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_compute
  end interface
  !
  interface
    subroutine fftsub(line,is,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !	COMPUTE Varout FFT+ VarIn [REAL]
      ! or	COMPUTE Varout FFT- VarIn [REAL]
      !
      ! Where
      !	VarIn is the input variable, of dimension
      !		[N,1] if REAL, [N,2] if COMPLEX
      !	VarOut is the output variable, of dimension
      !		[N,2]
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      integer                         :: is     !
      logical,          intent(inout) :: error  !
    end subroutine fftsub
  end interface
  !
  interface
    subroutine fourts(line,is,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !       COMPUTE VarOut FOURT+ VarIn
      !       COMPUTE VarOut FOURT- VarIn
      !
      ! Where
      !       VarIn is the input variable, of type Complex
      !               [NX,NY,Nz,Nw]
      !       VarOut is the output variable, of type Complex
      !               [NX,NY,Nz,Nw]
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      integer                         :: is     !
      logical,          intent(inout) :: error  !
    end subroutine fourts
  end interface
  !
  interface
    subroutine cmp_complex (line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine cmp_complex
  end interface
  !
  interface
    subroutine cmp_real (line,code,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      character(len=*)                :: code   !
      logical,          intent(inout) :: error  !
    end subroutine cmp_real
  end interface
  !
  interface
    subroutine cmp_operation(line,key,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      character(len=*), intent(in)    :: key    ! Kind of operation ( + - / *)
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine cmp_operation
  end interface
  !
  interface
    subroutine compute_gagdate(line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !     Convert an ASCII date in format DD-MM-YYYY in
      !     internal "GAG" code, as used by CLASS and CLIC.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine compute_gagdate
  end interface
  !
  interface
    subroutine compute_histo(line,vblank8,eblank8,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      ! [SIC\]COMPUTE OutVar HISTOGRAM InVar [Hmin] [Hmax]
      ! 1             /BLANKING Bval [Eval]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     !
      real(kind=8),     intent(in)    :: vblank8  !
      real(kind=8),     intent(in)    :: eblank8  !
      logical,          intent(inout) :: error    !
    end subroutine compute_histo
  end interface
  !
  interface
    subroutine compute_file(line,key,error)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*), intent(in)    :: key    !
      logical,          intent(inout) :: error  !
    end subroutine compute_file
  end interface
  !
  interface
    subroutine compute_file_date(file,type,value,error)
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the file date from 01-jan-1970. If 'type' is I*4, returned
      ! value are seconds. If 'type' is I*8, returned value are nanoseconds.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: file   !
      integer(kind=4),  intent(in)    :: type   !
      integer(kind=8),  intent(out)   :: value  !
      logical,          intent(inout) :: error  !
    end subroutine compute_file_date
  end interface
  !
  interface
    subroutine compute_dimof(line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   COMPUTE VarOut DIMOF VarIn
      ! where
      !   VarIn is the input variable, of dimension
      !     [Nx,Ny,Nz,Nv,Na,Nb,Nc]
      !   VarOut is the output variable, of dimension
      !     [8]
      !   containing Nx,Ny,Nz,Nv,Na,Nb,Nc,Ndim
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine compute_dimof
  end interface
  !
  interface
    subroutine compute_location(line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! COMPUTE OutVar LOCATION Invar Value
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine compute_location
  end interface
  !
  interface
    subroutine compute_primitive (line,code,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      character(len=*)                :: code   !
      logical,          intent(inout) :: error  !
    end subroutine compute_primitive
  end interface
  !
  interface
    subroutine compute_rankorder (line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine compute_rankorder
  end interface
  !
  interface
    subroutine compute_gather (line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! COMPUTE Out GATHER In
      ! Builds (in Out) the list of values found in In
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine compute_gather
  end interface
  !
  interface
    subroutine compute_btest(line,error)
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   COMPUTE VarOut BTEST VarIn
      ! Bit Test (same as Fortran BTEST)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine compute_btest
  end interface
  !
  interface
    subroutine compute_isasicvar (line,error)
      use gildas_def
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! COMPUTE Out IS_A_SIC_VAR In
      !   Test whether In is a known SIC variable
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine compute_isasicvar
  end interface
  !
  interface
    subroutine reduce (line,key,vblank8,eblank8,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   COMPUTE MAX|MIN|MEAN|RMS|SUM|PRODUCT|MEDIAN
      ! (mathematical reduction functions)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     !
      character(len=*), intent(in)    :: key      !
      real(kind=8),     intent(in)    :: vblank8  !
      real(kind=8),     intent(in)    :: eblank8  !
      logical,          intent(inout) :: error    !
    end subroutine reduce
  end interface
  !
  interface
    subroutine collect_w4tow4(ndim,a,n1,n2,n3,n4,n5,n6,n7,w,j1,j2,j3,j4,j5,j6)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  Word-to-word collection of dimensions. Type is not relevant, use
      ! integer type (avoid IEEE floats because bits in NaN values may not
      ! preserved during copy)
      ! This version for sic_maxdims==7
      !---------------------------------------------------------------------
      integer(kind=4),            intent(in)  :: ndim
      integer(kind=index_length), intent(in)  :: n1,n2,n3,n4,n5,n6,n7
      real(kind=4),               intent(in)  :: a(n1,n2,n3,n4,n5,n6,n7)
      real(kind=4),               intent(out) :: w(*)
      integer(kind=index_length), intent(in)  :: j1,j2,j3,j4,j5,j6
    end subroutine collect_w4tow4
  end interface
  !
  interface
    subroutine collect_w8tow8(ndim,a,n1,n2,n3,n4,n5,n6,n7,w,j1,j2,j3,j4,j5,j6)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  Word-to-word collection of dimensions. Type is not relevant, use
      ! integer type (avoid IEEE floats because bits in NaN values may not
      ! preserved during copy)
      ! This version for sic_maxdims==7
      !---------------------------------------------------------------------
      integer(kind=4),            intent(in)  :: ndim
      integer(kind=index_length), intent(in)  :: n1,n2,n3,n4,n5,n6,n7
      real(kind=8),               intent(in)  :: a(n1,n2,n3,n4,n5,n6,n7)
      real(kind=8),               intent(out) :: w(*)
      integer(kind=index_length), intent(in)  :: j1,j2,j3,j4,j5,j6
    end subroutine collect_w8tow8
  end interface
  !
  interface
    subroutine collect_r8tor4(ndim,a,n1,n2,n3,n4,n5,n6,n7,w,j1,j2,j3,j4,j5,j6)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  real(kind=8)-to-real(kind=4) collection of dimensions (including float
      ! conversion at the same time).
      ! This version for sic_maxdims==7
      !---------------------------------------------------------------------
      integer(kind=4),            intent(in)  :: ndim
      integer(kind=index_length), intent(in)  :: n1,n2,n3,n4,n5,n6,n7
      real(kind=8),               intent(in)  :: a(n1,n2,n3,n4,n5,n6,n7)
      real(kind=4),               intent(out) :: w(*)
      integer(kind=index_length), intent(in)  :: j1,j2,j3,j4,j5,j6
    end subroutine collect_r8tor4
  end interface
  !
  interface
    subroutine comp_r4_mean(x,n,vblank4,eblank4,out)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar MEAN InVar  (single precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=4),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=4),              intent(in)  :: vblank4  !
      real(kind=4),              intent(in)  :: eblank4  !
      real(kind=4),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r4_mean
  end interface
  !
  interface
    subroutine comp_r4_sum(x,n,vblank4,eblank4,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar SUM InVar  (single precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=4),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=4),              intent(in)  :: vblank4  !
      real(kind=4),              intent(in)  :: eblank4  !
      real(kind=4),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r4_sum
  end interface
  !
  interface
    subroutine comp_r4_prod(x,n,vblank4,eblank4,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar PRODUCT InVar  (single precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=4),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=4),              intent(in)  :: vblank4  !
      real(kind=4),              intent(in)  :: eblank4  !
      real(kind=4),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r4_prod
  end interface
  !
  interface
    subroutine comp_r4_rms (x,n,vblank4,eblank4,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar RMS InVar  (single precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=4),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=4),              intent(in)  :: vblank4  !
      real(kind=4),              intent(in)  :: eblank4  !
      real(kind=4),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r4_rms
  end interface
  !
  interface
    subroutine comp_r4_median(x,n,vblank4,eblank4,out)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar MEDIAN InVar  (single precision)
      ! If Blanking enabled, will return blanking value if no valid result.
      ! If Blanking is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=4),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=4),              intent(in)  :: vblank4  !
      real(kind=4),              intent(in)  :: eblank4  !
      real(kind=4),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r4_median
  end interface
  !
  interface
    subroutine comp_r8_mean(x,n,vblank8,eblank8,out)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar MEAN InVar  (double precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=8),              intent(in)  :: vblank8  !
      real(kind=8),              intent(in)  :: eblank8  !
      real(kind=8),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r8_mean
  end interface
  !
  interface
    subroutine comp_r8_sum(x,n,vblank8,eblank8,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar SUM InVar  (double precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=8),              intent(in)  :: vblank8  !
      real(kind=8),              intent(in)  :: eblank8  !
      real(kind=8),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r8_sum
  end interface
  !
  interface
    subroutine comp_r8_prod(x,n,vblank8,eblank8,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar PRODUCT InVar  (double precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=8),              intent(in)  :: vblank8  !
      real(kind=8),              intent(in)  :: eblank8  !
      real(kind=8),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r8_prod
  end interface
  !
  interface
    subroutine comp_r8_rms (x,n,vblank8,eblank8,out)
      !$ use omp_lib
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar RMS InVar  (double precision)
      ! Protected against NaNs and Using Blanking Values. If Blanking
      ! enabled, will return blanking value if no valid result. If Blanking
      ! is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=8),              intent(in)  :: vblank8  !
      real(kind=8),              intent(in)  :: eblank8  !
      real(kind=8),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r8_rms
  end interface
  !
  interface
    subroutine comp_r8_median(x,n,vblank8,eblank8,out)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Support routine for command
      !   COMPUTE OutVar MEDIAN InVar  (double precision)
      ! If Blanking enabled, will return blanking value if no valid result.
      ! If Blanking is not enabled, will return NaN if no valid result.
      !---------------------------------------------------------------------
      real(kind=8),              intent(in)  :: x(*)     ! Data values to compute
      integer(kind=size_length), intent(in)  :: n        ! Number of data values
      real(kind=8),              intent(in)  :: vblank8  !
      real(kind=8),              intent(in)  :: eblank8  !
      real(kind=8),              intent(out) :: out      ! Output scalar value
    end subroutine comp_r8_median
  end interface
  !
  interface
    subroutine sic_fits(line,global,error)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !
      !     Support routine for command DEFINE FITS
      !
      ! SIC\DEFINE FITS  Var File.fits  [Key1 Key2 Key3 Key4]
      !    where the optional Key is any of [index#|Header|Transpose|Basic]
      !    index# being the extension number
      !
      ! READS a FITS FILE, and defines/load SIC variables according to
      ! what is found.
      !      if index# is present, only load this Xtension number
      !      if Header is present, do not load the main data
      !      if Transpose is present, transpose Tables in Xtension
      !      if Basic is present, only load basic keywords, no fancy ones
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! Command line
      logical, intent(in) :: global                  ! Global status of variable
      logical, intent(out) :: error                  ! Error flag
    end subroutine sic_fits
  end interface
  !
  interface
    subroutine fits_read_extension(headofstruct,ns,global,error,eof,iext,define,  &
      transpo)
      use gildas_def
      use gfits_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS
      !     Support routine for reading extensions
      !   DEFINE FITS  /EXTENSION extension_id
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: headofstruct  !  Structure name
      integer, intent(in) :: ns                     !
      logical, intent(in) :: global                 !  Global status
      logical, intent(out) :: error                 !  Error flag
      logical, intent(out) :: eof                   !  End of File indicator
      integer, intent(in) :: iext                   !  Extension number
      logical, intent(in) :: define                 !  Define or skip variables ?
      logical, intent(in) :: transpo                !  Transpose array ?
    end subroutine fits_read_extension
  end interface
  !
  interface
    subroutine fits_read_image(head,ns,global,error,eof,iext,define)
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS
      !     Support routine for XTENSION of type IMAGE
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: head          !  Structure name
      integer, intent(in) :: ns                     !  Length of name
      logical, intent(in) :: global                 !  Global status
      logical, intent(out) :: error                 !  Error flag
      logical, intent(out) :: eof                   !  End of File indicator
      integer, intent(in) :: iext                   !  Extension number
      logical, intent(in) :: define                 !  Define or skip variables ?
    end subroutine fits_read_image
  end interface
  !
  interface
    subroutine get_btable_item(item,nitem,fmtout,buffer,fmtin,error,jrow)
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      ! Transfers (and translate) a data vector from BINTABLE row.
      !---------------------------------------------------------------------
      integer(kind=1), intent(out)   :: item(*)    ! Requested information
      integer(kind=4), intent(in)    :: nitem      ! Number of requested items
      integer(kind=4), intent(in)    :: fmtout     ! Format for requested items
      integer(kind=1), intent(in)    :: buffer(*)  ! Row buffer
      integer(kind=4), intent(in)    :: fmtin      ! Format of data in buffer
      logical,         intent(inout) :: error      ! Error flag
      integer(kind=4), intent(in)    :: jrow       ! Row number
    end subroutine get_btable_item
  end interface
  !
  interface
    subroutine get_table_item(item,fmtout,line,i,j,fmt,blank,error)
      use gildas_def
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Transfers (and translate) a data vector from TABLE row.
      !---------------------------------------------------------------------
      integer(kind=1), intent(out) :: item(*)       ! Requested information
      integer, intent(in) :: fmtout                 ! Output Format
      character(len=*), intent(in) :: line          ! Row buffer
      integer, intent(in) :: i                      ! Start position
      integer, intent(in) :: j                      ! End position
      character(len=12), intent(in) :: fmt          ! Input format
      real, intent(in) :: blank                     ! Blanking value
      logical, intent(inout) :: error               ! Error flag
    end subroutine get_table_item
  end interface
  !
  interface
    subroutine fits_defstructure(strname,global,error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Define a new Structure, with an unambiguous name/version
      !---------------------------------------------------------------------
      character(len=*)                :: strname  !
      logical                         :: global   !
      logical,          intent(inout) :: error    !
    end subroutine fits_defstructure
  end interface
  !
  interface
    subroutine fits_defvariable(strname,nrows,global,error)
      use gildas_def
      use sic_structures
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   This version passes a character description of the variable
      !   to the generic SIC function SIC_DEFVARIABLE.
      !   This solves problems of allocation and character arrays
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: strname  ! Structure name
      integer,          intent(in)    :: nrows    ! Number of rows
      logical,          intent(in)    :: global   ! Global flag
      logical,          intent(inout) :: error    ! Error flag
    end subroutine fits_defvariable
  end interface
  !
  interface
    subroutine fits_decode_binpar(error)
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Try to decode the column description (for binary tables).
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error flag
    end subroutine fits_decode_binpar
  end interface
  !
  interface
    subroutine fits_decode_par(error)
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !     Try to decode the column description (for ascii tables).
      !     Adds the '(' around the format for further use
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error flag
    end subroutine fits_decode_par
  end interface
  !
  interface
    subroutine sic_underscore(a)
      !---------------------------------------------------------------------
      ! @ private
      !  Replace all SIC reserved characters by "_" to produce a valid SIC
      ! variable name. Length of string not modified.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: a  ! Variable name to be checked
    end subroutine sic_underscore
  end interface
  !
  interface
    subroutine fits_read_basic(head,ns,global,fmt,nsize,ndim,dim,error,extend,  &
      therank,data,skipsz)
      use gildas_def
      use gbl_constant
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Read a basic FITS header
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: head              ! Header name
      integer(kind=4),            intent(in)    :: ns                ! Length of name
      logical,                    intent(in)    :: global            ! Global / Local status
      integer(kind=4),            intent(in)    :: fmt               ! Format of data
      integer(kind=size_length),  intent(in)    :: nsize             ! Size in output words 
      integer(kind=4),            intent(inout) :: ndim              ! Number of dimensions
      integer(kind=index_length), intent(inout) :: dim(sic_maxdims)  ! Dimensions
      logical,                    intent(inout) :: error             ! Error flag
      logical,                    intent(out)   :: extend            ! Are there extensions ?
      integer(kind=4),            intent(in)    :: therank           ! Desired trimming
      logical,                    intent(in)    :: data              ! Do you want the data ?
      integer(kind=size_length),  intent(in)    :: skipsz            ! Size in bytes in FITS data
    end subroutine fits_read_basic
  end interface
  !
  interface
    subroutine fits_read_rndm(head,ns,global,fmt,nsize,ndim,dim,error,extend,  &
      gcount,pcount,rndmsize,data,skipsz, nbits)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Read a Random Group
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: head              ! Header name
      integer(kind=4),            intent(in)    :: ns                ! Length of name
      logical,                    intent(in)    :: global            ! Global / Local status
      integer(kind=4),            intent(in)    :: fmt               ! Format of data
      integer(kind=size_length),  intent(inout) :: nsize             ! Size in words of data
      integer(kind=4),            intent(inout) :: ndim              ! Number of dimensions
      integer(kind=index_length), intent(inout) :: dim(sic_maxdims)  ! Dimensions
      logical,                    intent(inout) :: error             ! Error flag
      logical,                    intent(out)   :: extend            ! Are there extensions ?
      integer(kind=4),            intent(inout)    :: gcount            ! GCOUNT value
      integer(kind=4),            intent(inout)    :: pcount            ! PCOUNT value
      integer(kind=4),            intent(inout)    :: rndmsize          ! Random group size
      logical,                    intent(in)    :: data              ! Read data ?
      integer(kind=size_length),  intent(inout)    :: skipsz            ! Size in Bytes in FITS file
      integer(kind=4),            intent(in)    :: nbits             ! Number of Bits in data
    end subroutine fits_read_rndm
  end interface
  !
  interface
    subroutine sgetreal(ndat,data,scale,zero,error,fmt)
      use gildas_def
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Read NDAT data values in FITS data area,
      !   with scaling SCALE and offset ZERO
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)    :: ndat        ! Number of data
      real(kind=4),              intent(out)   :: data(ndat)  ! Data array
      real(kind=4),              intent(in)    :: scale       ! Scaling factor
      real(kind=4),              intent(in)    :: zero        ! Offset
      logical,                   intent(inout) :: error       ! Error flag
      integer(kind=4),           intent(in)    :: fmt         ! Data format
    end subroutine sgetreal
  end interface
  !
  interface
    subroutine do_hierarch(s,n,val,global)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Support for command
      !   SIC\DEFINE FITS
      ! Support for HIERARCH keywords (ESO certainly, most possibly all
      ! others). Note that due to the nature of the SIC variables, blanks in
      ! the keyword are translated to underscores.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: s       ! Header name
      integer(kind=4),  intent(in)    :: n       ! Length of name
      character(len=*), intent(inout) :: val     ! HIERARCH value
      logical,          intent(in)    :: global  ! Global flag
    end subroutine do_hierarch
  end interface
  !
  interface
    subroutine sic_blanktostruct(a,n)
      !---------------------------------------------------------------------
      ! @ private
      !   Replace spaces by % sign
      !---------------------------------------------------------------------
      character(len=*) :: a             !
      integer :: n                      !
    end subroutine sic_blanktostruct
  end interface
  !
  interface
    subroutine sic_makestructhier(namein,global,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Create a SIC structure from a HIERARCH keyword
      !   Creates all intermediate structures if needed
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: namein        ! Structure name
      logical, intent(in)  :: global                 ! Global status
      logical, intent(out)  :: error                 ! Error flag
    end subroutine sic_makestructhier
  end interface
  !
  interface
    subroutine fits_sicvariable(name,chain,global)
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Create a Sic variable from a FITS value string
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: name    ! Variable name
      character(len=*), intent(in) :: chain   ! Value to be decoded
      logical,          intent(in) :: global  ! Logical error flag
    end subroutine fits_sicvariable
  end interface
  !
  interface
    subroutine fits_sicstr(str,lstr,name,global)
      use gildas_def
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Fill a SIC character variable from a FITS keyword value
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: str
      integer, intent(in) :: lstr
      character(len=*), intent(in) :: name
      logical, intent(in) :: global
    end subroutine fits_sicstr
  end interface
  !
  interface
    subroutine fits_trim(ndim,dim,therank)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! SIC\DEFINE FITS command
      !   Support for option /TRIM
      !   Truncate degenerate dimensions
      !---------------------------------------------------------------------
      integer(kind=4), intent(inout) :: ndim
      integer(kind=index_length), intent(in) :: dim(sic_maxdims)
      integer(kind=4), intent(in) :: therank
    end subroutine fits_trim
  end interface
  !
  interface
    subroutine sicset_message(line,error)
      use gbl_message
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      !    Support routine for command SIC\SIC MESSAGE. Should only be
      ! called by SICSET subroutine, which supports the SIC\SIC command.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine sicset_message
  end interface
  !
  interface
    subroutine sicset_message_rules(line,error)
      use gbl_message
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      ! Syntax may be:
      !    SIC MESSAGE
      !       (nothing done, just print active filters)
      !    SIC MESSAGE PackName [ [S|L|A][-|=|+]F|E|W|R|I|D|T ] [...]
      !       (update and print considered package(s) filters)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine sicset_message_rules
  end interface
  !
  interface
    subroutine sicset_message_colors(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Syntax may be:
      !    SIC MESSAGE /COLOR
      !       (nothing done, just print active colors)
      !    SIC MESSAGE /COLOR ON|OFF
      !       (turn on/off the message coloring)
      !    SIC MESSAGE /COLOR E=RED W=ORANGE ...
      !       (update relevant message colors)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine sicset_message_colors
  end interface
  !
  interface
    subroutine sic_message_command(line,error)
      use gbl_message
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      !    Support routine for command SIC\MESSAGE. Syntax is:
      !    MESSAGE F|E|W|R|I|D|T  ProcName  "Message"
      !    MESSAGE F|E|W|R|I|D|T  ProcName  Arg1 Arg2 ... ArgN
      ! 1  [/FORMAT <fmt1> <fmt2> ... <fmtN>]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      logical,          intent(out) :: error  ! Error flag
    end subroutine sic_message_command
  end interface
  !
  interface
    subroutine sic_open_log(logf,error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Open the LOG file
      !---------------------------------------------------------------------
      character(len=*) :: logf       ! The log file name
      logical :: error               ! Logical error flag
    end subroutine sic_open_log
  end interface
  !
  interface
    subroutine sic_flush_log(error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Flush the LOG file buffer onto disk
      ! Arguments :
      !       ERROR   L       Error returned
      !---------------------------------------------------------------------
      logical :: error                  ! INTENT(INOUT)
    end subroutine sic_flush_log
  end interface
  !
  interface
    subroutine sic_close_log(error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Close the LOG file and free its unit
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_close_log
  end interface
  !
  interface
    subroutine make_gag_magic(gag_logical)
      use gildas_def
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Get a Gildas logical name and:
      !  1) append a magic (unique) number
      !  2) create the resulting directory
      !  3) update the value in the dictionnary
      ! Any error raised here is FATAL.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: gag_logical
    end subroutine make_gag_magic
  end interface
  !
  interface
    subroutine make_gag(logdir)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Creates input directory if it does not exist. Input must be a
      !       symbolic name.
      ! Arguments :
      !       LOGDIR     C*(*)       Symbolic name      Input
      !---------------------------------------------------------------------
      character(len=*) :: logdir        !
    end subroutine make_gag
  end interface
  !
  interface
    subroutine sic_build_environment
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Set the log file
      !---------------------------------------------------------------------
    end subroutine sic_build_environment
  end interface
  !
  interface
    subroutine sic_variables_system(error)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Define system-related variables in the SIC% structure
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_variables_system
  end interface
  !
  interface
    subroutine sic_variables_openmp(error)
      !$ use omp_lib
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Define OpenMP-related variables in the SIC% structure
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_variables_openmp
  end interface
  !
  interface
    subroutine sic_variables_python(error)
      use sic_interactions
      use sic_python
      !---------------------------------------------------------------------
      ! @ private
      ! Define Gildas-Python related variables in the SIC% structure
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_variables_python
  end interface
  !
  interface
    subroutine sic_variables_codes(parent,error)
      use gbl_constant
      !---------------------------------------------------------------------
      ! @ private
      !  Define SIC variables providing various codes
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: parent  ! Parent structure name
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_variables_codes
  end interface
  !
  interface
    subroutine sic_ansi_termcodes(error)
      use gbl_ansicodes
      !---------------------------------------------------------------------
      ! @ private
      ! Define and populate the ANSI% structure
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine sic_ansi_termcodes
  end interface
  !
  interface
    subroutine sic_pack_init(gpack_id,error)
      use sic_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      integer, intent(in) :: gpack_id  !
      logical, intent(inout) :: error  ! Error flag
    end subroutine sic_pack_init
  end interface
  !
  interface
    subroutine sic_pack_exec_on_child(building_pack,error)
      use gpack_def
      use sic_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Load package and user procedure initializations.
      !---------------------------------------------------------------------
      type(gpack_info_t), intent(inout) :: building_pack  ! Package properties
      logical,            intent(inout) :: error          ! Error flag
    end subroutine sic_pack_exec_on_child
  end interface
  !
  interface
    subroutine sic_pack_clean(error)
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error flag
    end subroutine sic_pack_clean
  end interface
  !
  interface
    subroutine gmaster_set_ismaster(ismaster)
      use sic_python
      !---------------------------------------------------------------------
      ! @ private
      ! Python binding private routine.
      ! Set the Gildas programs master status
      !---------------------------------------------------------------------
      logical, intent(in) :: ismaster  !
    end subroutine gmaster_set_ismaster
  end interface
  !
  interface
    function gmaster_get_ismaster()
      use sic_python
      !---------------------------------------------------------------------
      ! @ private
      ! Python binding private routine.
      ! Get the Gildas programs master status
      !---------------------------------------------------------------------
      integer :: gmaster_get_ismaster  ! Function value on return
    end function gmaster_get_ismaster
  end interface
  !
#if defined(GAG_USE_PYTHON)
  interface
    subroutine gmaster_build_sic(flag)
      !---------------------------------------------------------------------
      ! @private
      ! Python binding private routine.
      ! Initialize Gildas with SIC as slave
      !---------------------------------------------------------------------
      integer :: flag           ! Error flag
    end subroutine gmaster_build_sic
  end interface
  !
  interface
    function sic_verify()
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Return the SIC%VERIFY status
      !---------------------------------------------------------------------
      logical :: sic_verify  ! Function value on return
    end function sic_verify
  end interface
  !
  interface
    subroutine sic_run_python(line,reentrant,error)
      use gildas_def
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !   SIC\PYTHON
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Input command line
      logical,          intent(in)    :: reentrant  ! Allow (re)entrant call to Python prompt
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_run_python
  end interface
  !
#else
  interface
    subroutine sic_run_python(line,reentrant,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Input command line
      logical,          intent(in)    :: reentrant  ! Allow (re)entrant call to Python prompt
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_run_python
  end interface
  !
#endif
  interface
    subroutine sicsay (line,nline,error)
      use gildas_def
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command SAY
      !   SAY 'Expression' "String" 'Character_variable' ...
      ! 1 [/FORMAT  a10 i2 f5.2 ...]
      ! 2 [/NOADVANCE]
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      integer                         :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine sicsay
  end interface
  !
  interface
    subroutine sicmsg(line)
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
    end subroutine sicmsg
  end interface
  !
  interface
    subroutine say_format_arg(line,iarg,iform,outlun,fmtd_string,lstring,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @private
      ! From input line, format the iarg-th argument of the command
      ! according to the iform-th format of the option /FORMAT. Formatted
      ! string is returned in fmtd_string, and its usefull length is lstring.
      ! Warnings!
      !  1) /FORMAT option must be the 1st option of the calling command
      !  2) In return, fmtd_string(1:lstring) != fmtd_string
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line         ! Line to analyze
      integer,          intent(in)    :: iarg         ! The arg number to analyze
      integer,          intent(in)    :: iform        ! The format number to be used
      integer,          intent(in)    :: outlun       ! Logical unit to write to (if > 0)
      character(len=*), intent(out)   :: fmtd_string  ! Output string (if outlun == 0)
      integer,          intent(out)   :: lstring      ! Output string usefull length
      logical,          intent(inout) :: error        ! Error status
    end subroutine say_format_arg
  end interface
  !
  interface
    subroutine say_getformat(line,iform,forma,lforma,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Retrieve the format from the command line, and format it to make
      ! it applicable on arrays.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: iform
      character(len=*), intent(out)   :: forma
      integer(kind=4),  intent(out)   :: lforma
      logical,          intent(inout) :: error
    end subroutine say_getformat
  end interface
  !
  interface
    subroutine say_incarnate_arg(line,iarg,forma,inca,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Fetch an incarnation of the argument, according to the type
      ! requested by the format
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: line
      integer(kind=4),        intent(in)    :: iarg
      character(len=*),       intent(in)    :: forma
      type(sic_descriptor_t), intent(out)   :: inca
      logical,                intent(inout) :: error
    end subroutine say_incarnate_arg
  end interface
  !
  interface
    subroutine say_array_ch(addr,nc,na,form,lun,string,advance,error)
      use gildas_def
      use iso_fortran_env
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=address_length), intent(in)    :: addr     ! Address of first element
      integer(kind=4),              intent(in)    :: nc       ! Full length of 1 element
      integer(kind=size_length),    intent(in)    :: na       ! Number of elements in array
      character(len=*),             intent(in)    :: form     ! Format to be used
      integer,                      intent(in)    :: lun      ! Send output to this logical unit
      character(len=*),             intent(inout) :: string   ! Output string (used if lun=0)
      logical,                      intent(in)    :: advance  ! Advancing or not ?
      logical,                      intent(inout) :: error    ! Error flag
    end subroutine say_array_ch
  end interface
  !
  interface
    subroutine sicset(line,lire,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use gsys_types
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  SIC Internal routine
      ! Modifies the current status of SIC monitor:
      !     MEMORY       Switch
      !     VERIFY       Switch
      !     EDIT         Status or Editor
      !     HELP         Status
      !     PRECISION    Default mathematic precision
      !     DIRECTORY    Working directory
      !     EXTENSION    Default macro extension
      !     OUTPUT       Output file name
      !     DATE         Current date
      !     USER         Current user
      !     SEARCH       Search for file existence
      !     LOGICAL      Logical name translation
      !     BEEP n       Beep n times
      !     SYNTAX       Select Free/Fixed syntax for assignment
      !     VERSION      Give the program version...
      !     WAIT         Wait n seconds
      !     WINDOW       Activate / de-activate the Window mode
      !     MACRO        Procedure search path
      !     APPEND       Append Small_file Appended_file
      !     SAVE         File Command : Save all variable in File with Command
      !     CPU          Compute CPU time (for debugging purpose)
      !     FIND         Find list of files
      !     GREP         Grep a pattern from a file
      !     EXPAND       Expand command in a macro file
      !     PARSE File Name Ext Dir
      !     MESSAGE <Filters>  Alter on-screen and to-logfile verbosity
      !     FLUSH        Force flushing message and log files buffers onto disk
      !     WHICH Proc   Print full path of the Procedure which will be executed
      !     LOCK <FileName>  Create the lock-file, or exit if it exists
      !     HEADERS      GILDAS Header version
      !     INTEGER      Default SIC INTEGER kind (short or long)
      !     DELAY        Delay execution of next command
      !     SYSTEM       Switch: error from command SIC\SYSTEM should be ignored or not?
      !     TIMER        Set/Get the timer for automatic logout after inactivity
      !     MODIFIED     Set/get the modification status of a file
      !     RANDOM_SEED  (Re)set the Fortran random seed generator
      !     PARALLEL     Set the number of Open-MP threads
      !     <noarg>      Language in active scope
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Command line
      integer(kind=4),  intent(in)    :: lire   ! Execution level
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sicset
  end interface
  !
#if defined(CFITSIO)
  interface
    subroutine sicapropos_cfitsio_vers(version)
      use cfitsio_api
      !---------------------------------------------------------------------
      ! @ private
      ! Return the CFITSIO version.
      !---------------------------------------------------------------------
      real(kind=4), intent(out) :: version
    end subroutine sicapropos_cfitsio_vers
  end interface
  !
#endif
  interface
    subroutine sic_directory(line,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   SIC\SIC FIND [Filter [Directory]]
      ! Define  DIR%NFILE and DIR%FILE variables.
      !-----------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_directory
  end interface
  !
  interface
    subroutine sic_error_command
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! List the current ON ERROR Command
      !---------------------------------------------------------------------
    end subroutine sic_error_command
  end interface
  !
  interface
    subroutine sic_switch(line,action,switch,lire,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! List and/or Toggle an internal SIC switch
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line    !
      character(len=*), intent(in)    :: action  !
      logical,          intent(inout) :: switch  !
      integer(kind=4),  intent(in)    :: lire    !
      logical,          intent(inout) :: error   !
    end subroutine sic_switch
  end interface
  !
  interface
    subroutine parse_priority(line,error)
      use sic_interactions
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine parse_priority
  end interface
  !
  interface
    subroutine parse_priority_recompute(error)
      use gbl_message
      use eclass_types
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !   Recompute the priority structures according to the (new)
      ! priorities in 'languages(:)%prio'
      !   Sorting rules (example):
      !    Prio   Level
      !     1       1
      !     2       2
      !     3       3
      !     0       4  (automatic = after the explicit positive priorities
      !                         and before the negative ones)
      !    -2       5
      !    -1       6  (negative = position from the end)
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine parse_priority_recompute
  end interface
  !
  interface
    subroutine parse_filename (line,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC PARSE Filename Name [Ext [Dir]]
      ! Parse an input filename, and return the short Name, and optionally
      ! the Extension and Directory as SIC variables. The name of the
      ! corresponding variables are given in the command line.
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine parse_filename
  end interface
  !
  interface
    subroutine get_achar_desc(line,iopt,iarg,desc,error)
      use gildas_def
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Get the descriptor of an (existing, writeable) character string
      ! whose name is the Iarg argument of option Iopt of the command line
      !---------------------------------------------------------------------
      character(len=*)                      :: line   !
      integer                               :: iopt   !
      integer                               :: iarg   !
      type(sic_descriptor_t)                :: desc   !
      logical,                intent(inout) :: error  !
    end subroutine get_achar_desc
  end interface
  !
  interface
    subroutine sic_debug_gfortran(error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Try to reproduce the bug observed with gfortran when writing
      ! Gildas binary files.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_debug_gfortran
  end interface
  !
  interface
    subroutine sic_debug_memalign
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Check how:
      !   1) Fortran allocatables,
      !   2) malloc-ed buffers,
      !   3) and fftw_malloc-ed buffers
      !  are aligned in memory
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_debug_memalign
  end interface
  !
  interface
    subroutine sic_cpu(line,error)
      use gbl_message
      use gsys_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  SIC\SIC CPU [VERBOSE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_cpu
  end interface
  !
  interface
    subroutine sic_delay(line,error)
      use gbl_message
      use gsys_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for commande SIC\SIC DELAY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_delay
  end interface
  !
  interface
    subroutine sic_modified(line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use gsys_types
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   SIC MODIFIED FileName StructName
      ! Check if File has been modified or not
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_modified
  end interface
  !
  interface
    subroutine sic_output(line,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC OUTPUT [FileName [NEW|APPEND]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_output
  end interface
  !
  interface
    subroutine sic_output_close(error)
      use gildas_def
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    SIC OUTPUT (no arg)
      !  Close the SIC OUTPUT file, if any
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_output_close
  end interface
  !
  interface
    subroutine sic_openmp(line,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_openmp
  end interface
  !
  interface
    subroutine sic_get_openmp
      use gbl_message
      use sic_interactions
      !$  use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_get_openmp
  end interface
  !
  interface
    subroutine sic_set_openmp(line,error)
      use gildas_def
      use gbl_message
      use sic_interactions
      !$  use omp_lib
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_set_openmp
  end interface
  !
  interface
    subroutine sic_setverify(line,lire,error)
      use gbl_message
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! List and/or Toggle the VERIFY switches
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line    !
      integer,          intent(in)    :: lire    !
      logical,          intent(inout) :: error   !
    end subroutine sic_setverify
  end interface
  !
  interface
    subroutine sic_if(case)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(in) :: case  !
    end subroutine sic_if
  end interface
  !
  interface
    subroutine sic_run_comm(command,line,nline,ocode,recall,nolog,error)
      use gbl_message
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! Execute a basic SIC\ command. A "basic" SIC command is one of these
      ! SIC\ commands which is deeply bound with the SIC interpreter, i.e.
      ! which can not be executed out of it.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: command  ! Command
      character(len=*), intent(inout) :: line     ! Command line
      integer(kind=4),  intent(inout) :: nline    ! Length of command line
      integer(kind=4),  intent(out)   :: ocode    ! Output code
      integer(kind=4),  intent(out)   :: recall   ! Execute a new command?
      logical,          intent(out)   :: nolog    ! Log it or not on return?
      logical,          intent(inout) :: error    ! Logical error flag (intent(out)?)
    end subroutine sic_run_comm
  end interface
  !
  interface
    subroutine sic_run_exit(line,ocode,error)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Perform proper actions for the EXIT command
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(inout) :: ocode
      logical,          intent(inout) :: error
    end subroutine sic_run_exit
  end interface
  !
  interface
    subroutine sic_proced(line,nline,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Compilation mode for BEGIN PROC/END PROC blocks
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Input command line
      integer,          intent(inout) :: nline  ! Line length
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_proced
  end interface
  !
  interface
    subroutine sic_compil(line,nline,goto_code,error)
      use sic_dictionaries
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Compilation mode for FOR/NEXT blocks
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line       ! Input command line
      integer,          intent(inout) :: nline      ! Line length
      integer,          intent(out)   :: goto_code  ! Where to go on return?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_compil
  end interface
  !
  interface
    subroutine sic_ifblock(line,nline,command,goto_code,error)
      use gbl_message
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      ! IF block exploration
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Command line
      integer(kind=4),  intent(in)    :: nline      ! Length of command line
      character(len=*), intent(in)    :: command    !
      integer(kind=4),  intent(out)   :: goto_code  !
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_ifblock
  end interface
  !
  interface
    subroutine sic_ifblock_elseend(line,nline,command,dolog,error)
      use gbl_message
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !  Parsing of END or ELSE [IF] in an IF block
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: nline
      character(len=*), intent(in)    :: command
      logical,          intent(out)   :: dolog
      logical,          intent(inout) :: error
    end subroutine sic_ifblock_elseend
  end interface
  !
  interface
    subroutine reset_if(error)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(out) :: error  !
    end subroutine reset_if
  end interface
  !
  interface
    subroutine sic_ifconstruct(rname,iline,iarg,allowed,ifconstruct,error)
      use gildas_def
      use gbl_message
      use sic_structures
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Detect if command line "[ELSE] IF (Logi) ..." is an IF statement
      ! or an IF construct
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname        ! Calling routine name
      character(len=*), intent(in)    :: iline        ! Input command line
      integer(kind=4),  intent(in)    :: iarg         !
      logical,          intent(in)    :: allowed      ! IF statement allowed or not?
      logical,          intent(out)   :: ifconstruct  ! Is an IF construct or not?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_ifconstruct
  end interface
  !
  interface
    subroutine setlet(line,nline,freelet,error)
      use sic_interactions
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Check if input line is a free syntax assignment (e.g "A = 1"). If
      ! yes, add a "SIC\LET" command at the beginning of the line.
      ! If not in execution mode (i.e. COMPIL and PROCED), we cannot check
      ! if variable really exists.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line     ! Modified on return
      integer,          intent(inout) :: nline    !
      logical,          intent(inout) :: freelet  ! Can possibly be a free syntax assignment?
      logical,          intent(inout) :: error    !
    end subroutine setlet
  end interface
  !
  interface
    subroutine sic_dcl(line,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Support routine for command
      !       SYSTEM ["commands"]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_dcl
  end interface
  !
  interface
    subroutine simann(n,x,max,rt,eps,ns,nt,neps,maxevl,lb,ub,c,iprint,iseed1,  &
      iseed2,t,vm,xopt,fopt,nacc,nfcnev,nobds,ier,fstar,xp,nacp, fcn)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Version: 3.2
      !  Date: 1/22/94.
      !  Differences compared to Version 2.0:
      !     1. If a trial is out of bounds, a point is randomly selected
      !        from LB(i) to UB(i). Unlike in version 2.0, this trial is
      !        evaluated and is counted in acceptances and rejections.
      !        All corresponding documentation was changed as well.
      !  Differences compared to Version 3.0:
      !     1. If VM(i) > (UB(i) - LB(i)), VM is set to UB(i) - LB(i).
      !        The idea is that if T is high relative to LB & UB, most
      !        points will be accepted, causing VM to rise. But, in this
      !        situation, VM has little meaning; particularly if VM is
      !        larger than the acceptable region. Setting VM to this size
      !        still allows all parts of the allowable region to be selected.
      !  Differences compared to Version 3.1:
      !     1. Test made to see if the initial temperature is positive.
      !     2. WRITE statements prettied up.
      !     3. References to paper updated.
      !
      !  Synopsis:
      !  This routine implements the continuous simulated annealing global
      !  optimization algorithm described in Corana et al article
      !       Minimizing Multimodal Functions of Continuous Variables 
      !       with the Simulated Annealing Algorithm
      !  in the September 1987 (vol. 13, no. 3, pp. 262-280) issue of 
      !  the ACM Transactions on Mathematical Software.
      !
      !  A very quick (perhaps too quick) overview of SA:
      !     SA tries to find the global optimum of an N dimensional function.
      !  It moves both up and downhill and as the optimization process
      !  proceeds, it focuses on the most promising area.
      !     To start, it randomly chooses a trial point within the step length
      !  VM (a vector of length N) of the user selected starting point. The
      !  function is evaluated at this trial point and its value is compared
      !  to its value at the initial point.
      !     In a maximization problem, all uphill moves are accepted and the
      !  algorithm continues from that trial point. Downhill moves may be
      !  accepted; the decision is made by the Metropolis criteria. It uses T
      !  (temperature) and the size of the downhill move in a probabilistic
      !  manner. The smaller T and the size of the downhill move are, the more
      !  likely that move will be accepted. If the trial is accepted, the
      !  algorithm moves on from that point. If it is rejected, another point
      !  is chosen instead for a trial evaluation.
      !     Each element of VM periodically adjusted so that half of all
      !  function evaluations in that direction are accepted.
      !     A fall in T is imposed upon the system with the RT variable by
      !  T(i+1) = RT*T(i) where i is the ith iteration. Thus, as T declines,
      !  downhill moves are less likely to be accepted and the percentage of
      !  rejections rise. Given the scheme for the selection for VM, VM falls.
      !  Thus, as T declines, VM falls and SA focuses upon the most promising
      !  area for optimization.
      !
      !  The importance of the parameter T:
      !     The parameter T is crucial in using SA successfully. It influences
      !  VM, the step length over which the algorithm searches for optima. For
      !  a small intial T, the step length may be too small; thus not enough
      !  of the function might be evaluated to find the global optima. The user
      !  should carefully examine VM in the intermediate output (set IPRINT =
      !  1) to make sure that VM is appropriate. The relationship between the
      !  initial temperature and the resulting step length is function
      !  dependent.
      !     To determine the starting temperature that is consistent with
      !  optimizing a function, it is worthwhile to run a trial run first. Set
      !  RT = 1.5 and T = 1.0. With RT > 1.0, the temperature increases and VM
      !  rises as well. Then select the T that produces a large enough VM.
      !
      !  For modifications to the algorithm and many details on its use,
      !  (particularly for econometric applications) see Goffe, Ferrier
      !  and Rogers, 
      !    Global Optimization of Statistical Functions with Simulated Annealing
      !  Journal of Econometrics, vol. 60, no. 1/2, Jan./Feb. 1994, pp. 65-100.
      !  For more information, contact
      !              Bill Goffe
      !              Department of Economics and International Business
      !              University of Southern Mississippi
      !              Hattiesburg, MS  39506-5072
      !              (601) 266-4484 (office)
      !              (601) 266-4920 (fax)
      !              bgoffe@whale.st.usm.edu (Internet)
      !
      !  As far as possible, the parameters here have the same name as in
      !  the description of the algorithm on pp. 266-8 of Corana et al.
      !
      !  In this description, SP is single precision, DP is real(kind=8) ,
      !  INT is integer, L is logical and (N) denotes an array of length n.
      !  Thus, DP(N) denotes a real(kind=8)  array of length n.
      !
      !  Input Parameters:
      !    Note: The suggested values generally come from Corana et al. To
      !          drastically reduce runtime, see Goffe et al., pp. 90-1 for
      !          suggestions on choosing the appropriate RT and NT.
      !    N - Number of variables in the function to be optimized. (INT)
      !    X - The starting values for the variables of the function to be
      !        optimized. (DP(N))
      !    MAX - Denotes whether the function should be maximized or
      !          minimized. A true value denotes maximization while a false
      !          value denotes minimization. Intermediate output (see IPRINT)
      !          takes this into account. (L)
      !    RT - The temperature reduction factor. The value suggested by
      !         Corana et al. is .85. See Goffe et al. for more advice. (DP)
      !    EPS - Error tolerance for termination. If the final function
      !          values from the last neps temperatures differ from the
      !          corresponding value at the current temperature by less than
      !          EPS and the final function value at the current temperature
      !          differs from the current optimal function value by less than
      !          EPS, execution terminates and IER = 0 is returned. (EP)
      !    NS - Number of cycles. After NS*N function evaluations, each
      !         element of VM is adjusted so that approximately half of
      !         all function evaluations are accepted. The suggested value
      !         is 20. (INT)
      !    NT - Number of iterations before temperature reduction. After
      !         NT*NS*N function evaluations, temperature (T) is changed
      !         by the factor RT. Value suggested by Corana et al. is
      !         MAX(100, 5*N). See Goffe et al. for further advice. (INT)
      !    NEPS - Number of final function values used to decide upon termi-
      !           nation. See EPS. Suggested value is 4. (INT)
      !    MAXEVL - The maximum number of function evaluations. If it is
      !             exceeded, IER = 1. (INT)
      !    LB - The lower bound for the allowable solution variables. (DP(N))
      !    UB - The upper bound for the allowable solution variables. (DP(N))
      !         If the algorithm chooses X(I) .LT. LB(I) or X(I) .GT. UB(I),
      !         I = 1, N, a point is from inside is randomly selected. This
      !         This focuses the algorithm on the region inside UB and LB.
      !         Unless the user wishes to concentrate the search to a par-
      !         ticular region, UB and LB should be set to very large positive
      !         and negative values, respectively. Note that the starting
      !         vector X should be inside this region. Also note that LB and
      !         UB are fixed in position, while VM is centered on the last
      !         accepted trial set of variables that optimizes the function.
      !    C - Vector that controls the step length adjustment. The suggested
      !        value for all elements is 2.0. (DP(N))
      !    IPRINT - controls printing inside SA. (INT)
      !             Values: 0 - Nothing printed.
      !                     1 - Function value for the starting value and
      !                         summary results before each temperature
      !                         reduction. This includes the optimal
      !                         function value found so far, the total
      !                         number of moves (broken up into uphill,
      !                         downhill, accepted and rejected), the
      !                         number of out of bounds trials, the
      !                         number of new optima found at this
      !                         temperature, the current optimal X and
      !                         the step length VM. Note that there are
      !                         N*NS*NT function evalutations before each
      !                         temperature reduction. Finally, notice is
      !                         is also given upon achieveing the termination
      !                         criteria.
      !                     2 - Each new step length (VM), the current optimal
      !                         X (XOPT) and the current trial X (X). This
      !                         gives the user some idea about how far X
      !                         strays from XOPT as well as how VM is adapting
      !                         to the function.
      !                     3 - Each function evaluation, its acceptance or
      !                         rejection and new optima. For many problems,
      !                         this option will likely require a small tree
      !                         if hard copy is used. This option is best
      !                         used to learn about the algorithm. A small
      !                         value for MAXEVL is thus recommended when
      !                         using IPRINT = 3.
      !             Suggested value: 1
      !             Note: For a given value of IPRINT, the lower valued
      !                   options (other than 0) are utilized.
      !    ISEED1 - The first seed for the random number generator RANMAR.
      !             0 .LE. ISEED1 .LE. 31328. (INT)
      !    ISEED2 - The second seed for the random number generator RANMAR.
      !             0 .LE. ISEED2 .LE. 30081. Different values for ISEED1
      !             and ISEED2 will lead to an entirely different sequence
      !             of trial points and decisions on downhill moves (when
      !             maximizing). See Goffe et al. on how this can be used
      !             to test the results of SA. (INT)
      !
      !  Input/Output Parameters:
      !    T - On input, the initial temperature. See Goffe et al. for advice.
      !        On output, the final temperature. (DP)
      !    VM - The step length vector. On input it should encompass the
      !         region of interest given the starting value X. For point
      !         X(I), the next trial point is selected is from X(I) - VM(I)
      !         to  X(I) + VM(I). Since VM is adjusted so that about half
      !         of all points are accepted, the input value is not very
      !         important (i.e. is the value is off, SA adjusts VM to the
      !         correct value). (DP(N))
      !
      !  Output Parameters:
      !    XOPT - The variables that optimize the function. (DP(N))
      !    FOPT - The optimal value of the function. (DP)
      !    NACC - The number of accepted function evaluations. (INT)
      !    NFCNEV - The total number of function evaluations. In a minor
      !             point, note that the first evaluation is not used in the
      !             core of the algorithm; it simply initializes the
      !             algorithm. (INT).
      !    NOBDS - The total number of trial function evaluations that
      !            would have been out of bounds of LB and UB. Note that
      !            a trial point is randomly selected between LB and UB.
      !            (INT)
      !    IER - The error return number. (INT)
      !          Values: 0 - Normal return; termination criteria achieved.
      !                  1 - Number of function evaluations (NFCNEV) is
      !                      greater than the maximum number (MAXEVL).
      !                  2 - The starting value (X) is not inside the
      !                      bounds (LB and UB).
      !                  3 - The initial temperature is not positive.
      !                  99 - Should not be seen; only used internally.
      !
      !  Work arrays that must be dimensioned in the calling routine:
      !       RWK1 (DP(NEPS))  (FSTAR in SA)
      !       RWK2 (DP(N))     (XP    "  " )
      !       IWK  (INT(N))    (NACP  "  " )
      !
      !  Required Functions (included):
      !    EXPREP - Replaces the function EXP to avoid under- and overflows.
      !             It may have to be modified for non IBM-type main-
      !             frames. (DP)
      !    RMARIN - Initializes the random number generator RANMAR.
      !    RANMAR - The actual random number generator. Note that
      !             RMARIN must run first (SA does this). It produces uniform
      !             random numbers on [0,1]. These routines are from
      !             Usenet comp.lang.fortran. For a reference, see
      !             "Toward a Universal Random Number Generator"
      !             by George Marsaglia and Arif Zaman, Florida State
      !             University Report: FSU-SCRI-87-50 (1987).
      !             It was later modified by F. James and published in
      !             "A Review of Pseudo-random Number Generators." For
      !             further information, contact stuart@ads.com. These
      !             routines are designed to be portable on any machine
      !             with a 24-bit or more mantissa. I have found it produces
      !             identical results on a IBM 3081 and a Cray Y-MP.
      !
      !  Required Subroutines (included):
      !    SIMAN_PRTVEC - Prints vectors.
      !    SIMAN_PRT1 ... SIMAN_PRT10 - Prints intermediate output.
      !    FCN - Function to be optimized. The form is
      !            SUBROUTINE FCN(N,X,F)
      !            INTEGER N
      !            real(kind=8)   X(N), F
      !            ...
      !            function code with F = F(X)
      !            ...
      !            RETURN
      !            END
      !          Note: This is the same form used in the multivariable
      !          minimization algorithms in the IMSL edition 10 library.
      !
      !  Machine Specific Features:
      !    1. EXPREP may have to be modified if used on non-IBM type main-
      !       frames. Watch for under- and overflows in EXPREP.
      !    2. Some FORMAT statements use G25.18; this may be excessive for
      !       some machines.
      !    3. RMARIN and RANMAR are designed to be protable; they should not
      !       cause any problems.
      !  Type all external variables.
      !---------------------------------------------------------------------
      integer ::  n                     !
      real(kind=8) ::   x(*)            !
      logical ::  max                   !
      real(kind=8) ::   rt              !
      real(kind=8) ::   eps             !
      integer ::  ns                    !
      integer ::  nt                    !
      integer ::  neps                  !
      integer ::  maxevl                !
      real(kind=8) ::   lb(*)           !
      real(kind=8) ::   ub(*)           !
      real(kind=8) ::   c(*)            !
      integer ::  iprint                !
      integer ::  iseed1                !
      integer ::  iseed2                !
      real(kind=8) ::   t               !
      real(kind=8) ::   vm(*)           !
      real(kind=8) ::   xopt(*)         !
      real(kind=8) ::   fopt            !
      integer ::  nacc                  !
      integer ::  nfcnev                !
      integer ::  nobds                 !
      integer ::  ier                   !
      real(kind=8) ::   fstar(*)        !
      real(kind=8) ::   xp(*)           !
      integer ::  nacp(*)               !
      external :: fcn                   !
    end subroutine simann
  end interface
  !
  interface
    function exprep(rdum)
      !---------------------------------------------------------------------
      ! @ private
      !  This function replaces exp to avoid under- and overflows and is
      !  designed for IBM 370 type machines. It may be necessary to modify
      !  it for other machines. Note that the maximum and minimum values of
      !  EXPREP are such that they has no effect on the algorithm.
      !---------------------------------------------------------------------
      real(kind=8) :: exprep  ! Function value on return
      real(kind=8), intent(in) :: rdum  !
    end function exprep
  end interface
  !
  interface
    subroutine rmarin(ij,kl)
      !---------------------------------------------------------------------
      ! @ private
      !  This subroutine and the next function generate random numbers. See
      !  the comments for SA for more information. The only changes from the
      !  orginal code is that (1) the test to make sure that RMARIN runs first
      !  was taken out since SA assures that this is done (this test did not
      !  compile under IBM VS Fortran) and (2) typing ivec as integer was
      !  taken out since ivec is not used. With these exceptions, all following
      !  lines are original.
      ! This is the initialization routine for the random number generator
      !     RANMAR()
      ! NOTE: The seed variables can have values between:    0 <= IJ <= 31328
      !                                                      0 <= KL <= 30081
      !---------------------------------------------------------------------
      integer :: ij                     !
      integer :: kl                     !
    end subroutine rmarin
  end interface
  !
  interface
    function ranmar()
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: ranmar                    !
    end function ranmar
  end interface
  !
  interface
    subroutine siman_prt1
      !---------------------------------------------------------------------
      ! @ private
      !  This subroutine prints intermediate output, as does SIMAN_PRT2 through
      !  SIMAN_PRT10. Note that if SA is minimizing the function, the sign of the
      !  function value and the directions (up/down) are reversed in all
      !  output to correspond with the actual function optimization. This
      !  correction is because SA was written to maximize functions and
      !  it minimizes by maximizing the negative a function.
      !---------------------------------------------------------------------
    end subroutine siman_prt1
  end interface
  !
  interface
    subroutine siman_prt2(max,n,x,f)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
      integer ::  n                     !
      real(kind=8) ::   x(*)            !
      real(kind=8) ::   f               !
    end subroutine siman_prt2
  end interface
  !
  interface
    subroutine siman_prt3(max,n,xp,x,fp,f)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
      integer ::  n                     !
      real(kind=8) ::   xp(*)           !
      real(kind=8) ::   x(*)            !
      real(kind=8) ::   fp              !
      real(kind=8) ::   f               !
    end subroutine siman_prt3
  end interface
  !
  interface
    subroutine siman_prt4(max,n,xp,x,fp,f)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
      integer ::  n                     !
      real(kind=8) ::   xp(*)           !
      real(kind=8) ::   x(*)            !
      real(kind=8) ::   fp              !
      real(kind=8) ::   f               !
    end subroutine siman_prt4
  end interface
  !
  interface
    subroutine siman_prt6(max)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
    end subroutine siman_prt6
  end interface
  !
  interface
    subroutine siman_prt7(max)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
    end subroutine siman_prt7
  end interface
  !
  interface
    subroutine siman_prt8(n,vm,xopt,x)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer ::  n                     !
      real(kind=8) ::   vm(*)           !
      real(kind=8) ::   xopt(*)         !
      real(kind=8) ::   x(*)            !
    end subroutine siman_prt8
  end interface
  !
  interface
    subroutine siman_prt9(max,n,t,xopt,vm,fopt,nup,ndown,nrej,lnobds,nnew)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical ::  max                   !
      integer ::  n                     !
      real(kind=8) ::   t               !
      real(kind=8) ::   xopt(*)         !
      real(kind=8) ::   vm(*)           !
      real(kind=8) ::   fopt            !
      integer ::  nup                   !
      integer ::  ndown                 !
      integer ::  nrej                  !
      integer ::  lnobds                !
      integer ::  nnew                  !
    end subroutine siman_prt9
  end interface
  !
  interface
    subroutine siman_prtvec(vector,ncols,name)
      !---------------------------------------------------------------------
      ! @ private
      !  This subroutine prints the real(kind=8)  vector named VECTOR.
      !  Elements 1 thru NCOLS will be printed. NAME is a character variable
      !  that describes VECTOR. Note that if NAME is given in the call to
      !  SIMAN_PRTVEC, it must be enclosed in quotes. If there are more than 10
      !  elements in VECTOR, 10 elements will be printed on each line.
      !---------------------------------------------------------------------
      integer :: ncols                  !
      real(kind=8) ::  vector(ncols)    !
      character(len=*) :: name          !
    end subroutine siman_prtvec
  end interface
  !
  interface
    subroutine sicsort(line,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Support routine for command
      !  SORT KeyVar Var1 Var2 ... VarN
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sicsort
  end interface
  !
  interface
    subroutine init_stack
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Initialize the stack
      ! To be revised for general use
      !---------------------------------------------------------------------
      ! Local
    end subroutine init_stack
  end interface
  !
  interface
    subroutine edit_stack(error,edit)
      use gildas_def
      use sic_structures
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine : edit the stack using the default text
      !       editor specified by the user
      !---------------------------------------------------------------------
      logical :: error                  !
      logical :: edit                   !
    end subroutine edit_stack
  end interface
  !
  interface
    subroutine sic_recall (line,nline,iline,error)
      use gildas_def
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Recall a command line from the stack
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: line   ! Recalled command
      integer(kind=4),  intent(out)   :: nline  ! Length of line
      integer(kind=4),  intent(inout) :: iline  ! Line number
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_recall
  end interface
  !
  interface
    subroutine sic_type(line,tofile,goto_code,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  TYPE [FileIn] [/OUTPUT FileOut]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       ! Input command line
      logical,          intent(in)    :: tofile     ! Is /OUTPUT allowed?
      integer,          intent(out)   :: goto_code  !
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_type
  end interface
  !
  interface
    subroutine wstack(lun,line,nl,chain,nc)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Write a line of the stack to a file
      !       Avoid line overflows by using the continuation flag '-'
      ! Arguments :
      !       LUN     I       Logical unit number		Input
      !       LINE    C*(*)   Command line			Input
      !       NL      I       Length of line			Input
      !	CHAIN	C*(*)	String inserted after command	Input
      !	NC	I	Length of CHAIN			Input
      !---------------------------------------------------------------------
      integer :: lun                    !
      character(len=*) :: line          !
      integer :: nl                     !
      character(len=*) :: chain         !
      integer :: nc                     !
    end subroutine wstack
  end interface
  !
  interface
    subroutine type_stack(lun)
      use gildas_def
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !  List the stack onto the given logical unit (6 = STDOUT)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: lun  ! Output logical unit
    end subroutine type_stack
  end interface
  !
  interface
    subroutine recfin(line,nline,iline,error)
      use gildas_def
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Retrieve a command line from its first characters
      ! Arguments
      !       LINE    C*(*)   Abbreviation of the command     Input
      !       NLINE   I       Length of LINE                  Input
      !       ILINE   I       Command number                  Output
      !       ERROR   L       Logical error flag              Output
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      integer :: nline                  !
      integer :: iline                  !
      logical :: error                  !
    end subroutine recfin
  end interface
  !
  interface
    function cindex(line,nline,chain)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Logical function called by RECFIN to determine whether
      !       the input line is an abbreviation of the command line.
      !       Skips language field if required.
      ! Arguments
      !       LINE    C*(*)   Input line
      !       NLINE   I       length of line
      !       CHAIN   C*(*)   Command line
      !---------------------------------------------------------------------
      logical :: cindex                 !
      character(len=*) :: line          !
      integer :: nline                  !
      character(len=*) :: chain         !
    end function cindex
  end interface
  !
  interface
    subroutine type_file(file,nfile,lunout,error)
      use sic_structures
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC Internal routine
      ! Support routine for command:
      !       TYPE File_name
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: file    ! File name
      integer,          intent(in)    :: nfile   ! Length of file name
      integer,          intent(in)    :: lunout  ! Where to display the file
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine type_file
  end interface
  !
  interface
    subroutine sic_delsymbol(symb,error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Remove a symbol from the SIC symbol table
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb   ! Symbol name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_delsymbol
  end interface
  !
  interface
    subroutine sic_inisymbol(error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Initialize the SIC symbol table
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_inisymbol
  end interface
  !
  interface
    subroutine sic_freesymbol(error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      ! Clean the SIC symbol table
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine sic_freesymbol
  end interface
  !
  interface
    subroutine sic_symdict_init(dict,msym,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Initialize any dictionary of type 'sic_symdict_t'
      !---------------------------------------------------------------------
      type(sic_symdict_t), intent(out)   :: dict   !
      integer(kind=4),     intent(in)    :: msym   ! Maximum number of elements
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine sic_symdict_init
  end interface
  !
  interface
    subroutine sic_symdict_free(dict,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Clean any dictionary of type 'sic_symdict_t'
      !---------------------------------------------------------------------
      type(sic_symdict_t), intent(inout) :: dict   !
      logical,             intent(inout) :: error  !
    end subroutine sic_symdict_free
  end interface
  !
  interface
    subroutine sic_symdict_set(dict,symb,tran,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !  Add a new entry in the input symbol dictioanry
      !---------------------------------------------------------------------
      type(sic_symdict_t), intent(inout) :: dict   !
      character(len=*),    intent(in)    :: symb   ! Symbol name
      character(len=*),    intent(in)    :: tran   ! Symbol translation
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine sic_symdict_set
  end interface
  !
  interface
    subroutine sic_symdict_get(dict,symb,tran,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Get a symbol definition
      !---------------------------------------------------------------------
      type(sic_symdict_t), intent(inout) :: dict   !
      character(len=*),    intent(in)    :: symb   ! Symbol name
      character(len=*),    intent(out)   :: tran   ! Symbol translation
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine sic_symdict_get
  end interface
  !
  interface
    subroutine sic_symdict_del(dict,symb,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      ! Remove a symbol from the input dictionary
      !---------------------------------------------------------------------
      type(sic_symdict_t), intent(inout) :: dict   !
      character(len=*),    intent(in)    :: symb   ! Symbol name
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine sic_symdict_del
  end interface
  !
  interface
    subroutine exec_task (buffer)
      use gildas_def
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: buffer        !
    end subroutine exec_task
  end interface
  !
  interface
    subroutine run_task (line,comm,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Dispatch the command line
      ! Arguments :
      ! LINE  C*(*) Command line      Input/Output
      ! COMM  C*(*) Command       Input
      !---------------------------------------------------------------------
      character(len=*)              :: line   !
      character(len=*)              :: comm   !
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine run_task
  end interface
  !
  interface
    function err_task (dummy)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical :: err_task               !
      logical :: dummy                  !
    end function err_task
  end interface
  !
  interface
    subroutine load_task
      use gildas_def
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine load_task
  end interface
  !
  interface
    subroutine exit_task(error)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  Clean the TASK-related components
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine exit_task
  end interface
  !
  interface
    subroutine treals (line,error)
      use gildas_def
      use sic_interactions
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS  Support routine for command
      ! TASK\REAL "Prompt text" Name = [Value]
      !
      ! Define a parameter of specified type (Real, Integer, Logical,
      ! Character, or File) name, and assign it a value if specified.
      ! Otherwise, prompt with the text for a value.
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine treals
  end interface
  !
  interface
    subroutine twrite(line,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS  Support routine for command
      ! TASK\WRITE Parameter
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine twrite
  end interface
  !
  interface
    subroutine tmore(line,error)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine tmore
  end interface
  !
  interface
    subroutine thelp(line,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   TASK\HELP  [TaskName [VarName]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  !
    end subroutine thelp
  end interface
  !
  interface
    subroutine sic_timer(line,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    TIMER
      !    TIMER ON|OFF
      !    TIMER [Time [HOURS|MINUTES|SECONDS]] [/COMMAND String]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_timer
  end interface
  !
  interface
    subroutine sic_timer_show(error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Show current status of the timer
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine sic_timer_show
  end interface
  !
  interface
    subroutine sic_timer_dotime(line,iargval,iargunit,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Set the timer duration
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: iargval
      integer(kind=4),  intent(in)    :: iargunit
      logical,          intent(inout) :: error
    end subroutine sic_timer_dotime
  end interface
  !
  interface
    subroutine sic_timer_docommand(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Set the timer command
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine sic_timer_docommand
  end interface
  !
  interface
    subroutine sic_transpose(line,error)
      use gildas_def
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS Transpose a cube
      !  The algorithm is stupid, but it should not matter on relatively
      ! small cubes
      !---------------------------------------------------------------------
      character(len=*)               :: line   !
      logical,         intent(inout) :: error  !
    end subroutine sic_transpose
  end interface
  !
  interface
    subroutine get_dims(desc,dims)
      use sic_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sic_descriptor_t)     :: desc  !
      integer(kind=index_length) :: dims(sic_maxdims)  !
    end subroutine get_dims
  end interface
  !
  interface
    subroutine var_transpose(descin,descout,code,error,namei,nameo)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      use sic_types
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(sic_descriptor_t)               :: descin   !
      type(sic_descriptor_t)               :: descout  !
      character(len=*),      intent(in)    :: code     !
      logical,               intent(inout) :: error    !
      character(len=*),      intent(in)    :: namei     !
      character(len=*),      intent(in)    :: nameo     !
    end subroutine var_transpose
  end interface
  !
  interface
    subroutine sic_let_auto (var,dble,error)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   External routine
      !       Attributes a value to an already defined variable of numeric type.
      !       No data type checking for this entry. Only Global
      !       variables are affected by this entry, since it is
      !       called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      real*8                          :: dble   !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_auto
  end interface
  !
  interface
    subroutine sic_let_var(namein,value,dble,inte,long,logi,string,fmt,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !       Attributes a value to an already defined variable.
      !       No data type checking for this entry. Only Global
      !       variables are affected by this entry, since it is
      !       called by program only.
      !
      !     Uses the appropriate input variable according to FMT
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: namein  !
      real(kind=4),     intent(in)  :: value   !
      real(kind=8),     intent(in)  :: dble    !
      integer(kind=4),  intent(in)  :: inte    !
      integer(kind=8),  intent(in)  :: long    !
      logical,          intent(in)  :: logi    !
      character(len=*), intent(in)  :: string  !
      integer(kind=4),  intent(in)  :: fmt     !
      logical,          intent(out) :: error   !
    end subroutine sic_let_var
  end interface
  !
  interface
    subroutine sic_def_avar(symb,vaddr,vtype,size,ndim,jdim,readonly,lev,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use sic_types
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! Defines Sic variables by program or user
      !---------------------------------------------------------------------
      character(len=*),             intent(in)  :: symb        ! Variable name
      integer(kind=address_length), intent(in)  :: vaddr       ! Variable address
      integer(kind=4),              intent(in)  :: vtype       ! Variable type
      integer(kind=size_length),    intent(in)  :: size        ! Variable size (4-bytes words)
      integer(kind=4),              intent(in)  :: ndim        ! Number of dimensions
      integer(kind=index_length),   intent(in)  :: jdim(ndim)  ! Dimensions
      logical,                      intent(in)  :: readonly    ! ReadOnly flag
      integer(kind=4),              intent(in)  :: lev         ! Variable level
      logical,                      intent(out) :: error       ! Logical error flag
    end subroutine sic_def_avar
  end interface
  !
  interface
    subroutine sic_validname(name,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SIC  Internal Routine
      !     Check if a variable name is valid
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      logical :: error                  !
    end subroutine sic_validname
  end interface
  !
  interface
    function err_vector (dummy)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical :: err_vector             !
      logical :: dummy                  !
    end function err_vector
  end interface
  !
  interface
    subroutine load_vector
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine load_vector
  end interface
  !
  interface
    subroutine detach(area,name,file,nowait,error)
      use gildas_def
      use gbl_message
      use gsys_types
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !  GILDAS Support routine for command
      !     RUN
      !  Creates a detached job to run the command
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: area    !
      character(len=*), intent(in)  :: name    ! Program name
      character(len=*), intent(in)  :: file    ! Parameter file name
      logical,          intent(in)  :: nowait  !
      logical,          intent(out) :: error   ! Logical error flag
    end subroutine detach
  end interface
  !
  interface
    subroutine spy (line,error)
      use gildas_def
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !	Support routine for command SPY
      !	List all active Tasks of current session.
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine spy
  end interface
  !
  interface
    subroutine remote (name,lun,file,error,inter,node)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS	Internal routine
      !	Submit a program to run in batch queue GILDAS_BATCH
      ! Arguments:
      !	NAME	C*(*)	Program Name
      !	LUN	I	Logical unit where file is opened
      !       FILE    C*(*)   Input file name
      !	ERROR	L	Logical error flag
      !       INTER   L       RUN or SUBMIT command ?
      !       NODE    C*(*)   Remote Node
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      integer :: lun                    !
      character(len=*) :: file          !
      logical :: error                  !
      logical :: inter                  !
      character(len=*) :: node          !
    end subroutine remote
  end interface
  !
  interface
    subroutine local_to_remote (name,file,node)
      !---------------------------------------------------------------------
      ! @ private
      !  VECTOR
      !     Find the equivalent remote path of a local path
      ! Arguments:
      ! 	NAME	C*(*)	File name			Input
      !	FILE	C*(*)	Fully extended file name	Output
      !	NODE    C*(*)	Remote NODE                     Input
      !---------------------------------------------------------------------
      character(len=*) :: name          !
      character(len=*) :: file          !
      character(len=*) :: node          !
    end subroutine local_to_remote
  end interface
  !
  interface
    subroutine submit(area,name,lun,file,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS	Internal routine
      !	Submit a program to run in batch queue GILDAS_BATCH
      ! Arguments:
      !	NAME	C*(*)	Program Name
      !	LUN	I	Logical unit where file is opened
      !	ERROR	L	Logical error flag
      !---------------------------------------------------------------------
      character(len=*) :: area          !
      character(len=*) :: name          !
      integer :: lun                    !
      character(len=*) :: file          !
      logical :: error                  !
    end subroutine submit
  end interface
  !
  interface
    subroutine parameter(comm,line,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS	Support routine for commands
      !	RUN 	Program [Parameter_File] [/EDIT] [/NOWINDOW] [/WINDOW]
      !	SUBMIT	Program [Parameter_File] [/EDIT] [/NOWINDOW] [/WINDOW]
      ! Arguments
      !	COMM	C*(*)	Command (RUN or SUBMIT)
      !	LINE	C*(*)	Command line
      !	ERROR	L	Logical error flag
      !---------------------------------------------------------------------
      character(len=*) :: comm          !
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine parameter
  end interface
  !
  interface
    subroutine edtask (line,comm,iopt,task,ntas,init,nini,area,narea,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      character(len=*)                :: comm   !
      integer                         :: iopt   !
      character(len=*)                :: task   !
      integer                         :: ntas   !
      character(len=*)                :: init   !
      integer                         :: nini   !
      character(len=*)                :: area   !
      integer                         :: narea  !
      logical,          intent(inout) :: error  !
    end subroutine edtask
  end interface
  !
  interface
    subroutine copy_parfile (line,comm,iopt,task,ntas,init,nini,area,narea,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Copy Parameter File
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      character(len=*)                :: comm   !
      integer                         :: iopt   !
      character(len=*)                :: task   !
      integer                         :: ntas   !
      character(len=*)                :: init   !
      integer                         :: nini   !
      character(len=*)                :: area   !
      integer                         :: narea  !
      logical,          intent(inout) :: error  !
    end subroutine copy_parfile
  end interface
  !
  interface
    subroutine prepare(comm,task,exec,file,check_file,lun,rem,error)
      use gildas_def
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GILDAS	Internal routine
      !	Execute the two procedures necessary to setup
      !	task parameters before running or submitting the task
      !
      ! Arguments :
      !	TASK		Task name
      !	EXEC		Program to be executed
      !	FILE		User edited procedure (input)
      !			    command file to execute (output)
      !	CHECK_FILE	Checking procedure
      !	LUN		  Logical unit number where procedure is editted
      !       REM     Remote Flag
      !     ERROR		Logical error flag
      !---------------------------------------------------------------------
      character(len=*) :: comm          !
      character(len=*) :: task          !
      character(len=*) :: exec          !
      character(len=*) :: file          !
      character(len=*) :: check_file    !
      integer :: lun                    !
      logical :: rem                    !
      logical :: error                  !
    end subroutine prepare
  end interface
  !
  interface
    function parse_file(chain,stat)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! VECTOR
      !	Support routine for commands FILE [/OLD] [/NEW]
      !	CHAIN	C*(*)	File name
      !	STAT	I	Status of file (0 Unknown, -1 Old, 1 New)
      !---------------------------------------------------------------------
      integer :: parse_file             !
      character(len=*) :: chain         !
      integer :: stat                   !
    end function parse_file
  end interface
  !
  interface
    subroutine task_out(file,task,error)
      !---------------------------------------------------------------------
      ! @ private
      ! TASK
      !     Copy the .INIT file after X-Window input
      !---------------------------------------------------------------------
      character(len=*) :: file          !
      character(len=*) :: task          !
      logical :: error                  !
    end subroutine task_out
  end interface
  !
  interface
    subroutine write_check(chkfil,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Automatically creat the .check file if it is not already
      ! existing.
      !---------------------------------------------------------------------
      character(len=*) :: chkfil        !
      logical :: error                  !
    end subroutine write_check
  end interface
  !
  interface
    subroutine wordre(lun,line,nl)
      !---------------------------------------------------------------------
      ! @ private
      ! SIC   Internal routine
      !
      !       Writes a command line according to SIC syntax
      !       LUN     Logical unit number
      !       LINE    Command line to write
      !       NL      Length of line
      !---------------------------------------------------------------------
      integer :: lun                    !
      character(len=*) :: line          !
      integer :: nl                     !
    end subroutine wordre
  end interface
  !
  interface
    subroutine xgag_input(line,error)
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GUI\PANEL "Title" HELP_FILE [/CLOSE] [/DETACH]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  !
    end subroutine xgag_input
  end interface
  !
  interface
    subroutine xgag_menus(line,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GUI\MENU "Title"
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine xgag_menus
  end interface
  !
  interface
    subroutine xgag_submenu(line,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! GUI\SUBMENU "Title"
      !   1      [/CLOSE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine xgag_submenu
  end interface
  !
  interface
    subroutine xgag_uri(line,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for GUI Language, URI Uri Label command
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine xgag_uri
  end interface
  !
  interface
    subroutine xgag_comm(line,error)
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GUI\BUTTON Command Bouton [Title Help_file [Option_Title]]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  !
    end subroutine xgag_comm
  end interface
  !
  interface
    subroutine xgag_more(bouton,command,texte,option)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Support for command
      !   TASK\MORE
      ! Create a sub-menu in the TASK\ Window interface
      !---------------------------------------------------------------------
      character(len=*) :: bouton        !
      character(len=*) :: command       !
      character(len=*) :: texte         !
      character(len=*) :: option        !
    end subroutine xgag_more
  end interface
  !
  interface
    subroutine xgag_variable(line,nline,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      use sic_structures
      use sic_types
      use let_options
      !---------------------------------------------------------------------
      ! @ private
      ! This routine fills in the widget structure that will be exchanged
      ! between the SIC-using Program and the standalone Widget Panel. The
      ! structures are defined in sic_structures.h and constructed by the C
      ! routines of sic_xgag.c
      ! The standalone widget panels define mirror structures (their C code
      ! include sic_structures.h). They communicate with SIC by setting the
      ! SIGUSER2 signal. The structure contents are exchanged via a shared
      ! memory segment, whose address is passed by xgag_dialog. The
      ! following FORTRAN code merely allocate room for strings containing
      ! variable names, prompts, options... as read on the command line. It
      ! calls then the C function to fill the C structures. Once passed to
      ! the C code, the memory allocated by the FORTRAN can be freed.
      ! REMARKS:
      ! There should not be any "null-terminated" character chain in the
      ! following FORTRAN code. All the pointers to Strings passed to the
      ! routines in sic_xgag.c should concern well-defined character arrays,
      ! of length known by the sic_xgag routines (varname_length for
      ! VARIABLES NAMES for example). It is up to the C interface routines
      ! of sic-xgag.c (and easier, too) to convert fixed-length arrays to
      ! null-terminated strings in the widgets structure, BACK AND FORTH!
      !---------------------------------------------------------------------
      !     LET Variable [Pre_loaded_value]
      !     [/NEW Type [Attr]]                         Invalid here
      !     [/PROMPT "Prompt text"]
      !     [/WHERE Logical_Expression]                Invalid here
      !     [/RANGE Min Max]
      !     [/CHOICE C1 .. CN [*]]
      !     [/FILE Filter ]
      !     [/INDEX C1 .. CN]
      !     [/SEXAGESIMAL]                             Invalid here ?
      !     [/LOWER]
      !     [/UPPER]
      !     [/FORMAT String]                           Invalid here
      !     [/NOQUOTE]                                 For VALUES in Widget modes
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      integer(kind=4),  intent(in)    :: nline  !
      logical,          intent(inout) :: error  !
    end subroutine xgag_variable
  end interface
  !
  interface
    subroutine xgag_go(line,error)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for commands
      !  GUI\GO  [CommandString]
      !  TASK\GO
      ! End menus (in this case call xgag_launch that activates an xmenu
      ! detached menubar, or activates a Widget panel via the call to
      ! xgag_dialog)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine xgag_go
  end interface
  !
  interface
    subroutine xgag_end(icode,error)
      use gildas_def
      use sic_structures
      use sic_interactions
      use sic_dictionaries
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      ! We come back here after the GO or ABORT button on a xmenu panel
      ! has been pressed.
      ! We write (through unit XLUN) a temporary file to hold
      ! the original commands to re-write the .init file later.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: icode  ! 0 ABORT=DISMISS, 1 ABORT=Error
      logical,         intent(inout) :: error  !
    end subroutine xgag_end
  end interface
  !
  interface
    subroutine xgag_finish
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine xgag_finish
  end interface
  !
  interface
    subroutine xgag_quote(expr,chain)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: expr
      character(len=*), intent(out) :: chain
    end subroutine xgag_quote
  end interface
  !
  interface
    function gmaster_get_id()
      !---------------------------------------------------------------------
      ! @ private
      ! Return master package id.
      !---------------------------------------------------------------------
      integer :: gmaster_get_id         !
    end function gmaster_get_id
  end interface
  !
  interface
    subroutine gmaster_build_logname
      !---------------------------------------------------------------------
      ! @ private
      ! Build effective logname by appending date to logname.
      !---------------------------------------------------------------------
    end subroutine gmaster_build_logname
  end interface
  !
  interface
    subroutine gmaster_set_usage
      !---------------------------------------------------------------------
      ! @ private
      ! Activate 'usage' message at startup
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_usage
  end interface
  !
  interface
    subroutine gmaster_gui(pack_id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Show user interface of loaded packages
      !---------------------------------------------------------------------
      integer, intent(in) :: pack_id    !
    end subroutine gmaster_gui
  end interface
  !
  interface
    subroutine gmaster_gui_menus(lun,error)
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      !  Define the menus in the gui. One menu per package, if provided by
      ! the package.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun    ! The logical to write in
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gmaster_gui_menus
  end interface
  !
  interface
    subroutine gmaster_gui_demo(lun,error)
      use gpack_def
      !---------------------------------------------------------------------
      ! @ private
      !  Define the Demo menu in the gui. One submenu per package, if
      ! provided by the package.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun    ! The logical to write in
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gmaster_gui_demo
  end interface
  !
  interface
    subroutine gmaster_gui_help(lun,error)
      use gpack_def
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ private
      !  Define the Help menu in the gui. One submenu per package, if
      ! provided by the package.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun    ! The logical to write in
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gmaster_gui_help
  end interface
  !
  interface
    subroutine gmaster_show_loaded_packages
      !---------------------------------------------------------------------
      ! @ private
      ! Print informations on screen of all loaded packages
      !---------------------------------------------------------------------
    end subroutine gmaster_show_loaded_packages
  end interface
  !
  interface
    subroutine gmaster_welcome(pack_id)
      !---------------------------------------------------------------------
      ! @ private
      ! Execute the welcome procedure from master package
      !---------------------------------------------------------------------
      integer, intent(in) :: pack_id    !
    end subroutine gmaster_welcome
  end interface
  !
  interface
    subroutine gmaster_loop
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Execute optionnal commands from command line and run the interpreter
      ! loop
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_loop
  end interface
  !
  interface
    subroutine gmaster_custom_init
      !---------------------------------------------------------------------
      ! @ private
      ! Check if some features are customized in dictionaries and different
      ! from defaults.
      ! Nothing is done if values are the defaults (e.g. "sic_window yes")
      !---------------------------------------------------------------------
      ! Local
    end subroutine gmaster_custom_init
  end interface
  !
  interface
    subroutine gmaster_parse_command_line(nomore,error)
      use sic_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Parse command line options. Override customizations from
      ! dictionaries
      !---------------------------------------------------------------------
      logical, intent(out)   :: nomore  ! Should program stop after this?
      logical, intent(inout) :: error   !
    end subroutine gmaster_parse_command_line
  end interface
  !
  interface
    subroutine gmaster_usage
      !---------------------------------------------------------------------
      ! @ private
      ! Display the usage and exit. In particular, describe the available
      ! options.
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_usage
  end interface
  !
  interface
    subroutine gmaster_on_exit(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Execute the on_exit routines of all registered packages
      ! Program can exit if error is false on output
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine gmaster_on_exit
  end interface
  !
  interface
    subroutine gmaster_clean(error)
      use sic_def
      !---------------------------------------------------------------------
      ! @ private
      ! Execute the clean routines of all registered packages
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine gmaster_clean
  end interface
  !
  interface
    subroutine gmaster_build_info(pack_set)
      !---------------------------------------------------------------------
      ! @ private
      ! Build master info
      !---------------------------------------------------------------------
      external :: pack_set              ! Package definition routine
    end subroutine gmaster_build_info
  end interface
  !
  interface
    subroutine gmaster_main_import(pack_set,debug,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Imports the main package and sets the master package id
      !---------------------------------------------------------------------
      external               :: pack_set  ! Package definition method
      logical, intent(in)    :: debug     ! Debug mode
      logical, intent(inout) :: error     !
    end subroutine gmaster_main_import
  end interface
  !
  interface
    subroutine gmaster_import(pack_set,debug,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Imports a package
      !---------------------------------------------------------------------
      external               :: pack_set  ! Package definition method
      logical, intent(in)    :: debug     ! Debug mode
      logical, intent(inout) :: error     !
    end subroutine gmaster_import
  end interface
  !
  interface
    subroutine gmaster_set_exitcode(code)
      !---------------------------------------------------------------------
      ! @ private (should it be public?)
      ! Set the 'exit code' to be issued when leaving
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: code
    end subroutine gmaster_set_exitcode
  end interface
  !
  interface
    subroutine gmaster_set_exitnoprompt(flag)
      !---------------------------------------------------------------------
      ! @ private (should it be public?)
      ! Set the 'exit noprompt' flag
      !---------------------------------------------------------------------
      logical, intent(in) :: flag
    end subroutine gmaster_set_exitnoprompt
  end interface
  !
  interface
    subroutine sic_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine sic_message_set_id
  end interface
  !
  interface
    subroutine sic_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @private
      ! Messaging facility for the current library. Calls the low-level
      ! messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine sic_message
  end interface
  !
end module sic_interfaces_private
