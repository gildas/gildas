!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_interactions
  use gildas_def
  !--------------------------------------------------------------------
  ! Support for low level interactions with SIC: interaction mode,
  ! terminal characteristics, stack or tasks support.
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !--------------------------------------------------------------------
  !
  ! Command line execution:
  ! - input codes
  integer(kind=4), parameter :: code_run   =  0
  integer(kind=4), parameter :: code_exec  = -1
  integer(kind=4), parameter :: code_enter =  1
  integer(kind=4), parameter :: code_play  =  2
  ! - output codes
  integer(kind=4), parameter :: code_next  =  0
  integer(kind=4), parameter :: code_end   = -1
  integer(kind=4), parameter :: code_exit  =  1
  !
  ! Defines if orders reading on unit 5 is interactive or not and if
  ! <^C> has been typed or not.
  logical, save :: inter_state=.true.     ! Session is interactive
  logical :: edit_mode                    ! Line editing is possible
  logical :: loaded=.false.               ! SIC has been initialized
  logical, save :: lexist                 ! File exists...
  logical :: lxwindow                     ! X-Window input mode (current)
  logical, save :: sic_window=.true.      ! X-Window input mode (keyboard)
  logical, save :: sic_initwindow=.true.  ! Initial default menu and plotting window
  integer(kind=8), save :: sic_ramsize=0  ! Support for SIC%RAMSIZE
  integer(kind=8), save :: sic_cachel(4)=0   ! Support for SIC%CACHEL(1:4)
  logical,         save :: sic_omp_compiled  ! Support for SIC%OPENMP%ACTIVE
  integer(kind=4), save :: sic_omp_ncores    ! Support for SIC%OPENMP%NCORES (number of physical cores)
  integer(kind=4), save :: sic_omp_mthreads  ! Support for SIC%OPENMP%MTHREADS
  integer(kind=4), save :: sic_omp_nthreads  ! Support for SIC%OPENMP%NTHREADS
  !
  ! Terminal characteristics
  integer(kind=4) :: sicprecis          ! Code for precision in computations
  integer(kind=4) :: sicinteger         ! Default SIC INTEGER (short or long)
  integer(kind=4) :: siclun=0           ! Support for SIC OUTPUT
  character(len=256), save :: tt_edit   ! Editor name
  character(len=filename_length), save :: cur_dir  ! Current working directory
  character(len=8), save :: sic_precis  ! Arithmetic precision
  logical :: sicsystemerror             ! Error from command SIC\SYSTEM ignored or not?
  !
  ! Editor codes
  integer, parameter :: p_tpu=1     !
  integer, parameter :: p_edt=-1    !
  integer, parameter :: p_auto=0    !
  integer, parameter :: p_emacs=2   ! Emacs
  integer, parameter :: p_vi=-2     ! Vi
  !
  ! Characteristics for one line on screen
  integer, parameter :: eline=24    ! Edit line number
  integer, parameter :: mscreen=40  ! Width of the screen
  integer :: jchar                  ! Character counter
  integer :: jfirst                 ! Leftmost visible character
  integer :: jlast                  ! Rightmost visible character
  integer :: jscreen                ! Current character position on screen
  logical :: updscr                 !
  !
  ! Prompt attributes
  integer(kind=4), parameter :: gpr_base_length=64    ! Max prompt length offered to programmer
  integer(kind=4), parameter :: gpr_suffix_length=5   ! Max prompt suffix length e.g. "_99> "
  integer(kind=4), parameter :: gprompt_length=gpr_base_length+gpr_suffix_length
  character(len=gprompt_length) :: gprompt_master     ! e.g. "SIC"
  character(len=gprompt_length) :: gprompt_base       ! e.g. "SIC" or "procname"
  character(len=gprompt_length) :: gprompt            ! e.g. "SIC_3> " or "procname_3> "
  integer(kind=4)               :: lgprompt_base      ! Length of gprompt_base
  integer(kind=4)               :: lgprompt           ! Length of gprompt with a terminating whitespace included
  integer(kind=4)               :: gprompt_nlire_old  ! Last level used for building gprompt_curr
  !
  ! Stack
  integer, parameter :: maxlin=300           ! Maximum number of lines
  integer :: iend                            ! Last line in buffer
  integer :: istart                          ! First line in buffer
  integer(kind=address_length) :: jstart     ! First byte in buffer
  integer(kind=address_length) :: iloc       ! Insertion location
  integer(kind=address_length) :: stack_desc(2,maxlin) ! Character descriptors
  integer(kind=address_length) :: stack_addr ! Adress of stack
  integer :: maxbyt                          ! Length of stack
  logical :: logbuf=.true.                   ! Stack status
  !
  ! Xgag
  integer, parameter :: m_commands=256              !
  integer, parameter :: x_cmdlen  =filename_length  ! Must be synchronized with CHAINLENGTH in gcore
  integer, parameter :: x_cholen  =8                ! 32 Characters for choices.
  integer, parameter :: m_choices =128              !
  integer(kind=address_length) :: x_variab(2,m_commands) ! Variable names
  integer(kind=address_length) :: x_string(2,m_commands) ! Variable values
  integer(kind=address_length) :: x_choice(2,m_choices)  ! Possible choices
  integer :: x_vtypes(m_commands)            ! Type of variables
  integer :: x_commands                      ! Number of variables
  integer :: x_choices                       ! Number of choices
  integer :: x_mode                          ! Mode indicator
  integer :: x_group                         ! Group indicator
  integer :: noptscr                         ! Number of optional screens
  integer :: xlun                            ! Unit for "save" file
  integer :: x_number(m_commands)            ! Variable number
  !
  integer, parameter :: slider_offset=32000  !
  integer, parameter :: upper_offset=10000   !
  integer, parameter :: lower_offset=20000   !
  integer, parameter :: quote_offset=64000   ! Sufficient ?
  integer, parameter :: resize_offset=128000 !
  !
  ! Tasks support
  integer(kind=4), parameter :: mpar=2048   ! Maximum number of input values in par files
  integer(kind=4), parameter :: mreal=2048  ! Maximum number of input reals  in par files
  integer(kind=4), parameter :: minte=128   ! Same for integers
  integer(kind=4), parameter :: mlogi=128   ! Same for logicals
  integer(kind=4), parameter :: mchar=128   ! Same for character strings
  !
  logical, save :: run_window         ! Parameter in X-Window mode (default)
  logical :: x_window                 ! Parameter in X-Window mode (current)
  logical, save :: tee                ! Tee to screen ?
  integer(kind=4) :: piperr           ! Type of Pipe fo STDERR
  !
  integer(kind=4)         :: tlun        ! Logical unit for parameter file
  logical                 :: tabort      ! Error flag in parameter checks
  real(kind=8),                   save :: treal(mreal)  ! Space for real parameters
  integer(kind=8),                save :: tinte(minte)  ! Space for integer parameters
  logical,                        save :: tlogi(mlogi)  ! Space for logical parameters
  character(len=filename_length), save :: tchar(mchar)  ! Space for character parameters
  integer(kind=4)         :: nreal       !
  integer(kind=4)         :: ninte       !
  integer(kind=4)         :: nlogi       !
  integer(kind=4)         :: nchar       !
  integer(kind=4)         :: npar        !
  character(len=80)       :: tprom(mpar) ! Space for prompt
  character(len=16)       :: tname(mpar) ! Name of parameters
  character(len=16)       :: tprog       ! Name of task
  character(len=4)        :: taskext     ! Task name extension (changes with systems)
  character(len=64)       :: last_task   ! Last Task
  character(len=64), save :: task_node   ! Executor node name
  integer(kind=4)         :: ttype(mpar) ! Type of parameter
  integer(kind=4)         :: tnumb(mpar) ! Number of parameter
  !
  ! Help File name 
  character(len=256), save :: help_text_file=char(0)
end module sic_interactions
!
module let_options
  !
  integer(kind=4), parameter :: mopt=16
  !
  ! Code for options of the LET command (alphabetic order). Must match
  ! declaration in subroutine sic_build.
  integer(kind=4), parameter :: optchoice  = 1   ! /CHOICE
  integer(kind=4), parameter :: optfile    = 2   ! /FILE
  integer(kind=4), parameter :: optformat  = 3   ! /FORMAT
  integer(kind=4), parameter :: optformula = 4   ! /FORMULA
  integer(kind=4), parameter :: optindex   = 5   ! /INDEX
  integer(kind=4), parameter :: optlower   = 6   ! /LOWER
  integer(kind=4), parameter :: optnew     = 7   ! /NEW
  integer(kind=4), parameter :: optnoquote = 8   ! /NOQUOTE
  integer(kind=4), parameter :: optprompt  = 9   ! /PROMPT
  integer(kind=4), parameter :: optrange   = 10  ! /RANGE
  integer(kind=4), parameter :: optreplace = 11  ! /REPLACE
  integer(kind=4), parameter :: optresize  = 12  ! /RESIZE
  integer(kind=4), parameter :: optsexage  = 13  ! /SEXAGESIMAL
  integer(kind=4), parameter :: optstatus  = 14  ! /STATUS
  integer(kind=4), parameter :: optupper   = 15  ! /UPPER
  integer(kind=4), parameter :: optwhere   = 16  ! /WHERE
  !
  ! Option names. This duplicates the actual declaration in subroutine
  ! sic_build, but there is no obvious solution given the way we declare
  ! and use options for now.
  character(len=12), parameter :: optnames(mopt) = (/             &
    '/CHOICE     ','/FILE       ','/FORMAT     ','/FORMULA    ',  &
    '/INDEX      ','/LOWER      ','/NEW        ','/NOQUOTE    ',  &
    '/PROMPT     ','/RANGE      ','/REPLACE    ','/RESIZE     ',  &
    '/SEXAGESIMAL','/STATUS     ','/UPPER      ','/WHERE      '   /)
  !
end module let_options
!
module sic_python
  !---------------------------------------------------------------------
  ! @ private
  ! SIC-Python binding support variables
  !---------------------------------------------------------------------
  !
  ! 'ismaster' flag:
  ! - if it is found .true., it has been changed by the building
  !   routines which are invoked when starting a Gildas program as
  !   master,
  ! - if it is found .false., it has not been changed i.e. Gildas
  !   program is slave.
  ! - is used as support variable for SIC%MASTER
  !
  logical, save :: gildas_ismaster=.false.
  !
end module sic_python
!
module sic_adjust
  use gildas_def
  use sic_expressions, only: formula_length
  !---------------------------------------------------------------------
  ! For ALL methods, unless specified
  !---------------------------------------------------------------------
  integer, parameter :: adj_m=2*26             ! Maximum number of variables
  integer, save :: adj_n                       ! Current number of variables
  integer, save :: ndata                       ! Number of data points
  integer, save :: adj_ncall                   ! Number of function calls (book-keeping)
  real(8), save :: adj_user(adj_m)             ! Parameter values
  real(8), save :: adj_guess(adj_m)            ! Initial guesses
  real(8), save :: adj_step(adj_m)             ! Step for adjustment or gradients
  real(8), save :: adj_errors(adj_m)           ! Parameter errors
  real(8), save :: adj_prog(adj_m)             ! Parameter (working) values
  real(8), save :: adj_lower(adj_m)            ! Lower bounds
  real(8), save :: adj_upper(adj_m)            ! Upper bounds
  integer, save :: adj_bound(adj_m)            ! Bound flags
  character(len=commandline_length), save :: diff_expression  ! Command to execute
  character(len=64), save :: adj_names(adj_m)  ! Parameter names
  character(len=16), save :: adj_root='MFIT'   ! Root name
  integer(4), save :: adj_lroot=4              ! Length of Root name
  integer(1), save :: membyt(8)                ! Address reference
  integer(kind=address_length), save :: adj_exec  ! Address for ADJUST function
  integer(kind=address_length), save :: cur_exec  ! Address of Currently called function
  integer(kind=address_length), save :: fun_exec  ! Address for MFIT function
  !
  ! For Simplex and Powell methods
  real(4) :: press_sum         ! Sum of squares
  ! real(4) :: press_para(27)    ! Variables - unused
  integer :: press_pdim        ! Number of variables
  integer :: press_itmax       ! Maximum number of iterations
  logical, save :: press_error
  !
  ! For MCMC
  real(8) :: adj_chi2          ! Chi2 value
  integer, save :: adj_length=10               ! Length of MCMC run
  integer, save :: adj_walkers=0               ! Number of Walkers
  integer, save :: adj_over=4                  ! Number of Walkers per parameter
  logical :: adj_restart       ! Restart status
  !
  ! For the results
  character(len=formula_length), save :: math_expression  ! Support for MFIT%MATH (formula)
  !
  real(8), allocatable, save :: dvec(:)    ! Difference array
  real(8), allocatable, save :: wvec(:)    ! Weight array
end module sic_adjust
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
