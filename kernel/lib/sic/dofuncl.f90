!-----------------------------------------------------------------------
! Logical tests on REAL*4 arrays
function lsic_s_eq (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! REAL*4 Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_eq      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_eq = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).eq.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).eq.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(i)
    enddo
  elseif (n.eq.1 .and. m1.eq.m2) then
    ! Special case to check identity of two arrays
    result(1) = .false.
    do i=1,m1
      if (oper1(i).ne.oper2(i)) return
    enddo
    result(1) = .true.
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_EQ',mess)
    lsic_s_eq = 1
  endif
end function lsic_s_eq
!
function lsic_s_ne (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Non-Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_ne      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_ne = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ne.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ne.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_NE',mess)
    lsic_s_ne = 1
  endif
end function lsic_s_ne
!
function lsic_s_ge (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_ge      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_ge = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ge.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ge.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_GE',mess)
    lsic_s_ge = 1
  endif
end function lsic_s_ge
!
function lsic_s_gt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_gt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_gt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).gt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).gt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_GT',mess)
    lsic_s_gt = 1
  endif
end function lsic_s_gt
!
function lsic_s_le (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_le      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_le = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).le.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).le.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).le.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).le.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_LE',mess)
    lsic_s_le = 1
  endif
end function lsic_s_le
!
function lsic_s_lt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*4 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_s_lt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*4 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*4 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_s_lt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).lt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).lt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'S_LT',mess)
    lsic_s_lt = 1
  endif
end function lsic_s_lt
!
!----------------------------------------------------------------------
! Logical tests on REAL*8 arrays
function lsic_d_eq (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! REAL*8 Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_eq      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_d_eq = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).eq.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).eq.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(i)
    enddo
  elseif (n.eq.1 .and. m1.eq.m2) then
    ! Special case to check identity of two arrays
    result(1) = .false.
    do i=1,m1
      if (oper1(i).ne.oper2(i)) return
    enddo
    result(1) = .true.
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_EQ',mess)
    lsic_d_eq = 1
  endif
end function lsic_d_eq
!
function lsic_d_ne (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*8 Non-Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_ne      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_d_ne = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ne.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ne.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_NE',mess)
    lsic_d_ne = 1
  endif
end function lsic_d_ne
!
function lsic_d_ge (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_ge      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_d_ge = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ge.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ge.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_GE',mess)
    lsic_d_ge = 1
  endif
end function lsic_d_ge
!
function lsic_d_gt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_gt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_d_gt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).gt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).gt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_GT',mess)
    lsic_d_gt = 1
  endif
end function lsic_d_gt
!
function lsic_d_le (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_le      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer :: i
  character(len=message_length) :: mess
  !
  lsic_d_le = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).le.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).le.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).le.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).le.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_LE',mess)
    lsic_d_le = 1
  endif
end function lsic_d_le
!
function lsic_d_lt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! REAL*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_d_lt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  real*8 :: oper1(m1)               !
  integer(kind=size_length) :: m2   !
  real*8 :: oper2(m2)               !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_d_lt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).lt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).lt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'D_LT',mess)
    lsic_d_lt = 1
  endif
end function lsic_d_lt
!
!-----------------------------------------------------------------------
! Logical tests on LONG INTEGER arrays
function lsic_i_eq (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! INTEGER*8 Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_eq      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_eq = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).eq.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).eq.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).eq.oper2(i)
    enddo
  elseif (n.eq.1 .and. m1.eq.m2) then
    ! Special case to check identity of two arrays
    result(1) = .false.
    do i=1,m1
      if (oper1(i).ne.oper2(i)) return
    enddo
    result(1) = .true.
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_EQ',mess)
    lsic_i_eq = 1
  endif
end function lsic_i_eq
!
function lsic_i_ne (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Non-Equality of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_ne      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_ne = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ne.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ne.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ne.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_NE',mess)
    lsic_i_ne = 1
  endif
end function lsic_i_ne
!
function lsic_i_ge (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_ge      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_ge = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).ge.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).ge.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).ge.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_GE',mess)
    lsic_i_ge = 1
  endif
end function lsic_i_ge
!
function lsic_i_gt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_gt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_gt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).gt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).gt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).gt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_GT',mess)
    lsic_i_gt = 1
  endif
end function lsic_i_gt
!
function lsic_i_le (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_le      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_le = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).le.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).le.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).le.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).le.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_LE',mess)
    lsic_i_le = 1
  endif
end function lsic_i_le
!
function lsic_i_lt (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! INTEGER*8 Comparison of two vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_i_lt      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  integer(kind=8) :: oper1(m1)      !
  integer(kind=size_length) :: m2   !
  integer(kind=8) :: oper2(m2)      !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_i_lt = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).lt.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(1)
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(1).lt.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).lt.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'I_LT',mess)
    lsic_i_lt = 1
  endif
end function lsic_i_lt
!
!----------------------------------------------------------------------
! Logical tests on LOGICAL arrays
function lsic_l_eq (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Comparison of two LOGICAL vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_eq      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  integer(kind=size_length) :: m2   !
  logical :: oper2(m2)              !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_eq = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).eqv.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i).eqv.a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a.eqv.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).eqv.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'L_EQ',mess)
    lsic_l_eq = 1
  endif
end function lsic_l_eq
!
function lsic_l_ne (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Comparison of two LOGICAL vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_ne      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  integer(kind=size_length) :: m2   !
  logical :: oper2(m2)              !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_ne = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).neqv.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i).neqv.a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a.neqv.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).neqv.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'L_NE',mess)
    lsic_l_ne = 1
  endif
end function lsic_l_ne
!
function lsic_l_and (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Comparison of two LOGICAL vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_and     !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  integer(kind=size_length) :: m2   !
  logical :: oper2(m2)              !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_and = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).and.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i).and.a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a.and.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).and.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'L_AND',mess)
    lsic_l_and = 1
  endif
end function lsic_l_and
!
function lsic_l_or (n,result,m1,oper1,m2,oper2)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Comparison of two LOGICAL vectors
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_or      !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  integer(kind=size_length) :: m2   !
  logical :: oper2(m2)              !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_or = 0
  if (m1.eq.1 .and. m2.eq.1) then
    a = oper1(1).or.oper2(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n .and. m2.eq.1) then
    a = oper2(1)
    do i=1,n
      result(i) = oper1(i).or.a
    enddo
  elseif (m1.eq.1 .and. m2.eq.n) then
    a = oper1(1)
    do i=1,n
      result(i) = a.or.oper2(i)
    enddo
  elseif (m1.eq.n .and. m2.eq.n) then
    do i=1,n
      result(i) = oper1(i).or.oper2(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1,m2
    call sic_message(seve%e,'L_OR',mess)
    lsic_l_or = 1
  endif
end function lsic_l_or
!
function lsic_l_not (n,result,m1,oper1)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Negate a LOGICAL vector
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_not     !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  ! Local
  logical :: a
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_not = 0
  if (m1.eq.1) then
    a = .not.oper1(1)
    do i=1,n
      result(i) = a
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = .not.oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'L_NOT',mess)
    lsic_l_not = 1
  endif
end function lsic_l_not
!
function lsic_l_exist (n,result,m1,oper1)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !     Set a LOGICAL vector
  !---------------------------------------------------------------------
  integer(kind=4) :: lsic_l_exist   !
  integer(kind=size_length) :: n    !
  logical :: result(n)              !
  integer(kind=size_length) :: m1   !
  logical :: oper1(m1)              !
  ! Local
  integer(kind=size_length) :: i
  character(len=message_length) :: mess
  !
  lsic_l_exist = 0
  if (m1.eq.1) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  elseif (m1.eq.n) then
    do i=1,n
      result(i) = oper1(i)
    enddo
  else
    write(mess,*) 'Inconsistent dimensions ',n,m1
    call sic_message(seve%e,'L_EXIST',mess)
    lsic_l_exist = 1
  endif
end function lsic_l_exist
