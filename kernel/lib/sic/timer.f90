subroutine sic_timer(line,error)
  use sic_interfaces, except_this=>sic_timer
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    TIMER
  !    TIMER ON|OFF
  !    TIMER [Time [HOURS|MINUTES|SECONDS]] [/COMMAND String]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  logical :: show
  !
  show = .true.
  if (sic_present(0,1)) then
    call sic_timer_dotime(line,1,2,error)
    if (error)  return
    show = .false.
  endif
  if (sic_present(1,0)) then
    call sic_timer_docommand(line,error)
    if (error)  return
    show = .false.
  endif
  if (show) then
    call sic_timer_show(error)
    if (error)  return
  endif
  !  
end subroutine sic_timer
!
subroutine sic_timer_show(error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_timer_show
  !---------------------------------------------------------------------
  ! @ private
  ! Show current status of the timer
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TIMER'
  character(len=message_length) :: mess
  character(len=commandline_length) :: command
  integer(kind=4) :: status,timersec
  !
  call xgag_gettimer_status(status)
  if (status.eq.0) then
    call sic_message(seve%i,rname,'Timer is OFF')
  else
    call sic_message(seve%i,rname,'Timer is ON')
  endif
  !
  call xgag_gettimer(timersec)
  if (timersec.eq.0) then
    write(mess,'(A)')         'No timer value set'
  elseif (timersec.ge.3600) then
    write(mess,'(A,F0.2,A)')  'Timer value: ',timersec/3600.,' hour(s)'
  elseif (timersec.ge.60) then
    write(mess,'(A,F0.2,A)')  'Timer value: ',timersec/60.,' minute(s)'
  else
    write(mess,'(A,I0,A)')    'Timer value: ',timersec,' second(s)'
  endif
  call sic_message(seve%i,rname,mess)
  !
  call xgag_gettimer_command(command)
  write(mess,'(A,A)')  'Timer command: ',trim(command)
  call sic_message(seve%i,rname,mess)
  !
end subroutine sic_timer_show
!
subroutine sic_timer_dotime(line,iargval,iargunit,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_timer_dotime
  !---------------------------------------------------------------------
  ! @ private
  ! Set the timer duration
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  integer(kind=4),  intent(in)    :: iargval
  integer(kind=4),  intent(in)    :: iargunit
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TIMER'
  integer(kind=4), parameter :: nstatus=2
  character(len=3), parameter :: status(nstatus) = (/ 'ON ','OFF' /)
  integer(kind=4), parameter :: ntime=3
  character(len=7), parameter :: time(ntime) = (/ 'HOURS  ','MINUTES','SECONDS' /)
  character(len=7) :: key
  integer(kind=4) :: n,ikey
  character(len=40) :: argum
  integer(kind=4) :: timersec
  real(kind=4) :: timer
  !
  ! Try first ON or OFF
  call sic_ke(line,0,iargval,argum,n,.true.,error)
  if (error) return
  call sic_ambigs_sub(rname,argum,key,ikey,status,nstatus,error)
  if (error)  return
  if (ikey.ne.0) then
    ! Found a status
    if (ikey.eq.2)  ikey = 0   ! OFF => 0
    call xgag_settimer_status(ikey)
    return
  endif
  !
  ! No status keyword found, try a numeric value
  call sic_r4(line,0,iargval,timer,.true.,error)
  if (error)  return
  argum = 'HOURS'
  call sic_ke(line,0,iargunit,argum,n,.false.,error)
  if (error)  return
  call sic_ambigs(rname,argum,key,ikey,time,ntime,error)
  if (error)  return
  !
  if (ikey.eq.1) then
    timersec = timer*3600.
  elseif (ikey.eq.2) then
    timersec = timer*60.
  else
    timersec = timer
  endif
  call xgag_settimer(timersec)
  !
end subroutine sic_timer_dotime
!
subroutine sic_timer_docommand(line,error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_timer_docommand
  !---------------------------------------------------------------------
  ! @ private
  ! Set the timer command
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TIMER'
  integer(kind=4), parameter :: optcommand=1
  character(len=commandline_length) :: command
  integer(kind=4) :: n
  !
  if (sic_present(optcommand,2)) then
    call sic_message(seve%e,rname,'Trailing arguments to option /COMMAND')
    call sic_message(seve%e,rname,'Command string must be given as single argument, possibly double-quoted')
    error = .true.
    return
  endif
  !
  call sic_ch(line,optcommand,1,command,n,.true.,error)
  if (error)  return
  !
  call xgag_settimer_command(trim(command))
  !
end subroutine sic_timer_docommand
