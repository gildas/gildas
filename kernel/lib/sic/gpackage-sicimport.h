/* Add this include file to define the Sic entry point named
   'foo_sic_import' for package 'foo'. Used by command "SIC\IMPORT" from
   Sic or any Gildas package */

#include "gsys/cfc.h"

#define GPACK_SET_FCT_NAME(package) CFC_EXPORT_NAME(name2(package,_pack_set))

#define GPACK_DEFINE_SIC_IMPORT(package)                                      \
                                                                              \
/**                                                                           \
 * SIC import entry point                                                     \
 */                                                                           \
void name2(package,_sic_import)( int debug, int *error)                       \
{                                                                             \
    void gpack_c_import( );                                                   \
    void CFC_API GPACK_SET_FCT_NAME(package)( );                              \
                                                                              \
    gpack_c_import( GPACK_SET_FCT_NAME(package), debug, error);               \
}
