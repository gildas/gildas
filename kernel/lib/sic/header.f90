subroutine vector_header(line,error)
  use image_def
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>vector_header
  use gbl_message
  use sic_structures
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !  VECTOR\HEADER GDFFile|FITSFile|SicVariable
  ! 1 /EXTREMA         Add/update the extrema section
  ! 2 /TELESCOPE Name  Add/update the telescope section
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='HEADER'
  integer(kind=4), parameter :: optextr=1
  integer(kind=4), parameter :: opttele=2
  logical :: doextr,dotele,dohead,needdata,readonly
  type(gildas), target :: mine
  type(gildas), pointer :: theheader
  integer(kind=4) :: i, n, ier, what
  real(kind=4), allocatable :: mydata(:,:)
  integer(kind=size_length) :: asize
  character(len=filename_length) :: chain
  type(sic_descriptor_t) :: desc
  logical :: found
  !
  if (.not.sic_present(0,1)) then
    call sic_message(seve%e,rname,'Missing file name as argument')
    call sic_message(seve%e,rname,  &
      'Use [GREG3\]IMAGE (with no argument) to list the IMAGE header')
    error = .true.
    return
  endif
  !
  ! Command line parsing
  call gildas_null(mine)
  call sic_ch(line,0,1,chain,n,.true.,error)
  if (error) return
  doextr = sic_present(optextr,0)
  dotele = sic_present(opttele,0)
  dohead = (.not.doextr) .and. (.not.dotele)
  needdata = doextr
  readonly = .true.
  theheader => null()
  !
  ! Check if file or header variable. 1=GDF, 2=FITS, 3=variable
  if (sic_query_file(chain,' ',' ',mine%file)) then
    call gag_file_guess(rname,mine%file,what,error)
    if (error)  return
    if (what.eq.0) then
      call sic_message(seve%w,rname,'File '//trim(chain)//' is neither GDF nor FITS')
      return
    endif
    readonly = what.eq.2
  else
    call sic_descriptor(chain,desc,found)
    if (.not.found) then
      call sic_message(seve%w,rname,'No such file or variable '//chain)
      return
    endif
    if (.not.associated(desc%head)) then
      call sic_message(seve%w,rname,  &
        'Variable '//trim(chain)//' does not provide a header')
      return
    endif
    what = 3
    readonly = desc%readonly
  endif
  !
  ! Read GDF if relevant
  select case (what)
  case (1)
    call gdf_read_header(mine,error) ! Just read header
    if (error) return
    theheader => mine
    if (needdata) then
      if (mine%gil%type_gdf.eq.code_gdf_uvt) then
        allocate(mydata(mine%gil%dim(1),mine%gil%dim(2)),stat=ier)
        call gdf_read_data(mine,mydata,error)
        if (error)  return
        mine%loca%addr = locwrd(mydata)
      elseif (mine%gil%type_gdf.eq.code_gdf_tuv) then
        allocate(mydata(mine%gil%dim(1),2),stat=ier)
        mine%blc(2) = 1
        mine%trc(2) = 2
        call gdf_read_data(mine,mydata,error)
        if (error)  return
        mine%loca%addr = locwrd(mydata)
      else
        ! The Real*8 case is not well treated yet...
        asize = 1
        do i=1,mine%gil%ndim
          asize = asize*mine%gil%dim(i)
        enddo
        allocate(mydata(asize,1),stat=ier)
        if (failed_allocate(rname,'data',ier,error))  return
        call gdf_read_data(mine,mydata,error)
        if (error)  return
        mine%loca%addr = locwrd(mydata)
      endif
    endif
  case (2)
    theheader => null()
    if (needdata) then
      call sic_message(seve%w,rname,'Can not access data on FITS files')
      goto 10
    endif
  case (3)
    theheader => desc%head
    if (needdata .and. desc%addr.eq.ptr_null) then
      call sic_message(seve%w,rname,trim(chain)//' does not provide data')
      goto 10
    endif
    desc%head%loca%addr = desc%addr
    desc%head%loca%size = desc%size
  end select
  !
  ! /EXTREMA
  if (doextr) then
    if (readonly) then
      call sic_message(seve%w,rname,trim(chain)//' is read-only')
      goto 10
    endif
    if (abs(theheader%gil%type_gdf).eq.code_gdf_uvt) then
      call gdf_get_baselines (theheader,error)
      if (error)  goto 10
    else
      call gdf_get_extrema (theheader,error)
      if (error)  goto 10
    endif
  endif
  !
  ! /TELESCOPE
  if (dotele) then
    if (readonly) then
      call sic_message(seve%w,rname,trim(chain)//' is read-only')
      goto 10
    endif
    call vector_header_telescope(line,theheader,error)
    if (error)  goto 10
  endif
  !
  ! No option (display header)
  if (dohead) then
    if (what.eq.2) then
      call gdf_fits_header(chain,error)
    else
      call gdf_print_header(theheader)
    endif
  endif
  !
  ! Free
10 continue
  if (what.eq.1) then
    if (doextr .or. dotele) then
      call gdf_update_header(theheader,error)
      if (error)  continue
    endif
    call gdf_close_image(mine,error)   ! Do not forget
    if (error)  return
  endif
  if (allocated(mydata))  deallocate(mydata)  ! Useless
  !
end subroutine vector_header
!
subroutine vector_header_telescope(line,img,error)
  use image_def
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>vector_header_telescope
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   V\HEADER File /TELESCOPE [+-]Name1 ... [+-]NameN
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  type(gildas),     intent(inout) :: img
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='HEADER'
  integer(kind=4), parameter :: opttele=2
  character(len=24) :: teles
  integer(kind=4) :: iarg,n
  logical :: add
  real(kind=8) :: dummy(3)
  !
  do iarg=1,max(1,sic_narg(opttele))
    call sic_ke(line,opttele,iarg,teles,n,.true.,error)
    if (error)  return
    !
    if (teles(1:1).eq.'+') then
      add = .true.
      teles = teles(2:)
    elseif (teles(1:1).eq.'-') then
      add = .false.
      teles = teles(2:)
    else
      add = .true.
    endif
    !
    if (add) then
      call gdf_addteles(img,'TELE',teles,dummy,error)
    else
      call gdf_rmteles(img,teles,error)
    endif
    if (error)  return
  enddo
  !
end subroutine vector_header_telescope
!
subroutine gdf_fits_header(file,error)
  use image_def
  use sic_dependencies_interfaces
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command VECTOR\HEADER File
  !    for FITS file format
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: file
  logical, intent(inout) :: error
  !
  integer :: flun,ier, i,j
  integer(kind=4) :: farray(20,36)
  character(len=8) :: fkeyw
  character(len=80) :: fline
  character(len=filename_length) :: fich
  !
  ier = sic_getlun(flun)
  call sic_parse_file(file,' ',' ',fich)
  open(unit=flun,file=trim(fich),recl=720*facunf,status='OLD',form='UNFORMATTED',  &
       access='DIRECT',action='READ',iostat=ier)
  if (ier.ne.0) then
    call putios('E-OPEN,  ',ier)
    call sic_frelun(flun)
    error = .true.
    return
  endif
  !
  read(flun,rec=1) farray
  call bytoch(farray(1:20,1),fkeyw,8)
  if (fkeyw.ne.'SIMPLE ') then
    error = .true.
    close(unit=flun)
    call sic_frelun(flun)
    return
  endif
  !
  j = 1
  do
    do i=1,36
      call bytoch(farray(1:20,i),fline,80)
      write(*,'(a)') trim(fline)
      if (fline(1:4).eq.'END ') exit
    enddo
    if (fline(1:4).eq.'END ') exit
    j = j+1
    read(flun,rec=j) farray
  enddo
  !
  close(unit=flun)
  call sic_frelun(flun)
  error = .false.
end subroutine gdf_fits_header
!
