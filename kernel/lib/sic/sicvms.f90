subroutine sic_dcl(line,error)
  use sic_dependencies_interfaces
  use gbl_message
  use sic_interfaces, except_this=>sic_dcl
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Support routine for command
  !       SYSTEM ["commands"]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SYSTEM'
  character(len=512) :: chain,arg
  integer(kind=4) :: lc,ier,sev
  character(len=512) :: namshell
  !
  if (.not.sic_present(0,1)) then
    ! SYSTEM without argument : switch to current shell
    ier = sic_getlog('GAG_PROCESS',chain)
    if (ier.ne.0) then
      ! get the SHELL environment variable
      namshell=' '
      ! Note by DB: raz NAMSHELL (otherwise LC= LEN_TRIM(NAMSHELL)+1
      !    may be incoherent if compiler don't initialize
      !    local data
      call sic_getenv('SHELL',namshell)
      if (namshell.eq.' ')  namshell = '/bin/sh'
      chain = namshell
    endif
    ier = gag_system(chain)
    if (ier.ne.0 .and. sicsystemerror)  error = .true.
    !
  elseif (sic_present(0,2)) then
    ! More than one argument: unsupported
    call sic_message(seve%e,rname,'Supported syntax is : SYSTEM')
    call sic_message(seve%e,rname,'                 or : SYSTEM file')
    call sic_message(seve%e,rname,  &
           '                 or : SYSTEM "args" (ex: SYSTEM "ls -l")')
    error = .true.
    !
  else
    ! SYSTEM command execute a single command with -c option
    call sic_ch (line,0,1,arg,lc,.false.,error)
    if (error) return
    ! The following is provided for compatibility with a previous version
    ! of SYSTEM, when the ! was sometimes inserted to prevent lowercase
    ! conversion of the command line.
    if (arg(1:1).eq.'!') then
      chain = arg(2:)
    else
      chain = arg
    endif
    ier = gag_system(chain)
    if (ier.ne.0) then
      if (sicsystemerror) then
        sev = seve%e
        error = .true.
      else
        sev = seve%w
      endif
      call sic_message(sev,rname,'Non-zero status returned by command: '//chain)
    endif
  endif
  !
end subroutine sic_dcl
