!-----------------------------------------------------------------------
!  Support file for writing the content of a Sic variable into a target
! Fortran variable, with automatic type casting.
!  (For the reverse operation, i.e. writing a Sic variable into a
! Fortran variable, see file get.f90)
!-----------------------------------------------------------------------
!
subroutine r8_fill(n,array,value)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n         ! Size of array
  real(kind=8),              intent(out) :: array(n)  ! Array
  real(kind=8),              intent(in)  :: value     ! Scalar value to assign
  !
  array(1:n) = value
end subroutine r8_fill
!
subroutine r4_fill(n,array,value)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n         ! Size of array
  real(kind=4),              intent(out) :: array(n)  ! Array
  real(kind=4),              intent(in)  :: value     ! Scalar value to assign
  !
  array(1:n) = value
end subroutine r4_fill
!
subroutine i8_fill(n,array,value)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n         ! Size of array
  integer(kind=8),           intent(out) :: array(n)  ! Array
  integer(kind=8),           intent(in)  :: value     ! Scalar value to assign
  !
  array(1:n) = value
end subroutine i8_fill
!
subroutine i4_fill(n,array,value)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n         ! Size of array
  integer(kind=4),           intent(out) :: array(n)  ! Array
  integer(kind=4),           intent(in)  :: value     ! Scalar value to assign
  !
  array(1:n) = value
end subroutine i4_fill
!
subroutine c4_fill(n,array,value)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n         ! Size of array
  complex(kind=4),           intent(out) :: array(n)  ! Array
  complex(kind=4),           intent(in)  :: value     ! Scalar value to assign
  !
  array(1:n) = value
end subroutine c4_fill
!
subroutine mask_fill (idesc,odesc,ldesc)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>mask_fill
  use gildas_def
  use gbl_format
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(sic_descriptor_t) :: idesc  !
  type(sic_descriptor_t) :: odesc  !
  type(sic_descriptor_t) :: ldesc  !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  integer(kind=size_length) :: size
  integer(kind=address_length) :: ipnt,opnt,lpnt
  !
  ipnt = gag_pointer(idesc%addr,memory)
  opnt = gag_pointer(odesc%addr,memory)
  lpnt = gag_pointer(ldesc%addr,memory)
  size = ldesc%size
  if (idesc%type.eq.fmt_r8 .or. idesc%type.eq.fmt_i8) then
    call mask_fill_r8 (memory(ipnt),memory(opnt),memory(lpnt),size)
  else
    call mask_fill_r4 (memory(ipnt),memory(opnt),memory(lpnt),size)
  endif
end subroutine mask_fill
!
subroutine mask_fill_r4(in,out,mask,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Fill OUT array with IN array values where MASK logical array is .true.
  ! Integer*4 / Real*4 / Logical*4 version for IN/OUT arrays.
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in)  :: n  ! Number of elements in arrays
  integer*4, intent(in)    :: in(n)            ! Read values from this one
  integer*4, intent(inout) :: out(n)           ! Put values in this one
  logical,   intent(in)    :: mask(n)          ! Mask array
  !
  where (mask) out = in
end subroutine mask_fill_r4
!
subroutine mask_fill_r8(in,out,mask,n)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! Fill OUT array with IN array values where MASK logical array is .true.
  ! Integer*8 / Real*8 version for IN/OUT arrays.
  !---------------------------------------------------------------------
  integer(kind=size_length), intent(in) :: n  ! Number of elements in arrays
  real*8,  intent(in)    :: in(n)             ! Read values from this one
  real*8,  intent(inout) :: out(n)            ! Put values in this one
  logical, intent(in)    :: mask(n)           ! Mask array
  !
  where (mask) out = in
end subroutine mask_fill_r8
!
!-----------------------------------------------------------------------
!
subroutine sic_variable_filli4_0d(caller,varname,i4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_filli4_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the i4 value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  integer(kind=4),  intent(in)    :: i4       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  integer(kind=4) :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = i4
  call sic_variable_filli4_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_filli4_0d
!
subroutine sic_variable_filli4_1d(caller,varname,i4,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_filli4_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the i4 value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  integer(kind=4),           intent(in)    :: i4(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of i4 values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_i41d(desc,i4,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_filli4_1d
!
subroutine sic_variable_filli8_0d(caller,varname,i8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_filli8_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the i8 value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  integer(kind=8),  intent(in)    :: i8       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  integer(kind=8) :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = i8
  call sic_variable_filli8_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_filli8_0d
!
subroutine sic_variable_filli8_1d(caller,varname,i8,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_filli8_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the i8 value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  integer(kind=8),           intent(in)    :: i8(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of i8 values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_i81d(desc,i8,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_filli8_1d
!
subroutine sic_variable_fillr4_0d(caller,varname,r4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_fillr4_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the r4 value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  real(kind=4),     intent(in)    :: r4       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  real(kind=4) :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = r4
  call sic_variable_fillr4_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_fillr4_0d
!
subroutine sic_variable_fillr4_1d(caller,varname,r4,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_fillr4_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the r4 value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  real(kind=4),              intent(in)    :: r4(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of r4 values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_r41d(desc,r4,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_fillr4_1d
!
subroutine sic_variable_fillr8_0d(caller,varname,r8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_fillr8_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the r8 value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  real(kind=8),     intent(in)    :: r8       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  real(kind=8) :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = r8
  call sic_variable_fillr8_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_fillr8_0d
!
subroutine sic_variable_fillr8_1d(caller,varname,r8,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_fillr8_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the r8 value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  real(kind=8),              intent(in)    :: r8(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of r8 values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_r81d(desc,r8,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_fillr8_1d
!
subroutine sic_variable_fillc4_0d(caller,varname,c4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_fillc4_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the c4 value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  complex(kind=4),  intent(in)    :: c4       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  complex(kind=4) :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = c4
  call sic_variable_fillc4_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_fillc4_0d
!
subroutine sic_variable_fillc4_1d(caller,varname,c4,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_fillc4_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the c4 value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  complex(kind=4),           intent(in)    :: c4(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of c4 values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_c41d(desc,c4,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_fillc4_1d
!
subroutine sic_variable_filll_0d(caller,varname,l,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_filll_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the l value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  logical,          intent(in)    :: l        ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  logical :: val(1)
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = l
  call sic_variable_filll_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_filll_0d
!
subroutine sic_variable_filll_1d(caller,varname,l,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_filll_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the l value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  logical,                   intent(in)    :: l(*)     ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of l values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_l1d(desc,l,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_filll_1d
!
subroutine sic_variable_fillch_0d(caller,varname,ch,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_variable_fillch_0d
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the ch value
  ! ---
  !  Scalar version
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller   !
  character(len=*), intent(in)    :: varname  !
  character(len=*), intent(in)    :: ch       ! Scalar value to be copied
  logical,          intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=512) :: val(1)  ! Arbitrary limited to 512 characters
  integer(kind=size_length), parameter :: one=1
  !
  val(1) = ch
  call sic_variable_fillch_1d(caller,varname,val,one,error)
  if (error)  return
  !
end subroutine sic_variable_fillch_0d
!
subroutine sic_variable_fillch_1d(caller,varname,ch,nelem,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_fillch_1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_variable_fill
  !  Fill a SIC variable with the ch value
  ! ---
  !  Flat array version
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: caller   !
  character(len=*),          intent(in)    :: varname  !
  character(len=*),          intent(in)    :: ch(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: nelem    ! Number of l values
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  !
  call sic_variable_getdesc(caller,varname,.true.,desc,error)
  if (error)  return
  !
  call sic_descriptor_fill_ch1d(desc,ch,nelem,error)
  if (error)  return
  !
end subroutine sic_variable_fillch_1d
!
subroutine sic_variable_getdesc(caller,varname,readwrite,desc,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_variable_getdesc
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Get the descriptor of a variable and perform the appropriate checks
  ! for use by the sic_variable_fill* subroutines
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: caller     ! Calling routine name
  character(len=*),       intent(in)    :: varname    !
  logical,                intent(in)    :: readwrite  ! Need read-write access?
  type(sic_descriptor_t), intent(out)   :: desc       !
  logical,                intent(inout) :: error      !
  ! Local
  logical :: found
  !
  found = .true.  ! Verbose
  call sic_descriptor(varname,desc,found)
  if (.not.found) then
    call sic_message(seve%e,caller,'No such variable '//varname)
    error = .true.
    return
  endif
  if (readwrite .and. desc%readonly) then
    ! NB: maybe we would like to allow writing in a read-only variable
    ! from the program. Pass a flag to the subroutine?
    call sic_message(seve%e,caller,'Readonly variables cannot be modified')
    error = .true.
    return
  endif
  !
end subroutine sic_variable_getdesc
!
!-----------------------------------------------------------------------
!
subroutine sic_descriptor_getnelem(desc,neleml,istarget,nelem0,ipnt0,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getnelem
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute and return the number of elements and the 'ipnt' of the Sic
  ! variable described by its descriptor, and check if it is consistent
  ! with 'neleml'.
  !  Technical subroutine mostly to be used by the sic_descriptor_fill*
  ! and sic_descriptor_getval* subroutines below.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),       intent(in)    :: desc      !
  integer(kind=size_length),    intent(in)    :: neleml    !
  logical,                      intent(in)    :: istarget  ! 'desc' is target or source of values?
  integer(kind=size_length),    intent(out)   :: nelem0    !
  integer(kind=address_length), intent(out)   :: ipnt0     !
  logical,                      intent(inout) :: error     !
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  character(len=message_length) :: mess
  logical :: match
  !
  if (istarget .and. desc%readonly) then
    call sic_message(seve%e,rname,'Variable is read-only')
    error = .true.
    return
  endif
  !
  ! Convert Nwords to Nelem
  select case (desc%type)
  case (fmt_r8,fmt_i8,fmt_c4)
    nelem0 = desc%size/2
  case (fmt_r4,fmt_i4,fmt_l)
    nelem0 = desc%size
  case (fmt_un)
    call sic_message(seve%e,rname,'Data type is unknown (fmt_un)')
    error = .true.
    return
  case default
    if (desc%type.gt.0) then
      call sic_message(seve%e,rname,  &
        'Can not convert character string to/from numeric or logical')
    else
      call sic_message(seve%e,rname,'Data type not supported (4)')
    endif
    error = .true.
    return
  end select
  !
  ! Accepted combinations are:
  ! - arrays of equal size
  ! - input is scalar and output is array
  match = nelem0.eq.neleml             .or.  &
          (istarget .and. neleml.eq.1) .or.  &  ! desc is output
          (.not.istarget .and. nelem0.eq.1)     ! desc is input
  if (.not.match) then
    write(mess,'(A,I0,1X,I0)')  &
      'Mathematics on arrays of inconsistent dimensions ',nelem0,neleml
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ipnt0 = gag_pointer(desc%addr,memory)
  !
end subroutine sic_descriptor_getnelem
!
subroutine sic_descriptor_get1elem(desc,istarget,ielem,ipnt0,error)
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_get1elem
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Get the address of the ielem-th element in array
  !---------------------------------------------------------------------
  type(sic_descriptor_t),       intent(in)    :: desc
  logical,                      intent(in)    :: istarget  ! 'desc' is target or source of values?
  integer(kind=size_length),    intent(in)    :: ielem
  integer(kind=address_length), intent(out)   :: ipnt0
  logical,                      intent(inout) :: error
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=4) :: nwords
  character(len=message_length) :: mess
  !
  if (istarget .and. desc%readonly) then
    call sic_message(seve%e,rname,'Variable is read-only')
    error = .true.
    return
  endif
  !
  ! Convert Nwords to Nelem
  select case (desc%type)
  case (fmt_r8,fmt_i8,fmt_c4)
    nwords = 2
  case (fmt_r4,fmt_i4,fmt_l)
    nwords = 1
  case (fmt_un)
    call sic_message(seve%e,rname,'Data type is unknown (fmt_un)')
    error = .true.
    return
  case default
    if (desc%type.gt.0) then
      call sic_message(seve%e,rname,  &
        'Can not convert character string to/from numeric or logical')
    else
      call sic_message(seve%e,rname,'Data type not supported (4)')
    endif
    error = .true.
    return
  end select
  !
  if (ielem.lt.1 .or. ielem.gt.desc%size/nwords) then
    write(mess,'(A,I0,1X,I0)')  'Out of bounds ',ielem,desc%size/nwords
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ipnt0 = gag_pointer(desc%addr,memory)+(ielem-1)*nwords
  !
end subroutine sic_descriptor_get1elem
!
subroutine sic_descriptor_fill_i40d(desc,ielem,i4,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_i40d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill the ielem-th value from the descriptor target. The value is
  ! casted to the descriptor type if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  integer(kind=4),           intent(in)    :: i4      ! Scalar value to fill
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.true.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    call w4tow4_sl (i4,memory(ipnt0),one)
  case (fmt_r8)
    call i4tor8_sl (i4,memory(ipnt0),one)
  case (fmt_r4)
    call i4tor4_sl (i4,memory(ipnt0),one)
  case (fmt_i8)
    call i4toi8_sl (i4,memory(ipnt0),one)
  case (fmt_c4)
    call i4toc4_sl (i4,memory(ipnt0),one)
  case default
    call sic_message(seve%e,rname,'Result type mismatch (1)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_i40d
!
subroutine sic_descriptor_fill_r80d(desc,ielem,r8,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_r80d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill the ielem-th value from the descriptor target. The value is
  ! casted to the descriptor type if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  real(kind=8),              intent(in)    :: r8      ! Scalar value to fill
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.true.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    call w8tow8_sl (r8,memory(ipnt0),one)
  case (fmt_r4)
    call r8tor4_sl (r8,memory(ipnt0),one)
  case (fmt_i4)
    call r8toi4_fini_sl (r8,memory(ipnt0),one,error)
    if (error)  return
  case (fmt_i8)
    call r8toi8_fini_sl (r8,memory(ipnt0),one,error)
    if (error)  return
  case (fmt_c4)
    call r8toc4_sl (r8,memory(ipnt0),one)
  case default
    call sic_message(seve%e,rname,'Result type mismatch (2)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_r80d
!
subroutine sic_descriptor_fill_ch0d(desc,ielem,ch,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_ch0d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the ch(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  character(len=*),          intent(in)    :: ch      ! Scalar string to fill
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: addr
  !
  if (ielem.lt.1 .or. ielem.gt.desc_nelem(desc)) then
    call sic_message(seve%e,rname,'Out of bounds (1)')
    error = .true.
    return
  endif
  !
  if (desc%type.le.0) then
    call sic_message(seve%e,rname,'Can not convert character to numeric/logical')
    error = .true.
    return
  endif
  !
  addr = desc%addr+(ielem-1)*desc%type
  call ctodes(ch,desc%type,addr)
  !
end subroutine sic_descriptor_fill_ch0d
!
subroutine sic_descriptor_fill_i41d(desc,i4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_i41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the i4(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=4),           intent(in)    :: i4(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of i4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    if (neleml.eq.nelem0) then
      call w4tow4_sl (i4,memory(ipnt0),neleml)  ! Word copy
    elseif (neleml.eq.1) then
      call i4_fill(nelem0,memory(ipnt0),i4)
    endif
    !
  case (fmt_r8)
    if (neleml.eq.nelem0) then
      call i4tor8_sl (i4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i4tor8_sl (i4,memory(ipnt0),neleml)  ! Convert first element
      call r8_fill(nelem0,memory(ipnt0),memory(ipnt0) )  ! Duplicate
    endif
    !
  case (fmt_r4)
    if (neleml.eq.nelem0) then
      call i4tor4_sl (i4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i4tor4_sl (i4,memory(ipnt0),neleml)
      call r4_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_i8)
    if (neleml.eq.nelem0) then
      call i4toi8_sl (i4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i4toi8_sl (i4,memory(ipnt0),neleml)
      call i8_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_c4)
    if (neleml.eq.nelem0) then
      call i4toc4_sl (i4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i4toc4_sl (i4,memory(ipnt0),neleml)
      call c4_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (3)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_i41d
!
subroutine sic_descriptor_fill_i81d(desc,i8,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_i81d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the i8(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=8),           intent(in)    :: i8(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of i8 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i8)
    if (neleml.eq.nelem0) then
      call w8tow8_sl (i8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i8_fill(nelem0,memory(ipnt0),i8)
    endif
    !
  case (fmt_r8)
    if (neleml.eq.nelem0) then
      call i8tor8_sl (i8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i8tor8_sl (i8,memory(ipnt0),neleml)
      call r8_fill(nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_r4)
    if (neleml.eq.nelem0) then
      call i8tor4_sl (i8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i8tor4_sl (i8,memory(ipnt0),neleml)
      call r4_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_i4)
    if (nelem0.eq.neleml) then
      call i8toi4_fini_sl (i8,memory(ipnt0),neleml,error)
      if (error)  return
    elseif (neleml.eq.1) then
      call i8toi4_fini_sl (i8,memory(ipnt0),neleml,error)
      if (error)  return
      call i4_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case (fmt_c4)
    if (nelem0.eq.neleml) then
      call i8toc4_sl (i8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i8toc4_sl (i8,memory(ipnt0),neleml)
      call c4_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (4)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_i81d
!
subroutine sic_descriptor_fill_r41d(desc,r4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_r41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the r4(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  real(kind=4),              intent(in)    :: r4(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r4)
    if (neleml.eq.nelem0) then
      call w4tow4_sl (r4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r4_fill(nelem0,memory(ipnt0),r4)
    endif
    !
  case (fmt_r8)
    if (neleml.eq.nelem0) then
      call r4tor8_sl (r4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r4tor8_sl (r4,memory(ipnt0),neleml)
      call r8_fill(nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case (fmt_i4)
    if (neleml.eq.nelem0) then
      call r4toi4_fini_sl (r4,memory(ipnt0),neleml,error)
      if (error)  return
    elseif (neleml.eq.1) then
      call r4toi4_fini_sl (r4,memory(ipnt0),neleml,error)
      if (error)  return
      call i4_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_i8)
    if (neleml.eq.nelem0) then
      call r4toi8_fini_sl (r4,memory(ipnt0),neleml,error)
      if (error)  return
    elseif (neleml.eq.1) then
      call r4toi8_fini_sl (r4,memory(ipnt0),neleml,error)
      if (error)  return
      call i8_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case (fmt_c4)
    if (neleml.eq.nelem0) then
      call r4toc4_sl (r4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r4toc4_sl (r4,memory(ipnt0),neleml)
      call c4_fill (nelem0,memory(ipnt0),memory(ipnt0) )
    endif
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (5)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_r41d
!
subroutine sic_descriptor_fill_r81d(desc,r8,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_r81d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the r8(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  real(kind=8),              intent(in)    :: r8(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of r8 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    if (nelem0.eq.neleml) then
      call w8tow8_sl (r8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r8_fill(nelem0,memory(ipnt0),r8)
    endif
    !
  case (fmt_r4)
    if (nelem0.eq.neleml) then
      call r8tor4_sl (r8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r8tor4_sl (r8,memory(ipnt0),neleml)
      call r4_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case (fmt_i4)
    if (nelem0.eq.neleml) then
      call r8toi4_fini_sl (r8,memory(ipnt0),neleml,error)
      if (error)  return
    elseif (neleml.eq.1) then
      call r8toi4_fini_sl (r8,memory(ipnt0),neleml,error)
      if (error)  return
      call i4_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case (fmt_i8)
    if (nelem0.eq.neleml) then
      call r8toi8_fini_sl (r8,memory(ipnt0),neleml,error)
      if (error)  return
    elseif (neleml.eq.1) then
      call r8toi8_fini_sl (r8,memory(ipnt0),neleml,error)
      if (error)  return
      call i8_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
  case (fmt_c4)
    if (nelem0.eq.neleml) then
      call r8toc4_sl (r8,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call r8toc4_sl (r8,memory(ipnt0),neleml)
      call c4_fill (nelem0,memory(ipnt0),memory(ipnt0))
    endif
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (6)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_fill_r81d
!
subroutine sic_descriptor_fill_c41d(desc,c4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_c41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the c4(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  complex(kind=4),           intent(in)    :: c4(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of c4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_c4)
    if (nelem0.eq.neleml) then
      call w8tow8_sl (c4,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call c4_fill(nelem0,memory(ipnt0),c4)
    endif
    !
  case (fmt_r4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*4')
    error = .true.
    !
  case (fmt_r8)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*8')
    error = .true.
    !
  case (fmt_i4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*4')
    error = .true.
    !
  case (fmt_i8)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*8')
    error = .true.
    !
  case default
    call sic_message(seve%e,rname,'Complex not supported (3)')
    error = .true.
    !
  end select
  !
end subroutine sic_descriptor_fill_c41d
!
subroutine sic_descriptor_fill_l1d(desc,l,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_l1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the l(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  logical,                   intent(in)    :: l(*)    ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of l values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.true.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_l)
    if (nelem0.eq.neleml) then
      call w4tow4_sl (l,memory(ipnt0),neleml)
    elseif (neleml.eq.1) then
      call i4_fill(nelem0,memory(ipnt0),l)
    endif
    !
  case default
    call sic_message(seve%e,rname,'Can not convert numeric to logical')
    error = .true.
    return
    !
  end select
  !
end subroutine sic_descriptor_fill_l1d
!
subroutine sic_descriptor_fill_ch1d(desc,ch,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_fill_ch1d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_fill
  !  Fill a descriptor target with the ch(*) values. A consistency check
  ! of the number of values is performed, and value is casted if needed
  ! to be SIC variable type if needed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  character(len=*),          intent(in)    :: ch(*)   ! Flat array of values to be copied
  integer(kind=size_length), intent(in)    :: neleml  ! Number of l values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0,ielem,addr
  integer(kind=4) :: nc
  character(len=message_length) :: mess
  !
  nelem0 = desc_nelem(desc)
  !
  if (nelem0.ne.neleml .and. neleml.ne.1) then
    write(mess,'(A,I0,1X,I0)')  &
      'Mathematics on arrays of inconsistent dimensions ',nelem0,neleml
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (desc%type.le.0) then
    call sic_message(seve%e,rname,'Can not convert numeric/logical to character')
    error = .true.
    return
  endif
  !
  nc = len(ch(1))
  addr = desc%addr
  if (nelem0.eq.neleml) then
    do ielem=1,nelem0
      call ctodes(ch(ielem),desc%type,addr)
      addr = addr+desc%type
    enddo
  elseif (neleml.eq.1) then
    do ielem=1,nelem0
      call ctodes(ch(1),desc%type,addr)
      addr = addr+desc%type
    enddo
  endif
  !
end subroutine sic_descriptor_fill_ch1d
!
!-----------------------------------------------------------------------
!
subroutine sic_descriptor_getval_i40d(desc,ielem,i4,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_i40d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target. The value is
  ! casted to I*4 if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  integer(kind=4),           intent(out)   :: i4      ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    call w4tow4_sl (memory(ipnt0),i4,one)
  case (fmt_r8)
    call r8toi4_sl (memory(ipnt0),i4,one)
  case (fmt_r4)
    call r4toi4_sl (memory(ipnt0),i4,one)
  case (fmt_i8)
    call i8toi4_sl (memory(ipnt0),i4,one)
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*4')
    error = .true.
    return
  case default
    call sic_message(seve%e,rname,'Result type mismatch (7)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_i40d
!
subroutine sic_descriptor_getval_i80d(desc,ielem,i8,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_i80d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target. The value is
  ! casted to I*8 if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  integer(kind=8),           intent(out)   :: i8      ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    call i4toi8_sl (memory(ipnt0),i8,one)
  case (fmt_r8)
    call r8toi8_fini_sl (memory(ipnt0),i8,one,error)
    if (error)  return
  case (fmt_r4)
    call r4toi8_fini_sl (memory(ipnt0),i8,one,error)
    if (error)  return
  case (fmt_i8)
    call w8tow8_sl (memory(ipnt0),i8,one)
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*8')
    error = .true.
    return
  case default
    call sic_message(seve%e,rname,'Result type mismatch (8)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_i80d
!
subroutine sic_descriptor_getval_r40d(desc,ielem,r4,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_r40d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target. The value is
  ! casted to R*4 if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  real(kind=4),              intent(out)   :: r4      ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    call r8tor4_sl (memory(ipnt0),r4,one)
  case (fmt_r4)
    call w4tow4_sl (memory(ipnt0),r4,one)
  case (fmt_i4)
    call i4tor4_sl (memory(ipnt0),r4,one)
  case (fmt_i8)
    call i8tor4_sl (memory(ipnt0),r4,one)
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*4')
    error = .true.
    return
  case default
    call sic_message(seve%e,rname,'Result type mismatch (9)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_r40d
!
subroutine sic_descriptor_getval_r80d(desc,ielem,r8,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_r80d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target. The value is
  ! casted to R*8 if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  real(kind=8),              intent(out)   :: r8      ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    call w8tow8_sl (memory(ipnt0),r8,one)
  case (fmt_r4)
    call r4tor8_sl (memory(ipnt0),r8,one)
  case (fmt_i4)
    call i4tor8_sl (memory(ipnt0),r8,one)
  case (fmt_i8)
    call i8tor8_sl (memory(ipnt0),r8,one)
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*8')
    error = .true.
    return
  case default
    call sic_message(seve%e,rname,'Result type mismatch (10)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_r80d
!
subroutine sic_descriptor_getval_c40d(desc,ielem,c4,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_c40d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target. The value is
  ! casted to C*4 if needed
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  complex(kind=4),           intent(out)   :: c4      ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    call r8toc4_sl (memory(ipnt0),c4,one)
  case (fmt_r4)
    call r4toc4_sl (memory(ipnt0),c4,one)
  case (fmt_i4)
    call i4toc4_sl (memory(ipnt0),c4,one)
  case (fmt_i8)
    call i8toc4_sl (memory(ipnt0),c4,one)
  case (fmt_c4)
    call c4toc4_sl (memory(ipnt0),c4,one)
  case default
    call sic_message(seve%e,rname,'Result type mismatch (11)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_c40d
!
subroutine sic_descriptor_getval_l0d(desc,ielem,l,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_l0d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem   ! The value position
  logical,                   intent(out)   :: l       ! Scalar value to be filled
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: ipnt0
  integer(kind=size_length), parameter :: one=1
  !
  call sic_descriptor_get1elem(desc,.false.,ielem,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_l)
    call l4tol4_sl (memory(ipnt0),l,one)
  case (fmt_r8)
    call sic_message(seve%e,rname,'Cannot convert REAL*8 to LOGICAL*4')
    error = .true.
    return
  case (fmt_r4)
    call sic_message(seve%e,rname,'Cannot convert REAL*4 to LOGICAL*4')
    error = .true.
    return
  case (fmt_i4)
    call sic_message(seve%e,rname,'Cannot convert INTEGER*4 to LOGICAL*4')
    error = .true.
    return
  case (fmt_i8)
    call sic_message(seve%e,rname,'Cannot convert INTEGER*8 to LOGICAL*4')
    error = .true.
    return
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to LOGICAL*4')
    error = .true.
    return
  case default
    call sic_message(seve%e,rname,'Result type mismatch (12)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_l0d
!
subroutine sic_descriptor_getval_ch0d(desc,ielem,ch,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_ch0d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the ielem-th value from the descriptor target.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc   ! The variable descriptor
  integer(kind=size_length), intent(in)    :: ielem  ! The value position
  character(len=*),          intent(out)   :: ch     ! Scalar value to be filled
  logical,                   intent(inout) :: error  ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=address_length) :: addr
  !
  if (ielem.le.0 .or. ielem.gt.desc_nelem(desc)) then
    call sic_message(seve%e,rname,'Out of bounds (4)')
    error = .true.
    return
  endif
  !
  if (desc%type.le.0) then
    call sic_message(seve%e,rname,'Can not convert numeric/logical to character')
    error = .true.
    return
  endif
  !
  addr = desc%addr+(ielem-1)*desc%type
  call destoc(desc%type,addr,ch)
  !
end subroutine sic_descriptor_getval_ch0d
!
subroutine sic_descriptor_getval_i4nd(desc,i4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_i4nd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i4(*) array. A
  ! consistency check of the number of values is performed, and values
  ! are casted if needed to I*4
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=4),           intent(out)   :: i4(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of i4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.false.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    if (neleml.eq.nelem0) then
      call w4tow4_sl (memory(ipnt0),i4,nelem0)
    elseif (nelem0.eq.1) then
      call i4_fill(neleml,i4,memory(ipnt0))
    endif
    !
  case (fmt_r8)
    if (neleml.eq.nelem0) then
      call r8toi4_sl (memory(ipnt0),i4,nelem0)
    elseif (nelem0.eq.1) then
      call r8toi4_sl (memory(ipnt0),i4,nelem0)  ! Convert first element
      call i4_fill(neleml,i4,i4)  ! Duplicate
    endif
    !
  case (fmt_r4)
    if (neleml.eq.nelem0) then
      call r4toi4_sl (memory(ipnt0),i4,nelem0)
    elseif (nelem0.eq.1) then
      call r4toi4_sl (memory(ipnt0),i4,nelem0)
      call i4_fill (neleml,i4,i4)
    endif
    !
  case (fmt_i8)
    if (neleml.eq.nelem0) then
      call i8toi4_sl (memory(ipnt0),i4,nelem0)
    elseif (nelem0.eq.1) then
      call i8toi4_sl (memory(ipnt0),i4,nelem0)
      call i4_fill (neleml,i4,i4)
    endif
    !
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*4')
    error = .true.
    return
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (13)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_i4nd
!
subroutine sic_descriptor_getval_i41d(desc,i4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i4(:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
  integer(kind=4),        intent(out)   :: i4(:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error  ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i4,kind=size_length)
  call sic_descriptor_getval_i4nd(desc,i4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i41d
!
subroutine sic_descriptor_getval_i42d(desc,i4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i42d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i4(:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
  integer(kind=4),        intent(out)   :: i4(:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error    ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i4,kind=size_length)
  call sic_descriptor_getval_i4nd(desc,i4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i42d
!
subroutine sic_descriptor_getval_i43d(desc,i4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i43d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i4(:,:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
  integer(kind=4),        intent(out)   :: i4(:,:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error      ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i4,kind=size_length)
  call sic_descriptor_getval_i4nd(desc,i4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i43d
!
subroutine sic_descriptor_getval_i8nd(desc,i8,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_i8nd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i8(*) array. A
  ! consistency check of the number of values is performed, and values
  ! are casted if needed to I*8
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  integer(kind=8),           intent(out)   :: i8(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of i8 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.false.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_i4)
    if (neleml.eq.nelem0) then
      call i4toi8_sl (memory(ipnt0),i8,nelem0)
    elseif (nelem0.eq.1) then
      call i4toi8_sl (memory(ipnt0),i8,nelem0)
      call i8_fill(neleml,i8,i8)
    endif
    !
  case (fmt_r8)
    if (neleml.eq.nelem0) then
      call r8toi8_fini_sl (memory(ipnt0),i8,nelem0,error)
      if (error)  return
    elseif (nelem0.eq.1) then
      call r8toi8_fini_sl (memory(ipnt0),i8,nelem0,error)  ! Convert first element
      if (error)  return
      call i8_fill(neleml,i8,i8)  ! Duplicate
    endif
    !
  case (fmt_r4)
    if (neleml.eq.nelem0) then
      call r4toi8_fini_sl (memory(ipnt0),i8,nelem0,error)
      if (error)  return
    elseif (nelem0.eq.1) then
      call r4toi8_fini_sl (memory(ipnt0),i8,nelem0,error)
      if (error)  return
      call i8_fill (neleml,i8,i8)
    endif
    !
  case (fmt_i8)
    if (neleml.eq.nelem0) then
      call w8tow8_sl (memory(ipnt0),i8,nelem0)
    elseif (nelem0.eq.1) then
      call i8_fill (neleml,i8,memory(ipnt0))
    endif
    !
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to INTEGER*8')
    error = .true.
    return
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (14)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_i8nd
!
subroutine sic_descriptor_getval_i81d(desc,i8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i81d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i8(:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
  integer(kind=8),        intent(out)   :: i8(:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error  ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i8,kind=size_length)
  call sic_descriptor_getval_i8nd(desc,i8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i81d
!
subroutine sic_descriptor_getval_i82d(desc,i8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i82d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i8(:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
  integer(kind=8),        intent(out)   :: i8(:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error    ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i8,kind=size_length)
  call sic_descriptor_getval_i8nd(desc,i8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i82d
!
subroutine sic_descriptor_getval_i83d(desc,i8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_i83d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the i8(:,:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
  integer(kind=8),        intent(out)   :: i8(:,:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error      ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(i8,kind=size_length)
  call sic_descriptor_getval_i8nd(desc,i8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_i83d
!
subroutine sic_descriptor_getval_r4nd(desc,r4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_r4nd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r4(*) array. A
  ! consistency check of the number of values is performed, and values
  ! are casted if needed to R*4
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  real(kind=4),              intent(out)   :: r4(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.false.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r4)
    if (nelem0.eq.neleml) then
      call w4tow4_sl (memory(ipnt0),r4,nelem0)
    elseif (nelem0.eq.1) then
      call r4_fill(neleml,r4,memory(ipnt0))
    endif
    !
  case (fmt_r8)
    if (nelem0.eq.neleml) then
      call r8tor4_sl (memory(ipnt0),r4,nelem0)
    elseif (nelem0.eq.1) then
      call r8tor4_sl (memory(ipnt0),r4,nelem0)
      call r4_fill (neleml,r4,r4)
    endif
    !
  case (fmt_i4)
    if (nelem0.eq.neleml) then
      call i4tor4_sl (memory(ipnt0),r4,nelem0)
    elseif (nelem0.eq.1) then
      call i4tor4_sl (memory(ipnt0),r4,nelem0)
      call r4_fill (neleml,r4,r4)
    endif
    !
  case (fmt_i8)
    if (nelem0.eq.neleml) then
      call i8tor4_sl (memory(ipnt0),r4,nelem0)
    elseif (nelem0.eq.1) then
      call i8tor4_sl (memory(ipnt0),r4,nelem0)
      call r4_fill (neleml,r4,r4)
    endif
    !
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*4')
    error = .true.
    return
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (15)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_r4nd
!
subroutine sic_descriptor_getval_r41d(desc,r4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r4(:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
  real(kind=4),           intent(out)   :: r4(:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error  ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r4,kind=size_length)
  call sic_descriptor_getval_r4nd(desc,r4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r41d
!
subroutine sic_descriptor_getval_r42d(desc,r4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r42d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r4(:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
  real(kind=4),           intent(out)   :: r4(:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error    ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r4,kind=size_length)
  call sic_descriptor_getval_r4nd(desc,r4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r42d
!
subroutine sic_descriptor_getval_r43d(desc,r4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r43d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r4(:,:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
  real(kind=4),           intent(out)   :: r4(:,:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error      ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r4,kind=size_length)
  call sic_descriptor_getval_r4nd(desc,r4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r43d
!
subroutine sic_descriptor_getval_r8nd(desc,r8,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_r8nd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r8(*) array. A
  ! consistency check of the number of values is performed, and values
  ! are casted if needed to R*8
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  real(kind=8),              intent(out)   :: r8(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of r8 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.false.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r8)
    if (nelem0.eq.neleml) then
      call w8tow8_sl (memory(ipnt0),r8,nelem0)
    elseif (nelem0.eq.1) then
      call r8_fill(neleml,r8,memory(ipnt0))
    endif
    !
  case (fmt_r4)
    if (nelem0.eq.neleml) then
      call r4tor8_sl (memory(ipnt0),r8,nelem0)
    elseif (nelem0.eq.1) then
      call r4tor8_sl (memory(ipnt0),r8,nelem0)
      call r8_fill (neleml,r8,r8)
    endif
    !
  case (fmt_i4)
    if (nelem0.eq.neleml) then
      call i4tor8_sl (memory(ipnt0),r8,nelem0)
    elseif (nelem0.eq.1) then
      call i4tor8_sl (memory(ipnt0),r8,nelem0)
      call r8_fill (neleml,r8,r8)
    endif
    !
  case (fmt_i8)
    if (nelem0.eq.neleml) then
      call i8tor8_sl (memory(ipnt0),r8,nelem0)
    elseif (nelem0.eq.1) then
      call i8tor8_sl (memory(ipnt0),r8,nelem0)
      call r8_fill (neleml,r8,r8)
    endif
    !
  case (fmt_c4)
    call sic_message(seve%e,rname,'Cannot convert COMPLEX*4 to REAL*8')
    error = .true.
    return
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (16)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_r8nd
!
subroutine sic_descriptor_getval_r81d(desc,r8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r81d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r8(:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
  real(kind=8),           intent(out)   :: r8(:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error  ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r8,kind=size_length)
  call sic_descriptor_getval_r8nd(desc,r8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r81d
!
subroutine sic_descriptor_getval_r82d(desc,r8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r82d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r8(:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
  real(kind=8),           intent(out)   :: r8(:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error    ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r8,kind=size_length)
  call sic_descriptor_getval_r8nd(desc,r8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r82d
!
subroutine sic_descriptor_getval_r83d(desc,r8,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_r83d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the r8(:,:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
  real(kind=8),           intent(out)   :: r8(:,:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error      ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(r8,kind=size_length)
  call sic_descriptor_getval_r8nd(desc,r8,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_r83d
!
subroutine sic_descriptor_getval_c4nd(desc,c4,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_c4nd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the c4(*) array. A
  ! consistency check of the number of values is performed, and values
  ! are casted if needed to R*4
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  complex(kind=4),           intent(out)   :: c4(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of c4 values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0
  integer(kind=address_length) :: ipnt0
  !
  ! Get appropriate and perform consistency check:
  call sic_descriptor_getnelem(desc,neleml,.false.,nelem0,ipnt0,error)
  if (error)  return
  !
  select case (desc%type)
  case (fmt_r4)
    if (nelem0.eq.neleml) then
      call r4toc4_sl (memory(ipnt0),c4,nelem0)
    elseif (nelem0.eq.1) then
      call r4toc4_sl (memory(ipnt0),c4,nelem0)
      call c4_fill(neleml,c4,c4)
    endif
    !
  case (fmt_r8)
    if (nelem0.eq.neleml) then
      call r8toc4_sl (memory(ipnt0),c4,nelem0)
    elseif (nelem0.eq.1) then
      call r8toc4_sl (memory(ipnt0),c4,nelem0)
      call c4_fill (neleml,c4,c4)
    endif
    !
  case (fmt_i4)
    if (nelem0.eq.neleml) then
      call i4toc4_sl (memory(ipnt0),c4,nelem0)
    elseif (nelem0.eq.1) then
      call i4toc4_sl (memory(ipnt0),c4,nelem0)
      call c4_fill (neleml,c4,c4)
    endif
    !
  case (fmt_i8)
    if (nelem0.eq.neleml) then
      call i8toc4_sl (memory(ipnt0),c4,nelem0)
    elseif (nelem0.eq.1) then
      call i8toc4_sl (memory(ipnt0),c4,nelem0)
      call c4_fill (neleml,c4,c4)
    endif
    !
  case (fmt_c4)
    if (nelem0.eq.neleml) then
      call c4toc4_sl (memory(ipnt0),c4,nelem0)
    elseif (nelem0.eq.1) then
      call c4_fill(neleml,c4,memory(ipnt0))
    endif
    !
  case default
    call sic_message(seve%e,rname,'Result type mismatch (17)')
    error = .true.
    return
  end select
  !
end subroutine sic_descriptor_getval_c4nd
!
subroutine sic_descriptor_getval_c41d(desc,c4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_c41d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the c4(:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
  complex(kind=4),        intent(out)   :: c4(:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error  ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(c4,kind=size_length)
  call sic_descriptor_getval_c4nd(desc,c4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_c41d
!
subroutine sic_descriptor_getval_c42d(desc,c4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_c42d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the c4(:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
  complex(kind=4),        intent(out)   :: c4(:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error    ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(c4,kind=size_length)
  call sic_descriptor_getval_c4nd(desc,c4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_c42d
!
subroutine sic_descriptor_getval_c43d(desc,c4,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_descriptor_getval_c43d
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the c4(:,:,:) array.
  !---------------------------------------------------------------------
  type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
  complex(kind=4),        intent(out)   :: c4(:,:,:)  ! Flat array of values to be filled
  logical,                intent(inout) :: error      ! Logical error flag
  !
  integer(kind=size_length) :: nelem
  !
  nelem = size(c4,kind=size_length)
  call sic_descriptor_getval_c4nd(desc,c4,nelem,error)
  if (error)  return
end subroutine sic_descriptor_getval_c43d
!
subroutine sic_descriptor_getval_chnd(desc,ch,neleml,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_descriptor_getval_chnd
  use sic_types
  !---------------------------------------------------------------------
  ! @ public-generic sic_descriptor_getval
  !  Get the descriptor target values into the c*(*) array. A
  ! consistency check of the number of values is performed.
  !---------------------------------------------------------------------
  type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
  character(len=*),          intent(out)   :: ch(*)   ! Flat array of values to be filled
  integer(kind=size_length), intent(in)    :: neleml  ! Number of ch values
  logical,                   intent(inout) :: error   ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='MTH'
  integer(kind=size_length) :: nelem0,ielem,addr
  integer(kind=4) :: nc
  character(len=message_length) :: mess
  !
  nelem0 = desc_nelem(desc)
  !
  if (nelem0.ne.neleml .and. nelem0.ne.1) then
    write(mess,'(A,I0,1X,I0)')  &
      'Mathematics on arrays of inconsistent dimensions ',nelem0,neleml
    call sic_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (desc%type.le.0) then
    call sic_message(seve%e,rname,'Can not convert numeric/logical to character')
    error = .true.
    return
  endif
  !
  nc = len(ch(1))
  addr = desc%addr
  if (nelem0.eq.neleml) then
    do ielem=1,nelem0
      call destoc(desc%type,addr,ch(ielem))
      addr = addr+desc%type
    enddo
  elseif (nelem0.eq.1) then
    do ielem=1,neleml
      call destoc(desc%type,addr,ch(ielem))
    enddo
  endif
  !
end subroutine sic_descriptor_getval_chnd
