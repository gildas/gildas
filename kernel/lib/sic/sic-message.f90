!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage SIC messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module sic_message_private
  use gpack_def
  !---------------------------------------------------------------------
  ! @private
  ! Identifier used for message identification
  !---------------------------------------------------------------------
  integer :: sic_message_id = gpack_global_id  ! Default@startup
  !
end module sic_message_private
!
subroutine sic_message_set_id(id)
  use sic_interfaces, except_this=>sic_message_set_id
  use sic_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  sic_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',sic_message_id
  call sic_message(seve%d,'sic_message_set_id',mess)
  !
end subroutine sic_message_set_id
!
subroutine sic_message(mkind,procname,message)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_message
  use sic_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @private
  ! Messaging facility for the current library. Calls the low-level
  ! messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(sic_message_id,mkind,procname,message)
  !
end subroutine sic_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
