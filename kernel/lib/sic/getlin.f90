subroutine find_edit
  use sic_interfaces, except_this=>find_edit
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !       Determines if editing is possible on this device
  !---------------------------------------------------------------------
  edit_mode = inter_state
  !
end subroutine find_edit
!
function history_old_behaviour(dummy)
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  logical :: history_old_behaviour  !
  integer :: dummy                  !
  ! Local
  integer(kind=4) :: ier
  !
  history_old_behaviour = .false.
  ier = sic_getlog('HISTORY_OLD_BEHAVIOUR',history_old_behaviour)
  !
end function history_old_behaviour
!
function sub_kbdline(pr,chain,code,command)
  use sic_dependencies_interfaces
  use sic_interactions
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private
  !  Read one command line from terminal. Lines from stack buffer can
  ! be recovered if ^P or ^N or UP and DOWN arrow are pressed. This
  ! routine is only called when line editing is enabled. COMMAND is
  ! true if waiting for a command.
  !---------------------------------------------------------------------
  integer(kind=4) :: sub_kbdline  ! Function value on return
  character(len=*) :: pr            !
  character(len=*) :: chain         !
  integer :: code                   !
  integer :: command                !
  !
  if (sic_window) then
    sub_kbdline = read_linec(pr,chain,code,command)
  else
    sub_kbdline = kbd_line(pr,chain,code)
  endif
end function sub_kbdline
!
subroutine read_line(chain,nchain,invite,ninv)
  use sic_interfaces, except_this=>read_line
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !     Read one command line from terminal. Lines from stack buffer
  !     can be recovered if ^P or ^N or UP and DOWN arrow are pressed.
  !     This routine is only called when line editing is enabled.
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  integer :: nchain                 !
  character(len=*) :: invite        !
  integer :: ninv                   !
  ! Local
  integer :: code
  logical :: error
  character(len=64) :: pr
  integer :: curr_line
  !
  error = .false.
  curr_line = iend+1
  pr = invite(1:ninv)//char(0)
  code = 0
  !
  nchain = sub_kbdline(pr,chain,code,1)
  do while (code.ne.0)
    if (code.eq.-1) then
      if (curr_line.gt.1) curr_line = curr_line-1
    else
      curr_line = curr_line+1
    endif
    call sic_recall(chain,nchain,curr_line,error)
    chain(nchain+1:nchain+1) = char(0)
    code = -1
    nchain = sub_kbdline (pr,chain,code,1)
  enddo
  chain(nchain+1:nchain+1) = ' '
end subroutine read_line
!
subroutine get_line(chain,nchain,invite,ninv)
  use sic_interfaces, except_this=>get_line
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC
  !     Get line with prompt and editing.
  !     Lines from the stack cannot be recovered, but the original
  !     line can be restored when UP and DOWN arrows are pressed
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  integer :: nchain                 !
  character(len=*) :: invite        !
  integer :: ninv                   !
  ! Local
  integer :: code,mpr
  character(len=64) :: pr
  !
  mpr = min(ninv,len(pr)-1)
  pr = invite(1:mpr)//char(0)
  code = 2
  chain = char(0)
  nchain = sub_kbdline(pr,chain,code,0)
  do while (code.ne.0)
    nchain = sub_kbdline(pr,chain,code,0)
  enddo
  chain(nchain+1:nchain+1) = ' '
end subroutine get_line
!
subroutine edit_line(chain,nchain,invite,ninv)
  use sic_interfaces, except_this=>edit_line
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! SIC
  !     Get line with prompt and editing.
  !     Lines from the stack cannot be recovered, but the original
  !     line can be restored when UP and DOWN arrows are pressed
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  integer :: nchain                 !
  character(len=*) :: invite        !
  integer :: ninv                   !
  ! Local
  integer :: code
  character(len=64) :: pr
  integer :: curr_line
  logical :: error
  !
  error = .false.
  curr_line = iend+1
  !
  ! Write prompt in reverse video. Assumes ANSI device...
  pr = invite(1:ninv-1)//' '//char(0)
  chain(nchain+1:nchain+1) = char(0)
  code = 1
  nchain = sub_kbdline(pr,chain,code,1)
  do while (code.ne.0)
    if (code.eq.-1) then
      if (curr_line.gt.1) curr_line = curr_line-1
    else
      curr_line = curr_line+1
    endif
    call sic_recall(chain,nchain,curr_line,error)
    chain(nchain+1:nchain+1) = char(0)
    code = -1
    nchain = sub_kbdline(pr,chain,code,1)
  enddo
  chain(nchain+1:nchain+1) = ' '
end subroutine edit_line
!
subroutine sic_edit(chain,nchain,invite,ninv)
  use sic_interfaces, except_this=>sic_edit
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*) :: chain         !
  integer :: nchain                 !
  character(len=*) :: invite        !
  integer :: ninv                   !
  ! Local
  integer :: code
  character(len=64) :: pr
  !
  pr = invite(1:ninv)//char(0)
  code = 0
  nchain = sub_kbdline(pr,chain,code,0)
  do while (code.ne.0)
    nchain = sub_kbdline(pr,chain,code,0)
  enddo
  chain(nchain+1:nchain+1) = ' '
end subroutine sic_edit
