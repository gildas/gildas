subroutine sic_accept(line,error)
  use gildas_def
  use gbl_message
  use gbl_format
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! SIC Support routine for command
  !   ACCEPT Var1 [Var2 [...]]
  !    3     [/COLUMN Filename [Separ]]
  !    4     [/FORMAT Form]
  !    5     [/LINE l1 l2]
  ! or
  !   ACCEPT Var1
  !    1 [/ARRAY  Filename]
  !    4 [/FORMAT Form]
  !    5 [/LINE l1 l2]
  ! or
  !   ACCEPT Var1
  !    2 [/BINARY Filename Offset]
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   !
  logical,          intent(out) :: error  !
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  integer(kind=4):: line1,line2,lun,iopt,ier,nc,nbyt
  integer(kind=index_length) :: i
  character(len=filename_length) :: datafile,chain
  !
  ! File name
  if (sic_present(1,0)) then
    ! /ARRAY option
    iopt = 1
    if (sic_present(3,0)) then
      call sic_message(seve%e,rname,'Incompatible options /ARRAY and /COLUMN')
      error = .true.
      return
    elseif (sic_present(2,0)) then
      call sic_message(seve%e,rname,'Incompatible options /ARRAY and /BINARY')
      error = .true.
      return
    endif
  elseif (sic_present(2,0)) then
    ! /BINARY
    if (sic_present(3,0)) then
      call sic_message(seve%e,rname,'Incompatible options /BINARY and /COLUMN')
      error = .true.
      return
    else
      if (sic_present(4,0))  &
        call sic_message(seve%w,rname,'/FORMAT Option ignored with /BINARY')
      if (sic_present(5,0))  &
        call sic_message(seve%w,rname,'/LINE Option ignored with /BINARY')
    endif
    iopt = 2
  elseif (sic_present(3,0)) then
    ! /COLUMN
    iopt = 3
  else
    call sic_message(seve%e,rname,'Missing option /ARRAY, /BINARY or /COLUMN')
    error = .true.
    return
  endif
  call sic_ch (line,iopt,1,chain,nc,.true.,error)
  if (error) return
  call sic_parse_file (chain,' ','.dat',datafile)
  !
  ! Binary case
  if (iopt.eq.2) then
    ! Use Portable Code ?
    call gag_fillook (lun,datafile)
    if (lun.eq.-1) then
      error = .true.
      return
    endif
    nbyt = 0
    call sic_i4 (line,iopt,2,nbyt,.false.,error)
    if (error) goto 99
    if (nbyt.gt.0) then
      ier = gag_filseek (lun,nbyt)
      if (ier.eq.-1) then
        call gag_filclose (lun)
        call sic_message(seve%e,rname,'Skip error on binary file')
        error = .true.
        return
      endif
    endif
  else
    !
    ! Other cases
    ier = sic_getlun(lun)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    ier = sic_open(lun,datafile,'OLD',.true.)
    if (ier.ne.0) then
      call sic_message(seve%e,rname,'Error opening '//datafile)
      call putios('E-ACCEPT,  ',ier)
      close (unit=lun)
      call sic_frelun (lun)
      error = .true.
      return
    endif
    !
    ! Set up Lines
    error = .true.
    line1 = 1
    line2 = 0
    if (sic_present(5,0)) then
      call sic_i4 (line,5,1,line1,.false.,error)
      if (error) goto 99
      call sic_i4 (line,5,2,line2,.false.,error)
      if (error) goto 99
    endif
    if (line2.ne.0) line2 = line2-line1+1
    !
    ! Skip beginning of file
    do i=1,line1-1
      read(lun,'(A)',iostat=ier)
      if (ier.ne.0) then
        call sic_message(seve%e,rname,'Error skipping lines')
        call putios('E-ACCEPT,  ',ier)
        goto 99
      endif
    enddo
  endif
  !
  ! Now do the work
  select case (iopt)
  case (1)
    call sic_accept_array(line,lun,error)
  case (2)
    call sic_accept_binary(line,lun,error)
  case (3)
    call sic_accept_column(line,lun,line1,line2,error)
  end select
  if (error)  continue  ! Need to close the files anyway
  !
99 continue
  if (iopt.eq.2) then
    call gag_filclose(lun)
  else
    close (unit=lun)
    call sic_frelun(lun)
  endif
  return
end subroutine sic_accept
!
subroutine sic_accept_array(line,lun,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_array
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ACCEPT /ARRAY
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line      ! Input command line
  integer(kind=4),  intent(in)    :: lun       !
  logical,          intent(inout) :: error     ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  character(len=128) :: format
  integer(kind=4) :: ier,type,nf
  integer(kind=address_length) :: ip
  integer(kind=size_length) :: size
  character(len=8196) :: local_line
  logical :: opt_format
  !
  if (sic_narg(0).ne.1) then
    call sic_message(seve%e,rname,'Only one variable accepted with /ARRAY')
    error = .true.
    return
  endif
  !
  call sic_accept_getvar(line,1,sic_maxdims,type,ip,size,error)
  if (error)  return
  !
  call sic_accept_getformat(line,opt_format,format,nf,error)
  if (error)  return
  !
  if (type.eq.fmt_r4) then
    call read_one_r4 (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.eq.fmt_r8) then
    call read_one_r8 (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.eq.fmt_i4) then
    call read_one_i4 (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.eq.fmt_i8) then
    call read_one_i8 (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.eq.fmt_i2) then
    call read_one_i2 (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.eq.fmt_by) then
    call read_one_by (lun,opt_format,format(1:nf),ier,memory(ip),size)
  elseif (type.gt.0) then   ! write array (RO case rejected)
    size = type*size  ! Total number of characters to read
    ! Normally, Nobody will find the correct format, since the character
    ! array appears as one single line. ah well.
    call read_one_ch (lun,opt_format,format(1:nf),ier,local_line,size)
    call chtoby(local_line,membyt(ip),size)
  else
    call sic_message(seve%e,rname,'Unsupported variable type')
  endif
  !
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Error reading file')
    call putios('E-ACCEPT,  ',ier)
    error = .true.
  endif
  !
end subroutine sic_accept_array
!
subroutine sic_accept_binary(line,lun,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_binary
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ACCEPT /BINARY
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line      ! Input command line
  integer(kind=4),  intent(in)    :: lun       !
  logical,          intent(inout) :: error     ! Logical error flag
  ! Global
  integer(kind=4) :: gag_filread
  include 'gbl_memory.inc'
  ! Local
  character(len=8), parameter :: rname='ACCEPT'
  integer(kind=4) :: type,nbytin,nbytou
  integer(kind=address_length) :: ip
  integer(kind=size_length) :: size
  !
  if (sic_narg(0).ne.1) then
    call sic_message(seve%e,rname,'Only one variable accepted with /BINARY')
    error = .true.
    return
  endif
  !
  call sic_accept_getvar(line,1,sic_maxdims,type,ip,size,error)
  if (error)  return
  !
  ! The following works for Character Arrays...
  if (type.eq.fmt_r8 .or. type.eq.fmt_i8) then
    nbytin = 8*size
  elseif (type.eq.fmt_i2) then
    nbytin = 2*size
  elseif ((type.ne.fmt_by).and.(type.lt.0)) then
    nbytin = 4*size
  elseif (type.gt.0) then
    nbytin = type*size  ! Total number of characters==bytes in the array
  endif
  if (type.gt.0) then
    nbytou = gag_filread(lun,membyt(ip),nbytin)
  else
    nbytou = gag_filread(lun,memory(ip),nbytin)
  endif
  if (nbytou.ne.nbytin) then
    call sic_message(seve%e,rname,'End of file during read')
    error = .true.
  endif
  !
end subroutine sic_accept_binary
!
subroutine sic_accept_column(line,lun,line1,line2,error)
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_column
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ACCEPT /COLUMN
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line    ! Input command line
  integer(kind=4),  intent(in)    :: lun     ! Logical unit
  integer(kind=4),  intent(in)    :: line1   !
  integer(kind=4),  intent(in)    :: line2   !
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=8), parameter :: rname='ACCEPT'
  integer(kind=4), parameter :: marg=99
  integer(kind=4) :: fmt(marg),iarg,narg
  integer(kind=address_length) :: ip(marg)
  integer(kind=size_length) :: is(marg)
  !
  ! More than 1 variable : column like format
  narg = sic_narg(0)
  do iarg=1,narg
    call sic_accept_getvar(line,iarg,1,fmt(iarg),ip(iarg),is(iarg),error)
    if (error)  return
  enddo  ! iarg
  !
  ! Either we have a Format list, in which case we use the old, hard way to
  ! read variables, we cannot read more than 1 char array, etc..
  if (sic_present(4,0)) then  ! /FORMAT
    call sic_accept_column_format(line,lun,line1,narg,fmt,ip,is,error)
  else
    call sic_accept_column_noformat(line,lun,line1,line2,narg,fmt,ip,is,error)
  endif
  if (error)  return
  !
end subroutine sic_accept_column
!
subroutine sic_accept_column_noformat(line,lun,line1,line2,narg,fmt,ip,is,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_column_noformat
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ACCEPT /COLUMN (no /FORMAT)
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: line       ! Input command line
  integer(kind=4),              intent(in)    :: lun        !
  integer(kind=4),              intent(in)    :: line1      !
  integer(kind=4),              intent(in)    :: line2      !
  integer(kind=4),              intent(in)    :: narg       !
  integer(kind=4),              intent(in)    :: fmt(narg)  !
  integer(kind=address_length), intent(in)    :: ip(narg)   !
  integer(kind=size_length),    intent(in)    :: is(narg)   !
  logical,                      intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  integer(kind=4) :: j,ier,ilin,nl,ns
  character(len=8196) :: local_line
  integer(kind=address_length) :: val(narg)
  integer(kind=4), parameter :: dummy=1  ! Value must conform with subroutine sic_accept_getvar
  character(len=1) :: separ
  integer(kind=size_length) :: iwritten,maxsize
  character(len=80) :: errchain
  !
  separ = char(0)
  call sic_ch(line,3,2,separ,ns,.false.,error)
  if (error)  return
  !
  ! Here we do not have a format, and we read user-separated columns.
  val(1:narg) = ip(1:narg)  ! Initialise pointer list (modified on the fly hereafter)
  ier = 0
  ilin = 0
  iwritten = 0
  !
  ! Read lines, decode and update pointer list
  maxsize = maxval(is(1:narg))
  do while (iwritten.lt.maxsize)
    ilin = ilin+1
    read (lun,'(A)',end=98,iostat=ier) local_line
    if (ier.ne.0)  goto 98
    nl=len_trim(local_line)
    if (local_line(1:1).eq.'!')  &  ! Ignore comments
      cycle
    call sic_blanc(local_line,nl)   ! Suppress everything after a '!'
    if (nl.eq.0)  &                 ! Ignore empty lines after SIC_BLANC
      cycle
    !
    call sic_accept_oneline(separ,local_line,narg,fmt,val,error)
    if (error)  goto 98
    iwritten = iwritten+1
    !
    do j=1,narg
      if (iwritten.ge.is(j)) then
        val(j) = dummy  ! No more to be written!
      elseif (fmt(j).gt.0) then
        val(j)=val(j)+gag_sizeof(fmt(j))  ! Offset in memory in chars
      else
        val(j)=val(j)+gag_sizeof(fmt(j))/gag_sizeof(fmt_i4)  ! Offset in memory in I4
      endif
    enddo
    if (line2.ne.0 .and. ilin.ge.line2) exit ! Enough lines read
  enddo  ! while (iwritten.le.maxsize)
  !
98 continue
  if (error .or. ier.ne.0) then
    if (ilin.gt.0) then
      write (errchain,'(A,I0)') 'Error During Read line #',ilin+line1-1
      call sic_message(seve%e,rname,errchain)
    else
      call sic_message(seve%e,rname,'Error reading file')
    endif
    if (ier.ne.0)  call putios('E-ACCEPT,  ',ier)
    error = .true.
  endif
  !
end subroutine sic_accept_column_noformat
!
subroutine sic_accept_column_format(line,lun,line1,narg,fmt,ip,is,error)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_column_format
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ACCEPT /COLUMN /FORMAT
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: line       ! Input command line
  integer(kind=4),              intent(in)    :: lun        !
  integer(kind=4),              intent(in)    :: line1      !
  integer(kind=4),              intent(in)    :: narg       !
  integer(kind=4),              intent(in)    :: fmt(narg)  !
  integer(kind=address_length), intent(inout) :: ip(narg)   !
  integer(kind=size_length),    intent(in)    :: is(narg)   !
  logical,                      intent(inout) :: error      ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  integer(kind=size_length) :: maxsize
  integer(kind=4) :: ier,ilin,nf
  integer(kind=size_length) :: i,size
  character(len=8196) :: local_line
  real(kind=8) :: work(narg)
  integer(kind=address_length) :: ipline
  character(len=80) :: errchain
  character(len=128) :: format
  logical :: opt_format
  !
  maxsize = maxval(is(1:narg))
  !
  ! Here variables must be scalar or 1D of the same size.
  if (any(is(1:narg).ne.maxsize .and. is(1:narg).gt.1)) then
    call sic_message(seve%e,rname,'Variables in list have different sizes')
    error = .true.
    return
  endif
  !
  call sic_accept_getformat(line,opt_format,format,nf,error)
  if (error)  return
  !
  if (any(fmt(1:narg).gt.0)) then  ! At least 1 character variable is present
    !
    if (any(fmt(1:narg).le.0)) then  ! ... and a numeric also. Rejected.
      call sic_message(seve%e,rname,  &
        'Mixing Character and Numeric variables in a FORMAT-specified way is NOT available')
      call sic_message(seve%e,rname,'Try to read variables separately')
      error = .true.
      return
    endif
    !
    ! Do not yet read several char arrays...
    if (narg.gt.1) then
      call sic_message(seve%e,rname,'Only One (1) Character variable '//  &
        'may be read by this command if a FORMAT option is present')
      call sic_message(seve%e,rname,'Try to read variables separately')
      error = .true.
      return
    endif
    ipline = bytpnt(locstr(local_line),membyt)
    !     the following is true IF one char array only
    size = fmt(1)
    do i=1,maxsize
      call read_one_ch(lun,opt_format,format(1:nf),ier,local_line,size)
      if (ier.ne.0) then
        ilin = i
        goto 98
      endif
      call bytoby(membyt(ipline),membyt(ip(1)),fmt(1))
      ip(1) = ip(1)+fmt(1)
    enddo
  else
    if (error)  return
    call read_all_var(lun,opt_format,format(1:nf),ier,ilin,ip,is,fmt,maxsize,  &
      narg,memory,work,error)
    if (error)  return
  endif
  !
98 continue
  if (ier.ne.0) then
    if (ilin.gt.0) then
      write (errchain,'(A,I8)') 'Error During Read line #',ilin+line1-1
      call sic_message(seve%e,rname,errchain)
    else
      call sic_message(seve%e,rname,'Error reading file')
    endif
    call putios('E-ACCEPT,  ',ier)
    error = .true.
  else
    error = .false.
  endif
end subroutine sic_accept_column_format
!
subroutine sic_accept_getvar(line,iarg,maxdims,type,ip,siz,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_getvar
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Return the some elements describing the variable, and make some
  ! checks
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: line     ! Input command line
  integer(kind=4),              intent(in)    :: iarg     ! Argument position
  integer(kind=4),              intent(in)    :: maxdims  ! Maximum dimension allowed
  integer(kind=4),              intent(out)   :: type     ! Variable type
  integer(kind=address_length), intent(out)   :: ip       ! Data address
  integer(kind=size_length),    intent(out)   :: siz      ! Number of elements
  logical,                      intent(inout) :: error    ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  character(len=80) :: name
  integer(kind=4) :: nc
  type(sic_descriptor_t) :: desc
  logical :: found
  integer(kind=4), parameter :: dummy=1
  character(len=message_length) :: mess
  !
  call sic_ke(line,0,iarg,name,nc,.true.,error)
  if (error)  return
  !
  if (name.eq.'*') then
    type = fmt_r4
    ip = dummy  ! Set relative offset to 1 (memory(1))
    siz = 1     ! At most one word
    return
  endif
  !
  found = .true.
  call sic_descriptor(name,desc,found)
  !
  if (.not.found) then
    call sic_message(seve%e,rname,'No such variable '//name)
    error = .true.
  elseif (desc%readonly) then
    call sic_message(seve%e,rname,'ReadOnly variable cannot be read')
    error = .true.
  elseif (desc%ndim.gt.maxdims) then
    write(mess,'(A,I0,A,A)')  'Variable must be at most ',maxdims,'-D: ',name
    call sic_message(seve%e,rname,mess)
    error = .true.
  endif
  if (error)  return
  !
  type = desc%type
  !
  siz = desc_nelem(desc)
  !
  if (type.gt.0) then
    ip = bytpnt(desc%addr,membyt)
  else
    ip = gag_pointer(desc%addr,memory)
  endif
  !
end subroutine sic_accept_getvar
!
subroutine sic_accept_getformat(line,opt_form,format,nf,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_accept_getformat
  !---------------------------------------------------------------------
  ! @ private
  ! Parse the command line for the /FORMAT arguments
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line       ! Input command line
  logical,          intent(out)   :: opt_form   ! /FORMAT is present?
  character(len=*), intent(out)   :: format     ! Argument
  integer(kind=4),  intent(out)   :: nf         ! Length of format
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  character(len=128) :: chain
  integer(kind=4) :: nc
  !
  opt_form = sic_present(4,0)
  if (opt_form) then
    if (sic_narg(4).gt.1) then
      call sic_message(seve%e,rname,'Invalid /FORMAT argument')
      call sic_message(seve%e,rname,'Format must be a single string e.g. "F8.2" or "F5.3,5(2X,I4)"')
      error = .true.
      return
    endif
    !
    call sic_ch(line,4,1,chain,nc,.true.,error)
    if (error)  return
    if (chain(1:1).ne.'(') then
      format = '('//trim(chain)//')'
    else
      format = chain
    endif
  else
    format = ''  ! or '*' maybe?
  endif
  nf = len_trim(format)
  !
end subroutine sic_accept_getformat
!
subroutine read_one_r4(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  real(kind=4),              intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_r4
!
subroutine read_one_r8(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  real(kind=8),              intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_r8
!
subroutine read_one_i4(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  integer(kind=4),           intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_i4
!
subroutine read_one_i8(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  integer(kind=8),           intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_i8
!
subroutine read_one_i2(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  integer(kind=2),           intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_i2
!
subroutine read_one_by(lun,form,format,ier,array,size)
  use sic_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun          !
  logical,                   intent(in)  :: form         !
  character(len=*),          intent(in)  :: format       !
  integer(kind=4),           intent(out) :: ier          !
  integer(kind=size_length), intent(in)  :: size         !
  integer(kind=1),           intent(out) :: array(size)  !
  !
  if (form) then
    read(lun,format,iostat=ier) array
  else
    read(lun,*,iostat=ier) array
  endif
end subroutine read_one_by
!
subroutine read_one_ch(lun,form,format,ier,array,size)
  use gildas_def
  use sic_interfaces, except_this=>read_one_ch
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)  :: lun     !
  logical,                   intent(in)  :: form    !
  character(len=*),          intent(in)  :: format  !
  integer(kind=4),           intent(out) :: ier     !
  character(len=*),          intent(out) :: array   !
  integer(kind=size_length), intent(in)  :: size    !
  !
  if (form) then
    read(lun,format,iostat=ier) array(1:size)
  else
    read(lun,*,iostat=ier) array(1:size)
  endif
end subroutine read_one_ch
!
subroutine read_all_var(lun,form,format,ier,ilin,ip,is,fmt,size,narg,memory,  &
  work,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use sic_interfaces, except_this=>read_all_var
  !---------------------------------------------------------------------
  ! @ private
  ! This assumes that any kind of values can be stored in double
  ! precision floats, which is false for long integers.
  !---------------------------------------------------------------------
  integer(kind=4),              intent(in)    :: lun         !
  logical,                      intent(in)    :: form        !
  character(len=*),             intent(in)    :: format      !
  integer(kind=4),              intent(out)   :: ier         !
  integer(kind=4),              intent(out)   :: ilin        !
  integer(kind=4),              intent(in)    :: narg        !
  integer(kind=address_length), intent(inout) :: ip(narg)    !
  integer(kind=size_length),    intent(in)    :: is(narg)    !
  integer(kind=4),              intent(in)    :: fmt(narg)   !
  integer(kind=size_length),    intent(in)    :: size        !
  integer(kind=4)                             :: memory(*)   !
  real(kind=8)                                :: work(narg)  ! Working array
  logical,                      intent(inout) :: error       !
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  integer(kind=size_length) :: i
  integer(kind=4) :: j,inte,ipos,ichara
  logical :: yes,no
  data yes/.true./, no/.false./
  !
  if (form) then
    ! The Fortran read() statement below will read the string into the real
    ! 'work' array. The standard forbids reading strings into floats using
    ! integer edit descriptors (gfortran raises an error, ifort returns
    ! incorrect value). Trap this:
    ipos = index(format,'I')
    if (ipos.gt.0) then
      ! Format contains a I edit descriptor. Check a bit more:
      ichara = ichar(format(ipos+1:ipos+1))-ichar('0')
      if (ichara.ge.0 .and. ichara.le.9) then
        call sic_message(seve%e,rname,'Integer edit descriptors (e.g. I4) can not be used in this context')
        call sic_message(seve%e,rname,'Use float edit descriptors instead (e.g. F4.0)')
        error = .true.
        return
      endif
    endif
  endif
  !
  do i=1,size
    if (form) then
      read(lun,format,iostat=ier) work
    else
      read(lun,*,iostat=ier) work
    endif
    if (ier.ne.0) then
      ilin = i
      return
    endif
    do j=1,narg
      if (fmt(j).eq.fmt_r4) then
        call r8tor4(work(j),memory(ip(j)),1)
      elseif (fmt(j).eq.fmt_r8) then
        call r8tor8(work(j),memory(ip(j)),1)
      elseif (fmt(j).eq.fmt_i4) then
        call r8toi4(work(j),memory(ip(j)),1)
      elseif (fmt(j).eq.fmt_i8) then
        call r8toi8(work(j),memory(ip(j)),1)
      elseif (fmt(j).eq.fmt_l) then
        call r8toi4(work(j),inte,1)
        if (inte.eq.0) then
          call l4tol4(no,memory(ip(j)),1)
        else
          call l4tol4(yes,memory(ip(j)),1)
        endif
      endif
      if (is(j).gt.1) then
        if (fmt(j).eq.fmt_r8 .or. fmt(j).eq.fmt_i8) then
          ip(j) = ip(j)+2
        else
          ip(j) = ip(j)+1
        endif
      endif
    enddo
  enddo
end subroutine read_all_var
!
subroutine sic_accept_oneline(separ,line,nvar,vtype,val,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_accept_oneline
  use gildas_def
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ private
  ! Read the input line and fill the variables thanks to their type
  ! and pointer to their value.
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: separ     ! Separator
  character(len=*),             intent(in)    :: line      ! Valid input line to read
  integer(kind=4),              intent(in)    :: nvar      ! Number of variables to fill
  integer(kind=4)                             :: vtype(*)  ! Variable types
  integer(kind=address_length), intent(in)    :: val(*)    ! Pointers to variables
  logical,                      intent(inout) :: error     ! Logical error flag
  ! Global
  include 'gbl_memory.inc'
  ! Local
  character(len=*), parameter :: rname='ACCEPT'
  real(kind=4) :: r4
  real(kind=8) :: r8
  integer(kind=4) :: i4
  integer(kind=8) :: i8
  integer(kind=4) :: i,npar,lpar,nmax,ich
  character(len=256) :: par
  character(len=128) :: errorline
  integer(kind=address_length) :: ipar,jpar
  character(len=1), parameter :: blank = ' '
  integer(kind=4), parameter :: dummy=1  ! Value must conform with subroutine sic_accept_getvar
  !
  npar=1
  do i=1,nvar
    ! char(0) means a "normal" list-directed read, or so...
    call sic_separ(separ,line(npar:),par,lpar,npar)
    if (lpar.le.0) then
      write(errorline,'(a,i6)') 'Missing parameter in column ',i
      call sic_message(seve%e,rname,errorline)
      call sic_message(seve%i,rname,line)
      error = .true.
      return
    endif
    !
    ! Check if it is a true argument
    if (val(i).eq.dummy)  cycle  ! Just skip the column
    !
    if (vtype(i).gt.0) then
      nmax=min(lpar,vtype(i))
      if (nmax.gt.0) then
        ipar = locstr(par)
        jpar = bytpnt(ipar,membyt)
        call bytoby(membyt(jpar),membyt(val(i)),nmax)
      endif
      do ich=nmax+1,vtype(i)  ! Blank the end of the string
        call bytoby(blank,membyt(val(i)+ich-1),1)
      enddo
    else
      if (vtype(i).eq.fmt_r4) then
        call sic_math_real(par,lpar,r4,error)
        if (.not.error)  call r4tor4(r4,memory(val(i)),1)
      elseif (vtype(i).eq.fmt_r8) then
        call sic_math_dble(par,lpar,r8,error)
        if (.not.error)  call r8tor8(r8,memory(val(i)),1)
      elseif (vtype(i).eq.fmt_i4) then
        call sic_math_inte(par,lpar,i4,error)
        if (.not.error)  call i4toi4(i4,memory(val(i)),1)
      elseif (vtype(i).eq.fmt_i8) then
        call sic_math_long(par,lpar,i8,error)
        if (.not.error)  call i8toi8(i8,memory(val(i)),1)
      endif
      if (error) then
        call sic_message(seve%e,rname,'Decoding error at '//par(1:lpar))
        return
      endif
    endif
  enddo
  !
end subroutine sic_accept_oneline
