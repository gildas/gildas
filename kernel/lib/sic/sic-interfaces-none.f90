module sic_interfaces_none
  interface
    subroutine sic_build
      use ieee_arithmetic
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      use sic_structures
      !---------------------------------------------------------------------
      ! SIC   External routine
      !       Initialize SIC
      !---------------------------------------------------------------------
      ! Local
    end subroutine sic_build
  end interface
  !
  interface
    subroutine tmp_print(iarray,n)
      integer(kind=8) n
      integer iarray(n)
    end subroutine tmp_print
  end interface
  !
end module sic_interfaces_none
