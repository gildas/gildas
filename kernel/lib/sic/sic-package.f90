!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the SIC package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sic_pack_set(pack)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_pack_set
  use gpack_def
  !---------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='sic'
  pack%ext='.sic'
  pack%authors="J.Pety, S.Bardeau, S.Guilloteau, E.Reynier"
  pack%init=locwrd(sic_pack_init)
  pack%clean=locwrd(sic_pack_clean)
  pack%exec_on_child=locwrd(sic_pack_exec_on_child)
  !
end subroutine sic_pack_set
!
subroutine sic_pack_init(gpack_id,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_pack_init
  use sic_def
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  integer, intent(in) :: gpack_id  !
  logical, intent(inout) :: error  ! Error flag
  !
  ! Set libraries id's
  call gsys_message_set_id(gpack_id)
  call gsys_c_message_set_id(gpack_id)
  call gmath_message_set_id(gpack_id)
  call gwcs_message_set_id(gpack_id)
  call gfits_message_set_id(gpack_id)
  call gio_message_set_id(gpack_id)
  call sic_message_set_id(gpack_id)
  call sic_c_message_set_id(gpack_id)
  !
  call sic_build()
  !
  ! Set default procedure extension
  call exec_program('sic'//backslash//'sic extension .pro')
  !
end subroutine sic_pack_init
!
subroutine sic_pack_exec_on_child(building_pack,error)
  use gpack_def
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_pack_exec_on_child
  use sic_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Load package and user procedure initializations.
  !---------------------------------------------------------------------
  type(gpack_info_t), intent(inout) :: building_pack  ! Package properties
  logical,            intent(inout) :: error          ! Error flag
  ! Local
  character(len=filename_length) :: file
  !
  ! Call initialization procedures
  if (building_pack%ext.eq.'') return
  !
  call exec_program('sic'//backslash//'sic extension '//building_pack%ext)
  !
  ! Execute default initialization macro
  if (sic_query_file('define'//trim(building_pack%pro_suffix)//building_pack%ext,'macro#dir:',building_pack%ext,file)) then
     call exec_program('@ "'//trim(file)//'"')
  endif
  !
  ! Execute user initialization macro
  if (sic_query_file('init','gag_init:',building_pack%ext,file)) then
     call exec_program('@ "'//trim(file)//'"')
  endif
  !
end subroutine sic_pack_exec_on_child
!
subroutine sic_pack_clean(error)
  use gbl_message
  use sic_interfaces, except_this=>sic_pack_clean
  use sic_dependencies_interfaces
  use sic_structures
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error flag
  ! Local
  integer(kind=4) :: i,nmacro2,ier
  character(len=filename_length) :: file
  !
  ! Close the procedures at each execution level
  nmacro2 = nmacro
  do i=1,nmacro2
    call endmac
  enddo
  !
  ! Delete all image variables to ensure updating
  call del_ima_var
  !
  ! Delete temporary directories (unique per session)
  call end_procedure  ! GAG_PROC: support files for procedures (begin/end procedure blocks)
  file = 'GAG_SCRATCH:'
  ier = sic_getlog(file)
  ier = gag_rmdir(file)
  if (ier.ne.0)  call sic_message(seve%w,'SIC','Error removing directory '//file)
  !
#ifndef CYGWIN
  ! Kill sub-process if any
  call end_dialog
#endif
  !
  ! Deallocate the SIC variables arrays
  call sic_free_variable
  !
  call sic_freesymbol(error)
  ! if (error)  continue
  !
  call sic_free_languages(error)
  ! if (error)  continue
  !
  call sic_lunmac_exit(error)
  ! if (error)  continue
  !
  call sic_output_close(error)
  ! if (error)  continue
  !
  call exit_task(error)
  ! if (error)  continue
  !
end subroutine sic_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
