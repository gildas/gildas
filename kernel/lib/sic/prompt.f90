subroutine bldprt (klire)
  use sic_interfaces, except_this=>bldprt
  use sic_structures
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  !  Modify the suffix of the prompt, suited for the current execution
  ! level and execution mode
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: klire  ! Execution level
  !
  if (klire.eq.gprompt_nlire_old) return
  !
  if (klire.gt.9) then
    write(gprompt(lgprompt_base+1:),'(A,I2)') '_',klire
    lgprompt = lgprompt_base+5
  elseif (klire.gt.1) then
    gprompt(lgprompt_base+1:) = '_'//char(klire+48)
    lgprompt = lgprompt_base+4
  else
    lgprompt = lgprompt_base+2
  endif
  !
  if (compil.or.proced) then
    gprompt(lgprompt-1:) = ':'
  else
    gprompt(lgprompt-1:) = '>'
  endif
  !
  gprompt_nlire_old = klire
end subroutine bldprt
!
subroutine gprompt_set(newprompt)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>gprompt_set
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  !  Modify the prompt, the base prompt, and the master prompt
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: newprompt
  ! Local
  character(len=gprompt_length) :: prefix
  integer(kind=4) :: ier
  !
  ! Get prompt prefix from logical value
  prefix = ''
  ier = sic_getlog('prompt_prefix',prefix)
  !
  gprompt_master = trim(prefix)//newprompt
  if (gprompt_master.eq.'')  gprompt_master = 'SIC'
  !
  call gprompt_base_set(gprompt_master)
  !
end subroutine gprompt_set
!
subroutine gprompt_base_set(newbase)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>gprompt_base_set
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  !  Modify the prompt and the base prompt, do not modify the master
  ! prompt.
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: newbase
  !
  gprompt_base = newbase
  lgprompt_base = min(len_trim(gprompt_base),gprompt_length-5)  ! for suffix "_99> "
  !
  gprompt = gprompt_base(1:lgprompt_base)//'>'
  lgprompt = lgprompt_base+2
  !
end subroutine gprompt_base_set
!
subroutine gprompt_get(pr)
  use sic_interfaces, except_this=>gprompt_get
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! Get current prompt
  !---------------------------------------------------------------------
  character(len=*), intent(out) :: pr
  !
  pr = gprompt
  !
end subroutine gprompt_get
