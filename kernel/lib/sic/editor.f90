subroutine editor (file,error)
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>editor
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Edit a file using one of the following editors
  !       - VI
  !       - EMACS
  !  according to SIC EDITOR TT_EDIT variable, or GAG logical name
  !  GAG_EDIT.
  !---------------------------------------------------------------------
  character(len=*) :: file          ! File to be edited
  logical :: error                  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='EDIT'
  character(len=filename_length) :: chain
  integer :: nl,nk,ier
  !
  error = .false.
  !
  if (tt_edit.eq."") then
    call sic_message(seve%e,rname,'No editor defined')
    error = .true.
    return
  endif
  !
  call sic_message(seve%i,rname,'Using "'//trim(tt_edit)//'" editor')
  nl = len_trim(tt_edit)
  nk = len_trim(file)
  if (tt_edit(nl:nl).eq.'&') then
    chain = tt_edit(1:nl-1)//' '//file(1:nk)//' &'
    nl = nl+nk+3
  else
    chain = tt_edit(1:nl)//' '//file(1:nk)
    nl = nl+nk+2
  endif
  if (nl.gt.filename_length) then
    call sic_message(seve%e,rname,'Filename too long')
    error = .true.
    return
  endif
  ier = gag_system(chain)
  error = ier.ne.0
  !
end subroutine editor
