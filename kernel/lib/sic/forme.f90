subroutine sic_format(c,n)
  use sic_interfaces, except_this=>sic_format
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*) :: c             !
  integer :: n                      !
  ! Local
  integer :: ier
  logical :: suite
  !
  suite = .false.
  call sblanc(c,n,suite,ier)
end subroutine sic_format
!
subroutine sblanc(c,n,suite,ier)
  use sic_interfaces, except_this=>sblanc
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Reduce the character string C to the standard SIC internal syntax:
  !  - Suppress all unecessary separators.
  !  - Character strings "abcd... " are not modified.
  !  - Reject blanks in single quote expressions e.g. '1 + 2 3' (same
  !    behaviour as LET A 1 + 2 3)
  !  - Ignores and destroys comments.
  !  - Take care of continuations lines
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: c      ! Character string to be formatted
  integer(kind=4),  intent(inout) :: n      ! Length of C
  logical,          intent(inout) :: suite  ! Indicates continuation
  integer(kind=4),  intent(out)   :: ier    ! Error return code
  ! Local
  character(len=*), parameter :: rname='FORME'
  logical :: lo,li,chaine,math,some,slo,more
  integer(kind=4) :: i,m,nl
  character(len=1), parameter :: quote="'"
  character(len=1) :: b,p
  save chaine,math,lo,li
  !
  data chaine/.false./, math/.false./
  !
  ier = 1
  if (n.eq.0) return
  m = len(c)
  !
  if (.not.suite) then
    lo = .true.                ! Leading blanks should be removed
    slo = lo
    chaine=.false.             ! Reset string indicator
    math  =.false.
  else
    slo = lo
  endif
  some = .false.
  nl = n
  n  = 0
  !
  i = 1
  more = i.le.nl
  do while(more)
    !
    ! Toggle character string indicator
    if (c(i:i).eq.'"') then
      if (chaine) then
        b = c(i+1:i+1)         ! End of chain
        p = ')'
      else
        b = c(n:n)             ! Last character before Start of chain
        p = '('
      endif
      ! Start/End of chain can only be preceded/followed by
      !     Double quote 
      !     Simple quote 
      !     Back quote `
      !     Dot . (in logical expressions)
      !     Comma (e.g. func("a",1))
      !     Parenthesis ( preceding, ) following (in logical expressions)
      !     Space or tab
      if (b.ne.quote   .and. b.ne.'"' .and. b.ne.'.' .and. b.ne.' ' .and.  &
          b.ne.char(9) .and. b.ne.p   .and. b.ne.'`' .and. b.ne.',') then
        n = n+1
        c(n:n) = '"'
        call sic_message(seve%e,rname,'Syntax error at or near :')
        call sic_message(seve%e,rname,c(1:n))
        ier = 0
        return
      endif
      chaine=.not.chaine
    endif
    !
    ! Skip character strings
    if (chaine) then
      n = n+1
      if (i.ne.n) c(n:n)=c(i:i)
      if (i.gt.n) c(i:i)=' '
    else
      !
      ! Suppress redondant separators
      li=.false.
      !
      ! Space separator
      if (c(i:i).eq.' ' .or. c(i:i).eq.char(9)) then
        if (math) then
          call sic_message(seve%e,rname,'Spaces not allowed in single quote expressions')
          ier = 0
          return
        else
          li=.true.
          ! First one
          if (.not.lo) then
            c(i:i) = ' '
            n = n+1
            if (i.ne.n) c(n:n)=c(i:i)
            if (i.gt.n) c(i:i)=' '
          endif
        endif
        !
        ! Skip comment area
      elseif (c(i:i).eq.'!') then
        some = .true.
        more = .false.
        !
        ! Otherwise just increment
      else
        if (c(i:i).eq.quote) then
          math = .not.math
        endif
        n=n+1
        if (i.ne.n) c(n:n)=c(i:i)
        if (i.gt.n) c(i:i)=' '
      endif
    endif
    !
    ! End of loop
    if (more) then
      lo = li
      i = i+1
      more = i.le.nl
    endif
  enddo
  !
  ! Clean up end of line
  if (n.eq.0) then             ! No significant character
    if (suite) then
      if (slo) then
        n = -1
      else
        n = 0
      endif
    elseif (some) then
      n = 1
      c(n:n) = ' '
    endif
  else
    if (.not.chaine .and. c(n:n).eq.' ') then
      n = n-1
    endif
  endif
  !
  ! Take care of continuation line
  if (n.lt.1) then
    suite = .false.
  elseif (c(n:n).ne.'-') then
    suite = .false.
  else
    suite = .true.
  endif
  if (.not.suite) then
    if (chaine) then
      call sic_message(seve%e,rname,'Unbalanced quote count')
      ier = 0
      return
    endif
  else
    c(n:n) = ' '
    n = n-1
    if (c(n:n).eq.' ') lo=.true.
  endif
  if (n.lt.m) c(n+1:) = ' '
end subroutine sblanc
!
function sic_eqchain(a,b)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  logical :: sic_eqchain            !
  character(len=*) :: a             !
  character(len=*) :: b             !
  ! Local
  character(len=1) :: ca,cb
  integer :: i,ic,la,lb
  !
  sic_eqchain = .false.
  la = len_trim(a)
  lb = len_trim(b)
  if (la.ne.lb) return
  !
  do i=1,la
    ca = a(i:i)
    cb = b(i:i)
    if (ca.ne.cb) then
      if (ca.ge.'a' .and. ca.le.'z') then
        ic=ichar(ca)+ichar('A')-ichar('a')
        ca=char(ic)
        if (ca.ne.cb) return
      elseif  (cb.ge.'a' .and. cb.le.'z') then
        ic=ichar(cb)+ichar('A')-ichar('a')
        cb=char(ic)
        if (ca.ne.cb) return
      else
        return
      endif
    endif
  enddo
  sic_eqchain = .true.
end function sic_eqchain
