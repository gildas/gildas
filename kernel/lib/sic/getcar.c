
#include "getcar.h"

#include "gcore/gcomm.h"
#include "gcore/glaunch.h"

#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif

/* When the main task must be mono-user the command memory
   segment must be shared between all instances */
void CFC_API search_clients( int *code)
{
    *code = (sic_get_comm_id( ) == -1) ? 1 : 0;

#ifndef WIN32
#ifndef darwin
    setpgrp( );
#else
    setpgid( 0, 0);                     /* This should also work on Linux */
#endif
#endif

    /* set error handlers */
    /* signal( SIGINT, SIG_IGN); */ /* done by calling trap_ctrlc */

    if (*code == 1) {              /* First client ... */
        sic_create_comm_board( );
    } else {                       /* remote client ... */
        sic_open_comm_board( sic_get_comm_id( ));
    }
}

void CFC_API sic_c_do_exit( int *status)
{
    sic_do_exit( *status);
}

void CFC_API sic_c_on_exit( )
{
    sic_on_exit( );
}

#ifdef GAG_USE_GTK
#define gtk_get_version CFC_EXPORT_NAME(gtk_get_version)
void CFC_API gtk_get_version( CFC_FString version
 CFC_DECLARE_STRING_LENGTH(version))
{
    CFC_c2f_strcpy( version, CFC_STRING_LENGTH( version),
     call_dialog_version_handler());
}
#endif /* GAG_USE_GTK */
