module sic_interfaces_public
  interface
    subroutine sic_def_alias(aliasname,targetname,global,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !
      ! An "alias" is a variable pointing towards an existing one
      ! It need a code under -5, and different from -999, and under the
      ! ReadOnly offset. Let say -6, and call it ALIAS_DEFINED
      !
      ! We have a list of "alias" as a separate table
      ! For each entry, this list contains
      !     - the variable number
      !     - the pointed variable number
      !
      ! In deleting a variable, one checks
      !     - whether it is an alias (code = ALIAS_DEFINED).
      !         - then deleted it from the list aliases
      !         - delete the variable number and name, but do not free memory
      !     - or not
      !         - then check if it has aliases in the alias list.
      !         - Delete all aliases first
      !         - delete variable only after...
      !
      ! Structure canNOT have aliases
      !
      ! A problem concerns aliases of Header variables... They must be deleted
      ! at the same time than the others. This is handled by having the pointee
      ! number being the variable number of the header. To do so, when defining
      ! an alias, we have to figure out if the variable it
      ! points to is an header/image...
      !
      ! Alias can points towards sub-arrays
      !
      ! Alias have the same level as there pointees.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: aliasname   ! Alias name
      character(len=*), intent(in)    :: targetname  ! Target name
      logical,          intent(in)    :: global      ! Is the target global?
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_alias
  end interface
  !
  interface
    subroutine sic_inca (line,iopt,iarg,desc,default,present,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !       Retrieves the IARG-th argument of the IOPT-th option
      !       and returns a SIC incarnation of the result.
      !
      ! Arguments:
      !       LINE    C*(*)   Command line                    Input
      !       IOPT    I       Option number (0=command)       Input
      !       IARG    I       Argument number (0=command)     Input
      !       PRESENT L       Must the argument be present ?  Input
      !---------------------------------------------------------------------
      character(len=*)                      :: line     !
      integer                               :: iopt     !
      integer                               :: iarg     !
      type(sic_descriptor_t), intent(out)   :: desc     ! Descriptor
      type(sic_descriptor_t), intent(in)    :: default  ! Default descriptor
      logical                               :: present  !
      logical,                intent(inout) :: error    ! Error return
    end subroutine sic_inca
  end interface
  !
  interface
    subroutine sic_analyse(command,line,nline,error)
      use gildas_def
      use sic_expressions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ public
      ! SIC command line analyser. First entry point:
      ! SIC_ANALYSE     Look for all languages including "library"
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: command  ! The command found, if any
      character(len=*), intent(inout) :: line     ! The line to be analysed (updated)
      integer(kind=4),  intent(inout) :: nline    ! Length of LINE (updated)
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_analyse
  end interface
  !
  interface
    subroutine sic_find(command,line,nline,error)
      use gildas_def
      use sic_expressions
      use sic_structures
      !---------------------------------------------------------------------
      ! @ public
      ! SIC command line analyser. Second entry point:
      ! SIC_FIND        Look only for interactive languages.
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: command  ! The command found, if any
      character(len=*), intent(inout) :: line     ! The line to be analysed (updated)
      integer(kind=4),  intent(inout) :: nline    ! Length of LINE (updated)
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_find
  end interface
  !
  interface
    subroutine sic_lang(current_language)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      !  Returns the language of the current command
      !---------------------------------------------------------------------
      character(len=*) :: current_language  !
    end subroutine sic_lang
  end interface
  !
  interface
    subroutine sic_ilang(ilang,language)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      !  Returns the name of the i-th language
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: ilang
      character(len=*), intent(out) :: language
    end subroutine sic_ilang
  end interface
  !
  interface
    subroutine sic_comm(current_command)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      !  Return the current command number (number in language, options
      ! excluded).
      !---------------------------------------------------------------------
      integer(kind=4) :: current_command
    end subroutine sic_comm
  end interface
  !
  interface
    subroutine sic_r8 (line,iopt,iarg,dbl,present,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the IARG-th argument of the IOPT-th option
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      real(kind=8),     intent(inout) :: dbl      ! Argument value
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_r8
  end interface
  !
  interface
    subroutine sic_l4 (line,iopt,iarg,logi,present,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the IARG-th argument of the IOPT-th option
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      logical,          intent(inout) :: logi     ! Argument value
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_l4
  end interface
  !
  interface
    subroutine sic_ch (line,iopt,iarg,argum,length,present,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the IARG-th argument of the IOPT-th option
      !  Character string. Length returned.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      character(len=*), intent(inout) :: argum    ! Argument value
      integer(kind=4),  intent(out)   :: length   !
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_ch
  end interface
  !
  interface
    subroutine sic_st (line,iopt,iarg,argum,length,present,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the IARG-th argument of the IOPT-th option
      !  Character string, with no formatting.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      character(len=*), intent(inout) :: argum    ! Argument value
      integer(kind=4),  intent(out)   :: length   !
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_st
  end interface
  !
  interface
    subroutine sic_ke (line,iopt,iarg,argum,length,present,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the IARG-th argument of the IOPT-th option
      !  Keyword: Expansion, Uppercase conversion
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      character(len=*), intent(inout) :: argum    ! Argument value
      integer(kind=4),  intent(out)   :: length   !
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_ke
  end interface
  !
  interface
    subroutine sic_de(line,iopt,iarg,desc,present,error)
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieves the descriptor of the IARG-th argument of the IOPT-th
      ! option.
      !
      !  - If argum is a variable name, return its descriptor
      !  - If argum is a variable non-contiguous subset, return the
      !    descriptor of its materialization
      !  - If argum is a scalar value (numeric, logical, or character),
      !    return the descriptor of its incarnation
      !
      ! IT IS THE RESPONSIBILITY OF THE CALLER TO FREE SCRATCH VARIABLES
      ! CREATED HERE: call sic_volatile(desc)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: line     ! Command line
      integer(kind=4),        intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),        intent(in)    :: iarg     ! Argument number (0=command)
      type(sic_descriptor_t), intent(inout) :: desc     !
      logical,                intent(in)    :: present  ! Must the argument be present ?
      logical,                intent(inout) :: error    ! Error flag
    end subroutine sic_de
  end interface
  !
  interface
    function sic_present(iopt,iarg)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ public
      !  Is argument IARG of option IOPT present ?
      !---------------------------------------------------------------------
      logical :: sic_present  ! Function value on return
      integer(kind=4), intent(in) :: iopt  ! Option number (0=command)
      integer(kind=4), intent(in) :: iarg  ! Argument number (0=option)
    end function sic_present
  end interface
  !
  interface
    function sic_len(iopt,iarg)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! Length (number of characters) of argument IARG of option IOPT
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_len  ! Function value on return
      integer(kind=4), intent(in) :: iopt  ! Argument number (0=option)
      integer(kind=4), intent(in) :: iarg  ! Option number (0=command
    end function sic_len
  end interface
  !
  interface
    function sic_start(iopt,iarg)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      !  Return the position (first character) of argument IARG of option
      ! IOPT
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_start  ! Function value on return
      integer(kind=4), intent(in) :: iopt  ! Argument number (0=option)
      integer(kind=4), intent(in) :: iarg  ! Option number (0=command)
    end function sic_start
  end interface
  !
  interface
    function sic_end(iopt,iarg)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      !  Return the position (last character) of argument IARG of option
      ! IOPT
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_end  ! Function value on return
      integer(kind=4), intent(in) :: iopt  ! Argument number (0=option)
      integer(kind=4), intent(in) :: iarg  ! Option number (0=command)
    end function sic_end
  end interface
  !
  interface
    function sic_narg(iopt)
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! Return the number of arguments passed to the Ith option (0 ==
      ! command)
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_narg  ! Function value on return
      integer(kind=4), intent(in) :: iopt  !
    end function sic_narg
  end interface
  !
  interface
    function sic_nopt()
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! Return the number of options passed to the current command
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_nopt  ! Function value on return
    end function sic_nopt
  end interface
  !
  interface
    function sic_mopt()
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! Return the total number of known options to the current command
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_mopt  ! Function value on return
    end function sic_mopt
  end interface
  !
  interface
    function sic_lire()
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! To be checked and documented
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_lire  ! Function value on return
    end function sic_lire
  end interface
  !
  interface
    subroutine sic_spanum(string,anum,nfx,nf,ndg,range,digit,ndc)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   This routine is called for numbering. It translates a number into
      ! a character string.
      !
      !   Maximum number of significant digits is MXDIG0 or DIGIT if non
      ! zero. If your computer has a lower accuracy, you should accordingly
      ! decrease the value of MXDIG0 in order to get rid of round-off
      ! troubles.
      !
      !   NDC is the range in which fixed format is used.
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: string  ! Formatted result
      real(kind=8),     intent(in)  :: anum    ! Number to be formatted
      integer(kind=4),  intent(in)  :: nfx     ! Desired format
      integer(kind=4),  intent(out) :: nf      ! Used format 1 fixed, 0 Integer, -1 Float
      integer(kind=4),  intent(out) :: ndg     ! Number of characters in STRING
      real(kind=8),     intent(in)  :: range   ! Reduce number of digits for smaller numbers
      integer(kind=4),  intent(in)  :: digit   ! Max number of digits
      integer(kind=4),  intent(in)  :: ndc     ! Max number of decimals
    end subroutine sic_spanum
  end interface
  !
  interface sic_i0
    subroutine sic_i8 (line,iopt,iarg,long,present,error)
      !---------------------------------------------------------------------
      ! @ public-generic sic_i0
      !  Retrieves the IARG-th argument of the IOPT-th option
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      integer(kind=8),  intent(inout) :: long     ! Argument value
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_i8
    subroutine sic_i4 (line,iopt,iarg,inte,present,error)
      !---------------------------------------------------------------------
      ! @ public-generic sic_i0
      !  Retrieves the IARG-th argument of the IOPT-th option
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      integer(kind=4),  intent(inout) :: inte     ! Argument value
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_i4
    subroutine sic_i2 (line,iopt,iarg,short,present,error)
      !---------------------------------------------------------------------
      ! @ public-generic sic_i0
      !  Retrieves the IARG-th argument of the IOPT-th option
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     ! Command line
      integer(kind=4),  intent(in)    :: iopt     ! Option number (0=command)
      integer(kind=4),  intent(in)    :: iarg     ! Argument number (0=command)
      integer(kind=2),  intent(inout) :: short    ! Argument value
      logical,          intent(in)    :: present  ! Must the argument be present ?
      logical,          intent(inout) :: error    ! Error flag
    end subroutine sic_i2
  end interface sic_i0
  !
  interface
    subroutine sic_def_func(name_in,s_func,d_func,nfunarg,error,hlpfile)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !  Defines user functions
      !---------------------------------------------------------------------
      character(len=*), intent(in)           :: name_in  ! Function name
      real(kind=4),     external             :: s_func   ! Real*4 function
      real(kind=8),     external             :: d_func   ! Real*8 function
      integer(kind=4),  intent(in)           :: nfunarg  ! Number of arguments for function
      logical,          intent(out)          :: error    ! Logical error flag
      character(len=*), intent(in), optional :: hlpfile  ! Help file for "HELP FUNCTION Name"
    end subroutine sic_def_func
  end interface
  !
  interface
    subroutine sic_defvariable (type,namein,global,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      !  Define a new user-defined variable (usually from command DEFINE).
      ! Variable necessarily has Read-Write access
      !---------------------------------------------------------------------
      integer                         :: type    ! Data type code          Input
      character(len=*)                :: namein  ! Variable name
      logical                         :: global  ! Is variable global?     Input
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_defvariable
  end interface
  !
  interface
    subroutine sic_crestructure(namein,global,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      !     Define a new structure from command
      !     DEFINE STRUCT Variable Read|Write
      !     Variable may have ReadOnly protection.
      !---------------------------------------------------------------------
      character(len=*)                :: namein  ! Structure name
      logical                         :: global  ! Local / Global flag        Input
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine sic_crestructure
  end interface
  !
  interface
    subroutine sic_defstructure(namein,global,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !     Define a new structure from program.
      !     Variable may have ReadOnly protection ?
      !---------------------------------------------------------------------
      character(len=*) :: namein        ! Structure name
      logical :: global                 ! Local / Global flag        Input
      logical :: error                  ! Logical error flag         Output
    end subroutine sic_defstructure
  end interface
  !
  interface
    subroutine sic_def_header(name,head,readonly,error)
      use image_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !   Public entry point which allows to fill a SIC structure mapping
      ! the input image header. The structure is always global. Input object
      ! must be the whole 'gildas' type because we map head%gil% and
      ! head%char% elements.
      !   NB: if missing, the blanking section is enabled to default values
      ! (with eval<0). Because of this the input target header is
      ! intent(inout).
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name      ! Structure name
      type(gildas),     intent(inout) :: head      ! Target header
      logical,          intent(in)    :: readonly  ! Readonly status
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_header
  end interface
  !
  interface
    subroutine sic_mapheader(name,head,error)
      use image_def
      !---------------------------------------------------------------------
      ! @ public
      !   Create a HEADER SIC variable from an existing Header structure.
      ! It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name   ! Header name
      type(gildas), target, intent(inout) :: head   ! Header
      logical(kind=4),      intent(inout) :: error  ! Logical error flag
    end subroutine sic_mapheader
  end interface
  !
  interface sic_mapgildas
    subroutine sic_mapgildas_r41d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=4),         intent(in)    :: data(:)  !
    end subroutine sic_mapgildas_r41d
    subroutine sic_mapgildas_r42d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=4),         intent(in)    :: data(:,:)  !
    end subroutine sic_mapgildas_r42d
    subroutine sic_mapgildas_r43d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=4),         intent(in)    :: data(:,:,:)  !
    end subroutine sic_mapgildas_r43d
    subroutine sic_mapgildas_r44d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=4),         intent(in)    :: data(:,:,:,:)  !
    end subroutine sic_mapgildas_r44d
    subroutine sic_mapgildas_r81d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=8),         intent(in)    :: data(:)  !
    end subroutine sic_mapgildas_r81d
    subroutine sic_mapgildas_r82d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=8),         intent(in)    :: data(:,:)  !
    end subroutine sic_mapgildas_r82d
    subroutine sic_mapgildas_r83d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=8),         intent(in)    :: data(:,:,:)  !
    end subroutine sic_mapgildas_r83d
    subroutine sic_mapgildas_r84d(name,head,error,data)
      use image_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data. It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: name     ! Header name
      type(gildas), target, intent(inout) :: head     ! Header
      logical(kind=4),      intent(inout) :: error    ! Logical error flag
      real(kind=8),         intent(in)    :: data(:,:,:,:)  !
    end subroutine sic_mapgildas_r84d
    subroutine sub_mapgildas(name,head,fmt,address,error)
      use image_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_mapgildas
      !   Create an IMAGE SIC variable from an existing Header structure
      ! and data (specified by its address and size). 
      ! It is global and program_defined.
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: name     ! Header name
      type(gildas), target,         intent(inout) :: head     ! Header
      integer(kind=4),              intent(in)    :: fmt      ! Data type (fmt_r4,...)
      integer(kind=address_length), intent(in)    :: address  ! Address of data
      logical(kind=4),              intent(inout) :: error    ! Logical error flag
    end subroutine sub_mapgildas
  end interface sic_mapgildas
  !
  interface
    subroutine sic_delvariable (var,user,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (as calls with named arguments are used)
      !   Delete Variable VAR (any kind)
      !   If variable does not exist and request comes from user, an error
      ! is raised. If variable does not exist and request comes from
      ! program, no error is raised.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: var    ! Name of variable
      logical,          intent(in) :: user   ! User (TRUE) or Program request
      logical                      :: error  ! Return error code               Output
    end subroutine sic_delvariable
  end interface
  !
  interface
    function desc_nelem(desc)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Return the number of elements from the input descriptor
      !---------------------------------------------------------------------
      integer(kind=size_length) :: desc_nelem
      type(sic_descriptor_t), intent(in) :: desc
    end function desc_nelem
  end interface
  !
  interface
    function desc_nword(desc)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Return the number of 4 bytes words from the input descriptor.
      ! Usually suited for the desc%size value.
      !---------------------------------------------------------------------
      integer(kind=size_length) :: desc_nword
      type(sic_descriptor_t), intent(in) :: desc
    end function desc_nword
  end interface
  !
  interface
    function desc_size(desc)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Return the whole data size in bytes
      !---------------------------------------------------------------------
      integer(kind=size_length) :: desc_size
      type(sic_descriptor_t), intent(in) :: desc
    end function desc_size
  end interface
  !
  interface
    subroutine sic_descriptor(namein,desc,found)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !   Retrieve the descriptor of an existing variable
      !  Returns FOUND = .FALSE. if no such variable exists
      !  Automatic transposition is not allowed at this stage
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: namein  ! Variable name
      type(sic_descriptor_t), intent(out)   :: desc    ! Descriptor as array
      logical,                intent(inout) :: found   ! Verbose (in) / Found (out)
    end subroutine sic_descriptor
  end interface
  !
  interface
    subroutine sic_materialize(namein,desc,found)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !   Retrieve the descriptor of an existing variable or of an
      ! incarnation of an expression (including automatic transposition)
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: namein  ! Variable name
      type(sic_descriptor_t), intent(out)   :: desc    ! Descriptor
      logical,                intent(inout) :: found   ! Verbose (in) / Found (out)
    end subroutine sic_materialize
  end interface
  !
  interface
    function sic_varexist(name)
      use gildas_def
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !   Check if the input variable exists
      !---------------------------------------------------------------------
      logical :: sic_varexist  ! Function value on return
      character(len=*), intent(in) :: name  ! Variable name
    end function sic_varexist
  end interface
  !
  interface
    function sic_level(namein)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Retrieve the level of a variable
      !---------------------------------------------------------------------
      integer(kind=4) :: sic_level  ! Function value on return
      character(len=*), intent(in) :: namein  ! Variable name
    end function sic_level
  end interface
  !
  interface
    subroutine sic_volatile(desc)
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Free data associated to a variable if it is Scratch
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in) :: desc  !
    end subroutine sic_volatile
  end interface
  !
  interface
    function sic_notsamedesc(one,two)
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Check if two descriptors are equal
      !---------------------------------------------------------------------
      logical :: sic_notsamedesc  ! Function value on return
      type(sic_descriptor_t), intent(in) :: one  !
      type(sic_descriptor_t), intent(in) :: two  !
    end function sic_notsamedesc
  end interface
  !
  interface sic_incarnate
    subroutine sic_incarnate_desc(form,desc,inca,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of a variable under a specified type
      ! (REAL, DOUBLE, INTEGER...).
      ! DESC and INCA can be the same descriptor if needed (=> do not
      ! put INTENT statements).
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: form   ! Format of the desired incarnation
      type(sic_descriptor_t), intent(in)    :: desc   ! Input descriptor of the variable
      type(sic_descriptor_t), intent(inout) :: inca   ! Descriptor of the incarnation
      logical,                intent(out)   :: error  ! Logical error flag
    end subroutine sic_incarnate_desc
    subroutine sic_incarnate_i4(i4,inca,error)
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of an explicit I*4 value
      !---------------------------------------------------------------------
      integer(kind=4),        intent(in)    :: i4     ! The value to be incarnated
      type(sic_descriptor_t), intent(out)   :: inca   ! Descriptor of the incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_incarnate_i4
    subroutine sic_incarnate_i8(i8,inca,error)
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of an explicit I*8 value
      !---------------------------------------------------------------------
      integer(kind=8),        intent(in)    :: i8     ! The value to be incarnated
      type(sic_descriptor_t), intent(out)   :: inca   ! Descriptor of the incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_incarnate_i8
    subroutine sic_incarnate_r8(r8,inca,error)
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of an explicit R*8 value
      !---------------------------------------------------------------------
      real(kind=8),           intent(in)    :: r8     ! The value to be incarnated
      type(sic_descriptor_t), intent(out)   :: inca   ! Descriptor of the incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_incarnate_r8
    subroutine sic_incarnate_l4(l4,inca,error)
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of an explicit L*4 value
      !---------------------------------------------------------------------
      logical,                intent(in)    :: l4     ! The value to be incarnated
      type(sic_descriptor_t), intent(out)   :: inca   ! Descriptor of the incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_incarnate_l4
    subroutine sic_incarnate_ch(ch,inca,error)
      use gbl_format
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_incarnate
      !  Creates an "incarnation" of an explicit C*(*) value
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: ch     ! The string to be incarnated
      type(sic_descriptor_t), intent(out)   :: inca   ! Descriptor of the incarnation
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_incarnate_ch
  end interface sic_incarnate
  !
  interface
    subroutine sic_begin(lang,help,scom,ccom,version,inter,prob)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public
      ! Main initialisation routine for Languages
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: lang     !
      character(len=*), intent(in) :: help     !
      integer,          intent(in) :: scom     !
      character(len=*), intent(in) :: ccom(1)  !
      character(len=*), intent(in) :: version  !
      external :: inter                        ! Subroutine for executable code
      logical, external :: prob                ! Logical error function
    end subroutine sic_begin
  end interface
  !
  interface
    recursive subroutine exec_program(buffer)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      ! Main routine to execute a command or enter a program
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: buffer  !
    end subroutine exec_program
  end interface
  !
  interface
    subroutine exec_adjust(buffer,error)
      use gildas_def
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      ! Execute any command, possibly a procedure. Non recursive version
      ! used by ADJUST only
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: buffer  !
      logical,          intent(out) :: error   !
    end subroutine exec_adjust
  end interface
  !
  interface
    subroutine exec_string (line,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Executes a single command. Input line is protected 
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Line to execute
      logical,          intent(inout) :: error  ! Error flag
    end subroutine exec_string
  end interface
  !
  interface
    subroutine exec_command(line,error)
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      ! Executes a single command. Input line is also resolved on output.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Line to execute
      logical,          intent(inout) :: error  ! Error flag
    end subroutine exec_command
  end interface
  !
  interface
    subroutine run_sic(line,command,error)
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public-mandatory (public but should probably not) (mandatory
      ! because symbol is used elsewhere)
      ! Execute an elemental SIC command
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line     ! Command line, can be updated on return
      character(len=*), intent(in)    :: command  !
      logical,          intent(out)   :: error    !
    end subroutine run_sic
  end interface
  !
  interface sic_descriptor_fill
    subroutine sic_descriptor_fill_i40d(desc,ielem,i4,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill the ielem-th value from the descriptor target. The value is
      ! casted to the descriptor type if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      integer(kind=4),           intent(in)    :: i4      ! Scalar value to fill
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_i40d
    subroutine sic_descriptor_fill_r80d(desc,ielem,r8,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill the ielem-th value from the descriptor target. The value is
      ! casted to the descriptor type if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      real(kind=8),              intent(in)    :: r8      ! Scalar value to fill
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_r80d
    subroutine sic_descriptor_fill_ch0d(desc,ielem,ch,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the ch(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      character(len=*),          intent(in)    :: ch      ! Scalar string to fill
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_ch0d
    subroutine sic_descriptor_fill_i41d(desc,i4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the i4(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=4),           intent(in)    :: i4(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of i4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_i41d
    subroutine sic_descriptor_fill_i81d(desc,i8,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the i8(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=8),           intent(in)    :: i8(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of i8 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_i81d
    subroutine sic_descriptor_fill_r41d(desc,r4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the r4(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=4),              intent(in)    :: r4(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_r41d
    subroutine sic_descriptor_fill_r81d(desc,r8,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the r8(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=8),              intent(in)    :: r8(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r8 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_r81d
    subroutine sic_descriptor_fill_c41d(desc,c4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the c4(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      complex(kind=4),           intent(in)    :: c4(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of c4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_c41d
    subroutine sic_descriptor_fill_l1d(desc,l,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the l(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      logical,                   intent(in)    :: l(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of l values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_l1d
    subroutine sic_descriptor_fill_ch1d(desc,ch,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_fill
      !  Fill a descriptor target with the ch(*) values. A consistency check
      ! of the number of values is performed, and value is casted if needed
      ! to be SIC variable type if needed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      character(len=*),          intent(in)    :: ch(*)   ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: neleml  ! Number of l values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_fill_ch1d
  end interface sic_descriptor_fill
  !
  interface sic_descriptor_getval
    subroutine sic_descriptor_getval_i40d(desc,ielem,i4,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target. The value is
      ! casted to I*4 if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      integer(kind=4),           intent(out)   :: i4      ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_i40d
    subroutine sic_descriptor_getval_i80d(desc,ielem,i8,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target. The value is
      ! casted to I*8 if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      integer(kind=8),           intent(out)   :: i8      ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_i80d
    subroutine sic_descriptor_getval_r40d(desc,ielem,r4,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target. The value is
      ! casted to R*4 if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      real(kind=4),              intent(out)   :: r4      ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_r40d
    subroutine sic_descriptor_getval_r80d(desc,ielem,r8,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target. The value is
      ! casted to R*8 if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      real(kind=8),              intent(out)   :: r8      ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_r80d
    subroutine sic_descriptor_getval_c40d(desc,ielem,c4,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target. The value is
      ! casted to C*4 if needed
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      complex(kind=4),           intent(out)   :: c4      ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_c40d
    subroutine sic_descriptor_getval_l0d(desc,ielem,l,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem   ! The value position
      logical,                   intent(out)   :: l       ! Scalar value to be filled
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_l0d
    subroutine sic_descriptor_getval_ch0d(desc,ielem,ch,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the ielem-th value from the descriptor target.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc   ! The variable descriptor
      integer(kind=size_length), intent(in)    :: ielem  ! The value position
      character(len=*),          intent(out)   :: ch     ! Scalar value to be filled
      logical,                   intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_ch0d
    subroutine sic_descriptor_getval_i4nd(desc,i4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i4(*) array. A
      ! consistency check of the number of values is performed, and values
      ! are casted if needed to I*4
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=4),           intent(out)   :: i4(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of i4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_i4nd
    subroutine sic_descriptor_getval_i41d(desc,i4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i4(:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
      integer(kind=4),        intent(out)   :: i4(:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_i41d
    subroutine sic_descriptor_getval_i42d(desc,i4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i4(:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
      integer(kind=4),        intent(out)   :: i4(:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine sic_descriptor_getval_i42d
    subroutine sic_descriptor_getval_i43d(desc,i4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i4(:,:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
      integer(kind=4),        intent(out)   :: i4(:,:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_descriptor_getval_i43d
    subroutine sic_descriptor_getval_i8nd(desc,i8,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i8(*) array. A
      ! consistency check of the number of values is performed, and values
      ! are casted if needed to I*8
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      integer(kind=8),           intent(out)   :: i8(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of i8 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_i8nd
    subroutine sic_descriptor_getval_i81d(desc,i8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i8(:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
      integer(kind=8),        intent(out)   :: i8(:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_i81d
    subroutine sic_descriptor_getval_i82d(desc,i8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i8(:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
      integer(kind=8),        intent(out)   :: i8(:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine sic_descriptor_getval_i82d
    subroutine sic_descriptor_getval_i83d(desc,i8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the i8(:,:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
      integer(kind=8),        intent(out)   :: i8(:,:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_descriptor_getval_i83d
    subroutine sic_descriptor_getval_r4nd(desc,r4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r4(*) array. A
      ! consistency check of the number of values is performed, and values
      ! are casted if needed to R*4
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=4),              intent(out)   :: r4(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_r4nd
    subroutine sic_descriptor_getval_r41d(desc,r4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r4(:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
      real(kind=4),           intent(out)   :: r4(:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_r41d
    subroutine sic_descriptor_getval_r42d(desc,r4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r4(:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
      real(kind=4),           intent(out)   :: r4(:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine sic_descriptor_getval_r42d
    subroutine sic_descriptor_getval_r43d(desc,r4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r4(:,:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
      real(kind=4),           intent(out)   :: r4(:,:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_descriptor_getval_r43d
    subroutine sic_descriptor_getval_r8nd(desc,r8,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r8(*) array. A
      ! consistency check of the number of values is performed, and values
      ! are casted if needed to R*8
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      real(kind=8),              intent(out)   :: r8(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of r8 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_r8nd
    subroutine sic_descriptor_getval_r81d(desc,r8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r8(:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
      real(kind=8),           intent(out)   :: r8(:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_r81d
    subroutine sic_descriptor_getval_r82d(desc,r8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r8(:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
      real(kind=8),           intent(out)   :: r8(:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine sic_descriptor_getval_r82d
    subroutine sic_descriptor_getval_r83d(desc,r8,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the r8(:,:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
      real(kind=8),           intent(out)   :: r8(:,:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_descriptor_getval_r83d
    subroutine sic_descriptor_getval_c4nd(desc,c4,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the c4(*) array. A
      ! consistency check of the number of values is performed, and values
      ! are casted if needed to R*4
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      complex(kind=4),           intent(out)   :: c4(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of c4 values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_c4nd
    subroutine sic_descriptor_getval_c41d(desc,c4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the c4(:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc   ! The variable descriptor
      complex(kind=4),        intent(out)   :: c4(:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error  ! Logical error flag
    end subroutine sic_descriptor_getval_c41d
    subroutine sic_descriptor_getval_c42d(desc,c4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the c4(:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc     ! The variable descriptor
      complex(kind=4),        intent(out)   :: c4(:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error    ! Logical error flag
    end subroutine sic_descriptor_getval_c42d
    subroutine sic_descriptor_getval_c43d(desc,c4,error)
      use gildas_def
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the c4(:,:,:) array.
      !---------------------------------------------------------------------
      type(sic_descriptor_t), intent(in)    :: desc       ! The variable descriptor
      complex(kind=4),        intent(out)   :: c4(:,:,:)  ! Flat array of values to be filled
      logical,                intent(inout) :: error      ! Logical error flag
    end subroutine sic_descriptor_getval_c43d
    subroutine sic_descriptor_getval_chnd(desc,ch,neleml,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_descriptor_getval
      !  Get the descriptor target values into the c*(*) array. A
      ! consistency check of the number of values is performed.
      !---------------------------------------------------------------------
      type(sic_descriptor_t),    intent(in)    :: desc    ! The variable descriptor
      character(len=*),          intent(out)   :: ch(*)   ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: neleml  ! Number of ch values
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine sic_descriptor_getval_chnd
  end interface sic_descriptor_getval
  !
  interface sic_variable_fill
    subroutine sic_variable_filli4_0d(caller,varname,i4,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the i4 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      integer(kind=4),  intent(in)    :: i4       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filli4_0d
    subroutine sic_variable_filli4_1d(caller,varname,i4,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the i4 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      integer(kind=4),           intent(in)    :: i4(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of i4 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filli4_1d
    subroutine sic_variable_filli8_0d(caller,varname,i8,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the i8 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      integer(kind=8),  intent(in)    :: i8       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filli8_0d
    subroutine sic_variable_filli8_1d(caller,varname,i8,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the i8 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      integer(kind=8),           intent(in)    :: i8(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of i8 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filli8_1d
    subroutine sic_variable_fillr4_0d(caller,varname,r4,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the r4 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      real(kind=4),     intent(in)    :: r4       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillr4_0d
    subroutine sic_variable_fillr4_1d(caller,varname,r4,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the r4 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      real(kind=4),              intent(in)    :: r4(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of r4 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillr4_1d
    subroutine sic_variable_fillr8_0d(caller,varname,r8,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the r8 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      real(kind=8),     intent(in)    :: r8       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillr8_0d
    subroutine sic_variable_fillr8_1d(caller,varname,r8,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the r8 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      real(kind=8),              intent(in)    :: r8(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of r8 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillr8_1d
    subroutine sic_variable_fillc4_0d(caller,varname,c4,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the c4 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      complex(kind=4),  intent(in)    :: c4       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillc4_0d
    subroutine sic_variable_fillc4_1d(caller,varname,c4,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the c4 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      complex(kind=4),           intent(in)    :: c4(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of c4 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillc4_1d
    subroutine sic_variable_filll_0d(caller,varname,l,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the l value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      logical,          intent(in)    :: l        ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filll_0d
    subroutine sic_variable_filll_1d(caller,varname,l,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the l value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      logical,                   intent(in)    :: l(*)     ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of l values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_filll_1d
    subroutine sic_variable_fillch_0d(caller,varname,ch,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the ch value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      character(len=*), intent(in)    :: ch       ! Scalar value to be copied
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillch_0d
    subroutine sic_variable_fillch_1d(caller,varname,ch,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_fill
      !  Fill a SIC variable with the ch value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      character(len=*),          intent(in)    :: ch(*)    ! Flat array of values to be copied
      integer(kind=size_length), intent(in)    :: nelem    ! Number of l values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_fillch_1d
  end interface sic_variable_fill
  !
  interface
    subroutine fits_gildas(line,error)
      use gildas_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   FITS File.fits FROM File.gdf /STYLE [/HDU Number] /CHECK /OVERWRITE
      !   FITS File.fits TO File.gdf   /STYLE /BITS /BLC /TRC /OVERWRITE
      !
      ! Internal FITS_GILDAS or GILDAS_FITS conversion.
      ! Allow unrecognized FITS keyword handling, either by Symbol translation
      ! or by user input.
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine fits_gildas
  end interface
  !
  interface
    subroutine sic_format(c,n)
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: c             !
      integer :: n                      !
    end subroutine sic_format
  end interface
  !
  interface
    subroutine sic_get_real (var,rel,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      real*4 :: rel                     !
      logical :: error                  !
    end subroutine sic_get_real
  end interface
  !
  interface
    subroutine sic_get_dble (var,doble,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      real*8 :: doble                   !
      logical :: error                  !
    end subroutine sic_get_dble
  end interface
  !
  interface
    subroutine sic_get_inte (var,inte,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      integer*4 :: inte                 !
      logical :: error                  !
    end subroutine sic_get_inte
  end interface
  !
  interface
    subroutine sic_get_long (var,long,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      integer*8 :: long                 !
      logical :: error                  !
    end subroutine sic_get_long
  end interface
  !
  interface
    subroutine sic_get_logi (var,logi,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      logical :: logi                   !
      logical :: error                  !
    end subroutine sic_get_logi
  end interface
  !
  interface
    subroutine sic_get_char (var,chain,lc,error)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      ! Retrieve a global variable value
      !---------------------------------------------------------------------
      character(len=*) :: var           !
      character(len=*) :: chain         !
      integer :: lc                     !
      logical :: error                  !
    end subroutine sic_get_char
  end interface
  !
  interface sic_variable_get
    subroutine sic_variable_getr4_0d(caller,varname,r4,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_get
      !  Get a SIC variable into the r4 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      real(kind=4),     intent(out)   :: r4       ! Scalar value to be filled
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_getr4_0d
    subroutine sic_variable_getr4_1d(caller,varname,r4,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_get
      !  Get a SIC variable into the r4 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      real(kind=4),              intent(out)   :: r4(*)    ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: nelem    ! Number of r4 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_getr4_1d
    subroutine sic_variable_getr8_0d(caller,varname,r8,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_get
      !  Get a SIC variable into the r8 value
      ! ---
      !  Scalar version
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller   !
      character(len=*), intent(in)    :: varname  !
      real(kind=8),     intent(out)   :: r8       ! Scalar value to be filled
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_getr8_0d
    subroutine sic_variable_getr8_1d(caller,varname,r8,nelem,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public-generic sic_variable_get
      !  Get a SIC variable into the r8 value
      ! ---
      !  Flat array version
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: caller   !
      character(len=*),          intent(in)    :: varname  !
      real(kind=8),              intent(out)   :: r8(*)    ! Flat array of values to be filled
      integer(kind=size_length), intent(in)    :: nelem    ! Number of r8 values
      logical,                   intent(inout) :: error    ! Logical error flag
    end subroutine sic_variable_getr8_1d
  end interface sic_variable_get
  !
  interface
    subroutine sic_edit(chain,nchain,invite,ninv)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      integer :: nchain                 !
      character(len=*) :: invite        !
      integer :: ninv                   !
    end subroutine sic_edit
  end interface
  !
  interface
    subroutine help_button(hlpfile,varname,buffer)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ public-mandatory (used by ggui, symbol is used elsewhere)
      !  Fill the help lines into the output buffer. This is suited
      ! only for the guis.
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: hlpfile
      character(len=*), intent(in)  :: varname
      character(len=*), intent(out) :: buffer
    end subroutine help_button
  end interface
  !
  interface
    subroutine sic_load(lang,help,scom,ccom,version)
      use sic_dictionaries
      use sic_interactions
      use gbl_message
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public (but private in the context of new language initialization)
      !       Initialise a SIC vocabulary
      ! Arguments :
      !       LANG    C*(*)   Language name
      !       HELP    C*(*)   Logical name for help file
      !       SCOM    I       Signed number of commands and options
      !                       Negative means library only mode
      !       CCOM    C*(*)   Vocabulary
      !       VERSION C*(*)   Character string indicating the version number
      !                       and other relevant information
      !	#1	S.Guilloteau 	1-SEP-1989
      !		No log file in library only mode
      !---------------------------------------------------------------------
      character(len=*) :: lang          !
      character(len=*) :: help          !
      integer :: scom                   !
      character(len=*) :: ccom(1)       !
      character(len=*) :: version       !
    end subroutine sic_load
  end interface
  !
  interface
    function sic_inter_state(dummy)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      ! Return the inter_state status.
      !---------------------------------------------------------------------
      logical             :: sic_inter_state  ! Value on return
      integer, intent(in) :: dummy            ! Unused
    end function sic_inter_state
  end interface
  !
  interface
    subroutine sic_parse_listi4(rname,chain,l4,mlist,error)
      use gbl_message
      use sic_types
      !----------------------------------------------------------------------
      ! @ public
      !  Decode a list as integer*4 values
      !----------------------------------------------------------------------
      character(len=*),   intent(in)    :: rname  ! Procedure name
      character(len=*),   intent(in)    :: chain  ! Input line to be decoded
      type(sic_listi4_t), intent(inout) :: l4     ! The lists
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_parse_listi4
  end interface
  !
  interface
    subroutine sic_parse_listi8(rname,chain,l,mlist,error)
      use gbl_message
      use sic_types
      !----------------------------------------------------------------------
      ! @ public
      !  Decode a list as integer*8 values
      !----------------------------------------------------------------------
      character(len=*),   intent(in)    :: rname  ! Procedure name
      character(len=*),   intent(in)    :: chain  ! Input line to be decoded
      type(sic_listi8_t), intent(inout) :: l      ! The lists
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_parse_listi8
  end interface
  !
  interface
    subroutine sic_parse_listr8(rname,chain,l,mlist,error)
      use gbl_message
      use sic_types
      !----------------------------------------------------------------------
      ! @ public
      !  Decode a list as real*8 values
      !----------------------------------------------------------------------
      character(len=*),   intent(in)    :: rname  ! Procedure name
      character(len=*),   intent(in)    :: chain  ! Input line to be decoded
      type(sic_listr8_t), intent(inout) :: l      ! The lists
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_parse_listr8
  end interface
  !
  interface
    subroutine sic_build_listr4(levels,nlev,mlev,chain,rname,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Decode a list of numbers in the following format
      !    (CHAIN =) n1 n2 n3 TO n4 n5 n6 n7 TO n8 BY n9
      !  The list will contain
      !    n1,n2, n3,n3+1,...,n4, n5, n6,n6+n9,n6+2*n9,...,n7
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: mlev          ! Size of levels array
      real(kind=4),     intent(out)   :: levels(mlev)  ! Array to receive the list of values
      integer(kind=4),  intent(out)   :: nlev          ! Number of values
      character(len=*), intent(in)    :: chain         ! Input line to be decoded
      character(len=*), intent(in)    :: rname         ! Procedure name
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine sic_build_listr4
  end interface
  !
  interface
    subroutine sic_build_listi4 (level,nlev,mlev,chain,rname,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Decode a list of numbers in the following format
      !    (CHAIN =) n1 n2 n3 TO n4 n5 n6 n7 TO n8 BY n9
      !  The list will contain
      !    n1,n2, n3,n3+1,...,n4, n5, n6,n6+n9,n6+2*n9,...,n7
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: mlev         ! Size of LEVEL array
      integer(kind=4),  intent(out)   :: level(mlev)  ! Array to receive the list of values
      integer(kind=4),  intent(out)   :: nlev         ! Number of values
      character(len=*), intent(in)    :: chain        ! String to be decoded
      character(len=*), intent(in)    :: rname        ! Header of message
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_build_listi4
  end interface
  !
  interface
    subroutine sic_allocate_listi4(l,mlist,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Allocate the input sic_list type.
      ! No deallocation subroutine, Fortran ensure that allocatable arrays
      ! are implicitly deallocated before destruction.
      !---------------------------------------------------------------------
      type(sic_listi4_t), intent(inout) :: l      !
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_allocate_listi4
  end interface
  !
  interface
    subroutine sic_allocate_listi8(l,mlist,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Allocate the input sic_list type.
      ! No deallocation subroutine, Fortran ensure that allocatable arrays
      ! are implicitly deallocated before destruction.
      !---------------------------------------------------------------------
      type(sic_listi8_t), intent(inout) :: l      !
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_allocate_listi8
  end interface
  !
  interface
    subroutine sic_allocate_listr8(l,mlist,error)
      use gbl_message
      use sic_types
      !---------------------------------------------------------------------
      ! @ public
      !  Allocate the input sic_list type.
      ! No deallocation subroutine, Fortran ensure that allocatable arrays
      ! are implicitly deallocated before destruction.
      !---------------------------------------------------------------------
      type(sic_listr8_t), intent(inout) :: l      !
      integer(kind=4),    intent(in)    :: mlist  ! Maximum number of lists
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine sic_allocate_listr8
  end interface
  !
  interface
    subroutine sic_math_logi (chain,nch,logi,error)
      use gildas_def
      use sic_expressions
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Decode a string as a logical argument :
      !  Attempt to read it
      !  Find if it is a variable
      !  Compute logical expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      logical,          intent(out)   :: logi   !
      logical,          intent(inout) :: error  !
    end subroutine sic_math_logi
  end interface
  !
  interface
    subroutine sic_math_long (chain,nch,argum,error)
      use gildas_def
      use sic_expressions
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Decode a string as a long integer :
      !  Attempt to read it
      !  Find if it is a variable
      !  Compute arithmetic expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      integer(kind=8),  intent(out)   :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_math_long
  end interface
  !
  interface
    subroutine sic_math_inte (chain,nch,argum,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Decode a string as a normal integer :
      !  Attempt to read it
      !  Find if it is a variable
      !  Compute arithmetic expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      integer(kind=4),  intent(out)   :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_math_inte
  end interface
  !
  interface
    subroutine sic_math_dble (chain,nch,argum,error)
      use gildas_def
      use sic_expressions
      use sic_types
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Decode a string as a double precision float :
      !  Attempt to read it
      !  Find if it is a variable
      ! Compute arithmetic expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      real(kind=8),     intent(out)   :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_math_dble
  end interface
  !
  interface
    subroutine sic_math_real (chain,nch,argum,error)
      !---------------------------------------------------------------------
      ! @ public
      ! Decode a string as a single precision float :
      !  Attempt to read it
      !  Find if it is a variable
      ! Compute arithmetic expressions.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      real(kind=4),     intent(out)   :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_math_real
  end interface
  !
  interface
    subroutine sic_math (chain,nch,argum,error)
      !---------------------------------------------------------------------
      ! @ public obsolete
      !  Use one of the following routines to evaluate a Sic expression
      ! in a variable:
      !   sic_math_logi
      !   sic_math_inte
      !   sic_math_long
      !   sic_math_real
      !   sic_math_dble
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: chain  !
      integer(kind=4),  intent(in)    :: nch    !
      real(kind=8),     intent(out)   :: argum  !
      logical,          intent(inout) :: error  !
    end subroutine sic_math
  end interface
  !
  interface
    subroutine sic_sexa(line,nline,value,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Decode a generalized sexagesimal notation
      !  Expression_1[:Expression_2[:Expression_3]]
      !  The unit of Expression_1 (Degrees or hours) is used for the result.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      integer(kind=4),  intent(in)    :: nline  !
      real(kind=8),     intent(out)   :: value  !
      logical,          intent(inout) :: error  !
    end subroutine sic_sexa
  end interface
  !
  interface
    subroutine init_adjust
      use sic_adjust
      !---------------------------------------------------------------------
      ! @ public
      ! Initialize the ADJUST Language
      !---------------------------------------------------------------------
      ! Local
    end subroutine init_adjust
  end interface
  !
  interface
    subroutine sic_libr(buffer,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Execute a restricted set of SIC commands in "library" mode
      !   No commands with level change allowed here
      !---------------------------------------------------------------------
      character(len=*)                :: buffer  ! Command line
      logical,          intent(inout) :: error   ! Error flag
    end subroutine sic_libr
  end interface
  !
  interface
    subroutine gprompt_set(newprompt)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !  Modify the prompt, the base prompt, and the master prompt
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: newprompt
    end subroutine gprompt_set
  end interface
  !
  interface
    subroutine gprompt_base_set(newbase)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !  Modify the prompt and the base prompt, do not modify the master
      ! prompt.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: newbase
    end subroutine gprompt_base_set
  end interface
  !
  interface
    subroutine sic_opt(pr,log,mem)
      use gpack_def
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public (but obsolete in the context of new inits (at least since
      !   2008-04-11))
      ! Please use instead:
      !  - gprompt_set(pr): to set/modify the prompt
      !  - sic_open_log(log,error): to open the log file
      !  - no routine provided to turn ON|OFF the memory stack (default ON
      !    at startup, then user can call SIC\SIC MEMORY ON|OFF
      !---------------------------------------------------------------------
      ! SIC   External routine
      !       Modify the prompt and logfile logical name and status of stack
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: pr   ! The prompt
      character(len=*), intent(in) :: log  ! The log file name
      logical,          intent(in) :: mem  ! Status of stack editor
    end subroutine sic_opt
  end interface
  !
  interface
    subroutine sic_pack_set(pack)
      use gpack_def
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine sic_pack_set
  end interface
  !
#if defined(GAG_USE_PYTHON)
  interface
    function sic_varlevel()
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      ! Retrieve current execution level
      !---------------------------------------------------------------------
      integer :: sic_varlevel  ! Function value on return
    end function sic_varlevel
  end interface
  !
  interface
    function sic_dictsize()
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      ! Retrieve size of SIC hashing list (dictionary)
      !---------------------------------------------------------------------
      integer :: sic_dictsize  ! Function value on return
    end function sic_dictsize
  end interface
  !
  interface
    subroutine sic_typecodes(fmt_r4_out,fmt_r8_out,fmt_i4_out,fmt_i8_out,  &
      fmt_l_out,fmt_by_out,fmt_c4_out)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      ! Retrieve SIC type codes
      !---------------------------------------------------------------------
      integer, intent(out) :: fmt_r4_out  !
      integer, intent(out) :: fmt_r8_out  !
      integer, intent(out) :: fmt_i4_out  !
      integer, intent(out) :: fmt_i8_out  !
      integer, intent(out) :: fmt_l_out   !
      integer, intent(out) :: fmt_by_out  !
      integer, intent(out) :: fmt_c4_out  !
    end subroutine sic_typecodes
  end interface
  !
  interface
    subroutine sic_descptr(varname,level,address)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_types
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      ! Gives back descriptor memory address, given the SIC name.
      ! It is called once or twice, but only at variable initialization.
      ! Return address = -1 means that variable was not found (can be an
      ! error or not depending on the caller).
      !---------------------------------------------------------------------
      character(len=*),             intent(in)  :: varname  !
      integer,                      intent(in)  :: level    !
      integer(kind=address_length), intent(out) :: address  !
    end subroutine sic_descptr
  end interface
  !
  interface
    subroutine sic_getname(varnum,varname,namelength,varlevel)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public (for Python port)
      ! Retrieve variable name from its number in dictionary
      !---------------------------------------------------------------------
      integer,                       intent(in)  :: varnum      !
      character(len=varname_length), intent(out) :: varname     !
      integer,                       intent(out) :: namelength  !
      integer,                       intent(out) :: varlevel    !
    end subroutine sic_getname
  end interface
  !
#else
#endif
  interface
    subroutine sicapropos
      use sic_dictionaries
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Display the versions of various elements of the Gildas executables
      !---------------------------------------------------------------------
      ! Local
    end subroutine sicapropos
  end interface
  !
  interface
    subroutine sic_debug_system(switch)
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: switch
    end subroutine sic_debug_system
  end interface
  !
  interface
    subroutine sic_user(user)
      !---------------------------------------------------------------------
      ! @ public
      ! Return "user[@host]" in output string
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: user  ! Output string
    end subroutine sic_user
  end interface
  !
  interface
    subroutine sic_run(line,lang,command,error,icode,ocode)
      use gildas_def
      use sic_structures
      use sic_dictionaries
      use sic_interactions
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (but private in the context of new language initialization)
      !       Read next command and interprets it, then return
      !       control to the caller.
      !
      ! Arguments:
      ! LINE:  Command line. On input: text to be stored in stack and log
      !        file. On output: new command line
      ! ERROR: Logical error flag, indicating whether last command was
      !        successfully executed.
      !
      ! Note on labels in the code
      !       10      Error handling after command execution
      !       20      Read new command
      !       30      Check for error in command reading and level changes
      !       40      Substitute symbols and analyse line
      !       50      Log file after command execution
      !       70      DO WHILE in QUIT ALL
      !       80      DO WHILE in RETURN BASE
      !       90      DO WHILE in EXIT
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line     ! Input/output command line
      character(len=*), intent(out)   :: lang     ! Language found
      character(len=*), intent(out)   :: command  ! Command found
      logical,          intent(inout) :: error    ! Logical error flag
      integer,          intent(in)    :: icode    ! Input code
      integer,          intent(out)   :: ocode    ! Output code
    end subroutine sic_run
  end interface
  !
  interface
    subroutine sic_insert (line)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !       Insert a command line in the stack if possible.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! Command line
    end subroutine sic_insert
  end interface
  !
  interface
    subroutine sic_setsymbol(symb,tran,error)
      use gbl_message
      use sic_dictionaries
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !   Add a new entry in the SIC symbol table
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: symb   ! Symbol name
      character(len=*), intent(in)  :: tran   ! Symbol translation
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine sic_setsymbol
  end interface
  !
  interface
    subroutine sic_getsymbol(symb,tran,error)
      use sic_dictionaries
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      ! Get a symbol definition from the SIC symbol table
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb   ! Symbol name
      character(len=*), intent(out)   :: tran   ! Symbol translation
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine sic_getsymbol
  end interface
  !
  interface
    subroutine sic_let_real (var,value,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      real*4                          :: value  !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_real
  end interface
  !
  interface
    subroutine sic_let_dble (var,dble,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      real*8                          :: dble   !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_dble
  end interface
  !
  interface
    subroutine sic_let_inte (var,inte,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      integer*4                       :: inte   !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_inte
  end interface
  !
  interface
    subroutine sic_let_long (var,long,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      integer*8                       :: long   !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_long
  end interface
  !
  interface
    subroutine sic_let_logi (var,logi,error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var    !
      logical                         :: logi   !
      logical,          intent(inout) :: error  !
    end subroutine sic_let_logi
  end interface
  !
  interface
    subroutine sic_let_char (var,string,error)
      !---------------------------------------------------------------------
      ! @ public
      !  Attributes a value to an already defined variable.
      !  No data type checking for this entry. Only Global variables are
      ! affected by this entry, since it is called by program only.
      !---------------------------------------------------------------------
      character(len=*)                :: var     !
      character(len=*)                :: string  !
      logical,          intent(inout) :: error   !
    end subroutine sic_let_char
  end interface
  !
  interface
    subroutine sic_def_logi (symb,logi,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !  Define a LOGICAL scalar Sic variable
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      logical,          intent(in)    :: logi      ! Target scalar
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_logi
  end interface
  !
  interface
    subroutine sic_def_char(symb,chain,readonly,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Define a CHARACTER scalar Sic variable
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      character(len=*), intent(in)    :: chain     ! Target scalar string
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_char
  end interface
  !
  interface
    subroutine sic_def_charpnt(symb,vaddr,vsize,ndim,dim,readonly,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Define a CHARACTER array Sic variable
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: symb       ! Variable name
      integer(kind=address_length), intent(in)    :: vaddr      ! Target address
      integer(kind=4),              intent(in)    :: vsize      ! Size of each string
      integer(kind=4),              intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),   intent(in)    :: dim(ndim)  ! Dimensions
      logical,                      intent(in)    :: readonly   ! Read only?
      logical,                      intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_charpnt
  end interface
  !
  interface sic_def_charn
    subroutine sic_def_charn_0d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      character(len=*), intent(in)    :: chain     ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_charn_0d_1i4
    subroutine sic_def_charn_0d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      character(len=*), intent(in)    :: chain     ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_charn_0d_1i8
    subroutine sic_def_charn_0d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      character(len=*),            intent(in)    :: chain      ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_charn_0d_nil
    subroutine sic_def_charn_1d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      character(len=*), intent(in)    :: chain(:)  ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_charn_1d_1i4
    subroutine sic_def_charn_1d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      character(len=*), intent(in)    :: chain(:)  ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_charn_1d_1i8
    subroutine sic_def_charn_1d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      character(len=*),            intent(in)    :: chain(:)   ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_charn_1d_nil
    subroutine sic_def_charn_2d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb        ! Variable name
      character(len=*), intent(in)    :: chain(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim         ! Dimensions
      logical,          intent(in)    :: readonly    ! Read only?
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_charn_2d_1i4
    subroutine sic_def_charn_2d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb        ! Variable name
      character(len=*), intent(in)    :: chain(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim         ! Dimensions
      logical,          intent(in)    :: readonly    ! Read only?
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_charn_2d_1i8
    subroutine sic_def_charn_2d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb        ! Variable name
      character(len=*),            intent(in)    :: chain(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim        ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)   ! Dimensions
      logical,                     intent(in)    :: readonly    ! Read only?
      logical,                     intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_charn_2d_nil
    subroutine sic_def_charn_3d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb          ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim           ! Dimensions
      logical,          intent(in)    :: readonly      ! Read only?
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_charn_3d_1i4
    subroutine sic_def_charn_3d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb          ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim           ! Dimensions
      logical,          intent(in)    :: readonly      ! Read only?
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_charn_3d_1i8
    subroutine sic_def_charn_3d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb          ! Variable name
      character(len=*),            intent(in)    :: chain(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim          ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)     ! Dimensions
      logical,                     intent(in)    :: readonly      ! Read only?
      logical,                     intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_charn_3d_nil
    subroutine sic_def_charn_4d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb            ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim             ! Dimensions
      logical,          intent(in)    :: readonly        ! Read only?
      logical,          intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_charn_4d_1i4
    subroutine sic_def_charn_4d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb            ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim             ! Dimensions
      logical,          intent(in)    :: readonly        ! Read only?
      logical,          intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_charn_4d_1i8
    subroutine sic_def_charn_4d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb            ! Variable name
      character(len=*),            intent(in)    :: chain(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim            ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)       ! Dimensions
      logical,                     intent(in)    :: readonly        ! Read only?
      logical,                     intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_charn_4d_nil
    subroutine sic_def_charn_5d_1i4(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb              ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim              ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim               ! Dimensions
      logical,          intent(in)    :: readonly          ! Read only?
      logical,          intent(inout) :: error             ! Logical error flag
    end subroutine sic_def_charn_5d_1i4
    subroutine sic_def_charn_5d_1i8(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb              ! Variable name
      character(len=*), intent(in)    :: chain(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim              ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim               ! Dimensions
      logical,          intent(in)    :: readonly          ! Read only?
      logical,          intent(inout) :: error             ! Logical error flag
    end subroutine sic_def_charn_5d_1i8
    subroutine sic_def_charn_5d_nil(symb,chain,ndim,dim,readonly,error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_charn
      !  Define a CHARACTER array Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb              ! Variable name
      character(len=*),            intent(in)    :: chain(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim              ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)         ! Dimensions
      logical,                     intent(in)    :: readonly          ! Read only?
      logical,                     intent(inout) :: error             ! Logical error flag
    end subroutine sic_def_charn_5d_nil
  end interface sic_def_charn
  !
  interface sic_def_cplx
    subroutine sic_def_cplx_0d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      complex(kind=4),  intent(in)    :: cplx      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_cplx_0d_1i4
    subroutine sic_def_cplx_0d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      complex(kind=4),  intent(in)    :: cplx      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_cplx_0d_1i8
    subroutine sic_def_cplx_0d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      complex(kind=4),             intent(in)    :: cplx       ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_cplx_0d_nil
    subroutine sic_def_cplx_1d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_cplx_1d_1i4
    subroutine sic_def_cplx_1d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_cplx_1d_1i8
    subroutine sic_def_cplx_1d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      complex(kind=4),             intent(in)    :: cplx(:)    ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_cplx_1d_nil
    subroutine sic_def_cplx_2d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_cplx_2d_1i4
    subroutine sic_def_cplx_2d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_cplx_2d_1i8
    subroutine sic_def_cplx_2d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      complex(kind=4),             intent(in)    :: cplx(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_cplx_2d_nil
    subroutine sic_def_cplx_3d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_cplx_3d_1i4
    subroutine sic_def_cplx_3d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_cplx_3d_1i8
    subroutine sic_def_cplx_3d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb         ! Variable name
      complex(kind=4),             intent(in)    :: cplx(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
      logical,                     intent(in)    :: readonly     ! Read only?
      logical,                     intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_cplx_3d_nil
    subroutine sic_def_cplx_4d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_cplx_4d_1i4
    subroutine sic_def_cplx_4d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_cplx_4d_1i8
    subroutine sic_def_cplx_4d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb           ! Variable name
      complex(kind=4),             intent(in)    :: cplx(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
      logical,                     intent(in)    :: readonly       ! Read only?
      logical,                     intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_cplx_4d_nil
    subroutine sic_def_cplx_5d_1i4(symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_cplx_5d_1i4
    subroutine sic_def_cplx_5d_1i8 (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      complex(kind=4),  intent(in)    :: cplx(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_cplx_5d_1i8
    subroutine sic_def_cplx_5d_nil (symb,cplx,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_cplx
      !  Define a COMPLEX Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb             ! Variable name
      complex(kind=4),             intent(in)    :: cplx(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
      logical,                     intent(in)    :: readonly         ! Read only?
      logical,                     intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_cplx_5d_nil
  end interface sic_def_cplx
  !
  interface sic_def_dble
    subroutine sic_def_dble_0d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=8),     intent(in)    :: dble      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_dble_0d_1i4
    subroutine sic_def_dble_0d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=8),     intent(in)    :: dble      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_dble_0d_1i8
    subroutine sic_def_dble_0d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=8),                intent(in)    :: dble       ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_dble_0d_nil
    subroutine sic_def_dble_1d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=8),     intent(in)    :: dble(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_dble_1d_1i4
    subroutine sic_def_dble_1d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=8),     intent(in)    :: dble(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_dble_1d_1i8
    subroutine sic_def_dble_1d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=8),                intent(in)    :: dble(:)    ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_dble_1d_nil
    subroutine sic_def_dble_2d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_dble_2d_1i4
    subroutine sic_def_dble_2d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_dble_2d_1i8
    subroutine sic_def_dble_2d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=8),                intent(in)    :: dble(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_dble_2d_nil
    subroutine sic_def_dble_3d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_dble_3d_1i4
    subroutine sic_def_dble_3d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_dble_3d_1i8
    subroutine sic_def_dble_3d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb         ! Variable name
      real(kind=8),                intent(in)    :: dble(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
      logical,                     intent(in)    :: readonly     ! Read only?
      logical,                     intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_dble_3d_nil
    subroutine sic_def_dble_4d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_dble_4d_1i4
    subroutine sic_def_dble_4d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_dble_4d_1i8
    subroutine sic_def_dble_4d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb           ! Variable name
      real(kind=8),                intent(in)    :: dble(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
      logical,                     intent(in)    :: readonly       ! Read only?
      logical,                     intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_dble_4d_nil
    subroutine sic_def_dble_5d_1i4(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_dble_5d_1i4
    subroutine sic_def_dble_5d_1i8(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      real(kind=8),     intent(in)    :: dble(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_dble_5d_1i8
    subroutine sic_def_dble_5d_nil(symb,dble,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_dble
      !  Define a DOUBLE Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb             ! Variable name
      real(kind=8),                intent(in)    :: dble(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
      logical,                     intent(in)    :: readonly         ! Read only?
      logical,                     intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_dble_5d_nil
  end interface sic_def_dble
  !
  interface sic_def_inte
    subroutine sic_def_inte_0d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=4),  intent(in)    :: inte      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_inte_0d_1i4
    subroutine sic_def_inte_0d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=4),  intent(in)    :: inte      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_inte_0d_1i8
    subroutine sic_def_inte_0d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=4),             intent(in)    :: inte       ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_inte_0d_nil
    subroutine sic_def_inte_1d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=4),  intent(in)    :: inte(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_inte_1d_1i4
    subroutine sic_def_inte_1d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=4),  intent(in)    :: inte(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_inte_1d_1i8
    subroutine sic_def_inte_1d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=4),             intent(in)    :: inte(:)    ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_inte_1d_nil
    subroutine sic_def_inte_2d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_inte_2d_1i4
    subroutine sic_def_inte_2d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_inte_2d_1i8
    subroutine sic_def_inte_2d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=4),             intent(in)    :: inte(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_inte_2d_nil
    subroutine sic_def_inte_3d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_inte_3d_1i4
    subroutine sic_def_inte_3d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_inte_3d_1i8
    subroutine sic_def_inte_3d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb         ! Variable name
      integer(kind=4),             intent(in)    :: inte(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
      logical,                     intent(in)    :: readonly     ! Read only?
      logical,                     intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_inte_3d_nil
    subroutine sic_def_inte_4d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_inte_4d_1i4
    subroutine sic_def_inte_4d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_inte_4d_1i8
    subroutine sic_def_inte_4d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb           ! Variable name
      integer(kind=4),             intent(in)    :: inte(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
      logical,                     intent(in)    :: readonly       ! Read only?
      logical,                     intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_inte_4d_nil
    subroutine sic_def_inte_5d_1i4(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_inte_5d_1i4
    subroutine sic_def_inte_5d_1i8(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      integer(kind=4),  intent(in)    :: inte(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_inte_5d_1i8
    subroutine sic_def_inte_5d_nil(symb,inte,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_inte
      !  Define an INTEGER Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb             ! Variable name
      integer(kind=4),             intent(in)    :: inte(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
      logical,                     intent(in)    :: readonly         ! Read only?
      logical,                     intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_inte_5d_nil
  end interface sic_def_inte
  !
  interface sic_def_login
    subroutine sic_def_login_0d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      logical,          intent(in)    :: logi      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_login_0d_1i4
    subroutine sic_def_login_0d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      logical,          intent(in)    :: logi      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_login_0d_1i8
    subroutine sic_def_login_0d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      logical,                     intent(in)    :: logi       ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_login_0d_nil
    subroutine sic_def_login_1d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      logical,          intent(in)    :: logi(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_login_1d_1i4
    subroutine sic_def_login_1d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      logical,          intent(in)    :: logi(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_login_1d_1i8
    subroutine sic_def_login_1d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      logical,                     intent(in)    :: logi(:)    ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_login_1d_nil
    subroutine sic_def_login_2d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      logical,          intent(in)    :: logi(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_login_2d_1i4
    subroutine sic_def_login_2d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      logical,          intent(in)    :: logi(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_login_2d_1i8
    subroutine sic_def_login_2d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      logical,                     intent(in)    :: logi(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_login_2d_nil
    subroutine sic_def_login_3d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      logical,          intent(in)    :: logi(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_login_3d_1i4
    subroutine sic_def_login_3d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      logical,          intent(in)    :: logi(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_login_3d_1i8
    subroutine sic_def_login_3d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb         ! Variable name
      logical,                     intent(in)    :: logi(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
      logical,                     intent(in)    :: readonly     ! Read only?
      logical,                     intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_login_3d_nil
    subroutine sic_def_login_4d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      logical,          intent(in)    :: logi(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_login_4d_1i4
    subroutine sic_def_login_4d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      logical,          intent(in)    :: logi(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_login_4d_1i8
    subroutine sic_def_login_4d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb           ! Variable name
      logical,                     intent(in)    :: logi(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
      logical,                     intent(in)    :: readonly       ! Read only?
      logical,                     intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_login_4d_nil
    subroutine sic_def_login_5d_1i4(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      logical,          intent(in)    :: logi(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_login_5d_1i4
    subroutine sic_def_login_5d_1i8(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      logical,          intent(in)    :: logi(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_login_5d_1i8
    subroutine sic_def_login_5d_nil(symb,logi,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_login
      !  Define a LOGICAL array Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb             ! Variable name
      logical,                     intent(in)    :: logi(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
      logical,                     intent(in)    :: readonly         ! Read only?
      logical,                     intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_login_5d_nil
  end interface sic_def_login
  !
  interface sic_def_long
    subroutine sic_def_long_0d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=8),  intent(in)    :: long      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_long_0d_1i4
    subroutine sic_def_long_0d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=8),  intent(in)    :: long      ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_long_0d_1i8
    subroutine sic_def_long_0d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=8),             intent(in)    :: long       ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_long_0d_nil
    subroutine sic_def_long_1d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=8),  intent(in)    :: long(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_long_1d_1i4
    subroutine sic_def_long_1d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      integer(kind=8),  intent(in)    :: long(:)   ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_long_1d_1i8
    subroutine sic_def_long_1d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=8),             intent(in)    :: long(:)    ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_long_1d_nil
    subroutine sic_def_long_2d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_long_2d_1i4
    subroutine sic_def_long_2d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb       ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim       ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim        ! Dimensions
      logical,          intent(in)    :: readonly   ! Read only?
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_long_2d_1i8
    subroutine sic_def_long_2d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      integer(kind=8),             intent(in)    :: long(:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_long_2d_nil
    subroutine sic_def_long_3d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_long_3d_1i4
    subroutine sic_def_long_3d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb         ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim         ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim          ! Dimensions
      logical,          intent(in)    :: readonly     ! Read only?
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_long_3d_1i8
    subroutine sic_def_long_3d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb         ! Variable name
      integer(kind=8),             intent(in)    :: long(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim         ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)    ! Dimensions
      logical,                     intent(in)    :: readonly     ! Read only?
      logical,                     intent(inout) :: error        ! Logical error flag
    end subroutine sic_def_long_3d_nil
    subroutine sic_def_long_4d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_long_4d_1i4
    subroutine sic_def_long_4d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb           ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim           ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim            ! Dimensions
      logical,          intent(in)    :: readonly       ! Read only?
      logical,          intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_long_4d_1i8
    subroutine sic_def_long_4d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb           ! Variable name
      integer(kind=8),             intent(in)    :: long(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim           ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)      ! Dimensions
      logical,                     intent(in)    :: readonly       ! Read only?
      logical,                     intent(inout) :: error          ! Logical error flag
    end subroutine sic_def_long_4d_nil
    subroutine sic_def_long_5d_1i4(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_long_5d_1i4
    subroutine sic_def_long_5d_1i8(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb             ! Variable name
      integer(kind=8),  intent(in)    :: long(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim             ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim              ! Dimensions
      logical,          intent(in)    :: readonly         ! Read only?
      logical,          intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_long_5d_1i8
    subroutine sic_def_long_5d_nil(symb,long,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_long
      !  Define a LONG Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb             ! Variable name
      integer(kind=8),             intent(in)    :: long(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim             ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)        ! Dimensions
      logical,                     intent(in)    :: readonly         ! Read only?
      logical,                     intent(inout) :: error            ! Logical error flag
    end subroutine sic_def_long_5d_nil
  end interface sic_def_long
  !
  interface sic_def_real
    subroutine sic_def_real_0d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel       ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_0d_1i4
    subroutine sic_def_real_0d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  Scalar target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel       ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_0d_1i8
    subroutine sic_def_real_0d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  Scalar target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=4),                intent(in)    :: rel        ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_real_0d_nil
    subroutine sic_def_real_1d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel(:)    ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_1d_1i4
    subroutine sic_def_real_1d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  1D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel(:)    ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_1d_1i8
    subroutine sic_def_real_1d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  1D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=4),                intent(in)    :: rel(:)     ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_real_1d_nil
    subroutine sic_def_real_2d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_2d_1i4
    subroutine sic_def_real_2d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  2D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb      ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim      ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim       ! Dimensions
      logical,          intent(in)    :: readonly  ! Read only?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_def_real_2d_1i8
    subroutine sic_def_real_2d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  2D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb       ! Variable name
      real(kind=4),                intent(in)    :: rel(:,:)   ! Target
      integer(kind=4),             intent(in)    :: ndim       ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)  ! Dimensions
      logical,                     intent(in)    :: readonly   ! Read only?
      logical,                     intent(inout) :: error      ! Logical error flag
    end subroutine sic_def_real_2d_nil
    subroutine sic_def_real_3d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb        ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim         ! Dimensions
      logical,          intent(in)    :: readonly    ! Read only?
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_real_3d_1i4
    subroutine sic_def_real_3d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  3D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb        ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim        ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim         ! Dimensions
      logical,          intent(in)    :: readonly    ! Read only?
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_real_3d_1i8
    subroutine sic_def_real_3d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  3D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb        ! Variable name
      real(kind=4),                intent(in)    :: rel(:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim        ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)   ! Dimensions
      logical,                     intent(in)    :: readonly    ! Read only?
      logical,                     intent(inout) :: error       ! Logical error flag
    end subroutine sic_def_real_3d_nil
    subroutine sic_def_real_4d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb          ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim           ! Dimensions
      logical,          intent(in)    :: readonly      ! Read only?
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_real_4d_1i4
    subroutine sic_def_real_4d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  4D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb          ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim          ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim           ! Dimensions
      logical,          intent(in)    :: readonly      ! Read only?
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_real_4d_1i8
    subroutine sic_def_real_4d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  4D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb          ! Variable name
      real(kind=4),                intent(in)    :: rel(:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim          ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)     ! Dimensions
      logical,                     intent(in)    :: readonly      ! Read only?
      logical,                     intent(inout) :: error         ! Logical error flag
    end subroutine sic_def_real_4d_nil
    subroutine sic_def_real_5d_1i4(symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 4
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb            ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
      integer(kind=4),  intent(in)    :: dim             ! Dimensions
      logical,          intent(in)    :: readonly        ! Read only?
      logical,          intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_real_5d_1i4
    subroutine sic_def_real_5d_1i8 (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  5D target
      !  Dimension as a scalar integer of kind 8
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: symb            ! Variable name
      real(kind=4),     intent(in)    :: rel(:,:,:,:,:)  ! Target
      integer(kind=4),  intent(in)    :: ndim            ! Number of dimensions
      integer(kind=8),  intent(in)    :: dim             ! Dimensions
      logical,          intent(in)    :: readonly        ! Read only?
      logical,          intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_real_5d_1i8
    subroutine sic_def_real_5d_nil (symb,rel,ndim,dim,readonly,error)
      use gildas_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public-generic sic_def_real
      !  Define a REAL Sic variable
      !---
      !  5D target
      !  Dimensions as an integer array of kind index_length
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: symb            ! Variable name
      real(kind=4),                intent(in)    :: rel(:,:,:,:,:)  ! Target
      integer(kind=4),             intent(in)    :: ndim            ! Number of dimensions
      integer(kind=index_length),  intent(in)    :: dim(ndim)       ! Dimensions
      logical,                     intent(in)    :: readonly        ! Read only?
      logical,                     intent(inout) :: error           ! Logical error flag
    end subroutine sic_def_real_5d_nil
  end interface sic_def_real
  !
  interface
    subroutine run_vector (line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (but should be private in the context of new
      ! language initialization) (mandatory because symbol is used elsewhere)
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=*) :: comm          !
      logical :: error                  !
    end subroutine run_vector
  end interface
  !
  interface
    subroutine sic_wpr (invite,string)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !  Read a character string with prompt
      !  Traps the empty string
      !  Traps <^Z> and returns "EXIT" instead
      !---------------------------------------------------------------------
      character(len=*) :: invite        ! Prompt for reading              Input
      character(len=*) :: string        ! String read                     Output
    end subroutine sic_wpr
  end interface
  !
  interface
    subroutine sic_wprn (invite,string,n)
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      !  Read a character string with prompt
      !  An empty string or a <^Z> returns N=0
      !---------------------------------------------------------------------
      character(len=*) :: invite        ! Prompt for reading              Input
      character(len=*) :: string        ! String read                     Output
      integer :: n                      ! Number of characters read       Output
    end subroutine sic_wprn
  end interface
  !
  interface
    subroutine sic_log (line,nl,lire)
      use sic_structures
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  !
      integer(kind=4),  intent(in) :: nl    !
      integer(kind=4),  intent(in) :: lire  !
    end subroutine sic_log
  end interface
  !
  interface
    subroutine gmaster_set_logname(logname)
      !---------------------------------------------------------------------
      ! @ public
      ! Set logname instead of default (i.e. package name).
      !---------------------------------------------------------------------
      character(len=*) :: logname       !
    end subroutine gmaster_set_logname
  end interface
  !
  interface
    subroutine gmaster_set_message_file(message_file)
      !---------------------------------------------------------------------
      ! @ public
      ! Set logname instead of default (i.e. package name).
      !---------------------------------------------------------------------
      character(len=*) :: message_file  !
    end subroutine gmaster_set_message_file
  end interface
  !
  interface
    subroutine gmaster_set_log_file(log_file)
      !---------------------------------------------------------------------
      ! @ public
      ! Set logname instead of default (i.e. package name).
      !---------------------------------------------------------------------
      character(len=*) :: log_file      !
    end subroutine gmaster_set_log_file
  end interface
  !
  interface
    subroutine gmaster_append_date_on_logname
      !---------------------------------------------------------------------
      ! @ public
      ! Append date on logname
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_append_date_on_logname
  end interface
  !
  interface
    subroutine gmaster_set_prompt(prompt)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Set prompt instead of default (i.e. package name)
      !---------------------------------------------------------------------
      character(len=*) :: prompt        !
    end subroutine gmaster_set_prompt
  end interface
  !
  interface
    subroutine gmaster_set_display(display)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Set display instead of default
      !---------------------------------------------------------------------
      character(len=*) :: display       !
    end subroutine gmaster_set_display
  end interface
  !
  interface
    subroutine gmaster_set_disable_log
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      ! Disable the log files (.log and .mes)
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_disable_log
  end interface
  !
  interface
    subroutine gmaster_set_disable_gui
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      ! Disable gui
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_disable_gui
  end interface
  !
  interface
    subroutine gmaster_set_disable_menu
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Disable gui
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_disable_menu
  end interface
  !
  interface
    subroutine gmaster_set_hide_gui
      use gbl_message
      use sic_interactions
      !---------------------------------------------------------------------
      ! @ public
      ! Hide gui at startup
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_hide_gui
  end interface
  !
  interface
    function gmaster_hide_gui()
      !---------------------------------------------------------------------
      ! @ public
      ! return "hide gui" property
      !---------------------------------------------------------------------
      logical :: gmaster_hide_gui       !
    end function gmaster_hide_gui
  end interface
  !
  interface
    subroutine gmaster_set_hide_welcome
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Hide welcome at startup
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_hide_welcome
  end interface
  !
  interface
    subroutine gmaster_set_no_welcome_procedure
      !---------------------------------------------------------------------
      ! @ public
      ! Hide welcome at startup
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_no_welcome_procedure
  end interface
  !
  interface
    subroutine gmaster_set_use_debug
      !---------------------------------------------------------------------
      ! @ public
      ! Enable display of debug messages
      !---------------------------------------------------------------------
      !
    end subroutine gmaster_set_use_debug
  end interface
  !
  interface
    subroutine gmaster_get_command_line(command_line)
      !---------------------------------------------------------------------
      ! @ public
      ! Get input command line
      !---------------------------------------------------------------------
      character(len=*) , intent(out) :: command_line  !
    end subroutine gmaster_get_command_line
  end interface
  !
  interface
    subroutine gmaster_raw_import(master,pack_set,debug,error)
      !---------------------------------------------------------------------
      ! @ public
      ! Imports a package, and returns package registration id
      !---------------------------------------------------------------------
      logical, intent(in)    :: master    ! Import master package
      external               :: pack_set  ! Package definition method
      logical, intent(in)    :: debug     ! Debug mode
      logical, intent(inout) :: error     !
    end subroutine gmaster_raw_import
  end interface
  !
  interface
    subroutine gmaster_build(pack_set,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (should be private!)
      ! Build master package
      !---------------------------------------------------------------------
      external               :: pack_set  ! Package definition routine
      logical, intent(inout) :: error     ! Error code
    end subroutine gmaster_build
  end interface
  !
  interface
    subroutine gmaster_run(pack_set)
      use gpack_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Run master program
      !---------------------------------------------------------------------
      external :: pack_set              ! Package definition routine
    end subroutine gmaster_run
  end interface
  !
  interface
    subroutine gmaster_get_exitnoprompt(flag)
      !---------------------------------------------------------------------
      ! @ public
      ! Get the 'exit noprompt' flag. To be called by the exit subroutine of
      ! the program (and their dependent packages) to know if they are
      ! allowed to ask anything at exit time.
      !---------------------------------------------------------------------
      logical, intent(out) :: flag
    end subroutine gmaster_get_exitnoprompt
  end interface
  !
end module sic_interfaces_public
