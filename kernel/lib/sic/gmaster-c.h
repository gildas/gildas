
#ifndef _GMASTER_C_H_
#define _GMASTER_C_H_

char *gmaster_c_get_prompt();
const char *gmaster_c_get_command_line();
void gmaster_launch_interpreter();

#endif /* _GMASTER_C_H_ */

