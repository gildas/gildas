!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! EXPAND_MACRO:
!
!     SIC EXPAND infilename outfilename
!
!     Purpose: Add the langage name before each command of the macro
!              file "infilename" and write the results into the new
!              macro file "outfilename".
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine expand_macro(line,error)
  use sic_interfaces, except_this=>expand_macro
  use gbl_message
  use sic_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*)                :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=256) :: infile,outfile,chain
  character(len=1024) :: ligne,long,space
  character(len=16) :: command
  integer :: ier,nb,nc,nl,nk,lun1,lun2
  logical :: readonly=.true.
  logical :: readwrite=.false.
  character(len=10) :: rname = 'SIC_EXPAND'
  data space /' '/
  !
  call sic_ch(line,0,2,chain,nl,.true.,error)
  if (error) then
     call sic_message(seve%e,rname,'Input macro name mandatory')
     return
  endif
  call sic_parse_file(chain,' ','.pro',infile)
  ier = sic_getlun(lun1)
  ier = sic_open(lun1,infile,'OLD',readonly)
  if (ier.ne.0) then
     call putios('E-SIC_EXPAND,  ',ier)
     error = .true.
     return
  endif
  !
  call sic_ch(line,0,3,chain,nl,.true.,error)
  call sic_parse_file(chain,' ','.pro',outfile)
  if (error) then
     call sic_message(seve%e,rname,'Output macro name mandatory')
     return
  endif
  ier = sic_getlun(lun2)
  ier = sic_open(lun2,outfile,'NEW',readwrite)
  if (ier.ne.0) then
     call putios('E-SIC_EXPAND,  ',ier)
     error = .true.
     return
  endif
  rewind(lun2)
  !
  do while (.true.)
     nl = 0
     read(lun1,'(A)',iostat=ier) ligne
     if (ier.ne.0) exit
     nl = len_trim(ligne)
     if (nl.eq.0) then
        write(lun2,'(A)') '!'
     else
        ! nb: position where the line starts (ie first non-space character)
        ! nc: position where the line first word ends
        ! nk: First word length
        ! nl: Line length
        long = ' '
        nc = 1
        do while ((nc.le.nl).and.(ligne(nc:nc).eq.' '))
           nc = nc+1
        enddo
        nb = nc-1
        nk = 1
        do while ((nc.le.nl).and.(ligne(nc:nc).ne.' '))
           long(nk:nk) = ligne(nc:nc)
           nk = nk+1
           nc = nc+1
        enddo
        nk = nk-1
        call sic_upper(long)
!        print *,"ligne",ligne
!        print *,"long",long
        !
        ! Expand it
        error = .false.
        command = ' '
        if (long(1:1).eq.'!') then
           write (lun2,'(A)') ligne(1:nl)
        else
           call sic_find(command,long,nk,error)
!          print *,"command",command
!          print *,"long",long
           if (.not.error) then
              if (long(1:3).eq.'SIC') then
                 write (lun2,'(A)') ligne(1:nl)
              else
                 call sic_lower(long)
                 if (nb.ne.0) then
                    write (lun2,'(A,A,A)') space(1:nb), long(1:nk), ligne(nc:nl)
                 else
                    write (lun2,'(A,A)') long(1:nk), ligne(nc:nl)
                 endif
              endif
           else
              call sic_message(seve%e,rname,ligne(1:nl))
              write (lun2,'(A)') ligne(1:nl)
           endif
        endif
     endif
  enddo
  !
  close(unit=lun2)
  call sic_frelun(lun2)
  close(unit=lun1)
  call sic_frelun(lun1)
  error = .false.
end subroutine expand_macro
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
