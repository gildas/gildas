!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine import_package(line,error)
  use sic_dependencies_interfaces
  use gpack_def
  use sic_interfaces, except_this=>import_package
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! SIC Internal routine
  !    Support routine for command SIC\IMPORT. Syntax is:
  !    IMPORT foo [/DEBUG]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Error flag
  ! Local
  character(len=gpack_name_length) :: packname
  character(len=6) :: rname = 'IMPORT'
  integer :: pl,narg
  logical :: debug = .false.  ! Load library in debug mode?
  !
  narg = sic_narg(0)
  if (narg.ne.1) then
    call sic_message(seve%e,rname,'Command IMPORT needs 1 and only 1 argument')
    error = .true.
    return
  endif
  !
  ! Retrieve package name
  call sic_ch (line,0,1,packname,pl,.true.,error)
  if (error) return
  call sic_lower(packname)
  !
  if (sic_present(1,0)) then
    debug = .true.
  endif
  !
  ! Dynamic load the library
  call gpack_sic_import(packname,debug,error)
  !
end subroutine import_package
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
