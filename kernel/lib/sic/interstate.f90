subroutine sense_inter_state
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sense_inter_state
  use sic_interactions
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Sense automatically whether SIC has an interactive access or not.
  ! Action is different from sic_isatty() since there is also a special
  ! patch for XML widgets.
  ! + Compute some other global variables.
  !---------------------------------------------------------------------
  ! Local
  character(len=60) :: name
  integer(kind=4) :: ier
  !
  ! Special patch for XML widgets
  name = ''
  ier = sic_getlog('GAG_WIDGETS',name)
  if (name(1:3).ne."XML") then
    inter_state = sic_isatty().eq.1
  else
    inter_state = .true.      ! Force interstate for XML Widgets.
  endif
  !
  if (.not.inter_state) then
    edit_mode = .false.
    logbuf = .false.
    call gmessage_colors_swap(.false.)
    call sic_message(seve%w,'SIC',  &
      'Session is not interactive, SIC EDIT and SIC MEMORY off')
  else
    call find_edit  ! Check if line editing is possible
  endif
  !
  ! Determine prefered page editor
  ier = sic_getlog('GAG_EDIT',name)
  if (ier.ne.0) then
    tt_edit = 'AUTO'
  else
    tt_edit = name
  endif
  !
end subroutine sense_inter_state
!
subroutine set_inter_state(flag)
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ private
  ! Force the inter_state status.
  !---------------------------------------------------------------------
  logical, intent(in) :: flag
  !
  inter_state = flag
end subroutine set_inter_state
!
function sic_inter_state(dummy)
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  ! Return the inter_state status.
  !---------------------------------------------------------------------
  logical             :: sic_inter_state  ! Value on return
  integer, intent(in) :: dummy            ! Unused
  !
  sic_inter_state = inter_state
end function sic_inter_state
