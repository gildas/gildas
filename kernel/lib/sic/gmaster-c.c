
#include "gmaster-c.h"

#include <stdio.h>
#include "gsys/cfc.h"
#include "gcore/gcomm.h"
#include "gcore/gcommand_line.h"
#include "ggui/gkbd_line.h"
#include "getcar.h"
#include "ggui/gprompt.h"

#define gmaster_loop CFC_EXPORT_NAME(gmaster_loop)
void CFC_API gmaster_loop( );

static int s_gmaster_started = 0;

static int gmaster_start_loop( void *data)
{
    if (s_gmaster_started)
        return 0;

    s_gmaster_started = 1;
    gmaster_loop( );
    keyboard_exit_loop( );
    return 1;
}

#define gprompt_get CFC_EXPORT_NAME(gprompt_get)
void CFC_API gprompt_get( );

char *gmaster_c_get_prompt( )
{
    static char s_prompt[PROMPT + 1];
    CFC_DECLARE_LOCAL_STRING(prompt);
    CFC_STRING_VALUE(prompt) = s_prompt;
    CFC_STRING_LENGTH(prompt) = PROMPT;
    gprompt_get( prompt CFC_PASS_STRING_LENGTH(prompt));
    s_prompt[PROMPT] = '\0';
    CFC_suppressEndingSpaces( s_prompt);
    return s_prompt;
}

#define gmaster_enter_loop CFC_EXPORT_NAME(gmaster_enter_loop)
void CFC_API gmaster_enter_loop( CFC_FString command_line
                                 CFC_DECLARE_STRING_LENGTH(command_line))
{
    gmaster_start_loop( NULL);
}

void gmaster_launch_interpreter( )
{
    if (!s_gmaster_started) {
        sic_launch( gmaster_start_loop, NULL);
    }
}

