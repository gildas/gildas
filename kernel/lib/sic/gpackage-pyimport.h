/* Add this include file to define the Python module entry point named
   'initpyfoo' for package 'foo'. Used by command "import pyfoo" from
   Python */

#include <Python.h>
#include "gsys/cfc.h"

#define GPACK_LIB_NAME(package) "py"#package
#define GPACK_SET_FCT_NAME(package) CFC_EXPORT_NAME(name2(package,_pack_set))

/* Support for module extensions */

#ifdef GPACK_PYTHON_EXTENSIONS
#define GPACK_DECLARE_PYTHON_EXTENSIONS(package) void name3(py,package,_extensions) (PyObject *)
#define GPACK_CALL_PYTHON_EXTENSIONS(package) name3(py,package,_extensions) (modu)
#else
#define GPACK_DECLARE_PYTHON_EXTENSIONS(package)
#define GPACK_CALL_PYTHON_EXTENSIONS(package)
#endif

/* Macros for module entry point (Python 2 vs Python 3). Some of these macros
   are used by other hand made Python modules (e.g. pygildas.c) */

#ifndef PyMODINIT_FUNC  /* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif

#if PY_MAJOR_VERSION >= 3
    #define MOD_ERROR_VAL NULL
    #define MOD_SUCCESS_VAL(val) val
    #define MOD_TYPE PyMODINIT_FUNC
    #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
    #define MOD_DEF_DECL(name, methods) \
    static struct PyModuleDef moduledef = { \
        PyModuleDef_HEAD_INIT, name, NULL, -1, methods, };
    #define MOD_DEF_CODE(ob, name, doc, methods) \
        moduledef.m_doc = doc; ob = PyModule_Create(&moduledef);
#else
    #define MOD_ERROR_VAL
    #define MOD_SUCCESS_VAL(val)
    #define MOD_TYPE PyObject*
    #define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
    #define MOD_DEF_DECL(name, methods)
    #define MOD_DEF_CODE(ob, name, doc, methods) \
        ob = Py_InitModule3(name, methods, doc);
#endif

/* Actual module entry point */

#define GPACK_DEFINE_PYTHON_IMPORT(package)                                   \
                                                                              \
/**                                                                           \
 * Python import entry point                                                  \
 */                                                                           \
MOD_INIT(py##package)                                                         \
{                                                                             \
    PyObject *modu;                                                           \
    MOD_TYPE gpy_pack_import( );                                              \
    void CFC_API GPACK_SET_FCT_NAME(package)( );                              \
    GPACK_DECLARE_PYTHON_EXTENSIONS(package);                                 \
                                                                              \
    modu = gpy_pack_import( GPACK_SET_FCT_NAME(package),                      \
                            GPACK_LIB_NAME(package),                          \
                            "Please call welcome() method for module info");  \
                                                                              \
    GPACK_CALL_PYTHON_EXTENSIONS(package);                                    \
                                                                              \
    return MOD_SUCCESS_VAL(modu);                                             \
}
