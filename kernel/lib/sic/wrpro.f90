subroutine wordre(lun,line,nl)
  use sic_interfaces, except_this=>wordre
  !---------------------------------------------------------------------
  ! @ private
  ! SIC   Internal routine
  !
  !       Writes a command line according to SIC syntax
  !       LUN     Logical unit number
  !       LINE    Command line to write
  !       NL      Length of line
  !---------------------------------------------------------------------
  integer :: lun                    !
  character(len=*) :: line          !
  integer :: nl                     !
  ! Local
  integer, parameter :: length=70
  integer :: n1,n2
  !
  n1 = 1
  n2 = length
10 continue
  if (n2.ge.nl) then
    write(lun,100) line(n1:nl)
    return
  endif
  write(lun,100) line(n1:n2), '-'
  n1 = n2+1
  n2 = n2+length
  goto 10
100 format(1x,20(a))
end subroutine wordre
!
subroutine sic_wpr (invite,string)
  use sic_interfaces, except_this=>sic_wpr
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  !  Read a character string with prompt
  !  Traps the empty string
  !  Traps <^Z> and returns "EXIT" instead
  !---------------------------------------------------------------------
  character(len=*) :: invite        ! Prompt for reading              Input
  character(len=*) :: string        ! String read                     Output
  ! Local
  integer :: n,nc
  !
10 continue
  if (edit_mode.and.inter_state) then
    nc = len(invite)
    call get_line(string,n,invite,nc)
  else
    write(6,101) invite
    read (5,102,err=10,end=20) string
  endif
  if (string.eq.' ') goto 10
  goto 30
20 string='EXIT'
30 if (inter_state) return
  write(6,101) invite,trim(string)
101 format(1x,a,' ',$)
102 format(a)
end subroutine sic_wpr
!
subroutine sic_wprn (invite,string,n)
  use sic_interfaces, except_this=>sic_wprn
  use sic_interactions
  !---------------------------------------------------------------------
  ! @ public
  !  Read a character string with prompt
  !  An empty string or a <^Z> returns N=0
  !---------------------------------------------------------------------
  character(len=*) :: invite        ! Prompt for reading              Input
  character(len=*) :: string        ! String read                     Output
  integer :: n                      ! Number of characters read       Output
  ! Local
  integer :: nc
  !
  if (edit_mode.and.inter_state) then
    nc = len(invite)
    call get_line(string,n,invite,nc)
  else
    write(6,101) invite
    read (5,'(A)',err=10,end=10) string
    n = len_trim(string)
  endif
  if (string.eq.' ') goto 20
  if (inter_state) return
  write(6,101) invite,trim(string)
  return
  !
10 string=' '
20 n = 0
  !
101 format(1x,a,' ',$)
end subroutine sic_wprn
!
subroutine sic_log (line,nl,lire)
  use sic_interfaces, except_this=>sic_log
  use sic_structures
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line  !
  integer(kind=4),  intent(in) :: nl    !
  integer(kind=4),  intent(in) :: lire  !
  ! Local
  integer(kind=4) :: n1,n2
  integer(kind=4), parameter :: length=67
  !
  if (lire.ne.0) return
  if (lunlog.eq.0) return
  !
  n1 = 1
  n2 = length
  !
10 continue
  if (n2.ge.nl) then
    write(lunlog,101) line(n1:nl),nlire
    return
  endif
  write(lunlog,100) line(n1:n2), '-'
  n1 = n2+1
  n2 = n2+length
  goto 10
  !
100 format(20(a))
101 format(a,'      !',i2)
end subroutine sic_log
