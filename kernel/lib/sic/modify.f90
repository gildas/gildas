subroutine sic_modify(line,error)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_modify
  use sic_dictionaries
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    SIC\MODIFY  VarName|GDFName
  ! 1  /FREQUENCY Linename RestFreq
  ! 2  /VELOCITY Velocity
  ! 3  /SPECUNIT Unit
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='MODIFY'
  character(len=filename_length) :: name
  integer(kind=4) :: nc,ier,in
  type(sic_identifier_t) :: var
  !
  ! Command argument
  call sic_ch(line,0,1,name,nc,.true.,error)
  if (error) return
  !
  ! Guess if a Variable or a File
  var%name = name
  var%lname = len_trim(name)
  var%level = var_level
  ier = sic_hasfin(maxvar,pfvar,pnvar,dicvar,var,in)
  if (ier.ne.1) then
    var%level = 0
    ier = sic_hasfin(maxvar,pfvar,pnvar,dicvar,var,in)
  endif
  if (ier.eq.1) then
    ! Found a variable
    call sic_modify_variable(line,dicvar(in)%desc,error)
  else
    ! Assume a file, error will be raised if not
    call sic_modify_file(line,name,error)
  endif
  if (error)  return
  !
end subroutine sic_modify
!
subroutine sic_modify_variable(cline,desc,error)
  use gildas_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_modify_variable
  use sic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    SIC\MODIFY VarName
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: cline  !
  type(sic_descriptor_t), intent(inout) :: desc   !
  logical,                intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='MODIFY'
  !
  if (desc%readonly) then
    call sic_message(seve%e,rname,'Can not modify read-only variables')
    error = .true.
    return
  elseif (desc%status.le.0) then
    call sic_message(seve%e,rname,'Can only modify image variables')
    error = .true.
    return
  endif
  !
  call sic_modify_header(cline,desc%head,error)
  if (error)  return
  !
end subroutine sic_modify_variable
!
subroutine sic_modify_file(cline,filename,error)
  use image_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_modify_file
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    SIC\MODIFY GDFName
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: cline     !
  character(len=*), intent(in)    :: filename  !
  logical,          intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='MODIFY'
  type(gildas) :: x
  logical :: error2
  !
  call gildas_null(x)
  x%file = filename
  !
  call gdf_read_header(x,error)
  if (error)  return
  !
  call sic_modify_header(cline,x,error)
  if (error)  goto 100
  !
  call gdf_update_header(x,error)
  if (error)  goto 100
  !
100 continue
  error2 = .false.
  call gdf_close_image(x,error2)
  if (error2)  error = .true.
  !
end subroutine sic_modify_file
!
subroutine sic_modify_header(cline,x,error)
  use image_def
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_modify_header
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !  Modify consistently:
  !   - the line name
  !   - the rest frequency
  !   - the velocity
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: cline  !
  type(gildas),     intent(inout) :: x      !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='MODIFY'
  integer(kind=4), parameter :: optfreq=1
  integer(kind=4), parameter :: optvelo=2
  integer(kind=4), parameter :: optunit=3
  integer(kind=4) :: nc,iunit
  character(len=12) :: new_linename,argum,new_unit
  real(kind=8) :: new_restf
  real(kind=4) :: new_velo
  integer(kind=4), parameter :: nunit=2
  character(len=9), parameter :: unit(nunit) = (/ 'FREQUENCY','VELOCITY ' /)
  !
  ! /FREQUENCY arguments
  new_linename = x%char%line
  call sic_ch(cline,optfreq,1,new_linename,nc,.false.,error)
  if (error)  return
  new_restf = x%gil%freq
  call sic_r8(cline,optfreq,2,new_restf,.false.,error)
  if (error)  return
  !
  ! /VELOCITY arguments
  new_velo = x%gil%voff
  call sic_r4(cline,optvelo,1,new_velo,.false.,error)
  if (error)  return
  !
  ! /SPECUNIT. Default is unchanged unit.
  if (sic_present(optunit,0)) then
    if (x%gil%faxi.gt.0) then
      argum = x%char%code(x%gil%faxi)
    else
      ! No faxi will be rejected anyway by gdf_modify. Use any value
      argum = 'VELOCITY'
    endif
    call sic_ke(cline,optunit,1,argum,nc,.false.,error)
    if (error)  return
    call sic_ambigs(rname,argum,new_unit,iunit,unit,nunit,error)
    if (error)  return
    !
    call gdf_modify(x,new_velo,new_restf,new_unit,error)
    if (error)  return
  else
    call gdf_modify(x,new_velo,new_restf,error=error)
  endif
  ! New line name
  x%char%line = new_linename
  !
end subroutine sic_modify_header
