subroutine sic_grep(line,error)
  use gildas_def
  use sic_interfaces, except_this=>sic_grep
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    SIC GREP Pattern File
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GREP'
  character(len=128) :: pattern
  character(len=filename_length) :: file
  integer(kind=4) :: nl
  !
  call sic_ch(line,0,2,pattern,nl,.true.,error)
  if (error)  return
  !
  call sic_ch(line,0,3,file,nl,.true.,error)
  if (error)  return
  !
  call sic_grep_do(file,pattern,error)
  if (error)  return
  !
end subroutine sic_grep
!
subroutine sic_grep_do(filename,pattern,error)
  use gbl_message
  use sic_dependencies_interfaces
  use sic_interfaces, except_this=>sic_grep_do
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: filename
  character(len=*), intent(in)    :: pattern
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GREP'
  character(len=filename_length) :: file
  integer(kind=4) :: ier,lun
  !
  call sic_parse_file(filename,' ','',file)
  !
  ier = sic_getlun(lun)
  if (mod(ier,2).eq.0) then
    error = .true.
    return
  endif
  ier = sic_open(lun,file,'OLD',.true.)
  if (ier.ne.0) then
    call sic_message(seve%e,rname,'Error opening '//file)
    call putios('E-GREP,  ',ier)
    close(unit=lun)
    call sic_frelun(lun)
    error = .true.
    return
  endif
  !
  call sic_grep_engine(lun,pattern,error)
  if (error)  continue
  !
  ier = sic_close(lun)
  call sic_frelun(lun)
  !
end subroutine sic_grep_do
!
subroutine sic_grep_engine(lun,pattern,error)
  use gbl_message
  use sic_interfaces, except_this=>sic_grep_engine
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)    :: lun
  character(len=*), intent(in)    :: pattern
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GREP'
  integer(kind=4) :: iline,ier
  integer(kind=4), parameter :: line_length=1024
  character(len=line_length) :: line
  character(len=message_length) :: mess
  ! Saved
  character(len=line_length), allocatable, save :: lines(:)
  integer(kind=4), save :: nmatch
  logical, save :: first=.true.
  !
  if (first) then
    call sic_defstructure('GREP',.true.,error)
    if (error)  return
    call sic_def_inte('GREP%N',nmatch,0,0,.true.,error)
    if (error)  return
    first = .false.
  endif
  call sic_delvariable('GREP%LINES',.false.,error)
  if (error)  error = .false.
  !
  iline = 0
  nmatch = 0
  do
    iline = iline+1
    read(lun,'(A)',iostat=ier)  line
    if (ier.lt.0)  exit
    if (ier.gt.0) then
      write(mess,'(A,I0)')  'Error reading file at line ',iline
      call sic_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    if (index(line,trim(pattern)).le.0)  cycle
    nmatch = nmatch+1
    call reallocate_lines(lines,nmatch,error)
    if (error)  return
    lines(nmatch) = line
  enddo
  !
  if (nmatch.gt.0) then
    call sic_def_charn('GREP%LINES',lines,1,nmatch,.true.,error)
    if (error)  return
  endif
  !
contains
  !
  subroutine reallocate_lines(list,rsize,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    character(len=*), allocatable   :: list(:)
    integer(kind=4),  intent(in)    :: rsize  ! Requested size
    logical,          intent(inout) :: error
    !
    integer(kind=4), parameter :: lines_min_alloc=10
    character(len=line_length), allocatable :: tmp(:)
    integer(kind=4) :: osize,nsize,ilist
    !
    if (allocated(list)) then
      osize = size(list)
    else
      osize = 0
    endif
    if (osize.ge.rsize)  return
    !
    nsize = maxval( (/rsize,lines_min_alloc,2*osize/) )
    !
    if (osize.gt.0) then
      call move_alloc(from=list,to=tmp)
      ! => list is now deallocated
    endif
    !
    allocate(list(nsize))
    !
    if (osize.gt.0) then
      do ilist=1,osize
        list(ilist) = tmp(ilist)
      enddo
      deallocate(tmp)
    endif
    !
  end subroutine reallocate_lines
  !
end subroutine sic_grep_engine
