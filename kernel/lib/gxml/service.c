

#include "service.h"

#include "gcore/gwidget.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
/* for sockets */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define QUEUELEN  1
#define PORT  52013

/* for socket use */
int nread, len;
int sockfd;
struct sockaddr_in serv_addr;
int port = PORT;

/* char * hostname, * portnumber; */

#include <fcntl.h>

int connect_to_server( )
{
	struct hostent *hp;

        sockfd = open( "toto.txt", O_RDWR | O_CREAT, 0700);
        return 1;
	/* create socket endpoint */
	if ((sockfd = socket( AF_INET, SOCK_STREAM, 0)) < 0) {
		perror( "socket");
		return 0;
	}
	/* connect to server */
	serv_addr.sin_family = AF_INET;
	/* use the localhost address or use env vars */

	if (getenv( "GILDASGUIHOST") != NULL) {
		if ((hp = gethostbyname( getenv( "GILDASGUIHOST"))) == NULL) {
			perror( "gethostbyname");
			return 0;
		}
		bcopy( hp->h_addr, &serv_addr.sin_addr, hp->h_length);
	} else {
		serv_addr.sin_addr.s_addr = inet_addr( "127.0.0.1");
	}
	if (getenv( "GILDASGUIPORT") != NULL) {
		port = atoi( getenv( "GILDASGUIPORT"));
	} else {
		return 0;
	}
	serv_addr.sin_port = htons( port);
	if (connect( sockfd, (struct sockaddr *)&serv_addr, sizeof( serv_addr)) < 0) {
		perror( "connect");
		return 0;
	}
	return 1;
}

void sendConfig( char *progname, char *replyto)
{
	char conf[1024];

	if (replyto) {
		sprintf( conf, "<config entityName=\"%s\" to=\"JavaGui\" replyTo=\"%s\"></config>\n", progname, replyto);
	} else {
		sprintf( conf, "<config entityName=\"%s\" to=\"JavaGui\"></config>\n", progname);
	}
	write( sockfd, conf, strlen( conf));
}

char *unblank( char *blank)
{
    static char nonblank[SICVLN];
    int i;

    strcpy( nonblank, blank);
    for (i = strlen( blank); i > -1; i--) {
        switch (blank[i]) {
        case ' ':
        case '\0':
        case '\t':
        case '\n':
        case '\r':
            nonblank[i] = '\0';
            break;
        default:
            return nonblank;
        }
    }
    return nonblank;
}

char *xmlAttributeFilter( char *str)
{
	int i = 0, j = 0;
	static char resp[1024];

	resp[0] = 0;
	for (i = 0; i < strlen( str); i++) {
		switch (str[i]) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 11:
		case 12:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
			break;
		case 61:
			sprintf( &resp[j], "&#61;");
			j += 5;
			break;
		case 62:
			sprintf( &resp[j], "&gt;");
			j += 4;
			break;
		case 38:
			sprintf( &resp[j], "&amp;");
			j += 5;
			break;
		case 60:
			sprintf( &resp[j], "&lt;");
			j += 4;
			break;
		case 34:
			sprintf( &resp[j], "&quot;");
			j += 6;
			break;

/*            case 39 :  sprintf( &resp[j],"&apos;"); j+=6;
                break;*/
		default:
			resp[j] = str[i];
			j++;
		}
	}
	resp[j] = 0;

	return (char *)resp;
}

char *xmlTextFilter( char *str)
{
	int i = 0, j = 0;
	static char resp[1024];

	resp[0] = 0;
	for (i = 0; i < strlen( str); i++) {
		switch (str[i]) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 11:
		case 12:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:
			break;
		case 38:
			sprintf( &resp[j], "&amp;");
			j += 5;
			break;
		case 60:
			sprintf( &resp[j], "&lt;");
			j += 4;
			break;
		default:
			resp[j] = str[i];
			j++;
		}
	}
	resp[j] = 0;

	return (char *)resp;
}

