
#include "xml-menu.h"

#include <sys/types.h>
#include <sys/ipc.h>			/*      /usr/include/sys/ipc.h    */
#include <sys/shm.h>			/*      /usr/include/sys/shm.h    */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>

#define L 512
#define NBUTTON 128
#define min( A,B) ((A)<(B)?(A):(B))

#include "gcore/gcomm.h"
#include "service.h"

#define BUFSIZE 4048*8
static char buffer_out[BUFSIZE];

int run_xml_menu_args( int argc, char **argv)
{
    int i;
    int comm_id;
    char filename[PATH_MAX];
    FILE *fd;
    char windowtitle[TITLELENGTH];
    char helpfilename[HLPFILELNGTH];
    char line[512];

    i = 1;
    comm_id = atoi( argv[i++]);
    strcpy( filename, argv[i++]);

    fd = fopen( filename, "r");

    fgets( windowtitle, TITLELENGTH, fd);
    windowtitle[strlen( windowtitle) - 1] = '\0'; // suppress line feed

    fgets( helpfilename, HLPFILELNGTH, fd);
    helpfilename[strlen( helpfilename) - 1] = '\0'; // suppress line feed

    /* create socket endpoint */
    if (!connect_to_server( )) {
        fprintf( stderr, "%s: unable to connect, exiting.\n", argv[0]);
        sic_do_exit( 1);
    }
    sendConfig( argv[0], "sic_keyboard");

    sprintf( buffer_out, "<gui_menu>\n");

    while (fgets( line, sizeof( line), fd) != NULL) {
        line[strlen( line) - 1] = '\0'; // suppress line feed
        if (strncmp( line, "EXIT", 4) == 0) {
            sprintf( buffer_out + strlen( buffer_out), "<MENU label=\"EXIT\">");
            fgets( line, sizeof( line), fd);
            line[strlen( line) - 1] = '\0'; // suppress line feed
            sprintf( buffer_out + strlen( buffer_out),
                "<ITEM label=\"EXIT\" command=\"%s\" /></MENU>\n", line);
        } else if (strncmp( line, "MENU", 4) == 0) {
            fgets( line, sizeof( line), fd);
            line[strlen( line) - 1] = '\0'; // suppress line feed

            sprintf( buffer_out + strlen( buffer_out),
                "<MENU label=\"%s\">\n", xmlAttributeFilter( line));

            for (;;) {
                fgets( line, sizeof( line), fd);
                line[strlen( line) - 1] = '\0'; // suppress line feed
                if (strncmp( line, "ENDMENU", 7) == 0)
                    break;

                sprintf( buffer_out + strlen( buffer_out),
                    "<ITEM label=\"%s\"", xmlAttributeFilter( line));

                fgets( line, sizeof( line), fd);
                line[strlen( line) - 1] = '\0'; // suppress line feed

                sprintf( buffer_out + strlen( buffer_out),
                    " command=\"%s\"/>\n ", line);
            }
            sprintf( buffer_out + strlen( buffer_out), "</MENU>\n");
        } else {
            fgets( line, sizeof( line), fd);
            line[strlen( line) - 1] = '\0'; // suppress line feed
        }
    }
    strcat( buffer_out, "</gui_menu>");

    fclose( fd);

    unlink( filename);

    /* complete with help informations */

    if ((fd = fopen( helpfilename, "r")) != NULL) {
        strcat( buffer_out, "<HELP>\n");
        while (fgets( line, L, fd) != NULL) {
            strcat( buffer_out, xmlTextFilter( line));
        }
        strcat( buffer_out, "\n------\n");
        strcat( buffer_out, "</HELP>\n");
        fclose( fd);
    }

    write( sockfd, buffer_out, strlen( buffer_out));
    close( sockfd);
    return 0;
}

void run_xml_menu( const char *file)
{
    int i;
    char **argv;

    argv = sic_get_static_argv( );
    i = 0;
    sprintf( argv[i++], "undefined");
    sprintf( argv[i++], "0");
    sprintf( argv[i++], file);
    argv[i] = NULL;

    run_xml_menu_args( i, argv);
}

