
#include "gsys/cfc.h"
#include "gcore/glaunch.h"

#define init_gui CFC_EXPORT_NAME( init_gui)
#define gui_c_message_set_id CFC_EXPORT_NAME( gui_c_message_set_id)
#define xml_c_message_set_id CFC_EXPORT_NAME( xml_c_message_set_id)

void CFC_API init_gui( )
{
    int run_xml_keyboard( void *command_line);
    void run_xml_menu( const char *file);
    int run_dialog( );

    set_keyboard_handler( run_xml_keyboard, NULL);
    set_menu_handlers( run_xml_menu, NULL);
    set_dialog_handler( run_dialog);
}

void CFC_API gui_c_message_set_id( int *gpack_id)
{
    /*
    xml_c_message_set_id( gpack_id);
    */
}

