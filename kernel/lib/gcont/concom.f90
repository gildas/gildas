function concom (xq,yq,xd,yd,zd,ndp,wk,iwk,loc)
  use gcont_interfaces, except_this=>concom
  use gcont_contour
  !---------------------------------------------------------------------
  ! @ private
  !	Interpolate the values at a given point.
  ! Arguments :
  !	XQ	R*4	X coordinate of point			Input
  !	YQ	R*4	Y coordinate of point			Input
  !	XD	R*4 (*)	Array of X coordinates of points	Input
  !	YD	R*4 (*)	Array of Y coordinates of points	Input
  !	ZD	R*4 (*)	Array of values				Input
  !	NDP	I	Number of data points			Input
  !	WK	R*4 (*)	Work space				Input
  !	IWK	I (*)	Pointer space				Input
  !	LOC	I	Position of point			Output
  !	CONCOM	R*4	Interpolated value			Output
  ! Subroutines :
  !	CONLOC
  !	CONCAL
  !---------------------------------------------------------------------
  real :: concom                    !
  real :: xq                        !
  real :: yq                        !
  real :: xd(1)                     !
  real :: yd(1)                     !
  real :: zd(1)                     !
  integer :: ndp                    !
  real :: wk(1)                     !
  integer :: iwk(1)                 !
  integer :: loc                    !
  ! Local
  !  Interpolate a given X,Y pair and return its location
  !
  real :: temp
  !
  ! Locate proper triangle
  call conloc(ndp,xd,yd,nt,iwk(jwipt),nl,iwk(jwipl),xq,yq,loc,iwk(jwiwl),wk)
  !
  !  Interpolate the location
  call concal(xd,yd,zd,nt,iwk(jwipt),nl,iwk(jwipl),wk(ipr),loc,xq,yq,temp,itpv)
  concom = temp
end function concom
