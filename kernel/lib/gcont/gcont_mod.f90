!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gcont_contour
  !--------------------------------------------------------------------
  ! ???
  !-------------------------------------------------------------------- 
  !
  ! Error management??? (old conerr.inc)
  integer :: nit   !
  integer :: itipv !
  integer :: irec  !
  integer :: nerr  !
  !
  ! ??? (old conran.inc)
  integer :: ncp, ncpsz
  real :: xst, yst, xed, yed, stpsx, stpsy
  integer :: igrad, ig
  integer :: np, itll, ibll, itrl, ibrl, jx, jy, iloc, ioc, nc
  real :: tr, br, tl, bl, conv, xn, yn, xc, yc, xo, yo
  integer :: nt, nl, ntnl, jwipt, jwiwl, jwiwp, jwipl, ipr, itpv
  integer :: ixmax, iymax
  real :: xumax, yumax
  logical :: repeat, extrap, look, pldvls
  logical :: pmimx, extri, loaded
  !
  ! Contour levels
  integer, parameter :: maxcl=40
  !
  ! Regular grid mapping
  integer :: ioffp            ! Blanking or not
  integer :: ix,iy            ! X and Y pixels currently processed
  integer :: idx,idy
  integer :: ixbits,iybits    ! Number of bits in arithmetic
  integer :: is,iss
  integer :: np2              ! Number of points used in contour storage
  integer :: nr               ! Size of contour storage
  integer :: inx(8),iny(8)    ! Number of contour values
  integer :: ncl
  real :: spval               ! Blanking value
  real :: epsval              ! Tolerance in blanking
  real :: clev(maxcl)         ! Array of contour values
  real :: qlev                ! Multiplicative factor for contour values
  real :: cv
end module gcont_contour
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gcont_gridder
  !--------------------------------------------------------------------
  ! Setup of user preferences for the gridding (command GREG\RANDOM_MAP)
  !--------------------------------------------------------------------
  logical :: lextrp           ! Extrapolate or not?
  real :: rblank              ! Blanking value if not
end module gcont_gridder
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
