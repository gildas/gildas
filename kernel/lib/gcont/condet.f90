subroutine condet (ndp,xd,yd,ncp,ipc)
  !---------------------------------------------------------------------
  ! @ public
  !	This subroutine selects several data points that are closest
  !	to each of the data point.
  ! Arguments :
  !	NDP	I	Number of data points
  !	XD	R*4 (*)	Arrays containing the X coordinates
  !             		of data points.
  !	YD	R*4 (*) Array of Y coordinates
  !	NCP	I	number of data points closest to each
  !			data point.
  !	IPC	I (*)	Array of dimension NCP*NDP, where the
  !			point numbers of NCP data points closest
  !			to each of the NDP data points are to be
  !			stored.
  !----------------------------------------------------------------------
  !
  ! this subroutine arbitrarily sets a restriction that NCP must
  ! not exceed 25 without modification to the arrays DSQ0 and IPC0.
  ! Declaration statements
  !---------------------------------------------------------------------
  integer :: ndp                    !
  real :: xd(ndp)                   !
  real :: yd(ndp)                   !
  integer :: ncp                    !
  integer :: ipc(*)                 !
  ! Local
  real :: dsq0(25)
  integer :: ipc0(25)
  !
  real :: x1, y1, dsqmx, dsqi, dx12, dy12, dx13, dy13, dsqmn
  integer :: ip1, ip2, jmx, j1, ip2mn, j3, ip3, nclpt, j4
  integer :: ip3mn, j2
  !
  ! Statement function
  !      DSQF(U1,V1,U2,V2) = (U2-U1)**2+(V2-V1)**2
  !
  ! Calculation
  !
  do ip1=1,ndp
    !
    ! Selects NCP points.
    !
    x1 = xd(ip1)
    y1 = yd(ip1)
    j1 = 0
    dsqmx = 0.0
    do ip2=1,ndp
      if (ip2 .eq. ip1) cycle  ! IP2
      !      DSQF(U1,V1,U2,V2) = (U2-U1)**2+(V2-V1)**2
      !            DSQI = DSQF(X1,Y1,XD(IP2),YD(IP2))
      dsqi = (xd(ip2)-x1)**2 + (yd(ip2)-y1)**2
      j1 = j1+1
      dsq0(j1) = dsqi
      ipc0(j1) = ip2
      if (dsqi .le. dsqmx) go to  100
      dsqmx = dsqi
      jmx = j1
100   if (j1 .ge. ncp) go to  120
    enddo                      ! IP2
120 ip2mn = ip2+1
    if (ip2mn .gt. ndp) go to  150
    do ip2=ip2mn,ndp
      if (ip2 .eq. ip1) cycle  ! IP2
      !      DSQF(U1,V1,U2,V2) = (U2-U1)**2+(V2-V1)**2
      !            DSQI = DSQF(X1,Y1,XD(IP2),YD(IP2))
      dsqi = (xd(ip2)-x1)**2 + (yd(ip2)-y1)**2
      if (dsqi .ge. dsqmx) cycle   ! IP2
      dsq0(jmx) = dsqi
      ipc0(jmx) = ip2
      dsqmx = 0.0
      do j1=1,ncp
        if (dsq0(j1) .le. dsqmx) cycle ! J1
        dsqmx = dsq0(j1)
        jmx = j1
      enddo                    ! J1
    enddo                      ! IP2
    !
    ! Checks if all the NCP+1 points are collinear.
    !
150 ip2 = ipc0(1)
    dx12 = xd(ip2)-x1
    dy12 = yd(ip2)-y1
    do j3=2,ncp
      ip3 = ipc0(j3)
      dx13 = xd(ip3)-x1
      dy13 = yd(ip3)-y1
      if ((dy13*dx12-dx13*dy12) .ne. 0.0) go to  200
    enddo                      ! J3
    !
    ! Searches for the closest noncollinear point.
    !
    nclpt = 0
    do ip3=1,ndp
      if (ip3 .eq. ip1) cycle  ! IP3
      do j4=1,ncp
        if (ip3 .eq. ipc0(j4)) go to  190
      enddo                    ! J4
      dx13 = xd(ip3)-x1
      dy13 = yd(ip3)-y1
      if ((dy13*dx12-dx13*dy12) .eq. 0.0) cycle    ! IP3
      !      DSQF(U1,V1,U2,V2) = (U2-U1)**2+(V2-V1)**2
      !            DSQI = DSQF(X1,Y1,XD(IP3),YD(IP3))
      dsqi = (xd(ip3)-x1)**2 + (yd(ip3)-y1)**2
      if (nclpt .eq. 0) go to  180
      if (dsqi .ge. dsqmn) cycle   ! IP3
180   nclpt = 1
      dsqmn = dsqi
      ip3mn = ip3
190   continue
    enddo                      ! IP3
    dsqmx = dsqmn
    ipc0(jmx) = ip3mn
    !
    ! Replaces the local array for the output array.
    !
200 j1 = (ip1-1)*ncp
    do j2=1,ncp
      j1 = j1+1
      ipc(j1) = ipc0(j2)
    enddo
  enddo                        ! IP1
end subroutine condet
