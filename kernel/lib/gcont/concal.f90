subroutine concal (xd,yd,zd,nt,ipt,nl,ipl,pdd,iti,xii,yii,zii,itpv)
  use gcont_gridder
  !---------------------------------------------------------------------
  ! @ public
  ! 	This subroutine performs punctual interpolation or extrapolation
  ! 	i.e., determines the Z value at a point.
  ! Arguments :
  !	XD	R*4 (*)	Array of X coordinates			Input
  !	YD	R*4 (*)	Array of Y coordinates			Input
  !	ZD	R*4 (*) Array of values				Input
  !	NT	I	Number of triangles			Input
  !	IPT	I (*)	Array containing the point numbers of
  !			the vertexes of the triangles		Input
  !	NL	I	Number of border line segments		Input
  !	IPL	I (*)	Array containing the point numbers of
  !			the end points of the border line
  !			segments and their respective triangle
  !			numbers					Input
  !	PDD	R*4 (*)	Array containing the partial derivatives
  !			at the data points			Input
  !	ITI	I	triangle number of the triangle in which
  !			lies the point for which interpolation
  !			is to be performed			Input
  !	XII	R*4	X coordinate of the point for which
  !			interpolation is to be performed.	Input
  !	YII	R*4	Y coordinate of this point		Input
  !	ZII	R*4	Interpolated Z value			Output
  ! No subroutine referenced
  !---------------------------------------------------------------------
  real :: xd(*)                     !
  real :: yd(*)                     !
  real :: zd(*)                     !
  integer :: nt                     !
  integer :: ipt(*)                 !
  integer :: nl                     !
  integer :: ipl(*)                 !
  real :: pdd(*)                    !
  integer :: iti                    !
  real :: xii                       !
  real :: yii                       !
  real :: zii                       !
  integer :: itpv                   !
  ! Local
  real :: x(3), y(3), z(3), pd(15)
  !
  real :: zu(3), zv(3), zuu(3), zuv(3), zvv(3)
  real :: lu, lv
  !
  integer :: it0, ntl, il1, il2, jipt, jpd, i, idp, jpdd, kpd
  real :: x0, y0, a, b, c, d, ad, bc, dlt, ap, bp, cp, dp
  real :: aa, act2, cc, ab, adbc, cd, bb, bdt2, dd
  real :: p0, p1, p2, p3, p4, p5
  real :: p00, p10, p20, p30, p40, p50
  real :: p01, p02, p03, p04, p05
  real :: p11, p21, p31, p41, p12, p13, p14, p22, p23, p32
  real :: h1, h2, h3, g1, g2
  real :: thxu, thuv, csuv, thus, thsv, ac, dx, dy, u, v
  integer :: jipl
  equivalence     (p5,p50)
  !
  ! need SAVE ...
  save x0, y0, a, b, c, d
  save ad, bc, dlt, ap, bp, cp, dp
  save p00, p10, p20, p30, p40, p50
  save p01, p02, p03, p04, p05
  save p11, p21, p31, p41, p12, p13, p14, p22, p23, p32
  !
  ! Preliminary processing
  !
  it0 = iti
  ntl = nt+nl
  if (it0 .le. ntl) go to  100
  !
  ! Patch for not extrapolating	S.Guilloteau
  il1 = it0/ntl
  il2 = it0-il1*ntl
  if (il1 .eq. il2) then
    if (.not.lextrp) then
      zii = rblank
      return
    endif
    goto 150
  endif
  if (.not.lextrp) then
    zii = rblank
    return
  endif
  go to  200
  ! End patch
  !
  ! Calculation of ZII by interpolation.
  ! Checks if the necessary coefficients have been calculated.
  !
100 if (it0 .eq. itpv) go to  140
  !
  ! Loads coordinate and partial derivative values at the
  ! IPI 102 vertexes.
  ! IPI 103
  !
  jipt = 3*(it0-1)
  jpd = 0
  do i=1,3
    jipt = jipt+1
    idp = ipt(jipt)
    x(i) = xd(idp)
    y(i) = yd(idp)
    z(i) = zd(idp)
    jpdd = 5*(idp-1)
    do kpd=1,5
      jpd = jpd+1
      jpdd = jpdd+1
      pd(jpd) = pdd(jpdd)
    enddo
  enddo
  !
  ! Determines the coefficients for the coordinate system
  ! transformation from the X-Y system to the U-V system
  ! and vice versa.
  !
  x0 = x(1)
  y0 = y(1)
  a = x(2)-x0
  b = x(3)-x0
  c = y(2)-y0
  d = y(3)-y0
  ad = a*d
  bc = b*c
  dlt = ad-bc
  ap = d/dlt
  bp = -b/dlt
  cp = -c/dlt
  dp = a/dlt
  !
  ! Converts the partial derivatives at the vertexes of the
  ! triangle for the U-V coordinate system.
  !
  aa = a*a
  act2 = 2.0*a*c
  cc = c*c
  ab = a*b
  adbc = ad+bc
  cd = c*d
  bb = b*b
  bdt2 = 2.0*b*d
  dd = d*d
  do i=1,3
    jpd = 5*i
    zu(i) = a*pd(jpd-4)+c*pd(jpd-3)
    zv(i) = b*pd(jpd-4)+d*pd(jpd-3)
    zuu(i) = aa*pd(jpd-2)+act2*pd(jpd-1)+cc*pd(jpd)
    zuv(i) = ab*pd(jpd-2)+adbc*pd(jpd-1)+cd*pd(jpd)
    zvv(i) = bb*pd(jpd-2)+bdt2*pd(jpd-1)+dd*pd(jpd)
  enddo
  !
  ! Calculates the coefficients of the polynomial.
  !
  p00 = z(1)
  p10 = zu(1)
  p01 = zv(1)
  p20 = 0.5*zuu(1)
  p11 = zuv(1)
  p02 = 0.5*zvv(1)
  h1 = z(2)-p00-p10-p20
  h2 = zu(2)-p10-zuu(1)
  h3 = zuu(2)-zuu(1)
  p30 = 10.0*h1-4.0*h2+0.5*h3
  p40 = -15.0*h1+7.0*h2-h3
  p50 = 6.0*h1-3.0*h2+0.5*h3
  h1 = z(3)-p00-p01-p02
  h2 = zv(3)-p01-zvv(1)
  h3 = zvv(3)-zvv(1)
  p03 = 10.0*h1-4.0*h2+0.5*h3
  p04 = -15.0*h1+7.0*h2-h3
  p05 = 6.0*h1-3.0*h2+0.5*h3
  lu = sqrt(aa+cc)
  lv = sqrt(bb+dd)
  thxu = atan2(c,a)
  thuv = atan2(d,b)-thxu
  csuv = cos(thuv)
  p41 = 5.0*lv*csuv/lu*p50
  p14 = 5.0*lu*csuv/lv*p05
  h1 = zv(2)-p01-p11-p41
  h2 = zuv(2)-p11-4.0*p41
  p21 = 3.0*h1-h2
  p31 = -2.0*h1+h2
  h1 = zu(3)-p10-p11-p14
  h2 = zuv(3)-p11-4.0*p14
  p12 = 3.0*h1-h2
  p13 = -2.0*h1+h2
  thus = atan2(d-c,b-a)-thxu
  thsv = thuv-thus
  aa = sin(thsv)/lu
  bb = -cos(thsv)/lu
  cc = sin(thus)/lv
  dd = cos(thus)/lv
  ac = aa*cc
  ad = aa*dd
  bc = bb*cc
  g1 = aa*ac*(3.0*bc+2.0*ad)
  g2 = cc*ac*(3.0*ad+2.0*bc)
  h1 = -aa*aa*aa*(5.0*aa*bb*p50+(4.0*bc+ad)*p41) - &
        cc*cc*cc*(5.0*cc*dd*p05+(4.0*ad+bc)*p14)
  h2 = 0.5*zvv(2)-p02-p12
  h3 = 0.5*zuu(3)-p20-p21
  p22 = (g1*h2+g2*h3-h1)/(g1+g2)
  p32 = h2-p22
  p23 = h3-p22
  itpv = it0
  !
  ! Converts XII and YII to U-V system.
  !
140 dx = xii-x0
  dy = yii-y0
  u = ap*dx+bp*dy
  v = cp*dx+dp*dy
  !
  ! Evaluates the polynomial.
  !
  p0 = p00+v*(p01+v*(p02+v*(p03+v*(p04+v*p05))))
  p1 = p10+v*(p11+v*(p12+v*(p13+v*p14)))
  p2 = p20+v*(p21+v*(p22+v*p23))
  p3 = p30+v*(p31+v*p32)
  p4 = p40+v*p41
  zii = p0+u*(p1+u*(p2+u*(p3+u*(p4+u*p5))))
  return
  !
  ! Calculation of ZII by extraterpolation in the rectangle.
  ! Checks if the necessary coefficients have been calculated.
  !
150 if (it0 .eq. itpv) go to  190
  !
  ! Loads coordinate and partial derivative values at the end
  ! points of the border line segment.
  !
  jipl = 3*(il1-1)
  jpd = 0
  do i=1,2
    jipl = jipl+1
    idp = ipl(jipl)
    x(i) = xd(idp)
    y(i) = yd(idp)
    z(i) = zd(idp)
    jpdd = 5*(idp-1)
    do kpd=1,5
      jpd = jpd+1
      jpdd = jpdd+1
      pd(jpd) = pdd(jpdd)
    enddo
  enddo
  !
  ! Determines the coefficients for the coordinate system
  ! transformation from the X-Y system to the U-V system
  ! and vice versa.
  !
  x0 = x(1)
  y0 = y(1)
  a = y(2)-y(1)
  b = x(2)-x(1)
  c = -b
  d = a
  ad = a*d
  bc = b*c
  dlt = ad-bc
  ap = d/dlt
  bp = -b/dlt
  cp = -bp
  dp = ap
  !
  ! Converts the partial derivatives at the end points of the
  ! border line segment for the U-V coordinate system.
  !
  aa = a*a
  act2 = 2.0*a*c
  cc = c*c
  ab = a*b
  adbc = ad+bc
  cd = c*d
  bb = b*b
  bdt2 = 2.0*b*d
  dd = d*d
  do i=1,2
    jpd = 5*i
    zu(i) = a*pd(jpd-4)+c*pd(jpd-3)
    zv(i) = b*pd(jpd-4)+d*pd(jpd-3)
    zuu(i) = aa*pd(jpd-2)+act2*pd(jpd-1)+cc*pd(jpd)
    zuv(i) = ab*pd(jpd-2)+adbc*pd(jpd-1)+cd*pd(jpd)
    zvv(i) = bb*pd(jpd-2)+bdt2*pd(jpd-1)+dd*pd(jpd)
  enddo
  !
  ! Calculates the coefficients of the polynomial.
  !
  p00 = z(1)
  p10 = zu(1)
  p01 = zv(1)
  p20 = 0.5*zuu(1)
  p11 = zuv(1)
  p02 = 0.5*zvv(1)
  h1 = z(2)-p00-p01-p02
  h2 = zv(2)-p01-zvv(1)
  h3 = zvv(2)-zvv(1)
  p03 = 10.0*h1-4.0*h2+0.5*h3
  p04 = -15.0*h1+7.0*h2-h3
  p05 = 6.0*h1-3.0*h2+0.5*h3
  h1 = zu(2)-p10-p11
  h2 = zuv(2)-p11
  p12 = 3.0*h1-h2
  p13 = -2.0*h1+h2
  p21 = 0.0
  p23 = -zuu(2)+zuu(1)
  p22 = -1.5*p23
  itpv = it0
  !
  ! Converts XII and YII to U-V system.
  !
190 dx = xii-x0
  dy = yii-y0
  u = ap*dx+bp*dy
  v = cp*dx+dp*dy
  !
  ! Evaluates the polynomial.
  !
  p0 = p00+v*(p01+v*(p02+v*(p03+v*(p04+v*p05))))
  p1 = p10+v*(p11+v*(p12+v*p13))
  p2 = p20+v*(p21+v*(p22+v*p23))
  zii = p0+u*(p1+u*p2)
  return
  !
  ! Calculation of ZII by extraterpolation in the triangle.
  ! Checks if the necessary coefficients have been calculated.
  !
200 if (it0 .eq. itpv) go to  220
  !
  ! Loads coordinate and partial derivative values at the vertex
  ! of the triangle.
  !
  jipl = 3*il2-2
  idp = ipl(jipl)
  x(1) = xd(idp)
  y(1) = yd(idp)
  z(1) = zd(idp)
  jpdd = 5*(idp-1)
  do kpd=1,5
    jpdd = jpdd+1
    pd(kpd) = pdd(jpdd)
  enddo
  !
  ! Calculates the coefficients of the polynomial.
  !
  p00 = z(1)
  p10 = pd(1)
  p01 = pd(2)
  p20 = 0.5*pd(3)
  p11 = pd(4)
  p02 = 0.5*pd(5)
  itpv = it0
  !
  ! Converts XII and YII to U-V system.
  !
220 u = xii-x(1)
  v = yii-y(1)
  !
  ! Evaluates the polynomial.
  !
  p0 = p00+v*(p01+v*p02)
  p1 = p10+v*p11
  zii = p0+u*(p1+u*p20)
  return
end subroutine concal
