function conxch (x,y,i1,i2,i3,i4)
  use gcont_interfaces, except_this=>conxch
  !---------------------------------------------------------------------
  ! @ private
  ! 	This function determines whether or not the exchange of two
  ! 	triangles is necessary on the basis of max-min-angle criterion
  ! 	by C. L. LAWSON.
  !
  ! Arguments :
  !     	X	R*4 (*)	arrays containing the X coordinates
  !			of the data points			Input
  !	Y	R*4 (*) Array of Y coordinates			Input
  !     	I1	I	Point numbers of point P1		Input
  !     	I2	I	Point numbers of point P2		Input
  !     	I3	I	Point numbers of point P3		Input
  !     	I4	I	Point numbers of point P4		Input
  !			The four points P1, P2, P3, and P4
  !			form a quadrilateral with P3 and P4
  !			connected diagonally.
  ! This function returns a value 1 (one) when an exchange is
  !	needed, and 0 (zero) otherwise.
  !---------------------------------------------------------------------
  integer :: conxch                 !
  real :: x(*)                      !
  real :: y(*)                      !
  integer :: i1                     !
  integer :: i2                     !
  integer :: i3                     !
  integer :: i4                     !
  ! Local
  real :: x0(4), y0(4)
  real :: a1sq, a2sq, a3sq, a4sq
  real :: b1sq, b2sq, b3sq, b4sq
  real :: c1sq, c2sq, c3sq, c4sq
  real :: s1sq, s2sq, s3sq, s4sq
  real :: u1, u2, u3, u4
  integer :: idx
  equivalence (c2sq,c1sq),(a3sq,b2sq),(b3sq,a1sq),  &
              (a4sq,b1sq),(b4sq,a2sq),(c4sq,c3sq)
  !
  ! Calculation
  !
  x0(1) = x(i1)
  y0(1) = y(i1)
  x0(2) = x(i2)
  y0(2) = y(i2)
  x0(3) = x(i3)
  y0(3) = y(i3)
  x0(4) = x(i4)
  y0(4) = y(i4)
  idx = 0
  u3 = (y0(2)-y0(3))*(x0(1)-x0(3))-(x0(2)-x0(3))*(y0(1)-y0(3))
  u4 = (y0(1)-y0(4))*(x0(2)-x0(4))-(x0(1)-x0(4))*(y0(2)-y0(4))
  if (u3*u4 .le. 0.0) go to  100
  u1 = (y0(3)-y0(1))*(x0(4)-x0(1))-(x0(3)-x0(1))*(y0(4)-y0(1))
  u2 = (y0(4)-y0(2))*(x0(3)-x0(2))-(x0(4)-x0(2))*(y0(3)-y0(2))
  a1sq = (x0(1)-x0(3))**2+(y0(1)-y0(3))**2
  b1sq = (x0(4)-x0(1))**2+(y0(4)-y0(1))**2
  c1sq = (x0(3)-x0(4))**2+(y0(3)-y0(4))**2
  a2sq = (x0(2)-x0(4))**2+(y0(2)-y0(4))**2
  b2sq = (x0(3)-x0(2))**2+(y0(3)-y0(2))**2
  c3sq = (x0(2)-x0(1))**2+(y0(2)-y0(1))**2
  s1sq = u1*u1/(c1sq*amax1(a1sq,b1sq))
  s2sq = u2*u2/(c2sq*amax1(a2sq,b2sq))
  s3sq = u3*u3/(c3sq*amax1(a3sq,b3sq))
  s4sq = u4*u4/(c4sq*amax1(a4sq,b4sq))
  if (amin1(s1sq,s2sq) .lt. amin1(s3sq,s4sq)) idx = 1
100 conxch = idx
end function conxch
