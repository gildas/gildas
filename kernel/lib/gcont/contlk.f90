subroutine contlk (xd,yd,ndp,ipt,vectd,frstd)
  use gcont_contour
  !---------------------------------------------------------------------
  ! @ private
  !	Draw the triangles created by CONTNG
  ! Arguments :
  !	XD	R*4 (*)	Array of X coordinates			Input
  !	YD	R*4 (*)	Array of Y coordinates			Input
  !	NDP	I	Number of data points			Input
  !	IPT	I (*)	Array of triangle pointers		Input
  !	VECTD	Ext	Vector drawing routine
  !	FRSTD	Ext	Pen up drawing routine
  ! Subroutines :
  !	VECTD, FRSTD
  !---------------------------------------------------------------------
  real :: xd(1)                     !
  real :: yd(1)                     !
  integer :: ndp                    !
  integer :: ipt(1)                 !
  external :: vectd                 !
  external :: frstd                 !
  ! Local
  integer :: i,k, i1,i2,i3
  !
  !  Draw triangles
  i = 1
  do k=1,nt
    i3 = ipt(i)
    i = i+1
    i2 = ipt(i)
    i = i+1
    i1 = ipt(i)
    i = i+1
    call frstd(xd(i1),yd(i1))
    call vectd(xd(i2),yd(i2))
    call vectd(xd(i3),yd(i3))
    call vectd(xd(i1),yd(i1))
  enddo
end subroutine contlk
