module gcont_interfaces_private
  interface
    function concom (xq,yq,xd,yd,zd,ndp,wk,iwk,loc)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ private
      !	Interpolate the values at a given point.
      ! Arguments :
      !	XQ	R*4	X coordinate of point			Input
      !	YQ	R*4	Y coordinate of point			Input
      !	XD	R*4 (*)	Array of X coordinates of points	Input
      !	YD	R*4 (*)	Array of Y coordinates of points	Input
      !	ZD	R*4 (*)	Array of values				Input
      !	NDP	I	Number of data points			Input
      !	WK	R*4 (*)	Work space				Input
      !	IWK	I (*)	Pointer space				Input
      !	LOC	I	Position of point			Output
      !	CONCOM	R*4	Interpolated value			Output
      ! Subroutines :
      !	CONLOC
      !	CONCAL
      !---------------------------------------------------------------------
      real :: concom                    !
      real :: xq                        !
      real :: yq                        !
      real :: xd(1)                     !
      real :: yd(1)                     !
      real :: zd(1)                     !
      integer :: ndp                    !
      real :: wk(1)                     !
      integer :: iwk(1)                 !
      integer :: loc                    !
    end function concom
  end interface
  !
  interface
    subroutine conpdv (xd,yd,zd,ndp,gwrite)
      !---------------------------------------------------------------------
      ! @ private
      !	Plot the data values on the contour map
      !	currently up to 10 characters for each value are displayed
      ! Arguments :
      !	XD	R*4 (*)	Array of X coordinates		Input
      !	YD	R*4 (*)	Array of Y coordinates		Input
      !	ZD	R*4 (*)	Array of values			Input
      !	NDP	I	Number of values		Input
      !	GWRITE	Ext	Label drawing routine		Input
      ! Subroutines :
      !	GWRITE
      !---------------------------------------------------------------------
      real :: xd(1)                     !
      real :: yd(1)                     !
      real :: zd(1)                     !
      integer :: ndp                    !
      external :: gwrite                !
    end subroutine conpdv
  end interface
  !
  interface
    subroutine stline(conval,nx,ny,z,vectd,lastd,ir,error)
      use gcont_contour
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! 	This routine finds the beginnings of all contour lines at
      !	level CONVAL. First the edges are searched for lines intersecting
      !	the edge (open lines) then the interior is searched for lines
      !	which do not intersect the edge (closed lines).  Beginnings are
      !	stored in IR to prevent retracing of lines.  If IR is filled,
      !	the search is stopped for this CONVAL.
      ! Arguments :
      !	CONVAL	R*4	Contour value				Input
      !	NX	I	Number of X pixels			Input
      !	NY	I	Number of Y pixels			Input
      !	Z	R*4	Array of values				Input
      !	VECTD	Ext	Vector drawing function
      !	LASTD	Ext	End of contour drawing function
      !	IR	I	Work space for contour storage
      ! Subroutines :
      !	DRLINE
      !---------------------------------------------------------------------
      real :: conval                    !
      integer :: nx                     !
      integer :: ny                     !
      real :: z(*)                      !
      external :: vectd                 !
      external :: lastd                 !
      integer :: ir(2,*)                !
      logical, intent(inout) :: error   ! Error return
    end subroutine stline
  end interface
  !
  interface
    subroutine drline (nx,ny,z,vectd,lastd,ir,error)
      use gcont_contour
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! 	This routine traces a contour line when given the beginning by STLINE.
      ! 	Transformations can be added by deleting the statement functions for
      ! 	FX and FY in DRLINE and MINMAX and adding external functions.
      ! 	X=1. at Z(1,J), X=FLOAT(M) at Z(M,J). X takes on non-integer values.
      ! 	Y=1. at Z(I,1), Y=FLOAT(N) at Z(I,N). Y takes on non-integer values.
      ! Arguments :
      !	NX	I	Number of pixels along X axis		Input
      !	NY	I	Number of pixels along Y axis		Input
      !	Z	R*4 (*)	Array of values				Input
      !	VECTD	Ext	Vector drawing function
      !	LASTD	Ext	End of contour drawing function
      !	IR	I (*)	Workspace for contour storage
      !		It is assumed that all the necessary setup
      !		have been done before the call to DRLINE
      ! Subroutines :
      !	VECTD, LASTD, ISHIFT
      !---------------------------------------------------------------------
      integer :: nx                     !
      integer :: ny                     !
      real :: z(*)                      !
      external :: vectd                 !
      external :: lastd                 !
      integer :: ir(2,*)                !
      logical, intent(inout) :: error   ! Error return
    end subroutine drline
  end interface
  !
  interface
    subroutine conbit(nx,ny,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !	Set up the Bit arithmetic for Regular mapping
      ! Arguments :
      !	NX	I	Number of I pixels		Input
      !	NY	I	Number of J Pixels		Input
      !	ERROR	L	Error return
      ! No subroutines
      !---------------------------------------------------------------------
      integer :: nx                     !
      integer :: ny                     !
      logical :: error                  !
    end subroutine conbit
  end interface
  !
  interface
    subroutine contlk (xd,yd,ndp,ipt,vectd,frstd)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ private
      !	Draw the triangles created by CONTNG
      ! Arguments :
      !	XD	R*4 (*)	Array of X coordinates			Input
      !	YD	R*4 (*)	Array of Y coordinates			Input
      !	NDP	I	Number of data points			Input
      !	IPT	I (*)	Array of triangle pointers		Input
      !	VECTD	Ext	Vector drawing routine
      !	FRSTD	Ext	Pen up drawing routine
      ! Subroutines :
      !	VECTD, FRSTD
      !---------------------------------------------------------------------
      real :: xd(1)                     !
      real :: yd(1)                     !
      integer :: ndp                    !
      integer :: ipt(1)                 !
      external :: vectd                 !
      external :: frstd                 !
    end subroutine contlk
  end interface
  !
  interface
    function conxch (x,y,i1,i2,i3,i4)
      !---------------------------------------------------------------------
      ! @ private
      ! 	This function determines whether or not the exchange of two
      ! 	triangles is necessary on the basis of max-min-angle criterion
      ! 	by C. L. LAWSON.
      !
      ! Arguments :
      !     	X	R*4 (*)	arrays containing the X coordinates
      !			of the data points			Input
      !	Y	R*4 (*) Array of Y coordinates			Input
      !     	I1	I	Point numbers of point P1		Input
      !     	I2	I	Point numbers of point P2		Input
      !     	I3	I	Point numbers of point P3		Input
      !     	I4	I	Point numbers of point P4		Input
      !			The four points P1, P2, P3, and P4
      !			form a quadrilateral with P3 and P4
      !			connected diagonally.
      ! This function returns a value 1 (one) when an exchange is
      !	needed, and 0 (zero) otherwise.
      !---------------------------------------------------------------------
      integer :: conxch                 !
      real :: x(*)                      !
      real :: y(*)                      !
      integer :: i1                     !
      integer :: i2                     !
      integer :: i3                     !
      integer :: i4                     !
    end function conxch
  end interface
  !
  interface
    subroutine gcont_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gcont_message
  end interface
  !
  interface
    subroutine conbdn
      use gcont_contour
      use gcont_gridder
      !---------------------------------------------------------------------
      ! @ private
      ! COMMON DATA
      !
      ! NOTE The common blocks listed include all the common used by
      !      the entire conran family, not all members will use all
      !      the common data.
      !---------------------------------------------------------------------
    end subroutine conbdn
  end interface
  !
end module gcont_interfaces_private
