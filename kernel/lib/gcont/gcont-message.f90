!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GCONT messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gcont_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gcont_message_id = gpack_global_id  ! Default value for startup message
  !
end module gcont_message_private
!
subroutine gcont_message_set_id(id)
  use gcont_interfaces, except_this=>gcont_message_set_id
  use gcont_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gcont_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gcont_message_id
  call gcont_message(seve%d,'gcont_message_set_id',mess)
  !
end subroutine gcont_message_set_id
!
subroutine gcont_message(mkind,procname,message)
  use gcont_dependencies_interfaces
  use gcont_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gcont_message_id,mkind,procname,message)
  !
end subroutine gcont_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
