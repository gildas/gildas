module gcont_interfaces_public
  interface
    subroutine concal (xd,yd,zd,nt,ipt,nl,ipl,pdd,iti,xii,yii,zii,itpv)
      use gcont_gridder
      !---------------------------------------------------------------------
      ! @ public
      ! 	This subroutine performs punctual interpolation or extrapolation
      ! 	i.e., determines the Z value at a point.
      ! Arguments :
      !	XD	R*4 (*)	Array of X coordinates			Input
      !	YD	R*4 (*)	Array of Y coordinates			Input
      !	ZD	R*4 (*) Array of values				Input
      !	NT	I	Number of triangles			Input
      !	IPT	I (*)	Array containing the point numbers of
      !			the vertexes of the triangles		Input
      !	NL	I	Number of border line segments		Input
      !	IPL	I (*)	Array containing the point numbers of
      !			the end points of the border line
      !			segments and their respective triangle
      !			numbers					Input
      !	PDD	R*4 (*)	Array containing the partial derivatives
      !			at the data points			Input
      !	ITI	I	triangle number of the triangle in which
      !			lies the point for which interpolation
      !			is to be performed			Input
      !	XII	R*4	X coordinate of the point for which
      !			interpolation is to be performed.	Input
      !	YII	R*4	Y coordinate of this point		Input
      !	ZII	R*4	Interpolated Z value			Output
      ! No subroutine referenced
      !---------------------------------------------------------------------
      real :: xd(*)                     !
      real :: yd(*)                     !
      real :: zd(*)                     !
      integer :: nt                     !
      integer :: ipt(*)                 !
      integer :: nl                     !
      integer :: ipl(*)                 !
      real :: pdd(*)                    !
      integer :: iti                    !
      real :: xii                       !
      real :: yii                       !
      real :: zii                       !
      integer :: itpv                   !
    end subroutine concal
  end interface
  !
  interface
    subroutine condet (ndp,xd,yd,ncp,ipc)
      !---------------------------------------------------------------------
      ! @ public
      !	This subroutine selects several data points that are closest
      !	to each of the data point.
      ! Arguments :
      !	NDP	I	Number of data points
      !	XD	R*4 (*)	Arrays containing the X coordinates
      !             		of data points.
      !	YD	R*4 (*) Array of Y coordinates
      !	NCP	I	number of data points closest to each
      !			data point.
      !	IPC	I (*)	Array of dimension NCP*NDP, where the
      !			point numbers of NCP data points closest
      !			to each of the NDP data points are to be
      !			stored.
      !----------------------------------------------------------------------
      !
      ! this subroutine arbitrarily sets a restriction that NCP must
      ! not exceed 25 without modification to the arrays DSQ0 and IPC0.
      ! Declaration statements
      !---------------------------------------------------------------------
      integer :: ndp                    !
      real :: xd(ndp)                   !
      real :: yd(ndp)                   !
      integer :: ncp                    !
      integer :: ipc(*)                 !
    end subroutine condet
  end interface
  !
  interface
    subroutine conint (ndp,xd,yd,zd,ncp,ipc,pd)
      !---------------------------------------------------------------------
      ! @ public
      !	This subroutine estimates partial derivatives of the first and
      !	second order at the data points.
      ! Arguments :
      !	NDP	I	Number of data points			Input
      !	XD	R*4 (*)	Array of X coordinates			Input
      !	YD	R*4 (*)	Array of Y coordinates			Input
      !	ZD	R*4 (*)	Array of values				Input
      !	NCP	I	Number of data points to be used for
      !			estimation of partial derivatives at
      !			each data point				Input
      !	IPC	I (*)	Array containing the point numbers of
      !			NCP data points closest to each of the
      !			NDP data point.				Input
      !	PD	R*4 (*) Array of dimension 5*NDP, where the
      !			estimated ZX, ZY, ZXX, ZXY, and ZYY
      !			values at the data points are to be
      !			stored.					Output
      ! No subroutines referenced
      !---------------------------------------------------------------------
      integer :: ndp                    !
      real :: xd(ndp)                   !
      real :: yd(ndp)                   !
      real :: zd(ndp)                   !
      integer :: ncp                    !
      integer :: ipc(*)                 !
      real :: pd(*)                     !
    end subroutine conint
  end interface
  !
  interface
    subroutine conloc (ndp,xd,yd,ntr,ipt,nli,ipl,xii,yii,iti,iwk,wk)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ public
      !	This subroutine locates a point, i.e., determines to what
      !	triangle a given point (xii,yii) belongs.  When the given
      !	point does not lie inside the data area, this subroutine
      !	determines the border line segment when the point lies in
      !	an outside rectangular area, and two border line segments
      !	when the point lies in an outside triangular area.
      ! Arguments :
      !	NDP	I	number of data points			Input
      !	XD	R*4 (*)	Arrays of dimension NDP containing the
      !			X coordinates of the data points	Input
      !	YD	R*4 (*) Array of Y coordinates			Input
      !	NTR	I	Number of triangles			Input
      !	IPT	I (*)	Array of dimension 3*NTR containing the
      !			point numbers of the vertexes of the
      !			triangles				Input
      !	NLI	I	Number of border line segments		Input
      !	IPL	I (*)	Array of dimension 3*NLI containing the
      !			point numbers of the end points of the
      !			border line segments and their respective
      !			triangle numbers			Input
      !	XII	R*4	X coordinate of point to locate		Input
      !	YII 	R*4	Y coordinate of point to locate		Input
      !	ITI 	I	Triangle number, when the point is
      !			inside the data area, or two border
      !			line segment numbers, IL1 and IL2,
      !			coded to IL1*(NTR+NLI)+IL2, when the
      !			point is outside the data area.		Output
      !	IWK 	I (*)	Array of dimension 18*NDP used
      !			internally as a work area
      !	WK  	R*4 (*)	Array of dimension 8*NDP used internally
      !			as a work area.
      !---------------------------------------------------------------------
      integer :: ndp                    !
      real :: xd(*)                     !
      real :: yd(*)                     !
      integer :: ntr                    !
      integer :: ipt(*)                 !
      integer :: nli                    !
      integer :: ipl(*)                 !
      real :: xii                       !
      real :: yii                       !
      integer :: iti                    !
      integer :: iwk(*)                 !
      real :: wk(*)                     !
    end subroutine conloc
  end interface
  !
  interface
    subroutine conreg (nx,ny,z,frstd,vectd,lastd,flushd,ir,n,error)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !	Draw contour levels according to the parameters set up by
      !	subroutine MAP
      ! Arguments :
      !	NX	I	Number of X pixels			Input
      !	NY	I	Number of Y pixels			Input
      !	Z	R*4	Array of values				Input
      !	VECTD	Ext 	Vector drawing function			Input
      !	LASTD	Ext	End of contour drawing function		Input
      !	FRSTD	Ext	First vector drawing routine 		Input
      !	FLUSHD	Ext	A plot flushing routine			Input
      !	IR	I	Work space for contour storage
      !	N	I	Size of work space
      ! Subroutines :
      !	CONBIT
      !	STLINE
      !---------------------------------------------------------------------
      integer :: nx                     !
      integer :: ny                     !
      real :: z(*)                      !
      external :: frstd                 !
      external :: vectd                 !
      external :: lastd                 !
      external :: flushd                !
      integer :: ir(*)                  !
      integer :: n                      !
      logical, intent(inout) :: error   ! Error return
    end subroutine conreg
  end interface
  !
  interface
    subroutine conblk(blank,value,tol)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ public
      !	Set blanking value for Regular mapping
      ! Arguments :
      !	BLANK	L	Blanking or not			Input
      !	VALUE	R*4	The blanking value		Input
      !	TOL	R*4	The tolerance			Input
      ! No subroutines
      !---------------------------------------------------------------------
      logical :: blank                  !
      real :: value                     !
      real :: tol                       !
    end subroutine conblk
  end interface
  !
  interface
    subroutine conlev(fact,array,n)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ public
      !	Setup the array of contour values
      ! Arguments :
      !	FACT	R*4	Multiplicative factor
      !	ARRAY	R*4	Contour value array
      !	N	I	Dimension of array
      ! No subroutine
      !---------------------------------------------------------------------
      real :: fact                      !
      real :: array(*)                  !
      integer :: n                      !
    end subroutine conlev
  end interface
  !
  interface
    subroutine contng (ndp,xd,yd,ntr,ipt,nli,ipl,iwl,iwp,wk)
      use gcont_contour
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !	This subroutine performs triangulation.  It divides the X-Y
      !	plane into a number of triangles according to given data
      !	points in the plane, determines line segments that form the
      !	border of data area, and determines the triangle numbers
      !	corresponding to the border line segments.
      !	At completion, point numbers of the vertexes of each triangle
      !	are listed counter-clockwise.  Point numbers of the end points
      !	of each border line segment are listed counter-clockwise,
      !	listing order of the line segments being counter-clockwise.
      ! Arguments :
      !	NDP 	I	Number of data points			Input
      !	XD  	R*4 (*)	Array of dimension NDP containing the
      !			X coordinates of the data points	Input
      !	YD  	R*4 (*) Array of Y coordinates			Input
      !	NTR  	I	Number of triangles			Output
      !	IPT 	I (*)	Array of dimension 6*NDP-15, where the
      !			point numbers of the vertexes of the
      !			(IT)th triangle are to be stored as the
      !			(3*IT-2)nd, (3*IT-1)st, and (3*IT)th
      !			elements, IT=1,2,...,NTR		Output
      !	NLI  	I	Number of border line segments		Output
      !	IPL 	I (*)	Array of dimension 6*NDP, where the point
      !			numbers of the end points of the (IL)th
      !			border line segment and its respective
      !			triangle number are to be stored as the
      !			(3*IL-2)nd, (3*IL-1)st, and (3*IL)th
      !			elements, IL=1,2,..., NLI.		Output
      !	IWL 	I (*)	Array of dimension 18*NDP used
      !			internally as a work area
      !	IWP 	I (*)	Array of dimension NDP used
      !			internally as a work area
      !	WK  	R*4 (*)	Array of dimension NDP used
      !			internally as a work area.
      ! No subroutine referenced
      !---------------------------------------------------------------------
      integer :: ndp                    !
      real :: xd(*)                     !
      real :: yd(*)                     !
      integer :: ntr                    !
      integer :: ipt(*)                 !
      integer :: nli                    !
      integer :: ipl(*)                 !
      integer :: iwl(*)                 !
      integer :: iwp(*)                 !
      real :: wk(*)                     !
    end subroutine contng
  end interface
  !
  interface
    subroutine gcont_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine gcont_message_set_id
  end interface
  !
  interface
    subroutine gridran (yd,xd,zd,ndp,wk,iwk,scrarr,vectd,frstd,label,error)
      use gcont_contour
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !	Stephane Guilloteau modification of CONRAN from ULIB 10-MAY-1984
      !	This is the pilot routine for random interpolation. Note that
      !	array XD and YD have been exchanged in order to obtain an
      !	interpolated grid with same organisation as the regular grid array.
      ! Arguments :
      !	YD	R*4 (*)	Array of X coordinates			Input
      !	XD	R*4 (*)	Array of Y coordinates			Input
      !	ZD	R*4 (*)	Array of values				Input
      !	NDP	I	Number of data points			Input
      !	WK	R*4 (*)	Work space
      !	IWK	I (*)	Work space
      !	SCRARR	R*4 (*)	Gridded space				Output
      !	VECTD	Ext	Vector drawing function			External
      !	FRTSD	Ext	Pen up drawing function			External
      !	LABEL	Ext	Label drawing function			External
      !	ERROR   L       Logical error flag
      ! Subroutines :
      !	CONTNG, CONDET, CONINT, CONBDN
      !---------------------------------------------------------------------
      real :: yd(*)                     !
      real :: xd(*)                     !
      real :: zd(*)                     !
      integer :: ndp                    !
      real :: wk(*)                     !
      integer :: iwk(*)                 !
      real :: scrarr(*)                 !
      external :: vectd                 !
      external :: frstd                 !
      external :: label                 !
      logical :: error                  !
    end subroutine gridran
  end interface
  !
  interface
    subroutine gridset(n,blank,log)
      use gcont_contour
      use gcont_gridder
      !---------------------------------------------------------------------
      ! @ public
      !	Setup internal parameters of Random_Map
      ! Arguments :
      !	N	I	Number of closest points NCP
      !	BLANK	R*4	Blanking value
      !	LOG	L (*)	Extrapolate or not
      !---------------------------------------------------------------------
      integer :: n                      !
      real :: blank                     !
      logical :: log(7)                 !
    end subroutine gridset
  end interface
  !
  interface
    subroutine gridini(nx,xref,xval,xinc,ny,yref,yval,yinc)
      use gcont_contour
      !---------------------------------------------------------------------
      ! @ public
      !	Transmit Random_Map gridded array parameters to external space
      ! Arguments :
      !	NX	I	Number of X (I) pixels
      !	XREF	R*8	X Reference pixel
      !	XVAL	R*8	X Value at reference pixel
      !	XINC	R*8	X Increment per I pixel
      !	NY	I	Number of Y (J) pixels
      !	YREF	R*8	Y Reference pixel
      !	YVAL	R*8	Y Value at reference pixel
      !	YINC	R*8	Y Increment per J pixel
      !---------------------------------------------------------------------
      integer :: nx                     !
      real*8 :: xref                    !
      real*8 :: xval                    !
      real*8 :: xinc                    !
      integer :: ny                     !
      real*8 :: yref                    !
      real*8 :: yval                    !
      real*8 :: yinc                    !
    end subroutine gridini
  end interface
  !
end module gcont_interfaces_public
