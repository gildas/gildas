subroutine gridran (yd,xd,zd,ndp,wk,iwk,scrarr,vectd,frstd,label,error)
  use gcont_interfaces, except_this=>gridran
  use gcont_contour
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  !	Stephane Guilloteau modification of CONRAN from ULIB 10-MAY-1984
  !	This is the pilot routine for random interpolation. Note that
  !	array XD and YD have been exchanged in order to obtain an
  !	interpolated grid with same organisation as the regular grid array.
  ! Arguments :
  !	YD	R*4 (*)	Array of X coordinates			Input
  !	XD	R*4 (*)	Array of Y coordinates			Input
  !	ZD	R*4 (*)	Array of values				Input
  !	NDP	I	Number of data points			Input
  !	WK	R*4 (*)	Work space
  !	IWK	I (*)	Work space
  !	SCRARR	R*4 (*)	Gridded space				Output
  !	VECTD	Ext	Vector drawing function			External
  !	FRTSD	Ext	Pen up drawing function			External
  !	LABEL	Ext	Label drawing function			External
  !	ERROR   L       Logical error flag
  ! Subroutines :
  !	CONTNG, CONDET, CONINT, CONBDN
  !---------------------------------------------------------------------
  real :: yd(*)                     !
  real :: xd(*)                     !
  real :: zd(*)                     !
  integer :: ndp                    !
  real :: wk(*)                     !
  integer :: iwk(*)                 !
  real :: scrarr(*)                 !
  external :: vectd                 !
  external :: frstd                 !
  external :: label                 !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='RANDOM'
  integer :: lngths(4),jwipc,it,k
  real :: x,y
  !
  save lngths,jwipc,it,k,x,y
  ! Data
  data lngths /13,4,21,6/
  !
  error = .false.
  !
  ! Set switch to map triangles, in CONLOC, for quick searches
  nit = 0
  irec = 1
  !
  ! Test to see if enough input data
  if (ncpsz.lt.ncp .or. ncp.lt.2) then
    call gcont_message(seve%e,rname,'Invalid number of neighbours')
    error = .true.
    return
  endif
  if (ndp.lt.ncp) then
    call gcont_message(seve%e,rname,'Too few data points')
    error = .true.
    return
  endif
  !
  iwk(1) = ndp
  iwk(2) = ncp
  iwk(3) = 1
  !
  ! Triangle point numbers
  jwipt = 16
  !
  ! Scratch space
  jwiwl = 6*ndp + 1
  !
  ! End points of border line segments and triangle number
  jwipl = 24*ndp + 1
  !
  ! Point numbers where the NCP data points around each point
  jwipc = 27*ndp + 1
  !
  ! Scratch space
  jwiwp = 30*ndp + 1
  !
  ! Partial derivatives at each data point
  ipr = 8*ndp + 1
  !
  if (.not.repeat) then
    !
    ! Triangulates the X-Y plane.
    call gcont_message(seve%i,rname,'Triangulating ... ')
    call contng(ndp,xd,yd,nt,iwk(jwipt),nl,iwk(jwipl),iwk(jwiwl),iwk(jwiwp),wk)
    if (irec.ne.1) then
      error = .true.
      return
    endif
    iwk(5) = nt
    iwk(6) = nl
    ntnl = nt+nl
    !
    ! Determines NCP points closest to each data point.
    call gcont_message(seve%i,rname,'Finding neighbours ...')
    call condet (ndp,xd,yd,ncp,iwk(jwipc))
    !
    ! Estimate the partial derivatives at all data points
  endif
  !
  ! Interpolate new values
  if (.not.loaded) then
    call gcont_message(seve%i,rname,'Gridding ... ')
    call conint (ndp,xd,yd,zd,ncp,iwk(jwipc),wk(ipr))
    !
    ! Verify data values valid
    nt = iwk(5)
    nl = iwk(6)
    ntnl = nt+nl
    !
    ! Load the gridded array
    call gcont_message(seve%i,rname,'Interpolating ...')
    k = 0
    x = xst
    do jx=1,ixmax
      y = yst
      do jy=1,iymax
        k = k+1
        ! scrarr(k) = concom(x,y,xd,yd,zd,ndp,wk,iwk,it)
        !
        ! Locate proper triangle
        call conloc(ndp,xd,yd,nt,iwk(jwipt),nl,iwk(jwipl),x,y,it,iwk(jwiwl),wk)
        !
        !  Interpolate the location
        call concal(xd,yd,zd,nt,iwk(jwipt),nl,iwk(jwipl),wk(ipr),it,x,y,  &
        scrarr(k),itpv)
        y = y + stpsy
      enddo
      x = x + stpsx
    enddo
  endif
  !
  if (pldvls) call conpdv(xd,yd,zd,ndp,label)
  if (look) call contlk(xd,yd,ndp,iwk(jwipt),vectd,frstd)
end subroutine gridran
!
subroutine gridset(n,blank,log)
  use gcont_interfaces, except_this=>gridset
  use gcont_contour
  use gcont_gridder
  !---------------------------------------------------------------------
  ! @ public
  !	Setup internal parameters of Random_Map
  ! Arguments :
  !	N	I	Number of closest points NCP
  !	BLANK	R*4	Blanking value
  !	LOG	L (*)	Extrapolate or not
  !---------------------------------------------------------------------
  integer :: n                      !
  real :: blank                     !
  logical :: log(7)                 !
  !
  call conbdn
  ncp = n
  lextrp = log(1)
  rblank = blank
  loaded = log(2)
  repeat = log(3)
  extri = log(4)
  look = log(5)
  pmimx = log(6)
  pldvls = log(7)
end subroutine gridset
!
subroutine gridini(nx,xref,xval,xinc,ny,yref,yval,yinc)
  use gcont_interfaces, except_this=>gridini
  use gcont_contour
  !---------------------------------------------------------------------
  ! @ public
  !	Transmit Random_Map gridded array parameters to external space
  ! Arguments :
  !	NX	I	Number of X (I) pixels
  !	XREF	R*8	X Reference pixel
  !	XVAL	R*8	X Value at reference pixel
  !	XINC	R*8	X Increment per I pixel
  !	NY	I	Number of Y (J) pixels
  !	YREF	R*8	Y Reference pixel
  !	YVAL	R*8	Y Value at reference pixel
  !	YINC	R*8	Y Increment per J pixel
  !---------------------------------------------------------------------
  integer :: nx                     !
  real*8 :: xref                    !
  real*8 :: xval                    !
  real*8 :: xinc                    !
  integer :: ny                     !
  real*8 :: yref                    !
  real*8 :: yval                    !
  real*8 :: yinc                    !
  !
  call conbdn
  !
  ! Note exchange of X and Y
  iymax = nx
  stpsy = xinc
  yst = xval + (1.d0-xref)*xinc
  yed = xval + (nx-xref)*xinc
  ixmax = ny
  stpsx = yinc
  xst = yval + (1.d0-yref)*yinc
  xed = yval + (ny-xref)*yinc
end subroutine gridini
!
subroutine conbdn
  use gcont_contour
  use gcont_gridder
  !---------------------------------------------------------------------
  ! @ private
  ! COMMON DATA
  !
  ! NOTE The common blocks listed include all the common used by
  !      the entire conran family, not all members will use all
  !      the common data.
  !---------------------------------------------------------------------
  logical, save :: initdone
  data initdone/.false./
  !
  ! Always reset this one: we have to forget the previous triangle
  ! memory when new data is loaded.
  itpv = 0  ! The triangle where the previous value came from
  !
  if (initdone) return
  !
  !   CONRA2
  repeat = .false.             ! Flag to triangulate and draw or just draw
  extrap = .false.             ! Plot data outside of convex data hull
  look = .true.                ! Plot triangles flag
  pldvls = .false.             ! Plot the data values flag
  pmimx = .false.              ! Flag to control the plotting of min's and max's
  !       EXTRI	- Only plot triangulation
  !   CONRA3
  irec = 1                     !       IREC	- Port recoverable error flag
  nerr = 0
  !
  !   CONRA4
  ncp = 4                      !   Number of data points used at each point for polynomial
  ncpsz = 25                   ! Max size allowed for NCP
  !
  !   CONERR
  nit = 0                      !  Flag to indicate status of search data base
  itipv = 0                    !    Last triangle interpolation occurred in
  !
  !  CONRA6
  xst = 0.0                    !   X coordinate start point for contouring
  yst = 0.0                    !   Y coordinate start point for contouring
  xed = 0.0                    !   X coordinate end point for contouring
  yed = 0.0                    !   Y coordinate end point for contouring
  stpsx = 0.0                  !  Step size for X change when contouring (Old STPSZ)
  stpsy = 0.0                  !  Step size for Y change when contouring (Old STPSZ)
  igrad = 40                   !  Number of graduations for contouring (scratch array size)
  ig = 40                      !  Reset value for IGRAD
  !
  !  CONRA7	- UNUSED -
  !
  !  CONRA8 	- UNUSED -
  !
  !  CONRA9
  !       X	- Array of X coordinates of contours drawn at current contour
  !          	  level
  !       Y	- Array of Y coordinates of contours drawn at current contour
  !          	  level
  np = 0                       !	- Count in X and Y
  tr = 0.0                     !	- Top right corner value of current cell
  br = 0.0                     !	- Bottom right corner value of current cell
  tl = 0.0                     !	- Top left corner value of current cell
  bl = 0.0                     !	- Bottom left corner value of current cell
  conv = 0.0                   !	- Current contour value
  xn = 0.0                     !	- X position where contour is being drawn
  yn = 0.0                     !	- Y position where contour is being drawn
  itll = 0                     !	- Triangle where top left corner of current cell lies
  ibll = 0                     !	- Triangle of bottom left corner
  itrl = 0                     !	- Triangle of top right corner
  ibrl = 0                     !	- Triangle of bottom left corner
  xc = 0.0                     !	- X coordinate of current cell
  yc = 0.0                     !	- Y corrdinate of current cell
  !
  jx = 0
  jy = 0
  iloc = 0
  ioc = 0
  nc = 0
  xo = 0.0
  yo = 0.0
  !
  ! CONR10: always reset (in particular optimization 'itpv')
  nt = 0                       ! - Number of triangles generated
  nl = 0                       ! - Number of line segments
  ntnl = 0                     ! - NT+NL
  jwipt = 0                    ! - Pointer into IWK where where triangle point numbers are stored
  jwiwl = 0                    ! - In IWK the location of a scratch space
  jwiwp = 0                    ! - In IWK the location of a scratch space
  jwipl = 0                    ! - In IWK the location of end points for border line segments
  ipr = 0                      ! - In WK the location of the partial derivitives at each data point
  itpv = 0                     ! - The triangle where the previous value came from
  !
  ! CONR11	- UNUSED -
  !
  ! CONR12
  !       IXMAX,IYMAX	- Maximum X and Y coordinates relative to the
  !                 scratch array, scrarr
  !       XUMAX,YUMAX	- Maximum X and Y coordinates relative to users
  !                 coordinate space
  !
  ! initialisation pour la subroutine STLINE
  inx(1) = -1
  inx(2) = -1
  inx(3) =  0
  inx(4) =  1
  inx(5) =  1
  inx(6) =  1
  inx(7) =  0
  inx(8) = -1
  iny(1) = 0
  iny(2) = 1
  iny(3) = 1
  iny(4) = 1
  iny(5) = 0
  iny(6) = -1
  iny(7) = -1
  iny(8) = -1
  ix = 0
  iy = 0
  idx = 0
  idy = 0
  is = 0
  iss = 0
  np2 = 0
  nr = 0
  ! GRIDDER.INC
  rblank = 0.0
  lextrp = .false.
  !
  initdone = .true.
end subroutine conbdn
