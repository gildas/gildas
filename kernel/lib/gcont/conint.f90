subroutine conint (ndp,xd,yd,zd,ncp,ipc,pd)
  !---------------------------------------------------------------------
  ! @ public
  !	This subroutine estimates partial derivatives of the first and
  !	second order at the data points.
  ! Arguments :
  !	NDP	I	Number of data points			Input
  !	XD	R*4 (*)	Array of X coordinates			Input
  !	YD	R*4 (*)	Array of Y coordinates			Input
  !	ZD	R*4 (*)	Array of values				Input
  !	NCP	I	Number of data points to be used for
  !			estimation of partial derivatives at
  !			each data point				Input
  !	IPC	I (*)	Array containing the point numbers of
  !			NCP data points closest to each of the
  !			NDP data point.				Input
  !	PD	R*4 (*) Array of dimension 5*NDP, where the
  !			estimated ZX, ZY, ZXX, ZXY, and ZYY
  !			values at the data points are to be
  !			stored.					Output
  ! No subroutines referenced
  !---------------------------------------------------------------------
  integer :: ndp                    !
  real :: xd(ndp)                   !
  real :: yd(ndp)                   !
  real :: zd(ndp)                   !
  integer :: ncp                    !
  integer :: ipc(*)                 !
  real :: pd(*)                     !
  ! Local
  real :: nmx, nmy, nmz, nmxx, nmxy, nmyx, nmyy
  integer :: ncpm1, ip0, jipc0, ic1, jipc, ipi, ic2mn
  real :: x0, y0, z0, dx1, dy1, dz1
  integer :: ic2, jpd0, jpd
  real :: dx2, dy2, dz2, dnmx, dnmy, dnmz
  real :: zx0, zy0, dzx1, dzy1, dzx2, dzy2
  real :: dnmxx, dnmxy, dnmyx, dnmyy
  !
  ! Preliminary processing
  !
  ncpm1 = ncp-1
  !
  ! Estimation of ZX and ZY
  !
  do ip0=1,ndp
    x0 = xd(ip0)
    y0 = yd(ip0)
    z0 = zd(ip0)
    nmx = 0.0
    nmy = 0.0
    nmz = 0.0
    jipc0 = ncp*(ip0-1)
    do ic1=1,ncpm1
      jipc = jipc0+ic1
      ipi = ipc(jipc)
      dx1 = xd(ipi)-x0
      dy1 = yd(ipi)-y0
      dz1 = zd(ipi)-z0
      ic2mn = ic1+1
      do ic2=ic2mn,ncp
        jipc = jipc0+ic2
        ipi = ipc(jipc)
        dx2 = xd(ipi)-x0
        dy2 = yd(ipi)-y0
        dnmz = dx1*dy2-dy1*dx2
        if (dnmz .eq. 0.0) cycle   ! IC2
        dz2 = zd(ipi)-z0
        dnmx = dy1*dz2-dz1*dy2
        dnmy = dz1*dx2-dx1*dz2
        if (dnmz .ge. 0.0) go to  100
        dnmx = -dnmx
        dnmy = -dnmy
        dnmz = -dnmz
100     nmx = nmx+dnmx
        nmy = nmy+dnmy
        nmz = nmz+dnmz
      enddo                    ! IC2
    enddo                      ! IC1
    jpd0 = 5*ip0
    pd(jpd0-4) = -nmx/nmz
    pd(jpd0-3) = -nmy/nmz
  enddo                        ! IP0
  !
  ! Estimation of ZXX, ZXY, and ZYY
  !
  do ip0=1,ndp
    jpd0 = jpd0+5
    x0 = xd(ip0)
    jpd0 = 5*ip0
    y0 = yd(ip0)
    zx0 = pd(jpd0-4)
    zy0 = pd(jpd0-3)
    nmxx = 0.0
    nmxy = 0.0
    nmyx = 0.0
    nmyy = 0.0
    nmz = 0.0
    jipc0 = ncp*(ip0-1)
    do ic1=1,ncpm1
      jipc = jipc0+ic1
      ipi = ipc(jipc)
      dx1 = xd(ipi)-x0
      dy1 = yd(ipi)-y0
      jpd = 5*ipi
      dzx1 = pd(jpd-4)-zx0
      dzy1 = pd(jpd-3)-zy0
      ic2mn = ic1+1
      do ic2=ic2mn,ncp
        jipc = jipc0+ic2
        ipi = ipc(jipc)
        dx2 = xd(ipi)-x0
        dy2 = yd(ipi)-y0
        dnmz = dx1*dy2-dy1*dx2
        if (dnmz .eq. 0.0) cycle   ! IC2
        jpd = 5*ipi
        dzx2 = pd(jpd-4)-zx0
        dzy2 = pd(jpd-3)-zy0
        dnmxx = dy1*dzx2-dzx1*dy2
        dnmxy = dzx1*dx2-dx1*dzx2
        dnmyx = dy1*dzy2-dzy1*dy2
        dnmyy = dzy1*dx2-dx1*dzy2
        if (dnmz .ge. 0.0) go to  140
        dnmxx = -dnmxx
        dnmxy = -dnmxy
        dnmyx = -dnmyx
        dnmyy = -dnmyy
        dnmz = -dnmz
140     nmxx = nmxx+dnmxx
        nmxy = nmxy+dnmxy
        nmyx = nmyx+dnmyx
        nmyy = nmyy+dnmyy
        nmz = nmz+dnmz
      enddo                    ! IC2
    enddo                      ! IC1
    pd(jpd0-2) = -nmxx/nmz
    pd(jpd0-1) = -(nmxy+nmyx)/(2.0*nmz)
    pd(jpd0) = -nmyy/nmz
  enddo                        ! IP0
end subroutine conint
