module gcont_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GCONT interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gcont_interfaces_public
  use gcont_interfaces_private
  !
end module gcont_interfaces
!
module gcont_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GCONT dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
end module gcont_dependencies_interfaces
