subroutine conpdv (xd,yd,zd,ndp,gwrite)
  !---------------------------------------------------------------------
  ! @ private
  !	Plot the data values on the contour map
  !	currently up to 10 characters for each value are displayed
  ! Arguments :
  !	XD	R*4 (*)	Array of X coordinates		Input
  !	YD	R*4 (*)	Array of Y coordinates		Input
  !	ZD	R*4 (*)	Array of values			Input
  !	NDP	I	Number of values		Input
  !	GWRITE	Ext	Label drawing routine		Input
  ! Subroutines :
  !	GWRITE
  !---------------------------------------------------------------------
  real :: xd(1)                     !
  real :: yd(1)                     !
  real :: zd(1)                     !
  integer :: ndp                    !
  external :: gwrite                !
  ! Local
  integer :: k
  !
  !  Loop and plot all values
  do k=1,ndp
    call gwrite(zd(k),xd(k),yd(k))
  enddo
end subroutine conpdv
