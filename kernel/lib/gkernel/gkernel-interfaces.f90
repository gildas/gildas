module gkernel_interfaces
  use ginc_interfaces_public
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
  use gwcs_interfaces_public
  use gfits_interfaces_public
  use gio_interfaces_public
  use ggui_interfaces_public_c
  use sic_interfaces_public
  use gtv_interfaces_public
  use greg_interfaces_public
end module gkernel_interfaces
