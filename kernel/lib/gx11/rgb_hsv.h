
#include "gsys/cfc.h"

#define hsv_to_rgb CFC_EXPORT_NAME( hsv_to_rgb)
#define rgb_to_hsv CFC_EXPORT_NAME( rgb_to_hsv)

extern void CFC_API hsv_to_rgb( float *h, float *s, float *v, float *r, float *g, float *b);
extern void CFC_API rgb_to_hsv( float *r, float *g, float *b, float *h, float *s, float *v);

