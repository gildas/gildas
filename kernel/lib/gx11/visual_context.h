
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define MAXCOL 127
#define MAXCOL_WITH_BLACK (MAXCOL+1)

struct visual_context {
    Display *display;
    Screen *screen_pt;
    Window root_window;
    Visual *visual;
    int depth;
    unsigned long white_pixel;
    unsigned long black_pixel;
    int static_display;
    int color_display;
    int is_default_visual;
    Colormap colormap;
    GC gc;
    XVisualInfo visual_info;
    XColor exact_defs[MAXCOL];
    unsigned char colmap[MAXCOL_WITH_BLACK];
    unsigned long red_max;
    unsigned long blue_max;
    unsigned long green_max;
    unsigned long red_mult;
    unsigned long blue_mult;
    unsigned long green_mult;
};
typedef struct visual_context visual_context_t;

extern visual_context_t g_vc;

void vc_build( Display *display, Screen *screen);
void vc_set_colormap( Window w);
void AffSimplePixmap( char *sdata, Drawable drawable, int sx, int sy);
void AffPixmap( char *sdata, Drawable drawable, int sx, int sy, int src_x, int src_y, int dest_x, int dest_y, int width, int height);

