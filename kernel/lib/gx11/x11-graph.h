
#include "gtv/xsub.h"
#include "gcore/glaunch.h"
#include <X11/Xutil.h>

#define NFIXED 8

typedef struct _gx11_env {
    G_env _herit;
	Window	win_main;
	Window	win_graph;
	GC		myGC;
	int		plume_cour;
	Pixmap	vertical,horizontal;
	Pixmap	box1,box2,box3,box4;
} gx11_env_t;

typedef struct _gx11_toolbar_args {
    gtv_toolbar_args_t _herit;
	gx11_env_t *env;
    char *window_name;
} gx11_toolbar_args_t;

int x11_open_display( char *display_name);
void x11_set_destroy_window_handler( void (*destroy_handler)(void *), void *destroy_data);
void x11_on_destroy( G_env *_env);
long x11_get_integer_property(Window win, char *name);
void x11_set_integer_property( Window win, char *name, long value);
void x11_delete_integer_property( Window win, char *name);

extern Display *display;
extern Window root_win;

