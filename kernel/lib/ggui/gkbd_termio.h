
#ifndef _SIC_KBDTERMIO_H_
#define _SIC_KBDTERMIO_H_

/**
 * @file
 * @see kbdtermio.c
 * This file should be included where you need termio control
 */

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void set_termio();
void reset_termio();
int get_termio_char( int code);

#endif /* _SIC_KBDTERMIO_H_ */

