
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#ifdef WIN32
#include <windows.h>
#include <conio.h>
#else /* WIN32 */
#include "gkbd_termio.h"
#endif /* WIN32 */

#include "gsys/cfc.h"

/* Define Fortran names */
#define hlp_more CFC_EXPORT_NAME( hlp_more)

#define CR '\r'

/**
 * Prints a message, asking for a Return or something else and erases the
 * message.
 *
 * @return 1 if character read is printable, 0 if not.
 */
int CFC_API hlp_more( )
{
#ifdef WIN32
	int cur_char;

	_cputs( "... Press RETURN for more ...");
	cur_char = _getch( );
	_cputs( "\r                             \r");

	return isprint( cur_char);
#else /* WIN32 */
	char cur_char;

	/* set the termio so we can do our own input processing */
	set_termio( );

	/* print the prompt if necessary */
	fputs( "... Press RETURN for more ...", stderr);

	/* get character */
	cur_char = getc( stdin);
	putc( CR, stderr);
	fputs( "                             ", stderr);
	putc( CR, stderr);
	reset_termio( );

	return isprint( cur_char);
#endif /* WIN32 */
}

