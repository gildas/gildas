
#ifndef _SIC_XGAG_H_
#define _SIC_XGAG_H_

/**
 * @file
 * @see sic_xgag.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/cfc.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

typedef struct {
	int lngth;
	char *chs;
} xgag_choicelst;

/* Calling sequence for Fortran subroutine 'help_button' */
typedef void (*SicCallbackHelp)(CFC_FString hlpfile, CFC_FString varname, CFC_FString buffer
    CFC_DECLARE_STRING_LENGTH(hlpfile) CFC_DECLARE_STRING_LENGTH(varname) CFC_DECLARE_STRING_LENGTH(buffer) );

#define end_dialog CFC_EXPORT_NAME( end_dialog)
#define get_user_input CFC_EXPORT_NAME( get_user_input)
#define xgag_button CFC_EXPORT_NAME( xgag_button)
#define xgag_choice CFC_EXPORT_NAME( xgag_choice)
#define xgag_ch CFC_EXPORT_NAME( xgag_ch)
#define xgag_dialog CFC_EXPORT_NAME( xgag_dialog)
#define xgag_file CFC_EXPORT_NAME( xgag_file)
#define xgag_logic CFC_EXPORT_NAME( xgag_logic)
#define xgag_open CFC_EXPORT_NAME( xgag_open)
#define xgag_separator CFC_EXPORT_NAME( xgag_separator)
#define xgag_show CFC_EXPORT_NAME( xgag_show)
#define xgag_slider CFC_EXPORT_NAME( xgag_slider)
#define xgag_update CFC_EXPORT_NAME( xgag_update)
#define xgag_begin_group CFC_EXPORT_NAME( xgag_begin_group)
#define xgag_end_group CFC_EXPORT_NAME( xgag_end_group)
#define xgag_wait CFC_EXPORT_NAME( xgag_wait)
#define xgag_set_callback_help CFC_EXPORT_NAME( xgag_set_callback_help)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void CFC_API end_dialog( void);
void CFC_API get_user_input( int *err_code);
void CFC_API xgag_button( CFC_FString title, CFC_FString command, CFC_FString label, int *length, CFC_FString helptxt, CFC_FString moretxt
                          CFC_DECLARE_STRING_LENGTH(title)
                          CFC_DECLARE_STRING_LENGTH(command)
                          CFC_DECLARE_STRING_LENGTH(label)
                          CFC_DECLARE_STRING_LENGTH(helptxt)
                          CFC_DECLARE_STRING_LENGTH(moretxt) );
void CFC_API xgag_ch( CFC_FString variable, CFC_FString txt, char *c, int *length,
                      int *win_id, int *editable
                      CFC_DECLARE_STRING_LENGTH(variable)
                      CFC_DECLARE_STRING_LENGTH(txt) );
void CFC_API xgag_choice( CFC_FString variable, CFC_FString txt, char *ch, int *length,
                          xgag_choicelst * liste, int *nch, int *ind, int *win_id
                          CFC_DECLARE_STRING_LENGTH(variable)
                          CFC_DECLARE_STRING_LENGTH(txt) );
void CFC_API xgag_dialog( CFC_FzString return_command, int *err_code);
void CFC_API xgag_file( CFC_FString variable, CFC_FString txt, char *c, int *length,
                        CFC_FString filter, int *win_id
                        CFC_DECLARE_STRING_LENGTH(variable)
                        CFC_DECLARE_STRING_LENGTH(txt)
                        CFC_DECLARE_STRING_LENGTH(filter) );
void CFC_API xgag_logic( CFC_FString variable, CFC_FString txt, int *flag, int *win_id
                         CFC_DECLARE_STRING_LENGTH(variable)
                         CFC_DECLARE_STRING_LENGTH(txt) );
void CFC_API xgag_open( CFC_FString txt, CFC_FString name, int *code
                        CFC_DECLARE_STRING_LENGTH(txt)
                        CFC_DECLARE_STRING_LENGTH(name) );
void CFC_API xgag_separator( int *win_id);
void CFC_API xgag_show( char *c, int *length, int *win_id);
void CFC_API xgag_slider( CFC_FString variable, CFC_FString txt, double *f,
                          double *min, double *max, int *win_id
                          CFC_DECLARE_STRING_LENGTH(variable)
                          CFC_DECLARE_STRING_LENGTH(txt) );
void CFC_API xgag_update( CFC_FString variable, CFC_FzString update
                          CFC_DECLARE_STRING_LENGTH(variable) );
void CFC_API xgag_begin_group( CFC_FzString label);
void CFC_API xgag_end_group( );
void CFC_API xgag_wait( );
void CFC_API xgag_set_callback_help( SicCallbackHelp fct);

void build_help_variable(const char *help_file, const char *variable_name, char *output_text);

#endif /* _SIC_XGAG_H_ */

