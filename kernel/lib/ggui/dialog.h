
#ifndef _GGUI_DIALOG_H_
#define _GGUI_DIALOG_H_

#include "gcore/gcomm.h"
#include "gcore/gwidget.h"

#define MINI(a,b) (((a)<(b))?(a):(b))
#define MAXI(a,b) (((a)>(b))?(a):(b))

#define GGUI_OK (-1)
/* #define GGUI_UPDATE (-2) */
#define GGUI_ABORT (-3)

typedef void widget_t;

typedef struct {
    generic_struct *generic;
    int need_redraw;
} widget_info_struct;
typedef void widget_info_t;

typedef struct {
    int dialog_id;
    int nb_items;
    int nb_actions;
    int group_box_created;
} dialog_info_t;

/* API to implement for each widget library */
typedef struct {
    void (*dialog_info_create_group_box)( dialog_info_t *dialog);
    void (*dialog_separator_add)( dialog_info_t *dialog);
    void (*dialog_chain_add)( dialog_info_t *dialog, chain_struct *chain);
    void (*chain_info_get_value)( widget_info_t *chain_info);
    void (*chain_info_set_value)( widget_info_t *chain_info, const char *value);
    void (*dialog_slider_add)( dialog_info_t *dialog, slider_struct *slider);
    void (*slider_info_set_value)( widget_info_t *slider_info, double value);
    void (*dialog_show_add)( dialog_info_t *dialog, show_struct *show);
    void (*dialog_logic_add)( dialog_info_t *dialog, logic_struct *logic);
    void (*logic_info_get_value)( widget_info_t *logic_info);
    void (*logic_info_set_value)( widget_info_t *logic_info, int value);
    void (*dialog_browser_add)( dialog_info_t *dialog, file_struct *file);
    void (*file_info_get_value)( widget_info_t *file_info);
    void (*file_info_set_value)( widget_info_t *file_info, const char *value);
    void (*dialog_choice_add)( dialog_info_t *dialog, choice_struct *choice);
    void (*choice_info_get_value)( widget_info_t *choice_info);
    void (*choice_info_set_value)( widget_info_t *choice_info, const char *value);
    void (*dialog_button_add)( dialog_info_t *dialog, button_struct *button);
    void (*dialog_on_create_item)( dialog_info_t *dialog, generic_struct *generic);
    void (*dialog_close)( dialog_info_t *dialog);
} widget_api_t;

int widget_get_index( widget_t *generic);
widget_info_t *widget_info_new( int size, widget_t *generic);
widget_info_t *widget_info_find( int index);
void widget_info_open( void);
void widget_info_close( void (*on_widget_info_close)( widget_info_struct *));

void dialog_info_prepare( dialog_info_t *dialog, int dialog_id);
void dialog_info_build( widget_api_t *api, dialog_info_t *dialog, int dialog_id);
int update_other_widgets( widget_api_t *api, widget_t *generic);
void widget_update_variable( widget_api_t *api, sic_widget_def_t *widget);
void dialog_widget_set_need_redraw( int widget_index);
void widget_update( widget_api_t *api, int widget_index);
void save_context( widget_api_t *api, int win, int destroy_widgets);
void gdialog_build_help( const char *help_file, const char *variable_name,
    char *output_text, int *nb_cols, int *nb_rows);
int on_close_dialog( widget_api_t *api, dialog_info_t *dialog_info, char *return_command, int returned_code);
void on_run_dialog( void (*update_variable)( int, sic_widget_def_t *, void *), void *data);
button_struct *widget_find_button_from_window_id( int window_id);
void *parse_menu_button_begin( );
button_struct *parse_menu_button_next( void *iter);
char *choice_get_value( choice_struct *choice);
void choice_set_value( choice_struct *choice, char *value);
int choice_get_index( choice_struct *choice);
void choice_set_index( choice_struct *choice, int index);

#endif /* _GGUI_DIALOG_H_ */
