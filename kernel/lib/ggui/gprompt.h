
#ifndef _GPROMPT_H_
#define _GPROMPT_H_

/**
 * @file
 * @see gprompt.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/cfc.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

#define xgag_settimer_status CFC_EXPORT_NAME( xgag_settimer_status)
#define xgag_gettimer_status CFC_EXPORT_NAME( xgag_gettimer_status)
#define xgag_settimer CFC_EXPORT_NAME( xgag_settimer)
#define xgag_gettimer CFC_EXPORT_NAME( xgag_gettimer)
#define xgag_gettimer_command CFC_EXPORT_NAME( xgag_gettimer_command)
#define xgag_settimer_command CFC_EXPORT_NAME( xgag_settimer_command)
#define init_gui CFC_EXPORT_NAME( init_gui)
#define prompt_loop CFC_EXPORT_NAME( prompt_loop)
#define read_linec CFC_EXPORT_NAME( read_linec)
#define gprompt_set_command_line CFC_EXPORT_NAME( gprompt_set_command_line)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void CFC_API xgag_settimer_status( int *ns);
void CFC_API xgag_gettimer_status( int *ns);
void CFC_API xgag_settimer( int *ns);
void CFC_API xgag_gettimer( int *ns);
void CFC_API xgag_gettimer_command( CFC_FString command_line CFC_DECLARE_STRING_LENGTH( command_line));
void CFC_API xgag_settimer_command( CFC_FString command_line CFC_DECLARE_STRING_LENGTH( command_line));
void CFC_API init_gui( );
void CFC_API prompt_loop( int *code);
int CFC_API read_linec( CFC_FzString prompt, CFC_FzString line, int *code, int *wait_command);
void CFC_API gprompt_set_command_line( CFC_FString command_line CFC_DECLARE_STRING_LENGTH( command_line));

#endif /* _GPROMPT_H_ */

