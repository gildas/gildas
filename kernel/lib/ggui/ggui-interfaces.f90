module ggui_interfaces_public_c
  !---------------------------------------------------------------------
  ! Fortran interfaces to C public subroutines (hand made)
  !---------------------------------------------------------------------
  !
  interface
    function hlp_more()
      integer(kind=4) :: hlp_more
    end function hlp_more
  end interface
  !
  interface
    subroutine gkbd_f_histo_set_filename(filename)
      character(len=*), intent(in) :: filename
    end subroutine gkbd_f_histo_set_filename
  end interface
  !
  interface
    function kbd_line(prompt,line,code)
      integer(kind=4) :: kbd_line
      character(len=*), intent(in)    :: prompt
      character(len=*), intent(out)   :: line
      integer(kind=4),  intent(inout) :: code
    end function kbd_line
  end interface
  !
  interface
    subroutine prompt_loop(code)
      integer(kind=4), intent(inout) :: code
    end subroutine prompt_loop
  end interface
  !
  interface
    function read_linec(prompt,line,code,wait_command)
      integer(kind=4) :: read_linec
      character(len=*), intent(inout) :: prompt
      character(len=*), intent(inout) :: line
      integer(kind=4),  intent(inout) :: code
      integer(kind=4),  intent(in)    :: wait_command
    end function read_linec
  end interface
  !
  interface
    subroutine xgag_command(label,command)
      character(len=*), intent(in) :: label
      character(len=*), intent(in) :: command
    end subroutine xgag_command
  end interface
  !
  interface
    subroutine xgag_detach(title,name)
      character(len=*), intent(in) :: title
      character(len=*), intent(in) :: name
    end subroutine xgag_detach
  end interface
  !
  interface
    subroutine xgag_end_detach(name)
      character(len=*), intent(in) :: name
    end  subroutine xgag_end_detach
  end interface
  !
  interface
    subroutine xgag_endmenu()
    end subroutine xgag_endmenu
  end interface
  !
  interface
    subroutine xgag_launch()
    end subroutine xgag_launch
  end interface
  !
  interface
    subroutine xgag_menu(label)
      character(len=*), intent(in) :: label
    end subroutine xgag_menu
  end interface
  !
  interface
    subroutine xgag_settimer_status(ns)
      integer(kind=4), intent(in) :: ns
    end subroutine xgag_settimer_status
    subroutine xgag_gettimer_status(ns)
      integer(kind=4), intent(out) :: ns
    end subroutine xgag_gettimer_status
    subroutine xgag_settimer(ns)
      integer(kind=4), intent(in) :: ns
    end subroutine xgag_settimer
    subroutine xgag_gettimer(ns)
      integer(kind=4), intent(out) :: ns
    end subroutine xgag_gettimer
    subroutine xgag_gettimer_command(command)
      character(len=*), intent(out) :: command
    end subroutine xgag_gettimer_command
    subroutine xgag_settimer_command(command)
      character(len=*), intent(in) :: command
    end subroutine xgag_settimer_command
  end interface
  !
  interface
    subroutine xgag_wait()
    end subroutine xgag_wait
  end interface
  !
  interface
    subroutine end_dialog()
    end subroutine end_dialog
  end interface
  !
  interface
    subroutine get_user_input(err_code)
      integer(kind=4), intent(out) :: err_code
    end subroutine get_user_input
  end interface
  !
  interface
    subroutine xgag_button(title,command,label,length,helptxt,moretxt)
      character(len=*), intent(in) :: title
      character(len=*), intent(in) :: command
      character(len=*), intent(in) :: label
      integer(kind=4),  intent(in) :: length
      character(len=*), intent(in) :: helptxt
      character(len=*), intent(in) :: moretxt
    end subroutine xgag_button
  end interface
  !
  interface
    subroutine xgag_dialog(return_command,err_code)
      character(len=*), intent(in) :: return_command
      integer(kind=4)              :: err_code
    end subroutine xgag_dialog
  end interface
  !
  interface
    subroutine xgag_open(txt,name,code)
      character(len=*), intent(in) :: txt
      character(len=*), intent(in) :: name
      integer(kind=4)              :: code
    end subroutine xgag_open
  end interface
  !
  interface
    subroutine xgag_separator(win_id)
      integer(kind=4), intent(in) :: win_id
    end subroutine xgag_separator
  end interface
  !
  interface
    subroutine xgag_update(variable,update)
      character(len=*), intent(in) :: variable
      character(len=*), intent(in) :: update
    end subroutine xgag_update
  end interface
  !
  interface
    subroutine xgag_begin_group(label)
      character(len=*) :: label
    end subroutine xgag_begin_group
  end interface
  !
  interface
    subroutine xgag_end_group()
    end subroutine xgag_end_group
  end interface
  !
  interface
    subroutine gprompt_set_command_line(command_line)
      character(len=*), intent(in) :: command_line
    end subroutine gprompt_set_command_line
  end interface
  !
  interface
    subroutine xgag_end_detach_all()
    end subroutine xgag_end_detach_all
  end interface
  !
  interface
    subroutine xgag_uri_menu(label,uri)
      character(len=*), intent(in) :: label
      character(len=*), intent(in) :: uri
    end subroutine xgag_uri_menu
  end interface
  !
  interface
    subroutine xgag_set_callback_help(sub)
      external :: sub
    end subroutine xgag_set_callback_help
  end interface
  !
end module ggui_interfaces_public_c
