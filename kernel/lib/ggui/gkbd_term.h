
#ifndef _SIC_KBDTERM_H_
#define _SIC_KBDTERM_H_

/**
 * @file
 * @see kbdterm.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/cfc.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

#define kbd_line CFC_EXPORT_NAME( kbd_line)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void reset_kbd_line( char *prompt, char *line, int *code);
int CFC_API kbd_line( char *prompt, char *line, int *code);

#endif /* _SIC_KBDTERM_H_ */

