
#ifndef _GMENU_H_
#define _GMENU_H_

/**
 * @file
 * @see gmenu.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/cfc.h"
#include "gcore/gcomm.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

typedef struct {
    char name[256];
    sic_task_t task;
#ifdef WIN32
    HWND hWnd;
#endif
} win_struct_t;

#define NWINDOWS 10
extern win_struct_t detach_win[NWINDOWS];

#define xgag_command CFC_EXPORT_NAME( xgag_command)
#define xgag_uri_menu CFC_EXPORT_NAME( xgag_uri_menu)
#define xgag_detach CFC_EXPORT_NAME( xgag_detach)
#define xgag_end_detach CFC_EXPORT_NAME( xgag_end_detach)
#define xgag_end_detach_all CFC_EXPORT_NAME( xgag_end_detach_all)
#define xgag_endmenu CFC_EXPORT_NAME( xgag_endmenu)
#define xgag_launch CFC_EXPORT_NAME( xgag_launch)
#define xgag_menu CFC_EXPORT_NAME( xgag_menu)

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

char *xgag_get_detach_name();

void CFC_API xgag_command( CFC_FzString label, CFC_FzString command);
void CFC_API xgag_uri_menu( CFC_FzString label, CFC_FzString uri);
void CFC_API xgag_detach( CFC_FzString txt, CFC_FzString name);
void CFC_API xgag_end_detach( CFC_FzString name);
void CFC_API xgag_endmenu( );
void CFC_API xgag_launch( );
void CFC_API xgag_menu( CFC_FzString label);

#endif /* _GMENU_H_ */

