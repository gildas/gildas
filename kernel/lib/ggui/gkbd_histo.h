
#ifndef _GKBD_HISTO_H_
#define _GKBD_HISTO_H_

void gkbd_histo_set_filename( char *filename);
void gkbd_histo_enable( );
void gkbd_histo_disable( );
void gkbd_histo_reset( );
void gkbd_histo_push( char *line);
void gkbd_histo_pop( );
void gkbd_histo_prev( char *line);
void gkbd_histo_next( char *line);
void gkbd_histo_save( );
void gkbd_histo_load( );

#endif /* _GKBD_HISTO_H_ */

