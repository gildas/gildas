!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GCHAR messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gchar_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gchar_message_id = gpack_global_id  ! Default value for startup message
  !
end module gchar_message_private
!
subroutine gchar_message_set_id(id)
  use gchar_interfaces, except_this=>gchar_message_set_id
  use gchar_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gchar_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gchar_message_id
  call gchar_message(seve%d,'gchar_message_set_id',mess)
  !
end subroutine gchar_message_set_id
!
subroutine gchar_message(mkind,procname,message)
  use gchar_dependencies_interfaces
  use gchar_interfaces, except_this=>gchar_message
  use gchar_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gchar_message_id,mkind,procname,message)
  !
end subroutine gchar_message
