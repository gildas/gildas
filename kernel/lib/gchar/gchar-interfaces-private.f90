module gchar_interfaces_private
  interface
    subroutine vsconv(ficode,sycode)
      use gchar_variables
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: ficode        !
      character(len=*) :: sycode        !
    end subroutine vsconv
  end interface
  !
  interface
    subroutine gchar_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gchar_message
  end interface
  !
  interface
    subroutine gsymxd(number,xygrid,nstrok,unused)
      !---------------------------------------------------------------------
      ! @ private
      ! GRPCKG (internal routine):
      !	Return the digitization coordinates of a character. Each
      !	character is built up on a grid with X and Y coordinates
      !	in the range (-49,49), with the origin (0,0) at the center
      !	of the character.  The coordinate system is right-handed,
      !	with X positive to the right, and Y positive upward.
      !	Characters are drawn with their tops upward, towards
      !	positive Y values.
      !
      ! Arguments:
      ! 	NUMBER 	I	symbol number in range (1..3000).	Input
      ! 	XYGRID 	L*1(*)	Height range, width range, and pairs	Output
      !			of (X,Y) coordinates returned.
      !			Height range = (XYGRID(1),XYGRID(3)).
      !			Width range = (XYGRID(4),XYGRID(5)).
      !			(X,Y) = (XYGRID(K),XYGRID(K+1)) (K=6,8,...).
      !	NSTROK	I	Number of strokes			Output
      ! 	UNUSED	L	Set to .TRUE. if NUMBER is an unused 	Output
      !			symbol number. A character of normal
      !			height and zero width is returned.
      !			Set to .FALSE. if NUMBER is a valid
      !			symbol number.
      !
      !	The height range consists of 3 values: (minimum Y, baseline
      !	Y, maximum Y).  The first is reached by descenders on
      !	lower-case g, p, q, and y.  The second is the bottom of
      !	upper-case letters.  The third is the top of upper-case
      !	letters.  A coordinate pair (-64,0) requests a pen raise,
      !	and a pair (-64,-64) terminates the coordinate list.  It is
      !	assumed that movement to the first coordinate position will
      !	be done with the pen raised - no raise command is
      !	explicitly included to do this.
      !
      ! (7-Mar-1983)
      !---------------------------------------------------------------------
      integer :: number                 !
      integer(kind=1) :: xygrid(400)    !
      integer :: nstrok                 !
      logical :: unused                 !
    end subroutine gsymxd
  end interface
  !
  interface
    subroutine bldfil
      !---------------------------------------------------------------------
      ! @ private
      !  (build HERSHEY font file)
      !  This program converts the character digitizations on the
      !  distribution HERSHEY font file to a series of records on a
      !  fortran unformatted (binary) file in which coordinate pairs
      !  have been packed into single integers to reduce the space
      !  requirements.  The font file contains the character
      !  digitizations in record groups, as follows:
      !
      !  first: NC,LENGTH,(XYGRID(I),I=1,5)
      !  remaining: (XYGRID(I),I=6,LENGTH)
      !
      !  the format for each record is (7(2X,2I4)).  NC is the
      !  HERSHEY character number, which at present consists of one of
      !  1611 integers in the range (1..NCLAST), with NCLAST = 4000.
      !  these character numbers must never be altered, since
      !  the entire set of routines which handle the hershey fonts
      !  use them. The first five entries in XYGRID(*) are the
      !  character extents:
      !
      !  XYGRID(1) = minimum Y value.
      !  XYGRID(2) = baseline Y value.
      !  XYGRID(3) = maximum Y value.
      !  XYGRID(4) = minimum X value.
      !  XYGRID(5) = maximum X value.
      !
      !  the minimum and maximum values do not necessarily
      !  correspond to the actual (X,Y) coordinates in the remainder
      !  of XYGRID(*), but specify the box in which characters are
      !  drawn.  The center of each character grid is at (0,0), and
      !  actual coordinates lie in the range (-49..+49).  a
      !  coordinate pair (-64,0) is a request to raise the pen
      !  before moving to the next point, and a pair (-64,-64)
      !  signals end-of-data for the current character.
      !
      !  By biasing each coordinate by +64, the coordinate range
      !  becomes (0..113), which can be represented by 7 bits.  Thus
      !  an (x,y) pair can be packed into 14 data bits.  The
      !  distribution version of <PLOT79> packs only one pair per
      !  integer word, so that the system can be installed without
      !  change on any machine with 16 or more bits/integer.  Later
      !  on, it may be desirable to increase the number of
      !  coordinate pairs stored in a single word.  On machines
      !  which permit this, it may be most convenient to define the
      !  data arrays as integer*2 or integer*3, rather than to
      !  revise the packing algorithm.
      !
      !  The entire set of fonts contains at present 105,557
      !  individual coordinate values, to which space for pointers
      !  to individual characters must be added.  If the coordinates
      !  are packed as (X,Y) pairs in 16-bit integers, then about
      !  57,000 16-bit integers are required for the coordinates and
      !  pointers. Since this is an excessive amount of storage on
      !  many machines, the character data is subdivided into
      !  smaller sections and stored as fortran unformatted (binary)
      !  records. Each such record contains the data list
      !
      !  NC1,NC2,NC3,INDEX,BUFFER
      !
      !  the data items in the record are
      !
      !  NC1......... starting character number in buffer.
      !  NC2......... last character number in buffer.
      !  NC3......... starting character number in next buffer, if there
      !               would be one.
      !  INDEX(*).... index array, locating the start of data
      !               for character K (assumed in range NC1..NC2) at
      !               BUFFER(INDEX(K+1-NC1)).
      !  BUFFER(*)... array for packed coordinates.
      !
      !  The actual size of BUFFER(*) and INDEX(*) may be adjusted
      !  to an installation's requirements, and the code in
      !  subroutine SYMXD must be appropriately modified to reflect
      !  any change in the size of these arrays.
      !
      !  This version of BLDFIL omits the indexical, triplex, and
      !  gothic fonts (Hershey numbers 1000-1999, 3000-3999). A
      !  buffer size of 27000 words and an index size of 3000 are
      !  adequate.
      !
      ! (09-aug-80)
      !---------------------------------------------------------------------
    end subroutine bldfil
  end interface
  !
  interface
    subroutine wwrite(iunit,array,block)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: iunit                  !
      integer :: array(128)             !
      integer :: block                  !
    end subroutine wwrite
  end interface
  !
  interface
    subroutine coresp(i)
      use gchar_variables
      !---------------------------------------------------------------------
      ! @ private
      ! CHAR
      !	Create the correspondance table between Hershey and Greg fonts
      !---------------------------------------------------------------------
      integer :: i(nkchar)              !
    end subroutine coresp
  end interface
  !
end module gchar_interfaces_private
