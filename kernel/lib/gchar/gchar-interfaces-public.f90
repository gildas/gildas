module gchar_interfaces_public
  interface
    subroutine vstrin(nchar,string,drawit,slengt,xp,yp,angle,ifont,size,  &
      doclip,gpoly_noclip,gpoly_clip)
      use gildas_def
      use phys_const
      use gchar_variables
      !---------------------------------------------------------------------
      ! @ public
      !
      !	Interprets a string of character with escape sequences
      !	\P or \\P  and plots out the characters according to
      !	current font quality SIMPLEX or DUPLEX
      !
      !	If the shareable common is not initialised, VSTRING attempts
      !	to initialise it using VSINIT. Just do not forget to assign
      !	properly the CHAR_FONT logical name to your GREGFONT.BIN file
      !	when you run a non-shareable image.
      !
      !	The reference point is located at the left of the string, and
      !	at middle in height. To append a second string, just move the
      !	reference point as following before calling again VSTRING :
      !		...
      !		CO = COS(ANGLE*PI/180.)
      !		SI = SIN(ANGLE*PI/180.)
      !		CALL VSTRING (..., SLENGT, XP, YP, ANGLE, ...)
      !		XP = XP + CO*SLENGT
      !		YP = YP + SI*SLENGT
      !		CALL VSTRING (..., SLENGT, XP, YP, ANGLE, ...)
      !
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: nchar         ! String length
      character(len=*), intent(in)  :: string        ! String to be drawn
      logical,          intent(in)  :: drawit        ! Draw string or compute length ?
      real(kind=4),     intent(out) :: slengt        ! Length of string in centimeters
      real(kind=4),     intent(in)  :: xp            ! Abscissa of reference point
      real(kind=4),     intent(in)  :: yp            ! Ordinate of reference point
      real(kind=4),     intent(in)  :: angle         ! Angle (Degrees)
      integer(kind=4),  intent(in)  :: ifont         ! Quality 0=Simplex 1=Duplex
      real(kind=4),     intent(in)  :: size          ! Character size
      logical,          intent(in)  :: doclip        ! Clip with current Greg box?
      external                      :: gpoly_noclip  !
      external                      :: gpoly_clip    !
      interface
        subroutine gpoly_noclip(n,vx,vy,error)
          integer, intent(in)    :: n      ! Number of points
          real*4,  intent(in)    :: vx(n)  ! Array of abscissae
          real*4,  intent(in)    :: vy(n)  ! Array of ordinates
          logical, intent(inout) :: error  ! Logical error flag
        end subroutine gpoly_noclip
        subroutine gpoly_clip(nq,xq,yq)
          use gildas_def
          integer(kind=size_length), intent(in) :: nq     ! Number of points
          real(kind=4),              intent(in) :: xq(*)  ! Plot X coordinates
          real(kind=4),              intent(in) :: yq(*)  ! Plot Y coordinates
        end subroutine gpoly_clip
      end interface
    end subroutine vstrin
  end interface
  !
  interface
    subroutine gchar_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine gchar_message_set_id
  end interface
  !
  interface
    subroutine hershey2gag(file)
      use gchar_variables
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Build of the GREG character set from the HERSHEY one
      !
      ! Organisation :
      !	2 Quality of characters
      !		1 FAST		2 DUPLEX
      !	3 Set of characters per quality
      !		1 ROMAN		2 GREEK		3 SCRIPT
      ! 	95 Characters per set
      !
      ! Addressing is based on the ASCII code of the character:
      ! INDEX = ICHAR(caractere) - ICHAR(' ') + 1 + 100*(NJEU-1) + 300*(NQUAL-1)
      !---------------------------------------------------------------------
      character(len=*) :: file          !
    end subroutine hershey2gag
  end interface
  !
end module gchar_interfaces_public
