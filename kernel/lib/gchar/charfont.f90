subroutine vsinit (array,npage)
  use gildas_def
  use gbl_format
  use gbl_message
  use gchar_dependencies_interfaces
  use gchar_interfaces
  !---------------------------------------------------------------------
  ! @ no-interface (because 'vstrsh1' is used as 1st word of a common block)
  ! CHAR	Initialise the common from the disk file
  !---------------------------------------------------------------------
  integer :: npage                  !
  logical :: array(128,npage)       !
  ! Local
  character(len=4) :: ficode,sycode
  integer :: i,ier,lun
  character(len=filename_length) :: filnam
  !
  if (.not.sic_query_file('gag_font','data#dir:','',filnam)) then
    call gchar_message(seve%f,'VSINIT','gag_font not found')
    call sysexi(fatale)
  endif
  !
  ier = sic_getlun(lun)
  if (mod(ier,2).eq.0) call sysexi (ier)
  open(unit=lun,status='OLD',file=filnam,form='UNFORMATTED',access='DIRECT',  &
       recl=facunf*128)
  do i=1,npage
    call wread (lun,array(1,i),i)
  enddo
  !
  call bytoch(array(1,1),ficode,4)
  call gdf_getcod(sycode)
  !
  if (ficode.ne.sycode) then
    call gchar_message(seve%w,'CHAR','Font File not in native format')
    call vsconv (ficode,sycode)
  else
    call gchar_message(seve%d,'CHAR','Fonts loaded')
  endif
  close(unit=lun)
  call sic_frelun (lun)
end subroutine vsinit
!
subroutine wread (iunit,array,block)
  !---------------------------------------------------------------------
  ! @ no-interface (because of type mismatch)
  !---------------------------------------------------------------------
  integer :: iunit                  !
  integer :: array(128)             !
  integer :: block                  !
  read  (unit=iunit,rec=block) array
end subroutine wread
