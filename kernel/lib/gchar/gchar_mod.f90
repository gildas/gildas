!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gchar_variables
  !--------------------------------------------------------------------
  ! Definition of gchar global variables
  !--------------------------------------------------------------------
  ! Only 26826 used up to now
  integer, parameter :: nkbyte=28000
  integer, parameter :: nkchar=600
  integer, parameter :: nksize=4+(3*2+4)*nkchar+nkbyte  ! Number of bytes needed by the common
  integer, parameter :: nkpage=(nksize+511)/512         ! Number block of 512 bytes = 128 words
  integer, parameter :: nkempt=(512*nkpage-nksize)/4    ! Padding for the last block
  !
  logical :: vstrsh1                  ! First word in common
  integer(2) :: nstrok(nkchar)        !
  integer(2) :: ladj(nkchar)          !
  integer(2) :: radj(nkchar)          !
  integer    :: pointe(nkchar)        !
  integer(kind=1) :: font(0:nkbyte-1) !
  integer :: lastad(nkempt)           ! Padding array
  !
  ! The following common is required, because all these variables
  ! must be aligned in memory.
  common /vstrsh/ vstrsh1,nstrok,ladj,radj,pointe,font,lastad
end module gchar_variables
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
