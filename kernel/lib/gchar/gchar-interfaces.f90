module gchar_interfaces
  !---------------------------------------------------------------------
  ! GCHAR interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gchar_interfaces_public
  use gchar_interfaces_private
  !
end module gchar_interfaces
!
module gchar_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GCHAR dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gio_interfaces_public
end module gchar_dependencies_interfaces
