
/**
 * @file
 * This file defines the communication API involved in Gildas software.
 * All inter process communication features are encapsulated inside this API.
 * The main part consists of a communication board which is created by the main
 * process and opened by child processes. This board is defined by a unique
 * integer which is passed to each process.
 * Synchronization between parallel tasks is provided throw post and wait
 * operations. A wait operation is a blocking call until a post operation is
 * performed. A post operation always returns immediatly.
 * As regards asynchronous events, they are handled by listeners and triggers.
 * A event listener must be registered with the specific handler to call when
 * an event is triggered.
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

/* define _GNU_SOURCE to enable declaration of strsignal */
#define _GNU_SOURCE

#include "gcomm.h"

#ifndef WIN32
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#endif
char *strsignal(int);

#if defined(WIN32)
#else
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <semaphore.h>
#ifndef WIN32
#include <pthread.h>
#ifdef linux
#include <sys/syscall.h>
#endif
#endif /* WIN32 */
#endif
#ifdef cygwin
#include <limits.h>
#endif /* cygwin */
#ifdef darwin
#ifndef PATH_MAX
#include <sys/syslimits.h>  /* only if PATH_MAX missing from limits.h */
#endif
#endif /* darwin */

#include "gerror.h"
#include "gsys/gag_trace.h"
#include "gcore-message-c.h"

/***************************************************************************** 
 *                          Macros & Definitions                             * 
 *****************************************************************************/

#define SIC_DEBUG_COMM

#include <time.h>

#if defined(darwin) || defined(cygwin)
#define SIC_USE_SEM_OPEN
#endif

#ifndef SIGCLD
#define SIGCLD SIGCHLD
#endif /* SIGCLD */

#ifdef SIC_DEBUG_COMM
#define SIC_ASSERT( message, condition) \
    { if (!(condition)) fprintf( stderr, "SIC_ASSERT: %s\n", message); }
#else /* SIC_DEBUG_COMM */
#define SIC_ASSERT( message, condition)
#endif /* SIC_DEBUG_COMM */

#define MAX_ARGC 32
#define MAX_ARGV 256

#define SIC_MAX_WIDGET_ON_BOARD 1000
#define SIC_MAX_SHM_NAME 32
#define SIC_MAX_SEM_NAME_FOR_PORTABILITY 14
#define SIC_MAX_HANDLERS 32

/* Event Ids */
#define SIC_MODIFIED_VARIABLE_EVENT 1
#define SIC_REDRAW_PROMPT_EVENT 2
#define SIC_EXIT_EVENT 3

//#define _SIC_USE_MUTEX
#if defined(_SIC_USE_MUTEX)
typedef struct {
#ifdef GAG_USE_DEBUG
    int value;
#endif
    pthread_mutex_t sem_id;
} sic_sem_t;
#define SIC_SEM_ID(x) (&(x)->sem_id)
#elif defined(WIN32)
typedef struct {
    HANDLE sem_id;
} sic_sem_t;
#define SIC_SEM_ID(x) ((x)->sem_id)
#else
typedef struct {
#ifdef SIC_USE_SEM_OPEN
    sem_t *sem_id;
#define SIC_SEM_ID(x) ((x)->sem_id)
#else /* SIC_USE_SEM_OPEN */
    sem_t sem_id;
#define SIC_SEM_ID(x) (&(x)->sem_id)
#endif /* SIC_USE_SEM_OPEN */
    char name[SIC_MAX_SEM_NAME_FOR_PORTABILITY];
} sic_sem_t;

#endif

typedef struct {
    sic_task_t task;
    int event_id;
    void (*listener)();
    void *data;
    int sig_num;
} sic_listener_handler_t;

typedef struct {
    sic_task_t master_task;
    sic_task_t keyboard_task;
    int trace_fd;
    sic_sem_t comm_board_access;
    sic_sem_t draw_prompt;
    sic_sem_t synchronous_mode;
    sic_sem_t write_command;
    sic_sem_t read_command;
    sic_sem_t listener_acquit;
    sic_sem_t widget_board_access;
    sic_sem_t widget_created;
    sic_sem_t event_stack_access;
    int widget_board_id;
    command_line_t command_line;
    char pushed_command_text[MAXBUF];
    sic_command_from_t from;
    int listener_count;
    sic_listener_handler_t listeners[SIC_MAX_HANDLERS];
    int event_id;
} sic_comm_board_t;

typedef struct {
    char window_title[TITLELENGTH];
    char help_filename[HLPFILELNGTH];
    char returned_command[COMMANDLENGTH];
    int returned_code;
    int modified_widget_index;
    sic_widget_def_t modified_widget;
    int widget_count;
    sic_widget_def_t widgets[SIC_MAX_WIDGET_ON_BOARD];
} sic_widget_board_t;

/* Communication board */
static int s_comm_board_id = -1;
static char s_comm_shm_name[SIC_MAX_SHM_NAME];
static sic_comm_board_t *s_comm_board = NULL;
static sic_comm_board_t *s_sem_comm_board = NULL;

/* Widget board */
static char s_widget_shm_name[SIC_MAX_SHM_NAME];
static sic_widget_board_t *s_widget_board = NULL;

static sic_task_t _sic_task_null = SIC_TASK_NULL;

static char alarm_command[COMMANDLENGTH] = "EXIT";

/***************************************************************************** 
 *                        Internal function bodies                           * 
 *****************************************************************************/

static int sic_shm_create( const char* name, int size)
{
    return 0;
}

static void sic_shm_destroy( const char* name, int shm_id)
{
}

static void sic_shm_detach( void *addr, int size)
{
}

static void sic_sem_open( sic_sem_t *sem)
{
#ifdef SIC_USE_SEM_OPEN
    static char sem_name[SIC_MAX_SEM_NAME_FOR_PORTABILITY];
    static int count = 0;
    sem_t *ret;

#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    gag_trace("/S%8.8x%2.2x", getpid( ), ++count);
    sprintf( sem_name, "/S%8.8x%2.2x", getpid( ), ++count);
    ret = sem_open( sem_name, 0);
    if (ret == (sem_t *)SEM_FAILED) {
        sic_perror( "sem_open");
    }
    SIC_SEM_ID(sem) = ret;
#endif /* SIC_USE_SEM_OPEN */
}

static void sic_sem_create( sic_sem_t *sem, unsigned int initial_value)
{
#if defined(_SIC_USE_MUTEX)
    if (pthread_mutex_init(SIC_SEM_ID(sem), NULL)) {
        sic_perror( "pthread_mutex_init");
    }
#ifdef GAG_USE_DEBUG
    sem->value = initial_value;
#endif
    if (!initial_value) {
        if (pthread_mutex_lock(SIC_SEM_ID(sem))) {
            sic_perror( "pthread_mutex_lock");
        }
    }
#elif defined(WIN32)
    SIC_SEM_ID(sem) = CreateSemaphore( NULL, initial_value, 1, NULL);
    if (SIC_SEM_ID(sem) == NULL) {
        sic_perror( "CreateSemaphore");
    }
#else
#ifdef SIC_USE_SEM_OPEN
    static char sem_name[SIC_MAX_SEM_NAME_FOR_PORTABILITY];
    static int count = 0;
    sem_t *ret;

    gag_trace("/S%8.8x%2.2x", getpid( ), ++count);
    sprintf( sem_name, "/S%8.8x%2.2x", getpid( ), ++count);
    ret = sem_open( sem_name, O_CREAT, S_IRUSR | S_IWUSR, initial_value);
    if (ret == (sem_t *)SEM_FAILED) {
        sic_perror( "sem_open");
    }
    SIC_SEM_ID(sem) = ret;
    strncpy( sem->name, sem_name, SIC_MAX_SEM_NAME_FOR_PORTABILITY);
#else /* SIC_USE_SEM_OPEN */
    if (sem_init( SIC_SEM_ID(sem), 1, initial_value) == -1) {
        sic_perror( "sem_init");
    }
#endif /* SIC_USE_SEM_OPEN */
#endif
}

static void sic_sem_close( sic_sem_t *sem)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

#if defined(_SIC_USE_MUTEX)
#elif defined(WIN32)
    if (CloseHandle( SIC_SEM_ID(sem)) == FALSE) {
        sic_perror( "CloseHandle");
    }
#elif defined(SIC_USE_SEM_OPEN)
#ifndef cygwin
    if (sem_close( SIC_SEM_ID(sem)) == -1) {
        sic_perror( "sem_close");
    }
#endif /* cygwin */
#endif
}

static void sic_sem_destroy( sic_sem_t *sem)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    sic_sem_close( sem);
#if defined(_SIC_USE_MUTEX)
    if (0 && pthread_mutex_destroy(SIC_SEM_ID(sem))) {
        sic_perror( "pthread_mutex_destroy");
    }
#elif defined(WIN32)
#elif defined(SIC_USE_SEM_OPEN)
#ifndef cygwin
    if (sem_unlink( sem->name) == -1) {
        sic_perror( "sem_unlink");
    }
#else /* cygwin */
    if (sem_close( SIC_SEM_ID(sem)) == -1) {
        sic_perror( "sem_close");
    }
#endif /* cygwin */
#else
    if (sem_destroy( SIC_SEM_ID(sem)) == -1) {
        sic_perror( "sem_destroy");
    }
#endif
}

static void sic_sem_post( sic_sem_t *sem)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

#if defined(_SIC_USE_MUTEX)
#ifdef GAG_USE_DEBUG
    if (sem->value != 0) {
        fprintf(stderr, "pthread_mutex_unlock without lock\n");
    }
    sem->value = 1;
#endif
    if (pthread_mutex_unlock(SIC_SEM_ID(sem))) {
        sic_perror( "pthread_mutex_lock");
    }
#elif defined(WIN32)
    if (!ReleaseSemaphore( SIC_SEM_ID(sem), 1, NULL)) {
        sic_perror( "ReleaseSemaphore");
    }
#else
    if (sem_post( SIC_SEM_ID(sem)) == -1) {
        sic_perror( "sem_post");
    }
#endif
}

static void sic_sem_wait( sic_sem_t *sem)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

#if defined(_SIC_USE_MUTEX)
    if (pthread_mutex_lock(SIC_SEM_ID(sem))) {
        sic_perror( "pthread_mutex_lock");
    }
#ifdef GAG_USE_DEBUG
    if (sem->value != 1) {
        fprintf(stderr, "pthread_mutex_lock wihout unlock\n");
    }
    sem->value = 0;
#endif
#elif defined(WIN32)
    if (WaitForSingleObject( SIC_SEM_ID(sem), INFINITE) != WAIT_OBJECT_0) {
        sic_perror( "WaitForSingleObject");
    }
#else
    while (sem_wait( SIC_SEM_ID(sem)) == -1) {
        if (errno == EINTR)
            errno = 0;
        else
            break;
    }
#endif
}

static int sic_sem_timed_wait( sic_sem_t *sem, int timeout)
{
    int ret = 0;

#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return 0;
#endif /* SIC_DEBUG_COMM */

    if (timeout == -1) {
        sic_sem_wait( sem);
        return 0;
    } else {
#if defined(darwin)
        ret = sem_trywait( SIC_SEM_ID(sem));
#elif defined(_SIC_USE_MUTEX)
        struct timespec t;
	time_t nb_sec;
	long nb_nsec;

        clock_gettime( CLOCK_REALTIME, &t);

        /* seconds */
        nb_sec = timeout / 1000;
        /* nano seconds */
        nb_nsec = (timeout - nb_sec * 1000) * 1000000L;
        if (nb_nsec + t.tv_nsec > 1000000000L) {
            nb_sec++;
            nb_nsec -= 1000000000L;
        }
        nb_sec = 1;
        nb_nsec = 0;
        t.tv_sec += nb_sec;
        t.tv_nsec += nb_nsec;

        if (!pthread_mutex_timedlock(SIC_SEM_ID(sem), &t)) {
#ifdef GAG_USE_DEBUG
            if (sem->value != 1) {
                fprintf(stderr, "pthread_mutex_timedlock wihout unlock\n");
            }
            sem->value = 0;
#endif
        }
#elif defined(WIN32)
        DWORD wait = WaitForSingleObject( SIC_SEM_ID(sem), timeout);
        if (wait != WAIT_OBJECT_0) {
            if (wait == WAIT_TIMEOUT)
                ret = -1;
            else
                sic_perror( "WaitForSingleObject");
        }
#else
        struct timespec t;
	time_t nb_sec;
	long nb_nsec;

        clock_gettime( CLOCK_REALTIME, &t);

        /* seconds */
        nb_sec = timeout / 1000;
        /* nano seconds */
        nb_nsec = (timeout - nb_sec * 1000) * 1000000L;
        if (nb_nsec + t.tv_nsec > 1000000000L) {
            nb_sec++;
            nb_nsec -= 1000000000L;
        }
        t.tv_sec += nb_sec;
        t.tv_nsec += nb_nsec;

        ret = sem_timedwait( SIC_SEM_ID(sem), &t);
        if (ret == -1) {
            if (errno == EINTR || errno == ETIMEDOUT)
                errno = 0;
            else
                sic_perror( "sem_timedwait");
        }
#endif
    }
    return ret;
}

static int sic_sem_trywait( sic_sem_t *sem)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return 0;
#endif /* SIC_DEBUG_COMM */

#if defined(_SIC_USE_MUTEX)
    if (pthread_mutex_trylock(SIC_SEM_ID(sem))) {
        return -1;
    }
#ifdef GAG_USE_DEBUG
    if (sem->value != 1) {
        fprintf(stderr, "pthread_mutex_timedlock wihout unlock\n");
    }
    sem->value = 0;
    return 0;
#endif
#elif defined(WIN32)
    return sic_sem_timed_wait( sem, 1);
#else
    return sem_trywait( SIC_SEM_ID(sem));
#endif
}

static void sic_on_modified_variable( sic_listener_handler_t *h)
{
    gag_trace( "<trace> sic_on_modified_variable");
    /* no need to call sic_open_widget_board because already done by sic_fire_modified_variable_event */
    (*(sic_modified_variable_listener_t)h->listener)( s_widget_board->modified_widget_index, &s_widget_board->modified_widget, h->data);
}

static void sic_on_redraw_prompt( sic_listener_handler_t *h)
{
    gag_trace( "<trace> sic_on_redraw_prompt");
    (*(sic_redraw_prompt_listener_t)h->listener)( &s_comm_board->command_line);
}

static void sic_on_exit_event( sic_listener_handler_t *h)
{
    gag_trace( "<trace> sic_on_exit_event");
    (*(sic_exit_listener_t)h->listener)( );
}

static void sic_on_event( sic_listener_handler_t *h)
{
    switch (h->event_id) {
    case SIC_MODIFIED_VARIABLE_EVENT:
        sic_on_modified_variable( h);
        break;
    case SIC_REDRAW_PROMPT_EVENT:
        sic_on_redraw_prompt( h);
        break;
    case SIC_EXIT_EVENT:
        sic_on_exit_event( h);
        break;
    }
}

static void sic_record_listener( sic_listener_handler_t *widget_handler)
{
    gag_trace( "<trace> sic_record_listener %d with %d", sic_get_task_id( widget_handler->task), widget_handler->event_id);
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    sic_sem_wait( &s_sem_comm_board->comm_board_access);

    s_comm_board->listeners[s_comm_board->listener_count++] = *widget_handler;

    sic_sem_post( &s_sem_comm_board->comm_board_access);
}

static void sic_trigger_listener( sic_listener_handler_t *h)
{
    gag_trace( "<trace: enter> sic_trigger_listener %d with %d", sic_get_task_id( h->task), h->event_id);
    sic_on_event( h);
    gag_trace( "<trace: leave> sic_trigger_listener %d with %d", sic_get_task_id( h->task), h->event_id);
}

static void sic_trigger_listeners( int event_id)
{
    int i;

    sic_sem_wait( &s_sem_comm_board->comm_board_access);

    s_comm_board->event_id = event_id;

    for (i = 0; i < s_comm_board->listener_count; i++) {
        sic_listener_handler_t *h = &s_comm_board->listeners[i];
        if (h->event_id == event_id)
            sic_trigger_listener( h);
    }

    sic_sem_post( &s_sem_comm_board->comm_board_access);
}

static void sic_purge_listeners( sic_task_t task)
{
    int i;
    int j;

    sic_sem_wait( &s_sem_comm_board->comm_board_access);

    j = 0;
    for (i = 0; i < s_comm_board->listener_count; i++) {
        sic_listener_handler_t *h = &s_comm_board->listeners[i];
        if (sic_get_task_id( h->task) != sic_get_task_id( task)) {
            if (j != i)
                s_comm_board->listeners[j] = *h;
            j++;
        }
    }
    s_comm_board->listener_count = j;

    sic_sem_post( &s_sem_comm_board->comm_board_access);
}

static int sic_is_master( void)
{
    return s_comm_board_id != -1;
}

static void sic_kill_all_child_processes( void)
{
#ifndef WIN32
    gag_trace( "<trace: enter> sic_kill_all_child_processes");
    signal( SIGCLD, SIG_IGN);           /* Avoid zombies */
    signal( SIGTERM, SIG_IGN);          /* don't kill itself */
    kill( 0, SIGTERM);
    wait( 0);
    gag_trace( "<trace: leave> sic_kill_all_child_processes");
#endif /* WIN32 */
}

static void sic_signal_handler( int sig_num)
{
    gag_trace( "<trace: signal> sic_signal_handler on %d", sig_num);
    if (sig_num != SIGTERM) {
#ifdef MINGW
        gcore_c_message(seve_f, "SIC", "signal %d", sig_num);
#else
        gcore_c_message(seve_f, "SIC", strsignal( sig_num));
#endif
        if (!sic_is_master( )) {
            /* set ABORT returned code */
            sic_set_widget_returned_code( -3);
            sic_post_command_text_from( "", SIC_CHILD_EXIT);
        }
    }
    sic_do_exit( 1);
}

/**
 * Post the desired command when SIGALRM is received. Usually
 * used by the Sic timer invoking system's setitimer.
 */
static void sic_post_alarm( int sig_num)
{
    gag_trace( "<trace: signal> sic_post_alarm on %d", sig_num);
    sic_post_command_text( alarm_command);
}

void sic_get_alarm( char *alarm)
{
    strcpy( alarm,alarm_command);

}

void sic_set_alarm( char *alarm)
{
    strcpy( alarm_command,alarm);

}

static void sic_init_handlers( void)
{
    static int done = 0;

    if (done)
        // avoid running twice in multi-threads environment
        return;
    done = 1;

    atexit( sic_on_exit);
#ifndef WIN32
    signal( SIGTTOU, SIG_IGN);

    signal( SIGALRM, sic_post_alarm);

    signal( SIGHUP, sic_signal_handler);
    signal( SIGQUIT, sic_signal_handler);
    signal( SIGILL, sic_signal_handler);
#ifndef GAG_USE_DEBUG
    signal( SIGABRT, sic_signal_handler);
    signal( SIGBUS, sic_signal_handler);
    signal( SIGFPE, sic_signal_handler);
    signal( SIGSEGV, sic_signal_handler);
#endif
    signal( SIGPIPE, sic_signal_handler);
    signal( SIGTERM, sic_signal_handler);
    signal( SIGSYS, sic_signal_handler);
#if defined(SIGIO)
    signal( SIGIO, sic_signal_handler);
#elif defined(SIGPOLL)
    signal( SIGPOLL, sic_signal_handler);
#endif
#endif /* WIN32 */
}

/**
 * Send a command to the communication board.
 *
 * @param[in] command_line is the command.
 * @param[in] timeout given in milliseconds.
 * @param[in] from is the type of the caller.
 * @return 0 on success, -1 if timeout has been reached.
 */
static int sic_raw_post_command_from( const command_line_t *command_line, int timeout, sic_command_from_t from)
{
    int ret;

#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return 0;
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace: enter> sic_post_command");
    if (timeout < 0) {
        sic_sem_wait( &s_sem_comm_board->write_command);
        ret = 0;
    } else {
        ret = sic_sem_trywait( &s_sem_comm_board->write_command);
    }
    if (ret == 0) {
        /* keep current prompt */
        strncpy( (char*)command_line->prompt, s_comm_board->command_line.prompt, PROMPT);
        s_comm_board->command_line = *command_line;
        s_comm_board->from = from;
        gag_trace( "<command: send> \"%s%s\"", command_line->prompt, command_line->line);
        sic_sem_post( &s_sem_comm_board->read_command);
        if (from == SIC_SYNCHRONOUS_MODE) {
            sic_sem_wait( &s_sem_comm_board->synchronous_mode);
        }
    }
    gag_trace( "<trace: leave> sic_post_command");
    return ret;
}

static void sic_build_command_line( const char* text, command_line_t *command_line)
{
    strncpy( command_line->line, text, MAXBUF);
    command_line->nc = (int)strlen( text);
    command_line->prompt[0] = '\0';
    command_line->code = 0;
}

static void sic_remove_previous_comm_files( )
{
#ifdef SIC_USE_SEM_OPEN
    DIR *dp;
    char *dir;
    
    dir = "/dev/shm";
    dp = opendir( dir);
    if (dp != NULL)
    {
        struct dirent *de;
        int last_pid;
        char last_cwd[PATH_MAX];
        uid_t uid;

        uid = getuid( );
        getcwd( last_cwd, PATH_MAX);
        chdir( dir);
        last_pid = 0;
        for (de = readdir( dp); de != NULL; de = readdir( dp))
        {
            struct stat st;
            char *tmp;
            char *tmp2;

            if (stat( de->d_name, &st) || st.st_uid != uid)
                /* file owned by another user */
                continue;

            tmp = strstr( de->d_name, "SIC_");
            if (tmp == NULL)
                /* it is not a sic file */
                continue;

            tmp += 4;
            tmp2 = strchr( tmp, '_');
            if (tmp2 != NULL)
            {
                /* it is a sic file */
                char buf[PATH_MAX];
                int l;
                int pid;
                
                /* extract pid */
                l = tmp2 - tmp;
                strncpy( buf, tmp, l);
                buf[l] = '\0';
                pid = atoi( buf);

                if (pid && (pid == last_pid || kill( pid, 0)))
                {
                    /* process doesn't exist, remove associated file */
                    last_pid = pid;
                    unlink( de->d_name);
                }
            }
        }
        closedir( dp);
        chdir( last_cwd);
    }
#endif /* SIC_USE_SEM_OPEN */
}

/***************************************************************************** 
 *                             Function bodies                               * 
 *****************************************************************************/

/**
 * Initialize a communication board for all sic IPC features.
 * It must be called once by the main process.
 */
void sic_create_comm_board( void)
{
    gag_trace_activate( NULL);

    gag_trace( "<trace: enter> sic_create_comm_board");
    if (s_comm_board_id == -1) {
        sic_remove_previous_comm_files( );
        sprintf( s_comm_shm_name, "/SC%8.8x", sic_get_current_task_id( ));
        s_comm_board_id = sic_shm_create( s_comm_shm_name, sizeof( sic_comm_board_t));
        sic_open_comm_board( s_comm_board_id);
        s_comm_board->master_task = sic_get_current_task( );
        s_comm_board->keyboard_task = _sic_task_null;
        if (gag_trace_get_file_pointer( ) != NULL)
            s_comm_board->trace_fd = fileno( gag_trace_get_file_pointer( ));
        else
            s_comm_board->trace_fd = -1;
        sic_sem_create( &s_sem_comm_board->comm_board_access, 1);
        sic_sem_create( &s_sem_comm_board->draw_prompt, 0);
        sic_sem_create( &s_sem_comm_board->synchronous_mode, 0);
        sic_sem_create( &s_sem_comm_board->write_command, 0);
        sic_sem_create( &s_sem_comm_board->read_command, 0);
        sic_sem_create( &s_sem_comm_board->widget_board_access, 1);
        sic_sem_create( &s_sem_comm_board->widget_created, 0);
        sic_sem_create( &s_sem_comm_board->listener_acquit, 0);
        sic_sem_create( &s_sem_comm_board->event_stack_access, 1);
        s_comm_board->widget_board_id = -1;
        sic_build_command_line( "", &s_comm_board->command_line);
        s_comm_board->pushed_command_text[0] = '\0';
        s_comm_board->from = -1;
        s_comm_board->listener_count = 0;
        s_comm_board->event_id = -1;
    }
    gag_trace( "<trace: leave> sic_create_comm_board");
}

/**
 * Destroy the communication board from main process.
 */
void sic_destroy_comm_board( void)
{
    gag_trace( "<trace: enter> sic_destroy_comm_board");

    SIC_ASSERT( "sic_destroy_comm_board called from child process", sic_is_master( ));

    sic_kill_all_child_processes( );
    if (s_sem_comm_board != NULL) {
        sic_sem_destroy( &s_sem_comm_board->comm_board_access);
        sic_sem_destroy( &s_sem_comm_board->draw_prompt);
        sic_sem_destroy( &s_sem_comm_board->synchronous_mode);
        sic_sem_destroy( &s_sem_comm_board->write_command);
        sic_sem_destroy( &s_sem_comm_board->read_command);
        sic_sem_destroy( &s_sem_comm_board->widget_board_access);
        sic_sem_destroy( &s_sem_comm_board->widget_created);
        sic_sem_destroy( &s_sem_comm_board->listener_acquit);
        sic_sem_destroy( &s_sem_comm_board->event_stack_access);
        s_sem_comm_board = NULL;
    }

    sic_close_comm_board( );

    if (s_comm_board_id >= 0) {
        sic_shm_destroy( s_comm_shm_name, s_comm_board_id);
        /* do it last for sic_is_master to work */
        s_comm_board_id = -1;
    }

    if (gag_trace_get_file_pointer( ) != NULL)
        gcore_c_message(seve_i, "SIC", "Cleaning done on exit");

    gag_trace( "<trace: leave> sic_destroy_comm_board");
}

static void sic_on_thread_cancel( void *data)
{
    gag_trace( "<trace: thread> ending");
}

typedef struct {
    int (*start_entry)( void *);
    void *data;
} sic_start_thread_args_t;

#ifdef SIC_DEBUG_COMM
static void *sic_start_thread_entry( void *data)
{
    sic_start_thread_args_t tmp;

    gag_trace( "<trace: thread> starting");
#ifdef _SIC_USE_PTHREAD
    pthread_cleanup_push( sic_on_thread_cancel, NULL);
#endif
    tmp = *(sic_start_thread_args_t *)data;
    free( data);
    (*tmp.start_entry)( tmp.data);
#ifdef _SIC_USE_PTHREAD
    pthread_cleanup_pop( 1);
#endif

    return NULL;
}
#endif /* SIC_DEBUG_COMM */

/*
 * Create a thread
 */
sic_task_t sic_create_thread( int (*start_entry)( void *), void *data)
{
#ifdef _SIC_USE_PTHREAD
    sic_task_t tid;
#ifdef SIC_DEBUG_COMM
    sic_start_thread_args_t *tmp;
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace: thread> creation");
#ifdef SIC_DEBUG_COMM
    tmp = malloc( sizeof( sic_start_thread_args_t));
    tmp->start_entry = start_entry;
    tmp->data = data;
    if (pthread_create( &tid, NULL, sic_start_thread_entry, tmp) < 0)
#else /* SIC_DEBUG_COMM */
    if (pthread_create( &tid, NULL, (void *(*)(void *))start_entry, data) < 0)
#endif /* SIC_DEBUG_COMM */
    {
        sic_perror( "pthread_create");
    }
    gag_trace( "<trace: thread> thread %d created", tid);

    return tid;
#else /* _SIC_USE_PTHREAD */
    DWORD dwThreadId; 
    HANDLE hthrd;

    hthrd = CreateThread( 
        NULL,                        // no security attributes 
        0,                           // use default stack size  
        (LPTHREAD_START_ROUTINE) start_entry, // thread function 
        data,                        // argument to thread function 
        0,                           // use default creation flags 
        &dwThreadId);                // returns the thread identifier 

    // Check the return value for success. 

    if (hthrd == NULL) 
        sic_perror( "CreateThread");

    return hthrd;
#endif /* _SIC_USE_PTHREAD */
}

/**
 * Get a static buffer for building argv.
 */
char **sic_get_static_argv( void)
{
    static char *s_argv[MAX_ARGC];
    static char s_argv_buf[MAX_ARGC][MAX_ARGV];
    int i;

    /* make argv pointing on allocated buffers */
    for (i = 0; i < MAX_ARGC; i++)
        s_argv[i] = s_argv_buf[i];

    return s_argv;
}

/**
 * Call execvp. If SIC_DEBUG_COMM flag is set and environnement variable
 * SIC_DEBUG_GDB is set, exec gdb with supplied arguments inside a xterm.
 *
 * @param[in] argv is an array of argument strings.
 * @return 0 on success, -1 otherwise.
 */
int sic_execvp( char *argv[])
{
#ifndef WIN32
#ifdef SIC_DEBUG_COMM
    char gdb_cmd_file[MAX_ARGV];
    FILE *fp;
    int i;
    static char *exec_argv[MAX_ARGC];
    static char exec_argv_buf[MAX_ARGC][MAX_ARGV];
#endif /* SIC_DEBUG_COMM */
    char *app_name = argv[0];
#ifdef SIC_DEBUG_COMM
    char mess[GAG_MAX_MESSAGE_LENGTH];
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace: exec> %s", app_name);

#ifdef SIC_DEBUG_COMM
    if (getenv( "SIC_DEBUG_GDB") == NULL)
#endif /* SIC_DEBUG_COMM */

        return execvp( argv[0], argv);

#ifdef SIC_DEBUG_COMM
    /* don't use sic_get_static_argv( ) because it can be use by the caller */
    /* make argv pointing on allocated buffers */
    for (i = 0; i < MAX_ARGC; i++)
        exec_argv[i] = exec_argv_buf[i];

    /* create gdb command file */
    sprintf( gdb_cmd_file, ".gdb_%s", app_name);
    fp = fopen( gdb_cmd_file, "w");
    fprintf( fp, "set args");
    for (i = 1; argv[i] != NULL; i++) {
        fprintf( fp, " \"%s\"", argv[i]);
    }
    fprintf( fp, "\n");
    fclose( fp);

    /* create command line with call to gdb */
    i = 0;
    strcpy( exec_argv[i++], "xterm");
    strcpy( exec_argv[i++], "-T");
    strcpy( exec_argv[i++], app_name);
    strcpy( exec_argv[i++], "-e");
    strcpy( exec_argv[i++], "gdb");
    strcpy( exec_argv[i++], "-x");
    strcpy( exec_argv[i++], gdb_cmd_file);
    strcpy( exec_argv[i++], app_name);
    exec_argv[i++] = NULL;
    if (i >= MAX_ARGC) {
        gcore_c_message(seve_e, "SIC", "Too many arguments, exiting");
        sic_do_exit( 1);
    }

    /* show original command line */
    strcpy(mess, "Launching:");
    for (i = 0; argv[i] != NULL; i++)
        sprintf(mess+strlen(mess), " %s", argv[i]);
    gcore_c_message(seve_i, "SIC", mess);

    return execvp( exec_argv[0], exec_argv);
#endif /* SIC_DEBUG_COMM */
#else /* WIN32 */
    return 0;
#endif /* WIN32 */
}

/**
 * Launch a procedure in a separate task.
 *
 * @param[in] start_entry is the procedure to launch.
 * @param[in] data is a pointer passed as argument to the procedure.
 */
sic_task_t sic_launch( int (*start_entry)( void *), void *data)
{
    return sic_create_thread( start_entry, data);
}

/**
 * Terminate a thread created with sic_create_thread.
 *
 * @param[in] task is a task returned by a call to sic_create_thread.
 * @return 0 on success, -1 otherwise.
*/
int sic_terminate_thread( sic_task_t task)
{
#ifdef _SIC_USE_PTHREAD
    if (pthread_cancel( (sic_task_t)task) != 0) {
        sic_perror( "pthread_cancel");
        return -1;
    }
    {
        void *ret;
        pthread_join( (sic_task_t)task, &ret);
    }
#else /* _SIC_USE_PTHREAD */
    if (!TerminateThread( task, 0)) {
        sic_perror( "TerminateThread");
        return -1;
    }
#endif /* _SIC_USE_PTHREAD */
    return 0;
}

/**
 * Terminate a task created with sic_launch.
 *
 * @param[in] task is a task returned by a call to sic_launch.
 * @return 0 on success, -1 otherwise.
*/
int sic_terminate( sic_task_t task)
{
    gag_trace( "<trace: sic_terminate>");
    return sic_terminate_thread( task);
}

/**
 * Informs SIC that program will exit.
 */
void sic_on_exit( void)
{
    static int recur_level = 0;

    gag_trace( "<trace: sic_on_exit>");
    if (!recur_level) {
        recur_level++;
        if (sic_is_master( )) {
            sic_fire_exit_event( );
            sic_destroy_comm_board( );
        } else {
            sic_close_comm_board( );
        }
    }

    gag_trace( "<trace: exit>");
    gag_trace_close( );
}

/**
 * Exit in a clean way.
 *
 * @param[in] status is the value returned to the calling process.
 */
void sic_do_exit( int status)
{
    gag_trace( "<trace: sic_do_exit>");

    exit( status);
}

/**
 * Returns the communication board id.
 */
int sic_get_comm_id( void)
{
    return s_comm_board_id;
}

/**
 * Open the communication board.
 * It must be called once by each child process.
 *
 * @param[in] comm_board_id is the identifier of the communication board.
 * @return 0 on success, -1 otherwise.
 */
int sic_open_comm_board( int comm_board_id)
{
    if (comm_board_id == -1) {
        SIC_ASSERT( "sic_open_comm_board with bad id", 0);
        return -1;
    }

    sic_init_handlers( );

    /* negative comm_board_id can be used for debugging purpose */
    if (s_comm_board == NULL
#ifdef SIC_DEBUG_COMM
        && comm_board_id >= 0
#endif /* SIC_DEBUG_COMM */
        ) {
        static sic_comm_board_t single_process_comm_board;
        s_comm_board = &single_process_comm_board;
        s_sem_comm_board = s_comm_board;

        if (!sic_is_master( )) {
#ifdef SIC_USE_SEM_OPEN
            static sic_comm_board_t process_local_sem_comm_board;
            s_sem_comm_board = &process_local_sem_comm_board;
#endif /* SIC_USE_SEM_OPEN */

            if (s_comm_board->trace_fd >= 0)
                gag_trace_set_file_pointer( fdopen( s_comm_board->trace_fd, "w"));

            sic_sem_open( &s_sem_comm_board->comm_board_access);
            sic_sem_open( &s_sem_comm_board->draw_prompt);
            sic_sem_open( &s_sem_comm_board->synchronous_mode);
            sic_sem_open( &s_sem_comm_board->write_command);
            sic_sem_open( &s_sem_comm_board->read_command);
            sic_sem_open( &s_sem_comm_board->widget_board_access);
            sic_sem_open( &s_sem_comm_board->widget_created);
            sic_sem_open( &s_sem_comm_board->listener_acquit);
            sic_sem_open( &s_sem_comm_board->event_stack_access);
        }
    }
    gag_trace( "<trace> sic_open_comm_board (master=%d)", sic_is_master( ));
    return 0;
}

/**
 * Close the communication board from a child process.
 */
void sic_close_comm_board( void)
{
    gag_trace( "<trace: enter> sic_close_comm_board");
    if (s_comm_board != NULL) {
        sic_destroy_widget_board( );
        if (s_sem_comm_board != NULL) {
            sic_purge_listeners( sic_get_current_task( ));
            sic_sem_close( &s_sem_comm_board->comm_board_access);
            sic_sem_close( &s_sem_comm_board->draw_prompt);
            sic_sem_close( &s_sem_comm_board->synchronous_mode);
            sic_sem_close( &s_sem_comm_board->write_command);
            sic_sem_close( &s_sem_comm_board->read_command);
            sic_sem_close( &s_sem_comm_board->widget_board_access);
            sic_sem_close( &s_sem_comm_board->widget_created);
            sic_sem_close( &s_sem_comm_board->listener_acquit);
            sic_sem_close( &s_sem_comm_board->event_stack_access);
            s_sem_comm_board = NULL;
        }
        sic_shm_detach( s_comm_board, sizeof( sic_comm_board_t));
        s_comm_board = NULL;
    }
    gag_trace( "<trace: leave> sic_close_comm_board");
}

/**
 * Get the unique identifier of a task.
 *
 * @param[in] task is a task returned by a call to sic_launch.
 * @return the identifier of @e task.
 */
task_id_t sic_get_task_id( sic_task_t task)
{
#if defined(_SIC_USE_PTHREAD)
    return (task_id_t)task;
#else
    return (task_id_t)GetThreadId(task);
#endif
}

/**
 * Get the master task.
 *
 * @return the task.
 */
sic_task_t sic_get_master_task( void)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return _sic_task_null;
#endif /* SIC_DEBUG_COMM */

    return s_comm_board->master_task;
}

/**
 * Get the master task id.
 *
 * @return the id.
 */
task_id_t sic_get_master_task_id( void)
{
    return sic_get_task_id( sic_get_master_task( ));
}

/**
 * Get the current task.
 *
 * @return the task.
 */
sic_task_t sic_get_current_task( void)
{
#if defined(_SIC_USE_PTHREAD)
    return pthread_self( );
#else
    return GetCurrentThread( );
#endif
}

/**
 * Get the current task id.
 *
 * @return the id.
 */
task_id_t sic_get_current_task_id( void)
{
    return sic_get_task_id( sic_get_current_task( ));
}

/**
 * Stop the master task (sending a SIGINT).
 */
void sic_send_ctrlc( void)
{
    sic_do_exit( 0);
}

/**
 * Terminate the master task (sending a SIGTERM).
 */
void sic_kill_master_task( void)
{
    sic_do_exit( 0);
}

static int s_disable_prompt = 0;

/**
 * Enable sic_post_prompt and sic_fire_redraw_prompt_event.
 */
void sic_enable_prompt( )
{
    s_disable_prompt = 0;
}

/**
 * Disable sic_post_prompt and sic_fire_redraw_prompt_event.
 */
void sic_disable_prompt( )
{
    s_disable_prompt = 1;
}

/**
 * Enable prompt drawing (called by main process).
 *
 * @param[in] command_line with prompt and current line.
 */
void sic_post_prompt( const command_line_t *command_line)
{
    if (s_comm_board->from == SIC_SYNCHRONOUS_MODE) {
        sic_sem_post( &s_sem_comm_board->synchronous_mode);
        return;
    }
    if (s_disable_prompt)
        return;
    gag_trace( "<trace> sic_post_prompt");
    s_comm_board->command_line = *command_line;
    sic_sem_post( &s_sem_comm_board->draw_prompt);
}

/**
 * Enable prompt drawing (called by main process).
 *
 * @param[in] prompt to draw.
 */
void sic_post_prompt_text( const char *prompt)
{
    command_line_t command_line;

    strcpy( command_line.prompt, prompt);
    command_line.line[0] = '\0';
    command_line.nc = 0;
    command_line.code = 0;
    sic_post_prompt( &command_line);
}

/**
 * Wait to draw the prompt (called by keyboard process).
 *
 * @param[out] command_line contains the prompt and optionally a command line.
 * @param[in] timeout given in milliseconds.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_wait_prompt( command_line_t *command_line, int timeout)
{
    int ret;

#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return 0;
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace: enter> sic_wait_prompt");
    ret = sic_sem_timed_wait( &s_sem_comm_board->draw_prompt, timeout);
    if (ret == 0) {
        *command_line = s_comm_board->command_line;
    }
    gag_trace( "<trace: leave> sic_wait_prompt");
    return ret;
}

/**
 * Get the current prompt in the communication board.
 *
 * @param[out] prompt is returned.
 */
void sic_get_current_prompt( char prompt[])
{
    strncpy( prompt, s_comm_board->command_line.prompt, PROMPT);
}

/**
 * Push a command to the communication board.
 *
 * @param[in] text is the command.
 * @return 0 on success, -1 if a command has already been pushed.
 */
int sic_push_command_text( const char *text)
{
    gag_trace( "<trace: enter> sic_push_command_text");

    if (s_comm_board->pushed_command_text[0]) {
        gag_trace( "<trace: leave> sic_push_command_text return error");
        return -1;
    }

    strncpy( s_comm_board->pushed_command_text, text, MAXBUF);
    gag_trace( "<command: push> \"%s\"", text);
    sic_sem_post( &s_sem_comm_board->read_command);

    gag_trace( "<trace: leave> sic_push_command_text");
    return 0;
}

/**
 * Send a command to the communication board.
 *
 * @param[in] command_line is the command.
 * @param[in] from is the type of the caller.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command_from( const command_line_t *command_line, sic_command_from_t from)
{
    return sic_raw_post_command_from( command_line, 2000, from);
}

/**
 * Send a command to the communication board.
 *
 * @param[in] command_line is the command.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command( const command_line_t *command_line)
{
    return sic_post_command_from( command_line, SIC_DEFAULT);
}

/**
 * Post a command after a call to sic_wait_prompt.
 *
 * @param[in] command_line is the command.
 */
void sic_post_command_from_prompt( const command_line_t *command_line)
{
    sic_raw_post_command_from( command_line, -1, SIC_KEYBOARD);
}

/**
 * Send a command to the communication board.
 *
 * @param[in] text is the command.
 * @param[in] from is the type of the caller.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command_text_from( const char *text, sic_command_from_t from)
{
    command_line_t command_line;
    
    sic_build_command_line( text, &command_line);

    return sic_post_command_from( &command_line, from);
}

/**
 * Send a command to the communication board.
 *
 * @param[in] text is the command.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command_text( const char *text)
{
    return sic_post_command_text_from( text, SIC_DEFAULT);
}

/**
 * Send a command to the communication board with variable argument list.
 *
 * @param[in] args is a format like printf first argument.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command_va( const char *args, va_list l)
{
    char command[MAXBUF];
    vsprintf( command, args, l);

    return sic_post_command_text( command);
}

/**
 * Send a command to the communication board with specified format.
 *
 * @param[in] args is a format like printf first argument.
 * @return 0 on success, -1 if timeout has been reached.
 */
int sic_post_command_args( const char *args, ...)
{
    int ret;
    va_list l;

    va_start( l, args);
    ret = sic_post_command_va( args, l);
    va_end( l);

    return ret;
}

/**
 * Wait for a command in the communication board.
 *
 * @param[out] command_line contains the command.
 * @param[out] from informs about the sender of the command.
 */
void sic_wait_command( command_line_t *command_line, sic_command_from_t *from)
{
    gag_trace( "<trace: enter> sic_wait_command");
    if (!s_comm_board->pushed_command_text[0])
        sic_sem_post( &s_sem_comm_board->write_command);
    sic_sem_wait( &s_sem_comm_board->read_command);
    if (!s_comm_board->pushed_command_text[0]) {
        *command_line = s_comm_board->command_line;
        *from = s_comm_board->from;
    } else {
        sic_build_command_line( s_comm_board->pushed_command_text, command_line);
        *from = SIC_DEFAULT;
        s_comm_board->pushed_command_text[0] = '\0';
    }
    gag_trace( "<command: receive> \"%s%s\"", command_line->prompt, command_line->line);

    gag_trace( "<trace: leave> sic_wait_command");
}

/**
 * Create the widget definition board if necessary and start a widget definition
 * task.
 */
void sic_create_widget_board( void)
{
    gag_trace( "<trace> sic_create_widget_board");
    if (s_comm_board->widget_board_id < 0) {
        sprintf( s_widget_shm_name, "/SW%8.8x", sic_get_current_task_id( ));
        s_comm_board->widget_board_id = sic_shm_create( s_widget_shm_name, sizeof( sic_widget_board_t));
    }
    sic_open_widget_board( );
    s_widget_board->widget_count = 0;
}

/**
 * Set global widget infos.
 *
 * @param[in] window_title.
 * @param[in] help_filename.
 * @param[in] returned_command.
 */
void sic_set_widget_global_infos( const char window_title[], const char help_filename[], const char returned_command[])
{
    strcpy( s_widget_board->window_title, window_title);
    strcpy( s_widget_board->help_filename, help_filename);
    strcpy( s_widget_board->returned_command, returned_command);
}

/**
 * Set widget returned command.
 *
 * @param[in] returned_command.
 */
void sic_set_widget_returned_command( const char returned_command[])
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    strcpy( s_widget_board->returned_command, returned_command);
}

/**
 * Get widget returned command.
 *
 * @param[out] returned_command.
 */
void sic_get_widget_returned_command( char returned_command[])
{
    strcpy( returned_command, s_widget_board->returned_command);
}

/**
 * Set widget returned code.
 *
 * @param[in] code is the id of the widget ending the dialog.
 */
void sic_set_widget_returned_code( int code)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    s_widget_board->returned_code = code;
}

/**
 * Get widget returned code.
 *
 * @return the id of the widget ending the dialog.
 */
int sic_get_widget_returned_code( void)
{
    return s_widget_board->returned_code;
}

/**
 * Add a listener for the modified_variable event.
 *
 * @param[in] l is the handler of the listener.
 */
void sic_add_modified_variable_listener( sic_modified_variable_listener_t l, void *data)
{
    sic_listener_handler_t h = {
        _sic_task_null,
        SIC_MODIFIED_VARIABLE_EVENT,
        NULL,
        NULL,
#ifndef WIN32
        SIGUSR2
#else /* WIN32 */
        0
#endif /* WIN32 */
    };
    h.task = sic_get_current_task( );
    h.listener = l;
    h.data = data;
    sic_record_listener( &h);
}

/**
 * Trigger a modified_variable event.
 *
 * @param[in] widget contains informations on the modified widget.
 */
void sic_fire_modified_variable_event( int index, const sic_widget_def_t *widget)
{
    gag_trace( "<trace: enter> sic_fire_modified_variable_event");
    sic_open_widget_board( );
    s_widget_board->modified_widget_index = index;
    s_widget_board->modified_widget = *widget;
    sic_trigger_listeners( SIC_MODIFIED_VARIABLE_EVENT);
    sic_close_widget_board( );
    gag_trace( "<trace: leave> sic_fire_modified_variable_event");
}

/**
 * Add a listener for the redraw_prompt event.
 *
 * @param[in] l is the handler of the listener.
 */
void sic_add_redraw_prompt_listener( sic_redraw_prompt_listener_t l)
{
    sic_listener_handler_t h = {
        _sic_task_null,
        SIC_REDRAW_PROMPT_EVENT,
        NULL,
        NULL,
#ifndef WIN32
        SIGUSR2
#else /* WIN32 */
        0
#endif /* WIN32 */
    };
    h.task = sic_get_current_task( );
    h.listener = l;
    sic_record_listener( &h);
}

/**
 * Trigger a redraw_prompt event.
 *
 * @param[in] command_line the prompt to draw.
 */
void sic_fire_redraw_prompt_event( const command_line_t *command_line)
{
    if (s_disable_prompt)
        return;
    gag_trace( "<trace: enter> sic_fire_redraw_prompt_event");
    s_comm_board->command_line = *command_line;
    sic_trigger_listeners( SIC_REDRAW_PROMPT_EVENT);
    gag_trace( "<trace: leave> sic_fire_redraw_prompt_event");
}

/**
 * Add a listener for the exit event.
 *
 * @param[in] l is the handler of the listener.
 */
void sic_add_exit_listener( sic_exit_listener_t l)
{
    sic_listener_handler_t h = {
        _sic_task_null,
        SIC_EXIT_EVENT,
        NULL,
        NULL,
#ifndef WIN32
        SIGUSR2
#else /* WIN32 */
        0
#endif /* WIN32 */
    };
    h.task = sic_get_current_task( );
    h.listener = l;
    sic_record_listener( &h);
}

/**
 * Trigger a exit event.
 *
 * @param[in] command_line the prompt to draw.
 */
void sic_fire_exit_event( )
{
    gag_trace( "<trace: enter> sic_fire_exit_event");
    sic_trigger_listeners( SIC_EXIT_EVENT);
    gag_trace( "<trace: leave> sic_fire_exit_event");
}

/**
 * Suspend the prompt to ensure output goes to a new line of the terminal.
 *
 * @param[in] from informs about the sender of the command.
 */
void sic_suspend_prompt( sic_command_from_t from)
{
    if (from != SIC_KEYBOARD && from != SIC_SILENT_MODE && from !=
     SIC_SYNCHRONOUS_MODE)
#ifdef SIC_ECHO_COMMAND_FROM_GUI
        fprintf( stderr, "%s\n", s_comm_board->command_line.line);
#else
        fprintf( stderr, "\n");
#endif
}

/**
 * Add a widget definition
 *
 * @param[in] def is the widget definition.
 * @return 0 on success, -1 if no space available for new widget.
 */
int sic_add_widget_def( sic_widget_def_t *def)
{
    if (s_widget_board->widget_count >= SIC_MAX_WIDGET_ON_BOARD)
        return -1;

    s_widget_board->widgets[s_widget_board->widget_count++] = *def;

    return 0;
}

/**
 * Set a widget definition.
 *
 * @param[in] index of the widget.
 * @param[in] widget to defined.
 */
void sic_set_widget_def( int index, const sic_widget_def_t *widget)
{
    s_widget_board->widgets[index] = *widget;
}

/**
 * Destroy the widget definition board.
 */
void sic_destroy_widget_board( void)
{
    gag_trace( "<trace> sic_destroy_widget_board");
    if (s_widget_board != NULL) {
        sic_shm_detach( s_widget_board, sizeof( sic_widget_board_t));
        s_widget_board = NULL;
    }
    /* destroy share memory from master */
    if (sic_is_master( ) && s_comm_board->widget_board_id >= 0) {
        sic_shm_destroy( s_widget_shm_name, s_comm_board->widget_board_id);
        s_comm_board->widget_board_id = -1;
    }
}

/**
 * Open the widget definition board.
 * It must be called once by each child process.
 *
 * @return the count of widgets.
 */
int sic_open_widget_board( void)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return 0;
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace: enter> sic_open_widget_board");
    sic_sem_wait( &s_sem_comm_board->widget_board_access);
    if (s_widget_board == NULL) {
        static sic_widget_board_t single_process_widget_board;
        s_widget_board = &single_process_widget_board;
    }
    gag_trace( "<trace: leave> sic_open_widget_board");
    return s_widget_board->widget_count;
}

/**
 * Get global widget infos
 *
 * @param[out] window_title must be of size TITLELENGTH
 * @param[out] help_filename must be of size HLPFILELNGTH
 * @param[out] returned_command must be of size COMMANDLENGTH
 */
void sic_get_widget_global_infos( char window_title[], char help_filename[], char returned_command[])
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    strcpy( window_title, s_widget_board->window_title);
    strcpy( help_filename, s_widget_board->help_filename);
    strcpy( returned_command, s_widget_board->returned_command);
}

/**
 * Get a widget definition.
 *
 * @param[in] index of the widget.
 * @param[out] widget to be filled with the definition.
 */
void sic_get_widget_def( int index, sic_widget_def_t *widget)
{
    *widget = s_widget_board->widgets[index];
}

/**
 * Close the widget definition board from a child process.
 */
void sic_close_widget_board( void)
{
#ifdef SIC_DEBUG_COMM
    if (s_comm_board == NULL)
        return;
#endif /* SIC_DEBUG_COMM */

    gag_trace( "<trace> sic_close_widget_board");
    sic_sem_post( &s_sem_comm_board->widget_board_access);
}

/**
 * Post a widget creation event.
 */
void sic_post_widget_created( void)
{
    gag_trace( "<trace> sic_post_widget_created");
    sic_sem_post( &s_sem_comm_board->widget_created);
}

/**
 * Wait for widget creation.
 */
void sic_wait_widget_created( void)
{
    gag_trace( "<trace: enter> sic_wait_widget_created");
    sic_sem_wait( &s_sem_comm_board->widget_created);
    gag_trace( "<trace: leave> sic_wait_widget_created");
}

/**
 * Wait for event stack access.
 */
void sic_open_event_stack( void)
{
    gag_trace( "<trace: enter> sic_open_event_stack");
    sic_sem_wait( &s_sem_comm_board->event_stack_access);
    gag_trace( "<trace: leave> sic_open_event_stack");
}

/**
 * Release event stack access.
 */
void sic_close_event_stack( void)
{
    gag_trace( "<trace> sic_close_event_stack");
    sic_sem_post( &s_sem_comm_board->event_stack_access);
}

#define _SEMAPHORES_MAX_COUNT 10
static sic_sem_t _semaphores_by_index[_SEMAPHORES_MAX_COUNT];
static int _semaphores_count = 0;

/**
 * Create a new semaphore.
 */
int sic_sem_create_index( unsigned int initial_value)
{
    if (_semaphores_count == _SEMAPHORES_MAX_COUNT) {
        fprintf(stderr, "sic_sem_create_index: max semaphores count reached\n");
        return -1;
    }
    sic_sem_create( &_semaphores_by_index[_semaphores_count], initial_value);
    return _semaphores_count++;
}

/**
 * Unlock a semaphore.
 */
void sic_sem_post_index( int index)
{
    sic_sem_post( &_semaphores_by_index[index]);
}

/**
 * Lock a semaphore.
 */
void sic_sem_wait_index( int index)
{
    sic_sem_wait( &_semaphores_by_index[index]);
}

