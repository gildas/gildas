
#ifndef _GCOMMAND_LINE_H_
#define _GCOMMAND_LINE_H_

#define MAXBUF 2048
#define PROMPT 64

typedef struct {
	char prompt[PROMPT];
	char line[MAXBUF];
	int code;
	size_t nc;
} command_line_t;

#endif /* _GCOMMAND_LINE_H_ */

