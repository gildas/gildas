
#ifndef _GCORE_GWIDGET_H_
#define _GCORE_GWIDGET_H_

/* definition of size_t */
#include <sys/types.h>

/*
don't define on WIN32
#define SIZE 1024*1024
*/
#define CHAINLENGTH 512  /* Must be synchronized with x_cmdlen in Sic */
#define SICVLN 64
#define LABELLENGTH 80
#define COMMANDLENGTH 256
#define FILTERLENGTH 80
#define TITLELENGTH 256 /* Was 32 on X-Window before */
#define TEXTLENGTH 256
#define HLPFILELNGTH 256
#define HLPTEXTLNGTH 32768
#define X_CHOLEN 32
#define M_CHOICES 128
#define NSTRUCT 256   /* 256=M_COMMANDS */
#define SEPARATOR 1
#define LOGIC 2
#define BROWSER 3
#define CHOICE 4
#define SLIDER 5
#define CHAIN 6
#define SHOW 7
#define BUTTON 8

typedef struct {
    int type;                    /* Pointer on input types */
    int window_id;               /* Pointer on window id */
    char label[LABELLENGTH];     /* Pointer on titles */
    char variable[SICVLN];       /* Name of variable */ 
} generic_struct;

typedef struct {
    int type;                    /* Pointer on input types */
    int window_id;               /* Pointer on window id */
    char label[LABELLENGTH];     /* Pointer on titles (ignored) */
    char variable[SICVLN];       /* Name of variable (ignored) */ 
    /* end generic*/
    char text[TEXTLENGTH];       /* Pointer on show item text */
} show_struct;

typedef struct {
    int type;                    /* Pointer on input types */
    int window_id;               /* Pointer on window id */
    char label[LABELLENGTH];     /* Pointer on label */
    char variable[SICVLN];       /* Name of variable */ 
    /* end generic*/
    char userchain[CHAINLENGTH];
    char *chain;                 /* Pointer on character chains */
    size_t length;               /* Pointer on character lenghts */
    int editable;                /* Static or dynamic chain */
} chain_struct;

typedef struct {
    int type;                    /* Pointer on input type */
    int window_id;               /* Pointer on window id */
    char label[LABELLENGTH];     /* Pointer on label (text on the left) */
    char variable[SICVLN];       /* Name of variable : ignored */ 
    /* end generic*/
    char command[COMMANDLENGTH]; /* Command returned when pressing button (or GO) */
    char title[TITLELENGTH];     /*  Title written on the Button */
    int showlength;              /* if not 0, indicates the presence of a submenu */
    char moretxt[TITLELENGTH];   /* if present, add a submenu button (moreoption).
                                    This is the texte written on the button */
    char helptxt[HLPFILELNGTH];  /* Name of submenu help file (if any) */
    int group;                   /* 1 for begin, -1 for end, 0 otherwise */
    int popup_window_id;         /* window id of popup associated with button */
} button_struct;

typedef struct {
    int type;             /* Pointer on input type */
    int window_id;        /* Pointer on window id */
    char label[LABELLENGTH];       /* Pointer on logic label */
    char variable[SICVLN];    /* Name of variable */ 
    /* end generic*/
    int  *logic;          /* Pointer on returned logic */
    int userlogic;        /* Working logic in shared memory */
} logic_struct;

typedef struct {
    int type;             /* Pointer on input type */
    int window_id;        /* Pointer on window id */
    char label[LABELLENGTH];       /* Pointer on file label */
    char variable[SICVLN];    /* Name of variable */ 
    /* end generic*/
    char *chain;          /* Pointer on returned file chain */
    char userchain[CHAINLENGTH];  /* Working file chain in shared memory */
    size_t length;           /* Pointer on file chain length */
    char filter[FILTERLENGTH];      /* Pointer on file filter */
} file_struct ;

typedef struct {
    int type;             /* Pointer on input type */
    int window_id;        /* Pointer on window id */
    char label[LABELLENGTH];       /* Pointer on choice label */
    char variable[SICVLN];    /* Name of variable */ 
    /* end generic*/
    char *choice;         /* Pointer on return choice */
    char userchoice[CHAINLENGTH]; /* Working choice in shared memory */
    size_t length;           /* Pointer on choice lenght */
    char choices[M_CHOICES][X_CHOLEN]; /* Pointer on choices */
    int nchoices;         /* Pointer on number of choices */
    int mode;             /* editable, non editable or index mode */
} choice_struct;

typedef struct {
    int type;          /* Pointer on input types */
    int window_id;     /* Pointer on window id */
    char label[LABELLENGTH];    /* slider label */
    char variable[SICVLN]; /* Name of variable */ 
    /* end generic*/
    double *value;      /* Pointer on slider pointed values */
    double uservalue;
    double width;       /* Pointer on slider widths */
    double min;         /* Pointer on slider minimums */
} slider_struct;

typedef union {
    generic_struct generic;
    chain_struct chain;
    show_struct show;
    button_struct button;
    logic_struct logic;
    file_struct file;
    choice_struct choice;
    slider_struct slider;
} global_struct;

#endif /* _GCORE_GWIDGET_H_ */

