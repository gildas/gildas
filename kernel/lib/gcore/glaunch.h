
#ifndef _SIC_LAUNCH_H_
#define _SIC_LAUNCH_H_

/**
 * @file
 * @see sic_launch.c
 */

/***************************************************************************** 
 *                              Dependencies                                 * 
 *****************************************************************************/

#include "gsys/sic_util.h"
#include "gcomm.h"

/***************************************************************************** 
 *                           Function prototypes                             * 
 *****************************************************************************/

void set_gtv_window_handler( int (*handler)( void *data));
sic_task_t launch_gtv_x11_window( );

void set_dialog_handler( int (*handler)( void), char *(*version_handler)(void));
char *call_dialog_version_handler();
sic_task_t launch_dialog( );

typedef struct {
    unsigned long win_graph;
    unsigned int win_width;
    unsigned int win_height;
    unsigned long win_main;
    unsigned long cmap;
    char lut_path[SIC_MAX_TRANSLATION_LENGTH];
    char tmp_path[SIC_MAX_TRANSLATION_LENGTH];
    int *pix_colors;
    int ncells;
    int *named_colors;
    int *pencil_colors;
    int ncells_pen;
} gtv_toolbar_args_t;

void set_gtv_toolbar_handler( int (*handler)( gtv_toolbar_args_t *args));
sic_task_t launch_gtv_toolbar( gtv_toolbar_args_t *args);
void set_gtv_main_loop_handler( int (*handler)( void *data));
sic_task_t launch_gtv_main_loop( void *data);

void set_new_graph_handler( void (*handler)( int, size_t *, char *, int *, int *, int *, int *, int [], int, int));
int launch_new_graph(
    int backg,
    size_t *graph_env,
    char *dirname,
    int *x1, int *x2, int *y1, int *y2,
    int dir_cour[],
    int ldir, int reuse);

void set_menu_handlers( void (*start_handler)( const char *), void (*kill_handler)());
void launch_menu( const char *temporary_file);
void kill_menu();

void set_keyboard_handler( int (*handler)( void *), void *data);
sic_task_t launch_keyboard( );

void run_xremote( );

void set_close_dialog_handler( void (*handler)( void *), void *data);
void call_close_dialog_handler( );
void set_on_end_dialog_handler( void (*handler)( void));
void call_on_end_dialog_handler( );
void set_on_open_dialog_handler( void (*handler)( const char *helpfilename));
void call_on_open_dialog_handler( const char *helpfilename);

#endif /* _SIC_LAUNCH_H_ */

