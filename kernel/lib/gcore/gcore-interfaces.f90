module gcore_interfaces_public_c
  !---------------------------------------------------------------------
  ! Fortran interfaces to C public subroutines (hand made)
  !---------------------------------------------------------------------
  !
  interface
    subroutine hsv_to_rgb(h,s,v,r,g,b)
      real(kind=4), intent(in)  :: h  !
      real(kind=4), intent(in)  :: s  !
      real(kind=4), intent(in)  :: v  !
      real(kind=4), intent(out) :: r  !
      real(kind=4), intent(out) :: g  !
      real(kind=4), intent(out) :: b  !
    end subroutine hsv_to_rgb
  end interface
  !
  interface
    subroutine rgb_to_hsv(r,g,b,h,s,v)
      real(kind=4), intent(in)  :: r  !
      real(kind=4), intent(in)  :: g  !
      real(kind=4), intent(in)  :: b  !
      real(kind=4), intent(out) :: h  !
      real(kind=4), intent(out) :: s  !
      real(kind=4), intent(out) :: v  !
    end subroutine rgb_to_hsv
  end interface
  !
end module gcore_interfaces_public_c
