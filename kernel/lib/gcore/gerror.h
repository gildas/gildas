
#ifndef _SIC_ERROR_H_
#define _SIC_ERROR_H_

void sic_perror( const char *s);
void sic_pwarning( const char *s);

#endif /* _SIC_ERROR_H_ */

