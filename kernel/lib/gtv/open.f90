subroutine gtopen(type,device,devopt,haut,larg,ncol,error)
  use gildas_def
  use gtv_buffers
  use gtv_graphic
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtopen
  use gtv_plot
  use gtv_protocol
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Opens a specific Device of given Type, eventually using options for
  ! setting parameters. Each device receives a unique identifier IDENT,
  ! which is used internally by the library.
  !
  !  TYPE  The type of Graphics device to be used
  !
  !  DEVICE  Optional argument to indicate to which device the plot must
  !          be sent. Default is DEFOUT(IDENT) for supposedly interactive
  !          devices (usually TT:), and BENSON.VEC for Metacode files.
  !
  !  DEVOPT  Optional string to define some modes for specific devices
  !---------------------------------------------------------------------
  character(len=*)                :: type    ! intent(in)
  character(len=*)                :: device  ! intent(in)
  character(len=*), intent(inout) :: devopt  !
  integer                         :: haut    !
  integer                         :: larg    !
  integer                         :: ncol    ! ignored
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTOPEN'
  logical :: is_default
  integer :: ier,bigcurs,nc
  type(gt_directory), pointer :: topdir
  type(x_output) :: xdisplay
  character(len=*), parameter :: black='BLACK'
  character(len=*), parameter :: white='WHITE'
  character(len=*), parameter :: small='SMALLCURSOR'
  character(len=*), parameter :: big='BIGCURSOR'
  !
  if (.not.awake) then
    call gtv_message(seve%f,rname,'Graphic library not initialized')
    call gtx_err
    return
  endif
  if (error_condition) then
    call gtv_message(seve%e,rname,'Library is in error condition')
    call gtx_err
    return
  endif
  !
  ! Close any previous device and reset parameters
  ! Initialise new device number to I.
  call gtclos(error)
  if (error) then
    call gtv_message(seve%e,rname,'Could not close current device')
    call gtx_err
    call gtnone  ! Fall back to the null device
    return
  endif
  !
  ! Setup device characteristics.
  ! Load it if not yet loaded. We should not try to optimize this, i.e.
  ! in case of reopening the same device, since some parameters are modified
  ! by open_x and other subroutines. Better to start over again.
  call gtx_setup(type,cw_device,error)
  if (error) then
    call gtnone  ! Fall back to the null device
    return
  endif
  gtv_device = device_list(cw_device%ident)
  !
  if (cw_device%protocol.ne.p_x .and.  &
      cw_device%protocol.ne.p_svg) then
    call gtv_message(seve%e,rname,'Unsupported interactive device')
    call gtx_err
    call gtnone  ! Fall back to the null device
    return
  endif
  !
  cw_device%hardw_cursor = .false.
  !
  ! Translate Logical Name DEVICE and find if it is an interactive device
  if (device.eq.' ') then
    if (cw_device%defout.ne.' ') then
      cw_output%file = cw_device%defout
    else
      call sic_terminal(cw_output%file)
    endif
  else
    ! Another device
    cw_output%file = device
  endif
  ier = sic_getlog(cw_output%file)
  !
  cw_device%rxy = abs(cw_device%rxy)
  !
  ! Select appropriate protocol
  call sic_upper(devopt)
  if (cw_device%protocol.eq.p_x) then
    ! Default attributes
    cw_device%background = 1
    bigcurs = 1
    !
    nc = len_trim(devopt)
    if (nc.eq.0) then
      continue
    elseif (devopt.eq.black(1:nc)) then  ! BLACK
      cw_device%background = 0
    elseif (devopt.eq.white(1:nc)) then  ! WHITE
      cw_device%background = 1
    elseif (devopt.eq.big(1:nc))   then  ! BIGCURSOR
      bigcurs = 1
    elseif (devopt.eq.small(1:nc)) then  ! SMALLCURSOR
      bigcurs = 0
    endif
    !
    ! Default Dash for X
#if defined(WIN32) && !defined(MINGW)
    cw_device%hardw_line_weig = .false.  ! No hardware generator
    cw_device%hardw_line_dash = .false.
#else
    cw_device%hardw_line_weig = .true.
    cw_device%hardw_line_dash = .true.
#endif
    !
    ! Open the X connection
    ier = open_x(bigcurs)
    if (ier.eq.0) then
      call gtx_reset
      call gtx_err
      call gtnone  ! Fall back to the null device
      return
    endif
    !
    ! A new window will be attached to each top directory.
    ! ZZZ Ideally we want to reattach windows where we had ones before...
    topdir => root%son_first
    do while (associated(topdir))
      call x_display_reset(xdisplay)  ! Default values
      call gtv_mkdir_topwindow(topdir,xdisplay,error)
      if (error) then
        call gtx_reset
        call gtx_err
        return
      endif
      !
      topdir => topdir%brother
    enddo
    !
    ! Select arbitrarily the first window (if any) as the active window
    call cd_by_win(cw_directory,0,error)
    if (error)  return
    !
  elseif (cw_device%protocol.eq.p_svg) then
    !
    ! Define the current working output
    cw_output%dev => cw_device
    !
    ! Parse DEVOPT
    call gt_hardcopy_svg(devopt,cw_output,error)
    if (error) then
      call gtx_reset
      call gtx_err
      call gtnone  ! Fall back to the null device
      return
    endif
    !
    call svg_open(cw_output,error)
    if (error) then
      call gtv_message(seve%e,'GTZ_OPEN','SVG device open error')
      call gtx_reset
      call gtx_err
      call gtnone  ! Fall back to the null device
      return
    endif
    !
  endif
  !
  ! Inquire the colormap capabilities of the device.
  ! ZZZ Should go just after gtx_setup but some globals (for X windows)
  ! are not yet available.
  call protocol_image_inquire(cw_device)
  ! And load the global colormap and penlut in the device
  is_default = .true.
  call protocol_loadlut(cw_device,gbl_colormap,is_default)
  !
  if (cw_device%linit1.gt.0)  &
    call cwrite(cw_output,cw_device%init1,cw_device%linit1)
  !
  ! Set the %gx attributes
  call sp_gtwindow(cw_output,0.0,phys_sizex_def,0.0,phys_sizey_def)
  !
  call gtview('Update')
  !
end subroutine gtopen
