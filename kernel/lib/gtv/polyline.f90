!-----------------------------------------------------------------------
! General remark on several routines in that file:
!  - I do not understand why calls to GTX_PEN have been suppressed.
!    It may explain the problems mentionned elsewhere with the "Current
!    Pen" (S.Guilloteau 15-Oct-1992)
!-----------------------------------------------------------------------
subroutine gt_polyl(out,poly)
  use gtv_interfaces, except_this=>gt_polyl
  use gtv_protocol
  use gtv_graphic
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !   Generic routine: draw the input polyline in the display 'out',
  ! whereever this polyline comes from.
  !---------------------------------------------------------------------
  type(gt_display),  intent(inout) :: out   ! Input gt_display
  type(gt_polyline), intent(in)    :: poly  ! The polyline instance
  ! Local
  integer :: k,c0,c1,c,cold
  real :: x,y,x0,y0,x1,y1
  real*4 :: gx1,gx2,gy1,gy2
  ! Petite modif pour examiner avec dbx le contenu des tableaux
  logical :: up
  !
  if (.not.awake .or. error_condition) return
  ! call gtx_pen(out,ipenx_i)
  !
  ! No device selected
  if (out%dev%protocol.eq.p_null)  return
  !
  if (associated(poly%penlut)) then
    ! LUT STATIC:
    ! Load the appropriate pen into the device
    call gtx_pen(out,out%icolou,poly%penlut)
  endif
  !
  gx1 = out%gx1
  gx2 = out%gx2
  gy1 = out%gy1
  gy2 = out%gy2
  !
  ! End-points of line are (X0,Y0), (X1,Y1)
  x0 = poly%x(1)
  y0 = poly%y(1)
  call gtx_clip(out,x0,y0,c0)
  up = .true.
  !
  ! Loop on points
  ! Change the end-points of the line (X0,Y0) - (X1,Y1)
  ! to clip the line at the window boundary.
  ! The algorithm is that of Cohen and Sutherland (ref: Newman & Sproull)
  !
  do k=2,poly%n
    x1 = poly%x(k)
    y1 = poly%y(k)
    call gtx_clip(out,x1,y1,c1)
    cold = c1
    !
    do while (ior(c0,c1).ne.0)
      if (iand(c0,c1).ne.0) goto 100   ! line is invisible
      if (c0.eq.0) then
        c = c1
      else
        c = c0
      endif
      if (iand(c,1).ne.0) then ! crosses GX1
        y = y0 + (y1-y0)*(gx1-x0)/(x1-x0)
        x = gx1
      elseif (iand(c,2).ne.0) then ! crosses GX2
        y = y0 + (y1-y0)*(gx2-x0)/(x1-x0)
        x = gx2
      elseif (iand(c,4).ne.0) then ! crosses GY1
        x = x0 + (x1-x0)*(gy1-y0)/(y1-y0)
        y = gy1
      elseif (iand(c,8).ne.0) then ! crosses GY2
        x = x0 + (x1-x0)*(gy2-y0)/(y1-y0)
        y = gy2
      endif
      if (c.eq.c0) then
        x0 = x
        y0 = y
        call gtx_clip(out,x,y,c0)
      else
        x1 = x
        y1 = y
        call gtx_clip(out,x,y,c1)
      endif
    enddo
    !
    ! Selon les protocoles, le travail est different.
    ! Il y a les protocoles intelligent capables de faire du dash et des
    ! largeurs de trait et les autres. Pour les premiers, les parametres
    ! de dash et largeur ont deja ete fixe au moment du set_attributs(...)
    ! une ligne simple suffira dans tous les cas.
    !
    ! Plot the line : Pen up to X0,Y0 then pen down to X1,Y1
    !
    if (user_hardw_line .and.  &
      out%dev%hardw_line_weig .and. out%dev%hardw_line_dash) then
      ! No chopper needed, device knows how to produce width and dash
      if (up) then
        call gtx_plot(out,x0,y0,3)
        call gtx_plot(out,x1,y1,2)
      else
        call gtx_plot(out,x1,y1,2)
      endif
    else
      ! Simulate thickness and/or dash with several lines
      call gtx_chopper(out,x0,y0,x1,y1,up)
    endif
    !
    ! End of loop
100 c0 = cold
    x0 = poly%x(k)                 ! and not X1
    y0 = poly%y(k)                 ! and not Y1
    up = cold.ne.0
  enddo
  !
end subroutine gt_polyl
!
subroutine gtx_clip(out,x,y,c)
  use gtv_interfaces, except_this=>gtx_clip
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !   Routine for the clipping algorithm; called from GTI_CLIP only. C
  ! is a 4 bit code indicating the relationship between point (X,Y) and
  ! the window boundaries; 0 implies the point is within the window.
  !---------------------------------------------------------------------
  type(gt_display), intent(in)  :: out  ! Input gt_display
  real*4,           intent(in)  :: x    !
  real*4,           intent(in)  :: y    !
  integer,          intent(out) :: c    !
  !
  c = 0
  if (x.lt.out%gx1) then
    c = 1
  elseif (x.gt.out%gx2) then
    c = 2
  endif
  if (y.lt.out%gy1) then
    c = c+4
  elseif (y.gt.out%gy2) then
    c = c+8
  endif
  !
end subroutine gtx_clip
