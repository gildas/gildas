
#ifndef _GTV_EVENT_STACK_H_
#define _GTV_EVENT_STACK_H_

#include "xsub.h"
#include "gcore/glaunch.h" 
#include "ggui/dialog.h" 

typedef enum {
    WINDOW_OPERATION_MOVE,
    WINDOW_OPERATION_RESIZE,
    WINDOW_OPERATION_RAISE,
    WINDOW_OPERATION_LOWER,
} window_operation_e;

void gtv_push_refresh( fortran_type adr_dir, int mode, G_env *env);
void gtv_push_create_window( gtv_toolbar_args_t *args);
void gtv_push_clear( G_env *env);
void gtv_push_destroy( G_env *env);
void gtv_push_flush_points( G_env *env);
void gtv_push_zoom( gtv_zoom_data_t *data);
void gtv_push_lens( G_env *env);
void gtv_push_flush( int do_quit);
void gtv_push_set_point( G_env *env, int x, int y);
void gtv_push_add_point( G_env *env, int x, int y);
void gtv_push_set_pen_invert( G_env *env);
void gtv_push_set_pen_rgb( G_env *env, int r, int g, int b);
void gtv_push_set_pen_color( G_env *env, char *color);
void gtv_push_set_pen_width( G_env *env, int width);
void gtv_push_set_pen_dash( G_env *env, int flag_dash, int *ds);
void gtv_push_destroy_segment( fortran_type adr_segment);
void gtv_push_destroy_directory( fortran_type adr_directory);
void gtv_pop_events();
void gtv_on_event( void *event);
void gtv_push_colormap_set_default( size_t colormap);
void gtv_push_colormap_delete( size_t colormap);
void gtv_push_modified_variable( widget_api_t *api, int widget_index);
void gtv_push_window_operation( G_env *genv, window_operation_e oper, int
 data1, int data2);
void gtv_push_callback(void (*callback)(void *), void *data);
char* gtv_event_name(void *event);

#endif /* _GTV_EVENT_STACK_H_ */

