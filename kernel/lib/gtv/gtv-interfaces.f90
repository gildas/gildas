module gtv_interfaces_private_c
  !---------------------------------------------------------------------
  ! @ private
  ! Private interfaces to C routines (hand made)
  !---------------------------------------------------------------------
  !
  logical, external :: x_refresh_genv  ! No-interface (argument type mismatch)
  !
  interface
    function c_get_win_genv(genv_array,iwin)
    use gildas_def
    integer(kind=address_length) :: c_get_win_genv  ! Function value on return
    integer(kind=address_length), intent(in) :: genv_array  !
    integer(kind=4),              intent(in) :: iwin        ! Window C number
    end function c_get_win_genv
    !
    function c_new_genv_array(mwindows)
    use gildas_def
    integer(kind=address_length) :: c_new_genv_array
    integer(kind=4), intent(in) :: mwindows
    end function c_new_genv_array
    !
    function open_x(cross)
    integer(kind=4) :: open_x
    integer(kind=4), intent(in) :: cross
    end function open_x
    !
    function xcolormap_create(r,g,b,siz,defaul)
    use gildas_def
    integer(kind=address_length) :: xcolormap_create
    integer(kind=4), intent(in) :: siz
    real(kind=4),    intent(in) :: r(siz)
    real(kind=4),    intent(in) :: g(siz)
    real(kind=4),    intent(in) :: b(siz)
    logical,         intent(in) :: defaul
    end function xcolormap_create
    !
    function gpng_zopen(filename,lf,npixx,npixy,nchan)
    integer(kind=4) :: gpng_zopen
    character(len=*), intent(in) :: filename
    integer(kind=4),  intent(in) :: lf
    integer(kind=4),  intent(in) :: npixx
    integer(kind=4),  intent(in) :: npixy
    integer(kind=4),  intent(in) :: nchan
    end function gpng_zopen
    !
    subroutine gpng_copy_data(row,r,g,b,a)
    integer(kind=4), intent(in) :: row
    integer(kind=1), intent(in) :: r,g,b,a
    end subroutine gpng_copy_data
    !
    subroutine gpng_finish()
    end subroutine gpng_finish
    !
    subroutine gpng_cleanup()
    end subroutine gpng_cleanup
    !
    subroutine gpng_getbuf(nwords32,addr)
    use gildas_def
    integer(kind=4),              intent(out) :: nwords32
    integer(kind=address_length), intent(out) :: addr
    end subroutine gpng_getbuf
    !
    function si4_to_ui1(si4)
    integer(kind=1) :: si4_to_ui1
    integer(kind=4), intent(in) :: si4
    end function si4_to_ui1
  end interface
  !
end module gtv_interfaces_private_c
!
module gtv_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GTV interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use gtv_interfaces_private
  use gtv_interfaces_private_c
  use gtv_interfaces_public
  !
end module gtv_interfaces
!
module gtv_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! GTV dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gsys_interfaces_public
  use gsys_interfaces_public_c
  use gmath_interfaces_public
  use sic_interfaces_public
  use gcore_interfaces_public_c
  use gchar_interfaces_public
end module gtv_dependencies_interfaces
