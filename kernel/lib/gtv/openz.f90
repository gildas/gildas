subroutine gtz_open(output,error)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtz_open
  use gtv_types
  use gtv_protocol
  use gtv_graphic
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GTVIRT, Z Routine
  !   Open a graphic device
  !---------------------------------------------------------------------
  type(gt_display)       :: output  ! Output instance
  logical, intent(inout) :: error   !
  ! Local
  integer :: ier
  !
  if (output%dev%protocol.eq.p_x          .or.  &
      output%dev%protocol.eq.p_postscript .or.  &
      output%dev%protocol.eq.p_png        .or.  &
      output%dev%protocol.eq.p_svg) then
    ! Do nothing for devices which will open the unit themselves.
    output%opened = .false.
    return
  endif
  !
  ! Open the file for non FORTRAN I/O
  ier = sic_open(output%iunit,output%file,'UNKNOWN',.false.)
  if (ier.ne.0) then
    call gtv_message(seve%f,'GTOPEN','Cannot open graphics device '//  &
    output%file)
    call putios ('E-GTOPEN,  ',ier)
    call gtx_err
    ! Switch to the null_device
    error = .true.
    return
  endif
  !
  output%opened = .true.
  !
end subroutine gtz_open
!
subroutine gtz_close(output)
  use gtv_dependencies_interfaces
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! GTVIRT Z routine
  ! Close a graphic device
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: output  ! Output instance
  ! Local
  integer(kind=4) :: ier
  !
  if (output%iunit.eq.6) return
  if (output%opened) then
    ier = sic_close(output%iunit)
    output%opened = .false.
  endif
end subroutine gtz_close
