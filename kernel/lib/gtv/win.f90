!-----------------------------------------------------------------------
! Drawing routines specific to the Windows protocol (p_x)
!-----------------------------------------------------------------------
subroutine gti_xforceupdate(output)
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Specifique au protocole X
  !---------------------------------------------------------------------
  type(gt_display) :: output  ! Output instance
  output%x%update_status = 2
end subroutine gti_xforceupdate
!
subroutine gti_ximage(output,image)
  use gtv_interfaces, except_this=>gti_ximage
  use gildas_def
  use gtv_bitmap
  use gtv_buffers
  use gtv_plot
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Specifique au protocole X. Version MAC (support de PICT) plus bas
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: output  ! Input gt_display
  type(gt_image),   target        :: image   ! Image instance
  ! Global
  include 'gbl_memory.inc'
  ! Local
  real(kind=4) :: clip(4),trans(4),rmin
  integer(kind=4) :: window(4),ier,wnx,wny
  logical :: recompute,visible
  type(gt_lut), pointer :: lut
  type(gt_bitmap), pointer :: xbitmap,xbtmp
  !
  if (.not.associated(image%r%values))  return  ! Image has been cleaned
  !
  ! Test ridiculous scaling mode:
  rmin = min(image%r%cuts(1),image%r%cuts(2))
  if (rmin.le.0.0 .and. image%scaling.eq.scale_log) then
    call gtv_message(seve%e,'GTI_BITMAP','LowCut is negative!')
    return
  endif
  !
  ! Check the clipping box
  call clip_image(output,  &
    image%r%taille(1),  &
    image%r%taille(2),  &
    image%conv,         &
    image%position,     &
    image%limits,       &
    .true.,             &  ! Always resample to device pixels
    1.0,                &  ! Resampling factor (1 = no extra factor)
    visible,clip,window,trans)
  if (.not.visible)  return
  !
  ! if we are here because of a screen that needs update, we know
  ! that the image pixels have been saved. Else we create the image pixels
  wnx=window(1)
  wny=window(2)
  !
  if (.not.image%isrgb) then
    ! Image is indexed: which LUT use?
    if (associated(image%lut)) then
      lut => image%lut  ! Custom LUT
      if (lut%xlut.eq.0)  &
        call gti_lut(output,lut)  ! Load this LUT, since it is not yet
    else
      lut => gbl_colormap  ! Global LUT
    endif
  endif
  !
  ! Check if we already have a bitmap for this image
  xbitmap => null()
  xbtmp => output%x%bitmap_first
  do while (associated(xbtmp))
    if (associated(xbtmp%image,image)) then
      xbitmap => xbtmp
      exit
    endif
    xbtmp => xbtmp%next
  enddo
  !
  if (associated(xbitmap)) then
    recompute = xbitmap%size(1).ne.wnx .or. xbitmap%size(2).ne.wny .or. &
                any(xbitmap%trans.ne.trans)
  else
    allocate(xbitmap)
    xbitmap%next => null()
    xbitmap%image => image
    xbitmap%irvalues => null()
    xbitmap%igvalues => null()
    xbitmap%ibvalues => null()
    !
    ! Set up the list
    if (associated(output%x%bitmap_first)) then
      output%x%bitmap_last%next => xbitmap
    else
      output%x%bitmap_first => xbitmap
    endif
    output%x%bitmap_last => xbitmap
    !
    recompute = .true.
  endif
  !
  ! No need to recompute if the position on the window changes. Just
  ! update the position.
  xbitmap%position(1) = window(3)
  xbitmap%position(2) = window(4)
  !
  ! Do we need to recompute the bitmap?
  if (output%x%update_status.eq.2 .or. recompute) then
    ! Deallocate the previous resampled image
    if (associated(xbitmap%irvalues))  deallocate(xbitmap%irvalues)
    if (associated(xbitmap%igvalues))  deallocate(xbitmap%igvalues)
    if (associated(xbitmap%ibvalues))  deallocate(xbitmap%ibvalues)
    !
    ! Allocate the new resampled image
    xbitmap%size(1) = window(1)
    xbitmap%size(2) = window(2)
    xbitmap%trans = trans
    if (image%isrgb) then
      allocate(xbitmap%irvalues(wnx,wny),  &
               xbitmap%igvalues(wnx,wny),  &
               xbitmap%ibvalues(wnx,wny),stat=ier)
    else
      allocate(xbitmap%irvalues(wnx,wny),stat=ier)
    endif
    if (ier.ne.0) then
      call gtv_message(seve%e,'GTI_BITMAP','Memory allocation failure')
      return
    endif
    call gti_bitmap(image,output,xbitmap)
    !
  endif
  !
  ! Display it in X_Window
  if (image%isrgb) then
    call x_draw_rgb(output%x%graph_env,         &  ! The window
      xbitmap%irvalues,                         &  ! The R bitmap
      xbitmap%igvalues,                         &  ! The G bitmap
      xbitmap%ibvalues,                         &  ! The B bitmap
      xbitmap%position(1),xbitmap%position(2),  &  ! Its position
      xbitmap%size(1),xbitmap%size(2))             ! Its size
  else
    call x_affiche_image(output%x%graph_env,    &  ! The window
      xbitmap%irvalues,                         &  ! The bitmap
      xbitmap%position(1),xbitmap%position(2),  &  ! Its position
      xbitmap%size(1),xbitmap%size(2),          &  ! Its size
      lut%xlut)                                    ! The LUT to use
  endif
  !
end subroutine gti_ximage
