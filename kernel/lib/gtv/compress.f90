subroutine gtl_compress(line,error)
  use gtv_interfaces, except_this=>gtl_compress
  use gtv_buffers
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command COMPRESS
  !  Permet un retassage de la memoire utilisee sous GREG en version
  ! multifenetre.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='COMPRESS'
  type(gt_directory), pointer :: otherdir
  !
  ! Are we in a directory to be destroyed?
  !
  ! Protect Current Working Directory
  otherdir => cw_directory
  do while(associated(otherdir))
    if (.not.otherdir%gen%visible) then
      call gtv_message(seve%e,rname,  &
        'You are in a directory to be destroyed, move somewhere else before')
      error = .true.
      return
    endif
    !
    ! Check next parent
    otherdir => otherdir%father
  enddo
  !
  call gtv_compress(root,error)  ! Start from root i.e. compress all the tree
  if (error)  return
  !
end subroutine gtl_compress
!
recursive subroutine gtv_compress(dir,error)
  use gtv_interfaces, except_this=>gtv_compress
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Cleanly remove the input directory (and its subdirectories) if it
  ! is invisible, or its invisible segments and subdirectories.
  !---------------------------------------------------------------------
  type(gt_directory), pointer       :: dir    ! Pointer to directory to clean
  logical,            intent(inout) :: error  ! Logical error flag
  ! Local
  type(gt_segment), pointer :: segm,nextsegm
  type(gt_directory), pointer :: son,nextson
  !
  if (.not.associated(dir))  return
  !
  if (.not.dir%gen%visible) then
    ! All the directory and its subdirectories have to be deleted
    call destroy_directory(dir,error)  ! Thread-safe
    if (error)  return
    !
  else
    !
    ! 1) Loop on its segments
    segm => dir%leaf_first
    do while (associated(segm))
      nextsegm => segm%nextseg  ! Keep track *before* destroying 'segm'
      if (.not.segm%head%gen%visible) then
        call clear_segment_elem(segm,error)
        if (error)  exit
      endif
      !
      segm => nextsegm
    enddo
    if (error)  return
    !
    ! 2) Loop on its subdirectories
    son => dir%son_first
    do while (associated(son))
      nextson => son%brother  ! Keep track *before* destroying 'son'
      call gtv_compress(son,error)
      if (error)  return
      !
      son => nextson
    enddo
    !
  endif
  !
end subroutine gtv_compress
