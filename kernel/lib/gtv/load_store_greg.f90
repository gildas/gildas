subroutine attach_greg_values(dir,tab_greg,error)
  use gtv_buffers
  use gtv_interfaces, except_this=>attach_greg_values
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Update the input Greg values and save them in the input directory
  ! descriptor.
  !---------------------------------------------------------------------
  type(gt_directory), intent(inout) :: dir       ! Directory
  type(greg_values),  intent(inout) :: tab_greg  ! Greg values
  logical,            intent(inout) :: error     ! Logical error flag
  !
  tab_greg%lx1 = 0.0
  tab_greg%lx2 = dir%phys_size(1)
  tab_greg%ly1 = 0.0
  tab_greg%ly2 = dir%phys_size(2)
  !
  dir%val_greg = tab_greg
  !
end subroutine attach_greg_values
!
subroutine get_greg_values(tab_greg)
  use gtv_dependencies_interfaces
  use gtv_interfaces
  use gildas_def
  use gtv_graphic
  !---------------------------------------------------------------------
  ! @ no-interface
  !---------------------------------------------------------------------
  integer(kind=4) :: tab_greg(*)  ! Whole block of Greg values
  !
  ! Get values from GreG values in TAB_GREG
  call gt_greg_user(1,tab_greg)
  !
end subroutine get_greg_values
!
subroutine send_greg_values(tab_greg)
  use gtv_dependencies_interfaces
  use gtv_interfaces
  use gildas_def
  use gtv_graphic
  !---------------------------------------------------------------------
  ! @ no-interface
  !---------------------------------------------------------------------
  integer(kind=4) :: tab_greg(*)  ! Whole block of Greg values
  !
  ! Transmit values in TAB_GREG to GreG
  call gt_greg_user(-1,tab_greg)
  !
end subroutine send_greg_values
!
subroutine reset_greg_val(tab_greg)
  use gtv_dependencies_interfaces
  use gtv_interfaces
  use gildas_def
  use gtv_graphic
  !---------------------------------------------------------------------
  ! @ no-interface
  !---------------------------------------------------------------------
  integer(kind=4) :: tab_greg(*)  ! Whole block of Greg values
  !
  ! Reset current position & pen in GreG
  call gt_greg_user(0,tab_greg)
  !
end subroutine reset_greg_val
