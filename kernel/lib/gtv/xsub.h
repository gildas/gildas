
#ifndef _GTV_XSUB_H_
#define _GTV_XSUB_H_

#include "gtv-message-c.h"
#include "gsys/cfc.h"
#include "gsys/sysc.h"
#include "gcore/glaunch.h"

/*
 * Constants
 */

#define NCOL 230

#define NTAB      24

/* variables gerant le mode d'affichage */

#define NO_DITHERING    0
#define BW_DITHERING    1
#define GRAY_DITHERING  2
#define COLOR_DITHERING 3

#define MAX_INTENSITY 0xFFFF /* according to X...*/
#define RED_PERCENT   0.299
#define GREEN_PERCENT 0.587
#define BLUE_PERCENT  0.114

#define MaxGrey       32768    /* limits on the grey levels used */
#define Threshold     16384    /* in the dithering process */
#define MinGrey           0

#define RightToLeft     1
#define LeftToRight    -1

#define GTV_REFRESH_MODE_REWIND ( 0)
#define GTV_REFRESH_MODE_APPEND (-1)
#define GTV_REFRESH_MODE_UPDATE (-2)
#define GTV_REFRESH_MODE_PURGE  (-3)
#define GTV_REFRESH_MODE_DELETE (-4)
#define GTV_REFRESH_MODE_ZAP    (-5)
#define GTV_REFRESH_MODE_LIMITS (-6)
#define GTV_REFRESH_MODE_COLOR  (-7)
#define GTV_REFRESH_MODE_REFRESH (-999) /* used by graphic on expose event */

/*
 * Types
 */

typedef void *fortran_type;

/*----------------------------------------------------------------------
 *
 * Gestion dynamique de l'environnement
 *
 * definition of the graphical environment struct
 * in fact, not all values of the structure are reported in load/store_genv_val
 * these functions only load and store the parts of the genv struct that are
 * used as c global variables.  the gtvirt values of the genv struct, which
 * are used only by the fortran functions, are not saved as c global variables
 * the address of the successor genv struct in the chain is also not saved
 * ----------------------------------------------------------------------
 */
typedef struct genv {
    /* global infos */
    int widthofscreen;
    int heightofscreen;

    int          win_backg;
    int          Width_graphique,Height_graphique;
    struct genv  **genv_array;
    int          fen_num;
    fortran_type adr_dir;
    int          destroy_by_program;

    /* Polyline buffer */
    struct _point {
        int x;
        int y;
    } tabpts[1024];
    int npts;
    int update_gtv_lut;
} G_env;

typedef struct _gtv_zoom_data {
    int start_zoom;
    G_env *env;
    int flg_zoom;
    int width;
    int height;
    int *xpos;
    int *ypos;
    char *command;
    int called_from_main_thread;
} gtv_zoom_data_t;

/* API to implement for each graphic library */
typedef struct {
    G_env *(*graph_creer_genv)( void);
    void (*graph_destroy_window)( G_env *_env);
    void (*graph_refresh)( fortran_type adr_dir, int mode, G_env *env);
    void (*graph_open_window)( G_env *env, int mode);
    void (*graph_close_window)( G_env *env, int mode);
    void (*graph_flush)();
    void (*graph_move_window)( G_env *env, int x, int y);
    void (*graph_resize_window)( G_env *env, int width, int height);
    void (*graph_invalidate_window)( G_env *env);
    void (*graph_clear_window)( G_env *env);
    void (*graph_get_corners)( G_env *env, int x[3], int y[3]);
    int (*graph_get_image_width)( int w);
    void (*graph_new_graph)(int,G_env *,char *,int,int,fortran_type,int);
    void (*graph_create_edit_lut_window)();
    int (*graph_open_x)( int);
    void (*graph_x_pen_invert)( G_env *env);
    void (*graph_x_pen_rgb)( G_env *env, int, int, int);
    void (*graph_x_pen_color)( G_env *env, char *);
    void (*graph_x_fill_poly)( G_env *env, int, int *, int *);
    void (*graph_x_curs)( gtv_zoom_data_t *data);
    void (*graph_ask_for_corners)( int *ax, int *bx, int *ay, int *by);
    void (*graph_other_x_curs)( G_env *env, char *cde);
    void (*graph_x_weigh)( G_env *env, int);
    void (*graph_x_dash)( G_env *env, int, int [4]);
    int (*graph_x_test_event)( G_env *env, int *, int *, int *, int *);
    void (*graph_ximage_loadrgb)( int [], int [], int [], int, int);
    size_t (*graph_xcolormap_create)( float [], float [], float [], int, int);
    void (*graph_xcolormap_set_default)( size_t);
    void (*graph_xcolormap_delete)( size_t);
    int (*graph_ximage_inquire)( int *, int *, int *, int *, int *, int *);
    void (*graph_x_affiche_image)( G_env *env, size_t *, int, int, int, int, int, size_t);
    void (*graph_x_draw_rgb)( G_env *env, size_t *, size_t *, size_t *, int, int, int, int);
    void (*graph_x_clal)( G_env *env);
    void (*graph_x_clpl)( G_env *env);
    void (*graph_x_cmdwin)();
    void (*graph_x_size)( G_env *env, int *, int *);
    void (*graph_x_screen_size)( int *, int *);
    void (*graph_flush_points)( G_env *env, struct _point tabpts[], int npts);
    void (*graph_x_close)( void);
    void (*graph_create_drawing_area)( gtv_toolbar_args_t *args);
    void (*graph_push_event)( void *event);
    void (*graph_x_quit)();
    void (*graph_activate_lens)( G_env *genv);
} graph_api_t;

/*
 *  global definitions
 */

extern graph_api_t *gtv_graph_api;

int gtv_open_segment_writer();
void init_genv( G_env *env);
int gtv_on_resize( G_env *env, int width, int height);
void gtv_on_destroy_genv( G_env *env);
void gtv_refresh( G_env *env, int mode);
void gtv_refresh_segment( fortran_type adr_seg, G_env *env, int mode);
void gtv_refresh_win( fortran_type adr_dir, G_env *env, int mode);
int gtv_called_from_main(void);
void gtv_lens_register( G_env *parent_env, G_env *env);
int gtv_lens_limits( G_env *parent_env, G_env *env, int x, int y, float zoom);
void gtv_get_dir_name( G_env *env, char *dir_name, int dir_name_length);
void gtv_lut_fromeditor(float *red, float *green, float *blue, int size);

void set_graph_api( graph_api_t *api);
/* to define in graphic library */
extern void init_graph_api();

#define gtv_open_segments_for_writing_from_main CFC_EXPORT_NAME(gtv_open_segments_for_writing_from_main)
#define gtv_close_segments_for_writing_from_main CFC_EXPORT_NAME(gtv_close_segments_for_writing_from_main)
#define gtv_open_segments_for_writing_from_graph CFC_EXPORT_NAME(gtv_open_segments_for_writing_from_graph)
#define gtv_close_segments_for_writing_from_graph CFC_EXPORT_NAME(gtv_close_segments_for_writing_from_graph)
#define gtv_open_segments_for_reading_from_main CFC_EXPORT_NAME(gtv_open_segments_for_reading_from_main)
#define gtv_close_segments_for_reading_from_main CFC_EXPORT_NAME(gtv_close_segments_for_reading_from_main)
#define gtv_open_segments_for_reading_from_graph CFC_EXPORT_NAME(gtv_open_segments_for_reading_from_graph)
#define gtv_close_segments_for_reading_from_graph CFC_EXPORT_NAME(gtv_close_segments_for_reading_from_graph)
void CFC_API gtv_open_segments_for_writing_from_main();
void CFC_API gtv_close_segments_for_writing_from_main();
void CFC_API gtv_open_segments_for_writing_from_graph();
void CFC_API gtv_close_segments_for_writing_from_graph();
void CFC_API gtv_open_segments_for_reading_from_main();
void CFC_API gtv_close_segments_for_reading_from_main();
void CFC_API gtv_open_segments_for_reading_from_graph();
void CFC_API gtv_close_segments_for_reading_from_graph();

#endif /* _GTV_XSUB_H_ */

