module gtv_interfaces_public
  interface
    function gtexist(name)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ public
      ! Logical function indicating wether a segment/directory exists
      !---------------------------------------------------------------------
      logical :: gtexist                    ! Function value on return
      character(len=*), intent(in) :: name  ! Name to search for
    end function gtexist
  end interface
  !
  interface
    subroutine gtchar(nchars,string,size,slength,xp,yp,co,si,ic,ifont,  &
      doclip,gpoly_clip)
      use phys_const
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      !   Plots a character string according to current line attributes.
      ! Indices, subscripts, and special fonts are selected using the escape
      ! character \ (C.f. GREG and VSTRIN routine in CHARSHR).
      !
      !   The hard font line attribute is incompatible with the handling of
      ! the escape character \ . The argument IFONT indicates which font
      ! should be taken into account: 0 = Hardware, 1 Simplex, 2 Duplex. If
      ! the hardware generator cannot reasonably match (CO,SI) and SIZE, the
      ! SIMPLEX font is used.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: nchars      ! Number of characters
      character(len=*), intent(in)  :: string      ! Character string
      real(kind=4),     intent(in)  :: size        ! Character width (plot page units)
      real(kind=4),     intent(out) :: slength     ! String length (plot page units)
      real(kind=4),     intent(in)  :: xp          ! Position where to write the string
      real(kind=4),     intent(in)  :: yp          !
      real(kind=4),     intent(in)  :: co          ! Cosine of angle
      real(kind=4),     intent(in)  :: si          ! Sine of angle
      integer(kind=4),  intent(in)  :: ic          ! Centering option according to GREG conventions
      integer(kind=4),  intent(in)  :: ifont       ! Font type (0 Simplex, 1 Duplex)
      logical,          intent(in)  :: doclip      ! Clip with current Greg box?
      external                      :: gpoly_clip  !
      interface
        subroutine gpoly_clip(nq,xq,yq)
          use gildas_def
          integer(kind=size_length), intent(in) :: nq     ! Number of points
          real(kind=4),              intent(in) :: xq(*)  ! Plot X coordinates
          real(kind=4),              intent(in) :: yq(*)  ! Plot Y coordinates
        end subroutine gpoly_clip
      end interface
    end subroutine gtchar
  end interface
  !
  interface
    subroutine gtg_charlen (nchars,string,size,slength,ifont)
      use gtv_tree
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public
      !   Returns the length of a character string according to current
      ! font attributes. No action on the plot. See also GTCHAR
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: nchars   ! Number of characters
      character(len=*), intent(in)  :: string   ! Character string
      real(kind=4),     intent(in)  :: size     ! Character size (plot page units)
      real(kind=4),     intent(out) :: slength  ! String length (plot page units)
      integer(kind=4),  intent(in)  :: ifont    ! Font type
    end subroutine gtg_charlen
  end interface
  !
  interface
    subroutine gtclal
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public obsolescent 22-sep-2009
      ! Obsolete entry point to 'gtv_clear_alpha'. To be removed from
      ! official documentation before.
      !---------------------------------------------------------------------
    end subroutine gtclal
  end interface
  !
  interface
    subroutine gt_clear_segment(argum_in,present,error)
      use gtv_buffers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Destroy the input segment(s) given its(their) name. They can have
      ! an absolute path. Argument 'present' indicates if the segment(s)
      ! must exist i.e. if it should be an error if it(they) is(are) not
      ! found.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: argum_in  ! Segment name
      logical,          intent(in)    :: present   ! Must the segment exist?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine gt_clear_segment
  end interface
  !
  interface
    subroutine gtv_clear_directory(name,error)
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support for CLEAR DIRECTORY, using a name as input argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   !
      logical,          intent(inout) :: error  !
    end subroutine gtv_clear_directory
  end interface
  !
  interface
    subroutine exit_clear
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      !  This function exists solely to prevent all the wild machinations
      ! that 'gtv_destroy_all' does in recreating the x graphical
      ! environment, when all you are really trying to do is exit the
      ! program so you can go home. It is therefore only called by program
      ! GreG before calling GTCLOS
      !---------------------------------------------------------------------
      ! Local
    end subroutine exit_clear
  end interface
  !
  interface
    subroutine gtg_screen(qx1,qx2,qy1,qy2)
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      !  Returns the coordinates of the end pixels of the physical screen
      ! (world units). Very useful for interactive zooming. Works on the
      ! Current Working Output.
      !---------------------------------------------------------------------
      real, intent(out) :: qx1,qx2  !
      real, intent(out) :: qy1,qy2  !
    end subroutine gtg_screen
  end interface
  !
  interface
    function gtg_curs()
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      !   Informs if a cursor is available on the current device
      !---------------------------------------------------------------------
      logical :: gtg_curs  ! Function value on return
    end function gtg_curs
  end interface
  !
  interface
    subroutine gtcurs(xcm,ycm,ch,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      ! Same as gtcurs_sub but works on the Current Working Output
      !---------------------------------------------------------------------
      real,             intent(inout) :: xcm,ycm  !
      character(len=*), intent(out)   :: ch       !
      logical,          intent(inout) :: error    !
    end subroutine gtcurs
  end interface
  !
  interface
    subroutine gicurs(xcm,ycm,rect_xw_cm,rect_yw_cm,ch,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ public
      ! Same as gicurs_sub but works on the Current Working Output
      !---------------------------------------------------------------------
      real(kind=4),     intent(inout) :: xcm,ycm                !
      real(kind=4),     intent(in)    :: rect_xw_cm,rect_yw_cm  !
      character(len=*), intent(out)   :: ch                     !
      logical,          intent(inout) :: error                  !
    end subroutine gicurs
  end interface
  !
  interface
    subroutine gtv_destroy_directory(name,error)
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support for DESTROY DIRECTORY, using a name as input argument
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   !
      logical,          intent(inout) :: error  !
    end subroutine gtv_destroy_directory
  end interface
  !
  interface
    subroutine gtv_destroy_all(error)
      use gildas_def
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_plot
      use gtv_segatt
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Destroy all the trees and windows.
      !   Since it is forbidden to user to be in root (<) and to create
      ! segments in it, he would have first to create a new top-directory
      ! and go in it. So we do it implicitely for him.
      !   In practice, destroy all the trees and windows EXCEPT the first
      ! window of the first child under root (normally, <GREG). This avoids
      ! to destroy and recreate a new window which could be moved to another
      ! place by the window manager.
      ! ---
      !  Thread status: safe to call from main thread.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtv_destroy_all
  end interface
  !
  interface
    subroutine gtclear
      !---------------------------------------------------------------------
      ! @ public obsolete
      ! Obsolete entry point to 'gtv_destroy_all'. To be removed from
      ! some packages and official documentation before.
      ! SB, 22-09-2009
      !---------------------------------------------------------------------
    end subroutine gtclear
  end interface
  !
  interface
    subroutine gtv_image(nx,ny,data,location,limits,convert,scaling,cuts,extrema,  &
      blank,is_visible,is_curima,error)
      use gbl_message
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !  Allocate a new GTVIRT image slot. Image numbers may not be
      ! continuous, search for one from start.
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nx   ! Image size in X
      integer(kind=index_length), intent(in) :: ny   ! Image size in Y
      real(kind=4),    intent(in)    :: data(nx,ny)  ! Image values
      real(kind=4),    intent(in)    :: location(4)  ! Position in Paper coordinates
      real(kind=4),    intent(in)    :: limits(4)    ! User limits
      real(kind=4),    intent(in)    :: convert(6)   ! Image pixel to User conversion formula
      integer(kind=4), intent(in)    :: scaling      ! Scaling mode (1=Lin, 2=Log, 3=Equ)
      real(kind=4),    intent(in)    :: cuts(2)      ! Low and High cut
      real(kind=4),    intent(in)    :: extrema(2)   ! Low and High extrema
      real(kind=4),    intent(in)    :: blank(3)     ! Blanking value
      logical,         intent(in)    :: is_visible   ! Always Visible ?
      logical,         intent(in)    :: is_curima    ! Update the SIC structure CURIMA% ?
      logical,         intent(inout) :: error        ! Logical error flag
    end subroutine gtv_image
  end interface
  !
  interface
    subroutine gtv_rgbimage(nx,ny,    &
        rdata,rblank,rcuts,rextrema,  &
        gdata,gblank,gcuts,gextrema,  &
        bdata,bblank,bcuts,bextrema,  &
        scaling,scaling_lup_b,        &
        location,limits,convert,is_visible,error)
      use gbl_message
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !  Allocate a new GTVIRT image slot. Image numbers may not be
      ! continuous, search for one from start.
      !---------------------------------------------------------------------
      integer(kind=index_length), intent(in) :: nx         ! RGB image size in X
      integer(kind=index_length), intent(in) :: ny         ! RGB image size in Y
      real(kind=4),    intent(in)    :: rdata(nx,ny)       ! R image values
      real(kind=4),    intent(in)    :: rblank(3)          ! R blanking value
      real(kind=4),    intent(in)    :: rcuts(2)           ! R low and high cut
      real(kind=4),    intent(in)    :: rextrema(2)        ! R low and high extrema
      real(kind=4),    intent(in)    :: gdata(nx,ny)       ! G image values
      real(kind=4),    intent(in)    :: gblank(3)          ! G blanking value
      real(kind=4),    intent(in)    :: gcuts(2)           ! G low and high cut
      real(kind=4),    intent(in)    :: gextrema(2)        ! G low and high extrema
      real(kind=4),    intent(in)    :: bdata(nx,ny)       ! B image values
      real(kind=4),    intent(in)    :: bblank(3)          ! B blanking value
      real(kind=4),    intent(in)    :: bcuts(2)           ! B low and high cut
      real(kind=4),    intent(in)    :: bextrema(2)        ! B low and ligh extrema
      integer(kind=4), intent(in)    :: scaling            ! RGB scaling mode
      real(kind=4),    intent(in)    :: scaling_lup_b      ! Beta parameter for Lupton scaling mode
      real(kind=4),    intent(in)    :: location(4)        ! Position in Paper coordinates
      real(kind=4),    intent(in)    :: limits(4)          ! User limits
      real(kind=4),    intent(in)    :: convert(6)         ! Image pixel to User conversion formula
      logical,         intent(in)    :: is_visible         ! Always Visible ?
      logical,         intent(inout) :: error              ! Logical error flag
    end subroutine gtv_rgbimage
  end interface
  !
  interface
    subroutine gtv_fillpoly(n,x,y,vert,error)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !     Plot a filled polygon. Filling based on Horizontal scanning.
      !     or Vertical scanning if VERT = .TRUE.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n      ! Number of points
      real(kind=4),    intent(in)    :: x(n)   !
      real(kind=4),    intent(in)    :: y(n)   !
      logical,         intent(in)    :: vert   ! Horizontal or vertical polygon?
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gtv_fillpoly
  end interface
  !
  interface
    subroutine gr8_bltlis(lpoly,llv,npoly,nvert,xv,yv,iy,lnx,ixl,ixu)
      !---------------------------------------------------------------------
      ! @ public
      ! From a list of polygons, BLTLIS makes a list of horizontal line
      ! segments included in the polygons. There might several segments
      ! to fill for a row crossed by a single polygon.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: lpoly      ! Beginning polygon number
      integer(kind=4), intent(in)  :: llv        ! Beginning vertex number
      integer(kind=4), intent(in)  :: npoly      ! Number of polygons
      integer(kind=4), intent(in)  :: nvert(2)   ! Number of vertices in each polygon
      real(kind=8),    intent(in)  :: xv(1024)   ! X position of vertices
      real(kind=8),    intent(in)  :: yv(1024)   ! Y position of vertices
      real(kind=8),    intent(in)  :: iy         ! Row number
      integer(kind=4), intent(out) :: lnx        ! Number of segments
      real(kind=8),    intent(out) :: ixl(1024)  ! Low x index of segments
      real(kind=8),    intent(out) :: ixu(1024)  ! High x index of segments
    end subroutine gr8_bltlis
  end interface
  !
  interface
    subroutine gr4_bltlis(lpoly,llv,npoly,nvert,xv,yv,iy,lnx,ixl,ixu)
      !---------------------------------------------------------------------
      ! @ public
      ! From a list of polygons, BLTLIS makes a list of horizontal line
      ! segments included in the polygons. There might several segments
      ! to fill for a row crossed by a single polygon.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: lpoly      ! Beginning polygon number
      integer(kind=4), intent(in)  :: llv        ! Beginning vertex number
      integer(kind=4), intent(in)  :: npoly      ! Number of polygons
      integer(kind=4), intent(in)  :: nvert(2)   ! Number of vertices in each polygon
      real(kind=4),    intent(in)  :: xv(1024)   ! X position of vertices
      real(kind=4),    intent(in)  :: yv(1024)   ! Y position of vertices
      real(kind=4),    intent(in)  :: iy         ! Row number
      integer(kind=4), intent(out) :: lnx        ! Number of segments
      real(kind=4),    intent(out) :: ixl(1024)  ! Low x index of segments
      real(kind=4),    intent(out) :: ixu(1024)  ! High x index of segments
    end subroutine gr4_bltlis
  end interface
  !
  interface
    subroutine gtv_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine gtv_message_set_id
  end interface
  !
  interface
    subroutine gtview(acha)
      use gtv_protocol
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Plot to the interactive graphic device
      !
      ! 'Append'   Plot from last vector drawn
      ! 'Rewind'   Clear the screen, and plot from first vector
      ! 'Update'   Update the screens (Clear + Redraw)
      ! 'Color'    Update the screens (Redraw only)
      !
      ! Does not modify the virtuel pen position and the immediate pen
      ! position. P. Valiron
      !
      ! Modifiee pour parcourir une arborescence par R.Gras. (Octobre 1992)
      ! Modifiee par S.Guilloteau pour tracer les images AVANT les
      ! segments graphiques en cas de Rewind. Bigrement modifiee par GD, pendant
      ! qu'on y est...
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: acha  ! Key character of the required mode
    end subroutine gtview
  end interface
  !
  interface
    subroutine gtinit(sx,sy,lunt,lunh,main,greg_user,error)
      use gildas_def
      use gbl_message
      use gtv_bitmap
      use gtv_buffers
      use gtv_colors
      use gtv_graphic
      use gtv_plot
      use gtv_protocol
      use gtv_segatt
      use gtv_tree
      !---------------------------------------------------------------------
      ! @ public
      ! Initializes the GTVIRT on a blank world space
      !---------------------------------------------------------------------
      real(kind=4),     intent(in)    :: sx     ! World space in X [0...SX]
      real(kind=4),     intent(in)    :: sy     ! World space in Y [0...SY]
      integer(kind=4),  intent(in)    :: lunt   ! Logical unit number for terminal
      integer(kind=4),  intent(in)    :: lunh   ! Logical unit number for hardcopy. Unused.
      character(len=*), intent(in)    :: main   ! Main directory name
      external :: greg_user  ! Address of a routine to get user coordinates transformation
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtinit
  end interface
  !
  interface
    subroutine gt_setphysical(sx,sy,error)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      !  @ public
      !  Reset the physical size of the current tree (only): top directory
      ! must be emptied and use the new values before storing anything. Next
      ! top directories will also use these values.
      !  In practice, this is done like this:
      !   1) go to top directory
      !   2) empty top directory
      !   3) set new physical size of top directory in GTV
      !   4) store new physical size of top directory in GREG (callback)
      !---------------------------------------------------------------------
      real(kind=4), intent(in)    :: sx     ! World space in X [0...SX]
      real(kind=4), intent(in)    :: sy     ! World space in X [0...SX]
      logical,      intent(inout) :: error  ! Logical error flag
    end subroutine gt_setphysical
  end interface
  !
  interface
    subroutine gtreloc(x,y)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !
      !	Relocates the virtual pen to (X,Y)
      !	X,Y	World coordinates			Real*4
      !---------------------------------------------------------------------
      real*4 :: x                       !
      real*4 :: y                       !
    end subroutine gtreloc
  end interface
  !
  interface
    subroutine gtdraw(x,y)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !
      !	Draws to (X,Y)
      !	X,Y	World coordinates			Real*4
      !---------------------------------------------------------------------
      real*4 :: x                       !
      real*4 :: y                       !
    end subroutine gtdraw
  end interface
  !
  interface
    subroutine gtsegm(name,error)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Initialise a new segment with the current line attributes. No
      ! action if the current segment is empty (except making the
      ! Attribute(4) positive, i.e. visible).
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: name   ! Name of the new segment
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine gtsegm
  end interface
  !
  interface
    subroutine gtsegm_close(error)
      use gbl_message
      use gtv_buffers
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public?
      !  Close the segment currently opened. It is harmless to close several
      ! time the same segment.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtsegm_close
  end interface
  !
  interface
    subroutine gtedit(dashed,weight,colour,level,visible)
      use gbl_message
      use gtv_buffers
      use gtv_graphic
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ public
      !   Edit the current segment with the current line attributes
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: dashed   ! Line Type
      real(kind=4),    intent(in) :: weight   ! [cm] Line width
      integer(kind=4), intent(in) :: colour   ! Colour index
      integer(kind=4), intent(in) :: level    ! Display Depth
      logical,         intent(in) :: visible  ! Visible segment attribute
    end subroutine gtedit
  end interface
  !
  interface
    subroutine gtdls
      use gtv_buffers
      use gtv_graphic
      use gtv_segatt
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !   Deletes the last segment in the current directory, taking care
      ! of the chain and pointers in the tree.
      !
      ! Does not modify:
      !  - the virtuel pen position,
      !  - the immediate pen position,
      !  - the pen attributes. Those from the previous segment might not be
      !    consistent with the current pen exposed in GreG.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      ! Local
    end subroutine gtdls
  end interface
  !
  interface
    subroutine gtl_corner(error)
      use gtv_buffers
      !---------------------------------------------------------------------
      ! @ public
      ! GTVIRT support routine for command
      !   CORNERS
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtl_corner
  end interface
  !
  interface
    subroutine run_gtvl(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-mandatory (public but should not) (mandatory because
      ! symbol is used elsewhere)
      !---------------------------------------------------------------------
      character(len=*)              :: line   !
      character(len=*)              :: comm   !
      logical,          intent(out) :: error  !
    end subroutine run_gtvl
  end interface
  !
  interface
    subroutine gtv_lut(error)
      use gtv_tree
      use gtv_buffers
      use gtv_plot
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Save the current bitmap color Look Up Table into the METACODE
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtv_lut
  end interface
  !
  interface
    subroutine gterflag(flag)
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public
      !
      !	Controls whether fatal errors cause an Abort or just set an
      !	error condition that can be sensed using the GTERRGTO or
      !	GTERRTST routines.
      !
      !	This is the only routine that you can call BEFORE the first call
      !	to GTINIT.
      !
      ! Argument :
      !	FLAG	.FALSE.	Errors are fatal (Default behaviour)
      !		.TRUE.  Errors are not fatal and can be sensed using
      !			the routine GTERRTST
      !---------------------------------------------------------------------
      logical :: flag                   !
    end subroutine gterflag
  end interface
  !
  interface
    function gterrtst()
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ public
      !
      !	Logical function. Senses the ERROR_CONDITION switch.
      !
      !	Returns the value of the ERROR_CONDITION flag.
      !	Resets the ERROR_CONDITION flag.
      !---------------------------------------------------------------------
      logical :: gterrtst               !
    end function gterrtst
  end interface
  !
  interface
    subroutine gtv_pencol_arg2id(rname,line,iopt,iarg,icol,error)
      use gbl_message
      use gtv_colors
      !---------------------------------------------------------------------
      ! @ public
      ! Decode the a color argument and return the GTV internal identifier
      ! for this color. Input can be:
      !  - A color name, e.g. RED, BLUE, FOREST_GREEN,...
      !  - A Greg color number (1=RED, ..., 23=user custom color)
      !  - An integer variable providing a Greg color number
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: iopt
      integer(kind=4),  intent(in)    :: iarg
      integer(kind=4),  intent(out)   :: icol
      logical,          intent(inout) :: error
    end subroutine gtv_pencol_arg2id
  end interface
  !
  interface
    subroutine gtv_pencol_name2id(rname,colname,icol,error)
      use gtv_colors
      !---------------------------------------------------------------------
      ! @ public
      ! Return the color identifier given its name. Input must be
      ! uppercased. A full name is not ambiguous. Error if not found.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      character(len=*), intent(in)    :: colname
      integer(kind=4),  intent(out)   :: icol
      logical,          intent(inout) :: error
    end subroutine gtv_pencol_name2id
  end interface
  !
  interface
    subroutine gtv_pencol_num2id(rname,colnum,colid,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Translate Greg color number [-1:23] to GTV color identifier
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      integer(kind=4),  intent(in)    :: colnum
      integer(kind=4),  intent(out)   :: colid
      logical,          intent(inout) :: error
    end subroutine gtv_pencol_num2id
  end interface
  !
  interface
    function gtv_pencol_id2name(icol)
      use gtv_colors
      !---------------------------------------------------------------------
      ! @ public
      ! Return the color name given its identifier. No error, returns ???
      ! if not found.
      !---------------------------------------------------------------------
      character(len=colname_length) :: gtv_pencol_id2name  ! Function value on return
      integer(kind=4), intent(in) :: icol
    end function gtv_pencol_id2name
  end interface
  !
  interface
    subroutine gtv_penwei_arg2val(rname,line,iopt,iarg,wei,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Decode the weight argument and return the width (cm) for this
      ! weight. Input can be:
      !  - A Greg weight number (1 to 5 according to 'graisse()' predefined
      !    weights) and possible formulas or variable
      !  - A value of the form "1.23mm" (mm are used for user interface)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: iopt
      integer(kind=4),  intent(in)    :: iarg
      real(kind=4),     intent(out)   :: wei
      logical,          intent(inout) :: error
    end subroutine gtv_penwei_arg2val
  end interface
  !
  interface
    subroutine gtv_penwei_num2val(rname,iwei,wei,error)
      use gbl_message
      use gtv_segatt
      !---------------------------------------------------------------------
      ! @ public
      ! Decode the Greg weight number as the actual width (cm)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      integer(kind=4),  intent(in)    :: iwei
      real(kind=4),     intent(out)   :: wei
      logical,          intent(inout) :: error
    end subroutine gtv_penwei_num2val
  end interface
  !
  interface
    function gtv_penwei_tostr(wei)
      use gtv_segatt
      !---------------------------------------------------------------------
      ! @ public
      !  Convert the input weight value (cm) to a user friendly output (mm),
      ! e.g.
      !   "1 0.15mm" for predefined weights, or
      !   "  1.23mm" for custom weights
      !---------------------------------------------------------------------
      character(len=8) :: gtv_penwei_tostr  ! Function value on return
      real(kind=4), intent(in) :: wei  ! [cm]
    end function gtv_penwei_tostr
  end interface
  !
  interface
    subroutine gtv_points(n,x,y,error)
      use gbl_message
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ public
      !  Store a list of dotted points in the GTV tree. For larger points,
      ! polylines (one per point) must be used.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n      ! Number of points
      real(kind=4),    intent(in)    :: x(n)   !
      real(kind=4),    intent(in)    :: y(n)   !
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gtv_points
  end interface
  !
end module gtv_interfaces_public
