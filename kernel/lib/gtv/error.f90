subroutine gtx_err
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtx_err
  use gildas_def
  use gtv_graphic
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Traps an error according to ERFLAG.
  !---------------------------------------------------------------------
  error_condition = .true.
  if (erflag) return
  call gtv_message(seve%f,'GTVIRT','Error not trapped')
  call sysexi (fatale)
end subroutine gtx_err
