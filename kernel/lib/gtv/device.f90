subroutine gtl_device(line,error)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_device
  use gtv_protocol
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! GTVIRT Support routine for command
  ! DEVICE [Dev_type [Options]]
  ! 1  [/OUTPUT Dev_name]
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DEVICE'
  logical :: optout
  character(len=132) :: file
  character(len=12) :: term1,term2
  character(len=1), parameter :: backslash=char(92)
  integer :: haut,larg,ncol,nc
  !
  ! If no argument, print the current device
  if (.not.sic_present(0,1)) then
    call gtv_message(seve%i,rname,  &
      'Current device is '//device_list(cw_device%ident))
    return
  endif
  !
  ! Option /OUTPUT ?
  file = ' '
  optout = sic_present(1,1)
  call sic_ch (line,1,1,file,nc,.false.,error)
  if (error) return
  !
  ! Get the terminal names
  term1 = ' '
  term2 = ' '
  call sic_ke (line,0,1,term1,nc,.false.,error)
  call sic_ke (line,0,2,term2,nc,.false.,error)
  if (error) return
  !
  ! Device NONE
  if (term1(1:2).eq.'NO') then
    if (index('NONE',trim(term1)).eq.1 .and. term2.eq.' ') then
      call gtclos(error)  ! Close the current device
      call gtnone  ! Open the "none" device
      call gtv_message(seve%w,rname,'No device active')
      return
    endif
  endif
  !
  ! Output device
  error = gterrtst()
  if (error) return
  haut = 0
  larg = 0
  call gtopen(term1,file,term2,haut,larg,ncol,error)
  error = error .or. gterrtst()
  if (error) return
  !
  ! Format LINE
  line = 'GTVL'//backslash//'DEVICE '//trim(term1)//' '//term2
  if (optout) then
    line = trim(line)//' /OUTPUT '//file
  endif
  !
end subroutine gtl_device
