subroutine gtx_plot(out,x,y,jmode)
  use gildas_def
  use gtv_types
  use gtv_graphic
  use gtv_interfaces, except_this=>gtx_plot
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  !  This is a PLOT routine, simulating the calcomp PLOT routine, X,Y
  ! are the co-ordinates (World units) of a plotter point, and
  !    MODE=3 means plot a dark vector (pen up) to X,Y
  !    MODE=2 means plot a real vector (pen down) to X,Y
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: out    ! Input gt_display
  real*4,           intent(in)    :: x,y    ! World units
  integer,          intent(in)    :: jmode  !
  ! Local
  integer*4 :: ixc,iyc
  !
  if (.not.awake .or. error_condition) return
  !
  if (out%dev%protocol.eq.p_null)  then
    ! No device selected.
    return
    !
  elseif (out%dev%protocol.eq.p_postscript) then
    ! Plot a dark vector
    if (jmode.eq.3) then
      call ps_moveto(x,y)
    elseif (jmode.eq.2) then
      call ps_lineto(x,y)
    endif
    return
    !
  elseif (out%dev%protocol.eq.p_svg) then
    ! Plot a dark vector
    if (jmode.eq.3) then
      call svg_moveto(out,x,y,.false.)
    elseif (jmode.eq.2) then
      call svg_lineto(out,x,y)
    endif
    return
    !
  elseif (out%dev%protocol.eq.p_png) then
    ! Plot a dark vector
    if (jmode.eq.3) then
      call png_moveto(out,x,y)
    elseif (jmode.eq.2) then
      call png_lineto(out,x,y)
    endif
    return
  endif
  !
  call world_to_pixel(out,x,y,ixc,iyc)
  !
  ! Plot a dark vector
  if (jmode.eq.3) then
    if (out%dev%protocol.eq.p_x) then
      call x_movp1(out%x%graph_env,ixc,iyc)
    endif
    !
    !  In mode 3, a dark vector, always set the full address into the
    !  buffer.  Strictly, this is not always necessary, but play safe.
    !
    ! Plot a light vector
  elseif (jmode.eq.2) then
    !
    if (out%dev%protocol.eq.p_x) then
      call x_movp2(out%x%graph_env,ixc,iyc)
    endif
    !
  endif
  !
end subroutine gtx_plot
