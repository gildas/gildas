!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gtl_gtv(line,error)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_gtv
  use gildas_def
  use gtv_graphic
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command GTVL\GTV
  !  - GTV SEARCH Search for the existence of a GTV directory
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTV'
  integer :: nc,nact
  character(len=filename_length) :: directory
  integer, parameter :: mact=1
  character(len=12) :: name,action,acts(mact)
  data acts /'SEARCH'/
  !
  ! Retrieve action
  call sic_ke(line,0,1,name,nc,.true.,error)
  if (error) return
  !
  ! Resolve ambiguities
  call sic_ambigs(rname,name(1:nc),action,nact,acts,mact,error)
  if (error) return
  !
  ! Do the job
  if (action.eq.'SEARCH') then
    ! Get directory name
    call sic_ch(line,0,2,directory,nc,.true.,error)
    if (error) return
    ! Upcase
    call sic_upper(directory)
    ! Search for the directory existence
    dexist = gtexist(directory)
  endif
  !
end subroutine gtl_gtv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
