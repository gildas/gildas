subroutine gtl_mkdir(line,error)
  use gildas_def
  use gtv_buffers
  use gtv_graphic
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_mkdir
  use gtv_protocol
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   CREATE DIRECTORY nomdir
  !   1      [/PLOT_PAGE SizeX SizeY]
  !   2      [/PIXEL Px Py]
  !   5      [/SIZE SizeX SizeY]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='CREATE DIRECTORY'
  character(len=32) :: nomdir
  character(len=80) :: dir_parent,chemin_cour
  integer :: nc,indice,ld
  logical :: opt,isdir,found
  type(greg_values) :: tab_greg
  type(gt_directory), pointer :: dir,parent
  type(gt_segment), pointer :: segm
  !
  call sic_ch(line,0,2,nomdir,nc,.true.,error)
  if (error) return
  call sic_upper(nomdir)
  opt = sic_present(1,0) .or. sic_present(2,0) .or.  &
        sic_present(3,0) .or. sic_present(4,0) .or.  &
        sic_present(5,0)  ! /SIZE, but is obsolete
  !
  ! 0) Current working directory
  call cree_chemin_dir(cw_directory,chemin_cour,nc)
  ! Update GREG values of Current Working Directory. If we create a
  ! subdirectory of this cwd hereafter, it will inherit up-to-date
  ! GREG values from its father.
  if (flag_greg) then
    call get_greg_values(tab_greg)
    call attach_greg_values(cw_directory,tab_greg,error)
  endif
  !
  ! 1) Check if input directory does not exists
  call decode_chemin(nomdir,cw_directory,dir,isdir,segm,found)
  if (found) then
     if (isdir) then
       call cree_chemin_dir(dir,nomdir,ld)
       call gtv_message(seve%e,rname,'Directory '//trim(nomdir)//  &
       ' already exists')
     else
       call cree_chemin_seg(segm,nomdir,ld)
       call gtv_message(seve%e,rname,trim(nomdir)//' already exists and '//  &
       'is a segment')
     endif
     error = .true.
     return
  endif
  !
  ! 2) Rewind up to the parent directory, which must exist
  indice = index(nomdir,dir_sep,.true.)
  if (indice.eq.1) then
     dir_parent = dir_root
     parent => root
     !
  elseif (indice.gt.1) then
     dir_parent = nomdir(:indice-1)  ! e.g. A<B<..<C<D
     ! If path is absolute (starts with '<'), decode_chemin ignores input
     ! ad_cour and searches from root. Fine!
     call decode_chemin(dir_parent,cw_directory,dir,isdir,segm,found)
     if (.not.found .or. .not.isdir) then
        call gtv_message(seve%e,rname,'Parent directory '//trim(dir_parent)//  &
        ' does not exist')
        error = .true.
        return
     else
        call cree_chemin_dir(dir,dir_parent,ld)
        ! call gtv_message(seve%i,rname,'Found parent directory '//dir_parent)
        parent => dir
     endif
     !
  else
     dir_parent = chemin_cour
     parent => cw_directory
  endif
  !
  ! 3) Create the son
  nomdir = nomdir(indice+1:)
  call gtv_message(seve%d,rname,  &
    'Creating directory '//trim(nomdir)//' in '//dir_parent)
  !
  if (associated(parent,root)) then
    call gtl_mkdir_topdir(line,nomdir,error)
    if (error)  return
    !
  elseif (opt) then
    call gtv_message(seve%e,rname,'Options are forbidden for sub-directories')
    error = .true.
    return
    !
  else
    ! Create a standard sub-directory. Inherits its parent physical size.
    call gtsegm_dir(nomdir,parent,parent%phys_size(1),parent%phys_size(2),error)
    if (error) then
      ! This means directory already exists. This is already tested, should
      ! not happen.
      return
    endif
  endif
  !
end subroutine gtl_mkdir
!
subroutine gtl_mkdir_topdir(line,nomdir,error)
  use gbl_message
  use gtv_buffers
  use gtv_graphic
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_mkdir_topdir
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   CREATE DIRECTORY DirName
  ! 1 [/PLOT_PAGE Sx Sy]
  ! 5 [/SIZE Sx Sy]  (obsolete)
  ! Create a top-level directory
  !---------------------------------------------------------------------
  character(len=*),   intent(in)    :: line    ! Command line
  character(len=*),   intent(in)    :: nomdir  ! The new directory name
  logical,            intent(inout) :: error   ! Error flag
  ! Local
  character(len=*), parameter :: rname='CREATE DIRECTORY'
  integer(kind=4) :: nc,sever
  real*4 :: sizex,sizey
  type(x_output) :: xdisplay
  logical :: isdir,found
  type(gt_directory), pointer :: dir
  type(gt_segment), pointer :: segm
  character(len=12) :: argx,argy
  !
  ! 1) Option /PLOT_PAGE
  sizex = phys_sizex_def
  call sic_r4(line,1,1,sizex,.false.,error)
  if (error) return
  sizex = abs(sizex)
  !
  sizey = phys_sizey_def
  call sic_r4(line,1,2,sizey,.false.,error)
  if (error) return
  sizey = abs(sizey)
  !
  if (sic_present(5,0)) then
    ! /SIZE
    if (strict2011) then
      sever = seve%e
    else
      sever = seve%w
    endif
    call gtv_message(sever,rname,  &
      'Option /SIZE is obsolete. Use /PLOT_PAGE instead')
    if (strict2011) then
      error = .true.
      return
    endif
    !
    call sic_r4(line,5,1,sizex,.true.,error)
    if (error) return
    sizex = abs(sizex)
    !
    call sic_r4(line,5,2,sizey,.true.,error)
    if (error) return
    sizey = abs(sizey)
  endif
  !
  call gtsegm_dir(nomdir,root,sizex,sizey,error)
  if (error) then
    ! This means that the directory already exists, but it has been already
    ! tested, i.e. it should not happen
    call gtv_message(seve%e,rname,'Internal error when creating '//nomdir)
    return
  else
    ! Get the directory just created in 'dir'
    call decode_chemin(nomdir,root,dir,isdir,segm,found)
  endif
  !
  ! go back from whence we came if ERROR or Single-window terminal
  if (error .or. (cw_device%protocol.ne.p_x.and.cw_device%protocol.ne.p_svg)) then
    continue
    !
  else
    ! X protocol: create a top window. Get its characteristics
    call x_display_reset(xdisplay)  ! Default values
    !
    ! /GEOMETRY arguments
    if (sic_present(3,0)) then
      call sic_ch(line,3,1,argx,nc,.true.,error)
      if (error) return
      call sic_ch(line,3,2,argy,nc,.true.,error)
      if (error) return
      call decode_coordinates(xdisplay%geometry,argx,argy,error)
      if (error)  return
    endif
    !
    ! /POSITION arguments
    if (sic_present(4,0)) then
      call sic_ch(line,4,1,argx,nc,.true.,error)
      if (error) return
      call sic_ch(line,4,2,argy,nc,.true.,error)
      if (error) return
      call decode_coordinates(xdisplay%position,argx,argy,error)
      if (error)  return
    endif
    !
    ! /PIXEL (obsolete since 11-oct-2010)
    if (sic_present(2,0)) then
      if (strict2011) then
        sever = seve%e
      else
        sever = seve%w
      endif
      call gtv_message(sever,rname,  &
        'Option /PIXEL is obsolete. Use /GEOMETRY instead.')
      if (strict2011) then
        error = .true.
        return
      endif
      !
      call sic_r4(line,2,1,xdisplay%geometry%x,.true.,error)
      if (error) return
      xdisplay%geometry%unitx = 'p'  ! pixels
      !
      call sic_r4(line,2,2,xdisplay%geometry%y,.true.,error)
      if (error) return
      xdisplay%geometry%unity = 'p'  ! pixels
    endif
    !
    ! Create the initial window for that directory for graphical displays
    call gtv_mkdir_topwindow(dir,xdisplay,error)
    if (error)  return
  endif
  !
end subroutine gtl_mkdir_topdir
!
subroutine gtv_mkdir_topwindow(topdir,xdisplay,error)
  use gtv_interfaces, except_this=>gtv_mkdir_topwindow
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Create the initial window for a top directory for graphical displays
  ! ---
  !  Thread status: safe to call from main thread.
  !---------------------------------------------------------------------
  type(gt_directory), intent(inout) :: topdir    ! The top directory
  type(x_output),     intent(in)    :: xdisplay  ! The new window descriptor
  logical,            intent(inout) :: error     ! Logical error flag
  ! Local
  type(gt_display), pointer :: output
  !
  ! A new window will be attached to the output instances list:
  call get_free_slot_output(output,error)
  if (error)  return
  output%dev => cw_device  ! Point on the shared X-protocol instance
  output%color = .true.
  output%background = output%dev%background  ! Use the device default color
  output%x = xdisplay
  !
  call create_window(output,.false.,topdir,.true.,.true.,error)
  if (error) return
  !
  call sp_gtwindow(output,0.0,topdir%phys_size(1),0.0,topdir%phys_size(2))
  !
end subroutine gtv_mkdir_topwindow
