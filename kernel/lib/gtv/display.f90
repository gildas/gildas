!-----------------------------------------------------------------------
! Support file for command:
!   GTV\DISPLAY
!-----------------------------------------------------------------------
subroutine gtl_display(line,error)
  use gtv_buffers
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_display
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !   GTV\DISPLAY Item
  !
  ! DIRECTORY: Display current directory
  ! POINTER:   Display the pointer chains. For debugging purpose.
  ! SEGMENT:   Segment attributes
  ! TREE:      Directory Tree. If empty, list the content of current
  !            macrosegment. If not, list the content of input
  !            macrosegment (recursive).
  ! WINDOW:    Current Window
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=12) :: to_check,comm
  integer :: ncom,nc
  character(len=gtvpath_length) :: argum
  logical :: found,dir
  type(gt_segment), pointer :: segm
  type(gt_directory), pointer :: arrivee
  ! DISPLAY arguments
  integer, parameter :: nvocab=6
  character(len=12) :: vocab(nvocab)
  data vocab /'DATA','DIRECTORY','POINTER','SEGMENT','TREE','WINDOW'/
  ! DISPLAY POINTER arguments
  integer, parameter :: npointers=2
  character(len=12) :: pointers(npointers)
  data pointers /'TREE','SEGMENT'/
  !
  ! Check if type of variable is known
  call sic_ke (line,0,1,to_check,nc,.true.,error)
  if (error) return
  call sic_ambigs('DISPLAY',to_check,comm,ncom,vocab,nvocab,error)
  if (error)  return
  !
  select case (comm)
  case('DIRECTORY')
    call gt_pwd(error)
    !
  case('POINTER')
    to_check='TREE'
    nc=4
    call sic_ke (line,0,2,to_check,nc,.false.,error)
    if (error) return
    call sic_ambigs('DISPLAY POINTER',to_check,argum,nc,pointers,npointers,  &
    error)
    if (error) return
    !
    ! Header
    write(6,101) 'Name    ','Father   ','Brother   ','Ancestor  ',  &
      'First son  ','Last son  ','First leaf ','Last leaf  '
    if (argum.eq.'TREE') then
      call display_pointer_tree(root,.false.)
    elseif (argum.eq.'SEGMENT') then
      call display_pointer_tree(root,.true.)
    endif
    !
  case('WINDOW')
    call display_window(cw_directory,error)
    if (error)  return
    !
  case('TREE')
    if (sic_present(0,2)) then
      call sic_ch (line,0,2,argum,nc,.true.,error)
      call sic_upper(argum)
      if (error) return
      call decode_chemin(argum,cw_directory,arrivee,dir,segm,found)
      if (.not.found .or. .not.dir) then
        error = .true.
        call gtv_message(seve%e,rname,'No such directory '//argum)
        return
      endif
    else
      arrivee => cw_directory
    endif
    !
    write (*,100)
    call display_tree(arrivee)
    !
  case('SEGMENT')
    if (sic_present(0,2)) then
      call sic_ch (line,0,2,argum,nc,.true.,error)
      call sic_upper (argum)
      if (error) return
      call decode_chemin(argum,cw_directory,arrivee,dir,segm,found)
      if (.not.found) then
        error = .true.
        call gtv_message(seve%e,rname,'No such segment '//argum)
        return
      endif
      if (dir) then
        call display_segment_dir(arrivee,error)
      else
        call display_segment_seg(segm)
      endif
    else
      call display_segment_dir(cw_directory,error)
    endif
    !
  case('DATA')
    call sic_ch (line,0,2,argum,nc,.true.,error)
    call sic_upper (argum)
    if (error) return
    call decode_chemin(argum,cw_directory,arrivee,dir,segm,found)
    if (.not.found) then
      error = .true.
      call gtv_message(seve%e,rname,'No such segment '//argum)
      return
    endif
    if (dir) then
      error = .true.
      call gtv_message(seve%e,rname,'Directories have no data to display')
      return
    endif
    call display_data(segm)
    !
  case default
    call gtv_message(seve%e,rname,  &
      'Internal error: '''//trim(comm)//''' not recognized')
    error = .true.
    return
  end select
  !
  ! --- DISPLAY TREE (1st section: column header) ----------------------
100 format('   Segment                  Dashed   Weight         Colour       Depth  Visib')
  ! --- DISPLAY POINTER TREE|SEGMENT -----------------------------------
101 format(8('|',a12))
  !
end subroutine gtl_display
!
subroutine display_segment_dir(directory,error)
  use gtv_bitmap
  use gtv_buffers
  use gtv_interfaces, except_this=>display_segment_dir
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! display_segment permet de lister:
  !  - les attributs du segment
  !  - les noms et attributs des segments du macrosegment repere par
  !    ad_dir si detail = .true., les noms seulement sinon
  !  - toute l'arborescence depuis le macrosegment repere par ad_dir:
  !    noms de macrosegments et segments
  !---------------------------------------------------------------------
  type(gt_directory), intent(in)    :: directory  ! Directory to analyse
  logical,            intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=segname_length) :: name
  character(len=gtvpath_length) :: path
  integer :: lpath,winnum,numloc,numrecur
  type(gt_segment), pointer :: othersegm
  type(gt_directory), pointer :: otherdir
  !
  ! Name
  write(*,'(A)') ''''//trim(directory%gen%name)//''' attributes:'
  !
  ! Path
  call cree_chemin_dir(directory,path,lpath)
  write(*,'(2X,A,T24,A)') 'Full path:',trim(path)
  !
  ! Type
  write(*,'(2X,A,T24,A)') 'Type:','directory'
  !
  ! Father
  if (.not.associated(directory%father)) then
    name = 'none'
  else
    otherdir => directory%father
    name = otherdir%gen%name
  endif
  write(*,'(2X,A,T24,A)') 'Parent directory:',trim(name)
  !
  ! Grand-father
  if (.not.associated(directory%ancestor)) then
    name = 'none'
  else
    otherdir => directory%ancestor
    name = otherdir%gen%name
  endif
  write(*,'(2X,A,T24,A)')  'Top parent directory:',trim(name)
  !
  ! First brother
  if (.not.associated(directory%brother)) then
    name = 'none'
  else
    otherdir => directory%brother
    name = otherdir%gen%name
  endif
  write(*,'(2X,A,T24,A)') 'Next brother:',trim(name)
  !
  ! Subdirectories
  write(*,'(2X,A)') 'Subdirectories:'
  !
  ! First son
  if (.not.associated(directory%son_first)) then
    name = 'none'
  else
    otherdir => directory%son_first
    name = otherdir%gen%name
  endif
  write(*,'(4X,A,T24,A)') 'First:',trim(name)
  !
  ! Last son
  if (.not.associated(directory%son_last)) then
    name = 'none'
  else
    otherdir => directory%son_last
    name = otherdir%gen%name
  endif
  write(*,'(4X,A,T24,A)') 'Last:',trim(name)
  !
  ! Number of subdirectories
  numloc = 0
  call numberof_dir_local(directory,numloc)
  numrecur = 0
  call numberof_dir_recurs(directory,numrecur)
  write(*,'(4X,A,T24,2(I0,1X,A))')  &
    'Number of:',numloc,'(local), ',numrecur,'(recursive)'
  !
  ! Subdirectories
  write(*,'(2X,A)') 'Segments:'
  !
  ! Next_leaf
  if (.not.associated(directory%leaf_first)) then
    name = 'none'
  else
    othersegm => directory%leaf_first
    name = othersegm%head%gen%name
  endif
  write(*,'(4X,A,T24,A)') 'Next:',trim(name)
  !
  ! Last_leaf
  if (.not.associated(directory%leaf_last)) then
    name = 'none'
  else
    othersegm => directory%leaf_last
    name = othersegm%head%gen%name
  endif
  write(*,'(4X,A,T24,A)') 'Last:',trim(name)
  !
  ! Number of segments
  numloc = 0
  call numberof_seg_local(directory,numloc)
  numrecur = 0
  call numberof_seg_recurs(directory,numrecur)
  write(*,'(4X,A,T24,2(I0,1X,A))')  &
    'Number of:',numloc,'(local), ',numrecur,'(recursive)'
  !
  ! Visibility
  if (directory%gen%visible) then
    write(*,'(2X,A,T24,A)') 'Visible:','yes'
  else
    write(*,'(2X,A,T24,A)') 'Visible:','no'
  endif
  !
  ! Physical size
  write(*,'(2X,A,T24,2F9.3)') 'Physical size:',directory%phys_size
  !
  ! Minmax
  write(*,'(2X,A,T24,4F9.3)') 'Min-max ranges:',directory%gen%minmax
  !
  ! Greg values
  write(*,'(2X,A,T24,4F9.3)') 'User limits:',directory%val_greg%gux1,  &
                                             directory%val_greg%gux2,  &
                                             directory%val_greg%guy1,  &
                                             directory%val_greg%guy2
  !
  ! Windows
  if (directory%x%nbwin.le.0) then
    write(*,'(2X,A,T24,I0,1X,A)')  &
      'Window(s):',directory%x%nbwin,'attached'
    ! genv_array is null when no window attached
  else
    winnum = get_window_usernum(directory,directory%x%curwin,error)
    if (error)  return
    write(*,'(2X,A,T24,I0,1X,A,I0,A)')  &
      'Window(s):',directory%x%nbwin,'attached (active #',winnum,')'
    write(*,'(2X,A,T24,I0)') 'Genv array address:',directory%x%genv_array
  endif
  !
end subroutine display_segment_dir
!
subroutine display_segment_seg(segment)
  use gtv_interfaces, except_this=>display_segment_seg
  use gtv_bitmap
  use gtv_buffers
  use gtv_colors
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! display_segment permet de lister:
  !  - les attributs du segment
  !  - les noms et attributs des segments du macrosegment repere par
  !    ad_dep si detail = .true., les noms seulement sinon
  !  - toute l'arborescence depuis le macrosegment repere par ad_dep:
  !    noms de macrosegments et segments
  !---------------------------------------------------------------------
  type(gt_segment), intent(in) :: segment  ! Segment to analyse
  ! Local
  character(len=segname_length) :: name
  character(len=gtvpath_length) :: path
  integer :: lpath
  type(gt_segment), pointer :: othersegm
  !
  ! Name
  write(*,'(A)') ''''//trim(segment%head%gen%name)//''' attributes:'
  !
  ! Path
  call cree_chemin_seg(segment,path,lpath)
  write(*,'(2X,A,T24,A)') 'Full path:',trim(path)
  !
  ! Type
  write(*,'(2X,A,T24,A)') 'Type:','segment'
  !
  ! Father
  write(*,'(2X,A,T24,3A)') 'Parent directory:',trim(segment%father%gen%name)
  !
  ! Next_leaf
  if (.not.associated(segment%nextseg)) then
    name = 'none'
  else
    othersegm => segment%nextseg
    name = othersegm%head%gen%name
  endif
  write(*,'(2X,A,T24,A)') 'Next leaf:',trim(name)
  !
  ! Attributes
  write(*,'(2X,A)') 'Pen attributes:'
  write(*,'(4X,A,T24,I0)')      'dashed:',segment%head%attr%dash
  write(*,'(4X,A,T24,A)')       'weight:',gtv_penwei_tostr(segment%head%attr%weight)
  write(*,'(4X,A,T24,I0,1X,A)') 'colour:',segment%head%attr%colour,gtv_pencol_id2name(segment%head%attr%colour)
  write(*,'(4X,A,T24,I0)')      'depth:', segment%head%attr%depth
  !
  ! Visibility
  if (segment%head%gen%visible) then
    write(*,'(2X,A,T24,A)') 'Visible:','yes'
  else
    write(*,'(2X,A,T24,A)') 'Visible:','no'
  endif
  !
  ! Minmax
  write(*,'(2X,A,T24,4F9.3)') 'Min-max ranges:',segment%head%gen%minmax
  !
  ! Data
  call display_segment_seg_data(segment)
  !
end subroutine display_segment_seg
!
subroutine display_segment_seg_data(segment)
  use gtv_buffers
  use gtv_interfaces, except_this=>display_segment_seg_data
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(gt_segment) :: segment  ! Segment data
  ! Local
  integer(kind=4) :: nbpoints,nbpol,nbhfpol,nbvfpol,nbpenlut,nblut,nbima
  logical :: found
  type(gt_segment_data), pointer :: segdata
  !
  segdata => segment%data
  found = .false.
  nbpoints = 0
  nbpol = 0
  nbhfpol = 0
  nbvfpol = 0
  nbpenlut = 0
  nblut = 0
  nbima = 0
  do while (associated(segdata))
    select case (segdata%kind)
    case (seg_points)
      found = .true.
      nbpoints = nbpoints+1
    case (seg_poly)
      found = .true.
      nbpol = nbpol+1
    case (seg_hfpoly)
      found = .true.
      nbhfpol = nbhfpol+1
    case (seg_vfpoly)
      found = .true.
      nbvfpol = nbvfpol+1
    case (seg_pencol)
      found = .true.
      nbpenlut = nbpenlut+1
    case (seg_lutcol)
      found = .true.
      nblut = nblut+1
    case (seg_image)
      if (segdata%image%isrgb) then
        call display_segment_seg_rgb(segdata%image)
      else
        call display_segment_seg_ind(segdata%image)
      endif
      found = .true.
      nbima = nbima+1
    end select
    !
    segdata => segdata%nextdata
  enddo
  !
  if (found) then
    write(*,'(2X,A)')  'Data (number of):'
    write(*,'(4X,A,T24,I0)') 'list of points:',nbpoints
    write(*,'(4X,A,T24,I0)') 'polylines:',     nbpol
    write(*,'(4X,A,T24,I0)') 'vert. polygons:',nbvfpol
    write(*,'(4X,A,T24,I0)') 'hori. polygons:',nbhfpol
    write(*,'(4X,A,T24,I0)') 'pen LUTs:',      nbpenlut
    write(*,'(4X,A,T24,I0)') 'image LUTs:',    nblut
    write(*,'(4X,A,T24,I0)') 'images:',        nbima
  else
    write(*,'(2X,A)')  'No data found'
  endif
  !
end subroutine display_segment_seg_data
!
subroutine display_segment_seg_ind(segima)
  use gtv_interfaces, except_this=>display_segment_seg_ind
  use gtv_bitmap
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_image), intent(in) :: segima  ! Image descriptor
  ! Local
  character(len=12) :: name
  !
  write(*,'(2X,A)')  'Contains an indexed image:'
  !
  ! Image size
  write(*,'(4X,A,T24,I0,1X,I0)') 'Dimensions:',segima%r%taille(1),segima%r%taille(2)
  !
  ! Image position in page
  write(*,'(4X,A,T24,4F8.3)') 'Position:',segima%position
  !
  ! Image scaling (code)
  write(*,'(4X,A,T24,I2)') 'Scaling (code):',segima%scaling
  !
  ! Image scaling (actual extrema)
  write(*,'(4X,A,T24,2(1PG9.3))') 'Scaling (extrema):',segima%r%extrema
  !
  ! Image scaling (cuts)
  write(*,'(4X,A,T24,2(1PG9.3))') 'Scaling (cuts):',segima%r%cuts
  !
  ! Image blanks (Bval, Eval, flag for using background color, ignored?)
  write(*,'(4X,A,T24,3F9.2)') 'Blanking:',segima%r%blank
  !
  ! Associated LUT in the metacode, if any
  if (.not.associated(segima%lut)) then
    name = 'none'
  else
    name = 'custom'
  endif
  write(*,'(4X,A,T24,A)') 'LUT:',trim(name)
  !
end subroutine display_segment_seg_ind
!
subroutine display_segment_seg_rgb(segima)
  use gtv_interfaces, except_this=>display_segment_seg_rgb
  use gtv_bitmap
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_image), intent(in) :: segima  ! Image descriptor
  !
  write(*,'(2X,A)')  'Contains an RGB image:'
  !
  ! Images size
  write(*,'(4X,A,T24,I0,1X,I0)') 'R dimensions:',segima%r%taille(1),segima%r%taille(2)
  write(*,'(4X,A,T24,I0,1X,I0)') 'G dimensions:',segima%g%taille(1),segima%g%taille(2)
  write(*,'(4X,A,T24,I0,1X,I0)') 'B dimensions:',segima%b%taille(1),segima%b%taille(2)
  !
  ! Image position in page
  write(*,'(4X,A,T24,4F8.3)') 'Position:',segima%position
  !
  ! Image scaling (code)
  write(*,'(4X,A,T24,I2)') 'Scaling (code):',segima%scaling
  if (segima%scaling.eq.scale_lup) then
    write(*,'(4X,A,T24,I2)') 'Scaling (beta factor):',segima%scaling_lup_b
  endif
  !
  ! Image scaling (actual extrema)
  write(*,'(4X,A,T24,2(1PG9.3))') 'R scaling (extrema):',segima%r%extrema
  write(*,'(4X,A,T24,2(1PG9.3))') 'G scaling (extrema):',segima%g%extrema
  write(*,'(4X,A,T24,2(1PG9.3))') 'B scaling (extrema):',segima%b%extrema
  !
  ! Image scaling (cuts)
  write(*,'(4X,A,T24,2(1PG9.3))') 'R scaling (cuts):',segima%r%cuts
  write(*,'(4X,A,T24,2(1PG9.3))') 'G scaling (cuts):',segima%g%cuts
  write(*,'(4X,A,T24,2(1PG9.3))') 'B scaling (cuts):',segima%b%cuts
  !
  ! Image blanks (Bval, Eval, flag for using background color, ignored?)
  write(*,'(4X,A,T24,3F9.2)') 'R blanking:',segima%r%blank
  write(*,'(4X,A,T24,3F9.2)') 'G blanking:',segima%r%blank
  write(*,'(4X,A,T24,3F9.2)') 'B blanking:',segima%r%blank
  !
end subroutine display_segment_seg_rgb
!
subroutine display_data(segment)
  use gtv_buffers
  use gtv_interfaces, except_this=>display_data
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_segment) :: segment  ! Segment data
  ! Local
  logical :: found
  type(gt_segment_data), pointer :: segdata
  !
  segdata => segment%data
  found = .false.
  do while (associated(segdata))
    found = .true.
    select case (segdata%kind)
    case (seg_points)
      write(*,100)  'Found a list of points:'
      call display_data_poly(segdata%poly)
    case (seg_poly)
      write(*,100)  'Found a polyline:'
      call display_data_poly(segdata%poly)
    case (seg_hfpoly)
      write(*,100)  'Found an horizontal filled polygon:'
      call display_data_poly(segdata%poly)
    case (seg_vfpoly)
      write(*,100)  'Found a vertical filled polygon:'
      call display_data_poly(segdata%poly)
    case (seg_pencol)
      write(*,100)  'Found a pen LUT:'
      call display_data_lut(segdata%lut)
    case (seg_lutcol)
      write(*,100)  'Found an image LUT:'
      call display_data_lut(segdata%lut)
    case (seg_image)
      if (segdata%image%isrgb) then
        write(*,100)  'Found a rgb image:'
        call display_data_rgbima(segdata%image)
      else
        write(*,100)  'Found an indexed image:'
        call display_data_indima(segdata%image)
      endif
    end select
    !
    segdata => segdata%nextdata
  enddo
  !
  if (.not.found) then
    write(*,100)  'No data found'
  endif
  !
100 format(a)
end subroutine display_data
!
subroutine display_data_poly(poly)
  use gtv_interfaces, except_this=>display_data_poly
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_polyline) :: poly  !
  ! Local
  integer(kind=size_length) :: n
  character(len=6) :: name
  !
  ! Associated PENLUT in the metacode, if any
  if (associated(poly%penlut)) then
    name = 'custom'
  else
    name = 'none'
  endif
  write(*,'(2X,A,1X,A)') 'PENLUT:',trim(name)
  !
  n = poly%n
  write(*,'(2X,A,I0,A)')  'X (',n,' values):'
  call r4_type(n,poly%x)
  write(*,'(2X,A,I0,A)')  'Y (',n,' values):'
  call r4_type(n,poly%y)
  !
end subroutine display_data_poly
!
subroutine display_data_lut(lut)
  use gtv_interfaces, except_this=>display_data_lut
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_lut) :: lut  !
  ! Local
  integer(kind=size_length) :: n
  !
  n = lut%size
  write(*,'(2X,A,I0,A)')  'R (',n,' values):'
  call r4_type(n,lut%r)
  write(*,'(2X,A,I0,A)')  'G (',n,' values):'
  call r4_type(n,lut%g)
  write(*,'(2X,A,I0,A)')  'B (',n,' values):'
  call r4_type(n,lut%b)
  write(*,'(2X,A,I0,A)')  'H (',n,' values):'
  call r4_type(n,lut%hue)
  write(*,'(2X,A,I0,A)')  'S (',n,' values):'
  call r4_type(n,lut%sat)
  write(*,'(2X,A,I0,A)')  'V (',n,' values):'
  call r4_type(n,lut%val)
end subroutine display_data_lut
!
subroutine display_data_rgbima(ima)
  use gtv_interfaces, except_this=>display_data_rgbima
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_image) :: ima  !
  !
  write(*,'(2X,A)')  '(not yet implemented)'
end subroutine display_data_rgbima
!
subroutine display_data_indima(ima)
  use gtv_interfaces, except_this=>display_data_indima
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(gt_image) :: ima  !
  !
  write(*,'(2X,A)')  '(not yet implemented)'
end subroutine display_data_indima
!
recursive subroutine display_tree(start)
  use gtv_buffers
  use gtv_interfaces, except_this=>display_tree
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! display_tree parcourt l'arborescence inferieure au macrosegment
  ! repere par 'start'.
  !---------------------------------------------------------------------
  type(gt_directory) :: start  ! Starting directory
  ! Local
  type(gt_directory), pointer :: son
  !
  ! Input directory
  call display_tree_1dir(start)
  !
  ! Subdirectories
  son => start%son_first
  do while (associated(son))
    call display_tree(son)
    son => son%brother
  enddo
  !
end subroutine display_tree
!
subroutine display_tree_1dir(dir)
  use gtv_buffers
  use gtv_interfaces, except_this=>display_tree_1dir
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Depuis le macrosegment courant repere par ad_dep, display_tree_1dir
  ! renvoie le nom des macrosegments et segments et les valeurs des
  ! attributs respectifs.
  !---------------------------------------------------------------------
  type(gt_directory) :: dir  ! Directory to analyze
  ! Local
  integer :: lpath
  character(len=gtvpath_length) :: path
  type(gt_segment), pointer :: seg
  !
  call cree_chemin_dir(dir,path,lpath)
  write(6,102) path,dir%gen%visible
  !
  ! Parcours des feuilles et lecture de leur nom et de leurs attributs
  seg => dir%leaf_first
  do while (associated(seg))
    write(6,101) seg%head%gen%name, &
                 seg%head%attr%dash, &
                 gtv_penwei_tostr(seg%head%attr%weight),  &
                 seg%head%attr%colour,gtv_pencol_id2name(seg%head%attr%colour),  &
                 seg%head%attr%depth,  &
                 seg%head%gen%visible
    !
    seg => seg%nextseg
  enddo
  !
  return
  !
  ! --- DISPLAY TREE (2nd section: column content) ---------------------
102 format(1x,a70,                                                          '|',2x,l1)
101 format(4x,a23,'|',2x,i2,2x,'|',1x,a,1x,'|',1x,i3,1x,a12,1x,'|',1x,i3,2x,'|',2x,l1)
end subroutine display_tree_1dir
!
recursive subroutine display_pointer_tree(dir,verbose)
  use gildas_def
  use gtv_buffers
  use gtv_interfaces, except_this=>display_pointer_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! GTVIRT  Support routine for debugging
  !   Parcours les segments et affiche les valeurs des pointeurs
  ! last_leaf, window etc...
  !---------------------------------------------------------------------
  type(gt_directory)  :: dir      ! Directory to print from
  logical, intent(in) :: verbose  ! Display also the segments?
  ! Local
  type(gt_directory), pointer :: son
  !
  ! Root
  call display_pointer_tree_1dir(dir,verbose)
  !
  ! Subdirectories
  son => dir%son_first
  do while (associated(son))
    call display_pointer_tree(son,verbose)
    son => son%brother
  enddo
  !
end subroutine display_pointer_tree
!
subroutine display_pointer_tree_1dir(dir,verbose)
  use gtv_interfaces, except_this=>display_pointer_tree_1dir
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(gt_directory)  :: dir      ! Directory to display
  logical, intent(in) :: verbose  ! Display also the segments?
  ! Local
  character(len=12) :: name,ancestor,father,brother,sonf,sonl,leaff,leafl
  type(gt_segment), pointer :: segm
  !
  name = dir%gen%name
  if (associated(dir%ancestor)) then
    ancestor = dir%ancestor%gen%name
  else
    ancestor = '-'
  endif
  if (associated(dir%father)) then
    father = dir%father%gen%name
  else
    father = '-'
  endif
  if (associated(dir%brother)) then
    brother = dir%brother%gen%name
  else
    brother = '-'
  endif
  if (associated(dir%son_first)) then
    sonf = dir%son_first%gen%name
  else
    sonf = '-'
  endif
  if (associated(dir%son_last)) then
    sonl = dir%son_last%gen%name
  else
    sonl = '-'
  endif
  if (associated(dir%leaf_first)) then
    leaff = dir%leaf_first%head%gen%name
  else
    leaff = '-'
  endif
  if (associated(dir%leaf_last)) then
    leafl = dir%leaf_last%head%gen%name
  else
    leafl = '-'
  endif
  write (6,100) name,father,brother,ancestor,sonf,sonl,leaff,leafl
  !
  if (verbose) then
    segm => dir%leaf_first
    do while(associated(segm))
      call display_pointer_tree_1seg(segm)
      segm => segm%nextseg
    enddo
  endif
  !
100 format(8('|',a12))
end subroutine display_pointer_tree_1dir
!
subroutine display_pointer_tree_1seg(segm)
  use gtv_interfaces, except_this=>display_pointer_tree_1seg
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(gt_segment), intent(in) :: segm  ! Segment to display
  ! Local
  character(len=12) :: name,ancestor,father,brother,sonf,sonl,leaff,leafl
  !
  name = segm%head%gen%name
  if (associated(segm%father)) then
    father = segm%father%gen%name
  else
    father = '-'
  endif
  if (associated(segm%nextseg)) then
    brother = segm%nextseg%head%gen%name
  else
    brother = '-'
  endif
  !
  ! These ones have no meaning:
  ancestor = ' '
  sonf = ' '
  sonl = ' '
  leaff = ' '
  leafl = ' '
  !
  write (6,100) name,father,brother,ancestor,sonf,sonl,leaff,leafl
  !
100 format(8('|',a12))
end subroutine display_pointer_tree_1seg
!
subroutine gt_pwd(error)
  use gtv_buffers
  use gtv_interfaces, except_this=>gt_pwd
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! gt_pwd permet de se situer dans l'arborescence des macrosegments et
  ! affiche un chemin absolu.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  integer :: nl,winnum
  character(len=message_length) :: message
  character(len=4) :: wind
  character(len=gtvpath_length) :: dirname
  !
  call cree_chemin_dir(cw_directory,dirname,nl)
  !
  if (cw_directory%x%nbwin.gt.0) then
    winnum = get_window_usernum(cw_directory,cw_directory%x%curwin,error)
    if (error)  return
    !
    write(wind,'(I0)') winnum
    message = trim(dirname)//' Window #'//wind
  else
    message = trim(dirname)//' (no window)'
  endif
  call gtv_message(seve%r,'GT_PWD',message)
  !
end subroutine gt_pwd
!
subroutine display_window(dir,error)
  use gildas_def
  use gbl_message
  use gtv_buffers
  use gtv_interfaces, except_this=>display_window
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Display the attributes of the Current Working Device (Window)
  !---------------------------------------------------------------------
  type(gt_directory)     :: dir    ! Directory to look at
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=64) :: nom_dir
  integer :: lnom,px1,px2,py1,py2,usernum
  !
  call cree_chemin_dir(dir,nom_dir,lnom)
  if (dir%x%nbwin.le.0) then
    call gtv_message(seve%w,rname,'Directory '//trim(nom_dir)//  &
      ' has no window attached.')
    return
  endif
  !
  ! Window identifier
  usernum = get_window_usernum(cw_directory,cw_directory%x%curwin,error)
  if (error)  return
  write(*,'(A,I0,A)') 'Window #',usernum,' attributes:'
  !
  ! Internal number (C and continuous numbering)
  write(*,'(2X,A,T24,I0)') 'Internal number:',cw_directory%x%curwin
  !
  ! Attached to which directory?
  write(*,'(2X,A,T24,A,A,I0,A)') 'Viewing directory:',trim(nom_dir),  &
    ' (',dir%x%nbwin,' window(s) attached)'
  !
  ! Graphical environment address
  write(*,'(2X,A,T24,I0)') 'Address:',dir%x%genv
  !
  ! Size in pixels
  call get_win_pixel_info(dir%x%genv,px1,py1,px2,py2)
  write(*,'(2X,A,T24,2(A,I0,A1,I0))') 'XY pixel ranges:',  &
    'X ',px1,':',px2,', Y ',py1,':',py2
  !
end subroutine display_window
!
subroutine numberof_dir_local(parent,nb)
  use gtv_interfaces, except_this=>numberof_dir_local
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Increment 'nb' with the number of subdirectories which are directly
  ! attached to the parent directory
  !---------------------------------------------------------------------
  type(gt_directory), intent(in)    :: parent  ! Parent directory
  integer(kind=4),    intent(inout) :: nb      ! Number
  ! Local
  type(gt_directory), pointer :: son
  !
  son => parent%son_first
  do while (associated(son))
    nb = nb+1
    son => son%brother
  enddo
  !
end subroutine numberof_dir_local
!
recursive subroutine numberof_dir_recurs(parent,nb)
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Increment 'nb' with the number of subdirectories which are directly
  ! or indirectly (i.e. recursively) attached to the parent directory.
  !---------------------------------------------------------------------
  type(gt_directory), intent(in)    :: parent  ! Parent directory
  integer(kind=4),    intent(inout) :: nb      ! Number
  ! Local
  type(gt_directory), pointer :: son
  !
  son => parent%son_first
  do while (associated(son))
    nb = nb+1
    call numberof_dir_recurs(son,nb)
    son => son%brother
  enddo
  !
end subroutine numberof_dir_recurs
!
subroutine numberof_seg_local(parent,nb)
  use gtv_interfaces, except_this=>numberof_seg_local
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Increment 'nb' with the number of segments which are directly
  ! attached to the parent directory
  !---------------------------------------------------------------------
  type(gt_directory), intent(in)    :: parent  ! Parent directory
  integer(kind=4),    intent(inout) :: nb      ! Number
  ! Local
  type(gt_segment), pointer :: segm
  !
  segm => parent%leaf_first
  do while (associated(segm))
    nb = nb+1
    segm => segm%nextseg
  enddo
  !
end subroutine numberof_seg_local
!
recursive subroutine numberof_seg_recurs(parent,nb)
  use gtv_interfaces, except_this=>numberof_seg_recurs
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Increment 'nb' with the number of subdirectories which are directly
  ! or indirectly (i.e. recursively) attached to the parent directory.
  !---------------------------------------------------------------------
  type(gt_directory), intent(in)    :: parent  ! Parent directory
  integer(kind=4),    intent(inout) :: nb      ! Number
  ! Local
  type(gt_directory), pointer :: son
  !
  ! 1) Local segments
  call numberof_seg_local(parent,nb)
  !
  ! 2) Segments in subdirectories
  son => parent%son_first
  do while (associated(son))
    call numberof_seg_recurs(son,nb)
    son => son%brother
  enddo
  !
end subroutine numberof_seg_recurs
