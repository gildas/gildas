!-----------------------------------------------------------------------
!  Find below the generic routines which can work on any kind of output
! and any instance, e.g. X-Windows, PS hardcopy, or SVG "interactive"
! device.
!-----------------------------------------------------------------------
module gtview_modes
  !---------------------------------------------------------------------
  ! Support module for GTVIEW. Store here variables shared by routines
  ! in this file and only this file.
  !---------------------------------------------------------------------
  ! GTVIEW modes:
  integer, parameter :: rewind =  0
  integer, parameter :: append = -1
  integer, parameter :: update = -2
  integer, parameter :: purge  = -3  ! Obsolete, to be removed
  integer, parameter :: delete = -4  ! Obsolete, to be removed
  integer, parameter :: zap    = -5  ! Obsolete, to be removed
  integer, parameter :: limits = -6  ! Obsolete, to be removed
  integer, parameter :: color  = -7
  !
end module gtview_modes
!
subroutine gtview(acha)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtview
  use gtv_protocol
  use gtv_graphic
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Plot to the interactive graphic device
  !
  ! 'Append'   Plot from last vector drawn
  ! 'Rewind'   Clear the screen, and plot from first vector
  ! 'Update'   Update the screens (Clear + Redraw)
  ! 'Color'    Update the screens (Redraw only)
  !
  ! Does not modify the virtuel pen position and the immediate pen
  ! position. P. Valiron
  !
  ! Modifiee pour parcourir une arborescence par R.Gras. (Octobre 1992)
  ! Modifiee par S.Guilloteau pour tracer les images AVANT les
  ! segments graphiques en cas de Rewind. Bigrement modifiee par GD, pendant
  ! qu'on y est...
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: acha  ! Key character of the required mode
  ! Local
  character(len=*), parameter :: rname='GTVIEW'
  integer :: sever
  character(len=1) :: opt
  logical :: error
  !
  opt = acha(1:1)
  call sic_upper(opt)
  !
  if (.not.awake .or. error_condition) return
  !
  if (opt.eq.'S') then
    if (strict2011) then
      sever = seve%e
    else
      sever = seve%w
    endif
    call gtv_message(sever,rname,'Sleep mode is obsolete. GTVirt is always awake.')
    if (strict2011)  return
    !
    return
  elseif (opt.eq.'W') then
    if (strict2011) then
      sever = seve%e
    else
      sever = seve%w
    endif
    call gtv_message(sever,rname,'Wake-Up mode is obsolete. GTVirt is always awake.')
    if (strict2011)  return
    !
    opt = 'A'
  endif
  !
  ! Draw if needed
  if (cw_output%dev%protocol.eq.p_null)  return
  !
  error = .false.
  call gtview_sub(cw_output,opt,error)
  !
end subroutine gtview
!
subroutine gtview_sub(output,chara,error)
  use gtv_types
  use gtv_buffers
  use gtv_graphic
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtview_sub
  use gtv_tree
  use gtv_types
  use gtview_modes
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Plot to the output graphic device given as argument
  !
  ! 'Append'   Plot from last vector drawn
  ! 'Rewind'   Clear the screen, and plot from first vector
  ! 'Update'   Update the screens (Clear + Redraw)
  ! 'Color'    Update the screens (Redraw only)
  !
  ! Does not modify the virtuel pen position and the immediate pen
  ! position. P. Valiron
  ! Modifiee pour parcourir une arborescence par R.Gras. (Octobre 1992)
  !---------------------------------------------------------------------
  type(gt_display)                :: output  ! Output instance
  character(len=*), intent(in)    :: chara   ! Key character of the required mode
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTVIEW'
  character(len=1) :: opt
  !
  opt = chara(1:1)
  call sic_upper(opt)
  !
  ! Do NOT close the current segment. It should have been explicitely
  ! closed by the programmer. Furthermore, 'gtsegm_close' implicitely
  ! appends the segment being closed.
  ! Only flush the internal buffers, in case we will have to plot
  ! a segment which is not yet closed (this is valid since it can be under
  ! construction but we want to see it during its build).
  call gtsegm_flush(error)
  if (error)  return
  !
  select case (opt)
  case ('R')
    ! --- Rewind ---
    ! Called after a change of segment properties, will redraw the CURRENT
    ! window ONLY, at all levels. Obsolete at MODE=0 (Rewind), see Update below.
    call gtview_rewind(output,cw_directory)  ! ZZZ Global dependency here!
    !
  case ('A')
    ! --- Append ---
    ! Called after a creation of new segment.
    ! Explore all the arborescence, and draws on concerned windows (the windows at
    ! directory levels BELOW the one at which the append is done)
    !
    ! Nothing to do if there is no segment opened (e.g. a directory
    ! has just been created)
    if (.not.associated(co_segment))  return
    call gtview_append(co_segment,error)
    !
  case ('U')
    ! --- Update ---
    ! Called after a change of segment properties, will explore all the
    ! arborescence, and redraw concerned windows (the windows at a directory
    ! level BELOW the one at which the change was made). Update all the
    ! windows: start from root
    call gtview_update(root,error)
    !
  case ('C')
    ! --- Update ---
    ! Called after a change of LUT, will explore all the arborescence, and
    ! redraw concerned windows (the windows at a directory level BELOW the
    ! one at which the change was made). Update all the windows: start from
    ! root
    call gtview_color(root,error)
    !
  case default
    call gtv_message(seve%e,rname,'Unknown code '//opt)
    return
    !
  end select
  !
end subroutine gtview_sub
!
subroutine gtview_rewind(output,dir)
  use gtv_interfaces, except_this=>gtview_rewind
  use gtv_protocol
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! Deuxieme mode de parcours de l'arbre, correspond a gtview('Rewind')
  ! Update is made in the output device given in the calling sequence
  ! SOLELY, for all the depth in correct order.
  !---------------------------------------------------------------------
  type(gt_display)   :: output  ! Output instance
  type(gt_directory) :: dir     ! Starting directory
  !
  if (output%dev%protocol.eq.p_x) then
    if (x_refresh_genv(dir,rewind,output%x%graph_env)) then
      call gtview_work_recursdir(output,dir,rewind)
    endif
  else
    call gtview_work_recursdir(output,dir,rewind)
  endif
  !
end subroutine gtview_rewind
!
subroutine gtview_update(dir_start,error)
  use gtv_interfaces, except_this=>gtview_update
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! gtview('Update')
  ! Redraw completely all the windows attached to dir_start and its
  ! subdirectories.
  !---------------------------------------------------------------------
  type(gt_directory)                :: dir_start  ! Starting directory
  logical,            intent(inout) :: error      ! Logical error flag
  !
  call win_gtview_work_allwin_recurs(dir_start,update,error)
  !
end subroutine gtview_update
!
subroutine gtview_color(dir_start,error)
  use gtv_interfaces, except_this=>gtview_color
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! gtview('Update')
  ! Redraw completely all the windows attached to dir_start and its
  ! subdirectories.
  !---------------------------------------------------------------------
  type(gt_directory)                :: dir_start  ! Starting directory
  logical,            intent(inout) :: error      ! Logical error flag
  !
  call win_gtview_work_allwin_recurs(dir_start,color,error)
  !
end subroutine gtview_color
!
subroutine gtview_append(seg_start,error)
  use gtv_interfaces, except_this=>gtview_append
  use gildas_def
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! gtview('Append')
  !---------------------------------------------------------------------
  type(gt_segment)                :: seg_start  ! Starting segment
  logical,          intent(inout) :: error      ! Logical error flag
  !
  call win_gtview_work_allwin_updir(seg_start,append,error)
  !
end subroutine gtview_append
!
recursive subroutine gtview_work_recursdir(out,dir,mode)
  use gtv_buffers
  use gtv_interfaces, except_this=>gtview_work_recursdir
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  !  Work (draw, update, clean...) in the input 'gt_display' instance,
  ! reading the tree 'dir', and recursively in its subdirectories.
  !---------------------------------------------------------------------
  type(gt_display),   intent(inout) :: out   ! Input gt_display
  type(gt_directory)                :: dir   ! Starting directory
  integer(kind=4),    intent(in)    :: mode  !
  ! Local
  type(gt_directory), pointer :: son
  !
  ! 1) Segments in subdirectories first
  son => dir%son_first
  do while (associated(son))
    call gtview_work_recursdir(out,son,mode)
    son => son%brother
  enddo
  !
  ! 2) Segments in current directory last
  call gtview_work_1dir(out,dir,mode)
  !
end subroutine gtview_work_recursdir
!
subroutine gtview_work_1dir(out,dir,mode)
  use gtv_interfaces, except_this=>gtview_work_1dir
  use gtv_buffers
  use gtv_graphic
  use gtv_protocol
  use gtv_segatt
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  !  Work (draw, update, clean...) in the input 'gt_display' instance,
  ! reading the tree @ ad_start (only). No recursion here!
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: out   ! Input gt_display
  type(gt_directory)              :: dir   ! Starting directory
  integer,          intent(in)    :: mode  !
  ! Local
  integer(kind=4) :: vis_mode
  integer :: idep
  !
  if (mode.eq.update .or. mode.eq.rewind .or. mode.eq.color) then
    ! We need to traverse the tree but layer by layer
    do idep=cdepth,1,-1
      vis_mode = idep
      if (.not.vdepth(vis_mode))  cycle  ! Nothing to draw at this depth
      !
      call affich_dir(out,dir,vis_mode)
    enddo
    !
  else
    ! Other modes
    call affich_dir(out,dir,mode)
  endif
  !
  ! We may need to flush the input X window (if any)
  if (out%dev%protocol.eq.p_x .and. out%x%graph_env.ne.0)  &
    call x_flush_points(out%x%graph_env)
  !
end subroutine gtview_work_1dir
!
subroutine gtview_work_1seg(out,leaf,mode)
  use gtv_interfaces, except_this=>gtview_work_1seg
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Work (draw, update, clean...) in the input 'gt_display' instance,
  ! reading the input elemental segment only.
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: out   ! Input gt_display
  type(gt_segment)                :: leaf  ! Leaf instance
  integer,          intent(in)    :: mode  !
  ! Local
  logical :: plot_leaf
  !
  ! Check if it should be plotted
  call lect_descr(out,leaf,mode,plot_leaf)
  !
  if (plot_leaf)  &
    call traite_codeop(out,leaf)
  !
  ! We may need to flush the input X window (if any)
  if (out%dev%protocol.eq.p_x .and. out%x%graph_env.ne.0)  &
    call x_flush_points(out%x%graph_env)
  !
end subroutine gtview_work_1seg
!
subroutine affich_dir(out,dir,mode)
  use gtview_modes
  use gtv_buffers
  use gtv_interfaces, except_this=>affich_dir
  use gtv_protocol
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Procedure de parcours des feuilles d'un repertoire.
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: out   ! Input gt_display
  type(gt_directory)              :: dir   ! Starting leaf
  integer,          intent(in)    :: mode  !
  ! Local
  logical :: plot_leaf
  type(gt_segment), pointer :: leaf
  !
  leaf => dir%leaf_first
  ! Loop on all leaves
  do while (associated(leaf))
    ! Check if it should be plotted
    call lect_descr(out,leaf,mode,plot_leaf)
    if (plot_leaf)  &
      call traite_codeop(out,leaf)
    !
    leaf => leaf%nextseg
  enddo
  !
end subroutine affich_dir
!
subroutine lect_descr(output,segm,mode,plot_segment)
  use gtv_buffers
  use gtv_graphic
  use gtv_interfaces, except_this=>lect_descr
  use gtv_protocol
  use gtv_tree
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! Read the input segment and check if we have to plot it or not.
  !---------------------------------------------------------------------
  type(gt_display)     :: output        ! Output instance
  type(gt_segment)     :: segm          ! Address of the descriptor to analyse
  integer, intent(in)  :: mode          ! Plot mode
  logical, intent(out) :: plot_segment  ! Need to plot the segment?
  !
  if (.not.segm%head%gen%visible) then
    plot_segment = .false.
    !
  elseif (segm%head%gen%minmax(1).ge.output%gx2 .or.  &
          segm%head%gen%minmax(2).le.output%gx1 .or.  &
          segm%head%gen%minmax(3).ge.output%gy2 .or.  &
          segm%head%gen%minmax(4).le.output%gy1) then
    ! Segment is out of the plotting area
    plot_segment = .false.
    !
  elseif (mode.gt.0) then
    ! 'mode' is a depth
    plot_segment = segm%head%attr%depth.eq.mode
    !
  elseif (mode.eq.rewind) then
    plot_segment = .true.
    !
  else
    ! Can this happen?
    plot_segment = .true.
  endif
  !
  if (plot_segment) then
    ! Travaillons avec tous les attributs, ne nous contentons pas de la couleur
    call set_attributs(output,segm%head%attr)
    !
    ! Selon les protocoles, le travail est different.
    ! Il y a les protocoles intelligent capables de faire du dash et des
    ! largeurs de trait et les autres. Pour les premiers, les parametres
    ! de dash et largeur ont deja ete fixe au moment du set_attributs(...)
    ! une ligne simple suffira dans tous les cas.
    !
    if (output%dev%protocol.eq.p_x) then
      ! Nothing to do, dash problems solved in set_dash
    elseif (output%dev%protocol.eq.p_svg) then
      ! output%dev%hardw_line_weig&dash = svg_hard(output%iweigh,output%idashe)
    elseif (output%dev%protocol.eq.p_postscript) then
      call ps_hard(output)
    elseif (output%dev%protocol.eq.p_png) then
      call png_hard(output)
    else
      output%dev%hardw_line_weig = output%iweigh.le.graisse(1)
      output%dev%hardw_line_dash = output%idashe.eq.1
    endif
    !
  endif
  !
end subroutine lect_descr
!
subroutine traite_codeop(out,leaf)
  use gtv_interfaces, except_this=>traite_codeop
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !   Elemental procedure which draws a graphic segment into the
  ! 'output' device. Read from the standard GTV buffer ('cw_buffer').
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: out   ! Input gt_display
  type(gt_segment)                :: leaf  ! Segment instance
  ! Local
  type(gt_segment_data), pointer :: segdata
  !
  call gti_trace(out,"Here starts segment "//leaf%head%gen%name)
  !
  ! Follow the linked list of data
  segdata => leaf%data
  do while (associated(segdata))
    select case (segdata%kind)
    case (seg_points)
      call gti_points(out,segdata%poly)
    case (seg_poly)
      call gt_polyl(out,segdata%poly)
    case (seg_hfpoly)
      call gti_fillpoly(out,segdata%poly,.false.)
    case (seg_vfpoly)
      call gti_fillpoly(out,segdata%poly,.true.)
    case (seg_pencol)
      call gti_penlut(out,segdata%lut)
    case (seg_lutcol)
      call gti_lut(out,segdata%lut)
    case (seg_image)
      call gti_image(out,segdata%image)
    end select
    !
    segdata => segdata%nextdata
  enddo
  !
  call gti_trace(out,"Here ends segment "//leaf%head%gen%name)
  !
end subroutine traite_codeop
!
!-----------------------------------------------------------------------
!  Find below the specific routines which can work on X-Windows
!-----------------------------------------------------------------------
subroutine win_gtview_work_allwin_updir(segm,mode,error)
  use gtv_interfaces, except_this=>win_gtview_work_allwin_updir
  use gtv_buffers
  use gtv_protocol
  use gtv_types
  use gtview_modes
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - all windows which see 'segm'
  !  - a single leaf 'segm'
  !---------------------------------------------------------------------
  type(gt_segment)                :: segm   ! Leaf to work on
  integer(kind=4),  intent(in)    :: mode   ! Working mode
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  type(gt_directory), pointer :: dir
  !
  dir => segm%father
  !
  do while (associated(dir))
    call win_gtview_work_allwin_1seg(dir,segm,mode,error)
    if (error)  return
    !
    dir => dir%father  ! Next parent
  enddo
  !
end subroutine win_gtview_work_allwin_updir
!
subroutine win_gtview_work_allwin_1seg(dir_win,segm,mode,error)
  use gbl_message
  use gtv_buffers
  use gtv_interfaces, except_this=>win_gtview_work_allwin_1seg
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - all windows of 1 directory (and only this directory)
  !  - 1 segment (not a directory)
  !---------------------------------------------------------------------
  type(gt_directory)                :: dir_win  ! The directory to which the windows are attached
  type(gt_segment)                  :: segm     ! The segment we work on
  integer(kind=4),    intent(in)    :: mode     ! What to do?
  logical,            intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='WIN_GTVIEW_WORK_ALLWIN_1SEG'
  integer*4 :: iwin
  logical :: found
  type(gt_display), pointer :: output
  integer(kind=address_length) :: allgenv(0:mwindows-1)
  character(len=message_length) :: mess
  !
  if (dir_win%x%nbwin.le.0)  return
  !
  ! This subroutine will draw 1 segment in several windows (this is the purpose
  ! of the 2nd loop after). BUT, looping from 0 to dir_win%x%nbwin-1 can be a
  ! problem since a window can disappear (typically, the lens) while we were
  ! drawing in another one. So, let's protect this.
  ! 1) Keep memory of all the genvs when entering. This is safer: the other
  !    arrays are compressed when destroying a window, we could be lost.
  do iwin=0,dir_win%x%nbwin-1
    allgenv(iwin) = c_get_win_genv(dir_win%x%genv_array,iwin)
  enddo
  !
  ! 2) Then, draw in all known genvs. Since a window/genv can be destroyed
  !    at any time, 'get_slot_output_by_genv' is called in tolerant mode
  !    (present=.false.) so that we only jump to the next window without
  !    complaining.
  do iwin=0,dir_win%x%nbwin-1
    call get_slot_output_by_genv(allgenv(iwin),output,.false.,found,error)
    if (.not.found) then
      cycle
    elseif (error) then
      write(mess,*) 'Internal error: lost window #',iwin,', genv #',allgenv(iwin)
      call gtv_message(seve%e,rname,mess)
      return
    endif
    !
    if (x_refresh_genv(segm,mode,output%x%graph_env)) then
      call x_open_window(output%x%graph_env,mode)
      call gtview_work_1seg(output,segm,mode)
      call x_close_window(output%x%graph_env,mode)
    endif
  enddo
  !
end subroutine win_gtview_work_allwin_1seg
!
recursive subroutine win_gtview_work_allwin_recurs(dir,mode,error)
  use gtv_buffers
  use gtv_interfaces, except_this=>win_gtview_work_allwin_recurs
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - all windows of all directories starting from ad_start
  !  - the directory each window is attached on, recursively
  !---------------------------------------------------------------------
  type(gt_directory)                :: dir    ! The directory we will work on
  integer(kind=4),    intent(in)    :: mode   ! What to do?
  logical,            intent(inout) :: error  ! Logical error flag
  ! Local
  type(gt_directory), pointer :: son
  !
  ! 1) Windows attached to current directory
  call win_gtview_work_1dir(dir,mode,error)
  if (error)  return
  !
  ! 2) Windows attached to subdirectories
  son => dir%son_first
  do while (associated(son))
    call win_gtview_work_allwin_recurs(son,mode,error)
    if (error)  return
    son => son%brother
  enddo
  !
end subroutine win_gtview_work_allwin_recurs
!
subroutine win_gtview_work_1dir(dir,mode,error)
  use gbl_message
  use gtv_buffers
  use gtv_interfaces, except_this=>win_gtview_work_1dir
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - all windows of 1 directory (and only this directory)
  !  - the directory the windows are attached on, recursively
  !---------------------------------------------------------------------
  type(gt_directory)                :: dir    ! The directory to which the windows are attached
  integer(kind=4),    intent(in)    :: mode   ! What to do?
  logical,            intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='WIN_GTVIEW_WORK_1DIR'
  integer*4 :: iwin
  logical :: found
  type(gt_display), pointer :: output
  integer(kind=address_length) :: allgenv(0:mwindows-1)
  character(len=message_length) :: mess
  !
  if (dir%x%nbwin.le.0)  return
  !
  ! This subroutine will draw in several windows (this is the purpose
  ! of the 2nd loop after). BUT, looping from 0 to dir_win%x%nbwin-1 can be a
  ! problem since a window can disappear (typically, the lens) while we were
  ! drawing in another one. So, let's protect this.
  ! 1) Keep memory of all the genvs when entering. This is safer: the other
  !    arrays are compressed when destroying a window, we could be lost.
  do iwin=0,dir%x%nbwin-1
    allgenv(iwin) = c_get_win_genv(dir%x%genv_array,iwin)
  enddo
  !
  ! 2) Then, draw in all known genvs. Since a window/genv can be destroyed
  !    at any time, 'get_slot_output_by_genv' is called in tolerant mode
  !    (present=.false.) so that we only jump to the next window without
  !    complaining.
  do iwin=0,dir%x%nbwin-1
    call get_slot_output_by_genv(allgenv(iwin),output,.false.,found,error)
    if (.not.found) then
      cycle
    elseif (error) then
      write(mess,*) 'Internal error: lost window #',iwin,', genv #',allgenv(iwin)
      call gtv_message(seve%e,rname,mess)
      return
    endif
    !
    if (x_refresh_genv(dir,mode,output%x%graph_env)) then
      call x_open_window(output%x%graph_env,mode)
      call gtview_work_recursdir(output,dir,mode)
      call x_close_window(output%x%graph_env,mode)
    endif
  enddo
  !
end subroutine win_gtview_work_1dir
!
subroutine win_gtview_work_recursdir(dir,win_num,mode,error)
  use gtv_protocol
  use gtv_interfaces, except_this=>win_gtview_work_recursdir
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - 1 window of 1 directory
  !  - the directory the window is attached on, recursively
  !---------------------------------------------------------------------
  type(gt_directory)             :: dir      ! Directory to start from
  integer(kind=4), intent(in)    :: win_num  ! Window to draw in
  integer(kind=4), intent(in)    :: mode     ! Drawing mode
  logical,         intent(inout) :: error    ! Logical error flag
  ! Local
  type(gt_display), pointer :: output
  !
  call get_slot_output_by_num(dir,win_num,output,error)
  if (error) return
  !
  call gtview_work_recursdir(output,dir,mode)
  !
end subroutine win_gtview_work_recursdir
!
subroutine win_gtview_work_1seg(dir,win_num,segm,mode,error)
  use gtv_protocol
  use gtv_interfaces, except_this=>win_gtview_work_1seg
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Work on:
  !  - 1 window of 1 directory
  !  - the directory the window is attached on, recursively
  !---------------------------------------------------------------------
  type(gt_directory)             :: dir      ! Directory the window is attached on
  integer(kind=4), intent(in)    :: win_num  ! The window number in the directory
  type(gt_segment)               :: segm     ! The segment to draw
  integer(kind=4), intent(in)    :: mode     ! Drawing mode
  logical,         intent(inout) :: error    ! Logical error flag
  ! Local
  type(gt_display), pointer :: output
  !
  call get_slot_output_by_num(dir,win_num,output,error)
  if (error)  return
  !
  call gtview_work_1seg(output,segm,mode)
  !
end subroutine win_gtview_work_1seg
