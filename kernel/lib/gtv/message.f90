subroutine gterflag(flag)
  use gtv_graphic
  !---------------------------------------------------------------------
  ! @ public
  !
  !	Controls whether fatal errors cause an Abort or just set an
  !	error condition that can be sensed using the GTERRGTO or
  !	GTERRTST routines.
  !
  !	This is the only routine that you can call BEFORE the first call
  !	to GTINIT.
  !
  ! Argument :
  !	FLAG	.FALSE.	Errors are fatal (Default behaviour)
  !		.TRUE.  Errors are not fatal and can be sensed using
  !			the routine GTERRTST
  !---------------------------------------------------------------------
  logical :: flag                   !
  erflag = flag
  error_condition = .false.
end subroutine gterflag
!
function gterrtst()
  use gtv_graphic
  !---------------------------------------------------------------------
  ! @ public
  !
  !	Logical function. Senses the ERROR_CONDITION switch.
  !
  !	Returns the value of the ERROR_CONDITION flag.
  !	Resets the ERROR_CONDITION flag.
  !---------------------------------------------------------------------
  logical :: gterrtst               !
  !
  if (error_condition) then
    error_condition = .false.
    gterrtst = .true.
  else
    gterrtst = .false.
  endif
end function gterrtst
