subroutine set_attributs(output,attr_v)
  use gtv_interfaces, except_this=>set_attributs
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !   Mise en place des differents attributs de trace.
  !---------------------------------------------------------------------
  type(gt_display),      intent(inout) :: output  ! Output instance
  type(gt_segment_attr), intent(in)    :: attr_v  !
  call set_dash (output,attr_v%dash)
  call set_weigh(output,attr_v%weight)
  call set_color(output,attr_v%colour)
end subroutine set_attributs
!
subroutine set_dash(output,z_dash)
  use gtv_interfaces, except_this=>set_dash
  use gtv_buffers
  use gtv_graphic
  use gtv_protocol
  use gtv_plot
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fixe le parametre de pointille, pour les protocoles capables de le
  ! faire tous seuls.
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: output  ! Output instance
  integer*4,        intent(in)    :: z_dash  !
  ! Local
  logical :: z_flag_dash
  real*4 :: z_xy_scale
  integer(kind=4) :: z_pix_dash_style(4)
  integer :: i
  real*4 :: z_ps_dash_style(4),sizex,sizey,gsclx,gscly
  !
  output%idashe=z_dash
  !
  if (output%dev%protocol.eq.p_x) then
    ! je calcule un rapport valable en x et y pour les longueurs
    ! des piti traits diagonaux. Ce n'est utile que lorsqu'une
    ! diagonale de pixels m'est pas exactement a 45' sur l'ecran,
    ! sinon gsclx et gscly etant egaux, l'un des deux suffit.
    !
    sizex = cw_directory%phys_size(1)
    sizey = cw_directory%phys_size(2)
    call get_scale_awd(output,gsclx,gscly)
    z_xy_scale = (abs(gsclx)*sizex+abs(gscly)*sizey) / (sizex+sizey)
    !
    ! calcul du dash_style, les piti points sont au moins de longueur 1
    ! ds le cas ou HARDW_LINE_DASH est .FALSE., on CONSERVE IDASH  (ce qui
    ! force GTX_CHOPPE a travailler, mais on FORCE le dash hardware a
    ! "pas de dash pattern".
    if (output%idashe.eq.1 .or.  &
        .not.(user_hardw_line.and.output%dev%hardw_line_dash)) then
      z_flag_dash=.false.
    else
      z_flag_dash=.true.
      do i=1,4
        z_pix_dash_style(i)=z_xy_scale*onoff(i,output%idashe)
        if (z_pix_dash_style(i).lt.1) then
          z_pix_dash_style(i)=1
        endif
      enddo
    endif
    call x_dash(output%x%graph_env,z_flag_dash,z_pix_dash_style)
    !
  elseif (output%dev%protocol.eq.p_postscript) then
    if (output%idashe.eq.1) then
      z_flag_dash=.false.
    else
      z_flag_dash=.true.
      do i=1,4
        z_ps_dash_style(i)=onoff(i,output%idashe)
      enddo
    endif
    call ps_dash(z_flag_dash,z_ps_dash_style)
    !
  elseif (output%dev%protocol.eq.p_svg) then
    if (output%idashe.eq.1) then
      z_flag_dash=.false.
    else
      z_flag_dash=.true.
      do i=1,4
        z_ps_dash_style(i)=onoff(i,output%idashe)
      enddo
    endif
    call svg_dash(output,z_flag_dash,z_ps_dash_style)
  endif
  !
end subroutine set_dash
!
subroutine set_weigh(output,z_weigh)
  use gtv_interfaces, except_this=>set_weigh
  use gtv_buffers
  use gtv_graphic
  use gtv_protocol
  use gtv_plot
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! fixe le parametre de graisse, pour les protocoles capables de le
  ! faire tous seuls.
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: output   ! Output instance
  real(kind=4),     intent(in)    :: z_weigh  !
  !
  output%iweigh = z_weigh
  !
  if (output%dev%protocol.eq.p_x) then
    call x_weigh(output%x%graph_env,set_weight_pixel(output))
    !
  elseif (output%dev%protocol.eq.p_postscript) then
    call ps_weigh(output%iweigh)
    !
  elseif (output%dev%protocol.eq.p_svg) then
    call svg_weigh(output,output%iweigh)
    !
  elseif (output%dev%protocol.eq.p_png) then
    call png_weigh(output,set_weight_pixel(output))
  endif
  !
end subroutine set_weigh
!
function set_weight_pixel(output)
  use gtv_interfaces, except_this=>set_weight_pixel
  use gtv_buffers
  use gtv_plot
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Convert the line width from physical size (cm) to pixel
  !---------------------------------------------------------------------
  integer(kind=4) :: set_weight_pixel  ! Function value on return
  type(gt_display), intent(in) :: output  ! Output instance
  ! Local
  real(kind=4) :: z_xy_scale,sizex,sizey,gsclx,gscly
  !
  ! If weigh is 0 ou 1, we do not enlarge the line, else zooms won't
  ! help! Note that 0 ("hairline") gives better result than 1 on X
  ! devices (!!!)
  if (output%iweigh.le.graisse(1)) then
    set_weight_pixel = 0
    return
  endif
  !
  ! je calcule un rapport valable en x et y pour les longueurs
  ! des piti traits diagonaux. Ce n'est utile que lorsqu'une
  ! diagonale de pixels n'est pas exactement a 45' sur l'ecran,
  ! sinon gsclx et gscly etant egaux, l'un des deux suffit.
  sizex = cw_directory%phys_size(1)  ! Global dependency here!!!
  sizey = cw_directory%phys_size(2)  ! Global dependency here!!!
  call get_scale_awd(output,gsclx,gscly)
  z_xy_scale = (abs(gsclx)*sizex+abs(gscly)*sizey) / (sizex+sizey)
  !
  ! Calcul de l'epaisseur en pixel, limite au moins a 1
  set_weight_pixel = output%iweigh*z_xy_scale
  if (set_weight_pixel.lt.2)  set_weight_pixel = 0
  !
end function set_weight_pixel
!
subroutine set_color(output,z_color)
  use gtv_interfaces, except_this=>set_color
  use gtv_types
  use gtv_graphic
  use gtv_plot
  !---------------------------------------------------------------------
  ! @ private
  ! fixe le parametre de couleur
  !---------------------------------------------------------------------
  type(gt_display), intent(inout) :: output   ! Output instance
  integer*4,        intent(in)    :: z_color  !
  !
  output%icolou = z_color
  call gtx_pen(output,output%icolou,gbl_pen)
end subroutine set_color
