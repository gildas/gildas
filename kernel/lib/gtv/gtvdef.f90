subroutine gtx_setup(name,dev,error)
  use gbl_message
  use gtv_interfaces, except_this=>gtx_setup
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the device instance according to its identifier
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name   ! Terminal identifier (name)
  type(gt_device),  intent(inout) :: dev    ! The device instance to be set up
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTOPEN'
  character(len=80) :: found,paper_size
  integer(kind=4) :: ifound,idev
  !
  idev = 0
  call sic_ambigs(rname,name,found,ifound,device_list,1+ndevices,error)
  if (error)  return
  idev = ifound-1  ! because device_list numbering starts at 0
  !
  ! Reset everything
  call gtv_default(dev)
  !
  ! Then the specific values
  dev%ident = idev
  select case (device_list(idev))
  case ('NONE')
    dev%protocol = p_null
    dev%map_size = 128
    !
  case ('IMAGE')
    dev%protocol = p_x      !
    dev%init1    = "B"      ! Initialisation sequences init1 = B(ig cursor), B(lack)
                            !                                  S(mall cursor), W(hite)
    dev%linit1   = 1        ! Length of init1
    dev%init2    = "ZZ"     ! Initialisation sequences init2 = lotsofcolors because more than 1 char
    dev%linit2   = 2        ! Length of init2
    dev%px1      = 1        !
    dev%px2      = 750      !
    dev%py1      = 1        !
    dev%py2      = 545      !
    !
  case ('XPORTRAIT')
    dev%protocol = p_x      !
    dev%px1      = 1        !
    dev%px2      = 500      !
    dev%py1      = 1        !
    dev%py2      = 750      !
    !
  case ('XLANDSCAPE')
    dev%protocol = p_x      !
    dev%px1      = 1        !
    dev%px2      = 715      !
    dev%py1      = 1        !
    dev%py2      = 545      !
    !
  case ('PS','PDF')
    dev%protocol = p_postscript
    call gtx_paper_size
    if (paper_size.eq.'A4') then
      dev%px1    = 180      ! 65
      dev%px2    = 2600     ! 2850
      dev%py1    = 60       ! 60
      dev%py2    = 1960     ! 1960
    elseif (paper_size.eq."8.5x11".or.paper_size.eq."US_LETTER") then
      dev%px1    = 180      !
      dev%px2    = 2500     !
      dev%py1    = 60       !
      dev%py2    = 1900     !
    else
      call gtv_message(seve%e,rname,'Unknown paper format '//paper_size)
      error = .true.
      return
    endif
    dev%rxy      = 1.00503  !
    !
  case ('EPS','EPDF')
    dev%protocol = p_postscript
    call gtx_paper_size
    if (paper_size.eq.'A4') then
      dev%px1    = 180      ! 65
      dev%px2    = 2600     ! 2850
      dev%py1    = 60       ! 60
      dev%py2    = 1960     ! 1960
    elseif (paper_size.eq."8.5x11".or.paper_size.eq."US_LETTER") then
      dev%px1    = 180      !
      dev%px2    = 2500     !
      dev%py1    = 60       !
      dev%py2    = 1900     !
    else
      call gtv_message(seve%e,rname,'Unknown paper format '//paper_size)
      error = .true.
      return
    endif
    dev%rxy      = 1.0      !
    !
  case ('SVG')
    dev%protocol = p_svg    !
    dev%px1      = 0        !
    dev%px2      = 799      !
    dev%py1      = 559      ! Y scale increases from top to bottom in SVG
    dev%py2      = 0        !
    dev%rxy      = 1.0      !
    dev%poff     = 0.0      ! pixel #1 runs from 0.0 to 1.0
    !
  case ('PNG')
    dev%protocol = p_png    !
    dev%px1      = 1        !
    dev%px2      = 800      !
    dev%py1      = 1        !
    dev%py2      = 560      !
    dev%rxy      = 1.0      !
    !
  case default
    call gtv_message(seve%e,rname,  &
      'Device '//trim(device_list(idev))//' is not implemented')
    error = .true.
    return
  end select
  !
contains
  subroutine gtx_paper_size
    use gtv_dependencies_interfaces
    integer(kind=4) :: ier
    paper_size = 'A4'  ! Default
    ier = sic_getlog('GAG_PAPER_SIZE',paper_size)
    call sic_upper(paper_size)
  end subroutine gtx_paper_size
  !
end subroutine gtx_setup
!
subroutine gtv_default(dev)
  use gtv_interfaces, except_this=>gtv_default
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Reset a gt_device instance
  !---------------------------------------------------------------------
  type(gt_device), intent(inout) :: dev
  !
  dev%protocol = 0
  dev%defout = ' '
  dev%linit1 = -1
  dev%init1 = ' '
  dev%linit2 = -1
  dev%init2 = ' '
  dev%background = 1  ! White
  dev%px1 = 0
  dev%px2 = 250
  dev%py1 = 0
  dev%py2 = 200
  dev%rxy  = 1.00
  dev%poff = 0.5  ! pixel #1 runs from 0.5 to 1.5
  !
  dev%map_size = 128
  dev%map_offset = 0
  dev%map_color = .true.
  dev%map_static = .false.
  !
  dev%hardw_line_weig = .true.
  dev%hardw_line_dash = .true.
  dev%hardw_cursor = .false.
  !
end subroutine gtv_default
