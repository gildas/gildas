!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GTV messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gtv_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gtv_message_id = gpack_global_id  ! Default value for startup message
  !
end module gtv_message_private
!
subroutine gtv_message_set_id(id)
  use gtv_interfaces, except_this=>gtv_message_set_id
  use gtv_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gtv_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gtv_message_id
  call gtv_message(seve%d,'gtv_message_set_id',mess)
  !
end subroutine gtv_message_set_id
!
subroutine gtv_message(mkind,procname,message)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtv_message
  use gtv_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gtv_message_id,mkind,procname,message)
  !
end subroutine gtv_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
