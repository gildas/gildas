module gtv_interfaces_private
  interface
    subroutine get_free_slot_output(output,error)
      use gtv_types
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! RenameMe!
      !  Get a free slot for a 'output' instance.
      ! ---
      !  Thread status: not thread safe. Should the 'all_outputs' structure
      ! be protected?
      !---------------------------------------------------------------------
      type(gt_display), pointer :: output  ! The 'gt_display' instance found
      logical,    intent(inout) :: error   ! Logical error flag
    end subroutine get_free_slot_output
  end interface
  !
  interface
    subroutine get_slot_output_by_num(dir,win_num,output,error)
      use gbl_message
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! RenameMe!
      !  Return the index of the output instance which is identified by
      ! the input directory and window number.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe
      !---------------------------------------------------------------------
      type(gt_directory)              :: dir      ! Input directory
      integer(kind=4),  intent(in)    :: win_num  ! Window number
      type(gt_display), pointer       :: output   ! The 'gt_display' instance found
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine get_slot_output_by_num
  end interface
  !
  interface
    subroutine get_slot_output_by_genv(graph_env,output,present,found,error)
      use gildas_def
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! RenameMe!
      !  Return the index of the output instance which is identified by
      ! the input graph_env.
      ! ---
      !  Thread status: not thread safe. Should the 'all_outputs' structure
      ! be protected?
      !---------------------------------------------------------------------
      integer(kind=address_length), intent(in)    :: graph_env  ! Identifier
      type(gt_display),             pointer       :: output     ! The 'gt_display' instance found
      logical,                      intent(in)    :: present    ! Must the graph_env exist?
      logical,                      intent(out)   :: found      ! .true. if found
      logical,                      intent(inout) :: error      ! Logical error flag
    end subroutine get_slot_output_by_genv
  end interface
  !
  interface
    function get_window_cnum_byname(dir,rname,line,iopt,iarg,error)
      use gbl_message
      use gtv_types
      use gtv_tree
      !---------------------------------------------------------------------
      ! @ private
      !  Return the window instance in the input directory which is
      ! identified by its name (can be "ZOOM") or window number (as seen
      ! by user).
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe
      !---------------------------------------------------------------------
      integer(kind=4) :: get_window_cnum_byname  ! Function value on return
      type(gt_directory), intent(in)    :: dir    ! Directory to search in
      character(len=*),   intent(in)    :: rname  ! Calling routine name
      character(len=*),   intent(in)    :: line   ! Command line
      integer(kind=4),    intent(in)    :: iopt   ! Option number in command line
      integer(kind=4),    intent(in)    :: iarg   ! Argument number in option
      logical,            intent(inout) :: error  ! Logical error flag
    end function get_window_cnum_byname
  end interface
  !
  interface
    function get_window_cnum(dir,usernum,error)
      use gbl_message
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! RenameMe!
      !  Return the window instance which is identified by its parent
      ! directory and window number (as seen by user).
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe
      !---------------------------------------------------------------------
      integer(kind=4) :: get_window_cnum  ! Function value on return
      type(gt_directory), intent(in)    :: dir      ! Input directory
      integer(kind=4),    intent(in)    :: usernum  ! Window number (as seen by user)
      logical,            intent(inout) :: error    ! Logical error flag
    end function get_window_cnum
  end interface
  !
  interface
    function get_window_usernum(dir,cnum,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Return the window number in the user system of numbering, given
      ! the internal number in the C-and-continuous internal system of
      ! numbering.
      !---------------------------------------------------------------------
      integer(kind=4) :: get_window_usernum  ! Function value on return
      type(gt_directory), intent(in)    :: dir    ! The directory the window is attached on
      integer(kind=4),    intent(in)    :: cnum   ! The C number of the window we look for
      logical,            intent(inout) :: error  ! Logical error flag
    end function get_window_usernum
  end interface
  !
  interface
    subroutine free_slot_output_by_genv(graph_env,error)
      use gildas_def
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! RenameMe!
      !  Free the output instance which is identified by the input
      ! graph_env.
      !---------------------------------------------------------------------
      integer(kind=address_length), intent(in)  :: graph_env  ! Identifier
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine free_slot_output_by_genv
  end interface
  !
  interface
    subroutine gt_output_reset(output)
      use gtv_types
      use gtv_bitmap
      !---------------------------------------------------------------------
      ! @ private
      !   Reset the instance parameters, set it unused, and free the
      ! allocated memory it was using for images.
      !---------------------------------------------------------------------
      type(gt_display) :: output  ! Output instance
    end subroutine gt_output_reset
  end interface
  !
  interface
    subroutine x_display_reset(xdisplay)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(x_output), intent(inout) :: xdisplay  ! Instance to be reset
    end subroutine x_display_reset
  end interface
  !
  interface
    subroutine gt_bitmap_deallocate(bitmap)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental routine which deallocates the input 'gt_bitmap' and
      ! frees the data associated. This routine does NOT take care of the
      ! pointers to this instance: it is up to the calling routine to
      ! remake the chain, if required.
      !---------------------------------------------------------------------
      type(gt_bitmap), pointer :: bitmap  !
    end subroutine gt_bitmap_deallocate
  end interface
  !
  interface
    subroutine print_gt_device(dev)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Print the content various variables attached to the input gt_device
      ! structure. For debugging purpose.
      !---------------------------------------------------------------------
      type(gt_device), intent(in) :: dev  ! Input gt_device
    end subroutine print_gt_device
  end interface
  !
  interface
    subroutine print_gt_output(output)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Print the content of various variables attached to the input
      ! gt_device structure. For debugging purpose.
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: output  ! Input gt_display instance
    end subroutine print_gt_output
  end interface
  !
  interface
    subroutine get_scale_awd(out,scalex,scaley)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Compute the X and Y scaling relation between physical and pixel
      ! units, for Input Working Device.
      !  Note that the range of pixels [out%px1:out%px2] is fully mapped
      ! on the physical range [out%gx1:out%gx2]. Pixels have a finite size:
      ! the left border of the first pixel is aligned on the left border of
      ! the physical range, and the right border of the last pixel is
      ! aligned on the right border of the physical range.
      !  This is very useful for images: when you ask a 100 pixels image to
      ! be displayed on a 100 pixels device, you except a 1 to 1 pixel
      ! alignment (not 100 image pixels on 99 device pixels). Note also that
      ! you want to align the boundaries of the side pixels, NOT the center
      ! of the side pixels. Example:
      ! 1) Same number of pixels
      !  Image:  |--.--|--.--|--.--|--.--|--.--|--.--|--.--|--.--|  8 pixels
      !  Device: |--.--|--.--|--.--|--.--|--.--|--.--|--.--|--.--|  8 pixels
      ! 2a) Twice more, we want boundaries aligned:
      !  Image:  |-----.-----|-----.-----|-----.-----|-----.-----|  4 pixels
      !  Device: |--.--|--.--|--.--|--.--|--.--|--.--|--.--|--.--|  8 pixels
      ! 2b) Twice more, we DO NOT want centers aligned because this truncate
      !     the original pixels:
      !  Image:  |-----.-----|-----.-----|-----.-----|-----.-----|  4 pixels
      !  Device:     |-.--|--.-|--.-|--.-|--.-|--.-|--.-|--.-|      8 pixels
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out     ! Input gt_display
      real(kind=4),     intent(out) :: scalex  ! Number of pixels per world unit in X
      real(kind=4),     intent(out) :: scaley  ! Number of pixels per world unit in Y
    end subroutine get_scale_awd
  end interface
  !
  interface
    subroutine get_central_pixel_awd(out,xpix_center,ypix_center)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return the center pixel of Input Working Device. Should be dynamic
      ! at some point i.e. will dynamically get the device size (and NOT
      ! from some global variable), and then compute this center.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out          ! Input gt_display
      real(kind=4),     intent(out) :: xpix_center  ! Central pixel X coordinate
      real(kind=4),     intent(out) :: ypix_center  ! Central pixel X coordinate
    end subroutine get_central_pixel_awd
  end interface
  !
  interface
    subroutine get_central_clipping_awd(out,x_center,y_center)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return the center of the clipping window of the Input Working
      ! Device. Should be dynamic at some point i.e. will dynamically get
      ! the current device clipping window (and NOT from some global
      ! variable), and then compute this center.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out       ! Input gt_display
      real(kind=4),     intent(out) :: x_center  ! Center X point of plot space
      real(kind=4),     intent(out) :: y_center  ! Center Y point of plot space
    end subroutine get_central_clipping_awd
  end interface
  !
  interface pixel_to_world
    subroutine pixel_to_world_i4(out,px,py,wx,wy)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic pixel_to_world
      !  Convert pixel coordinates into world coordinates for the input
      ! working device.
      ! ---
      !  Integer(kind=4) version: do not call directly. Use generic
      ! interface 'pixel_to_world' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      integer(kind=4),  intent(in)  :: px,py  ! Input pixel coordinates
      real(kind=4),     intent(out) :: wx,wy  ! Output world coordinates
    end subroutine pixel_to_world_i4
    subroutine pixel_to_world_r4(out,px,py,wx,wy)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic pixel_to_world
      !  Convert pixel coordinates into world coordinates for the input
      ! working device.
      ! ---
      !  Real(kind=4) version: do not call directly. Use generic
      ! interface 'pixel_to_world' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: px,py  ! Input pixel coordinates
      real(kind=4),     intent(out) :: wx,wy  ! Output world coordinates
    end subroutine pixel_to_world_r4
  end interface pixel_to_world
  !
  interface pixel_to_world_size
    subroutine pixel_to_world_size_i4(out,px,py,wx,wy)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic pixel_to_world_size
      !  Convert pixel SIZE into world size for the input working device.
      ! ---
      !  Integer(kind=4) version: do not call directly. Use generic
      ! interface 'pixel_to_world_size' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      integer(kind=4),  intent(in)  :: px,py  ! Input pixel size
      real(kind=4),     intent(out) :: wx,wy  ! Output world size
    end subroutine pixel_to_world_size_i4
    subroutine pixel_to_world_size_r4(out,px,py,wx,wy)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic pixel_to_world_size
      !  Convert pixel SIZE into world size for the input working device.
      ! ---
      !  Real(kind=4) version: do not call directly. Use generic
      ! interface 'pixel_to_world_size' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: px,py  ! Input pixel size
      real(kind=4),     intent(out) :: wx,wy  ! Output world size
    end subroutine pixel_to_world_size_r4
  end interface pixel_to_world_size
  !
  interface world_to_pixel
    subroutine world_to_pixel_i4(out,wx,wy,px,py)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic world_to_pixel
      ! Convert world COORDINATES into pixel coordinates for the input
      ! working device.
      ! ---
      !  Integer(kind=4) version: do not call directly. Use generic
      ! interface 'world_to_pixel' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: wx,wy  ! Input world coordinates
      integer(kind=4),  intent(out) :: px,py  ! Output pixel coordinates
    end subroutine world_to_pixel_i4
    subroutine world_to_pixel_r4(out,wx,wy,px,py)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic world_to_pixel
      ! Convert world COORDINATES into pixel coordinates for the input
      ! working device.
      ! ---
      !  Real(kind=4) version: do not call directly. Use generic
      ! interface 'world_to_pixel' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: wx,wy  ! Input world coordinates
      real(kind=4),     intent(out) :: px,py  ! Output pixel coordinates
    end subroutine world_to_pixel_r4
  end interface world_to_pixel
  !
  interface world_to_pixel_size
    subroutine world_to_pixel_size_i4(out,wx,wy,px,py)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic world_to_pixel_size
      ! Convert world SIZE into pixel size for the input working device.
      ! ---
      !  Integer(kind=4) version: do not call directly. Use generic
      ! interface 'world_to_pixel_size' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: wx,wy  ! Input size in world coordinates
      integer(kind=4),  intent(out) :: px,py  ! Output size in pixels
    end subroutine world_to_pixel_size_i4
    subroutine world_to_pixel_size_r4(out,wx,wy,px,py)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-generic world_to_pixel_size
      ! Convert world SIZE into pixel size for the input working device.
      ! ---
      !  Real(kind=4) version: do not call directly. Use generic
      ! interface 'world_to_pixel_size' instead.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out    ! Input gt_display
      real(kind=4),     intent(in)  :: wx,wy  ! Input size in world coordinates
      real(kind=4),     intent(out) :: px,py  ! Output size in pixels
    end subroutine world_to_pixel_size_r4
  end interface world_to_pixel_size
  !
  interface
    subroutine cd_by_name(argum,active,win_num,error)
      use gildas_def
      use gtv_buffers
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Effectively move to the explicit directory named ARGUM, and on the
      ! specified window number if any.
      !  Warning: the window number is the number as seen by user!
      ! ---
      !  Thread status: thread safe for reading/writing from main.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: argum    ! Directory name to go to
      logical,          intent(in)    :: active   ! Use the active window?
      integer(kind=4),  intent(inout) :: win_num  ! Else, window number to use
      logical,          intent(inout) :: error    ! Error flag
    end subroutine cd_by_name
  end interface
  !
  interface
    subroutine cd_by_adr(dir_new,win_num,error)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Effectively move to the directory identified by DIR_NEW, and on the
      ! specified window number if any.
      !  Warning: the window number is the internal number!
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing (because the active window can be changed).
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: dir_new  ! Directory to move to
      integer*4,          intent(inout) :: win_num  ! Can be adjusted on return
      logical,            intent(inout) :: error    ! Logical error flag
    end subroutine cd_by_adr
  end interface
  !
  interface
    subroutine cd_by_win(dir,win_num_in,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Change the active window of the input directory.
      !  Warning: the window number is the internal number!
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing (because the active window can be changed).
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir         ! Directory to work on
      integer(kind=4), intent(in)    :: win_num_in  ! Window to move to
      logical,         intent(inout) :: error       ! Logical error flag
    end subroutine cd_by_win
  end interface
  !
  interface
    subroutine gtl_change(line,error)
      use gtv_graphic
      use gtv_tree
      use gtv_buffers
      use gtv_protocol
      use gtv_plot
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    CHANGE Item SegName Value [Val2 [Val3]]
      !    CHANGE Behaviour Value
      !---------------------------------------------------------------------
      character(len=*)                :: line   !
      logical,          intent(inout) :: error  !
    end subroutine gtl_change
  end interface
  !
  interface
    subroutine gtl_change_position(line,error)
      use gbl_message
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  CHANGE POSITION Num
      !  CHANGE POSITION Posx[Unitx] Posy[Unity]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_change_position
  end interface
  !
  interface
    subroutine gtl_change_attr(line,comm,error)
      use gbl_message
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_plot
      use gtv_segatt
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   CHANGE SCALING|BLANKING|COLOUR|DASH|DEPTH|VISIBILITY|WEIGHT SegName Value
      ! Modify (macro)segment attributes
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      character(len=*), intent(in)    :: comm   ! Attribute (already resolved)
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_change_attr
  end interface
  !
  interface
    subroutine gtl_change_parse(line,iarg,reset,value,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Retrieve the iarg-th argument of the command. Return reset=.true.
      ! (i.e. use internal default) if argument is wildcard '*', else
      ! reset=.false. and value is filled.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      integer(kind=4),  intent(in)    :: iarg   ! Argument number
      logical,          intent(out)   :: reset  !
      real(kind=4),     intent(out)   :: value  !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_change_parse
  end interface
  !
  interface
    subroutine gtl_segment_parse(argum,iseg1,iseg2,error)
      use gbl_message
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Parse segment name given in input, return segments number (min and
      ! max) to search in
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: argum  ! Segment name to parse
      integer,          intent(out)   :: iseg1  ! Lower segment number
      integer,          intent(out)   :: iseg2  ! Upper segment number
      logical,          intent(inout) :: error  ! Error flag
    end subroutine gtl_segment_parse
  end interface
  !
  interface
    subroutine change_attr_seg(segm,att_code,att_val)
      use gtv_buffers
      use gtv_segatt
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Elemental routine which modifies the ATT_CODE-th attribute and
      ! assigns it ATT_VAl. Work on a single segment only.
      !---------------------------------------------------------------------
      type(gt_segment)            :: segm      ! Segment instance
      integer(kind=4), intent(in) :: att_code  ! Attribute code
      integer(kind=4), intent(in) :: att_val   ! Attribute value
    end subroutine change_attr_seg
  end interface
  !
  interface
    recursive subroutine change_attr_dir(dir,att_code,att_val)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_segatt
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Routine which modifies the ATT_CODE-th attribute and assigns it
      ! ATT_VAl. Work on the input directory and its elements.
      !---------------------------------------------------------------------
      type(gt_directory)          :: dir       ! Directory
      integer(kind=4), intent(in) :: att_code  ! Attribute code
      integer(kind=4), intent(in) :: att_val   ! Attribute value
    end subroutine change_attr_dir
  end interface
  !
  interface
    subroutine change_scaling_image(segm,scaling,reset,value,strict,error)
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_segatt
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Modifie la valeur des attributs SCALING et CUTS
      !---------------------------------------------------------------------
      type(gt_segment), intent(inout) :: segm      ! Segment
      integer,          intent(in)    :: scaling   ! Scaling code (0 = do not change)
      logical,          intent(in)    :: reset(6)  ! Reset the cut limits?
      real(kind=4),     intent(in)    :: value(6)  ! Cut value, if reset
      logical,          intent(in)    :: strict    ! Is it an error that the segment is not an image?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine change_scaling_image
  end interface
  !
  interface
    recursive subroutine change_scaling_dir(dir,scaling,reset,value,error)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: dir       ! Directory
      integer,            intent(in)    :: scaling   ! Scaling code
      logical,            intent(in)    :: reset(6)  ! Reset the cut limits?
      real(kind=4),       intent(in)    :: value(6)  ! Cut value, if reset
      logical,            intent(inout) :: error     ! Logical error flag
    end subroutine change_scaling_dir
  end interface
  !
  interface
    subroutine change_image_lut(head,image)
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_segatt
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Change the input image LUT according to the current status of
      ! 'lut_static' flag
      !---------------------------------------------------------------------
      type(gt_segment_head), intent(in) :: head   ! Segment header
      type(gt_image)                    :: image  ! Image instance
    end subroutine change_image_lut
  end interface
  !
  interface
    subroutine change_poly_penlut(head,poly)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Change the input polyline/polygon PENLUT according to the current
      ! status of 'lut_static' flag
      !---------------------------------------------------------------------
      type(gt_segment_head), intent(in) :: head  ! Segment header
      type(gt_polyline)                 :: poly  ! Polyline instance
    end subroutine change_poly_penlut
  end interface
  !
  interface
    subroutine dummy_clip(nq,xq,yq)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Unused subroutine sent to 'vstrin' (doclip = .false.)
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in) :: nq     ! Number of points
      real(kind=4),              intent(in) :: xq(*)  ! Plot X coordinates
      real(kind=4),              intent(in) :: yq(*)  ! Plot Y coordinates
    end subroutine dummy_clip
  end interface
  !
  interface
    subroutine gtv_clear_alpha(output)
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  The purpose of this routine is to show the graphic display at best.
      ! To do that it
      !  - Clears the "Alphanumeric" plane of the input device
      !    is the graphic device.
      !  - Pop the graphic window on window systems
      !  - Does nothing in other cases.
      !---------------------------------------------------------------------
      type(gt_display) :: output         ! Output instance
    end subroutine gtv_clear_alpha
  end interface
  !
  interface
    subroutine gtv_clear_graphic(output)
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  The purpose of this routine is to show the ALPHA display at best.
      ! To do that it
      !  - Lower the graphic window on window systems
      !  - Does nothing in other cases.
      !---------------------------------------------------------------------
      type(gt_display) :: output         ! Output instance
    end subroutine gtv_clear_graphic
  end interface
  !
  interface
    subroutine gtl_clear(line,error)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command CLEAR
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_clear
  end interface
  !
  interface
    subroutine gtl_clear_segment(line,error)
      use gtv_buffers
      !---------------------------------------------------------------------
      ! @ private
      !  Cleanly destroy named segment(s) from the GTV tree, reading name
      ! from the command line.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line  ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_clear_segment
  end interface
  !
  interface
    subroutine clear_segment_elem(segm,error)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Cleanly clear the input segment, taking care of the surrounding
      ! chain and pointers in the GTV tree.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      type(gt_segment), pointer :: segm   ! Segment to clear
      logical,    intent(inout) :: error  ! Logical error flag
    end subroutine clear_segment_elem
  end interface
  !
  interface
    subroutine gtv_clear_all(error)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command:
      !    CLEAR ALL
      ! Clean (i.e. destroy the content of) all the top directories,
      ! leaving them alive, with all their windows.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtv_clear_all
  end interface
  !
  interface
    subroutine gtv_clear_tree(name_in,error)
      use gildas_def
      use gtv_buffers
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Erases the current tree only. If the current tree is alone,
      ! erases the whole plot, which saves space for the images...
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name_in  ! Tree name
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine gtv_clear_tree
  end interface
  !
  interface
    subroutine gtl_clear_directory(line,error)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command CLEAR DIRECTORY [Name]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_clear_directory
  end interface
  !
  interface
    subroutine gt_clear_directory(dir,keepall,error)
      use gbl_message
      use gildas_def
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Cleanly destroy the content of the input directory, taking care of
      ! the surrounding elements in the GTV tree. The top directory and its
      ! first window is kept.
      !---------------------------------------------------------------------
      type(gt_directory), pointer :: dir      ! Directory to destroy
      logical,      intent(in)    :: keepall  ! Keep all the windows, or only one?
      logical,      intent(inout) :: error    ! Logical error flag
    end subroutine gt_clear_directory
  end interface
  !
  interface
    subroutine gti_clear(output)
      use gtv_types
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! - Erase the graphic screen only
      ! - Erase all bit planes
      ! - Reset most device features, and should normally be used to be sure
      !   that the initialisation is correct.
      ! - Rewinds the viewing pointers
      !---------------------------------------------------------------------
      type(gt_display) :: output  ! Output instance
    end subroutine gti_clear
  end interface
  !
  interface
    subroutine gtwindow(output,ax,bx,ay,by)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Defines the clipping area and redraw the plot.
      !---------------------------------------------------------------------
      type(gt_display)   :: output      ! Output instance
      real*4, intent(in) :: ax,bx       ! Clipping area coordinates
      real*4, intent(in) :: ay,by       !
    end subroutine gtwindow
  end interface
  !
  interface
    subroutine sp_gtwindow(output,ax,bx,ay,by)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Defines the clipping area but does not redraw the plot.
      !---------------------------------------------------------------------
      type(gt_display)   :: output      ! Output instance
      real*4, intent(in) :: ax,bx       ! Clipping area coordinates
      real*4, intent(in) :: ay,by       !
    end subroutine sp_gtwindow
  end interface
  !
  interface
    subroutine sub_gtwindow(output,ax,bx,ay,by,redraw,error)
      use gtv_types
      use gtv_graphic
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Defines the clipping area and optionally redraw the plot. Works on
      ! the current working directory.
      !---------------------------------------------------------------------
      type(gt_display)       :: output  ! Output instance
      real*4,  intent(in)    :: ax,bx   ! Clipping area coordinates
      real*4,  intent(in)    :: ay,by   !
      logical, intent(in)    :: redraw  !
      logical, intent(inout) :: error   ! Logical error flag
    end subroutine sub_gtwindow
  end interface
  !
  interface
    subroutine on_resize(graph_env,error)
      use gildas_def
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Document me!
      !---------------------------------------------------------------------
      integer(kind=address_length), intent(in)    :: graph_env
      logical,                      intent(inout) :: error
    end subroutine on_resize
  end interface
  !
  interface
    subroutine gtclos(error)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   AWAKE flag is not reset by GTCLOS, as GTINIT is required only
      ! once even for multiple GTOPEN calls. Similarly, IUNIT is closed,
      ! but not freeed.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtclos
  end interface
  !
  interface
    subroutine gtx_reset
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  Resets flags.
      !----------------------------------------------------------------------
      !
      ! Reset flags
    end subroutine gtx_reset
  end interface
  !
  interface
    subroutine gtnone
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Define the null device as current device.
      !---------------------------------------------------------------------
    end subroutine gtnone
  end interface
  !
  interface
    subroutine gtl_compress(line,error)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command COMPRESS
      !  Permet un retassage de la memoire utilisee sous GREG en version
      ! multifenetre.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_compress
  end interface
  !
  interface
    recursive subroutine gtv_compress(dir,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Cleanly remove the input directory (and its subdirectories) if it
      ! is invisible, or its invisible segments and subdirectories.
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: dir    ! Pointer to directory to clean
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine gtv_compress
  end interface
  !
  interface
    subroutine gtl_create(line,error)
      use gtv_tree
      use gtv_buffers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Support routine for command
      ! CREATE WINDOW|DIRECTORY|PENLUT|LUT [/SIZE SizeX SizeY] [/PIXEL Px Py]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_create
  end interface
  !
  interface
    subroutine gtl_create_window(line,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !  CREATE WINDOW  [WHITE|BLACK] [NAME Name] /PIXEL Px Py
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_create_window
  end interface
  !
  interface
    subroutine create_window(output,first,dir,default,frommain,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Create a new window and register it in the GTV tree. If 'output'
      ! already knows a non-null graph_env, we assume the window as already
      ! been created and we only need to register it.
      ! ---
      !  Thread status: safe to call from main or graphic thread.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output    ! Output instance
      logical,          intent(in)    :: first     ! Very first window of the device?
      type(gt_directory)              :: dir       ! Directory to which the window is attached
      logical,          intent(in)    :: default   ! Make it the default window of the directory?
      logical,          intent(in)    :: frommain  ! Call from main or graphic thread?
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine create_window
  end interface
  !
  interface
    function create_window_number(dir,error)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: create_window_number     ! Function value on return
      type(gt_directory), intent(in)    :: dir    ! Parent directory
      logical,            intent(inout) :: error  ! Logical error flag
    end function create_window_number
  end interface
  !
  interface
    subroutine decode_coordinates(xcoord,argx,argy,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(x_coordinates), intent(inout) :: xcoord  !
      character(len=*),    intent(in)    :: argx    ! X value
      character(len=*),    intent(in)    :: argy    ! Y value
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine decode_coordinates
  end interface
  !
  interface
    subroutine sic_getlog_coordinates(logic_in,argx,argy,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: logic_in  ! SIC logical
      character(len=*), intent(out)   :: argx      !
      character(len=*), intent(out)   :: argy      !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine sic_getlog_coordinates
  end interface
  !
  interface
    subroutine compute_coordinates_geometry(rname,xcoord,npixx,npixy,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: rname   ! Caller name
      type(x_coordinates), intent(in)    :: xcoord  !
      integer(kind=4),     intent(out)   :: npixx   !
      integer(kind=4),     intent(out)   :: npixy   !
      logical,             intent(inout) :: error   ! Logical error flag
    end subroutine compute_coordinates_geometry
  end interface
  !
  interface
    subroutine compute_coordinates_position(output,npixx,npixy,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  !
      integer(kind=4),  intent(out)   :: npixx   !
      integer(kind=4),  intent(out)   :: npixy   !
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine compute_coordinates_position
  end interface
  !
  interface
    subroutine gtx_truncate(output,ax,ay)
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  Forces AX and AY within the clipping area
      !---------------------------------------------------------------------
      type(gt_display), intent(in)    :: output  ! 'gt_display' instance
      real,             intent(inout) :: ax,ay   !
    end subroutine gtx_truncate
  end interface
  !
  interface
    subroutine gtg_screen_sub(output,qx1,qx2,qy1,qy2)
      use gtv_graphic
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Returns the coordinates of the end pixels of the physical screen
      ! (world units). Very useful for interactive zooming. Works on the
      ! input instance
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: output   ! 'gt_display' instance
      real,             intent(out) :: qx1,qx2  !
      real,             intent(out) :: qy1,qy2  !
    end subroutine gtg_screen_sub
  end interface
  !
  interface
    function gtg_curs_sub(dev)
      use gtv_types
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ private
      !   Informs if a cursor is available on the input device
      !---------------------------------------------------------------------
      logical         :: gtg_curs_sub  ! Function value on return
      type(gt_device) :: dev           ! Device
    end function gtg_curs_sub
  end interface
  !
  interface
    subroutine gtcurs_sub(output,xcm,ycm,ch)
      use gtv_graphic
      use gtv_protocol
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !	Activates the cursor. CH contains the character struck
      !	and XCM YCM are the adresses of the cursor position, in
      !	plot units with respect to the current origin.
      !	Preset the cursor position at XCM YCM if possible
      !
      !	Does not modify the virtuel pen position and the immediate
      !	pen position.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)    :: output   ! Output instance
      real,             intent(inout) :: xcm,ycm  !
      character(len=*), intent(out)   :: ch       !
    end subroutine gtcurs_sub
  end interface
  !
  interface
    subroutine gtl_destroy(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command CLEAR
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_destroy
  end interface
  !
  interface
    subroutine gtl_destroy_directory(line,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command DESTROY DIRECTORY [Name]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_destroy_directory
  end interface
  !
  interface
    subroutine destroy_directory(dir,error)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Cleanly destroy the input directory, taking care of the surrounding
      ! pointers and chain in the GTV tree.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: dir    ! Directory to destroy
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine destroy_directory
  end interface
  !
  interface
    subroutine use_fen(dir,genv,error,nam)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Re-use a pre-existing window...
      !  Utilise UNIQUEMENT par gtv_destroy_all, apres un appel a DESTROY_MOST
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)              :: dir      ! Pointer to directory
      integer(kind=address_length)    :: genv     !
      logical,          intent(inout) :: error    ! Logical error flag
      character(len=*), intent(in)    :: nam      ! Optional window name
    end subroutine use_fen
  end interface
  !
  interface
    subroutine gtl_destroy_window(line,error)
      use gbl_message
      use gtv_buffers
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  DESTROY WINDOW [Dirname [WinNum|ZOOM]]
      ! Destroys all the windows of the input directory
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_destroy_window
  end interface
  !
  interface
    subroutine win_destroy_one_genv(dir,num_to_delete,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental routine which detaches the pointer to the input window
      ! from the input directory: the directory descriptor is updated
      ! accordingly. The code here assumes that c_delete_win_genv
      ! removes and shifts the window to be deleted from the genv_array
      ! list.
      !   Can be called by the window which is destroying itself (e.g.
      ! user has closed it manually).
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir            ! Directory where the window is attached
      integer(kind=4), intent(in)    :: num_to_delete  ! Window number to be deleted
      logical,         intent(inout) :: error          ! Logical error flag
    end subroutine win_destroy_one_genv
  end interface
  !
  interface
    subroutine win_destroy_one(dir,num_to_delete,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental routine which detaches the input window from the input
      ! directory: the window is destroyed and the directory descriptor is
      ! updated accordingly.
      !   Can be called by the user from the command line e.g. CLEAR
      ! WINDOW num.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir            ! Directory where the window is attached
      integer(kind=4), intent(in)    :: num_to_delete  ! Window number to be deleted
      logical,         intent(inout) :: error          ! Logical error flag
    end subroutine win_destroy_one
  end interface
  !
  interface
    subroutine win_destroy_all(dir,error)
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Detach all the windows attached to the input directory, and
      ! update the directory descriptor accordingly.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)     :: dir    ! Directory where the window is attached
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine win_destroy_all
  end interface
  !
  interface
    recursive subroutine win_destroy_all_recursive(dir,error)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Detach all the windows starting from input directory and
      ! recursively in its sub-directories.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)     :: dir    ! Directory to start from
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine win_destroy_all_recursive
  end interface
  !
  interface
    subroutine win_destroy_almost(dir,genv,error)
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Detach almost all the windows attached to the input directory, and
      ! update the directory descriptor accordingly. Keep only the first
      ! window of the input directory, and return its genv.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)                          :: dir    ! Directory where the window is attached
      integer(kind=address_length), intent(out)   :: genv   ! Saved Genv
      logical,                      intent(inout) :: error  ! Logical error flag
    end subroutine win_destroy_almost
  end interface
  !
  interface
    subroutine win_destroy_almost_recursive(dir,genv,error)
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Detach almost all the windows starting from input directory and
      ! recursively in its sub-directories. Keep only the first window
      ! of the input directory, and return its genv.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for writing.
      !---------------------------------------------------------------------
      type(gt_directory)                          :: dir    ! Directory to start from
      integer(kind=address_length), intent(out)   :: genv   ! Saved Genv
      logical,                      intent(inout) :: error  ! Logical error flag
    end subroutine win_destroy_almost_recursive
  end interface
  !
  interface
    subroutine gtl_device(line,error)
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GTVIRT Support routine for command
      ! DEVICE [Dev_type [Options]]
      ! 1  [/OUTPUT Dev_name]
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_device
  end interface
  !
  interface
    subroutine gtl_display(line,error)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   GTV\DISPLAY Item
      !
      ! DIRECTORY: Display current directory
      ! POINTER:   Display the pointer chains. For debugging purpose.
      ! SEGMENT:   Segment attributes
      ! TREE:      Directory Tree. If empty, list the content of current
      !            macrosegment. If not, list the content of input
      !            macrosegment (recursive).
      ! WINDOW:    Current Window
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_display
  end interface
  !
  interface
    subroutine display_segment_dir(directory,error)
      use gtv_bitmap
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! display_segment permet de lister:
      !  - les attributs du segment
      !  - les noms et attributs des segments du macrosegment repere par
      !    ad_dir si detail = .true., les noms seulement sinon
      !  - toute l'arborescence depuis le macrosegment repere par ad_dir:
      !    noms de macrosegments et segments
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: directory  ! Directory to analyse
      logical,            intent(inout) :: error      ! Logical error flag
    end subroutine display_segment_dir
  end interface
  !
  interface
    subroutine display_segment_seg(segment)
      use gtv_bitmap
      use gtv_buffers
      use gtv_colors
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! display_segment permet de lister:
      !  - les attributs du segment
      !  - les noms et attributs des segments du macrosegment repere par
      !    ad_dep si detail = .true., les noms seulement sinon
      !  - toute l'arborescence depuis le macrosegment repere par ad_dep:
      !    noms de macrosegments et segments
      !---------------------------------------------------------------------
      type(gt_segment), intent(in) :: segment  ! Segment to analyse
    end subroutine display_segment_seg
  end interface
  !
  interface
    subroutine display_segment_seg_data(segment)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment) :: segment  ! Segment data
    end subroutine display_segment_seg_data
  end interface
  !
  interface
    subroutine display_segment_seg_ind(segima)
      use gtv_bitmap
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_image), intent(in) :: segima  ! Image descriptor
    end subroutine display_segment_seg_ind
  end interface
  !
  interface
    subroutine display_segment_seg_rgb(segima)
      use gtv_bitmap
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_image), intent(in) :: segima  ! Image descriptor
    end subroutine display_segment_seg_rgb
  end interface
  !
  interface
    subroutine display_data(segment)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_segment) :: segment  ! Segment data
    end subroutine display_data
  end interface
  !
  interface
    subroutine display_data_poly(poly)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_polyline) :: poly  !
    end subroutine display_data_poly
  end interface
  !
  interface
    subroutine display_data_lut(lut)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_lut) :: lut  !
    end subroutine display_data_lut
  end interface
  !
  interface
    subroutine display_data_rgbima(ima)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_image) :: ima  !
    end subroutine display_data_rgbima
  end interface
  !
  interface
    subroutine display_data_indima(ima)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_image) :: ima  !
    end subroutine display_data_indima
  end interface
  !
  interface
    recursive subroutine display_tree(start)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! display_tree parcourt l'arborescence inferieure au macrosegment
      ! repere par 'start'.
      !---------------------------------------------------------------------
      type(gt_directory) :: start  ! Starting directory
    end subroutine display_tree
  end interface
  !
  interface
    subroutine display_tree_1dir(dir)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Depuis le macrosegment courant repere par ad_dep, display_tree_1dir
      ! renvoie le nom des macrosegments et segments et les valeurs des
      ! attributs respectifs.
      !---------------------------------------------------------------------
      type(gt_directory) :: dir  ! Directory to analyze
    end subroutine display_tree_1dir
  end interface
  !
  interface
    recursive subroutine display_pointer_tree(dir,verbose)
      use gildas_def
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! GTVIRT  Support routine for debugging
      !   Parcours les segments et affiche les valeurs des pointeurs
      ! last_leaf, window etc...
      !---------------------------------------------------------------------
      type(gt_directory)  :: dir      ! Directory to print from
      logical, intent(in) :: verbose  ! Display also the segments?
    end subroutine display_pointer_tree
  end interface
  !
  interface
    subroutine display_pointer_tree_1dir(dir,verbose)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_directory)  :: dir      ! Directory to display
      logical, intent(in) :: verbose  ! Display also the segments?
    end subroutine display_pointer_tree_1dir
  end interface
  !
  interface
    subroutine display_pointer_tree_1seg(segm)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment), intent(in) :: segm  ! Segment to display
    end subroutine display_pointer_tree_1seg
  end interface
  !
  interface
    subroutine gt_pwd(error)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! gt_pwd permet de se situer dans l'arborescence des macrosegments et
      ! affiche un chemin absolu.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gt_pwd
  end interface
  !
  interface
    subroutine display_window(dir,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Display the attributes of the Current Working Device (Window)
      !---------------------------------------------------------------------
      type(gt_directory)     :: dir    ! Directory to look at
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine display_window
  end interface
  !
  interface
    subroutine numberof_dir_local(parent,nb)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Increment 'nb' with the number of subdirectories which are directly
      ! attached to the parent directory
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: parent  ! Parent directory
      integer(kind=4),    intent(inout) :: nb      ! Number
    end subroutine numberof_dir_local
  end interface
  !
  interface
    recursive subroutine numberof_dir_recurs(parent,nb)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Increment 'nb' with the number of subdirectories which are directly
      ! or indirectly (i.e. recursively) attached to the parent directory.
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: parent  ! Parent directory
      integer(kind=4),    intent(inout) :: nb      ! Number
    end subroutine numberof_dir_recurs
  end interface
  !
  interface
    subroutine numberof_seg_local(parent,nb)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Increment 'nb' with the number of segments which are directly
      ! attached to the parent directory
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: parent  ! Parent directory
      integer(kind=4),    intent(inout) :: nb      ! Number
    end subroutine numberof_seg_local
  end interface
  !
  interface
    recursive subroutine numberof_seg_recurs(parent,nb)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Increment 'nb' with the number of subdirectories which are directly
      ! or indirectly (i.e. recursively) attached to the parent directory.
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: parent  ! Parent directory
      integer(kind=4),    intent(inout) :: nb      ! Number
    end subroutine numberof_seg_recurs
  end interface
  !
  interface
    subroutine gtx_err
      use gildas_def
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Traps an error according to ERFLAG.
      !---------------------------------------------------------------------
    end subroutine gtx_err
  end interface
  !
  interface
    subroutine gti_flush(output)
      use gtv_types
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_display) :: output         ! Output instance
    end subroutine gti_flush
  end interface
  !
  interface
    subroutine gti_trace(output,trace)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Handle traces, if supported by the device
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: output  ! Output instance
      character(len=*), intent(in) :: trace   ! The message trace
    end subroutine gti_trace
  end interface
  !
  interface
    subroutine gtv_image_segdata(ima,location,limits,convert,is_visible,error)
      use gtv_bitmap
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      use gtv_segatt
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Stores the image descriptor in the metacode, in the currently
      ! opened segment.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      type(gt_image),  target        :: ima          ! Image instance
      real(kind=4),    intent(in)    :: location(4)  ! Position in Paper coordinates
      real(kind=4),    intent(in)    :: limits(4)    ! User limits
      real(kind=4),    intent(in)    :: convert(6)   ! Image pixel to User conversion formula
      logical,         intent(in)    :: is_visible   ! Always Visible ?
      logical,         intent(inout) :: error        ! Logical error flag
    end subroutine gtv_image_segdata
  end interface
  !
  interface
    subroutine gtv_delimage(image)
      use gtv_bitmap
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !   Delete a GTVIRT image structure.
      !---------------------------------------------------------------------
      type(gt_image), target :: image  ! Image slot to free
    end subroutine gtv_delimage
  end interface
  !
  interface
    subroutine gtv_image_variables(ima,update,error)
      use gbl_message
      use gtv_bitmap
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Update or define the SIC structure CURIMA% according to the input
      ! image instance
      !---------------------------------------------------------------------
      type(gt_image), pointer       :: ima     ! Image instance
      logical,        intent(in)    :: update  ! Update or define?
      logical,        intent(inout) :: error   ! Logical error flag
    end subroutine gtv_image_variables
  end interface
  !
  interface
    subroutine gtv_image_logcuts(data,nxy,blank,rmin,rmax)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Compute the best extrema suited for LOG scaling
      !---------------------------------------------------------------------
      integer(kind=size_length), intent(in)   :: nxy
      real(kind=4),              intent(in)   :: data(nxy)
      real(kind=4),              intent(in)   :: blank(2)
      real(kind=4),              intent(out)  :: rmin,rmax
    end subroutine gtv_image_logcuts
  end interface
  !
  interface
    subroutine gtv_image_equalize(data,error)
      use gbl_message
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the scaled image if scaling is EQUALIZATION
      !---------------------------------------------------------------------
      type(gt_image_data), intent(inout) :: data   !
      logical,             intent(inout) :: error  !
    end subroutine gtv_image_equalize
  end interface
  !
  interface
    subroutine gt_image_equalize(imagein,eqimage,nx,ny,ncol,rmin,rmax,  &
      bval,eval,error)
      use gbl_message
      use gtv_bitmap
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4),                 intent(in)    :: imagein(*)  ! Original image
      integer(kind=2*bitmap_depth), intent(inout) :: eqimage(*)  ! Scaled values
      integer(kind=index_length),   intent(in)    :: nx,ny       ! Size of image
      integer(kind=4),              intent(in)    :: ncol        ! Number of colours
      real(kind=4),                 intent(in)    :: rmin,rmax   ! Range
      real(kind=4),                 intent(in)    :: bval,eval   ! Blanking value
      logical,                      intent(inout) :: error       ! Logical error flag
    end subroutine gt_image_equalize
  end interface
  !
  interface
    recursive subroutine exec_images_recurs(dir,execop)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Traverse all the leaves of the input directory and recursively in
      ! subdirectories and execute the input routine 'execop' on the GTV
      ! images it finds.
      !---------------------------------------------------------------------
      type(gt_directory) :: dir     ! Starting directory
      external           :: execop  !
    end subroutine exec_images_recurs
  end interface
  !
  interface
    subroutine image_codeop(leaf,execop)
      use gtv_buffers
      use gtv_plot
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Generic entry point which reads the input 'leaf' and execute
      ! the input routine if it finds a the GTV image (and only a GTV
      ! image).
      !  'execop' takes the image address as unique argument
      !---------------------------------------------------------------------
      type(gt_segment), pointer :: leaf    ! Segment instance
      external                  :: execop  ! Routine to execute on the image
    end subroutine image_codeop
  end interface
  !
  interface
    subroutine gti_fillpoly(out,poly,vert)
      use gtv_types
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !   Draws a filled polygon. Filling based on Horizontal scanning.
      !---------------------------------------------------------------------
      type(gt_display),  intent(inout) :: out   ! Input gt_display
      type(gt_polyline), intent(in)    :: poly  !
      logical,           intent(in)    :: vert  !
    end subroutine gti_fillpoly
  end interface
  !
  interface
    subroutine gi4_bltlis(lpoly,llv,npoly,nvert,xv,yv,iy,lnx,ixl,ixu)
      !---------------------------------------------------------------------
      ! @ private
      ! From a list of polygons, BLTLIS makes a list of horizontal line
      ! segments included in the polygons. There might several segments
      ! to fill for a row crossed by a single polygon.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: lpoly      ! Beginning polygon number
      integer(kind=4), intent(in)  :: llv        ! Beginning vertex number
      integer(kind=4), intent(in)  :: npoly      ! Number of polygons
      integer(kind=4), intent(in)  :: nvert(2)   ! Number of vertices in each polygon
      integer(kind=4), intent(in)  :: xv(1024)   ! X position of vertices
      integer(kind=4), intent(in)  :: yv(1024)   ! Y position of vertices
      integer(kind=4), intent(in)  :: iy         ! Row number
      integer(kind=4), intent(out) :: lnx        ! Number of segments
      integer(kind=4), intent(out) :: ixl(1024)  ! Low x index of segments
      integer(kind=4), intent(out) :: ixu(1024)  ! High x index of segments
    end subroutine gi4_bltlis
  end interface
  !
  interface
    subroutine gi4_bltgle (xt, yt, x1, y1, x2, y2, ang)
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for BLTLIS (INTEGER*4 version)
      !  Returns angle from A through a test position to B
      !  BLTGLE returns the angle in degrees from (X1, Y1) to (XT, YT) to
      ! (X2, Y2).
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: xt   ! Test position (X)
      real(kind=4),    intent(in)  :: yt   ! Test position (Y)
      integer(kind=4), intent(in)  :: x1   ! Mark position 1 (X)
      integer(kind=4), intent(in)  :: y1   ! Mark position 1 (Y)
      integer(kind=4), intent(in)  :: x2   ! Mark position 2 (X)
      integer(kind=4), intent(in)  :: y2   ! Mark position 2 (Y)
      real(kind=4),    intent(out) :: ang  ! Angle between 1 -> T -> 2
    end subroutine gi4_bltgle
  end interface
  !
  interface
    subroutine gr8_bltgle (xt, yt, x1, y1, x2, y2, ang)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Support routine for BLTLIS (REAL*8 version)
      !  Returns angle from A through a test position to B
      !  BLTGLE returns the angle in degrees from (X1, Y1) to (XT, YT) to
      ! (X2, Y2).
      !---------------------------------------------------------------------
      real(kind=4), intent(in)  :: xt   ! Test position (X)
      real(kind=4), intent(in)  :: yt   ! Test position (Y)
      real(kind=8), intent(in)  :: x1   ! Mark position 1 (X)
      real(kind=8), intent(in)  :: y1   ! Mark position 1 (Y)
      real(kind=8), intent(in)  :: x2   ! Mark position 2 (X)
      real(kind=8), intent(in)  :: y2   ! Mark position 2 (Y)
      real(kind=4), intent(out) :: ang  ! Angle between 1 -> T -> 2
    end subroutine gr8_bltgle
  end interface
  !
  interface
    subroutine gr4_bltgle (xt, yt, x1, y1, x2, y2, ang)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Support routine for BLTLIS (REAL*4 version)
      !  Returns angle from A through a test position to B
      !  BLTGLE returns the angle in degrees from (X1, Y1) to (XT, YT) to
      ! (X2, Y2).
      !---------------------------------------------------------------------
      real(kind=4), intent(in)  :: xt   ! Test position (X)
      real(kind=4), intent(in)  :: yt   ! Test position (Y)
      real(kind=4), intent(in)  :: x1   ! Mark position 1 (X)
      real(kind=4), intent(in)  :: y1   ! Mark position 1 (Y)
      real(kind=4), intent(in)  :: x2   ! Mark position 2 (X)
      real(kind=4), intent(in)  :: y2   ! Mark position 2 (Y)
      real(kind=4), intent(out) :: ang  ! Angle between 1 -> T -> 2
    end subroutine gr4_bltgle
  end interface
  !
  interface
    subroutine gti_image(output,image)
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Affichage d'une image a partir du descripteur lu dans le metacode
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! Input gt_display
      type(gt_image),   intent(in)    :: image   ! Image instance
    end subroutine gti_image
  end interface
  !
  interface
    subroutine clip_image(output,nx,ny,conv,box,user,resample,resfact,  &
      visible,clip,window,trans)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Compute clipping box of an image
      !---------------------------------------------------------------------
      type(gt_display)                        :: output     ! Output instance
      integer(kind=index_length), intent(in)  :: nx,ny      ! Image size
      real(kind=4),               intent(in)  :: conv(6)    ! Image pixel to User coordinates
      real(kind=4),               intent(in)  :: box(4)     ! Paper coordinates of bounding box
      real(kind=4),               intent(in)  :: user(4)    ! User coordinates of bounding box
      logical,                    intent(in)  :: resample   ! Is resample desired?
      real(kind=4),               intent(in)  :: resfact    ! Resampling factor (see below)
      logical,                    intent(out) :: visible    ! Is the image visible?
      real(kind=4),               intent(out) :: clip(4)    ! Clipping box in Paper coordinates
      integer(kind=4),            intent(out) :: window(4)  ! Clipping box in Window coordinates
      real(kind=4),               intent(out) :: trans(4)   ! Image to Window conversion
    end subroutine clip_image
  end interface
  !
  interface
    subroutine gti_bitmap(image,output,bitmap)
      use gtv_bitmap
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_image),   intent(in)    :: image   ! The image to be displayed
      type(gt_display), intent(in)    :: output  ! gt_display instance
      type(gt_bitmap),  intent(inout) :: bitmap  !
    end subroutine gti_bitmap
  end interface
  !
  interface
    subroutine compute_bitmap(image,bitmap,ncol,offset)
      use gtv_bitmap
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Generic entry point which computes the bitmap(s) for indexed or
      ! RGB images, standard or Lupton scaling
      !---------------------------------------------------------------------
      type(gt_image),  intent(in)    :: image   ! The image to be displayed
      type(gt_bitmap), intent(inout) :: bitmap  !
      integer(kind=4), intent(in)    :: ncol    ! Number of color or intensities per plane
      integer(kind=4), intent(in)    :: offset  ! Device offset for these intensities
    end subroutine compute_bitmap
  end interface
  !
  interface
    subroutine compute_bitmap_direct(data,scaling,bitmap,ivalues,ncol,offset)
      use gildas_def
      use gbl_message
      use gtv_bitmap
      !---------------------------------------------------------------------
      ! @ private
      !  Rescales the R*4 array IMAGE into the bitmap array DISPLAY with
      ! care of BLANKING values (put at 0) and LIN or LOG treatment
      ! (SCALING_MODE)
      !
      !  The image is scaled between the values 1 and NCOL. The colormap
      ! offset, if any, is handled by the device driver itself (cf
      ! x_affiche_image() in xsub.c)
      !---------------------------------------------------------------------
      type(gt_image_data),        intent(in)    :: data          ! The instance describing the image
      integer(kind=4),            intent(in)    :: scaling       ! Scaling mode
      type(gt_bitmap),            intent(inout) :: bitmap        ! The instance describing the bitmap
      integer(kind=bitmap_depth), intent(out)   :: ivalues(:,:)  ! The integer values
      integer,                    intent(in)    :: ncol          ! Number of colours
      integer,                    intent(in)    :: offset        ! Number of colours
    end subroutine compute_bitmap_direct
  end interface
  !
  interface
    subroutine compute_bitmap_lupton(data1,data2,data3,beta,bitmap,ivalues,ncol,offset)
      use gildas_def
      use gbl_message
      use gtv_bitmap
      !---------------------------------------------------------------------
      ! @ private
      !  Rescales the R*4 array IMAGE into the bitmap array DISPLAY with
      ! care of BLANKING values (put at 0) and LIN or LOG treatment
      ! (SCALING_MODE)
      !
      !  The image is scaled between the values 1 and NCOL. The colormap
      ! offset, if any, is handled by the device driver itself (cf
      ! x_affiche_image() in xsub.c)
      !---------------------------------------------------------------------
      type(gt_image_data),        intent(in)    :: data1         ! The instance describing the image
      type(gt_image_data),        intent(in)    :: data2         ! One of the complementary color
      type(gt_image_data),        intent(in)    :: data3         ! The second complementary color
      real(kind=4),               intent(in)    :: beta          ! Beta parameter
      type(gt_bitmap),            intent(inout) :: bitmap        ! The instance describing the bitmap
      integer(kind=bitmap_depth), intent(out)   :: ivalues(:,:)  ! The integer values
      integer,                    intent(in)    :: ncol          ! Number of colours
      integer,                    intent(in)    :: offset        ! Number of colours
    end subroutine compute_bitmap_lupton
  end interface
  !
  interface
    subroutine protocol_image_inquire(dev)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Inquire about device size and color map.
      ! Should be merged in the gtvdef definitions, since it is a property
      ! of the device.
      !---------------------------------------------------------------------
      type(gt_device), intent(inout) :: dev  ! Device to update
    end subroutine protocol_image_inquire
  end interface
  !
  interface
    subroutine gtx_setup(name,dev,error)
      use gbl_message
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the device instance according to its identifier
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Terminal identifier (name)
      type(gt_device),  intent(inout) :: dev    ! The device instance to be set up
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtx_setup
  end interface
  !
  interface
    subroutine gtv_default(dev)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Reset a gt_device instance
      !---------------------------------------------------------------------
      type(gt_device), intent(inout) :: dev
    end subroutine gtv_default
  end interface
  !
  interface
    subroutine gtl_gtv(line,error)
      use gildas_def
      use gtv_graphic
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command GTVL\GTV
      !  - GTV SEARCH Search for the existence of a GTV directory
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_gtv
  end interface
  !
  interface
    subroutine gtv_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gtv_message
  end interface
  !
  interface
    subroutine gtview_sub(output,chara,error)
      use gtv_types
      use gtv_buffers
      use gtv_graphic
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Plot to the output graphic device given as argument
      !
      ! 'Append'   Plot from last vector drawn
      ! 'Rewind'   Clear the screen, and plot from first vector
      ! 'Update'   Update the screens (Clear + Redraw)
      ! 'Color'    Update the screens (Redraw only)
      !
      ! Does not modify the virtuel pen position and the immediate pen
      ! position. P. Valiron
      ! Modifiee pour parcourir une arborescence par R.Gras. (Octobre 1992)
      !---------------------------------------------------------------------
      type(gt_display)                :: output  ! Output instance
      character(len=*), intent(in)    :: chara   ! Key character of the required mode
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtview_sub
  end interface
  !
  interface
    subroutine gtview_rewind(output,dir)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Deuxieme mode de parcours de l'arbre, correspond a gtview('Rewind')
      ! Update is made in the output device given in the calling sequence
      ! SOLELY, for all the depth in correct order.
      !---------------------------------------------------------------------
      type(gt_display)   :: output  ! Output instance
      type(gt_directory) :: dir     ! Starting directory
    end subroutine gtview_rewind
  end interface
  !
  interface
    subroutine gtview_update(dir_start,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! gtview('Update')
      ! Redraw completely all the windows attached to dir_start and its
      ! subdirectories.
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir_start  ! Starting directory
      logical,            intent(inout) :: error      ! Logical error flag
    end subroutine gtview_update
  end interface
  !
  interface
    subroutine gtview_color(dir_start,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! gtview('Update')
      ! Redraw completely all the windows attached to dir_start and its
      ! subdirectories.
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir_start  ! Starting directory
      logical,            intent(inout) :: error      ! Logical error flag
    end subroutine gtview_color
  end interface
  !
  interface
    subroutine gtview_append(seg_start,error)
      use gildas_def
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! gtview('Append')
      !---------------------------------------------------------------------
      type(gt_segment)                :: seg_start  ! Starting segment
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine gtview_append
  end interface
  !
  interface
    recursive subroutine gtview_work_recursdir(out,dir,mode)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Work (draw, update, clean...) in the input 'gt_display' instance,
      ! reading the tree 'dir', and recursively in its subdirectories.
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout) :: out   ! Input gt_display
      type(gt_directory)                :: dir   ! Starting directory
      integer(kind=4),    intent(in)    :: mode  !
    end subroutine gtview_work_recursdir
  end interface
  !
  interface
    subroutine gtview_work_1dir(out,dir,mode)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_segatt
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Work (draw, update, clean...) in the input 'gt_display' instance,
      ! reading the tree @ ad_start (only). No recursion here!
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out   ! Input gt_display
      type(gt_directory)              :: dir   ! Starting directory
      integer,          intent(in)    :: mode  !
    end subroutine gtview_work_1dir
  end interface
  !
  interface
    subroutine gtview_work_1seg(out,leaf,mode)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Work (draw, update, clean...) in the input 'gt_display' instance,
      ! reading the input elemental segment only.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out   ! Input gt_display
      type(gt_segment)                :: leaf  ! Leaf instance
      integer,          intent(in)    :: mode  !
    end subroutine gtview_work_1seg
  end interface
  !
  interface
    subroutine affich_dir(out,dir,mode)
      use gtv_buffers
      use gtv_protocol
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Procedure de parcours des feuilles d'un repertoire.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out   ! Input gt_display
      type(gt_directory)              :: dir   ! Starting leaf
      integer,          intent(in)    :: mode  !
    end subroutine affich_dir
  end interface
  !
  interface
    subroutine lect_descr(output,segm,mode,plot_segment)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the input segment and check if we have to plot it or not.
      !---------------------------------------------------------------------
      type(gt_display)     :: output        ! Output instance
      type(gt_segment)     :: segm          ! Address of the descriptor to analyse
      integer, intent(in)  :: mode          ! Plot mode
      logical, intent(out) :: plot_segment  ! Need to plot the segment?
    end subroutine lect_descr
  end interface
  !
  interface
    subroutine traite_codeop(out,leaf)
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental procedure which draws a graphic segment into the
      ! 'output' device. Read from the standard GTV buffer ('cw_buffer').
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out   ! Input gt_display
      type(gt_segment)                :: leaf  ! Segment instance
    end subroutine traite_codeop
  end interface
  !
  interface
    subroutine win_gtview_work_allwin_updir(segm,mode,error)
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - all windows which see 'segm'
      !  - a single leaf 'segm'
      !---------------------------------------------------------------------
      type(gt_segment)                :: segm   ! Leaf to work on
      integer(kind=4),  intent(in)    :: mode   ! Working mode
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine win_gtview_work_allwin_updir
  end interface
  !
  interface
    subroutine win_gtview_work_allwin_1seg(dir_win,segm,mode,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - all windows of 1 directory (and only this directory)
      !  - 1 segment (not a directory)
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir_win  ! The directory to which the windows are attached
      type(gt_segment)                  :: segm     ! The segment we work on
      integer(kind=4),    intent(in)    :: mode     ! What to do?
      logical,            intent(inout) :: error    ! Logical error flag
    end subroutine win_gtview_work_allwin_1seg
  end interface
  !
  interface
    recursive subroutine win_gtview_work_allwin_recurs(dir,mode,error)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - all windows of all directories starting from ad_start
      !  - the directory each window is attached on, recursively
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir    ! The directory we will work on
      integer(kind=4),    intent(in)    :: mode   ! What to do?
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine win_gtview_work_allwin_recurs
  end interface
  !
  interface
    subroutine win_gtview_work_1dir(dir,mode,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - all windows of 1 directory (and only this directory)
      !  - the directory the windows are attached on, recursively
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir    ! The directory to which the windows are attached
      integer(kind=4),    intent(in)    :: mode   ! What to do?
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine win_gtview_work_1dir
  end interface
  !
  interface
    subroutine win_gtview_work_recursdir(dir,win_num,mode,error)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - 1 window of 1 directory
      !  - the directory the window is attached on, recursively
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir      ! Directory to start from
      integer(kind=4), intent(in)    :: win_num  ! Window to draw in
      integer(kind=4), intent(in)    :: mode     ! Drawing mode
      logical,         intent(inout) :: error    ! Logical error flag
    end subroutine win_gtview_work_recursdir
  end interface
  !
  interface
    subroutine win_gtview_work_1seg(dir,win_num,segm,mode,error)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Work on:
      !  - 1 window of 1 directory
      !  - the directory the window is attached on, recursively
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir      ! Directory the window is attached on
      integer(kind=4), intent(in)    :: win_num  ! The window number in the directory
      type(gt_segment)               :: segm     ! The segment to draw
      integer(kind=4), intent(in)    :: mode     ! Drawing mode
      logical,         intent(inout) :: error    ! Logical error flag
    end subroutine win_gtview_work_1seg
  end interface
  !
  interface
    subroutine gtx_segm_0(error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Initialize the first segment "<" with default line attributes.
      !	WORD 1	Null pointer to previous segment
      !			Buffer number
      !			Address in buffer
      !	WORD 2	Undefined pointer to next segment  !Si si (0,0)
      !			Buffer number
      !			Address in buffer
      !	WORD 3	Default line attributes
      !			Line type 			1 byte
      !			Line width			1 byte
      !			Colour index			2 bytes
      !
      ! En plus de toutes ces bonnes choses faites par le Maitre ...
      ! GTX_SEGM_0 cree la racine de l'arborescence du metacode, initialise
      ! les pointeurs de chainage, cree le premier repertoire qui s'appelle
      ! TOP_DIR (definit par GTINIT), puis fait un CHANGE DIR TOP_DIR
      ! R. Gras
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtx_segm_0
  end interface
  !
  interface
    subroutine gtsegm_dir(name,parent,sizex,sizey,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Creation d'un descripteur de macrosegment de nom name
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name    ! Name of the new macrosegment
      type(gt_directory), pointer     :: parent  ! Parent directory
      real*4,           intent(in)    :: sizex   ! Physical paper size (x)
      real*4,           intent(in)    :: sizey   ! Physical paper size (y)
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtsegm_dir
  end interface
  !
  interface
    subroutine find_grandpere(pere,dir,gdpere)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Find the "grandpere" for the current directory, that is, the
      ! directory in this tree which is just under the root (1,1)
      ! NB: that may indeed be the current directory.
      !---------------------------------------------------------------------
      type(gt_directory)          :: pere    ! Parent directory
      type(gt_directory), pointer :: dir     ! Current directory
      type(gt_directory), pointer :: gdpere  ! Address of the grandfather
    end subroutine find_grandpere
  end interface
  !
  interface
    subroutine gtv_limits(dir_start,error)
      use gbl_message
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Traverse the current tree structure, and Recompute all the MinMax,
      ! taking care of the visibility status of all the segments. Do it
      ! from the ancestor of input directory, because parent directories
      ! are affected by the minmax or their child.
      !  Used after a CHANGE visibility off or GTDLS.
      !---------------------------------------------------------------------
      type(gt_directory), target :: dir_start  ! Starting directory
      logical,     intent(inout) :: error      ! Logical error flag
    end subroutine gtv_limits
  end interface
  !
  interface
    recursive subroutine gtv_limits_one(dir_start,dminmax,error)
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Traverse the current tree structure, from input directory, and
      ! Recompute all the MinMax, taking care of the visibility status of
      ! all the segments. Then store these MinMax in the input directory.
      !---------------------------------------------------------------------
      type(gt_directory)     :: dir_start   ! Starting address
      real*4,  intent(out)   :: dminmax(4)  ! Computed XY extrema
      logical, intent(inout) :: error       ! Logical error flag
    end subroutine gtv_limits_one
  end interface
  !
  interface
    subroutine minmax_dir(dir,minmax,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Procedure de parcours des feuilles d'un repertoire en vue de
      ! calculer les minmax sur le directory courant, taking care of the
      ! visibility status of all the segments.
      !---------------------------------------------------------------------
      type(gt_directory)     :: dir        ! Directory
      real*4,  intent(out)   :: minmax(4)  ! Computed XY extrema
      logical, intent(inout) :: error      ! Logical error flag
    end subroutine minmax_dir
  end interface
  !
  interface
    subroutine gtpolyl(n,vx,vy,error)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !  Connects points (world coordinates).
      !  GTPOLYL has no effect for N less than 1.
      !  GTPOLYL behaves like GTRELOC for N equal to 1.
      !  Much more efficient than calls to GTRELOC and GTDRAW when N is
      ! substancially larger than 2.
      !---------------------------------------------------------------------
      integer, intent(in)    :: n      ! Number of points
      real*4,  intent(in)    :: vx(n)  ! Array of abscissae
      real*4,  intent(in)    :: vy(n)  ! Array of ordinates
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtpolyl
  end interface
  !
  interface
    subroutine gtx_frxry(error)
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Flushes POLYL%X and POLYL%Y buffers into internal metacode.
      !
      !  CAUTION !!!
      !   The indirect return happens if the ERROR_CONDITION flag is set.
      ! This flag may have been set previously by another error condition.
      ! Thus the calling program SHOULD be inactive if he is called with
      ! the error_condition flag set.
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtx_frxry
  end interface
  !
  interface
    subroutine dir_extrema(dir,minmax)
      use gtv_tree
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Updates the minmax values of the directory given in input (compose
      ! the areas).
      !---------------------------------------------------------------------
      type(gt_directory), intent(inout) :: dir        ! The directory to be updated
      real(kind=4),       intent(in)    :: minmax(4)  ! The new minmax values
    end subroutine dir_extrema
  end interface
  !
  interface
    subroutine gtsegm_create(name,parent,error)
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),   intent(in)    :: name    ! Segment name
      type(gt_directory), pointer       :: parent  ! Parent directory
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine gtsegm_create
  end interface
  !
  interface
    subroutine gtsegm_flush(error)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Flush the internal buffers into the currently opened segment, i.e.
      ! make it ready to be drawn.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtsegm_flush
  end interface
  !
  interface
    function gtv_newsegment(error)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Allocate a new GTVIRT segment.
      !---------------------------------------------------------------------
      type(gt_segment), pointer :: gtv_newsegment  ! Function value on return
      logical,    intent(inout) :: error           ! Logical error flag
    end function gtv_newsegment
  end interface
  !
  interface
    subroutine gtv_delsegment_elem(segm)
      use gtv_buffers
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental routine which deallocates the input segment and frees
      ! the data associated. This routine does NOT take care of the pointers
      ! to this segment in the GTV tree: it is up to the calling routine
      ! to remake the chain, if required.
      ! ---
      !   Threads status: NOT thread safe -> calling routine must be thread
      !  safe.
      !---------------------------------------------------------------------
      type(gt_segment), target :: segm  ! Segment to clean
    end subroutine gtv_delsegment_elem
  end interface
  !
  interface
    subroutine gtv_delsegments(segm)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Destroy the input linked list of segments, without taking care of
      ! the surrounding elements in the GTV tree.
      !  This subroutine can only be called if:
      ! - all the segments we are about to destroy have been dereferenced
      !   from the GTV tree,
      ! - all the previous events in the stack (which may refer to these
      !   segments) have been processed, i.e. only the stack can call this
      !   subroutine.
      !
      ! BEWARE! As we receive a target, the caller must ensure that a
      !         it never passes a null() pointer to this subroutine!
      !---------------------------------------------------------------------
      type(gt_segment), target :: segm  ! First segment to destroy
    end subroutine gtv_delsegments
  end interface
  !
  interface
    function gtv_newdirectory(error)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Allocate a new GTVIRT directory.
      !---------------------------------------------------------------------
      type(gt_directory), pointer :: gtv_newdirectory  ! Function value on return
      logical,      intent(inout) :: error             ! Logical error flag
    end function gtv_newdirectory
  end interface
  !
  interface
    subroutine gtv_deldirectory_elem(dir)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Elemental routine which frees the data associated to the input
      ! directory. This routine:
      !  - does NOT take care of the pointers to this directory in the GTV
      !    tree,
      !  - does NOT delete the subdirectories it might contain, if any.
      ! ---
      !   Threads status: NOT thread safe -> calling routine must be thread
      !  safe.
      !---------------------------------------------------------------------
      type(gt_directory), target :: dir  !
    end subroutine gtv_deldirectory_elem
  end interface
  !
  interface
    recursive subroutine gtv_deldirectory_recurs(dir)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Destroy the input directory:
      !  - destroy its segments and data
      !  - destroy its subdirectories (by a recursive call to this routine)
      ! This does NOT take care of the pointers and chain to the destroyed
      ! directory.
      !---------------------------------------------------------------------
      type(gt_directory) :: dir  ! Directory to destroy
    end subroutine gtv_deldirectory_recurs
  end interface
  !
  interface
    subroutine gtv_deldirectories(dir)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Destroy the input linked list of directories, recursively for each
      ! of them, without taking care of the surrounding elements in the
      ! GTV tree.
      !  This subroutine can only be called if:
      ! - all the elements we are about to destroy have been dereferenced
      !   from the GTV tree,
      ! - all the previous events in the stack (which may refer to these
      !   elements) have been processed, i.e. only the stack can call this
      !   subroutine.
      !
      ! BEWARE! As we receive a target, the caller must ensure that a
      !         it never passes a null() pointer to this subroutine!
      !---------------------------------------------------------------------
      type(gt_directory), target :: dir  ! First directory to destroy
    end subroutine gtv_deldirectories
  end interface
  !
  interface
    recursive subroutine exec_poly_recurs(dir,execop)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Traverse all the leaves of the input directory and recursively in
      ! subdirectories and execute the input routine 'execop' on the GTV
      ! polylines and polygons it finds.
      !---------------------------------------------------------------------
      type(gt_directory) :: dir     ! Starting directory
      external           :: execop  !
    end subroutine exec_poly_recurs
  end interface
  !
  interface
    subroutine poly_codeop(leaf,execop)
      use gtv_buffers
      use gtv_plot
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Generic entry point which reads the input 'leaf' and execute
      ! the input routine if it finds a the GTV polyline or polygon.
      !  'execop' takes the segment header and the polyline/polygon address
      ! as argument
      !---------------------------------------------------------------------
      type(gt_segment), pointer :: leaf    ! Segment instance
      external                  :: execop  ! Routine to execute on the image
    end subroutine poly_codeop
  end interface
  !
  interface
    subroutine load_gtvl
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      ! Local
    end subroutine load_gtvl
  end interface
  !
  interface
    subroutine gtv_delseg(name,is_dir,error)
      use gtv_buffers
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !     Delete (i.e. make it invisible) a named segment or a directory
      !---------------------------------------------------------------------
      character(len=*)                :: name    !
      logical                         :: is_dir  !
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtv_delseg
  end interface
  !
  interface
    subroutine gtl_replicate(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_replicate
  end interface
  !
  interface
    subroutine gtl_hardcopy(line,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support command for command
      ! HARDCOPY [Name]
      !  1  [/PRINT Destination]
      !  2  [/DEVICE Type [Options]]
      !  3  [/OVERWRITE]
      !  4  [/DIRECTORY]
      !  5  [/GEOMETRY]
      !  6  [/EXACT]
      !  7  [/FITPAGE]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_hardcopy
  end interface
  !
  interface
    subroutine sic_getlog_hardcopy(devtyp,devopt)
      !---------------------------------------------------------------------
      ! @ private
      ! Decode the SIC logical GAG_HARDCOPY
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: devtyp  !
      character(len=*), intent(out) :: devopt  !
    end subroutine sic_getlog_hardcopy
  end interface
  !
  interface
    subroutine gtl_hardcopy_ps(line,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the options specific to the PostScript devices coming from
      ! the command line.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line    ! Input command line
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtl_hardcopy_ps
  end interface
  !
  interface
    subroutine gt_hardcopy_ps(keyw,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the input option specific to the PostScript devices.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: keyw    ! Keyword to decode
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gt_hardcopy_ps
  end interface
  !
  interface
    subroutine gtl_hardcopy_png(line,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the options specific to the PNG device coming from the
      ! command line.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line    ! Input command line
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtl_hardcopy_png
  end interface
  !
  interface
    subroutine gt_hardcopy_png(keyw,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the input option specific to the PNG devices.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: keyw    ! Keyword to decode
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gt_hardcopy_png
  end interface
  !
  interface
    subroutine gtl_hardcopy_svg(line,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the options specific to the SVG device coming from the
      ! command line.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line    ! Input command line
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtl_hardcopy_svg
  end interface
  !
  interface
    subroutine gt_hardcopy_svg(keyw,output,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Decode the input option specific to the SVG devices.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: keyw    ! Keyword to decode
      type(gt_display), intent(inout) :: output  ! Instance to customize
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gt_hardcopy_svg
  end interface
  !
  interface
    subroutine gt_hardcopy(output,devtyp,directory,keep,lover,doprint,  &
      queue,error)
      use gildas_def
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Generic interface to the internal hardcopy engine. Can be called
      ! either from command line or from menu.
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout) :: output      ! gt_display instance
      character(len=*),   intent(inout) :: devtyp      ! Device type
      type(gt_directory), intent(in)    :: directory   ! Directory to dump
      logical,            intent(in)    :: keep        ! Keep the file after printing?
      logical,            intent(in)    :: lover       ! Can overwrite existing file?
      logical,            intent(in)    :: doprint     ! Send to the printer immediately?
      character(len=*),   intent(inout) :: queue       ! Which printer?
      logical,            intent(inout) :: error       ! Logical error flag
    end subroutine gt_hardcopy
  end interface
  !
  interface
    subroutine ghopen(output,devtyp,directory,lover,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_types
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  Opens a specific Device of given Type, eventually using Options
      ! for setting parameters. Restricted version for hardcopies...
      !
      !  TYPE  The type of Graphics device to be used
      !        PostScript
      !
      !  DEVICE  Optional argument to indicate to which device the plot
      !          must be sent.
      !
      !  OPTION  Optional string to define some modes for specific devices
      !          PostScript Grey, Fast, Color
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout) :: output      ! gt_display instance
      character(len=*)                  :: devtyp      ! Type of device PostScript
      type(gt_directory), intent(in)    :: directory   ! Directory to dump
      logical,            intent(in)    :: lover       ! Allow overwriting existing file
      logical,            intent(inout) :: error       ! Logical error flag
    end subroutine ghopen
  end interface
  !
  interface
    subroutine ghclos(output)
      use gtv_types
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !   Switches back to alpha mode, and empties the buffer.
      !   GHCLOS also resets flags VECFIL and LL601.
      !
      !   AWAKE flag is not reset by GTCLOS, as GTINIT is required only once
      ! even for multiple GTOPEN calls IUNIT is closed AND libered.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! Output instance
    end subroutine ghclos
  end interface
  !
  interface
    function rgb_to_grey(r,g,b)
      !---------------------------------------------------------------------
      ! @ private
      ! Convert the 3 RGB intensities into a grey luminance
      ! Method choosen in the one implemented in the GIMP:
      !  see http://gimp-savvy.com/BOOK/index.html?node54.html
      ! ---
      !   This function transforms a 24-bit, three-channel, color image to
      ! an 8-bit, single-channel greyscale image by forming a weighted sum
      ! of the red, green, and blue components. The formula used in the GIMP
      ! is Y = 0.3R + 0.59G + 0.11B; this result is known as luminance (see
      ! [4] or [5]). The weights used to compute luminance are related to
      ! the monitor's phosphors. The explanation for these weights is due to
      ! the fact that for equal amounts of color the eye is most sensitive
      ! to green, then red, and then blue. This means that for equal amounts
      ! of green and blue light the green will, nevertheless, seem much
      ! brighter. Thus, the image obtained by the normal averaging of an
      ! image's three color components produces a greyscale brightness that
      ! is not perceptually equivalent to the brightness of the original
      ! color image. The weighted sum that defines Y, however, does.
      !---------------------------------------------------------------------
      real(kind=4) :: rgb_to_grey  ! Function value on return
      real(kind=4), intent(in) :: r  ! R intensity
      real(kind=4), intent(in) :: g  ! G intensity
      real(kind=4), intent(in) :: b  ! B intensity
    end function rgb_to_grey
  end interface
  !
  interface
    subroutine gt_hardcopy_crop(output,dir,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Crop the area viewed by the output instance
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout) :: output  ! Output instance
      type(gt_directory), intent(in)    :: dir     ! The directory to display
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine gt_hardcopy_crop
  end interface
  !
  interface
    subroutine gtl_lens(line,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command LENS
      !  Invoke the lens and suspend execution until the lens has been
      ! closed.
      ! ---
      !  Thread status: safe for reading from main thread
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_lens
  end interface
  !
  interface
    subroutine lens_register(dir,genv_main,genv_lens,ier)
      use gildas_def
      use gtv_protocol
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Register in the GTV structures the magnify lens which has already
      ! been opened. No need to create a new genv_array, there must be
      ! one since we zoom on an existing window.
      ! ---
      !  Thread status: safe to call from graphic thread.
      !---------------------------------------------------------------------
      type(gt_directory)                        :: dir        ! Parent directory
      integer(kind=address_length), intent(in)  :: genv_main  ! Genv of the window we zoom in
      integer(kind=address_length), intent(in)  :: genv_lens  ! Genv of the lens window
      integer(kind=4),              intent(out) :: ier        ! Error flag
    end subroutine lens_register
  end interface
  !
  interface
    subroutine lens_limits(genv_main,genv_lens,pixxc,pixyc,zoom,ier)
      use gildas_def
      use gbl_message
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Redraw the lens according to the input center position (in pixels
      ! coordinates of the parent window) and the zooming factor.
      !---------------------------------------------------------------------
      integer(kind=address_length), intent(in)  :: genv_main  ! Genv of the window we zoom in
      integer(kind=address_length), intent(in)  :: genv_lens  ! Genv of the lens window
      integer, intent(in)  :: pixxc  ! X center (pixels)
      integer, intent(in)  :: pixyc  ! Y center (pixels)
      real,    intent(in)  :: zoom   ! Zoom factor
      integer, intent(out) :: ier    ! Error flag
    end subroutine lens_limits
  end interface
  !
  interface
    subroutine gtx_chopper(out,cx1,cy1,cx2,cy2,up)
      use gtv_types
      use gtv_graphic
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      !  Dashed pattern and thickness generator
      !  Tente de corriger problemes vicieux si segments aleatoires tres
      ! courts ET segments nuls en mode pointille... Supprime le code
      ! specifique "segment longueur nulle"
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out      ! Input gt_display
      real*4,           intent(in)    :: cx1,cy1  ! X,Y coordinate of start point
      real*4,           intent(in)    :: cx2,cy2  ! X,Y coordinate of end point
      logical,          intent(in)    :: up       ! Force goto (CX1,CY1) pen up
    end subroutine gtx_chopper
  end interface
  !
  interface
    subroutine gtx_weight(out,cxa,cya,cxb,cyb,up)
      use gtv_types
      use gtv_graphic
      !---------------------------------------------------------------------
      ! @ private
      !  Thickness generator (Suited for pencil plotters)
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out      ! Input gt_display
      real*4,           intent(in)    :: cxa,cya  ! X,Y coordinate of start point
      real*4,           intent(in)    :: cxb,cyb  ! X,Y coordinate of end point
      logical,          intent(in)    :: up       ! Force goto (CXA,CYA) pen up
    end subroutine gtx_weight
  end interface
  !
  interface
    subroutine attach_greg_values(dir,tab_greg,error)
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Update the input Greg values and save them in the input directory
      ! descriptor.
      !---------------------------------------------------------------------
      type(gt_directory), intent(inout) :: dir       ! Directory
      type(greg_values),  intent(inout) :: tab_greg  ! Greg values
      logical,            intent(inout) :: error     ! Logical error flag
    end subroutine attach_greg_values
  end interface
  !
  interface
    subroutine init_lut(error)
      use gtv_protocol
      use gtv_plot
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Initialize a look-up-table for colours
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine init_lut
  end interface
  !
  interface
    subroutine load_lut(map,reuse,map_size,color,background,error)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Load the appropriate LUT in the input 'map' colormap.
      !---------------------------------------------------------------------
      type(gt_lut),     intent(inout) :: map         !
      logical,          intent(in)    :: reuse       ! Reuse current user's colormap?
      integer(kind=4),  intent(in)    :: map_size    !
      logical,          intent(in)    :: color       ! If not reuse, generate colors?
      integer(kind=4),  intent(in)    :: background  !
      logical,          intent(inout) :: error       !
    end subroutine load_lut
  end interface
  !
  interface
    subroutine gtl_lutpen(line,error)
      use gtv_buffers
      use gtv_protocol
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !  GTVL\LUT [Argum]
      !  1        [/PEN]
      !  2        [/EDIT]
      !  3        [/REVERSE]
      !  4        [/BLANK]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_lutpen
  end interface
  !
  interface
    subroutine gtl_lut(line,error)
      use gildas_def
      use gbl_message
      use gtv_plot
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   LUT [Argum]  (no option)
      ! LOADS a color table read in a file written in free format,
      !   or defined by the Hue, Saturation, Value
      !   or Red Green Blue arrays,
      ! depending upon the current representation.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_lut
  end interface
  !
  interface
    subroutine gtl_lutblank(line,error)
      use gildas_def
      use gbl_message
      use gtv_plot
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   LUT /BLANK Color
      ! Customize the blanking color, independently of the current lut.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_lutblank
  end interface
  !
  interface
    subroutine gt_lut_list(error)
      use gbl_message
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  List all the LUTs available to the user.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gt_lut_list
  end interface
  !
  interface
    subroutine gt_lut_segdata(error)
      use gtv_tree
      use gtv_buffers
      use gtv_plot
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Save the current bitmap color Look Up Table into the METACODE
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gt_lut_segdata
  end interface
  !
  interface
    subroutine gti_lut(out,lut)
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the colormap Look Up Table from the metacode and use it for the
      ! display. Starting from this point, next drawn images will use this
      ! colormap instead of the current working colormap ('gbl_colormap').
      !---------------------------------------------------------------------
      type(gt_display), intent(in)    :: out  ! Output device
      type(gt_lut),     intent(inout) :: lut  ! LUT in the metacode
    end subroutine gti_lut
  end interface
  !
  interface
    subroutine protocol_loadlut(dev,map,is_default)
      use gtv_plot
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Fonction d'aiguillage du chargement d'une palette en fonction du
      ! protocole
      !---------------------------------------------------------------------
      type(gt_device), intent(in)    :: dev         ! Output device
      type(gt_lut),    intent(inout) :: map         ! Colormap
      logical,         intent(in)    :: is_default  ! Is it the default/global colormap?
    end subroutine protocol_loadlut
  end interface
  !
  interface
    subroutine edit_lut
      use gtv_protocol
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   LUT /EDIT
      !---------------------------------------------------------------------
      !
    end subroutine edit_lut
  end interface
  !
  interface
    subroutine gt_lut_default(lut,size)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the input LUT with the default color LUT. The blanking is not
      ! taken into account here.
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: lut   ! The LUT to be filled
      integer(kind=4), intent(in)    :: size  ! Size of LUT
    end subroutine gt_lut_default
  end interface
  !
  interface
    subroutine gt_lut_color(lut,size)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the input LUT with the "color" LUT (circular hue value, from
      ! red to red). The blanking is not taken into account here.
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: lut   ! The LUT to be filled
      integer(kind=4), intent(in)    :: size  ! Size of LUT
    end subroutine gt_lut_color
  end interface
  !
  interface
    subroutine gt_lut_rgb(lut,size,rmin,rmax,gmin,gmax,bmin,bmax)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the input LUT with the R, G and B levels growing linearly from
      ! 0. to Rmax, Gmax and Bmax respectively. The blanking is not taken
      ! into account here.
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: lut        ! The LUT to be filled
      integer(kind=4), intent(in)    :: size       ! Size of LUT
      real(kind=4),    intent(in)    :: rmin,rmax  ! R extrema
      real(kind=4),    intent(in)    :: gmin,gmax  ! G extrema
      real(kind=4),    intent(in)    :: bmin,bmax  ! B extrema
    end subroutine gt_lut_rgb
  end interface
  !
  interface
    subroutine gt_lut_bluered(lut,size)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Program-defined BLUERED Look-Up-Table.
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: lut        ! The LUT to be filled
      integer(kind=4), intent(in)    :: size       ! Size of LUT
    end subroutine gt_lut_bluered
  end interface
  !
  interface
    subroutine gt_lut_fromfile(name_in,cmap,ncol,error)
      use gildas_def
      use gbl_message
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name_in  ! File name
      type(gt_lut),     intent(inout) :: cmap     ! Colormap to be filled
      integer(kind=4),  intent(in)    :: ncol     ! Number of colors in the colormap
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine gt_lut_fromfile
  end interface
  !
  interface
    subroutine gt_lut_resample(cmapin,sizein,cmapout,sizeout,interpolate)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Resample the input colormap into the output colormap such as:
      !  cmapout(1) = cmapin(1)
      !  cmapout(sizeout) = cmapin(sizein)
      ! and the intermediate values are either interpolated or nearest
      ! neighbour.
      !---------------------------------------------------------------------
      type(gt_lut),    intent(in)    :: cmapin       !
      integer(kind=4), intent(in)    :: sizein       !
      type(gt_lut),    intent(inout) :: cmapout      !
      integer(kind=4), intent(in)    :: sizeout      !
      logical,         intent(in)    :: interpolate  !
    end subroutine gt_lut_resample
  end interface
  !
  interface
    subroutine gt_lut_revert(cmap,ncol,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: cmap   ! The color map to revert
      integer(kind=4), intent(in)    :: ncol   ! The number of elements to revert
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gt_lut_revert
  end interface
  !
  interface
    subroutine gt_lut_alloc(lut,size,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Allocate the arrays of the input 'gt_lut' instance
      !---------------------------------------------------------------------
      type(gt_lut),    intent(inout) :: lut    !
      integer(kind=4), intent(in)    :: size   ! Requested lut size
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine gt_lut_alloc
  end interface
  !
  interface
    subroutine gt_lut_dealloc(lut,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Deallocate the arrays of the input 'gt_lut' instance
      !---------------------------------------------------------------------
      type(gt_lut), intent(inout) :: lut    !
      logical,      intent(inout) :: error  ! Logical error flag
    end subroutine gt_lut_dealloc
  end interface
  !
  interface
    subroutine gt_lut_fromeditor(r,g,b,sizein,error)
      use gbl_message
      use gtv_plot
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  Update the global colormap.
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)    :: r(*)    ! R values
      real(kind=4),    intent(in)    :: g(*)    ! G values
      real(kind=4),    intent(in)    :: b(*)    ! B values
      integer(kind=4), intent(in)    :: sizein  ! LUT size, without the blanking color
      logical,         intent(inout) :: error   ! Logical error flag
    end subroutine gt_lut_fromeditor
  end interface
  !
  interface
    subroutine gtl_metacode(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Support for command METACODE [IMPORT|EXPORT] [...]
      !
      ! Load or save in an binary file a sub-tree of the metacode.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_metacode
  end interface
  !
  interface
    subroutine meta_export(line,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Support for command METACODE EXPORT File [Dir]
      !
      ! Permet de sauver dans un fichier binaire une sous arborescence du
      ! metacode.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine meta_export
  end interface
  !
  interface
    recursive subroutine meta_export_dir(dir,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_directory), intent(in)    :: dir    ! Directory to dump
      integer(kind=4),    intent(in)    :: lun    ! Logical unit
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine meta_export_dir
  end interface
  !
  interface
    subroutine meta_export_seg(segm,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment), intent(in)    :: segm   ! Segment to dump
      integer(kind=4),  intent(in)    :: lun    ! Logical unit
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine meta_export_seg
  end interface
  !
  interface
    subroutine meta_export_segdata(segdata,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment_data), intent(in)    :: segdata  ! Segment data to dump
      integer(kind=4),       intent(in)    :: lun      ! Logical unit
      logical,               intent(inout) :: error    ! Logical error flag
    end subroutine meta_export_segdata
  end interface
  !
  interface
    subroutine meta_export_polyline(segdata,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment_data), intent(in)    :: segdata  ! Segment data to dump
      integer(kind=4),       intent(in)    :: lun      ! Logical unit
      logical,               intent(inout) :: error    ! Logical error flag
    end subroutine meta_export_polyline
  end interface
  !
  interface
    subroutine meta_export_image(segima,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_image),  intent(in)    :: segima  ! Image data to dump
      integer(kind=4), intent(in)    :: lun     ! Logical unit
      logical,         intent(inout) :: error   ! Logical error flag
    end subroutine meta_export_image
  end interface
  !
  interface
    subroutine meta_export_lut(segdata,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      type(gt_segment_data), intent(in)    :: segdata  ! Segment data to dump
      integer(kind=4),       intent(in)    :: lun      ! Logical unit
      logical,               intent(inout) :: error    ! Logical error flag
    end subroutine meta_export_lut
  end interface
  !
  interface
    subroutine meta_import(line,error)
      use gildas_def
      use gbl_message
      use gtv_buffers
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   METACODE IMPORT File [DirName] [/PATH LoadPath]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine meta_import
  end interface
  !
  interface
    subroutine meta_import_load(parent,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: parent  ! Directory to be filled
      integer(kind=4),    intent(in)    :: lun     ! Logical unit opened on the file
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_load
  end interface
  !
  interface
    recursive subroutine meta_import_dir(parent,lun,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: parent  ! Parent directory
      integer(kind=4),    intent(in)    :: lun     ! Logical unit opened on the file
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_dir
  end interface
  !
  interface
    subroutine meta_import_seg(parent,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_directory), pointer       :: parent  ! Parent directory
      integer(kind=4),    intent(in)    :: lun     ! Logical unit opened on the file
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_seg
  end interface
  !
  interface
    recursive subroutine meta_import_segdata(new,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_segment_data), pointer       :: new     ! New data segment to be filled
      integer(kind=4),       intent(in)    :: lun     ! Logical unit opened on the file
      logical,               intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_segdata
  end interface
  !
  interface
    subroutine meta_import_polyline(new,kind,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_segment_data), pointer       :: new     ! New data segment to be filled
      integer(kind=4),       intent(in)    :: kind    ! Kind of polyline
      integer(kind=4),       intent(in)    :: lun     ! Logical unit opened on the file
      logical,               intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_polyline
  end interface
  !
  interface
    subroutine meta_import_image(lun,error)
      use gbl_message
      use gtv_bitmap
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun    ! Logical unit opened on the file
      logical,         intent(inout) :: error  ! Logical error flag
    end subroutine meta_import_image
  end interface
  !
  interface
    subroutine meta_import_lut(new,kind,lun,error)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_segment_data), pointer       :: new     ! New data segment to be filled
      integer(kind=4),       intent(in)    :: kind    ! Kind of polyline
      integer(kind=4),       intent(in)    :: lun     ! Logical unit opened on the file
      logical,               intent(inout) :: error   ! Logical error flag
    end subroutine meta_import_lut
  end interface
  !
  interface
    subroutine gtl_mkdir(line,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   CREATE DIRECTORY nomdir
      !   1      [/PLOT_PAGE SizeX SizeY]
      !   2      [/PIXEL Px Py]
      !   5      [/SIZE SizeX SizeY]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine gtl_mkdir
  end interface
  !
  interface
    subroutine gtl_mkdir_topdir(line,nomdir,error)
      use gbl_message
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   CREATE DIRECTORY DirName
      ! 1 [/PLOT_PAGE Sx Sy]
      ! 5 [/SIZE Sx Sy]  (obsolete)
      ! Create a top-level directory
      !---------------------------------------------------------------------
      character(len=*),   intent(in)    :: line    ! Command line
      character(len=*),   intent(in)    :: nomdir  ! The new directory name
      logical,            intent(inout) :: error   ! Error flag
    end subroutine gtl_mkdir_topdir
  end interface
  !
  interface
    subroutine gtv_mkdir_topwindow(topdir,xdisplay,error)
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Create the initial window for a top directory for graphical displays
      ! ---
      !  Thread status: safe to call from main thread.
      !---------------------------------------------------------------------
      type(gt_directory), intent(inout) :: topdir    ! The top directory
      type(x_output),     intent(in)    :: xdisplay  ! The new window descriptor
      logical,            intent(inout) :: error     ! Logical error flag
    end subroutine gtv_mkdir_topwindow
  end interface
  !
  interface
    subroutine gtopen(type,device,devopt,haut,larg,ncol,error)
      use gildas_def
      use gtv_buffers
      use gtv_graphic
      use gtv_plot
      use gtv_protocol
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Opens a specific Device of given Type, eventually using options for
      ! setting parameters. Each device receives a unique identifier IDENT,
      ! which is used internally by the library.
      !
      !  TYPE  The type of Graphics device to be used
      !
      !  DEVICE  Optional argument to indicate to which device the plot must
      !          be sent. Default is DEFOUT(IDENT) for supposedly interactive
      !          devices (usually TT:), and BENSON.VEC for Metacode files.
      !
      !  DEVOPT  Optional string to define some modes for specific devices
      !---------------------------------------------------------------------
      character(len=*)                :: type    ! intent(in)
      character(len=*)                :: device  ! intent(in)
      character(len=*), intent(inout) :: devopt  !
      integer                         :: haut    !
      integer                         :: larg    !
      integer                         :: ncol    ! ignored
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine gtopen
  end interface
  !
  interface
    subroutine gtz_open(output,error)
      use gtv_types
      use gtv_protocol
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! GTVIRT, Z Routine
      !   Open a graphic device
      !---------------------------------------------------------------------
      type(gt_display)       :: output  ! Output instance
      logical, intent(inout) :: error   !
    end subroutine gtz_open
  end interface
  !
  interface
    subroutine gtz_close(output)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! GTVIRT Z routine
      ! Close a graphic device
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! Output instance
    end subroutine gtz_close
  end interface
  !
  interface
    subroutine gtx_pen(output,ipen,userpens)
      use gbl_message
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Set the color to be used by the forthcoming segments (lines,
      ! polygons, but not images).
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output    ! Output instance
      integer(kind=4),  intent(in)    :: ipen      !
      type(gt_lut),     intent(in)    :: userpens  ! Set of user pens (PEN /LUT)
    end subroutine gtx_pen
  end interface
  !
  interface
    subroutine gtv_pencol_id2rgb(output,userpens,ipen,r,g,b,do_neg)
      use gbl_message
      use gtv_colors
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Convert a pen identifier (number) into RGB values (range [0:255]).
      ! Be careful this can be context dependent (e.g. foreground/background
      ! colors).
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: output    ! Output instance
      type(gt_lut),     intent(in)  :: userpens  ! Set of user pens (PEN /LUT)
      integer(kind=4),  intent(in)  :: ipen      !
      integer(kind=4),  intent(out) :: r,g,b     !
      logical,          intent(out) :: do_neg    !
    end subroutine gtv_pencol_id2rgb
  end interface
  !
  interface
    subroutine init_pen(error)
      use gtv_protocol
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      !  Initialize the look-up-table for coloured Pens
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine init_pen
  end interface
  !
  interface
    subroutine load_pen(out,pen,reuse,color,error)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Load the appropriate LUT in the input 'pen' set of pen.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)    :: out    !
      type(gt_lut),     intent(inout) :: pen    !
      logical,          intent(in)    :: reuse  ! Reuse current user's colormap?
      logical,          intent(in)    :: color  ! If not reuse, generate colors?
      logical,          intent(inout) :: error  !
    end subroutine load_pen
  end interface
  !
  interface
    subroutine pen_lut(line,error)
      use gildas_def
      use gtv_plot
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command:
      !   LUT [Argum] /PEN
      ! LOADS a color table read in a file written in free format,
      !   or defined by the Hue, Saturation, Value
      !   or Red Green Blue arrays,
      ! depending upon the current representation.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine pen_lut
  end interface
  !
  interface
    subroutine gtv_penlut(error)
      use gtv_tree
      use gtv_buffers
      use gtv_plot
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Defines the color pens Look Up Table in the METACODE
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gtv_penlut
  end interface
  !
  interface
    subroutine gt_penlut_segdata(error)
      use gtv_tree
      use gtv_buffers
      use gtv_plot
      use gtv_graphic
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Save the current pen color Look Up Table into the METACODE
      ! ---
      !   Threads status: thread safe
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine gt_penlut_segdata
  end interface
  !
  interface
    subroutine gti_penlut(out,lut)
      use gtv_tree
      use gtv_types
      use gtv_graphic
      use gtv_plot
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Defines the color pens Look Up Table on the DISPLAY
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out  ! Input gt_display
      type(gt_lut),     intent(in) :: lut  !
    end subroutine gti_penlut
  end interface
  !
  interface
    subroutine gtx_plot(out,x,y,jmode)
      use gildas_def
      use gtv_types
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  This is a PLOT routine, simulating the calcomp PLOT routine, X,Y
      ! are the co-ordinates (World units) of a plotter point, and
      !    MODE=3 means plot a dark vector (pen up) to X,Y
      !    MODE=2 means plot a real vector (pen down) to X,Y
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out    ! Input gt_display
      real*4,           intent(in)    :: x,y    ! World units
      integer,          intent(in)    :: jmode  !
    end subroutine gtx_plot
  end interface
  !
  interface
    subroutine png_open(output,error,dir)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Open the PNG output file.
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout)        :: output  ! Output instance
      logical,            intent(inout)        :: error   ! Logical error flag
      type(gt_directory), intent(in), optional :: dir     ! The directory to display, or
    end subroutine png_open
  end interface
  !
  interface
    subroutine png_close(output,desc)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Close the current PNG device, i.e. copy the rows to the file
      ! opened by the C routines, and close it.
      !---------------------------------------------------------------------
      type(gt_display),       intent(inout) :: output
      real(kind=4), optional, intent(in)    :: desc(4)
    end subroutine png_close
  end interface
  !
  interface
    subroutine png_base64(output,desc)
      use gildas_def
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Dump the PNG currently in memory with base 64 encoding. This makes
      ! sense only for memory only PNG, which have no associated file on
      ! disk.
      !  Because the output is not the (binary) PNG but an encoded form,
      ! a header and footer are added.
      !---------------------------------------------------------------------
      type(gt_display),       intent(in) :: output
      real(kind=4), optional, intent(in) :: desc(4)
    end subroutine png_base64
  end interface
  !
  interface
    subroutine png_hard(out)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out  !
    end subroutine png_hard
  end interface
  !
  interface
    subroutine png_pen_negative(out)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current pen as negative, i.e. invert pixels below
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out  ! The PNG instance
    end subroutine png_pen_negative
  end interface
  !
  interface
    subroutine png_pen_rgb(out,r,g,b)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current pen from the input R G B values (0-255)
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out    ! The PNG instance
      integer(kind=4),  intent(in)    :: r,g,b  !
    end subroutine png_pen_rgb
  end interface
  !
  interface
    subroutine png_weigh(out,width)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current pen width. Note that we may get a 0 width, which
      ! means "hairline" (smallest possible on the device)
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out    ! The PNG instance
      integer(kind=4),  intent(in)    :: width  ! New pen width (pixels)
    end subroutine png_weigh
  end interface
  !
  interface
    subroutine png_moveto(out,x,y)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Move the pen to the input location
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out  ! Input gt_display
      real(kind=4),     intent(in)    :: x,y  ! New pen coordinates, in world coordinates
    end subroutine png_moveto
  end interface
  !
  interface
    subroutine png_lineto(out,x,y)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Draw a line from current pen position to input location, simulating
      ! its thickness if needed.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out  ! Input gt_display
      real(kind=4),     intent(in)    :: x,y  ! Draw line down to this location, in world coordinates
    end subroutine png_lineto
  end interface
  !
  interface
    subroutine png_wline(out,xr1,yr1,xr2,yr2)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a line from (x1,y1) to (x2,y2), simulating its width by
      ! drawing several 1-pixel lines
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out      ! Input gt_display
      real(kind=4),     intent(in) :: xr1,yr1  !
      real(kind=4),     intent(in) :: xr2,yr2  !
    end subroutine png_wline
  end interface
  !
  interface
    subroutine png_line(out,x1,y1,x2,y2)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a 1 pixel-wide line from (x1,y1) to (x2,y2), using Bresenham
      ! algorithm. See e.g.
      !  http://tech-algorithm.com/articles/drawing-line-using-bresenham-algorithm/
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out    ! Input gt_display
      integer(kind=4),  intent(in) :: x1,y1  !
      integer(kind=4),  intent(in) :: x2,y2  !
    end subroutine png_line
  end interface
  !
  interface
    subroutine png_fill(out,n,xr,yr)
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      ! Fill the input polygon
      !---------------------------------------------------------------------
      ! ZZZ Nettoyer cette routine (noms, etc)
      type(gt_display), intent(inout) :: out    ! Input gt_display
      integer,          intent(in)    :: n      ! Number of summits
      real*4,           intent(in)    :: xr(n)  ! X coordinates, in world units
      real*4,           intent(in)    :: yr(n)  ! Y coordinates, in world units
    end subroutine png_fill
  end interface
  !
  interface
    subroutine png_points(out,n,xr,yr)
      use gtv_types
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !   Draw dot-points at input coordinates (world units)
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out    ! Input gt_display
      integer(kind=4),  intent(in)    :: n      ! Number of summits
      real(kind=4),     intent(in)    :: xr(n)  ! X coordinates, in world units
      real(kind=4),     intent(in)    :: yr(n)  ! Y coordinates, in world units
    end subroutine png_points
  end interface
  !
  interface
    subroutine gti_pngimage(out,image)
      use gildas_def
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_types
      use gtv_tree
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Load the image to be drawn in the PNG device
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out    ! Input gt_display
      type(gt_image),   intent(in)    :: image  ! Image instance
    end subroutine gti_pngimage
  end interface
  !
  interface
    subroutine gti_pngmap(image,output,bitmap,lut)
      use gildas_def
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Draw the image in the PNG device.
      !---------------------------------------------------------------------
      type(gt_image),   intent(in)    :: image   ! The image to be displayed
      type(gt_display), intent(inout) :: output  ! gt_display instance
      type(gt_bitmap),  intent(inout) :: bitmap  !
      type(gt_lut),     intent(in)    :: lut     ! Colormap to use
    end subroutine gti_pngmap
  end interface
  !
  interface
    subroutine png_image_rgb_color(output,bitmap)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Draw the image in the PNG device, in color scale. The input RGB
      ! intensities are rescaled from 0 to 255.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! gt_display instance
      type(gt_bitmap),  intent(in)    :: bitmap  !
    end subroutine png_image_rgb_color
  end interface
  !
  interface
    subroutine png_image_rgb_grey(output,bitmap)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Draw the image in the PNG device, in color scale. The input RGB
      ! intensities are rescaled from 0 to 255.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! gt_display instance
      type(gt_bitmap),  intent(in)    :: bitmap  !
    end subroutine png_image_rgb_grey
  end interface
  !
  interface
    subroutine png_image_ind_color(output,bitmap,lut)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Draw the image in the PNG device, in color scale. The single-scale
      ! intensities are split in 3 RGB intensities from 0 to 255.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! gt_display instance
      type(gt_bitmap),  intent(in)    :: bitmap  !
      type(gt_lut),     intent(in)    :: lut     ! Colormap to use
    end subroutine png_image_ind_color
  end interface
  !
  interface
    subroutine png_image_ind_grey(output,bitmap,lut)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Draw the image in the PNG device, in grey scale
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! gt_display instance
      type(gt_bitmap),  intent(in)    :: bitmap  !
      type(gt_lut),     intent(in)    :: lut     ! Colormap to use
    end subroutine png_image_ind_grey
  end interface
  !
  interface
    subroutine png_image_inquire(map_size,map_offset,map_color,map_static)
      !---------------------------------------------------------------------
      ! @ private
      ! ZZZ Not sure all these really have to be returned...
      !---------------------------------------------------------------------
      integer, intent(out) :: map_size    ! Desired scale dynamics
      integer, intent(out) :: map_offset  !
      logical, intent(out) :: map_color   ! Is a color or grey map?
      logical, intent(out) :: map_static  !
    end subroutine png_image_inquire
  end interface
  !
  interface
    subroutine png_point(out,x,y)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Draw the pixel at input coordinates (pixel units)
      !---------------------------------------------------------------------
      type(gt_display)             :: out  ! Output instance
      integer(kind=4),  intent(in) :: x,y  ! Pixel coordinates, in pixel units
    end subroutine png_point
  end interface
  !
  interface
    subroutine gti_points(out,poly)
      use gtv_types
      use gtv_tree
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a list of dot points
      !---------------------------------------------------------------------
      type(gt_display),  intent(inout) :: out   ! Input gt_display
      type(gt_polyline), intent(in)    :: poly  !
    end subroutine gti_points
  end interface
  !
  interface
    subroutine gt_polyl(out,poly)
      use gtv_protocol
      use gtv_graphic
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Generic routine: draw the input polyline in the display 'out',
      ! whereever this polyline comes from.
      !---------------------------------------------------------------------
      type(gt_display),  intent(inout) :: out   ! Input gt_display
      type(gt_polyline), intent(in)    :: poly  ! The polyline instance
    end subroutine gt_polyl
  end interface
  !
  interface
    subroutine gtx_clip(out,x,y,c)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Routine for the clipping algorithm; called from GTI_CLIP only. C
      ! is a 4 bit code indicating the relationship between point (X,Y) and
      ! the window boundaries; 0 implies the point is within the window.
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: out  ! Input gt_display
      real*4,           intent(in)  :: x    !
      real*4,           intent(in)  :: y    !
      integer,          intent(out) :: c    !
    end subroutine gtx_clip
  end interface
  !
  interface
    subroutine ps_open(output,dir,error)
      use gildas_def
      use gtv_types
      use gtv_protocol
      use gtv_tree
      use gtv_buffers
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display)                  :: output  ! Output instance
      type(gt_directory), intent(in)    :: dir     ! The directory to print
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine ps_open
  end interface
  !
  interface
    subroutine ps_prolog_ps(output,dir)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display)               :: output  ! Output instance
      type(gt_directory), intent(in) :: dir     ! The directory to print
    end subroutine ps_prolog_ps
  end interface
  !
  interface
    subroutine ps_prolog_eps(output,dir)
      use gbl_message
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display)               :: output  ! Output instance
      type(gt_directory), intent(in) :: dir     ! The directory to print
    end subroutine ps_prolog_eps
  end interface
  !
  interface
    subroutine ps_prolog_gagheader(error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine ps_prolog_gagheader
  end interface
  !
  interface
    subroutine ps_pen_rgb(r,g,b)
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current pen from the input R G B values (0-255)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: r,g,b
    end subroutine ps_pen_rgb
  end interface
  !
  interface
    subroutine ps_dash (dashed,pattern)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical :: dashed                 !
      real :: pattern(4)                !
    end subroutine ps_dash
  end interface
  !
  interface
    subroutine ps_weigh(width)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: width  ! Line width, in centimeter
    end subroutine ps_weigh
  end interface
  !
  interface
    subroutine ps_moveto(x,y)
      !---------------------------------------------------------------------
      ! @ private
      ! Elemental 'moveto'
      !---------------------------------------------------------------------
      real, intent(in) :: x  ! X value
      real, intent(in) :: y  ! Y value
    end subroutine ps_moveto
  end interface
  !
  interface
    subroutine ps_lineto(x,y)
      !---------------------------------------------------------------------
      ! @ private
      ! Elemental 'lineto'
      !---------------------------------------------------------------------
      real, intent(in) :: x  ! X value
      real, intent(in) :: y  ! Y value
    end subroutine ps_lineto
  end interface
  !
  interface
    subroutine ps_close
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      !
    end subroutine ps_close
  end interface
  !
  interface
    subroutine ps_trace(trace)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: trace
    end subroutine ps_trace
  end interface
  !
  interface
    subroutine ps_stroke
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      !
    end subroutine ps_stroke
  end interface
  !
  interface
    subroutine ps_out(what)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=1) :: what           !
    end subroutine ps_out
  end interface
  !
  interface
    subroutine ps_write(buf,n,olun)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer :: n                      !
      integer(kind=1) :: buf(n)         !
      integer :: olun                   !
    end subroutine ps_write
  end interface
  !
  interface
    subroutine ps_image_inquire(map_size,map_offset,map_color,map_static)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer, intent(out) :: map_size    !
      integer, intent(out) :: map_offset  !
      logical, intent(out) :: map_color   !
      logical, intent(out) :: map_static  !
    end subroutine ps_image_inquire
  end interface
  !
  interface
    subroutine ps_fill(n,x,y)
      !---------------------------------------------------------------------
      ! @ private
      ! Draw a filled polygon in the PostScript
      !---------------------------------------------------------------------
      integer, intent(in) :: n          ! Number of summits
      real,    intent(in) :: x(n)       ! X array
      real,    intent(in) :: y(n)       ! Y array
    end subroutine ps_fill
  end interface
  !
  interface
    subroutine ps_points(n,x,y)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Draw a list of dot-points in the PostScript
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: n     ! Number of summits
      real(kind=4),    intent(in) :: x(n)  ! X array
      real(kind=4),    intent(in) :: y(n)  ! Y array
    end subroutine ps_points
  end interface
  !
  interface
    subroutine gti_psimage(out,image)
      use gildas_def
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_types
      use gtv_tree
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Specifique au protole PostScript
      !---------------------------------------------------------------------
      type(gt_display)           :: out    ! Output instance
      type(gt_image), intent(in) :: image  ! Image instance
    end subroutine gti_psimage
  end interface
  !
  interface
    subroutine ps_box(box)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real :: box(4)                    !
    end subroutine ps_box
  end interface
  !
  interface
    subroutine gti_psmap(image,output,bitmap,lut)
      use gildas_def
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_image),   intent(in)    :: image   ! The image to be displayed
      type(gt_display), intent(in)    :: output  ! gt_display instance
      type(gt_bitmap),  intent(inout) :: bitmap  !
      type(gt_lut),     intent(in)    :: lut     ! Colormap to use
    end subroutine gti_psmap
  end interface
  !
  interface
    subroutine ps_image_rgb_color(bitmap)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_bitmap), intent(in) :: bitmap  !
    end subroutine ps_image_rgb_color
  end interface
  !
  interface
    subroutine ps_image_rgb_grey(bitmap)
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_bitmap), intent(in) :: bitmap  !
    end subroutine ps_image_rgb_grey
  end interface
  !
  interface
    subroutine ps_image_ind_color(bitmap,lut)
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_bitmap), intent(in) :: bitmap  !
      type(gt_lut),    intent(in) :: lut     ! Colormap to use
    end subroutine ps_image_ind_color
  end interface
  !
  interface
    subroutine ps_image_ind_grey(bitmap,lut)
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_bitmap), intent(in) :: bitmap  !
      type(gt_lut),    intent(in) :: lut     ! Colormap to use
    end subroutine ps_image_ind_grey
  end interface
  !
  interface
    subroutine ps_hard(out)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: out  !
    end subroutine ps_hard
  end interface
  !
  interface
    subroutine gtl_refresh(line,error)
      use gbl_message
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    GTV\REFRESH [DirName [WinNum|ZOOM]]
      !  Redraw the active (or specified) X window.
      !
      !  X protocol is always refreshed on-demand of the system, so there
      ! should be no need for such a user request.
      !  However, images are implicitly created at depth 2, so what you see
      ! at first drawing (when segments are stacked in chronological order)
      ! is not what you see at subsequent redrawing. This is annoying and
      ! requires some action. One would think about implicit redraw when an
      ! image is plotted, but this would be very uneffecient (think about
      ! GO BIT). On the other hand, users are well used to this
      ! "bug-feature" (formerly known as ZOOM REFRESH), so we let them
      ! decide when they want a redraw.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line  ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine gtl_refresh
  end interface
  !
  interface
    subroutine set_attributs(output,attr_v)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Mise en place des differents attributs de trace.
      !---------------------------------------------------------------------
      type(gt_display),      intent(inout) :: output  ! Output instance
      type(gt_segment_attr), intent(in)    :: attr_v  !
    end subroutine set_attributs
  end interface
  !
  interface
    subroutine set_dash(output,z_dash)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Fixe le parametre de pointille, pour les protocoles capables de le
      ! faire tous seuls.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! Output instance
      integer*4,        intent(in)    :: z_dash  !
    end subroutine set_dash
  end interface
  !
  interface
    subroutine set_weigh(output,z_weigh)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! fixe le parametre de graisse, pour les protocoles capables de le
      ! faire tous seuls.
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output   ! Output instance
      real(kind=4),     intent(in)    :: z_weigh  !
    end subroutine set_weigh
  end interface
  !
  interface
    function set_weight_pixel(output)
      use gtv_buffers
      use gtv_plot
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Convert the line width from physical size (cm) to pixel
      !---------------------------------------------------------------------
      integer(kind=4) :: set_weight_pixel  ! Function value on return
      type(gt_display), intent(in) :: output  ! Output instance
    end function set_weight_pixel
  end interface
  !
  interface
    subroutine set_color(output,z_color)
      use gtv_types
      use gtv_graphic
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      ! fixe le parametre de couleur
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output   ! Output instance
      integer*4,        intent(in)    :: z_color  !
    end subroutine set_color
  end interface
  !
  interface
    subroutine svg_open(output,error,dir)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display),   intent(inout)        :: output  ! Output instance
      logical,            intent(inout)        :: error   !
      type(gt_directory), intent(in), optional :: dir     ! The directory to display
    end subroutine svg_open
  end interface
  !
  interface
    subroutine svg_zopen(fich,iunit,px1,px2,py1,py2,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: fich   !
      integer(kind=4),  intent(in)    :: iunit  !
      integer(kind=4),  intent(in)    :: px1    ! First pixel in X
      integer(kind=4),  intent(in)    :: px2    ! Last  pixel in X
      integer(kind=4),  intent(in)    :: py1    ! First pixel in Y
      integer(kind=4),  intent(in)    :: py2    ! Last  pixel in Y
      logical,          intent(inout) :: error  !
    end subroutine svg_zopen
  end interface
  !
  interface
    subroutine svg_pen_rgb(r,g,b)
      !---------------------------------------------------------------------
      ! @ private
      ! Set the current pen color from the R G B values (0-255)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: r,g,b
    end subroutine svg_pen_rgb
  end interface
  !
  interface
    subroutine svg_setfill(fill)
      !---------------------------------------------------------------------
      ! @ private
      !  Set the polygon filling status
      !---------------------------------------------------------------------
      logical, intent(in) :: fill
    end subroutine svg_setfill
  end interface
  !
  interface
    subroutine svg_dash(out,dashed,pattern)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      logical,          intent(in) :: dashed
      real(kind=4),     intent(in) :: pattern(4)
    end subroutine svg_dash
  end interface
  !
  interface
    subroutine svg_weigh(out,width)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      real(kind=4),     intent(in) :: width
    end subroutine svg_weigh
  end interface
  !
  interface
    subroutine svg_group_open
      !---------------------------------------------------------------------
      ! @ private
      !  Open a new group containing the next graphical elements.
      !---------------------------------------------------------------------
      !
    end subroutine svg_group_open
  end interface
  !
  interface
    subroutine svg_group_close
      !---------------------------------------------------------------------
      ! @ private
      !  Close the current group
      !---------------------------------------------------------------------
      !
    end subroutine svg_group_close
  end interface
  !
  interface
    subroutine svg_moveto(out,x,y,fill)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      real(kind=4),     intent(in) :: x,y
      logical,          intent(in) :: fill  ! Start a filled polygon?
    end subroutine svg_moveto
  end interface
  !
  interface
    subroutine svg_lineto(out,x,y)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      real(kind=4),     intent(in) :: x,y
    end subroutine svg_lineto
  end interface
  !
  interface
    subroutine svg_close
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      !
      ! Close current line
    end subroutine svg_close
  end interface
  !
  interface
    subroutine svg_stroke
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      !
    end subroutine svg_stroke
  end interface
  !
  interface
    subroutine svg_fill(out,n,x,y)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      integer(kind=4),  intent(in) :: n
      real(kind=4),     intent(in) :: x(n)
      real(kind=4),     intent(in) :: y(n)
    end subroutine svg_fill
  end interface
  !
  interface
    subroutine svg_points(out,n,xr,yr)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in) :: out
      integer(kind=4),  intent(in) :: n
      real(kind=4),     intent(in) :: xr(n)
      real(kind=4),     intent(in) :: yr(n)
    end subroutine svg_points
  end interface
  !
  interface
    subroutine svg_image_inquire(map_size,map_offset,map_color,map_static)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(out) :: map_size    !
      integer(kind=4), intent(out) :: map_offset  !
      logical,         intent(out) :: map_color   !
      logical,         intent(out) :: map_static  !
    end subroutine svg_image_inquire
  end interface
  !
  interface
    subroutine gti_svgimage(svgoutput,image,error)
      use gildas_def
      use gbl_message
      use gtv_bitmap
      use gtv_buffers
      use gtv_tree
      use gtv_plot
      !---------------------------------------------------------------------
      ! @ private
      ! Specifique au protocole SVG
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: svgoutput  ! The whole SVG output description
      type(gt_image),   intent(in)    :: image      ! Image instance
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine gti_svgimage
  end interface
  !
  interface
    subroutine svg_image_open(svgout,svgpos,desc)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gt_display), intent(in)  :: svgout
      real(kind=4),     intent(in)  :: svgpos(4)  ! Image position in the SVG device
      real(kind=4),     intent(out) :: desc(4)    ! Image description (reference position, size)
    end subroutine svg_image_open
  end interface
  !
  interface
    subroutine feuille_cherche(dir,nom,segm,found)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Recherche une feuille donnee en argument, dans un macrosegment
      ! repere par 'dir'. Rend found a vrai si la feuille existe, a faux
      ! sinon.
      !---------------------------------------------------------------------
      type(gt_directory)            :: dir    ! Starting directory
      character(len=*), intent(in)  :: nom    ! Name to search for
      type(gt_segment), pointer     :: segm   ! Segment found, if any
      logical,          intent(out) :: found  ! Found it?
    end subroutine feuille_cherche
  end interface
  !
  interface
    subroutine fils_cherche(dir,nom,trouve,found)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Recherche un fils donnee en argument, dans un macrosegment repere
      ! par 'dir'. Rend l'adresse de ce fils dans 'trouve'. Rend found a
      ! vrai si le fils existe, a faux sinon
      !---------------------------------------------------------------------
      type(gt_directory)              :: dir     ! Starting directory
      character(len=*),   intent(in)  :: nom     ! Name to search for
      type(gt_directory), pointer     :: trouve  ! Found address
      logical,            intent(out) :: found   ! Found it?
    end subroutine fils_cherche
  end interface
  !
  interface
    subroutine ret_pere(dir,trouve,error)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Partant de la directorie reperee par 'dir', on remonte jusqu'a son
      ! pere repere par 'trouve'. error est mis a True si on cherche a
      ! remonter au dela de root.
      !---------------------------------------------------------------------
      type(gt_directory)          :: dir     ! Starting directory
      type(gt_directory), pointer :: trouve  ! Found address
      logical,      intent(inout) :: error   ! Logical error flag
    end subroutine ret_pere
  end interface
  !
  interface
    subroutine decode_chemin(chemin,init,dir,isdir,segm,found)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Partant d'une directorie reperee par 'init', on cherche a
      ! atteindre un segment ou un macro segment specifie par chemin
      ! (relatif). Si le chemin est absolu, 'init' est ignore et on
      ! cherche depuis la racine.
      ! On peut aussi lire le directory courant et renvoyer son adresse si
      ! son nom correspond a chemin. Si on y arrive alors, le but est
      ! specifie par 'dir' et found est true; sinon found est false.
      ! isdir = .true. si le dernier element lu est un macrosegment, .false.
      ! sinon.
      !---------------------------------------------------------------------
      character(len=*),   intent(in)  :: chemin  ! Input path
      type(gt_directory), pointer     :: init    ! Directory to search from
      type(gt_directory), pointer     :: dir     ! Directory of the target
      logical,            intent(out) :: isdir   ! Target is a directory
      type(gt_segment),   pointer     :: segm    ! Pointer to the segment target, if not a directory
      logical,            intent(out) :: found   ! Target was found
    end subroutine decode_chemin
  end interface
  !
  interface
    subroutine next_nom(nom,ptr_deb,ptr_fin,last)
      use gtv_tree
      !---------------------------------------------------------------------
      ! @ private
      ! Rend dans ptr l'indice de fin d'un nom lu entre deux separateurs <
      ! last est vrai si c'est le dernier nom de la chaine de caracteres
      ! ATTENTION
      ! ptr_deb et ptr_fin sont des pointeurs dans la chaine d'ORIGINE
      ! (Chaine d'origine transmise par nom(ptr_deb:)
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: nom      ! String to parse
      integer,          intent(in)  :: ptr_deb  ! Offset to add to the returned index
      integer,          intent(out) :: ptr_fin  ! Index of the end of next name
      logical,          intent(out) :: last     ! No more name after this
    end subroutine next_nom
  end interface
  !
  interface
    subroutine cree_chemin_dir(start,chemin,lch)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return the absolute path from root '<' to directory given by 'start'
      !---------------------------------------------------------------------
      type(gt_directory)            :: start   ! Directory to be resolved
      character(len=*), intent(out) :: chemin  ! Output name (full path)
      integer,          intent(out) :: lch     ! Length of output
    end subroutine cree_chemin_dir
  end interface
  !
  interface
    subroutine cree_chemin_seg(segm,chemin,lch)
      use gtv_tree
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !   Return the absolute path from root '<' to the segment given by
      ! 'segm'.
      !---------------------------------------------------------------------
      type(gt_segment)              :: segm    ! Pointer to segment
      character(len=*), intent(out) :: chemin  ! Output name (full path)
      integer,          intent(out) :: lch     ! Length of output
    end subroutine cree_chemin_seg
  end interface
  !
  interface
    subroutine get_nomgrandpere(nom)
      use gtv_buffers
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Return the name of the 'grandpere' branch from current directory.
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: nom  ! Returned name
    end subroutine get_nomgrandpere
  end interface
  !
  interface
    subroutine gti_xforceupdate(output)
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Specifique au protocole X
      !---------------------------------------------------------------------
      type(gt_display) :: output  ! Output instance
    end subroutine gti_xforceupdate
  end interface
  !
  interface
    subroutine gti_ximage(output,image)
      use gildas_def
      use gtv_bitmap
      use gtv_buffers
      use gtv_plot
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Specifique au protocole X. Version MAC (support de PICT) plus bas
      !---------------------------------------------------------------------
      type(gt_display), intent(inout) :: output  ! Input gt_display
      type(gt_image),   target        :: image   ! Image instance
    end subroutine gti_ximage
  end interface
  !
  interface
    subroutine cwrite(output,chain,n)
      use gtv_types
      use gtv_protocol
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(gt_display) :: output        ! Output instance
      integer          :: n             ! Number of characters in chain
      character(len=*) :: chain         ! Input character string
    end subroutine cwrite
  end interface
  !
  interface
    subroutine gtl_zoom(line,error)
      use gtv_buffers
      use gtv_graphic
      use gtv_protocol
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !   Support for command:
      ! GTVL\ZOOM [Off|Ax Bx Ay By]
      !
      !   Map mouse button events (^, &, and * are returned by x_curs()) to
      ! keyboard events (" ", "Z", and "E", respectively).
      ! ---
      !  Thread status: safe for reading from main thread
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine gtl_zoom
  end interface
  !
  interface
    subroutine gti_zoom_interactive(dir,main_output,error)
      use gildas_def
      use gbl_message
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      ! Create the ZOOM window with the cursor
      !---------------------------------------------------------------------
      type(gt_directory)             :: dir          ! Directory where we perform the zoom
      type(gt_display), intent(in)   :: main_output  ! Window where we perform the zoom
      logical,         intent(inout) :: error        ! Logical error flag
    end subroutine gti_zoom_interactive
  end interface
  !
  interface
    subroutine new_zoom_window(dir,main_output,width,height,px1,px2,py1,py2,error)
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      use gtv_tree
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Open or reuse a zoom window attached to the input directory.
      ! Default size is 2/3 of the window we zoom in.
      ! ---
      !  Thread status: safe to call from main thread.
      !---------------------------------------------------------------------
      type(gt_directory)              :: dir          ! Directory where we attach the zoom window
      type(gt_display), intent(in)    :: main_output  ! Number of the window we zoom in
      integer(kind=4),  intent(in)    :: width        ! Zoom window width (0 if default)
      integer(kind=4),  intent(in)    :: height       ! Zoom window height (0 if default)
      real(kind=4),     intent(in)    :: px1,px2      ! X range for zoom window (world unit)
      real(kind=4),     intent(in)    :: py1,py2      ! Y range for zoom window (world unit)
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine new_zoom_window
  end interface
  !
  interface
    subroutine get_zoom_win(dir,win_num,zoomfound)
      use gildas_def
      use gtv_buffers
      use gtv_protocol
      use gtv_types
      !---------------------------------------------------------------------
      ! @ private
      !  Get the last zoom window number of input directory.
      ! ---
      !  Thread status: not thread safe. Calling routine must be thread
      ! safe for reading.
      !---------------------------------------------------------------------
      type(gt_directory)           :: dir        ! Input directory
      integer(kind=4), intent(out) :: win_num    ! Zoom window number
      logical,         intent(out) :: zoomfound  ! Found one zoom window?
    end subroutine get_zoom_win
  end interface
  !
  interface
    subroutine clear_zoom_win(dir,error)
      use gtv_types
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Clear the last zoom window of input directory. Nothing done if
      ! no zoom window is attached.
      !---------------------------------------------------------------------
      type(gt_directory)                :: dir    ! Input directory
      logical,            intent(inout) :: error  ! Logical error flag
    end subroutine clear_zoom_win
  end interface
  !
end module gtv_interfaces_private
