subroutine gtx_segm_0(error)
  use gildas_def
  use gtv_buffers
  use gtv_graphic
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtx_segm_0
  use gtv_protocol
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  Initialize the first segment "<" with default line attributes.
  !	WORD 1	Null pointer to previous segment
  !			Buffer number
  !			Address in buffer
  !	WORD 2	Undefined pointer to next segment  !Si si (0,0)
  !			Buffer number
  !			Address in buffer
  !	WORD 3	Default line attributes
  !			Line type 			1 byte
  !			Line width			1 byte
  !			Colour index			2 bytes
  !
  ! En plus de toutes ces bonnes choses faites par le Maitre ...
  ! GTX_SEGM_0 cree la racine de l'arborescence du metacode, initialise
  ! les pointeurs de chainage, cree le premier repertoire qui s'appelle
  ! TOP_DIR (definit par GTINIT), puis fait un CHANGE DIR TOP_DIR
  ! R. Gras
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTVIRT'
  logical :: isdir,found
  type(greg_values) :: tab_greg
  type(gt_segment), pointer :: segm
  type(gt_directory), pointer :: arrivee
  integer :: nc
  !
  ! Very first default values for pen attributes are now set at variable
  ! declaration. On subsequent calls to GTX_SEGM_0 (e.g. after a CLEAR
  ! WHOLE), we DO NOT want to reset ATTR. It MUST keep its current values
  ! to be consistent with the pen exposed in GreG. Only GreG should be
  ! allowed to change ATTR when it knows when ATTR has to be changed.
  !  attr(1) = 1                        ! Dashed 1
  !  attr(2) = 1                        ! Weight 1
  !  attr(3) = 0                        ! Colour 0
  !  attr(4) = 1                        ! First Level
  !
  call gtv_open_segments_for_writing_from_main()
  !
  ! 1) (Re)create the root directory, i.e. "<"
  root => gtv_newdirectory(error)
  if (error)  return
  !
  ! Reset the global pointers
  co_segment => null()
  co_segment_data => null()
  !
  ! Default line attributes
  root%gen%visible = .true.
  ! Undefined paper size since it has sense only in real trees.
  root%phys_size = 0.
  !
  ! Initialisation de l'arbre
  root%father => null()      ! Pointer on the father
  root%son_first => null()   ! Pointer on the first son
  root%son_last => null()    ! Pointer on the last son
  root%brother => null()     ! Pointer on the next brother
  root%leaf_first => null()  ! Pointer on the first leaf
  root%leaf_last => null()   ! Pointer on the last leaf
  root%segn = 0
  !
  ! Pointer to the "grandpere", defined as the directory in this tree which is
  ! just under the root. Since this is the root, the value is undefined
  root%ancestor => null()
  !
  ! Mise a jour du nom du segment
  root%gen%name = dir_root
  !
  ! Mise a jour de l'environnement graphique (Yen a pas he he)
  ! the graphical information in the directory descriptor has 3 parts:  the
  ! current graphical environment (ENV_GRAPH), the table of addresses of
  ! the graphical environments for all the windows in the directory (GENV_ARRAY),
  ! and the number of the current window and total number of windows (WINDOW(1)
  ! and WINDOW(2), respectively)
  !
  root%x%genv = 0
  root%x%genv_array = 0
  root%x%curwin = 0
  root%x%nbwin = 0
  !
  ! GREG constants
  if (flag_greg) call get_greg_values(tab_greg)
  call attach_greg_values(root,tab_greg,error)
  !
  ! 2) Set the global values
  polyl%n = 1
  polyl%x(1) = 0
  polyl%y(1) = 0
  !
  ! Current directory extrema
  root%gen%minmax(1) = phys_sizex_def  ! xmin
  root%gen%minmax(2) = 0.              ! xmax
  root%gen%minmax(3) = phys_sizey_def  ! ymin
  root%gen%minmax(4) = 0.              ! ymax
  !
  call gtv_close_segments_for_writing_from_main()
  !
  ! 3) Creation of the first directory tree, i.e. certainly <GREG
  error = .false.
  call gtsegm_dir(top_dir(2:),root,phys_sizex_def,phys_sizey_def,error)
  if (error) then
    call gtv_message(seve%f,rname,'Cannot create main directory '//top_dir)
    call sysexi(fatale)
  endif
  !
  ! 4) Change the current directory to <GREG
  call decode_chemin(top_dir,root,arrivee,isdir,segm,found)
  if (.not.isdir .or. .not.found) then
    call gtv_message(seve%f,rname,'Cannot move to main directory '//top_dir)
    call sysexi(fatale)
  endif
  !
  ! 5) Ok, turn the global values
  cw_directory => arrivee
  call cree_chemin_dir(cw_directory,pwd,nc)
  call sp_gtwindow(cw_output,0.0,phys_sizex_def,0.0,phys_sizey_def)  ! Reset clipping area
  !
end subroutine gtx_segm_0
!
subroutine gtsegm_dir(name,parent,sizex,sizey,error)
  use gildas_def
  use gtv_buffers
  use gtv_graphic
  use gtv_interfaces, except_this=>gtsegm_dir
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Creation d'un descripteur de macrosegment de nom name
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name    ! Name of the new macrosegment
  type(gt_directory), pointer     :: parent  ! Parent directory
  real*4,           intent(in)    :: sizex   ! Physical paper size (x)
  real*4,           intent(in)    :: sizey   ! Physical paper size (y)
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTSEGM_DIR'
  logical :: found
  type(greg_values) :: tab_greg
  type(gt_directory), pointer :: gdparent,new_dir,trouve
  character(len=message_length) :: mess
  integer :: sever
  !
  if (.not.awake .or. error_condition) return
  !
  ! Reset the global pointers
  if (associated(co_segment)) then
    if (strict2011) then
      sever = seve%e
    else
      sever = seve%w
    endif
    write(mess,'(3A)')  'Attempt to create a new directory "',trim(name),'"'
    call gtv_message(sever,rname,mess)
    write(mess,'(3A)')  'while a segment (',trim(co_segment%head%gen%name),') is still opened'
    call gtv_message(sever,rname,mess)
    call gtv_message(sever,rname,  &
      'Programmer: call ''gr_segm_close(error)'' to close first this segment')
    if (strict2011) then
      error = .true.
      return
    endif
    ! Close the segment currently opened. Should not be automatic in the
    ! future, i.e. always raise an error.
    call gtsegm_close(error)  ! Write protected
    if (error)  return
  endif
  !
  ! Test si le macrosegment n'a pas deja un fils de meme nom que le
  ! nouveau a creer
  call fils_cherche(parent,name,trouve,found)
  if (found) then
    ! This is already tested in mkdir
    call gtv_message(seve%e,rname,'Internal error: subdirectory '//  &
      trim(name)//' already exists')
    error = .true.
    return
  endif
  !
  ! No action if the current segment is empty
  ! Absurde un repertoire ne peut pas etre assimile a 1 segment vide
  !
  call gtv_open_segments_for_writing_from_main()
  !
  ! Descriptor of the new segment
  new_dir => gtv_newdirectory(error)
  if (error)  return
  !
  ! Current line attributes
  new_dir%gen%visible = .true.
  !
  ! Undefined MINMAX information
  new_dir%gen%minmax(1) = sizex
  new_dir%gen%minmax(2) = 0.0
  new_dir%gen%minmax(3) = sizey
  new_dir%gen%minmax(4) = 0.0
  !
  ! Physical paper size.
  new_dir%phys_size(1) = sizex
  new_dir%phys_size(2) = sizey
  !
  ! Mise a jour des differents pointeurs.
  new_dir%father => parent      ! Pointer on the parent directory
  new_dir%son_first => null()   ! Pointer on the first son
  new_dir%son_last => null()    ! Pointer on the last son
  new_dir%brother => null()     ! Pointer on the next brother
  new_dir%leaf_first => null()  ! Pointer on the first leaf
  new_dir%leaf_last => null()   ! Pointer on the last leaf
  new_dir%segn = 0
  !
  ! Pointer to the "grandpere" (that is, the directory just under the root)
  call find_grandpere(parent,new_dir,gdparent)
  new_dir%ancestor => gdparent
  !
  ! Mise a jour de l'environnement graphique
  ! the graphical information in the directory descriptor has 3 parts:  the
  ! current graphical environment (ENV_GRAPH), the table of addresses of
  ! the graphical environments for all the windows in the directory (GENV_ARRAY),
  ! and the number of the current window and total number of windows (WINDOW(1)
  ! and WINDOW(2), respectively)
  new_dir%x%genv = 0
  new_dir%x%genv_array = 0
  new_dir%x%curwin = 0
  new_dir%x%nbwin = 0
  !
  if (flag_greg) then
    if (associated(parent,root)) then
      ! This is a new top-directory. GREG values are set to GREG current ones
      call get_greg_values(tab_greg)
    else
      ! This is a new subdirectory. GREG values are inherited from its
      ! father (which can be different from current directory, e.g. if user
      ! asks: CREATE DIRECTORY <OTHER<SUBDIR ). The GREG values of the
      ! current working directory have been updated just before, so if it is
      ! the father, all should be ok for the son.
      tab_greg = parent%val_greg  ! Get a copy
    endif
    call attach_greg_values(new_dir,tab_greg,error)
  endif
  !
  ! Mise a jour du nom du nouveau segment : le nom des freres a deja ete teste
  new_dir%gen%name = name
  !
  ! Update the parent directory pointers
  if (.not.associated(parent%son_first)) then
    ! Mise a jour du pointeur fils gauche
    parent%son_first => new_dir
  else
    ! There was already a son
    parent%son_last%brother => new_dir
  endif
  parent%son_last => new_dir
  !
  ! Initialize the current directory extrema
  new_dir%gen%minmax(1) = sizex  ! xmin
  new_dir%gen%minmax(2) = 0.     ! xmax
  new_dir%gen%minmax(3) = sizey  ! ymin
  new_dir%gen%minmax(4) = 0.     ! ymax
  !
  call gtv_close_segments_for_writing_from_main()
  !
end subroutine gtsegm_dir
!
subroutine find_grandpere(pere,dir,gdpere)
  use gtv_interfaces, except_this=>find_grandpere
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Find the "grandpere" for the current directory, that is, the
  ! directory in this tree which is just under the root (1,1)
  ! NB: that may indeed be the current directory.
  !---------------------------------------------------------------------
  type(gt_directory)          :: pere    ! Parent directory
  type(gt_directory), pointer :: dir     ! Current directory
  type(gt_directory), pointer :: gdpere  ! Address of the grandfather
  !
  ! First, the special case where the pere is the root directory. Then
  ! by definition, the grandpere, ie the directory just under the root, is
  ! the current directory.
  if (.not.associated(pere%father)) then
    ! 'pere' is the root
    gdpere => dir
  else
    gdpere => pere%ancestor
  endif
  !
end subroutine find_grandpere
!
subroutine gtv_limits(dir_start,error)
  use gbl_message
  use gtv_buffers
  use gtv_interfaces, except_this=>gtv_limits
  use gtv_tree
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Traverse the current tree structure, and Recompute all the MinMax,
  ! taking care of the visibility status of all the segments. Do it
  ! from the ancestor of input directory, because parent directories
  ! are affected by the minmax or their child.
  !  Used after a CHANGE visibility off or GTDLS.
  !---------------------------------------------------------------------
  type(gt_directory), target :: dir_start  ! Starting directory
  logical,     intent(inout) :: error      ! Logical error flag
  ! Local
  real*4 :: minmax(4)
  type(gt_directory), pointer :: workdir
  !
  if (associated(dir_start%ancestor)) then
    workdir => dir_start%ancestor
  else
    ! Should be the root <
    workdir => dir_start
  endif
  !
  ! Recompute all the tree from the ancestor
  call gtv_limits_one(workdir,minmax,error)
  if (error) return
  !
end subroutine gtv_limits
!
recursive subroutine gtv_limits_one(dir_start,dminmax,error)
  use gbl_message
  use gtv_buffers
  use gtv_interfaces, except_this=>gtv_limits_one
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Traverse the current tree structure, from input directory, and
  ! Recompute all the MinMax, taking care of the visibility status of
  ! all the segments. Then store these MinMax in the input directory.
  !---------------------------------------------------------------------
  type(gt_directory)     :: dir_start   ! Starting address
  real*4,  intent(out)   :: dminmax(4)  ! Computed XY extrema
  logical, intent(inout) :: error       ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTV_LIMITS'
  real*4 :: minmax(4)
  type(gt_directory), pointer :: dir,next
  !
  ! Attached segments
  call minmax_dir(dir_start,dminmax,error)
  if (error)  return
  !
  ! Attached sub-directories (first level only, deeper levels will be
  ! performed in recursive calls).
  next => dir_start%son_first
  do while (associated(next))
    dir => next
    !
    call gtv_limits_one(dir,minmax,error)
    if (error) return
    !
    dminmax(1) = min(dminmax(1),minmax(1))  ! xmin
    dminmax(2) = max(dminmax(2),minmax(2))  ! xmax
    dminmax(3) = min(dminmax(3),minmax(3))  ! ymin
    dminmax(4) = max(dminmax(4),minmax(4))  ! ymax
    !
    next => dir%brother
  enddo
  !
  dir_start%gen%minmax = dminmax
  !
end subroutine gtv_limits_one
!
subroutine minmax_dir(dir,minmax,error)
  use gtv_interfaces, except_this=>minmax_dir
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !   Procedure de parcours des feuilles d'un repertoire en vue de
  ! calculer les minmax sur le directory courant, taking care of the
  ! visibility status of all the segments.
  !---------------------------------------------------------------------
  type(gt_directory)     :: dir        ! Directory
  real*4,  intent(out)   :: minmax(4)  ! Computed XY extrema
  logical, intent(inout) :: error      ! Logical error flag
  ! Local
  type(gt_segment), pointer :: segm
  !
  minmax(1) = dir%phys_size(1)  ! xmin
  minmax(2) = 0.                ! xmax
  minmax(3) = dir%phys_size(2)  ! ymin
  minmax(4) = 0.                ! ymax
  !
  segm => dir%leaf_first
  do while (associated(segm))
    if (segm%head%gen%visible) then
      minmax(1) = min(minmax(1),segm%head%gen%minmax(1))  ! xmin
      minmax(2) = max(minmax(2),segm%head%gen%minmax(2))  ! xmax
      minmax(3) = min(minmax(3),segm%head%gen%minmax(3))  ! ymin
      minmax(4) = max(minmax(4),segm%head%gen%minmax(4))  ! ymax
    endif
    !
    segm => segm%nextseg
  enddo
  !
end subroutine minmax_dir
