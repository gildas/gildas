subroutine gtv_clear_alpha(output)
  use gtv_interfaces, except_this=>gtv_clear_alpha
  use gtv_types
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  !  The purpose of this routine is to show the graphic display at best.
  ! To do that it
  !  - Clears the "Alphanumeric" plane of the input device
  !    is the graphic device.
  !  - Pop the graphic window on window systems
  !  - Does nothing in other cases.
  !---------------------------------------------------------------------
  type(gt_display) :: output         ! Output instance
  !
  ! Should add here protocol dependent code, such as "pop in front"
  if (output%dev%protocol.eq.p_x)  &
    call x_clal(output%x%graph_env)  ! Raise Window
  !
end subroutine gtv_clear_alpha
!
subroutine gtclal
  use gtv_interfaces, except_this=>gtclal
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ public obsolescent 22-sep-2009
  ! Obsolete entry point to 'gtv_clear_alpha'. To be removed from
  ! official documentation before.
  !---------------------------------------------------------------------
  call gtv_clear_alpha(cw_output)
end subroutine gtclal
!
subroutine gtv_clear_graphic(output)
  use gtv_interfaces, except_this=>gtv_clear_graphic
  use gtv_types
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  !  The purpose of this routine is to show the ALPHA display at best.
  ! To do that it
  !  - Lower the graphic window on window systems
  !  - Does nothing in other cases.
  !---------------------------------------------------------------------
  type(gt_display) :: output         ! Output instance
  !
  if (output%dev%protocol.eq.p_x)  &
    call x_clpl(output%x%graph_env)       ! Lower Window
  !
end subroutine gtv_clear_graphic
