subroutine cwrite(output,chain,n)
  use gtv_types
  use gtv_protocol
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  type(gt_display) :: output        ! Output instance
  integer          :: n             ! Number of characters in chain
  character(len=*) :: chain         ! Input character string
  !
  ! Cannot do this way on image devices:
  if (output%dev%protocol.eq.p_x .or. &
      output%dev%protocol.eq.p_postscript )  return
  !
  if (output%opened)  write(output%iunit,1) chain(1:n)
  !
1 format(a,$)
  ! This format may be required on some systems, but does not compile on others
! 2  FORMAT(<N>A,$)
end subroutine cwrite
