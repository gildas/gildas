subroutine gtl_refresh(line,error)
  use gbl_message
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gtl_refresh
  use gtv_buffers
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    GTV\REFRESH [DirName [WinNum|ZOOM]]
  !  Redraw the active (or specified) X window.
  !
  !  X protocol is always refreshed on-demand of the system, so there
  ! should be no need for such a user request.
  !  However, images are implicitly created at depth 2, so what you see
  ! at first drawing (when segments are stacked in chronological order)
  ! is not what you see at subsequent redrawing. This is annoying and
  ! requires some action. One would think about implicit redraw when an
  ! image is plotted, but this would be very uneffecient (think about
  ! GO BIT). On the other hand, users are well used to this
  ! "bug-feature" (formerly known as ZOOM REFRESH), so we let them
  ! decide when they want a redraw.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line  ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='REFRESH'
  integer(kind=4), parameter :: mode_update=-2  ! See module 'gtview_modes'
  integer(kind=4) :: cnum,nc
  character(len=gtvpath_length) :: dirname
  logical :: isdir,found,err
  type(gt_directory), pointer :: dir
  type(gt_segment), pointer :: segm
  integer(kind=address_length) :: genv
  !
  if (cw_device%protocol.ne.p_x) then
    call gtv_message(seve%w,rname,'Ignored when not using X-Window')
    return
  endif
  !
  ! Argument parsing
  if (sic_present(0,1)) then
    ! 1) Directory, if present
    call sic_ch (line,0,1,dirname,nc,.true.,error)
    if (error) return
    call sic_upper(dirname)
    call decode_chemin(dirname,cw_directory,dir,isdir,segm,found)
    ! Not found?
    if (.not. (found.and.isdir)) then
      call gtv_message(seve%e,rname,'No such directory '//dirname)
      error = .true.
      return
    endif
  else
    dir => root
  endif
  !
  if (sic_present(0,2)) then
    ! 2 arguments: 1 window in one directory
    !
    cnum = get_window_cnum_byname(dir,rname,line,0,2,error)
    if (cnum.lt.0) then
      call gtv_message(seve%w,rname,'No such window, nothing done')
      return
    endif
    !
    genv = c_get_win_genv(dir%x%genv_array,cnum)
    err = x_refresh_genv(dir,mode_update,genv)
    !
  else
    call gtview_update(dir,error)
  endif
  !
end subroutine gtl_refresh
