subroutine gtwindow(output,ax,bx,ay,by)
  use gtv_interfaces, except_this=>gtwindow
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Defines the clipping area and redraw the plot.
  !---------------------------------------------------------------------
  type(gt_display)   :: output      ! Output instance
  real*4, intent(in) :: ax,bx       ! Clipping area coordinates
  real*4, intent(in) :: ay,by       !
  ! Local
  logical :: error
  error = .false.
  call sub_gtwindow(output,ax,bx,ay,by,.true.,error)
end subroutine gtwindow
!
subroutine sp_gtwindow(output,ax,bx,ay,by)
  use gtv_interfaces, except_this=>sp_gtwindow
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  ! Defines the clipping area but does not redraw the plot.
  !---------------------------------------------------------------------
  type(gt_display)   :: output      ! Output instance
  real*4, intent(in) :: ax,bx       ! Clipping area coordinates
  real*4, intent(in) :: ay,by       !
  ! Local
  logical :: error
  error = .false.
  call sub_gtwindow(output,ax,bx,ay,by,.false.,error)
end subroutine sp_gtwindow
!
subroutine sub_gtwindow(output,ax,bx,ay,by,redraw,error)
  use gtv_interfaces, except_this=>sub_gtwindow
  use gtv_types
  use gtv_graphic
  use gtv_protocol
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Defines the clipping area and optionally redraw the plot. Works on
  ! the current working directory.
  !---------------------------------------------------------------------
  type(gt_display)       :: output  ! Output instance
  real*4,  intent(in)    :: ax,bx   ! Clipping area coordinates
  real*4,  intent(in)    :: ay,by   !
  logical, intent(in)    :: redraw  !
  logical, intent(inout) :: error   ! Logical error flag
  !
  if (.not.awake .or. error_condition) return
  !
  output%gx1 = min(ax,bx)
  output%gx2 = max(ax,bx)
  output%gy1 = min(ay,by)
  output%gy2 = max(ay,by)
  !
  ! Clip with the physical paper size
  ! This clipping is disabled because it is not desirable. Especially when
  ! zooming completely out of the paper area, we do not want to raise an
  ! error.
  ! output%gx1 = max(output%gx1,0.)
  ! output%gx2 = min(output%gx2,cw_directory%phys_size(1))
  ! output%gy1 = max(output%gy1,0.)
  ! output%gy2 = min(output%gy2,cw_directory%phys_size(2))
  !
  if (output%gx1.ge.output%gx2 .or. output%gy1.ge.output%gy2) then
    call gtv_message(seve%e,'GTWINDOW','Invalid clipping window')
    call gtx_err
    return
  endif
  !
  ! Make it visible.
  if (redraw) then
    if (output%dev%protocol.eq.p_x)  call gti_xforceupdate(output)
    call gtview_sub(output,'R',error)  ! Rewind
  endif
  !
end subroutine sub_gtwindow
!
subroutine on_resize(graph_env,error)
  use gildas_def
  use gtv_interfaces, except_this=>on_resize
  use gtv_types
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  ! Document me!
  !---------------------------------------------------------------------
  integer(kind=address_length), intent(in)    :: graph_env
  logical,                      intent(inout) :: error
  ! Local
  logical :: found
  type(gt_display), pointer :: output
  !
  ! Get the output instance associated to the input graph_env
  call get_slot_output_by_genv(graph_env,output,.true.,found,error)
  if (error)  return
  !
  if (graph_env.gt.0)  &
    call get_win_pixel_info(graph_env,output%px1,output%py1,output%px2,output%py2)
  !
  ! Clear needed for gtk but this code doesn't do anything
  ! if (graph_env.ne.0) call x_clear(graph_env)
  !
end subroutine on_resize
