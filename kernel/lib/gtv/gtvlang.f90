subroutine load_gtvl
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>load_gtvl
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  ! Local
  integer, parameter :: mcom=34
  character(len=12) :: vocab(mcom)
  !
  data vocab /                                     &
    ' CHANGE',                                     &
    ' CLEAR',                                      &
    ' COMPRESS',                                   &
    ' CREATE', '/PLOT_PAGE','/PIXEL','/GEOMETRY','/POSITION','/SIZE',  &
    ' DESTROY',                                    &
    '$DEVICE', '/OUTPUT',                          &
    ' DISPLAY',                                    &
    ' FLUSH',                                      &
    ' GTV',                                        &
    ' HARDCOPY', '/PRINT','/DEVICE','/OVERWRITE','/DIRECTORY',  &
                 '/GEOMETRY','/EXACT','/FITPAGE',  &
    ' LENS',                                       &
    ' LUT', '/PEN','/EDIT','/REVERSE','/BLANK',    &
    ' METACODE', '/PATH',                          &
    ' REFRESH',                                    &
    ' REPLICATE',                                  &
    ' ZOOM' /
  !
  call sic_begin('GTVL','GAG_HELP_GTVL',mcom,vocab,  &
    '4.0    S.Bardeau,E.Reynier,J.Pety',run_gtvl,gterrtst)
  !
end subroutine load_gtvl
!
subroutine gtv_delseg(name,is_dir,error)
  use gtv_buffers
  use gtv_interfaces, except_this=>gtv_delseg
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !     Delete (i.e. make it invisible) a named segment or a directory
  !---------------------------------------------------------------------
  character(len=*)                :: name    !
  logical                         :: is_dir  !
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CLEAR'
  logical :: isdir,found
  type(gt_segment), pointer :: segm
  type(gt_directory), pointer :: dir
  !
  ! Le segment donne en parametre existe t il ?
  call decode_chemin(name,cw_directory,dir,isdir,segm,found)
  if (.not.found) then
    call gtv_message(seve%e,rname,'No such segment '//name)
    error = .true.
  elseif (is_dir.and..not.isdir) then
    call gtv_message(seve%e,rname,'This is NOT a directory ')
    error = .true.
  elseif (isdir.and..not.is_dir) then
    call gtv_message(seve%e,rname,'This is a directory')
    error = .true.
  elseif (isdir) then
    call gtv_open_segments_for_writing_from_main()
    call change_attr_dir(dir,att_vis,-1)
    call gtv_close_segments_for_writing_from_main()
  else
    call gtv_open_segments_for_writing_from_main()
    call change_attr_seg(segm,att_vis,-1)
    call gtv_close_segments_for_writing_from_main()
  endif
end subroutine gtv_delseg
!
subroutine gtl_replicate(line,error)
  use gtv_interfaces, except_this=>gtl_replicate
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  !
  call gtv_message(seve%e,'REPLICATE','Not yet implemented')
  error = .true.
  !
end subroutine gtl_replicate
!
subroutine gtl_corner(error)
  use gtv_interfaces, except_this=>gtl_corner
  use gtv_buffers
  !---------------------------------------------------------------------
  ! @ public
  ! GTVIRT support routine for command
  !   CORNERS
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: sizex,sizey
  !
  ! Get physical paper size of current active directory
  sizex = cw_directory%phys_size(1)
  sizey = cw_directory%phys_size(2)
  !
  call gtreloc(0.0,      0.1*sizey)
  call gtdraw (0.0,      0.0      )
  call gtdraw (0.1*sizex,0.0      )
  call gtreloc(0.9*sizex,0.0      )
  call gtdraw (    sizex,0.0      )
  call gtdraw (    sizex,0.1*sizey)
  call gtreloc(    sizex,0.9*sizey)
  call gtdraw (    sizex,    sizey)
  call gtdraw (0.9*sizex,    sizey)
  call gtreloc(0.1*sizex,    sizey)
  call gtdraw (0.0,          sizey)
  call gtdraw (0.0,      0.9*sizey)
  !
end subroutine gtl_corner
!
subroutine run_gtvl(line,comm,error)
  use gtv_interfaces, except_this=>run_gtvl
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public-mandatory (public but should not) (mandatory because
  ! symbol is used elsewhere)
  !---------------------------------------------------------------------
  character(len=*)              :: line   !
  character(len=*)              :: comm   !
  logical,          intent(out) :: error  !
  ! Local
  character(len=*), parameter :: rname = 'GTVL'
  character(len=60) :: chain
  integer, save :: icall=0
  !
  if (icall.ne.0)  &
    call gtv_message(seve%d,rname,'Reentrant call to RUN_GTVL '//comm)
  !
  icall = icall+1
  !
  call gtv_message(seve%c,rname,line)
  !
  error = .false.
  select case (comm)
  case('CLEAR')
    call gtl_clear(line,error)
  case('CREATE')
    call gtl_create(line,error)
  case('REPLICATE')
    call gtl_replicate(line,error)
  case('CHANGE')
    call gtl_change(line,error)
  case('COMPRESS')
    call gtl_compress(line,error)
  case('METACODE')
    call gtl_metacode(line,error)
  case('DISPLAY')
    call gtl_display(line,error)
  case('DESTROY')
    call gtl_destroy(line,error)
  case('DEVICE')
    call gtl_device(line,error)
  case('GTV')
    call gtl_gtv(line,error)
  case('HARDCOPY')
    call gtl_hardcopy(line,error)
  case('ZOOM')
    call gtl_zoom(line,error)
  case('LENS')
    call gtl_lens(line,error)
  case('LUT')
    call gtl_lutpen(line,error)
  case('FLUSH')
    ! Flush the stack (it can contain directories destruction, so call
    ! it whatever the device is).
    call x_flush(0)
  case('REFRESH')
    call gtl_refresh(line,error)
  case default
    chain = 'No code to execute for '//comm
    call gtv_message(seve%e,rname,chain)
    error = .true.
  end select
  !
  icall = icall-1
end subroutine run_gtvl
