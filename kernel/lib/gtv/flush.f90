subroutine gti_flush(output)
  use gtv_dependencies_interfaces
  use gtv_interfaces, except_this=>gti_flush
  use gtv_types
  use gtv_graphic
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(gt_display) :: output         ! Output instance
  ! Local
  integer :: ier
  !
  ! Close and reopen the output device.
  close(unit=output%iunit)
  !
  open(unit=output%iunit,file=output%file,status='OLD',iostat=ier)
  if (ier.ne.0) then
    call gtv_message(seve%e,'GTFLUSH','Cannot flush graphics device '//  &
    output%file)
    call putios ('E-GTFLUSH,  ',ier)
    call gtx_err
  endif
  !
end subroutine gti_flush
!
subroutine gti_trace(output,trace)
  use gtv_interfaces, except_this=>gti_trace
  use gtv_protocol
  use gtv_types
  !---------------------------------------------------------------------
  ! @ private
  !  Handle traces, if supported by the device
  !---------------------------------------------------------------------
  type(gt_display), intent(in) :: output  ! Output instance
  character(len=*), intent(in) :: trace   ! The message trace
  !
  select case (output%dev%protocol)
  case (p_postscript)
    call ps_trace(trace)
  ! Other protocols: not implemented
  end select
  !
end subroutine gti_trace
