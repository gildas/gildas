subroutine gtclos(error)
  use gtv_interfaces, except_this=>gtclos
  use gtv_buffers
  use gtv_graphic
  use gtv_protocol
  use gtv_tree
  use gtv_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !   AWAKE flag is not reset by GTCLOS, as GTINIT is required only
  ! once even for multiple GTOPEN calls. Similarly, IUNIT is closed,
  ! but not freeed.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GTCLOS'
  !
  if (.not.awake) then
    call gtv_message(seve%w,rname,'Library is not awake')
    return
  endif
  if (error_condition) then
    call gtv_message(seve%e,rname,'Library is in error condition')
    call gtx_err
    return
  endif
  !
  if (cw_device%protocol.eq.p_x) then
    ! Cleanly detach all the windows in the metacode.
    call gtv_open_segments_for_writing_from_main()
    call win_destroy_all_recursive(root,error)
    call gtv_close_segments_for_writing_from_main()
    if (error)  return
    !
    ! Clean the X ressources
    call x_close
    !
  elseif (cw_device%protocol.eq.p_svg) then
    call svg_close
    !
  endif
    !
  if (cw_output%dev%linit2.gt.0) then
    call cwrite(cw_output,cw_output%dev%init2,cw_output%dev%linit2)
    !
    call gtv_message(seve%i,rname,'Device closed')
  endif
    !
  if (cw_output%opened) then
    ! Close File
    call gtz_close(cw_output)
    call gtv_message(seve%i,rname,'Device closed')
  endif
  !
  ! Reset flags
  call gtx_reset
  !
end subroutine gtclos
!
subroutine gtx_reset
  use gtv_interfaces, except_this=>gtx_reset
  use gtv_graphic
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  !  Resets flags.
  !----------------------------------------------------------------------
  !
  ! Reset flags
  cw_output%opened=.false.
  !
  ! Switch to the NONE device?
  ! Reset the hardware character parameters
  !
  ! To define it for DEVICE NONE
  cw_device%hardw_cursor = .false.
  !
end subroutine gtx_reset
!
subroutine gtnone
  use gtv_interfaces, except_this=>gtnone
  use gtv_buffers
  use gtv_graphic
  use gtv_protocol
  !---------------------------------------------------------------------
  ! @ private
  ! Define the null device as current device.
  !---------------------------------------------------------------------
  logical :: error
  !
  ! Setup the NONE device
  error = .false.
  call gtx_setup('NONE',cw_device,error)
  if (error)  continue  ! ?
  gtv_device = device_list(cw_device%ident)
  !
  ! Setup a null output
  call gt_output_reset(null_output)
  null_output%dev => cw_device
  cw_output => null_output
  !
end subroutine gtnone
