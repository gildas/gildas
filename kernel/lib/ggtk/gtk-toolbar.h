
typedef int (*_event_handler_t)(GdkEvent *, gpointer, gpointer);

extern GdkColor ggtk_black;
extern GdkColor ggtk_white;

void ggtk_set_pointer_event_handler( _event_handler_t h, gpointer user_data);
int ggtk_call_pointer_event_handler( GdkEvent *event, gpointer event_data);
int run_gtk_main_loop( void *data);
void ggtk_create_drawing_area(gtv_toolbar_args_t *args);
void ggtk_env_change_win( ggtk_env_t *genv, GdkWindow *window);
void ggtk_attach_window_genv(ggtk_env_t *env, GtkWidget *window, GtkWidget
 *drawing_area, gboolean connect_expose);

