
#ifndef _GTK_MENU_H_
#define _GTK_MENU_H_

int run_gtk_menu_args( int argc, char **argv);
void run_gtk_menu( const char *file);
void kill_gtk_menu();

#endif /* _GTK_MENU_H_ */

