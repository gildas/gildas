
#include "gtv/xsub.h"
#include "gcore/glaunch.h"
#include <gtk/gtk.h>

//#define GAG_USE_CAIRO
#ifdef GAG_USE_CAIRO
#include <cairo.h>
#endif

#undef NO_CALL_TO_PUSH_REFRESH_WITH_NULL_GENV_DURING_ZOOM

typedef void (*ggtk_gpointer_handler_t)(gpointer);

typedef struct _ggtk_env {
    G_env herit;
    GtkWidget *main_widget;
    GtkWidget *drawing_area;
    GdkWindow *win_graph;
    ggtk_gpointer_handler_t post_refresh_handler;
    gpointer post_refresh_handler_data;
#ifdef GAG_USE_CAIRO
    cairo_t *cr;
#else
    GdkGC *gc;
#endif
    int plume_cour;
    GdkPixmap *vertical,*horizontal;
    GdkPixmap *box1,*box2,*box3,*box4;

    int line_width;
    GdkLineStyle line_style;
    GdkCapStyle cap_style;
    GdkJoinStyle join_style;
} ggtk_env_t;

typedef struct _ggtk_toolbar_args {
    gtv_toolbar_args_t herit;
    ggtk_env_t *env;
    char *window_name;
} ggtk_toolbar_args_t;

