
#include "gtk-local.h"

#include "gtk-toolbar.h"
#include "gtk-menu.h"
#include "gtk-dialog.h"

#include <stdlib.h>
#include <locale.h>

#define gui_c_message_set_id CFC_EXPORT_NAME( gui_c_message_set_id)
#define gtk_c_message_set_id CFC_EXPORT_NAME( gtk_c_message_set_id)

char *ggtk_version()
{
    static char ret[256];
    sprintf( ret, "%d.%d.%d", GTK_MAJOR_VERSION, GTK_MINOR_VERSION,
     GTK_MICRO_VERSION);
    return ret;
}


/**
 * Gildas overlay to gtk_init. Does nothing if already called.
 *
 * @return nothing
 */

void ggtk_init()
{
    static int init_done = 0;

    if (init_done)  return;

    /* init gtk */
#ifndef WIN32
    setenv("UBUNTU_MENUPROXY", "1", 1);  /* do not use main menu with Unity */
#endif
    gtk_init( NULL, NULL);

    /* It seems that gtk_init enables the user locale, which breaks reading
       or writing numbers with some locales. Re-force locale to "C" */
    if (setlocale( LC_ALL, "C") == NULL) {
        if (setlocale( LC_ALL, "UTF-8") == NULL) {
            /* suppress warning */
            fprintf( stderr, "Warning, Unable to set locale to \"C\" or \"UTF-8\", numeric values in widgets may be unreadable.\n");
        }
    }

    init_done = 1;
}


#if defined(GAG_USE_STATICLINK)
#define init_gui CFC_EXPORT_NAME( init_gui)
void CFC_API init_gui()
#else
void ggtk_init_gui()
#endif
{
    /* init threads */    
    g_thread_init( NULL);
    /*
    g_type_init();
    gdk_threads_init();
    */

    set_gtv_main_loop_handler( run_gtk_main_loop);
    set_menu_handlers( run_gtk_menu, kill_gtk_menu);
    set_dialog_handler( run_dialog, ggtk_version);
}

void CFC_API gui_c_message_set_id( int *gpack_id)
{
    /*
    gtk_c_message_set_id( gpack_id);
    */
}

