
#ifndef _GTK_GRAPH_H_
#define _GTK_GRAPH_H_

void ggtk_set_post_refresh_handler( ggtk_env_t *genv,
 ggtk_gpointer_handler_t h, gpointer user_data);
void ggtk_call_post_refresh_handler( ggtk_env_t *genv);
G_env *ggtk_new_genv_from( G_env *ref_genv, GtkWidget *drawing_area, int width,
 int height, gboolean connect_expose);
GdkColor *ggtk_default_colormap();
int ggtk_default_colormap_size();

#endif /* _GTK_GRAPH_H_ */

