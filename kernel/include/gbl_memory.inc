!-----------------------------------------------------------------
!
! All addresses, either returned by the system following a call to
! SIC_GETVM, or computed using the LOCWRD() or LOCSTR() functions,
! are conveyed in the program(s) as an offset in an array of
! INTEGERS named MEMORY.
!
! The software is built on 4-byte integers and reals, and SIC_GETVM
! allocates memory by multiples of 4 bytes. Usually one computes the
! offset as
!	INCLUDE 'gbl_memory.inc'
!	INTEGER(KIND=ADDRESS_LENGTH) IP,GAG_POINTER,ADDRESS
!	( ... )
!	IP=GAG_POINTER(ADDRESS,MEMORY)
! where ADDRESS_LENGTH is a parameter of value 4 for 32-bit 
! machines, 8 for 64 bit machines, defined in Fortran-90 in
! module GILDAS_DEF
!
!
! Afterwards, the adress is "recalled"  as MEMORY(IP).
! It is possible to perform some algebra on the adresses (ADDRESS) or
! the pointers (IP), such as IP=IP+1 => MEMORY(IP) will point
! 1*4 bytes later, i.e., on another R*4 if IP was already "remembering"
! the address of an R*4.
!
! It would have been equivalent to compute ADDRESS=ADDRESS+4, then
!	IP=GAG_POINTER(ADDRESS,MEMORY).
! It is then evident that working on adresses and IPs is NOT the
! same.
!
! We have also defined the byte array MEMBYT for the rare case where
! the address returned does not fall naturally on a 32-byte word in
! memory. This can happen when getting the address of a (sub)-string
! through the LOCSTR function. In that case, one uses IP = BYTPNT
! (ADDRESS,MEMBYT) and MEMBYT(IP).
!
! Note: Memory allocation provides 32-bit alignement.
! The case in point above is the address of an
! internal character array (got by LOCSTR) or function address
! (got by LOCWRD), which can be 8 or 16 bit aligned on some machines.
!
! To alleviate potential problems, the routine GAG_POINTER
! will complain if it cannot translate an address to an offset
! in a I*4 array on computer not 32 bit aligned. It will be then up to
! the programmer to use BYTPNT and additional arithmetics instead
! of GAG_POINTER.
!
! EQUIVALENCE between CHARACTERs and other variables is strictly
! forbidden.
!
!---------------------------------------------------------------------
      INTEGER(KIND=1)  MEMBYT(8)
      INTEGER*4 MEMORY(2)
! The whole thing is is common now
      COMMON /OURPOINTERREF/ MEMORY, MEMBYT
! Make sure the address remains the same
      SAVE /OURPOINTERREF/
