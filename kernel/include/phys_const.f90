!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Physical constants
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module phys_const
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real(8), parameter :: clight = 299792458.0d0   ! [  m/s] Light velocity
  real(8), parameter :: clight_kms = clight/1d3   ! [ km/s] Frequencies are in MHz in GILDAS
  real(8), parameter :: clight_mhz = clight/1d6   ! [m.MHz] Frequencies are in MHz in GILDAS
  real(8), parameter :: kbolt  = 1.3806504d-23    ! [  J/K] Boltzman 
  real(8), parameter :: hplanck = 6.62606896d-34  ! [ J.s] Planck Constant
  ! PI is defined with more digits than necessary to avoid losing the last
  ! few bits in the decimal to binary conversion
  real(8), parameter :: pi  = 3.14159265358979323846d0
  real(4), parameter :: pis = 3.141592653
  real(8), parameter :: twopi = 2.d0*pi
  real(8), parameter :: rad_per_hang = pi/12d0
  real(8), parameter :: hang_per_rad = 12d0/pi
  real(8), parameter :: rad_per_msec = pi/(180d0*3600d0*1d3)
  real(8), parameter :: msec_per_rad = (180d0*3600d0*1d3)/pi
  real(8), parameter :: rad_per_sec = pi/(180d0*3600d0)
  real(8), parameter :: sec_per_rad = (180d0*3600d0)/pi
  real(8), parameter :: rad_per_min = pi/(180d0*60d0)
  real(8), parameter :: min_per_rad = (180d0*60d0)/pi
  real(8), parameter :: rad_per_deg = pi/(180d0)
  real(8), parameter :: deg_per_rad = (180d0)/pi
  real(8), parameter :: fwhm_per_sdev = 2.3548200450309493d0  ! sqrt(8d0*log(2d0))
  real(8), parameter :: f_to_k = twopi/clight_mhz
  integer(4), parameter :: gagzero_in_mjd = 60549
  real(kind=8), parameter :: mjdzero_in_jd = 2400000.5d0
end module phys_const
!
!!!!!!!!!!!!!!!!!!!!!!!! phys_const.f90 ends here !!!!!!!!!!!!!!!!!!!!!!!!!
