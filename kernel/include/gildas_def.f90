!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gildas_def
  !
  integer(kind=4), parameter :: code_null = 0  ! All purpose null parameter
  !
  ! address_length: kind of integers which store memory addresses
  ! index_length: kind of integers which store array indices along 1 dimension
  ! size_length: kind of integers which store the total array size
#if defined(BITS64)
  integer, parameter :: address_length=8  ! Kind of integers storing a memory address
#if defined(INDEX4)
  integer, parameter :: index_length=4    ! Kind of integers storing an index along one dimension
#else
  integer, parameter :: index_length=8    ! Kind of integers storing an index along one dimension
#endif
  integer, parameter :: size_length=8     ! Kind of integers storing an index in a whole array
#else
  integer, parameter :: address_length=4  ! Kind of integers storing a memory address
  integer, parameter :: index_length=4    ! Kind of integers storing an index along one dimension
  integer, parameter :: size_length=4  ! or 8?  ! Kind of integers storing an index in a whole array
#endif
  integer, parameter :: record_length=size_length  ! Kind of integers storing a record number
  integer, parameter :: varname_length=64  ! Maximum length of Sic variable names
  integer, parameter :: filename_length=512  ! Maximum length of file names
  integer, parameter :: path_length=10*filename_length  ! For collection of paths, e.g. $PATH
  integer, parameter :: commandline_length=2048  ! Maximum length of command lines
  integer, parameter :: argument_length=512
  !
  ! Maximum number of dimensions supported by SIC. Is this parameter
  ! private to the libsic?
  integer(kind=4), parameter :: sic_maxdims=7  ! Number of dimensions supported by Sic
  !
  integer, parameter :: fatale=44            ! Fatal error code: '2C'X = 44
  !
  ! Codes for Fortran allocatable pointers. Fortran associated() function
  ! do not discriminate between allocated and associated pointers.
  integer(kind=4), parameter :: code_pointer_null=1000
  integer(kind=4), parameter :: code_pointer_associated=1001
  integer(kind=4), parameter :: code_pointer_allocated=1002
  !
  ! Null address or pointer. This will become a (c_ptr) for Fortran 2003
  integer(kind=address_length), parameter :: ptr_null=0
end module gildas_def
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
