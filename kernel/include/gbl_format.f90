module gbl_format
!
! Note : these parameters must be negative
!
! Byte and untyped data
   integer, parameter :: read_only=1000000
   integer, parameter :: mhead=128
   integer, parameter :: fmt_by=-6
   integer, parameter :: fmt_un=0
!
! VAX reals imply D_FLOAT
   integer, parameter :: vax_fmt=0
   integer, parameter :: vax_r4=-1
   integer, parameter :: vax_r8=-2
   integer, parameter :: vax_i4=-13
   integer, parameter :: vax_i2=-15
   integer, parameter :: vax_l=-4
   integer, parameter :: vax_c4=-7
   integer, parameter :: vax_c8=-8
!
! IEEE for non swapped machines (VAX like or DEC like)
   integer, parameter :: ieee_fmt=-10
   integer, parameter :: iee_r4=-11
   integer, parameter :: iee_r8=-12
   integer, parameter :: iee_i4=-13
   integer, parameter :: iee_i2=-15
   integer, parameter :: iee_l=-14
   integer, parameter :: iee_c4=-17
   integer, parameter :: iee_c8=-18
   integer, parameter :: iee_i8=-19
!
! EEEI for swapped machines (IBM like)
   integer, parameter :: eeei_fmt=-20
   integer, parameter :: eei_r4=-21
   integer, parameter :: eei_r8=-22
   integer, parameter :: eei_i4=-3
   integer, parameter :: eei_i2=-5
   integer, parameter :: eei_l=-24
   integer, parameter :: eei_c4=-27
   integer, parameter :: eei_c8=-28
   integer, parameter :: eei_i8=-29
!
#if defined(VAX)
   integer, parameter :: fmt_i4=vax_i4
   integer, parameter :: fmt_i2=vax_i2
   integer, parameter :: fmt_l=vax_l
   integer, parameter :: fmt_r4=vax_r4
   integer, parameter :: fmt_r8=vax_r8
   integer, parameter :: fmt_c4=vax_c4
   integer, parameter :: fmt_c8=vax_c8
   integer, parameter :: hardware=vax_fmt
#endif
#if defined(EEEI)
   integer, parameter :: fmt_i4=eei_i4
   integer, parameter :: fmt_i2=eei_i2
   integer, parameter :: fmt_l=eei_l
   integer, parameter :: fmt_r4=eei_r4
   integer, parameter :: fmt_r8=eei_r8
   integer, parameter :: fmt_c4=eei_c4
   integer, parameter :: fmt_c8=eei_c8
   integer, parameter :: fmt_i8=eei_i8
   integer, parameter :: hardware=eeei_fmt
#endif
#if defined(IEEE)
   integer, parameter :: fmt_i4=iee_i4
   integer, parameter :: fmt_i2=iee_i2
   integer, parameter :: fmt_l=iee_l
   integer, parameter :: fmt_r4=iee_r4
   integer, parameter :: fmt_r8=iee_r8
   integer, parameter :: fmt_c4=iee_c4
   integer, parameter :: fmt_c8=iee_c8
   integer, parameter :: fmt_i8=iee_i8
   integer, parameter :: hardware=ieee_fmt
#endif
! ------------------------------------------------------------------------
! Size unit for unformatted files. Recordsizes are counted in words by DEC
! and in bytes by every other constructor.
#if defined(WIN32) && !defined(MINGW) || defined(IFORT)
   integer, parameter :: facunf=1
#else
   integer, parameter :: facunf=4
#endif
!
end module gbl_format
!
module gbl_convert
  integer(kind=4), parameter :: vax_to_ieee=1
  integer(kind=4), parameter :: vax_fr_ieee=-2
  integer(kind=4), parameter :: ieee_to_vax=2
  integer(kind=4), parameter :: ieee_fr_vax=-1
  integer(kind=4), parameter :: vax_to_eeei=3
  integer(kind=4), parameter :: vax_fr_eeei=-4
  integer(kind=4), parameter :: eeei_to_vax=4
  integer(kind=4), parameter :: eeei_fr_vax=-3
  integer(kind=4), parameter :: ieee_to_eeei=5
  integer(kind=4), parameter :: eeei_fr_ieee=-6
  integer(kind=4), parameter :: eeei_to_ieee=6
  integer(kind=4), parameter :: ieee_fr_eeei=-5
  integer(kind=4), parameter :: unknown=-10
  integer(kind=4), parameter :: mconve=6
end module gbl_convert
!
module gbl_constant
   !
   integer, parameter :: mproj = 11       ! Number of supported projections
   !
   integer, parameter :: p_unknown   = -1 ! Unknown projection
   integer, parameter :: p_none      = 0  ! Unprojected data
   integer, parameter :: p_gnomonic  = 1  ! Radial Tangent plane
   integer, parameter :: p_ortho     = 2  ! Dixon Tangent plane
   integer, parameter :: p_azimuthal = 3  ! Schmidt Tangent plane
   integer, parameter :: p_stereo    = 4  ! Stereographic
   integer, parameter :: p_lambert   = 5  ! Lambert equal area
   integer, parameter :: p_aitoff    = 6  ! Aitoff equal area
   integer, parameter :: p_radio     = 7  ! Classic Single dish radio mapping
   integer, parameter :: p_sfl       = 8  ! SFL Projection
   integer, parameter :: p_mollweide = 9  ! Mollweide projection
   integer, parameter :: p_ncp       = 10 ! North Celestial Pole projection
   integer, parameter :: p_cartesian = 11 ! Cartesian projection
   !
   integer, parameter :: type_un = 1      ! Unknown system
   integer, parameter :: type_eq = 2      ! Equatorial
   integer, parameter :: type_ga = 3      ! Galactic
   integer, parameter :: type_ho = 4      ! Horizontal
   integer, parameter :: type_ic = 5      ! ICRS
   real(kind=4), parameter :: equinox_null = -1000.  ! Null/undefined equinox of coordinates
   !
   ! Velocity type
   integer, parameter :: vel_unk = 0      ! Unsupported referential :: planetary...)
   integer, parameter :: vel_lsr = 1      ! LSR referential
   integer, parameter :: vel_hel = 2      ! Heliocentric referential
   integer, parameter :: vel_obs = 3      ! Observatory referential
   integer, parameter :: vel_ear = 4      ! Earth-Moon barycenter referential
   integer, parameter :: vel_aut = -1     ! Take referential from data
   !
   ! Velocity convention
   integer, parameter :: vconv_30m = -1   ! 30m convention (neither radio nor optical)
   integer, parameter :: vconv_unk = 0    ! Unknown
   integer, parameter :: vconv_rad = 1    ! Radio convention
   integer, parameter :: vconv_opt = 2    ! Optical convention (reserved but unsupported)
   !
   integer, parameter :: kind_any   = -1  ! Any kind (DO NOT USE IN DATA!)
   integer, parameter :: kind_spec  = 0   ! Spectroscopic data
   integer, parameter :: kind_cont  = 1   ! Continuum drift
   integer, parameter :: kind_sky   = 2   ! Skydip data
   integer, parameter :: kind_onoff = 3   ! Continuum on/off
   integer, parameter :: kind_focus = 4   ! Focus data
   !
   integer, parameter :: a_velo = 1       ! X unit is velocity
   integer, parameter :: a_freq = 2       ! X unit is frequency
   integer, parameter :: a_wave = 3       ! X unit is wavelength
   !
   integer, parameter :: mod_unk  = -1    ! Unknown
   integer, parameter :: mod_freq =  0    ! Unfolded frequency switching
   integer, parameter :: mod_pos  =  1    ! Position switching
   integer, parameter :: mod_fold =  2    ! Folded frequency switching
   integer, parameter :: mod_wob  =  3    ! Wobbler switching
   integer, parameter :: mod_mix  =  4    ! Mixed switching modes
   integer, parameter :: mod_bea  =  5    ! Beam switching modes
   !
   integer, parameter :: u_second = 1     !
   integer, parameter :: u_minute = 2     !
   integer, parameter :: u_degree = 3     !
   integer, parameter :: u_radian = 4     !
   !
end module gbl_constant
