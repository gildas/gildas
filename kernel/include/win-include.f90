!  include.f90 
!
!  FUNCTIONS:
!	include      - Entry point of console application.
!
!	Example of displaying 'Hello World' at execution time.
!

!****************************************************************************
!
!  PROGRAM: include
!
!  PURPOSE:  Entry point for 'Hello World' sample console application.
!
!****************************************************************************
	program include
	use gildas_def
	use image_def
	use phys_const
	use gbl_format

	print *, 'Hello World'

	end program include

