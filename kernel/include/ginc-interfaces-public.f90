module ginc_interfaces_public
  interface
    function uv_column_name(code)
      !---------------------------------------------------------------------
      ! @ public
      ! As it is not portable to share a character array between libraries,
      ! this function returns the UV column name given its code.
      !---------------------------------------------------------------------
      character(len=12) :: uv_column_name
      integer(kind=4), intent(in) :: code
    end function uv_column_name
  end interface
  !
  interface
    subroutine uv_column_names(list)
      !---------------------------------------------------------------------
      ! @ public
      ! As it is not portable to share a character array between libraries,
      ! this function returns the UV column name list.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: list(:)
    end subroutine uv_column_names
  end interface
  !
  interface
    function gdf_polar_name(code)
      !---------------------------------------------------------------------
      ! @ public
      ! Return the polarization name given its code
      !---------------------------------------------------------------------
      character(len=1) :: gdf_polar_name
      integer(kind=4), intent(in) :: code
    end function gdf_polar_name
  end interface
  !
  interface
    subroutine gdf_polar_code(name,code,error)
      !---------------------------------------------------------------------
      ! @ public
      ! This function returns the polarization code given its name (must be
      ! upcased)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer(kind=4),  intent(out)   :: code
      logical,          intent(inout) :: error
    end subroutine gdf_polar_code
  end interface
  !
  interface
    function gdf_stokes_name(code)
      !---------------------------------------------------------------------
      ! @ public
      ! Return the Stokes name given its code
      !---------------------------------------------------------------------
      character(len=4) :: gdf_stokes_name
      integer(kind=4),  intent(in)    :: code
    end function gdf_stokes_name
  end interface
  !
  interface
    subroutine gdf_stokes_code(name,code,error)
      !---------------------------------------------------------------------
      ! @ public
      ! This function returns the Stokes code given its name (must be
      ! upcased)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer(kind=4),  intent(out)   :: code
      logical,          intent(inout) :: error
    end subroutine gdf_stokes_code
  end interface
  !
end module ginc_interfaces_public
