program grfont
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Construction du jeu de caracteres de GREG a partir de celui de HERSHEY
  !
  ! Organisation :
  !	2 Qualites de caracteres
  !		1 FAST		2 DUPLEX
  !	3 Jeux de caracteres par qualite
  !		1 ROMAIN	2 GREC		3 SCRIPT
  ! 	95 Caracteres par jeu
  !
  ! Addressage base sur le code ASCII du caractere
  !
  ! INDEX = ICHAR(caractere) - ICHAR(' ') + 1 + 100*(NJEU-1) + 300*(NQUAL-1)
  !---------------------------------------------------------------------
  ! Local
  integer :: nl,ier
  character(len=filename_length) :: argum,file
  !
  select case (command_argument_count())
  case (0)
    file = 'gag-font.bin'
  case (1)
    call get_command_argument(1,argum,nl,ier)
    if (ier.ne.0)  call sysexi(fatale)
    ! Parse input pattern
    call sic_parse_file(argum,' ','.bin',file)
  case default
    print *,'F-HERSHEY, Program expects 0 or 1 argument'
    call sysexi(fatale)
  end select
  !
  call hershey2gag(file)
  !
end program grfont
