.ec _
.ll 76
.ad b
.in 4
.\"====================================================================
.ti -4
1 Language
.ce
PARAMS\ Command Language Summary

.nf
ALPHA       : Sequence to toggle into VT100-like alpha plane
CURS1       : Sequence to request the graphic cursor
CURS2       : Sequence to reset the terminal when the cursor has exited
COLOUR      : Sequence(s) to select colours
DASH        : Sequence(s) to select hardware dashed lines
DEFOUT      : Default logical name pointing to the terminal device
ERASE       : Sequence to clear the graphic screen
FORMAT      : Select a format to specify and list escape sequences
GRAPHIC     : Sequence to toggle to graphic plane
INIT1       : Initialisation sequence (GTOPEN)
INIT2       : Reset sequence (GTCLOS)
PHEIGHT     : Height of hardware characters (pixels)
PIXEL__RANGE : Limits of the screen in pixels
PROTECT     : Screen protection
PROTOCOL    : Graphic protocol supported by the terminal
PWIDTH      : Width of hardware characters (pixels)
RXY         : Aspect ratio of the screen
TEXT1       : Sequence to initiate a hardware character generator
TEXT2       : Sequence to terminate hardware characters
WAIT__ERASE  : Specify a wait time after ERASE
WEIGHT      : Sequence(s) to select hardware weightened lines
.fi
.\"--------------------------------------------------------------------
.ti -4
2 NEWS
.ti +4

See HELP GTVDEF NEWS
.\"====================================================================
.ti -4
1 FORMAT
.ti +4
[PARAMS_\]FORMAT [format]

FORMAT selects a format to specify and list the character strings that
define the escape sequences. Available formats are DECIMAL,
HEXADECIMAL and ASCII.
.sp
1._ Decimal format is in the form "<d1><d2>..." where d1, d2,... are
decimal unsigned byte values enclosed in brackets.
.sp
2._ Hexadecimal format is in the form "<h1><h2>..."  where h1,
h2,... are hexadecimal unsigned byte values enclosed in brackets.
.sp
3._ Ascii format is in the form "c1c2..." where c1, c2,...  are either
ordinary ascii characters (SIC separators and brackets excluded) or
decimal unsigned byte values enclosed in brackets (as for decimal
format) or usual mnemonics for control characters enclosed in brackets
(such as <ESC>, <US>, etc.).

To list or modify the format, just enter FORMAT with no argument. The
current format is returned with an error condition, and the line
keypad editing mode of SIC is triggered.

Escape sequences are always preceded by a decimal count corresponding
to the number of bytes of the string. A count -1 or 0 suppresses the
escape sequence. By convention, a count value of -1 generally means
that the corresponding action is not available (e.g. ALPHA, GRAPHIC if
no alpha plane is available, CURS1 if no cursor is available, etc...),
and 0 means that the corresponding action is available but is managed
internally for the current protocol.

To list or modify an escape sequence, just enter the associated
command with no argument. The current sequence is returned with an
error condition, and the line keypad editing mode of SIC is triggered.
.\"====================================================================
.ti -4
1 ENDOFHELP
