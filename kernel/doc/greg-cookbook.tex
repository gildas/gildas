\section{GreG CookBook}

This chapter is a very simplified introductory manual to \greg\ designed
for occasional user who just want an introduction or better, a recipe.  It
just describes by means of a commented procedure, how to obtain a standard
plot for the most usual cases. A user will often want to do more than
described here. This is usually not only possible, but even easy. However,
such user will have to understand fully the basic concepts of \greg\, and
to read more or less completely the \greg\ {\tt MANUAL}.

Before starting, remember that \greg\ keeps track of all effective commands
in an internal buffer for replay capabilities used mostly for repetitive
actions and in a log file for post-session control. This log file can be
replayed by \greg\ as any other valid procedure of \greg\ commands, thus
making of \greg\ a safe system protected against system (or internal...)
crashes.

You must know also that \greg\ uses virtual memory to store all plot
coordinates in a device independent way. Hence a plot is entirely
independent of any graphic device. You can turn off your graphic device,
provided you do not exit from \greg\, your plot is still available. This
Virtual Plot storage also implies that your plot and the way you display it
are really two different things. While it may seem confusing at the
beginning, this make possible very nice features such as internal Zooming,
corrections of erroneous part of the plot and a fast Hardcopy system.

\subsection {Starting, Syntax and Help}

\greg\ is started by typing {\tt GREG} on your computer.  You should
normally get a few text lines, then the prompt {\tt GreG>} by which \greg\ 
indicates it is at your service. If that does not work, ask your system
manager, he should know.

The command syntax is describe in the \sic\ manual. Commands have
positional arguments, and the argument list can be followed by options
(names preceded by a slash $/$), each of which may also have a list of
argument.  Arguments and options are separated by spaces.  If your are
lost, do not hesitate to type \com{HELP}. \greg\ is largely self
documented, and it is quite a good idea to use this facility rather than
always refer to the manual.

\subsection {Coordinates}

There are several coordinate systems in \greg\, two of which are the usual
basic coordinate systems of any graphic package :
\begin{itemize}
\item {\em the paper or Physical Coordinate System}, whose units are
  arbitrarily defined as ``centimeters'' (the size of such ``centimeter''
  may vary from one graphic support to the other, but the dimension is
  always that of a distance)
\item {\em the User Coordinate System}, whose units may be anything from
  ``Butterflies per Megaparsec cube'' to ``Molecules per Kelvin degree and
  per km/s'', or anything sensible like this.
\end{itemize}

Plotting data is only a way to match these two coordinate systems, and
additional work such as labelling and annotation may be most conveniently
referred to one or the other system. \greg\ handles the transformation of
coordinates and allows access to each system individually.

\subsection{Demonstration Procedure for \greg\ capabilities}

All commands given here are followed by a comment area (after the
exclamation mark {\tt !}) in which a number refers to the detailed
explanation after the text of the procedure. Comment lines are also in the
body of the procedure for clarity.

\begin{verbatim}
!
! Basic plot : a simple curve
DEVICE XLANDSCAPE WHITE                      !1
SET PLOT LANDSCAPE                           !2
SET FONT SIMPLEX                             !3
COLUMN X 1 Y 2 /FILE gag_demo:greg1.tst      !4
LIMITS                                       !5
BOX                                          !6
LABEL "Offset frequency (s\\U-1\\D)" /X      !7
LABEL "Signal strength (Jy)" /Y              !8
CONNECT                                      !9
!
! More difficult: Markers and Errorbars
CLEAR PLOT                                   !10
LIMITS -50 50 * *                            !11
SET MARKER 4 1 .1                            !12
POINTS                                       !13
CURVE                                        !14
COLUMN Z 3                                   !15
ERRORBAR Y                                   !16
LABEL "Offset frequency (s\\U-1\\D)" /X      !17
LABEL "Signal strength (Jy)" /Y              !18
SET CENTERING 5                              !19
DRAW TEXT 0.0000E+00 10.70 "Demonstration of GREG : Curve and Errorb-
ars" /USER                                   !20
!
HARDCOPY /PRINT                              !21
\end{verbatim}

\begin{enumerate}
\item \index{DEVICE} First, define the graphic support to which you want to
  send all graphic output. Here this is an X-Window terminal in landscape
  mode, with white background: a new window with the title {\tt $<$GREG}
  will appear. Note that it is not necessary to define a graphic support to
  begin drawing data. You may define (or change) your graphic support at
  any moment during the session without losing your plot.
\item Specify the plot page has a ``landscape'' aspect (30 by 21 cm).
\item Specify the character font is the simplex one (the faster to plot).
  \index{COLUMN}
\item Then read your data. The data file has a ``Table''-like organisation
  one column describes all the values of a given variable for all the data
  points, while one line contains the values of all the variables for a
  given data point. You can access any part of the input table.  The data
  is read in list-directed format, so that values must be written in
  formatted way, with spaces, tabs or commas as separators.  Here, the X
  value is read in column 1 (the Y value in column 2) from lines 4 to 30 of
  the input file {\tt gag\_demo:greg1.tst}.  {\tt gag\_demo:} is a {\em
    logical name} translated to a directory path by the software, so that
  {\tt gag\_demo:greg1.tst} will be expanded to something like {\tt
    /users/soft/gag/demo/greg1.tst}.  \index{LIMITS}
\item Define the values of the X and Y coordinates at the corners of the
  plot window. Here, the automatic setting is used; it computes extrema of
  X and Y values, then add some 10 percent margin to get the plot limits.
  {\tt LIMITS} is a very fundamental command, because it defines the
  conversion formula between the user coordinates and the physical
  coordinates. As \greg\ almost always plots in User coordinates, no plot
  action should be issued before command {\tt LIMITS}.  \index{BOX}
\item Draw labelled axis around the plot window. This is useful in any
  plot, but contrary to {\tt LIMITS}, this command is not compulsory. It
  can be issued at any moment (after {\tt LIMITS} of course).
  \index{LABEL!/X}
\item Write the caption of the X axis, in particular the type and units of
  the user coordinate.  \index{LABEL!/Y}
\item Same as above for the Y axis. User coordinates can be anything as you
  can see...  \index{CONNECT}
\item Now connect the data points by straight lines to get a simple curve.
  If your data is not sorted, it will be quite a mess... Note that command
  {\tt BOX, LABEL} and {\tt CONNECT} could appear in any order, but after
  the command {\tt LIMITS}. At this stage, your plot looks like the figure
  1.  
  \begin{figure}[htbp]
    \centering
    \includegraphics[width=0.66\textwidth{},angle=270.0]{greg-fig1}
    \caption{The X-Window graphic window just before clearing the plot 
      in the demonstration procedure.}
    \label{fig:1}
  \end{figure}
  \index{CLEAR!PLOT}
\item This command not only clears the graphic screen, but also destroys
  the current drawing. A new empty window is re-created afterwards.
  \index{LIMITS}
\item Define the values of the X and Y coordinates at the corners of the
  plot window. Here, manual setting is used for X axis, and automatic
  setting for Y axis.  \index{SET!MARKER}
\item Define the type of marker you want. A marker is a regular polygon
  defined by its number of summits (0 to infinite), the type of polygon (0
  to 4) and diameter in physical units (which on a paper output will be
  centimeters). Try the different values for the type to see what it means.
  \index{POINTS}
\item Now, plot a marker as defined above at each data point. Note that you
  do not need to read again your data, neither to define again the user
  coordinates, the last values have been retained.  \index{CURVE}
\item and connect the data points by a smooth curve.  \index{COLUMN}
\item Read data into the Z buffer from column 3. You need not redefine the
  data file, neither the line range which are the same as in command number
  2. The Z buffer is similar to the X and Y arrays into \greg\, but used in
  different ways.
%%EDIT
%\index{CLEAR!SEGMENT}
%\item Delete the part of the plot corresponding to the last graphic
%command ({\tt POINTS}). The screen is not updated.
%\index{ZOOM!REFRESH}
%\item Refresh the graphic screen to reflect the previous operation. It
%involves only reading the internal plot storage and involves very
%little computation. Your screen now only displays the box and the labels.
  \index{ERRORBAR}
\item Use the values in the Z buffer to draw symmetric error bars along the
  Y axis at each data point. The Z values will define the length of each
  side of the bar in Y user coordinates.
\item Same effect as above, but since the plot has been cleared\ldots
\item idem\ldots
\item idem\ldots
\item Specify the text centering mode
\item write some legend to the figure \index{HARDCOPY!/PRINT}
\item This plot seems good, make a hardcopy to save it. With the option
  /PRINT the command sends the hardcopy to the default system printer. This
  command has many other possibilities, such as creating PostScript files.
  Your plot should look like figure 2.
  \begin{figure}[htbp]
    \centering
    \includegraphics[width=0.66\textwidth{},angle=270.0]{greg-fig2}
    \caption{The printed output of the simple demonstration procedure.}
    \label{fig:2}
  \end{figure}
\end{enumerate}

You have now mastered most of the 1 dimensional capabilities. At this
point,  you may want to add some title or any other comment on your 
plot. Then you will need to get a hardcopy of your drawing and to
send it to a plotter.

\subsection{Annotations or Using the Cursor}

Most additional annotations can be done using the cursor, which is called
by command \com{DRAW}.  In command \com{DRAW}, the user can specify
coordinates in three available systems :
\begin{itemize}
  \index{SET!COORDINATES}
\item The USER coordinate system, as specified above, which can be defined
  as the default system by command {\tt SET COORDINATE USER}
\item The BOX coordinate system. In this system, coordinates are offsets in
  physical units from one of the 9 most remarkable points in the box (the
  corners, the center and the middle of sides) numbered according to a
  standard numeric keypad notation as on VT100 terminals. This system can
  be specified using command {\tt SET COORDINATE BOX N}, where N can take
  values 1-9 to specify the corner. The default value N=0 may also be used,
  it behaves differently when the command {\tt DRAW} is used with the
  cursor or explicitly. When using the cursor in {\tt DRAW}, the nearest
  remarkable point will automatically be used as the reference point. If
  you use the explicit form of command {\tt DRAW}, 0 behaves as 1.
\item The {\tt CHARACTER} coordinate system, which is essentially the same
  as the {\tt BOX} system except that offsets are specified in units of
  character size.  \index{SET!CHARACTER} This system can be specified with
  the command {\tt SET CHARACTER N}, where N has the same meaning and
  default value as in BOX. The character size is specified by the command
  {\tt SET CHARACTER Size}.
\end{itemize}

\index{DRAW} By typing DRAW, the cursor appears on the screen.  By hitting
the appropriate key on your keyboard, you can obtain different actions :

\begin{itemize}
  \index{DRAW!RELOCATE}
\item{\tt R for RELOCATE} defines the current cursor position as the new
  pen position. Nothing visible happens because this is a pen up movement.
  \index{DRAW!LINE}
\item{\tt L for LINE} draws a line from the current pen position to the
  cursor position (pen down movement).  \index{DRAW!MARKER}
\item{\tt M for MARKER} draws a graphic marker of the current type and size
  centered at the cursor position.  \index{DRAW!ARROW}
\item{\tt A for ARROW} draws an arrow from the last pen position to the
  cursor position.  \index{DRAW!TEXT}
\item{\tt T for TEXT} prompts you for a string to be written at the cursor
  position (with the current centering option). Enter your text, then type
  {\tt RETURN}, and the text will appear on the graphic screen.
\item{\tt C for ``Centered TEXT''} allows to override the current centering
  option. It prompts you for a string, then for the centering option you
  want to use.
\item{\tt E for EXIT} allows you to escape from this forever looping
  command.
\item{\tt D for DELETE} destroys the vectors drawn in the last operation.
  This is equivalent to \comm{CLEAR}{SEGMENT}, except that only the
  segments created by the current \com{DRAW} command can be deleted in this
  way.  Note that the plot is not refreshed. To refresh the plot, hit E to
  {\tt EXIT} from the {\tt DRAW} loop and use the command
  \comm{ZOOM}{REFRESH}.
\end{itemize}

After each of the precedent action but {\tt EXIT}, the cursor position
becomes the current pen position. Anything else usually gives you the
cursor coordinates. However some letters may be used to add new
possibilities in command {\tt DRAW}. Never press {\tt RETURN} or {\tt
  \^{}Z} while the cursor is on, as this causes sometimes dramatic
effects...

Note that by default, clipping within the box is turned off when you use
command {\tt DRAW}. Clipping can be enforced for the actions {\tt LINE,
  ARROW} or {\tt MARKER} by using the {\tt /CLIP} option when
\index{DRAW!/CLIP} you type the command. The action {\tt TEXT} is never
clipped.

When there is no cursor available (no graphic device active, or no cursor
on the graphic device), to use command {\tt DRAW} you must type explicitly
the complete command as follows :

\begin{itemize}
\item{\tt DRAW RELOCATE Xc Yc [/BOX N] [/CHARACTER N] [/USER] [/CLIP]} to
  relocate the pen at position (Xc,Yc). The coordinates is the coordinate
  system specified in the option, or the default coordinate system as
  defined by command {\tt SET COORDINATE}.
\item{\tt DRAW LINE Xc Yc} to draw a line
\item{\tt DRAW ARROW Xc Yc} to draw an arrow
\item{\tt DRAW MARKER Xc Yc} to draw a marker
\item{\tt DRAW TEXT Xc Yc ``Text to be written'' I} to write the string
  ``Text to be written'' at (Xc,Yc) where I is the centering option used
  (this explicit form also corresponds to the code C used with the cursor).
\end{itemize}

This explicit form can also be used on interactive devices. The cursor will
not be called in such case. There is no explicit form for D and E.  In
interactive mode, the explicit command corresponding to the cursor action
is written to the Log File and to the internal stack. Accordingly, the
stack can be replayed to produce the same results without any interaction
with the cursor.  The choice of the coordinate system is beyond the scope
of this cookbook; for a single plot it should not matter.

\subsection{Getting Hardcopies}

\subsubsection{The easy way}

Now that you have obtained a wonderful plot on the screen, you may want to
draw it on a paper sheet. As shown in the procedure, command
\comm{HARDCOPY}{/PRINT} prints out such a hardcopy on the default printer of
your installation. That is the easy way, but it should have been configured
properly by your system manager (or your local {\bf GILDAS} expert) to work
properly.

\subsubsection{Printable (PostScript or others) Files}

Instead of getting an immediate paper copy, you may prefer to create
a file in some graphic format, such as PostScript, HPGL or others, for
later printing or insertion in other documents.

To get a PostScript file from your \greg\ plot, use the following command\\
\index{HARDCOPY!/DEVICE}
{\tt HARDCOPY plot.ps/DEVICE PS FAST}\\
which create a {\tt plot.ps} file containing a PostScript translation of
the plot. The {\tt /DEVICE PS} option indicates that the format of the file
is PostScript, and the {\tt FAST} argument indicates that line thickness
and dash pattern should be generated by PostScript rather than by \greg\
(this produces smaller files, but at the expense of small clipping errors
in case of very wide lines). Two other variants of the {\tt PS} device
exist: {\tt GREY} for greyscale plots, and {\tt COLOR} for color devices.
Device EPS is similar to device PS, but the {\tt BoundingBox} (in PostScript
sense) is computed from the the plot boundaries rather than from the
Plot Page size.

A {\tt /DEVICE HPGL} option would have created an output file using the
HP-GL command language. So far, PS and HPGL are the only supported output
formats. Others (such as PCL for example) may come later.

\subsubsection{Metacode File}

Rather than preparing an output file in PostScript or HPGL, you can
also produce it in \greg\ format directly. This is done using the
\com{METACODE EXPORT} command. The file produced can be later
reinserted in a \greg\ plot for further processing using the
\com{METACODE IMPORT} command. These commands are described in more
details in the section ``Plot Structure''.

\subsubsection{Raster Devices}

For raster devices (not supporting vectors), a somewhat different method
is used to get a printout. First, a vector file (default extension .vec)
is created, and it is then processed by another utility to produce a
drawing. Several different utilities are provided to do so. Their use
and availability will depend on your own site specificities.

\begin{itemize}

\index{HARDCOPY!PLXY}
\item{\tt PLXY} is used to provide a bit-file for {\tt LXY11 Dec} printers
  (also called {\tt Printronix 300}). This bit-file extension is {\tt .PLT}
  and the name is the same as the input metacode file.
  \index{HARDCOPY!VTXY}
\item{\tt VTXY} is used to plot on Versatec.
  \index{HARDCOPY!LA100}
\item{LA100} is used to create bit-file for an {\tt LA100} Decwriter
  (extension {\tt .LA100}).
\item{ } And others for other supports...
\end{itemize}

\subsection{Internal Zooming}

\greg\ has the possibility of zooming a plot being created. Use command
\com{ZOOM} to do it. {\tt ZOOM X1 X2 Y1 Y2} will compute scale factors to
match the designed area (in Physical Coordinates) to the screen, preserving
the aspect ratio. \comm{ZOOM}{OFF} restores the full screen,
\comm{ZOOM}{REFRESH} redraws the plot with the current zooming factor.
Without arguments {\tt ZOOM} calls the cursor, and several keys can be used
to control the zooming factor :
\begin{itemize}
\item{``0''} turns off zooming and displays the full plot page. The
  previous zooming factor is not destroyed, so that typing a space (see
  below) will restore a Viewing Window of same size as the last one
  centered on the cursor position. The cursor position is not modified by
  the command itself.
\item {``\ ''} (space) executes the standing zooming operations. With
  ``0'', it is the only effective command. The zoomed area is centered
  around the cursor position and can be delimited using command B.
\item{``Z''} increases the zooming factor by 1.414 and draws the
  corresponding box around the zoomed area. The zooming is deferred until
  ``\ '' has been struck, allowing zooming factors to be changed by any
  power of square root of 2.  The box drawn around the zoomed area is not a
  part of the current plot, and will not be visible in a Hardcopy for
  example.
\item{``-''} decreases the zooming factor by 1.414, and draws the
  corresponding box if visible.
\item{``B''} draws a box delimiting the area to be zoomed.
\item{``A''} to erase the alphanumeric screen
\item{``E''} to exit...
\end{itemize} 
Any other gives you the cursor position.  Zooming can be used for example
to enlarge part of a complex drawing to make precise annotations. Note that
the zooming has no effect on the hardcopy.

On X-Window systems, the zoomed area normally appears in another window.
The mouse can be used instead of the keyboard for some actions:
\begin{itemize}
\item left button: as spacebar, execute the zoom
\item middle button: increase zooming factor by 1.414
\item right button: exit
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "greg"
%%% End: 
