.ec _
.ll 76
.ad b
.in 4
.\"====================================================================
.ti -4
1 Language
.ce
ADJUST_\ Language Summary

.nf
ADJUST : Allows fitting arbitrary data with up to 26 parameters.
EMCEE  : Compute an EMCEE Markov Chain
ESHOW  : Show results from an EMCEE Markov Chain
.fi
.\"====================================================================
.ti -4
1 ADJUST
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" [/START G1 G2 ... Gn]
[/STEP S1 S2 .. Sn] [/EPSILON e] [/WEIGHTS w]
[/METHOD Powell|Robust|Simplex|Slatec|Anneal] [/ITER Niter]
[/QUIET] [/ROOT__NAME Name] /PARAMETERS V1 V2 ... Vn
[/BOUNDS Va Lowa Higha [Vb Lowb Highb ...]]

Allows fitting arbitrary data with (almost) arbitrary number of
parameters (well, less than 26 so far, I believe).

The command ADJUST defines the following global SIC variables to hold
its results:
.nf
       Root%PAR    : Fit parameters
       Root%ERRORS : The parameter errors, if they have been computed
       Root%RES    : Residuals (i.e. My__Data-Root%FIT)
       Root%STATUS : .FALSE. on successful completion.
.fi
where Root is the name given in the /ROOT__NAME option (default is ADJ
or MFIT).

My__Data is a SIC variable containing your data to be fitted. It can
be a variable of any rank, but (currently) it must be of type DOUBLE.

"My Command" is a character string specifying the command to be
executed to compute the difference between My__Data and the model.
This difference must be returned in SIC variable Root%RES. The command
is typically
.ti +4
    "@ my__difference"
.br
where "my__difference" is a procedure containing the code to compute
the model and a line to compute Root%RES.  Here is a simple example to
fit a Gaussian (see HELP ADJUST EXAMPLE for more details)
.nf
!
begin procedure my__difference
   let ADJ%res my__data-gauss(xx,amp,pos,width)
end procedure my__difference
.fi
.\"--------------------------------------------------------------------
.ti -4
2 Example
.ti +4

Here is a complete procedure to generate a noisy Gaussian and fit
it with ADJUST. The call sequence is
.nf

  @ gag_demo:demo-adjust Amp Pos Width Noise

.fi
where Amp is the desired amplitude, Pos the position, Width the width
and Noise the noise on the data. 

.nf
! Procedure gag__demo:demo-adjust
!
! An example of ADJUST usage
! For simplicity, let us define the GAUSSIAN function
 define function gauss(x,a,b,c) a*exp(-0.5*((x-b)/c)^2)
!
! The Gaussian parameters, Amplitude, Position and Width
 define double amp pos width /global
! Now the data...
 define integer nx
 let nx 512
 define double xx[nx] my__data[nx] /global
! Define the Measurement points XX
let xx[i] 10*(i-nx/2)/nx
!
! Fill the data with some noisy values. In reality, this may come from
! an experiment or observation
let my__data gauss(xx,&1,&2,&3)
let my__data[i] my__data+&4*noise(i/i)
!
begin procedure my__difference
   let ADJ%res my__data-gauss(xx,amp,pos,width)
end procedure my__difference
!
! Minimize
adjust my__data "@ my__difference" /start 3 2 1 /par amp pos width /root ADJ
examine ADJ%PAR
examine ADJ%ERRORS
.fi
.\"--------------------------------------------------------------------
.ti -4
2 /EPSILON
.ti +4

This option is used to specify the desired tolerance. Its
interpretation is method dependent. For SIMPLEX and POWELL, it means
the relative deviation of the mean squared difference of two fit
iterations. For SLATEC, which uses non-reduced chi-2, it is the
absolute difference between two iterations. For ANNEAL, its
interpretation is totally different.

Use default value 0 to let the program guess. In subtle cases, or to
gain speed in case a coarse result is desired, use values around 1D-5
for SIMPLEX and POWELL, and of order 1 for SLATEC (provided your
weights are really 1/sigma^2...). For ANNEAL, see details in the
/METHOD option.

Note that this is not the absolute error of the fit parameters.
.\"--------------------------------------------------------------------
.ti -4
2 /METHOD
.ti +4
.in +4
.ti -2
ANNEAL_ _ Simulated annealing technique. This may require a very large
number of function evaluation.
.ti -2
POWELL_ _ Gradient using the Powell method with the Davidon-Fletcher
variable metric.
.ti -2
SIMPLEX_ Classical Simplex amoeba search
.ti -2
ROBUST_ _ A combination of Simplex + Slatec, to be used with large
enough initial steps.
.ti -2
SLATEC_ _ Modified Levenberg Marquardt method with adaptive steps, as
implemented in the Slatec (LINPACK) library
.in -4

All methods require a proper choice of initial values and steps. The
ANNEAL method is much more robust against poor guesses, but may
require 10 to 100 times more function evaluations than any other.
It is also difficult to control.
.\"--------------------------------------------------------------------
.ti -4
2 /START
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" /START G1 G2 ... Gn

G1 G2 .. Qn are used to pass starting guesses for the parameters P1 to
Pn. The default is 1.0.  The starting values should not be too far from
a potential solution, otherwise the convergence may not be possible.
.\"--------------------------------------------------------------------
.ti -4
2 /STEP
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" /STEP S1 S2 .. Sn

S1 S2 .. Sn are used to pass the unity vectors (steps) for the
iteration on parameters P1 to. Pn. The default depends on the starting
guesses Gi: Si is equal to abs(0.1_ x_ Gi) or 0.1 if Gi is 0.0 . Poor choice of
initial steps may lead to non convergence. Too small steps will not
help converging when one starts too far from the solution.  Too large
steps may lead to incorrect evaluation of the parameter errors. The
optimal step for the determination of the errors is about the error
bar, so that all parameters become dimensionless.
.\"--------------------------------------------------------------------
.ti -4
2 /PARAMETER
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" /PARAMETER  Aname Bname Cname [...]

Defines the parameter names. The parameter names are Global SIC
variables which are then used in the user supplied command to compute
the model. The variables must exist: they are not created by the
ADJUST command.
.\"--------------------------------------------------------------------
.ti -4
2 /QUIET
.ti +4

Requires ADJUST to be silent (useful to avoid too many messages in
loops).
.\"====================================================================
.ti -4
2 /WEIGHTS
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" /WEIGHT My__weights

Define the weights of each data. The array My__Weights must be real, and of
same size as the array My__Data.

.ti -4
2 /BOUNDS
.ti +4
[ADJUST_\]ADJUST My__Data "My Command" /PARAMETER  Aname Bname Cname [...] 
/BOUNDS Name1 Low1 High1 [Name2 Low2 High2 ...]]

Specify the search boundaries for some parameters. This can be useful to
prevent the  minimization to enter non physical regions. The code does
not (yet) support one sided boundaries such as [Low, +Inf[

.ti +4
Caution:
.br
/BOUNDS is still experimental and may affect the values of the error
bars. Please check the result using unbounded minimization once a minimum
has been found with a bounded search.


.\"====================================================================
.ti -4
1 EMCEE
.ti +4
[ADJUST\]EMCEE [Data Command] [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] [/PARAMETERS Va Vb Vc ...]  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk] [/ROOT__NAME Root]

.ce
***************
.br
Foreword: this command is a wrapper around the "emcee" Python
module. Gildas must have been compiled with the Gildas-Python binding
enabled, and the modules "emcee" and "pickle" must be installed in
your Python version.
.ce
***************

Prepare, begin or continue a Monte Carlo Markov Chain using the EMCEE
method.

Command is the command line which computes a Chi^2 function on your
data.  Typically, it is "@ my__chi2" where my__chi2 is a script
gathering all necessary computations. The resulting chi^2 must be put
into the global variable EMCEE%CHI2

Argument Data is unused so far.

The command has 3 forms. Without argument, it can accept only options
/BEGIN and /LENGTH. It then begins or continues a previously defined
MCMC chain.  With arguments, the /PARAMETER option must be
present. The EMCEE command then defines all parameters for the MCMC
chain, and optionally starts it if /BEGIN is present.
.\"--------------------------------------------------------------------
.ti -4
2 Caution!...

- Contrary to MFIT or ADJUST, EMCEE does ***NOT*** return best fit
values. This can be done a posteriori using the ESHOW command.

- EMCEE cannot be interrupted by ^C.  While EMCEE will react to ^C, it
will not stop properly. You may no longer be able to continue
afterwards, and you may have to kill the whole process. This is
because it uses an intrincate nesting of SIC and Python commands and
macros.
.\"--------------------------------------------------------------------
.ti -4
2 Credits
.ti +4

The EMCEE method uses the affine-invariant ensemble sampler proposed
by Goodman & Weare (2010). The implementation is based on the "emcee"
Python implementation developped by Foreman Mac-Key et al 2012, and is
only available with the Python binding of SIC.
.\"--------------------------------------------------------------------
.ti -4
2 Example
.ti +4

The following demo will fit a Gaussian. Amp must be in range [5,15],
Pos in range [-1,1] and Width in range [0,3].
.eo
.nf
  !
  ! @ gag_demo:demo-emcee  Amp Pos Width Noise
  !
  ! An example of EMCEE usage
  ! For simplicity, let us define the GAUSSIAN function
  define function gauss(x,a,b,c) a*exp(-0.5*((x-b)/c)^2)
  !
  if .not.exist(my_data) then
    ! The Gaussian parameters, Amplitude, Position and Width
    define double amp pos width /global
    ! Now the data...
    define integer nx
    let nx 512
    define double my_xx[nx] my_data[nx] my_noise /global
    ! Define the Measurement points XX
    let my_xx[i] 10*(i-nx/2)/nx
  endif
  !
  ! Fill the data with some noisy values. In reality, this may come from
  ! an experiment or observation
  let my_noise &4
  let my_data gauss(my_xx,&1,&2,&3)
  let my_data[i] my_data+my_noise*noise(i/i)
  !
  ! Start with some random value
  let amp &1*(1+noise(my_noise))
  let width &3*(1+noise(my_noise))
  let pos &2+noise(my_noise)
  !
  begin procedure my_chi2
    define double res /like my_data
    let res my_data-gauss(my_xx,amp,pos,width)
    let res (res/my_noise)^2
    compute emcee%chi2 sum res ! The Chi^2 must be returned here
    @ emcee-timer ! Optional: report advances in the chain
  end procedure my_chi2
  !
  emcee my_data "@ my_chi2" /start 'amp' 'pos' 'width' -
        /par amp pos width /bound amp 5 15 pos -1 1 width 0 10 -
        /step 0.02 0.02 0.02 /length 100 /walk 4 /begin
  for i 1 to 4
    emcee
  next
  !
.fi
.ec _
.\"--------------------------------------------------------------------
.ti -4
2 /BEGIN
.ti +4
[ADJUST\]EMCEE Data Command /BEGIN [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk] [/ROOT__NAME Root]

or

.ti +4
[ADJUST\]EMCEE /BEGIN  [/LENGTH Length]

  Start a Monte Carlo Markov Chain using the EMCEE method.
The short form uses parameters defined in a previous EMCEE command.
.\"--------------------------------------------------------------------
.ti -4
2 /BOUNDS
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] /BOUNDS Par Low Up [...]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk]

Specify the parameter bounds for some or all parameters. If not
specified, the bounds are derived from +/- 10 times the parameter
step.
.\"--------------------------------------------------------------------
.ti -4
2 /LENGTH
.ti +4
[ADJUST\]EMCEE [Data Command] [/BEGIN] [/BOUNDS Par Low Up [...]]
/LENGTH Length [/PARAMETERS Va Vb Vc ...]  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk]

Specify the length of the MCMC chain. The total number of executions
will be the Length times the number of parameters times the number of
walkers per parameter.  Length is the only control allowed to change
when restarting a MCMC chain.
.\"--------------------------------------------------------------------
.ti -4
2 /PARAMETERS
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk]

Specify the parameter names. These must be global SIC variables, so
that they can be used the the "Command" computing the Chi^2.
.\"--------------------------------------------------------------------
.ti -4
2 /ROOT__NAME
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...]  [/WALKERS NWalk] /ROOT__NAME RootName

where Root is the name given in the /ROOT__NAME option (default is ADJ
or MFIT).

Specify the root name for the results.  Results are ***NOT*** computed
by the EMCEEE command, but by the ESHOW command (ESHOW Errors or ESHOW
Results). They will be returned in RootName%PAR and RootName%ERRORS,
like for commands MFIT or ADJUST.
.\"--------------------------------------------------------------------
.ti -4
2 /START
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  /START A1 B1 C1 ... [/STEP
A2 B2 C2 ...]  [/WALKERS NWalk]

Specify the starting values for the parameters (in the same order as the
parameter names).
.\"--------------------------------------------------------------------
.ti -4
2 /STEP
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ...  [/START A1 B1 C1 ...]
/STEP A2 B2 C2 ...  [/WALKERS NWalk]

Specify the random steps for the parameters (in the same order as the
parameter names). If no bound is specified, the Lower and Upper bounds
are +/- 10 times the step away from the starting value.
.\"--------------------------------------------------------------------
.ti -4
2 /WALKERS
.ti +4
[ADJUST\]EMCEE Data Command [/BEGIN] [/BOUNDS Par Low Up [...]]
[/LENGTH Length] /PARAMETERS Va Vb Vc ... [/START A1 B1 C1 ...]
[/STEP A2 B2 C2 ...] [/WALKERS NWalk]

Specify the number of walkers per parameter.
.\"====================================================================
.ti -4
1 ESHOW
.ti +4
[ADJUST_\]ESHOW AUTOCORR|CHAINS|ERRORS|TRIANGLES [Args ...] [BURN
Burn] [/SPLIT]

Show some results of an EMCEE Markov Chain. Except for ESHOW RESULTS and ESHOW
ERRORS, GreG must be available for this action. (Trick: if it is not, just use
command IMPORT MAPPING, and it will be...)
.\"--------------------------------------------------------------------
.ti -4
2 AUTOCORR
.ti +4
[ADJUST_\]ESHOW AUTOCORR [Length] [/SPLIT] [/BURN Burn]

Plot the time correlation for the parameters, and compute the
autocorrelation length up to the specified Length (default
100). "Burn" is the number of steps which must be ignored at the
beginning of the EMCEE chain before performing the action. Default is
50.  /SPLIT option will compute this for each chain instead of
globally.
.\"--------------------------------------------------------------------
.ti -4
2 CHAINS
.ti +4
[ADJUST_\]ESHOW CHAINS [Last]

Plot the MCMC chains.  Number indicates the last iteration to be
displayed.
.\"--------------------------------------------------------------------
.ti -4
2 ERRORS
.ti +4
[ADJUST_\]ESHOW ERRORS [/BURN Burn]

Print on screen the most likely values and their (asymmetric) errors.
"Burn" is the number of steps which must be ignored at the beginning
of the EMCEE chain before performing the action. Default is 50.

Results (median of the distribution) and Errors (68 _% percentiles)
are also returned by this command, in variables RootName%PAR and
RootName%ERRORS (where RootName is the name specified in option
/ROOT__NAME of command EMCEE), like for commands MFIT or ADJUST. The
original variables specified in the /PARAMETER option of command EMCEE
are also set to their median values.
.\"--------------------------------------------------------------------
.ti -4
2 RESULTS
.ti +4
[ADJUST_\]ESHOW Results [Filename] [/BURN Burn]

Print the results and errors (symmetric and asymmetric) errors in a
file.  "Burn" is the number of steps which must be ignored at the
beginning of the EMCEE chain before performing the action. Default is
50.

Results (median of the distribution) and Errors (68 _% percentiles) are
also returned by this command, in variables RootName%PAR
and RootName%ERRORS (where RootName is the name specified in option
/ROOT__NAME of command EMCEE), like for commands MFIT or ADJUST. The
original variables specified in the /PARAMETER option of command EMCEE
are also set to their median values.

The created script is called 'Filename'.sic (default results.sic) and
when executed, will print the results on screen in the same format as
the ESHOW Errors command.  The script can be customized by re-defining
symbol emcee__result (which by default executes "@ emcee-error").
.\"--------------------------------------------------------------------
.ti -4
2 TRIANGLES
.ti +4
[ADJUST_\]ESHOW TRIANGLES [Parameter] [/BURN Burn]

Plot the 2x2 correlation surfaces for all parameters. If a Parameter
name is given, only plot the correlation with this parameter.

"Burn" is the number of steps which must be ignored at the beginning
of the EMCEE chain before performing the action. Default is 50.
.\"--------------------------------------------------------------------
.ti -4
2 /BURN
.ti +4
[ADJUST_\]ESHOW Action [Parameter] /BURN Burn

Indicate the size of the "burn-out" region, i.e. how many steps must
be ignored at the beginning of the chains. Default is 50.
.\"--------------------------------------------------------------------
.ti -4
2 /SPLIT
.ti +4
[ADJUST_\]ESHOW AUTO [length] [/BURN Burn] /SPLIT

Plot the time correlation for the parameters, and compute the
autocorrelation length for each chain.
.\"--------------------------------------------------------------------
.ti -4
1 ENDOFHELP
