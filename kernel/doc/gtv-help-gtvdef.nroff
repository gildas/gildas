.ec _
.ll 76
.ad b
.in 4
.\"====================================================================
.ti -4
1 Language
.ce
GTVDEF_\ Command Language Summary

.nf
DEFAULT   : Resets parameters to default values
DELETE    : Deletes a terminal definition
DIRECTORY : Directory of terminal definitions in the current file
LIST      : Lists terminal parameters (local or binary file)
NAME1     : Primary terminal name
NAME2     : Secondary terminal name
READ      : Reads terminal parameters from current binary file
RENAME    : Renames a terminal definition in the current file
USE       : Selects/creates a Binary Terminal Definition File
WRITE     : Writes terminal parameters to current binary file
.fi
.\"--------------------------------------------------------------------
.ti -4
2 NEWS
.sp
1._ Preliminary version in use since 7-August-1986
.sp
2._ Special features: TEST command not yet fully debugged, consult
HELP_ TEST; WAIT__ERASE is an integer; specify a negative value for
RXY to disable the packing of Tektro bytes; 12 bit adresses in the
Tektro GIN cursor sequence are not supported...
.sp
3._ In project: more TEST facilities, selection of default plotter
configurations and procedures, HP-GL and GKS protocols, ...
.\"====================================================================
.ti -4
1 DEFAULT
.ti +4
[GTVDEF_\]DEFAULT

DEFAULT resets the parameters of the local table to their default
values.

The primary and secondary terminal names and the terminal protocol
(NAME1, NAME2 and PROTOCOL) are set to an "undefined" state. DEFOUT
is set to "TT".

Escape sequences are disabled (count -1), and most other parameters
are set to zero.
.\"====================================================================
.ti -4
1 DELETE
.ti +4
[GTVDEF_\]DELETE name1 [name2]

Deletes a terminal definition in the current Binary Terminal
Definition File (see USE).

Arguments specify the primary terminal name and optionally the
secondary name. These names can be abbreviated as far as they remain
non ambiguous.
.\"====================================================================
.ti -4
1 DIRECTORY
.ti +4
[GTVDEF_\]DIRECTORY [/OUTPUT OutputFile]

DIRECTORY lists the names of the terminal definitions available in the
current Binary Terminal Definition File (see USE).

The output list is directed to STDOUT by default, or to the file in
argument if specified.
.\"====================================================================
.ti -4
1 LIST
.ti +4
[GTVDEF_\]LIST [name1 [name2]] [/OUTPUT File]

Lists terminal parameters.

The option /OUTPUT allows to redirect the output to a specified file
(default is STDOUT). An output file can be used later as a command
procedure for GTVDEF. For clarity, default parameters values are not
listed.

Three modes of operation are selected by the arguments of the command.
.sp
1._ With no argument, lists the local parameters.
.sp
2._ If a primary terminal name (and optionally a secondary name) are
specified, lists the associated definitions in the current Binary
Terminal Definition File (see USE). These names can be abbreviated as
far as they remain non ambiguous.
.sp
3._ If the argument is a star (*), lists ALL definitions in the
current BTDF (Binary Terminal Definition File) file. This feature is
designed to facilitate the migration of existing terminal definitions
to a new version of the GTVIRT environment.
.\"====================================================================
.ti -4
1 NAME1
.ti +4
[GTVDEF_\]NAME1 [name1]

NAME1 sets the primary terminal name. A secondary terminal name can be
also defined to discriminate between several modes of operation for
the same terminal (see NAME2).

Primary and secondary terminal names are used by the GTVIRT library
and by the associated softwares to retrieve a terminal definition in
the Binary Terminal Definition File (see USE). They appear for
instance as argument of the command DEVICE to select the current
terminal in LAS, GreG and Gildas.

To modify a name, just enter the command with no argument. The current
name is returned with an error condition, and the line keypad editing
mode of SIC is triggered.
.\"====================================================================
.ti -4
1 NAME2
.ti +4
[GTVDEF_\]NAME2 [name2]

NAME2 sets the secondary terminal name. A secondary terminal name may
be specified to discriminate between several modes of operation for
the same terminal (see NAME1).

To modify a name, just enter the command with no argument. The current
name is returned with an error condition, and the line keypad editing
mode of SIC is triggered.
.\"====================================================================
.ti -4
1 READ
.ti +4
[GTVDEF_\]READ name1 [name2]

READ reads terminal parameters from the currently selected Binary
Terminal Definition File (see USE) into the local parameter table in
core memory.

The terminal is identified by its primary name and optionally by its
secondary name as specified in argument(s). These names can be
abbreviated as far as they remain non ambiguous.
.\"====================================================================
.ti -4
1 RENAME
.ti +4
[GTVDEF_\]RENAME old1 [old2] /AS new1 [new2]

Renames a terminal definition in the current Binary Terminal
Definition File (see USE).

Arguments of the command refer to the old primary and secondary names.
Arguments of the option /AS refer to the new names.

Old names can be abbreviated as far as they remain non ambiguous. New
names are checked against any ambiguity with other terminal names.
.\"====================================================================
.ti -4
1 USE
.ti +4
[GTVDEF_\]USE [File [/CREATE]]

USE selects a Binary Terminal Definition File (BTDF) to be used in
connection with commands DIRECTORY, READ, WRITE, LIST, RENAME and
DELETE. The option /CREATE creates an empty BTDF file. Remember that
the GTVDEF library accesses the BTDF file pointed to by the logical
name GAG__TERMINAL.

USE with no argument lists the name of the current BTDF file.

The GTVDEF utility allows to manipulate a local terminal definition
table which is volatile (in core memory). The local table can be
initialised with the command DEFAULT or can be read from an external
file using the commands USE and READ. The target terminal name is
defined by the commands NAME1 and NAME2.

The language PARAMS_\ is designed to set up or to modify the current
terminal parameters in the local table.

Do not forget to issue the command WRITE in order to save the local
table into some external BTDF file of your choice.

Other commands of language GTVDEF_\ allow to maintain BTDF
files. Command LIST is very useful to dump the contents of a BTDF file
in a readable format suitable for later input by the GTVDEF utility.
.\"====================================================================
.ti -4
1 WRITE
.ti +4
[GTVDEF_\]WRITE

WRITE writes terminal parameters from the local parameter table to the
current Binary Terminal Definition File (BTDF, see USE).

Current primary and secondary terminal names are used (see NAME1 and
NAME2), and checked against any ambiguity with existing terminal names
in the BTDF file. If the terminal name is already present, its
definition is updated.

The total number of terminal definitions in a BTDF is limited by the
NTMX built-in parameter (currently 32).
.\"====================================================================
.ti -4
1 ENDOFHELP
