.ec _
.ll 76
.ad b
.in 4
.\"====================================================================
.ti -4
1 Language
.ce
GUI_\ Command Language Summary

.nf
BUTTON : Associate a command with a button
END    : Read parameters from window and set variables accordingly
GO     : Activate the currently defined window
MENU   : Create a pulldown menu for next buttons
SUBMENU: Create a submenu in the current menu
PANEL  : Define or delete an input window or menubar
WAIT   : Wait for GO button in the current window
URI    : Open any kind of URI, e.g. html link or file name.
.fi
.\"====================================================================
.ti -4
1 BUTTON
.ti +4
GUI_\BUTTON "Command" Button ["Title" HelpFile [Option_Title]]
.ti +20
(Graphic-User-Interface mode only)

Creates a button widget to execute the specified "Command".

If no "Title" is given, the button will have no associated variables,
and will appear with other similar buttons at the top of the window.

If a "Title" argument is present, a "secondary parameters" window is
created. In the main window, the "title" appears followed by 3
buttons: one with the button name, one pointing to the secondary
parameters window, and a "HELP" button. All subsequent LET commands
will create widgets in this secondary window, until a new GUI_\BUTTON
command is typed. "HelpFile" specifies a text file where the help for
the variables can be found, and "Option_Title" is a title for the
secondary window (and associated button).
.\"====================================================================
.ti -4
1 END
.ti +4
GUI_\END

Reads all parameters from the current window(s) and set the modified
variables accordingly. Normally reserved for programming applications.
.\"====================================================================
.ti -4
1 GO
.ti +4
GUI_\GO ["Command"]

Map the windows defined by the previous GUI_\PANEL command and its
associated GUI_\BUTTON and LET commands. "Command" is an optional
command to be executed when button "GO" is pressed.
.\"====================================================================
.ti -4
1 MENU
.ti +4
GUI_\MENU "Title" [/CLOSE] [/CHOICES]

Creates a pulldown menu to group a set of buttons without associated
parameters. Subsequent GUI_\BUTTON or GUI_\URI commands will add
buttons in the pulldown menu.

When option /CLOSE is present, closes the current pulldown menu.
Subsequent GUI_\BUTTON or GUI_\URI commands will create buttons on the
main menubar.

This command is valid only when creating a detached menubar, i.e.
after a GUI_\PANEL/DETACH command has been typed.
.\"====================================================================
.ti -4
1 SUBMENU
.ti +4
GUI_\SUBMENU "Title" [/CLOSE]

Create a new submenu in the current menu. Subsequent GUI_\BUTTON or
GUI_\URI commands will add buttons in this submenu.

When option /CLOSE is invoked, closes the current submenu. Subsequent
GUI_\BUTTON or GUI_\URI commands will create buttons in the parent
menu.
.\"====================================================================
.ti -4
1 PANEL
.ti +4
GUI_\PANEL "Title" HelpFile [/DETACH] [/LOG LogFile]
.ti +4
GUI_\PANEL [HelpFile] /CLOSE

Activate the Graphic-User-Interface input mode for variables. A window
with the specified title is created, but not mapped to the screen.
Successive LET commands will create widgets in this window to allow to
modify variables by entering values in the widgets. Command GUI_\GO
"Command" will map the window to the screen. Once the proper input has
been defined, clicking on the "GO" button will setup all the related
variables in the main program, and execute the associated
command. Clicking on button "UPDATE" will only set the variables.
Clicking on button "ABORT" will return without modifying the
variables.

Help is available in the window through the "HELP" button, but also
clicking in any prompt area.

See LET command for details.

If option /DETACH is present, a menubar is created instead of a normal
window. Several buttons can be attached to this menubar using the
GUI_\BUTTON and GUI_\MENU commands, but no variables can be set in
this mode. The menubar is mapped when command GUI_\GO is typed.

If option /CLOSE is present, the last detached menubar, or the
specified one, is deleted.

/LOG option stores variable definitions in specified log file.
.\"====================================================================
.ti -4
1 WAIT
.ti +4
GUI_\WAIT

Wait for button "GO" "UPDATE" or "ABORT" to be pressed. The command
monitor stays in hold state until one of these buttons in the main
window are pressed.
.\"====================================================================
.ti -4
1 URI
.ti +4
GUI_\URI

Open any kind of URI (Uniform Resource Identifier), e.g. html link or
file names, for example:
.nf
  GUI\URI  "http://www.iram.fr/IRAMFR/GILDAS/"  "GILDAS Web Page"
  GUI\URI  gag__doc:pdf/gildas-intro.pdf         "GILDAS Introduction"
.fi

The choice of the software used to open the URI is let as much as
possible to the system (which should itself honor the user's
preferences).
.\"====================================================================
.ti -4
1 ENDOFHELP
