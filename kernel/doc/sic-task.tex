\section{Running Tasks}

This section contains the minimum information required to use the \gildas\ 
image processing tasks.

To run tasks, use the commands {\tt RUN} and {\tt SUBMIT} from the
{\tt VECTOR$\backslash$} language.  Both commands are
very similar. The \com{RUN} command will execute the task as a detached
process, and the \com{SUBMIT} command in a batch queue named {\tt
GILDAS\_BATCH}. 

The {\tt VECTOR$\backslash$} language contains the following commands :
\begin{verbatim}
  FITS            : A simple FITS -- Gildas conversion tool
  RUN Program     : Activates a GILDAS task in a detached process
  SPY [Task]      : Look at the status of one or all GILDAS tasks.
  SUBMIT Program  : Submit a GILDAS task to GILDAS_BATCH queue
  TRANSPOSE       : A command to transpose a Gildas data file.
\end{verbatim}

\subsection{Window Mode}

The window mode is the default mode on X-Window systems with Motif
interface. Let us assume in the following example we want to execute a task
named {\tt example}. To activate {\tt example}, the user will type
\begin{verbatim}
     VECTOR> RUN example
or 
     VECTOR> SUBMIT example
\end{verbatim}
A separate input window is created:
\begin{figure}
  \includegraphics[width=\textwidth{}]{sic-task-example}
\end{figure}
The user can then modify any of the parameters by clicking in the dialog
areas. Help can be obtained by clicking on the HELP button, or on any
parameter description.

Since \sic\ is used, parameter values can be variables or arithmetic
expressions (e.g.  {\tt 2*PI+EXP(X[3]}) is a perfectly valid value for a
real, provided the array {\tt X[n]} with {\tt n>3} has been previously
defined).

Once all parameters are defined, the task can be launched by clicking the
OK button, or aborted using the ABORT button. Parameter values are checked,
and if all parameters are valid, the task is executed (or submitted). If
one parameter is invalid, the {\tt RUN} or {\tt SUBMIT}
command sends back a message :\\
{\tt E-RUN,  Missing GO command}\\
and returns an error.

\subsection{Query Mode}

When no window-mode is available, the user is prompted for the parameters.
In this example, the dialog will be
\begin{verbatim}
     An integer value
     INTEGER     I$           1 <CR>
     A value between 0 and 1
     REAL        A$           0.1 <CR>
     Any character string
     CHARACTER   CHAIN$       ABCD <CR>
     4 Real values 
     REAL        ARRAY$[4]    1 2 3 4 <CR>
     A valid name
     FILE        FILE$        TESTFILE.DAT <CR>
     Any values 
     VALUES      OLD$         acos(-1) 1.234 <CR>
\end{verbatim}
The prompting method is always the same: an explanatory first line
indicating the meaning of the parameter, and a second line in the following
format:
\begin{verbatim}
     TYPE NAME[Dimensions] 
\end{verbatim}
where
\begin{itemize}
\item {\tt TYPE} indicates the type of parameter {\tt (CHARACTER, FILE,
    INTEGER, LOGICAL, REAL)}. A parameter of type {\tt FILE} is a character
  string containing a valid file name. {\tt VALUES} is intended to hold
  multiple values (which can be mathematical expression), without any prior
  on the number of values.
\item {\tt NAME} is the parameter name
\item {\tt [Dimensions]} are the parameter dimensions, in case it is an
  array.  Only {\tt REAL} and {\tt INTEGER} parameters may be arrays.
\end{itemize}

Query mode is also used for missing parameters in Window-mode.

\subsection{EDIT Mode}

Commands {\tt RUN} and {\tt SUBMIT} execute two \sic\ command procedures,
the {\em Initialization File} {\tt Task.init}, which defines all parameters
needed for {\tt example}, and the {\em Checker File} {\tt Task.check},
which verifies that all parameters are valid. In the example above, the
{\tt example.init} file is

\begin{verbatim}
     
TASK\INTEGER "An integer value"    I$           
TASK\REAL "A value between 0 and 1" A$ 
TASK\CHARACTER "Any character string" CHAIN$ 
TASK\REAL "4 Real values" ARRAY$[4]
TASK\FILE "A valid file name" FILE$ 
TASK\VALUES "Any values"  OLD$
TASK\GO
\end{verbatim}

This is a standard procedure containing commands of a \sic\ language named
TASK$\backslash$. Commands from this language are used to define the
\index{TASK$\backslash$} parameters required by the task, and cannot be
called interactively.  The command syntax is always the same :
\begin{verbatim}
TASK\Command "Prompt String" Parameter$[Dimensions] [Value [...]]
\end{verbatim} %$
where
\begin{itemize}
\item {\tt Command} indicates the type of parameter ({\tt CHARACTER, FILE,
    INTEGER, LOGICAL, REAL}). A parameter of type {\tt FILE} is a character
  string containing a valid file name. {\tt VALUES} is intended to hold
  multiple values (which can be mathematical expression), without any prior
  on the number of values.
  \index{TASK$\backslash$!CHARACTER} \index{TASK$\backslash$!FILE}
  \index{TASK$\backslash$!REAL} \index{TASK$\backslash$!INTEGER}
  \index{TASK$\backslash$!LOGICAL}   \index{TASK$\backslash$!VALUES}
\item {\tt "Prompt String"} is a character string used as a prompt to ask
  for the parameter value(s)
\item {\tt Parameter\$} is the parameter name
\item {\tt [Dimensions]} are the parameter dimensions, in case it is an
  array.  Only {\tt REAL} and {\tt INTEGER} parameters may be arrays.
\item {\tt Value(s)} are the parameter values, an array requiring as many
  values as array elements.
\end{itemize}

Once all parameters have been assigned values, \com{RUN} and \com{SUBMIT}
commands execute the {\tt example.check} file, writing the
current parameter values in an auxiliary file which will be used by the
task {\tt example}. If a parameter is incorrect, an error is returned, and
the task {\tt example} not executed.

Instead of supplying the parameters in a query mode, the user can use a
text editor to edit the {\tt .init} file using command \index{RUN!/EDIT}
\index{SUBMIT!/EDIT}
\begin{verbatim}
     VECTOR> RUN example /EDIT
or 
     VECTOR> SUBMIT example /EDIT
\end{verbatim}
The parameter values can then be typed after the parameter names in the
{\tt example.init} file, using \sic\ continuation marks (``-'' as the last
character of a line) if needed for long command lines.  {\tt example.init}
will be executed after exiting the editor.  If a parameter value is
missing, the user will nevertheless be prompted for it after exiting the
editor.

The text editor called is user defined by the command {\tt
  SIC$\backslash$SIC EDITOR} or the logical name {\tt GAG\_EDIT}.

\subsection{Specifying the .init File}

By default, in Query mode {\tt RUN} and {\tt SUBMIT} commands use the {\tt
  .init} file located in {\tt TASK\#DIR:} search path. 
In {\tt EDIT} mode, the {\tt  .init} file located in the current 
default directory is used if it
exists. To override this default behaviour, you can specify any {\tt .init}
file as the second argument to commands {\tt RUN} and {\tt SUBMIT}.

\subsection{Errors and Aborting}

If an error occurs in the {\tt .init} or {\tt .check} procedure, the
erroneous command will be returned to the user, and the procedure execution
is interrupted by a pause. You can then correct the error, execute the
command, and type {\tt C} or {\tt CONTINUE} to resume the procedure
execution. Or you can type {\tt QUIT} (as in any \sic\ procedure indeed) to
abort the execution, until the {\tt RUN} or {\tt SUBMIT} command returns an
error.

You may also want to abort a {\tt RUN} or {\tt SUBMIT} command while you
are in the editor: typing {\tt QUIT} instead of {\tt EXIT} to end the
editing will do it.

\subsection{Log Files}

A log file is created by the {\tt RUN} command in your {\tt GAG\_LOG:}
directory with the task name as file name and the extension {\tt .gildas};
this log is printed by the {\tt SUBMIT} command. If the user is still
running the main program (GreG or Mapping, etc\ldots ) when a task
completes, he (or she) is warned of the completion with the return status.
Log files are not purged automatically, so that you should take care of
that. They are intended essentially as a debugging aid if something goes
wrong, but you can print them as archive of your data processing.

A command file is created in your {\tt GAG\_LOG:} directory to run or
submit the programs. It is in principle deleted at task completion.

\subsection{Synchronizing Jobs}

The batch queue {\tt GILDAS\_BATCH} should have a job limit of 1, so that
all tasks submitted by command {\tt SUBMIT} execute in sequence.  There may
even be intervening jobs from other users.

Tasks activate by command \com{RUN} must complete before control is returned
to the user.

Command \com{SPY} can be used to monitor the execution of tasks
activated by command {\tt RUN}.

\subsection{Obtaining Explanations: {\tt HELP RUN TaskName} Command}

Help on each individual task can be obtained with the {\tt HELP RUN}
followed by the task name. Help on their parameters can be obtained by
adding the parameter name, or * for all parameters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "sic"
%%% End: 
