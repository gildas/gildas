\section{Advanced Users Guide}

Most of the features described in this section (with the exception of the
{\tt PLOT} and {\tt RGMAP /GREY} commands), are available through commands
of the {\bf GTVL} language, which is the command language supporting the
facilities offered by the graphic library on which \greg\ is build. This
library may also be accessed directly through other programs.

\subsection{Greyscale and Color plots}

\greg\ now has the ability to display color or greyscale images, overlaid
with contours if needed. \greg\ also has area filling capabilities. These
possibilities are best exploited on X-Window terminals and PostScript
printers. They are usually not available on any other devices.

\subsubsection{Bitmap display of images}

The \com{PLOT} creates a bitmap display of a 2-D images, very much like the
command {\tt RGMAP} draws contour maps. As all other commands in the \greg\ 
software, the {\tt PLOT} command essentially works in user coordinates.
The \com{PLOT} command is essentially used in the following way:
\begin{verbatim}
    Greg> SET BOX LOCATION Gx1 Gx2 Gy1 Gy2  ! Define box position
    Greg> RGDATA Varname /VARIABLE          ! Get characteristics from image.
    Greg> LIMITS /RGDATA                    ! Get limits
    Greg> PLOT Varname                      ! Plot the array in the box
! And then other commands
    Greg> BOX
    Greg> RGMAP                             ! Draw contour levels (...)
\end{verbatim}
which shows the similarity between the {\tt PLOT} and {\tt RGMAP} commands.

The {\tt PLOT} command has several options, among which the most important
is the {\tt /SCALING Type Low\_cut High\_cut} option. \index{PLOT!/SCALING}
{\tt Type} indicates which type of transfer function is desired between the
image and the bitmap, and can be either {\tt LINEAR} or {\tt LOGARITHMIC}.
{\tt Low\_cut} and {\tt High\_cut} are respectively the smallest and
largest values plotted. Values below (above) are truncated to {\tt
  Low\_cut} (resp.  {\tt High\_cut}.

Blanking values are recognized by the {\tt PLOT} command. The values used
for blanking are derived from the variable characteristics, or if not set,
by the current \greg\ blanking, or from the {\tt /BLANKING} option.
\index{PLOT!/BLANKING}

The {\tt PLOT} command also has less frequently used options. See the
internal help for details. The sample procedure below produces figure 3:
\begin{verbatim}
SIC\DEFINE IMAGE A GAG_DEMO:SMALL READ
GREG1\LIMITS /RGDATA A
GREG1\SET BOX_LOCATION MATCH
GREG2\PLOT
GREG1\BOX /ABSOLUTE
GREG1\LABEL "Right Ascension" /X
GREG1\SET ORIENTATION 90
GREG1\DRAW TEXT 1.000 .0000E+00 "Declination" 5 /CHARACTER 6
GREG1\SET ORIENTATION 0
GREG1\DRAW TEXT .0000E+00 1.000 "Example of PLOT command" 2 /BOX 8
GTVL\HARDCOPY FIG3.PS /DEVICE PS GREY
\end{verbatim}

\begin{figure}[htbp]
  \centering
% Does not work either in HTML
% \resizebox{12.0cm}{!}{\includegraphics[angle=270]{greg-fig3}}
  \includegraphics[width=0.66\textwidth{},angle=270.0]{greg-fig3}
  \caption{\label{fig:3} An example of bitmap display in GreG.}
\end{figure}

\subsubsection{Filled Areas}

Connected to the bitmap capability, \greg\ can also produce filled curves,
histograms or pie charts. The option {\tt /FILL} of commands \com{CONNECT}
\com{HISTOGRAM} and \com{ELLIPSE} are used for hat.
\begin{itemize}
\item{\tt CONNECT /FILL Colour} \\
  \index{CONNECT!/FILL} closes the polygon and fills it with the current or
  specified Colour. This is incompatible with blanking values.
  

\item{\tt HISTOGRAM /FILL Colour} \\
  \index{HISTOGRAM!/FILL} \index{HISTOGRAM!/BASE} fills the histogram with
  the current or specified colour. It is intended to be used in conjunction
  with the {\tt  /BASE} option.
  

\item{\tt ELLIPSE /FILL Colour}\\
  \index{ELLIPSE!/FILL} \index{ELLIPSE!/ARC} fills the ellipse with the
  current or specified Colour. If the {\tt /ARC} option is also present,
  the corresponding ``part of the pie'' is filled rather than the subtended
  arc.
\end{itemize}
So far, no filling pattern is available.

Contours can also be filled automatically, using command
\comm{RGMAP}{/GREY}.  The {\tt RGMAP /GREY} command does not use the current
pen, but scans pencil colours from 8 to 24, changing the pencil colour by 1
for each new contour. Colours can later be edited using the look-up table
facilities provided by \greg\ (see below).

Finally, the polygon defined by command {\tt POLYGON} can be filled using
the \index{POLYGON!/FILL} {\tt POLYGON/FILL Colour} command, where {\tt
  Colour} is a pen color (0 through 23).

\subsubsection{Bitmap Color Editing}

\greg\ offers the possibility of editing the color look-up table (LUT) for
bitmaps. The colors are represented by three \sic\ variables:
\begin{itemize}
\item{\tt HUE}\\
  The {\tt HUE} array, with values between 0 and 360, represent the color
  on a standard color cycle (from Red to Yellow, Green, Blue, Violet and
  Red again).
\item{\tt SATURATION} The {\tt SATURATION} array, with values between 0 and
  1, indicates the fraction of the original (saturated) color mixed with
  white. 0 is pure white, 1 a purely saturated color.
\item{\tt VALUE} The {\tt VALUE} array, with values between 0 and 1,
  indicates the intensity.  0 is black, 1 is maximum lightness.
\end{itemize}

These variable can be modified by any standard \sic\ expression. The new
values get transferred to the display by typing the command \com{LUT}. This
command also accepts other arguments:
\begin{itemize}
\item{\tt COLOR} Load a default color table
\item{\tt WHITE} Load a black and white color table (White background,
  black sources)
\item{\tt BLACK} Load a black and white color table (Black background,
  white sources).
\item{\tt LUT} Load the color table defined by the arrays. Same as without
  any argument.
\item{\tt EDIT} Calls an interactive look-up table editor (on X-Window
  systems onlyt) {\em not yet implemented}.
\item{\tt Any other} Red Green Blue values are read from the formatted file
  specified as argument. The RGB representation is converted to HSV before
  being used.
\end{itemize}

The color associated with the blanking value can be modified using the {\tt
  B\_HUE, B\_SATURATION} and {\tt B\_VALUE} scalar variables.

\subsubsection{Pen Colors}

\greg\ uses 24 colors for the pencils. Colors 0-7 are fixed colors, with
the following values:
\begin{verbatim}
0 Black or White  (depending on window background color)
1 Red
2 Green
3 Blue
4 Cyan
5 Yellow
6 Magenta
7 White or Black  (the window background color)
\end{verbatim}
Colors 8-23 can be modified by the user through the {\tt P\_HUE,
  P\_SATURATION, P\_VALUE} arrays and \comm{LUT}{/PEN} command, in the same
way as the bitmap colors. The default values for these arrays correspond to
grey intensities from white to black.


\subsubsection{Devices and Hardcopies}

\index{DEVICE!IMAGE} The color facilities are available only under X-Window
systems. Moreover, not all X-Window like devices recognized by \greg\ 
effectively use the color for bitmaps. Only the {\tt DEVICE IMAGE} does.
This restriction exists because the color map allocate by \greg\ is a
private color map. As a consequence, no more than a single \greg\ with {\tt
  DEVICE IMAGE} can operate at any one time on an X-Window terminal. The
pencil colors (pencils from 8 to 23) can always be edited. The number of
bitmap colors is usually 128, but can be less if many color applications
already use most of the available colors of the X-Window terminal.

\index{PostScript!GREY} \index{PostScript!COLOR} For hardcopies, color is
supported only the PostScript format. Two options exists for the type of
PostSsript created: {\tt GREY} or {\tt COLOR}.  The {\tt GREY} version,
used by {\tt HARDCOPY /DEVICE PS GREY}, has a fixed transfer function of
greyscales. The transfer function is non-linear, and reasonably suited for
most astronomical images. The {\tt LUT} command has absolutely no influence
on this type of HARDCOPY.  \index{HARDCOPY!/DEVICE} The {\tt COLOR}
version, used by {\tt HARDCOPY /DEVICE PS COLOR}, uses the current {\tt LUT}
to produce the PostScript output. On Greyscale printers, the colors are
converted to grey intensities using standard formulas (what you would see
by replacing a color display by a monochrome one). Colors are used on color
printers.

\subsection{Plot Architecture and Multi-Window Capabilities}

\subsubsection{Directories}

\greg\ incorporates a fairly sophisticated plot architecture, which allows
the user to organize its plot in logical elements. The organisation is like
a directory tree, with graphic segments attached to a ``directory''.
Directories as a whole (and all their subdirectories) can be manipulated as
single entities. While this organisation is mostly transparent for simple
plots, it can help organize more complex drawings, specially when the user
uses the capability to save subtrees and reincorporate them later in a
plot.

One or more graphic segments are created for each \greg\ command.  The
graphic segment is the smallest entity recognized in the plot.  The
directory architecture is under user-control, although some applications
layered on \greg\ may automatically create their own plot organisation.
Directories can be created using command \comm{CREATE}{DIRECTORY}.  The top
directory is named {\tt '<'}, and cannot be accessed by the user (you
cannot type {\tt CD <}).  Directories attached to the top directory are
named ``main directories''.  To create a main directory, use the following
syntax:
\begin{verbatim}
CREATE DIRECTORY <MAIN
\end{verbatim}
where {\tt MAIN} is the name of the directory to be created.  The command
\begin{verbatim}
CREATE DIRECTORY SUB
\end{verbatim}
creates a subdirectory from the current working directory, for example {\tt
  <MAIN<SUB}.


The user can move across the directory structure using command
\comm{CHANGE}{DIRECTORY} (abbreviation {\tt CD}), print his current working
directory using command \comm{DISPLAY}{DIRECTORY} (abbreviation {\tt PWD}).
Newly created graphic segments are attached to the current working
directory.  A whole directory can be erased using command
\comm{CLEAR}{DIRECTORY}, saved on a metacode file for later use by command
\com{EXPORT}.

Directories and segments have attributes (pen colour and thickness,
transfer function for images, visibility and depth) which can be edited
using command \com{CHANGE}. Directories and segment can be made invisible
by the \comm{CHANGE}{VISIBILITY} command; this is a reversible action until
the \com{COMPRESS} command has been used to effectively destroy all
invisible segments and directories.

Each directory may have its own coordinate system attached. When created,
subdirectories inherit the coordinate system from their parent directory.
The coordinate system is automatically modified by the {\tt SET BOX} and
{\tt LIMITS} commands typed within the directory. This feature allows to
maintain several coordinate systems in a plot. However, the user must then
realize that user coordinates may change when changing from one directory
to another, as for example:
\begin{verbatim}
LIMIT 0 1 0 1
CREATE DIRECTORY NEW
CHANGE DIRECTORY NEW
LIMIT 0 2 0 2
DRAW RELOCATE 0.5 0.5 /USER
CHANGE DIRECTORY ..
DRAW      ! User coordinates are now (0.25, 0.25)
\end{verbatim}



\subsubsection{Windows}
Each directory may have one or several X-Window windows attached to it to
display its content. Each window has its own magnification factor. Windows
are created by command \comm{CREATE}{WINDOW}, which create a new window for
the current directory. They can be deleted by command \comm{CLEAR}{WINDOW}.

The combination of multi-window capabilities with directory structure
allows the user to save a plot in one window, and create a new version of
it in another window for comparison. Each window has its own color table,
although because of terminal limitations, only one color table is active at
a time.

\subsubsection{The {\tt CLEAR} command}

Because of the multi-window and directory capabilities, the \com{CLEAR}
command has many possible arguments.
\begin{itemize}
\item{\tt CLEAR ALPHA} erases the alphanumeric screen alone, without
  affecting the plot.  If possible, rather than erasing the alpha screen,
  the graphic screen (current window) is raised up.
\item{\tt CLEAR DIRECTORY [DirName]} makes the named directory (or by
  default the current directory) invisible. All the attached structure
  becomes invisible.
\item{\tt CLEAR GRAPHIC} lowers the graphic windows, popping up the alpha
  screen.
\item{\tt CLEAR PLOT} is equivalent to either {\tt CLEAR TREE} or {\tt
    CLEAR WHOLE}, depending on user preference as expressed in command
  \comm{CHANGE}{CLEAR}.
\item{\tt CLEAR SEGMENT} destroys the last graphic segment, i.e.  the part
  of the plot corresponding to the last graphic command.  Can be used
  repetitively.
\item{\tt CLEAR SEGMENT SegName} makes the named segment invisible.
  This is equivalent to {\tt CHANGE VISIBILITY NameSeg OFF}.
\item{\tt CLEAR TREE} deletes the current directory, that is all
  subdirectories descending from the current main directory. Other parallel
  trees (if any) are not destroyed.
\item{\tt CLEAR WHOLE} deletes the whole plot, all main directories and
  their subdirectories. A new (empty) main directory is automatically
  recreated with one associated window.
\item{\tt CLEAR WINDOW} delete the current graphic window.  Beware that
  this may leave the plot with no associated window.
\end{itemize}

\subsubsection{Plotting Depth}

Another feature which is essential with the bitmap and area filling
capabilities is the \index{CHANGE!DEPTH} {\em depth} of the graphic
segment. Because bitmaps or filled areas are opaque, it is important
to know in which order they should be displayed. \greg\ uses the {\em
  depth} attribute of the graphic segment to do so. {\em Depths} range
between 1 and 40. Depth 1 is the top layer: graphic segments at depth
1 are in front of any graphic segment at other depths. Depths 40 to 2
are plotted in this order. This means that segment at depth 40 are
behind any other segment, segments at depth 39 are behind segments at
depth 38, etc\ldots Whenever segments have the same depth, they are
drawn in their chronological (creation time) order.

To save unecessary refresh of the drawing, the depth attribute is not
used when a segment is created: the segment appears on top of the
existing ones at this time. The depth attributes becomes effective
after a {\tt ZOOM} action, a redraw (e.g. window resized), or for
hardcopies. \greg\ incorporates a default handling of depths when
segment are created in the following way:
\begin{itemize}
\item Normal (vector) segments are created with depth = 1
\item Bitmaps are created with depth = 2
\item Filled areas are created with depth = 1
\end{itemize}
This allow bitmap images to be plotted in the background, with all standard
graphic overlaid on top of them without user intervention.

This behaviour is often sufficient. When not, the user can modify the depth
of any graphic segment using the command \comm{CHANGE}{DEPTH}. See the
internal help for details.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "greg"
%%% End: 
