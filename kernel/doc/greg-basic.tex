\section{GreG Manual}

\subsection{Basic Concepts}

\greg\ uses a graphic library with a virtual memory storage for all normal
plot actions. Accordingly, the ``plot'' is a notion entirely independent of
any graphic device, and it can be generated without any.  The only thing
necessary to define a plot is the {\em Plot Page} size in a conventional
unit called the Physical Unit.  Plot coordinates on the Plot Page are
called {\em Physical Coordinates}, or {\em World Coordinates}. By
convention, the Physical Unit will be called a centimeter ; this is
meaningful because when you produce a drawing from a metacode file using a
specific driver such as {\tt PLXY}, a Physical Unit will match a centimeter
if you use the {\tt /EXACT} option.

\subsubsection{Coordinate Systems}

\greg\ is often used (but far from being limited to) for plotting two
dimensional graphs showing the dependence of one variable on another. A
typical graph has data points, represented by special markers such as
diamonds or stars, and possibly with error bars, or perhaps plotted on the
same scale a theoretical model drawn as a smooth curve. The graph must be
labelled with two axes to indicate the coordinates. The other major \greg\ 
application, namely contouring, also requires such a labelling of the
plotting area.

The meaning of the coordinates is entirely defined by the user. For example
in an astronomical map it may be Right Ascension Offset in arcsec along the
X-axis and Declination Offset in arcsec along the Y-axis; a correlation
study between visual extinction and molecular content will have the visual
extinction Av as abscissa (no units) and {\tt $^{13}$CO} column density as
ordinate (molecules per square centimeter).

Throughout this document, these coordinates will be referred as the {\em
  User Coordinates}. \greg\ maps a selected region of the User Coordinate
Space onto a specified rectangle, called the {\em Box}, of the {\em Plot
  Page}.

All data plotting is done in User Coordinates and clipped in the Box,
except for the perspective algorithm which uses a specific sequence. Other
reference systems are available for annotations (see command {\tt DRAW}).

\subsubsection{The Plot Page}

\greg\ is a completely device independent graphic system. This means that a
sequence of plot actions will give exactly the same drawing on any device,
except possibly for a global scale factor (contrary to many systems where
the plot is distorted by some X/Y aspect ratio). It is then possible to
define a unique Physical Coordinate unit. By convention in \greg\, this
{\em Physical Unit} corresponds to 1 true centimeter on a paper output, if
you use the {\tt /EXACT} option in the metacode device driver. The Plot
Page is defined in terms of such units.

\index{DEVICE} A default Plot Page is defined at \greg\ initialization. It
is an A4 format page (30 by 21 cm) with the {\tt LANDSCAPE} orientation,
i.e. with the largest dimension along the X coordinate. Plot pages of other
dimensions can be defined using command \index{SET!PLOT\_PAGE}

{\tt SET PLOT\_PAGE SizeX SizeY}

Another frequently used Plot Page corresponds to the A4 format with the
{\tt PORTRAIT} orientation (long dimension corresponding to Y) which can be
accessed by command {\tt SET PLOT\_PAGE PORTRAIT}.

\subsubsection{The Viewing Window}

Only a part of the Plot Page need be displayed on the graphic device (if
any). This part is called the {\em Viewing Window}, (not to be confused
with the Box). This window can be modified at any time using the command
\com{ZOOM}. When you define a new plot page, the window is automatically
reset to the full plot page. Changing device does not modify the Viewing
Window.

This window has no effect at all on the metacode file produced by command
\com{HARDCOPY}, which always contain the full plot page.

\subsubsection{The Box}

The Box is defined by the position of its corner in Physical Coordinates in
the Plot Page by command \index{SET!BOX\_LOCATION}
{\tt SET BOX\_LOCATION Xmin Xmax Ymin Ymax}\\
The default Box for the default Plot Page can be accessed by typing {\tt
  SET BOX LANDSCAPE} or {\tt SET BOX PORTRAIT} as required.  This default
box is however usually inadequate for maps, where the User Coordinates in X
and Y may be related. The box must of course stay within the limits of the
PLOT\_PAGE, so that attempts to define a PORTRAIT page in a LANDSCAPE page
(or vice versa) will fail: an apropriate SET PLOT\_PAGE should have been
issued before the SET BOX command.

\subsubsection{The User Coordinates}

There is a single command to define the correspondence between the User
Coordinates and the Physical Coordinates.  \index{LIMITS}
{\tt LIMITS Xleft Xright Ylow Yup}\\
by which you specify the User Coordinates values at the left and right ends
of the X-axis and low and up ends of the Y-axis. Note that Xleft may be
greater than Xright. \greg\ stores all User Coordinates as Real*8 numbers
to allow high precision labelling.

If you have no specific idea about the range of values spanned by your
data, just type {\tt LIMITS}\\
\greg\ then automatically computes the extrema of the input data (read by
COLUMN see section 3.2) and adds some reasonable margin to set the limits.
You can mix automatic and fixed limits using the ``wild'' value ``{\tt *}''
{\tt LIMITS * Xright * Yup}\\
will computes automatic limits for the left end of the X axis and low end
of Y axis.

The previous examples set {\em Linear Conversion} formula between the User
Coordinates and the Physical Coordinates. You can obtain a {\em Logarithmic
  Conversion} formula for the abscissa or the ordinate \index{LIMITS!/XLOG}
\index{LIMITS!/YLOG} using the options {\tt /XLOG} or {\tt /YLOG}
respectively. Note that, contrary to most systems, \greg\ includes a {\em
  true} logarithmic conversion : you do {\em NOT} have to provide the
logarithm of your data. Also, error bars will be correctly plotted for the
logarithmic conversion : all the necessary computations are done into
\greg\ .

\subsection{The Data Structure}

\greg\ has space reserved into internal buffers for the user's data.  Four
buffers are available, those with conventional names X Y Z and the Regular
Grid array. Buffers X and Y are one dimensional arrays used to store the X
and Y data for graphs Y=f(X) (or pairs (X,Y) in some applications). Z is
also a one dimensional array and is used for storing additional values such
as error bars corresponding to the X (or Y) data. The Regular Grid array is
a two dimensional array used to store regularly spaced data (maps) for
two-dimensional applications like contouring. Additional buffers are used
for specific applications, such as the Polygon buffer.

\subsubsection{One-Dimensional Arrays X Y Z}

A single command, \com{COLUMN} is used to specify the input file, select
part of this input file, and load data in the X Y Z buffers.

\paragraph{Formatted Files :}
The input file must be organised in a Table-like way, with formatted
numbers arranged in column and lines. \greg\ uses list-directed format to
read this file, so that the numbers need only be separated by spaces, tabs
or commas.  However, a single complete logical line of the table must be
contained in a logical record of the input file. Any column, and any range
of lines, can then be accessed typing \index{COLUMN}
{\tt COLUMN X Nx Y Ny Z Nz [/FILE Infile] [/LINES Lmin Lmax]}\\
where Nx is the column number from which the X buffer is to be read (resp.
Ny and Nz), Infile is the input file name, and Lmin Lmax define the range
of lines to select. By default, the last connected file is used. Changing
the input file resets Lmin and Lmax to select the complete file. The order
of the X Y Z keywords is not compulsory, and some may be missing ; however
each keyword must be immediately followed by its associate column number.
There is an implementation dependent limit (usually 50000) on the number of
lines that can be read by one COLUMN command in a formatted file. To go
over that limit, you can convert the formatted file to a table (usually a
good idea), or work by pieces using /LINES.

\paragraph{Table Format :}
Tables are ensembles of columns strictly equivalent to the formatted files.
The only difference is that they are unformatted, and hence access time is
typically 50 times faster... The number of lines is fixed, but the number
of columns may be extended indefinitely. In fact, a table may be consider
as a 2-D image (and vice-versa if you want) but does not require ``axis''
information... Tables can be accessed in command {\tt COLUMN} using option
{\tt /TABLE} \index{COLUMN!/TABLE} instead of {\tt /FILE}.  Using tables is
recommended whenever a large number of rows or columns. Two programs can be
used to convert from tables to formatted files ({\tt GILDAS\_RUN:LIST.EXE})
and vice versa ({\tt GILDAS\_RUN:TABLE.EXE}).

\subsubsection{Two-Dimensional Regular Grid Array}

For mapping purpose, one need to store RG=f(I,J) data into the Regular Grid
array and to define a linear correspondence between the User Coordinate
Space and the indices I and J, X=x(I) and Y=y(I). All this is done using
command \com{RGDATA} to read a purposely formatted file. Contrary to the X
Y Z buffers which are Real*8, the Regular Grid RG array is declared Real*4
as there is usually no meaning in defining contours with very high
precision.

The Regular Grid array is loaded using command \index{RGDATA}
\index{RGDATA!/VARIABLE}
{\tt GREG2$\backslash$RGDATA Name [/VARIABLE]}\\
where Name is a variable or the input file name if the {\tt /VARIABLE}
option is not present.

\paragraph{Variables and Images:}

The \sic\ image format is the equivalent for maps of the table format for
columns. Images are regularly sampled 2-d, 3-d or 4-d data which require
conversion formula for the coordinates along each axis.  Data values are
real (single precision) or double precision.  Access time to \sic\ images
is typically 20 times faster than the {\tt RGDATA} format. \greg\ can read
images using the \sic\ command {\tt DEFINE IMAGE} (see the \sic\ manual for
details).  Images are then available as multi-dimensional variables, whose
names can be specified as argument to a {\tt RGDATA/VARIABLE} command.  Any
information about projection, coordinate system, etc\ldots associated with
the image is given to \greg\ to define astronomically correct plots.  The
{\em GRAPHIC} program, a superset of \greg, also incorporates an {\tt
  IMAGE} command for similar purpose.  \greg\ can write Images, by saving
the Regular Grid array as an image (see \comm{WRITE}{IMAGE}). Header
information (conversion formulae, projection information...) is written
according to the current parameters defined by command {\tt SET}.

\paragraph{RGDATA File Format :}

A somewhat obsolete way of initializing the RGDATA array is rge so-called
``RGDATA file format''. Suitably formatted files can be read by the {\tt
  RGDATA} command when no {\tt /VARIABLE} option is specified.  A header is
read first to find the array dimensions, and then the array, using a
user-specified format. The default format is {\tt Z8.8} and can be changed
using the {\tt /FORMAT Expression} option.  \index{RGDATA!/FORMAT}. {\tt
  Expression} must be a valid FORTRAN format.  It is possible to select
only a subset of the input array, using the \index{RGDATA!/SUBSET} option
{\tt /SUBSET IX1 IY1 IX2 IY2}, where IX1 and IY1 are the pixel values of
the bottom left corner, IX2 IY2 those of the top right corner of the area
to be selected.

The input file for the RG buffer must be a fixed length formatted
sequential file with 80-Bytes record length. The first four records are
used to describe the correspondence between indices and User Coordinates
and must give the following values

\begin{itemize}
\item{\tt Record 1 NX XREF XVAL XINC}\\
  describing the X axis Coordinates, corresponding to indice I, where
  \begin{itemize}
  \item NX is the number of (I) pixels
  \item XREF is the X-axis reference pixel. XREF is a real, that is one can
    place the reference pixel on non-integer values.
  \item XVAL is the User Coordinate (abscissa) at the reference pixel
  \item XINC is the User Coordinate increment per pixel along the X-axis,
    which can be negative.
  \end{itemize}
\item{\tt Record 2 ``Any comment you want for the X axis'}\\
  is a comment line for bookkeeping. It may be empty but must be present.
  This comment will be written by \greg\ when it finds it (as an
  alphanumeric comment, not on the plot). It may help you remember the X
  coordinate type for example, or simply the file content.
\item{\tt Record 3 NY YREF YVAL YINC}\\
  similar to record 1, but for the Y axis Coordinate (indice J).
\item{\tt Record 4 ``Any comment you want for the Y axis''}\\
  similar to record 2.
\end{itemize}
All the following records contain the RG array, written in the standard
Fortran ordering (I varies first) in format 20A4. The variables XREF, XVAL,
XINC, YREF, YVAL and YINC are declared Real*8 to provide accurate
conversion formulae. The conversion formulae are thus
\begin{verbatim}
         X(I) = XINC*(I-XREF)+XVAL
         Y(J) = YINC*(J-YREF)+YVAL
\end{verbatim}
respectively for X and Y coordinates.

\subsubsection{Using Variables}

Instead of reading data into the X, Y, Z or Regular Grid buffers, it is
possible to use directly \sic\ variables in some \greg\ commands.
Currently, the following commands support \sic\ variables as extra
arguments :
\begin{verbatim}
      GREG1\CONNECT [Xv Yv]
      GREG1\CURVE [Xv Yv] [/VARIABLE Z [Zv]]
      GREG1\HISTOGRAM [Xv Yv]
      GREG1\ERRORBAR Type [Xv Yv Zv [Orv]]
      GREG1\POINTS [Xv Yv] [/SIZE maxsize [Zv]]
      GREG1\VALUES [Xv Yv Zv]
\end{verbatim}
where, if not specified, the Xv Yv Zv variables default to the X Y Z
buffers, which are known as \sic\ variables of the same names.

\sic\ variables are defined using command {\tt SIC$\backslash$DEFINE}, and
can be external tables or images. See \sic\ documentation, or use {\tt HELP
  DEFINE} within \greg\ for more details.

\index{RGDATA!/VARIABLE} The option {\tt /VARIABLE} of command {\tt RGDATA}
indicates that the argument is the name of a known \sic\ variable, instead
of a file name.  This can be used to contour directly 2-D \sic\ variables.
Moreover, the Regular Grid is known as a \sic\ variable named RG, hence it
is possible to assign values to RG within \greg\ . This does not modify the
pixel to user coordinates conversion formula.

\subsection{The Pen Attributes}

\index{PENCIL} \greg\ can draw different style of lines : solid lines or
dashed ones, thin or thick ones, dim or bright ones, and possibly coloured
ones on devices supporting this possibility. All these capabilities are
controlled by means of virtual pen attributes.

\subsubsection{Pen definition}

A virtual pen is defined by its conventional number (0 to 15), and its
attributes. Currently, the attributes can be
\begin{itemize}
  \index{PENCIL!/COLOUR}
\item{COLOUR}, an integer code for the colour. The integer can take values
  from 0 to 15 with undefined conventions, because the colour table is
  dynamically selectable on some devices.  (e.g. ARGS). On a non-color
  device with default terminal settings, it corresponds to an intensity
  level.  0 is the brightest available intensity. Values 1 to 15 correspond
  to increasing intensities, scaled to cover the full range of available
  intensities on the current device. As most devices have three intensity
  levels, 1-5 will give the dimmest intensity, 6-10 the intermediate one
  and 11-15 the brightest (i.e. the same as 0).
  
  \index{PENCIL!/DASHED}
\item{DASHED} pattern, an integer from 1 to 7 with the following
  significance
\begin{enumerate}
\item Solid line
\item Short dashed line
\item Dotted line
\item Short dash - Dot line pattern
\item Long dashed line
\item Long dash - Dot
\item Short dash - Long dash
\end{enumerate}
The first four attributes corresponds to the standard GKS (Graphic Kernel
System) convention, and the hardware generation of the output device may be
used. It is not used for the other ones.  \index{PENCIL!/WEIGHT}
\item{WEIGHT}, an integer from 1 to 5 giving the thickening factor of the
  line. Hardware generation may be used.
\end{itemize}

A pen is loaded and defined by command 
{\tt PENCIL Ipen /DASHED Idash /WEIGHT Iweig /COLOUR IColo /DEFAULT}\\
All subsequent plots will be done with this virtual pen, until a new
command \com{PENCIL} is issued. If the attributes are not given, the
previous ones (resulting from the last definition) are used. If a device
has not the required capability, the code is ignored. The {\tt /DEFAULT}
\index{PENCIL!/DEFAULT} option can be used to reset the default settings of
the attributes which are not specified. If no pen number is specified, the
{\tt /DEFAULT} option reset all default attributes to every pen.

\subsubsection{Pen Usage and Default Settings}

Pen number 0 is preset with no thickening, colour 0, solid line.  Pen
number 1,2,3 are preset with no thickening, colour 4,8,12 respectively (in
order to match the three intensity levels available on most devices) and
solid line. Pen number 15 is preset with no thickening, colour 0 dashed
pattern number 2. All other pens have the same default attributes as Pen 0.

In general, a plot command uses the current pen with all its attributes.
Exceptions are
\begin{itemize}
\item The dashed pattern is ignored by the character plotting routine
  (commands {\tt LABEL} and {\tt DRAW TEXT})
\item The dashed pattern is ignored by the marker plotting routine
  (commands {\tt POINTS, DRAW MARKER} and {\tt DRAW ARROW})
\item The dashed pattern is ignored by the axis plotting routine.
  (commands {\tt BOX} and {\tt AXIS})
\item Contour plots use Pen number 0 for positive contours and pen number
  15 for negative ones (command {\tt RGMAP}), unless specified in option.
  This is described in more detail later.
\end{itemize}

\subsection {Characters and Markers}

For labelling and annotations of the plots, \greg\ includes a complex
character set with special symbols, and the possibility of plotting any
regular polygon. The characters and markers are always drawn without the
dashed pattern, as mentioned above.

\subsubsection{The Character Set}

\index{SET!FONT} \greg\ includes two basic character sets generated by
software.  The {\tt SIMPLEX} set is the default for interactive devices.
The {\tt DUPLEX} set is nicer, but slower and requires a higher resolution
device ; typically it will be reserved for quality publication plots.
Except for the aspect of the characters, there is no difference between the
{\tt SIMPLEX} and {\tt DUPLEX} fonts, that is the sizes and proportional
spacings are identical.  The default font may be overridden by {\tt SET
  FONT SIMPLEX or DUPLEX}.  In each quality, complete roman, script and
greek alphabets are available.  Special characters are used to access
specific symbols not available on a keyboard, including mathematical and
astronomical symbols. The table of correspondence is shown in Section 7. As
a rule, astronomical symbols are obtained in the greek character set, and
mathematical symbols in the script set, the roman set having of course an
exact correspondence with the keyboard, except for the double quote (key
not available because of \sic\ character handling) which is obtained using
the counter-quote, degree symbol which is obtained using the circumflex
symbol, and the back-slash which is not available.

\subsubsection{Handling Text Strings}

A text string (plotted by command \com{LABEL} or \comm{DRAW}{TEXT}) can
include the escape character ``$\backslash$'' which is used to generate
superscripts and subscripts and to monitor the font used. This escape
character may be used in 2 ways
\begin{verbatim}
        \\X - set mode X until other mode change sequence
        \X  - set mode X for next character only
\end{verbatim}
where X can be any of the following values and causes the following actions
:
\begin{verbatim}
        N - Switch to current default character set
        1 - Switch to Simplex character set
        2 - Switch to Duplex character set         
        R - Roman font
        G - Greek font
        S - Script font
        I - Toggle italics
        U - Increase superscript/subscript level (move Up)
        D - Decrease superscript/subscript level (move Down)
        B - Backspace over previous character
\end{verbatim}
Superscript and subscripts are handled using a ``level'' philosophy.
$\backslash$U increases the level by 1, $\backslash$D decreases it by 1 and
the size of the characters is (0.7)**ABS(level) times the current defined
size.  The italic mode is toggled by the $\backslash$I escape code. The
character size is defined using command \comm{SET}{CHARACTER}.

\subsubsection{Centering Facility}

Labels may be automatically positioned with respect to a given position.
This is done using the Centering code, which can be specified in
\index{LABEL!/CENTERING} the {\tt /CENTERING} option of command {\tt
  LABEL}, as the 5-th argument of command \comm{DRAW}{TEXT}, or defaulted
to a value set by command \comm{SET}{CENTERING} N.  This code N is an
integer in the range [0-9] with the following meaning :
\begin{itemize}
\item 1 to 9 : The character string is put in position 1 to 9 with respect
  to the current pen position ({\tt LABEL}) or coordinates specified ({\tt
    DRAW TEXT}).  The position code corresponds to a standard numeric
  keypad as on most terminals, 1 being lower left, 2 lower middle, etc...
\item 0 : For command {\tt LABEL}, it corresponds to code 6, i.e. the label
  is just written on the right of the current position. For command {\tt
    DRAW TEXT}, the centering is computed according to the location of the
  current position with respect to the Box. This location gives a code in
  range 1-9 with the same conventions as above. Hence a string written at
  the left of the box will be automatically left adjusted (code 4) while a
  string written inside the box will be centered and so on.
\end{itemize}

\subsubsection{Graphic Markers}

\index{SET!MARKER} In addition to the character set which includes symbols,
\greg\ can plot any regular polygon under 4 different styles of any size.
The current type of graphic marker is set by command
{\tt SET MARKER Nsides Istyle Size}\\
where Nsides is the number of sides (or of summits) of the regular polygon,
Size is the diameter of the marker in physical units, and Istyle defines
one of the four possible styles according to
\begin{itemize}
\item 0 - Regular empty polygon
\item 1 - Vertex connection only. The sides of the polygon are not drawn,
  but the radii connecting the polygon center to the summits are plotted.
\item 2 - Fancy starred marker. The star with Nsides summits is drawn.  The
  inner radius of the star is half the outer radius.
\item 3 - Filled regular polygon. The filling may be a rather slow process
  on some devices. Also the filling may fail for very high resolution
  supports (e.g. a pen plotter with a very thin pencil).  If this happens,
  please notify the authors of \greg\ .
\end{itemize}
The number of sides is not limited, but the larger this number, the slower
the plot will be...

\subsection{GREG1 : One Dimensional Problems}

By ``one dimensional data'' we mean any distribution of (X,Y) data points.
This obviously includes functions Y=f(X) and X=f(Y), but also more general
cases. Plotting such kind of data is the simplest thing \greg\ can do.
This can be done basically in four modes :
\begin{enumerate}
\item The broken line mode using command \com{CONNECT}, in which data
  points are simply connected by straight lines. Note that data must be
  ordered.
\item The curve mode using command \com{CURVE}, in which the data is
  interpolated to produce a smooth aspect. Again, the data must be
  adequately sorted.
\item The histogram mode using command \com{HISTOGRAM}. As above, data must
  be ordered. Note that this capability is restricted to functions Y=f(X).
\item The marker mode using command \com{POINTS}, in which a graphic marker
  is plotted at each data point. The size of the markers may depend on
  values in the Z buffer. Contrary to the other commands, the data need not
  be ordered at all.
\end{enumerate}

In addition to these four commands, the command \com{ERRORBAR} which uses
the Z buffer to define errors may also be relevant to one-dimensional data.
A facility to sort data by ascending order is given by means of command
\com{SORT}.

All these commands belong to the so-called ``GREG1'' language (in the \sic\ 
meaning) ; this language also includes all the utility functions of \greg\,
such as pen definitions, annotations, coordinate conversions, etc...

\subsection{GREG2 : Two Dimensional Problems}

We call ``two dimensional data'' any data represented by a function f(X,Y).
In practice, this representation may be regularly or randomly sampled. The
first case corresponds to the Regular Grid array for which RG=f(I,J) and
linear conversion formulae exist to convert between I and X and J and Y
respectively. The second corresponds to triplets (X,Y,Z) stored in the
corresponding buffers. Basic handling of 2-D data includes contouring,
perspective view, bitmap processing, and extraction of one dimensional
data.

Only regularly sampled 2-D data (i.e. a Regular Grid) can be conveniently
handled. The limits of a Regular Grid can be defined as the plot limits
using command \comm{LIMITS}{/RGDATA}. In this case, \greg\ computes the
user coordinates corresponding to pixels 0.5 and NX+0.5 in X (same in Y)
and defines these values as the user coordinate limits. This means that the
Box will exactly cover the {\em Image} corresponding to the given map.

For randomly sampled data, \greg\ provides a way of interpolating them
\index{RANDOM\_MAP} on a Regular Grid by command {\tt RANDOM\_MAP}. All
other commands are provided to work exclusively on the Regular Grid array.
This includes :
\begin{itemize}
\item A general contouring facility using command \com{RGMAP}
\item A simple perspective view using command \com{PERSPECTIVE}
\item A bitmap display capability for X-Window terminals and PostScript
  output, command \com{PLOT}
\item A resampling facility either to produce finer contours, or to produce
  maps with the same sampling, and hence comparable pixel by pixel.
\item Limited map analysis facilities : pixel value interrogation with
  command \comm{DRAW}{VALUE}, statistics using command \com{MEAN}, masking
  part of a map using command \com{MASK}, computations of global and local
  extrema using command \com{EXTREMA}, strip extraction using command
  \com{STRIP}.  For more general image processing facilities, the user is
  referred to the {\em GILDAS} documentation.
\end{itemize}

All these commands belong to the ``GREG2'' language.

\subsection{Astronomical Mapping}

\begin{center} 
  ``No matter how tricky you are, you cannot pave a sphere with square tiles.''\\
  (Attributed to A. Einstein)\\
\end{center}

About $101 \%$ of the difficulties encountered in astronomical mapping
comes from this very profound evidence. The remaining few percents (within
the errors) come from the multiplicity of coordinate systems used by the
astronomers. For small field mapping, the sphere can reasonably be
approximated by its tangent plane at the field center, but for fields
larger than a few degrees (depending on the required positional accuracy),
curvature effects become important.

As spherical plotters are not easily commercially available (and their
output hardly accepted by journal editors), the big problem is to represent
part of a sphere on a plane sheet.  \greg\ offers several facilities to
deal with this problem, by means of commands \com{PROJECTION},
\com{CONVERT} and \com{GRID} in the {\tt GREG2} language.

The current philosophy to handle this problem is to make all plotting {\em
  in relative coordinates on the projection plane}. No drawing is done in
absolute coordinates on the sphere. This was decided because the plot page
is plane indeed. Another apparently restrictive convention in \greg\ 
projections is that all angles are internally in the natural angular unit
Radian. Accordingly, when you give a map to be contoured, the map
coordinates should be offsets in radians from the projection center. For
user convenience, it is possible to specify limits in Degrees, Minutes or
Seconds using the command {\tt SET ANGLE\_UNIT}, but
\index{SET!ANGLE\_UNIT} this command has no effect on internal conversion
formulae. Again, the rationale behind this convention is that we are in
fact working in the projection plane, where angles have no meaning but only
their projections remain.

However, it is {\em always possible to bypass} these {\em apparent}
restrictions provided your understand what a conversion formula and a
projection are. As an example is always much better than lengthy
discussion, assume you want to overlay a contour map to the Equatorial and
Galactic grids relevant to the mapped area. Unfortunately, the map
coordinates are in Degrees from a point situated at (0.5,0.5) (``degrees''
in projection) from the projection center (note that this sentence is
complete nonsense, because you cannot have ``degrees'' in the projection
plane...)
\begin{verbatim}

SET ANGLE_UNIT/DEFAULT                          ! (1)
LIMITS 5 -5 -5 5                                ! (2)
RGDATA MYMAP                                    ! (3)
LEVELS -1 1 TO 5                                ! (3)
RGMAP/BLANKING -2 .1                            ! (3)
!
SET SYSTEM EQUATORIAL                           ! (4)
PROJECTION 6:25:30 35:40.5 0.0/TYPE GNOMONIC    ! (5)
SET ANGLE_UNIT DEGREES                          ! (6)
LIMITS 4.5 -5.5 -5.5 4.5                        ! (7)
SHOW LIMITS                                     ! (7)
PEN 1                                           ! 
GRID 2 2                                        ! (8)
PEN 2                                           !
GRID 2 2/ALTERNATE                              ! (9)
\end{verbatim}
\begin{enumerate}
\item Make sure to work as if you had no problem at all
\item Define the map limits. As you are not worrying about the angular
  units, the internal limits will be just what you type.
\item Read your map and draw your contours. This is the usual process for
  any map.
\item Here start the specific astronomical problem.  You know that your
  coordinate system is Equatorial. Specify it.
\item Define the projection in this system. Note that Right Ascension is in
  Hours, and Declination in Degrees as usual for Equatorial coordinates.
  The projection type depends on your problem of course.
\item Specify that your will {\em now be giving limits in Degrees}
\item Give the limits of the map with respect to the projection center.
  The system automatically converts the values typed in to internal limits
  in radians, as you can check by the {\tt SHOW} command.  Note how you
  handle the shift between the (0,0) of your map and the projection center
  which is always the (0,0) of the projection plane (compare with command
  (2))
\item Everything is now correct to plot the grid of meridians and parallels
  over your map.
\item You may even plot a {\em Galactic} grid just by specifying the {\tt
    /ALTERNATE} option in the {\tt GRID} command.  \index{GRID!/ALTERNATE}
\end{enumerate}
As shown in this example, the {\em only} effect of the {\tt SET
  ANGLE\_UNIT} command is to force automatic conversion of the typed limits
to radians. The same result would have been obtained by typing
{\tt LIMITS 4.5 -5.5 -5.5 4.5 DEGREE}\\
instead of commands (6) and (7).

You can work in three different systems :
\begin{itemize}
\item{\tt UNKNOWN}, for which \greg\ makes no assumption at all about the
  meaning of the unprojected coordinates. In particular, there is no
  alternate system in this case.
\item{\tt EQUATORIAL} with a custom equinox (\greg\ assumes to be at
  equinox 2000.0 by default). The alternate system is {\tt GALACTIC}
  of course.
\item{\tt GALACTIC}, with {\tt EQUATORIAL 2000.0} system as alternate.
\end{itemize}

When you measure positions with the cursor in the last two systems, both
galactic and equatorial coordinates are given.  For user convenience,
sexagesimal notations in Hours Minutes Seconds for Right Ascension and
Degrees, Minutes Seconds for Declination are used, while decimal notations
with angle in degrees are used for L and B (as well as if the system is
{\tt UNKNOWN}).

Approximate absolute labelling in Hours and Degrees (sexagesimal notation)
can be obtained for the Box by specifying the option {\tt /ABSOLUTE} to
command {\tt BOX}\index{BOX!/ABSOLUTE} if the system is {\tt EQUATORIAL}.
If the system is {\tt GALACTIC} or {\tt UNKNOWN}, this option produces
absolute labelling in Degrees. Note that it is only meaningful for small
fields of view.  Without this option, {\tt BOX} produces relative labelling
in current angle units.

In addition, command \com{CONVERT} can convert absolute positions or
projected coordinates from a different projection (in any angular units),
to projected coordinates (in radians) in the current projection. This
command may be used to plot star positions on a map, and so on.

\subsection{The Blanking Capability}

Experimental data are seldom completely sampled. For example, a
spectrometer may have a bad channel, an image bad pixels and so on. To
handle this problem, \greg\ includes the notion of {\em Blanking Value}.
Most \greg\ data functions handle this blanking value which is defined
using command \index{SET!BLANKING} {\tt SET BLANKING Bval Eval}\\
where Bval is the blanking value and Eval the tolerance for blanked data,
necessary because of limited accuracy. Once a blanking value has been
defined, all functions liable to handle it effectively use it.  The default
value can be overridden using the appropriate option {\tt /BLANKING}.
However some functions which should do it still do not handle blanked
values ; this currently includes {\tt PERSPECTIVE}.

To turn off the blanking value checking, you must specify a negative value
for Eval, or simply use the command {\tt SET BLANKING/DEFAULT}.

The blanking value can be used in two different ways :
\begin{enumerate}
\item Avoiding bad points in a curve. In this case it is assumed that
  points with the Y value blanked are meaningless. Note that this feature
  allows you to plot a complete set of curves in a single operation : you
  just need to specify a blanked point to separate each curve.
\item Avoiding bad pixels in an image. In this case, pixels with the
  blanked value are not considered (for example in command {\tt MEAN})
\end{enumerate}
One command considers both aspects at the same time : this is command
\index{RGMAP!/KEEP} {\tt RGMAP/KEEP Blanking\_bis}, for which the default
blanking is used to check bad pixels and the Blanking\_bis value to force
pen up between contours (or parts of a single contour).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "greg"
%%% End: 
