!-----------------------------------------------------------------------
!  Demonstration procedure for command SIC\MFIT
!  MFIT is a sic-only feature, but this demo also performs some plots
!  to display resulting fits.
!-----------------------------------------------------------------------

begin procedure plot_legend
  g\draw relo 0 &1 /user
  g\draw line 10 &1 /user
  g\draw text 13 &1 "&2" 6 /user
end procedure plot_legend

begin procedure plot_model
  g\set /def
  g\clear
  ! Plot the model and data points
  g\limits /var xdata ydata
  g\box
  g\label "X" /x
  g\label "Y" /y
  g\set marker 4 0 .2 0.
  g\points xdata ydata
  g\connect xdata ymodel
  @ plot_legend &1 "Model"
end procedure plot_model

begin procedure plot_fit
  ! Plot the fit
  g\connect xdata mfit%fit
  @ plot_legend &1 "&2"
end procedure plot_fit

! --- Define a model ---------------------------------------------------
define real ampli pulsa phase slope
let ampli 2.0
let pulsa 0.2
let phase 1.0
let slope 0.1
message i demo "Model parameters are:"
say "  Amplitude: " 'ampli'  /format A F0.3
say "  Pulsation: " 'pulsa'  /format A F0.3
say "  Phase:     " 'phase'  /format A F0.3
say "  Slope:     " 'slope'  /format A F0.3
!
define integer np
let np 100
define real xdata[np] ymodel[np] ydata[np] /global
for idata 1 to np
  let xdata[idata]  idata
  let ymodel[idata] ampli*sin(idata*pulsa+phase)+idata*slope
  let ydata[idata]  ymodel[idata]+noise(0.4)
next
!
@ plot_model 11
!
! --- Fit (from command line) ------------------------------------------
say ""
message i demo "Fit from command line:"
s\mfit ydata=&a*sin(xdata*&b+&c)+xdata*&d
!
g\pen /co 1
@ plot_fit 10 "Fit (from command line)"
g\pen /co 0
!
! --- Fit (from file) --------------------------------------------------
say ""
message i demo "Fit from file:"
begin data demo-mfit.grf
  ydata =
    &a * sin(xdata*&b+&c) +
    xdata*&d
end data demo-mfit.grf
s\mfit demo-mfit.grf
sic delete demo-mfit.grf
!
g\pen /co 3 /dash 2
@ plot_fit 9 "Fit (from file)"
g\pen /co 0 /dash 1

! --- Cleaning ---------------------------------------------------------
delete /var xdata ymodel ydata
