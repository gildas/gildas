! Procedure DEMO-FONT.GREG to display the GreG fonts
! Generates the 3 alphabets of GreG fonts in 6 columns
!    Roman    Greek    Script
!    Duplex   Duplex   Duplex
!
begin procedure ff.greg
  ! Draws a character in all GreG fonts and quality
  ! Parameter &amp;1          Y position of character
  ! Parameter &2          Character to be drawn
  !
  G\DRAW TEXT  0.5 &1 "\\1&2"
  G\DRAW TEXT  1.5 &1 "\\1\\G&2"
  G\DRAW TEXT  2.5 &1 "\\1\\S&2"
end procedure ff
!
SIC VERIFY ON
G\SET PLOT_PAGE PORTRAIT
G\SET BOX 0.3 10.3 1 28
G\LIMITS 0 3 1 49
G\SET COORDINATE USER
G\SET CENTERING 5
DEFINE REAL TX[3] TY[3]
LET TX 0.5 1.5 2.5
LET TY 50
DEFINE CHARACTER TITLE*12[3]
LET TITLE[1]    "\\1Roman"
LET TITLE[2]    "\\1Greek"
LET TITLE[3]    "\\1Script"
DEFINE CHARACTER ICHAR*1[96]
LET ICHAR[01]  " "     ! Space         Space                   Space
LET ICHAR[02]  "!"     ! !             Aries                   Existencial
LET ICHAR[03]  "`"     ! Double quote  Taurus                  Up-Down arrow
LET ICHAR[04]  "#"     ! #             Gemini                  Included in
LET ICHAR[05]  "$"     ! $             Cancer                  Reunion
LET ICHAR[06]  "%"     ! %             Leo                     Includes
LET ICHAR[07]  "&"     ! &             Virgo                   Intersection
LET ICHAR[08]  "'"     ! '             Libra                   Belongs to
LET ICHAR[09]  "("     ! (             Scorpius                Integral
LET ICHAR[10]  ")"     ! )             Sagittarius             Circular Integral
LET ICHAR[11]  "*"     ! *             Capricornus             Multiply
LET ICHAR[12]  "+"     ! +             Aquarius                Plus/Minus
LET ICHAR[13]  ","     ! ,             Pisces                  Divide
LET ICHAR[14]  "-"     ! -             Space                   Minus/Plus
LET ICHAR[15]  "."     ! .             Square                  .
LET ICHAR[16]  "/"     ! /             Space                   Square root
LET ICHAR[17]  "0"     ! 0             Sun                     Zero
LET ICHAR[18]  "1"     ! 1             Mercury
LET ICHAR[19]  "2"     ! 2             Venus
LET ICHAR[20]  "3"     ! 3             Earth
LET ICHAR[21]  "4"     ! 4             Mars
LET ICHAR[22]  "5"     ! 5             Jupiter
LET ICHAR[23]  "6"     ! 6             Saturn
LET ICHAR[24]  "7"     ! 7             Uranus
LET ICHAR[25]  "8"     ! 8             Neptun
LET ICHAR[26]  "9"     ! 9             Pluto
LET ICHAR[27]  ":"     ! :             Moon quarter            Space
LET ICHAR[28]  ";"     ! ;             Comet                   Perpendicular
LET ICHAR[29]  "<"     ! <             Eight branch star       Less or equal
LET ICHAR[30]  "="     ! =             Ascending node          Equivalent
LET ICHAR[31]  ">"     ! >             Descending node         Greater or equal
LET ICHAR[32]  "?"     ! ?             Space                   Different
LET ICHAR[33]  "@"     ! @@             Space                   Proportional
LET ICHAR[34]  "A"     ! A             Alpha
LET ICHAR[35]  "B"     ! B             Beta
LET ICHAR[36]  "C"     ! C             Chi
LET ICHAR[37]  "D"     ! D             Delta
LET ICHAR[38]  "E"     ! E             Epsilon
LET ICHAR[39]  "F"     ! F             Phi
LET ICHAR[40]  "G"     ! G             Gamma
LET ICHAR[41]  "H"     ! H             Eta
LET ICHAR[42]  "I"     ! I             Iota
LET ICHAR[43]  "J"     ! J             Nabla (Gradient)
LET ICHAR[44]  "K"     ! K             Kappa
LET ICHAR[45]  "L"     ! L             Lambda
LET ICHAR[46]  "M"     ! M             Mu
LET ICHAR[47]  "N"     ! N             Nu
LET ICHAR[48]  "O"     ! O             Omicron
LET ICHAR[49]  "P"     ! P             Pi
LET ICHAR[50]  "Q"     ! Q             Theta
LET ICHAR[51]  "R"     ! R             Rho
LET ICHAR[52]  "S"     ! S             Sigma
LET ICHAR[53]  "T"     ! T             Tau
LET ICHAR[54]  "U"     ! U             Upsilon
LET ICHAR[55]  "V"     ! V             Space
LET ICHAR[56]  "W"     ! W             Omega
LET ICHAR[57]  "X"     ! X             Xi
LET ICHAR[58]  "Y"     ! Y             Psi
LET ICHAR[59]  "Z"     ! Z             Zeta
LET ICHAR[60]  "["     ! [             Dagger                  Approximately
LET ICHAR[61]  "\"     ! Unaccessible because this is the " escape" character
LET ICHAR[62]  "]"     ! ]             Double dagger           Approximately
LET ICHAR[63]  "^"     ! Degree        Paragraph               Chemical eq.
LET ICHAR[64]  "_"     ! Space         Three dots              Left/Right arrow
LET ICHAR[65]  "`"     ! Double quote  Taurus                  Up/Down arrow
LET ICHAR[66]  "a"     ! a             alpha
LET ICHAR[67]  "b"     ! b             beta
LET ICHAR[68]  "c"     ! c             chi
LET ICHAR[69]  "d"     ! d             delta
LET ICHAR[70]  "e"     ! e             epsilon
LET ICHAR[71]  "f"     ! f             phi
LET ICHAR[72]  "g"     ! g             gamma
LET ICHAR[73]  "h"     ! h             eta
LET ICHAR[74]  "i"     ! i             iota
LET ICHAR[75]  "j"     ! j             Partial derivative
LET ICHAR[76]  "k"     ! k             kappa
LET ICHAR[77]  "l"     ! l             lambda
LET ICHAR[78]  "m"     ! m             mu
LET ICHAR[79]  "n"     ! n             nu
LET ICHAR[80]  "o"     ! o             omicron
LET ICHAR[81]  "p"     ! p             pi
LET ICHAR[82]  "q"     ! q             theta
LET ICHAR[83]  "r"     ! r             rho
LET ICHAR[84]  "s"     ! s             sigma
LET ICHAR[85]  "t"     ! t             tau
LET ICHAR[86]  "u"     ! u             upsilon
LET ICHAR[87]  "v"     ! v             Infinite
LET ICHAR[88]  "w"     ! w             omega
LET ICHAR[89]  "x"     ! x             xi
LET ICHAR[90]  "y"     ! y             psi
LET ICHAR[91]  "z"     ! z             zeta
LET ICHAR[92]  "{"     ! {             Large {                 Up arrow
LET ICHAR[93]  "|"     ! Vertical bar  Large integral          Right arrow
LET ICHAR[94]  "}"     ! }             Large }                 Down arrow
LET ICHAR[95]  "~"     ! ~             Large root              Left arrow


SYMBOL S0 "G\SET CHARACTER 0.5"
SYMBOL S1 "G\SET CHARACTER 0.6"
SYMBOL H "G\DRAW TEXT TX TY TITLE"
SYMBOL F "@ ff"
!
!    Keyboard    ROMAN FONT    GREEK FONT     SCRIPT FONT
S1
H
S0
FOR I 48.5 to 1.5 by -1
  F 'i' 'ICHAR[i]'
NEXT
G\BOX N N N
G\RULE Y /MINOR
G\RULE Y /MAJOR
G\RULE X /MAJOR

G\SET BOX 10.5 20.5 1 28
S1
H
S0
FOR I 95.5 to 49.5 by -1
  F 'i-47' 'ICHAR[i]'
NEXT
G\BOX N N N
G\RULE Y /MINOR
G\RULE Y /MAJOR
G\RULE X /MAJOR
