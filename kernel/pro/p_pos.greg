!------------------------------------------------------------------------
! P_POS.GRAPHIC
! Procedure to plot multiple contour maps of spectral line images,
! in whichever order. Does not reset user limits. Uses several
! tricks for box size.
!
! Global variables defined in DEFINE.GRAPHIC
! Calls
! P_LOAD.GREG (to load image)
! P_HEADER.GREG (to plot header information)
! P_LEVEL.GREG    (to compute automatic contour levels)
! P_SCALE.GREG  (to compute automatic scale)
!
! S.Guilloteau 10-Jan-1994
!------------------------------------------------------------------------
! ON ERROR RETURN
if (name.eq." ") then
  say "Please enter map file name (without extension) "
  let name
endif
if (type.eq." ") then
  say "Please enter map file extension "
  let type
endif
gtvl\destroy all
def real x2 dx y2 x1 y1
def double xmark ratio
def integer m n n1 n2 imark nmap kdir
def character cmark*12
def logical do_clean do_dirty do_coverage
let do_clean beam_type.eq."CLEAN"
let do_dirty beam_type.eq."DIRTY"
let do_coverage extra.eq."COVERAGE"
!
! Check frequency axis
!
define header ddd 'name'.'type' read
if (ddd%f_axis.eq.3) then
  message w pos "Transposing "'name'.'type'" to "'name'".vxy for display"
  vector\transpose 'name'.'type' 'name'.vxy 312
  let type vxy
  message w pos "Type set to vxy"
endif
dele /var ddd
!
@ p_load.greg aaa
@ p_scale.greg aaa
@ p_level.greg aaa
@ p_header.greg aaa
!
!    No limits /rg
if (aaa%ndim.le.2) then
  greg2\rgdata aaa /variable
else if (aaa%ndim.eq.3) then
  greg2\rgdata aaa[1] /variable
else
  greg2\rgdata aaa[1,1] /variable
endif
if (box_limits.eq." ") then
  greg1\limits /rgdata
else
  symbol alimits 'box_limits'
  greg1\limits 'alimits' /rgdata
endif
!
if (aspect.eq.0) then
  let ratio 1.0
else
  let ratio aspect
endif
let x2 box_xmax
let y2 box_ymax
let x1 box_xmin
let y1 box_ymin
if first.eq.0 then
  let n1 1
else
  let n1 first
endif
if last.eq.0 then
  let n2 aaa%dim[3]
else
  let n2 last
endif
if (step.eq.0) then
  let nmap (n2-n1+1)
  let step 1
else
  let nmap (n2-n1+1)|step
endif
if ratio.gt.1.d0 then           ! M > N
  let m int(sqrt(nmap*ratio+1))
  let n nint(m|ratio)
  if (m*n.lt.nmap) then
    let m = m+1
    if (m*n.lt.nmap) then
      let n n+1
    endif
  endif
else
  let n int(sqrt(nmap|ratio+1))
  let m nint(n*ratio)
  if (m*n.lt.nmap) then
    let n = n+1
    if (m*n.lt.nmap) then
      let m m+1
    endif
  endif
endif
!
! Now plot
greg1\set expand min(6.0|(m+n),1.)
let dx (x2-box_xmin)|n|ratio
let dx min((y2-box_ymin)|m,dx)
let k n1-step /new integer
!
if (do_header) then
  on error
  define real dz
  greg1\set expand 0.6
  if (page_x.gt.page_y) then
    let dz min((page_x-x2)|ratio,0.5*page_y)
    greg1\set box (page_x+x2-0.4*dz*ratio)*0.5 (page_x+x2+0.8*dz*ratio)*0.5 0.45*page_y-0.3*dz 0.45*page_y+0.3*dz
  else
    let dz min((0.5*page_x|ratio),(page_y-y2))
    greg1\set box (page_x-0.6*dz*ratio)*0.5 (page_x+0.6*dz*ratio)*0.5 (page_y+y2-0.3*dz)*0.5 (page_y+y2+0.3*dz)*0.5
  endif
  @ p_box.greg label aaa
  greg1\set expand 1.0
endif
on error
let kdir 1
for j 1 to m
  for i 1 to n
    if (do_tree) then
      gtvl\change directory <greg
      gtvl\create directory p'kdir'
      gtvl\change directory p'kdir'
      let kdir kdir+1
    endif
    greg1\set box x2+(i-n-1)*dx*ratio x2+(i-n)*dx*ratio y2-j*dx y2+(1-j)*dx
    let k k+step
    if (k.gt.aaa%dim[3]) then
      gtvl\change directory <greg
      return
    endif
    if (aaa%ndim.le.2) then
      greg2\rgdata aaa /variable
    else if (aaa%ndim.eq.3) then
      greg2\rgdata aaa[k] /variable
    else
      greg2\rgdata aaa[k,1] /variable
    endif
    let xmark aaa%convert[2,3]+(k-aaa%convert[1,3])*aaa%convert[3,3]
    if (mark.eq."CHANNEL") then
      let xmark 1.0*k
    else if (aaa%unit3.eq."RA").or.(aaa%unit3.eq."DEC") then
      let xmark 0.1*nint(xmark|sec*10.)
    else if (mark.eq."FREQUENCY") then
      let imark nint((-(xmark-aaa%veloff)*aaa%restfre|299792.458+aaa%restfre)*1e3)
      let xmark 1d-6*imark
    else
      let xmark 0.1*nint(xmark*10.)
    endif
! Plot the RGDATA array...
    if (do_bit) then
      greg2\plot /scaling linear true_scale[1] true_scale[2]
    endif
    if (do_grey) then
      greg2\rgmap quiet /abs 1.0 /grey
    endif
    if (do_contour) then
      greg2\rgmap quiet /abs 1.0
    endif
    if (box_xmin.lt.1.001*x1).and.(box_ymin.lt.1.001*y1) then
      @ p_box.greg tick aaa
    else
      @ p_box.greg no aaa
    endif
    greg1\set coord box
    let cmark 'xmark'
    greg1\draw text 0.1 -0.1 'cmark' 3 /box 7
  next
next
!
gtvl\change directory <greg
!
