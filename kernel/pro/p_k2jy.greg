!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Go procedure to convert a data cube from Jansky/Beam to Main beam
! temperature.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin procedure bright-def
  on error "return base"
  if .not.exist(bright) then
     define structure bright /global
     define double bright%kperjy bright%jyperk /global
  endif
end procedure bright-def
!
begin procedure bright-jansky
  on error "return base"
  ! Compute conversion factor between Jy/Beam and K.
  define double nu theta[2] lambda rad_per_sec
  let nu 'pro%arg[1]' ! [GHz]
  let nu nu*1e9 ! [Hz]
  let theta 'pro%arg[2]' 'pro%arg[3]'
  let lambda 2.99792458e8/nu
  ! User feedback
  message i k2jy "Wavelength: "'nint(1e5*lambda)/100'" mm, Beam: "'nint(1000*theta[1])/1000'" x "'nint(1000*theta[2])/1000'
  let rad_per_sec pi/(180*3600)
  let theta theta*rad_per_sec       ! Convert from arcsec to radian
  let theta theta/2./sqrt(log(2.0)) ! Convert from FWHM to 1/e width
  let bright%jyperk 2.0*1.38e3*pi*theta[1]*theta[2]/lambda^2
  let bright%kperjy 1/bright%jyperk
  message i k2jy "K per Jy/Beam:  "'bright%kperjy'
  message i k2jy "mJy/beam per K: "'1e3*bright%jyperk'
  ! User Feedback, if said here shows the 1/e beam width
  !!$ say "Wavelength: "'nint(1e5*lambda)/100'" mm, Beam: "'nint(1000*theta[1]/rad_per_sec)/1000'" x "'nint(1000*theta[2]/rad_per_sec)/1000'
  !!$ say "K per Jy/Beam:  "'bright%kperjy'
  !!$ say "mJy/beam per K: "'1e3*bright%jyperk'
end procedure bright-jansky
!
begin procedure bright-k2jy
  on error "return base"
  ! Load data
  define image kelvin 'name'"."'type' read
  ! Sanity check
  define character unit*12
  let unit 'kelvin%unit' /upper
  if (unit[1:1].ne."K") then  ! Check only for leading K
     message e k2jy "Input image unit is not K"
     delete /var kelvin
     return base
  endif
  ! Compute conversion factor
  @ bright-jansky 'kelvin%restfre*1e-3' 'kelvin%major*(180*3600)/pi' 'kelvin%minor*(180*3600)/pi'
  ! Define output data
  define image jansky 'name'"-jy."'type' real /like kelvin
  ! Update output header
  let jansky% kelvin%
  let jansky%unit "Jy/beam"
  let jansky%noise bright%jyperk*kelvin%noise
  let jansky%rms   bright%jyperk*kelvin%rms
  ! Update output data
  let jansky jansky%blank[1] ! Initialization for blanking values
  let jansky bright%jyperk*kelvin /where abs(kelvin-kelvin%blank[1]).gt.kelvin%blank[2]
  delete /var jansky kelvin
  ! Update extrema
  v\header 'name'"-jy."'type' /extrema
  ! Update name variable for later use
  let name 'name'"-jy"
  message w k2jy "Name set to "'name'
end procedure bright-k2jy
!
@ bright-def
@ bright-k2jy
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
