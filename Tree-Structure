***************************************************************************

                        GILDAS Tree Structure

***************************************************************************

The current tree structure was devised mainly by J.Pety, S.Guilloteau and
F.Gueth during 2002 to answer two opposite needs of GILDAS: i) keep GILDAS
unity and ii) allow more and more different developpers being responsible
for particular parts of the software. This is why was introduced modularity
through the separation between "kernel" which contain the GILDAS common
parts and "packages" which contain in separate directories every
astronomical GILDAS applications. In details, the tree structure is now
composed of the following directories:

 - admin: Set of i) scripts used to administrate GILDAS and ii) definition
          makefiles needed to build the software. Those makefiles are
          included inside all the others one spread throughout the tree.

 - cookbooks: General documentation that does not directly document a 
              special part of the GILDAS software. 

    * 30m: General documentation about the use of the 30m.

    * pdbi: General documentation about the pdbi (calibration methods,
	    potential troubles, and so on).

    * gildas: Introduction, design, and programing manuals of GILDAS
	      softwares. 

 - kernel: The GILDAS kernel. Mainly a command interpretator (SIC) with
           graphical and file input/output possibilities (GREG). Needed to
           build any other package.

    * demo: Demonstration procedures of the kernel possibilities. In the
            medium term, should also be used to make blind, consistency
            checks through a 'make check' command.

    * doc: Online and offline documentation of the kernel.

    * etc: Creation of configuration files (definition of logical
           variables, font set, graphical terminal description, lut).

    * include: Global include files: definition of general constants,
               error codes and binary data formats.

    * lib: Kernel libraries. Most hardware dependancies should be located
           in the "gio" library. Most compiler and OS dependancies should
           be located inside the "gsys" library.

       o gchar: Implement character plots.

       o gcont: Implement contour plots.

       o gcore:

       o ggtk: support of the gtk2 widget and plotting library

       o ggui:

       o gio: Implement input and output of binary formats (FITS and
              GILDAS format).

       o gkernel: overlay to the other libraries, to gather libraries
                  and other products under some systems

       o gmath: Implement mathematical methods of general interest. For the
                time being, only 1D and 2D Gaussian fitting methods used by
                tasks are here.

       o gmotif: Obsolete support of the Motif widget library

       o greg: Implement users graphical commands.

       o gsys: Implement the basic GILDAS shell capabilities.

       o gtv: Implement in-depth graphical commands (in particular
              definition of new kinds of graphical windows).

       o gwcs: World Coordinate System related routines. Currently, only
               implement some astronomical projection capabilities.

       o gwidget: generic part of the Gildas widgets,

       o gwindows: obsolete support of the MS Windows widget and plotting
                   facility,

       o gx11: obsolete support of the native X11 system for plotting,

       o gxml: obsolescent support of "XML widgets"

       o sic: Implement basic GILDAS commands (variable definitions,
              basic array mathematics, input/output, command spawning).

    * main: Main programs (mainly SIC and GREG, plus others used to setup
            the graphical configuration files).

    * pro: Procedures implementing useful plotting macros (mainly to ease
           the view of a spectra cube).

    * python: Support for the Gildas-Python binding

    * tasks: Some general interest tasks sorted by use.

 - legacy: Legacy libraries needed by some packages. It is convenient to
           have them in the GILDAS tree ready for compilation by new
           compilers. Currently contain linear algebra and minimization
           libraries: blas, lapack and slatec.

 - packages: 

   Built on top of the kernel functionnalities, they should do something
   "intelligent" on your data! In this release, they all are directed
   toward mm single-dish or interferometric data acquisition and
   reduction. They may depends on each other through the use of their
   libraries. Subdirectories are organized as follows:

    * demo: Demonstration procedures (many are missing). In the
            medium term, should also be used to make blind, consistency
            checks through a 'make check' command.

    * doc: Online and offline documentation.

    * etc: Possible definition of configuration files.

    * lib: Associated library.

    * main: Main program.

    * pro: Associated procedures.

    * tasks: Associated tasks. In two cases (map and nic), there is a "lib"
             subdirectory which should disappear in my view.

   Some of those subdirectory may be missing depending of the
   application. Current list of application is:

    o astro
    o atm       Old version of the atmospheric modelization library
    o class     Analysis of Single-Dish spectra
    o classic   Support of the Class and Clic Data Format
    o clic      Calibration of PdBI data
    o flux      Monitoring of quasar fluxes
    o gtemplate A demonstration of a Gildas program
    o map       Imaging and deconvolution of interferometric visibilities
    o mis       Millimeter Interferometry Simulator
    o mira      Calibration of 30m data
    o mrtcal    Calibration of 30m data
    o mrtholo   Conversion of 30m holographies to the Clic Data Format
    o nic       Calibration and analysis of bolometer data (obsolete)
    o point     Definition of a pointing model from data
    o telcal    Library for telescope calibrations
    o wifisyn   Wide Field Synthesis of interferometric data

 - utilities: Tools to format the FORTRAN sources and to build
              documentation.

 - contrib: external applications (i.e. made by external contributors)
            based on Gildas programs may be hosted here for 1) the best
            support from Gildas programmers and 2) a better visibility to
            Gildas users.

***************************************************************************

 - integ: the "integration branch" is not part of the sources. It is
          created at 'make' time to store "compilation" products of any
          kind. The basic idea is to separate sources from those products,
          and to allow compilation of the same sources under several
          systems. This directory contains non-system dependent products
          (e.g. ascii files), and system dependent ones (e.g. binaries)
          under one subdirectory per system (e.g. x86_64-redhat6.3-ifort).
          Finally, 'make install' will duplicate the integration branch
          (for the current system) to the target installation directory.

***************************************************************************
